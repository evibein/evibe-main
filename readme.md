# Evibe.in Main

An online platform to book party services such as decorators, artists, caterers, entertainers, places, cakes and more at best deals from reliable service providers with 100% delivery guarantee.

### Major Features
- View listings with complete information along with customer reviews
- Book instantly by paying advance amount

### Upcoming Features
- Partner dashboard
- Customer dashboard
- Customer post booking