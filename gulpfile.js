// disable notification
process.env.DISABLE_NOTIFIER = true;

var elixir = require("laravel-elixir");

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.sourcemaps = false;

/* CSS */
elixir(function (mix) {

	/* Above the fold */
	mix.styles([
		// External files
		"bootstrap/bootstrap.min.css",
		"material/material.min.css",
		"menu/jquery.menu.css",
		"responsive-slides/responsiveslides.css",
		"datetimepicker/jquery.datetimepicker.css",
		"selectize/selectize.bootstrap3.css",

		// Applications files
		"app/sprite.css",
		"app/util.css",
		"app/header.css",
		"app/footer.css",
		"app/home.css",
		"app/app.css",
		"app/popup.css",
		"app/orders.css",
		"app/campaign.css",
		"app/browsing-history.css",

		// party-bag
		"party-bag/party-bag-util.css",
		"party-bag/party-bag-sidebar.css",

		// common
		"common/item-card.css",

		// master home page
		"master/home.css",

		// resolution based
		"res/1024.css",
		"res/400_600.css",
		"res/400.css",

		"rating/star-rating.min.css",

		// Owl-Carousel
		"owl-carousel/owl.carousel.css",
		"owl-carousel/owl.theme.default.css",

		//Jquery-AutoComplete
		"jquery-autocomplete/jquery-autocomplete.css",

	], "public/css/af-home.css");

	mix.styles([
		"owl-carousel/owl.carousel.css",
	], "public/css/owl-carousel.css");

	mix.styles([
		"app/vday-landingpage.css",
	], "public/css/app/vday-landingpage.css");

	mix.styles([
		"app/occasion-home.css",
	], "public/css/app/occasion-home.css");



	mix.styles([
		"bootstrap/bootstrap-custom.css",
		"material/material.min.css",
		"menu/jquery.menu.css",
		"app/util.css",
		"app/header.css",
		"app/footer.css",
		// resolution based
		"res/1024.css",
		"res/400_600.css",
		"res/400.css",
		"master/home.css",
		"virtual-party/virtual-party-v2.css",

		//Jquery-AutoComplete
		"jquery-autocomplete/jquery-autocomplete.css",

	], "public/css/virtual-party-updated.css");


	/* Default all.css file */
	mix.styles([
		// External files
		"bootstrap/bootstrap.min.css",
		"material/material.min.css",
		"menu/jquery.menu.css",
		"responsive-slides/responsiveslides.css",
		"slider/pretty-photo.css",
		"jquery-ui/jquery-ui-1.11.4.custom.min.css",
		"jquery-ui/jquery-ui-1.11.4.custom.structure.min.css",
		"jquery-ui/jquery-ui-1.11.4.custom.theme.min.css",
		"range-slider/jquery-ui-slider-pips.css",
		"datetimepicker/jquery.datetimepicker.css",
		"rating/star-rating.min.css",
		"selectize/selectize.bootstrap3.css",
		"fotorama/fotorama.min.css",

		// Applications files
		"app/sprite.css",
		"app/util.css",
		"app/header.css",
		"app/footer.css",
		"app/home.css",
		"app/app.css",
		"app/custom-feedback.css",
		"app/popup.css",
		"app/stories.css",
		"app/recommendation.css",
		"app/orders.css",
		"app/partner.css",
		"app/campaign.css",
		"app/collection.css",
		"app/fallout.css",
		"app/browsing-history.css",

		// common
		"common/item-card.css",
		"common/profile-list-common.css",

		// party-bag
		"party-bag/party-bag-util.css",
		"party-bag/party-bag-sidebar.css",

		// Payments
		"pay/checkout/checkout.css",
		"pay/checkout/venue_deals_co.css",

		// master home page
		"master/home.css",

		// resolution based
		"res/1024.css",
		"res/400_600.css",
		"res/400.css",

		//Jquery-AutoComplete
		"jquery-autocomplete/jquery-autocomplete.css",
	]);

	mix.styles([
		"app/thank-you.css"
	], "public/css/app/page-thank-you.css");

	mix.styles([
		"app/inspiration-hub.css"
	], "public/css/app/inspiration-hub.css");

	mix.styles([
		"app/virtual-party.css"
	], "public/css/app/virtual-party.css");




	mix.styles([
		"common/profile-list-common.css",
		"common/filters.css",
		"portfolios/portfolios_util.css",
		"reviews/reviews_util.css",
		"portfolios/planner_portfolio.css",
		"portfolios/package_portfolio.css",
		"portfolios/venue_portfolio.css",
		"portfolios/trending_portfolio.css",
		"portfolios/cake_portfolio.css",
		"portfolios/decor_portfolio.css",
		"portfolios/venue_deals_portfolio.css",
		"profile/common.css",
		"profile/desktop.css",
		"profile/tab.css",
		"profile/mobile.css",
		"product/profile/common.css",
		"product/profile/desktop.css",
		"product/profile/tab.css",
		"product/profile/mobile.css",
		"owl-carousel/owl.carousel.css",
		"owl-carousel/owl.theme.default.css"
	], "public/css/app/page-profile.css");

	mix.styles([
		"common/profile-list-common.css",
		"common/filters.css",
		"results/results_util.css",
		"results/venue_results.css",
		"results/planner_results.css",
		"results/package_results.css",
		"results/cakes_results.css",
		"results/decor_results.css",
		"results/entertainment_results.css",
		"results/venue_deals_results.css",
		"list/common.css",
		"list/desktop.css",
		"list/tab.css",
		"list/mobile.css",
		"product/list/common.css",
		"product/list/desktop.css",
		"product/list/tab.css",
		"product/list/mobile.css",
		"app/ad-campaign.css"
	], "public/css/app/page-list.css");

	mix.styles([
		"home/common.css",
		"home/desktop.css",
		"home/mobile.css",
		"product/home/common.css",
		"product/home/desktop.css",
		"product/home/tab.css",
		"product/home/mobile.css"
	], "public/css/app/page-home.css");

	mix.styles([
		"partner/profile.css"
	], "public/css/app/page-partner-profile.css");

	mix.styles([
		// External files
		"bootstrap/bootstrap.css",
		"datetimepicker/jquery.datetimepicker.css",

		// Applications files
		"app/sprite.css",
		"app/util.css",
		"app/header.css",
		"app/footer.css",
		"app/home.css",
		"app/app.css",
		"app/about-us.css",
		"app/browsing-history.css",

		// party-bag
		"party-bag/party-bag-util.css",
		"party-bag/party-bag-sidebar.css",

		// master home page
		"master/home.css",

		// resolution based
		"res/1024.css",
		"res/400_600.css",
		"res/400.css"

	], "public/css/page-about-us.css");

	mix.styles([
		"reviews/reviews_util.css",
		"reviews/reviews_all.css"
	], "public/css/app/page-all-reviews.css");

	mix.styles([
		"common/filters.css",
		"common/profile-list-common.css",
		"party-bag/party-bag-results.css"
	], "public/css/app/page-party-bag-list.css");

	mix.styles([
		"common/profile-list-common.css",
		"common/filters.css",
		"app/global-search.css"
	], "public/css/app/page-gs.css");

	mix.styles([
		"app/landing-pages.css",
		"res/res-landing-pages.css"
	], "public/css/app/page-landing-pages.css");

	mix.styles([
		"jquery-ui/jquery.mosaic.min.css"
	], "public/css/app/jquery-mosaic.css");

	mix.styles([
		"rating/star-rating.min.css"
	], "public/css/util/star-rating.css");

	mix.styles([
		"app/track-my-order.css"
	], "public/css/util/track-my-order.css");

	mix.styles([
		"app/wallet.css"
	], "public/css/app/wallet.css");

	mix.styles([
		"list/common.css",
		"list/desktop.css",
		"list/mobile.css",
		"list/tab.css",
		"common/filters.css"
	], "public/css/app/profile-page.css");

	/* Partner Dashboard */
	mix.styles([
		"app/util.css",
		"partner/template/bootstrap.min.css",
		"partner/template/font-awesome.min.css",
		"partner/template/bootstrap-progressbar-3.3.4.min.css",
		"partner/template/bootstrap3-wysihtml5.min.css",
		"partner/template/noty.css",
		"partner/template/custom.min.css",
		"datetimepicker/jquery.datetimepicker.css",
		"partner/template/daterangepicker.css",
		"partner/partner.css",
		"res/1024.css",
		"res/400_600.css",
		"res/400.css",
	], "public/css/app/partner-profile.css");

	/* Refer & earn pages */
	mix.styles([
		"bootstrap/bootstrap.min.css",
		"material/material.min.css",
		"datetimepicker/jquery.datetimepicker.css",
		"app/util.css",
		"refer-earn/refer-earn.css",
		"responsive-slides/responsiveslides.css"
	], "public/css/re-app.css");

	/* WorkFlow specific css */
	mix.styles([
		"material/material.min.css",
		"bootstrap/bootstrap.min.css",
		"intro-js/introjs.min.css",
		"app/util.css",

		// Applications files
		"workflow/noty/noty.css",
		"workflow/workflow.css"
	], "public/css/workflow.css");

	/* Invitation specific css */
	mix.styles([
		"app/invite.css"
	], "public/css/app/invite.css");

	/* CSS landing page */
	mix.styles([
		"partner/partner-landing.css"
	], "public/css/partner-landing.css");

	mix.styles([
		"slick/slick.css"
	], "public/css/util/slick.css");

	mix.styles([
		"slick/slick-theme.css"
	], "public/css/util/slick-theme.css");
});

/* SASS */
elixir(function (mix) {
	mix.sass([
		"home.scss"
	], "public/assets/css/home.css");
});

/* Javascript */
elixir(function (mix) {

	/* Above the fold */
	mix.scripts([
		// jQuery
		"jquery/jquery.min.js",

		// Bootstrap
		"bootstrap/bootstrap.min.js",

		// cookies
		"cookie/cookie.js",

		// Other
		"material/material.min.js",
		"modernizr/modernizr.custom.54542.js",
		"responsive-slides/responsiveslides.min.js",
		"selectize/selectize.min.js",

		// jQuery plugins
		"datetimepicker/jquery.datetimepicker.js",

		// header pages
		"app/header.js",
		"app/home_util.js",

		// vue js files
		"vuejs/vue.js",
		"vuejs/vue-resource.js",

		// individual components
		"vuejs/evibe-app.js",

		// Application file
		"app/global_helpers.js",
		"app/app.js",

		// Noty
		"noty/jquery.noty.min.js",
		"noty/themes/default.min.js",
		"noty/layouts/top.min.js",

		//Mail-Checker Plugin
		"email/mail-checker.js",
		//Owl Carousel
		"owl-carousel/owl.carousel.js",
		// This file will load after JS load
		"app/window_load.js",

	], "public/js/af-home.js");

	/* Above the fold */
	mix.scripts([
		// Jquery-AutoComplete
		"owl-carousel/owl.carousel.js",
		"app/cities.js",
	], "public/js/owl-carousel.js");

	mix.scripts([
		// Owl Carousel
		"jquery-autocomplete/jquery-autocomplete.js",
		"app/cities.js",
	], "public/js/auto-complete.js");

	/*CIties list*/
	mix.scripts([
		// Jquery-AutoComplete
		"app/cities.js",
	], "public/js/citiesList.js");

	/* Above the fold */
	mix.scripts([
		// jQuery
		"jquery/jquery.min.js",

		// Bootstrap
		"bootstrap/bootstrap.min.js",

		// jQuery plugins
		"datetimepicker/jquery.datetimepicker.js",

		// Noty
		"noty/jquery.noty.min.js",
		"noty/themes/default.min.js",
		"noty/layouts/top.min.js",

		// Application file
		"app/global_helpers.js",
		"app/app.js",
		"app/home_util.js",
		"plugins/evibe.shortlist.js",

		// header pages
		"app/header.js",

		// vue js files
		"vuejs/vue.js",
		"vuejs/vue-resource.js",

		//Mail-Checker Plugin
		"email/mail-checker.js",

		// This file will load after JS load
		"app/window_load.js",

		// cookies
		"cookie/cookie.js"

	], "public/js/af-about-us.js");

	/* Default all.js file */
	mix.scripts([
		// jQuery
		"jquery/jquery.min.js",
		"jquery-ui/jquery-ui.min.js",

		// Bootstrap
		"bootstrap/bootstrap.min.js",

		// Other
		"material/material.min.js",
		"modernizr/modernizr.custom.54542.js",

		// jQuery plugins
		"scroll/jquery.scrollTo-1.4.3.1-min.js",
		"slider/jquery.pretty.photo.js",
		"rating/star-rating.min.js",
		"menu/jquery.menu.js",
		"range-slider/jquery-ui-slider-pips.js",
		"datetimepicker/jquery.datetimepicker.js",
		"responsive-slides/responsiveslides.min.js",
		"fotorama/fotorama.min.js",
		"jquery/jquery-colors.min.js",
		"selectize/selectize.min.js",

		// Noty
		"noty/jquery.noty.min.js",
		"noty/themes/default.min.js",
		"noty/layouts/top.min.js",

		// Application file
		"app/global_helpers.js",
		"app/app.js",
		"app/home_util.js",
		"plugins/evibe.shortlist.js",

		// header pages
		"app/header.js",

		// partner pages
		"app/partner_util.js",

		// vue js files
		"vuejs/vue.js",
		"vuejs/vue-resource.js",

		// individual components
		"vuejs/evibe-app.js",

		//Mail-Checker Plugin
		"email/mail-checker.js",

		// cookies
		"cookie/cookie.js",

		// This file will load after JS load
		"app/window_load.js"
	]);

	mix.scripts([
		"jquery/jquery.min.js",
		"bootstrap/bootstrap.min.js",
		"menu/jquery.menu.js",
		// vue js files
		"vuejs/vue.js",
		"vuejs/vue-resource.js",
		// individual components
		"app/app.js",
		"vuejs/evibe-app.js",
		"app/header.js",
		"app/window_load.js"


	], "public/js/virtual-party.js");

	mix.scripts([
		"app/400.js"
	], "public/js/app/400.js");

	mix.scripts([
		"pay/pay.js"
	], "public/js/pay/pay.js");

	mix.scripts([
		"app/results_util.js"
	], "public/js/app/results_util.js");

	mix.scripts([
		"app/profile_util.js"
	], "public/js/app/profile_util.js");

	mix.scripts([
		"cakes/cake_profile.js"
	], "public/js/cakes/cake_profile.js");

	mix.scripts([
		"decors/decor_profile.js"
	], "public/js/decors/decor_profile.js");

	mix.scripts(["app/reviews_util.js"
	], "public/js/app/reviews_util.js");

	mix.scripts([
		"profile/mobile.js"
	], "public/js/profile/mobile.js");

	mix.scripts([
		"profile/desktop.js"
	], "public/js/profile/desktop.js");

	mix.scripts([
		"profile/common.js"
	], "public/js/profile/common.js");

	mix.scripts([
		"list/mobile.js"
	], "public/js/list/mobile.js");

	mix.scripts([
		"list/desktop.js"
	], "public/js/list/desktop.js");

	mix.scripts([
		"list/common.js"
	], "public/js/list/common.js");

	mix.scripts([
		"app/results_util.js",
		"list/common.js",
		"list/mobile.js"
	], "public/js/list-mobile.js");

	mix.scripts([
		"app/results_util.js",
		"list/common.js",
		"list/desktop.js"
	], "public/js/list-desktop.js");

	mix.scripts([
		"app/profile_util.js",
		"profile/common.js",
		"profile/mobile.js"
	], "public/js/profile-mobile.js");

	mix.scripts([
		"profile/desktop.js",
		"profile/common.js",
		"app/profile_util.js"
	], "public/js/profile-desktop.js");

	mix.scripts([
		"app/profile_util.js",
		"profile/common.js",
		"profile/desktop.js"
	], "public/js/venue-profile-desktop.js");

	mix.scripts([
		"jquery-ui/jquery.mosaic.min.js"
	], "public/js/app/jquery-mosaic.js");

	mix.scripts([
		"jquery/masonry.min.js",
		"jquery/imagesloaded.min.js"
	], "public/js/app/pt.js");

	mix.scripts([
		"rating/star-rating.min.js"
	], "public/js/util/star-rating.js");

	mix.scripts([
		"typeahead/typeahead.js"
	], "public/js/util/typeahead.js");

	/* Partner dashboard */
	mix.scripts([
		"partner/template/jquery.min.js",
		"partner/template/bootstrap.min.js",
		"partner/template/bootstrap-progressbar.min.js",
		"partner/template/bootstrap3-wysihtml5.min.js",
		"partner/template/noty.min.js",
		"partner/template/validator.js",
		"partner/template/moment.min.js",
		"datetimepicker/jquery.datetimepicker.js",
		"partner/template/daterangepicker.js",
		"partner/template/custom.js",
		"partner/partner.js"
	], "public/js/partner/partner.js");

	/* Refer & Earn */
	mix.scripts([
		// jQuery
		"jquery/jquery.min.js",
		"jquery-ui/jquery-ui.min.js",

		// Bootstrap
		"bootstrap/bootstrap.min.js",

		// Other
		"material/material.min.js",
		"modernizr/modernizr.custom.54542.js",
		"responsive-slides/responsiveslides.min.js",

		// jQuery plugins
		"datetimepicker/jquery.datetimepicker.js",

		// Noty
		"noty/jquery.noty.min.js",
		"noty/themes/default.min.js",
		"noty/layouts/top.min.js",

		// Application file
		"app/global_helpers.js",
		"app/app.js"
	], "public/js/re-base.js");

	/* workflow */
	mix.scripts([
		// jQuery
		"jquery/jquery.min.js",
		"jquery-ui/jquery-ui.min.js",
		"material/material.min.js",

		// Bootstrap
		"bootstrap/bootstrap.min.js",

		// Noty
		"workflow/noty/noty.min.js",

		// vue js files
		"vuejs/minJs/vue.min.js",
		"vuejs/vue-resource.js",
		"workflow/workflow.js",
		"vuejs/workflow-vue.js",

		"intro-js/intro.min.js",

		// cookies
		"cookie/cookie.js"
	], "public/js/workflow.js");

	mix.scripts([
		"app/offer.js"
	], "public/js/util/offer-v1.min.js");

	mix.scripts([
		"app/offer.js"
	], "public/js/util/offer-v1563255647.min.js");

	mix.scripts([
		"app/offer-piab.js"
	], "public/js/util/offer-piab-v1.min.js");

	mix.scripts([
		"slick/slick.min.js"
	], "public/js/util/slick.js");

	mix.scripts([
		// Snow effect for heder
		"snow-effect/snowfall.jquery.js",
		"snow-effect/snowfall.js"
	], "public/js/snowfall.js");
});

/* Copy files as it is */
elixir(function (mix) {
	mix.copy("resources/assets/fonts", "public/build/fonts"); // copy fonts for party bag
	mix.copy("resources/assets/fonts", "public/css/fonts");
	mix.copy("resources/assets/css/partner/fonts", "public/css/fonts");// copy fonts for partner profile
	mix.copy("resources/assets/css/refer-earn/refer-earn.css", "public/css/app/refer-earn.css");
	mix.copy("resources/assets/css/intro-js/introjs.min.css", "public/css/app/intro.css");
	mix.copy("resources/assets/js/lazyload-images/lazyLoadImages.min.js", "public/js/util/lazyLoadImages.js");
	mix.copy("resources/assets/js/addtoany/addtoany.min.js", "public/js/util/addtoany.js");
	mix.copy("resources/assets/js/jquery/bootstrapCarousalSwipe.min.js", "public/js/util/bootstrapCarousalSwipe.js");
	mix.copy("resources/assets/js/intro-js/intro.min.js", "public/js/util/intro.js");
	mix.copy("resources/assets/js/service-worker/sw.js", "public/sw.js");
	mix.copy("resources/assets/util/slick-ajax-loader.gif", "public/util/slick-ajax-loader.gif");
});

/* modify files names every time we do gulp, tells browser to get the new file after update */
elixir(function (mix) {
	mix.version([

		// css versions
		"css/all.css",
		"css/re-app.css",
		"css/af-home.css",
		"css/app/page-profile.css",
		"css/app/page-list.css",
		"css/app/page-partner-profile.css",
		"css/app/page-party-bag-list.css",
		"css/app/page-thank-you.css",
		"css/page-about-us.css",
		"css/app/page-gs.css",
		"assets/css/home.css",
		"css/workflow.css",
		"css/app/invite.css",
		"css/app/wallet.css",
		"css/app/page-home.css",
		"css/app/page-landing-pages.css",
		"css/app/occasion-home.css",
		"css/app/inspiration-hub.css",
		"css/app/virtual-party.css",
		"css/virtual-party-updated.css",

		// js versions
		"js/all.js",
		"js/virtual-party.js",
		"js/af-home.js",
		"js/re-base.js",
		"js/app/400.js",
		"js/app/reviews_util.js",
		"js/app/results_util.js",
		"js/app/profile_util.js",
		"js/cakes/cake_profile.js",
		"js/decors/decor_profile.js",
		"js/workflow.js",
		"js/pay/pay.js",
		"js/partner/partner.js"
	]);
});