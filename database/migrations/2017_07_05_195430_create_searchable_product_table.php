<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchableProductTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('searchable_product', function (Blueprint $table)
		{
			$table->engine = 'InnoDB';

			$table->increments('id');
			$table->string('code');
			$table->string('name');
			$table->longText('inclusions');
			$table->double('price_min', 10, 2);
			$table->double('price_max', 10, 2)->nullable();

			// indexes
			$table->integer('city_id')->unsigned();
			$table->integer('option_type_id')->unsigned();
			$table->integer('option_id')->unsigned();
			$table->integer('option_sub_type_id')->unsigned()->nullable();

			$table->timestamps();
			$table->softDeletes();

			// foreign keys
			$table->foreign('city_id')->references('id')->on('city')
			      ->onDelete('restrict')->onUpdate('restrict');

			$table->foreign('option_type_id')->references('id')->on('type_ticket')
			      ->onDelete('restrict')->onUpdate('restrict');
		});

		Schema::disableForeignKeyConstraints();
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('searchable_product', function (Blueprint $table)
		{
			$table->dropForeign(['city_id', 'option_type_id']);
		});

		Schema::drop('searchable_product');
	}
}
