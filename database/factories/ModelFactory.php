<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
use Carbon\Carbon;

$factory->define(\App\Models\Ticket\Ticket::class, function (Faker\Generator $faker)
{
	return [
		'id'             => $faker->randomNumber(),
		'name'           => $faker->name,
		'email'          => $faker->email,
		'password'       => bcrypt(str_random(10)),
		'remember_token' => str_random(10),
		'phone'          => $faker->phoneNumber,
		'event_date'     => Carbon::createFromFormat('Y-m-d H:i:s', $faker->date() . '00:00:00')->timestamp,
		'party_location' => $faker->address
	];
});
