<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/**
 * |--------------------------------------------------------------------------
 * | Internal AJAX Requests
 * |
 * | @todo: few of them to be moved to APIs project
 * |--------------------------------------------------------------------------
 */
Route::group([], function () {
	Route::group(['prefix' => 'ajax', 'as' => 'ajax.'], function () {
		Route::group(['prefix' => 'auto-book', 'as' => 'auto-book.', 'namespace' => 'Ajax'], function () {
			Route::post('alert-team/{ticketInviteId}', [
				'as'   => 'alert-team',
				'uses' => 'AjaxAutoBookingController@alertTeamOnEnteringMobileNumber'
			]);

			Route::post('failed-to-proceed/{mapId}/{typeTicketId}/{occasionId}', [
				'as'   => 'failed-to-proceed',
				'uses' => 'AjaxAutoBookingController@alertTeamOnCustomerDidNotProceed'
			]);

			Route::post('upload-customer-images/{ticketId}', [
				'as'   => 'upload-customer-images',
				'uses' => 'AjaxAutoBookingController@uploadCustomerImages'
			]);

			Route::post('check-availability', [
				'as'   => 'check-availability',
				'uses' => 'AjaxAutoBookingController@checkAvailability'
			]);

			Route::post('check-availability-fail', [
				'as'   => 'check-availability-fail',
				'uses' => 'AjaxAutoBookingController@alertTeamOnFailedAvailabilityCheck'
			]);

			Route::post('dynamic-price', [
				'as'   => 'dynamic-price',
				'uses' => 'AjaxAutoBookingController@getDynamicPrice'
			]);
		});

		Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Ajax'], function () {
			Route::post('similar-products/{mapId}/{mapTypeId}/{eventId}', [
				'as'   => 'similar-products',
				'uses' => 'AjaxProfilePagesController@fetchSimilarProducts'
			]);
			Route::post('similar-products-global/{eventId}/{mapTypeId}/{mapId}', [
				'as'   => 'similar-products-global',
				'uses' => 'AjaxProfilePagesController@fetchSimilarProductsUsingGlobalSearch'
			]);
			Route::post('most-viewed-products/{mapId}/{mapTypeId}/{eventId}', [
				'as'   => 'most-viewed-products',
				'uses' => 'AjaxProfilePagesController@fetchMostViewedProducts'
			]);

			Route::post('other-profile-cards/{mapId}/{mapTypeId}/{eventId}/{category}', [
				'as'   => 'other-profile-cards',
				'uses' => 'AjaxProfilePagesController@fetchOtherProfileCards'
			]);

			Route::post('product-reviews/{mapId}/{mapTypeId}', [
				'as'   => 'product-reviews',
				'uses' => 'AjaxProfilePagesController@fetchProductReviewsAndRatings'
			]);

			Route::post('calculate-distance', [
				'as'   => 'calculate-distance',
				'uses' => 'AjaxProfilePagesController@calculateDistance'
			]);
		});

		Route::group(['prefix' => 'list', 'as' => 'list.', 'namespace' => 'Ajax'], function () {
			Route::post('options-ratings/{optionTypeId}', [
				'as'   => 'options-ratings',
				'uses' => 'AjaxListPagesController@fetchOptionsRatings'
			]);

			Route::post('get-option-images', [
				'as'   => 'get-option-images',
				'uses' => 'AjaxListPagesController@getOptionImages'
			]);
		});

		Route::group(['prefix' => 'home', 'as' => 'home.', 'namespace' => 'Util'], function () {
			// Home page content
			Route::post('posts', [
				'as'   => 'posts',
				'uses' => 'CityHomeController@ajaxGetPosts'
			]);

			Route::post('benefits', [
				'as'   => 'benefits',
				'uses' => 'CityHomeController@ajaxGetBenefits'
			]);

			Route::post('{cityUrl}/top-collections', [
				'as'   => 'top.collections',
				'uses' => 'CityHomeController@ajaxGetTopCollections'
			]);

			Route::post('customer-review', [
				'as'   => 'customers.review',
				'uses' => 'CityHomeController@ajaxGetCustomerReviews'
			]);

			Route::post('{cityUrl}/evibe-media', [
				'as'   => 'media',
				'uses' => 'CityHomeController@ajaxGetEvibeInMedia'
			]);

			Route::post('partner-signup', [
				'as'   => 'partner.signup',
				'uses' => 'CityHomeController@ajaxGetPartnerSignup'
			]);

			Route::post('{eventId}/top-options', [
				'as'   => 'event.top.options',
				'uses' => 'CityHomeController@ajaxGetTopOption'
			]);
		});

		Route::group(['prefix' => 'checkout', 'as' => 'checkout.', 'namespace' => 'Ajax'], function () {
			Route::post('add-ons/{ticketId}', [
				'as'   => 'add-ons',
				'uses' => 'AjaxCheckoutController@updateCheckoutAddOns'
			]);

			Route::post('upload-order-proofs/{ticketId}', [
				'as'   => 'upload-order-proofs',
				'uses' => 'AjaxCheckoutController@uploadCustomerOrderProofs'
			]);
		});

		Route::group(['prefix' => 'util', 'as' => 'util.', 'namespace' => 'Ajax'], function () {
			Route::get('google-reviews', [
				'as'   => 'google-reviews',
				'uses' => 'AjaxUtilController@fetchGoogleReviews'
			]);

			Route::get('surprise-categories', [
				'as'   => 'surprise-categories',
				'uses' => 'AjaxUtilController@fetchSurpriseCategories'
			]);
		});
	});

	// check if user logged in before mandatory login
	Route::group(['namespace' => 'Auth', 'as' => 'check.user.', 'prefix' => 'check/user'], function () {
		Route::post('login', [
			'as'   => 'login',
			'uses' => 'UserAuthController@handleUserLoginModal'
		]);
	});
});

/**
 * |--------------------------------------------------------------------------
 * | Virtual Birthday routes
 * |--------------------------------------------------------------------------
 */

Route::group(['prefix' => '/virtual-birthday-party', 'as' => 'virtual-birthday-party.', 'namespace' => 'LandingPages'], function () {
	Route::get('/', [
		'as'   => 'home',
		'uses' => 'LandingPagesController@showVirtualBdyPage'
	]);
	Route::get('/test', [
		'as'   => 'test',
		'uses' => 'LandingPagesController@showVirtualBdyPageTest'
	]);
	
});

Route::group(['prefix' => 'us/virtual-birthday-party', 'as' => 'abroad.virtual-birthday-party.', 'namespace' => 'LandingPages'], function () {
	Route::get('/', [
		'as'   => 'virtual-birthday-party',
		'uses' => 'LandingPagesController@showVirtualBdyPage'
	]);
});

/**
 * |--------------------------------------------------------------------------
 * | Virtual Party routes
 * |--------------------------------------------------------------------------
 */
Route::group(['prefix' => 'virtual-party', 'as' => 'virtual-party.', 'namespace' => 'Util'], function () {
	Route::get('{url}', [
		'as'   => 'home',
		'uses' => 'VirtualPartyController@showPartyHomePage'
	]);

	Route::post('fetch-feed', [
		'as'   => 'fetch.feed',
		'uses' => 'VirtualPartyController@fetchFeed'
	]);

	Route::post('validate-wishes', [
		'as'   => 'validate-wishes',
		'uses' => 'VirtualPartyController@validateWishes'
	]);

	Route::post('send-wishes', [
		'as'   => 'send-wishes',
		'uses' => 'VirtualPartyController@sendWishes'
	]);

	Route::post('accept-rsvp', [
		'as'   => 'accept-rsvp',
		'uses' => 'VirtualPartyController@submitRSVP'
	]);
});

/**
 * |--------------------------------------------------------------------------
 * | Inspiration Hub
 * |--------------------------------------------------------------------------
 */
Route::group(['prefix' => 'inspirations/i/{pageUrl}', 'as' => 'inspirations.', 'namespace' => 'Inspirations'], function () {
	Route::get('/', [
		'as'   => 'feed',
		'uses' => 'InspirationsController@fetchFeed'
	]);
	Route::get('/{albumUrl}', [
		'as'   => 'albumInfo',
		'uses' => 'InspirationsController@fetchFeed'
	]);
	Route::post('/store-details', [
		'as'   => 'store-details',
		'uses' => 'InspirationsController@storeDetails'
	]);
});

/**
 * |--------------------------------------------------------------------------
 * | Ad Campaign Landing Pages
 * |--------------------------------------------------------------------------
 */
Route::group(['prefix' => '/u/{campaignUrl}', 'as' => 'campaign.', 'namespace' => 'Campaigns'], function () {
	Route::get('/', [
		'as'   => 'landingpage',
		'uses' => 'CampaignController@landingPage'
	]);
});

/**
 * |--------------------------------------------------------------------------
 * | Other Occassional Landing Pages
 * |--------------------------------------------------------------------------
 */
Route::group(['prefix' => '/l/{pageUrl}', 'as' => 'landindpage.', 'namespace' => 'LandingPages'], function () {
	Route::get('/', [
		'as'   => 'landingpage',
		'uses' => 'LandingPagesController@showVDayPage'
	]);
});

/**
 * |--------------------------------------------------------------------------
 * | Third party promotional sign-up landing page
 * |--------------------------------------------------------------------------
 */
Route::get('party-supplies', [
	'as'   => 'party-supplies',
	'uses' => 'Util\InstapartyPromotionController@showPromotionalPage'
]);

/**
 * |--------------------------------------------------------------------------
 * | User History
 * |--------------------------------------------------------------------------
 */
Route::group(['prefix' => 'user-browse-history', 'as' => 'user.browsing.history.', 'namespace' => 'Util'], function () {
	// Route::post('close-window', [
	// 	'as'   => 'close.window',
	// 	'uses' => 'UserBrowsingHistoryController@updateUserActivityTime'
	// ]);
	Route::post('focused-window', [
		'as'   => 'focused.window',
		'uses' => 'UserBrowsingHistoryController@setNewBrowsingInstance'
	]);

	Route::post('data', [
		'as'   => 'data',
		'uses' => 'UserBrowsingHistoryController@userVisitActivityUpdate'
	]);
});

Route::group(['prefix' => 'my/browsing-history', 'as' => 'user.browsing.history.', 'namespace' => 'Util'], function () {
	Route::get('/', [
		'as'   => 'list.new',
		'uses' => 'UserBrowsingHistoryController@showUserHistoryData'
	]);
});

/**
 * |--------------------------------------------------------------------------
 * | Intermediate page for store redirect
 * |--------------------------------------------------------------------------
 */
Route::get('store-redirect', [
	'as'   => 'store-redirect',
	'uses' => 'Base\BaseController@showStoreRedirectPage'
]);


/**
 * |--------------------------------------------------------------------------
 * | Static page, tickets, tracking, Ads
 * |--------------------------------------------------------------------------
 */
Route::group(['namespace' => 'Util'], function () {
	Route::any('/', ['uses' => 'PagesController@redirectToCity']);

	// Page to enter corrected email id
	Route::group(['prefix' => 'validate-email', 'as' => 'validate-email.'], function () {
		Route::get('/{suppressionId}', [
			'as'   => 'show',
			'uses' => 'EmailCorrectionController@showEmailCorrectionPage',
		]);

		Route::post('save', [
			'as'   => 'save',
			'uses' => 'EmailCorrectionController@saveEmailAddress'
		]);
	});

	// Landing Pages route for Google Ad Words
	Route::group(['prefix' => 'ga/landing', 'as' => 'ga.landing.'], function () {

		Route::post('enquiry', [
			'as'   => 'enquiry',
			'uses' => 'LandingPagesController@submitEnquiryForm'
		]);

		Route::get('intermediate', [
			'as'   => 'intermediate',
			'uses' => 'LandingPagesController@showIntermediatePage'
		]);

		Route::get('{keyWord}', [
			'uses' => 'LandingPagesController@showLandingPage'
		]);

	});

	//// Payment Modes
	//Route::group(['prefix' => 'payment-modes', 'as' => 'payment.modes.'], function () {
	//	Route::get('all', [
	//		'as'   => 'data',
	//		'uses' => 'PaymentMethodModesController@showAllPaymentModes'
	//	]);
	//});

	// Offer landing popup
	Route::group(['prefix' => 'of/landing', 'as' => 'of.landing.'], function () {
		Route::post('validate', [
			'as'   => 'validate',
			'uses' => 'LandingPagesController@getOfferData'
		]);

		Route::post('validate/piab', [
			'as'   => 'validate',
			'uses' => 'LandingPagesController@getOfferData'
		]);

		Route::post('submit', [
			'as'   => 'submit',
			'uses' => 'LandingPagesController@saveOfferData'
		]);

	});

	// Landing Pages
	Route::group(['prefix' => 'surprises', 'as' => 'surprises.'], function () {
		Route::get('theatre-movie-wishes/{profileUrl}', [
			'as'   => 'surprises.ad.profile',
			'uses' => 'LandingPagesController@showLandingAdSurprisePage'
		]);

		Route::post('theatre-movie-wishes/get-price', [
			'as'   => 'surprises.ad.get.price',
			'uses' => 'LandingPagesController@getPriceForOptions'
		]);
	});

	// Landing Pages
	Route::group(['prefix' => 'landing', 'as' => 'landing.'], function () {
		Route::post('surprise/enquiry', [
			'as'   => 'surprise.enquiry.submit',
			'uses' => 'LandingPagesController@submitSurprisesEnquiryForm'
		]);
	});

	// Landing Pages
	Route::group(['prefix' => 'track', 'as' => 'track.'], function () {
		Route::get('/', [
			'as'   => 'orders',
			'uses' => 'OrderTrackingController@checkLogin'
		]);

		Route::get('/orders', [
			'as'   => 'orders.all',
			'uses' => 'OrderTrackingController@showActiveOrders'
		]);

		Route::get('/login', [
			'as'   => 'login',
			'uses' => 'OrderTrackingController@showLoginScreen'
		]);

		Route::get('/login/{phone}', [
			'as'   => 'login.phone',
			'uses' => 'OrderTrackingController@showOTPScreen'
		]);

		Route::post('/login/getOTP', [
			'as'   => 'login.getOTP',
			'uses' => 'OrderTrackingController@getOTP'
		]);

		Route::post('/login/submit-otp', [
			'as'   => 'login.submitOTP',
			'uses' => 'OrderTrackingController@submitOTP'
		]);

		Route::post('/request/save/{ticketId}', [
			'as'   => 'request.save',
			'uses' => 'OrderTrackingController@saveRequest'
		]);

		Route::post('/request/cancel/charges/{bookingId}', [
			'as'   => 'request.cancel.charges',
			'uses' => 'OrderTrackingController@fetchCancellationCharges'
		]);

		Route::post('/request/ab/profile-images', [
			'as'   => 'request.ab.images',
			'uses' => 'OrderTrackingController@getABProfileImages'
		]);

		Route::post('/request/checkout-fields', [
			'as'   => 'request.checkout.fields',
			'uses' => 'OrderTrackingController@getCheckoutFields'
		]);

		Route::post('/upload/customer-proofs', [
			'as'   => 'upload.customer-proofs',
			'uses' => 'OrderTrackingController@uploadCustomerProofs'
		]);
	});

	// Error notification to team
	Route::group(['prefix' => 'report/notify', 'as' => 'notify.'], function () {

		Route::post("error", [
			"as"   => "error",
			"uses" => "NotifyController@notifyTechTeam"
		]);

		Route::post("error/css", [
			"as"   => "error",
			"uses" => "NotifyController@notifyTechTeam"
		]);
	});

	// Reminders page for customer and vendor
	// @author Vikash <vikash@evibe.in>
	// @since  25 August 2016
	Route::group(['as' => 'order.'], function () {

		Route::get('c/order/{id}', [
			'as'   => 'customer',
			'uses' => 'OrderController@showCustomerOrderDetails'
		]);

		Route::get('p/order/{id}', [
			'as'   => 'partner',
			'uses' => 'OrderController@showPartnerOrderDetails'
		]);

		Route::get('p/order/m/{providerId}/{mapTypeId}', [
			'as'   => 'multiple.partner',
			'uses' => 'OrderController@showPartnerMultipleOrderDetails'
		]);

	});

	/**
	 * Pop up alert saving data & sending alert to team
	 */
	Route::group([
		             'prefix' => 'auto-pop-up',
		             'as'     => 'auto.popup.'
	             ], function () {

		Route::post('/questions', [
			'as'   => "get.questions",
			'uses' => "AutoPopUpModalController@getPageSpecificQuestions"
		]);

		Route::post('/save/data', [
			'as'   => "save.data",
			'uses' => "AutoPopUpModalController@saveFormData"
		]);

		Route::post('partner-delivery/save/data', [
			'as'   => "partner.delivery.save.data",
			'uses' => "AutoPopUpModalController@saveProductDeliveryFormData"
		]);
	});

	Route::get('/provider-reviews/{productUrl}/{providerType}/{providerId}', [
		'as'   => 'provider-reviews:all',
		'uses' => 'PagesController@showAllProviderReviews'
	]);

	// Static pages
	Route::group([], function () {

		Route::get('about', [
			//'middleware' => 'cache.response',
			'uses' => 'PagesController@showAboutPage'
		]);

		Route::get('faqs', ['uses' => 'PagesController@showFAQsPage']);

		// Website feedback
		Route::group(['prefix' => 'feedback'], function () {
			Route::get('/', ['uses' => 'PagesController@showFeedbackPage']);

			Route::post('/', ['uses' => 'PagesController@postWebsiteFeedback']);
		});

		/* Job application form in about us page */
		Route::post('applicantform', [
			'as'   => 'application.form',
			'uses' => 'PagesController@postApplicationForm'
		]);

		Route::post('check-collections', [
			'as'   => 'collection.list.adv.card',
			'uses' => 'PagesController@showCollectionAdvCardInList'
		]);

		Route::get('terms', [
			'as'   => 'terms',
			'uses' => 'PagesController@showTermsPage'
		]);

		Route::get('privacy', [
			'as'   => 'privacy',
			'uses' => 'PagesController@showPrivacyPage'
		]);

		Route::get('refunds', [
			'as'   => 'refunds',
			'uses' => 'PagesController@showRefundsPage'
		]);

		Route::get('how-it-works', ['uses' => 'PagesController@showHowItWorksPage']);

		// Vendor sign-up
		Route::group(['prefix' => 'partner/signup', 'as' => 'partner.'], function () {

			Route::get('/', [
				'as'   => 'benefits',
				'uses' => 'PagesController@showPartnerSignUpLandingPage'
			]);

			Route::get('cld', [
				'as'   => 'cld',
				'uses' => 'PagesController@showCLDSignUpLandingPage'
			]);

			Route::get('group-table', [
				'as'   => 'group.table',
				'uses' => 'PagesController@showGroupTableSignUpLandingPage'
			]);

			Route::post('cld/submit', [
				'as'   => 'cld.submit',
				'uses' => 'PagesController@submitCLDSignUp'
			]);

			Route::get('form', [
				'as'   => 'sign-up.show',
				'uses' => 'PagesController@showVendorSignUpPage'
			]);

			Route::post('form', [
				'as'   => 'sign-up.save',
				'uses' => 'PagesController@postVendorSignUpDetails'
			]);

		});

		Route::get('missing', [
			'as'   => 'missing',
			'uses' => 'PagesController@showMissingPage'
		]);

		Route::post('report-issue', [
			'as'   => 'report.issue',
			'uses' => 'PagesController@postReportIssue'
		]);

		Route::get('/@{shortUrl}', ['uses' => 'PagesController@showShortUrlProfile']);

		// fallout feedback @todo: later merge this into customer dashboard
		Route::group(['prefix' => 'fallout-feedback'], function () {

			Route::get('/', ['uses' => 'PagesController@askFalloutFeedback']);
			Route::post('save', ['uses' => 'PagesController@saveFalloutFeedback']);
		});
	});

	// Customer reviews
	Route::group(['prefix' => 'give-feedback', 'as' => 'feedback.'], function () {

		Route::get('/', [
			'as'   => 'show',
			'uses' => 'FeedbackController@showCustomFeedbackPage'
		]);

		Route::post('/', [
			'as'   => 'save',
			'uses' => 'FeedbackController@postCustomerFeedback'
		]);

		Route::post('fail', [
			'as'   => 'fail',
			'uses' => 'FeedbackController@postFeedbackSubmitFail'
		]);

		Route::group(['prefix' => 'thanks', 'as' => 'success.'], function () {

			Route::get('/', [
				'as'   => 'show',
				'uses' => 'FeedbackController@showThanksPage'
			]);

			Route::get('rate', [
				'as'   => 'rate',
				'uses' => 'FeedbackController@showThanksPageWithGoogleLink'
			]);
		});

	});

	// Raise tickets
	Route::group(['as' => 'ticket.', 'prefix' => 'book'], function () {

		Route::group(["prefix" => "{ticketId}"], function () {

			Route::get('thank-you', [
				"as"   => "thank-you",
				"uses" => "TicketsController@showThankYouPage"
			]);

			// post additional ticket details
			Route::post("update", [
				"as"   => "update",
				"uses" => "TicketsController@updateTicket"
			]);

		});

		Route::group(['prefix' => '{occasionId}'], function () {

			// decor style
			Route::post('decor/{decorId}', [
				'as'   => 'decor',
				'uses' => 'TicketsController@postDecorTicket'
			]);

			// package
			Route::post('package/{packageId}', [
				'as'   => 'package',
				'uses' => 'TicketsController@postPackageTicket'
			]);

			// trend
			Route::post('trend/{trendId}', [
				'as'   => 'trend',
				'uses' => 'TicketsController@postTrendTicket'
			]);

			// cake
			Route::post('cake/{cakeId}', [
				'as'   => 'cake',
				'uses' => 'TicketsController@postCakeTicket'
			]);

			// service - ent + addons
			Route::post('service/{serviceId}', [
				'as'   => 'service',
				'uses' => 'TicketsController@postServiceTicket'
			]);

			// venue (hall)
			Route::post('venue/{venueId}', [
				'as'   => 'venue',
				'uses' => 'TicketsController@postVenueTicket'
			]);

		});

		// custom ticket
		Route::group(['as' => 'custom-ticket.', 'prefix' => 'custom-ticket/{occasionId}'], function () {

			// decor style
			Route::post('decor', [
				'as'   => 'decor',
				'uses' => 'CustomTicketController@postDecorEnquiry'
			]);

			// cake
			Route::post('cake', [
				'as'   => 'cake',
				'uses' => 'CustomTicketController@postCakeEnquiry'
			]);

			// food
			Route::post('food', [
				'as'   => 'food',
				'uses' => 'CustomTicketController@postFoodEnquiry'
			]);

		});

	});

	// Recommendation details page for a customer
	// @author Vikash <vikash@evibe.in>
	// @since  4 July 2016
	Route::group(['prefix' => 'recommendation', 'as' => 'customer.recommendation.'], function () {

		Route::get('/', [
			'as'   => 'show',
			'uses' => 'RecommendationController@showRecommendation'
		]);

		Route::post('{ticketId}/confirm', [
			'as'   => 'confirm',
			'uses' => 'RecommendationController@sendRecommendationChoices'
		]);

		Route::post('{ticketId}/confirm/dislike', [
			'as'   => 'confirm.dislike',
			'uses' => 'RecommendationController@processRecommendationDislike'
		]);

		Route::post('{ticketId}/next-steps', [
			'as'   => 'next-steps',
			'uses' => 'RecommendationController@processRecommendationNextSteps'
		]);

		Route::post('{ticketId}/new-recos', [
			'as'   => 'new-recos',
			'uses' => 'RecommendationController@processNewRecommendations'
		]);

		Route::post('rating', [
			'as'   => 'rating',
			'uses' => 'RecommendationController@processRecoRatingFeedback'
		]);
	});

	// Provide back support for SEO
	Route::group([], function () {

		// redirect vendor sign up
		Route::get('vendor/why-signup-with-us', ['uses' => 'BackSupport@redirectPartnerWhySignup']);
		Route::get('partner/why-signup-with-us', ['uses' => 'BackSupport@redirectPartnerWhySignup']);
		Route::get('vendor/signup', ['uses' => 'BackSupport@redirectPartnerSignup']);

		// city specific back support
		Route::group(['prefix' => '{cityUrl}'], function () {
			// redirect surprise Page URL
			Route::get('surprise-planners', ['uses' => 'BackSupport@redirectOldSurprisesUrl()']);

			// changed Stories to Reviews for SEO
			Route::get('stories', ['uses' => 'BackSupport@redirectStoriesToReviews']);

			/**
			 * Back support for new Birthday party SEO
			 *
			 * @author Anji <anji@evibe.in>
			 * @since  16 June 2016
			 *
			 * @todo   remove this on or before August 2016
			 */
			Route::group(['prefix' => 'birthday-party'], function () {
				Route::get('/', ['uses' => 'BirthdayBackSupport@newBirthdayHome']);

				// venues
				Route::group(['prefix' => 'venues'], function () {
					Route::get('/', ['uses' => 'BirthdayBackSupport@newBirthdayVenueList']);
					Route::get('/quick-links', ['uses' => 'BirthdayBackSupport@newBirthdayVenueLinks']);
					Route::get('/{profile}', ['uses' => 'BirthdayBackSupport@newBirthdayVenueProfile']);
				});

				// decors
				Route::group(['prefix' => 'decorations'], function () {
					Route::get('/', ['uses' => 'BirthdayBackSupport@newBirthdayDecorList']);
					Route::get('/{profile}', ['uses' => 'BirthdayBackSupport@newBirthdayDecorProfile']);
				});

				// ent
				Route::group(['prefix' => 'entertainment-activities-stalls'], function () {
					Route::get('/', ['uses' => 'BirthdayBackSupport@newBirthdayEntList']);
					Route::get('/{profile}', ['uses' => 'BirthdayBackSupport@newBirthdayEntProfile']);
				});

				// packages
				Route::group(['prefix' => 'packages'], function () {
					Route::get('/', ['uses' => 'BirthdayBackSupport@newBirthdayPackageList']);
					Route::get('/{profile}', ['uses' => 'BirthdayBackSupport@newBirthdayPackageProfile']);
				});

				// trends
				Route::group(['prefix' => 'trends'], function () {
					Route::get('/', ['uses' => 'BirthdayBackSupport@newBirthdayTrendList']);
					Route::get('/{profile}', ['uses' => 'BirthdayBackSupport@newBirthdayTrendProfile']);
				});

				// cakes
				Route::group(['prefix' => 'cakes'], function () {
					Route::get('/', ['uses' => 'BirthdayBackSupport@newBirthdayCakeList']);
					Route::get('/{profile}', ['uses' => 'BirthdayBackSupport@newBirthdayCakeProfile']);
				});

				// add-ons
				Route::group(['prefix' => 'photography-videos-fun-eats'], function () {
					Route::get('/', ['uses' => 'BirthdayBackSupport@newBirthdayAddOnList']);
					Route::get('/{profile}', ['uses' => 'BirthdayBackSupport@newBirthdayAddOnProfile']);
				});
			});

			// cake redirect
			Route::get('{id}/cakes', ['uses' => 'BackSupport@redirectCakesList'])->where('id', '[0-9]+');

			// new + old
			Route::group(['prefix' => 'birthday-party-planners'], function () {
				Route::get('decorations', ['uses' => 'BirthdayBackSupport@newBirthdayDecorList']);
				Route::get('birthday-party-decorations', ['uses' => 'BirthdayBackSupport@newBirthdayDecorList']);
				Route::get('decorations/{profile}', ['uses' => 'BirthdayBackSupport@newBirthdayDecorProfile']);
				Route::get('photography-videos-fun-eats', ['uses' => 'BirthdayBackSupport@newBirthdayAddOnList']);
				Route::get('photography-videos-fun-eats/{profile}', ['uses' => 'BirthdayBackSupport@newBirthdayAddOnProfile']);

				// return-gifts
				Route::group(['prefix' => 'return-gifts'], function () {
					Route::get('return-gifts-products', ['uses' => 'BirthdayBackSupport@newBirthdayHome']);
					Route::get('return-gifts-categories', ['uses' => 'BirthdayBackSupport@newBirthdayHome']);
					Route::get('buy/redirect/partyone', ['uses' => 'BirthdayBackSupport@newBirthdayHome']);
					Route::get('{categoryId?}/{categoryUrl?}', ['uses' => 'BirthdayBackSupport@newBirthdayHome']);
				});
			});

			// experiences (old)
			Route::get('unique-celebration-experiences', ['uses' => 'BackSupport@redirectExpList']);
			Route::get('unique-celebration-experience/{expUrl}', ['uses' => 'BackSupport@redirectExpProfile']);

			// campaigns
			Route::group(['prefix' => 'c'], function () {
				Route::get('valentines-day-2016/packages', ['uses' => 'BackSupport@redirectExpList']);
				Route::get('valentines-day-2016/packages/{packageUrl}', ['uses' => 'BackSupport@redirectExpList']);
				Route::get('valentines-day-special-2017/experiences/{packageUrl}', ['uses' => 'BackSupport@redirectExpList']);
				Route::get('valentines-day-special-2017/{packageUrl}', ['uses' => 'BackSupport@redirectExpList']);
			});

			// venues
			Route::get('venues', ['uses' => 'BackSupport@redirectVenuesList']);
			Route::get('quick-links', ['uses' => 'BackSupport@redirectVenuesLinks']);
			Route::get('venues/{venueUrl}', ['uses' => 'BackSupport@redirectVenueProfile']);
			Route::get('party-halls', ['uses' => 'BackSupport@redirectVenuesList']);
			Route::get('play-zones', ['uses' => 'BackSupport@redirectVenuesList']);
			Route::get('party-restaurants', ['uses' => 'BackSupport@redirectVenuesList']);

			// decors
			Route::get('party-decorations', ['uses' => 'BackSupport@redirectDecorsList']);
			Route::get('decorations', ['uses' => 'BackSupport@redirectDecorsList']);
			Route::get('party-decorations/{decorUrl}', ['uses' => 'BackSupport@redirectDecorProfile']);
			Route::get('balloon-decorators', ['uses' => 'BackSupport@redirectDecorBalloons']);
			Route::get('theme-decorators', ['uses' => 'BackSupport@redirectDecorThemes']);

			// packages
			Route::get('birthday-party-packages', ['uses' => 'BackSupport@redirectPackagesList']);
			Route::get('birthday-party-package/{packageUrl}', ['uses' => 'BackSupport@redirectPackageProfile']);

			// trends
			Route::get('kids-birthday-party-trends', ['uses' => 'BackSupport@redirectTrendsList']);
			Route::get('kids-birthday-party-trends/{trendUrl}', ['uses' => 'BackSupport@redirectTrendProfile']);

			// cakes
			Route::get('cakes', ['uses' => 'BackSupport@redirectCakesList']);
			Route::get('cakes/{cakeUrl}', ['uses' => 'BackSupport@redirectCakeProfile']);

			// ent
			Route::get('entertainment-activities-stalls', ['uses' => 'BackSupport@redirectEnt']);
			Route::get('caricature-artists', ['uses' => 'BackSupport@redirectEnt']);
			Route::get('bouncing-castle', ['uses' => 'BackSupport@redirectEnt']);
			Route::get('puppet-shows', ['uses' => 'BackSupport@redirectEnt']);
			Route::get('balloon-shooting', ['uses' => 'BackSupport@redirectEnt']);
			Route::get('tattoo-artists', ['uses' => 'BackSupport@redirectEnt']);
			Route::get('emcee', ['uses' => 'BackSupport@redirectEnt']);
			Route::get('clay-modelling', ['uses' => 'BackSupport@redirectEnt']);
			Route::get('magicians', ['uses' => 'BackSupport@redirectEnt']);
			Route::get('face-painting', ['uses' => 'BackSupport@redirectEnt']);
			Route::get('nail-art', ['uses' => 'BackSupport@redirectEnt']);
			Route::get('merry-go-around', ['uses' => 'BackSupport@redirectEnt']);

			// activities
			Route::get('photography-videos-fun-eats', ['uses' => 'BackSupport@redirectAddOns']);
			Route::get('ice-gola', ['uses' => 'BackSupport@redirectAddOns']);
			Route::get('popcorn', ['uses' => 'BackSupport@redirectAddOns']);
			Route::get('chocolate-fountain', ['uses' => 'BackSupport@redirectAddOns']);
			Route::get('candy-floss', ['uses' => 'BackSupport@redirectAddOns']);
			Route::get('sound-system', ['uses' => 'BackSupport@redirectAddOns']);

			// Engagements + Receptions
			Route::group(['prefix' => 'engagement-reception'], function () {
				Route::get('/', ['uses' => 'BackSupport@redirectToEngHome']);
				Route::get('decorations', ['uses' => 'BackSupport@redirectToEngDecor']);
				Route::get('decorations/{profile}', ['uses' => 'BackSupport@redirectToEngDecorProfile']);
				Route::get('entertainment', ['uses' => 'BackSupport@redirectToEngEnt']);
				Route::get('entertainment/{profile}', ['uses' => 'BackSupport@redirectToEngEntProfile']);
				Route::get('venues', ['uses' => 'BackSupport@redirectToEngVenue']);
				Route::get('venues/{profile}', ['uses' => 'BackSupport@redirectToEngVenueProfile']);
				Route::get('cakes', ['uses' => 'BackSupport@redirectToEngCake']);
				Route::get('cakes/{profile}', ['uses' => 'BackSupport@redirectToEngCakeProfile']);
			});
 
			// category style updated
			Route::get('engagement-wedding-reception/decorations', ['uses' => 'BackSupport@redirectToEngDecor']);

			// redirect to home page, dropped support for planner profiles
			Route::get('birthday-planner', ['uses' => 'BackSupport@redirectToHome']);
			Route::get('birthday-planners', ['uses' => 'BackSupport@redirectToHome']);
			Route::get('birthday-planner/{profile}', ['uses' => 'BackSupport@redirectToHome']);

			// Couple Experiences
			Route::group(['prefix' => 'special-couple-experiences'], function () {
				Route::get('/', ['uses' => 'BackSupport@redirectToSurprisesHome']);
				Route::get('packages', ['uses' => 'BackSupport@redirectToSurprisesPackage']);
				Route::get('packages/{packageUrl}', ['uses' => 'BackSupport@redirectToSurprisesPackageProfile']);
				Route::get('collections', ['uses' => 'BackSupport@redirectToSurprisesCollection']);
				Route::get('collections/{collectionUrl}', ['uses' => 'BackSupport@redirectToSurprisesCollectionProfile']);

			});
		});

		// PIAB pages
		Route::group(['prefix' => 'party-in-a-box'], function () {
			Route::get('/', ['uses' => 'BackSupport@redirectPartyKitsHome']);
			Route::get('{categoryUrl}', ['uses' => 'BackSupport@redirectPartyKitsList']);
		});

		Route::get('user-browse-history/view', ['uses' => 'BackSupport@redirectUserBrowsingHistory']);
	});

	// Coupon Integration
	Route::group(['prefix' => 'coupon', 'as' => 'coupon.'], function () {

		// Validate coupon
		Route::post('validate', [
			'as'   => 'validate',
			'uses' => 'CouponController@validateCoupon'
		]);

		// Validate coupon
		Route::post('validate/piab', [
			'as'   => 'validate/piab',
			'uses' => 'CouponController@validateCouponForPIAB'
		]);
	});

	// customer related pages
	Route::group(['prefix' => 'my', 'as' => 'customer.'], function () {

		// Thank you card
		Route::get('thank-you-card', [
			'as'   => 'thank-you-card',
			'uses' => 'CustomerController@showThankYouCard'
		]);

		Route::post('tyc-image', [
			'as'   => 'tyc-image',
			'uses' => 'CustomerController@downloadTYCImage'
		]);
	});

});

// Fetching variations data
Route::group(['prefix' => 'variations', 'as' => 'product.variations.', 'namespace' => 'Base'], function () {
	Route::post('data', [
		'as'   => 'data',
		'uses' => 'BaseResultsController@fetchOptionDataForVariations'
	]);
});

// Party In A Box pages
// @todo: need to use config for list and profile links
Route::group(['namespace' => 'Product', 'prefix' => 'party-kits', 'as' => 'party-kits.'], function () {

	Route::group(['prefix' => 'buy', 'as' => 'buy.'], function () {
		Route::post('init/{productStyleId}', [
			'as'   => 'init',
			'uses' => 'ProductBookingController@initProductBooking'
		]);

		Route::get('checkout', [
			'as'   => 'checkout',
			'uses' => 'ProductBookingController@showCheckoutPage'
		]);

		Route::group(['prefix' => 'validate/{productBookingId}', 'as' => 'validate.'], function () {
			Route::post('contact-details', [
				'as'   => 'contact-details',
				'uses' => 'ProductBookingController@validateBookingContactDetails'
			]);

			Route::post('delivery-details', [
				'as'   => 'delivery-details',
				'uses' => 'ProductBookingController@validateBookingDeliveryDetails'
			]);

			Route::post('order-details', [
				'as'   => 'order-details',
				'uses' => 'ProductBookingController@validateBookingOrderDetails'
			]);
		});

		Route::group(['prefix' => 'update/{productBookingId}', 'as' => 'update.'], function () {
			Route::post('price-details', [
				'as'   => 'price-details',
				'uses' => 'ProductBookingController@updateBookingPriceDetails'
			]);
		});

		Route::post('process/{productBookingId}', [
			'as'   => 'process',
			'uses' => 'ProductBookingController@processPayment'
		]);

		Route::get('success/{productBookingId}', [
			'as'   => 'success',
			'uses' => 'ProductBookingController@onPaymentSuccess'
		]);
	});

	Route::post('feedback', [
		'as'   => 'feedback',
		'uses' => 'ProductBookingController@submitFeedback'
	]);

	Route::group(['prefix' => 'delivery', 'as' => 'delivery.'], function () {
		Route::post('/', [
			'as'   => 'check',
			'uses' => 'ProductBookingController@checkDeliveryServiceability'
		]);

		Route::get('pin-data/{pinCode}', [
			'as'   => 'pin-data',
			'uses' => 'ProductBookingController@getDeliveryPinData'
		]);
	});

	/* @see: placing the pages roots at the end to avoid unnecessary link mismatches */
	Route::get('/', [
		'as'   => 'home',
		'uses' => 'ProductController@getProductsHomeData'
	]);

	Route::get('{categoryUrl}', [
		'as'   => 'list',
		'uses' => 'ProductController@getProductsListData'
	]);

	Route::get('{categoryUrl}/{typeUrl}/{styleUrl}', [
		'as'   => 'profile',
		'uses' => 'ProductController@getProductProfileData'
	]);
});

/*
 * Search default values & remote data on keystroke
 */
Route::group(['prefix' => 'search', 'namespace' => 'Search'], function () {

	// Search Prefetch elements
	Route::get('prefetch/{cityId}/{occasionUrl}/{pageTypeId}', [
		'as'   => 'search.prefetch',
		'uses' => 'GlobalSearchController@getDefaultDataForAutoHint'
	]);

	// Remote elements
	Route::get('remote/{cityId}/{query}', [
		'as'   => 'search.remote',
		'uses' => 'GlobalSearchController@getJsonDataForAutoHint'
	]);

	// Global Search update url & image
	Route::post('update-table', [
		'as'   => 'update.table',
		'uses' => 'GlobalSearchController@updateSearchableTable'
	]);
});

/**
 * |--------------------------------------------------------------------------
 * | Campaign list page and profile page routes.
 * |--------------------------------------------------------------------------
 */
Route::group(['namespace' => 'Campaigns', 'prefix' => 'c/{campaignUrl}', 'as' => 'campaign.'], function () {

	Route::get('/', [
		'as'   => 'list',
		'uses' => 'CampaignsController@showListPage'
	]);

	Route::get('{packageUrl}', [
		'as'   => 'profile.show',
		'uses' => 'CampaignsController@showProfilePage'
	]);

	Route::post('{packageUrl}', [
		'as'   => 'profile.similar',
		'uses' => 'CampaignsController@showSimilarPackages'
	]);
});

/*
 * View live Redirect URL from dash
 */
Route::group(['prefix' => 'liveUrl', 'as' => 'liveUrl.', 'namespace' => 'Util'], function () {
	Route::get('/{cityId}/{optionTypeId}/{optionId}/redirect', [
		'as'   => 'option',
		'uses' => 'ShowLiveUrlController@redirectToUrl'
	]);

	Route::post('delete-html-cache/{cityId}/{optionTypeId}/{optionId}', [
		'as'   => 'option',
		'uses' => 'ShowLiveUrlController@deleteCachedHTML'
	]);
});

/*
 * Refer & Earn
 */
Route::group(['namespace' => 'Util', 'prefix' => 're', 'as' => 're.'], function () {
	Route::get('{referralCode}', [
		'as'   => 'friend.share',
		'uses' => 'ReferAndEarnController@friendSignUpPage'
	]);

	Route::post('getUniqueLink/{ticketId}', [
		'as'   => 'get.unique.link',
		'uses' => 'ReferAndEarnController@getUniqueReferralCode'
	]);

	Route::post('friend/signup/submit', [
		'as'   => 'friend.signup.submit',
		'uses' => 'ReferAndEarnController@submitFriendSignup'
	]);

	Route::post('update/share/{userId}/{source}', [
		'as'   => 'update.share',
		'uses' => 'ReferAndEarnController@updateReferralShareCount'
	]);

	Route::get('friend/thankyou/{referralCode}/{couponCode}', [
		'as'   => 'friend.thankyou',
		'uses' => 'ReferAndEarnController@friendThankYouPage'
	]);
});

/*
 * Customer Account/Login
 */
Route::group(['namespace' => 'Util', 'prefix' => 'my', 'as' => 'my.'], function () {
	Route::get('re/share', [
		'as'   => 'share',
		'uses' => 'ReferAndEarnController@getCustomerStatistics'
	]);
});

/*
 * Invitation + RSVP
 */
Route::group(['namespace' => 'ECards', 'prefix' => 'e-cards', 'as' => 'e-cards.'], function () {
	Route::get('/', [
		'as'   => 'home',
		'uses' => 'ECardsController@showInviteTemplates'
	]);

	Route::get('invites', [
		'as'   => 'invites',
		'uses' => 'ECardsController@showInviteTemplates'
	]);

	Route::get('wish-cards', [
		'as'   => 'wish-cards',
		'uses' => 'ECardsController@showWishCardTemplates'
	]);

	Route::get('thank-you-cards', [
		'as'   => 'thank-you-cards',
		'uses' => 'ECardsController@showThankYouCardTemplates'
	]);

	Route::get('diwali', [
		'as'   => 'diwali',
		'uses' => 'ECardsController@showDiwaliTemplates'
	]);
});

/*
 * Evibe wallet
 */
Route::group(['namespace' => 'Wallet', 'prefix' => 'wallet', 'as' => 'wallet.'], function () {
	Route::get('/', [
		'as'   => 'home',
		'uses' => 'WalletController@transferEvibeCreditToWallet'
	]);

	Route::get('/credit', [
		'as'   => 'credit',
		'uses' => 'WalletController@transferEvibeCreditToWallet'
	]);

	Route::get('/credit/success', [
		'as'   => 'credit.success',
		'uses' => 'WalletController@evibeCreditSuccess'
	]);

	Route::get('/share/{uniqueId}', [
		'as'   => 'share',
		'uses' => 'WalletController@friendLandingPage'
	]);

	Route::post('/claim-my-reward/{uniqueId}', [
		'as'   => 'share.claim',
		'uses' => 'WalletController@claimRewardFromFriend'
	]);

	Route::post('/share-url', [
		'as'   => 'share.url',
		'uses' => 'WalletController@userShareUrl'
	]);

	// Route::post('/create-discount/{userId}/{amount}/{loginUserId}', [
	// 	'as'   => 'create.discount',
	// 	'uses' => 'WalletController@createEvibeCreditLink'
	// ]);
});

Route::group(['namespace' => 'Util'], function () {
	Route::get('invite/create/party-details/{inviteTemplateID}', [
		'uses' => 'BackSupport@redirectOldInvites'
	]);
});

Route::group(['namespace' => 'Util', 'prefix' => 'i', 'as' => 'iv.'], function () {
	Route::group(['prefix' => 'edit', 'as' => 'create.'], function () {
		Route::get('{inviteTemplateUrl}', [
			'as'   => 'party-details',
			'uses' => 'InvitationController@showInviteTemplateFields'
		]);

		Route::post('store-values', [
			'as'   => 'store-values',
			'uses' => 'InvitationController@validateAndStoreValues'
		]);

		Route::post('store-details', [
			'as'   => 'store-details',
			'uses' => 'InvitationController@validateAndStoreUserDetails'
		]);

		Route::post('download', [
			'as'   => 'download',
			'uses' => 'InvitationController@downloadInviteImage'
		]);

		Route::get('download', [
			'as'   => 'download',
			'uses' => 'InvitationController@downloadInviteImage'
		]);
	});

	Route::get('download-card/{ticketInviteId}', [
		'as'   => 'invite-design',
		'uses' => 'InvitationController@showInviteDesign'
	]);

	/*Route::get('{ivCode}', [
		'as'   => 'show',
		'uses' => 'InvitationController@showInvitationPage'
	]);

	Route::post('rsvp', [
		'as'   => 'rsvp',
		'uses' => 'InvitationController@submitRSVP'
	]);

	Route::post('image', [
		'as'   => 'image',
		'uses' => 'InvitationController@downloadInviteImage'
	]);*/
});

/**
 * |--------------------------------------------------------------------------
 * | Customer Dashboard
 * |--------------------------------------------------------------------------
 */
Route::group(['namespace' => 'Auth', 'prefix' => 'customer', 'as' => 'customer'], function () {
	Route::post('login', [
		'as'   => 'login',
		'uses' => 'LoginController@handleCustomerLogin'
	]);

	Route::get('login/app', [
		'as'   => 'login.app',
		'uses' => 'LoginController@handleCustomerAppLogin'
	]);

	Route::get('login/app/{provider}', [
		'as'   => 'login.app',
		'uses' => 'LoginController@handleCustomerAppLoginThroughSocial'
	]);

	Route::post('signup', [
		'as'   => 'signup',
		'uses' => 'LoginController@handleCustomerSignUp'
	]);

});

Route::group(['namespace' => 'Util', 'prefix' => 'my', 'as' => 'my.', 'middleware' => 'customer.custom'], function () {
	Route::group(['prefix' => 'order', 'as' => 'order.'], function () {
		Route::get('/', [
			'as'   => 'data',
			'uses' => 'CustomerController@showOrders'
		]);
		Route::get('{id}', [
			'as'   => 'customer',
			'uses' => 'OrderController@showCustomerOrderDetails'
		]);
	});
});

/**
 * |--------------------------------------------------------------------------
 * | Partner dashboard
 * |--------------------------------------------------------------------------
 */
Route::group(['namespace' => 'Partner', 'prefix' => 'partner', 'as' => 'partner.dash.'], function () {
	Route::get('login', [
		'as'   => 'new.login',
		'uses' => 'PartnerProfileController@getLoginPage'
	]);

	Route::post('login', [
		'as'   => 'new.login',
		'uses' => 'PartnerProfileController@handlePartnerLogin'
	]);

	Route::get('forgotpassword', [
		'as'   => 'forgot.password',
		'uses' => 'PartnerProfileController@forgotPassword'
	]);

	Route::post('forgotpassword', [
		'as'   => 'forgot.password.email',
		'uses' => 'PartnerProfileController@forgotPasswordEmail'
	]);

	Route::post('forgotpassword/check-token', [
		'as'   => 'forgot.password.check.token',
		'uses' => 'PartnerProfileController@resetPassword'
	]);

	Route::group(['middleware' => 'partner.custom'], function () {
		Route::get('profile', [
			'as'   => 'view.profile',
			'uses' => 'PartnerProfileController@showPartnerDetails'
		]);

		Route::get('profile/changePassword', [
			'as'   => 'change.password',
			'uses' => 'PartnerProfileController@showChangePasswordPage'
		]);

		Route::get('messages', [
			'as'   => 'messages',
			'uses' => 'PartnerProfileController@showMessages'
		]);

		Route::group(['prefix' => 'settlements', 'as' => 'settlements.'], function () {
			Route::get('/', [
				'as'   => 'list',
				'uses' => 'PartnerProfileController@showSettlements'
			]);

			Route::get('{settlementId}', [
				'as'   => 'info',
				'uses' => 'PartnerProfileController@showSettlementInfo'
			]);
		});

		Route::get('packages/{filter}', [
			'as'   => 'view.packages',
			'uses' => 'PartnerProfileController@showPackages'
		]);

		Route::get('add/package', [
			'as'   => 'add.package',
			'uses' => 'PartnerProfileController@addPackage'
		]);

		Route::post('add/package', [
			'as'   => 'add.package.next',
			'uses' => 'PartnerProfileController@redirectPageToFields'
		]);

		Route::get('add/package/1', [
			'as'   => 'add.package.first',
			'uses' => 'PartnerProfileController@getFieldsForType'
		]);

		Route::get('add/package/edit/{packageId}', [
			'as'   => 'add.package.edit',
			'uses' => 'PartnerProfileController@editDetails'
		]);

		Route::post('add/package/1', [
			'as'   => 'add.package.next.first',
			'uses' => 'PartnerProfileController@redirectPageToGallery'
		]);

		Route::get('add/package/images/{packageId}', [
			'as'   => 'add.package.second',
			'uses' => 'PartnerProfileController@getGalleryData'
		]);

		Route::post('add/package/2', [
			'as'   => 'add.package.next.second',
			'uses' => 'PartnerProfileController@redirectReview'
		]);

		Route::get('add/package/review/{packageId}', [
			'as'   => 'add.package.third',
			'uses' => 'PartnerProfileController@showSubmittedDetails'
		]);

		Route::post('add/package/submit/{packageId}', [
			'as'   => 'add.package.next.third.upgrade',
			'uses' => 'PartnerProfileController@upgradeRedirectThankYou'
		]);

		Route::get('add/package/submit/{packageId}', [
			'as'   => 'add.package.next.third',
			'uses' => 'PartnerProfileController@redirectThankYou'
		]);

		Route::get('add/package/thank-you', [
			'as'   => 'add.package.thankYou',
			'uses' => 'PartnerProfileController@showThankYouPage'
		]);

		Route::get('packages/view/{id}', [
			'as'   => 'new.profile.view',
			'uses' => 'PartnerProfileController@showNewPackageProfile'
		]);

		Route::post('get/notifications/messages', [
			'as'   => 'get.notifications.messages',
			'uses' => 'PartnerProfileController@getNotificationsMessages'
		]);

		Route::post('get/notifications/count', [
			'as'   => 'get.notifications.count',
			'uses' => 'PartnerProfileController@getNotificationsCount'
		]);

		Route::post('changePassword', [
			'as'   => 'update.password',
			'uses' => 'PartnerProfileController@updateOldPassword'
		]);

		Route::get('logout', [
			'as'   => 'logout',
			'uses' => 'PartnerProfileController@logout'
		]);

		Route::get('delete/package/{packageId}', [
			'as'   => 'delete.package',
			'uses' => 'PartnerProfileController@deletePackage'
		]);

		Route::get('delete/image/{packageId}/{imageId}', [
			'as'   => 'delete.image',
			'uses' => 'PartnerProfileController@deleteImage'
		]);

		Route::get("dashboard", [
			"as"   => "dashboard",
			"uses" => "PartnerBookingsController@showPartnerDigest"
		]);

		Route::post("dashboard/filter", [
			"as"   => "dashboard.filter",
			"uses" => "PartnerBookingsController@addDashboardFilter"
		]);

		Route::post("review/submit/{reviewId}", [
			"as"   => "submit.review",
			"uses" => "PartnerProfileController@submitPartnerResponse"
		]);

		Route::get("enquiries", [
			"as"   => "enquiries",
			"uses" => "PartnerBookingsController@showEnquiries"
		]);

		Route::post("enquiries/reply/{enquiryId}", [
			"as"   => "enquiries.reply",
			"uses" => "PartnerBookingsController@sendEnquiryReply"
		]);

		Route::get("orders", [
			"as"   => "orders",
			"uses" => "PartnerBookingsController@showOrders"
		]);

		Route::get("orders/{orderId}", [
			"as"   => "order.details",
			"uses" => "PartnerBookingsController@showOrderProfile"
		]);

		Route::get("deliveries", [
			"as"   => "deliveries",
			"uses" => "PartnerBookingsController@showDeliveries"
		]);

		Route::get("deliveries/{deliveryId}", [
			"as"   => "delivery.details",
			"uses" => "PartnerBookingsController@showDeliveryProfile"
		]);

		Route::post("delivery/{deliveryId}/upload", [
			"as"   => "delivery.upload",
			"uses" => "PartnerBookingsController@uploadImages"
		]);

		Route::get("delivery/{deliveryId}/done", [
			"as"   => "delivery.done",
			"uses" => "PartnerBookingsController@partnerDeliveryDone"
		]);

		Route::get("delivery/{deliveryId}/delete/{imageId}", [
			"as"   => "delivery.image.delete",
			"uses" => "PartnerBookingsController@deleteDeliveryImage"
		]);

		// eee
		Route::get('{shortUrl}/profile', [
			'as'   => 'new.profile',
			'uses' => 'PartnerProfileController@showPartnerProfile'
		]);

		Route::get('{shortUrl}', [
			'as'   => 'handleLogin',
			'uses' => 'PartnerProfileController@handlePartnerShortUrl'
		]);

		Route::post('contact', [
			'as'   => 'contact',
			'uses' => 'PartnerProfileController@postContact'
		]);
	});
});

/**
 * |--------------------------------------------------------------------------
 * | Back support for partner link change
 * |--------------------------------------------------------------------------
 */
Route::group(['namespace' => 'Util', 'as' => 'partner.'], function () {
	Route::get('p/login', [
		'as'   => 'login',
		'uses' => 'BackSupport@redirectPartnerProfile'
	]);

	Route::get('p/{shortUrl}/profile', [
		'as'   => 'profile',
		'uses' => 'BackSupport@redirectPartnerProfile'
	]);

	Route::get('p/{shortUrl}', [
		'as'   => 'handleLogin',
		'uses' => 'BackSupport@redirectPartnerProfile'
	]);
});

/**
 * |--------------------------------------------------------------------------
 * | Checkout page & Payments
 * |--------------------------------------------------------------------------
 */
Route::group(['namespace' => 'Payments', 'prefix' => 'pay', 'as' => 'pay.'], function () {
	// Normal bookings from order process email
	Route::group(['as' => 'normal.'], function () {
		Route::get('checkout', [
			'as'   => 'checkout',
			'uses' => 'TicketBookingPaymentController@showCheckoutPage'
		]);

		Route::post('process/{ticketId}', [
			'as'   => 'process',
			'uses' => 'TicketBookingPaymentController@processPayment'
		]);

		Route::get('success/{ticketId?}', [
			'as'   => 'success',
			'uses' => 'TicketBookingPaymentController@onPaymentSuccess'
		]);

		Route::get('request-new-payment-link/{ticketId}', [
			'as'   => 'requestNewPaymentLink',
			'uses' => 'TicketBookingPaymentController@triggerRequestNewPaymentLink'
		]);

		// send individual receipt to provider & customer
		Route::post('update-receipt/{ticketId}', 'TicketBookingPaymentController@sendUpdatedReceipt');
	});

	// Payment Gateways Group
	Route::group(['as' => 'pgs.'], function () {
		// RazorPay
		Route::group(['prefix' => 'razor', 'as' => 'razor.'], function () {
			Route::post('initiate/{ticketId}', [
				'as'   => 'init',
				'uses' => 'RazorPayController@onPaymentInit'
			]);

			Route::post('success', [
				'as'   => 'success',
				'uses' => 'RazorPayController@onPaymentReceived'
			]);
		});

		// PayUMoney
		Route::group(['prefix' => 'payUMoney', 'as' => 'payU.'], function () {
			Route::post('initiate/{ticketId}', [
				'as'   => 'init',
				'uses' => 'PayUController@onPaymentInit'
			]);

			Route::post('success/{param?}', [
				'as'   => 'success',
				'uses' => 'PayUController@onPaymentReceived'
			]);
		});

		// FreeCharge
		Route::group(['prefix' => 'freeCharge', 'as' => 'freeCharge.'], function () {
			Route::post('initiate/{ticketId}', [
				'as'   => 'init',
				'uses' => 'FreeChargeController@onPaymentInit'
			]);

			Route::post('success', [
				'as'   => 'success',
				'uses' => 'FreeChargeController@onPaymentReceived'
			]);
		});

		// Paytm
		Route::group(['prefix' => 'paytm', 'as' => 'paytm.'], function () {
			Route::post('initiate/{ticketId}', [
				'as'   => 'init',
				'uses' => 'PaytmController@onPaymentInit'
			]);

			Route::post('success', [
				'as'   => 'success',
				'uses' => 'PaytmController@onPaymentReceived'
			]);
		});
	});

	// Common routes for the auto bookings, normal bookings
	Route::group(['as' => 'common.'], function () {
		Route::post('intermediate/{ticketId}', [
			'as'   => 'intermediate',
			'uses' => 'BasePaymentController@showIntermediatePage'
		]);

		Route::any('fail', [
			'as'   => 'fail',
			'uses' => 'BasePaymentController@onPaymentFailure'
		]);

		// update google client ID in DB
		Route::post('update/google-client-id', [
			'as'   => 'update.gcid',
			'uses' => 'BasePaymentController@updateGoogleClientID'
		]);
	});

	// Checking the status of current transaction count for payment option
	Route::group(['as' => 'check.'], function () {
		Route::post('check/payment-txn/{ticketId}', [
			'as'   => 'payment.transaction',
			'uses' => 'BasePaymentController@checkPaymentTransactionCount'
		]);
	});
});

/**
 * |--------------------------------------------------------------------------
 * | Auto booking routes
 * |--------------------------------------------------------------------------
 */
Route::group(['namespace' => 'AutoBook', 'prefix' => 'auto-book', 'as' => 'auto-book.'], function () {

	// Route to calculate price based on transport distance
	Route::post('transport-price-calculate/{mapId}/{mapTypeId}', [
		'as'   => 'transport-price-calculate',
		'uses' => 'HandlerControllers\Util\BaseAutoBookingHandlerController@calculatePriceForDelivery'
	]);

	// Route to fetch price for a particular option
	Route::post('fetch-price/{optionId}/{optionTypeId}/{occasionId}', [
		'as'   => 'fetch-price',
		'uses' => 'HandlerControllers\Util\BaseAutoBookingHandlerController@fetchPrice'
	]);

	// Route to calculate booking price for a particular option
	Route::post('booking-price/{optionId}/{optionTypeId}/{occasionId}', [
		'as'   => 'booking-price',
		'uses' => 'HandlerControllers\Util\BaseAutoBookingHandlerController@calculateBookingPrice'
	]);

	Route::group(['namespace' => 'HandlerControllers\Util', 'prefix' => 'now', 'as' => 'pay.auto.'], function () {

		Route::group(['prefix' => 'validate/{ticketId}', 'as' => 'validate.'], function () {

			Route::post('venue-details', [
				'as'   => 'venue-details',
				'uses' => 'BaseAutoBookingHandlerController@validateCheckoutVenueDetails'
			]);

			Route::post('party-details', [
				'as'   => 'party-details',
				'uses' => 'BaseAutoBookingHandlerController@validateCheckoutPartyDetails'
			]);

			Route::post('order-details', [
				'as'   => 'order-details',
				'uses' => 'BaseAutoBookingHandlerController@validateCheckoutOrderDetails'
			]);

			Route::post('proof-details', [
				'as'   => 'proof-details',
				'uses' => 'BaseAutoBookingHandlerController@validateCheckoutProofDetails'
			]);

			Route::post('contact-details', [
				'as'   => 'contact-details',
				'uses' => 'BaseAutoBookingHandlerController@validateCheckoutContactDetails'
			]);

		});

		Route::group(['prefix' => 'update/{ticketId}', 'as' => 'update.'], function () {

			Route::post('order-details', [
				'as'   => 'order-details',
				'uses' => 'BaseAutoBookingHandlerController@updateCheckoutOrderDetails'
			]);

			Route::post('price-details', [
				'as'   => 'price-details',
				'uses' => 'BaseAutoBookingHandlerController@updateCheckoutPriceDetails'
			]);

		});

		Route::get('checkout', [
			'as'   => 'checkout',
			'uses' => 'BaseAutoBookingHandlerController@showABCheckoutPage'
		]);

		Route::post('process/{ticketId}', [
			'as'   => 'process',
			'uses' => 'BaseAutoBookingHandlerController@processPayment'
		]);

		Route::get('success/{ticketId}', [
			'as'   => 'success',
			'uses' => 'BaseAutoBookingHandlerController@onPaymentSuccess'
		]);
	});

	Route::group(['namespace' => 'ActionControllers\Util', 'as' => 'actions.'], function () {
		// Show the options to confirm or cancel the booking to partners
		Route::get('vendor/ask/{ticketId}/{partnerId}/{partnerTypeId}', [
			'as'   => 'partner.ask',
			'uses' => 'BaseAutoBookingActionController@showActionsToVendor'
		]);

		// Confirm auto booking
		Route::post('confirm/{ticketId}/{partnerId}/{partnerTypeId}', [
			'as'   => 'partner.accept',
			'uses' => 'BaseAutoBookingActionController@confirmAutoBooking'
		]);

		// Cancel auto booking
		Route::post('cancel/{ticketId}/{partnerId}/{partnerTypeId}', [
			'as'   => 'partner.reject',
			'uses' => 'BaseAutoBookingActionController@cancelAutoBooking'
		]);

		// confirm success
		Route::get('accept/success/{ticketId}/{partnerId}/{partnerTypeId}', [
			'as'   => 'accept.success',
			'uses' => 'BaseAutoBookingActionController@showAcceptSuccessPage'
		]);

		Route::get('reject/success/{ticketId}/{partnerId}/{partnerTypeId}', [
			'as'   => 'reject.success',
			'uses' => 'BaseAutoBookingActionController@showRejectSuccessPage'
		]);
	});

	// Service auto booking routes started
	Route::group(['prefix' => 'ent', 'as' => 'ent.'], function () {
		Route::group(['namespace' => 'HandlerControllers', 'prefix' => 'now', 'as' => 'pay.auto.'], function () {
			Route::post('init/{mapId}/{occasionId}', [
				'as'   => 'init',
				'uses' => 'EntAutoBookingHandlerController@initAutoBooking'
			]);

			Route::get('checkout', [
				'as'   => 'checkout',
				'uses' => 'EntAutoBookingHandlerController@showCheckoutPage'
			]);

			Route::post('process/{ticketId}', [
				'as'   => 'process',
				'uses' => 'EntAutoBookingHandlerController@processPayment'
			]);

			Route::get('success/{ticketId}', [
				'as'   => 'success',
				'uses' => 'EntAutoBookingHandlerController@onPaymentSuccess'
			]);
		});
	});

	// Decor auto booking routes started
	Route::group(['prefix' => 'decor', 'as' => 'decor.'], function () {
		Route::group(['namespace' => 'ActionControllers', 'as' => 'actions.'], function () {
			// Show the options to confirm or cancel the booking to partners
			Route::get('vendor/ask/{ticketId}', [
				'as'   => 'partner.ask',
				'uses' => 'DecorAutoBookingActionController@showActionsToVendor'
			]);

			// Confirm auto booking
			Route::post('confirm/{ticketId}', [
				'as'   => 'partner.accept',
				'uses' => 'DecorAutoBookingActionController@confirmAutoBooking'
			]);

			// Cancel auto booking
			Route::post('cancel/{ticketId}', [
				'as'   => 'partner.reject',
				'uses' => 'DecorAutoBookingActionController@cancelAutoBooking'
			]);

			// confirm success
			Route::get('accept/success/{ticketId}', [
				'as'   => 'accept.success',
				'uses' => 'DecorAutoBookingActionController@showAcceptSuccessPage'
			]);

			Route::get('reject/success/{ticketId}', [
				'as'   => 'reject.success',
				'uses' => 'DecorAutoBookingActionController@showRejectSuccessPage'
			]);
		});

		//
		Route::group(['namespace' => 'HandlerControllers', 'prefix' => 'now', 'as' => 'pay.auto.'], function () {
			Route::post('init/{mapId}/{occasionId}', [
				'as'   => 'init',
				'uses' => 'DecorAutoBookingHandlerController@initAutoBooking'
			]);

			Route::get('checkout', [
				'as'   => 'checkout',
				'uses' => 'DecorAutoBookingHandlerController@showCheckoutPage'
			]);

			Route::post('process/{ticketId}', [
				'as'   => 'process',
				'uses' => 'DecorAutoBookingHandlerController@processPayment'
			]);

			Route::get('success/{ticketId}', [
				'as'   => 'success',
				'uses' => 'DecorAutoBookingHandlerController@onPaymentSuccess'
			]);
		});

	});

	// Surprise Packages auto booking routes started
	Route::group(['prefix' => 'surprises-packages', 'as' => 'surprises-packages.'], function () {

		Route::group(['namespace' => 'ActionControllers', 'as' => 'actions.'], function () {
			// Show the options to confirm or cancel the booking to vendors
			Route::get('vendor/ask/{ticketId}', [
				'as'   => 'vendor.ask',
				'uses' => 'SurprisePackageAutoBookingActionController@showActionsToVendor'
			]);

			// Confirm auto booking
			Route::post('confirm/{ticketId}', [
				'as'   => 'vendor.accept',
				'uses' => 'SurprisePackageAutoBookingActionController@confirmAutoBooking'
			]);

			// Cancel auto booking
			Route::post('cancel/{ticketId}', [
				'as'   => 'vendor.reject',
				'uses' => 'SurprisePackageAutoBookingActionController@cancelAutoBooking'
			]);

			// confirm success
			Route::get('accept/success/{ticketId}', [
				'as'   => 'accept.success',
				'uses' => 'SurprisePackageAutoBookingActionController@showAcceptSuccessPage'
			]);

			Route::get('reject/success/{ticketId}', [
				'as'   => 'reject.success',
				'uses' => 'SurprisePackageAutoBookingActionController@showRejectSuccessPage'
			]);
		});

		// Auto bookings for Bachelor and pre post from main website
		Route::group(['namespace' => 'HandlerControllers', 'prefix' => 'now', 'as' => 'pay.auto.'], function () {
			Route::post('init/{mapTypeId}/{mapId}/{typeId}', [
				'as'   => 'init',
				'uses' => 'SurprisePackageAutoBookingHandlerController@initAutoBooking'
			]);

			Route::get('checkout', [
				'as'   => 'checkout',
				'uses' => 'SurprisePackageAutoBookingHandlerController@showCheckoutPage'
			]);

			Route::post('process/{ticketId}', [
				'as'   => 'process',
				'uses' => 'SurprisePackageAutoBookingHandlerController@processPayment'
			]);

			Route::get('success/{ticketId}', [
				'as'   => 'success',
				'uses' => 'SurprisePackageAutoBookingHandlerController@onPaymentSuccess'
			]);
		});

	});

	// Venue deals auto booking routes started
	Route::group(['prefix' => 'venue-deals', 'as' => 'venue-deals.'], function () {
		Route::group(['namespace' => 'ActionControllers', 'as' => 'actions.'], function () {
			// Show the options to confirm or cancel the booking to vendors
			Route::get('vendor/ask/{ticketId}', [
				'as'   => 'vendor.ask',
				'uses' => 'VenueDealsAutoBookingActionController@showActionsToVendor'
			]);

			// Confirm auto booking
			Route::post('confirm/{ticketId}', [
				'as'   => 'vendor.accept',
				'uses' => 'VenueDealsAutoBookingActionController@confirmAutoBooking'
			]);

			// Cancel auto booking
			Route::post('cancel/{ticketId}', [
				'as'   => 'vendor.reject',
				'uses' => 'VenueDealsAutoBookingActionController@cancelAutoBooking'
			]);

			// confirm success
			Route::get('accept/success/{ticketId}', [
				'as'   => 'accept.success',
				'uses' => 'VenueDealsAutoBookingActionController@showAcceptSuccessPage'
			]);

			Route::get('reject/success/{ticketId}', [
				'as'   => 'reject.success',
				'uses' => 'VenueDealsAutoBookingActionController@showRejectSuccessPage'
			]);
		});

		//
		Route::group(['namespace' => 'HandlerControllers', 'prefix' => 'now', 'as' => 'pay.auto.'], function () {
			Route::post('init/{mapTypeId}/{mapId}/{occasionId}/{typeId}', [
				'as'   => 'init',
				'uses' => 'VenueDealsAutoBookingHandlerController@initAutoBooking'
			]);

			Route::get('checkout', [
				'as'   => 'checkout',
				'uses' => 'VenueDealsAutoBookingHandlerController@showCheckoutPage'
			]);

			Route::post('process/{ticketId}', [
				'as'   => 'process',
				'uses' => 'VenueDealsAutoBookingHandlerController@processPayment'
			]);

			Route::get('success/{ticketId}', [
				'as'   => 'success',
				'uses' => 'VenueDealsAutoBookingHandlerController@onPaymentSuccess'
			]);
		});

	});

	/**
	 * Cakes auto booking routes
	 *
	 * @author vikash <vikash@evibe.in>
	 * @Since  : 3 November 2016, 5:00 PM
	 */

	Route::group(['prefix' => 'cake', 'as' => 'cake.'], function () {
		Route::group(['namespace' => 'ActionControllers', 'as' => 'actions.'], function () {
			// Show the options to confirm or cancel the booking to vendors
			Route::get('partner/ask/{ticketId}', [
				'as'   => 'partner.ask',
				'uses' => 'CakeAutoBookingActionController@showActionsToVendor'
			]);

			// Confirm auto booking
			Route::post('confirm/{ticketId}', [
				'as'   => 'partner.accept',
				'uses' => 'CakeAutoBookingActionController@confirmAutoBooking'
			]);

			// Cancel auto booking
			Route::post('cancel/{ticketId}', [
				'as'   => 'partner.reject',
				'uses' => 'CakeAutoBookingActionController@cancelAutoBooking'
			]);

			// confirm success
			Route::get('accept/success/{ticketId}', [
				'as'   => 'accept.success',
				'uses' => 'CakeAutoBookingActionController@showAcceptSuccessPage'
			]);

			Route::get('reject/success/{ticketId}', [
				'as'   => 'reject.success',
				'uses' => 'CakeAutoBookingActionController@showRejectSuccessPage'
			]);
		});

		Route::group(['namespace' => 'HandlerControllers', 'prefix' => 'now', 'as' => 'pay.auto.'], function () {
			Route::post('init/{cakeId}/{occasionId}', [
				'as'   => 'init',
				'uses' => 'CakeAutoBookingHandlerController@initAutoBooking'
			]);

			Route::get('checkout', [
				'as'   => 'checkout',
				'uses' => 'CakeAutoBookingHandlerController@showCheckoutPage'
			]);

			Route::post('process/{ticketId}', [
				'as'   => 'process',
				'uses' => 'CakeAutoBookingHandlerController@processPayment'
			]);

			Route::get('success/{ticketId}', [
				'as'   => 'success',
				'uses' => 'CakeAutoBookingHandlerController@onPaymentSuccess'
			]);
		});
	});

	// Resorts auto booking routes started
	Route::group(['prefix' => 'resorts', 'as' => 'resorts.'], function () {
		Route::group(['namespace' => 'ActionControllers', 'as' => 'actions.'], function () {
			// Show the options to confirm or cancel the booking to vendors
			Route::get('vendor/ask/{ticketId}', [
				'as'   => 'vendor.ask',
				'uses' => 'ResortAutoBookingActionController@showActionsToVendor'
			]);

			// Confirm auto booking
			Route::post('confirm/{ticketId}', [
				'as'   => 'vendor.accept',
				'uses' => 'ResortAutoBookingActionController@confirmAutoBooking'
			]);

			// Cancel auto booking
			Route::post('cancel/{ticketId}', [
				'as'   => 'vendor.reject',
				'uses' => 'ResortAutoBookingActionController@cancelAutoBooking'
			]);

			// confirm success
			Route::get('accept/success/{ticketId}', [
				'as'   => 'accept.success',
				'uses' => 'ResortAutoBookingActionController@showAcceptSuccessPage'
			]);

			Route::get('reject/success/{ticketId}', [
				'as'   => 'reject.success',
				'uses' => 'ResortAutoBookingActionController@showRejectSuccessPage'
			]);
		});

		//
		Route::group(['namespace' => 'HandlerControllers', 'prefix' => 'now', 'as' => 'pay.auto.'], function () {
			Route::post('init/{mapTypeId}/{mapId}/{typeId}', [
				'as'   => 'init',
				'uses' => 'ResortAutoBookingHandlerController@initAutoBooking'
			]);

			Route::get('checkout', [
				'as'   => 'checkout',
				'uses' => 'ResortAutoBookingHandlerController@showCheckoutPage'
			]);

			Route::post('process/{ticketId}', [
				'as'   => 'process',
				'uses' => 'ResortAutoBookingHandlerController@processPayment'
			]);

			Route::get('success/{ticketId}', [
				'as'   => 'success',
				'uses' => 'ResortAutoBookingHandlerController@onPaymentSuccess'
			]);
		});

	});

	// Villas auto booking routes started
	Route::group(['prefix' => 'villas', 'as' => 'villas.'], function () {
		Route::group(['namespace' => 'ActionControllers', 'as' => 'actions.'], function () {
			// Show the options to confirm or cancel the booking to vendors
			Route::get('vendor/ask/{ticketId}', [
				'as'   => 'vendor.ask',
				'uses' => 'VillaAutoBookingActionController@showActionsToVendor'
			]);

			// Confirm auto booking
			Route::post('confirm/{ticketId}', [
				'as'   => 'vendor.accept',
				'uses' => 'VillaAutoBookingActionController@confirmAutoBooking'
			]);

			// Cancel auto booking
			Route::post('cancel/{ticketId}', [
				'as'   => 'vendor.reject',
				'uses' => 'VillaAutoBookingActionController@cancelAutoBooking'
			]);

			// confirm success
			Route::get('accept/success/{ticketId}', [
				'as'   => 'accept.success',
				'uses' => 'VillaAutoBookingActionController@showAcceptSuccessPage'
			]);

			Route::get('reject/success/{ticketId}', [
				'as'   => 'reject.success',
				'uses' => 'VillaAutoBookingActionController@showRejectSuccessPage'
			]);
		});

		//
		Route::group(['namespace' => 'HandlerControllers', 'prefix' => 'now', 'as' => 'pay.auto.'], function () {
			Route::post('init/{mapTypeId}/{mapId}/{typeId}', [
				'as'   => 'init',
				'uses' => 'VillaAutoBookingHandlerController@initAutoBooking'
			]);

			Route::get('checkout', [
				'as'   => 'checkout',
				'uses' => 'VillaAutoBookingHandlerController@showCheckoutPage'
			]);

			Route::post('process/{ticketId}', [
				'as'   => 'process',
				'uses' => 'VillaAutoBookingHandlerController@processPayment'
			]);

			Route::get('success/{ticketId}', [
				'as'   => 'success',
				'uses' => 'VillaAutoBookingHandlerController@onPaymentSuccess'
			]);
		});

	});

	// Lounges auto booking routes started
	Route::group(['prefix' => 'lounges', 'as' => 'lounges.'], function () {
		Route::group(['namespace' => 'ActionControllers', 'as' => 'actions.'], function () {
			// Show the options to confirm or cancel the booking to vendors
			Route::get('vendor/ask/{ticketId}', [
				'as'   => 'vendor.ask',
				'uses' => 'LoungeAutoBookingActionController@showActionsToVendor'
			]);

			// Confirm auto booking
			Route::post('confirm/{ticketId}', [
				'as'   => 'vendor.accept',
				'uses' => 'LoungeAutoBookingActionController@confirmAutoBooking'
			]);

			// Cancel auto booking
			Route::post('cancel/{ticketId}', [
				'as'   => 'vendor.reject',
				'uses' => 'LoungeAutoBookingActionController@cancelAutoBooking'
			]);

			// confirm success
			Route::get('accept/success/{ticketId}', [
				'as'   => 'accept.success',
				'uses' => 'LoungeAutoBookingActionController@showAcceptSuccessPage'
			]);

			Route::get('reject/success/{ticketId}', [
				'as'   => 'reject.success',
				'uses' => 'LoungeAutoBookingActionController@showRejectSuccessPage'
			]);
		});

		//
		Route::group(['namespace' => 'HandlerControllers', 'prefix' => 'now', 'as' => 'pay.auto.'], function () {
			Route::post('init/{mapTypeId}/{mapId}/{typeId}', [
				'as'   => 'init',
				'uses' => 'LoungeAutoBookingHandlerController@initAutoBooking'
			]);

			Route::get('checkout', [
				'as'   => 'checkout',
				'uses' => 'LoungeAutoBookingHandlerController@showCheckoutPage'
			]);

			Route::post('process/{ticketId}', [
				'as'   => 'process',
				'uses' => 'LoungeAutoBookingHandlerController@processPayment'
			]);

			Route::get('success/{ticketId}', [
				'as'   => 'success',
				'uses' => 'LoungeAutoBookingHandlerController@onPaymentSuccess'
			]);
		});

	});

	// Campaign auto book routes started
	Route::group(['prefix' => 'campaign', 'as' => 'campaign.'], function () {
		Route::group(['prefix' => 'c', 'as' => 'generic-campaign.'], function () {
			Route::group(['namespace' => 'HandlerControllers\Campaign', 'prefix' => 'now', 'as' => 'pay.auto.'], function () {
				Route::post('init/{packageId}/{campaignEventId}/{imageUpload}', [
					'as'   => 'init',
					'uses' => 'GenericCampaignAutoBookingHandlerController@initAutoBooking'
				]);

				Route::get('checkout', [
					'as'   => 'checkout',
					'uses' => 'GenericCampaignAutoBookingHandlerController@showCheckoutPage'
				]);

				Route::post('process/{ticketId}', [
					'as'   => 'process',
					'uses' => 'GenericCampaignAutoBookingHandlerController@processPayment'
				]);

				Route::get('success/{ticketId}', [
					'as'   => 'success',
					'uses' => 'GenericCampaignAutoBookingHandlerController@onPaymentSuccess'
				]);
			});
		});
	});

	// Generic Packages auto booking routes started
	// Always place them at last
	Route::group(['prefix' => 'generic-package', 'as' => 'generic-package.'], function () {

		Route::group(['namespace' => 'ActionControllers\Util', 'as' => 'actions.'], function () {
			// Show the options to confirm or cancel the booking to vendors
			Route::get('vendor/ask/{ticketId}', [
				'as'   => 'vendor.ask',
				'uses' => 'GenericPackageAutoBookingActionController@showActionsToVendor'
			]);

			// Confirm auto booking
			Route::post('confirm/{ticketId}', [
				'as'   => 'vendor.accept',
				'uses' => 'GenericPackageAutoBookingActionController@confirmAutoBooking'
			]);

			// Cancel auto booking
			Route::post('cancel/{ticketId}', [
				'as'   => 'vendor.reject',
				'uses' => 'GenericPackageAutoBookingActionController@cancelAutoBooking'
			]);

			// confirm success
			Route::get('accept/success/{ticketId}', [
				'as'   => 'accept.success',
				'uses' => 'GenericPackageAutoBookingActionController@showAcceptSuccessPage'
			]);

			Route::get('reject/success/{ticketId}', [
				'as'   => 'reject.success',
				'uses' => 'GenericPackageAutoBookingActionController@showRejectSuccessPage'
			]);
		});

		// Auto bookings for Bachelor and pre post from main website
		Route::group(['namespace' => 'HandlerControllers\Util', 'prefix' => 'now', 'as' => 'pay.auto.'], function () {
			Route::post('init/{mapTypeId}/{mapId}', [
				'as'   => 'init',
				'uses' => 'GenericPackageAutoBookingHandlerController@initAutoBooking'
			]);

			Route::get('checkout', [
				'as'   => 'checkout',
				'uses' => 'GenericPackageAutoBookingHandlerController@showCheckoutPage'
			]);

			Route::post('process/{ticketId}', [
				'as'   => 'process',
				'uses' => 'GenericPackageAutoBookingHandlerController@processPayment'
			]);

			Route::get('success/{ticketId}', [
				'as'   => 'success',
				'uses' => 'GenericPackageAutoBookingHandlerController@onPaymentSuccess'
			]);
		});

	});
});

/**
 * |--------------------------------------------------------------------------
 * | Party Bag
 * |
 * | @author : Saachi <savyasaachi.nb@evibe.in>
 * | @since  : 11 November 2016
 * |--------------------------------------------------------------------------
 */
Route::group(['namespace' => 'PartyBag', 'prefix' => 'my/party-bag', 'as' => 'partybag.'], function () {

	// party bag options
	Route::get('list', [
		'as'   => 'list',
		'uses' => 'PartyBagController@getPartyBag'
	]);

	Route::group(['middleware' => 'customer.custom'], function () {
		// add to party bag
		Route::post('add', [
			'as'   => 'add',
			'uses' => 'PartyBagController@shortlistOption'
		]);

		// delete from party bag
		Route::post('delete', [
			'as'   => 'delete',
			'uses' => 'PartyBagController@undoShortlistOption'
		]);

		// fetch data for vuejs
		Route::get('partybag-data', [
			'as'   => 'partybag-data',
			'uses' => 'PartyBagController@getPartyBagJSONData'
		]);

		// enquire party bag
		Route::post('enquire', [
			'as'   => 'enquire',
			'uses' => 'PartyBagController@enquirePartyBagOptions'
		]);

		// validate party bag checkout
		Route::post('validate-checkout', [
			'as'   => 'validate-checkout',
			'uses' => 'PartyBagController@validatePartyBagCheckout'
		]);

		// enquire party bag
		Route::get('checkout', [
			'as'   => 'checkout',
			'uses' => 'PartyBagController@initPartyBagAutoBooking'
		]);

		Route::post('transport-price-calculate', [
			'as'   => 'transport-price-calculate',
			'uses' => 'PartyBagController@calculateTransport'
		]);
	});
});

Route::group(['namespace' => 'PartyBag', 'prefix' => 'my/party-bag', 'as' => 'partybag.'], function () {
	// add shortlist option data to session
	Route::post('temp', [
		'as'   => 'temp-shortlist',
		'uses' => 'PartyBagController@tempShortlistOption'
	]);

	Route::group(['prefix' => 'pre-shortlist', 'as' => 'pre-shortlist.'], function () {
		Route::post('/', [
			'as'   => 'show',
			'uses' => 'PartyBagController@showPreShortlistModal'
		]);

		Route::post('submit', [
			'as'   => 'submit',
			'uses' => 'PartyBagController@submitPreShortlistModal'
		]);
	});

	Route::post('clear', [
		'as'   => 'clear',
		'uses' => 'PartyBagController@clearPartyBag'
	]);
});

Route::group(['namespace' => 'PartyBag', 'prefix' => 'my/wish-list', 'as' => 'wishlist.'], function () {
	// add to wishlist
	Route::get('/', [
		'as'   => 'list',
		'uses' => 'PartyBagController@getWishListOptions'
	]);

	// add to wishlist
	Route::post('add', [
		'as'   => 'add',
		'uses' => 'PartyBagController@wishlistOption'
	]);

	// check whether product is there in the wishlist or not
	Route::post('status', [
		'as'   => 'status',
		'uses' => 'PartyBagController@wishlistOptionStatus'
	]);
});

/**
 * |--------------------------------------------------------------------------
 * | User Signup
 * |
 * | @author : Saachi <savyasaachi.nb@evibe.in>
 * | @since  : 11 November 2016
 * |--------------------------------------------------------------------------
 */
Route::group(['namespace' => 'Auth', 'as' => 'auth.'], function () {
	Route::get('/redirect/{provider}', [
		'as'   => 'login',
		'uses' => 'SocialAuthController@redirectToProvider'
	]);

	Route::get('/callback/{provider}', [
		'as'   => '',
		'uses' => 'SocialAuthController@handleProviderCallback'
	]);

	// redirect to this route if email is not provided by fb / g+
	Route::get('/user/confirm-email', [
		'as'   => '',
		'uses' => 'UserAuthController@requestSocialUserEmail'
	]);

	// post socialite auth information along with email
	Route::post('/user/createUser', [
		'as'   => 'create',
		'uses' => 'UserAuthController@handleSocialUserEmailSubmit'
	]);

	Route::get('/user/logout', [
		'as'   => 'logout',
		'uses' => 'UserAuthController@logout'
	]);

	// fetch the user specific data ex: name, party bag count, shortlist
	Route::post('/user/userdata', [
		'as'   => 'userdata',
		'uses' => 'UserAuthController@getUserData'
	]);
});

/**
 * |--------------------------------------------------------------------------
 * | City specific content
 * |--------------------------------------------------------------------------
 */
Route::group(['prefix' => '{cityUrl}', 'as' => 'city.'], function () {
	/**
	 * |--------------------------------------------------------------------------
	 * | Lockdown Packages Landing Page
	 * |--------------------------------------------------------------------------
	 */
	Route::group(['prefix' => '/lockdown-packages', 'as' => 'lockdown-packages.', 'namespace' => 'LandingPages'], function () {
		Route::get('/', [
			'as'   => 'landingpage',
			'uses' => 'LandingPagesController@showLockdownPackages'
		]);
	});

	/**
	 * |--------------------------------------------------------------------------
	 * | Occassional Home Pages
	 * |--------------------------------------------------------------------------
	 */
	Route::group(['prefix' => '/h/{pageUrl}', 'as' => 'occasion.home.', 'namespace' => 'Base'], function () {
		Route::get('/', [
			'as'   => 'landingpage',
			'uses' => 'BaseOccasionHomeController@showPage'
		]);
	});
	// Home page related
	Route::group(['namespace' => 'Util'], function () {

		// Show city specific home page
		Route::get('/', [
			'as'   => 'home',
			'uses' => 'CityHomeController@showCityHomePage'
		]);

		// Post website ticket
		Route::post('book', [
			'as'   => 'home.enquire',
			'uses' => 'TicketsController@postHomeTicket'
		]);

		// Post javascript website ticket
		Route::post('js-book', [
			"as"   => "header.enquire",
			'uses' => 'TicketsController@postHeaderTicket'
		]);

	});

	// City specific Collections
	Route::group(['namespace' => 'Cities\Util\Collections',
	              'as'        => 'collection.',
	              'prefix'    => config('evibe.occasion.collections.results_url')
	             ], function () {

		Route::get('/', [
			'as'   => 'list',
			'uses' => 'CityCollectionsController@showCollectionsList'
		]);

		Route::get('/{collectionUrl}', [
			'as'   => 'profile',
			'uses' => 'CityCollectionsController@showCollectionProfile'
		]);

	});

	// Occasions specific
	Route::group(['namespace' => 'Occasions'], function () {
		/**
		 * Routes for Naming Ceremony
		 */
		Route::group(['namespace' => 'NamingCeremony', 'prefix' => config('evibe.occasion.naming_ceremony.url'), 'as' => 'occasion.ncdecors.'], function () {
			Route::get('decorations', [
				'as'   => 'list',
				'uses' => 'NamingCeremonyDecorController@showList'
			]);

			Route::get('c/{categoryUrl}', [
				'as'   => 'category',
				'uses' => 'NamingCeremonyDecorController@showList'
			]);

			Route::get('{decorUrl}', [
				'as'   => 'profile',
				'uses' => 'NamingCeremonyDecorController@showProfile'
			]);
		});

		/**
		 * Routes for Store Openings
		 */
		Route::group(['namespace' => 'StoreOpening', 'prefix' => config('evibe.occasion.store-opening.url'), 'as' => 'occasion.sodecors.'], function () {
			Route::get('decorations', [
				'as'   => 'list',
				'uses' => 'StoreOpeningDecorController@showList'
			]);

			Route::get('c/{categoryUrl}', [
				'as'   => 'category',
				'uses' => 'StoreOpeningDecorController@showList'
			]);

			Route::get('{decorUrl}', [
				'as'   => 'profile',
				'uses' => 'StoreOpeningDecorController@showProfile'
			]);
		});

		/**
		 * Routes for Baby Shower
		 */
		Route::group(['namespace' => 'BabyShower', 'prefix' => config('evibe.occasion.baby-shower.url'), 'as' => 'occasion.bsdecors.'], function () {
			Route::get('decorations', [
				'as'   => 'list',
				'uses' => 'BabyShowerDecorController@showList'
			]);

			Route::get('c/{categoryUrl}', [
				'as'   => 'category',
				'uses' => 'BabyShowerDecorController@showList'
			]);

			Route::get('{decorUrl}', [
				'as'   => 'profile',
				'uses' => 'BabyShowerDecorController@showProfile'
			]);
		});

		// Candle light dinner
		Route::group(['namespace' => 'CandleLightDinner',
		              'as'        => 'cld.'
		             ], function () {

			Route::get('/candle-light-dinner/{location?}', [
				'as'   => 'list',
				'uses' => 'CLDHomeController@showOptions'
			]);

			Route::get('/{profileUrl}/candle-light-dinner', [
				//'middleware' => 'cache.response',
				'as'   => 'profile',
				'uses' => 'CLDHomeController@showProfileData'
			]);
		});

		// Corporate events
		Route::group(['namespace' => 'Corporates', 'prefix' => 'corporate-events', 'as' => 'occasion.corporate.'], function () {

			Route::get('/', [
				'as'   => 'home',
				'uses' => 'CorporatesHomeController@showCorporateHome'
			]);

			Route::post('enquiry/save', [
				'as'   => 'enquiry.save',
				'uses' => 'CorporatesHomeController@saveCorporateEnquiry'
			]);
		});

		// House Warming
		Route::group(['namespace' => 'HouseWarming', 'prefix' => config('evibe.occasion.house-warming.url'), 'as' => 'occasion.house-warming.'], function () {

			// For the House Warming home page
			Route::get('/', [
				'as'   => 'home',
				'uses' => 'HouseWarmingHomeController@showHome'
			]);

			// House Warming Decorations List and Profile Pages
			Route::group(['prefix' => config('evibe.occasion.house-warming.decors.results_url'),
			              'as'     => 'decors.'], function () {

				// @see: using same method for all list, category list
				Route::get('all', [
					'as'   => 'list',
					'uses' => 'HouseWarmingDecorController@showList'
				]);

				// understand why this is written in birthday decors
				Route::get('c/{categoryUrl}', [
					'as'   => 'category',
					'uses' => 'HouseWarmingDecorController@showList'
				]);
				// end of the understanding

				Route::get('{decorUrl}', [
					'as'   => 'profile',
					'uses' => 'HouseWarmingDecorController@showProfile'
				]);
			});

			//  House Warming Food Package List and Profile Pages
			Route::group(['as' => 'food.'], function () {

				Route::get(config('evibe.occasion.house-warming.food.results_url'), [
					'as'   => 'list',
					'uses' => 'HouseWarmingFoodController@showFoodList'
				]);

				Route::get(config('evibe.occasion.house-warming.food.profile_url') . '/{foodUrl}', [
					'as'   => 'profile',
					'uses' => 'HouseWarmingFoodController@showFoodProfile'
				]);

			});

			// House Warming Tents Package List and Profile Pages
			Route::group(['as' => 'tent.'], function () {

				Route::get(config('evibe.occasion.house-warming.tents.results_url'), [
					'as'   => 'list',
					'uses' => 'HouseWarmingTentController@showTentList'
				]);

				Route::get(config('evibe.occasion.house-warming.tents.profile_url') . '/{packageUrl}', [
					'as'   => 'profile',
					'uses' => 'HouseWarmingTentController@showTentProfile'
				]);

			});

			// House Warming Priests Package List and Profile Pages
			Route::group(['as' => 'priest.'], function () {

				Route::get(config('evibe.occasion.house-warming.priest.results_url'), [
					'as'   => 'list',
					'uses' => 'HouseWarmingPriestController@showPriestList'
				]);

				Route::get(config('evibe.occasion.house-warming.priest.profile_url') . '/{packageUrl}', [
					'as'   => 'profile',
					'uses' => 'HouseWarmingPriestController@showPriestProfile'
				]);

			});

			/**
			 * Routes for house warming collections
			 *
			 * @author : Saachi <savyasaachi.nb@evibe.in>
			 * @since  : 23 Dec 2016
			 */
			Route::group(['as' => 'collection.', 'prefix' => 'collections'], function () {

				Route::get('/', [
					'as'   => 'list',
					'uses' => 'HouseWarmingCollectionController@showCollectionList'
				]);

				Route::get('/{collectionUrl}', [
					'as'   => 'profile',
					'uses' => 'HouseWarmingCollectionController@showCollectionProfile'
				]);

			});

			/*==== House Warming cakes ====*/
			/**
			 * @author Nama Chaitanya
			 * @since  29 APr 2017
			 *
			 */
			Route::group(['as' => 'cakes.'], function () {

				Route::get(config('evibe.occasion.house-warming.cakes.results_url'), [
					'as'   => 'list',
					'uses' => 'HouseWarmingCakeController@showList'
				]);

				Route::get(config('evibe.occasion.house-warming.cakes.profile_url') . '/{cakeUrl}', [
					'as'   => 'profile',
					'uses' => 'HouseWarmingCakeController@showProfile'
				]);

			});

			/*==== House Warming Add-ons ====*/
			/**
			 * @author Nama Chaitanya
			 * @since  29 Apr 2017
			 *
			 */
			Route::group(['as' => 'ent.'], function () {

				Route::get(config('evibe.occasion.house-warming.ent.results_url'), [
					'as'   => 'list',
					'uses' => 'HouseWarmingAddonController@showList'
				]);

				Route::get(config('evibe.occasion.house-warming.ent.results_url') . '/c/{categoryUrl}', [
					'as'   => 'list.category',
					'uses' => 'HouseWarmingAddonController@showList'
				]);

				Route::get(config('evibe.occasion.house-warming.ent.profile_url') . '/{entUrl}', [
					'as'   => 'profile',
					'uses' => 'HouseWarmingAddonController@showProfile'
				]);

			});

		});

		// Couple Experiences changed to Surprises
		Route::group(['namespace' => 'Surprises', 'prefix' => config('evibe.occasion.surprises.url'), 'as' => 'occasion.surprises.'], function () {

			/* === For the Surprises home page =====*/
			Route::get('/', [
				'as'   => 'home',
				'uses' => 'SurprisesHomeController@showHome'
			]);

			/* ===== Special Surprise packages List and profile pages ===== */
			Route::group(['as' => 'package.'], function () {

				Route::get(config('evibe.occasion.surprises.package.results_url'), [
					'as'   => 'list',
					'uses' => 'SurprisesPackageController@showPackagesList'
				]);

				Route::get(config('evibe.occasion.surprises.package.results_url') . '/sr/{srString}/so/{soString}/sa/{saString}', [
					'as'   => 'list.surpriseCategories',
					'uses' => 'SurprisesPackageController@showPackagesList'
				]);

				Route::get(config('evibe.occasion.surprises.package.profile_url') . '/{packageUrl}', [
					'as'   => 'profile',
					'uses' => 'SurprisesPackageController@showPackageProfile'
				]);

			});

			/**
			 * Routes for surprise collections
			 *
			 * @author : Saachi <savyasaachi.nb@evibe.in>
			 * @since  : 23 Dec 2016
			 */
			Route::group(['as' => 'collection.', 'prefix' => 'collections'], function () {

				Route::get('/', [
					'as'   => 'list',
					'uses' => 'SurprisesCollectionController@showCollectionList'
				]);

				Route::get('/{collectionUrl}', [
					'as'   => 'profile',
					'uses' => 'SurprisesCollectionController@showCollectionProfile'
				]);

			});
		});

		// PrePost
		Route::group(['namespace' => 'PrePost', 'prefix' => config('evibe.occasion.pre-post.url'), 'as' => 'occasion.pre-post.'], function () {

			/* For the PrePost + Engagement home page */
			Route::group([], function () {

				Route::get('/', [
					'as'   => 'home',
					'uses' => 'PrePostHomeController@showHome'
				]);

				Route::post('/save/site-visit', [
					'as'   => 'save.site.visit',
					'uses' => 'PrePostSiteVisitController@saveSiteVisitEnquiry'
				]);

			});

			/*=== PrePost + Engagement party venue page ====*/
			Route::group(['as' => 'venues.', 'prefix' => 'venues'], function () {

				Route::get('/', [
					'as'   => 'list',
					'uses' => 'PrePostVenueController@showVenueList'
				]);

				Route::get('/{venueUrl}', [
					'as'   => 'profile',
					'uses' => 'PrePostVenueController@showVenueProfile'
				]);

			});

			/*==== Reception, engagement decor styles ====*/
			Route::group(['as' => 'decors.'], function () {

				Route::group(['prefix' => config('evibe.occasion.pre-post.decors.results')], function () {

					Route::get('all', [
						'as'   => 'list',
						'uses' => 'PrePostDecorController@showList'
					]);

					Route::get('c/{categoryUrl}', [
						'as'   => 'category',
						'uses' => 'PrePostDecorController@showList'
					]);

					Route::get('{decorUrl}', [
						'as'   => 'profile',
						'uses' => 'PrePostDecorController@showProfile'
					]);

				});

			});

			/*==== PrePost + Engagement party Entertainment =====*/
			Route::group(['as' => 'ent.'], function () {

				Route::get(config('evibe.occasion.pre-post.ent.results_url'), [
					'as'   => 'list',
					'uses' => 'PrePostEntController@showList'
				]);

				Route::get(config('evibe.occasion.pre-post.ent.results_url') . '/c/{categoryUrl}', [
					'as'   => 'list.category',
					'uses' => 'PrePostEntController@showList'
				]);

				Route::get(config('evibe.occasion.pre-post.ent.profile_url') . '/{entUrl}', [
					'as'   => 'profile',
					'uses' => 'PrePostEntController@showDetails'
				]);

			});

			/* ==== PrePost + Engagement Party Cake ====*/
			Route::group(['as' => 'cakes.'], function () {

				Route::get(config('evibe.occasion.pre-post.cakes.results_url'), [
					'as'   => 'list',
					'uses' => 'PrePostCakeController@showList'
				]);

				Route::get(config('evibe.occasion.pre-post.cakes.profile_url') . '/{cakeUrl}', [
					'as'   => 'profile',
					'uses' => 'PrePostCakeController@showProfile'
				]);

			});

			/**
			 * Routes for prepost + engagement party collections
			 *
			 * @author : Saachi <savyasaachi.nb@evibe.in>
			 * @since  : 23 Dec 2016
			 */
			Route::group(['as' => 'collection.', 'prefix' => 'collections'], function () {
				Route::get('/', [
					'as'   => 'list',
					'uses' => 'PrePostCollectionController@showCollectionList'
				]);

				Route::get('/{collectionUrl}', [
					'as'   => 'profile',
					'uses' => 'PrePostCollectionController@showCollectionProfile'
				]);
			});

		});

		// Youth parties
		Route::group(['namespace' => 'Bachelors', 'prefix' => config('evibe.occasion.bachelor.url'), 'as' => 'occasion.bachelor.', 'middleware' => 'redirect.home'], function () {

			// Youth party home
			Route::group([], function () {
				Route::get('/', [
					'as'   => 'home',
					'uses' => 'BachelorsHomeController@showHome'
				]);
			});

			// Youth party Resorts
			Route::group(['as' => 'resort.'], function () {

				Route::get(config('evibe.occasion.bachelor.resort.results_url'), [
					'as'   => 'list',
					'uses' => 'BachelorsResortController@showResortsList'
				]);

				Route::get(config('evibe.occasion.bachelor.resort.profile_url') . '/{resortUrl}', [
					'as'   => 'profile',
					'uses' => 'BachelorsResortController@showResortProfile'
				]);

			});

			/*==== Youth party Villas ====*/
			Route::group(['as' => 'villa.'], function () {
				Route::get(config('evibe.occasion.bachelor.villa.results_url'), [
					'as'   => 'list',
					'uses' => 'BachelorsVillaController@showVillasList'
				]);

				Route::get(config('evibe.occasion.bachelor.villa.profile_url') . '/{villaUrl}', [
					'as'   => 'profile',
					'uses' => 'BachelorsVillaController@showVillaProfile'
				]);
			});

			/*==== Youth party lounges ====*/
			Route::group(['as' => 'lounge.'], function () {
				Route::get(config('evibe.occasion.bachelor.lounge.results_url'), [
					'as'   => 'list',
					'uses' => 'BachelorsLoungeController@showLoungesList'
				]);

				Route::get(config('evibe.occasion.bachelor.lounge.profile_url') . '/{loungeUrl}', [
					'as'   => 'profile',
					'uses' => 'BachelorsLoungeController@showLoungeProfile'
				]);
			});

			/*==== Youth party entertainment ====*/
			Route::group(['as' => 'ent.'], function () {
				Route::get(config('evibe.occasion.bachelor.ent.results_url'), [
					'as'   => 'list',
					'uses' => 'BachelorsEntController@showList'
				]);

				Route::get(config('evibe.occasion.bachelor.ent.results_url') . '/c/{categoryUrl}', [
					'as'   => 'list.category',
					'uses' => 'BachelorsEntController@showList'
				]);

				Route::get(config('evibe.occasion.bachelor.ent.profile_url') . '/{entUrl}', [
					'as'   => 'profile',
					'uses' => 'BachelorsEntController@showDetails'
				]);
			});

			/*==== Youth party cake ====*/
			Route::group(['as' => 'cakes.'], function () {
				Route::get(config('evibe.occasion.bachelor.cakes.results_url'), [
					'as'   => 'list',
					'uses' => 'BachelorsCakeController@showList'
				]);

				Route::get(config('evibe.occasion.bachelor.cakes.profile_url') . '/{cakeUrl}', [
					'as'   => 'profile',
					'uses' => 'BachelorsCakeController@showProfile'
				]);
			});

			/*==== Youth party food ====*/
			Route::group(['as' => 'food.'], function () {
				Route::get(config('evibe.occasion.bachelor.food.results_url'), [
					'as'   => 'list',
					'uses' => 'BachelorsFoodController@showfoodList'
				]);

				Route::get(config('evibe.occasion.bachelor.food.profile_url') . '/{foodUrl}', [
					'as'   => 'profile',
					'uses' => 'BachelorsFoodController@showfoodProfile'
				]);
			});

			/* ===== Prev bachelor packages ===== */
			Route::group(['as' => 'package.', 'prefix' => 'packages'], function () {
				Route::get('/', [
					'as'   => 'list',
					'uses' => 'BachelorsPackageController@showPackagesList'
				]);

				Route::get('/{packageUrl}', [
					'as'   => 'profile',
					'uses' => 'BachelorsPackageController@showPackageProfile'
				]);
			});

			/**
			 * Routes for bachelors collections
			 *
			 * @author : Saachi <savyasaachi.nb@evibe.in>
			 * @since  : 23 Dec 2016
			 */
			Route::group(['as' => 'collection.', 'prefix' => 'collections'], function () {
				Route::get('/', [
					'as'   => 'list',
					'uses' => 'BachelorsCollectionController@showCollectionList'
				]);

				Route::get('/{collectionUrl}', [
					'as'   => 'profile',
					'uses' => 'BachelorsCollectionController@showCollectionProfile'
				]);
			});

			/*==== Youth party decorations ====*/
			/**
			 * @author Nama Chaitanya
			 * @since  29 APr 2017
			 *
			 */
			Route::group(['as' => 'decors.'], function () {
				Route::get(config('evibe.occasion.bachelor.decors.results_url'), [
					'as'   => 'list',
					'uses' => 'BachelorsDecorController@showList'
				]);

				Route::get(config('evibe.occasion.bachelor.decors.profile_url') . '/{decorUrl}', [
					'as'   => 'profile',
					'uses' => 'BachelorsDecorController@showProfile'
				]);
			});
		});

		/**
		 * |--------------------------------------------------------------------------
		 * | Birthdays
		 * |--------------------------------------------------------------------------
		 * | * @modified 1 Oct 2016 birthdays. to occasion.birthdays.
		 * | * @by: Saachi <savyasaachi.nb@evibe.in>
		 */
		Route::group(['namespace' => 'Birthdays', 'prefix' => config('evibe.occasion.kids_birthdays.url'), 'as' => 'occasion.birthdays.'], function () {

			/**
			 * Routes for birthdays home page
			 *
			 * @since    21 March 2016
			 * @modified 16  June 2016
			 */

			Route::get('/', [
				'as'   => 'home',
				'uses' => 'BirthdayHomeController@showHome'
			]);

			/**
			 * Routes for birthday packages
			 *
			 * @since    25 Jan 2015
			 * @modified 15 Mar 2016
			 */
			Route::group(['as' => 'packages.'], function () {
				Route::get(config('evibe.occasion.kids_birthdays.packages.results_url'), [
					'as'   => 'list',
					'uses' => 'BirthdayPackageController@showList'
				]);

				Route::get(config('evibe.occasion.kids_birthdays.packages.profile_url') . '/{packageUrl}', [
					'as'   => 'profile',
					'uses' => 'BirthdayPackageController@showProfile'
				]);
			});

			/**
			 * Routes for venue deals
			 *
			 * @Since : 24th July
			 * @author: Vikash <vikash@evibe.in>
			 */
			Route::group(['prefix' => config('evibe.results_url.venue_deals'), 'as' => 'venue_deals.'], function () {
				// @see don't change the order of route, both list and filterRoute are calling the same controller function
				Route::get('all', [
					'as'   => 'list',
					'uses' => 'BirthdayVenueDealsController@showList'
				]);

				Route::get('{locationUrl}/{CategoryUrl}', [
					'as'   => 'filterList',
					'uses' => 'BirthdayVenueDealsController@showList'
				]);

				Route::get('{packageUrl}', [
					'as'   => 'profile',
					'uses' => 'BirthdayVenueDealsController@showProfile'
				]);
			});

			/**
			 * Routes for birthday decors
			 *
			 * @since    4 Sept 2015
			 * @modified 15 Mar 2016
			 *
			 * Now categories have separate landing page instead of URL parameter
			 * @modified 16 June 2016
			 */
			Route::group(['prefix' => config('evibe.occasion.kids_birthdays.decors.results'), 'as' => 'decors.'], function () {
				// @see: using same method for all list, category list
				Route::get('all', [
					'as'   => 'list',
					'uses' => 'BirthdayDecorController@showList'
				]);

				Route::get('c/{categoryUrl}', [
					'as'   => 'category',
					'uses' => 'BirthdayDecorController@showList'
				]);

				Route::get('{decorUrl}', [
					'as'   => 'profile',
					'uses' => 'BirthdayDecorController@showProfile'
				]);
			});

			/**
			 * Routes for birthday cakes
			 */
			Route::group(['as' => 'cakes.'], function () {
				Route::get(config('evibe.occasion.kids_birthdays.cakes.results_url'), [
					'as'   => 'list',
					'uses' => 'BirthdayCakeController@showList'
				]);

				Route::get(config('evibe.occasion.kids_birthdays.cakes.profile_url') . '/{cakeUrl}', [
					'as'   => 'profile',
					'uses' => 'BirthdayCakeController@showProfile'
				]);
			});

			/**
			 * Routes for birthday party-halls
			 *
			 * @see: do not change this order
			 */
			Route::group([], function () {
				Route::group(['prefix' => config('evibe.results_url.party_halls'),
				              'as'     => 'party-halls.'], function () {
					Route::get('/', [
						'as'   => 'list',
						'uses' => 'BirthdayPartyHallsController@showList'
					]);

					Route::get('{partyHallUrl}', [
						'as'   => 'profile',
						'uses' => 'BirthdayPartyHallsController@showProfile'
					]);
				});
			});

			/**
			 * Routes for birthday venues
			 *
			 * @see: do not change this order
			 */
			Route::group([], function () {
				Route::group(['prefix' => config('evibe.results_url.venues'),
				              'as'     => 'venues.'], function () {
					Route::get('/', [
						'as'   => 'list',
						'uses' => 'BirthdayVenueController@showList'
					]);

					Route::get('/quick-links', [
						'as'   => 'links',
						'uses' => 'BirthdayVenueController@showQuickLinks'
					]);

					Route::get('{venueUrl}', [
						'as'   => 'profile',
						'uses' => 'BirthdayVenueController@showProfile'
					]);
				});
			});

			/**
			 * Routes for birthday trends
			 */
			Route::group(['prefix' => config('evibe.results_url.trends'),
			              'as'     => 'trends.'], function () {
				Route::get('/', [
					'as'   => 'list',
					'uses' => 'BirthdayTrendController@showList'
				]);

				Route::get('/{trendUrl}', [
					'as'   => 'profile',
					'uses' => 'BirthdayTrendController@showProfile'
				]);
			});

			/**
			 *    Routes For Kids Party Halls
			 */
			Route::group([], function () {
				Route::group([
					             'prefix' => config('evibe.results_url.kids_play_areas'),
					             'as'     => 'kids_play_areas.'
				             ], function () {
					Route::get('/', [
						'as'   => 'list',
						'uses' => 'BirthdayPartyHallsController@showKidsPlayAreas'
					]);

					Route::get('{placeId}', [
						'as'   => 'profile',
						'uses' => 'BirthdayPartyHallsController@showKidsPlayAreasProfile'
					]);
				});
			});

			/**
			 * Routes for birthday ent
			 */
			Route::group(['as' => 'ent.'], function () {
				Route::get(config('evibe.occasion.kids_birthdays.ent.results_url'), [
					'as'   => 'list',
					'uses' => 'BirthdayEntController@showList'
				]);

				Route::get(config('evibe.occasion.kids_birthdays.ent.results_url') . '/c/{categoryUrl}', [
					'as'   => 'list.category',
					'uses' => 'BirthdayEntController@showList'
				]);

				Route::get(config('evibe.occasion.kids_birthdays.ent.profile_url') . '/{entUrl}', [
					'as'   => 'profile',
					'uses' => 'BirthdayEntController@showDetails'
				]);
			});

			/**
			 * Routes for birthday food
			 *
			 * @since    12 Oct 2016
			 * @author   vikash <vikash@evibe.in>
			 */
			Route::group(['as' => 'food.'], function () {
				// @see: using same method for all list, category list
				Route::get(config('evibe.occasion.kids_birthdays.food.results_url'), [
					'as'   => 'list',
					'uses' => 'BirthdayFoodController@showFoodList'
				]);

				Route::get(config('evibe.occasion.kids_birthdays.food.profile_url') . '/{foodUrl}', [
					'as'   => 'profile',
					'uses' => 'BirthdayFoodController@showFoodProfile'
				]);
			});

			/**
			 * Routes for birthday collections
			 *
			 * @author : Saachi <savyasaachi.nb@evibe.in>
			 * @since  : 23 Dec 2016
			 */
			Route::group(['as' => 'collection.', 'prefix' => 'collections'], function () {
				Route::get('/', [
					'as'   => 'list',
					'uses' => 'BirthdayCollectionController@showCollectionList'
				]);

				Route::get('/{collectionUrl}', [
					'as'   => 'profile',
					'uses' => 'BirthdayCollectionController@showCollectionProfile'
				]);
			});

			/* === Birthday Tents Package List and Profile Pages ===*/
			Route::group(['as' => 'tent.'], function () {
				Route::get(config('evibe.occasion.kids_birthdays.tents.results_url'), [
					'as'   => 'list',
					'uses' => 'BirthdayTentController@showTentList'
				]);

				Route::get(config('evibe.occasion.kids_birthdays.tents.profile_url') . '/{packageUrl}', [
					'as'   => 'profile',
					'uses' => 'BirthdayTentController@showTentProfile'
				]);
			});
		});
	});

	/**
	 * Customer stories
	 *
	 * @author: Vikash <vikash@evibe.in>
	 * @Since : 9 july 2016
	 */
	Route::group(['prefix'    => 'customer-reviews',
	              'as'        => 'stories.',
	              'namespace' => 'Util'], function () {

		Route::get('/', [
			'as'   => 'customer',
			'uses' => 'StoriesController@showCustomerStories'
		]);

	});

	/**
	 * Global Search
	 *
	 * @author : Saachi <savyasaachi.nb@evibe.in>
	 * @Since  : 19 september 2016
	 */
	Route::group(['prefix' => 'search', 'namespace' => 'Search'], function () {

		// Global Search Query handler
		Route::get('/', [
			'as'   => 'search',
			'uses' => 'GlobalSearchController@getSearchResultsDBSearch'
		]);
	});

	/**
	 * All campaign
	 *
	 * @author: Vikash
	 * @since : 10 Dec 2016
	 */

	Route::group(['prefix' => 'offers', 'as' => 'offers.', 'namespace' => 'Util'], function () {
		Route::get('/', [
			'as'   => 'freecharge',
			'uses' => 'PagesController@showOffers'
		]);
	});

	/**
	 * Workflow for showing recommendations
	 */
	Route::group(['namespace' => 'Workflow', 'prefix' => 'plan', 'as' => 'workflow.'], function () {
		Route::get('/{planToken?}', [
			'as'   => 'home',
			'uses' => 'WorkflowController@showRequirements'
		]);

		/* Route::post('/save', [
			'as'   => 'save',
			'uses' => 'WorkflowController@saveOptions'
		]);

		Route::post('/{planToken?}', [
			'as'   => 'home.data',
			'uses' => 'WorkflowController@getRequirements'
		]);

		Route::get('/options/{planToken}', [
			'as'   => 'options',
			'uses' => 'WorkflowController@showOptions'
		]);

		Route::post('/options/{planToken}', [
			'as'   => 'options.data',
			'uses' => 'WorkflowController@getOptions'
		]);

		Route::post('/options/enquire/{planToken}', [
			'as'   => 'options.enquire',
			'uses' => 'WorkflowController@enquireNow'
		]);

		Route::post('/options/{planToken}/shortlist/options', [
			'as'   => 'options.shortlist.list',
			'uses' => 'WorkflowController@getShortlistedOptions'
		]);

		Route::post('/options/{planToken}/shortlist', [
			'as'   => 'options.shortlist',
			'uses' => 'WorkflowController@shortlistOption'
		]);

		Route::post('/options/{planToken}/remove', [
			'as'   => 'options.delete',
			'uses' => 'WorkflowController@removeShortlistedOption'
		]); */
	});
});

Route::post('delivery-images', [
	'as'   => 'delivery-images',
	'uses' => 'Base\BaseResultsController@getSelectedDeliveryImages'
]);