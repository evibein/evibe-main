# Evibe.in Main
This change log will contain all the code changes for bug fixes, new features etc. Each version will contain sections: `Added`, `Changed`, `Removed`, `Fixed` (whichever is applicable).

### v5.8.66 (*16 Mar 2022*) `@NGK`
##### Changed
- Updated payment keys to break the payments

### v5.8.65 (*14 Feb 2022*) `@NGK`
##### Changed
- Blocked auto bookings across all the cities

### v5.8.64 (*06 Feb 2022*) `@NGK`
##### Added
- VDay 2022 campaign page
- VDay promotional banner

### v5.8.63 (*05 Feb 2022*) `@NGK`
##### Changed
- Blocked normal CLD bookings for VDay

### v5.8.62 (*03 Feb 2022*) `@NGK`
##### Added
- Decommissioning header
##### Changed
- Search has been hidden for both desktop & mobile

### v5.8.61 (*27 Jan 2022*) `@NGK`
##### Changed
- Disabled auto-booking for cities other than BLR, HYD

### v5.8.60 (*07 Jan 2022*) `@NGK`
##### Changed
- SMS templates as per DLT guidelines

### v5.8.59 (*30 Nov 2020*) `@UM`
##### Fixed
- Customer Images Loading issue
- Tab UI Fix
- global search results UI 
- wedding page

### v5.8.58 (*05 May 2020*) `@UM`
##### Changed
- Virtual Birthday Party content and icon locations
- 9640204000 to 8639213191

### v5.8.57 (*02 May 2020*) `@UM`
##### Added
- Virtual Birthday Party v2

### v5.8.56 (*30 Apr 2020*) `@UM`
##### Changed
- Virtual Birthday Party page content

### v5.8.55 (*28 Apr 2020*) `@UM`
##### Changed
- Virtual Birthday Party page content

### v5.8.54 (*25 Apr 2020*) `@NGK`
##### Added
- Virtual Party page

### v5.8.53 (*16 Apr 2020*) `@UM`
##### Added
- Virtual Birthday Party page

### v5.8.52 (*14 Mar 2020*) `@NGK`
##### Changed
- evib.es to bit.ly

### v5.8.51 (*13 Mar 2020*) `@NGK`
##### Changed
- Email communication optimisations 2

### v5.8.50 (*08 Mar 2020*) `@NGK`
##### Changed
- Email communication optimisations 1 part 2

### v5.8.49 (*07 Mar 2020*) `@NGK`
##### Changed
- Email communication optimisations 1 part 1

### v5.8.48 (*03 Mar 2020*) `@NGK`
##### Added
- Occasion and city in customer feedback emails

### v5.8.47 (*02 Mar 2020*) `@MUR`
##### Added
- Added surprises category cards if navigated through URL

### v5.8.46 (*23 Feb 2020*) `@NGK`
##### Removed
- Evibe Store visibility

### v5.8.45 (*22 Feb 2020*) `@NGK`
##### Added
- Baby shower inspiration visibility

### v5.8.44 (*15 Feb 2020*) `@NGK`
##### Removed
- VDay promotional banners

### v5.8.43 (*12 Feb 2020*) `@NGK`
##### Added
- Made add-ons unavailable for vday CLDs

### v5.8.42 (*12 Feb 2020*) `@NGK`
##### Added
- Slot availability urgency message

### v5.8.41 (*12 Feb 2020*) `@NGK`
##### Added
- Other city vday landing pages

### v5.8.40 (*08 Feb 2020*) `@NGK`
##### Added
- VDay packages can be booked through out the vday week

### v5.8.39 (*03 Feb 2020*) `@NGK`
##### Changed
- Package highlights visibility

### v5.8.38 (*01 Feb 2020*) `@NGK`
##### Changed
- VDay CLDs will not be shown in normal CLD list pages

### v5.8.37 (*01 Feb 2020*) `@JR`
##### Added
- Blur Effect in home page & similar packages
##### Fixed
- Changed vday source
- Why us alignment fix in campaign page

### v5.8.36 (*30 Jan 2020*) `@UM`
##### Added
- Inspirations page

### v5.8.35 (*29 Jan 2020*) `@NGK`
##### Fixed
- City home page top options order

### v5.8.34 (*27 Jan 2020*) `@NGK`
##### Fixed
- Alt text for profile images
##### Changed
- Fotoroma js file

### v5.8.33 (*27 Jan 2020*) `@JR`
##### Added
- Navigation modal
##### Fixed
- Campaign pages line height fix

### v5.8.32 (*25 Jan 2020*) `@NGK`
##### Changed
- City home page top options order
##### Removed
- Sub-occasion cards and invites section in mobile city home page

### v5.8.31 (*24 Jan 2020*) `@NGK`
##### Added
- VDay promotion

### v5.8.30 (*24 Jan 2020*) `@NGK`
##### Added
- Support for VDay CLD packages

### v5.8.29 (*22 Jan 2020*) `@JR`
##### Fixed
- Prefetch error fix in some pages

### v5.8.28 (*22 Jan 2020*) `@JR`
##### Modified
- Modified loading animation in profile page

### v5.8.27 (*22 Jan 2020*) `@NGK`
##### Added
- Intermediate store redirect page
##### Changed
- Hide giffy timer modal in mobile
##### Removed
- Party props campaign header

### v5.8.26 (*20 Jan 2020*) `@NGK`
##### Added
- Redirecting kids pay areas traffic to city home page

### v5.8.25 (*20 Jan 2020*) `@NGK`
##### Added
- Tracking classes for enquire changes

### v5.8.24 (*18 Jan 2020*) `@NGK`
##### Changed
- Enquiry thank you modal
##### Added
- Enquire now buttons and links in product profile pages

### v5.8.23 (*18 Jan 2020*) `@JR`
##### Modified
- Fotorama loading modal fixes

### v5.8.22 (*17 Jan 2020*) `@NGK`
##### Changed
- Mobile list page design changes

### v5.8.21 (*16 Jan 2020*) `@NGK`
##### Added
- UTM tagging for store promotion links

### v5.8.20 (*13 Jan 2020*) `@JR`
##### Modified
- Reviews logic optimized

### v5.8.19 (*11 Jan 2020*) `@JR`
##### Modified
- Handled null cases for php7 upgrade
##### Added
- Increased height & background blur for all the categories

### v5.8.18 (*09 Jan 2020*) `@NGK`
##### Added
- Store promotion in checkout page add-ons section

### v5.8.17 (*08 Jan 2020*) `@NGK`
##### Changed
- Store promotion names to "Party Props" from "Party Supplies"

### v5.8.16 (*03 Jan 2020*) `@NGK`
##### Changed
- Updated cancellation refund policy msg - "Total Booking Amount" to "Total Advance Amount"

### v5.8.15 (*02 Jan 2020*) `@NGK`
##### Changed
- Checkout pages to load them directly instead of extending them to pay base file

### v5.8.14 (*02 Jan 2020*) `@NGK`
##### Changed
- New year promo header to normal store promotion

### v5.8.13 (*27 Dec 2019*) `@NGK`
##### Changed
- Carousel visibility for Evibe Store

### v5.8.12 (*26 Dec 2019*) `@NGK`
##### Removed
- Christmas promotion
##### Added
- New Year party supplies promotion `@NGK`

### v5.8.11 (*24 Dec 2019*)
##### Changed
- Checkout source question is mandatory `@MUR`
##### Fixed
- Area filter results `@NGK`

### v5.8.10 (*23 Dec 2019*) `@NGK`
##### Changed
- Store visibility changes

### v5.8.9 (*23 Dec 2019*) `@JR`
##### Added
- Snowfall animation error
- Showing tiles design in all the home pages

### v5.8.8 (*21 Dec 2019*) `@MUR`
##### Added
- Snowfall effect for christmas header

### v5.8.7 (*20 Dec 2019*) `@JR`
##### Added
- Cache & optimization for campaign pages
##### Modified
- Feedback question modifications
- Removed default redirect to bangalore city

### v5.8.6 (*20 Dec 2019*) `@NGK`
##### Fixed
- User update override after booking service is modified

### v5.8.5 (*13 Dec 2019*) `@NGK`
##### Added
- Visibility for evibe store

### v5.8.4 (*12 Dec 2019*) `@MUR`
##### Fixed
- Fetching profile images for decors and cakes in campaign pages

### v5.8.3 (*10 Dec 2019*) `@NGK`
##### Added
- Functionality to raise complaint from TMO page

### v5.8.2 (*06 Dec 2019*) `@NGK`
##### Fixed
- Vendor accept page for old partner

### v5.8.1 (*02 Dec 2019*) `@MUR`
##### Added
- Added new cities jaipur, kolkata, bhubaneshwar

### v5.8.0 (*27 Nov 2019*) `@JR`
##### Added
- Wallet

### v5.7.45 (*23 Nov 2019*) `@NGK`
##### Changed
- Corporate enquiries will now go to BD team

### v5.7.44 (*23 Nov 2019*) `@NGK`
##### Changed
- Party halls UI

### v5.7.43 (*22 Nov 2019*) `@NGK`
##### Fixed
- Unavailability of city url for campaign products

### v5.7.42 (*18 Nov 2019*) `@MUR`
##### Modified
- Bday occasion home page revamp

### v5.7.41 (*18 Nov 2019*) `@MUR`
##### Added
- Fetching google reviews from DB

### v5.7.40 (*16 Nov 2019*) `@NGK`
##### Changed
- Updated ip promotional signup page after review
##### Added
- Sticky promotional header for mobile

### v5.7.39 (*16 Nov 2019*) `@NGK`
##### Added
- Instaparty enquiry signup page

### v5.7.38 (*16 Nov 2019*) `@JR`
##### Fixed
- TMO no bookings found bug fix
- Removing call FAB, testing with only form

### v5.7.37 (*13 Nov 2019*) `@NGK`
##### Added
- Book Now and Enquire buttons for pre-post decors
- Force login for pre-post pages

### v5.7.36 (*13 Nov 2019*) `@JR`
##### Modified
- Redirecting to surprises list rather than home from home page

### v5.7.35 (*12 Nov 2019*)
##### Modified `@JR`
- Merged all loading modals into single one
- removed material css from about us to test pagespeed
##### Added `@NGK`
- Privy code

### v5.7.34 (*11 Nov 2019*) `@NGK`
##### Added
- Venues [Venue Halls] in kids occasion
##### Changed
- Update old venue_halls with deleted_at = '2019-11-11 11:00:00'

### v5.7.33 (*25 Oct 2019*) `@MUR`
##### Added
- Landing page for other cities
- Search box for city modal

### v5.7.32 (*30 Oct 2019*) `@JR`
##### Modified
- Restriction for decor enquiry to reduce duplicate tickets
- Removed restriction of showing "get invite" option to only few customers

### v5.7.31 (*24 Oct 2019*) `@JR`
##### Added
- Enabling Enquiry form for premium invites

### v5.7.30 (*24 Oct 2019*) `@JR`
##### Added
- GIF making support

### v5.7.29 (*19 Oct 2019*) `@MUR`
##### Added
- Store Openings As Occasion

### v5.7.28 (*22 Oct 2019*) `@NGK`
##### Added
- Invites opt-in option in checkout
- Payment success page promotion

### v5.7.27 (*18 Oct 2019*) `@JR`
##### Added
- Back button support for iOS app
- applying watermark for premium invites

### v5.7.26 (*3 Oct 2019*) `@MUR`
##### Changed
- URLs for invites according to SEO

### v5.7.25 (*12 Oct 2019*) `@JR`
##### Modified
- Invites header Images change & UI update

### v5.7.24 (*11 Oct 2019*) `@MUR`
##### Fixed
- CSS issue in campaign page

### v5.7.23 (*11 Oct 2019*) `@NGK`
##### Added
- Option add-on mappings

### v5.7.22 (*07 Oct 2019*) `@MUR`
##### Removed
- Dussehra Campaign text in the header removed

### v5.7.21 (*07 Oct 2019*) `@JR`
##### Fixed
- Removed booking markup, since tmo link is already provided in the email to solve AWS max email size

### v5.7.20 (*05 Oct 2019*) `@NGK`
##### Added
- Add-ons in TMO page
- add-ons in booking communication emails

### v5.7.19 (*03 Oct 2019*) `@MUR`
##### Added
- Added wishcards & promotional header for desktop

### v5.7.18 (*29 Sep 2019*) `@NGK`
##### Changed
- Hid customer and partner contact information in emails
##### Added
- TMO and partner dash quick links in emails

### v5.7.17 (*28 Sep 2019*) `@NGK`
##### Added
- Add decor finish time to decor related AB by type ticket booking checkout fields

### v5.7.16 (*27 Sep 2019*) `@NGK`
##### Changed
- Party venue location will be taken from package rather than venue partner
- Package profile pages will fetch venue related data from package rather than venue partner

### v5.7.15 (*24 Sep 2019*) `@NGK`
##### Added
- Code to fetch generic occasion add-ons if tag based add-ons are not available

### v5.7.14 (*23 Sep 2019*) `@NGK`
##### Added
- Option availability for CCP

### v5.7.13 (*21 Sep 2019*) `@NGK`
##### Added
- E-cards (Launched Thank You Cards)

### v5.7.12 (*21 Sep 2019*) `@JR`
##### Added
- Customer app login after validation

### v5.7.11 (*16 Sep 2019*) `@JR`
##### Fixed
- Chennai city surprises home

### v5.7.10 (*14 Sep 2019*) `@NGK`
##### Fixed
- Pagination issue while using category tags filters in list pages

### v5.7.9 (*14 Sep 2019*) `@JR`
##### Modified
- Chennai city birthday home
##### Fixed
- Occasion home page hero image height
- Surprises fetching locations code optimize

### v5.7.7 (*14 Sep 2019*) `@NGK`
##### Added
- Support for quick link access to partner settlements

### v5.7.6 (*13 Sep 2019*) `@NGK`
##### Added
- Partner settlements in Partner Dashboard

### v5.7.5 (*11 Sep 2019*) `@NGK`
##### Added
- Bookings will be marked as star orders is applicable while making AB

### v5.7.4 (*11 Sep 2019*) `@JR`
##### Modified
- Home page UI
- Re-enabling page count & user browsing history

### v5.7.3 (*07 Sep 2019*) `@NGK`
##### Fixed
- Payment checkout fields issue
- Feedback fail report to admin billing issue

### v5.7.2 (*03 Sep 2019*) `@MUR`
##### Removed
- Ganesh Carousel removed

### v5.7.1 (*31 Aug 2019*) `@NGK`
##### Changed
- Booking specific checkout fields

### v5.7.0 (*30 Aug 2019*) `@JR`
##### Modified
- Home page UI upgrade

### v5.6.5 (*29 Aug 2019*) `@NGK`
##### Fixed
- Quick enquiry scroll issue in home page

### v5.6.4 (*28 Aug 2019*) `@JR`
##### Modified
- Invites steps UI upgrade & list page pinterest modal

### v5.6.3 (*26 Aug 2019*) `@NGK`
##### Changed
- Customer feedback email will be sent to business team

### v5.6.2 (*16 Aug 2019*) `@MUR`
##### Fixed
- Partner delivery images as option review images default image issue

### v5.6.1 (*13 Aug 2019*) `@MUR`
##### Changed
- Partner delivery images as option review images

### v5.6.0 (*05 Aug 2019*) `@JR`
##### Added
- Blog

### v5.5.1 (*28 Jul 2019*) `@MUR`
##### Added
- Campaign pages

### v5.5.0 (*26 Jul 2019*) `@JR`
##### Added
- Invites

### v5.4.90 (*22 Jul 2019*) `@MUR`
##### Changed
- Items Arrangement on homepage

### v5.4.89 (*11 Jul 2019*) `@JR`
##### Added
- Variations for options

### v5.4.88 (*09 Jul 2019*) `@JR`
##### Modified
- Bringing all window load functions into one js file

### v5.4.87 (*09 Jul 2019*) `@MUR`
##### Fixed
- H1 titles in profile, list pages & collection pages
- login modal UI

### v5.4.86 (*08 Jul 2019*) `@JR`
##### Added
- Cake Filters multiple theme selection

### v5.4.85 (*08 Jul 2019*) `@JR`
##### Added
- URL added for package schema

### v5.4.84 (*06 Jul 2019*) `@JR`
##### Added
- Added categories for candle light dinner for seo

### v5.4.83 (*05 Jul 2019*) `@NGK`
##### Changed
- Advance selection option for all non-venue packages

### v5.4.82 (*18 Jun 2019*) `@MUR`
##### Modified
- Revamped the Logic and UI of recommendations page

### v5.4.81 (*28 Jun 2019*) `@MUR`
- Updated Navbar and Footer with Naming Ceremony and Baby shower
- Created URLs for Naming ceremony and baby shower

### v5.4.80 (*02 Jul 2019*) `@MUR`
##### Fixed
- Converting all the title to H1 for better SEO
- 404 page post requirements functionality fix

### v5.4.79 (*02 Jul 2019*) `@JR`
##### Fixed
- Not showing home decor in all locations
- Broken URL's in footer

### v5.4.78 (*01 Jul 2019*) `@JR`
##### Fixed
- Tested with all the cases for international charges

### v5.4.77 (*28 Jun 2019*) `@JR`
##### Fixed
- Changing page titles & description after applying filters

### v5.4.76 (*26 Jun 2019*) `@JR`
##### Changed
- Cancellation charges are being fetched from API rather than direct calculations

### v5.4.75 (*26 jun 2019*) `@NGK`
##### Added
- Availability of surprise options based on booking availability per slot

### v5.4.74 (*26 Jun 2019*) `@JR`
##### Changed
- Cleaned some code in vuejs file related to add to cart
- animation fix for modile add to cart icon

### v5.4.73 (*25 Jun 2019*) `@GS`
##### Changed
- Special notes will be available for each booking.

### v5.4.72 (*21 Jun 2019*) `@MUR`
##### Added
- Added UBH in mobile navbar
##### Modified
- header and footer in 404 page
- Fetching price in Cart page

### v5.4.71 (*21 Jun 2019*) `@NGK`
##### Changed
- Changed AB and enquiry communication from business to ops

### v5.4.70 (*20 Jun 2019*) `@MUR`
##### Fixed
- Without Login, User can visit Cart page

### v5.4.69 (*15 Jun 2019*) `@MUR`
##### Fixed
- Changing text from "add to bag" to "add to cart"

### v5.4.68 (*15 Jun 2019*) `@MUR`
##### Fixed
- Overlay of User Browsing History
- Added common header in feedback & reviews pages

### v5.4.67 (*17 Jun 2019*) `@NGK`
##### Changed
- Input box position in price filter

### v5.4.66 (*17 Jun 2019*) `@NGK`
##### Added
- Input capability for price filter

### v5.4.65 (*16 Jun 2019*) `@NGK`
##### Fixed
- Removed usage of same id for capturing email for popup enquiry and login modal

### v5.4.64 (*14 Jun 2019*) `@NGK`
##### Fixed
- Hardcoded data to get empty array to fix user browsing history temporarily

### v5.4.63 (*13 Jun 2019*) `@MUR`
##### Fixed
- Recommendations page error fixed

### v5.4.62 (*13 Jun 2019*) `@MUR`
##### Fixed
- Redirecting to the same add to cart page after clicking on checkout button

### v5.4.61 (*12 Jun 2019*) `@MUR`
##### Modified
- Add to Cart page

### v5.4.60 (*11 Jun 2019*) `@NGK`
##### Changed
- Other color variation selection in PIAB profile page

### v5.4.59 (*6 May 2019*) `@MUR`
##### Changed
- Party Bag to Cart name change

### v5.4.58 (*4 Jun 2019*) `@NGK`
##### Added
- Default coupon code for PIAB offer landing customers

### v5.4.57 (*4 Jun 2019*) `@NGK`
##### Added
- Customer reviews in PIAB home page
##### Fixed
- PIAB offer modal css

### v5.4.56 (*4 Jun 2019*) `@NGK`
##### Added
- Offer landing modal for PIAB

### v5.4.55 (*3 Jun 2019*) `@NGK`
##### Added
- Customer reviews for PIAB

### v5.4.54 (*31 May 2019*) `@NGK`
##### Added
- Expected delivery date in product booking

### v5.4.53 (*30 May 2019*) `@JR`
##### Added
- USP in PIAB pages

### v5.4.52 (*30 May 2019*) `@MUR`
##### Changed
- User Browsing History dedicated page stylings

### v5.4.51 (*30 May 2019*) `@MUR`
##### Fixed
- Not showing user browsing history for few page
- Removed unused css from browsing history file
- Fixed urls bug in UBH

### v5.4.50 (*29 May 2019*) `@NGK`
##### Changed
- Added postal pin codes DB tabl einstead of API

### v5.4.49 (*29 May 2019*) `@NGK`
##### Added
- Added postal pin codes DB table instead of API

### v5.4.48 (*28 May 2019*) `@MUR`
##### Added
- Header Navigation

### v5.4.47 (*27 May 2019*) `@JR`
##### Added
- Offer model for party in a box

### v5.4.46 (*25 May 2019*) `@NGK`
##### Added
- Plain loading divs for PIAB slick carousels

### v5.4.45 (*24 May 2019*) `@NGK`
##### Added
- Style selection in PIAB profile page

### v5.4.44 (*23 May 2019*) `@MUR`
##### Changed
- User Browsing History Improvements

### v5.4.43 (*24 may 2019*) `@NGK`
##### Fixed
- PIAB list UI

### v5.4.42 (*24 may 2019*) `@NGK`
##### Fixed
- Commented faulty postal pin code API

### v5.4.41 (*23 May 2019*) `@NGK`
##### Changed
- PIAB types list page
##### Added
- Other type styles in PIAB style profile page

### v5.4.40 (*23 May 2019*)
##### Added `@JR`
- Changing payment gateway to razorpay after first payment failure automatically
##### Fixed `@MUR`
- Footer links

### v5.4.39 (*23 May 2019*) `@NGK`
##### Added
- PIAB kits list page

### v5.4.38 (*23 May 2019*) `@AR`
##### Added
- GTM e-commerce tracking enabled for PIAB

### v5.4.37 (*15 May 2019*) `@MUR`
##### Added
- User Browsing History carousel above footer
- User Browsing History separate dedicated page
##### Modified
- Login and sign up pages have been updated with material design

### v5.4.36 (*22 May 2019*) `@AR`
##### Added
- Email in GTM dataLayer
##### Removed
- FB tracker code, files
- Google analytics event trackers
- Some old commented code
- Hotjar, Tawk.to tracker files

### v5.4.35 (*22 May 2019*) `@NGK`
##### Changed
- PIAB delivery date will be shown based on the entered pin code city (1 day for HYD, 2 days for metro cities and 3 days for rest)

### v5.4.34 (*21 May 2019*) `@JR`
##### Added
- Added triggers for PIAB

### v5.4.33 (*21 May 2019*) `@NGK`
##### Changed
- PIAB delivery date limit to 3 days from 5 days

### v5.4.32 (*18 May 2019*) `@NGK`
##### Added
- Extra message in party info for decor bookings

### v5.4.31 (*17 May 2019*) `@NGK`
##### Added
- PIAB additional info in team emails
##### Changed
- Desktop list page price position and worth CSS

### v5.4.30 (*16 May 2019*) `@JR`
##### Added
- Added wishlist page for customer

### v5.4.29 (*16 May 2019*) `@JR`
##### Added
- Added wishlist
##### Removed
- Party kits promotional header

### v5.4.28 (*16 may 2019*) `@NGK`
##### Added
- Auto fill of city and state on entering pin code

### v5.4.27 (*14 May 2019*) `@NGK`
##### Added
- Other product categories in PIAB category profile pages

### v5.4.26 (*13 May 2019*) `@JR`
##### Added
- Added fixed header in mobile
##### Modified
- Removed serivce worker file
##### Fixed
- TMO order page showing same details for all the bookings

### v5.4.25 (*13 May 2019*) `@NGK`
##### Added
- Tags for redis cache

### v5.4.24 (*11 May 2019*) `@NGK`
##### Added
- Background for scroll arrows
##### Changed
- Trusted customers count
- PIAB desktop gallery css

### v5.4.23 (*11 May 2019*) `@NGK`
##### Added
- Default explainer image to all options

### v5.4.22 (*09 May 2019*) `@NGK`
##### Changed
- Brought add-ons to below inclusions
- Removed evibe.in link in PIAB header
- 'Tell Us' to 'Ask Us'

### v5.4.21 (*07 May 2019*) `@JR`
##### Added
- Able to select multiple category for decorations

### v5.4.20 (*06 May 2019*) `@NGK`
##### Fixed
- Issue with disabled party time selection in checkout page.

### v5.4.19 (*04 May 2019*) `@MUR`
##### Added
- Pin code serviceability check

### v5.4.18 (*03 May 2019*) `@MUR`
##### Changed
- Updated footer design

### v5.4.17 (*03 May 2019*) `@NGK`
##### Changed
- Placed social share at the end of inclusions in mobile
- Removed giffy for mobile
- Placed enquire now at the bottom of all the description for mobile

### v5.4.16 (*02 May 2019*) `@NGK`
##### Added
- Additional field in PIAB checkout

### v5.4.15 (*02 May 2019*) `@NGK`
##### Changed
- Hiding booking form in mobile profile by default and then sliding on click

### v5.4.14 (*02 May 2019*) `@NGK`
##### Added
- Storing PIAB feedback in DB
##### Changed
- Using slick plugin to show PIAB product gallery in desktop

### v5.4.13 (*01 May 2019*) `@JR`
##### Added
- Added trackers for checkout page to track payment issues
- Retry payment button

### v5.4.12 (*01 May 2019*) `@JR`
##### Modified
- Disabling youth occasion

### v5.4.11 (*29 Apr 2019*) `@JR`
##### Added
- Coupon code mapping integration & coupon redeem integration

### v5.4.10 (*29 Apr 2019*) `@MUR`
##### Added
- All the cities and their links in footer with updated UI
##### Fixed
- Duplicate footer issue for trends

### v5.4.9 (*29 Apr 2019*) `@NGK`
##### Added
- Priority order for PIAB types

### v5.4.8 (*28 Apr 2019*) `@NGK`
##### Added
- Separate path for mobile profile pics for PIAB

### v5.4.7 (*27 Apr 2019*) `@NGK`
##### Added
- Product categories
##### Changed
- Product types

### v5.4.6 (*26 Apr 2019*) `@NGK`
##### Added
- Coupon support for PIAB

### v5.4.5 (*25 Apr 2019*) `@NGK`
##### Changed
- PIAB category structure

### v5.4.4 (*25 Apr 2019*) `@MUR`
##### FIXED
- HTML rendering and Footer issue

### v5.4.3 (*24 Apr 2019*) `@NGK`
##### Changed
- Reverted back piab des gallery plugin
##### Fixed
- G-reviews being shown multiple times in footer
- Unwanted review question being shown in product partner reviews

### v5.4.2 (*24 Apr 2019*) `@NGK`
##### Added
- Jssor plugin for desktop images
##### Changed
- "Party In A Box" to "Party Kits"

### v5.4.1 (*24 Apr 2019*) `@JR`
##### Fixed
- Added custom log file to solve log permission issues

### v5.4.0 (*23 Apr 2019*) `@JR`
##### Added
- Left bar integration

### v5.3.1 (*23 Apr 2019*) `@NGK`
##### Fixed
- TMO login and user fix for product users
- SMS, email and PS fixes
- Minor fixes.

### v5.3.0 (*22 Apr 2019*) `@NGK`
##### Added
- Party In A Box

### v5.2.24 (*15 Apr 2019*) `@JR`
##### Added
- Added signup page for group tables

### v5.2.23 (*13 Apr 2019*) `@NGK`
##### Fixed
- Foreach argument issue in partybag shortlist modal

### v5.2.22 (*10 Apr 2019*) `@NGK`
##### Fixed
- Booking info URL fix

### v5.2.21 (*09 Apr 2019*) `@NGK`
##### Fixed
- JS error in mobile profile pages, which in turn affected party bag shortlisting

### v5.2.20 (*08 Apr 2019*) `@NGK`
##### Fixed
- Added notification in case price gets changed

### v5.2.19 (*05 Apr 2019*) `@JR`
##### Added
- CLD partner signup page

### v5.2.18 (*3 Apr 2019*) `@NGK`
##### Added
- Customer order proofs

### v5.2.17 (*2 Apr 2019*) `@NGK`
##### Fixed
- Package profile price section

### v5.2.16 (*30 Mar 2019*) `@NGK`
##### Fixed
- Villa list pages

### v5.2.15 (*29 Mar 2019*) `@NGK`
##### Added
- Dynamic product params for villa packages

### v5.2.14 (*22 Mar 2019*) `@NGK`
##### Changed
- SMS sender id to EVIBES from EEVIBE

### v5.2.13 (*22 Mar 2019*) `@JR`
##### Modified
- converting goo.gl to evib.es

### v5.2.12 (*19 Mar 2019*) `@JR`
##### Added
- Deleting HTML cache while deactivating/deleting the file

### v5.2.11 (*19 Mar 2019*) `@NGK`
##### Added
- Add-ons for home surprises

### v5.2.10 (*18 Mar 2019*) `@JR`
##### Fixed
- Removed unused CSS for delivery images & old search page
- Added laravel mix & purgecss
##### Added
- Loading profile images & add-ons through window.load

### v5.2.9 (*18 Mar 2019*) `@NGK`
##### Fixed
- Product links that are being stored in booking info
- 'Unable to find shortlist for the user' issue

### v5.2.8 (*18 Mar 2019*) `@NGK`
##### Added
- Hashbang will stay even after city selection
- Adding 'fb ad' as source for hashbang 'fbEnquiry'

### v5.2.7 (*13 Mar 2019*) `@JR`
##### Modified
- Optimizing about us page for performance test
##### Modified v5.2.7.1 (*14 Mar 2019*)
- Removed material css, added lazyload
##### Modified v5.2.7.2 (*14 Mar 2019*)
- Removed unnecessary JS
##### Modified v5.2.7.3 (*14 Mar 2019*)
- Loading youtube videos after page load

### v5.2.6 (*13 Mar 2019*) `@NGK`
##### Added
- Dynamic pricing for villas (disabled)
##### Changed
- Package checkout modal to better display guests count changes

### v5.2.5 (*12 Mar 2019*) `@JR`
##### Added
- Loading animation for fotorama

### v5.2.4 (*12 Mar 2019*) `@NGK`
##### Changed
- Disabled dates css in calender
##### Fixed
- Coupon code amount not being detected while making payment

### v.5.2.3 (*11 Mar 2019*) `@JR`
##### Added
- Added HTML cache

### v.5.2.2 (*9 Mar 2019*) `@NGK`
##### Fixed
- Added v-clock to all vue js rendered links to avoid broken urls

### v5.2.1 (*9 Mar 2019*) `@NGK`
##### Added
- Internal package availability calendar integration for villas

### v5.2.0 (*8 Mar 2019*) `@JR`
##### Added
- PWA support for the website

### v5.1.5 (*7 Mar 2019*) `@NGK`
##### Fixed
- Wrong calling code in AB tickets
- Guest count in bachelor venues
- Transport charges in cakes
##### Changed
- Brought Villas category directly on header
##### Added
- Book Now for villas

### v5.1.4 (*7 Mar 2019*) `@NGK`
##### Fixed
- Broken party bag urls due to google url crawling before the page is rendered

### v5.1.3 (*6 Mar 2019*) `@JR`
##### Modified
- Removed dark background for search
- Removed CLD old links

### v5.1.2 (*2 Mar 2019*) `@NGK`
##### Fixed
- Added array variable offset validation for 'price' in party-bag card data

### v5.1.1 (*1 Mar 2019*) `@NGK`
##### Added
- City specific default partners

### v5.1.0 (*1 Mar 2019*) `@NGK`
##### Added
- Re-opening bachelor party villas

### v5.0.10 (*26 Feb 2019*) `@JR`
##### Modified
- solved warnings for schema generation

### v5.0.9 (*26 Feb 2019*) `@JR`
##### Modified
- Removing all the payment gateways & making payumoney as direct checkout

### v5.0.8 (*25 Feb 2019*) `@JR`
##### Added
- Modifying cld profile url's in global search

### v5.0.7 (*22 Feb 2019*) `@JR`
##### Added
- CLD packages modifications
- Surprise home page top options modifications

### v5.0.6 (*21 Feb 2019*) `@NGK`
##### Added
- Calling code in Auto Bookings
- Cross city booking restriction (with local data)

### v5.0.5 (*20 Feb 2019*) `@JR`
##### Added
- Getting CLD out as a separate occasion

### v5.0.4 (*20 Feb 2019*) `@NGK`
##### Fixed
- Checkout page add-ons continue bottom CTA

### v5.0.3 (*20 Feb 2019*) `@JR`
##### Fixed
- HTML rendering errors in email
- Removing freecharge gateway support

### v5.0.2 (*20 Feb 2019*) `@NGK`
##### Fixed
- Calling code css

### v5.0.1 (*18 Feb 2019*) `@JR`
##### Fixed
- HTML rendering errors

### v5.0.0 (*18 Feb 2019*) `@JR`
##### Upgraded
- Upgraded laravel version from 5.3 to 5.4

### v4.2.14 (*18 Feb 2019*) `@NGK`
##### Added
- Country calling code in enquiry forms

### v4.2.13 (*15 Feb 2019*) `@NGK`
##### Fixed
- Party bag checkout input js and css issues

### v4.2.12 (*14 Feb 2019*) `@NGK`
##### Fixed
- Updated partner will receive new receipt instead of updated

### v4.2.11 (*13 Feb 2019*) `@NGK`
##### Fixed
- Forward navigation in checkout page
- Image upload variable issue

### v4.2.10 (*12 Feb 2019*) `@NGK`
##### Added
- Error reporting system for party bag checkout method
##### Fixed
- Undefined index $ issue in checkout page

### v4.2.9 (*12 Feb 2019*) `@NGK`
##### Changed
- Multi book party bag interface
##### Fixed
- Payment link expiry - invalid status issue

### v4.2.8 (*9 Feb 2019*) `@NGK`
##### Changed
- Updated 2018 to 2019 year in footer

### v4.2.7 (*9 Feb 2019*) `@NGK`
##### Fixed
- Added Book Now modal for decors in house warming occasion

### v4.2.6 (*8 Feb 2019*) `@NGK`
##### Fixed
- Status change issue upon payment link expiry
##### Added
- Valid status variable to avoid unnecessary ajax requests for price and order details update

### v4.2.5 (*7 Feb 2019*) `@NGK`
##### Fixed
- Collection title in city home page

### v4.2.4 (*7 Feb 2019*) `@NGK`
##### Added
- Error email reports rather than logs
##### Fixed
- Coupon code inclusion in advance amount calculation in ticket data validation
##### Removed
- Unnecessary debug logs

### v4.2.3 (*6 Feb 2019*) `@NGK`
##### Fixed
- Undefined index jquery issue in occasion home pages

### v4.2.2 (*5 Feb 2019*) `@NGK`
##### Fixed
- Checkout price details update
##### Changed
- Coupon code card will appear only payment details screen

### v4.2.1 (*4 Feb 2019*) `@NGK`
##### Fixed
- Temp fix for shortlistData

### v4.2.0 (*4 Feb 2019*) `@NGK`
##### Added
- Party Bag Multi Book

### v4.1.45 (*4 Feb 2019*) `@NGK`
##### Fixed
- Occasion cards CSS in city home pages

### v4.1.44 (*4 Feb 2019*) `@JR`
##### Changed
- Showing default gateway as payumoney in mobile

### v4.1.43 (*4 Feb 2019*) `@AB`
##### Changed
- SEO

### v4.1.42 (*4 Feb 2019*) `@NGK`
##### Added
- Added links to promote CLDs a separate occasion

### v4.1.41 (*2 Feb 2019*) `@NGK`
##### Added
- VDay collection promotion urls for Delhi and Pune cities

### v4.1.40 (*31 Jan 2019*) `@JR`
##### Added
- Auto marking auto-init tickets as duplicate, if they initiate new ticket

### v4.1.39 (*31 Jan 2019*) `@NGK`
##### Changed
- VDay normal surprise package booking restriction is limited to only CLDs
- Improved header text

### v4.1.38 (*31 Jan 2019*) `@NGK`
##### Changed
- Modified VDay header

### v4.1.37 (*25 Jan 2019*) `@NGK`
##### Added
- VDay header and package booking restriction

### v4.1.36 (*21 Jan 2019*) `@NGK`
##### Changed
- Using the term 'Track My Order' and hid partner details

### v4.1.35 (*19 Jan 2019*) `@NGK`
##### Added
- TMO CTA in checkout page if advance is paid
- Checkout page v1.1 changes
##### Fixed
- Fixed issue with old checkout links (being redirected to new page)

### v4.1.34 (*18 Jan 2019*) `@NGK`
##### Changed
- CLD FAQs related to and drinks and lunch
##### Added
- Lunch slots for CLDs

### v4.1.33 (*18 Jan 2019*) `@NGK`
##### Removed
- Surprises primary filters in both home and list pages
##### Changed
- CLD FAQs related to cake and drinks

### v4.1.32 (*17 Jan 2019*) `@JR`
##### Fixed
- Handled no handler Id for some tickets

### v4.1.31 (*12 Jan 2019*) `@NGK`
##### Fixed
- Removed duplicate forms in list and decor mobile pages
- Included missing ticketId in TicketBookingPaymentController

### v4.1.30 (*10 Jan 2019*) `@NGK`
##### Changed
- Single page checkout application to navigational checkout page

### v4.1.29 (*10 Jan 2019*) `@JR`
##### Modified
- Sending separate emails for enquiry team & customer for tracking

### v4.1.28 (*08 Jan 2019*) `@JR`
##### Modified
- Updating DB after payment with end time

### v4.1.27 (*08 Jan 2019*) `@JR`
##### Modified
- Converting bitly to goo.gl until 31st mar 2019

### v4.1.26 (*08 Jan 2019*) `@JR`
##### Added
- Storing payment initiated data to DB

### v4.1.25 (*07 Jan 2019*) `@JR`
##### Added
- Party date for surprises
- COPY change from "Most loved" to "Most Trusted"

### v4.1.24 (*05 Jan 2019*) `@JR`
##### Added
- Dynamic offer table

### v4.1.23 (*03 Jan 2019*) `@JR`
##### Added
- Feedback page upgrades

### v4.1.22 (*02 Jan 2019*) `@JR`
##### Added
- Storing request details in DB
##### Fixed
- Manual payment acceptance TMO error fix

### v4.1.21 (*31 Dec 2018*) `@JR`
##### Added
- Showing Cancellation charges in request cancellation model
- Showing Reference Images

### v4.1.20 (*28 Dec 2018*) `@AR`
##### Fixed
- Issue with using Gtag & GTM
- Validation message for gender in checkout page
##### Changed
- GA e-commerce implementation to GTM

### v4.1.19 (*27 Dec 2018*) `@AR`
##### Added
- Implemented Google Tag Manager
##### Removed
- Google Analytics
- Tawk.to
- Leads Conversion Tracking

### v4.1.18 (*26 Dec 2018*) `@NGK`
##### Fixed
- Placed party time check to hide time in checkout page in case of decors

### v4.1.17 (*26 Dec 2018*) `@JR`
##### Added
- Removed booking amount in feedback page
- Hide workflow strip
- Don't show rent duration for house warming

### v4.1.16 (*21 Dec 2018*) `@JR`
##### Added
- Added additional security for TMO request

### v4.1.15 (*19 Dec 2018*) `@JR`
##### Added
- Added title & source in checkout page
- Adding user Id to google events to track the complete user journey

### v4.1.14 (*18 Dec 2018*) `@NGK`
##### Fixed
- Default empty string for campaign in case if anything is removed

### v4.1.13 (*18 Dec 2018*) `@NGK`
##### Added
- Christmas campaign header
##### Changed
- Booking restriction for packages on campaign dates

### v4.1.12 (*18 Dec 2018*) `@JR`
##### Added
- Removing google bot CSS errors

### v4.1.11 (*17 Dec 2018*) `@JR`
##### Added
- Auto-pay support in TMO
- SMS templates & Email subject lines changes
- Text change 24hrs to 4hrs for auto-pay acceptance time

### v4.1.10 (*17 Dec 2018*) `@NGK`
##### Fixed
- Checkbox checkout support for add-on fields.

### v4.1.9 (*16 Dec 2018*) `@JR`
##### Added
- Showing checkout fields & special notes in order details

### v4.1.8 (*14 Dec 2018*) `@NGK`
##### Added
- Support for checkbox checkout fields

### v4.1.7 (*13 Dec 2018*) `@JR`
##### Added
- Getting profile images for auto-booking tickets in TMO page

### v4.1.6 (*13 Dec 2018*) `@JR`
##### Modified
- Booking success SMS templates upgrade
##### Fixed
- Partner page error fix

### v4.1.4 (*12 Dec 2018*) `@JR`
##### Modified
- Sending different SMS for kids & Surprises, changes SMS format to promotional

### v4.1.3 (*10 Dec 2018*) `@JR`
##### Modified
- UI changes for TMO v1

### v4.1.2 (*10 Dec 2018*) `@JR`
##### Modified
- UI changes for partner coordinator details

### v4.1.1 (*8 Dec 2018*) `@NGK`
##### Added
- Communication for add-ons partner to deliver to the venue manager for applicable add-ons

### v4.1.0 (*8 Dec 2018*) `@JR`
##### Added
- Track My Order Page

### v4.0.39 (*7 Dec 2018*) `@NGK`
##### Changed
- Checkout page time validation allow same day bookings

### v4.0.38 (*7 Dec 2018*) `@NGK`
##### Fixed
- Party bag image css

### v4.0.37 (*6 Dec 2018*) `@JR`
##### Modified
- Showing party date input field only for surprise landing pages modal, not showing for kids birthday

### v4.0.36 (*6 Dec 2018*) `@JR`
##### Modified
- Modified code to get enquiry Id if it is null
- Booking email subject with previous enquiry Id, if already booking exists

### v4.0.35 (*5 Dec 2018*) `@NGK`
##### Added
- Google Event to track quick view

### v4.0.34 (*5 Dec 2018*) `@NGK`
##### Fixed
- Book before hrs carbon validation

### v4.0.33 (*5 Dec 2018*) `@JR`
##### Added
- Modified subject lines for Normal booking emails

### v4.0.32 (*5 Dec 2018*) `@NGK`
##### Added
- Location in surprises list for venue packages
- 'table_setup' for CLDs
- 'book_before_hrs' validation for surprises packages

### v4.0.31 (*4 Dec 2018*) `@AR`
##### Changed
- Updated website phone number to exotel number

### v4.0.30 (*4 Dec 2018*) `@NGK`
##### Added
- Pre checkout modal option to pay 50%

### v4.0.29 (*4 Dec 2018*) `@NGK`
##### Fixed
- Corrected party date time in updated customer receipt

### v4.0.28 (*4 Dec 2018*) `@NGK`
##### Fixed
- Corrected render file name

### v4.0.27 (*4 Dec 2018*) `@NGK`
##### Added
- Event name in customer and partner booking communication

### v4.0.26 (*3 Dec 2018*) `@JR`
##### Fixed
- Error mails for CSS home page fix

### v4.0.25 (*3 Dec 2018*) `@NGK`
##### Changed
- Cancellation policies

### v4.0.24 (*2 Dec 2018*) `@JR`
##### Fixed
- Error mails for CSS page load error

### v4.0.23 (*1 Dec 2018*) `@JR`
##### Fixed
- Payment receipts not going for paytm payment tickets
- Enabling Ajax request with empty error message

### v4.0.22 (*1 Dec 2018*) `@NGK`
##### Changed
- Desktop enquire now btn to link
##### Fixed
- Show more FAQs height

### v4.0.21 (*30 Nov 2018*) `@NGK`
##### Changed
- FAQs content

### v4.0.20 (*29 Nov 2018*) `@NGK`
##### Fixed
- Checkout page not loading CSS issue fix. Ref: https://stackoverflow.com/questions/9604177/error-330-neterr-content-decoding-failed-with-minify-on-first-load

### v4.0.19 (*29 Nov 2018*) `@NGK`
##### Fixed
- Enquire Now text

### v4.0.18 (*29 Nov 2018*) `@NGK`
##### Removed
- Sticky nav in mobile
- TTR in Non-CLD
##### Changed
- Enquire Now in mobile profile pages

### v4.0.17 (*28 Nov 2018*) `@NGK`
##### Fixed
- Cancellation slots and checkInSlots bug fix

### v4.0.16 (*28 Nov 2018*) `@NGK`
##### Changed
- Surprises content and UI

### v4.0.15 (*26 Nov 2018*) `@JR`
##### Fixed
- Paytm payment issue fix
##### Modified
- Updating event date in signup table
- Updated SMS message for G-AD signup users

### v4.0.14 (*24 Nov 2018*) `@NGK`
##### Removed
- Temporarily halting the invitation services until DIY platform is ready

### v4.0.13 (*23 Nov 2018*) `@JR`
##### Added
- A/B testing for the google ad tickets party date

### v4.0.12 (*19 Nov 2018*) `@JR`
##### Fixed
- Fixed issue with coupon code & special notes

### v4.0.11 (*19 Nov 2018*) `@JR`
##### Added
- Added special notes for each booking

### v4.0.10 (*21 Nov 2018*)
##### Fixed
- Bug with clearing recommendation selected date `@AR`
##### Changed
- Allowing CLD customer book even on the same day until 6 PM `@JR`

### v4.0.9 (*19 Nov 2018*) `@AR`
##### Fixed
- Fixed issue with website feedback
- Removed usage of incorrect `Exception`

### v4.0.8 (*16 Nov 2018*) `@NGK`
##### Fixed
- FAQs nav btn in mobile

### v4.0.7 (*15 Nov 2018*) `@NGK`
##### Fixed
- Multiple booking reviews were being asked for same provider. Now only 1 review per partner

### v4.0.6 (*13 Nov 2018*) `@JR`
##### Fixed
- Signup duplicate user fix
- Failed to proceed ajax error fix

### v4.0.5 (*13 Nov 2018*) `@NGK`
##### Fixed
- Decors mobile spelling correction
##### Changed
- Mobile desc from tabs to scroll

### v4.0.4 (*10 Nov 2018*) `@JR`
##### Fixed
- Ajax request failure error fix
- Unable to shortlist in list/profile page fix

### v4.0.3 (*10 Nov 2018*) `@NGK`
##### Fixed
- Temporary time switch to avoid bitly error

### v4.0.2 (*10 Nov 2018*) `@NGK`
##### Changed
- Made UX changes to better show information to the customer (mobile and desktop)
- Moved How It Works? to right side
- CSS changes

### v4.0.1 (*10 Nov 2018*) `@JR`
##### Modified
- Hiding phone, collections, global search type hints

### v4.0.0 (*06 Nov 2018*) `@JR`
##### Changed
- Hiding Weddings, youth party & corporates from website

### v3.8.67 (*06 Nov 2018*) `@NGK`
##### Changed
- Job position for ticket booking payment to update booking lookup table

### v3.8.66 (*03 Nov 2018*) `@NGK`
##### Changed
- Invites related pages, url, checkout page to test paid invites

### v3.8.65 (*03 Nov 2018*) `@JR`
##### Changed
- Added interested category in popup enquiry

### v3.8.64 (*03 Nov 2018*) `@JR`
##### Changed
- Removed email for surprises landing modal after A/B testing

### v3.8.63 (*01 Nov 2018*) `@Manav`
##### Changed
- Text changes for 'Enquiry Forms'
- Hiding the whatsapp number
- Changing the header for delivery images

### v3.8.62 (*04 Sept 2018*) `@Manav`
##### Added
- Login page and orders

### v3.8.61 (*30 Oct 2018*) `@JR`
##### Fixed
- Irrelevant words removal in similar product search

### v3.8.60 (*30 Oct 2018*) `@NGK`
##### Added
- Added google event to track distance if available

### v3.8.59 (*29 Oct 2018*) `@NGK`
##### Fixed
- Max-height css in add-ons info

### v3.8.58 (*29 Oct 2018*) `@NGK`
##### Added
- Add-ons selected while making an enquiry will be shown notified to team
##### Changed
- Changes add-on card css to start text alongside image

### v3.8.57 (*29 Oct 2018*) `@JR`
##### Fixed
- CSS upgrades for product delivery images

### v3.8.56 (*27 Oct 2018*) `@JR`
##### Fixed
- Coupon code applying for multiple bookings

### v3.8.55 (*27 Oct 2018*) `@NGK`
##### Changed
- Distance calculator CSS based on the feedback

### v3.8.54 (*26 Oct 2018*) `@NGK`
##### Added
- Distance calculator for surprises venues

### v3.8.53 (*26 Oct 2018*) `@JR`
##### Added
- Partner delivery images showing on main

### v3.8.52 (*24 Oct 2018) `@NGK`
##### Added
- Location filter in surprises
##### Fixed
- Keys to show top options

### v3.8.51 (*23 Oct 2018*) `@JR`
##### Added
- Education screen for recommendations screen shortlist options

### v3.8.50 (*23 Oct 2018*) `@NGK`
##### Added
- Top surprise categories in home page
##### Changed
- Sort and filter btn location in mobile
- Provider reviews to Customer reviews
##### Removed
- Provider Card
- Rend duration in surprise packages

### v3.8.49 (*23 Oct 2018*) `@AR`
##### Fixed
- No spaces in about us page. Updated copy
- No space in footer for Evibe.in Blog

### v3.8.48 (*20 Oct 2018*) `@NGK`
##### Changed
- Added 'skip to payment' at top in mobile and changed text of 'skip---' to 'skip to payment without add-ons'
##### Fixed
- Footer links in checkout page

### v3.8.47 (*19 Oct 2018*) `@JR`
##### Added
- Landing modal for kids events

### v3.8.46 (*17 Oct 2018*) `@JR`
##### Fixed
- Removing special characters for similar product search

### v3.8.45 (*16 Oct 2018*) `@JR`
##### Added
- Doing A/B testing for surprise landing pages

### v3.8.44 (*16 Oct 2018*) `@NGK`
##### Fixed
- Fixed issue with payment success page with no 'ticketId'

### v3.8.43 (*13 Oct 2018*) `@NGK`
##### Added
- GAEvents for FAQ action buttons

### v3.8.42 (*12 Oct 2018*) `@manav`
##### Added
- Added `In 3 Days` for booking likeliness
- Algorithm to calculate closure date and saving in db.

### v3.8.41 (*12 Oct 2018*) `@NGK`
##### Fixed
- Re-enabled cache for cakes profile data
- Included missing FAQ in mobile page

### v3.8.40 (*11 Oct 2018*) `@NGK`
##### Added
- FAQs for surprises

### v3.8.39 (*9 Oct 2018 ~ 10 Oct 2018*)
##### Modified `@Manav`
- Enabled emails to crm team when someone lands on payment page
##### Modified `@JR`
- Recommendation algorithm for getting similar products

### v3.8.38 (*10 Oct 2018*) `@NGK`
##### Changed
- Disabled cake cache data as it is conflicting with modified keys

### v3.8.37 (*10 Oct 2018*) `@NGK`
##### Changed
- Pushing reverted branch with 2 small code indentation changes

### v3.8.36 (*9 Oct 2018*) `@NGK`
##### Added
- AB add-ons for kids occasions

### v3.8.35 (*9 Oct 2018*) `@NGK`
##### Added
- AB add-ons for kids decors

### v3.8.34 (*9 Oct 2018*) `@JR`
##### Added
- Added theatre landing page

### v3.8.33 (*8 Oct 2018*) `@NGK`
##### Fixed
- Issue with add-ons for planner mappings

### v3.8.32 (*6 Oct 2018*) `@AR`
##### Modified
- Changed default tracking to google ads for surprise landing page

### v3.8.31 (*5 Oct 2018*) `@NGK`
##### Added
- Add-ons for kids NB

### v3.8.30 (*04 Oct 2018*) `@JR`
##### Modified
- Modified mail subject & source for FB ads

### v3.8.29 (*04 Oct 2018*) `@ANJI`
##### Modified
- Added fb tracker to track the conversion

### v3.8.28 (*01 Oct 2018*) `@JR`
##### Modified
- Workflow tracking options clicks
- UI/UX upgrades

### v3.8.27 (*1 Oct 2018*) `@NGK`
##### Modified
- Updated invitation related communication with invites email group

### v3.8.26 (*29 Sep 2018*) `@JR`
##### Modified
- Updated tracker Ids & event Ids for better tracking
- Auto-popup call out integration
- Not showing refer&earn review question to customers

### v3.8.25 (*28 Sep 2018*) `@NGK`
##### Modified
- Add-ons Normal Booking copy changes

### v3.8.24 (*27 Sep 2018*) `@JR`
##### Modified
- Collecting phone number rather than email for landing pages
##### Added
- Added landing page popoup for profile pages

### v3.8.23 (*27 Sep 2018*) `@NGK`
##### Fixed
- 'ticketId' issue for old checkout pages and partner reject pages

### v3.8.23 (*27 Sep 2018*) `@NGK`
##### Fixed
- 'ticketId' issue for old checkout pages and partner reject pages

### v3.8.22 (*26 Sep 2018*) `@NGK`
##### Added
- Added add-ons for Normal Bookings

### v3.8.21 (*25 Sep 2018*)
##### Fixed
- Ajax failure fix for similar product
- Workflow loading modal `@JR`

### v3.8.20 (*15 Sep 2018 ~ 22 sep 2018*)
##### Fixed
- Sending mails for non-live products `@MC`
- Unhide workflow header `@JR`

### v3.8.19 (*24 Sep 2018*) `@NGK`
##### Fixed
- Added 'ticketId' in payment link expiry page

### v3.8.18 (*22 Sep 2018*) `@AR`
##### Added
- Implemented e-commerce tracking for Google Analytics

### v3.8.17 (*21 Sep 2018 ~ 22 Sep 2018*) `@NGK`
##### Added
- SMS to notify customer regarding form

### v3.8.16 (*22 Sep 2018*) `@JR`
##### Added
- Added fab button for auto-popup

### v3.8.15 (*19 Sep 2018*) `@NGK`
##### Changed
- Time difference validation for invites from 4 days to 3 days

### V3.8.14 (*19 Sep 2018*) `@JR`
##### Fixed
- Desktop UI fix for workflow

### V3.8.13 (*19 Sep 2018*) `@NGK`
##### Fixed
- Added defaults for google reviews

### V3.8.12 (*18 Sep 2018*) `@NGK`
##### Fixed
- Add-ons scroll issue for mobile pages

### v3.8.11 (*17 Sep 2018*) `@NGK`
##### Fixed
- Made invitation page work for other invites

### v3.8.10 (*15 Sep 2018*) `@NGK`
##### Fixed
- Added temp code to show rsvp page

### v3.8.9 (*15 Sep 2018*) `@NGK`
##### Fixed
- Removed enquiry team from 'cc' for invites related emails
##### Added
- Made complementary services checked by default

### v3.8.8 (*14 Sep 2018*) `@JR`
##### Added
- Surprise landing page

### v3.8.7 (*12 Sep 2018*) `@NGK`
##### Fixed
- Complementary benefits validation

### v3.8.6 (*12 Sep 2018*) `@NGK`
##### Added
- Google Reviews
##### Removed
- Check Feasibility
##### Changed
- Home page top options

### v3.8.5 (*11 Sep 2018*) `@NGK`
##### Added
- RSVP Invitations

### v3.8.4 (*29 Aug 2018*) `@Manav`
##### Modified
- Showing an error and sending an email to customer and team if a customer checks for a non-live product.

### v3.8.3 (*10 Sep 2018*)
##### Fixed
- Shortlist button fix for list pages `@Manav`
- Added Css for Collections list page prepost and housewarming `@Manav`
- List/profile page showing enquire now below footer `@JR`
- $cityUrl issue in thank you page `@JR`
##### Added
- Shortlist screen in workflow `@JR`

### v3.8.1 (*10 Sep 2018*) `@NGK`
##### Fixed
- Order processed booking receipts will consider partner data from bookings

### v3.8.0 (*07 Sep 2018*) `@JR`
##### Added
- Workflow for customers

### v3.7.13 (*04 Sep 2018*) `@JR`
##### Changed
- Auto-popup cookies triggered only after second popup close

### v3.7.12 (*31 Aug 2018*) `@NGK`
##### Changed
- Payment success mobile page

### v3.7.11 (*31 Aug 2018*) `@JR`
##### Added `@JR`
- TOP options for prepost

### v3.7.10 (*26 Aug 2018 ~ 30 Aug 2018*)
##### Fixed @Manav`
- Benefits integration fixes for mobile and desktop views.

### v3.7.9 (*30 Aug 2018*) `@JR`
##### Added
- Whatsapp integration in every page
- Hero copy upgrades
- Job error upgrade

### v3.7.8 (*30 Aug 2018*) `@JR`
##### Fixed
- Job error for enquiry notifications

### v3.7.7 (*29 Aug 2018*) `@NGK`
##### Fixed
- Partner app notification for entertainment options

### v3.7.6 (*29 Aug 2018*) `@NGK`
##### Added
- Post feedback add-on changes

### v3.7.5 (*23 Aug 2018* ~ *28 Aug 2018*)
##### Added     `@Manav`
- Enquiry message based on customer previous status

### v3.7.4 (*27 Aug 2018*) `@NGK`
##### Added
- Occasion category header in collection profile page
- R&E nav link in mobile payment success page

### v3.7.3 (*25 Aug 2018*) `@NGK`
##### Fixed
- Included sms error details and will be sent to sysad@evibe.in

### v3.7.2 (*25 Aug 2018*) `@JR`
##### Added
- Adding top Bday options to homepage

### v3.7.1 (*24 Aug 2018*) `@NGK`
##### Added
- Type ticket booking for packages and app notification switch

### v3.7.0 (*24 Aug 2018*) `@NGK`
##### Added
- Add-ons for surprises
- Multiple Auto bookings back support

### v3.6.26 (*24 Aug 2017*) `@JR`
##### Modified
- SMS modification for refer&earn feedback SMS.
##### Fixed
- Cookie not storing properly, updated the code

### v3.6.25 (*17 Aug 2017*) `@Manav`
##### Added
- Showing email typo error when user enters wrong email and moves to next input.

### v3.6.24.1 (*22 Aug 2018*)
##### Fixed
- Enquiry source storing as auto-popup by default bug fix

### v3.6.24 (*22 Aug 2018*)
##### Fixed
- Ajax request error
- Auto-popup cookie placement & top searched upgrade

### v3.6.23 (*28 July 2018*)
##### Added `@JR`
- Enquire now button modal update for all profile pages & header
##### Modified `@Manav`
- Auto PopUp timings & Added requirements box in autopopup.
- Sending occasion id in autopopup mail using sessions
- URL tracking for dynamic footer

### v3.6.22 (*17 Aug 2018*) `@Manav`
##### Added
- Added benefits for all the pages

### v3.6.21 (*18 Aug 2018*) `@JR`
##### Fixed
- Coupon code fix
- Request new payment link default deadline fix
- chennai city default files

### v3.6.20 (*17 Aug 2018*) `@JR`
##### Modified
- Trackers loading after 7.5 sec, because window.onload is overloading other functions

### v3.6.19 (*16 Aug 2018*) `@JR`
##### Modified
- Loading few trackers after load & added cityUrl validation

### v3.6.18 (*09 Aug 2018 ~ 14 Aug 2018*) `@JR`
##### Modified
- Modified home page for better performance

### v3.6.17 (*07 Aug 2018 ~ 13 Aug 2018*) `@Manav`
##### Added
- Dynamic Footer

### v3.6.16 (*11 Aug 2018*) `@JR`
##### Added
- Text changes for R&E

### v3.6.15 (*06 Aug 2018 ~ 11 Aug 2018*) `@NGK`
##### Added
- Order details in order processed payment success page

### v3.6.14 (*10 Aug 2018*) `@JR`
##### Modified
- Refer & Earn copy upgrades

### v3.6.13 (*08 Aug 2018*) `@JR`
##### Modified
- Enquire form remove defaults, sending page url for team, logging the url for debugging

### v3.6.12 (*08 Aug 2018*) `@Manav`
##### Modified
- Text for thank you card

### v3.6.11 (*07 Aug 2018*) `@AR`
##### Fixed
- Event id check in auto popups

### v3.6.10 (*04 Aug 2018*) `@NGK`
##### Fixed
- Fixed issue with order process payment page going into infinite loop

### v3.6.9 (*04 Aug 2018*)
##### Modified `@AR`
- Refer & Earn friend page copy upgraded

### v3.6.8 (*03 Aug 2018*)
##### Modified `@JR`
- Refer & Earn friend page upgraded

### v3.6.7 (*01 Aug 2018*)
##### Modified `@JR`
- Global search results page upgrades

### v3.6.6 (*31-07-2018 ~ 01 Aug 2018*) `@Manav`
##### Changed
- Made default advance payment to 50% from 30%.

### v3.6.5 (*28 July 2018 ~ 01 Aug 2018*)
##### Added `@Manav`
- Added budget calculation on recon page.

### v3.6.4 (*01 Aug 2018*) `@AR`
##### Fixed
- Timing updated for auto-popup second pop-up
- env for cache refresh time

### v3.6.3 (*25 July 2018*)
##### Fixed `@JR`
- Event id storing issue
##### Added `@JR`
- Updated CSS for popup modal
- Optimise js code

### v3.6.2 (*25 July 2018 ~ 27 Jul 2018*) `@NGK`
##### Removed
- Recommendation open google event.
##### Changed
- Increased cache refresh time to have cache data for longer period of time.
- Same cache key will be hit even if some of the known params like utms and ref hitting.

### v3.6.1 (*21 July 2018*) `@JR
##### Added
- SMS integration
##### Removed
- Return gifts code
- Auto popup for bachelor parties

### v3.6.0 (*21 July 2018*)
##### Added `@JR`
- Refer & Earn

### v3.5.33 (*24 July 2018*)
##### Fixed `@JR`
- Global search auto-popup issue

### v3.5.32 (*23 July 2018*)
##### Fixed `@NGK`
- Fixed issue related to caching

### v3.5.31 (*21 July 2018*)
##### Added `@JR`
- Auto popup for every page

### v3.5.30 (*21 July 2018*) `@NGK`
##### Changed
+ Modified algorithms.
+ Changed cache key from Hash::make() to base64_encode.
+ Removed lazy loading to Similar and MV.
##### Added
+ Implemented cache to the algorithms.
+ Added google events to recommendation page.

### v3.5.29 (*21 July 2018*)
##### Modified `@JR`
- Modified icon for what's app share

### v3.5.28 (*20 July 2018*) `@NGK`
##### Fixed
- Updating and stopping followups not utilise default handler is to create access token.
- Created CouponUser model.

### v3.5.27 (*11 July 2018 ~ 19 Jul 2018*)
##### Added `@JR`
- Checking coupon against user

### v3.5.26 (*17 July 2018 ~ 19 Jul 2018*) `@NGK`
##### Added
- Added support to un-subscribe from retention

### v3.5.25 (*21 July 2018*)
##### Added
- Auto Popup Modal `@JR`

### v3.5.24 (*11 July 2018 *)
##### Fixed `@Manav`
- Adding the payment link expiry and payment link regeneration request by the customer.

### v3.5.23 (*11 July 2018*)
##### Fixed `@NGK`
- Ticket image uploads will now create ticket folders with public write permissions using umask

### v3.5.22 (*06 July 2018 ~ 09 Jul 2018*)
##### Added `@NGK`
- Thank You cards download page for booked customers

### v3.5.21 (*03 July 2018 ~ 06 Jul 2018*)
##### Changed `@NGK`
- Occasion home pages for bangalore and hyderabad

### v3.5.20 (*30 June 2018 ~ 02 Jul 2018*)
##### Modified `@Manav`
- Sending the feasibility check email to team only if distance calculated is in 100km range of any active cities.

### v3.5.19 (*24 Mar 2018 ~ 02TYPE_REMINDER_THANK_YOU_CARD Jul 2018*)
##### Added
- Showing option unavailable to decors that are not live  `@Ramu`
- Fixed issue with fetching options by package sub types `@AR`

### v3.5.18 (*28 June 2018 ~ 30 Jun 2018*)
##### Fixed `@NGK`
- HTML minification wrapping issue for static pages (terms, privacy)

### v3.5.17 (*28 June 2018*)
##### Removed `@AR`
- SiteVisit auto popup is not shown by default

### v3.5.16 (*28 June 2018*)
##### Changed `@AR`
- Bachelor party keyword to youth party
- Removed youtube videos for customer, partner stories on home page
- Punch lines for kids events, surprises in all cities

### v3.5.15 (*27 June 2018*)
##### Changed
- Upgraded recommendations page to get to an end point after sending recommendations `@NGK`

### v3.5.14 (*20 Apr 2018 ~ 26 Jun 2018*)
##### Added
- Added occasion specific categories in birthday home footer `@NGK`
- Added sprite images in footer for all occasions in all cities `@NGK`
- Standalone page to enter correct emails for customers `@NGK`

### v3.5.13 (*23 June 2018*)
##### Changed
- Fetching category for decors `@AR`

### v3.5.12 (*22 June 2018*)
##### Added
- Using sort order score to reorder list pages `@NGK`

### v3.5.11 (*21 June 2018*)
##### Added
- Question to know whether partners are wearing uniform or not? `@NGK`
- Interest in refer and earn program `@NGK`

### v3.5.10 (*18 June 2018*)
##### Changed
- Disabled recommendation rating, negative feedback in recommendations page `@AR`

### v3.5.9 (*13 June 2018*)
##### Added
- Getting default occasion URL based on the parent event Id `@JR`

### v3.5.8 (*07 June 2018 ~ 08 June 2018*)
##### Changed
- Now using AddThis plugin instead of AddToAny for social sharing `@ANJI`
##### Fixed
- Enquire Now modal in pre-post desktop profile pages `@NGK`
- Book Site Visit in decor quick view for pre-post instead of Book Now `@NGK`
- Replaced Book Now and Feasibility with Site Visit in pre-pose decors mobile `@NGK`

### v3.5.7 (*08 June 2018*)
##### Fixed
- Issue with Chineese SPAM tickets `@ANJI`

### v3.5.6 (*05 June 2018*)
##### Changed
- Enquire Now btn for decor profile mobile pages `@NGK`

### v3.5.5 (*04 June 2018*)
##### Changed
- Switched places of party bag and enquire now `@NGK`
##### Fixed
- Added support to use prioritisation `@NGK`

### v3.5.4 (*02 June 2018*)
##### Added
- Google events to track each booking, availability and enquire actions `@NGK`

### v3.5.3 (*01 June 2018*)
##### Fixed
- Social sharing options added `@Ramu`

### v3.5.2 (*02 Jun 2018*)
##### Changed
- CTA buttons in profile, list pages (mob, dek) `@NGK`
##### Fixed
- Enquiry forms `@NGK`

### v3.5.1 (*30 May 2018*)
##### Added
- See all results not working in mobile `@JR`

### v3.5.0 (*30 May 2018*)
##### Added
- Auto hint for search `@JR`

### v3.4.28 (*30 May 2018*)
##### Added
- Cake list, profile pages for House Warming section `@NGK`
- Applied theming to cakes `@NGK`

### v3.4.27 (*29 May 2018*)
##### Fixed
- Surprise Packages main category filter `@NGK`
- Ent category filter `@NGK`
##### Added
- Themed cakes list pages `@NGK`

### v3.4.26 (*29 May 2018*)
##### Fixed
- Product ratings fix for ent category `@NGK`

### v3.4.25 (*28 May 2018*)
##### Added
- HTML minify package to reduce page load speed `@NGK`
##### Changed
- Unwrapped html element tags and comment style in js `@NGK`
- All category theming except cakes `@NGK`

### v3.4.24 (*23 May 2018*)
##### Fixed
- Fixed view live URL option request from DASH `@Ramu`

### v3.4.23 (*22 May 2018*)
##### Added `@JR`
- Header search bar change in mobile

### v3.4.22 (*18 May 2018 ~ 19 May 2018*)
##### Added `@JR`
- Categories based on the results
- Storing the invalid options
- Header search bar change

### v3.4.21 (*19 May 2018*)
##### Changed
- Fetching only required results from Base Decor Controller `@NGK`
- Loading single js file instead on doing request 3 times `@NGK`

### v3.4.20 (*15 May 2018*)
##### Fixed
- Category URL issue & mobile resolution changes `@JR`

### v3.4.19 (*14 May 2018*)
##### Added
- Optimize algorithm for search results
- UI changes in search results page
- Lazyload for search results page images `@JR`

### v3.4.18 (*11 May 2018*)
##### Added
- Added lazyload for decor & package images `@JR`

### v3.4.17 (*08 May 2018*)
##### Added
- Added events for Global search `@JR`

### v3.4.16 (*03 May 2018 ~ 08 May 2018*)
##### Added
- We'll be able to get nearest area based on the pincode entered, in given area in invalid `@NGK`
- Gave access to artists in partner login `@Ramu`
- Validated party date when request header and front pages `@Ramu`
##### Removed
- Commented trigger to update lookup at the time of payment as there exits another at the time of sending receipts `@NGK`

### v3.4.15 (*01 May 2018*)
##### Added
- default showing city results for global search `@NGK`
##### Added
- Trigger to update lookup table `@NGK`
##### Fixed
- Some old code related to ticket handlers `@NGK`

### v3.4.14 (*28 Apr 2018*)
##### Removed
- Hid IPL collection header `@NGK`

### v3.4.13 (*23 Apr 2018*)
##### Added
- Coupon Integration `@JR`

### v3.4.12 (*13 Apr 2018 ~ 19 Apr 2018*)
##### Fixed
- Reject success page "Trying to get property of non-object" error
- Success page "Undefined offset: 0" error

### v3.4.11 (*19 Apr 2018*)
##### Added
- PayTM payment gateway integration `@JR`
##### Changed
- Function to get booking data will not depend on ticket booking `@NGK`

### v3.4.10 (*18 Apr 2018*)
##### Changed
- Recommendations page will open in no-response state `@NGK`

### v3.4.9 (*18 Apr 2018*)
##### Added
- Implemented bitly url shortener as goo.gl is no longer functional `@NGK`

### v3.4.8 (*11 Apr 2018*)
##### Added
- IPL 2018 farmhouses promotional header `@NGK`

### v3.4.7 (*02 Apr 2018 ~ 08 Apr 2018*)
##### Fixed
- Six star question included `@NGK`

### v3.4.6 (*03 Apr 2018 ~ 08 Apr 2018*)
##### Fixed
- Fixed filter issues in some pages`@Ramu`

### v3.4.5 (*28 Mar 2018* ~ 04 Apr 2018)
##### Added
- Send new booking receipts to partners on partner change for send updated receipt command `@NGK`

### v3.4.4 (*01 Apr 2018*)
##### Changed
- Corporate page upgrades

### v3.4.3 (*31 Mar 2018*)
##### Changed
- Hiding the partner location and transportation charges estimate from food profile pages.
- Send email only if recipient email id available

### v3.4.2 (*24 Mar 2018* ~ 26 Mar 2018)
##### Fixed
- Not showing checkout fields in order process payment page from DASH `@JR`
- UI changes payment gateway upgrades `@JR`

### v3.4.1 (*24 Mar 2018*)
##### Fixed
- Fixed feedback error `@Ramu`

### v3.4.0 (*14 Mar 2018*)
##### Added
- Auto Settlements v1 `@NGK`

### v3.3.41 (*17 Mar 2018*)
##### Fixed
- Small code indentation for the <img> tag to work `@NGK`

### v3.3.40 (*15 Mar 2018*)
##### Fixed
- Fixed the bug of job which do API call for customer feedback `@Ramu`

### v3.3.39 (*15 Mar 2018*)
##### Removed
- PartyOne integrated return gifts have been removed and provided back-support `@NGK`

### v3.3.38 (*14 Mar 2018*)
##### Removed
- Added hndler_id to reco shortlisted ticket update `@JR`

### v3.3.37 (*13 Mar 2018*)
##### Changed
Commented handler id in case of auto or partner update  `@Ramu`

### v3.3.36 (*02 Mar 2018*)
##### Added
Added a job for customer auto feedback API call  `@Ramu`

### v3.3.35 (*03 Mar 2018*)
##### Added
- Added gallery & how it works section to the corporate page `@JR`

### v3.3.34 (*03 Mar 2018*)
##### Added
- Added google analytics trigger to the site visit modal & mail to CRM `@JR`

### v3.3.33 (*03 Mar 2018*)
##### Added
- LogSiteError to alert team function to debug issue `@NGK`

### v3.3.32 (*01 Mar 2018*)
##### Added
- Pre-post upgrades - Book site visit modal `@JR`

### v3.3.31 (*24 Feb 2018*)
##### Changed
- Moved corporates to an occasions.
- Modified routes `@ANJI`

### v3.3.30 (*22 Feb 2018*)
##### Added
- Corporate landing page `@JR`

### v3.3.29 (*22 Feb 2018*)
##### Fixed
- Update party location alert will not occur for Normal tickets and won't repeat `@NGK`

### v3.3.28 (*21 Feb 2018*)
##### Fixed
- Added 'isset' array check in booking-feasibility-modal `@NGK`
##### Changed
- Email content of 'update-party-location' that is being sent to CRM team `@NGK`

### v3.3.27 (*21 Feb 2018*)
##### Added
- Automate partner accept/reject `@JR`

### v3.3.26 (*20 Feb 2018*)
##### Added
- Email alert to team if customer entered location is not in our database `@NGK`
##### Fixed
- Decors AB feasibility modal image `@NGK`
- Party bag redirect error `@JR`

### v3.3.25 (*19 Feb 2018*)
##### Added
- Updated surprises and ent list to decors page `@NGK`
- Added list page caching to decors and ent `@NGK`

### v3.3.24 (*19 Feb 2018*)
##### Added
- Remove special chars function even for array in arrays. `@JR`
##### Changed
- Changes the AJAX error mail format for getting the browser version, reverted back data-type to JSON `@JR`

### v3.3.23 (*19 Feb 2018*)
##### Added
- Redirect to specific occasion if they change city `@JR`
##### Fixed
- Birthday collections being redirected to surprises `@NGK`

### v3.3.22 (*14 Feb 2018*)
##### Changed
- Transportation charges will be calculated without addition of min. transport charges `@NGK`

### v3.3.21 (*14 Feb 2018*)
##### Added
- Search bar added back due to SEO decrement issues `@JR`

### v3.3.20 (*14 Feb 2018*)
##### Added
- Freecharge payment gateway integration `@JR`

### v3.3.19 (*13 Feb 2018*)
##### Added
- V-day collection headers for all cities `@NGK`

### v3.3.18 (*08 Feb 2018*)
##### Added
- 100% mandatory advance for surprises and bachelor venue packages `@NGK`
- Added hack to change full url of vd_2018 collection packages `@NGK`
##### Changed
- Removed datatype for AJAX requests to test the reason for JSON parse errors `@JR`

### v3.3.17 (*07 Feb 2018*)
##### Changed
- 'Book Now' and 'Feasibility Check' changes after reverting back from 'Price Check' `@NGK`

### v3.3.16 (*06 Feb 2018*)
##### Added
- Added intermediate view files to launch Pune city `@NGK`

### v3.3.15 (*06 Feb 2018*)
##### Changed
- Collection occasion page CSS & removal of category header for occasion collection pages. `@JR`

### v3.3.14 (*02 Feb 2018*)
##### Changed
- Updated collections CSS `@ANJI`

### v3.3.13 (*01 Feb 2018*)
##### Changed
- Optimisations for the city home page
- Thank-you page text change
- Decor profile page footer is not visible `@JR`

### v3.3.10 (*01 Feb 2018*)
##### Added
- Valentines day header & added back partner signup link in headers `@JR`

### v3.3.9 (*01 Feb 2018*)
##### Changed
- Conversion tracking code updated `@ANJI`

### v3.3.8 (*31 Jan 2018*)
##### Fixed
- Fixed alignment issue for entertainment & decor loading animation fix for 1024 resolution `@JR`

### v3.3.7 (*30 Jan 2018*)
##### Fixed
- Fixed issue with collection profile & removed header in city collections home `@JR`

### v3.3.6 (*29 Jan 2018*)
##### Changed
- Change rupee icon to html entity code `@JR`

### v3.3.5 (*29 Jan 2018*)
##### Added
- Book Now link for collection options `@NGK`
- In mobile resolutions, clicking an option from results page will open profile page in new page `@NGK`

### v3.3.4 (*29 Jan 2018*)
##### Fixed
- List category filters for surprises in mobile resolution `@NGK`

### v3.3.3 (*27 Jan 2018*)
- Reducing loading time for decors list page, removing tags & getting images through AJAX `@JR`

### v3.3.2 (*27 Jan 2018*)
##### Changed
- 'code' to 'option code' in search filter `@NGK`
##### Added
- 'SiteErrorLog' instead of Log::error() `@NGK`
##### Fixed
- Auto Booking payment success routes have been changes to generic-campaign routes `@NGK`

### v3.3.1 (*23 Jan 2018*)
##### Fixed
- Issue with loading images `@ANJI`
- Code revamp in vuejs file `@ANJI`

### v3.3.0 (*22 Jan 2018*)
##### Added
- Partyone integration `@JR`

### v3.2.9 (*22 Jan 2018*)
##### Changed
- Email reporting changed based on the errors, ignoring few redundant errors `@ANJI`

### v3.2.8 (*22 Jan 2018*)
##### Fixed
- Partner dashboard data not showing bug fix, ticket data loading issue `@JR`

### v3.2.7 (*20 Jan 2018*)
##### Fixed
- Optimised code for handling post payment job `@ANJI`

### v3.2.6 (*20 Jan 2018*)
##### Added
- City name in checkout page(also included area for venues) `@JR`

### v3.2.5 (*19 Jan 2018*)
##### Fixed
- Party bag customer mail generation issue `@NGK`

### v3.2.4 (*18 Jan 2018*)
##### Changed
- Removed condition to show Auto Book button `@NGK`

### v3.2.3 (*11 Jan 2018*)
##### Changed
- SEO for surprises section `@ANJI`

### v3.2.2 (*11 Jan 2018*)
##### Changed
- Triggers for auto followup APIs `@NGK`

### v3.2.1 (*12 Jan 2018*)
##### Added
- Entertainment section revamp `@JR`

### v3.2.0 (*11 Dec 2017*)
##### Added
- Surprises revamp  `@JR`

### v3.1.18 (*09 Jan 2017*)
##### Added
- Triggers for auto followup APIs `@NGK`
##### Fixed
- Not interested case handling `@NGK`

### v3.1.17 (*03 Jan 2018*)
##### Changed
- Updated dashboard UI & added pending deliveries, new orders in the dashboard home. `@JR`

### v3.1.16 (*27 Dec 2017*)
##### Changed
- Using routes for terms `@ANJI`
- Text of login modal changed `@ANJI`

### v3.1.15 (*27 Dec 2017*)
##### Added
- Using multiple queue connections based on the usage `@ANJI`

### v3.1.14 (*27 Dec 2017*)
##### Added
- Added compulsory login for the pre-post pages `@JR`

### v3.1.13 (*26 DEC 2017*)
##### Added
- Fallout feedback page and triggers for followup automation `@NGK`

### v3.1.12 (*26 Dec 2017*)
##### Fixed
- Pre-post cityName not found error & Pune house-warming footer not found `@JR`

### v3.1.11 (*23 Dec 2017*)
##### Added
- Showing reviews based on the occasion `@JR`

### v3.1.10 (*22 Dec 2017*)
##### Changed
- On hover desktop list cards and decor code is now shown in highlights `@NGK`

### v3.1.9 (*21 Dec 2017*)
##### Changed
- Modified Headers for all the desktop & iPad resolutions `@JR`

### v3.1.8 (*15 Dec 2017*)
##### Fixed
- Added UTM tags to promotional header for better analytics and enabled christmas collection for HYD `@NGK`

### v3.1.7 (*14 Dec 2017*)
##### Fixed
- Farmhouse collection for new year occasion `@NGK`
##### Added
- New year collection in header `@NGK`

### v3.1.6 (*13 Dec 2017*)
##### Fixed
- Partner dashboard header text changes & deliveries not loading fix `@JR`

### v3.1.5 (*12 Dec 2017*)
##### Added
- Header for christmas `@JR`

### v3.1.4 (*05 Dec 2017*)
##### Added
- Added landing page for google ad campaigns for Bangalore `@ANJI`

### v3.1.3 (*22 Nov 2017*)
##### Fixed
- Party time will now be asked for packages provided by an artist `@NGK`

### v3.1.2 (*20 Nov 2017*)
##### Fixed
- Partner SMS being sent to customer for updated orders `@ANJI`
##### Changed
- Log::info are changed to Log::error for better error reporting `@ANJI`

### v3.1.1 (*20 Nov 2017*)
##### Fixed
- Ajax error while loading the partner login page
- Loading and waiting modals not showing for partner dashboard `@JR`

### v3.1.0 (*18 Nov 2017*)
##### Added
- Launching partner dashboard `@JR`

### v3.0.12 (*18 Nov 2017*)
##### Added
- Price category filter is now applicable to food category `@NGK`

##### Changed
- Now, bachelor venues can't be auto booked for 31st Dec 2017 `@NGK`

### v3.0.10 (*14 Nov 2017*)
##### Fixed
- Price category filter will be shown even though the category ranges are outside of options price range `@NGK`
- Will be shown only to applicable categories `@NGK`

### v3.0.9 (*13 Nov 2017*)
##### Fixed
- After changing the partner, emails are still being sent to the old partner `@Jeevan`

### v3.0.8 (*11 Nov 2017*)
##### Changed
- Price category filter will now have fixed ranges, which can be updated from config file `@NGK`

##### Fixed
- In Customer Stories section, city filter will now show only *public* cities to choose from `@NGK`
- Do not allow customers to make payment for a party having *party date time* before current time `@NGK`

##### Removed
- Log files that have been used to debug *AJAX* fail errors `@NGK`


### v3.0.5 (*9 Nov 2017*)
##### Changed
- Book Now button color has been changed from evibe-pink to green across the decor list and profile pages `@NGK`

##### Fixed
- Advance amount to be paid in decor feasibility check `@NGK`


### v3.0.4 (*9 Nov 2017*)
##### Changed
- For auto bookings, advance amount is computed to round off the balance amount to nearest 100 `@NGK`

##### Fixed
- Party date time fixes. Now computing final party date time based on the min and max bookings party date time `@NGK`
- Page number is now removed on changing any filter/search in list pages `@NGK`


### v3.0.1 (*9 Nov 2017*)
##### Fixed
- "Book Now" button for priest does not work `@NGK`