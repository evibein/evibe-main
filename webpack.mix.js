let mix = require('laravel-mix');
require('laravel-mix-purgecss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

if (!mix.inProduction()) {
	mix.styles([
		// Applications files
		"resources/assets/css/app/sprite.css",
		"resources/assets/css/app/util.css",
		"resources/assets/css/app/header.css",
		"resources/assets/css/app/footer.css",
		"resources/assets/css/app/app.css",
	], "public/css/app_profile_raw.scss");

	mix.styles([
		// Applications files
		"resources/assets/css/portfolios/portfolios_util.css",
		"resources/assets/css/portfolios/planner_portfolio.css",
		"resources/assets/css/portfolios/package_portfolio.css",
		"resources/assets/css/portfolios/venue_portfolio.css",
		"resources/assets/css/portfolios/trending_portfolio.css",
		"resources/assets/css/portfolios/cake_portfolio.css",
		"resources/assets/css/portfolios/decor_portfolio.css",
		"resources/assets/css/portfolios/venue_deals_portfolio.css"
	], "public/css/portfolios_raw.scss");
}

if (mix.inProduction()) {
	mix.sass("resources/assets/sass/bootstrap.scss", "public/css/bootstrap_valid.css")
		.sass("public/css/app_profile_raw.scss", "public/css/app_valid.css")
		.sass("public/css/portfolios_raw.scss", "public/css/portfolios_valid.css")
		.sass("resources/assets/sass/material.scss", "public/css/material_valid.css")
		.purgeCss();
}