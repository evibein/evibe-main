$(document).ready(function () {

	var oneMonth = new Date(new Date().setMonth(new Date().getMonth() + 1));

	window.loadIntrojsTourGuide = function () {
		if (parseInt(Cookies.get("workflowIntro"), 10) !== 2) {
			introJs().addSteps([
				{
					element: "#workflowIntrojsStep1",
					intro: "Switch categories here",
					position: "bottom",
					scrollTo: "element"
				},
				{
					element: ".shortlist-initialize-introjs-button",
					intro: "If you like an option, click here to shortlist it",
					position: "bottom",
					scrollTo: "element"
				},
				{
					element: ".bottom-navigation-enquire-button",
					intro: "Enquire all the options that are selected",
					position: "bottom",
					scrollTo: "element"
				},
				{
					element: ".workflow-change-requirement-button",
					intro: "You can change your party requirements anytime\n",
					position: "bottom",
					scrollTo: "element"
				}
			]).start()
				.oncomplete(function () {
					Cookies.set("workflowIntro", "2", {expires: oneMonth});
				})

				.onexit(function () {
					Cookies.set("workflowIntro", "2", {expires: oneMonth});
				});
		}
	};

	window.loadBasicFunctions = function () {
		var $point_arr, $points, $progress, active, max;

		$point_arr = $('.progress-point');
		$progress = $('.progress').first();

		max = $point_arr.length - 1;

		window.changeWorkflowProgress = function activate(index) {
			if (index !== active) {
				active = index;
				var $_active = $point_arr.eq(active);
				$point_arr
					.removeClass('completed active')
					.slice(0, active).addClass('completed');

				$_active.addClass('active');

				return $progress.css('width', (index / max * 100) + "%");
			}
		};
	};

	window.confirmationModal = function (message) {
		var n = new Noty({
			theme: 'bootstrap-v3',
			layout: 'topCenter',
			text: message,
			buttons: [
				Noty.button('custom text yes', 'btn btn-success', function () {
					window.location.replace("https://evibe.in/");
				}, {id: 'button1', 'data-status': 'ok'}),

				Noty.button('Custom text no', 'btn btn-error', function () {
					n.close();
				})
			]
		}).show();
	};

	window.showBackButton = function () {
		$(".bottom-navigation-next-button").css({
			"width": "79%"
		});
	};

	window.hideBackButton = function () {
		$('.bottom-navigation-next-button').css({
			"width": "100%"
		});
	};

	window.showNoty = function (type, message) {
		if (!message) message = 'An error occurred while submitting your request. Please try again later!';

		new Noty({
			theme: 'mint',
			layout: 'bottomCenter',
			text: message,
			type: type,
			timeout: 4000
		}).show();
	};

	window.showLoading = function () {
		$("#modalLoading").modal({
			keyboard: false,
			backdrop: "static",
			show: true
		});
	};

	window.hideLoading = function () {
		$("#modalLoading").modal("hide");
	};
});