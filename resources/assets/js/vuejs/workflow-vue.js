$(document).ready(function () {
    var currentPage = $('#workflowCurrentScreen').val();

    if (currentPage === "requirements") {
        $.ajax({
            url: $('#workflowDataUrl').val(),
            type: "POST",
            success: function (data) {
                if (data.success) {

                    var workflowData = {
                        screen1: {
                            question: "Which occasion are you planning to celebrate ?",
                            options: {}
                        },
                        screen2: {},
                        screen3: {
                            question: "What all services do you need ?",
                            options: {}
                        },
                        screen4: {
                            options: {}
                        },
                        currentScreen: 1,
                        maxScreenCount: 4,
                        occasion: '',
                        services: [],
                        serviceDetails: [],
                        form: {
                            parent_id: []
                        },
                        planToken: $('#dataPlanToken').val()
                    };

                    /* Pushing screen1 data */
                    if (typeof data.partyPlanningData.events[1] !== 'undefined') {
                        $.each(data.partyPlanningData.events[1]["childEvents"], function (key, value) {
                            workflowData.screen1.options[key] = {"eventName": value.eventName, "eventId": value.eventId};

                            if ((typeof value.isSelected !== "undefined") && value.isSelected == 1) {
                                workflowData.occasion = value.eventId;
                            }
                        });
                    }

                    /* Pushing screen2 data */
                    if (typeof data.partyPlanningData.eventDetails[1] !== 'undefined') {
                        $.each(data.partyPlanningData.eventDetails[1], function (key, value) {
                            var questionOptions = {};
                            $.each(value.options, function (optionKey, option) {
                                questionOptions[option.id] = option.name;
                            });

                            workflowData.form.parent_id[value.questionId] = "";
                            if ((typeof value.answerOptions !== 'undefined') && (typeof value.answerOptions[0] !== 'undefined')) {
                                workflowData.form.parent_id[value.questionId] = value.answerOptions[0];
                            }

                            workflowData.screen2[value.questionId] = ({
                                question: value.question,
                                options: questionOptions,
                                optionTypeId: value.questionTypeId
                            });
                        });
                    }

                    /* Pushing screen3 data */
                    if (typeof data.partyPlanningData.eventSpecificServices[1] !== 'undefined') {
                        $.each(data.partyPlanningData.eventSpecificServices[1], function (key, value) {
                            workflowData.screen3.options[key] = {"serviceName": value.serviceName, "serviceId": value.serviceId};
                        });
                    }

                    /* Pushing screen4 data */
                    if (typeof data.partyPlanningData.eventSpecificServices[1] !== 'undefined') {
                        $.each(data.partyPlanningData.eventSpecificServices[1], function (key, value) {
                            var subOptions = {};

                            if (Object.keys(value.serviceTags).length === 0) {
                                subOptions[value.serviceId] = {
                                    name: value.serviceName,
                                    min: 0,
                                    max: 0
                                };
                            }

                            $.each(value.serviceTags, function (optionKey, option) {
                                subOptions[optionKey] = {
                                    name: option.name,
                                    min: option.minPrice,
                                    max: option.maxPrice
                                };

                                if ((typeof option.isSelected !== "undefined") && option.isSelected == 1) {
                                    workflowData.serviceDetails.push(optionKey);
                                    workflowData.services.push(value.serviceId);
                                }
                            });

                            workflowData.screen4.options[value.serviceId] = ({
                                name: value.serviceName,
                                id: value.serviceId,
                                subOptions: subOptions
                            });
                        });
                        $.unique(workflowData.services);
                    }

                    Vue.component('workflow-top-nav', {
                        template: '<div><slot></slot></div>'
                    });

                    Vue.component('workflow-body-nav', {
                        template: '<div><slot></slot></div>'
                    });

                    Vue.component('workflow-bottom-nav', {
                        template: '<div><slot></slot></div>'
                    });

                    new Vue({
                        el: '#workflow',

                        data: workflowData,

                        mounted: function () {
                            this.$nextTick(function () {
                                $(".workflow-loading-screen").remove();
                                $(".kids-events-workflow-wrap").removeClass("hide");
                                window.loadBasicFunctions();
                            });
                        },

                        methods: {
                            home: function () {
                                if (this.currentScreen > 1) {
                                    window.confirmationModal("Changes that you made may not be saved, Are you sure to close?");
                                } else {
                                    window.location.replace("https://evibe.in/");
                                }
                            },

                            next: function () {
                                if (this.validateScreen(this.currentScreen)) {
                                    if (this.currentScreen < this.maxScreenCount) {
                                        window.changeWorkflowProgress(this.currentScreen);
                                        this.currentScreen += 1;
                                    }

                                    if (this.currentScreen > 1) {
                                        window.showBackButton();
                                    }
                                }
                            },

                            back: function () {
                                if (this.currentScreen > 1) {
                                    window.changeWorkflowProgress(this.currentScreen - 2);
                                    this.currentScreen -= 1;
                                }

                                if (this.currentScreen === 1) {
                                    window.hideBackButton();
                                }
                            },

                            progressPoint: function (screenId) {
                                if (this.currentScreen < screenId) {
                                    var i = 1;
                                    while (i < screenId) {
                                        if (!this.validateScreen(i)) {
                                            return false;
                                        }
                                        i++;
                                    }
                                }

                                this.currentScreen = screenId;

                                if (screenId > 1) {
                                    showBackButton();
                                } else {
                                    hideBackButton();
                                }

                                window.changeWorkflowProgress(this.currentScreen - 1);
                            },

                            validateScreen: function (screenId) {
                                if (screenId === 1) {
                                    if (this.occasion === "") {
                                        window.showNoty("error", 'Please select the occasion to continue');

                                        return false;
                                    }

                                    return true;
                                } else if (screenId === 2) {
                                    var screen2Data = this.form["parent_id"];
                                    var isTestPassed = true;

                                    $.each(this.screen2, function (index, value) {
                                        if (screen2Data[index] === "") {
                                            window.showNoty("error", 'Please select all the options');

                                            isTestPassed = false;
                                            return false;
                                        }
                                    });

                                    return isTestPassed;
                                } else if (screenId === 3) {
                                    if (this.services.length === 0) {
                                        window.showNoty("error", 'Please select atleast one service to continue');

                                        return false;
                                    }

                                    return true;
                                } else if (screenId === 4) {
                                    if (this.serviceDetails.length === 0) {
                                        window.showNoty("error", 'Please select your requirement to continue');

                                        return false;
                                    }

                                    var valid = "yes";
                                    var validTags = [];

                                    $.each(workflowData.services, function (key, value) {
                                        $.each(Object.keys(workflowData.screen4.options[value]["subOptions"]), function (tagKey, tag) {
                                            validTags.push(tag);
                                        });

                                        if ($('input[type=checkbox][name=serviceDetails' + value + ']:checked').length == 0) {
                                            valid = "no";
                                        }
                                    });

                                    if (valid === "yes") {
                                        var data = this;
                                        $.each(data.serviceDetails, function (key, value) {
                                            if ($.inArray(value, validTags) < 0) {
                                                delete data.serviceDetails[key];
                                            }
                                        });

                                        return true;
                                    } else {
                                        window.showNoty('error', 'Please select at least one service in each category');

                                        return false;
                                    }
                                }
                            },

                            submitPlan: function () {
                                if (this.validateScreen(this.currentScreen)) {
                                    showLoading();
                                    $.post($('#workflowDataSubmitUrl').val(),
                                        {
                                            occasion: this.occasion,
                                            questions: this.form["parent_id"],
                                            services: this.services,
                                            serviceDetails: this.serviceDetails,
                                            cityId: $('#userCityId').val(),
                                            planToken: this.planToken
                                        })
                                        .done(function (data) {
                                            if (data.success) {
                                                window.location.replace(data.redirectUrl);
                                            } else {
                                                hideLoading();
                                                window.showNoty('error', 'Error occurred while submitting your request, Please check the network & try again.');
                                            }
                                        })
                                        .fail(function (data) {
                                            hideLoading();
                                            window.showNoty('error', 'Error occurred while submitting your request, Please check the network & try again.');
                                        });
                                }
                            },

                            labelScreen1: function (value) {
                                return "occasionLabel" + value;
                            },

                            labelScreen2: function (value, optionValue) {
                                return "detailsLabel" + value + optionValue;
                            },

                            labelScreen3: function (value) {
                                return "serviceLabel" + value;
                            },

                            labelScreen4: function (value) {
                                return "serviceLabel" + value;
                            },

                            getServiceDetailsName: function (value) {
                                return "serviceDetails" + value;
                            },

                            inScreen3: function (value) {
                                return $.inArray(value, this.services) >= 0 ? true : false;
                            }
                        }
                    });
                } else {
                    alert("Error while fetching the data, Please reload the page & try again");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("Error while fetching the data, Please reload the page & try again");
            }
        });
    } else if (currentPage === "options") {
        $.ajax({
            url: $('#workflowDataUrl').val(),
            dataType: 'json',
            type: "POST",
            success: function (data) {
                if (data.success) {

                    var workflowData = {
                        occasion: data.eventDetails.name,
                        partyData: data.partyDate,
                        location: data.location,
                        services: {},
                        serviceDetails: {},
                        shortlistCount: 0,
                        currentScreen: data.services[Object.keys(data.services)[0]]["id"],
                        shortlistInitializeButton: 1,
                        screen6name: "",
                        screen6phone: "",
                        screen6email: "",
                        screen6comment: "",
                        screen6bookingLikeliness: "-1",
                        screen6timeSlot: "-1",
                        screen7ShortlistedOptions: ""
                    };

                    var images = {
                        1: "card_giftcard",
                        13: "local_florist",
                        20: "card_giftcard",
                        22: "card_giftcard",
                        6: "cake"
                    };

                    var dt = new Date();
                    var secs = dt.getSeconds() + (60 * (dt.getMinutes() + (60 * dt.getHours())));
                    $("#workflowEnquiryTimeSlotOptions option").each(function () {
                        if ((secs >= $(this).data("minTime")) && (secs <= $(this).data("maxTime"))) {
                            $("#workflowEnquiryTimeSlot").append($(this).clone());
                        }
                    });

                    $.each(data.services, function (key, value) {
                        workflowData.services[value.id] = {name: value.name, image: value.image};
                    });

                    $.each(data.options, function (key, value) {
                        workflowData.serviceDetails[key] = value.categoryOptions;

                        $.each(value.categoryOptions, function (optionKey, optionValue) {
                            if (optionValue.isSelected == 1) {
                                workflowData.shortlistCount++;
                            }
                        });
                    });

                    $.post($("#workflowShortlistedOptionsUrl").val())
                        .done(function (shortListData) {
                            if (shortListData.success) {
                                vm.screen7ShortlistedOptions = shortListData.shortlistedOptions;
                            } else {
                                window.showNoty("error", "Error while fetching your shortlisted options, Please check the network & try again.");
                            }
                        })
                        .fail(function (shortListData) {
                            window.showNoty("error", "Error while fetching your shortlisted options, Please check the network & try again.");
                        });

                    Vue.component("workflow-top-nav", {
                        template: "<div><slot></slot></div>"
                    });

                    Vue.component("workflow-body-nav", {
                        template: "<div><slot></slot></div>"
                    });

                    Vue.component("workflow-bottom-nav", {
                        template: "<div><slot></slot></div>"
                    });

                    var vm = new Vue({
                        el: '#workflow',

                        data: workflowData,

                        mounted: function () {
                            this.$nextTick(function () {
                                $('.workflow-loading-screen').remove();
                                $('.kids-events-workflow-wrap').removeClass('hide');
                                window.loadBasicFunctions();
                                window.loadIntrojsTourGuide();
                            });
                        },

                        methods: {
                            changeService: function (index) {
                                this.currentScreen = index;
                            },

                            formatPrice: function (price) {
                                return parseFloat(price, 10).toFixed(0).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
                            },

                            helpIconIntro: function () {
                                window.loadIntrojsTourGuide();
                            },

                            selectedService: function (index) {
                                return {
                                    'workflow-screen5-service-wrap-selected': index == this.currentScreen
                                };
                            },

                            shortlistOption: function (type, id) {
                                var WorkflowData = this.serviceDetails[type];
                                $.each(WorkflowData, function (key, value) {
                                    if (value.id == id) {
                                        value.isSelected = 1;
                                    }
                                });
                                increaseShortlistCount();

                                $.post($('#workflowShortlistUrl').val(),
                                    {
                                        productId: id,
                                        productTypeId: type
                                    })
                                    .done(function (responseData) {
                                        if (responseData.success) {
                                            /*Do nothing, count already got udpated*/
                                        } else {
                                            $.each(WorkflowData, function (key, value) {
                                                if (value.id == id) {
                                                    value.isSelected = 0;
                                                }
                                            });
                                            decreaseShortListCount();
                                            window.showNoty('error', 'Error occurred while submitting your request, Please check the network & try again.');
                                        }
                                    })
                                    .fail(function (data) {
                                        $.each(WorkflowData, function (key, value) {
                                            if (value.id == id) {
                                                value.isSelected = 0;
                                            }
                                        });
                                        decreaseShortListCount();
                                        window.showNoty('error', 'Error occurred while submitting your request, Please check the network & try again.');
                                    });
                            },

                            removeShortlistedOption: function (type, id) {
                                var WorkflowData = this.serviceDetails[type];
                                $.each(WorkflowData, function (key, value) {
                                    if (value.id == id) {
                                        value.isSelected = 0;
                                    }
                                });
                                decreaseShortListCount();

                                $.post($('#workflowRemoveShortlistUrl').val(),
                                    {
                                        productId: id,
                                        productTypeId: type
                                    })
                                    .done(function (data) {
                                        if (data.success) {
                                            /*Do nothing as the count already got decreased*/
                                        } else {
                                            $.each(WorkflowData, function (key, value) {
                                                if (value.id == id) {
                                                    value.isSelected = 1;
                                                }
                                            });
                                            increaseShortlistCount();
                                            window.showNoty('error', 'Error occurred while submitting your request, Please check the network & try again.');
                                        }
                                    })
                                    .fail(function (data) {
                                        $.each(WorkflowData, function (key, value) {
                                            if (value.id == id) {
                                                value.isSelected = 1;
                                            }
                                        });
                                        increaseShortlistCount();
                                        window.showNoty('error', 'Error occurred while submitting your request, Please check the network & try again.');
                                    });
                            },

                            removeScreen7ShortlistedOption: function (type, id) {
                                var WorkflowData = this.serviceDetails[type];
                                var WorkflowShortListData = this.screen7ShortlistedOptions[type];

                                $.each(WorkflowData, function (key, value) {
                                    if (value.id == id) {
                                        value.isSelected = 0;
                                    }
                                });

                                $.each(WorkflowShortListData["options"], function (key, value) {
                                    if (value.id == id) {
                                        value.isSelected = 0;
                                    }
                                });
                                decreaseShortListCount();

                                $.post($("#workflowRemoveShortlistUrl").val(),
                                    {
                                        productId: id,
                                        productTypeId: type
                                    })
                                    .done(function (data) {
                                        if (data.success) {
                                            /*Do nothing as the count already got decreased*/
                                        } else {
                                            $.each(WorkflowData, function (key, value) {
                                                if (value.id == id) {
                                                    value.isSelected = 1;
                                                }
                                            });

                                            $.each(WorkflowShortListData["options"], function (key, value) {
                                                if (value.id == id) {
                                                    value.isSelected = 0;
                                                }
                                            });
                                            increaseShortlistCount();
                                            window.showNoty('error', 'Error occurred while submitting your request, Please check the network & try again.');
                                        }
                                    })
                                    .fail(function (data) {
                                        $.each(WorkflowData, function (key, value) {
                                            if (value.id == id) {
                                                value.isSelected = 1;
                                            }
                                        });

                                        $.each(WorkflowShortListData["options"], function (key, value) {
                                            if (value.id == id) {
                                                value.isSelected = 0;
                                            }
                                        });
                                        increaseShortlistCount();
                                        window.showNoty('error', 'Error occurred while submitting your request, Please check the network & try again.');
                                    });
                            },

                            enquireNow: function () {
                                if (this.shortlistCount === 0) {
                                    showNoty('error', "Please select at least one option to continue");
                                } else {
                                    $('.kids-events-workflow-wrap-mob').addClass('hide');
                                    $('.workflow-shortlist-screen-mob').addClass('hide');
                                    $('.workflow-enquiry-screen-mob').removeClass('hide');
                                    $("#modalWorkflowEnquiryForm").modal({
                                        'show': true,
                                        'backdrop': 'static',
                                        'keyboard': false
                                    });

                                    $("#enquiryForm").find(".mdl-textfield").removeClass("is-upgraded").attr("data-upgraded", "");
                                    if (!(typeof(componentHandler) === 'undefined')) {
                                        componentHandler.upgradeDom();
                                    }

                                    $("#modalWorkflowEnquiryForm").find(".mdl-textfield").removeClass("is-upgraded").attr("data-upgraded", "");
                                    if (!(typeof(componentHandler) === 'undefined')) {
                                        componentHandler.upgradeDom();
                                    }
                                }
                            },

                            showShortListedOptions: function () {
                                if (this.shortlistCount === 0) {
                                    showNoty('error', "Your shopping cart is empty");
                                } else {
                                    $(".workflow-screen7-options").addClass("hide");
                                    $(".workflow-screen7-loading").removeClass("hide");

                                    $.post($("#workflowShortlistedOptionsUrl").val())
                                        .done(function (shortListData) {
                                            if (shortListData.success) {
                                                $(".workflow-screen7-loading").addClass("hide");
                                                $(".workflow-screen7-options").removeClass("hide");
                                                vm.screen7ShortlistedOptions = shortListData.shortlistedOptions;
                                            } else {
                                                window.showNoty("error", "Error while fetching your shortlisted options, Please check the network & try again.");
                                            }
                                        })
                                        .fail(function (shortListData) {
                                            window.showNoty("error", "Error while fetching your shortlisted options, Please check the network & try again.");
                                        });

                                    $('.kids-events-workflow-wrap').addClass('hide');
                                    $('.workflow-enquiry-screen').addClass('hide');
                                    $('.workflow-shortlist-screen').removeClass('hide');
                                }
                            },

                            submitEnquiry: function () {
                                $("#modalWorkflowEnquiryForm").modal("hide");
                                showLoading();

                                $.post($('#workflowEnquireUrl').val(),
                                    {
                                        name: this.screen6name,
                                        phone: this.screen6phone,
                                        email: this.screen6email,
                                        requirement: this.screen6comment,
                                        bookingLikeliness: this.screen6bookingLikeliness,
                                        timeSlot: this.screen6timeSlot
                                    })
                                    .done(function (data) {
                                        if (data.success) {
                                            window.location.replace(data.redirectUrl);
                                        } else {
                                            hideLoading();
                                            $("#modalWorkflowEnquiryForm").modal("show");

                                            window.showNoty('error', data.error);
                                        }
                                    })
                                    .fail(function (data) {
                                        hideLoading();
                                        $("#modalWorkflowEnquiryForm").modal("show");

                                        window.showNoty('error', 'Error occurred while submitting your request, Please check the network & try again.');
                                    });
                            },

                            closeEnquireNow: function () {
                                $('.kids-events-workflow-wrap').removeClass('hide');
                                $('.workflow-enquiry-screen').addClass('hide');
                            },

                            closeShortlistNow: function () {
                                $('.kids-events-workflow-wrap').removeClass('hide');
                                $('.workflow-shortlist-screen').addClass('hide');
                            },

                            getImageFromIndex: function (index) {
                                return images[index];
                            }
                        }
                    });

                    function increaseShortlistCount() {
                        vm.shortlistCount = vm.shortlistCount + 1;
                    }

                    function decreaseShortListCount() {
                        vm.shortlistCount = vm.shortlistCount - 1;
                    }

                    $(".list-option-image, .list-option-title").on("click", function () {
                        addGAEvent("workflow", "Screen5", "Option Clicked");
                    });
                } else {
                    showNoty('error', "Error while fetching the data, Please reload the page & try again");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                showNoty('error', "Error while fetching the data, Please reload the page & try again");
            }
        });
    }
});