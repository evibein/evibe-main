var evibeApp = new Vue({

	el: '#evibeApp',

	data: {
		user: '',
		pbCount: 0,
		partyDate: '',
		partyPinCode: '',
		partyBagList: '',
		currentOption: '',
		inputData: '',
		partyBagListData: '',
		partyBagBudget: {
			pbMin: 0,
			pbMax: 0,
			pbWorthMin: 0,
			pbWorthMax: 0
		},
		api: {
			tempShortlist: "/my/party-bag/temp",
			shortlistModal: "/my/party-bag/pre-shortlist",
			shortlistModalSubmit: "/my/party-bag/pre-shortlist/submit",
			clearPartyBag: "/my/party-bag/clear"
		},
		shortlistedElement: "",
		shortlistData: {},
		pageUrl: window.location.href,
		cityId: "",
		occasionId: "",
		productTypeCake: "",
		productTypeDecor: "",
		returnGifts: {
			categoryId: "",
			productDetails: [],
			categoryDetails: [],
			loadingProductWrapper: true,
			loadingCategoryWrapper: true,
			productWrapper: false,
			categoryWrapper: false,
			noProductWrapper: false,
			isFilterActive: false,
			api: {
				partyOneTracker: "/buy/redirect/partyone?src=",
				baseUrl: ""
			}
		}
	},

	mounted: function () {

		/* cannot un-shortlist without login */
		/* if data exists in local storage and not in shortlisted data and user is logged in trigger related modal */
		/* while storing in local storage, store 'isProfile' */
		/* after login trigger implementation comes here */
		/* alert(this.pageUrl); */

		this.fetchUserDataServer();
	},

	methods: {

		/* fetch user name and party bag data from server */
		fetchUserDataServer: function () {
			var self = this;
			$.post('/user/userdata', function (data, status, request) {
				if (data['isLoggedIn'] === true) {
					/* add to GTM dataLayer for event tracking */
					if (data.hasOwnProperty("email") && data['email']) {
						if (typeof addToGTMDataLayer == "function") {
							window.addToGTMDataLayer({
								"emailId": data['email']
							});
						}
					}

					Vue.set(evibeApp, 'user', data['name']);
					$.get('/my/party-bag/partybag-data', function (response, status, request) {
						if (response['partybag']) {
							Vue.set(evibeApp, 'pbCount', response['partybag']['count']);
							Vue.set(evibeApp, 'partyBagListData', response['partybag']['data']);

							self.updateDetailPage();
							self.updateListPages();
						}
					});
				} else {
					/* No Action */
				}
			});
		},

		/* update party bag count */
		updatePartyBag: function (count, inputData) {
			this.pbCount = this.pbCount + count;
		},

		/* update party bag data */
		updatePartyBagListData: function (count, shortlistOptionData) {
			if (count > 0) {
				this.partyBagListData.unshift(shortlistOptionData);
			} else {
				var inputDataIndex = -1;

				for (index in this.partyBagListData) {
					if (shortlistOptionData.cityId == this.partyBagListData[index].cityId &&
						shortlistOptionData.occasionId == this.partyBagListData[index].occasionId &&
						shortlistOptionData.mapTypeId == this.partyBagListData[index].mapTypeId &&
						shortlistOptionData.mapId == this.partyBagListData[index].mapId) {
						inputDataIndex = index;
						break;
					}
				}

				if (count === -1) {
					if (inputDataIndex > -1) {
						this.partyBagListData.splice(inputDataIndex, 1);
					}
				}
				if (count === 0) {
					if (inputDataIndex > -1) {
						this.partyBagListData.splice(inputDataIndex, 1, shortlistOptionData);
						/* this.partyBagListData[inputDataIndex] = shortlistOptionData; */
					}
				}
			}
		},

		/* show login modal */
		userLogin: function () {
			$('#loginModal').modal('show');
		},

		getSelectedData: function () {
			var self = this;
			var categories = self.getCurrentValue();
			var currentValues = [];

			$.each(currentValues, function (key, val) {
				categories.push({
					'id': val.id,
					'catId': val.catId,
					'catName': val.catName,
					'name': val.name,
					'price': val.price,
					'worth': val.worth,
					'isFixed': val.isFixed,
					'weight': val.weight
				});
			});

			return categories;
		},

		/* get all selected values */
		getCurrentValue: function () {
			var selectedData = [];
			$('.shortlist-modal-container .shortlist-modal-field-cat option:selected').each(function () {
				selectedData.push({
					'catId': $(this).parents('.shortlist-modal-field-wrap').data('catid'),
					'catName': $(this).parents('.shortlist-modal-field-wrap').data('name'),
					'id': $(this).val(),
					'price': $(this).data('price'),
					'worth': $(this).data('worth'),
					'isFixed': $(this).data('isfixed'),
					'weight': $(this).data('weight'),
					'name': $(this).data('name')
				})
			});
			$('.shortlist-modal-container .shortlist-modal-field-cat option').each(function () {
				if ($(this).is(":selected")) {
					$(this).text($(this).data('name'));
				} else if ($(this).data('displayoption').length) {
					$(this).text($(this).data('displayoption'));
				}
			});
			return selectedData;
		},

		shortlistModalAddToCartBtn: function () {
			var self = this;

			/* resetting pre-shortlist modal clear bag container */
			$('.shortlist-add-to-bag-container').removeClass('hide');
			$('.shortlist-clear-bag-container').addClass('hide');

			self.shortlistData['price'] = parseInt($('.shortlist-modal-container .total-price').data('price'), 10);
			self.shortlistData['priceWorth'] = parseInt($('.shortlist-modal-container .price-worth').data('price'), 10);
			self.shortlistData['productPrice'] = parseInt($('.shortlist-modal-container .price').data('price'), 10);
			self.shortlistData['transportAmount'] = parseInt($('.shortlist-modal-container .transport-price').data('price'), 10);
			self.shortlistData['advanceAmount'] = parseInt($('.shortlist-modal-container .advance-amount').data('price'), 10);
			self.shortlistData['categories'] = self.getSelectedData();
			self.shortlistData['deliverySlotId'] = $('.shortlist-modal-container #shortlistModalDeliverySlot').val();

			if (self.shortlistData['isProfile']) {
				self.shortlistOptionWithDataFromProfilePage(self.shortlistedElement, self.shortlistData);
			} else {
				self.shortlistOptionWithDataFromListPage(self.shortlistedElement, self.shortlistData);
			}

			/* since there are no validations to be done, we can close the modal */
			/* if anything fails, error message will be shown */
			$('#shortlistModal').modal('hide');
		},

		shortlistModalCloseClearBag: function () {
			$('#shortlistModal').modal('hide');
			$('.shortlist-add-to-bag-container').removeClass('hide');
			$('.shortlist-clear-bag-container').addClass('hide');
		},

		shortlistModalClearBagBtn: function () {
			/* trigger API to clear existing options */
			/* send present option data to validate before removing as this is an ajax request */

			var self = this;

			self.shortlistData['price'] = parseInt($('.shortlist-modal-container .total-price').data('price'), 10);
			self.shortlistData['priceWorth'] = parseInt($('.shortlist-modal-container .price-worth').data('price'), 10);
			self.shortlistData['productPrice'] = parseInt($('.shortlist-modal-container .price').data('price'), 10);
			self.shortlistData['transportAmount'] = parseInt($('.shortlist-modal-container .transport-price').data('price'), 10);
			self.shortlistData['advanceAmount'] = parseInt($('.shortlist-modal-container .advance-amount').data('price'), 10);
			self.shortlistData['categories'] = self.getSelectedData();
			self.shortlistData['deliverySlotId'] = $('.shortlist-modal-container #shortlistModalDeliverySlot').val();

			/* make form data */
			var inputData = {
				'productId': self.shortlistData['productId'],
				'productTypeId': self.shortlistData['productTypeId'],
				'cityId': self.shortlistData['cityId'],
				'occasionId': self.shortlistData['occasionId'],
				'partyDate': self.shortlistData['partyDate'],
				'partyPinCode': self.shortlistData['partyPinCode']
			};

			var clearPartyBagUrl = self.api.clearPartyBag;

			if (clearPartyBagUrl) {
				$.ajax({
					url: clearPartyBagUrl,
					type: 'POST',
					dataType: 'json',
					data: inputData,
					success: function (data, textStatus, jqXHR) {
						if (data.success) {
							$('#shortlistModal').modal('hide');
							if (self.shortlistData['isProfile']) {
								self.shortlistOptionWithDataFromProfilePage(self.shortlistedElement, self.shortlistData);
								location.reload();
							} else {
								self.shortlistOptionWithDataFromListPage(self.shortlistedElement, self.shortlistData);
								location.reload();
							}
						} else {
							self.alertDialog('Sorry, an error has occurred, please try again later.');
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						self.alertDialog('Sorry, an error has occurred, please try again later.');
						window.notifyTeam({
							"url": clearPartyBagUrl,
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			}
		},

		shortlistOptionFromListPage: function (event) {
			var self = this;
			var currentElement = event.target;
			self.shortlistedElement = currentElement;
			var parentEl = $(currentElement).parent();
			var dataEl = parentEl.find('.hideShortlistResultsData');
			$('#shortlistModalContainer').empty();

			if (!dataEl.length) {
				return false;
			}

			/* restore shortlist modal state */
			$('.shortlist-add-to-bag-container').removeClass('hide');
			$('.shortlist-clear-bag-container').addClass('hide');

			var productId = dataEl.data('mapid') ? dataEl.data('mapid') : false;
			var productTypeId = dataEl.data('maptypeid') ? dataEl.data('maptypeid') : false;
			var cityId = dataEl.data('cityid') ? dataEl.data('cityid') : false;
			var occasionId = dataEl.data('occasionid') ? dataEl.data('occasionid') : false;
			var urlAdd = dataEl.data('urladd') ? dataEl.data('urladd') : false;
			var urlDelete = dataEl.data('urldelete') ? dataEl.data('urldelete') : false;

			var clearOptionsModal = false;
			var clearOptionsText = "";

			if (!productId || !productTypeId || !cityId || !occasionId) {
				return false;
			}

			self.shortlistData = {
				'productId': productId,
				'productTypeId': productTypeId,
				'cityId': cityId,
				'occasionId': occasionId,
				'urlAdd': urlAdd,
				'urlDelete': urlDelete,
				'isProfile': 0
			};

			/* can only un-shortlist if logged in */
			if (this.user !== '') {
				/* check if we need to shortlist / delete shortlist */
				if (parentEl.hasClass('shortlistIcon')) {

					/* validate shortlist data */
					if (self.pbCount) {
						if (self.cityId && self.shortlistData['cityId'] && (self.cityId !== self.shortlistData['cityId'])) {
							clearOptionsModal = true;
							clearOptionsText = "Your party bag contains services which belong to other city. Do you wish to clear those services and add this service?"
						}

						if (self.occasionId && self.shortlistData['occasionId'] && (self.occasionId !== self.shortlistData['occasionId'])) {
							clearOptionsModal = true;
							clearOptionsText = "Your party bag contains services which belong to other occasion. Do you wish to clear those services and add this service?"
						}
					}

					/* show modal */
					var urlShortlistModal = self.api.shortlistModal;
					if (urlShortlistModal) {
						$.ajax({
							url: urlShortlistModal,
							type: 'POST',
							dataType: 'html',
							data: self.shortlistData,
							success: function (data) {
								if (data) {
									$('#shortlistModalContainer').append(data);
								}

								if (clearOptionsModal) {
									$('.shortlist-add-to-bag-container').addClass('hide');
									$('.shortlist-clear-bag-msg').text(clearOptionsText);
									$('.shortlist-clear-bag-container').removeClass('hide');
								}

								if (!$('.shortlist-modal-field-wrap').length) {
									$('.shortlist-modal-container').addClass('hide');
								}

								if ($('.shortlist-modal-field-wrap').length || clearOptionsModal) {
									$('#shortlistModal').modal('show');
								} else {
									self.shortlistData['price'] = parseInt($('.shortlist-modal-container .total-price').data('price'), 10);
									self.shortlistData['priceWorth'] = parseInt($('.shortlist-modal-container .price-worth').data('price'), 10);
									self.shortlistData['productPrice'] = parseInt($('.shortlist-modal-container .price').data('price'), 10);
									self.shortlistData['transportAmount'] = parseInt($('.shortlist-modal-container .transport-price').data('price'), 10);
									self.shortlistData['advanceAmount'] = parseInt($('.shortlist-modal-container .advance-amount').data('price'), 10);
									self.shortlistData['categories'] = self.getSelectedData();
									self.shortlistData['deliverySlotId'] = $('.shortlist-modal-container #shortlistModalDeliverySlot').val();

									self.shortlistOptionWithDataFromListPage(self.shortlistedElement, self.shortlistData);
								}
							},
							error: function (jqXHR, textStatus, errorThrown) {
								window.showNotyError('Sorry, an error has occurred, please try again later.');
								window.notifyTeam({
									"url": urlShortlistModal,
									"textStatus": textStatus,
									"errorThrown": errorThrown
								});
							}
						});
					}
				} else if (parentEl.hasClass('shortlistedIcon')) {

					var inputData = {
						'city_id': self.shortlistData['cityId'] ? self.shortlistData['cityId'] : false,
						'occasion_id': self.shortlistData['occasionId'] ? self.shortlistData['occasionId'] : false,
						'map_id': self.shortlistData['productId'] ? self.shortlistData['productId'] : false,
						'map_type_id': self.shortlistData['productTypeId'] ? self.shortlistData['productTypeId'] : false,
						'party_date': self.shortlistData['partyDate'] ? self.shortlistData['partyDate'] : false,
						'party_pin_code': self.shortlistData['partyPinCode'] ? self.shortlistData['partyPinCode'] : false,
						'price': self.shortlistData['price'] ? self.shortlistData['price'] : null,
						'price_worth': self.shortlistData['priceWorth'] ? self.shortlistData['priceWorth'] : null,
						'product_price': self.shortlistData['productPrice'] ? self.shortlistData['productPrice'] : null,
						'transport_amount': self.shortlistData['transportAmount'] ? self.shortlistData['transportAmount'] : null,
						'advance_amount': self.shortlistData['advanceAmount'] ? self.shortlistData['advanceAmount'] : null,
						'categories': self.shortlistData['categories'] ? self.shortlistData['categories'] : [],
						'deliverySlotId': self.shortlistData['deliverySlotId'] ? self.shortlistData['deliverySlotId'] : null
					};

					this.handleListPageDeleteShortlistOption(urlDelete, inputData, currentElement);
				} else {
					/* do nothing */
				}
			} else {
				/* @see: do not focus right now */
				/* store option basic data in local storage */
				/* trigger login modal */
				/* write some where to trigger shortlist modal after login */
				self.userLogin();
			}
		},

		shortlistOptionWithDataFromListPage: function (currentElement, shortlistData) {
			var self = this;
			/* var currentElement = event.target; */
			var parentEl = $(currentElement).parent();
			var dataEl = parentEl.find('.hideShortlistResultsData');

			if (!dataEl.length) {
				return false;
			}

			var productId = shortlistData['productId'] ? shortlistData['productId'] : false;
			var productTypeId = shortlistData['productTypeId'] ? shortlistData['productTypeId'] : false;
			var cityId = shortlistData['cityId'] ? shortlistData['cityId'] : false;
			var occasionId = shortlistData['occasionId'] ? shortlistData['occasionId'] : false;
			var price = shortlistData['price'] ? shortlistData['price'] : null;
			var priceWorth = shortlistData['priceWorth'] ? shortlistData['priceWorth'] : null;
			var productPrice = shortlistData['productPrice'] ? shortlistData['productPrice'] : null;
			var transportAmount = shortlistData['transportAmount'] ? shortlistData['transportAmount'] : null;
			var advanceAmount = shortlistData['advanceAmount'] ? shortlistData['advanceAmount'] : null;
			var categories = shortlistData['categories'] ? shortlistData['categories'] : [];
			var deliverySlotId = shortlistData['deliverySlotId'] ? shortlistData['deliverySlotId'] : null;
			var urlAdd = shortlistData['urlAdd'] ? shortlistData['urlAdd'] : false;
			var urlDelete = shortlistData['urlDelete'] ? shortlistData['urlDelete'] : false;

			if (!productId || !productTypeId || !cityId || !occasionId) {
				return false;
			}

			var inputData = {
				'city_id': cityId,
				'occasion_id': occasionId,
				'map_id': productId,
				'map_type_id': productTypeId,
				'price': price,
				'price_worth': priceWorth,
				'product_price': productPrice,
				'transport_amount': transportAmount,
				'advance_amount': advanceAmount,
				'categories': categories,
				'deliverySlotId': deliverySlotId
			};

			if (!parentEl.hasClass('disabled')) {
				Vue.set(evibeApp, 'inputData', inputData);

				if (this.user !== '') {
					parentEl.addClass('disabled');

					/* check if we need to shortlist / delete shortlist */
					if (parentEl.hasClass('shortlistIcon')) {
						this.handleListPageAddShortlistOption(urlAdd, inputData, currentElement);
					} else if (parentEl.hasClass('shortlistedIcon')) {
						this.handleListPageDeleteShortlistOption(urlDelete, inputData, currentElement);
					} else {
						/* do nothing */
					}
				} else {
					event.target.parentElement.classList.add('disabled');
					var urladdTemp = self.api.tempShortlist;
					if (urladdTemp) {
						$.ajax({
							url: urladdTemp,
							type: 'POST',
							dataType: 'json',
							data: inputData,
							success: function (data, textStatus, jqXHR) {
								currentElement.parentElement.classList.remove("disabled");
								if (data.success) {
									self.userLogin();
								} else {
									self.userLogin();
									/*self.alertDialog('Sorry, an error has occurred, please try again later.');*/
								}
							},
							error: function (jqXHR, textStatus, errorThrown) {
								currentElement.parentElement.classList.remove("disabled");
								self.alertDialog('Sorry, an error has occurred, please try again later.');
								window.notifyTeam({
									"url": urladdTemp,
									"textStatus": textStatus,
									"errorThrown": errorThrown
								});
							}
						});
					}
				}
			}
		},

		handleListPageAddShortlistOption: function (urlAdd, inputData, currentElement) {
			var self = this;
			var parentEl = $(currentElement).parent();

			/* update UI */
			self.alertDialog('<i class="glyphicon glyphicon-ok"></i> Added to your cart successfully');
			self.updatePartyBag(1, self.inputData);
			self.updateListPageShortlistIcon(currentElement);
			self.updateListPageShortlistBtn(currentElement);

			/* make ajax */
			$.ajax({
				url: urlAdd,
				type: 'POST',
				dataType: 'json',
				data: inputData,
				success: function (data) {
					parentEl.removeClass("disabled");

					if (data.success) {
						self.updatePartyBagListData(1, data.shortlistOption);
						self.partyBagBudget = data.budget;
					} else {
						self.updatePartyBag(-1, self.inputData);
						self.updateListPageUnShortlistIcon(currentElement);
						self.updateListPageUnShortlistBtn(currentElement);
						self.alertDialog('Sorry, an error has occurred, please try again later.');
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					parentEl.removeClass("disabled");

					self.updatePartyBag(-1, self.inputData);
					self.updateListPageUnShortlistIcon(currentElement);
					self.updateListPageUnShortlistBtn(currentElement);
					self.alertDialog("Sorry, an error has occurred, please try again later.");

					window.notifyTeam({
						"url": urlAdd,
						"textStatus": textStatus,
						"errorThrown": errorThrown
					});
				}
			});
		},

		handleListPageDeleteShortlistOption: function (urlDelete, inputData, currentElement) {
			var self = this;
			var parentEl = $(currentElement).parent();

			self.updatePartyBag(-1, self.inputData);
			self.updateListPageUnShortlistIcon(currentElement);
			self.updateListPageUnShortlistBtn(currentElement);

			$.ajax({
				url: urlDelete,
				type: 'POST',
				dataType: 'json',
				data: inputData,
				success: function (data) {
					parentEl.removeClass("disabled");
					if (data.success) {
						self.updatePartyBagListData(-1, data.shortlistOption);
						self.partyBagBudget = data.budget;
					} else {
						self.updatePartyBag(1, self.inputData);
						self.updateListPageShortlistIcon(currentElement);
						self.updateListPageShortlistBtn(currentElement);
						self.alertDialog('Sorry, an error has occurred, please try again later.');
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					parentEl.removeClass("disabled");

					self.updatePartyBag(1, self.inputData);
					self.updateListPageShortlistIcon(currentElement);
					self.updateListPageShortlistBtn(currentElement);
					self.alertDialog("Sorry, an error has occurred, please try again later.");

					window.notifyTeam({
						"url": urlDelete,
						"textStatus": textStatus,
						"errorThrown": errorThrown
					});
				}
			});
		},

		/* shortlist option from profile page */
		shortlistOptionFromProfilePage: function () {
			var el = this.$el.querySelector('.hideShortlistData');
			var self = this;
			self.shortlistedElement = el;
			$('#shortlistModalContainer').empty();

			/* restore shortlist modal state */
			$('.shortlist-add-to-bag-container').removeClass('hide');
			$('.shortlist-clear-bag-container').addClass('hide');

			var cityId = el.getAttribute('data-cityid');
			var productId = el.getAttribute('data-mapid');
			var productTypeId = el.getAttribute('data-maptypeid');
			var occasionId = el.getAttribute('data-occasionid');

			var clearOptionsModal = false;
			var clearOptionsText = "";

			self.shortlistData = {
				'productId': productId,
				'productTypeId': productTypeId,
				'cityId': cityId,
				'occasionId': occasionId,
				'isProfile': 1
			};

			if (this.user !== '') {
				if (el.parentElement.lastChild.classList.contains("shortlist")) {
					var urladd = el.getAttribute('data-urladd');
				}
				if (el.parentElement.lastChild.classList.contains("shortlisted")) {
					var urldelete = el.getAttribute('data-urldelete');
				}

				if (urladd) {
					/* validate shortlist data */
					/* loose comparision done due to some unknown issue (different datatype) */
					if (self.pbCount) {
						if (self.cityId && self.shortlistData['cityId'] && (self.cityId != self.shortlistData['cityId'])) {
							clearOptionsModal = true;
							clearOptionsText = "Your party bag contains services which belong to other city. Do you wish to clear those services and add this service?"
						}

						if (self.occasionId && self.shortlistData['occasionId'] && (self.occasionId != self.shortlistData['occasionId'])) {
							clearOptionsModal = true;
							clearOptionsText = "Your party bag contains services which belong to other occasion. Do you wish to clear those services and add this service?"
						}
					}

					var urlShortlistModal = self.api.shortlistModal;
					if (urlShortlistModal) {
						$.ajax({
							url: urlShortlistModal,
							type: 'POST',
							dataType: 'html',
							data: self.shortlistData,
							success: function (data) {
								if (data) {
									$('#shortlistModalContainer').append(data);
								}

								if (clearOptionsModal) {
									$('.shortlist-add-to-bag-container').addClass('hide');
									$('.shortlist-clear-bag-msg').text(clearOptionsText);
									$('.shortlist-clear-bag-container').removeClass('hide');
								}

								if (!$('.shortlist-modal-field-wrap').length) {
									$('.shortlist-modal-container').addClass('hide');
								}

								if ($('.shortlist-modal-field-wrap').length || clearOptionsModal) {
									$('#shortlistModal').modal('show');
								} else {
									self.shortlistData['price'] = parseInt($('.shortlist-modal-container .total-price').data('price'), 10);
									self.shortlistData['priceWorth'] = parseInt($('.shortlist-modal-container .price-worth').data('price'), 10);
									self.shortlistData['productPrice'] = parseInt($('.shortlist-modal-container .price').data('price'), 10);
									self.shortlistData['transportAmount'] = parseInt($('.shortlist-modal-container .transport-price').data('price'), 10);
									self.shortlistData['advanceAmount'] = parseInt($('.shortlist-modal-container .advance-amount').data('price'), 10);
									self.shortlistData['categories'] = self.getSelectedData();
									self.shortlistData['deliverySlotId'] = $('.shortlist-modal-container #shortlistModalDeliverySlot').val();

									self.shortlistOptionWithDataFromProfilePage(self.shortlistedElement, self.shortlistData);
								}
							},
							error: function (jqXHR, textStatus, errorThrown) {
								self.alertDialog('Sorry, an error has occurred, please try again later.');
								self.alertDialog('expected.');
								window.notifyTeam({
									"url": urlShortlistModal,
									"textStatus": textStatus,
									"errorThrown": errorThrown
								});
							}
						});
					}
				}

				if (urldelete) {
					var inputData = {
						'city_id': cityId,
						'occasion_id': occasionId,
						'map_id': productId,
						'map_type_id': productTypeId
					};

					self.updatePartyBag(-1, self.inputData);
					self.updateUnShortlistBtn();
					$.ajax({
						url: urldelete,
						type: 'POST',
						dataType: 'json',
						data: inputData,
						success: function (data, textStatus, jqXHR) {
							el.parentElement.classList.remove("btn-disabled");
							if (data.success) {
								self.updatePartyBagListData(-1, data.shortlistOption);
								self.partyBagBudget = data.budget;
							} else {
								self.alertDialog('Sorry, an error has occurred, please try again later.');
							}
						},
						error: function (jqXHR, textStatus, errorThrown) {
							el.parentElement.classList.remove("btn-disabled");
							self.alertDialog("Sorry, an error has occurred, please try again later.");
							window.notifyTeam({
								"url": urldelete,
								"textStatus": textStatus,
								"errorThrown": errorThrown
							});
						}
					});
				}
			} else {
				/* @see: do not focus right now */
				/* store option basic data in local storage */
				/* trigger login modal */
				/* write some where to trigger shortlist modal after login */
				self.userLogin();
			}
		},

		shortlistOptionWithDataFromProfilePage: function (el, shortlistData) {
			var self = this;

			var productId = shortlistData['productId'] ? shortlistData['productId'] : false;
			var productTypeId = shortlistData['productTypeId'] ? shortlistData['productTypeId'] : false;
			var cityId = shortlistData['cityId'] ? shortlistData['cityId'] : false;
			var occasionId = shortlistData['occasionId'] ? shortlistData['occasionId'] : false;
			var price = shortlistData['price'] ? shortlistData['price'] : null;
			var priceWorth = shortlistData['priceWorth'] ? shortlistData['priceWorth'] : null;
			var productPrice = shortlistData['productPrice'] ? shortlistData['productPrice'] : null;
			var transportAmount = shortlistData['transportAmount'] ? shortlistData['transportAmount'] : null;
			var advanceAmount = shortlistData['advanceAmount'] ? shortlistData['advanceAmount'] : null;
			var categories = shortlistData['categories'] ? shortlistData['categories'] : [];
			var deliverySlotId = shortlistData['deliverySlotId'] ? shortlistData['deliverySlotId'] : null;

			var inputData = {
				'city_id': cityId,
				'occasion_id': occasionId,
				'map_id': productId,
				'map_type_id': productTypeId,
				'price': price,
				'price_worth': priceWorth,
				'product_price': productPrice,
				'transport_amount': transportAmount,
				'advance_amount': advanceAmount,
				'categories': categories,
				'delivery_slot_id': deliverySlotId
			};
			if (!el.parentElement.classList.contains('btn-disabled')) {
				Vue.set(evibeApp, 'inputData', inputData);

				if (this.user !== '') {
					el.parentElement.classList.add("btn-disabled");

					if (el.parentElement.lastChild.classList.contains("shortlist")) {
						var urladd = el.getAttribute('data-urladd');
					}
					if (el.parentElement.lastChild.classList.contains("shortlisted")) {
						var urldelete = el.getAttribute('data-urldelete');
					}

					if (urladd) {
						self.alertDialog('<i class="glyphicon glyphicon-ok"></i> Added to party bag successfully');
						self.updatePartyBag(1, self.inputData);
						self.updateShortlistBtn();
						$.ajax({
							url: urladd,
							type: 'POST',
							dataType: 'json',
							data: inputData,
							success: function (data, textStatus, jqXHR) {
								el.parentElement.classList.remove("btn-disabled");
								if (data.success) {
									self.updatePartyBagListData(1, data.shortlistOption);
									self.partyBagBudget = data.budget;
								} else {
									self.alertDialog('Sorry, an error has occurred, please try again later.');
								}
							},
							error: function (jqXHR, textStatus, errorThrown) {
								el.parentElement.classList.remove("btn-disabled");
								self.alertDialog("Sorry, an error has occurred, please try again later.");
								window.notifyTeam({
									"url": urladd,
									"textStatus": textStatus,
									"errorThrown": errorThrown
								});
							}
						});
					}

					if (urldelete) {
						self.updatePartyBag(-1, self.inputData);
						self.updateUnShortlistBtn();
						$.ajax({
							url: urldelete,
							type: 'POST',
							dataType: 'json',
							data: inputData,
							success: function (data, textStatus, jqXHR) {
								el.parentElement.classList.remove("btn-disabled");
								if (data.success) {
									self.updatePartyBagListData(-1, data.shortlistOption);
									self.partyBagBudget = data.budget;
								} else {
									self.alertDialog('Sorry, an error has occurred, please try again later.');
								}
							},
							error: function (jqXHR, textStatus, errorThrown) {
								el.parentElement.classList.remove("btn-disabled");
								self.alertDialog("Sorry, an error has occurred, please try again later.");
								window.notifyTeam({
									"url": urldelete,
									"textStatus": textStatus,
									"errorThrown": errorThrown
								});
							}
						});
					}
				} else {
					el.parentElement.classList.add("btn-disabled");
					if (typeof (Storage) !== "undefined") {
						localStorage.setItem("inputData", JSON.stringify(this.inputData));
					}

					var urladdTemp = self.api.tempShortlist;
					if (urladdTemp) {
						$.ajax({
							url: urladdTemp,
							type: 'POST',
							dataType: 'json',
							data: inputData,
							success: function (data, textStatus, jqXHR) {
								el.parentElement.classList.remove("btn-disabled");
								if (data.success) {
									self.userLogin();
								} else {
									self.userLogin();
									/*self.alertDialog('Sorry, an error has occurred, please try again later.');*/
								}
							},
							error: function (jqXHR, textStatus, errorThrown) {
								el.parentElement.classList.remove("btn-disabled");
								self.alertDialog('Sorry, an error has occurred, please try again later.');
								window.notifyTeam({
									"url": urladdTemp,
									"textStatus": textStatus,
									"errorThrown": errorThrown
								});
							}
						});
					}
				}
			}
		},

		/* show alert dialog */
		alertDialog: function (msg) {
			var alertElement = document.createElement('div');
			alertElement.setAttribute("class", "shortlist-alert");
			alertElement.innerHTML = msg;
			setTimeout(function () {
				alertElement.parentNode.removeChild(alertElement);
			}, 1500);
			document.body.appendChild(alertElement);
		},

		/* update shortlist button in profile page */
		updateShortlistBtn: function () {
			var el = this.$el.querySelector('.hideShortlistData');
			el.parentElement.lastChild.innerHTML = "Remove";
			el.parentElement.classList.add("product-btn-shortlist-remove");
			el.parentElement.lastChild.classList.add("shortlisted");
			el.parentElement.lastChild.classList.remove("shortlist");

			/* update the image for the remove button */
			var removeimg = el.getAttribute('data-removeimggrey');
			$(el).parent().find('img').attr('src', removeimg);
		},

		/* update un-shortlist button in profile page */
		updateUnShortlistBtn: function () {
			var el = this.$el.querySelector('.hideShortlistData');
			el.parentElement.lastChild.innerHTML = "Add To Cart";
			el.parentElement.classList.remove("product-btn-shortlist-remove");
			el.parentElement.lastChild.classList.add("shortlist");
			el.parentElement.lastChild.classList.remove("shortlisted");

			/* update the image for the shortlist button */
			var shortlistimg = el.getAttribute('data-plainimgwhite');
			$(el).parent().find('img').attr('src', shortlistimg);
		},

		/* update detail page after shortlist / un-shortlist option */
		updateDetailPage: function () {
			var detailPageElement = this.$el.querySelector('.hideShortlistData');
			if (detailPageElement) {
				var city_id = detailPageElement.getAttribute('data-cityid');
				var map_id = detailPageElement.getAttribute('data-mapid');
				var map_type_id = detailPageElement.getAttribute('data-maptypeid');
				var occasion_id = detailPageElement.getAttribute('data-occasionid');

				var currentOption = {
					'city_id': city_id,
					'map_id': map_id,
					'map_type_id': map_type_id,
					'occasion_id': occasion_id
				};

				Vue.set(evibeApp, 'currentOption', currentOption);

				if (this.compareCurrentOption()) {
					this.updateShortlistBtn();
				} else {
					this.updateUnShortlistBtn();
				}
			}
		},

		compareCurrentOption: function () {
			for (partyBagOption in this.partyBagListData) {
				if (this.currentOption.city_id == this.partyBagListData[partyBagOption].cityId &&
					this.currentOption.occasion_id == this.partyBagListData[partyBagOption].occasionId &&
					this.currentOption.map_type_id == this.partyBagListData[partyBagOption].mapTypeId &&
					this.currentOption.map_id == this.partyBagListData[partyBagOption].mapId) {
					return true;
				}
			}
			return false;
		},

		updateListPages: function () {
			var self = this;

			$('.hideShortlistResultsData').each(function (key, el) {
				var cityId = $(el).data('cityid') ? $(el).data('cityid') : false;
				var occasionId = $(el).data('occasionid') ? $(el).data('occasionid') : false;
				var productTypeId = $(el).data('maptypeid') ? $(el).data('maptypeid') : false;
				var productId = $(el).data('mapid') ? $(el).data('mapid') : false;

				if (!cityId || !occasionId || !productTypeId || !productId) {
					return false;
				}

				var currentOption = {
					'city_id': cityId,
					'occasion_id': occasionId,
					'map_type_id': productTypeId,
					'map_id': productId
				};

				Vue.set(evibeApp, 'currentOption', currentOption);
				if (self.compareCurrentOption()) {
					self.updateListPageShortlistIcon(el);
					self.updateListPageShortlistBtn(el);
				} else {
					self.updateListPageUnShortlistIcon(el);
					self.updateListPageUnShortlistBtn(el);
				}
			});
		},

		updateListPageShortlistBtn: function (el) {
			if (el.classList.contains('list-cta-btn')) {
				el.innerHTML = "Remove";
				el.classList.add("list-btn-un-shortlist");
			}
		},

		updateListPageUnShortlistBtn: function (el) {
			if (el.classList.contains('list-cta-btn')) {
				el.innerHTML = "Add To Cart";
				el.classList.remove("list-btn-un-shortlist");
			}
		},

		updateListPageShortlistIcon: function (el) {
			var parentEl = $(el).parent();
			var filledImg = parentEl.data('filledimg');

			parentEl.addClass('shortlistedIcon')
				.removeClass('shortlistIcon')
				.attr('src', filledImg);
		},

		updateListPageUnShortlistIcon: function (el) {
			var parentEl = $(el).parent();

			var plainImg = parentEl.data('plainimg');

			parentEl.addClass('shortlistIcon')
				.removeClass('shortlistedIcon')
				.attr('src', plainImg);
		},

		/* showing the approximate budget to the user on enquiry */
		calculateBudget: function () {
			/* already in the party bag */
			var options = $(document).find('.item-result');
			var budget = 0;
			for (var i = 0; i < options.length; i++) {
				if (!$(options[i]).find('.shortlisted-label').hasClass('hide')) {
					var price = 0;
					if ($(options[i]).find('.card-price').length > 0) {
						price = parseInt($(options[i]).find('.card-price').text().trim().slice(2).replace(',', ''));
						budget = budget + price;
					}
				}
			}
			return budget;
		},

		/* submit details in party bag page for enquiry */
		submitEnquiry: function (event) {
			event.preventDefault();

			/* ajax call */
			var self = this;
			var form = $(event.target).parents('form');
			var enquireUrl = form.attr('action');
			var partyLocation = form.find('input[name="pbEnqPartyLocation"]').val();
			var phone = form.find('input[name="pbEnqPhone"]').val();
			var eventDate = form.find('input[name="pbEnqEventDate"]').val();

			/* budget should be calculated from the options selected and displayed to the user */
			var budget = self.calculateBudget();

			var inputData = {
				'partyLocation': partyLocation,
				'phone': phone,
				'eventDate': eventDate,
				'budget': budget
			};

			if (enquireUrl) {
				$.ajax({
					url: enquireUrl,
					type: 'POST',
					dataType: 'json',
					data: inputData,
					success: function (data, textStatus, jqXHR) {
						if (data.success) { /* true */
							/* get the updated shortlistoption details from data.messages */
							/* update the options available in the page */
							var successMsg = "We have received your enquiry. We will get in touch with you shortly.";
							$('.partybag-alert-success').html(successMsg);
							$('.partybag-alert-success').removeClass('hide');
							$('#partyBagEnquiryForm').addClass('hide');
						} else { /* false */
							if (data.messages) {
								self.updateEnquiryErrorResponse(form, '<span class="glyphicon glyphicon-alert"></span>' + data.messages[0]);
							} else {
								self.updateEnquiryErrorResponse(form, '<span class="glyphicon glyphicon-alert"></span>' + "Sorry, an error has occcured, please try again later.");
							}
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						self.alertDialog("Sorry, an error has occurred, please try again later.");
						window.notifyTeam({
							"url": enquireUrl,
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			}
		},

		/* error response from enquiry */
		updateEnquiryErrorResponse: function (form, message) {
			$(form).find('.errors-cnt').html(message);
			$(form).find('.errors-cnt').removeClass('hide');
		},

		isSelected: function (cardSelected) {
			if (cardSelected === 1) {
				return 'selected-shortlist';
			} else {
				return 'unselected-shortlist';
			}
		},

		formatPrice: function (price) {
			var formattedPrice = "";

			if (price !== undefined) {
				if (price.toString().length > 3) {
					if (price.toString().length > 5) {
						formattedPrice = (price / 100000).toFixed(2) + 'L';
					} else {
						formattedPrice = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					}
				} else {
					formattedPrice = price;
				}
			} else {
				formattedPrice = 0;
			}

			return formattedPrice;
		},

		partyBagCheckout: function () {
			var checkoutUrl = '/my/party-bag/checkout';
			window.location.href = checkoutUrl;
		}
	}
});

/* Disabling it in live, client shouldn't have access to view the data @todo: Should enable it while development */
Vue.config.devtools = $('#vueJsAppDebug').val();