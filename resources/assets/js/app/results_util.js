$(document).ready(function () {

	var removePageFromUrl = formUrl(null, "page", null, true);

	(function initFunctions() {
		var contactDetails = $('.contact-details-card');
		if (contactDetails.length) {
			$(".evibe-guarantee-wrap").after(contactDetails.clone());
			$(".evibe-guarantee-wrap").next().removeClass("hide");
			$(".contact-details-card-enquire-now").on("click", function () {
				$("#modalAutoPopup").modal("show");
			});
		}
	})();

	$.getScript("/js/util/lazyLoadImages.js", function () {
		$(".package-list-profile-image").removeClass("lazy-loading");
		new LazyLoad();
	});

	if ($(window).width() >= 479) {
		$(".item-result .img-wrap a").attr('target', '_blank');
		$(".item-result .title a").attr('target', '_blank');
		$(".list-option-image-wrap a").attr('target', '_blank');
		$(".list-option-title a").attr('target', '_blank');
	}

	function setCatLinks() {
		var url = "";
		/* iterate on each set and set values */
		$.each($(".link-filter"), function (i, item) {

			/* get category name */
			var catName = $(item).data('type');
			catName = catName ? catName : "category";

			$(item).find(".cat-link").each(function (i, item) {
				url = $(item).data("url");
				$(item).attr("href", formUrl(removePageFromUrl, catName, url));
			});

			var allCatsUrl = removePageFromUrl;
			allCatsUrl = formUrl(allCatsUrl, catName, "all");
			$(item).find(".all-cats").attr("href", allCatsUrl);
		});
	}

	/* show more/less location for venue-deals */
	function initLocationFiltersVisibility() {
		if ($('.more-location').length > 0) {
			$('#showMore').removeClass('hide');
			$('#showLess').addClass('hide');
		}
	}

	/* function to get url params */
	var getUrlParameter = function getUrlParameter(sParam) {
		var sPageURL = window.location.search.substring(1),
			sURLVariables = sPageURL.split('&'),
			sParameterName,
			i;

		for (i = 0; i < sURLVariables.length; i++) {
			sParameterName = sURLVariables[i].split('=');

			if (sParameterName[0] === sParam) {
				return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
			}
		}
	};

	/* Category filter */
	var categoryFilter = $(".category-label");
	if (categoryFilter.length > 0) {
		categoryFilter.on('mouseup', function () {
			setTimeout(function () {
				var $filter = "1";
				$('.category-input').each(function () {
					$filter = this.checked ? $filter + "__" + $(this).val() : $filter;
				});

				var url = ($filter == 1) ? $("#categoryFilterUrl").val().replace("c/replaceFilters", "all") : $("#categoryFilterUrl").val().replace("replaceFilters", $filter).replace("1__", "");

				url += window.location.search;

				/* remove pagination in any exists as the category has been changed */
				var pageNumber = getUrlParameter('page');
				if (pageNumber) {
					url = url.replace("page=" + pageNumber, "");
				}

				refreshUrl(url);
			}, 201);
		});
	}

	/* Theme filter */
	var themeLabel = $(".theme-label");
	if (themeLabel.length > 0) {
		themeLabel.on('mouseup', function () {
			setTimeout(function () {
				var $filter = "1";
				$('.theme-input').each(function () {
					$filter = this.checked ? $filter + "__" + $(this).val() : $filter;
				});

				$filter = ($filter == 1) ? formUrl(removePageFromUrl, "theme", null, true) : formUrl(removePageFromUrl, 'theme', $filter.replace("1__", ""));
				refreshUrl($filter);
			}, 201);
		});
	}

	/* Place filter */
	var placeTypeLabel = $(".venue-type-label");
	if (placeTypeLabel.length > 0) {
		placeTypeLabel.on('mouseup', function () {
			setTimeout(function () {
				var $filter = "1";
				$('.venue-type-input').each(function () {
					$filter = this.checked ? $filter + "__" + $(this).val() : $filter;
				});

				$filter = ($filter == 1) ? formUrl(removePageFromUrl, "venue-type", null, true) : formUrl(removePageFromUrl, 'venue-type', $filter.replace("1__", ""));
				refreshUrl($filter);
			}, 201);
		});
	}

	/* Gender filter */
	var genderLabel = $(".gender-label");
	if (genderLabel.length > 0) {
		genderLabel.on('mouseup', function () {
			setTimeout(function () {
				var $filter = "1";
				$('.gender-input').each(function () {
					$filter = this.checked ? $filter + "__" + $(this).val() : $filter;
				});

				$filter = ($filter == 1) ? formUrl(removePageFromUrl, "gender", null, true) : formUrl(removePageFromUrl, 'gender', $filter.replace("1__", ""));
				refreshUrl($filter);
			}, 201);
		});
	}

	/* Removing loading model once filter clicks are triggered */
	$(".loader-filter-list").remove();
	$(".loader-filter-list-wrap").removeClass("hide");

	/* allow only number */
	$(".price-input").keydown(function (e) {
		/* Allow: backspace, delete, tab, escape, enter */
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
			(e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) || /* Allow: Ctrl+A, Command+A */
			(e.keyCode >= 35 && e.keyCode <= 40)) { /* Allow: home, end, left, right, down, up */
			return;
			/* let it happen, don't do anything */
		}
		/* Ensure that it is a number and stop the keypress */
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});

	/* price filter */
	$("#filterPriceBtn").click(function (event) {
		event.preventDefault();
		var priceMin = parseInt($("#priceMin").val(), 10);
		var priceMax = parseInt($("#priceMax").val(), 10);
		if (priceMax < priceMin || !priceMin) {
			showNotyError("Please enter valid price range.");
		} else {
			refreshUrl(formUrl(formUrl(removePageFromUrl, 'price_min', priceMin), 'price_max', priceMax));
		}
	});

	/* price category filter */
	$(".price-category-option").click(function (event) {
		event.preventDefault();

		var priceCatMin = $(this).data('price-cat-min');
		var priceCatMax = $(this).data('price-cat-max');

		refreshUrl(formUrl(formUrl(removePageFromUrl, 'price_min', priceCatMin), 'price_max', priceCatMax));
	});

	/* price input filter */
	$('.price-filter-submit-btn').on('click', function (event) {
		event.preventDefault();

		var priceCatMin = $('.price-filter-input-min-price').val();
		var priceCatMax = $('.price-filter-input-max-price').val();

		priceCatMin = $.isNumeric(priceCatMin) ? priceCatMin : "";
		priceCatMax = $.isNumeric(priceCatMax) ? priceCatMax : "";

		refreshUrl(formUrl(formUrl(removePageFromUrl, 'price_min', priceCatMin), 'price_max', priceCatMax));
	});

	/* location filter */
	$(".location-filter-option").click(function (event) {
		event.preventDefault();
		var locationUrl = $(this).data('location-url');
		refreshUrl(formUrl(removePageFromUrl, 'location', locationUrl));
	});

	/* guests count filter */
	$('#filterGuestsBtn').click(function (event) {
		event.preventDefault();
		var guestsMin = parseInt($("#guestsMin").val(), 10);
		var guestsMax = parseInt($("#guestsMax").val(), 10);
		if (guestsMax < guestsMin || !guestsMin) return false;
		refreshUrl(formUrl(formUrl(removePageFromUrl, 'guests_min', guestsMin), 'guests_max', guestsMax));
	});

	/* search filter */
	$("#filterSearchBtn").click(function (event) {
		event.preventDefault();
		var $search = $("#searchInput");
		var searchVal = $search.val();
		var existingSearch = $search.data('old');
		if (searchVal && searchVal != existingSearch) {
			refreshUrl(formUrl(removePageFromUrl, 'search', searchVal));
		}
	});

	/* search on pressing enter key */
	$('#searchInput').keyup(function (e) {
		var existingSearch = $(this).data('old');
		var searchVal = $(this).val();
		if (e.keyCode == 13 && searchVal && searchVal != existingSearch) {
			refreshUrl(formUrl(removePageFromUrl, 'search', searchVal));
		}
	});

	$('#showMore').click(function (event) {
		event.preventDefault();
		$('.more-location').removeClass('hide');
		$(this).addClass('hide');
		$('#showLess').removeClass('hide');
	});

	$('#showLess').click(function (event) {
		event.preventDefault();
		$('.more-location').addClass('hide');
		$('#showMore').removeClass('hide');
		$(this).addClass('hide');
	});

	/* sort options */
	$('.sort-option').click(function (event) {
		event.preventDefault();
		refreshUrl(formUrl('', 'sort', $(this).data('value')));
	});

	/* rating stars */
	$('.provider-avg-rating').rating({
		showClear: 0,
		readonly: 'true',
		showCaption: 0,
		size: 's',
		min: 0,
		max: 5,
		step: 0.1
	});
	$('.provider-avg-rating, .rating-wrap').removeClass('hide');

	setCatLinks();
	/* set links */
	initLocationFiltersVisibility();
});