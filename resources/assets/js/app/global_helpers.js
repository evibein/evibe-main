/**
 * @author Anji
 * @since 25 OCt 2016
 *
 * All the global helper methods that will be used in
 * other modules.
 */

var getQueryParams = function (a) {
	if (a == "") return {};
	var b = {};
	for (var i = 0; i < a.length; ++i) {
		var p = a[i].split('=');
		if (p.length != 2) continue;
		b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
	}
	return b;
};

var formUrl = function (cUrl, key, value, isIgnore) {
	cUrl = cUrl ? cUrl.substr(cUrl.indexOf('?') + 1).split('&') : window.location.search.substr(1).split('&');
	var cParams = getQueryParams(cUrl), queryString = "", loc = window.location;
	var url = loc.protocol + '//' + loc.host + loc.pathname;
	if (isIgnore) { /* remove from current object */
		if (cParams.hasOwnProperty(key)) {
			try {
				delete cParams[key];
			} catch (e) {
			}
		}
	} else {
		cParams[key] = value;
	}
	for (key in cParams) { /* form string */
		queryString += key + "=" + encodeURIComponent(cParams[key]) + "&";
	}
	if (queryString) {
		url += '?' + queryString;
		url = url.slice(0, -1); /* remove trailing '&' */
	}
	return url;
};

var refreshUrl = function (url) {
	window.location.href = url;
};