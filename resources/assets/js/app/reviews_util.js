(function () {

	function applyRatingStar(to) {
		$(to).rating({
			showClear: 0,
			readonly: 'true',
			showCaption: 0,
			size: 'xl',
			min: 0,
			max: 5,
			step: 0.1
		});
		$(to).removeClass('hide');
	}

	$(".link-read").click(function (event) {
		event.preventDefault();
		$(this).parents(".r-txt-wrap")
			.find(".text-extras")
			.toggleClass("hide");
	});

	$("#rSortOptions").change(function (event) {
		console.log("here from util js file");
		event.preventDefault();
		var sortClass = "." + $(this).val();
		if ($(sortClass).length) {
			$(".r-review").addClass("hide");
			$(sortClass).removeClass("hide");
		}
	});

	applyRatingStar('.avg-rating, .review-rating');
	applyRatingStar('.provider-avg-rating');

})();
