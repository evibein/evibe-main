$(document).ready(function () {
	$(window).load(function () {
		(function runMansoryOnLoadedImages() {
			if ($("#inspirations-gallery").length) {
				$.getScript("/js/app/pt.js", function () {
					var $grid = $('#inspirations-gallery').masonry({
						itemSelector: '.hero-grid__item',
						percentPosition: true,
						horizontalOrder: true

					});

					$grid.imagesLoaded(function () {
						$grid.masonry();

					});
					$(".page-description-wrap").removeClass("hide");
					$("#inspirations-gallery").removeClass("hide");
					$(".loading-text").hide();
				});
			}
		})();

		if (typeof loadCustomerImages !== "undefined") {
			window.loadCustomerImages();
		}

		/* Show all cities in change city modal */
		(function showCitiesInModal() {
			$.getScript("/js/citiesList.js", function () {
				$(".btn-view-all-cities").on("click", function () {
					var cityList = cities;
					$(".all-cities-wrap div").removeClass("hide");
					for (var i = 0; i < cityList.length; i++) {
						$(".all-cities-wrap").append("<div class='in-blk pad-l-15 mar-b-10 city-modal-other-links'><a href='/" + (cityList[i].toLowerCase()).replace(" ", "-") + "' class='a-no-decoration-black'>" + cityList[i] + "</a></div>");
					}
					$(".btn-view-all-cities").hide();
				});
			});
		})();

		/* Loading Variations after window load & remove loading */
		(function loadVariationsProfile() {
			var profileVariationsSection = $(".variation-selection-wrap");
			if (profileVariationsSection.length > 0) {
				var i = 0;
				var variationsData = {};

				$(".variation-selection-img-wrap").each(function () {
					variationsData[i] = {"mapId": $(this).data("map-id"), "mapTypeId": $(this).data("map-type-id")};
					i++;
				});

				$.ajax({
					url: "/variations/data",
					type: "POST",
					data: {"variationsData": variationsData, "variationId": $("#optionVariationId").val()},
					success: function (data) {
						if (data.success) {
							$.each(data.variationsOptionsData, function ($key, $value) {
								$("." + $key).attr("href", $value.url)
									.find("img").attr("src", $value.image);
							});

							$(".variation-selection-img-loader").remove();
							profileVariationsSection.removeClass("hide");
						} else {
							$(".variation-selection-img-loader").remove();
							$(".variation-wrap").remove();
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						$(".variation-selection-img-loader").remove();
						$(".variation-wrap").remove();

						window.notifyTeam({
							"url": "/variations/data",
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			}
		})();

		/* Additional fields for each booking in TMO page */
		(function tmoAdditionalFields() {
			if ($(".normal-booking-order-details").length > 0) {
				var i = 0;
				var bookingIds = [];

				$(".normal-booking-order-details").each(function () {
					bookingIds[i] = $(this).data("ticket-booking-id");
					i++;
				});

				$.ajax({
					url: "/track/request/checkout-fields",
					type: "POST",
					data: {"bookingIds": bookingIds},
					success: function (data) {
						if (data.success) {
							var checkoutFields = data.checkoutFields;

							$(".normal-booking-order-details").each(function () {
								var checkoutField = checkoutFields[$(this).data("ticket-booking-id")];
								if (checkoutField) {
									var template = "<table class='table table-bordered'>";
									for (var key in checkoutField) {
										template += "<tr><td>" + key + "</td><td>" + checkoutField[key] + "</td>" + "</tr>"
									}
									template += "</table>";

									$(".order-details-" + $(this).data("ticket-booking-id")).append(template);
								}
							});
						} else if (data.error) {
							showNotyError(data.error);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						window.showNotyError("An error occurred while submitting your request. Please try again later.");
						window.notifyTeam({
							"url": "track/request/checkout-fields",
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			}
		})();

		/* Load user browsing history & reload it with */
		(function loadUserBrowsingData() {
			var $current_url = $(location).attr('pathname');
			var pageUrl = window.location.href;
			var urlsToBeRemoved = [
				'/my/browsing-history', '/terms', '/privacy', '/refunds', '/about', '/how-it-works', '/faqs', '/feedback', '/track', '/vendor/signup'
			];

			if (!((urlsToBeRemoved.indexOf($current_url) !== -1) || (pageUrl.search("/party-kits") !== -1) || (pageUrl.search("/track/") !== -1) || (pageUrl.search("/i/") !== -1) || (pageUrl.search("/e-cards/") !== -1) || (pageUrl.search("/u/") !== -1) || (pageUrl.search("/corporate-events")))) {
				$.ajax({
					url: '/user-browse-history/data',
					dataType: 'html',
					type: 'POST',
					success: function (data) {
						$('<div class="bg-white-imp"><div class="browsing-history-list pad-l-15 pad-r-15">' + data + '</div></div>').insertBefore(".footer-wrap:first");
					},
					error: function (jqXHR, textStatus, errorThrown) {
						window.notifyTeam({
							"url": '/user-browse-history/data',
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			}
		})();

		/* Lazy loading invite carousel images*/
		(function lazyLoadInviteImages() {
			if ($(".lazy-loading-invite-carousel-img").length > 0) {
				$('.lazy-loading-invite-carousel-img').each(function () {
					var img = $(this);
					img.attr('src', img.data('src'));
				});
			}
		})();

		/* Lazy loading invite carousel images*/
		(function removeLoadingScreen() {
			if ($(".loading-screen-gif").length > 0) {
				window.onbeforeunload = function () {
					$(".loading-screen-gif").css("display", "table");
				};
			}
		})();

		(function updateGoogleClientIdInDB() {
			if (window.location.href.indexOf("auto-book/now/checkout") > -1 || window.location.href.indexOf("pay/final-checkout") > -1) {
				var ticketId = $('#ticketId').val();
				if (typeof ga != "undefined") {
					var googleClientId = ga.getAll()[0].get('clientId');
					if (typeof googleClientId != "undefined") {
						var url = "/pay/update/google-client-id";

						$.ajax({
							url: url,
							type: "POST",
							async: true,
							data: {ticketId: ticketId, googleClientId: googleClientId},
							success: function (data) {
							},
							error: function (jqXHR, textStatus, errorThrown) {
								window.notifyTeam({
									"url": url,
									"textStatus": textStatus,
									"errorThrown": errorThrown
								});
							}
						});
					}
				}
			}
		})();

		(function iOSBackButton() {
			if ($("#headerBackButton").length > 0) {
				$('#headerBackButton').on('click', function (e) {
					e.preventDefault();
					if (document.referrer === "") {
						window.location.href = "https://evibe.in";
					} else {
						window.history.back();
					}
				});
			}
		})();

		(function customLazyLoading() {
			if ($(".lazyload-listpage-image").length > 0) {
				$('.lazyload-listpage-image').each(function () {
					$(this).empty().append('<img class="list-option-image" style="z-index:3;position:absolute;left:50%;transform:translate(-50%)" src="' + $(this).data("profile-url") + '" alt="' + $(this).data("image-alt-text") + '" onError="this.src=\'https://gallery.evibe.in/img/icons/gift.png\'; this.style=\'padding: 30px 60px\'">');
					$(this).append('<img class="list-option-image" style="z-index:2;position:absolute;filter:blur(5px);left:0;height:auto" src="' + $(this).data("profile-url") + '" alt="' + $(this).data("image-alt-text") + '" onError="this.src=\'https://gallery.evibe.in/img/icons/gift.png\'; this.style=\'padding: 30px 60px\'">');
				});
			}
		})();
			(function customLazyLoadingForVirtualPage() {
				
				$sectionWrapBg = "url('https://gallery.evibe.in/main/img/orange-yellow-bg.jpg')";
				$(".section-wrap-bg").css('background-image',$sectionWrapBg);

				if ($(".vp-image").length > 0) {
					$('.vp-image').each(function () {
						$src = $(this).data('src');
						$(this).attr('src',$src);
					});
				}
				
				if($(".add-bg-img").length > 0){
					$('.add-bg-img').each(function () {
							$src = "url('" + $(this).data('src') + "')";
							$(this).css('background-image',$src);

					});
					

				}

				if($(".youtube-iframe").length > 0){
					$(".youtube-video-wrap-desk").append("<iframe class=\"video-thumb\" height=\"215\" width=\"385\"  src=\"https://www.youtube-nocookie.com/embed/UJH7xeHbav4\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>")
					$(".youtube-video-wrap-mbl").append("<iframe class=\"video-thumb\" style=\"width:100% !important;height:190px !important;margin-bottom:30px\"  src=\"https://www.youtube-nocookie.com/embed/UJH7xeHbav4\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>")
				}
				

		})();

	});
});