function reportError(data) {
	var xhttp = new XMLHttpRequest();
	xhttp.open("POST", "/report/notify/error", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send(data);
}

if (typeof jQuery == 'undefined') {
	reportError("url=&textStatus=Jquery load error&errorThrown=Jquery not loaded while loading the offer model");
}

if (typeof Cookies == "undefined") {
	var jQueryScript = document.createElement('script');
	jQueryScript.setAttribute('src', 'https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js');
	document.head.appendChild(jQueryScript);
}

if (typeof datetimepicker == "undefined") {
	var jQueryScript = document.createElement('script');
	jQueryScript.setAttribute('src', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js');
	document.head.appendChild(jQueryScript);
}

var vars = [], hash;
var q = document.URL.split('?')[1];
if (document.URL.split('?')[0].indexOf("land-All") > 0) {
	vars.ref = "inorg";
} else if (q !== undefined) {
	q = q.split('&');
	for (var i = 0; i < q.length; i++) {
		hash = q[i].split('=');
		vars.push(hash[1]);
		vars[hash[0]] = hash[1];
	}
}

/* @see: need to show all for PIAB pages
 if (((vars.ofid !== undefined) && (vars.oftn !== undefined) && (vars.ofid.length > 0) && (vars.oftn.length > 0)) || (vars.ref == "inorg")) {

 }
 */

$.ajax({
	url: '/of/landing/validate/piab',
	type: "POST",
	tryCount: 0,
	retryLimit: 3,
	data: {"ofId": vars.ofid, "ofTn": vars.oftn, "ref": vars.ref, "isPiab": 1},
	success: function (data) {
		$('body').append(data);

		var biddingModal = $('#modalOfferLandingSignUp');
		var isClosed = 1;
		$(".offer-landing-party-date").datetimepicker({
			"timepicker": false,
			"minDate": 0,
			"format": 'Y/m/d',
			"scrollInput": false,
			"scrollMonth": false,
			"scrollTime": false,
			"closeOnDateSelect": true,
			"disabledDates": window.disableDates(),
			onSelectDate: function (ct, $i) {
				$i.parent().addClass("is-dirty");
			}
		});

		var $offerCloseCookieCount = 0;

		function triggerOfferModal() {
			var SignUpSubmitCookie = parseInt(Cookies.get('offerSignUpSubmitPIAB'), 10);
			var SignUpCloseCookie = parseInt(Cookies.get('offerSignUpClosePIAB'), 10);

			if (($('.modal.in').length === 0) && (SignUpSubmitCookie !== 2) && (SignUpCloseCookie !== 2)) {
				biddingModal.modal({
					'show': true,
					'backdrop': 'static',
					'keyboard': false
				});
			}
		}

		triggerOfferModal();

		$('.offer-signup-close-btn').on('click', function () {
			var endOfTheDay = new Date(new Date().setHours(23, 59, 59, 999));
			isClosed = 2;
			$offerCloseCookieCount++;
			Cookies.set('offerSignUpClosePIAB', $offerCloseCookieCount, {expires: endOfTheDay});

			setTimeout(function () {
				triggerOfferModal();
			}, 10000);
		});

		$('.offer-signup-submit-btn').on('click', function (e) {
			e.preventDefault();
			disableSubmitButton();

			var formUrl = $(this).data('url');

			var data = {
				"offerLandingSignUpPhone": $('.offer-landing-signup-phone').val(),
				"sourceUrl": window.location.href,
				"partyDate": $('.offer-landing-party-date').val(),
				"ofId": vars.ofid,
				"ofTn": vars.oftn,
				"ref": vars.ref,
				"isPiab": "1"
			};

			$.ajax({
				url: formUrl,
				type: "POST",
				data: data,
				success: function (data) {
					if (data.success) {
						$("#modalOfferLandingSignUp").find(".col-xs-12").empty();
						$(".offer-signup-thankyou").removeClass("hide");
						$(".offer-landing-coupon-code").text(data.referralCode);
						Cookies.set('offerSignUpSubmitPIAB', '2', {expires: 40});
					} else {
						showSubmitButton();
						window.showNotyError(data.errorMsg);
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					showSubmitButton();
					window.showNotyError("An error occurred while submitting your request. Please try again later.");
					reportError("url=" + formUrl + "&textStatus=" + textStatus + "&errorThrown=" + errorThrown);
				}
			});
		});

		function showSubmitButton() {
			$('.offer-signup-submit-btn').removeClass('hide');
			$('.submitting-offer-signup').addClass('hide');
		}

		function disableSubmitButton() {
			$('.offer-signup-submit-btn').addClass('hide');
			$('.submitting-offer-signup').removeClass('hide');
		}
	},
	error: function (jqXHR, textStatus, errorThrown) {
		this.tryCount++;
		if (this.tryCount <= this.retryLimit) {
			$.ajax(this);
		} else {
			reportError("url=of-landing-validate&textStatus=" + textStatus + "&errorThrown=" + errorThrown);
		}
	}
});