$(document).ready(function () {

	if ($(".loading-screen-gif").length > 0) {
		$(".loading-screen-gif").css("display", "none");
	}

	/*Customer images for profile pages*/
	window.loadCustomerImages = function () {
		if ($('#reviewSection').length > 0) {
			var currentImage, selector, counter = $("#imagecounter").val();
			$('#show-next-image, #show-previous-image').click(function () {
				if ($(this).attr('id') === 'show-previous-image') {
					currentImage--;
				} else {
					currentImage++;
				}
				selector = $('[data-image-id="' + currentImage + '"]');
				updateGallery(selector);
			});

			function updateGallery(selector) {
				var $sel = selector;
				currentImage = $sel.data('image-id');
				$('#image-gallery-title').text($sel.data('title'));
				$('#image-gallery-image').attr('src', $sel.data('image'));
				disableButtons(counter, $sel.data('image-id'));
			}

			function disableButtons(counterMax, counterCurrent) {
				$('#show-previous-image, #show-next-image').show();
				if (counterMax == counterCurrent) {
					$('#show-next-image').hide();
				}
				if (counterCurrent == 1) {
					$('#show-previous-image').hide();
				}
			}

			$(".thumbnail").on('click', function () {
				$("#image-library").modal("hide");
				updateGallery($(this));
			});

			$(".view-gallery-btn").on("click", function () {
				$('#image-gallery').modal('hide');
				$('#image-library').modal("toggle");
			});
		}
	};
	var pageUrl = window.location.href;

	(function initModals() {
		$("#modalLoading, #modalRecoSuccess").modal({
			keyboard: false,
			backdrop: "static",
			show: false
		});

		$("#waitingModal").modal({
			backdrop: "static",
			keyboard: false,
			show: false
		});

		if (pageUrl.indexOf('#bookSiteVisitModal') !== -1) {
			$('#modalBookFreeSiteVisit').modal('show');
		}
	})();

	(function globalFunctions() {

		window.addToGTMDataLayer = function (gtmDataObj) {
			window.dataLayer = window.dataLayer || [];
			dataLayer.push(gtmDataObj);
		};

		window.showLoading = function () {
			$("#modalLoading").modal("show");
		};

		window.hideLoading = function () {
			$("#modalLoading").modal("hide");
		};

		window.disableDates = function (date) {
			if (date) {
				return date;
			} else {
				return ["2016/12/31"];
			}
		};

		window.unavailableDates = function (date) {
			var $unavailableDatesArray = [];

			if ($('.package-unavailable-date').length) {
				$.each($('.package-unavailable-date'), function (i, val) {
					$unavailableDatesArray.push($(this).val());
				});
			}

			return $unavailableDatesArray;
		};

		window.campaignDates = function (date) {
			var $campaignDatesArray = [];

			if ($('.package-campaign-date').length) {
				$.each($('.package-campaign-date'), function (i, val) {
					$campaignDatesArray.push($(this).val());
				});
			}

			return $campaignDatesArray;
		};

		window.minDate = function (date) {
			var today = new Date();
			if ($('#hidMinBookingDate').length && $('#hidMinBookingDate').val()) {
				return $('#hidMinBookingDate').val();
			} else {
				return today;
			}
		};

		window.showNotyError = function (errorText) {
			if (!errorText) errorText = "An error occurred while submitting your request. Please try again later!";
			noty({
				text: errorText,
				layout: "top",
				type: "error",
				closeWith: ["click", "hover"],
				timeout: 7500
			});
		};

		window.showNotySuccess = function (successText) {
			if (!successText) successText = "Your response has been submitted successfully.";
			noty({
				text: successText,
				layout: "top",
				type: "success",
				closeWith: ["click", "hover"],
				timeout: 5000
			});
		};

		window.notifyTeam = function (data) {
			$.ajax({
				"url": "/report/notify/error",
				"type": "POST",
				"dataType": "JSON",
				"data": data
			});
		};

		/* used in quick view, profile page for decors */
		window.checkFeasibilityInOtherCities = function () {
			var $partyPinCode = $("#partyPincode");
			var $mobilePhone = $("#mobilePhone");
			var otherCityDistStatus = "no";

			/* Google's city zip codes */
			var $pinCodes = {
				"hyderabad": 500078,
				"bangalore": 560022,
				"pune": 412216,
				"mumbai": 400615,
				"delhi": 110001
			};
			var $gUrl = $partyPinCode.data("url");

			if ($gUrl) {
				$.each($pinCodes, function (key, pinCode) {
					if (otherCityDistStatus === "no") {
						$.ajax({
							url: $gUrl,
							dataType: "json",
							type: "POST",
							async: false,
							data: {
								"providerPinCode": pinCode,
								"partyPinCode": $partyPinCode.val(),
								"phone": $mobilePhone.val()
							},
							success: function (data) {
								if (data.success === true) {
									var otherCityDistCheck = data.partyDist;

									if (otherCityDistCheck < 100000) {
										otherCityDistStatus = "yes";
									}
								}
							}
						});
					}
				});
			}

			return otherCityDistStatus;
		};

		window.getUrlParameters = function () {
			var vars = [], hash;
			var q = document.URL.split('?')[1];

			if (q != undefined) {
				if (q.indexOf('#') > -1) {
					q = q.substring(0, q.indexOf('#'));
				}

				q = q.split('&');
				for (var i = 0; i < q.length; i++) {
					hash = q[i].split('=');
					vars.push(hash[1]);
					vars[hash[0]] = hash[1];
				}
			}

			return vars;
		};

		/* function to check if the city is disabled */
		window.isCityDisabled = function () {
			var currentPath = $(location).attr('pathname');
			var cityUrlArr = [
				'mumbai',
				'delhi',
				'pune',
				'chennai',
				'jaipur',
			];
	
			var isDisabledCity = false;
			if (currentPath) {
				for (i = 0; i < cityUrlArr.length; ++i) {
					if (currentPath.includes(cityUrlArr[i])) { 
						// var bookNowBtn = $('.product-btn-book-now');
						// bookNowBtn.attr('disabled', true);
						// bookNowBtn.on('click', false);
						isDisabledCity = true;
						break;
					}
				}
			}

			// return isDisabledCity;
			// @see: disabling all the cities
			return true;
		};		
	})();

	(function initElements() {
		/* tool tips */
		$("[data-toggle='tooltip']").tooltip({container: "body"});

		/* enlarge images on click */
		if ($(".gallery-images").length) {
			$.each($(".gallery-images"), function (i, val) {

				var queryString = ".gallery-images:nth(" + i + ") a[rel^='slider']";

				$(queryString).prettyPhoto({
					"show_title": true,
					"social_tools": "",
					"allow_resize": true
				});
			});
		}

		if ($('.country-calling-code').length > 0) {
			$('.country-calling-code').selectize();
		}

		if ($('.country-calling-code-popup-modal').length > 0) {
			$('.country-calling-code-popup-modal').selectize();
		}
	})();

	(function headerLeftNavigation() {
		
		var $listSlideEmpty = $('#listSlideEmpty');
		var $listSliderSection = $('#listSliderSection');
		var $mobileHeaderMenuBtn = $('#mobileHeaderMenuBtn');
		var $listSliderBackground = $('#listSliderBackground');
		var $listSlideMenu = $('#listSlideMenu');
		var $listSlideContent = $('#listSlideContent');
		var dropdown = document.getElementsByClassName("dropdown-btn");
		var i;

		for (i = 0; i < dropdown.length; i++) {
			dropdown[i].addEventListener("click", function () {
				var dropdownContent = this.nextElementSibling;
				if (dropdownContent.style.display === "block") {
					dropdownContent.style.display = "none";
				} else {
					dropdownContent.style.display = "block";
				}
			});
		}

		setTimeout(function () {
			$listSlideContent.removeClass('hide');
		}, 1500);

		$listSlideEmpty.click(function (event) {
			event.preventDefault();
			hideSliderMenu();
		});

		$mobileHeaderMenuBtn.click(function (event) {
			event.preventDefault();
			showSliderMenu();
		});

		$(".header-scroll-icons-wrap").on("click", function () {
			
			var iconId = $(this).data('id');
			if (iconId != 'direct-link') {
				showSliderMenuSection(iconId);
			}
		});

		function showSliderMenuSection(iconId) {
			
			showSliderMenu();
			$('#' + iconId).click();
			$(".dropdown-container").css("display", "none");
			$('#' + iconId).siblings().first().css("display", "block");
		}

		function hideSliderMenu() {
			$listSliderBackground.css("display", "none");
			$listSliderSection.animate({
				width: 'hide'
			}, 50);
			$('body').css("overflow", "visible");
		}

		function showSliderMenu() {
			
			$listSlideMenu.addClass('hide');
			$listSliderBackground.css("display", "block");

			$listSliderSection.animate({
				width: 'show'
			}, 300);

			setTimeout(function () {
				$listSlideMenu.removeClass('hide');
			}, 300);

			$listSliderBackground.animate({
				backgroundColor: 'rgba(0, 0, 0, 0.5)'
			}, 200);

			$('body').css("overflow", "hidden");
		}
	})();

	(function userLogin() {
		var signInButton = $('.form-sign-in-button');
		var signUpButton = $('.form-sign-up-button');

		signInButton.on('click', function (e) {
			e.preventDefault();
			var data =
				{
					'email': $('.email').val(),
					'password': $('.password').val()
				};
			signInButton.attr('disabled', 'disabled');
			$.ajax({
				url: '/customer/login',
				type: "POST",
				data: data,
				success: function (data) {
					if (data.success) {
						location.reload();
					} else {
						showNotyError(data.error);
						signInButton.removeAttr('disabled');
					}
				},
				error: function () {
					showNotyError(data.error);
					signInButton.removeAttr('disabled');
				}
			});
		});

		signUpButton.on('click', function (e) {
			e.preventDefault();
			var data =
				{
					'email': $('.sign-up-email').val(),
					'password': $('.sign-up-password').val(),
					'rePassword': $('.sign-up-rePassword').val(),
					'fullName': $('.sign-up-fullName').val()
				};
			signUpButton.attr('disabled', 'disabled');
			$.ajax({
				url: '/customer/signup',
				type: "POST",
				data: data,
				success: function (data) {
					if (data.success) {
						$('#loginModal').modal('hide');
						$('#thankYouModal').modal('show');
						setTimeout(function () {
							location.reload();
						}, 3000);
					} else {
						showNotyError(data.error);
						signUpButton.removeAttr('disabled');
					}
				},
				error: function () {
					showNotyError(data.error);
					signUpButton.removeAttr('disabled');
				}
			});
		});

		var rePass = $('.sign-up-rePassword');
		rePass.blur(function () {
			var pass = $('.sign-up-password').val();
			if (pass !== rePass.val()) {
				showNotyError("Passwords do not match, Please check");
			}
		});

		$('.button-sign-up').on('click', function () {
			$('.sign-up-model').removeClass('hide');
			$('.sign-in-model').addClass('hide');
		});

		$('.button-sign-in').on('click', function () {
			$('.sign-in-model').removeClass('hide');
			$('.sign-up-model').addClass('hide');
		});
	})();

	(function setDatePicker() {
		var today = new Date();
		var oneDay = new Date(today.getTime() + 24 * 60 * 60 * 1000);

		if ($("#partyDate").length) {
			$("#partyDate, #enquiryDate, #pbEnqEventDate").datetimepicker({
				"timepicker": false,
				"minDate": 0,
				"format": 'Y/m/d',
				"scrollInput": false,
				"scrollMonth": false,
				"scrollTime": false,
				"closeOnDateSelect": true,
				"disabledDates": window.disableDates(),
				onSelectDate: function (ct, $i) {
					$i.parent().addClass("is-dirty");
				}
			});
		}

		if ($("#bookNowPartyDate").length) {
			$("#bookNowPartyDate").datetimepicker({
				"timepicker": false,
				"minDate": oneDay,
				"format": "Y/m/d",
				"scrollInput": false,
				"scrollMonth": false,
				"scrollTime": false,
				"closeOnDateSelect": true,
				"disabledDates": window.disableDates(),
				onSelectDate: function (ct, $i) {
					$i.parent().addClass("is-dirty");
				}
			});
		}

		if ($("#profilePartyDate").length) {
			$("#profilePartyDate").datetimepicker({
				"timepicker:": false,
				"minDate:": oneDay,
				"format:": "Y/m/d",
				"scrollInput:": false,
				"scrollMonth:": false,
				"scrollTime:": false,
				"closeOnDateSelect:": true,
				"disabledDates:": window.disableDates(),
				onSelectDate: function (ct, $i) {
					$i.parent().addClass("is-dirty");
				}
			});
		}

		if ($("#eventDate").length) {
			$("#eventDate").datetimepicker({
				"timepicker": false,
				"minDate": 0,
				"format": "Y/m/d",
				"scrollInput": false,
				"scrollMonth": false,
				"scrollTime": false,
				"closeOnDateSelect": true,
				"disabledDates": window.disableDates(),
				onSelectDate: function (ct, $i) {
					$i.parent().addClass("is-dirty");
				}
			});
		}

		if ($("#enquiryDate").length) {
			$("#enquiryDate").datetimepicker({
				"timepicker": false,
				"minDate": 0,
				"format": "Y/m/d",
				"scrollInput": false,
				"scrollMonth": false,
				"scrollTime": false,
				"closeOnDateSelect": true,
				"disabledDates": window.disableDates(),
				onSelectDate: function (ct, $i) {
					$i.parent().addClass("is-dirty");
				}
			});
		}

		if ($("#enquiryFormDate").length) {
			$("#enquiryFormDate").datetimepicker({
				"timepicker": false,
				"minDate": 0,
				"format": "Y/m/d",
				"scrollInput": false,
				"scrollMonth": false,
				"scrollTime": false,
				"closeOnDateSelect": true,
				"disabledDates": window.disableDates(),
				onSelectDate: function (ct, $i) {
					$i.parent().addClass("is-dirty");
				}
			});
		}

		/* venue deals */
		if ($("#partyVdDate").length) {
			$("#partyVdDate").datetimepicker({
				"minDate": 0,
				"timepicker": false,
				"startDate": oneDay,
				"defaultDate": oneDay,
				"format": "Y/m/d",
				"scrollInput": false,
				"scrollMonth": false,
				"scrollTime": false,
				"closeOnDateSelect": true,
				"disabledDates": window.disableDates(),
				onSelectDate: function (ct, $i) {
					$i.parent().addClass("is-dirty");
				}
			});
		}

		if ($("#checkInDate").length) {
			console.log(window.campaignDates());
			if ($('.package-campaign-date').length) {
				$("#checkInDate").datetimepicker({
					timepicker: false,
					minDate: window.minDate(),
					format: "Y/m/d",
					scrollInput: false,
					scrollMonth: false,
					scrollTime: false,
					closeOnDateSelect: true,
					allowDates: window.campaignDates(),
					onSelectDate: function (ct, $i) {
						$i.parent().addClass("is-dirty");
						/* for venue, decor forms */
					}
				});
			}
			else {
				$("#checkInDate").datetimepicker({
					timepicker: false,
					minDate: window.minDate(),
					format: "Y/m/d",
					scrollInput: false,
					scrollMonth: false,
					scrollTime: false,
					closeOnDateSelect: true,
					disabledDates: window.unavailableDates(),
					onSelectDate: function (ct, $i) {
						$i.parent().addClass("is-dirty");
						/* for venue, decor forms */
					}
				});
			}
		}

		if ($("#partyDateTime").length) {
			$("#partyDateTime").datetimepicker({
				timepicker: true,
				format: "Y/m/d H:i",
				minDate: 0,
				scrollInput: false,
				scrollMonth: false,
				scrollTime: true,
				closeOnDateSelect: false,
				disabledDates: window.disableDates()
			});
		}

		if ($("#enquiryProfileDate").length) {
			$("#enquiryProfileDate").datetimepicker({
				timepicker: false,
				minDate: 0,
				format: "Y/m/d",
				scrollInput: false,
				scrollMonth: false,
				scrollTime: false,
				closeOnDateSelect: true,
				disabledDates: window.disableDates(),
				onSelectDate: function (ct, $i) {
					$i.parent().addClass("is-dirty");
				}
			});
		}

		if ($("#partyTime").length) {
			$("#partyTime").datetimepicker({
				datepicker: false,
				formatTime: "g:i A",
				format: "g:i A",
				step: 30,
				onSelectTime: function (ct, $i) {
					$i.parent().addClass("is-dirty");
				}
			});
		}

		/* birthday trends page */
		if ($("#tbEventDate").length) {
			$("#tbEventDate").datetimepicker({
				timepicker: false,
				minDate: 0,
				format: "Y/m/d",
				scrollInput: false,
				scrollMonth: false,
				scrollTime: false,
				closeOnDateSelect: true,
				disabledDates: window.disableDates()
			});
		}
	})();

	(function scrollHandlers() {
		/* scroll items to a position */
		$(document).on("click", ".scroll-item", function () {

			var id = $(this).data("scroll");
			var negativeOffset = $(this).data("neg") ? $(this).data("neg") : 40;
			var negTabsPos = 0;

			/* for package portfolio tabs */
			if ($(".package-data-wrap .tabs-sec").length) {
				var tabsPos = $(".package-data-wrap .tabs-sec").css("position");
				if (tabsPos != "fixed") negTabsPos = 72;
			}

			if(typeof id !== "undefined") {
				$("html,body").animate({
					scrollTop: $("#" + id).offset().top - (negativeOffset + negTabsPos)
				}, "slow");
			}

			return false;
		});

		$(window).scroll(function () { /* scroll to top */
			var searchOpen = $("#desktop-search").css("height");
			if (searchOpen === "0px") {
				var pos = $(this).scrollTop();
				if (pos > 280) $(".scrollup").fadeIn();
				else $(".scrollup").fadeOut();
				if (pos > 700) $(".scroll-down").fadeOut();
				else $(".scroll-down").fadeIn();
			}
		});

		$(".scrollup").click(function () {
			$("html, body").animate({scrollTop: 0}, 600);
			return false;
		});

		$(".scroll-down").click(function () {
			$("html, body").animate({scrollTop: 700}, 600);
			return false;
		});

		$(".tooltip-markup").tooltip({
			/* Tooltips */
			"placement": "auto top"
		});

		/* mouse scroll inside side bar for party bag */
		$(document).on("scroll", ".sidebar", function (ev) {
			ev.stopPropagation();
			ev.preventDefault();
			ev.returnValue = false;
			return false;
		});
		var scrollable = ".sidebar, .sidebar .sidebar-content";
		$(document).on("DOMMouseScroll mousewheel", scrollable, function (ev) {
			ev.stopPropagation();
			var up = (ev.type.toString() === "DOMMouseScroll" ? ev.originalEvent.detail * -40 : ev.originalEvent.wheelDelta) > 0;
			var c = this.scrollTop / (this.scrollHeight - this.clientHeight);
			var prevent = function () {
				ev.preventDefault();
				ev.returnValue = false;
				return false;
			};
			var scroll = document.querySelector(".sidebar .sidebar-content");
			var scrollBar = (scroll && scroll.scrollHeight > scroll.clientHeight) || false;
			if ((!up && c === 1) || (up && c === 0) || !scrollBar)
				return prevent();
		});

		/* mouse scroll in global search */
		$(document).on("scroll", "#desktop-search", function (ev) {
			ev.stopPropagation();
			ev.preventDefault();
			ev.returnValue = false;
			return false;
		});
	})();

	(function handleHeaderEnquiryForm() {

		/* booking form clicked, show modal */
		$(".btn-post, .btn-error-page-post").click(function (event) {
			event.preventDefault();

			var popUpEnquiryForm = $(".popup-need-help-btn");
			if (popUpEnquiryForm.length && (parseInt(popUpEnquiryForm.data("loaded"), 10) === 2)) {
				popUpEnquiryForm.click();
			} else {
				showEnquiryForm();
			}
		});

		if ((pageUrl.indexOf('#quickEnquiry') !== -1) || (pageUrl.indexOf('#fbEnquiry') !== -1)) {
			$('#formEnquiryOpen').val(1);
		}

		/* @see: not a solid fix */
		/* fire after an ajax request is completed */
		/* issue if there is any time gap between the finish and fire of ajax requests */
		$(document).ajaxStop(function () {
			if ((pageUrl.indexOf('#quickEnquiry') !== -1) || (pageUrl.indexOf('#fbEnquiry') !== -1)) {
				if ($('#formEnquiryOpen').val() === "1") {
					showEnquiryForm();
					setTimeout(function () {
						$('#formEnquiryOpen').val(0);
					}, 10000)
				}
			}
		});

		function showEnquiryForm() {
			/* if homepage, scroll to booking form */
			/* else show modal */
			var formWrap = $(".booking-form-wrap");
			if (formWrap.length) {
				setTimeout(function () {
					$("html,body").animate({
						scrollTop: formWrap.offset().top
					}, "slow");
				}, 1000);

				return false;
			} else {
				$("#modalEnquiryForm").modal({
					"backdrop": "static",
					"keyboard": false,
					"show": true
				});

				/* to negate any profile page url action */
				$("#enquiryForm").attr("action");
				$("#enquiryForm").attr("action", $("#hidHeaderEnqUrl").val());
			}
		}

		var $btnSubmit = $("#btnSubmitEnq");

		function showSubmitButton() {
			$btnSubmit.attr("disabled", false);
			$btnSubmit.find(".default").removeClass("hide");
			$btnSubmit.find(".waiting").addClass("hide");
		}

		function hideSubmitButton() {
			$btnSubmit.attr("disabled", true);
			$btnSubmit.find(".default").addClass("hide");
			$btnSubmit.find(".waiting").removeClass("hide");
		}

		/* clear any previous error messages in booking form */
		$("#modalEnquiryForm").on("show.bs.modal", function () {
			$("#accepts").prop("checked", false);
			/* uncheck - terms checkbox */
			$("#enquiryForm")[0].reset();
			$(".sticky-error-wrap").addClass("hide");
			$(".is-dirty").removeClass("is-dirty");
			showSubmitButton();
		});

		/* submit the form */
		$("#enquiryForm").submit(function (e) {
			e.preventDefault();

			var url = $(this).attr("action");
			var data = {
				"name": $("#enquiryName").val(),
				"phone": $("#enquiryPhone").val(),
				"email": $("#enquiryEmail").val(),
				"partyDate": $("#enquiryDate").val(),
				"comments": $("#enquiryComments").val(),
				"accepts": $("#accepts").is(":checked") ? "yes" : "no"
			};

			hideSubmitButton();

			$.ajax({
				url: url,
				type: "POST",
				data: data,
				success: function (data) {
					showSubmitButton();
					if (data.success) {
						if (data.redirectTo) {
							/* google event on enquiry success */
							var label = "home";
							var price = 0;
							if ($("#hidProductCode").length) {
								label = $("#hidProductCode").val();
							}
							if ($("#hidProductPrice").length) {
								price = $("#hidProductPrice").val();
							}

							/* redirect to thank you page */
							window.location = data.redirectTo;
						} else {
							window.showNotySuccess("Thank you! We've received your request. We will get in touch with you shortly.");
						}
					} else {
						var errorObj = $(".modal-enquiry-form").find(".sticky-error-wrap").removeClass("hide").find(".error-message");
						errorObj.text(data.errors);
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					showSubmitButton();
					window.showNotyError("Some error occurred while submitting your request.");
					window.notifyTeam({
						"url": url,
						"textStatus": textStatus,
						"errorThrown": errorThrown
					});
				}
			});
		});
	})();

	(function handleFormSubmits() {
		/* disable submit button, as it takes time to send email */
		$("#feedbackFormsumbit").click(function () {
			$(this).attr("disabled", "disabled");
			$(this).attr("value", "Submitting ...");
			$("#feedbackForm").submit();
		});

		/* disable submit button in signup form, as it takes time to send email */
		$("#vendorSignUpSubmit").click(function () {
			$(this).attr("disabled", "disabled");
			$(this).attr("value", "Submitting ...");
			$("#vendorSignUpForm").submit();
		});
	})();

	(function showGoogleReviews() {
		if (window.location.href.indexOf('party-kits') != -1) {
			return false;
		}

		var $ajaxUrl = '/ajax/util/google-reviews';
		$.ajax({
			url: $ajaxUrl,
			data: {},
			dataType: 'html',
			type: 'GET',
			success: function (data) {
				$('.footer-google-reviews').prepend(data);
			},
			error: function (jqXHR, textStatus, errorThrown) {
				window.notifyTeam({
					"url": $ajaxUrl,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});
	})();

	(function attachGooglePlacesToPartyLocation() {
		if (typeof google !== undefined && typeof google === "function") {
			if ($("#googleApiIntegration").val() === "yes") {
				/* @see: do not change to $() notation */
				var preventFormSubmit = function (event) {
					if (event.keyCode === 13) {
						event.preventDefault();
					}
				};

				var locationInputs = document.getElementsByClassName('google-auto-complete');
				for (var i = 0; i < locationInputs.length; i++) {
					var input = locationInputs[i];
					if (input) {
						google.maps.event.addDomListener(input, 'keydown', preventFormSubmit);
					}
				}

				var initialize = function () {
					var options = {
						types: ['(regions)'],
						componentRestrictions: {country: 'in'}
					};

					for (var i = 0; i < locationInputs.length; i++) {
						var input = locationInputs[i];
						if (input) {
							new google.maps.places.Autocomplete(input, options);
							var autoComplete = new google.maps.places.Autocomplete(input, options);
							autoComplete.addListener('place_changed', function () {
								var place = this.getPlace();
								var addressComponents = place.address_components;
								var postalCode = null;

								$.each(addressComponents, function (key, component) {
									if (component.types[0] == 'postal_code') {
										postalCode = component.long_name;
									}
								});

								var $partyPinCode = $('#partyPincode');
								if (postalCode && $partyPinCode[0]) {
									$partyPinCode.val(postalCode);
									$partyPinCode.parent().addClass('is-dirty');
									$partyPinCode.prop('disabled', true);
								} else {
									$partyPinCode.val('');
									$partyPinCode.parent().removeClass('is-dirty');
									$partyPinCode.prop('disabled', false);
								}

								$('.google-location-details').val(JSON.stringify(addressComponents));
							});
						}
					}
				};
				google.maps.event.addDomListener(window, 'load', initialize);
			}
		}
	})();

	(function loadTopHeaderSurpriseCategories() {
		if ($('#topHeaderSurpriseCategories').length) {
			var $ajaxUrl = '/ajax/util/surprise-categories';
			$.ajax({
				url: $ajaxUrl,
				data: {},
				dataType: 'JSON',
				type: 'GET',
				success: function (data) {
					var $element = "";
					if (data.categoryData && (data.categoryData.length != 0)) {
						$.each(data.categoryData, function (key, datum) {
							$element += "<a href='" + datum.categoryUrl + "'>" + datum.categoryName + "</a>";
						});
					}
					$('#topHeaderSurpriseCategories').append($element);
				},
				error: function (jqXHR, textStatus, errorThrown) {
					window.notifyTeam({
						"url": $ajaxUrl,
						"textStatus": textStatus,
						"errorThrown": errorThrown
					});
				}
			});
		}
	})();

	/* @see: do not remove unless written elsewhere and tested */
	/* js to append required properties to pb checkout inputs */
	$('body').on('mouseover', "#pbCheckoutEventDate", function () {
		/* add datetimepicker */
		$(this).datetimepicker({
			"timepicker": false,
			"minDate": 0,
			"format": 'Y/m/d',
			"scrollInput": false,
			"scrollMonth": false,
			"scrollTime": false,
			"closeOnDateSelect": true,
			"disabledDates": window.disableDates(),
			onSelectDate: function (ct, $i) {
				$i.parent().addClass("is-dirty");
			}
		});

		/* add material css */
		$(".sidebar #pbCheckoutEventDate").find(".mdl-textfield").removeClass("is-upgraded").attr("data-upgraded", "");
		if (!(typeof (componentHandler) === 'undefined')) {
			componentHandler.upgradeDom();
		}
	});

	$('body').on('mouseover', "#pbCheckoutPartyPinCode", function () {
		/* add material css */
		$(".sidebar #pbCheckoutPartyPinCode").find(".mdl-textfield").removeClass("is-upgraded").attr("data-upgraded", "");
		if (!(typeof (componentHandler) === 'undefined')) {
			componentHandler.upgradeDom();
		}
	});
});
