$(document).ready(function () {

	/* regarding venue deal - not necessary right now
	 var $venueDealToken = $(".venue-deal-token");
	 var $venueDealPrice = $(".venue-deal-price");
	 var $venueDealNewToken = $(".venue-deal-new-token");
	 */

	function onSubmitEnquiryForm(event) {
		event.preventDefault();

		var btnSubmit = $(this);
		var url = $(this).data('url');
		if (!url) {
			window.showNotyError("Sorry, could not save your details. Please try again in sometime.");
			return false;
		}
		var data = {
			"name": $("#enquiryProfileName").val(),
			"phone": $("#enquiryProfilePhone").val(),
			"email": $("#enquiryProfileEmail").val(),
			"partyDate": $("#enquiryProfileDate").val(),
			"location": $("#enquiryProfilePartyLocation").val(),
			"locationDetails": $(".google-location-details").val()
		};

		$(".errors-cnt").addClass("hide");
		btnSubmit.prop("disabled", true);
		window.showLoading();

		$.ajax({
			url: url,
			type: "POST",
			dataType: "json",
			data: data,
			success: function (data) {
				if (data.success) {
					btnSubmit.prop("disabled", false);
					window.hideLoading();

					/* redirect to thank you page */
					if (data.redirectTo) {
						window.location = data.redirectTo;
					} else {
						window.showNotySuccess("Thank you! We've received your request. We will get in touch with you shortly.");
					}
				} else {
					btnSubmit.prop("disabled", false);

					/* show errors */
					var errors = data.errors;
					$(".errors-cnt").removeClass("hide").text(errors);
					window.hideLoading();

					/* move to top of form */
					var refUrl = window.location.href;
					var hashValues = refUrl.split("#");
					if (hashValues.indexOf("book") >= 0) {
						window.location = refUrl;
					} else {
						window.location = refUrl + "#book"
					}
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				btnSubmit.removeAttr("disabled");
				$(".errors-cnt").removeClass("hide").text("Sorry, your request could not be saved. Please try again later.");
				window.hideLoading();
				window.notifyTeam({
					"url": url,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});
	}

	function onPopupClick(event) {
		event.preventDefault();
		window.open($(this).attr("href"), "popupWindow", "width=600,height=600,scrollbars=yes");
	}

	function onPreCheckoutSubmit(event) {
		event.preventDefault();

		if ($('.product-book-form').hasClass('product-book-form-hidden')) {
			$('.product-book-form').animate({
				height: 'show'
			}, "fast");
			$('.product-book-form').removeClass('product-book-form-hidden');
			$('#btnPcSubmit').children('span').text('Proceed To book');
			return false;
		}

		if ($('#partyTime').length && !$('#partyTime').val()) {
			$('#partyTime').focus();
			return false;
		}

		var form = $('#autoBookingForm');
		var optionId = $('#hidOptionId').val();
		var optionTypeId = $('#hidOptionTypeId').val();
		var occasionId = $('#occasionId').val();
		var partyDate = $('#autoBookingForm').find('#checkInDate').val();
		var phone = $('#autoBookingForm').find('#phone').val();

		/* calculate party time */
		var partyTime = $('#partyTime').val();
		var checkInTime = $('#checkInTime').val();

		if (partyTime === "" || partyTime === undefined) {
			partyTime = checkInTime;
		}

		/* hide error message if any */
		$('#availCheckFailMsg').addClass('hide');
		form.find('.errors-msg').addClass('hide');

		/* show loader */
		$('.generic-page-loader-wrap').removeClass('hide');

		var $data = {
			'optionId': optionId,
			'optionTypeId': optionTypeId,
			'partyDate': partyDate,
			'partyTime': partyTime,
			'occasionId': occasionId,
			'phone': phone
		};

		var $ajaxUrl = "/ajax/auto-book/check-availability";

		/* @todo: can be optimized by not making API request every time. */
		$.ajax({
			url: $ajaxUrl,
			type: 'POST',
			dataType: 'JSON',
			data: $data,
			success: function (data) {
				if (data.success === true) {
					ajaxToShowCheckoutPage();
				} else {
					if (data.validationError && (data.validationError === true)) {
						var $errorMsg = "Kindly provide all the required data";
						if (data.errorMsg) {
							$errorMsg = data.errorMsg;
						}
						form.find('.errors-msg').removeClass('hide').text($errorMsg);
					} else {
						$('.generic-page-loader-wrap').addClass('hide');
						$('#availCheckRegretMsg').text(partyDate + ' - ' + partyTime);
						$('#availCheckFailMsg').removeClass('hide');

						var $availCheckFailUrl = "/ajax/auto-book/check-availability-fail";
						$.ajax({
							url: $availCheckFailUrl,
							type: 'POST',
							dataType: 'JSON',
							data: $data,
							error: function (jqXHR, textStatus, errorThrown) {
								window.notifyTeam({
									"url": $availCheckFailUrl,
									"textStatus": textStatus,
									"errorThrown": errorThrown
								});
							}
						})
					}
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				ajaxToShowCheckoutPage();
				window.notifyTeam({
					"url": $ajaxUrl,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}

		});

	}

	function ajaxToShowCheckoutPage() {

		var form = $('#autoBookingForm');
		var url = form.data('url');
		var slotSelector = form.find('#pcPartySlot');
		var slotId = slotSelector.val();
		var guestsCount = form.find('#pcGuestsCount').val();
		var foodType = form.find('#pcFoodType').val();
		var forVenueDetails = $('#forVenueDetails').val();
		var partyTime = $('#partyTime').val();
		var advanceSelect = $("input[type='radio'][name='advSelectOption']:checked").val();

		var checkInTime = $('#checkInTime').val();
		var checkOutTime = undefined;

		$('.pc-package-unavailable-wrap').remove();

		if (partyTime === "" || partyTime === undefined) {
			partyTime = checkInTime;
			checkOutTime = $('#checkInTime').find(':selected').data('check-out-time');
		}

		var data = {
			'phone': form.find('#phone').val(),
			'callingCode': form.find('#checkoutModalCallingCode').val(),
			'partyDate': form.find('#partyDate').val(),
			'partySlot': slotId,
			'guestsCount': guestsCount,
			'foodType': foodType,
			'checkInDate': form.find('#checkInDate').val(),
			'forVenueDetails': forVenueDetails,
			'partyTime': partyTime,
			'checkOutTime': checkOutTime,
			'advanceSelect': advanceSelect,
			'pageUrl': window.location.href
		};

		/* add-ons */
		var $addOn = $('.add-on-cta-list');

		if ($addOn.length > 0) {

			var addOns = [];
			$.each($addOn, function (key, value) {
				if ($(this).data('count-units') > 0) {
					addOns[$(this).data('id')] = $(this).data('count-units');
				}
			});

			data['addOns'] = addOns;
		}

		$.ajax({
			'url': url,
			'type': 'POST',
			'dataType': 'JSON',
			'data': data,
			success: function (data) {
				if (data.success === true && data.redirectUrl) {
					location.href = data.redirectUrl;
				} else if (data.success === false) {
					form.find('.errors-msg').removeClass('hide').text(data.error);
					if (data.is_live === 0) {
						var $submit = $('#btnPcSubmit');
						$submit.text("Option Unavailable");
						$submit.attr('disabled', true);
					} else if (data.campaignUnavailability === true) {
						form.find('.errors-msg').addClass('hide');
						$markup = '<div class="pc-package-unavailable-wrap mar-t-15 text-center">';
						$markup += '<div class="text-r">We regret to inform that the package is unavailable for the selected date.</div>';
						if (data.campaignLandingLink) {
							$markup += '<div class="mar-t-10">However, we have some exclusive packages for this special day. Kindly click on the link below to explore.</div>';
							$markup += '<a class="mar-t-5 text-underline" href="' + data.campaignLandingLink + '" target="_blank">' + data.campaignLandingPage + '</a>';
						} else {
							$markup += '<div class="mar-t-10">However, we are adding exclusive packages for this special day. Please stay tuned.</div>';
						}
						$markup += '</div>';

						$('.pc-highlights-package-wrap').prepend($markup);
					}
					$('.generic-page-loader-wrap').addClass('hide');
				} else {
					$('.generic-page-loader-wrap').addClass('hide');
					window.showNotyError("Some error occurred while we are collecting your details, Please refresh the page and try again");
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				window.showNotyError("Some error occurred, please try again after refreshing the page");
				window.notifyTeam({
					"url": url,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});
	}

	(function attacheClickHandlers() {

		/* popup */
		$('.popup').on('click', onPopupClick);

		/* enquiry form */
		$('#btnSubmitForm').on('click', onSubmitEnquiryForm);

		/* form submit (auto booking) */
		// @see: check if the button should be enabled or disabled
		if(window.isCityDisabled()) {
			// disable the button/link
			var btnPcSubmit = $('#btnPcSubmit');
			btnPcSubmit.attr('disabled', true);
			btnPcSubmit.on('click', false);
		} else {
			$('#btnPcSubmit').on('click', onPreCheckoutSubmit);
		}

	})();

	(function fixProfileHeader() {
		var lastId,
			topMenu = $("#top-menu"),
			topMenuHeight = topMenu.outerHeight() + 90,
			menuItems = topMenu.find("a"), /* all list items */
			scrollItems = menuItems.map(function () {
				var item = $($(this).attr("href"));
				if (item.length) {
					return item;
				}
			});

		menuItems.click(function (e) {
			var href = $(this).attr("href"),
				offsetTop = href === "#" ? 0 : $(href).offset().top - topMenuHeight + 50;
			$('html, body').stop().animate({
				scrollTop: offsetTop
			}, 200);
			e.preventDefault();
		});

		$(window).scroll(function () {
			var sticky = $('.sticky'),
				scroll = $(window).scrollTop();

			if (scroll >= 650 && $(window).width() > 767) {
				sticky.addClass('fixed');
				$("#info-container").addClass('mar-t-90');
			} else {
				sticky.removeClass('fixed');
				$("#info-container").removeClass('mar-t-90');
			}

			var fromTop = $(this).scrollTop() + topMenuHeight;
			var cur = scrollItems.map(function () {
				if ($(this).offset().top < fromTop)
					return this;
			});
			cur = cur[cur.length - 1];
			var id = cur && cur.length ? cur[0].id : "";

			if (lastId !== id) {
				lastId = id;
				menuItems
					.parent().removeClass("active")
					.end().filter("[href='#" + id + "']").parent().addClass("active");
			}
		});
	})();

	function calculateRoundedAdvance($bookingAmount) {
		var $percentage = $('#hidAdvPercentage').val();
		var $advanceAmount = $bookingAmount * $percentage * 0.01;
		var $balAmount = $bookingAmount - $advanceAmount;
		if (($balAmount % 100) != 0 && $balAmount > 100) {
			var $round = parseInt($balAmount % 100);
			var $roundedAdvanceAmount = parseInt($bookingAmount) - parseInt($balAmount) + $round;
			return $roundedAdvanceAmount;
		}

		return parseInt($advanceAmount);
	}

	function priceFormat($num) {
		$num = $num.toString();
		var lastThree = $num.substring($num.length - 3);
		var otherNumbers = $num.substring(0, $num.length - 3);
		if (otherNumbers != '')
			lastThree = ',' + lastThree;
		var $res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

		return $res;
	}

	var bookNowModalOpen = false;
	$("#bookNowModal").on('shown.bs.modal', function () {

		$('.pc-package-unavailable-wrap').remove();

		if (!bookNowModalOpen) {
			/* functionality to hide and show add-ons content */
			(function toggleAddOnsContent() {

				var overflowedModalContent = $('.modal-content-overflow'),
					overflowedModalBtn = $('.btn-modal-overflow'),
					overflowedModalContentHeight = overflowedModalContent.height();

				if (overflowedModalContentHeight > 250) {
					overflowedModalContent.animate({'height': '250px'});
					overflowedModalBtn.addClass('less');
					overflowedModalBtn.css('display', 'inline-block');
				}

				overflowedModalBtn.on('click', function (e) {
					e.preventDefault();

					if (overflowedModalBtn.hasClass('less')) {
						overflowedModalContent.animate({'height': overflowedModalContentHeight});
						overflowedModalBtn.removeClass('less');
						overflowedModalBtn.addClass('more');
						overflowedModalBtn.text('- Show less add-ons');

					} else {
						overflowedModalContent.animate({'height': '250px'});
						overflowedModalBtn.addClass('less');
						overflowedModalBtn.removeClass('more');
						overflowedModalBtn.text('+ Show more add-ons');
					}
				});
			})();
		}

		bookNowModalOpen = true;
	});

	if($('#campaignTextMessage') !== undefined) {
		$('#checkInDate').on('change', function () {
			console.log($('#checkInDate').val());
			if($('#checkInDate').val() && ($('#checkInDate').val() == $('#campaignTextMessage').data('campaign-focus-date'))) {
				$('#campaignTextMessage').addClass('hide');
			}
			else {
				$('#campaignTextMessage').removeClass('hide');
			}
		});
	}

});