$(document).ready(function () {

	var lastId,
		partnerMenu = $("#partner-menu"),
		partnerMenuHeight = partnerMenu.outerHeight() + 90,
		partnerMenuItems = partnerMenu.find("a"), /* all list items */
		scrollItems = partnerMenu.map(function () {
			var item = $($(this).attr("href"));
			if (item.length) {
				return item;
			}
		});

	partnerMenuItems.click(function (e) {
		var href = $(this).attr("href"),
			offsetTop = href === "#" ? 0 : $(href).offset().top - partnerMenuHeight + 40;
		$('html, body').stop().animate({
			scrollTop: offsetTop
		}, 200);
		e.preventDefault();
	});

	/* sticky partner menu */
	$(window).scroll(function () {
		var partnerSticky = $('.partner-sticky'),
			scroll = $(window).scrollTop();
		if (scroll >= 421 && $(window).width() > 799) {
			partnerSticky.addClass('fixed');
			/* $("#partnerContent").addClass('mar-t-90'); */
			$('.header-signup-btn-cnt').removeClass('hide');
			$('.partner-sticky ul').removeClass('col-sm-8 col-md-8 col-lg-8 col-md-offset-2 col-sm-offset-2 col-lg-offset-2');
			$('.partner-sticky ul').addClass('col-sm-10 col-md-10 col-lg-10 col-md-offset-1 col-sm-offset-1 col-lg-offset-1');
		}
		else {
			partnerSticky.removeClass('fixed');
			/* $("#partnerContent").removeClass('mar-t-90'); */
			$('.header-signup-btn-cnt').addClass('hide');
			$('.partner-sticky ul').addClass('col-sm-8 col-md-8 col-lg-8 col-md-offset-2 col-sm-offset-2 col-lg-offset-2');
			$('.partner-sticky ul').removeClass('col-sm-10 col-md-10 col-lg-10 col-md-offset-1 col-sm-offset-1 col-lg-offset-1');
		}

		var fromTop = $(this).scrollTop() + partnerMenuHeight;
		var cur = scrollItems.map(function () {
			if ($(this).offset().top < fromTop)
				return this;
		});
		cur = cur[cur.length - 1];
		var id = cur && cur.length ? cur[0].id : "";

		if (lastId !== id) {
			lastId = id;
			partnerMenuItems
				.parent().removeClass("active")
				.end().filter("[href='#" + id + "']").parent().addClass("active");
		}
	});

	/* why partner carousel */
	$('#partner-slide').carousel({
		interval: 1000
	});

	$('#partner-slide .item').each(function () {
		if( $(window).width() > 899) {
			var next = $(this).next();
			if (!next.length) {
				next = $(this).siblings(':first');
			}
			next.children(':first-child').clone().appendTo($(this));

			for (var i = 0; i < 1; i++) {
				next = next.next();
				if (!next.length) {
					next = $(this).siblings(':first');
				}
				next.children(':first-child').clone().appendTo($(this));
			}
		}
	});
});