$(document).ready(function () {

	function openLoginModal() {
		//change header of the model
		$('#loginModal').modal({
			"backdrop": 'static',
			"keyboard": false,
			"show": true
		});
	}

	

	$('.dropdown-head').on('click', function () {
		$(this).find(".dropdown-icon").toggleClass('glyphicon-menu-down glyphicon-menu-right');
	});

	(function searchSubmit() {

		function handleSearchSubmit(userInputQuery, referral) {
			var inputQuery = validateSearchInput(userInputQuery);
			if (inputQuery !== '') {
				var url = $('#globalSearchAction').val() + "?q=" + inputQuery;
				window.location.replace(url);
			}
		}

		$('#desktopGlobalSearchInput').keypress(function (e) {
			var key = parseInt(e.which, 10);
			if (key === 13) {
				handleSearchSubmit($('#desktopGlobalSearchInput').val(), "desktop");
			}
		});

		$('#mobileGlobalSearchInput').keypress(function (e) {
			var key = parseInt(e.which, 10);
			if (key === 13) {
				handleSearchSubmit($('#mobileGlobalSearchInput').val(), "mobile");
			}
		});

		/* search icon */
		$('.header-search-icon-container').on('click', function (event) {
			event.preventDefault();
			var $searchInput = $(this).parent();
			var referral = $searchInput.data('referral');

			var userSearchQuery = "";
			if ($searchInput.find('#desktopGlobalSearchInput').length) {
				userSearchQuery = $searchInput.find('#desktopGlobalSearchInput').val();
			}
			if ($searchInput.find('#mobileGlobalSearchInput').length) {
				userSearchQuery = $searchInput.find('#mobileGlobalSearchInput').val();
			}

			handleSearchSubmit(userSearchQuery, referral);
		});

		$('.typeahead').bind("typeahead:select", function (ev, suggestion) {
			console.log(suggestion.name);
			if (suggestion.hasOwnProperty("profile_url") && suggestion.profile_url) {
				window.location.replace(suggestion.profile_url);
			} else {
				handleSearchSubmit(suggestion.name, "auto-select");
			}
		});
	})();

	(function loadTypeHintForSearch() {
		if ($('#globalSearchRemoteHost').length > 0) {
			var remoteHost = $('#globalSearchRemoteHost').val();
			var cityId = $('#globalSearchCity').val();
			var occasionId = $('#globalSearchOccasionId').val();
			var pageId = $('#pageId').val();

			if ((typeof pageId === "undefined") ||
				(pageId === "undefined") ||
				(pageId <= 0)) {
				pageId = 0;
			}

			$.getScript("/js/util/typeahead.js", function () {
				var engine = new Bloodhound({
					identify: function (o) {
						return o.id_str;
					},
					queryTokenizer: Bloodhound.tokenizers.whitespace,
					datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name', 'code'),
					dupDetector: function (a, b) {
						return a.id_str === b.id_str;
					},
					prefetch: remoteHost + '/search/prefetch/' + cityId + '/' + occasionId + '/' + pageId,
					remote: {
						url: remoteHost + '/search/remote/' + cityId + '/%QUERY',
						wildcard: '%QUERY'
					}
				});

				var defaultIds = [];
				var count = 1;
				while (count <= 5) {
					defaultIds.push(cityId + "_" + occasionId + "_" + pageId + "_" + count);
					count++;
				}

				/* ensure default users are read on initialization */
				engine.get(defaultIds);

				function engineWithDefaults(q, sync, async) {
					if (q === '') {
						sync(engine.get(defaultIds));
						async([]);
					} else {
						engine.search(q, sync, async);
					}
				}

				/* Append to that particular class based in the resolution */
				var $typeAhead = '';
				if ($(document).width() > 600) {
					$typeAhead = $('#desktopGlobalSearchInput');
				} else {
					$typeAhead = $('#mobileGlobalSearchInput');
				}

				$typeAhead.typeahead({
					hint: true,
					minLength: 0
				}, {
					source: engineWithDefaults,
					limit: 8,
					displayKey: 'name',
					templates: {
						suggestion: function (e) {
							var template = '';
							if ((typeof e.is_default !== "undefined") && (e.is_default == "yes")) {
								template = template + '<div class="profileCard-default">' +
									'<img class="profileCard-default-avatar" src="https://gallery.evibe.in/main/img/search/lightning_default.png">' +
									'<div class="profileCard-default-realName">' + e.name + '</div>' +
									'</div>';
							} else {
								template = template + '<div class="profileCard u-cf">';

								if (typeof e.profile_image !== "undefined" && e.profile_image) {
									template += '<img class="profileCard-avatar" src="' + e.profile_image + '">';
								}

								template += '<div class="profileCard-details">' +
									'<div class="profileCard-realName">' + e.name + '</div>' +
									'<div class="profileCard-screenName">#' + e.code + '</div>';

								if ($.isNumeric(e.price_max) && (e.price_max != 0)) {
									template = template + '<div class="profileCard-max-price">&#8377; ' + e.price_max.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '</div>\n';
								}

								template = template + '<div class="profileCard-price">&#8377; ' + e.price_min.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '</div>\n';

								template = template + '</div></div>';
							}

							return template;
						},
						empty: function (e1) {
							return ('<span></span>');
						}
					}
				})
					.on('typeahead:asyncrequest', function () {
						$('.typeahead-spinner').show();
					})
					.on('typeahead:asynccancel typeahead:asyncreceive', function () {
						var suggestionBox = '';
						var inputBox = '';
						if ($(document).width() > 600) {
							suggestionBox = $('.desktop-header-search-bar').find('.tt-dataset');
							inputBox = $('#desktopGlobalSearchInput');

						} else {
							suggestionBox = $('.mobile-header-search-bar').find('.tt-dataset');
							inputBox = $('#mobileGlobalSearchInput');
						}

						/* Adding custom message at the bottom */
						if (inputBox.val() !== '') {
							var $sbParent = suggestionBox.parent();
							if ($sbParent.find('.see-all-results-search-wrap').length) {
								$sbParent.find('.see-all-results-search-wrap').find('i').empty().text(inputBox.val());
							} else {
								$sbParent.append('<div class="see-all-results-search-wrap"><a href="#" class="see-all-results-search">See all results for <i class="text-bold text-e">' + inputBox.val() + '</i></a></div>');
							}

							$('.see-all-results-search').bind("click", function () {
								var inputQuery = '';
								if ($(document).width() > 600) {
									inputQuery = validateSearchInput($('#desktopGlobalSearchInput').val());
									if (inputQuery !== '') {
										var url = $('#globalSearchAction').val() + "?q=" + inputQuery;
										window.location.replace(url);
									}
								} else {
									inputQuery = validateSearchInput($('#mobileGlobalSearchInput').val());
									if (inputQuery !== '') {
										var url = $('#globalSearchAction').val() + "?q=" + inputQuery;
										window.location.replace(url);
									}
								}
							});
						}

						$('.typeahead-spinner').hide();
					});

				$("#mobile-header-search-icon").on("click", function () {
					$(this).addClass("hide");
					$(".mobile-search-btn").toggleClass("hide");
					$typeAhead.focus();
				});

				$("#mobileGlobalSearchOverlay").on("click", function () {
					$("#mobile-header-search-icon").removeClass("hide");
					$(".mobile-search-btn").toggleClass("hide");
				});
			});
		}
	})();

	(function selectCityModal() {
		/* get the variable shared across all the views if the session is not set at home page */
		$('#cityChangeModal a').on('click', function (event) {
			event.preventDefault();
			var url = $(this).data('url');
			var occasion = $('#cityOccasionUrl').val();
			var currentUrl = window.location.href;
			if ((currentUrl.search("/search?") !== -1) && ($("#globalSearchKeyword").length !== 0)) {
				url = url + "/search?q=" + $("#globalSearchKeyword").val();
			} else if (occasion) {
				url = url + "/" + occasion;
			}
			if (location.hash) {
				url = url + location.hash;
			}
			location.href = window.location.protocol + "//" + window.location.hostname + "/" + url;
		});

		$('#citySelectModal a').on('click', function (event) {
			event.preventDefault;
			var url = $(this).data('url');
			var occasion = $('#cityOccasionUrl').val();
			if (occasion) {
				url = url + "/" + occasion;
			}
			if (location.hash) {
				url = url + location.hash;
			}
			location.href = window.location.protocol + "//" + window.location.hostname + "/" + url;
		});

		/* show popup on city click escape, clicking outside the modal to close works here */
		$('.select-city').on('click', function (event) {
			$('#cityChangeModal').modal();
		});

		/* change the image on city of the city modal city-link on hover */
		$('.city-modal .active-cities .city-link').hover(
			/*over*/
			function (event) {
				var colorImg = $(this).data('colorimg');
				$(this).children('img').attr('src', colorImg);
			},

			/* out */
			function (event) {
				var bwImg = $(this).data('bwimg');
				$(this).children('img').attr('src', bwImg);
			}
		);
	})();

	if ($('.mob-promotional-header-wrap').length && $('.mob-promotional-header-close-btn').length) {
		function hidePromotionalHeader() {
			$('.mob-promotional-header-wrap').addClass('hide');
			$('.mobile-header-additional-space').css('height', '55px');
		}

		function showPromotionalHeader() {
			$('.mob-promotional-header-wrap').removeClass('hide').addClass('snow-container');

			$('.mobile-header-additional-space').css('height', '55px');
		}

		showPromotionalHeader();
		/* if (typeof Cookies.get('mobPromoVDayHeaderClose') === 'undefined') {
			showPromotionalHeader();
		} else {
			hidePromotionalHeader();
		} */

		$('.mob-promotional-header-close-btn').on('click', function (event) {
			event.preventDefault();
			hidePromotionalHeader();

			$('#mblPriceFilterSticky').removeClass('sticky-behave').addClass('hide');

			var endOfTheDay = new Date(new Date().setHours(400, 59, 59, 999));
			Cookies.set("mobPromoVDayHeaderClose", '', {expires: endOfTheDay});
		});
	}

	/* Removing all the special characters */
	function validateSearchInput($input) {
		return $input.replace(/"/g, "'").replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, ' ').replace(/ +(?= )/g, '');
	}

});
