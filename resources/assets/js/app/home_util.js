$(document).ready(function () {
	/* collection title and description truncation */
	$('.collection-card-title h3').each(function (i, el) {
		var $el = $(el);
		var story = $el.text();
		var maxLength = 30;
		if (story.length >= maxLength) {
			var lastSpaceIndex = story.substr(0, maxLength).lastIndexOf(' ');
			$el.text(story.substr(0, lastSpaceIndex) + ' ...');
		}
	});

	$('.collection-card-title h6').each(function (i, el) {
		var $el = $(el);
		var story = $el.text();
		var maxLength = 70;
		if (story.length >= maxLength) {
			var lastSpaceIndex = story.substr(0, maxLength).lastIndexOf(' ');
			$el.text(story.substr(0, lastSpaceIndex) + ' ...');
		}
	});

	$('.email-typo-error').on('blur', function () {
		$(this).mailcheck({
			suggested: function (element, suggestion) {
				var errorMsg = "<span class='email-typo-error-msg'>Did you mean " + suggestion.address + "@" + suggestion.domain + "</span>? (Click here to update)";
				new noty({
					text: errorMsg,
					layout: "top",
					type: "warning",
					closeWith: ["click"],
					timeout: 8000
				});

				$(".email-typo-error-msg").closest(".noty_text").closest(".noty_message").on("click", function () {
					$('.email-typo-error').val(suggestion.address + "@" + suggestion.domain);
				});
			}
		});
	});

	var dt = new Date();
	var secs = dt.getSeconds() + (60 * (dt.getMinutes() + (60 * dt.getHours())));
	$('#enquiryTimeSlotOptions option').each(function () {
		if ((secs >= $(this).data("minTime")) && (secs <= $(this).data("maxTime"))) {
			$('#enquiryTimeSlot').append($(this).clone());
		}
	});

	$('#btnSubmitFormHome').click(function (e) {
		e.preventDefault();

		var passiveCity = $("#passiveCity").val();
		if (passiveCity == "NA") {
			var cityUrl = $('#cityUrl').val();
		} else {
			var cityUrl = passiveCity;
		}
		if ((cityUrl == null || cityUrl.length == 0)) {
			cityUrl = 'bangalore';
		}
		var url = '/' + cityUrl + '/book';
		var data = {
			'name': $('#enquiryFormName').val(),
			'phone': $('#enquiryFormPhone').val(),
			'callingCode': $('#enquiryFormCallingCode').val(),
			'email': $('#enquiryFormEmail').val(),
			'partyDate': $('#enquiryFormDate').val(),
			'comments': $('#enquiryFormComments').val(),
			'accepts': $('#acceptsForm').is(':checked') ? 'yes' : 'no',
			'timeSlot': $('#enquiryTimeSlot :selected').val(),
			'planningStatus': $('#enquiryPlanningStatus :selected').val(),
			'passiveCity': passiveCity,
			'inspirationProductUrl': "NA"
		};

		$('#btnSubmitFormHome').prop('disabled', true);
		$('.errors-cnt').addClass('hide');
		window.showLoading();
		if (location.hash) {
			if (location.hash.substr(1) === "fbEnquiry") {
				data["typeSource"] = $('#hidTypeSourceFb').val();
			}
		}

		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'json',
			data: data,
			success: function (data) {
				window.hideLoading();
				$("#btnSubmitFormHome").removeAttr('disabled');
				if (data.success) {
					/* redirect to thank you page */
					if (data.redirectTo) {
						window.location = data.redirectTo;
					} else {
						window.showNotySuccess("Thank you! We've received your request. We will get in touch with you shortly.");
					}
				} else {
					var errors = data.errors;
					window.showNotyError(errors);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				$("#btnSubmitFormHome").removeAttr('disabled');
				$('.errors-cnt').removeClass('hide').text("Sorry, your request could not be saved. Please try again later.");
				window.hideLoading();
				window.notifyTeam({
					"url": url,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});

	});
});