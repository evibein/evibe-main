(function partnerAppLoading() {
	$(document).ready(function () {
		$(window).load(function () {
			if ($(".loading-screen-gif").length > 0) {
				$(".nav_menu").css("display", "none");
				$("footer").css("display", "none");
				$(".loading-screen-gif").css("display", "none");

				window.onbeforeunload = function () {
					$(".loading-screen-gif").css("display", "table");
				};
			}
		});
	});
})();

(function init() {

	/* var $height = $(window).height() - 30; */
	/* $('.right_col').attr('style', 'min-height:' + $height + 'px'); */

	window.showPartnerNotyError = function (errorText) {
		if (!errorText) errorText = 'An error occurred while submitting your request. Please try again later!';
		noty({
			text: errorText,
			layout: 'top',
			type: 'error',
			closeWith: ['click', 'hover'],
			timeout: 7500
		});
	};

	window.showPartnerNotySuccess = function (successText) {
		if (!successText) successText = 'Your response has been submitted successfully.';
		noty({
			text: successText,
			layout: 'top',
			type: 'success',
			closeWith: ['click', 'hover'],
			timeout: 5000
		});
	};

	window.partnerDashBoardNotifyTeam = function (data) {
		$.ajax({
			"url": "/report/notify/error",
			"type": "POST",
			"dataType": "JSON",
			"data": data
		});
	};

	$('#vendorPackageInclusions').wysihtml5({
		toolbar: {
			"image": false,
			'html': false,
			'blockquote': false,
			'size': 'xs'
		}
	});

	if ($("#check_in, #check_out").length) {
		$("#check_in").datetimepicker({
			datepicker: false,
			formatTime: 'g:i A',
			format: 'g:i A',
			step: 30,
			onSelectTime: function (ct, $i) {
				$i.parent().addClass('is-dirty');
			}
		});
	}

	$('input[name="bookingRange"]').daterangepicker({
		autoUpdateInput: false
	});

	$("#waitingModal, #modalPartnerLoading").modal({
		backdrop: 'static',
		keyboard: false,
		show: false
	});

	window.showPartnerWaitingModal = function () {
		$("#waitingModal").modal("show");
	};

	window.hidePartnerWaitingModal = function () {
		$("#waitingModal").modal("hide");
	};

	window.showPartnerLoading = function () {
		$('#modalPartnerLoading').modal('show');
	};

	window.hidePartnerLoading = function () {
		$('#modalPartnerLoading').modal('hide');
	};

	$('.dashboard-start-date, .dashboard-end-date')
		.css({'cursor': 'pointer'})
		.on('click', function (e) {
			e.preventDefault();
			$('html, body').animate({
				scrollTop: $(".completed-orders-wrap").offset().top
			}, 1000);
			$('#bookingRange').trigger("click");
		});

	$("#minSettlementProcessDate, #maxSettlementProcessDate").datetimepicker({
		timepicker: false,
		format: 'Y/m/d',
		scrollInput: false,
		scrollMonth: false,
		scrollTime: false,
		closeOnDateSelect: true
	});
})();

(function loadExtraDataInDashboard() {
	if ($('#isDashboardHome').val() == 1) {
		var today = new Date();
		var mondayOfWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay() - 6);
		var sundayOfWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay());

		$.ajax({
			url: '/partner/dashboard?isAjax=1&from=' + mondayOfWeek.getTime() / 1000 + '&to=' + sundayOfWeek.getTime() / 1000,
			dataType: 'html',
			type: "GET",
			success: function (data) {
				$('.dashboard-home-container').append(data);
				sendSecondRequest();
			},
			error: function (jqXHR, textStatus, errorThrown) {
				sendSecondRequest();
				window.partnerDashBoardNotifyTeam({
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});

		function sendSecondRequest() {
			$.ajax({
				url: '/partner/deliveries?isAjax=1',
				dataType: 'html',
				type: "GET",
				success: function (data) {
					$('.dashboard-home-container').append(data);
				},
				error: function (jqXHR, textStatus, errorThrown) {
					window.partnerDashBoardNotifyTeam({
						"textStatus": textStatus,
						"errorThrown": errorThrown
					});
				}
			});
		}
	}
})();

(function getMessageNotifications() {
	if ($('#isLoginPage').val() == 2) {
		$.ajax({
			url: '/partner/get/notifications/messages',
			dataType: 'html',
			type: "POST",
			success: function (data) {
				$('.top_nav_msg_list').append(data);
			},
			error: function (jqXHR, textStatus, errorThrown) {
				window.partnerDashBoardNotifyTeam({
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});

		$.ajax({
			url: '/partner/get/notifications/count',
			dataType: 'json',
			type: "POST",
			success: function (data) {
				$('.top_nav_msg_count').text(data.count);
			},
			error: function (jqXHR, textStatus, errorThrown) {
				window.partnerDashBoardNotifyTeam({
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});
	}
})();

(function enableDropzoneRemoveButton() {
	if($("#drop-zone").length > 0) {
		var dropZoneId = "drop-zone";
		var buttonId = "clickHere";
		var mouseOverClass = "mouse-over";
		var dropZone = $("#" + dropZoneId);
		var inputFile = dropZone.find("input");
		var finalFiles = {};
		$(function () {
			var ooleft = dropZone.offset().left;
			var ooright = dropZone.outerWidth() + ooleft;
			var ootop = dropZone.offset().top;
			var oobottom = dropZone.outerHeight() + ootop;

			document.getElementById(dropZoneId).addEventListener("dragover", function (e) {
				e.preventDefault();
				e.stopPropagation();
				dropZone.addClass(mouseOverClass);
				var x = e.pageX;
				var y = e.pageY;

				if (!(x < ooleft || x > ooright || y < ootop || y > oobottom)) {
					inputFile.offset({
						top: y - 15,
						left: x - 100
					});
				} else {
					inputFile.offset({
						top: -400,
						left: -400
					});
				}

			}, true);

			if (buttonId != "") {
				var clickZone = $("#" + buttonId);

				var oleft = clickZone.offset().left;
				var oright = clickZone.outerWidth() + oleft;
				var otop = clickZone.offset().top;
				var obottom = clickZone.outerHeight() + otop;

				$("#" + buttonId).mousemove(function (e) {
					var x = e.pageX;
					var y = e.pageY;
					if (!(x < oleft || x > oright || y < otop || y > obottom)) {
						inputFile.offset({
							top: y - 15,
							left: x - 160
						});
					} else {
						inputFile.offset({
							top: -400,
							left: -400
						});
					}
				});
			}

			document.getElementById(dropZoneId).addEventListener("drop", function (e) {
				$("#" + dropZoneId).removeClass(mouseOverClass);
			}, true);

			inputFile.on('change', function (e) {
				finalFiles = {};
				$('#filename').html("");
				var fileNum = this.files.length,
					initial = 0,
					counter = 0;

				$.each(this.files, function (idx, elm) {
					finalFiles[idx] = elm;
				});

				for (initial; initial < fileNum; initial++) {
					counter = counter + 1;
					$('#filename').append('<div id="file_' + initial + '"><span class="fa-stack fa-lg"><i class="fa fa-file fa-stack-1x "></i><strong class="fa-stack-1x" style="color:#FFFFFF; font-size:12px; margin-top:2px;">' + counter + '</strong></span> ' + this.files[initial].name + '&nbsp;&nbsp;</div>');
				}
			});

		});
	}
})();

(function firstFormSubmit() {
	$('#addPackageFirstForm').submit(function (e) {
		e.preventDefault();

		var btnSubmit = $('.add-package-first-submit');
		var btnWait = $('.add-package-first-submit-waiting');

		function hideSubmitButton() {
			btnSubmit.addClass('hide');
			btnWait.removeClass('hide');
		}

		function showSubmitButton() {
			btnSubmit.removeClass('hide');
			btnWait.addClass('hide');
		}

		var formUrl = $(this).attr('action');
		hideSubmitButton();

		$.post(formUrl, {"partnerPackageType": $('#partnerPackageType').val()})
			.done(function (data, textStatus, jqXHR) {
				if (data.success) {
					location.reload();
				} else {
					showSubmitButton();
					window.showNotyError("An error occurred while submitting your request. Please try again later.");
				}
			}).fail(function (data, textStatus, jqXHR) {
			showSubmitButton();
			window.showNotyError("An error occurred while submitting your request. Please try again later.");
			window.notifyTeam({
				"url": formUrl,
				"textStatus": textStatus,
				"errorThrown": errorThrown
			});
		});
	});
})();

(function secondFormSubmit() {

	/* function enableOnReload() {
	 /* 	window.onbeforeunload = function () {
	 /* 		return "Are you sure you want to leave, unsaved data will be lost?";
	 /* 	};
	 /* }

	 /* function disableOnReload() {
	 /* 	window.onbeforeunload = function () {
	 /* 	};
	 /* }

	 /* if ($('#loadPreventReloadModule').val() == 1) {
	 /* 	enableOnReload();
	 /* }

	 /* $('#addPackageSecondForm').submit(function (e) {
	 /* 	e.preventDefault();
	 //
	 /* 	var btnSubmit = $('.add-package-second-submit');
	 /* 	var btnWait = $('.add-package-second-submit-waiting');
	 /* 	disableOnReload();
	 //
	 /* 	function hideSubmitButton() {
	 /* 		btnSubmit.addClass('hide');
	 /* 		btnWait.removeClass('hide');
	 /* 	}
	 //
	 /* 	function showSubmitButton() {
	 /* 		btnSubmit.removeClass('hide');
	 /* 		btnWait.addClass('hide');
	 /* 	}
	 //
	 /* 	var formUrl = $(this).attr('action') + "?typePackageId=" + $('#typePackageId').val();
	 /* 	hideSubmitButton();
	 //
	 /* 	$.ajax({
	 /* 		url: formUrl,
	 /* 		type: "POST",
	 /* 		data: new FormData(this),
	 /* 		cache: false,
	 /* 		processData: false,
	 /* 		contentType: false,
	 /* 		success: function (data) {
	 /* 			hideSubmitButton();
	 /* 			if (data.success) {
	 /* 				window.location = data.redirectUrl;
	 /* 			}
	 /* 			else if (data.error) {
	 /* 				showSubmitButton();
	 /* 				enableOnReload();
	 /* 				window.showPartnerNotyError(data.error);
	 /* 			}
	 /* 			else {
	 /* 				showSubmitButton();
	 /* 				enableOnReload();
	 /* 				window.showPartnerNotyError("Some error occurred while submitting your request")
	 /* 			}
	 /* 		},
	 /* 		error: function (jqXHR, textStatus, errorThrown) {
	 /* 			showSubmitButton();
	 /* 			window.showNotyError("Some error occurred while submitting your request, Please reload the page &
	 /* try again"); window.notifyTeam({ "url": url, "textStatus": textStatus, "errorThrown": errorThrown }); } });  }); */
})();

(function packageReviewSubmit() {
	$('.review-package-submit').on('click', function submitPackage() {
		$('#modalSubmitUpdateReview').modal("show");
	})
})();

(function contactUsForm() {
	$('.btnContactEvibe').css('cursor', 'pointer');

	$('.btnContactEvibe').on('click', function (event) {
		event.preventDefault();
		var modal = $('#contactModal');

		$('#errorBox').addClass('hide');
		modal.modal('show');
		modal.find('#message').val('');
		modal.find('#subject').val('');
	});

	$('#btnContactSubmit').click(function (event) {
		event.preventDefault();

		var modal = $('#contactModal');
		var errorSelector = $('#errorBox');
		var url = modal.data('url');
		var subject = $('#subject').val();
		var message = $('#message').val();

		if (!subject) {
			errorSelector.removeClass('hide').text('Please enter subject.');
			return false;
		}
		if (!message) {
			errorSelector.removeClass('hide').text('Please enter message.');
			return false;
		}

		modal.modal('hide');
		window.showPartnerLoading();

		$.ajax({
			type: "POST",
			dataType: "JSON",
			url: url,
			data: {subject: subject, message: message},
			success: function (data) {
				window.hidePartnerLoading();

				if (data.success) {
					$('#partnerQuerySuccessModal').modal('show');
				} else if (data.error) {
					modal.modal('show');
					errorSelector.removeClass('hide').text(data.error);
				} else {
					window.showPartnerNotyError("Some error occurred, please contact our team");
				}

			},
			fail: function () {
				window.hidePartnerLoading();
				window.showPartnerNotyError("Some error occurred, please contact our team");
			}
		});
	});
})();

(function partnerDigestForm() {
	var btnSubmit = $('#formPartnerReviewBtnSubmit');

	function showSubmitButton() {
		btnSubmit.attr('disabled', false);
		btnSubmit.find('.default').removeClass('hide');
		btnSubmit.find('.waiting').addClass('hide');
	}

	function disableSubmitButton() {
		btnSubmit.attr('disabled', true);
		btnSubmit.find('.default').addClass('hide');
		btnSubmit.find('.waiting').removeClass('hide');
	}

	$('.review_btn').click(function (event) {
		event.preventDefault();

		$('#fromSubmitPartnerResponse').attr('data-url', $(this).data('url'));
		/* open the modal */
		var biddingModal = $('#modalSubmitPartnerResponse');
		biddingModal.modal({
			'show': true,
			'backdrop': 'static'
		});
	});

	/* submit the form */
	$('#fromSubmitPartnerResponse').submit(function (e) {
		e.preventDefault();

		var formUrl = $(this).data('url');
		disableSubmitButton();

		$.post(formUrl, {'review': $("#partnerStatus").val()})
			.done(function (data, textStatus, jqXHR) {
				if (data.success) {
					location.reload();
				} else {
					showSubmitButton();
					window.showNotyError("An error occurred while submitting your request. Please try again later.");
				}
			}).fail(function (data, textStatus, jqXHR) {
			showSubmitButton();
			window.showNotyError("An error occurred while submitting your request. Please try again later.");
			window.notifyTeam({
				"url": url,
				"textStatus": textStatus,
				"errorThrown": errorThrown
			});
		});
	});

	/* show details on click more details button */
	$('.deliver-more-details-btn').on('click', function () {
		var bookingType = $(this).attr('data-bookType');
		var response = $(this).attr('data-response');
		var feedback = $(this).attr('data-feedback');

		$("#deliveryMoreDetails").find('.booking-type').html(bookingType);
		$("#deliveryMoreDetails").find('.partner-response').html(response);
		$("#deliveryMoreDetails").find('.customer-feedback').html(feedback);

		$("#modalDeliveryMoreDetails").modal({
			'show': true,
			'backdrop': 'static'
		});
	})
})();

(function partnerEnquiryReply() {
	var btnSubmit = $('#formPartnerEnquiryReviewBtnSubmit');
	var avlCheckConfigValue = $('#avlCheckConfigValue').val();
	var customQuoteConfigValue = $('#customQuoteConfigValue').val();

	$('.btn-partner-enquiry-reply').on('click', function (e) {
		e.preventDefault();
		var reply = $(this).data('reply');
		var url = $(this).data('url');
		var btnClass = $(this).data('class');
		var text = $(this).data('text');
		btnSubmit.removeClass('btn-primary btn-success btn-danger');
		btnSubmit.addClass(btnClass);
		btnSubmit.find('.default').text(text);
		btnSubmit.attr("data-reply", reply);
		btnSubmit.attr("data-type", avlCheckConfigValue);
		btnSubmit.attr("data-url", url);

		$('#fromSubmitPartnerEnquiryResponse')[0].reset();
		$('.partner-enquiry-quote-input').addClass('hide');

		var replyModal = $('#modalSubmitPartnerEnquiryResponse');
		replyModal.modal({
			'show': true,
			'backdrop': 'static'
		});
	});

	$('.btn-partner-enquiry-quote-reply').on('click', function (e) {
		e.preventDefault();
		var url = $(this).data('url');
		var btnClass = $(this).data('class');
		var text = $(this).data('text');

		btnSubmit.attr("data-reply", "");
		btnSubmit.attr("data-type", customQuoteConfigValue);
		btnSubmit.attr("data-url", url);

		btnSubmit.removeClass('btn-primary btn-success btn-danger');
		btnSubmit.addClass(btnClass);
		btnSubmit.find('.default').text(text);

		$('#fromSubmitPartnerEnquiryResponse')[0].reset();
		$('.partner-enquiry-quote-input').removeClass('hide');

		var replyModal = $('#modalSubmitPartnerEnquiryResponse');
		replyModal.modal({
			'show': true,
			'backdrop': 'static'
		});
	});

	function showSubmitButton() {
		btnSubmit.attr('disabled', false);
		btnSubmit.find('.default').removeClass('hide');
		btnSubmit.find('.waiting').addClass('hide');
	}

	function disableSubmitButton() {
		btnSubmit.attr('disabled', true);
		btnSubmit.find('.default').addClass('hide');
		btnSubmit.find('.waiting').removeClass('hide');
	}

	btnSubmit.on('click', function (e) {
		e.preventDefault();

		disableSubmitButton();
		var isValidated = 1;
		var $enquiryType = $(this).attr("data-type");

		var partnerQuote = $('#partnerQuote').val();
		var partnerComments = $('#partnerComments').val();

		var decision = $(this).attr('data-reply');
		var text = partnerComments;

		if ($enquiryType != avlCheckConfigValue) {
			if (partnerQuote === "") {
				isValidated = 2;
				showSubmitButton();
				showPartnerNotyError("Please enter your best quote");
			}
			text = partnerQuote + "." + partnerComments;
		}

		var url = btnSubmit.attr('data-url');

		if (isValidated == 1) {
			$.ajax({
				url: url,
				type: "POST",
				data: {"typeId": $enquiryType, "replyDecision": decision, "replyText": text},
				success: function (data) {
					disableSubmitButton();
					if (data.success) {
						window.showPartnerNotySuccess(data.successMessage);
						setTimeout(function () {
							window.location.reload(1);
						}, 3000);
					} else if (data.error) {
						showSubmitButton();
						window.showPartnerNotyError(data.error);
					} else {
						window.showPartnerNotyError("Some error occurred while submitting your request")
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					showSubmitButton();
					window.showPartnerNotyError("Some error occurred while submitting your request, Please reload the page & try again");
					window.notifyTeam({
						"url": url,
						"textStatus": textStatus,
						"errorThrown": errorThrown
					});
				}
			});
		}
	});
})();

(function partnerDelivery() {
	$('.uploadDeliveryImagesSubmitButton').on('click', function (e) {
		e.preventDefault();
		$(this).attr('disabled', true);
		$('#uploadDeliveryImages').submit();
	});
})();

(function bookingDateRange() {
	$('#bookingRange').on('apply.daterangepicker', function (ev, picker) {
		showPartnerWaitingModal();
		/* $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY')); */

		var url = $('#postUrl').val();

		$.post(url, {
			'startDate': $('#bookingRange').data('daterangepicker').startDate.format('DD-MM-YYYY'),
			'endDate': $('#bookingRange').data('daterangepicker').endDate.format('DD-MM-YYYY')
		})
			.done(function (data, textStatus, jqXHR) {
				if (data.success) {
					window.location.href = data.redirectUrl;
				} else {
					hidePartnerWaitingModal();
					window.showPartnerNotyError("An error occurred while fetching your request. Please reload the page and try again.");
				}
			}).fail(function (data, textStatus, jqXHR) {
			window.showPartnerNotyError("An error occurred while submitting your request. Please reload and try again.");
			window.notifyTeam({
				"url": url,
				"textStatus": textStatus,
				"errorThrown": errorThrown
			});
		});
	});
})();

(function getPartnerSettlements() {
	$('#getSettlementsBtn').on('click', function () {
		var url = $(this).data('url');
		url += '?minPDate=' + $('#minSettlementProcessDate').val() + '&maxPDate=' + $('#maxSettlementProcessDate').val();
		window.location.href = url;
	});
})();