const CACHE = "pwabuilder-page";
const offlineFallbackPage = "/no_network.html";

self.addEventListener("install", function (event) {
	event.waitUntil(
		caches.open(CACHE).then(function (cache) {
			return cache.addAll(['/no_network.html']);
		})
	);
});

self.addEventListener("fetch", function (event) {
	if (event.request.method !== "GET") return;
	event.respondWith(
		fetch(event.request)
			.then(function (response) {
				// If request was success, add or update it in the cache
				event.waitUntil(updateCache(event.request, response.clone()));
				return response;
			})
			.catch(function (error) {
				return fromCache(event.request);
			})
	);
});

self.addEventListener("refreshOffline", function () {
	const offlinePageRequest = new Request(offlineFallbackPage);
	return fetch(offlineFallbackPage).then(function (response) {
		return caches.open(CACHE).then(function (cache) {
			return cache.put(offlinePageRequest, response);
		});
	});
});

function fromCache(request) {
	return caches.open(CACHE).then(function (cache) {
		return cache.match(request).then(function (matching) {
			if (!matching || matching.status === 404) {
				/*The following validates that the request was for a navigation to a new document*/
				if (request.destination !== "document" || request.mode !== "navigate") {
					return Promise.reject("no-match");
				}
				return cache.match(offlineFallbackPage);
			}
			return matching;
		});
	});
}

function updateCache(request, response) {
	return caches.open(CACHE).then(function (cache) {
		return cache.put(request, response);
	});
}