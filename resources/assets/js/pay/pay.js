/**
 * Handle coupons, payments
 *
 * @author Anji <anji@evibe.in> @since 22 Sep 2018
 */

$(document).ready(function () {

	$('.section-why-us').css('background', '#FFFFFF');

	var couponCodeVal = '';
	var couponDiscountAmount = 0;
	var internationalPaymentHandlingFee = 0;
	var walletDiscount = 0;

	(function CouponInits() {
		/* Declaring outside because customer has the access to edit these input values, will be problem while fetching */
		var ticketId = $('#ticketId').val();
		var totalAdvance = $('#totalAdvance').val();

		$('.coupon-code-apply-button').on('click', function () {
			/* Hide all the existing notifications */
			$('.coupon-success-msg-wrap').addClass('hide');
			$('.coupon-error-msg-wrap').addClass('hide');
			couponDiscountAmount = 0;

			var name = $('#inpName').val();
			var phone = $('#inpPhone').val();

			/* For normal booking tickets, customer do not have access to modify the email address */
			if ($("#inpEmail")[0]) {
				var email = $('#inpEmail').val();
			} else {
				email = $('#ticketEmailIdForNormalBooking').val();
			}

			var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			var couponCode = $('#couponCode').val();

			if (!name || !phone || !email) {
				showNotyError("Please enter your name, phone & email to apply the coupon code");
			} else if (!regex.test(email)) {
				showNotyError("Please enter a valid Email Address");
			} else if (phone.length != 10) {
				showNotyError("Please enter a valid Phone number");
			} else if (!couponCode && couponCode.length < 6) {
				showNotyError("Please enter valid coupon code");
			} else {
				window.showLoading();

				var data = {
					'name': name,
					'phone': phone,
					'email': email,
					'couponCode': couponCode,
					'ticketId': ticketId,
					'totalAdvance': totalAdvance,
					'couponToken': $('#couponSecretKey').val(),
					'isVenueDeal': $('#isVenueDeal').length ? $('#isVenueDeal').val() : 0
				};
				$.ajax({
					url: '/coupon/validate',
					type: 'POST',
					dataType: 'json',
					data: data
				}).done(function (data, textStatus, jqXHR) {
					if (data.success == "yes") {
						window.hideLoading();
						updateCouponCodeVal('');

						if (data.couponExist == "no") {
							$('.coupon-error-msg').text("Sorry, the coupon code is not valid.");
							$('.coupon-success-msg-wrap').addClass('hide');
							$('.coupon-error-msg-wrap').removeClass('hide');

							couponDiscountAmount = 0;
							updatePriceDetails();
						} else {
							couponDiscountAmount = parseFloat(data.discountAmount, 10);
							updatePriceDetails();

							if (data.discountAmount <= 0) {
								$('.coupon-error-msg').text("Sorry, the coupon code is not valid.");
								$('.coupon-success-msg-wrap').addClass('hide');
								$('.coupon-error-msg-wrap').removeClass('hide');
							} else {
								$('.coupon-code-apply-button').addClass('hide');
								$('.coupon-code-remove-button').removeClass('hide');
								$('#couponCode').attr('disabled', true).css({cursor: "not-allowed"});
								$('.coupon-error-msg-wrap').addClass('hide');
								$('.coupon-success-msg-wrap').removeClass('hide');

								if (data.discountAmount > 0) {
									/* Updating the discount amount in the success notification */
									$('.coupon-discount-amount').text(data.discountAmount);

									updateCouponCodeVal($('#couponCode').val());
								}
							}
						}
					} else if (data.success == "no") {
						window.hideLoading();
						showNotyError(data.error);
					}
				}).fail(function (jqXHR, textStatus, errorThrown) {
					window.notifyTeam({
						"url": "/coupon/validate",
						"textStatus": textStatus,
						"errorThrown": errorThrown
					});
				});
			}
		});

		$('.coupon-code-remove-button').on('click', function () {
			/* Hide all the existing notifications */
			$('.coupon-success-msg-wrap').addClass('hide');
			$('.coupon-error-msg-wrap').addClass('hide');
			couponDiscountAmount = 0;
			updatePriceDetails();

			$('.coupon-code-remove-button').addClass('hide');
			$('.coupon-code-apply-button').removeClass('hide');
			$('#couponCode').attr('disabled', false).css({cursor: "default"}).val('');
		});
	})();

	(function EvibeWallet() {
		$('#walletAmount').on('click', function (e) {
			showLoading();

			if (this.checked) {
				walletDiscount = parseFloat($("#walletMaxUsage").val(), 10);
			} else {
				walletDiscount = 0;
			}

			updatePriceDetails();
		});
	})();

	(function InternationalPayments() {
		$('#additionalCharges').on('click', function (e) {
			showLoading();

			if (this.checked) {
				internationalPaymentHandlingFee = parseFloat(Math.round(totalAdvanceAmount * 0.035), 10);

				$("#paymentMethod").remove();
				$("#paymentMethodPayU").remove();
				$(".payment-txn-inputs").append($("#retryPaymentMethod").clone().attr("id", "paymentMethod"));
				$(".payment-txn-inputs").append($("#retryPaymentMethod").clone().attr("id", "paymentMethodPayU"));
			} else {
				internationalPaymentHandlingFee = 0;

				$("#paymentMethod").remove();
				$("#paymentMethodPayU").remove();
				$(".payment-txn-inputs").append($("#retryPaymentMethodPayu").clone().attr("id", "paymentMethod"));
				$(".payment-txn-inputs").append($("#retryPaymentMethodPayu").clone().attr("id", "paymentMethodPayU"));
			}

			updatePriceDetails();
		});
	})();

	$('.lazy-loading').each(function () {
		var img = $(this);
		img.attr('src', img.data('src'));
	});

	function updateCouponCodeVal(val) {
		couponCodeVal = val;
	}

	/* payment related code */
	/* use variable for which values doesn't depend on element rendering */
	var ticketId = $('#ticketId').val();
	var userInput = {};
	var presentCheckoutCardCount = 0;
	var nextCheckoutCardCount = 0;
	var maxCheckoutCardCount = 0;

	/* check if browser is Google chrome */
	if (navigator.userAgent.toLowerCase().indexOf('chrome') == -1) {
		$('.chromeframe').removeClass('hide');
	}

	/* if total advance paid, disable input fields */
	if (!parseInt($('#advanceAmount').val(), 10)) {
		$('input[type="text"], textarea, select').attr("disabled", "disabled");
	}

	function reportAjaxFail(that, error) {
		if (that) that.removeClass('hide');
		error = error ? error : "";
		window.hideLoading();
		window.showNotyError(error);
	}

	function razorCapturePayment(response) {
		window.showLoading();
		var url = $('#razorPaySuccessUrl').val();
		var data = {
			'ticketId': ticketId,
			'razorpay_payment_id': response.razorpay_payment_id,
			'isVenueDeal': $('#isVenueDeal').length ? $('#isVenueDeal').val() : 0
		};

		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'json',
			data: data
		}).done(function (data, textStatus, jqXHR) {
			if (data.success) {
				/* form POST to intermediate page */
				$('#redirectToIntermediateForm').attr('action', data.redirectTo).submit();
			} else reportAjaxFail(null, data.error);
		}).fail(function (jqXHR, textStatus, errorThrown) {
			reportAjaxFail(null, null)
		});
	}

	function processRazorPostInit(data, paymentType) {
		var razorPayKey = $("#razorPayKey").val();
		var amount = parseInt(data.amount, 10);
		var merchant = $("#razorPayMerchant").val();
		var logo = $("#razorPayLogo").val();
		var $inpEmail = $('#inpEmail');
		var email = parseInt($inpEmail.length, 10) && $inpEmail.val() ? $inpEmail.val() : $('#custEmail').val();

		/* razorPay modal */
		var options = {
			key: razorPayKey,
			amount: amount,
			name: merchant,
			description: "Ticket #" + ticketId,
			image: logo,
			handler: function (response) {
				razorCapturePayment(response);
				/* capture response */
			},
			prefill: {
				name: $('#inpName').val(),
				email: email,
				contact: $('#inpPhone').val(),
				method: paymentType ? paymentType : ""
			},
			modal: {
				ondismiss: function () {
					/* show make payment button */
					$('#payBtn').removeClass('hide');
				}
			}
		};

		window.hideLoading();
		var rzp1 = new Razorpay(options);
		rzp1.open();
	}

	function processPayUPostInit(data) {
		var payU = data.payu;

		/* submit PayUMoney form */
		if (payU) {
			$('#key').attr('value', payU.key);
			$('#txnid').attr('value', payU.txnid);
			$('#amount').attr('value', payU.amount);
			$('#productinfo').attr('value', payU.productinfo);
			$('#firstname').attr('value', payU.firstname);
			$('#email').attr('value', payU.email);
			$('#phone').attr('value', payU.phone);
			$('#surl').attr('value', payU.surl);
			$('#furl').attr('value', payU.furl);
			$('#hash').attr('value', payU.hash);
			$('#payUMoneyForm').submit();
		}
	}

	function processPaytmInit(data) {
		var paytm = data.paytm;

		/* submit Paytm form */
		if (paytm) {
			$('#paytmMId').attr('value', paytm.MID);
			$('#paytmOrderId').attr('value', paytm.ORDER_ID);
			$('#paytmCustomerId').attr('value', paytm.CUST_ID);
			$('#paytmIndustryTypeId').attr('value', paytm.INDUSTRY_TYPE_ID);
			$('#paytmChannelId').attr('value', paytm.CHANNEL_ID);
			$('#paytmTransactionAmount').attr('value', paytm.TXN_AMOUNT);
			$('#paytmWebsite').attr('value', paytm.WEBSITE);
			$('#paytmChecksumHash').attr('value', paytm.checksum);
			$('#paytmSuccessUrl').attr('value', paytm.CALLBACK_URL);
			$('#paytmForm').submit();
		}
	}

	function ajaxTicketAlertToTeam() {
		if ($('#isAutoBooking').length && $('#isAutoBooking').val()) {
			var $phone = $('#inpPhone');
			var url = $phone.data('url');
			if (($phone.val() != $phone.data('old-phone')) &&
				!(isNaN($phone.val())) &&
				$phone.val().length == 10) {
				$.ajax({
					url: url,
					dataType: 'json',
					type: 'POST',
					data: {
						phone: $phone.val()
					},
					success: function (data) {
						if (data.success) {
							$phone.data('old-phone', data.oldPhone);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						window.notifyTeam({
							"url": url,
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			}
		}
	}

	/* make ajax request on entering mobile number */
	/* to alert team for auto booking options */
	$('#inpPhone').on('blur', ajaxTicketAlertToTeam);

	/* process payment */
	$('#payBtn').click(function (event) {
		event.preventDefault();

		window.showLoading();
		var that = $(this);
		that.addClass('hide');

		var selectedPaymentMethod = $("#paymentMethod");
		var initUrl = $(selectedPaymentMethod).data('url');
		var gateway = $(selectedPaymentMethod).val();
		var specialNotes = "";

		$(".special-notes-wrap").each(function () {
			if ($(this).find(".special-notes").val() != "") {
				specialNotes = specialNotes + '<div style="font-size: 11px; color: #636363"><i>' + $(this).find(".special-notes-title").text() + '</i></div><ul><li>' + $(this).find(".special-notes").val() + '</li></ul><span></span>';
			}
		});

		userInput.name = $('#inpName').val();
		userInput.phone = $('#inpPhone').val();
		userInput.callingCode = $('#inpCallingCode').val();
		userInput.altPhone = $('#altPhone').val();
		userInput.splNotes = specialNotes;
		userInput.advanceAmount = $('#advanceAmount').val();
		userInput.acceptsTerms = 1;
		userInput.isVenueDeal = $('#isVenueDeal').length ? $('#isVenueDeal').val() : 0;
		userInput.title = $('#inpTitle').val();
		userInput.customerSource = $('#inpCustomerSource').val();

		/* if auto booking, add email id */
		var $email = $('#inpEmail');
		if (parseInt($email.length, 10) === 1) {
			userInput.email = $email.val();
		}

		/* add area id of the ticket */
		userInput.preDefinedAreaId = $('#preDefinedAreaId').val();

		/* add party date and time */
		userInput.partyDate = $('#inpPartyDate').val();
		userInput.partyTime = $('#inpPartyTime').val();

		/* add upload truth value */
		userInput.uploadImage = $('.checkout-page-2 #uploadImage').val();
		userInput.uploadSuccess = $('#hidUploadSuccess').val();

		/* add venue info only for not venue booking */
		$venueAddressLine1 = $('#venueAddressLine1');
		if (parseInt($venueAddressLine1.length, 10) === 1) {
			userInput.venueAddressLine1 = $venueAddressLine1.val();
			userInput.venueAddressLine2 = $('#venueAddressLine2').val();
			userInput.venueLocation = $('#venueLocation').val();
			userInput.venueLandmark = $('#venueLandmark').val();
			userInput.venueZipCode = $('#venueZipCode').val();
			userInput.venueTypeId = $('#typeVenue').val();
			userInput.mapAddress = $('#mapAddress').data('address');
			userInput.lat = $('#mapLat').data('lat');
			userInput.lng = $('#mapLng').data('lng');
			userInput.locationDetails = $('.google-location-details').val();
		}

		userInput.isChristmasCampaign = $('#christmasCampaign').val() == 1 ? 1 : 0;
		userInput.couponCode = couponCodeVal;
		userInput.internationalPayment = $('#additionalCharges:checked').length;
		userInput.walletDiscount = $('#walletAmount:checked').length;

		/* get the dynamic field value */
		/*
		var $dynamicFields = $('#dynamicFieldsUser');
		if ($dynamicFields.length > 0) {
			var dynamicFields = JSON.parse($dynamicFields.val());
			$.each(dynamicFields, function (key, value) {
				var name = value['name'];
				var fieldType = value['type_field_id'];
				if (fieldType == $('#hidInputTypeCheckbox').val()) {
					var checkedFields = $('input[name="' + name + '"]:checked');
					var checkboxValue = '';
					$.each(checkedFields, function (key, value) {
						if (checkboxValue !== '') {
							checkboxValue = checkboxValue + ', ';
						}
						checkboxValue = checkboxValue + $(this).val();
					});
					userInput[name] = checkboxValue;
				} else {
					userInput[name] = $('#' + name).val();
				}
			});
		}
		*/

		/* get the dynamic field value */
		var $dynamicFields = $('#dynamicFieldsUser');
		var checkoutFieldValues = {};
		if ($dynamicFields.length > 0) {
			var dynamicFields = JSON.parse($dynamicFields.val());
			$.each(dynamicFields, function (key, value) {
				var name = value['booking_checkout_unique_id'];
				var fieldType = value['type_field_id'];
				if (fieldType == $('#hidInputTypeCheckbox').val()) {
					var checkedFields = $('input[name="' + name + '"]:checked');
					var checkboxValue = '';
					$.each(checkedFields, function (key, value) {
						if (checkboxValue !== '') {
							checkboxValue = checkboxValue + ', ';
						}
						checkboxValue = checkboxValue + $(this).val();
					});
					checkoutFieldValues[name] = checkboxValue;
				} else {
					checkoutFieldValues[name] = $('#' + name).val();
				}
			});
		}

		userInput.checkoutFieldValues = checkoutFieldValues;

		/* get the customer data dynamic field value */
		var $dynamicFields = $('#dynamicFieldsCustomerData');
		if ($dynamicFields.length > 0) {
			var dynamicFields = JSON.parse($dynamicFields.val());
			$.each(dynamicFields, function (key, value) {
				var name = value['name'];
				var fieldType = value['type_field_id'];
				if (fieldType == $('#hidInputTypeCheckbox').val()) {
					var checkedFields = $('input[name="' + name + '"]:checked');
					var checkboxValue = '';
					$.each(checkedFields, function (key, value) {
						if (checkboxValue !== '') {
							checkboxValue = checkboxValue + ', ';
						}
						checkboxValue = checkboxValue + $(this).val();
					});
					userInput[name] = checkboxValue;
				} else {
					userInput[name] = $('#' + name).val();
				}
			});
		}

		/* service selection */
		userInput.eInvite = $('#eInvite').prop("checked");
		userInput.eTYCard = $('#eTYCard').prop("checked");

		$.ajax({
			url: initUrl,
			type: 'POST',
			dataType: 'json',
			data: userInput
		}).done(function (data, textStatus, jqXHR) {
			if (data.success) {
				if (gateway == "razor") {
					var paymentType = selectedPaymentMethod.data("paymentType");
					processRazorPostInit(data, paymentType);
				}
				if (gateway == "payU") {
					processPayUPostInit(data);
				}
				if (gateway == "freeCharge") {
					showNotyError("Selected freecharge payment option is not active right now, Please use other payment options to complete your payment");
				}
				if (gateway == "paytm") {
					processPaytmInit(data);
				}
			} else {
				reportAjaxFail(that, data.error);
			}
		}).fail(function (jqXHR, textStatus, errorThrown) {

			reportAjaxFail(that, null);
		});
	});

	/* manage add-ons */
	$("#checkoutAddOns").on('click', function (event) {
		event.preventDefault();

		var $checkoutModalAddOn = $(".add-on-cta-list");
		$.each($checkoutModalAddOn, function (key, value) {
			$(this).data('count-units', 0);
			$(this).parent().parent().parent().parent().parent().css({"border-color": "#EFEFEF"});
			$(this).parent().parent().parent().parent().children('.shortlisted-label').addClass('hide');
			$(this).children('.add-on-add-unit').removeClass('hide');
			$(this).children('.add-on-remove-unit').addClass('hide');
		});

		/* update modal with existing data */
		var $bookingAddOn = $('.co-add-on-wrap');

		if ($bookingAddOn.length > 0) {
			$.each($bookingAddOn, function (key, value) {

				var $selectedCheckoutModalAddOn = $(".add-on-id-" + $(this).data('id'));
				if ($selectedCheckoutModalAddOn.length > 0) {
					$selectedCheckoutModalAddOn.data('count-units', $(this).data('booking-units'));
					$selectedCheckoutModalAddOn.parent().parent().parent().parent().parent().css({"border-color": "#30AC15"});
					$selectedCheckoutModalAddOn.parent().parent().parent().parent().children('.shortlisted-label').removeClass('hide');
					$selectedCheckoutModalAddOn.children('.add-on-add-unit').addClass('hide');
					$selectedCheckoutModalAddOn.children('.add-on-remove-unit').removeClass('hide');
				}
			});
		}

		calculateCheckoutAddOnsModalTotalPrice();

		$('#checkoutAddOnsModal').modal('show');
	});

	/* customer proofs upload choice */
	$('.checkout-proofs-choice').on('click', function () {
		$('.btn-checkout-proofs-choice').removeClass('btn-checkout-proofs-toggle');
		$(this).parent().addClass('btn-checkout-proofs-toggle');
		var proofUploadChoice = $("input[type='radio'][name='proofUploadChoice']:checked").val();
		if (proofUploadChoice && (proofUploadChoice == 1)) {
			$('#proofUploadWrap').removeClass('hide');
			$("#proofCancellationCheck").addClass('hide');
		} else {
			$('#proofUploadWrap').addClass('hide');
			$('#proofCancellationCheck').removeClass('hide');
		}
	});

	/* Auto update pre-checkout page add-ons from existing data */
	(function updateExistingAddOns() {
		$selectedAddOn = $('.checkout-selected-add-on');
		if ($selectedAddOn.length > 0) {
			$.each($selectedAddOn, function (key, value) {
				var $selectedCheckoutAddOn = $(".add-on-id-" + $(this).data('id'));
				if ($selectedCheckoutAddOn) {
					$selectedCheckoutAddOn.data('count-units', $(this).data('booking-units'));
					$selectedCheckoutAddOn.parent().parent().parent().parent().parent().css({"border-color": "#30AC15"});
					$selectedCheckoutAddOn.parent().parent().parent().parent().children('.shortlisted-label').removeClass('hide');
					$selectedCheckoutAddOn.children('.add-on-add-unit').addClass('hide');
					$selectedCheckoutAddOn.children('.add-on-remove-unit').removeClass('hide');
				}
			});

			calculateCheckoutAddOnsModalTotalPrice();
		}
	})();

	/* calculate add ons total amount */
	function calculateCheckoutAddOnsModalTotalPrice() {
		var $checkoutModalAddOn = $(".add-on-cta-list");
		var addOnAmount = 0;
		var addedAddOnsCount = 0;
		$.each($checkoutModalAddOn, function (key, value) {
			addOnAmount = addOnAmount + parseInt(($(this).data('price') * $(this).data('count-units')), 10);
			if ($(this).data('count-units') > 0) {
				addedAddOnsCount++;
			}
		});

		$('.co-ao-modal-price').html(addOnAmount);
		$('.co-ao-modal-count').html(addedAddOnsCount);

		if (addedAddOnsCount > 0) {
			$('.co-ao-modal-price-wrap').removeClass('hide');
		} else {
			$('.co-ao-modal-price-wrap').addClass('hide');
		}
	}

	function selectAddOn(parentElement) {
		var addOnCount = parentElement.data('count-units');
		var addOnMaxCount = parentElement.data('max-units');
		if (addOnCount < addOnMaxCount) {
			addOnCount++;
			parentElement.data('count-units', addOnCount);

			parentElement.parent().parent().parent().parent().parent().css({"border-color": "#30AC15"});
			parentElement.parent().parent().parent().parent().children('.shortlisted-label').removeClass('hide');
			parentElement.children('.add-on-add-unit').addClass('hide');
			parentElement.children('.add-on-remove-unit').removeClass('hide');

			calculateCheckoutAddOnsModalTotalPrice();

			/* all this logic needs to be intact with backend php code */
			orderPriceDetails.push({
				'productId': parentElement.data('id'),
				'productTypeId': $('#addOnTypeId').val(),
				'productTitle': parentElement.parent().parent().children('.add-on-name').text(),
				'productTransportCharges': 0,
				'productPrice': parentElement.data('price'),
				'productBookingAmount': parentElement.data('price'),
				'productWorthAmount': parentElement.data('price-worth'),
				'isVenueBooking': 0
			});

			totalBookingAmount = totalBookingAmount + parentElement.data('price');
			/* 100% advance for add-ons */
			totalAdvanceAmount = totalAdvanceAmount + parentElement.data('price');

			updatePriceDetailsMarkup();
		} else {
			window.showNotyError('You have added maximum units of this add on available.');
		}
	}

	function deselectAddOn(parentElement) {
		var addOnCount = parentElement.data('count-units');
		if (addOnCount > 0) {
			addOnCount--;
			parentElement.data('count-units', addOnCount);
			if (addOnCount == 0) {
				parentElement.parent().parent().parent().parent().parent().css({"border-color": "#EFEFEF"});
				parentElement.parent().parent().parent().parent().children('.shortlisted-label').addClass('hide');
				parentElement.children('.add-on-add-unit').removeClass('hide');
				parentElement.children('.add-on-remove-unit').addClass('hide');
			}

			calculateCheckoutAddOnsModalTotalPrice();

			for (var key in orderPriceDetails) {
				if (orderPriceDetails[key]['productId'] === parentElement.data('id')) {

					totalBookingAmount = totalBookingAmount - parentElement.data('price');
					/* 100% advance for add-ons */
					totalAdvanceAmount = totalAdvanceAmount - parentElement.data('price');

					delete orderPriceDetails[key];
					updatePriceDetailsMarkup();
				}
			}
		} else {
			/* window.showNotyError('Add on has already been removed'); */
		}
	}

	$('.add-on-remove-unit').on('click', function (event) {
		event.preventDefault();

		var parentElement = $(this).parent();

		deselectAddOn(parentElement);
	});

	$('.add-on-add-unit').on('click', function (event) {
		event.preventDefault();

		var parentElement = $(this).parent();

		selectAddOn(parentElement);
	});

	$('.checkout-add-ons-remove-btn').on('click', function (event) {
		event.preventDefault();

		$('.add-on-remove-unit').each(function (i, ele) {
			var parentElement = $(ele).parent();

			deselectAddOn(parentElement);
		});
	});

	/* update add-ons */
	$('#updateAddOns').on('click', function (event) {
		event.preventDefault();

		$('.generic-page-loader-wrap').removeClass('hide');

		var $ticketId = $('#ticketId').val();
		var $addOns = {};

		var $checkoutModalAddOn = $("#checkoutAddOnsModal .add-on-cta-list");
		$.each($checkoutModalAddOn, function (key, value) {
			if ($(this).data('count-units') > 0) {
				$addOns[$(this).data('id')] = $(this).data('count-units');
			}
		});

		$.ajax({
			url: "/ajax/checkout/add-ons/" + $ticketId,
			dataType: 'json',
			type: 'POST',
			data: {
				addOns: $addOns
			},
			success: function (data) {
				if (data.success) {
					location.reload();
				} else {
					$('.generic-page-loader-wrap').addClass('hide');
					window.showNotyError('Some error occurred while updating add-ons. Kindly refresh the page ang try again.');
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				$('.generic-page-loader-wrap').addClass('hide');
				window.notifyTeam({
					"url": "/ajax/checkout/add-ons/" + $ticketId,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});
	});

	/* google events for complementary services */
	$('#btnInviteKnowMore').on('click', function (event) {
		event.preventDefault();
	});

	$('#btnTYCKnowMore').on('click', function (event) {
		event.preventDefault();
	});

	/* if phone number already asked in pre-checkout modal */
	/**
	 * Disabled sending alert to team on auto book initiation
	 * @author Anji <anji@evibe.in>
	 * @since 22 Sep 2018
	 */
	/**
	 * *Enabled
	 * @author Manav
	 * @since 09 Oct 2018
	 */

	/* code written to support navigational checkout page */

	/* need to move to pay.js */

	function scrollToTop() {
		window.scrollTo(0, 0);
	}

	var totalBookingAmount = 0;
	var totalAdvanceAmount = 0;
	var orderPriceDetails = {};

	function updatePriceDetailsMarkup() {
		var totalSavedAmount = 0;
		$('#advanceAmount').val(totalAdvanceAmount - couponDiscountAmount - walletDiscount + internationalPaymentHandlingFee);

		var validPayableAmount = totalAdvanceAmount - couponDiscountAmount + internationalPaymentHandlingFee - walletDiscount;
		validPayableAmount = parseFloat(validPayableAmount, 10).toFixed(0).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();

		$('.checkout-advance-amount').text(priceFormat(validPayableAmount));
		$('.checkout-price-split-up-wrap').empty();

		var $element = "";
		if (orderPriceDetails) {
			$element += '<div class="checkout-price-split-up-orders">';
			orderPriceDetails.forEach(function (item) {
				/* @see: if transport price exists, then product price will definitely exist */
				/* added default in controller to fetch booking amount in the absence of product price */
				if (item['productBookingAmount']) {
					$element += '<div class="checkout-price-split-up-component checkout-price-split-up-order" data-product-id="' + item['productId'] + '" data-product-type-id="' + item['productTypeId'] + '">';
					$element += '<div class="col-xs-8 col-sm-8 text-left no-pad-l">';
					$element += '<span class="checkout-price-product-title">' + item['productTitle'] + '</span>';
					$element += '</div>';
					$element += '<div class="col-xs-4 col-sm-4 no-pad-l__400">';
					$element += '<span class="">';
					if (item['productWorthAmount']) {
						$element += '<div class="checkout-price-split-up-order-worth">';
						$element += '<span class="rupee-font">&#8377; </span>';
						$element += '<span class="">' + priceFormat(item['productWorthAmount']) + '</span>';
						$element += '</div>';
					}
					$element += '<div>';
					$element += '<span class="rupee-font">&#8377; </span>';
					$element += '<span class="">' + priceFormat(item['productPrice']) + '</span>';
					$element += '</div>';
					$element += '</span>';
					$element += '</div>';
					$element += '<div class="clearfix"></div>';
					if (item['isVenueBooking']) {
						/* No need to show delivery charges for venue bookings */
					} else {
						$element += '<div class="font-12">';
						$element += '<div class="col-xs-8 col-sm-8 text-left no-pad-l">';
						$element += '<span class="checkout-price-product-transport">Delivery Charges</span>';
						$element += '</div>';
						$element += '<div class="col-xs-4 col-sm-4 no-pad-l__400">';
						if (item['productTransportCharges']) {
							$element += '<span class="rupee-font">&#8377; </span>';
							$element += '<span class="">' + priceFormat(item['productTransportCharges']) + '</span>';
						} else {
							$element += '<span class="text-g">Free</span>';
						}
						$element += '</div>';
						$element += '<div class="clearfix"></div>';
						$element += '</div>';
					}
					$element += '</div>';

					/* calculate total saved amount */
					var productSavedAmount = 0;
					if (item['productWorthAmount'] && ((item['productWorthAmount'] - item['productBookingAmount']) > 0)) {
						productSavedAmount = item['productWorthAmount'] - item['productBookingAmount'];
					}
					totalSavedAmount = totalSavedAmount + productSavedAmount;
				}
			});
			$element += '</div>';
		}

		if (couponDiscountAmount) {
			$element += '<div class="checkout-price-split-up-component checkout-price-split-up-coupon">';
			$element += '<div class="col-xs-8 col-sm-8 text-left no-pad-l">';
			$element += '<span class="checkout-price-product-title">Coupon Discount</span>';
			$element += '</div>';
			$element += '<div class="col-xs-4 col-sm-4 no-pad-l__400">';
			$element += '<span class="text-g">';
			$element += '- <span class="rupee-font">&#8377; </span>';
			$element += '<span class="">' + priceFormat(couponDiscountAmount) + '</span>';
			$element += '</span>';
			$element += '</div>';
			$element += '<div class="clearfix"></div>';
			$element += '</div>';
		}

		if (walletDiscount > 0) {
			$element += '<div class="checkout-price-split-up-coupon">';
			$element += '<div class="col-xs-8 col-sm-8 text-left no-pad-l">';
			$element += '<span class="checkout-price-product-title">Wallet Discount</span>';
			$element += '</div>';
			$element += '<div class="col-xs-4 col-sm-4 no-pad-l__400">';
			$element += '<span class="text-g">';
			$element += '- <span class="rupee-font">&#8377; </span>';
			$element += '<span class="">' + priceFormat(walletDiscount) + '</span>';
			$element += '</span>';
			$element += '</div>';
			$element += '<div class="clearfix"></div>';
			$element += '</div>';
		}

		if ($('#additionalCharges:checked').length === 1) {
			$element += '<div class="checkout-price-split-up-coupon">';
			$element += '<div class="col-xs-8 col-sm-8 text-left no-pad-l">';
			$element += '<span class="checkout-price-product-title">Internet Handling Fee</span>';
			$element += '</div>';
			$element += '<div class="col-xs-4 col-sm-4 no-pad-l__400">';
			$element += '<span class="rupee-font">&#8377; </span>';
			$element += '<span class="">' + priceFormat(internationalPaymentHandlingFee) + '</span>';
			$element += '</div>';
			$element += '<div class="clearfix"></div>';
			$element += '</div>';
		}

		$element += '<hr class="checkout-price-split-up-hr">';
		$element += '<div class="checkout-price-split-up-component text-bold">';
		$element += '<div class="col-xs-8 col-sm-8 text-left no-pad-l">';
		$element += '<span class="checkout-price-product-title">Total Booking Amount</span>';
		$element += '</div>';
		$element += '<div class="col-xs-4 col-sm-4 no-pad-l__400">';
		$element += '<span class="">';
		$element += '<span class="rupee-font">&#8377; </span>';
		$element += '<span class="">' + priceFormat(totalBookingAmount - couponDiscountAmount - walletDiscount + internationalPaymentHandlingFee) + '</span>';
		$element += '</span>';
		$element += '</div>';
		$element += '<div class="clearfix"></div>';
		$element += '</div>';

		if ((totalSavedAmount + couponDiscountAmount) > 0) {
			$element += '<div class="checkout-price-split-up-save text-center">';
			$element += '<span class="">(You saved </span>';
			$element += '<span class="">';
			$element += '<span class="rupee-font">&#8377; </span>';
			$element += '<span class="">' + priceFormat(totalSavedAmount + couponDiscountAmount) + '</span>';
			$element += '</span>';
			$element += '<span class=""> on this purchase)</span>';
			$element += '</div>';
		}

		if (totalAdvanceAmount && (totalAdvanceAmount !== totalBookingAmount)) {
			$element += '<hr class="checkout-price-split-up-hr">';

			$element += '<div class="checkout-price-split-up-component text-bold">';
			$element += '<div class="col-xs-8 col-sm-8 text-left no-pad-l">';
			$element += '<span class="checkout-price-product-title">Advance To Pay</span>';
			$element += '</div>';
			$element += '<div class="col-xs-4 col-sm-4 no-pad-l__400">';
			$element += '<span class="text-e">';
			$element += '<span class="rupee-font">&#8377; </span>';
			$element += '<span class="">' + priceFormat(totalAdvanceAmount - couponDiscountAmount + internationalPaymentHandlingFee) + '</span>';
			$element += '</span>';
			$element += '</div>';
			$element += '<div class="clearfix"></div>';
			$element += '</div>';

			$element += '<div class="checkout-price-split-up-info text-center">';
			$element += '<span class="glyphicon glyphicon-info-sign"></span>';
			$element += '<span class="">Balance amount needs to be paid to our event co-ordinator(s)</span>';
			$element += '</div>';
		}

		/* If loading model is enabled before it will make sure to close it */
		hideLoading();
		$('.checkout-price-split-up-wrap').prepend($element);
	}

	function updatePriceDetails() {
		var $ajaxUrl = $('#hidUpdatePriceDetailsUrl').val();

		$.ajax({
			url: $ajaxUrl,
			dataType: 'json',
			type: 'POST',
			data: {},
			success: function (data) {
				if (data.success) {
					if (data.bookingAmount && data.advanceAmount) {
						totalBookingAmount = data.bookingAmount;
						totalAdvanceAmount = data.advanceAmount;
						/* do not update discount amount with ticket_booking->prepay_discount_amount */
						/* couponDiscountAmount = data.couponDiscountAmount; */
						orderPriceDetails = data.orderPriceDetails;

						updatePriceDetailsMarkup();
					} else {
						window.showNotyError("Some error occurred while submitting your data. Kindly refresh the page and try again");
					}
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				window.showNotyError("Some error occurred while submitting your data. Kindly refresh the page and try again");
				window.notifyTeam({
					"url": $ajaxUrl,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});
	}

	function updateOrdersCheckoutCard() {
		var $ajaxUrl = $('#hidUpdateOrderDetailsUrl').val();

		$.ajax({
			url: $ajaxUrl,
			dataType: 'html',
			type: 'POST',
			success: function (data) {
				/* to avoid loss of js functionality on cta */
				$('#orderDetails .checkout-card-body').empty();
				$('#orderDetails .checkout-card-body').prepend(data);
				$(".checkout-page-2 #orderDetails").find(".mdl-textfield").removeClass("is-upgraded").attr("data-upgraded", "");
				if (!(typeof (componentHandler) === 'undefined')) {
					componentHandler.upgradeDom();
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				window.showNotyError("Some error occurred while submitting your data. Kindly refresh the page and try again");
				window.notifyTeam({
					"url": $ajaxUrl,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});
	}

	function showNavTargetCheckoutCard($targetNavElement) {
		/*
		 * if (targetCount < presentCount) {
		 *      directly navigate;
		 * }
		 * else {
		 *      validate present card;
		 *      proceed to target card;
		 * }
		 *
		 * */
		var targetCheckoutCardCount = $targetNavElement.data('checkout-card-count');
		var presentCheckoutCardRef = location.hash.substr(1);

		if ($targetNavElement.hasClass('done')) {
			if (targetCheckoutCardCount < presentCheckoutCardCount) {
				/* no validation, as there is not a valid use case */
				/* @see: should implement validation if such case happens */
				showCheckoutCardFromRef($targetNavElement.data('ref'));
			} else {
				nextCheckoutCardCount = targetCheckoutCardCount;
				validateCheckoutCard(presentCheckoutCardRef);
			}
		}
	}

	function showNextCheckoutCard() {
		/*
		 * present checkout card count is updated whenever a card is shown
		 * This indicated that nav bar should be updated whenever a change happens
		 * */

		$('.checkout-navigation-btn').each(function (i, el) {
			if ($(el).data('checkout-card-count') == nextCheckoutCardCount) {
				showCheckoutCardFromRef($(el).data('ref'));
			}
		});
	}

	function showFirstCheckoutCard() {
		nextCheckoutCardCount = 1;

		$('.checkout-navigation-btn').each(function (i, el) {
			if ($(el).data('checkout-card-count') == nextCheckoutCardCount) {
				showCheckoutCardFromRef($(el).data('ref'));
			}
		});
	}

	function showCheckoutCardFromRef($cardRef) {
		var $cardId = '#' + $cardRef;
		$('.checkout-card-wrap').addClass('hide');
		$('#couponDetails').addClass('hide');

		if (!$($cardId).length) {
			showFirstCheckoutCard();
		} else {
			$($cardId).removeClass('hide');
			updateCheckoutCardNavigation($cardRef);
			scrollToTop();

			/* showing coupon code card only in payment details screen */
			if ($cardRef === "paymentDetails") {
				$('#couponDetails').removeClass('hide');
			}

			window.location.hash = "#" + $cardRef;
		}
	}

	function showVenueDetails() {
		$('.checkout-card-wrap').addClass('hide');
		$('#venueDetails').removeClass('hide');
		updateCheckoutCardNavigation('venueDetails');
		scrollToTop();
	}

	function showPartyDetails() {
		$('.checkout-card-wrap').addClass('hide');
		$('#partyDetails').removeClass('hide');
		updateCheckoutCardNavigation('partyDetails');
		scrollToTop();
	}

	function showAddOnDetails() {
		$('.checkout-card-wrap').addClass('hide');
		$('#addOnDetails').removeClass('hide');
		updateCheckoutCardNavigation('addOnDetails');
		scrollToTop();
	}

	function showOrderDetails() {
		$('.checkout-card-wrap').addClass('hide');
		$('#orderDetails').removeClass('hide');
		updateCheckoutCardNavigation('orderDetails');
		scrollToTop();
	}

	function showPaymentDetails() {
		$('.checkout-card-wrap').addClass('hide');
		$('#paymentDetails').removeClass('hide');
		updateCheckoutCardNavigation('paymentDetails');
		scrollToTop();
	}

	function showContactDetails() {
		$('.checkout-card-wrap').addClass('hide');
		$('#contactDetails').removeClass('hide');
		updateCheckoutCardNavigation('contactDetails');
		scrollToTop();
	}

	function uploadCustomerOrderProofs() {
		window.showLoading();
		var formData = new FormData();
		var customerProofFront = $("#customerProofFront")[0].files;
		var customerProofBack = $("#customerProofBack")[0].files;

		formData.append('proofType', $('#inpCustomerProofType').val());
		formData.append('customerProofFront', customerProofFront[0]);
		formData.append('customerProofBack', customerProofBack[0]);

		var $ajaxUrl = $('#uploadProofsUrl').val();

		$.ajax({
			url: $ajaxUrl,
			dataType: 'JSON',
			type: 'POST',
			processData: false,
			cache: false,
			contentType: false,
			data: formData,
			success: function (data) {
				window.hideLoading();
				if (data.success) {
					$('.checkout-proofs-upload-wrap').addClass('hide');
					$('.checkout-proofs-success-wrap').removeClass('hide');
					$('#hidProofUploadSuccess').val(1);
				} else {
					window.showNotyError(data.errorMsg);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				window.hideLoading();
				window.notifyTeam({
					"url": $ajaxUrl,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});
	}

	$('#proofUploadBtn').on('click', function (event) {
		event.preventDefault();
		uploadCustomerOrderProofs();
	});

	/*
	 * dynamic numbers
	 * for now don't change the order
	 * no. 1 should always be displayed
	 * dynamiv venue booking details
	 * */

	function validateVenueDetails() {
		window.showLoading();

		/* add area id of the ticket */
		userInput.preDefinedAreaId = $('#preDefinedAreaId').val();

		/* add venue info only for not venue booking */
		$venueAddressLine1 = $('#venueAddressLine1');
		if (parseInt($venueAddressLine1.length, 10) === 1) {
			userInput.venueAddressLine1 = $venueAddressLine1.val();
			userInput.venueAddressLine2 = $('#venueAddressLine2').val();
			userInput.venueLocation = $('#venueLocation').val();
			userInput.venueLandmark = $('#venueLandmark').val();
			userInput.venueZipCode = $('#venueZipCode').val();
			userInput.venueTypeId = $('#typeVenue').val();
			userInput.mapAddress = $('#mapAddress').data('address');
			userInput.lat = $('#mapLat').data('lat');
			userInput.lng = $('#mapLng').data('lng');
			userInput.locationDetails = $('.google-location-details').val();
		}

		var $errorMsg = "Some error occurred while submitting your data. Kindly refresh the page and try again.";
		var $ajaxUrl = $('#hidValidateVenueDetailsUrl').val();

		$.ajax({
			url: $ajaxUrl,
			dataType: 'json',
			data: userInput,
			type: 'POST',
			success: function (data) {
				window.hideLoading();
				if (data.success) {
					/* showPartyDetails(); */
					showNextCheckoutCard();
				} else {
					if (data.errorMsg) {
						$errorMsg = data.errorMsg;
					}
					window.showNotyError($errorMsg);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				window.hideLoading();
				window.showNotyError($errorMsg);
				window.notifyTeam({
					"url": $ajaxUrl,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});
	}

	function validatePartyDetails() {
		window.showLoading();

		/* add venue info only for not venue booking */
		userInput.partyDate = $('#inpPartyDate').val();
		userInput.partyTime = $('#inpPartyTime').val();

		/* get the dynamic field value */
		var $dynamicFields = $('#dynamicFieldsCustomerData');
		if ($dynamicFields.length > 0) {
			var dynamicFields = JSON.parse($dynamicFields.val());
			$.each(dynamicFields, function (key, value) {
				var name = value['name'];
				var fieldType = value['type_field_id'];
				if (fieldType == $('#hidInputTypeCheckbox').val()) {
					var checkedFields = $('input[name="' + name + '"]:checked');
					var checkboxValue = '';
					$.each(checkedFields, function (key, value) {
						if (checkboxValue !== '') {
							checkboxValue = checkboxValue + ', ';
						}
						checkboxValue = checkboxValue + $(this).val();
					});
					userInput[name] = checkboxValue;
				} else {
					userInput[name] = $('#' + name).val();
				}
			});
		}
		/* get the dynamic field value */
		var $dynamicFields = $('#dynamicFieldsCustomerData');
		if ($dynamicFields.length > 0) {
			var dynamicFields = JSON.parse($dynamicFields.val());
			$.each(dynamicFields, function (key, value) {
				var name = value['name'];
				var fieldType = value['type_field_id'];
				if (fieldType == $('#hidInputTypeCheckbox').val()) {
					var checkedFields = $('input[name="' + name + '"]:checked');
					var checkboxValue = '';
					$.each(checkedFields, function (key, value) {
						if (checkboxValue !== '') {
							checkboxValue = checkboxValue + ', ';
						}
						checkboxValue = checkboxValue + $(this).val();
					});
					userInput[name] = checkboxValue;
				} else {
					userInput[name] = $('#' + name).val();
				}
			});
		}

		var $errorMsg = "Some error occurred while submitting your data. Kindly refresh the page and try again.";
		var $ajaxUrl = $('#hidValidatePartyDetailsUrl').val();

		$.ajax({
			url: $ajaxUrl,
			dataType: 'json',
			data: userInput,
			type: 'POST',
			success: function (data) {
				window.hideLoading();
				if (data.success) {
					/* showOrderDetails(); */
					showNextCheckoutCard();
				} else {
					if (data.errorMsg) {
						$errorMsg = data.errorMsg;
					}
					window.showNotyError($errorMsg);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				window.hideLoading();
				window.showNotyError($errorMsg);
				window.notifyTeam({
					"url": $ajaxUrl,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});
	}

	/* no validation is required */

	/* but add-ons should be add/removed */
	function validateAddOnDetails() {
		var $ticketId = $('#ticketId').val();
		var $addOns = {};
		var addOnsCount = 0;

		var $checkoutModalAddOn = $(".add-on-cta-list");
		$.each($checkoutModalAddOn, function (key, value) {
			if ($(this).data('count-units') > 0) {
				$addOns[$(this).data('id')] = $(this).data('count-units');
				addOnsCount++;
			}
		});

		/* this check is disabled as we are using help text to make it understandable to customer */
		/*
		 if (!addOnsCount) {
		 window.showNotyError("Kindly select at least one add-on service");
		 return false;
		 }
		 */

		window.showLoading();
		$.ajax({
			url: "/ajax/checkout/add-ons/" + $ticketId,
			dataType: 'json',
			type: 'POST',
			data: {
				addOns: $addOns
			},
			success: function (data) {
				if (data.success) {
					window.hideLoading();
					updateOrdersCheckoutCard();
					updatePriceDetails();
					showNextCheckoutCard();
					/* window.location.href = $('#hidCheckoutUrl').val(); */
				} else {
					window.hideLoading();
					window.showNotyError('Some error occurred while updating add-ons. Kindly refresh the page ang try again.');
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				window.hideLoading();
				window.notifyTeam({
					"url": url,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});
	}

	function validateOrderDetails() {
		window.showLoading();

		userInput.uploadImage = $('.checkout-page-2 #uploadImage').val();
		userInput.uploadSuccess = $('#hidUploadSuccess').val();

		/* get the dynamic field value */
		var $dynamicFields = $('#dynamicFieldsUser');
		var checkoutFieldValues = {};
		if ($dynamicFields.length > 0) {
			var dynamicFields = JSON.parse($dynamicFields.val());
			$.each(dynamicFields, function (key, value) {
				var name = value['booking_checkout_unique_id'];
				var fieldType = value['type_field_id'];
				if (fieldType == $('#hidInputTypeCheckbox').val()) {
					var checkedFields = $('input[name="' + name + '"]:checked');
					var checkboxValue = '';
					$.each(checkedFields, function (key, value) {
						if (checkboxValue !== '') {
							checkboxValue = checkboxValue + ', ';
						}
						checkboxValue = checkboxValue + $(this).val();
					});
					checkoutFieldValues[name] = checkboxValue;
				} else {
					checkoutFieldValues[name] = $('#' + name).val();
				}
			});
		}

		userInput.checkoutFieldValues = checkoutFieldValues;

		userInput.eInvite = $('#eInvite').prop("checked");

		var $errorMsg = "Some error occurred while submitting your data. Kindly refresh the page and try again.";
		var $ajaxUrl = $('#hidValidateOrderDetailsUrl').val();

		$.ajax({
			url: $ajaxUrl,
			dataType: 'json',
			data: userInput,
			type: 'POST',
			success: function (data) {
				window.hideLoading();
				if (data.success) {
					/* showContactDetails(); */
					showNextCheckoutCard();
				} else {
					if (data.errorMsg) {
						$errorMsg = data.errorMsg;
					}
					window.showNotyError($errorMsg);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				window.hideLoading();
				window.showNotyError($errorMsg);
				window.notifyTeam({
					"url": $ajaxUrl,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});
	}

	function validateProofDetails() {
		window.showLoading();

		userInput.proofUploadChoice = $("input[type='radio'][name='proofUploadChoice']:checked").val();
		userInput.accountabilityCheck = $("input[type='checkbox'][name='accountabilityCheck']:checked").val();
		userInput.cancellationCheck = $("input[type='checkbox'][name='cancellationCheck']:checked").val();

		userInput.needCustomerProof = $('#needCustomerProof').val();
		userInput.proofUploadSuccess = $('#hidProofUploadSuccess').val();

		var $errorMsg = "Some error occurred while submitting your data. Kindly refresh the page and try again.";
		var $ajaxUrl = $('#hidValidateProofDetailsUrl').val();

		$.ajax({
			url: $ajaxUrl,
			dataType: 'json',
			data: userInput,
			type: 'POST',
			success: function (data) {
				window.hideLoading();
				if (data.success) {
					/* showContactDetails(); */
					showNextCheckoutCard();
				} else {
					if (data.errorMsg) {
						$errorMsg = data.errorMsg;
					}
					window.showNotyError($errorMsg);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				window.hideLoading();
				window.showNotyError($errorMsg);
				window.notifyTeam({
					"url": $ajaxUrl,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});
	}

	function validateContactDetails() {
		window.showLoading();

		var specialNotes = "";

		if ($('#isAutoBooking').length && $('#isAutoBooking').val()) {
			specialNotes = $('#splNotes').val();
		} else {
			$(".special-notes-wrap").each(function () {
				if ($(this).find(".special-notes").val() != "") {
					specialNotes = specialNotes + '<div style="font-size: 11px; color: #636363"><i>' + $(this).find(".special-notes-title").text() + '</i></div><ul><li>' + $(this).find(".special-notes").val() + '</li></ul><span></span>';
				}
			});
		}

		userInput.name = $('#inpName').val();
		userInput.phone = $('#inpPhone').val();
		userInput.altPhone = $('#altPhone').val();
		userInput.splNotes = specialNotes;
		userInput.advanceAmount = $('#advanceAmount').val();
		userInput.acceptsTerms = 1;
		userInput.isVenueDeal = $('#isVenueDeal').length ? $('#isVenueDeal').val() : 0;
		userInput.title = $('#inpTitle').val();
		userInput.customerSource = $('#inpCustomerSource').val();

		var $errorMsg = "Some error occurred while submitting your data. Kindly refresh the page and try again.";
		var $ajaxUrl = $('#hidValidateContactDetailsUrl').val();

		$.ajax({
			url: $ajaxUrl,
			dataType: 'json',
			data: userInput,
			type: 'POST',
			success: function (data) {
				window.hideLoading();
				if (data.success) {
					/* showPaymentDetails(); */
					showNextCheckoutCard();
				} else {
					if (data.errorMsg) {
						$errorMsg = data.errorMsg;
					}
					window.showNotyError($errorMsg);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				window.hideLoading();
				window.showNotyError($errorMsg);
				window.notifyTeam({
					"url": $ajaxUrl,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});
	}

	/*
	 $('.btn-checkout-venue-continue').on('click', function () {
	 validateVenueDetails();
	 });

	 $('.btn-checkout-party-continue').on('click', function () {
	 validatePartyDetails();
	 });

	 $('.btn-checkout-order-continue').on('click', function () {
	 validateOrderDetails();
	 });

	 $('.btn-checkout-contact-continue').on('click', function () {
	 validateContactDetails();
	 });
	 */

	$('.checkout-navigation-btn').on('click', function () {
		var $targetNavElement = $(this);
		showNavTargetCheckoutCard($targetNavElement);
	});

	/*
	 $('.btn-checkout-navigation-venue').on('click', function () {
	 if ($(this).hasClass('done')) {
	 showVenueDetails();
	 }
	 });

	 $('.btn-checkout-navigation-party').on('click', function () {
	 if ($(this).hasClass('done')) {
	 showPartyDetails();
	 }
	 });

	 $('.btn-checkout-navigation-order').on('click', function () {
	 if ($(this).hasClass('done')) {
	 showOrderDetails();
	 }
	 });

	 $('.btn-checkout-navigation-add-on').on('click', function () {
	 if ($(this).hasClass('done')) {
	 showAddOnDetails();
	 }
	 });

	 $('.btn-checkout-navigation-contact').on('click', function () {
	 if ($(this).hasClass('done')) {
	 showContactDetails();
	 }
	 });

	 $('.btn-checkout-navigation-payment').on('click', function () {
	 if ($(this).hasClass('done')) {
	 showPaymentDetails();
	 }
	 });
	 */

	$('.checkout-card-details-submit-btn').on('click', function () {
		var $ref = $(this).parent().closest('.checkout-card-wrap').attr('id');

		nextCheckoutCardCount = +presentCheckoutCardCount + 1;
		validateCheckoutCard($ref);
	});

	function validateCheckoutCard($ref) {
		switch ($ref) {
			case 'venueDetails':
				validateVenueDetails();
				break;
			case 'partyDetails':
				validatePartyDetails();
				break;
			case 'addOnDetails':
				validateAddOnDetails();
				break;
			case 'orderDetails':
				validateOrderDetails();
				break;
			case 'proofDetails':
				validateProofDetails();
				break;
			case 'contactDetails':
				validateContactDetails();
				break;
			default:
				break;
		}
	}

	function updateCheckoutCardNavigation(elementRef) {
		$('.checkout-navigation-btn').removeClass('selected');
		$('.checkout-navigation-btn').removeClass('done');
		$('.checkout-navigation-btn').addClass('disabled');
		$('.checkout-navigation-btn').each(function (i, el) {
			if ($(el).data('ref') == elementRef) {
				presentCheckoutCardCount = $(el).data('checkout-card-count');

				if (presentCheckoutCardCount >= maxCheckoutCardCount) {
					maxCheckoutCardCount = presentCheckoutCardCount;
				}
			}
		});

		$('.checkout-navigation-btn').each(function (i, el) {
			var navCheckoutCardCount = $(el).data('checkout-card-count');

			if (navCheckoutCardCount === presentCheckoutCardCount) {
				$(el).addClass('selected');
			}

			/* rest of the nav links are any way disabled */
			if (navCheckoutCardCount < maxCheckoutCardCount) {
				$(el).addClass('done');
				$(el).removeClass('disabled');
			}
		});
	}

	function priceFormat($num) {
		$num = $num.toString();
		var lastThree = $num.substring($num.length - 3);
		var otherNumbers = $num.substring(0, $num.length - 3);
		if (otherNumbers != '')
			lastThree = ',' + lastThree;
		var $res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

		return $res;
	}

	/* @see: updated according to navigational checkout page */
	$('.btn-skip-add-ons').on('click', function (event) {
		event.preventDefault();

		/* window.location.href = $('#hidCheckoutUrl').val(); */
		var $ticketId = $('#ticketId').val();
		var $addOns = {};
		var addOnsCount = 0;

		window.showLoading();
		$.ajax({
			url: "/ajax/checkout/add-ons/" + $ticketId,
			dataType: 'json',
			type: 'POST',
			data: {
				addOns: $addOns
			},
			success: function (data) {
				if (data.success) {
					window.hideLoading();
					updateOrdersCheckoutCard();
					updatePriceDetails();
					showNextCheckoutCard();
				} else {
					window.hideLoading();
					window.showNotyError('Some error occurred while updating add-ons. Kindly refresh the page ang try again.');
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				window.hideLoading();
				window.notifyTeam({
					"url": url,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});
	});

	/* update add-ons */
	/* @see: updated according to navigational checkout page */
	$('.btn-checkout-add-ons-continue').on('click', function (event) {
		event.preventDefault();

		nextCheckoutCardCount = +presentCheckoutCardCount + 1;
		validateAddOnDetails();
	});

	function uploadCustomerImages() {
		if ($('.checkout-page-2 #uploadImage').val() && parseInt($('.checkout-page-2 #uploadImage').val()) === 1) {
			var formData = new FormData();
			var fileUploads = $('input[type=file]')[0].files;

			$.each(fileUploads, function (i, file) {
				formData.append('images[' + i + ']', file);
			});

			var $ajaxUrl = $('.checkout-page-2 #uploadImagesForm').data('url');

			$.ajax({
				url: $ajaxUrl,
				dataType: 'JSON',
				type: 'POST',
				processData: false,
				cache: false,
				contentType: false,
				data: formData,
				success: function (data) {
					if (data.success) {
						$('#uploadDetails').addClass('hide');
						$('#uploadCompleted').removeClass('hide');
						$('#hidUploadSuccess').val(1);
					} else {
						window.showNotyError(data.errorMsg);
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					window.notifyTeam({
						"url": $ajaxUrl,
						"textStatus": textStatus,
						"errorThrown": errorThrown
					});
				}
			});
		}
	}

	$('.checkout-page-2').on('click', '#imageUploadBtn', function (event) {
		event.preventDefault();
		uploadCustomerImages();

	});

	/* update checkout cards with count */
	var checkoutStepCount = 1;
	$('.checkout-card-wrap').addClass('hide');
	$('.checkout-navigation-btn').each(function (i, el) {
		/* $(el).children('.step_no').text(checkoutStepCount); */
		$(el).data('checkout-card-count', checkoutStepCount);

		checkoutStepCount++;
	});

	/* using hashbangs to navigate through checkout page */
	window.onhashchange = function () {
		showCheckoutCardFromRef(location.hash.substr(1));
	};

	/* by this time, present checkout card count is 'o' */
	/* card change based on hashbang should only be attempted if it is checkout page */
	if (window.location.href.indexOf("checkout") > -1) {
		if (location.hash) {
			showCheckoutCardFromRef(location.hash.substr(1));
		} else {
			showFirstCheckoutCard();
		}
	}
	/* updating price card to be safe */
	if ($('#isValidStatus').val() == 1) {
		updateOrdersCheckoutCard();
		updatePriceDetails();
	}

	$(window).on('load', function () {
		$ajaxUrl = "/pay/check/payment-txn/" + $('#ticketId').val();
		$.ajax({
			url: $ajaxUrl,
			dataType: 'JSON',
			type: 'POST',
			data: {},
			success: function (data) {
				window.hideLoading();
				if (data.success) {
					if (data.retryCount > 1) {
						$("#paymentMethod").remove();
						$("#paymentMethodPayU").remove();
						$(".payment-txn-inputs").append($("#retryPaymentMethod").clone().attr("id", "paymentMethod"));
						$(".payment-txn-inputs").append($("#retryPaymentMethod").clone().attr("id", "paymentMethodPayU"));
					}
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				window.notifyTeam({
					"url": $ajaxUrl,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});
	});

	ajaxTicketAlertToTeam();
});