var tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);

$("#cakeDeliveryDate").datetimepicker({
	minDate: 0,
	timepicker: false,
	startDate: tomorrow,
	defaultDate: tomorrow,
	format: 'Y/m/d',
	scrollInput: false,
	scrollMonth: false,
	scrollTime: false,
	closeOnDateSelect: true,
	disabledDates: window.disableDates(),
	onSelectDate: function (ct, $i) {
		$i.parent().addClass('is-dirty');
	}
});

$('#cakeOrderDate').datetimepicker({
	timepicker: false,
	minDate: 0,
	format: 'Y/m/d',
	scrollInput: false,
	scrollMonth: false,
	scrollTime: false,
	closeOnDateSelect: true,
	disabledDates: window.disableDates(),
	onSelectDate: function (ct, $i) {
		$i.parent().addClass('is-dirty');
	}
});

$('#enquiryCakeDate').datetimepicker({
	timepicker: false,
	minDate: 0,
	format: 'Y/m/d',
	scrollInput: false,
	scrollMonth: false,
	scrollTime: false,
	closeOnDateSelect: true,
	disabledDates: window.disableDates(),
	onSelectDate: function (ct, $i) {
		$i.parent().addClass('is-dirty');
	}
});

if (window.location.href.indexOf('#bookNowModal') != -1) {
	showCheckoutModal();
}

/* add delivery charges on change (if applicable) */
$('#cakeSlot').change(function () {
	var slotId = $(this).val();
	var selector = $('#cakeAutoBookingForm').find('#cakeSlot');
	calculateAndShowDeliveryCharge(slotId, selector);
});

/* change price on changing of option */
$('.cake-profile .add-on-cat').change(function (event) {
	event.preventDefault();
	var selected = getCurrentValue();
	calculateAndShowPrice(selected);
});

$('#cakeAutoBookingForm').find('input').change(function () {
	validateInput();
});

/* show checkout modal on click */
/* form submit (auto booking) */
// @see: check if the button should be enabled or disabled
if(window.isCityDisabled()) {
	var $btnCakeBookNow = $('.btn-cake-book-now');
	$btnCakeBookNow.attr('disabled', true);
	$btnCakeBookNow.on('click', false);

	var $btnCakeSubmit = $('#btnCakeSubmit');
	$btnCakeSubmit.attr('disabled', true);
	$btnCakeSubmit.on('click', false);
} else {
	$('.btn-book-now, .btn-cake-book-now').click(function (event) {
		event.preventDefault();
		showCheckoutModal();
	});

	$('#btnCakeSubmit').click(function (event) {
		event.preventDefault();
	
		var form = $('#cakeAutoBookingForm');
		var url = form.data('url');
		var slotSelector = form.find('#cakeSlot');
		var slotId = slotSelector.val();
		var currentValues = getCurrentValue();
		var categories = getSelectedData();
	
		var data = {
			'name': form.find('#name').val(),
			'phone': form.find('#phone').val(),
			'callingCode': form.find('#checkoutModalCallingCode').val(),
			'email': form.find('#email').val(),
			'cakeDeliveryDate': form.find('#cakeDeliveryDate').val(),
			'cakeSlot': slotId,
			'message': form.find('#message').val(),
			'price': calculatePrice(currentValues).price,
			'deliveryCharge': calculateDeliveryCharges(slotId, slotSelector),
			'categories': categories,
			'pageUrl': window.location.href
		};
	
		/* add-ons */
		var $addOn = $('#bookNowModal .add-on-cta-list');
	
		if ($addOn.length > 0) {
	
			var addOns = [];
			$.each($addOn, function (key, value) {
				if ($(this).data('count-units') > 0) {
					addOns[$(this).data('id')] = $(this).data('count-units');
				}
			});
	
			data['addOns'] = addOns;
		}
	
		$.ajax({
			'url': url,
			'type': 'POST',
			'dataType': 'JSON',
			'data': data,
			success: function (data) {
				if (data.success && data.redirectUrl) {
					location.href = data.redirectUrl;
				}
				else if (data.error) {
					if (data.is_live === 0) {
						var $button = $('#btnCakeSubmit');
						$button.attr('disabled', true);
						$button.text('Option Unavailable');
					}
					form.find('.errors-msg').removeClass('hide').text(data.error);
				}
				else {
					window.showNotyError("Some error occurred while we are collecting your details, Please refresh the page and try again");
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				window.showNotyError("Some error occurred, please try again after refreshing the page");
				window.notifyTeam({
					"url": url,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});
	});
}

/* get all selected values */
function getCurrentValue() {
	var selectedData = [];
	$('.cake-profile .add-on-cat option:selected').each(function () {
		selectedData.push({
			'catId': $(this).parents('.cake-add-on-wrap').data('catid'),
			'catName': $(this).parents('.cake-add-on-wrap').data('name'),
			'id': $(this).val(),
			'price': $(this).data('price'),
			'worth': $(this).data('worth'),
			'isFixed': $(this).data('isfixed'),
			'weight': $(this).data('weight'),
			'name': $(this).data('name')
		})
	});
	$('.cake-profile .add-on-cat option').each(function () {
		if ($(this).is(":selected")) {
			$(this).text($(this).data('name'));
		}
		else if ($(this).data('displayoption').length) {
			$(this).text($(this).data('displayoption'));
		}
	});
	return selectedData;
}

/* calculate price based on selected options */
function calculatePrice(selected) {

	var defaultSelector = $('.add-on-cat option[value="-1"]');
	var defaultPrice = defaultSelector.data('price');
	var defaultWorth = defaultSelector.data('worth');
	var price = defaultPrice, worth = defaultWorth;
	var weight = 0;
	var thisWorth = 0;

	$.each(selected, function (key, data) {
		/* weight (fixed part) */
		/* @see: DO NOT CHANGE THIS ORDER */

		if (data.catId === 1) {
			price = data.price;
			worth = data.worth ? data.worth : data.price;
			weight = data.weight;
		}
		/* flavour or eggless */
		else {
			if (data.isFixed === 1) {
				thisWorth = data.worth ? data.worth : data.price;
				price = price + data.price;
				worth = worth + thisWorth;
			}
			else {
				/* calculate based on per kg */
				thisWorth = data.worth ? data.worth : data.price;
				price = price + weight * (data.price);
				worth = worth + weight * thisWorth;
			}
		}
	});

	return {'price': parseInt(price, 10), 'worth': parseInt(worth, 10)};
}

/* show price */
function calculateAndShowPrice(selected) {
	var allPrice = calculatePrice(selected);

	$('.displayPrice').text(allPrice.price);
	$('.displayWorth').text(allPrice.worth);
	$('.displayPriceOff').text(allPrice.worth - allPrice.price);

	$('.cake-price-cnt .loading-cnt').addClass('hide');
	$('.cake-price-cnt .price-content').removeClass('hide');
}

/* calculate delivery charges */
function calculateDeliveryCharges(slotId, slotSelector) {
	return slotSelector.find('option[value=' + slotId + ']').data('price');
}

/* show delivery charges */
function calculateAndShowDeliveryCharge(slotId, slotSelector) {
	var existingPrice = calculatePrice(getCurrentValue()).price;

	var price = calculateDeliveryCharges(slotId, slotSelector);
	$('.cake-extra-price').removeClass('hide');
	$('#deliveryCharge').text(price);
	$('#totalPrice').text(existingPrice + price);
}

/* show checkout modal */
function showCheckoutModal() {
	var modal = $('#bookNowModal');
	var selected = getCurrentValue();
	modal.find('.selected-field').text("");

	$.each(selected, function (key, data) {
		var markup = $("#selectedTypeMarkup").clone();
		markup.find('.title').text(data.catName);
		markup.find('.value').text(data.name);
		$('#bookNowModal').find('.selected-field').append(markup);
	});

	/* check if slot has any delivery charge */
	var slotSelector = $('#cakeAutoBookingForm').find('#cakeSlot');
	var slotId = slotSelector.val();
	calculateAndShowDeliveryCharge(slotId, slotSelector);

	modal.modal('show');
}

/* validate input (disable submit button when some required field is empty) */
function validateInput() {
	var $cakeForm = $('#cakeAutoBookingForm');
	var btnCakeSubmit = $('#btnCakeSubmit');

	if (!$cakeForm.find('#name').val() || (!$cakeForm.find('#phone').val() || $cakeForm.find('#phone').val().length != 10)
		|| !$cakeForm.find('#email').val() || !$cakeForm.find('#cakeDeliveryDate').val()) {
		btnCakeSubmit.prop('disabled', true);
	}
	else {
		btnCakeSubmit.prop('disabled', false);
	}
}

function getSelectedData() {
	var categories = getCurrentValue();
	var currentValues = [];

	$.each(currentValues, function (key, val) {
		categories.push({
			'id': val.id,
			'catId': val.catId,
			'catName': val.catName,
			'name': val.name,
			'price': val.price,
			'worth': val.worth,
			'isFixed': val.isFixed,
			'weight': val.weight
		});
	});

	return categories;
}

/* calculate price at the time of page loading */
calculateAndShowPrice(getCurrentValue());
validateInput();