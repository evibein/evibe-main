$(document).ready(function () {

	var $bookNowModal = $('#bookNowModal');
	var $btnDecorSubmit = $('#btnDecorSubmit');
	var $btnPincodeSubmit = $('#btnPincodeSubmit');
	var $transportSuccess = $('#ab-transport-success');
	var $transportFail = $('#ab-transport-fail');
	var $loadingText = $('#abLoading');
	var $baseBookingAmount = $('#baseBookingAmount').data('price');
	var $partyDist = 0;
	var $transCharges = 0;
	var $totalBookingAmount = $baseBookingAmount + $transCharges;
	var $percentage = $('#hidAdvPercentage').val();
	var $tokenAmount = parseInt(($totalBookingAmount) * $percentage * 0.01);
	var $distFree = $('#distFree').val();
	var $distMax = $('#distMax').val();
	var $proceed = false;
	var oldFailedPhone = 0;
	var $decorPhone = 0;

	var $distFreeMeters = $distFree * 1000;
	var $distMaxMeters = $distMax * 1000;

	function validateFeasibilityCheckBtn() {
		if ($('#partyPincode').val().length !== 6 || $('#decorPhone').val().length !== 10) {
			$btnPincodeSubmit.prop('disabled', true);
		}
		else {
			$btnPincodeSubmit.prop('disabled', false);
		}
	}

	$('#decorPhone, #partyPincode').keyup(function () {
		validateFeasibilityCheckBtn();
	});

	$('#decorPhone, #partyPincode').on('blur', function () {
		validateFeasibilityCheckBtn();
	});

	$('#btnChangePinCode').click(function (event) {
		event.preventDefault();
		revertHideClassChanges();
		$('#partyPincode').val('');
		$('#partyPincode').focus();
	});

	$('#partyPincode, #decorPhone').keypress(function (e) {
		var charCode = (e.which) ? e.which : e.keyCode;
		return (!(charCode > 31 && (charCode < 48 || charCode > 57)))
	});

	/* show checkout modal on click */
	$('.btn-book-now').click(function (event) {
		event.preventDefault();
		$bookNowModal.modal("show");
	});

	/* show checkout modal */
	(function initCheckoutModal() {
		$bookNowModal.modal({
			'show': false,
			'backdrop': 'static',
			'keyboard': false
		});
	})();

	if (window.location.href.indexOf('#bookNowModal') != -1) {
		$bookNowModal.modal("show");
	}

	/* revert back actions */
	function revertHideClassChanges() {
		$('#partyPinCodeSection').removeClass('hide');
		$transportSuccess.addClass('hide');
		$transportFail.addClass('hide');
		$('.basic-amount').removeClass('hide');
		$('.transport-free').removeClass('hide');
		$('#transportRupeeFont').addClass('hide');
		$('#transportAmount').addClass('hide');
		$('#transport-free-2').addClass('hide');
		$('#transportNotifyText').addClass('hide');
		$('#verifyPincode').addClass('ab-decor-extra-space');
	}

	function calculateAndShowAmount() {
		$('.basic-amount').addClass('hide');
		if ($transCharges > 0) {
			/* var $transAmt = $('#transportAmount'); */
			$('.transport-free').addClass('hide');
			/* $('#transportRupeeFont').removeClass('hide'); */
			/* $transAmt.removeClass('hide'); */
			/* $transAmt.text($transCharges); */

			/* todo: remove the below and uncomment the above when trans-charges are sorted out. */
			$('#transport-free-2').removeClass('hide');
			$('#transportNotifyText').removeClass('hide');
			$transCharges = 0;
			$totalBookingAmount = $baseBookingAmount;
			$tokenAmount = parseInt(($totalBookingAmount) * $percentage * 0.01);
		}
		$('#totalBookingAmount').text($totalBookingAmount);
		$('#tokenAmount').text($tokenAmount);
		$('#customerPinCode').text($partyPincode);
	}

	$btnPincodeSubmit.click(function (event) {
		event.preventDefault();

		revertHideClassChanges();
		$loadingText.removeClass('hide');

		$pp = $('#partyPincode');
		$partyPincode = $pp.val();
		$gurl = $pp.data('url');
		$providerPincode = $('#providerPincode').val();
		$decorPhone = $('#decorPhone').val();

		var dataToController = {
			'providerPincode': $providerPincode,
			'partyPincode': $partyPincode
		};

		$.ajax({
			url: $gurl,
			dataType: 'json',
			type: 'POST',
			data: dataToController,
			success: function (data) {
				if (data.success === true) {
					$partyDist = data.partyDist;
					$transCharges = data.transCharges;
					$totalBookingAmount = data.totalBookingAmount;
					$tokenAmount = data.tokenAmount;
					$distFreeMeters = data.distFreeMeters;
					if ($partyDist <= $distFreeMeters) {
						calculateAndShowAmount();
						$loadingText.addClass('hide');
						$('#verifyPincode').removeClass('ab-decor-extra-space');
						$transportSuccess.removeClass('hide');
						$('#partyPinCodeSection').addClass('hide');
					}
					else if (($partyDist > $distFreeMeters) && ($partyDist <= $distMaxMeters)) {
						calculateAndShowAmount();
						$loadingText.addClass('hide');
						$('#verifyPincode').removeClass('ab-decor-extra-space');
						$transportSuccess.removeClass('hide');
						$('#partyPinCodeSection').addClass('hide');
					}
					else if ($partyDist > $distMaxMeters) {
						calculateAndShowAmount();
						$loadingText.addClass('hide');
						$transportFail.removeClass('hide');
					}
				}
				else if (data.success === false) {
					validateSubmitBtn();
					$loadingText.addClass('hide');
					window.showNotyError('Please verify the entered Pin code and try again');
				}
				else {
					validateSubmitBtn();
					$loadingText.addClass('hide');
					window.showNotyError('Some error occurred while processing your data. Please try again after refreshing the page');
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				window.notifyTeam({
					"url": url,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});
	});

	$bookNowModal.on('hide.bs.modal', function () {
		$decorPhone = $('#decorPhone').val();
		if ($proceed === false && $decorPhone.length === 10 && $decorPhone !== oldFailedPhone) {
			oldFailedPhone = $decorPhone;
			$.ajax({
				url: $('#failedToProceedData').val(),
				dataType: 'json',
				type: 'POST',
				data: {
					'phone': $decorPhone
				},
				error: function (jqXHR, textStatus, errorThrown) {
					window.notifyTeam({
						"url": url,
						"textStatus": textStatus,
						"errorThrown": errorThrown
					});
				}
			})
		}
	});

	(function attachCheckoutModalEvents() {
		/* revert the form back to normal before showing */
		$bookNowModal.on('show.bs.modal', function () {
			revertHideClassChanges();
			$('#partyPincode').val('');
			$('#decorPhone').val('');
		});
	})();

	$btnDecorSubmit.click(function (event) {
		event.preventDefault();
		var form = $('#decorAutoBookingForm');
		var url = form.data('url');
		$.ajax({
			type: 'POST',
			dataType: 'JSON',
			url: url,
			data: {
				'phone': $('#decorPhone').val(),
				'forVenueDetails': $('#forVenueDetails').val(),
				'partyPincode': $('#partyPincode').val(),
				'transportCharges': $transCharges,
				'totalBookAmount': $totalBookingAmount,
				'tokenAdvance': $tokenAmount
			}, success: function (data) {
				if
				(data.success && data.redirectUrl) {
					$proceed = true;
					$('#bookNowModal').hide();
					location.href = data.redirectUrl;
				} else if (data.error) {
					form.find('.errors-msg').removeClass('hide').text(data.error);
				} else {
					window.showNotyError("Some error occurred while we are collecting your details, Please refresh the page and try again");
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				window.showNotyError("Some error occurred, please try again after refreshing the page");
				window.notifyTeam({
						"url": url,
						"textStatus": textStatus,
						"errorThrown": errorThrown
					}
				);
			}
		});
	});

	validateFeasibilityCheckBtn();

});
