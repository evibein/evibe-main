$(document).ready(function () {

	var $bookNowButton = $('.book-now-button');
	var $stickyFormWrap = $('.sticky-form-wrap');
	var $stickyBookNowFormWrap = $('.sticky-book-now-form-wrap');
	var $stickyEnquiryFormWrap = $('.sticky-enquiry-form-wrap');
	var $loadingSectionWrap = $('.loading-section-wrap');
	var $stickyEnquiryLoader = $('.loading-enquiry-section-wrap');
	var $closeFormButton = $('.mob-btn-close-form');
	var $preCheckFeasibilityButton = $('.pre-check-feasibility-button');
	var $preCheckBookNowButton = $('.pre-check-book-now-button');
	var $checkFeasibilityButton = $('.check-feasibility-button');
	var $proceedButton = $('.mob-btn-proceed');
	var $changePartyLocation = $('.change-party-location');
	var $stickyEnquiryErrorMsg = $('#stickyEnquiryFormErrorMsg');
	var $stickyFormClose = $('.sticky-form-close');
	var $desBookNowBtn = $('.des-btn-book-now');
	var $desFeasibilityBookNowBtn = $('.des-btn-feasibility-book-now');
	var $desBackBtn = $('.des-btn-back');
	var $desCheckFeasibilityBtn = $('.des-btn-check-feasibility');
	var $desFormLoader = $('.product-profile-form-loader');
	var $desFeasibilityChangePinCode = $('.des-feasibility-change-pincode');
	var $desBookNowChangePinCode = $('.des-book-now-change-pincode');
	var $mobProfileEnquiryBtn = $('.mobile-profile-enquire-button');
	var $mobProfileEnquiryCloseBtn = $('.mob-profile-enquiry-close-button');
	var $mobPostEnquiryBtn = $('.mob-post-enquiry-button');
	var $mobCloseBookNowBtn = $('.mob-btn-close-book-now-form');
	var $bookNowSubmitBtn = $('.btn-book-now-submit');
	var $formErrorMsg = $('.form-error-message');
	var $desPreFeasibilityBtn = $('.des-btn-pre-feasibility');
	var $desPreBookNowBtn = $('.des-btn-pre-book-now');
	var $bookingFeasibilityModal = $('#bookNowModal');
	var $percentage = $('#hidAdvPercentage').val();

	var distMax = parseInt($('#hidDistMax').val(), 10);
	var distFree = parseInt($('#hidDistFree').val(), 10);
	var basePrice = parseInt($('#hidBaseBookingAmount').val(), 10);
	var customerPinCode = 0;
	var feasibilityCheck = false;
	var productFeasibility = 0;
	var oldPhone = 0;
	var partyDist = 0;
	var transCharges = 0;
	var formatTransCharges = 0;
	var formatTokenAmount = 0;
	var formatBaseBookingAmount = 0;
	var formatTotalBookingAmount = 0;
	var distMaxMeters = distMax * 1000;
	var distFreeMeters = distFree * 1000;
	var totalBookingAmount = parseInt(basePrice + transCharges);
	var tokenAmount = parseInt(totalBookingAmount * $percentage * 0.01);

	var totalAddOnAmount = 0;
	var totalAddOnAmountWorth = 0;
	var addedAddOnsCount = 0;

	var upName = null;
	var upPhone = null;
	var upEmail = null;
	var upPartyDate = null;
	var upPartyArea = null;
	var upPartyPinCode = null;

	(function triggerWishlistButton() {
		var wishListOption = $(".wishlist-option");
		if (wishListOption.length > 0) {

			var parentDiv = wishListOption.parent();
			var optionData = {
				'productId': parentDiv.data("map-id"),
				'productTypeId': parentDiv.data("map-type-id"),
				'cityId': parentDiv.data("city-id"),
				'occasionId': parentDiv.data("occasion-id"),
				'isWishList': 1
			};

			$.ajax({
				url: wishListOption.data("status-url"),
				type: 'POST',
				data: optionData,
				success: function (data) {
					if (data.success) {
						wishListOption.remove();
						$(".wishlisted-option").removeClass("hide");
					} else {
						wishListOption.on("click", function wishListOptionClick() {
							if (data.loginFail) {
								$('#loginModal').modal('show');
							} else {
								wishListOption.addClass('hide');
								$(".wishlisted-option").removeClass('hide');

								$.ajax({
									url: wishListOption.data("url"),
									type: 'POST',
									data: optionData,
									success: function (data) {
										if (data.success) {
											showNotySuccess("Added to Wish List.");
										}
									},
									error: function (jqXHR, textStatus, errorThrown) {
										showNotyError('Sorry, an error has occurred, please try again later.');
										window.notifyTeam({
											"url": wishListOption.data("url"),
											"textStatus": textStatus,
											"errorThrown": errorThrown
										});
									}
								});
							}

						});
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					window.notifyTeam({
						"url": wishListOption.data("status-url"),
						"textStatus": textStatus,
						"errorThrown": errorThrown
					});
				}
			});
		}
	})();

	(function loadFotoramaForPackageProfile() {
		var fotoramaSection = $("#fotorama");
		if (fotoramaSection.length > 0) {
			fotoramaSection.fotorama();

			removeGalleryLoadingAnimation();
		} else {
			removeGalleryLoadingAnimation();
		}

		$('.lazy-loading').each(function () {
			var img = $(this);
			img.attr('src', img.data('src'));
		});
	})();

	function removeGalleryLoadingAnimation() {
		$('.loading-img-wrap').remove();
		$('.gallery-wrap').removeClass('hide');
	}

	function applyRatingStar(to) {
		$(to).rating({
			showClear: 0,
			readonly: 'true',
			showCaption: 0,
			size: 'xl',
			min: 0,
			max: 5,
			step: 0.1
		});
		$(to).removeClass('hide');
	}

	function calculateRoundedAdvance($bookingAmount) {
		var $advanceAmount = $bookingAmount * $percentage * 0.01;
		var $balAmount = $bookingAmount - $advanceAmount;
		if (($balAmount % 100) != 0 && $balAmount > 100) {
			var $round = parseInt($balAmount % 100);
			var $roundedAdvanceAmount = parseInt($bookingAmount) - parseInt($balAmount) + $round;
			return $roundedAdvanceAmount;
		}

		return parseInt($advanceAmount);
	}

	function priceFormat($num) {
		$num = $num.toString();
		var lastThree = $num.substring($num.length - 3);
		var otherNumbers = $num.substring(0, $num.length - 3);
		if (otherNumbers != '')
			lastThree = ',' + lastThree;
		var $res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

		return $res;
	}

	function getSimilarCategoryCards() {
		if ($('#hidOtherCategoryCardsUrl').length > 0) {
			var $ajaxUrl = $('#hidOtherCategoryCardsUrl').val();

			$.ajax({
				url: $ajaxUrl,
				dataType: 'html',
				type: 'POST',
				success: function (data) {
					$('#loadingCategoryCards').hide();
					$('#showOtherCategoryCards').append(data);

					if ($('.categoryCards').length > 0) {
						$.getScript("/js/owl-carousel.js", function () {
							$('.categoryCards').owlCarousel({
								stagePadding: 0,
								dots: false,
								rtl: false,
								loop: false,
								margin: 15,
								lazyload: true,
								responsive: {
									0: {
										items: 1.3
									},
									1000: {
										items: 4
									},
								},
								nav: true,
								navText: ["<span class='pad-10 bg-white-imp'><img src='https://image.flaticon.com/icons/svg/481/481130.svg' style='height:20px'></span>", "<span class='pad-10 bg-white-imp'><img src='https://image.flaticon.com/icons/svg/481/481127.svg' style='height:20px'></span>"]
							});
						});
					} else {
						$('#loadingCategoryCards').hide();
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					$('#loadingCategoryCards').hide();
					window.notifyTeam({
						"url": $ajaxUrl,
						"textStatus": textStatus,
						"errorThrown": errorThrown
					});
				}
			});
		} else {
			$('#loadingCategoryCards').hide();
		}
	}

	function showSimilarProducts() {
		if ($('#hidSimilarProductsUrl').length > 0) {
			$.ajax({
				url: $('#hidSimilarProductsUrl').val(),
				dataType: 'html',
				tryCount: 0,
				retryLimit: 3,
				type: 'POST',
				success: function (data) {
					$('#loadingSimilarProducts').addClass('hide');
					$('#showSimilarProducts').append(data);
					applyRatingStar('.avg-rating');
				},
				error: function (jqXHR, textStatus, errorThrown) {
					this.tryCount++;
					if (this.tryCount <= this.retryLimit) {
						$.ajax(this);
					} else {
						$('#loadingSimilarProducts').addClass('hide');
						window.notifyTeam({
							"url": $('#hidSimilarProductsUrl').val(),
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				}
			});
		}
	}

	function calculateAndShowAmount() {
		$('.sticky-price-content').removeClass('hide');

		$('.form-normal-text').addClass('hide');
		$('.feasibility-success-text').removeClass('hide');

		if (transCharges > 0) {
			$('.transport-free').addClass('hide');
			$('.transport-price-content').removeClass('hide');
			$('.transport-price').text(formatTransCharges);
			/* transCharges = 0; */
			/* totalBookingAmount = basePrice; */
			tokenAmount = calculateRoundedAdvance(parseInt(totalBookingAmount));
		}
		/* @todo: update after transportation prices are sorted */
		$('.updated-price').text(formatTotalBookingAmount);
		$('.token-amount').text(formatTokenAmount);
		$('#msgTokenAmount').text(formatTokenAmount);
		$('.updated-pincode').text(customerPinCode);

		$('.sticky-form').addClass('hide');
		$('.form-normal-cta').addClass('hide');
		$('.feasibility-success-cta').removeClass('hide');
	}

	function returnToBasicState() {
		$('.sticky-form').removeClass('hide');
		$('.form-normal-cta').removeClass('hide');
		$('.sticky-price-content').addClass('hide');
		$('.feasibility-success-cta').addClass('hide');

		$('.form-normal-text').removeClass('hide');
		$('.feasibility-success-text').addClass('hide');
		$('.feasibility-fail-text').addClass('hide');
		$('.transport-free').removeClass('hide');
		$('.transport-price-content').addClass('hide');
		$formErrorMsg.addClass('hide');
		$('#profilePartyDate').parent().parent().addClass('hide');
		/* $('#partyPincode').prop('disabled', false); */
		/* $('#mobilePhone').prop('disabled', false); */
		$('#partyPincode').parent().parent().removeClass('hide');
		$('#mobilePhone').parent().parent().removeClass('hide');
		$('.des-form-content').removeClass('hide');
	}

	$('.add-on-add-unit').on('click', function (event) {
		event.preventDefault();

		var parentElement = $(this).parent();

		var addOnId = parentElement.data('id');
		var addOnPrice = parentElement.data('price');
		var addOnPriceWorth = parentElement.data('price-worth');
		var addOnMaxCount = parentElement.data('max-units');
		var addOnCount = parentElement.data('count-units');

		if (addOnCount < addOnMaxCount) {
			addOnCount++;

			/* update all instances of this add-on */
			$.each($('.add-on-cta-list'), function (key, value) {
				if ($(this).data('id') == addOnId) {
					$(this).data('count-units', addOnCount);

					$(this).parent().parent().parent().parent().parent().css({"border-color": "#30AC15"});
					$(this).children('.add-on-add-unit').addClass('hide');
					$(this).children('.add-on-remove-unit').removeClass('hide');
				}
			});

			totalAddOnAmount = totalAddOnAmount + addOnPrice;
			totalAddOnAmountWorth = totalAddOnAmountWorth + addOnPriceWorth;
			if (addOnCount == 1) {
				addedAddOnsCount++;
			}
			updateAddOnPrices();
		} else {
			window.showNotyError("You have added maximum number of units of this add-on applicable");
		}

	});

	$('.add-on-remove-unit').on('click', function (event) {
		event.preventDefault();

		var parentElement = $(this).parent();

		var addOnId = parentElement.data('id');
		var addOnPrice = parentElement.data('price');
		var addOnPriceWorth = parentElement.data('price-worth');
		var addOnMaxCount = parentElement.data('max-units');
		var addOnCount = parentElement.data('count-units');

		if (addOnCount > 0) {
			addOnCount--;

			$.each($('.add-on-cta-list'), function (key, value) {
				if ($(this).data('id') == addOnId) {
					$(this).data('count-units', addOnCount);

					if (addOnCount == 0) {
						$(this).parent().parent().parent().parent().parent().css({"border-color": "#EFEFEF"});
						$(this).children('.add-on-add-unit').removeClass('hide');
						$(this).children('.add-on-remove-unit').addClass('hide');
					}
				}
			});

			/*
			 parentElement.data('count-units', addOnCount);
			 listParent.siblings('.add-on-unit-count').text(addOnCount);
			 */

			totalAddOnAmount = totalAddOnAmount - addOnPrice;
			totalAddOnAmountWorth = totalAddOnAmountWorth - addOnPriceWorth;

			if (addOnCount == 0) {
				addedAddOnsCount--;
			}
			updateAddOnPrices();
		} else {
			/* window.showNotyError("You have already removed this add-on"); */
		}

	});

	function updateAddOnPrices() {
		$('.add-ons-price').text(totalAddOnAmount);
		$('.add-ons-selected-count').text(addedAddOnsCount);

		if (addedAddOnsCount > 0) {
			$('.add-ons-price-wrap').removeClass('hide');
		} else {
			$('.add-ons-price-wrap').addClass('hide');
		}

		/* update prices in checkout modal */
		var productPrice = parseInt($("#pcBookingPrice").data('price'), 10);
		var productPriceWorth = parseInt($("#pcOffPrice").data('price-worth'), 10);
		var productPriceOff = (productPriceWorth >= productPrice) ? (productPriceWorth - productPrice) : 0;

		$("#pcAddOnsPrice").html(priceFormat(parseInt(totalAddOnAmount)));
		$("#pcAddOnsPrice").data('price', parseInt(totalAddOnAmount));

		$("#pcTotalBookingAmount").html(priceFormat(parseInt(productPrice + totalAddOnAmount)));
		$("#pcTotalBookingAmount").data('price', parseInt(productPrice + totalAddOnAmount));

		/* validation has already been taken care in blade files */
		$("#pcOffPrice").html(priceFormat(parseInt(productPriceOff + totalAddOnAmountWorth)));

		/* update prices in advance selection tabs */
		if ($('.adv-radio-advance-amount').length) {
			var advPercent = $('.adv-radio-advance-amount').data('adv-percent');
			var productAdvPrice = calculateRoundedAdvance(productPrice);
			$('.adv-radio-advance-amount').html(priceFormat(parseInt(productAdvPrice + totalAddOnAmount)));
		}

		if ($('.adv-radio-total-amount').length) {
			$('.adv-radio-total-amount').html(priceFormat(parseInt(productPrice + totalAddOnAmount)));
		}

		calculateAndUpdatePrices();
	}

	function checkFeasibilityOfTheProduct() {
		var $partyPinCode = $('#partyPincode');
		var $mobilePhone = $('#mobilePhone');

		upPhone = $mobilePhone.val();
		upPartyPinCode = $partyPinCode.val();
		upPartyDate = $('#profilePartyDate').val();

		var $gUrl = $partyPinCode.data('url');
		var dataToController = {
			'providerPinCode': $('#hidProviderPincode').val(),
			'partyPinCode': $partyPinCode.val(),
			'phone': $mobilePhone.val(),
			'pageUrl': window.location.href
		};

		showDesFeasibilityLoader();
		displayLoadingDiv();
		returnToBasicState();

		$.ajax({
			url: $gUrl,
			dataType: 'json',
			type: 'POST',
			data: dataToController,
			success: function (data) {
				if (data.success === true) {
					totalBookingAmount = data.totalBookingAmount;
					partyDist = data.partyDist;
					distFreeMeters = data.distFreeMeters;
					transCharges = data.transCharges;
					tokenAmount = data.tokenAmount;
					formatTokenAmount = data.formatTokenAmount;
					formatBaseBookingAmount = data.formatBaseBookingAmount;
					formatTotalBookingAmount = data.formatTotalBookingAmount;
					formatTransCharges = data.formatTransCharges;
					customerPinCode = $partyPinCode.val();
					if (partyDist <= distFreeMeters) {
						feasibilityCheck = true;
						productFeasibility = 1;

						/* @see: do not change order */
						ticketAlertToTeam();
						calculateAndShowAmount();

						$('.sticky-form').removeClass('hide');
						$('#profilePartyDate').parent().parent().removeClass('hide');
						$('#partyPincode').parent().parent().addClass('hide');
						$('#mobilePhone').parent().parent().addClass('hide');
						hideLoadingDiv();
						hideDesFeasibilityLoader();
					} else if ((partyDist > distFreeMeters) && (partyDist <= distMaxMeters)) {
						feasibilityCheck = true;
						productFeasibility = 1;
						ticketAlertToTeam();
						calculateAndShowAmount();
						$('.sticky-form').removeClass('hide');
						$('#profilePartyDate').parent().parent().removeClass('hide');
						$('#partyPincode').parent().parent().addClass('hide');
						$('#mobilePhone').parent().parent().addClass('hide');
						hideLoadingDiv();
						hideDesFeasibilityLoader();
					} else if (partyDist > distMaxMeters) {
						productFeasibility = 0;
						$('.form-normal-text').addClass('hide');
						$('.feasibility-fail-text').removeClass('hide');
						$('.updated-pincode').text(customerPinCode);
						hideLoadingDiv();
						hideDesFeasibilityLoader();
						ticketAlertToTeam();
					}
				} else if (data.success === false) {
					if (data.error) {
						productFeasibility = 0;
						$formErrorMsg.text(data.error);
						$formErrorMsg.removeClass('hide');
						hideLoadingDiv();
						hideDesFeasibilityLoader();
						var isLive = data.is_live;
						if (isLive === 0) {
							var $button = $('.check-feasibility-button');
							$button.text('Option Unavailable');
							$button.attr('disabled', true);
						}

					} else {
						productFeasibility = 0;
						hideLoadingDiv();
						hideDesFeasibilityLoader();
						ticketAlertToTeam();
						window.showNotyError('Please verify the entered Pin code and try again');
					}
				} else {
					productFeasibility = 0;
					hideLoadingDiv();
					hideDesFeasibilityLoader();
					ticketAlertToTeam();
					window.showNotyError('Some error occurred while processing your data. Please try again after refreshing the page');
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				productFeasibility = 0;
				ticketAlertToTeam();
				hideLoadingDiv();
				hideDesFeasibilityLoader();
				window.notifyTeam({
					"url": $gUrl,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});
	}

	function showCheckoutPage() {
		var url = $('#hidAutoBookUrl').val();

		$formErrorMsg.addClass('hide');

		var $data = {
			'phone': upPhone,
			'callingCode': $('#checkoutModalCallingCode').val(),
			'forVenueDetails': $('#hidForVenueDetails').val(),
			'partyPinCode': upPartyPinCode,
			'partyDate': upPartyDate,
			'transportCharges': transCharges,
			'totalBookAmount': totalBookingAmount,
			'tokenAdvance': tokenAmount
		};

		/* add-ons */
		var $addOn = $('#bookNowModal .add-on-cta-list');

		if ($addOn.length > 0) {

			var addOns = [];
			$.each($addOn, function (key, value) {
				if ($(this).data('count-units') > 0) {
					addOns[$(this).data('id')] = $(this).data('count-units');
				}
			});

			$data['addOns'] = addOns;
		}

		$.ajax({
			type: 'POST',
			dataType: 'JSON',
			url: url,
			data: $data,
			success: function (data) {
				if (data.success && data.redirectUrl) {
					location.href = data.redirectUrl;
				} else if (data.error) {
					$formErrorMsg.text(data.error);
					$formErrorMsg.removeClass('hide');
					hideLoadingDiv();
					hideDesFeasibilityLoader();
					hideDesBookNowLoader();
				} else {
					window.showNotyError("Some error occurred while we are collecting your details, Please refresh the page and try again");
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				window.showNotyError("Some error occurred, please try again after refreshing the page");
				window.notifyTeam({
						"url": url,
						"textStatus": textStatus,
						"errorThrown": errorThrown
					}
				);
			}
		});
	}

	function postProfileEnquiry() {
		$stickyEnquiryErrorMsg.text('');

		showStickyEnquiryLoader();
		var $ajaxUrl = $mobPostEnquiryBtn.data('url');

		$.ajax({
			url: $ajaxUrl,
			dataType: 'json',
			type: 'POST',
			data: {
				'name': $('#enquiryCustomerName').val(),
				'phone': $('#enquiryCustomerPhone').val(),
				'email': $('#enquiryCustomerEmail').val(),
				'partyDate': $('#enquiryProfileDate').val(),
				'comments': $('#enquiryComments').val(),
				'accepts': 'yes'
				/* 'accepts': $('#quickViewAccepts').is(':checked') ? 'yes' : 'no' */
			},
			success: function (data) {
				if (data.success === true) {
					if (data.redirectTo) {
						window.location = data.redirectTo;
					} else {
						window.showNotySuccess('Your response has been submitted successfully. Expect a call from us in 4 business hours');
						hideStickyEnquiryForm();
						hideStickyEnquiryLoader();
						$('#mobileStickyEnquiryForm')[0].reset();
					}
				} else if (data.errors) {
					setTimeout(function () {
						hideStickyEnquiryLoader();
						$stickyEnquiryErrorMsg.text(data.errors);
					}, 200);
				} else {
					hideStickyEnquiryLoader();
					window.showNotyError("Some error occurred while submitting your response. Kindly refresh the page and try again.")
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				hideStickyEnquiryLoader();
				window.showNotyError("Some error occurred while submitting your response. Kindly refresh the page and try again.");
				window.notifyTeam({
					"url": $ajaxUrl,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});
	}

	function checkBookingFeasibility() {
		var $partyPinCode = $('#bookNowPartyPinCode');
		var $mobilePhone = $('#bookNowPhone');
		var $partyDate = $('#bookNowPartyDate');

		upPhone = $mobilePhone.val();
		upPartyPinCode = $partyPinCode.val();
		upPartyDate = $partyDate.val();

		var dataToController = {
			'phone': $mobilePhone.val(),
			'partyDate': $partyDate.val(),
			'partyPinCode': $partyPinCode.val(),
			'providerPinCode': $('#hidProviderPincode').val(),
			'pageUrl': window.location.href
		};

		showDesBookNowLoader();
		displayLoadingDiv();
		returnToBasicState();

		var $ajaxUrl = $bookNowSubmitBtn.data('url');

		$.ajax({
			url: $bookNowSubmitBtn.data('url'),
			dataType: 'json',
			type: 'POST',
			data: dataToController,
			success: function (data) {
				if (data.success === true) {
					totalBookingAmount = data.totalBookingAmount;
					partyDist = data.partyDist;
					distFreeMeters = data.distFreeMeters;
					transCharges = data.transCharges;
					tokenAmount = data.tokenAmount;
					formatTokenAmount = data.formatTokenAmount;
					formatBaseBookingAmount = data.formatBaseBookingAmount;
					formatTotalBookingAmount = data.formatTotalBookingAmount;
					formatTransCharges = data.formatTransCharges;
					customerPinCode = $partyPinCode.val();
					if (partyDist <= distFreeMeters) {
						feasibilityCheck = true;
						productFeasibility = 1;
						/* @see: don not change order */
						ticketAlertToTeam();
						showCheckoutPage();
						/* @todo: show checkout page */
					} else if ((partyDist > distFreeMeters) && (partyDist <= distMaxMeters)) {
						feasibilityCheck = true;
						productFeasibility = 1;
						ticketAlertToTeam();
						calculateAndShowAmount();

						/* exception */
						$('.des-form-content').addClass('hide');

						hideLoadingDiv();
						hideDesBookNowLoader();
					} else if (partyDist > distMaxMeters) {
						productFeasibility = 0;
						$('.form-normal-text').addClass('hide');
						$('.feasibility-fail-text').removeClass('hide');
						$('.updated-pincode').text(customerPinCode);
						$('.sticky-form').removeClass('hide');
						hideLoadingDiv();
						hideDesBookNowLoader();
						ticketAlertToTeam();
					}
				} else if (data.success === false) {
					if (data.error) {
						productFeasibility = 0;
						$formErrorMsg.text(data.error);
						if (data.is_live === 0) {
							var $submitButton = $('.product-btn-book-now');
							$submitButton.text('Option Unavailable');
							$submitButton.attr('disabled', true);
						}
						$formErrorMsg.removeClass('hide');
						hideLoadingDiv();
						hideDesBookNowLoader();
						ticketAlertToTeam();
					} else {
						productFeasibility = 0;
						hideLoadingDiv();
						hideDesBookNowLoader();
						ticketAlertToTeam();
						window.showNotyError('Please verify the entered Pin code and try again');
					}
				} else {
					productFeasibility = 0;
					hideLoadingDiv();
					hideDesBookNowLoader();
					ticketAlertToTeam();
					window.showNotyError('Some error occurred while processing your data. Please try again after refreshing the page');
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				productFeasibility = 0;
				ticketAlertToTeam();
				hideLoadingDiv();
				hideDesBookNowLoader();
				window.notifyTeam({
					"url": $ajaxUrl,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});
	}

	function changePinCodeState() {
		returnToBasicState();
		$('#partyPincode').focus();
		feasibilityCheck = false;
		$('.sticky-form-title-text').removeClass('hide');
		$('.sticky-form-no-title-text').addClass('hide');
		hideLoadingDiv();
		hideDesFeasibilityLoader();
		hideDesBookNowLoader();
	}

	function updateUserData() {
		updateInput($('#partyPincode'), upPartyPinCode);
		updateInput($('#bookNowPartyPinCode'), upPartyPinCode);

		updateInput($('#mobilePhone'), upPhone);
		updateInput($('#bookNowPhone'), upPhone);

		/* updateInput($('#partyDate'), upPartyDate); */
		/* updateInput($('#bookNowPartyDate'), upPartyDate); */
	}

	function updateInput($varId, $value) {
		if ($value !== null) {
			$varId.val($value);
			if ($value !== "") {
				$varId.parent().addClass('is-dirty');
			} else {
				$varId.parent().removeClass('is-dirty');
			}
		}
	}

	function displayLoadingDiv() {
		/* var stickyFormHeight = $('.sticky-form.wrap').outerHeight(); */
		/* $loadingSectionWrap.css({"height": stickyFormHeight}); */
		$loadingSectionWrap.animate({
			height: 'show'
		}, "fast");
	}

	function hideLoadingDiv() {
		$loadingSectionWrap.animate({
			height: 'hide'
		}, "fast");
	}

	function displayStickyForm() {
		returnToBasicState();
		$stickyFormWrap.animate({
			height: 'show'
		}, "fast");
	}

	function hideStickyForm() {
		$stickyFormWrap.animate({
			height: 'hide'
		}, "fast");
	}

	function displayStickyBookNowForm() {
		returnToBasicState();
		$stickyBookNowFormWrap.animate({
			height: 'show'
		}, "fast");
	}

	function hideStickyBookNowForm() {
		$stickyBookNowFormWrap.animate({
			height: 'hide'
		}, "fast");
	}

	function displayStickyEnquiryForm() {
		$stickyEnquiryErrorMsg.text('');
		$stickyEnquiryFormWrap.animate({
			height: 'show'
		}, "fast");
	}

	function hideStickyEnquiryForm() {
		$stickyEnquiryFormWrap.animate({
			height: 'hide'
		}, "fast");
	}

	function showStickyEnquiryLoader() {
		$stickyEnquiryLoader.animate({
			height: 'show'
		}, "fast");
	}

	function hideStickyEnquiryLoader() {
		$stickyEnquiryLoader.animate({
			height: 'hide'
		}, "fast");
	}

	function showDesFeasibilityLoader() {
		/* var pPFormHeight = $('.des-product-feasibility-form').outerHeight(); */
		var pPFormHeight = $('.modal-content').outerHeight();
		$desFormLoader.css({"height": pPFormHeight});
		$desFormLoader.removeClass('hide');
		$('.des-product-feasibility-form').addClass('hide');
	}

	function hideDesFeasibilityLoader() {
		$('.des-product-feasibility-form').removeClass('hide');
		$desFormLoader.addClass('hide');
	}

	function showDesBookNowLoader() {
		/* var pPFormHeight = $('.des-product-book-now-form').outerHeight(); */
		/* var pPFormHeight = $('#bookNowModal .modal-content').outerHeight(); */
		$desFormLoader.css({"height": "300px"});
		$desFormLoader.removeClass('hide');
		$('.des-product-book-now-form').addClass('hide');
	}

	function hideDesBookNowLoader() {
		$('.des-product-book-now-form').removeClass('hide');
		$desFormLoader.addClass('hide');
	}

	function showDesBookModal() {
		$formErrorMsg.addClass('hide');
		$bookingFeasibilityModal.modal('show');
	}

	function feasibilityModalBasicState() {
		$('.des-feasibility-form-wrap').addClass('hide');
		$('.des-book-now-form-wrap').addClass('hide');
	}

	function ticketAlertToTeam() {
		var otherCityDistStatus = "no";
		if (productFeasibility === 0) {
			otherCityDistStatus = window.checkFeasibilityInOtherCities();
		}
		var $mobilePhone = $('#bookNowPhone');
		if ($mobilePhone.val().length === 10 && $mobilePhone.val() !== oldPhone) {
			oldPhone = $mobilePhone.val();
			var $ajaxUrl = $('#hidFailedToProceedData').val();
			$.ajax({
				url: $ajaxUrl,
				dataType: 'json',
				type: 'POST',
				data: {
					'phone': $mobilePhone.val(),
					'partnerPinCode': $('#hidProviderPincode').val(),
					'partyPinCode': $('#bookNowPartyPinCode').val(),
					'productFeasibility': productFeasibility,
					'typeDevice': $('#hidTypeDevice').val(),
					'typeBrowser': $('#hidTypeBrowser').val(),
					'partyDist': partyDist,
					'distFreeMeters': distFreeMeters,
					'transCharges': transCharges,
					'otherCitiesLimitStatus': otherCityDistStatus
				},
				error: function (jqXHR, textStatus, errorThrown) {
					window.notifyTeam({
						"url": $ajaxUrl,
						"textStatus": textStatus,
						"errorThrown": errorThrown
					});
				}
			});
		}
	}

	(function initPlugins() {
		if (window.location.href.indexOf('#bookNowModal') != -1) {

			if (!(typeof (componentHandler) == 'undefined')) {
				componentHandler.upgradeAllRegistered();
			}

			$('.des-book-now-form-wrap').removeClass('hide');
			showDesBookModal();
			displayStickyBookNowForm();

			/* @see: will always be active */
			$('#bookNowPhone').focus();
		}

		$bookingFeasibilityModal.modal({
			backdrop: 'static',
			keyboard: false,
			show: false
		});
		/*getSimilarCategoryCards(); */
		showSimilarProducts();
	})();

	(function eventHandlers() {

		$bookNowButton.click(function (event) {
			event.preventDefault();

			if (feasibilityCheck === false) {
				displayStickyForm();
			} else {
				displayLoadingDiv();
				showCheckoutPage();
			}
		});

		$closeFormButton.click(function (event) {
			event.preventDefault();

			hideStickyForm();
		});

		$stickyFormClose.click(function (event) {
			event.preventDefault();

			hideStickyForm();
		});

		$preCheckFeasibilityButton.click(function (event) {
			event.preventDefault();

			updateUserData();
			displayStickyForm();
		});

		$preCheckBookNowButton.click(function (event) {
			event.preventDefault();

			updateUserData();
			displayStickyBookNowForm();
		});

		$proceedButton.click(function (event) {
			event.preventDefault();

			displayLoadingDiv();
			upPartyDate = $('#profilePartyDate').val();
			showCheckoutPage();
		});

		$changePartyLocation.click(function (event) {
			event.preventDefault();

			displayLoadingDiv();
			setTimeout(changePinCodeState, 1000);

		});

		$checkFeasibilityButton.click(function (event) {
			event.preventDefault();

			checkFeasibilityOfTheProduct();
		});

		$desFeasibilityBookNowBtn.click(function (event) {
			event.preventDefault();

			showDesFeasibilityLoader();
			upPartyDate = $('#profilePartyDate').val();
			showCheckoutPage()
		});

		$desBookNowBtn.click(function (event) {
			event.preventDefault();

			showDesBookNowLoader();
			showCheckoutPage()
		});

		$desCheckFeasibilityBtn.click(function (event) {
			event.preventDefault();

			checkFeasibilityOfTheProduct();
		});

		$desBackBtn.click(function (event) {
			event.preventDefault();

			/* showDesLoader(); */
			setTimeout(function () {
				returnToBasicState();
				$('.des-feasibility-form').addClass('hide');
				$('.des-book-now-form').addClass('hide');
				$('.product-cta').removeClass('hide');
				/* hideDesLoader(); */
			}, 500);
		});

		$desFeasibilityChangePinCode.click(function (event) {
			event.preventDefault();

			showDesFeasibilityLoader();
			setTimeout(function () {
				changePinCodeState();
			}, 500);
		});

		$desBookNowChangePinCode.click(function (event) {
			event.preventDefault();

			showDesBookNowLoader();
			setTimeout(function () {
				changePinCodeState();
			}, 500);
		});

		$mobProfileEnquiryBtn.click(function (event) {
			event.preventDefault();

			displayStickyEnquiryForm();
		});

		$mobProfileEnquiryCloseBtn.click(function (event) {
			event.preventDefault();

			hideStickyEnquiryForm();
		});

		$mobPostEnquiryBtn.click(function (event) {
			event.preventDefault();

			postProfileEnquiry();
		});

		$mobCloseBookNowBtn.click(function (event) {
			event.preventDefault();

			hideStickyBookNowForm();
		});

		$desPreFeasibilityBtn.click(function (event) {
			event.preventDefault();

			/* showDesLoader(); */
			/* updateUserData(); */
			/* showDesFeasibilityForm(); */
			/* hideDesLoader(); */

			feasibilityModalBasicState();
			$('.des-feasibility-form-wrap').removeClass('hide');
			returnToBasicState();
			updateUserData();
			showDesBookModal();
		});

		// @see: check if the button should be enabled or disabled
		if(window.isCityDisabled()) {
			// disable the button/link
			$desPreBookNowBtn.attr('disabled', true);
			$desPreBookNowBtn.on('click', false);

			$bookNowSubmitBtn.attr('disabled', true);
			$bookNowSubmitBtn.on('click', false);

			var $productBtnBookNow = $('.product-btn-book-now');
			if  ($productBtnBookNow) {
				$productBtnBookNow.attr('disabled', true);
				$productBtnBookNow.on('click', false);
			}
		} else {
			$desPreBookNowBtn.click(function (event) {
				event.preventDefault();
	
				/* showDesLoader(); */
				/* updateUserData(); */
				/* showDesBookNowForm(); */
				/* hideDesLoader(); */
	
				
				feasibilityModalBasicState();
				$('.des-book-now-form-wrap').removeClass('hide');
				returnToBasicState();
				updateUserData();
				showDesBookModal();
			});

			$bookNowSubmitBtn.click(function (event) {
				event.preventDefault();
	
				checkBookingFeasibility();
			});
		}

	})();

	$('.btn-init-dist-check').on('click', function (event) {
		event.preventDefault();

		$('.btn-init-dist-check').addClass('hide');
		$('.dist-check-wrap').removeClass('hide');

		$('.product-dist-check-msg').addClass('hide');
		$('.product-dist-check-error').addClass('hide');
	});

	$('.dis-check-btn').on('click', function (event) {
		event.preventDefault();

		$('.generic-page-loader-wrap').removeClass('hide');

		$('.product-dist-check-msg').addClass('hide');
		$('.product-dist-check-error').addClass('hide');

		var $errorMsg = "Oops! Something seems to be wrong. Kindly refresh the page and try again";
		var $ajaxUrl = $(this).data('url');
		var $distCheckLocationPin = $('#distCheckLocationPin').val();

		if ($distCheckLocationPin === "") {
			$errorMsg = "Kindly enter a location pincode and try again";
			$('.product-dist-check-error').text($errorMsg);
			$('.product-dist-check-error').removeClass('hide');
			$('.generic-page-loader-wrap').addClass('hide');
			return false;
		}

		$('.product-dist-check-error').text($errorMsg);

		$.ajax({
			url: $ajaxUrl,
			dataType: 'json',
			type: 'POST',
			data: {
				venuePin: $(this).data('pin'),
				locationPin: $distCheckLocationPin
			},
			success: function (data) {
				if (data.success === true) {
					$('.generic-page-loader-wrap').addClass('hide');
					if (data.distance || (data.distance === 0)) {
						var $dist = parseInt((data.distance / 1000), 10);
						$('.dist-check-number').text($dist);
						$('.product-dist-check-msg').removeClass('hide');
					} else {
						$('.generic-page-loader-wrap').addClass('hide');
						if (data.errorMsg) {
							$errorMsg = data.errorMsg;
						}
						$('.product-dist-check-error').text($errorMsg);
						$('.product-dist-check-error').removeClass('hide');
					}
				} else {
					$('.generic-page-loader-wrap').addClass('hide');
					if (data.errorMsg) {
						$errorMsg = data.errorMsg;
					}
					$('.product-dist-check-error').text($errorMsg);
					$('.product-dist-check-error').removeClass('hide');
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				$('.generic-page-loader-wrap').addClass('hide');
				$('.product-dist-check-error').text($errorMsg);
				$('.product-dist-check-error').removeClass('hide');
				window.notifyTeam({
					"url": $ajaxUrl,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}

		});
	});

	function calculateAndUpdatePrices() {
		var productPrice = parseInt($('#pcProductPrice').val(), 10);
		var productPriceWorth = parseInt($('#pcProductPriceWorth').val(), 10);
		var productPriceOff = productPriceWorth - productPrice;
		var totalAddOnAmount = 0;
		var pricePerExtraGuest = $("#pcPricePerExtraGuest").data('value');
		var guestCount = $("#pcGuestsCount").val();
		var groupCount = $("#pcGuestsCount").data('groupcount');
		var bookingPrice = productPrice;
		var extraGuestsPrice = 0;
		var extraGuestsCount = 0;
		var bookingPriceWorth = productPriceWorth;
		var bookingPriceOff = productPriceOff;

		$('.product-price-wrap').addClass('hide');
		$('.product-price-loading-wrap').removeClass('hide');

		if (guestCount >= groupCount) {
			extraGuestsCount = guestCount - groupCount;
			extraGuestsPrice = extraGuestsCount * pricePerExtraGuest;
			bookingPrice = productPrice + extraGuestsPrice;
			bookingPrice = parseInt(bookingPrice, 10);

			bookingPriceWorth = productPriceWorth + extraGuestsPrice;
			bookingPriceWorth = parseInt(bookingPriceWorth, 10);
		}

		$('.product-book-price-per-extra-guest').html(priceFormat(parseInt(pricePerExtraGuest, 10)));
		$('#pcPricePerExtraGuest').html(priceFormat(parseInt(pricePerExtraGuest, 10)));
		$('#pcPricePerExtraGuest').data('value', parseInt(pricePerExtraGuest, 10));

		$('.product-book-extra-guests-count').html(parseInt(extraGuestsCount, 10));
		$('#pcExtraGuestsCount').html(parseInt(extraGuestsCount, 10));
		$('.product-book-extra-guests-price').html(priceFormat(parseInt(extraGuestsPrice, 10)));
		$('#pcExtraGuestsPrice').html(priceFormat(parseInt(extraGuestsPrice, 10)));

		/* regarding venue deal - not necessary right now
		 $venueDealToken.addClass('hide');
		 var newVenueDealPrice = parseInt($venueDealPrice.val() * $guestsCount.val() * $percentage * 0.01, 10);
		 $venueDealNewToken.html(newVenueDealPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
		 $('.venue-deal-price-wrap').removeClass('hide');
		 */

		/* update booking amount with add-ons amount */
		var $addOn = $('.add-on-cta-list');

		if ($addOn.length > 0) {
			totalAddOnAmount = parseInt($("#pcAddOnsPrice").data('price'), 10);

			$('.product-book-addons-price').html(priceFormat(parseInt(totalAddOnAmount, 10)));
			$("#pcAddOnsPrice").html(priceFormat(parseInt(totalAddOnAmount, 10)));
			$("#pcAddOnsPrice").data('price', parseInt(totalAddOnAmount, 10));
		}

		$(".product-book-price").html(priceFormat(parseInt(productPrice, 10)));
		$("#pcBookingPrice").html(priceFormat(parseInt(bookingPrice, 10)));
		$("#pcBookingPrice").data('price', parseInt(bookingPrice, 10));

		$("#pcBookingWorth").html(priceFormat(parseInt(bookingPriceWorth, 10)));
		$("#pcBookingWorth").data('price', parseInt(bookingPriceWorth, 10));

		productPriceOff = productPriceWorth - productPrice;
		bookingPriceOff = bookingPriceWorth - bookingPrice;
		if (bookingPriceOff > 0) {
			$("#pcProductPriceOff").html(priceFormat(parseInt(bookingPriceOff, 10)));
			$("#pcProductPriceOff").data('price', parseInt(bookingPriceOff, 10));
		}

		$(".product-book-total-booking-amount").html(priceFormat(parseInt((bookingPrice + totalAddOnAmount), 10)));
		$("#pcTotalBookingAmount").html(priceFormat(parseInt((bookingPrice + totalAddOnAmount), 10)));
		$("#pcTotalBookingAmount").data('price', parseInt((bookingPrice + totalAddOnAmount), 10));

		/* update advance amount too */
		$('#pcAdvanceAmount').html(priceFormat(calculateRoundedAdvance(parseInt(bookingPrice, 10))));
		$('#pcAdvanceAmountValentineDay').html(parseInt(bookingPrice, 10));

		setTimeout(function () {
			$('.product-price-wrap').removeClass('hide');
			$('.product-price-loading-wrap').addClass('hide');
		}, 50);
	}

	$("#pcGuestsCount").on('change', function () {
		calculateAndUpdatePrices();
	});

	if ($('.bachelor-checkout-date').length) {

		if ($('#hidMinBookingDate').length && $('#hidMinBookingDate').val()) {
			$('.bachelor-checkout-date').val($('#hidMinBookingDate').val());
		}

		$('.bachelor-checkout-date').on('change', function () {

			if (!$('.bachelor-checkout-date').val()) {
				return false;
			}

			/* show loader */
			$('.generic-page-loader-wrap').removeClass('hide');

			var $ajaxUrl = "/ajax/auto-book/dynamic-price";
			$.ajax({
				url: $ajaxUrl,
				type: "POST",
				dataType: "json",
				data: {
					'date': $(this).val(),
					'cityId': $('#hidCityId').val(),
					'occasionId': $('#hidOccasionId').val(),
					'productTypeId': $('#hidProductTypeId').val(),
					'productId': $('#hidProductId').val()
				},
				success: function (data) {
					$('.generic-page-loader-wrap').addClass('hide');
					if (data.success) {
						$('#pcProductPrice').val(data.productPrice);
						$('#pcProductPriceWorth').val(data.priceWorth);
						$("#pcPricePerExtraGuest").data('value', data.pricePerExtraGuest);
						var selectedGuestCount = $('#pcGuestsCount').val();
						if (data.groupCount && data.maxGuestCount) {
							var $element = "";
							for (var $i = data.groupCount; $i <= data.maxGuestCount; $i++) {
								var $selected = "";
								if ($i == selectedGuestCount) {
									$selected = "selected";
								}
								$element += "<option value='" + $i + "' " + $selected + ">" + $i + "</option>";
							}

							if ($element != "") {
								$('#pcGuestsCount').empty().append($element);
								$('#pcGuestsCount').data('groupcount', data.groupCount);
							}
						}
						calculateAndUpdatePrices();
					} else {
						/* keep the prices as it is */
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					$('.generic-page-loader-wrap').addClass('hide');
					window.notifyTeam({
						"url": $ajaxUrl,
						"textStatus": textStatus,
						"errorThrown": errorThrown
					});
				}
			});
		});
	}

	/* Book Now pre-checkout modal */
	(function handlePreCheckoutView() {
		var pageUrl = window.location.href;
		if (pageUrl.indexOf('#bookNowModal') !== -1) {
			$('#bookNowModal').modal('show');
		}

		var $btnPcSubmit = $('#btnPcSubmit');
		if ($('#phone').val().length !== 10) {
			$btnPcSubmit.prop('disabled', true);
		}

		var $venueDealPartyDate = $('#partyVdDate');
		$('#autoBookingForm').find('input').change(function () {
			if ($('#phone').val().length === 10) {
				$btnPcSubmit.prop('disabled', false);
			} else {
				$btnPcSubmit.prop('disabled', true);
			}
			if ($venueDealPartyDate.length && !$venueDealPartyDate.val()) {
				$btnPcSubmit.prop("disabled", true);
			}
		});

		if ($venueDealPartyDate.length && !$venueDealPartyDate.val()) {
			$btnPcSubmit.prop("disabled", true);
		}

		calculateAndUpdatePrices();
	})();

	updateAddOnPrices();
});