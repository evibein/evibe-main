$(document).ready(function () {

	(function initPlugins() {
		$('.occasion-selection-footer').addClass('hide');
		$('#loadingMostViewedProducts').addClass('hide');
		getProductReviewsAndRatings();
		/* getMostViewedProducts(); */ /* Removing until the logic is fixed */
	})();

	function getProductReviewsAndRatings() {
		var $ajaxUrl = $('#hidProductReviewsUrl').val();
		if (!$ajaxUrl) {
			return false;
		}
		$.ajax({
			url: $ajaxUrl,
			data: {
				'pageName': $('#hidPageName').val()
			},
			dataType: 'html',
			type: 'POST',
			success: function (data) {
				$('#loadingReviewsSection').addClass('hide');
				$('#showProductReviews').append(data);
				var $avgRating = $('#hidProductAvgRating').val();
				var $reviewCount = parseInt($('#hidProductReviewCount').val(), 10);
				var ratingHtml = "";
				var reviewHtml = "";
				if (!isNaN($reviewCount)) {
					ratingHtml += "<input type='number' class='avg-rating hide' value='" + $avgRating + "' title='" + $avgRating + " average rating for the provider of " + $('#hidProductName').val() + "'/>";
					reviewHtml += "<div id='reviews' class='product-rating-reviews scroll-item cur-point in-blk' data-scroll='reviewSection'> (" + $reviewCount + " reviews) </div>";

					/* Display the customer reviews reference link in title section */
					$('.customer-reviews-ref-link').removeClass('hide');
				} else {
					/* reviewHtml += "<div id='reviews' class='product-rating-reviews product-no-point cur-point in-blk'> (0 reviews) </div>"; */
					$('.product-rating').addClass('hide');

					/* @see: there won't be much time lapse */
					$('.des-pro-shortlist').css("margin", "10px 0 0 0");

				}
				$('#defaultRating').addClass('hide');
				$(ratingHtml).appendTo('#upProductAvgRating');
				$('#defaultReview').addClass('hide');
				$(reviewHtml).appendTo('#upProductReviewCount');
				applyRatingStar('.avg-rating, .review-rating');
				applyRatingStar('.provider-avg-rating');
				$('.des-pro-shortlist').css("vertical-align", "middle");
				$('.product-reviews-link').addClass('des-product-reviews-link');
			},
			error: function (jqXHR, textStatus, errorThrown) {
				$('#loadingReviewsSection').addClass('hide');
				window.notifyTeam({
					"url": $ajaxUrl,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});
	}

	function getMostViewedProducts() {
		var $ajaxUrl = $('#hidMostViewedProductsUrl').val();
		$.ajax({
			url: $ajaxUrl,
			dataType: 'html',
			type: 'POST',
			success: function (data) {
				$('#loadingMostViewedProducts').addClass('hide');
				$('#showMostViewedProducts').append(data);
				applyRatingStar('.avg-rating');
			},
			error: function (jqXHR, textStatus, errorThrown) {
				$('#loadingMostViewedProducts').addClass('hide');
				window.notifyTeam({
					"url": $ajaxUrl,
					"textStatus": textStatus,
					"errorThrown": errorThrown
				});
			}
		});
	}

	function applyRatingStar(to) {
		$(to).rating({
			showClear: 0,
			readonly: 'true',
			showCaption: 0,
			size: 'xl',
			min: 0,
			max: 5,
			step: 0.1
		});
		$(to).removeClass('hide');
	}

	(function reviewEvents() {
		$(document).on("click", ".link-read", function (event) {
			event.preventDefault();
			$(this).parents(".r-txt-wrap")
				.find(".text-extras")
				.toggleClass("hide");
		});

		/* to make the handlers bind to the dynamically generated elements */

		$(document).on("change", "#rSortOptions", function (event) {
			event.preventDefault();
			var sortClass = "." + $(this).val();
			if ($(sortClass).length) {
				$(".r-review").addClass("hide");
				$(sortClass).removeClass("hide");
			}
		});

		$('#desProfileEnquiry').click(function (event) {
			event.preventDefault();
			$('.btn-post').click();

			$('#modalEnquiryForm').on('shown.bs.modal', function (e) {

				/* if any profile related link exists */
				if ($('#hidProfileEnqUrl').length) {
					$('#enquiryForm').attr('action');
					$('#enquiryForm').attr('action', $('#hidProfileEnqUrl').val());
				}
			})
		});

	})();

	/* functionality to hide and show add-ons content */
	(function toggleAddOnsContent() {
		var overflowedContent = $('.content-overflow'),
			overflowBtn = $('.btn-overflow'),
			overflowedContentHeight = overflowedContent.height();

		if (overflowedContentHeight > 200) {
			overflowedContent.animate({'height': '200px'});
			overflowBtn.addClass('less');
			overflowBtn.css('display', 'inline-block');
		}

		overflowBtn.on('click', function (e) {
			e.preventDefault();

			if (overflowBtn.hasClass('less')) {
				overflowedContent.animate({'height': overflowedContentHeight});
				overflowBtn.removeClass('less');
				overflowBtn.addClass('more');
				overflowBtn.text('- Show less add-ons');

			} else {
				overflowedContent.animate({'height': '200px'});
				overflowBtn.addClass('less');
				overflowBtn.removeClass('more');
				overflowBtn.text('+ Show more add-ons');
			}
		});
	})();

	/* Functionality to hide and show faqs */
	(function toggleFAQs() {
		var overflowedContent = $('.product-faq-content-overflow'),
			overflowBtn = $('.product-faq-btn-overflow'),
			overflowedContentHeight = overflowedContent.height();

		if (overflowedContentHeight > 250) {
			overflowedContent.animate({'height': '200px'});
			overflowBtn.addClass('less');
			overflowBtn.css('display', 'inline-block');
		}

		overflowBtn.on('click', function (e) {
			e.preventDefault();

			if (overflowBtn.hasClass('less')) {
				overflowedContent.animate({'height': overflowedContentHeight});
				overflowBtn.removeClass('less');
				overflowBtn.addClass('more');
				overflowBtn.text('- Show Less FAQs');

			} else {
				overflowedContent.animate({'height': '200px'});
				overflowBtn.addClass('less');
				overflowBtn.removeClass('more');
				overflowBtn.text('+ Show More FAQs');
			}
		});
	})();
});