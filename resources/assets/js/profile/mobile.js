$(document).ready(function () {

	/*
	 function activateTab(tab) {
	 var $activatedTab = $('.product-info-tabs a[href="#' + tab + '"]');
	 $activatedTab.tab('show');
	 $('.product-info-tabs a').removeClass('scrollmenu-element-active');
	 $('.product-info-tabs a').removeClass('product-info-tab-active');
	 $activatedTab.addClass('scrollmenu-element-active');
	 $activatedTab.addClass('product-info-tab-active');
	 }

	 (function initPlugins() {
	 activateTab('inclusions');
	 })();

	 (function eventsHandler() {
	 $('.scroll-to-reviews').click(function (event) {
	 event.preventDefault();
	 activateTab('productReviews');
	 });

	 $('.product-info-tabs a').click(function (event) {
	 event.preventDefault();
	 $('.product-info-tabs a').removeClass('scrollmenu-element-active');
	 $('.product-info-tabs a').removeClass('product-info-tab-active');
	 $(this).addClass('scrollmenu-element-active');
	 $(this).addClass('product-info-tab-active');
	 });
	 })();

	 */

	/* sticky nav buttons */
	(function moveScroller() {
		var $anchor = $(".scroll-anchor");
		var $scroller = $('.mob-pro-into-section');
		var $emptyDiv = $('.scroll-empty-div');

		if($anchor.length && $scroller.length && $emptyDiv.length) {
			var $scrollDivHeight = $scroller.outerHeight();

			var move = function () {
				var st = $(window).scrollTop();
				var ot = $anchor.offset().top;
				if (st > ot) {
					$scroller.css({
						position: "fixed",
						top: "0px",
						background: "#FBFBFB"
					});
					$emptyDiv.css({
						height: $scrollDivHeight
					});
				} else {
					$scroller.css({
						position: "relative",
						top: "",
						background: "#FFFFFF"
					});
					$emptyDiv.css({
						height: "0"
					});
				}
			};
			$(window).scroll(move);
			move();
		}
	})();

	/* functionality to hide and show add-ons content */
	/* written separately in desktop and mobile pages to make it wok in mobile info tab */
	(function toggleAddOnsContent() {
		var overflowedContent = $('.content-overflow'),
			overflowBtn = $('.btn-overflow'),
			overflowedContentHeight = overflowedContent.height();

		if (overflowedContentHeight > 250) {
			overflowedContent.animate({'height': '250px'});
			overflowBtn.addClass('less');
			overflowBtn.css('display', 'inline-block');
		}

		overflowBtn.on('click', function (e) {
			e.preventDefault();

			if (overflowBtn.hasClass('less')) {
				overflowedContent.animate({'height': overflowedContentHeight});
				overflowBtn.removeClass('less');
				overflowBtn.addClass('more');
				overflowBtn.text('- Show less add-ons');

			} else {
				overflowedContent.animate({'height': '250px'});
				overflowBtn.addClass('less');
				overflowBtn.removeClass('more');
				overflowBtn.text('+ Show more add-ons');
			}
		});
	})();

	/* booking form clicked, show modal */
	$('#mobileProfileEnq').click(function (event) {
		event.preventDefault();

		$('#modalEnquiryForm').modal({
			"backdrop": 'static',
			"keyboard": false,
			"show": true
		});

		/* to negate any profile page url action */
		$('#enquiryForm').attr('action');
		$('#enquiryForm').attr('action', $('#hidProfileEnqUrl').val());
	});

	/* added GA Event if FAQs tab is toggled */
	$('#faqsTab').on('click', function (event) {
		event.preventDefault();
	});
});