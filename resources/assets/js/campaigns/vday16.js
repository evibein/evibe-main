$(document).ready(function () {

	function getQueryParams(a) {
		if (a == "") return {};
		var b = {};
		for (var i = 0; i < a.length; ++i) {
			var p = a[i].split('=');
			if (p.length != 2) continue;
			b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
		}
		return b;
	}

	function formUrl(cUrl, key, value, isIgnore) {
		cUrl = cUrl ? cUrl.substr(cUrl.indexOf('?') + 1).split('&') : window.location.search.substr(1).split('&');
		var cParams = getQueryParams(cUrl), queryString = "", loc = window.location;
		var url = loc.protocol + '//' + loc.host + loc.pathname;
		if (isIgnore) { /* remove from current object */
			if (cParams.hasOwnProperty(key)) {
				try {
					delete cParams[key];
				} catch (e) {
				}
			}
		} else {
			cParams[key] = value;
		}
		for (key in cParams) { /* form string */
			queryString += key + "=" + cParams[key] + "&";
		}
		if (queryString) {
			url += '?' + queryString;
			url = url.slice(0, -1); /* remove trailing '&' */
		}
		return url;
	}

	function refreshUrl(url) {
		window.location.href = url;
	}

	function setCatLinks() {
		var url = "";
		$.each($(".cat-link"), function (i, item) {
			url = $(item).data("url");
			$(item).attr("href", formUrl(formUrl(null, "page", null, true), "category", url));
		});

		var allStylesUrl = formUrl(null, "page", null, true);
		allStylesUrl = formUrl(allStylesUrl, "category", "all");
		$(".all-styles").attr("href", allStylesUrl);
	}

	/* allow only number */
	$(".price-input").keydown(function (e) {
		/* Allow: backspace, delete, tab, escape, enter */
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
			(e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) || /* Allow: Ctrl+A, Command+A */
			(e.keyCode >= 35 && e.keyCode <= 40)) { /* Allow: home, end, left, right, down, up */
			return; /* let it happen, don't do anything */
		}
		/* Ensure that it is a number and stop the keypress */
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});

	/* price filter */
	$("#filterPriceBtn").click(function (event) {
		event.preventDefault();
		var priceMin = parseInt($("#priceMin").val(), 10);
		var priceMax = parseInt($("#priceMax").val(), 10);
		if (priceMax < priceMin) return false;
		refreshUrl(formUrl(formUrl('', 'price_min', priceMin), 'price_max', priceMax));
	});

	/* sort options */
	$('.sort-option').click(function (event) {
		event.preventDefault();
		refreshUrl(formUrl('', 'sort', $(this).data('value')));
	});

	/* rating stars */
	$('.provider-avg-rating').rating({
		showClear: 0,
		readonly: 'true',
		showCaption: 0,
		size: 's',
		min: 0,
		max: 5,
		step: 0.1
	});
	$('.provider-avg-rating, .rating-wrap').removeClass('hide');

	setCatLinks(); /* set links */
});