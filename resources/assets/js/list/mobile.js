$(document).ready(function () {

	var $listSortWrap = $('#listSortWrap');
	var $listSortEmpty = $('#listSortEmpty');
	var $listSortBtn = $('#listSortBtn');
	var $ListFiltersWrap = $('#listFiltersWrap');
	var $listFilterBtn = $('#listFilterBtn');
	var $listFilterClose = $('#listFilterClose');
	var $listBtnBidding = $('.list-custom-upload-btn');
	var $listSlideTitle = $('#listSlideTitle');
	var $listBottomSectionWrap = $('#listBottomSectionWrap');

	(function initPlugins() {
		/* getListRatings(); */
		$('.partybag-wrap-mobile').addClass('hide');
		$('.footer-top').addClass('hide');
		/* $('.mobile-list-custom-title').css("font-size","16px"); */

		$('.scroll-empty-div').css({
			height: "0"
		});

		var footerDivTop = $('.mobile-list-footer').offset().top;
		$(window).scroll(function () {
			if (($(window).scrollTop()) > footerDivTop) { /* scrolled past the footer div? can be changed to 550 */
				$listBottomSectionWrap.addClass('hide');
				/* reached the desired point -- hide div */
			} else {
				$listBottomSectionWrap.removeClass('hide');
				/* crossed the desired point -- show div */
			}
		});
	})();

	$listBtnBidding.click(function (event) {
		event.preventDefault();

		$('.btn-bidding').click();
	});

	$listSlideTitle.click(function (event) {
		event.preventDefault();

		hideSliderMenu();
	});

	$('#listSlideEnquire').click(function (event) {
		event.preventDefault();

		hideSliderMenu();
		$('.btn-post').click();
	});

	$listSortBtn.click(function (event) {
		event.preventDefault();

		showSortOptions();
	});

	$listSortEmpty.click(function (event) {
		event.preventDefault();

		hideSortOptions();
	});

	$listFilterBtn.click(function (event) {
		event.preventDefault();

		showFilterOptions();
	});

	$listFilterClose.click(function (event) {
		event.preventDefault();

		hideFilterOptions();
	});

	function showSortOptions() {
		$listSortWrap.animate({
			height: 'show'
		}, "fast");

		/* Hiding popup fab button */
		if($(".fab-auto-popup").length > 0) {
			$(".fab-auto-popup").addClass("hide");
		} else {
			$(".fab-auto-popup-call").addClass("hide");
		}
	}

	function hideSortOptions() {
		$listSortWrap.animate({
			height: 'hide'
		}, "fast");

		if($(".fab-auto-popup").length > 0) {
			$(".fab-auto-popup").removeClass("hide");
		} else {
			$(".fab-auto-popup-call").removeClass("hide");
		}
	}

	function showFilterOptions() {
		$ListFiltersWrap.animate({
			height: 'show'
		}, 200);
		$('body').css("overflow", "hidden");

		if($(".fab-auto-popup").length > 0) {
			$(".fab-auto-popup").addClass("hide");
		} else {
			$(".fab-auto-popup-call").addClass("hide");
		}
	}

	function hideFilterOptions() {
		$ListFiltersWrap.animate({
			height: 'hide'
		}, 200);
		$('body').css("overflow", "visible");

		if($(".fab-auto-popup").length > 0) {
			$(".fab-auto-popup").removeClass("hide");
		} else {
			$(".fab-auto-popup-call").removeClass("hide");
		}
    }

	/*function moveFilterScroller() {
		var $anchor = $(".scroll-anchor");
		var $scroller = $('.mobile-list-sort-filter');
		var $emptyDiv = $('.scroll-empty-div');
		var $scrollDivHeight = $scroller.outerHeight();

		if($scroller.length) {
			var move = function () {
				var st = $(window).scrollTop();
				var ot = $anchor.offset().top;
				if (st > ot) {
					$scroller.css({
						position: "fixed",
						top: "0px"
					});
					$emptyDiv.css({
						height: $scrollDivHeight
					});
				} else {
					$scroller.css({
						position: "relative",
						top: ""
					});
					$emptyDiv.css({
						height: "0"
					});
				}
			};
			$(window).scroll(move);
			move();
        }
	}*/
});