$(document).ready(function () {

	(function initPlugins() {
		applyRatingStar('.avg-rating');
	})();

	function applyRatingStar(to) {
		$(to).rating({
			showClear: 0,
			readonly: 'true',
			showCaption: 0,
			size: 'xl',
			min: 0,
			max: 5,
			step: 0.1
		});
		$(to).removeClass('hide');
	}

	(function loadOptionImages() {
		var optionIds = [];

		$('.option-profile-image').each(function () {
			optionIds.push($(this).data("optionId"));
		});

		if (optionIds.length > 0) {
			$.ajax({
				url: "/ajax/list/get-option-images",
				type: 'post',
				dataType: 'json',
				data: {
					optionIds: optionIds,
					optionTypeId: $('#hidOptionTypeId').val()
				}
			}).done(function (data) {
				if (data.success) {
					$.each(data.optionImageUrls, function (key, value) {
						var $el = $('.option-profile-image-' + key);
						if ($(document).width() < 500) {
							var imgHTML = '<img data-src="' + value + '" alt="' + $el.data("imageAltText") + '" class="list-option-image lazy-loading" style="z-index:3;position:absolute;left:0;top:50%;transform:translateY(-50%);width:100%; height:auto;min-height: 100px;">' +
								'<img data-src="' + value + '" alt="' + $el.data("imageAltText") + '" class="list-option-image lazy-loading" style="z-index:2;position:absolute;filter:blur(5px);left:0;width:auto;height: 100%">';
						} else {
							var imgHTML = '<img class="list-option-image lazy-loading" style="z-index:3;position:absolute;left:50%;transform:translate(-50%)" data-src="' + value + '" alt="' + $el.data("imageAltText") + '" onError="this.src=\'https://gallery.evibe.in/img/icons/gift.png\'; this.style=\'padding: 30px 60px\'">' +
								'<img class="list-option-image lazy-loading" style="z-index:2;position:absolute;filter:blur(5px);left:0;height:auto" data-src="' + value + '" alt="' + $el.data("imageAltText") + '" onError="this.src=\'https://gallery.evibe.in/img/icons/gift.png\'; this.style=\'padding: 30px 60px\'">';
						}
						$el.empty().append(imgHTML);
					});

					$.getScript("/js/util/lazyLoadImages.js", function () {
						$(".list-option-image").removeClass("lazy-loading");
						new LazyLoad();
					});
				}
			}).fail(function (jqXHR, textStatus, errorThrown) {
				window.notifyTeam({
					"url": "https://evibe.in/get-decor-images",
					"textStatus": textStatus + ", errorThrown: " + errorThrown,
					"errorThrown": $.parseJSON(jqXHR.responseText)
				});
			});
		}
	})();

	/* functionality to hide and show location filter content */
	(function toggleLocationFilterContent() {
		var overflowedContent = $('.content-overflow'),
			overflowBtn = $('.btn-overflow'),
			overflowedContentHeight = overflowedContent.height();

		if (overflowedContentHeight > 120) {
			overflowedContent.animate({'height': '120px'});
			overflowBtn.addClass('less');
			overflowBtn.css('display', 'inline-block');
		}

		overflowBtn.on('click', function (e) {
			e.preventDefault();

			if (overflowBtn.hasClass('less')) {
				overflowedContent.animate({'height': overflowedContentHeight});
				overflowBtn.removeClass('less');
				overflowBtn.addClass('more');
				overflowBtn.text('- Show Less Locations');

			} else {
				overflowedContent.animate({'height': '120px'});
				overflowBtn.addClass('less');
				overflowBtn.removeClass('more');
				overflowBtn.text('+ Show More Locations');
			}
		});
	})();
});