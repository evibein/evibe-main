@extends('layout.base')

@section('page-title')
	<title>{{ $data['pageDetails']['seo_title'] }} </title>
@endsection

@section('meta-description')
	<meta name="description" content="{{ $data['pageDetails']['seo_description'] }}">
@endsection

@section('meta-keywords')
	<meta name="keywords" content="{{ $data['pageDetails']['seo_keywords'] }}">
@endsection

@section('og-title')
	<meta property="og:title" content="{{ $data['pageDetails']['seo_title'] }} | Evibe.in">
@endsection

@section('og-description')
	<meta property="og:description" content="{{ $data['pageDetails']['seo_description'] }}">
@endsection


@section("header")
	@include('base.home.header.header-home-city')
@endsection

@section('custom-css')
	<link rel="stylesheet" href="{{ elixir('css/af-home.css') }}">
	<link rel="stylesheet" href="{{ elixir('assets/css/home.css') }}">
	<link rel="stylesheet" href="{{ elixir('css/app/inspiration-hub.css') }}">
@endsection

@section("content")
	<div class=" w-1366 bg-white-imp">
		<div class="text-center container">
			<h3 class="mar-b-10" style="color:#393333;">{{$data['pageDetails']['title']}}</h3>
			<hr style="@if($agent->isMobile())width: 50%; @else width:15%; @endif margin: auto;background-color:yellow;" class="text-center no-pad-b mar-b-15-imp"/>
			<p class="page-description-wrap hide @if($agent->isMobile())text-center @endif">{{$data['pageDetails']['description']}}</p>
		</div>
		@if($agent->isMobile())
			<div class="container grid hide" id="inspirations-gallery" style="padding: 0; margin: 0 5px;">
				<div class="grid-sizer" style="width: 50% !important"></div>
				@foreach ($data['feedInfo'] as $feed)
					<div class="grid-item hero-grid__item text-center mar-b-20" style="width: 46% !important; margin: 0 2% 15px;">
						<a href="{{route('inspirations.albumInfo',['pageUrl' => $data['pageDetails']['url'],'albumUrl' => $feed['profile_url']])}}" class="a-no-decoration-black">
							<div class="inspiration-img-wrap">
								<img class="card-img-mobile" src="{{ $feed->img_url}}">
								<div class=" img-desc-mobile">
									<div class="img-title-mobile">{{$feed->title}}</div>
									<span class=" img-price-mobile">@price($feed->min_price) - @price($feed->max_price)</span>
								</div>
							</div>
						</a>
					</div>
				@endforeach
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		@else
			<div class="container grid hide" id="inspirations-gallery" style="margin:auto;margin-bottom: 30px">
				<div class="grid-sizer" style="width: 23%"></div>
				@foreach ($data['feedInfo'] as $feed)
					<div class="grid-item hero-grid__item text-center " style=";padding-right: 10px;padding-left: 10px;margin-top: 0;margin-bottom: 0">
						<a href="{{route('inspirations.albumInfo',['pageUrl' => $data['pageDetails']['url'],'albumUrl' => $feed['profile_url']])}}" class="a-no-decoration-black">
							<div class="inspiration-img-wrap">
								<img class="card-img-desk" src="{{ $feed->img_url}}">

							</div>
							<div class=" img-desc-mobile">
								<div class="img-title-mobile">{{$feed->title}}</div>
								<span class=" img-price-mobile">@price($feed->min_price) - @price($feed->max_price)</span>
							</div>
						</a>
					</div>
				@endforeach
				<div class="clearfix"></div>
			</div>
		@endif
		<div class="text-center loading-text" style="height: 80vh;">
			<h4><img src="https://evibe.in/images/loading.gif.pagespeed.ce.pRxWCNAazz.gif" style="height: 30px"></h4>
			<div class="col-xs-12 col-md-4 center col-md-offset-4 pad-t-10">
				<p style="font-style: italic;font-weight: 600">"The More You Praise & Celebrate Your Life, The More Is In Life To Celebrate"</p>
				<span class="fw-500">- Oprah Winfrey</span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection

@section("footer")
	<div class="footer-wrap">
		@include('base.home.footer.footer-common')
	</div>
@endsection

@section('js-framework')
	<script src="{{ elixir('js/af-home.js') }}"></script>
	<script type="text/javascript">
		$(".inspiration-img-wrap").hover(function () {
				$(this).find(".price-range-wrap").removeClass('hide').fadeIn(200);
			},
			function () {
				$(this).find(".price-range-wrap").addClass('hide');
			}
		)
	</script>
@endsection