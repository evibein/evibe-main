@extends('layout.base')

@section('page-title')
	<title>{{ $data['pageDetails']['seo_title'] }} | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="{{ $data['pageDetails']['seo_description'] }}">
@endsection

@section('meta-keywords')
	<meta name="keywords" content="{{ $data['pageDetails']['seo_keywords'] }}">
@endsection

@section('og-title')
	<meta property="og:title" content="{{ $data['pageDetails']['seo_title'] }} | Evibe.in">
@endsection

@section('og-description')
	<meta property="og:description" content="{{ $data['pageDetails']['seo_description'] }}">
@endsection

@section('og-url')
	<meta property="og:url" content="">
@endsection

@section('seo:schema')
	@parent
	<script type="application/ld+json">
		{
				"@context": "http://schema.org",
				"@type": "Product",
				"name": "{{$data['albumInfo']['title']}}",
				"image" : "{{$data['albumInfo']['img_url']}}",
				"description" :  "{{ $data['albumInfo']['description']}}",
				  "aggregateRating": {
				    "@type": "AggregateRating",
				    "ratingValue": "4.6",
				    "reviewCount": "1246"
				  },
				"offers": {
				    "@type": "AggregateOffer",
				    "lowPrice": "{{$data['albumInfo']['min_price']}}",
				    "highPrice": "{{$data['albumInfo']['max_price']}}",
				    "priceCurrency": "INR"
				  }
		}
	</script>
@endsection

@section("header")
	@include('base.home.header.header-home-city')
@endsection

@section('custom-css')
	<link rel="stylesheet" href="{{ elixir('css/af-home.css') }}">
	<link rel="stylesheet" href="{{ elixir('assets/css/home.css') }}">
	<link rel="stylesheet" href="{{ elixir('css/app/inspiration-hub.css') }}">
	<style>
		.desk-enquiry-inspirations {
			transition: all 0.4s;
			cursor: pointer;
		}

		.desk-enquiry-inspirations:hover {
			opacity: 0.8;
			box-shadow: 0px 8px 15px rgba(46, 229, 157, 0.4);
		}
	</style>
@endsection

@section("content")
	<div class=" w-1366  bg-white-imp ">
		<div class="text-left pad-l-15 pad-t-15 mar-b-15 product-breadcrumb">
			@if(!(is_null(getCityUrl())))
				<a href="{{ config('evibe.host') }}/{{ getCityUrl() }}" class="a-no-decoration-black in-blk" style=" color: #888;font-weight: 500">{{ ucfirst(getCityUrl()) }} </a>
				<span class="in-blk pad-l-5 pad-r-5"> > </span>
			@endif
			<a href="{{route('inspirations.feed',$data['backUrl'])}}" class="a-no-decoration-black in-blk" style=" color: #888;font-weight: 500">{{ ucfirst(($data['pageDetails']['occasion'])) }} Ideas </a>
		</div>
		<div class="clearfix"></div>
		@if(count($data['albumInfo']))
			@if($agent->isMobile())
				<div class="col-xs-12">
					<img src="{{$data['albumInfo']['img_url']}}" class="profile-img">
				</div>
				<div class="col-xs-12 text">
					<h5 style="color:#333;line-height: 28px;">{{$data['albumInfo']['title']}}</h5>
					<h6 class="no-mar-t in-blk fw-500" style="color:#ed3e72;;font-size:18px">
						@if($data['albumInfo']['min_price'] && $data['albumInfo']['max_price'])
							Price Range : @price($data['albumInfo']['min_price']) - @price($data['albumInfo']['max_price'])
						@endif
					</h6>
					<p class="album-desc" style="padding-bottom: 16px"><span class="fw-500">Description:</span><br>{{$data['albumInfo']['description']}}</p>
					<!-- CTA Button group -->
					<div class="button-group " style="margin-top: 15px;margin-bottom: 30px">
						<div>
							<span class="btn-evibe enquire-btn a-no-decoration-black contact-details-card-enquire-now" style=""><span class="glyphicon glyphicon-envelope"></span> Enquire Now</span>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<hr style="border-top: 1px solid #efefef;"/>
			@else
				<div class="container card no-pad-l profile-pic-wrap">
					<div class="col-md-6 no-pad-l no-pad-r text-center  blur-bg-wrap ">
						<img src="{{$data['albumInfo']['img_url']}}" class="thumb-img" style="border-radius: 0 !important">
						<img src="{{$data['albumInfo']['img_url']}}" class="blur-bg">
					</div>
					<div class="col-md-6 pad-l-20" style="height: 300px;overflow: hidden">
						<h5 style="color:#333;line-height: 28px;">{{$data['albumInfo']['title']}}</h5>
						<p class="album-desc"><span class="fw-500">Description:</span><br> {{$data['albumInfo']['description']}}</p>
						<div class="price-wrap">
							<h6 class="mar-t-15" style="color:#ed3e72;;font-size:18px">
								@if($data['albumInfo']['min_price'] && $data['albumInfo']['max_price'])
									Price Range : @price($data['albumInfo']['min_price']) - @price($data['albumInfo']['max_price'])
								@endif
							</h6>
						</div>
						<!-- CTA Button group -->
						<div class="button-group mar-t-30">
							<div class="full-width text-left">
								<span class="btn-evibe  desk-enquiry-inspirations enquire-btn contact-details-card-enquire-now">
									<span class="glyphicon glyphicon-envelope"></span>
									<span> Enquire Now</span>
								</span>
							</div>
						</div>
					</div>
				</div>
				<div class="usp-wrap mar-t-50">
					@include("base.home.why-us")
				</div>
			@endif
			<div class="text-center mar-t-30" @if($agent->isMobile()) style="margin-top: 30px;text-align:left;padding-left:15px" @else style="margin-top: 50px"@endif >
				@if(is_null($data['pageDetails']['occasion']))
					<h5> More Like This</h5>
				@else
					<h5> More {{$data['pageDetails']['occasion']}} Ideas
				@endif
			</div>
			@if($agent->isMobile())
				<div class="container grid hide" id="inspirations-gallery" style="padding: 0; margin: 0 5px;">
					<div class="grid-sizer" style="width: 50% !important"></div>
					@foreach ($data['feedInfo'] as $feed)
						<div class="grid-item hero-grid__item text-center mar-b-20" style="width: 46% !important; margin: 0 2% 15px;">
							<a href="{{route('inspirations.albumInfo',['pageUrl' => $data['pageDetails']['url'],'albumUrl' => $feed['profile_url']])}}" class="a-no-decoration-black">
								<div class="inspiration-img-wrap">
									<img class="card-img-mobile" src="{{ $feed->img_url}}">
									<div class=" img-desc-mobile">
										<div class="img-title-mobile">{{$feed->title}}</div>
										<span class=" img-price-mobile">@price($feed->min_price) - @price($feed->max_price)</span>
									</div>
								</div>
							</a>
						</div>
					@endforeach
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
				<div class="usp-wrap mar-t-30">
					@include("base.home.why-us")
				</div>
			@else
				<div class="container grid hide" id="inspirations-gallery" style="margin:auto">
					<div class="grid-sizer" style="width: 24%"></div>
					@foreach ($data['feedInfo'] as $feed)
						<div class="grid-item hero-grid__item text-center mar-b-20" style="padding-right: 10px;padding-left: 10px;margin-top: 0;margin-bottom: 0">
							<a href="{{route('inspirations.albumInfo',['pageUrl' => $data['pageDetails']['url'],'albumUrl' => $feed['profile_url']])}}" class="a-no-decoration-black">
								<div class="inspiration-img-wrap">
									<img class="card-img-desk" src="{{ $feed->img_url}}">
								</div>
								<div class=" img-desc-mobile">
									<div class="img-title-mobile">{{$feed->title}}</div>
									<span class=" img-price-mobile">@price($feed->min_price) - @price($feed->max_price)</span>
								</div>
							</a>
						</div>
					@endforeach
					<div class="clearfix"></div>
				</div>
			@endif
		@else
			<div class="text-center">
				<h4>Oh no!!!</h4>
				<div class="font-16 pad-b-15" style="color: #94989F;">This Album doesn't exist or removed.</div>
				<div>
					<img height="150" src="{{ $galleryUrl }}/main/img/icons/wishlist-empty.png" alt="wishlist">
				</div>
				<div class="pad-t-20">
					<a href="/" class="btn" style="border: 1px solid #ED3E72;	font-size: 20px;	color: #444444;	padding: 5px 60px;">
						VISIT HOME
					</a>
				</div>
			</div>
		@endif
	<!-- hidden data -->
		<div class="hide">
			<input type="hidden" id="inspirationProductUrl" value="{{ Request::url()}}">
		</div>

		@include('app.modals.auto-popup', [
			"cityId" => getCityId(),
			"occasionId" =>"19",
			"mapTypeId" => '',
			"mapId" => "",
			"optionName" => "",
			"countries" => (isset($data['countries']) && count($data['countries'])) ? $data['countries'] : [],
			'isInspirationsPageEnquiry' => 'true'
		]);
		@if($agent->isMobile())
			<hr style="border-top: 1px solid #efefef;"/>
			<div class="mar-t-50">
				@include("base.home.home-categories")
			</div>
		@endif
	</div>
@endsection

@section("footer")
	<div class="footer-wrap">
		@include('base.home.footer.footer-common')
	</div>
@endsection

@section('js-framework')
	<script src="{{ elixir('js/af-home.js') }}"></script>
	<script type="text/javascript">
		$(".contact-details-card-enquire-now").on("click", function () {
			$("#modalAutoPopup").modal("show");
		});
		$(".inspiration-img-wrap").hover(function () {
				$(this).find(".price-range-wrap").removeClass('hide').fadeIn(200);
			},
			function () {
				$(this).find(".price-range-wrap").addClass('hide');
			}
		)
	</script>
@endsection