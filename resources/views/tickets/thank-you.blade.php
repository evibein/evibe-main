@extends("layout.base")

@section("page-title")
	<title>Thank you, we received your enquiry | Evibe.in</title>
@endsection

@section("meta-description")
	<meta name="description" content="We have received your enquiry. We will get in touch with you soon to help you plan your party.">
@endsection

@section("og-title")
	<meta property="og:title" content="Thank you, we received your enquiry | Evibe.in">
@endsection

@section("og-description")
	<meta property="og:description" content="We have received your enquiry. We will get in touch with you soon to help you plan your party.">
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-thank-you.css') }}">
@endsection

@section("header")
	@include("base.home.header.header-home-city")
@endsection

@section('gtm-data-layer')
	@include('track.gtm-dl-ct', ['transactionId' => $ticketId, 'emailId' => $email])
@endsection

@section("content")
	<div class="thank-you-wrap">
		<div class="container">
			<div class="top-section">
				<div class="col-sm-12">
					<div class="ty-card ty-message-wrap">
						<div class="text-center">
							@if ($agent->isMobile() && !($agent->isTablet()))
								<div class="col-xs-2 col-sm-2 no-pad-l">
									<img src="{{ $galleryUrl }}/img/icons/tick-icon.png" alt="success icon" class="full-width__400-600">
								</div>
								<div class="col-xs-10 col-sm-10 no-pad">
									<div class="ty-msg-txt">
										We've received your enquiry. Our party planning expert will reach you in 2 - 4 business hours.
									</div>
								</div>
								<div class="clearfix"></div>
							@else
								<div class="col-xs-12 col-sm-12">
									<span class="mar-r-10">
										<img src="{{ $galleryUrl }}/img/icons/tick-icon.png" alt="success icon" class="ty-msg-icon">
									</span>
									<span class="ty-msg-txt">
										We've received your enquiry. Our party planning expert will reach you in 2 - 4 business hours.
									</span>
								</div>
								<div class="clearfix"></div>
							@endif
						</div>
						<div class="ty-msg-cta">
							<a class="btn btn-md btn-warning ga-ty-continue-browsing-button" href="{{ urldecode(request('from')) }}">CONTINUE BROWSING</a>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="bottom-section">
				<div class="col-sm-5">
					<div id="addFormCnt" class="ty-card">
						<div class="edit">
							<h4 class="ty-card-title text-center">Help us understand more about your party.</h4>
							<form id="additionalDetailsForm" class="form-add-details" data-url="">
								<div class="error-wrap text-danger hide">
									<i class="glyphicon glyphicon-warning-sign"></i>
									<span class="pad-l-5 error-message">Please fill all the required details.</span>
								</div>
								<div class="form-group">
									<label for="selectOccasion" class="custom-mdl-label pull-left">What is your occasion?</label>
									<select class="form-control" id="selectOccasion" name="selectOccasion">
										<option value="-1" selected="selected">-- Select Occasion --</option>
										@foreach($typeOccasions as $occasion)
											<option value="{{ $occasion['id'] }}">{{ $occasion['name'] }}</option>
										@endforeach
									</select>
								</div>
								@if (!isset($fields["askGuestsCount"]) || $fields["askGuestsCount"])
									<div class="form-group">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
											<input class="mdl-textfield__input" type="text" name="guestsCount" id="guestsCount" value=""/>
											<label class="mdl-textfield__label" for="guestsCount">Expected guests count?</label>
										</div>
									</div>
								@endif
								<div class="form-group">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<textarea class="mdl-textfield__input" name="moreServices" id="moreServices"></textarea>
										<label class="mdl-textfield__label" for="moreServices">What other services do you need?</label>
									</div>
								</div>
								<div class="form-group">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<textarea class="mdl-textfield__input" name="comments" id="comments"></textarea>
										<label class="mdl-textfield__label" for="comments">Anything else we need to know?</label>
									</div>
								</div>
								<div class="form-group">
									<label for="selectSource" class="custom-mdl-label pull-left">How did you find Evibe.in?</label>
									<select class="form-control" id="selectSource" name="selectSource">
										<option value="-1" selected="selected">-- Select Source --</option>
										@foreach($typeSources as $source)
											<option value="{{ $source['id'] }}">{{ $source['name'] }}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group text-center pad-t-10">
									<button id="btnAddFormSubmit" type="submit" class="btn btn-danger text-uppercase" data-url="{{ route("ticket.update", $ticketId) }}">Submit Details
									</button>
								</div>
							</form>
						</div>
						<div class="done hide">
							<div class="text-center">
								<img src="{{ $galleryUrl }}/img/icons/tick-icon.png" alt="tick icon">
								<h3>Thank You</h3>
								<div class="ty-msg-cta">
									<a class="btn btn-md btn-warning" href="{{ urldecode(request('from')) }}">CONTINUE BROWSING</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-7">
					<div class="ty-card ty-card-next-steps">
						<h4 class="ty-card-title">Next Steps</h4>
						<div class="steps-data mar-t-15">
							<div>
								<div class="col-md-4 no-pad-l mar-t-15">
									<div class="step-title-cnt">
										<img src="{{ $galleryUrl }}/img/icons/write.png" alt="clarify" class="step-icon">
										<span class="step-title">Clarify</span>
									</div>
									<div class="step-desc">
										Get all your queries answered from our expert planning team.
									</div>
								</div>
								<div class="col-md-4 no-pad-l mar-t-15">
									<div class="step-title-cnt">
										<img src="{{ $galleryUrl }}/img/icons/send-symbol.png" alt="provider check" class="step-icon">
										<span class="step-title">Avail. Check</span>
									</div>
									<div class="step-desc">
										We will check availability of option(s) you finalize.
									</div>
								</div>
								<div class="col-md-4 no-pad-l mar-t-15">
									<div class="step-title-cnt">
										<img src="{{ $galleryUrl }}/img/icons/finalize.png" alt="order process" class="step-icon">
										<span class="step-title">Order Process</span>
									</div>
									<div class="step-desc">
										If option(s) available, an email with order details will be sent.
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="mar-t-10">
								<div class="col-md-4 no-pad-l mar-t-15">
									<div class="step-title-cnt">
										<img src="{{ $galleryUrl }}/img/icons/credit-card.png" alt="book" class="step-icon">
										<span class="step-title">Book</span>
									</div>
									<div class="step-desc">
										Securely pay {{ config('evibe.ticket.advance.percentage') }}% of total order amount to block your slot.
									</div>
								</div>
								<div class="col-md-4 no-pad-l mar-t-15">
									<div class="step-title-cnt">
										<img src="{{ $galleryUrl }}/img/icons/wine-glasses.png" alt="relax & enjoy" class="step-icon">
										<span class="step-title">Relax & Enjoy</span>
									</div>
									<div class="step-desc">
										A coordinator will be assigned to take care of your party.
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="ty-card ty-card-adv hide">
						<h4 class="ty-card-title">Evibe.in Advantages</h4>
						<div class="adv-list">
							<div class="col-md-4">
								<div class="adv-item">
									<div class="adv-icon-cnt">
										<img src="{{ $galleryUrl }}/img/icons/ty-adv-price-tag.png" alt="best prices">
									</div>
									<div class="adv-title">Best <br>Deals</div>
									<div class="adv-desc"></div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="adv-item">
									<div class="adv-icon-cnt">
										<img src="{{ $galleryUrl }}/img/icons/ty-adv-shield.png" alt="best prices">
									</div>
									<div class="adv-title">100% Delivery <br>Guarantee</div>
									<div class="adv-desc"></div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="adv-item">
									<div class="adv-icon-cnt">
										<img src="{{ $galleryUrl }}/img/icons/ty-adv-confetti.png" alt="best prices">
									</div>
									<div class="adv-title">Hassle Free <br>Experience</div>
									<div class="adv-desc"></div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<div class="clearfix">

				</div>
			</div>
		</div>
	</div>
@endsection
@section('footer')
	@include('base.home.why-us')
@endsection

@section("javascript")
	<script type="text/javascript">
		$(document).ready(function () {
			$("#btnAddFormSubmit").click(function (event) {
				event.preventDefault();

				var that = $(this);
				var api = that.data("url");
				var payload = {
					"source": $("#selectSource").val(),
					"occasion": $("#selectOccasion").val(),
					"moreServices": $("#moreServices").val(),
					"requirements": $("#comments").val()
				};

				/* add dynamic fields */
				var gc = $("#guestsCount");
				if (gc.length) {
					payload.guestsCount = gc.val();
				}

				window.showLoading();
				$.ajax({
					url: api,
					type: "POST",
					dataType: 'json',
					data: payload,
					success: function (data) {
						window.hideLoading();
						if (data.success === true) {
							var wrapper = $("#addFormCnt");
							wrapper.find(".edit").addClass("hide");
							wrapper.find(".done").removeClass("hide");
						} else {
							if (data.error) {
								var errorObj = $("#additionalDetailsForm")
									.find(".error-wrap")
									.removeClass("hide")
									.find(".error-message");
								errorObj.text(data.error);
							} else {
								window.showNotyError("Sorry, we could not save your request. Please try again later.");
							}
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						window.hideLoading();
						window.showNotyError("Sorry, we could not save your request. Please try again later.");
						window.notifyTeam({
							"url": api,
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			});
		});
	</script>
@endsection