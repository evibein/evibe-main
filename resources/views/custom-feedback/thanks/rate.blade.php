@extends('layout.base')

@section('page-title')
	<title>Thanks for your feedback and time | Evibe.in</title>
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/refer-earn.css') }}">
@show

@section('meta-description')
	<meta name="description" content="Thanks for sharing your feedback"/>
@endsection

@section('meta-keywords')
	<meta name="keywords" content="Evibe, Feedback"/>
@endsection

@section('og-title')
	<meta property="og:title" content="Thanks for your feedback and time | Evibe.in"/>
@endsection

@section('og-description')
	<meta property="og:description" content="Thanks for sharing your feedback"/>
@endsection

@section("content")
	<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
		<div class="custom-feedback-rate-div">
			<section class="custom-feedback-rate-sec">
				<div class="custom-feedback-rate-head">
					<i class="material-icons">check_circle</i>
					Feedback Received
				</div>
				<div class="custom-feedback-rate-body">
					<div class="custom-feedback-rate-msg text-left">
						<b>{{ $data["name"] }}</b>, thank you for your feedback. We are very happy to know that you had a great party :).
					</div>
					<div class="custom-feedback-rate-msg no-pad-b text-left no-pad-t">
						<b><i>We need your help...</i></b>
					</div>
					<div class="custom-feedback-rate-msg text-left no-pad-t">
						Please take few seconds to rate us on Google to help more customers like you.
					</div>
					<div class="custom-feedback-rate-link">
						<a href="https://goo.gl/6wzhUV">
							<div class="custom-feedback-rate-link-wrap">
								<div class="i-w">
									<img src="{{ config('evibe.gallery.host') }}/main/img/icons/g_rate.png" alt="google-icon">
								</div>
								<div class="t-w">
									<div class="text-1">Rate us on</div>
									<div class="text-2">Google</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</a>
						@if(isset($data["isRecommendEvibe"]) && $data["isRecommendEvibe"] == 1)
							<hr class="sep">
							<div class="custom-feedback-rate-msg no-pad-b no-pad-t">
								@include("pay.referral-post-purchase-widget", ["ticketId" => $data["ticketId"]])
							</div>
						@endif
					</div>
				</div>
			</section>
		</div>
	</div>
	<div class="clearfix"></div>
@endsection