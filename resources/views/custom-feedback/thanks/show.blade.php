@extends('layout.base')

@section('page-title')
	<title>Thanks for your feedback and time | Evibe.in</title>
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/refer-earn.css') }}">
@show

@section('meta-description')
	<meta name="description" content="Thanks for sharing your feedback"/>
@endsection

@section('meta-keywords')
	<meta name="keywords" content="Evibe, Feedback"/>
@endsection

@section('og-title')
	<meta property="og:title" content="Thanks for your feedback and time | Evibe.in"/>
@endsection

@section('og-description')
	<meta property="og:description" content="Thanks for sharing your feedback"/>
@endsection

@section("content")
	<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
		<section class="page-sec custom-feedback-sec thanks-feedback">
			<h3 class="text-center success">
				<i class="material-icons">check_circle</i>
				Feedback Received
			</h3>
			<h1 class="feedback-thanks-msg">
				<b>Thank you {{ $data["name"] }}</b>, we have received your feedback. Thanks again for taking time to help us improve our services.
				@if(isset($data["isRecommendEvibe"]) && $data["isRecommendEvibe"] == 1)
					<hr class="sep">
					@include("pay.referral-post-purchase-widget", ["ticketId" => $data["ticketId"]])
				@endif
			</h1>
		</section>
	</div>
	<div class="clearfix"></div>
@endsection