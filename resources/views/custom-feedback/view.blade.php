@extends('layout.base')

@section('page-title')
	<title>Share your feedback | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="Share booking feedback with Evibe">
@endsection

@section('meta-keywords')
	<meta name="keywords" content="Evibe, Feedback">
@endsection

@section('og-title')
	<meta property="og:title" content="Share your feedback | Evibe.in"/>
@endsection

@section('og-description')
	<meta property="og:description" content="Share booking feedback with Evibe"/>
@endsection

@section("header")
	@include('pay.header')
@endsection

@section("content")
	<article class="feedback-wrap">
		<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 no-pad__400 no-pad__400-600">
			<section class="page-sec custom-feedback-sec no-pad__400 no-pad__400-600">
				<div class="sec-head">
					<div class="sec-title-wrap">
						<h1 class="feedback-title-text text-danger">
							<i class="glyphicon glyphicon-info-sign"></i>
							Please provide your valuable feedback
						</h1>
						<div class="text-center text-lower text-bold">(<span class="text-danger">*</span> fields are mandatory)
						</div>
					</div>
				</div>
				<div class="sec-body">
					<?php $count = 1; ?>
					<div class="col-md-8 col-lg-8 col-sm-8 no-pad-l no-pad-r col-md-offset-2 col-lg-offset-2 col-sm-offset-2">
						<div class="alert alert-danger errors-cnt hide">
							<ul class="errors-list no-mar no-pad"></ul>
						</div>
						<input type="hidden" id="ticketId" name="ticketId" value="{{ $data['booking']['ticketId'] }}"/>
						<div class="rating-vendor-wrapper">
							@if(count($data['vendors']) > 0)
								@foreach ($data['vendors'] as $vendor)
									<div class="vendor-question-wrap" data-id="{{$vendor['id']}}" data-name="{{ $vendor['name'] }}" data-type="{{ $vendor['type'] }}" data-bookingid="{{ $vendor['bookingId'] }}" data-tickettypeid="{{ $vendor['ticketTypeId'] }}" data-providermaptypeId="{{ $vendor['providerMapTypeId'] }}">
										<label class="mar-t-10 text-center__400 text-center-400-600">
											{{ $count++ }}. How do you rate {{ $vendor['type'] }}: {{ $vendor['name'] }}
										</label>
										@if(count($vendor['reviewQuestions']) > 0)
											@foreach($vendor['reviewQuestions'] as $reviewQuestion)
												<div class="rating-q-wrapper" data-qid="{{ $reviewQuestion->id }}" data-maxrating="{{ $reviewQuestion->max_rating }}" data-name="{{ $reviewQuestion->question }}">
													<div class="col-lg-4 no-pad-l">
														{{ ucfirst($reviewQuestion->question) }}
														<span class="text-e">*</span>
													</div>
													<div class="col-lg-8">
														<input type="text" value="" class="form-control rating-field"/>
													</div>
													<div class="clearfix"></div>
												</div>
											@endforeach
										@endif
										<div class="rating-q-wrapper">
											<div class="col-lg-4 no-pad-l">Review</div>
											<div class="col-lg-8">
												<textarea name="" id="" rows="2" placeholder="Start typing here..." class="form-control rating-text-area vendor-rating-comment mar-t-5"></textarea>
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
								@endforeach
							@endif
						</div>
						<div class="clearfix"></div>
						<div class="evibe-rating-wrapper">
							<label class="mar-t-10 text-center__400 text-center-400-600">
								{{$count++}}. Feedback for Evibe.in
							</label>
							<div class="rating-q-wrapper">
								<div class="col-lg-4 no-pad-l">
									Customer care <span class="text-e">*</span>
								</div>
								<div class="col-lg-8">
									<input type="text" value="" id="customerCareRating" class="form-control rating-field"/>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="rating-q-wrapper">
								<div class="col-lg-4 no-pad-l">
									Ease of booking <span class="text-e">*</span>
								</div>
								<div class="col-lg-8">
									<input type="text" id="easeOfBooking" value="" class="form-control rating-field"/>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="form-group">
								<div>
									<label class="mar-t-10 ">Do you recommend Evibe.in to your friends & family?
										<span class="text-e">*</span>
									</label>
								</div>
								<div class="radio in-blk no-mar">
									<label><input type="radio" name="recommendRadio" value="1" checked>Yes</label>
								</div>
								<div class="radio in-blk no-mar mar-l-10">
									<label><input type="radio" name="recommendRadio" value="0">No</label>
								</div>
							</div>
							<div class="form-group">
								<label>How was your experience with Evibe.in?
									<span class="text-e">*</span>
								</label>
								<textarea name="evibeExperience" id="evibeExperience" class="form-control rating-text-area vertical-resize" rows="3">{{ old('evibeExperience') }}</textarea>
							</div>
							<div class="form-group">
								<label>What convinced you to book from us?
									<span class="text-e">*</span>
								</label>
								<textarea name="bookEvibeConvince" id="bookEvibeConvince" class="form-control rating-text-area vertical-resize" rows="3">{{ old('bookEvibeConvince') }}</textarea>
							</div>
							@if($data['trqs']->count())
								@foreach($data['trqs'] as $trq)
									@if($trq->type_question_id == config('evibe.type_question.text'))
										<div class="form-group evibe-extra-q-wrap" data-id="{{ $trq->id }}">
											<label>{{ $trq->question }} @if($trq->hint)({{ $trq->hint }})@endif
												<span class="text-e">*</span></label>
											<div style="color:#444; margin-top:5px;">
												<textarea name="text" class="form-control rating-text-area ques-text vertical-resize" rows="3"></textarea>
											</div>
										</div>
									@elseif($trq->type_question_id == config('evibe.type_question.rating'))
									@else
										@if($trq->options && $trq->options->count())
											@if($trq->type_question_id == config('evibe.type_question.checkbox'))
												<div class="form-group evibe-extra-q-wrap" data-id="{{ $trq->id }}">
													<label>{{ $trq->question }} @if($trq->hint)({{ $trq->hint }})@endif
														@if($trq->is_required == 1)<span class="text-e">*</span></label>@endif
													<div style="color:#444; margin-top:5px;">
														@foreach($trq->options as $option)
															<div>
																<label style="font-weight: 200; margin-right: 2px">
																	<input type="checkbox" name="option" class="ques-check" value="{{ $option->id }}"> {{ $option->text }}
																</label>
															</div>
														@endforeach
													</div>
												</div>
											@elseif($trq->type_question_id == config('evibe.type_question.radio'))
												<div class="form-group evibe-extra-q-wrap" data-id="{{ $trq->id }}">
													<label>{{ $trq->question }} @if($trq->hint)({{ $trq->hint }})@endif
														@if($trq->is_required == 1)<span class="text-e">*</span></label>@endif
													<div style="color:#444; margin-top:5px;">
														@foreach($trq->options as $key => $option)
															<div class="in-blk @if($key) mar-l-10 @endif">
																<label style="font-weight: 200; margin-right: 2px">
																	<input type="radio" name="optionQuestion{{ $trq->id }}" class="ques-check valign-top" value="{{ $option->id }}"> {{ $option->text }}
																</label>
															</div>
														@endforeach
													</div>
												</div>
											@elseif($trq->type_question_id == config('evibe.type_question.select'))
											@endif
										@endif
									@endif
								@endforeach
							@endif
						</div>
						<div class="form-group text-center">
							<input type="submit" value="SUBMIT FEEDBACK" class="btn btn-lg btn-success btn-feedback-submit" data-url="{{  route('feedback.save') }}">
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</section>
		</div>
		<div class="clearfix"></div>
	</article>
@endsection

@section("javascript")
	<script type="text/javascript">

		$(document).ready(function () {

			$('.rating-field').rating({
				showClear: 0,
				showCaption: 1,
				size: 'l',
				min: 0,
				max: 5,
				step: 0.5
			});

			$('.rating-container').click(function (event) {
				event.preventDefault();

				var questionsCount = 0;
				var questionsRating = 0;

				$('.vendor-question-wrap').each(function () {
					$(this).find('.rating-q-wrapper').each(function () {
						var individualRating = $(this).find('.rating-field').val();

						if (individualRating !== undefined) {
							questionsRating = parseFloat(questionsRating) + parseFloat(individualRating);
							questionsCount = questionsCount + 1;
						}
					});
				});

				questionsRating += parseFloat($('#customerCareRating').val());
				questionsRating += parseFloat($('#easeOfBooking').val());
				questionsRating += parseFloat($('#priceForQuality').val());

				questionsCount = questionsCount + 3;
			});

			$('.btn-feedback-submit').click(function (event) {

				event.preventDefault();

				var url = $(this).data('url');
				if (!url) {
					alert('Some error occurred');
					return false;
				}

				var vendorRating = [];
				var evibeExtraRating = [];
				/* Static values */
				var isRecommend = $('input[name="recommendRadio"]:checked').val();
				/* var billingAmount = $('#billingAmount').val(); */
				/*var moreFeedback = $('#moreFeedback').val();*/
				var evibeExperience = $('#evibeExperience').val();
				var bookEvibeConvince = $('#bookEvibeConvince').val();
				var ticketId = $('#ticketId').val();
				var customerService = $('#customerCareRating').val();
				var easeOfBooking = $('#easeOfBooking').val();
				var qualityForService = $('#priceForQuality').val();

				$('.vendor-question-wrap').each(function () {
					var questions = [];

					$(this).find('.rating-q-wrapper').each(function () {
						questions.push({
							id: $(this).data('qid'),
							name: $(this).data('name'),
							ratings: $(this).find('.rating-field').val(),
							maxRating: $(this).data('maxrating')
						});
					});

					if ($(this).find('.partner-uniform').length) {
						$(this).find('.vendor-ques-check').each(function () {
							if ($(this).prop('checked')) {
								uniform = ($(this).val());
							}
						});
					}

					vendorRating.push({
						id: $(this).data('id'),
						name: $(this).data('name'),
						type: $(this).data('type'),
						ticketTypeId: $(this).data('tickettypeid'),
						ticketBookingId: $(this).data('bookingid'),
						providerMapTypeId: $(this).data('providermaptypeid'),
						comment: $(this).find('.vendor-rating-comment').val(),
						questions: questions
					});
				});

				$('.evibe-extra-q-wrap').each(function () {
					var checkedOptionIds = [];
					var answerText;

					answerText = $(this).find('.ques-text').val();

					$(this).find('.ques-check').each(function () {
						if ($(this).prop('checked')) {
							checkedOptionIds.push($(this).val())
						}
					});

					evibeExtraRating.push({
						'questionId': $(this).data('id'),
						'answerIds': checkedOptionIds,
						'answerText': answerText
					});
				});
				console.log(evibeExtraRating);

				var data = {
					'ticketId': ticketId,
					'isRecommend': isRecommend,
					'customerService': customerService,
					'easeOfBooking': easeOfBooking,
					'qualityForService': qualityForService,
					/* 'billingAmount': billingAmount, */
					'evibeExperience': evibeExperience,
					'bookEvibeConvince': bookEvibeConvince,
					'vendorRatings': vendorRating,
					'ticketExtraRatings': evibeExtraRating
				};

				/* show submitting modal - initiated in app.js */
				window.showLoading();

				$.ajax({
					url: url,
					type: 'POST',
					dataType: 'json',
					data: data,
					success: function (responseData, textStatus, jqXHR) {
						if (responseData.success) {
							window.location = responseData.redirectUrl;
						} else {
							if (responseData.apiRequestError) {
								/* alert admin */
								$.ajax({
									url: '/give-feedback/fail',
									type: 'POST',
									dataType: 'json',
									data: data
								});

								window.showNotyError("Sorry, an error has occurred, please try again later.");
							}

							/* hide modal */
							window.hideLoading();

							$('.errors-list').empty();
							for (var key in responseData.errors) {
								$('.errors-list').append('<li class="list-unstyled">' + responseData.errors[key] + '</li>');
							}

							$('.errors-cnt').removeClass('hide');
							window.scrollTo(0, 0);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						window.hideLoading();

						/* alert admin */
						$.ajax({
							url: '/give-feedback/fail',
							type: 'POST',
							dataType: 'json',
							data: data
						});

						window.showNotyError("Sorry, an error has occurred, please try again later.");
					}
				});

			});

		});

	</script>
@endsection