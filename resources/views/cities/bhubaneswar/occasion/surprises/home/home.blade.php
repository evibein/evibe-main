<section class="home-section slides-wrap home-occasion ce-no-categories">
	<div class="intro-header">
		<div class="img-slides">
			<ul class="rslides">
				<li>
					<div class="top-slide" style="background-image:url({{$galleryUrl}}/main/img/bg-couple-exp.jpg)"></div>
				</li>
			</ul>
			<div class="bg-overlay"></div>
			<div class="intro-message">
				<h1 class="text-bg">{{ $data['seo']['pageHeader'] }}</h1>
			</div>
		</div>
	</div>
</section>

<section class="home-section padding-30 no-pad__400">
	<div class="container">
		@if(count($data['collections']) > 0)
			<div class="row trending-items pad-t-20 pad-b-20">
				<div class="head-title mar-b-40 pos-rel">
					<div class="col-md-8 col-sm-12 col-xs-12">
						<h5 class="section-title">Collections</h5>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 pull-right">
						<a href="{{ route('city.occasion.surprises.collection.list', $cityUrl) }}?ref=top-all" class="btn-see-all pull-right"> See all Collections</a>
					</div>
					<div class="clearfix"></div>
				</div>
				@foreach($data['collections'] as $collection)
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
						<div class="collection-card-list">
							<a href="{{ $data['collectionBaseUrl'].$collection['url'] }}?ref=home-collections">
								<img src="{{ $collection['coverImg'] }}" alt="{{ $collection['name'] }}">
								<div class="collection-card-list-title">
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 collection-card-list-wrap">
										<img src="{{ $collection['profileImg'] }}" alt="{{ $collection['name'] }}">
									</div>
									<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 collection-card-list-title-wrap">
										<h3 class="mdl-color-text--white pull-left">{{ $collection['name'] }}</h3>
									</div>
									<h6 class="text-center mdl-color-text--white">{{ $collection['description'] }}</h6>
								</div>
							</a>
						</div>
					</div>
				@endforeach
				<div class="clearfix"></div>
			</div>
		@endif
		@if(count($data['topPackages']) > 0)
			<div class="row trending-items pad-t-20 pad-b-20">
				@if(count($data['collections']) > 0)
					<div class="head-title mar-b-40 pos-rel">
						<div class="col-md-8 col-sm-12 col-xs-12">
							<h5 class="section-title">Popular Packages</h5>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 pull-right">
							<a href="{{ route('city.occasion.surprises.package.list', $cityUrl) }}?ref=top-all" class="btn-see-all pull-right"> See all Packages</a>
						</div>
						<div class="clearfix"></div>
					</div>
				@endif
				@foreach($data['topPackages'] as $package)
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 trending mar-b-30">
						<div class="img-container">
							<a href="{{ route('city.occasion.surprises.package.profile', [$cityUrl, $package->url]) }}?ref=top-image">
								<img src="{{ $package->getProfileImg('results') }}" title="romantic experience {{ $package->code }}">
							</a>
							@include('app.shortlist_results', [
																"mapId" => $package->id,
																"mapTypeId" => config('evibe.ticket.type.surprises'),
																"occasionId" => $data['occasionId'],
																"cityId" => $data['cityId']
															])
							<div class="price">
								@price($package->price)
								<span class="star">*</span>
							</div>
							@if(in_array($package->id, $data['autoBook']))
								<div class="booking">
									<a href="{{ route('city.occasion.surprises.package.profile', [$cityUrl, $package->url]) }}?ref=top-image#bookNowModal" class="btn btn-primary btn-view-more focused">Book Now</a>
								</div>
							@endif
						</div>
						<a href="{{ route('city.occasion.surprises.package.profile', [$cityUrl, $package->url]) }}?ref=top-image">
							<h6 class="item-name ov-text-no-wrap">@truncateName($package->name)</h6>
						</a>
					</div>
				@endforeach
				<div class="clearfix"></div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 text-center mar-t-10 mar-b-20">
				<a href="{{ route('city.occasion.surprises.package.list', $cityUrl) }}" class=" btn btn-default btn-explore btn-lg-see-all">See all Options</a>
			</div>
		@endif
		<div class="clearfix"></div>
	</div>
</section>