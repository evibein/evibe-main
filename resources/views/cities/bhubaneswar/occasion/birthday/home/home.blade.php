@php $prodCount=1 @endphp
<section class="home-section slides-wrap home-occasion">
	@include("occasion.birthday.home.occasion-carousel")
</section>

<section>
	<div class="full-width pad-t-30">
		@include("occasion.birthday.home.occasion-categories")
	</div>
</section>

@if(isset($data['isShowTopContainer']) && $data['isShowTopContainer'])
	<section class="home-section padding-30 no-pad__400">
		<!-- Birthday Decors -->
		@if(isset($data['topDecors']) && count($data['topDecors']) > 0)
			<div class=" pad-b-20 inner-top-option-div">
				<div class="head-title no-mar-b">
					@if($agent->isMobile())
						<h6 class="mar-t-20 mar-b-20 in-blk pad-l-15 fw-normal font-18 text-upr">Popular Decorations</h6>
					@else
						<h6 class="mar-t-20 mar-b-20 in-blk pad-l-15 fw-normal font-18 text-upr">Popular Birthday Party Decorations</h6>
					@endif
					<div class="mar-t-20 mar-b-20 ib-blk pull-right floating-evibe-btn-wrap   pad-r-15 ">
						<a href="{{ route('city.occasion.birthdays.decors.list', $cityUrl) }}" class="text-muted floating-evibe-btn pad-t-5 pad-b-5 pad-l-10 pad-r-10"> View All</a>
					</div>

					<div class="clearfix"></div>
				</div>
				<hr class="hr-line">

				<div class="top-sec-items">
					@foreach($data['topDecors'] as $decor)
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 mar-b-15 trending">
							<a href="{{ route('city.occasion.birthdays.decors.profile', [$cityUrl, $decor->url]) }}?ref=top-image">
								<div class="img-container" style="height: auto;width: 100%;overflow: hidden;">
									<img src="{{ $decor->getProfileImg() }}" title="Birthday party decor - {{ $decor->name }}">
								</div>
								<div class="text-center">
									<h6 class="no-mar-b font-15 no-mar-t prod-service-name">@truncateName($decor->name)</h6>
									<div class="text-grey">
										<span>@price($decor->min_price)</span>
										<span class="star">*</span>
									</div>
								</div>
							</a>
						</div>
						@if(($prodCount++ >= 4 && $agent->isMobile()) || $loop->last)
							@php $prodCount = 1; @endphp
							@break
						@endif
					@endforeach
					<div class="clearfix"></div>
				</div>
			</div>
		@endif

		@if(isset($data['topCakes']) && count($data['topCakes']) > 0)
			<div class="mar-t-20  pad-b-20 inner-top-option-div">

				<div class="head-title no-mar-b">
					<h6 class="mar-t-20 mar-b-20 in-blk pad-l-15 fw-normal font-18 text-upr">Popular Party Cakes</h6>

					<div class="mar-t-20 mar-b-20 ib-blk pull-right floating-evibe-btn-wrap   pad-r-15 ">
						<a href="{{ route('city.occasion.birthdays.cakes.list', $cityUrl) }}" class="text-muted floating-evibe-btn pad-t-5 pad-b-5 pad-l-10 pad-r-10"> View All</a>
					</div>
					<div class="clearfix"></div>
				</div>
				<hr class="hr-line">
				<div class="top-sec-items">
					@foreach($data['topCakes'] as $cake)
						<div class="col-lg-2 col-md-3 col-sm-6 col-xs-6 mar-b-15 trending">
							<a href="{{ route('city.occasion.birthdays.cakes.profile', [$cityUrl, $cake->url]) }}?ref=top-image">
								<div class="img-container" style="height: auto;width: 100%;overflow: hidden;">
									<img src="{{ $cake->getProfileImg() }}" title="Birthday party cake - {{ $cake->title }}">
								</div>
								<div class="text-center">
									<h6 class="no-mar-b font-15 no-mar-t prod-service-name">@truncateName($cake->title)</h6>
									<div class="text-grey">
										<span>@price($cake->price)</span>
										<span class="star">*</span>
									</div>
								</div>
							</a>
						</div>
						@if(($prodCount++ >= 4 && $agent->isMobile()) || $loop->last)
							@php $prodCount = 1; @endphp
							@break
						@endif
					@endforeach
					<div class="clearfix"></div>
				</div>
			</div>
		@endif
		@if(!($agent->isMobile()||$agent->isTablet()))
			<div class=" inner-top-option-div mar-t-20 " style="background-image: url('{{ $galleryUrl }}/main/pages/occasion-home/cta-m.png');background-size: cover;height: 200px">
				<div style="z-index: 10;text-align: center">
					<h4 class="pad-t-15">Let us help you.</h4>
					<p> Put an enquiry, Our expert assists you in picking the best.</p>
					<div class="mar-t-20 mar-b-20 ib-blk floating-evibe-btn-wrap">
						<a href="{{ route('city.occasion.birthdays.cakes.list', $cityUrl) }}" class="text-muted floating-evibe-btn pad-t-5 pad-b-5 pad-l-10 pad-r-10"> Enquire Now</a>
					</div>
				</div>
			</div>
		@else
			<div class=" inner-top-option-div mar-t-20 " style="background-image: url('{{ $galleryUrl }}/main/pages/occasion-home/cta.png');background-size: cover;height: 200px">
				<div style="z-index: 10;text-align: center">
					<h4 class="pad-t-15">Let us help you.</h4>
					<p> Put an enquiry, Our expert assists you in picking the best.</p>
					<div class="mar-t-20 mar-b-20 ib-blk floating-evibe-btn-wrap">
						<a href="{{ route('city.occasion.birthdays.cakes.list', $cityUrl) }}" class="text-muted floating-evibe-btn pad-t-5 pad-b-5 pad-l-10 pad-r-10"> Enquire Now</a>
					</div>
				</div>
			</div>

		@endif
		@if(isset($data['topPackages']) && count($data['topPackages']) > 0)
			<div class="mar-t-20  pad-b-20 inner-top-option-div">
				<div class="head-title no-mar-b">

					<h6 class="mar-t-20 mar-b-20 in-blk pad-l-15 fw-normal font-18 text-upr">Popular Party Packages</h6>

					<div class="mar-t-20 mar-b-20 ib-blk pull-right floating-evibe-btn-wrap   pad-r-15 ">
						<a href="{{ route('city.occasion.birthdays.packages.list', $cityUrl) }}?ref=top-all" class="text-muted floating-evibe-btn pad-t-5 pad-b-5 pad-l-10 pad-r-10"> View All</a>
					</div>
					<div class="clearfix"></div>
				</div>

				<hr class="hr-line">

				<div class="top-sec-items">
					@foreach($data['topPackages'] as $package)
						<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 mar-b-15 trending">
							<a href="{{ route('city.occasion.birthdays.packages.profile', [$cityUrl, $package->url]) }}?ref=top-view">
								<div class="img-container" style="height: auto;width: 100%;overflow: hidden;">
									<img src="{{ $package->getProfileImg() }}" title="{{ $package->name }}" alt="Birthday party package - {{ $package->name }} - {{ $cityName }}">
								</div>
								<div class="text-center">
									<h6 class="no-mar-b font-15 no-mar-t prod-service-name">@truncateName($package->name)</h6>
									<div class="text-grey">
										<span>@price($package->price)</span>
										<span class="star">*</span>
									</div>
								</div>
							</a>
						</div>
						@if(($prodCount++ >= 4 && $agent->isMobile()) || $loop->last)
							@php $prodCount = 1; @endphp
							@break
						@endif
					@endforeach
					<div class="clearfix"></div>
				</div>
			</div>
		@endif

		@if(isset($data['topEntServices']) && count($data['topEntServices']) > 0)
			<div class="mar-t-20  pad-b-20 inner-top-option-div">
				<div class="head-title no-mar-b">
					@if(!($agent->isMobile()||$agent->isTablet()))
						<h6 class="mar-t-20 mar-b-20 in-blk pad-l-15 fw-normal font-18 text-upr">Popular Entertainment Services</h6>
					@else
						<h6 class="mar-t-20 mar-b-20 in-blk pad-l-15 fw-normal font-18 text-upr">Entertainment Services</h6>
					@endif

					<div class="mar-t-20 mar-b-20 ib-blk pull-right floating-evibe-btn-wrap   pad-r-15 ">
						<a href="{{ route('city.occasion.birthdays.ent.list', $cityUrl) }}" class="text-muted floating-evibe-btn pad-t-5 pad-b-5 pad-l-10 pad-r-10"> View All</a>
					</div>
					<div class="clearfix"></div>
				</div>
				<hr class="hr-line">

				<div class="top-sec-items">
					@foreach($data['topEntServices'] as $service)
						<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 mar-b-15 trending">
							<a href="{{ route('city.occasion.birthdays.ent.profile', [$cityUrl, $service->url]) }}?ref=top-image">
								<div class="img-container" style="height: auto;width: 100%;overflow: hidden;">
									<img src="{{ $service->getProfilePic() }}" title="Birthday party service - {{ $service->name }}">
								</div>
								<div class="text-center">
									<h6 class="no-mar-b font-15 no-mar-t prod-service-name">@truncateName($service->name)</h6>
									<div class="text-grey">
										<span>@price($service->min_price)</span>
										<span class="star">*</span>
									</div>
								</div>

							</a>
						</div>
						@if(($prodCount++ >= 4 && $agent->isMobile()) || $loop->last)
							@php $prodCount = 1; @endphp
							@break
						@endif
					@endforeach
					<div class="clearfix"></div>
				</div>
			</div>
		@endif

		@if(isset($data['topVenueDeals']) && count($data['topVenueDeals']) > 0)
			<div class="mar-t-20 pad-b-20 inner-top-option-div">
				<div class="head-title no-mar-b">
					<h6 class="mar-t-20 mar-b-20 in-blk pad-l-15 fw-normal font-18 text-upr">Popular Venue Deals</h6>

					<div class="mar-t-20 mar-b-20 ib-blk pull-right floating-evibe-btn-wrap   pad-r-15 ">
						<a href="{{ route('city.occasion.birthdays.venue_deals.list', $cityUrl) }}" class="text-muted floating-evibe-btn pad-t-5 pad-b-5 pad-l-10 pad-r-10"> View All</a>
					</div>
					<div class="clearfix"></div>
				</div>
				<hr class="hr-line">

				<div class="top-sec-items">
					@foreach($data['topVenueDeals'] as $venueDeal)
						<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 mar-b-15 trending">
							<a href="{{ route('city.occasion.birthdays.venue_deals.profile', [$cityUrl, $venueDeal->url]) }}?ref=top-image">
								<div class="img-container" style="height: auto;width: 100%;overflow: hidden;">
									<img src="{{ $venueDeal->getProfileImg() }}" title="Birthday party venue -  {{ $venueDeal->name }}">
								</div>
								<div class="text-center">
									<h6 class="no-mar-b font-15 no-mar-t prod-service-name"> @truncateName($venueDeal->name)</h6>
									<div class="text-grey">
										<span>@price($venueDeal->price)</span>
										<span class="star">*</span>
										@if ($venueDeal->price && $venueDeal->price_max > $venueDeal->price)
											- &nbsp; @price($venueDeal->price_max) <span class="star">*</span>
										@endif

									</div>
								</div>

							</a>
						</div>
						@if(($prodCount++ >= 4 && $agent->isMobile()) || $loop->last)
							@php $prodCount = 1; @endphp
							@break
						@endif
					@endforeach
					<div class="clearfix"></div>
				</div>
			</div>
		@endif

		@if(isset($data['trendingItems']) && count($data['trendingItems']) > 0)
			<div class="mar-t-20 pad-b-20 inner-top-option-div">
				<div class="head-title no-mar-b">

					<h6 class="mar-t-20 mar-b-20 in-blk pad-l-15 fw-normal font-18 text-upr">Latest Party Trends</h6>

					<div class="mar-t-20 mar-b-20 ib-blk pull-right floating-evibe-btn-wrap   pad-r-15 ">
						<a href="{{ route('city.occasion.birthdays.trends.list', $cityUrl) }}" class="text-muted floating-evibe-btn pad-t-5 pad-b-5 pad-l-10 pad-r-10"> View All</a>
					</div>
					<div class="clearfix"></div>
				</div>
				<hr class="hr-line">

				<div class="top-sec-items">
					@foreach($data['trendingItems'] as $trend)
						<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 mar-b-15 trending">
							<a href="{{ route('city.occasion.birthdays.trends.profile', [$cityUrl, $trend->url]) }}?ref=top-view">
								<div class="img-container" style="height: auto;width: 100%;overflow: hidden;">
									<img src="{{ $trend->getProfileImg() }}" title="{{ $trend->name }}" alt="Birthday party trend - {{ $trend->name }} in {{ $cityName }}">

								</div>
								<div class="text-center">
									<h6 class="no-mar-b font-15 no-mar-t prod-service-name">@truncateName($trend->name)</h6>
									<div class="text-grey">
										<span>@price($trend->price)</span>
										<span class="star">*</span>
									</div>
								</div>
							</a>
						</div>
						@if(($prodCount++ >= 4 && $agent->isMobile()) || $loop->last)
							@php $prodCount = 1; @endphp
							@break
						@endif
					@endforeach
					<div class="clearfix"></div>
				</div>
			</div>
		@endif

		<div class="clearfix"></div>
	</section>
@endif