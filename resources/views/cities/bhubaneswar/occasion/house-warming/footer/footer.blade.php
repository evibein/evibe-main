<?php
$ref = "house-warming";
$oFooterRef = "footer-master";
if (isset($refKeys) && is_array($refKeys) && array_key_exists('occasionImageRefKey', $refKeys))
{
	$ref = $refKeys['occasionImageRefKey'];
}
if (isset($refKeys) && is_array($refKeys) && array_key_exists('occasionFooterRefKey', $refKeys))
{
	$oFooterRef = $refKeys['occasionFooterRefKey'];
}
?>
<div class="footer-wrap">
	<div class="footer-top pad-t-20 pad-b-20 bg-white occasion-selection-footer">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 footer-links-list text-center">
			<h5 class="text-center text-col-gr">Not Looking For House Warming ?</h5>
			<div class="col-md-3 col-md-offset-3">
				@include('app.footer-occasions.birthday')
			</div>
			<div class="col-md-3 hide">
				@include('app.footer-occasions.bachelor')
			</div>
			<div class="col-md-3 hide">
				@include('app.footer-occasions.pre-post')
			</div>
			<div class="col-md-3">
				@include('app.footer-occasions.surprises')
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>

	@include('base.home.footer.footer-common')
</div>