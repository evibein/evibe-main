<div class="hide__400 hide-400-600">
	<nav class="occasion-top-navbar no-pad-l">
		<div class="header-hr-line"></div>
		<ul class="no-mar-b">
			<li>
				<a href="{{ route('city.occasion.house-warming.decors.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*'.config('evibe.occasion.house-warming.decors.results_url').'*')){{ "active" }}@endif">Decor Styles</a>
			</li>
			<li>
				<a href="{{ route('city.occasion.house-warming.food.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*'.config('evibe.occasion.house-warming.food.profile_url').'*')){{ "active" }}@endif">Food</a>
			</li>
			<li>
				<a href="{{ route('city.occasion.house-warming.priest.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*'.config('evibe.occasion.house-warming.priest.profile_url').'*')){{ "active" }}@endif">Priest</a>
			</li>
			<li>
				<a href="{{ route('city.occasion.house-warming.tent.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*'.config('evibe.occasion.house-warming.tents.profile_url').'*')){{ "active" }}@endif">Tent</a>
			</li>
			<li>
				<a href="{{ route('city.occasion.house-warming.ent.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*'.config('evibe.occasion.house-warming.ent.profile_url').'*')){{ "active" }}@endif">Add-ons</a>
			</li>
			<li>
				<a href="{{ route('city.occasion.house-warming.collection.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*collections*')){{ "active" }}@endif">Collections</a>
			</li>
		</ul>
		<div class="header-hr-line"></div>
	</nav>
</div>

<div class="hide show-400-600 unhide__400">
	<div class="header-bottom">
		<div class="container">
			<div id='cssmenu' class="menu-links">
				<ul id="header-ocs-lists">
					<li>
						<a href="{{ route('city.occasion.house-warming.decors.list', getCityUrl()) }}?ref=@if(isset($refKeys) && is_array($refKeys) && array_key_exists('occasionHeaderRefKey',$refKeys)){{ $refKeys['occasionHeaderRefKey'] }}@else{{ 'header' }} @endif">
							<img src="{{$galleryUrl}}/main/img/icons/decoration4_w.png" alt="House Warming party decorations">Decor Styles
						</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.house-warming.food.list', getCityUrl()) }}?ref=@if(isset($refKeys) && is_array($refKeys) && array_key_exists('occasionHeaderRefKey',$refKeys)){{ $refKeys['occasionHeaderRefKey'] }}@else{{ 'header' }} @endif">
							<img src="{{$galleryUrl}}/main/img/icons/food2_w.png" alt="House Warming food">Food
						</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.house-warming.priest.list', getCityUrl()) }}?ref=@if(isset($refKeys) && is_array($refKeys) && array_key_exists('occasionHeaderRefKey',$refKeys)){{ $refKeys['occasionHeaderRefKey'] }}@else{{ 'header' }} @endif">
							<img src="{{$galleryUrl}}/main/img/icons/priest_w.png" alt="House Warming party priest services">Priest
						</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.house-warming.tent.list', getCityUrl()) }}?ref=@if(isset($refKeys) && is_array($refKeys) && array_key_exists('occasionHeaderRefKey',$refKeys)){{ $refKeys['occasionHeaderRefKey'] }}@else{{ 'header' }} @endif">
							<img src="{{$galleryUrl}}/main/img/icons/tent2_w.png" alt="House Warming party tents">Tent
						</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.house-warming.ent.list', getCityUrl()) }}?ref=@if(isset($refKeys) && is_array($refKeys) && array_key_exists('occasionHeaderRefKey',$refKeys)){{ $refKeys['occasionHeaderRefKey'] }}@else{{ 'header' }} @endif">
							<img src="{{$galleryUrl}}/main/img/icons/entertainment2_w.png" alt="House Warming add-on options">Add-ons
						</a>
					</li>
					<li class="hide">
						<a href="{{ route('city.occasion.house-warming.cakes.list', getCityUrl()) }}?ref=@if(isset($refKeys) && is_array($refKeys) && array_key_exists('occasionHeaderRefKey',$refKeys)){{ $refKeys['occasionHeaderRefKey'] }}@else{{ 'header' }} @endif">
							<img src="{{$galleryUrl}}/main/img/icons/cake2_w.png" alt="House Warming party cakes">Cakes
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>