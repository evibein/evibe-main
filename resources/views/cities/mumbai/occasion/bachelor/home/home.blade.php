<!-- Home page image -->
<section class="home-section slides-wrap home-occasion">
	<div class="intro-header">
		<div class="img-slides">
			<ul class="rslides">
				<li>
					<div class="top-slide" style="background-image:url({{$galleryUrl}}/main/img/bg-bachelors.jpg)"></div>
				</li>
			</ul>
			<div class="bg-overlay"></div>
			<div class="intro-message bp-intro-message">
				<h1 class="text-bg">{{ $data['seo']['pageHeader'] }}</h1>
				<div class="mar-t-10">
					<h4>
						<img src="{{ $galleryUrl }}/main/img/icons/trust_w.png" height="45px" width="45px">
						<span class="pad-l-5">TRUSTED BY {{ config("evibe.stats.parties_done") }} CUSTOMERS</span>
					</h4>
				</div>
				<div class="mar-t-20">
					<a class="btn btn-evibe btn-lg btn-post">
						Enquire Now
					</a>
					<div class="pad-t-5 hide">
						<img src="{{ $galleryUrl }}/main/img/icons/whatsapp.png" height="18px" width="18px">
						@if ($agent->isMobile() && !($agent->isTablet()))
							<a href="whatsapp://send?phone=91{{ config("evibe.contact.customer.whatsapp.phone") }}&text=Hello Evibe.in, I am interested in your services for my party." class="whatsapp-wrap mar-t-8 pad-l-5 valign-mid mdl-color-text--white font-15">
								{{ config("evibe.contact.customer.whatsapp.phone") }}
							</a>
						@else
							<a href="https://web.whatsapp.com/send?phone=91{{ config("evibe.contact.customer.whatsapp.phone") }}&text=Hello Evibe.in, I am interested in your services for my party." target="_blank" class="whatsapp-wrap mar-t-8 pad-l-5 valign-mid mdl-color-text--white font-15">
								{{ config("evibe.contact.customer.whatsapp.phone") }}
							</a>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Categories section -->
	<div class="hide show-400-600 unhide__400">
		<div class="categories-wrap pos-abs">
			<ul class="categories-list no-mar ls-none no-pad">
				<!-- @see: do not close the li tag in same line to avoid mystery space -->
				<li>
					<div class="arrow-bottom"></div>
					<a href="{{ route('city.occasion.bachelor.villa.list', $cityUrl) }}?ref=header">
						Villas + Farms
					</a>
				</li>
				<div class="hide">
					<li>
						<div class="arrow-bottom"></div>
						<a href="{{ route('city.occasion.bachelor.resort.list', $cityUrl) }}?ref=header">
							Resorts
						</a>
					</li>
					<li>
						<div class="arrow-bottom"></div>
						<a href="{{ route('city.occasion.bachelor.lounge.list', $cityUrl) }}?ref=header">
							Lounges
						</a>
					</li>
					<li>
						<div class="arrow-bottom"></div>
						<a href="{{ route('city.occasion.bachelor.ent.list', $cityUrl) }}?ref=header">
							Entertainment
						</a>
					</li>
					<li>
						<div class="arrow-bottom"></div>
						<a href="{{ route('city.occasion.bachelor.food.list', $cityUrl) }}?ref=header">
							Food
						</a>
					</li>
					<li>
						<div class="arrow-bottom"></div>
						<a href="{{ route('city.occasion.bachelor.cakes.list', $cityUrl) }}?ref=header">
							Cakes
						</a>
					</li>
					<li class="hide">
						<div class="arrow-bottom"></div>
						<a href="{{ route('city.occasion.bachelor.decors.list', $cityUrl) }}?ref=header">
							Decor Styles
						</a>
					</li>
				</div>
			</ul>
		</div>
	</div>
</section>

<!-- Showcase popular listings -->
<section class="home-section padding-30 no-pad__400">
	<div class="container">
		@if(count($data['collections']) > 0)
			<div class="row trending-items pad-t-20 pad-b-20">
				<div class="head-title mar-b-40 pos-rel">
					<div class="col-md-8 col-sm-12 col-xs-12">
						<h5 class="section-title">Youth Party Collections</h5>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 pull-right">
						<a href="{{ route('city.occasion.bachelor.collection.list', $cityUrl) }}?ref=top-all" class="btn-see-all pull-right"> See all Collections</a>
					</div>
					<div class="clearfix"></div>
				</div>
				@foreach($data['collections'] as $collection)
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
						<div class="collection-card-list">
							<a href="{{ $data['collectionBaseUrl'].$collection['url'] }}?ref=home-collections">
								<img src="{{ $collection['coverImg'] }}" alt="{{ $collection['name'] }}">
								<div class="collection-card-list-title">
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 collection-card-list-wrap">
										<img src="{{ $collection['profileImg'] }}" alt="{{ $collection['name'] }}">
									</div>
									<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 collection-card-list-title-wrap">
										<h3 class="mdl-color-text--white pull-left">{{ $collection['name'] }}</h3>
									</div>
									<h6 class="text-center mdl-color-text--white">{{ $collection['description'] }}</h6>
								</div>
							</a>
						</div>
					</div>
				@endforeach
				<div class="clearfix"></div>
			</div>
		@endif

		@if(count($data['topVillas']) > 0)
			<div class="row trending-items pad-t-20 pad-b-20">
				<div class="head-title mar-b-40 pos-rel">
					<div class="col-md-8 col-sm-12 col-xs-12">
						<h5 class="section-title">Popular Youth Party Villas + Farms</h5>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 pull-right">
						<a href="{{ route('city.occasion.bachelor.villa.list', $cityUrl) }}?ref=top-all" class="btn-see-all pull-right"> See all Villas + Farms</a>
					</div>
					<div class="clearfix"></div>
				</div>
				@foreach($data['topVillas'] as $villa)
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 trending">
						<div class="img-container">
							<a href="{{ route('city.occasion.bachelor.villa.profile', [$cityUrl, $villa->url]) }}?ref=top-image">
								<img src="{{ $villa->getProfileImg('results') }}" title="Youth party villa {{ $villa->code }} in {{ $cityName }}">
							</a>
							<div class="hide">
								@include('app.shortlist_results', [
															"mapId" => $villa->id,
															"mapTypeId" => config('evibe.ticket.type.villas'),
															"occasionId" => $data['occasionId'],
															"cityId" => $data['cityId']
														])
							</div>
							<div class="price">
								@price($villa->price)
								<span class="star">*</span>
							</div>
							@if(in_array($villa->id, $data['autoBook']))
								<div class="booking">
									<a href="{{ route('city.occasion.bachelor.villa.profile', [$cityUrl, $villa->url]) }}?ref=top-image#bookNowModal" class="btn btn-primary btn-view-more focused" target="_blank" rel="noopener">Book Now</a>
								</div>
							@endif
						</div>
						<a href="{{ route('city.occasion.bachelor.villa.profile', [$cityUrl, $villa->url]) }}?ref=top-image">
							<h6 class="item-name ov-text-no-wrap">@truncateName($villa->name)</h6>
						</a>
					</div>
				@endforeach
				<div class="clearfix"></div>
			</div>
		@endif

		@if(count($data['topResorts']) > 0)
			<div class="row trending-items pad-t-20 pad-b-20 hide">
				<div class="head-title mar-b-40 pos-rel">
					<div class="col-md-8 col-sm-12 col-xs-12">
						<h5 class="section-title">Popular Youth Party Resorts</h5>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 pull-right">
						<a href="{{ route('city.occasion.bachelor.resort.list', $cityUrl) }}?ref=top-all" class="btn-see-all pull-right"> See all Resorts</a>
					</div>
					<div class="clearfix"></div>
				</div>
				@foreach($data['topResorts'] as $resort)
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 trending">
						<div class="img-container">
							<a href="{{ route('city.occasion.bachelor.resort.profile', [$cityUrl, $resort->url]) }}?ref=top-image">
								<img src="{{ $resort->getProfileImg('results') }}" title="Youth party resort {{ $resort->code }} in {{ $cityName }}">
							</a>
							@include('app.shortlist_results', [
															"mapId" => $resort->id,
															"mapTypeId" => config('evibe.ticket.type.resorts'),
															"occasionId" => $data['occasionId'],
															"cityId" => $data['cityId']
														])
							<div class="price">
								@price($resort->price)
								<span class="star">*</span>
							</div>
							@if(in_array($resort->id, $data['autoBook']))
								<div class="booking">
									<a href="{{ route('city.occasion.bachelor.resort.profile', [$cityUrl, $resort->url]) }}?ref=top-image#bookNowModal" class="btn btn-primary btn-view-more focused" target="_blank" rel="noopener">Book Now</a>
								</div>
							@endif
						</div>
						<a href="{{ route('city.occasion.bachelor.resort.profile', [$cityUrl, $resort->url]) }}?ref=top-image">
							<h6 class="item-name ov-text-no-wrap">@truncateName($resort->name)</h6>
						</a>
					</div>
				@endforeach
				<div class="clearfix"></div>
			</div>
		@endif

		@if(count($data['topEntOptions']) > 0)
			<div class="row trending-items pad-t-20 pad-b-20 hide">
				<div class="head-title mar-b-40 pos-rel">
					<div class="col-md-8 col-sm-12 col-xs-12">
						<h5 class="section-title">Popular Youth Party Entertainment Options</h5>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 pull-right">
						<a href="{{ route('city.occasion.bachelor.ent.list', $cityUrl) }}" class="btn-see-all pull-right"> See all Options</a>
					</div>
					<div class="clearfix"></div>
				</div>
				@foreach($data['topEntOptions'] as $ent)
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 trending">
						<a href="{{ route('city.occasion.bachelor.ent.profile', [$cityUrl, $ent->url]) }}?ref=top-image">
							<div class="img-container">
								<img src="{{ $ent->getProfilePic('results') }}" title="Youth party entertainment option {{ $ent->name }} in {{ $cityName }}">
								@include('app.shortlist_results', [
															"mapId" => $ent->id,
															"mapTypeId" => config('evibe.ticket.type.entertainment'),
															"occasionId" => $data['occasionId'],
															"cityId" => $data['cityId']
														])
								<div class="price">
									@price($ent->min_price)
									<span class="star">*</span>
								</div>
							</div>
							<h6 class="item-name">@truncateName($ent->name)</h6>
						</a>
					</div>
				@endforeach
				<div class="clearfix"></div>
			</div>
		@endif

		@if(count($data['topFoodOptions']) > 0)
			<div class="row trending-items pad-t-20 pad-b-20 hide">
				<div class="head-title mar-b-40 pos-rel">
					<div class="col-md-8 col-sm-12 col-xs-12">
						<h5 class="section-title">Popular Youth Party Food Packages</h5>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 pull-right">
						<a href="{{ route('city.occasion.bachelor.food.list', $cityUrl) }}" class="btn-see-all pull-right"> See All Food Packages</a>
					</div>
					<div class="clearfix"></div>
				</div>
				@foreach($data['topFoodOptions'] as $option)
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 trending">
						<a href="{{ route('city.occasion.bachelor.food.profile', [$cityUrl, $option->url]) }}?ref=top-image">
							<div class="img-container">
								<img src="{{ $option->getProfileImg('results') }}" title="Youth Party food packages">
								@include('app.shortlist_results', [
															"mapId" => $option->id,
															"mapTypeId" => config('evibe.ticket.type.food'),
															"occasionId" => $data['occasionId'],
															"cityId" => $data['cityId']
														])
								<div class="price">
									@price($option->price)
									<span class="star">*</span>
									<span class="onwards">Onwards</span>
								</div>
							</div>
							<h6 class="item-name">@truncateName($option->name)</h6>
						</a>
					</div>
				@endforeach
				<div class="clearfix"></div>
			</div>
		@endif

		<div class="clearfix"></div>
	</div>
</section>