<div class="hide__400 hide-400-600">
	<nav class="occasion-top-navbar no-pad-l">
		<div class="header-hr-line"></div>
		<ul class="no-mar-b">
			<li class="hide">
				<a href="{{ route('city.occasion.birthdays.venue_deals.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*'.config('evibe.results_url.venue_deals').'*')){{ "active" }}@endif">Venue Deals</a>
			</li>
			<li>
				<a href="{{ route('city.occasion.birthdays.decors.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*'.config('evibe.occasion.kids_birthdays.decors.results').'*')){{ "active" }}@endif">Decor Styles</a>
			</li>
			<li>
				<a href="{{ route('city.occasion.birthdays.ent.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*'.config('evibe.occasion.kids_birthdays.ent.profile_url').'*')){{ "active" }}@endif">Entertainment</a>
			</li>
			<li>
				<a href="{{ route('city.occasion.birthdays.packages.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*'.config('evibe.occasion.kids_birthdays.packages.profile_url').'*')){{ "active" }}@endif">Packages</a>
			</li>
			<li>
				<a href="{{ route('city.occasion.birthdays.cakes.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*'.config('evibe.occasion.kids_birthdays.cakes.profile_url').'*')){{ "active" }}@endif">Cakes</a>
			</li>
			<li>
				<a href="{{ route('city.occasion.birthdays.food.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*'.config('evibe.occasion.kids_birthdays.food.profile_url').'*')){{ "active" }}@endif">Food</a>
			</li>
			<li>
				<a href="{{ route('city.occasion.birthdays.tent.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*'.config('evibe.occasion.kids_birthdays.tents.profile_url').'*')){{ "active" }}@endif">Tents</a>
			</li>
			<li>
				<a href="{{ route('city.occasion.birthdays.trends.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*'.config('evibe.results_url.trends').'*')){{ "active" }}@endif">Trends</a>
			</li>
			<li>
				<a href="{{ route('city.occasion.birthdays.collection.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*collections*')){{ "active" }}@endif">Collections</a>
			</li>
		</ul>
		<div class="header-hr-line"></div>
	</nav>
	@include("occasion.util.list.d_components.workflow-promotion")
</div>

<div class="hide show-400-600 unhide__400">
	<div class="header-bottom">
		<div class="pad-l-10 pad-r-10 header-ocs-links-wrap">
			<div id='cssmenu' class="menu-links">
				<ul id="header-ocs-lists">
					<li>
						<a href="{{ route('city.occasion.birthdays.venue_deals.list', getCityUrl()) }}?ref={{ $linkRef }}">
							<img src="{{$galleryUrl}}/main/img/icons/deals.png" class="header-ocs-icon-extra" alt="Venue deals options">Venue Deals
						</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.birthdays.decors.list', getCityUrl()) }}?ref={{ $linkRef }}">
							<img src="{{$galleryUrl}}/main/img/icons/decoration2_w.png" class="header-ocs-icon-extra" alt="Birthday party decorations">Decor Styles
						</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.birthdays.ent.list', getCityUrl()) }}?ref={{ $linkRef }}">
							<img src="{{$galleryUrl}}/main/img/icons/entertainment2_w.png" class="header-ocs-icon-extra" alt="Birthday party entertainment options">Entertainment
						</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.birthdays.packages.list', getCityUrl()) }}?ref={{ $linkRef }}">
							<img src="{{$galleryUrl}}/main/img/icons/packages_w.png" class="header-ocs-icon-extra" alt="Birthday party packages">Packages
						</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.birthdays.cakes.list', getCityUrl()) }}?ref={{ $linkRef }}">
							<img src="{{$galleryUrl}}/main/img/icons/cake2_w.png" class="header-ocs-icon-extra" alt="Birthday party cakes">Cakes
						</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.birthdays.food.list', getCityUrl()) }}?ref={{ $linkRef }}">
							<img src="{{$galleryUrl}}/main/img/icons/food2_w.png" class="header-ocs-icon-extra" alt="Birthday Food">Food
						</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.birthdays.tent.list', getCityUrl()) }}?ref={{ $linkRef }}">
							<img src="{{$galleryUrl}}/main/img/icons/tent2_w.png" class="header-ocs-icon-extra" alt="Birthday party tent services">Tents
						</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.birthdays.trends.list', getCityUrl()) }}?ref={{ $linkRef }}">
							<img src="{{$galleryUrl}}/main/img/icons/trends_w.png" class="header-ocs-icon-extra" alt="Birthday party latest trends">Trends
						</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.birthdays.collection.list', getCityUrl()) }}?ref={{ $linkRef }}">
							<img src="{{$galleryUrl}}/main/img/icons/collection_w.png" class="header-ocs-icon-extra" alt="Birthday party collections">Collections
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>