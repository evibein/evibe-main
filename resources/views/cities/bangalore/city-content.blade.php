<div class="container">
	<div class="section-ocs @if(isset($data['collections']) && count($data['collections']) > 0) padding-t-75 @else margin-75 @endif ">
		<h4 class="sec-title text-center font-weight-500 text-pr-col mar-b-40">Birthday Party Essentials</h4>
		<h6 class="text-center top-10"></h6>
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 cat-card">
				<a href="{{ route('city.occasion.birthdays.venue_deals.list', getCityUrl()) }}">
					<img src="{{ $galleryUrl }}/main/img/home/birthday-venue.jpg" alt="Birthday Venue">
					<div class="cat-card-title">
						<h3 class="text-center mdl-color-text--white">Venue Deals</h3>
					</div>
				</a>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 cat-card">
				<a href="{{ route('city.occasion.birthdays.trends.list', getCityUrl()) }}">
					<img src="{{ $galleryUrl }}/main/img/home/birthday-trends.jpg" alt="Birthday Trend">
					<div class="cat-card-title">
						<h3 class="text-center mdl-color-text--white">Trends</h3>
					</div>
				</a>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 cat-card">
				<a href="{{ route('city.occasion.birthdays.decors.list', getCityUrl()) }}">
					<img src="{{ $galleryUrl }}/main/img/home/birthday-decor2.jpg" alt="Birthday Decorations">
					<div class="cat-card-title">
						<h3 class="text-center mdl-color-text--white">Decor Styles</h3>
					</div>
				</a>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 cat-card">
				<a href="{{ route('city.occasion.birthdays.packages.list', getCityUrl()) }}">
					<img src="{{ $galleryUrl }}/main/img/home/birthday-packages.jpg" alt="Birthday Package">
					<div class="cat-card-title">
						<h3 class="text-center mdl-color-text--white">Packages</h3>
					</div>
				</a>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 cat-card">
				<a href="{{ route('city.occasion.birthdays.ent.list', getCityUrl()) }}">
					<img src="{{ $galleryUrl }}/main/img/home/birthday-entertainment.jpg" alt="Birthday entertainment options">
					<div class="cat-card-title">
						<h3 class="text-center mdl-color-text--white">Entertainment</h3>
					</div>
				</a>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 cat-card">
				<a href="{{ route('city.occasion.birthdays.cakes.list', getCityUrl()) }}">
					<img src="{{ $galleryUrl }}/main/img/home/birthday-cakes.jpg" alt="Birthday cakes">
					<div class="cat-card-title">
						<h3 class="text-center mdl-color-text--white">Cakes</h3>

					</div>
				</a>
			</div>
		</div>
	</div>

	<div class="section-ocs padding-t-75">
		<h4 class="sec-title text-center font-500 text-pr-col top-10">Youth Party Essentials</h4>
		<div class="row top-30">
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 cat-card">
				<a href="{{ route('city.occasion.bachelor.resort.list', getCityUrl()) }}">
					<img src="{{ $galleryUrl }}/main/img/home/bachelors-resort.jpg" alt="Bachelors party resorts">
					<div class="cat-card-title">
						<h3 class="text-center mdl-color-text--white"> Resorts</h3>
					</div>
				</a>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 cat-card">
				<a href="{{ route('city.occasion.bachelor.villa.list', getCityUrl()) }}">
					<img src="{{ $galleryUrl }}/main/img/home/bachelors-villa.jpg" alt="Bachelors party villas, farm houses">
					<div class="cat-card-title">
						<h3 class="text-center mdl-color-text--white">Villas + Farms</h3>
					</div>
				</a>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 cat-card">
				<a href="{{ route('city.occasion.bachelor.lounge.list', getCityUrl()) }}">
					<img src="{{ $galleryUrl }}/main/img/home/bachelors-lounge.jpg" alt="Bachelors party lounges">
					<div class="cat-card-title">
						<h3 class="text-center mdl-color-text--white">Lounges</h3>
					</div>
				</a>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 cat-card">
				<a href="{{ route('city.occasion.bachelor.ent.list', getCityUrl()) }}">
					<img src="{{ $galleryUrl }}/main/img/home/bachelors-entertainment.jpg" alt="Bachelors party entertainment options">
					<div class="cat-card-title">
						<h3 class="text-center mdl-color-text--white"> Entertainment</h3>
					</div>
				</a>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 cat-card">
				<a href="{{ route('city.occasion.bachelor.food.list', getCityUrl()) }}">
					<img src="{{ $galleryUrl }}/main/img/home/bachelors-food.jpg" alt="Bachelors party food packages">
					<div class="cat-card-title">
						<h3 class="text-center mdl-color-text--white"> Food</h3>
					</div>
				</a>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 cat-card">
				<a href="{{ route('city.occasion.bachelor.cakes.list', getCityUrl()) }}">
					<img src="{{ $galleryUrl }}/main/img/home/bachelor-cake2.jpg" alt="Bachelors party cakes">
					<div class="cat-card-title">
						<h3 class="text-center mdl-color-text--white"> Cakes</h3>
					</div>
				</a>
			</div>
		</div>
	</div>

	<div class="section-ocs padding-t-75">
		<h4 class="sec-title text-center font-500 text-pr-col top-10">Engagement | Wedding Reception Essentials</h4>
		<div class="row top-30">
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 cat-card">
				<a href="{{ route('city.occasion.pre-post.venues.list', getCityUrl()) }}">
					<img src="{{ $galleryUrl }}/main/img/home/reception-venue2.jpg" alt="Receptions, engagement party venues">
					<div class="cat-card-title">
						<h3 class="text-center mdl-color-text--white"> Venues</h3>
					</div>
				</a>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 cat-card">
				<a href="{{ route('city.occasion.pre-post.decors.list', getCityUrl()) }}">
					<img src="{{ $galleryUrl }}/main/img/home/reception-decors2.jpg" alt="Receptions, engagement decorations">
					<div class="cat-card-title">
						<h3 class="text-center mdl-color-text--white"> Decors</h3>
					</div>
				</a>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 cat-card">
				<a href="{{ route('city.occasion.pre-post.ent.list', getCityUrl()) }}">
					<img src="{{ $galleryUrl }}/main/img/home/reception-ent2.jpg" alt="Receptions, engagement entertainment">
					<div class="cat-card-title">
						<h3 class="text-center mdl-color-text--white"> Entertainment</h3>
					</div>
				</a>
			</div>
		</div>
	</div>

	<div class="section-ocs padding-75">
		<h4 class="sec-title text-center font-500 text-pr-col top-10">House Warming Essentials</h4>
		<div class="row top-30">
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 cat-card">
				<a href="{{ route('city.occasion.house-warming.decors.list', getCityUrl()) }}">
					<img src="{{ $galleryUrl }}/main/img/home/housewarming-decor4.jpg" alt="House warming decorations">
					<div class="cat-card-title">
						<h3 class="text-center mdl-color-text--white">Decors</h3>
					</div>
				</a>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 cat-card">
				<a href="{{ route('city.occasion.house-warming.food.list', getCityUrl()) }}">
					<img src="{{ $galleryUrl }}/main/img/home/housewarming-food1.jpg" alt="House warming food packages">
					<div class="cat-card-title">
						<h3 class="text-center mdl-color-text--white">Food</h3>
					</div>
				</a>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 cat-card">
				<a href="{{ route('city.occasion.house-warming.priest.list', getCityUrl()) }}">
					<img src="{{ $galleryUrl }}/main/img/home/housewarming-priest1.png" alt="House warming priest packages">
					<div class="cat-card-title">
						<h3 class="text-center mdl-color-text--white">Priests</h3>
					</div>
				</a>
			</div>
		</div>
	</div>

</div>