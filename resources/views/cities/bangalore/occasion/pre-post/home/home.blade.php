<!-- Home page image -->
<section class="home-section slides-wrap home-occasion">
	<div class="hide__400 hide-400-600 hide">
		<nav class="occasion-top-navbar no-pad-l">
			<div class="header-hr-line"></div>
			<ul class="no-mar-b">
				<li>
					<a href="{{ route('city.occasion.pre-post.decors.list', $cityUrl) }}?ref=header" title="Stage, flower decorations for engagements, wedding receptions in {{ $cityName }}">Decor Styles</a>
				</li>
				<li>
					<a href="{{ route('city.occasion.pre-post.ent.list', $cityUrl) }}?ref=header" title="Entertainment options for engagements, wedding receptions in {{ $cityName }}">Entertainment</a>
				</li>
				<li>
					<a href="{{ route('city.occasion.pre-post.cakes.list', $cityUrl) }}?ref=header" title="Special cakes for wedding receptions, engagements in {{ $cityName }}">Cakes</a>
				</li>
				<li>
					<a href="{{ route('city.occasion.pre-post.collection.list', $cityUrl) }}?ref=header" title="Special cakes for wedding receptions, engagements in {{ $cityName }}">Collections</a>
				</li>
			</ul>
		</nav>
	</div>

	<div class="intro-header">
		<div class="img-slides">
			<ul class="rslides">
				<li>
					<div class="top-slide" style="background-image:url({{ $galleryUrl }}/main/img/bg-reception.jpg)"></div>
				</li>
			</ul>
			<div class="bg-overlay"></div>
			<div class="intro-message">
				<h1 class="text-bg">Let's plan your <span id="changeText"> Engagement </span> online</h1>
			</div>
		</div>
	</div>

	<!-- Categories section -->
	<div class="hide show-400-600 unhide__400">
		<div class="categories-wrap pos-abs">
			<ul class="categories-list no-mar ls-none no-pad">
				<!-- @see: do not close the li tag in same line to avoid mystery space -->
				<li>
					<div class="arrow-bottom"></div>
					<a href="{{ route('city.occasion.pre-post.decors.list', $cityUrl) }}?ref=header" title="Stage, flower decorations for engagements, wedding receptions in {{ $cityName }}">
						Decor Styles
					</a>
				</li>
				<li>
					<div class="arrow-bottom"></div>
					<a href="{{ route('city.occasion.pre-post.ent.list', $cityUrl) }}?ref=header" title="Entertainment options for engagements, wedding receptions in {{ $cityName }}">
						Entertainment
					</a>
				</li>
				<li>
					<div class="arrow-bottom"></div>
					<a href="{{ route('city.occasion.pre-post.cakes.list', $cityUrl) }}?ref=header" title="Special cakes for wedding receptions, engagements in {{ $cityName }}">
						Cakes
					</a>
				</li>
			</ul>
		</div>
	</div>
</section>

<!-- Showcase popular listings -->
@if(isset($data['isShowTopContainer']) && $data['isShowTopContainer'])
	<section class="home-section padding-30 no-pad__400">
		<div class="col-md-12">
			@if(isset($data['collections']) && count($data['collections']) > 0)
				<div class="pad-t-20 pad-b-20">
					<div class="head-title">
						<div class="col-md-8 col-sm-12 col-xs-12">
							<h5 class="section-title">Wedding Collections</h5>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 pull-right">
							<a href="{{ route('city.occasion.pre-post.collection.list', $cityUrl) }}?ref=top-all" class="btn-see-all pull-right"> See all Collections</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="top-sec-items">
						@foreach($data['collections'] as $collection)
							<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 mar-b-10">
								<div class="collection-card-list">
									<a href="{{ $data['collectionBaseUrl'].$collection['url'] }}?ref=home-collections">
										<img src="{{ $collection['coverImg'] }}" alt="{{ $collection['name'] }}">
										<div class="collection-card-list-title">
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 collection-card-list-wrap">
												<img src="{{ $collection['profileImg'] }}" alt="{{ $collection['name'] }}">
											</div>
											<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 collection-card-list-title-wrap">
												<h3 class="mdl-color-text--white pull-left">{{ $collection['name'] }}</h3>
											</div>
											<h6 class="text-center mdl-color-text--white">{{ $collection['description'] }}</h6>
										</div>
									</a>
								</div>
							</div>
						@endforeach
						<div class="clearfix"></div>
					</div>
				</div>
			@endif

			@if(isset($data['topVenues']) && count($data['topVenues']) > 0)
				<div class="pad-t-20 pad-b-20">
					<div class="head-title">
						<div class="col-md-8 col-sm-12 col-xs-12">
							<h5 class="section-title">Popular Venues</h5>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 pull-right">
							<a href="{{ route('city.occasion.pre-post.venues.list', $cityUrl) }}" class="btn-see-all pull-right"> See all Venues</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="top-sec-items">
						@foreach($data['topVenues'] as $hall)
							<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 mar-b-15 trending">
								<a href="{{ route('city.occasion.pre-post.venues.profile', [$cityUrl, $hall->url]) }}?ref=top-image">
									<div class="img-container">
										<img src="{{ $hall->getProfileImg() }}" title="Wedding reception / engagement venue - {{ $hall->name }}">
										@include('app.shortlist_results', [
																"mapId" => $hall->id,
																"mapTypeId" => config('evibe.ticket.type.halls'),
																"occasionId" => $data['occasionId'],
																"cityId" => $data['cityId']
															])
										<div class="price">
											@if ($hall->rent_min)
												@price($hall->rent_min) <span class="star">*</span>
												@if ($hall->rent_max && $hall->rent_min)
													@price($hall->rent_max) <span class="star">*</span>
												@endif
											@elseif ($hall->price_min_veg)
												@price($hall->price_min_veg) <span class="star">*</span> onwards
											@elseif ($hall->venue->price_min_rent)
												@price($hall->venue->price_min_rent)
												@if($hall->venue->price_rent_max && $hall->venue->price_rent_max > $hall->venue->price_min_rent)
													- &nbsp; @price($hall->venue->price_rent_max) <span
															class="star">*</span>
												@endif
											@elseif ($hall->venue->price_min_veg)
												@price($hall->venue->price_min_veg) <span class="star">*</span> onwards
											@endif
										</div>
									</div>
									<h6 class="item-name">
										@if ($hall->type_id == 1)
											Party space
										@else
											{{ $hall->type->name }}
										@endif
										<span> at @truncateName($hall->venue->area->name)</span>
									</h6>
								</a>
							</div>
						@endforeach
						<div class="clearfix"></div>
					</div>
				</div>
			@endif

			@if(isset($data['topDecors']) && count($data['topDecors']) > 0)
				<div class="pad-t-20 pad-b-20">
					<div class="head-title mar-b-20">
						<div class="col-md-8 col-sm-12 col-xs-12">
							<h5 class="section-title">Popular Wedding Reception / Engagement Decorations</h5>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 pull-right">
							<a href="{{ route('city.occasion.pre-post.decors.list', $cityUrl) }}" class="btn-see-all pull-right"> See all Options</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="top-sec-items">
						@foreach($data['topDecors'] as $decor)
							<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 mar-b-15 trending">
								<a href="{{ route('city.occasion.pre-post.decors.profile', [$cityUrl, $decor->url]) }}?ref=top-image">
									<div class="img-container">
										<img src="{{ $decor->getProfileImg() }}" title="Wedding reception / engagement decor - {{ $decor->name }}">
										@include('app.shortlist_results', [
																"mapId" => $decor->id,
																"mapTypeId" => config('evibe.ticket.type.decor'),
																"occasionId" => $data['occasionId'],
																"cityId" => $data['cityId']
															])
										<div class="price">
											@price($decor->min_price)
											<span class="star">*</span>
										</div>
									</div>
									<h6 class="item-name">@truncateName($decor->name)</h6>
								</a>
							</div>
						@endforeach
						<div class="clearfix"></div>
					</div>
				</div>
			@endif

			@if(isset($data['topEntOptions']) && count($data['topEntOptions']) > 0)
				<div class="pad-t-20 pad-b-20">
					<div class="head-title mar-b-20">
						<div class="col-md-8 col-sm-12 col-xs-12">
							<h5 class="section-title">Popular Entertainment Options</h5>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 pull-right">
							<a href="{{ route('city.occasion.pre-post.ent.list', $cityUrl) }}" class="btn-see-all pull-right"> See all Options</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="top-sec-items">
						@foreach($data['topEntOptions'] as $option)
							<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 mar-b-15 trending">
								<a href="{{ route('city.occasion.pre-post.ent.profile', [$cityUrl, $option->url]) }}?ref=top-image">
									<div class="img-container">
										<img src="{{ $option->getProfilePic() }}" title="Reception and engagement entertainment option - {{ $option->name }}">
										@include('app.shortlist_results', [
																"mapId" => $option->id,
																"mapTypeId" => config('evibe.ticket.type.entertainment'),
																"occasionId" => $data['occasionId'],
																"cityId" => $data['cityId']
															])
										<div class="price">
											@price($option->min_price)
											<span class="star">*</span>
										</div>
									</div>
									<h6 class="item-name">@truncateName($option->name)</h6>
								</a>
							</div>
						@endforeach
						<div class="clearfix"></div>
					</div>
				</div>
			@endif

			@if(isset($data['topCakes']) && count($data['topCakes']) > 0)
				<div class="pad-t-20 pad-b-20">
					<div class="head-title mar-b-20">
						<div class="col-md-8 col-sm-12 col-xs-12">
							<h5 class="section-title">Popular Wedding Reception / Engagement Cakes</h5>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 pull-right">
							<a href="{{ route('city.occasion.pre-post.cakes.list', $cityUrl) }}" class="btn-see-all pull-right"> See all Options</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="top-sec-items">
						@foreach($data['topCakes'] as $cake)
							<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 mar-b-15 trending">
								<a href="{{ route('city.occasion.pre-post.cakes.profile', [$cityUrl, $cake->url]) }}?ref=top-image">
									<div class="img-container">
										<img src="{{ $cake->getProfileImg() }}" title="Wedding engagement and reception cake - {{ $cake->title }}">
										@include('app.shortlist_results', [
																"mapId" => $cake->id,
																"mapTypeId" => config('evibe.ticket.type.cake'),
																"occasionId" => $data['occasionId'],
																"cityId" => $data['cityId']
															])
										<div class="price">
											@price($cake->price)
											<span class="star">*</span>
										</div>
									</div>
									<h6 class="item-name">@truncateName($cake->title)</h6>
								</a>
							</div>
						@endforeach
						<div class="clearfix"></div>
					</div>
				</div>
			@endif
		</div>
		<div class="clearfix"></div>
	</section>
@endif

