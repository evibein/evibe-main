<div class="header-bottom">
	<div class="container">
		<div id='cssmenu' class="menu-links">
			<ul>
				<li>
					<a href="{{ route('city.occasion.bachelor.home', getCityUrl()) }}">Youth Parties</a>
				</li>
				<li>
					<a href="{{ route('city.occasion.surprises.home', getCityUrl()) }}">Surprises</a>
				</li>
				<li>
					<a href="{{ route('city.occasion.pre-post.home', getCityUrl()) }}">Pre / Post Weddings</a>
				</li>
				<li>
					<a href="{{ route('city.occasion.birthdays.home', getCityUrl()) }}">Birthday Parties</a>
				</li>
				<li>
					<a href="{{ route('city.occasion.house-warming.home', getCityUrl()) }}">House Warming</a>
				</li>
			</ul>
		</div>
	</div>
</div>