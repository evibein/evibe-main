<?php
$ref = "pre-post";
$oFooterRef = "footer-master";
if (isset($refKeys) && is_array($refKeys) && array_key_exists('occasionImageRefKey', $refKeys))
{
	$ref = $refKeys['occasionImageRefKey'];
}
if (isset($refKeys) && is_array($refKeys) && array_key_exists('occasionFooterRefKey', $refKeys))
{
	$oFooterRef = $refKeys['occasionFooterRefKey'];
}
?>
<div class="footer-wrap">
	<div class="footer-top pad-t-20 pad-b-20 bg-white occasion-selection-footer hide">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 footer-links-list text-center">
			<h5 class="text-center text-col-gr"> Not Looking For Weddings ?</h5>
			<div class="col-md-3">
				@include('app.footer-occasions.birthday')
			</div>
			<div class="col-md-3">
				@include('app.footer-occasions.bachelor')
			</div>
			<div class="col-md-3">
				@include('app.footer-occasions.surprises')
			</div>
			<div class="col-md-3">
				@include('app.footer-occasions.house-warming')
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>

	<div class="footer-top pad-t-20 padding-0 bg-lg-gray hide">
		<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 col-sm-offset-1 col-md-offset-1 col-lg-offset-1 footer-links-list pad-b-20">
			<div class="col-xs-12 col-sm-12">
				<h6 class="text-center">Weddings</h6>
			</div>
			<div class="col-xs-12 col-sm-12 mar-t-10 ft-text-align footer-ocs-links">
				<ul class="categories-list no-mar text-center ls-none no-pad">
					<!-- do not close the li tag in same line to avoid mystery space -->
					<li>
						<div class="arrow-bottom"></div>
						<a href="{{ route('city.occasion.pre-post.venues.list', $cityUrl) }}?ref={{ $oFooterRef}}">
							<div class="evibe-footer-mid-venue2-w"></div>
							Venues
						</a>
					</li>
					<li>
						<div class="arrow-bottom"></div>
						<a href="{{ route('city.occasion.pre-post.decors.list', $cityUrl) }}?ref={{ $oFooterRef }}">
							<div class="evibe-footer-mid-decoration4-w"></div>
							Decor Styles
						</a>
					</li>
					<li>
						<div class="arrow-bottom"></div>
						<a href="{{ route('city.occasion.pre-post.ent.list', $cityUrl) }}?ref={{ $oFooterRef }}">
							<div class="evibe-footer-mid-entertainment2-w"></div>
							Entertainment
						</a>
					</li>
					<li>
						<div class="arrow-bottom"></div>
						<a href="{{ route('city.occasion.pre-post.cakes.list', $cityUrl) }}?ref={{ $oFooterRef }}">
							<div class="evibe-footer-mid-cake2-w"></div>
							Cakes
						</a>
					</li>
					<li>
						<div class="arrow-bottom"></div>
						<a href="{{ route('city.occasion.pre-post.collection.list', $cityUrl) }}?ref={{ $oFooterRef }}">
							<div class="evibe-footer-mid-collection-w"></div>
							Collections
						</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	@include('base.home.footer.footer-common')
</div>