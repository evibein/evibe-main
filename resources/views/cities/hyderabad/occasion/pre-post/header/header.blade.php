<div class="hide__400 hide-400-600 hide">
	<nav class="occasion-top-navbar no-pad-l">
		<div class="header-hr-line"></div>
		<ul class="no-mar-b">
			<li>
				<a href="{{ route('city.occasion.pre-post.decors.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*'.config('evibe.occasion.pre-post.decors.results').'*')){{ "active" }}@endif">Decor Styles</a>
			</li>
			<li>
				<a href="{{ route('city.occasion.pre-post.ent.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*'.config('evibe.occasion.pre-post.ent.profile_url').'*')){{ "active" }}@endif">Entertainment</a>
			</li>
			<li>
				<a href="{{ route('city.occasion.pre-post.cakes.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*'.config('evibe.occasion.pre-post.cakes.profile_url').'*')){{ "active" }}@endif">Cakes</a>
			</li>
			<li>
				<a href="{{ route('city.occasion.pre-post.collection.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*collections*')){{ "active" }}@endif">Collections</a>
			</li>
		</ul>
		<div class="header-hr-line"></div>
	</nav>
</div>

<div class="hide show-400-600 unhide__400">
	<div class="header-bottom">
		<div class="container">
			<div id='cssmenu' class="menu-links">
				<ul id="header-ocs-lists" class="padding-lists-20">
					<li>
						<a href="{{ route('city.occasion.pre-post.decors.list', getCityUrl()) }}?ref=@if(isset($refKeys) && is_array($refKeys) && array_key_exists('occasionHeaderRefKey',$refKeys)){{ $refKeys['occasionHeaderRefKey'] }}@else header @endif">
							<img src="{{$galleryUrl}}/main/img/icons/decoration4_w.png" alt="Receptions decorations"> Decor Styles
						</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.pre-post.ent.list', getCityUrl()) }}?ref=@if(isset($refKeys) && is_array($refKeys) && array_key_exists('occasionHeaderRefKey',$refKeys)){{ $refKeys['occasionHeaderRefKey'] }}@else header @endif">
							<img src="{{$galleryUrl}}/main/img/icons/entertainment2_w.png" alt="Receptions entertainment options"> Entertainment
						</a>
					</li>

					<li>
						<a href="{{ route('city.occasion.pre-post.cakes.list', getCityUrl()) }}?ref=@if(isset($refKeys) && is_array($refKeys) && array_key_exists('occasionHeaderRefKey',$refKeys)){{ $refKeys['occasionHeaderRefKey'] }}@else header @endif">
							<img src="{{$galleryUrl}}/main/img/icons/cake2_w.png" alt="Receptions cakes"> Cakes
						</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.pre-post.collection.list', getCityUrl()) }}?ref=@if(isset($refKeys) && is_array($refKeys) && array_key_exists('occasionHeaderRefKey',$refKeys)){{ $refKeys['occasionHeaderRefKey'] }}@else header @endif">
							<img src="{{$galleryUrl}}/main/img/icons/collection_w.png" alt="Receptions collections">Collections
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>