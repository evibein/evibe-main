<div class="header-bottom">
	<div class="hide__400 hide-400-600 hide">
		<nav class="occasion-top-navbar no-pad-l">
			<div class="header-hr-line"></div>
			<ul class="no-mar-b">
				<li>
					<a href="{{ route('city.occasion.bachelor.villa.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*'.config('evibe.occasion.bachelor.villa.profile_url').'*')){{ "active" }}@endif">Villas + Farms</a>
				</li>
				<li>
					<a href="{{ route('city.occasion.bachelor.resort.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*'.config('evibe.occasion.bachelor.resort.profile_url').'*')){{ "active" }}@endif">Resorts</a>
				</li>
				<li>
					<a href="{{ route('city.occasion.bachelor.lounge.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*'.config('evibe.occasion.bachelor.lounge.profile_url').'*')){{ "active" }}@endif">Lounges</a>
				</li>
				<li>
					<a href="{{ route('city.occasion.bachelor.ent.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*'.config('evibe.occasion.bachelor.ent.profile_url').'*')){{ "active" }}@endif">Entertainment</a>
				</li>
				<li>
					<a href="{{ route('city.occasion.bachelor.food.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*'.config('evibe.occasion.bachelor.food.profile_url').'*')){{ "active" }}@endif">Food</a>
				</li>
				<li>
					<a href="{{ route('city.occasion.bachelor.cakes.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*'.config('evibe.occasion.bachelor.cakes.profile_url').'*')){{ "active" }}@endif">Cakes</a>
				</li>
				<li>
					<a href="{{ route('city.occasion.bachelor.collection.list', getCityUrl()) }}?ref=header" class="@if(request()->is('*collections*')){{ "active" }}@endif">Collections</a>
				</li>
			</ul>
			<div class="header-hr-line"></div>
		</nav>
	</div>

	<div class="hide show-400-600 unhide__400">
		<div class="pad-l-10 pad-r-10">
			<div id='cssmenu' class="menu-links">
				<ul id="header-ocs-lists">
					<li>
						<a href="{{ route('city.occasion.bachelor.villa.list', getCityUrl()) }}?ref=@if(isset($refKeys) && is_array($refKeys) && array_key_exists('occasionHeaderRefKey',$refKeys)){{ $refKeys['occasionHeaderRefKey'] }}@else{{ 'header' }} @endif">
							<img src="{{$galleryUrl}}/main/img/icons/villa_w.png" class="header-ocs-icon-extra" alt="Bachelors party villas, farm houses"> Villas + Farms
						</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.bachelor.resort.list', getCityUrl()) }}?ref=@if(isset($refKeys) && is_array($refKeys) && array_key_exists('occasionHeaderRefKey',$refKeys)){{ $refKeys['occasionHeaderRefKey'] }}@else{{ 'header' }} @endif">
							<img src="{{$galleryUrl}}/main/img/icons/resorts_w.png" class="header-ocs-icon-extra" alt="Bachelors party resorts"> Resorts
						</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.bachelor.lounge.list', getCityUrl()) }}?ref=@if(isset($refKeys) && is_array($refKeys) && array_key_exists('occasionHeaderRefKey',$refKeys)){{ $refKeys['occasionHeaderRefKey'] }}@else{{ 'header' }} @endif">
							<img src="{{$galleryUrl}}/main/img/icons/lounge_w.png" class="header-ocs-icon-extra" alt="Bachelors party lounges"> Lounges
						</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.bachelor.ent.list', getCityUrl()) }}?ref=@if(isset($refKeys) && is_array($refKeys) && array_key_exists('occasionHeaderRefKey',$refKeys)){{ $refKeys['occasionHeaderRefKey'] }}@else{{ 'header' }} @endif">
							<img src="{{$galleryUrl}}/main/img/icons/entertainment2_w.png" class="header-ocs-icon-extra" alt="Bachelors party entertainment options"> Entertainment
						</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.bachelor.food.list', getCityUrl()) }}?ref=@if(isset($refKeys) && is_array($refKeys) && array_key_exists('occasionHeaderRefKey',$refKeys)){{ $refKeys['occasionHeaderRefKey'] }}@else{{ 'header' }} @endif">
							<img src="{{$galleryUrl}}/main/img/icons/food2_w.png" class="header-ocs-icon-extra" alt="Bachelors party food packages"> Food
						</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.bachelor.cakes.list', getCityUrl()) }}?ref=@if(isset($refKeys) && is_array($refKeys) && array_key_exists('occasionHeaderRefKey',$refKeys)){{ $refKeys['occasionHeaderRefKey'] }}@else{{ 'header' }} @endif">
							<img src="{{$galleryUrl}}/main/img/icons/cake2_w.png" class="header-ocs-icon-extra" alt="Bachelors party cakes"> Cakes
						</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.bachelor.collection.list', getCityUrl()) }}?ref=@if(isset($refKeys) && is_array($refKeys) && array_key_exists('occasionHeaderRefKey',$refKeys)){{ $refKeys['occasionHeaderRefKey'] }}@else{{ 'header' }} @endif">
							<img src="{{$galleryUrl}}/main/img/icons/collection_w.png" class="header-ocs-icon-extra" alt="Bachelors party collections">Collections
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>