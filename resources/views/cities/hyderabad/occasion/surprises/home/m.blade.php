<section class="home-section slides-wrap home-occasion ce-no-categories">
	<div class="intro-header">
		<div class="img-slides">
			<ul class="rslides">
				<li>
					<div class="top-slide" style="background-image:url({{$galleryUrl}}/main/img/bg-couple-exp.jpg)"></div>
				</li>
				<li>
					<div class="top-slide" style="background-image:url({{ $galleryUrl }}/main/img/home/surprises-home-bg-1.jpg)"></div>
				</li>
				<li>
					<div class="top-slide" style="background-image:url({{ $galleryUrl }}/main/img/home/surprises-home-bg-2.jpg)"></div>
				</li>
			</ul>
			<div class="bg-overlay"></div>
			<div class="intro-message surprise-intro-message">
				<h1 class="text-bg">{{ $data['seo']['pageHeader'] }}</h1>
				<a class="btn btn-lg btn-evibe mar-t-30" href="{{ config("evibe.host") . "/" .$cityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?utm_source=occasion-home&utm_campaign=category-lists&utm_medium=website&utm_content=surprise-packages">
					Explore All Packages
				</a>
			</div>
			<div class="surprise-filter-bar surprise-filter-bar-m hide">
				@include("occasion.util.list.m_components.surprises-filter-bar")
				<input type="hidden" id="surResultsUrl" data-ref="mob-sur-home" value="{{ route('city.occasion.'.config("evibe.type-event-route-name.".$data['occasionId']).'.'.config('evibe.type-product-route-name.'.config('evibe.ticket.type.surprises')).'.list', $cityUrl) }}">
			</div>
		</div>
	</div>
</section>

<section class="home-section padding-30 no-pad__400" style="background:#ffffff;">
	<div class="container surprise-container">
		@foreach($data['topPackages'] as $key => $package)
			@include('occasion.util.list.m_components.package.top-packages-category', ["name" => $key])
		@endforeach
		<div class="clearfix"></div>
	</div>
</section>