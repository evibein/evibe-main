<section class="section-press">
	<div class="container padding-30">
		<h4 class="sec-title title-text text-center">Evibe<span class="text-lower">.in</span> in media</h4>
		<div class="row press text-center">
			<div class="col-md-4 col-sm-12 col-xs-12 press-focus">
				<img src="{{ $galleryUrl }}/main/img/icons/press/logo-toi2.png" alt="Evibe on Times of India">
				<p align="justify">
					"A platform for event services to organise a party"
				</p>
			</div>
			<div class="col-md-4 col-sm-12 col-xs-12 press-focus">
				<img src="{{ $galleryUrl }}/main/img/icons/press/logo-indian-express.png" alt="Evibe on The New Indian Express">
				<p align="justify">
					"The Great Indian Party Planners"
				</p>
			</div>
			<div class="col-md-4 col-sm-12 col-xs-12 press-focus">
				<img src="{{ $galleryUrl }}/main/img/icons/press/logo-namaste-telangana.png" alt="Evibe on Namaste Telangana">
				<p align="justify">
					"Celebrate birthday in a special way"
				</p>
			</div>

			<ul class="press-logo top-30">
				<li>
					<img src="{{ $galleryUrl }}/main/img/icons/press/logo-bonnevie-small.png" class="img-responsive img-centered" alt="Evibe on Bonnevie News">
				</li>
				<li>
					<img src="{{ $galleryUrl }}/main/img/icons/press/logo-yourstory.png" class="img-responsive img-centered" alt="Evibe on YourStory">
				</li>
				<li>
					<img src="{{ $galleryUrl }}/main/img/icons/press/logo-vccircle.png" class="img-responsive img-centered" style="opacity:0.8; height: 25px; margin-top: 10px;" alt="Evibe on VCCircle">
				</li>
				<li>
					<img src="{{ $galleryUrl }}/main/img/icons/press/logo-inc42.png" class="img-responsive img-centered" style="opacity:0.8; height: 30px; margin-top: 10px;" alt="Evibe on Inc42">
				</li>
				<li>
					<img src="{{ $galleryUrl }}/main/img/icons/press/logo-medianama.png" class="img-responsive img-centered" style="opacity:0.8; height: 25px; margin-top: 12px;" alt="Evibe on Medianama.com">
				</li>
				<li>
					<img src="{{ $galleryUrl }}/main/img/icons/press/logo-tia.png" class="img-responsive img-centered" style="opacity:0.8; height:30px; margin-top: 10px;" alt="Evibe on TechInAsia.com">
				</li>
			</ul>
		</div>
	</div>
</section>