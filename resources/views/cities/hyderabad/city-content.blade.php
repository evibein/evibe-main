<div class="container">
	<div class="section-ocs @if(isset($data['collections']) && count($data['collections']) > 0) padding-t-75 @else margin-75 @endif">
		<h4 class="sec-title text-center font-weight-500 text-pr-col mar-b-40">Birthday Party Essentials</h4>
		<h6 class="text-center top-10"></h6>
		<div class="row">
			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 cat-card">
				<a href="{{ route('city.occasion.birthdays.venue_deals.list', $cityUrl) }}">
					<img src="{{ $galleryUrl }}/main/img/home/birthday-venue.jpg" alt="Birthday Venue">
					<div class="cat-card-title">
						<h3 class="text-center mdl-color-text--white">Venue Deals</h3>
					</div>
				</a>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 cat-card">
				<a href="{{ route('city.occasion.birthdays.trends.list', $cityUrl) }}">
					<img src="{{ $galleryUrl }}/main/img/home/birthday-trends.jpg" alt="Birthday Trend">
					<div class="cat-card-title">
						<h3 class="text-center mdl-color-text--white">Trends</h3>
					</div>
				</a>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 cat-card">
				<a href="{{ route('city.occasion.birthdays.decors.list', $cityUrl) }}">
					<img src="{{ $galleryUrl }}/main/img/home/birthday-decor2.jpg" alt="Birthday Decorations">
					<div class="cat-card-title">
						<h3 class="text-center mdl-color-text--white">Decor Styles</h3>
					</div>
				</a>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 cat-card">
				<a href="{{ route('city.occasion.birthdays.packages.list', $cityUrl) }}">
					<img src="{{ $galleryUrl }}/main/img/home/birthday-packages.jpg" alt="Birthday Package">
					<div class="cat-card-title">
						<h3 class="text-center mdl-color-text--white">Packages</h3>
					</div>
				</a>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 cat-card">
				<a href="{{ route('city.occasion.birthdays.ent.list', $cityUrl) }}">
					<img src="{{ $galleryUrl }}/main/img/home/birthday-entertainment.jpg" alt="Birthday entertainment options">
					<div class="cat-card-title">
						<h3 class="text-center mdl-color-text--white">Entertainment</h3>
					</div>
				</a>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 cat-card">
				<a href="{{ route('city.occasion.birthdays.cakes.list', $cityUrl) }}">
					<img src="{{ $galleryUrl }}/main/img/home/birthday-cakes.jpg" alt="Birthday cakes">
					<div class="cat-card-title">
						<h3 class="text-center mdl-color-text--white">Cakes</h3>

					</div>
				</a>
			</div>
		</div>
	</div>
</div>