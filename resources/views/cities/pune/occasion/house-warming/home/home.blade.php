<!-- Home page image -->
<section class="home-section slides-wrap home-occasion">
	<div class="intro-header ke-intro-header">
		<div class="img-slides">
			<ul class="rslides">
				<li>
					<div class="top-slide" style="background-image:url({{$galleryUrl}}/main/img/bg-house-warming.jpg)"></div>
				</li>
			</ul>
			<div class="bg-overlay"></div>
			<div class="intro-message ke-intro-message hw-intro-message">
				<!---- Change the header for the House warming -->
				<h1 class="text-bg">{{ $data['seo']['pageHeader'] }}</h1>
				<div class="mar-t-10">
					<h4 class="no-mar-t">
						<img src="{{ $galleryUrl }}/main/img/icons/trust_w.png" height="45px" width="45px">
						<span class="pad-l-5">TRUSTED BY {{ config("evibe.stats.parties_done") }} CUSTOMERS</span>
					</h4>
				</div>
				<div class="mar-t-20">
					<a class="btn btn-evibe btn-lg btn-post">
						Enquire Now
					</a>
					<div class="pad-t-5 hide">
						<img src="{{ $galleryUrl }}/main/img/icons/whatsapp.png" height="18px" width="18px">
						@if ($agent->isMobile() && !($agent->isTablet()))
							<a href="whatsapp://send?phone=91{{ config("evibe.contact.customer.whatsapp.phone") }}&text=Hello Evibe.in, I am interested in your services for my party." class="whatsapp-wrap mar-t-8 pad-l-5 valign-mid mdl-color-text--white font-15">
								{{ config("evibe.contact.customer.whatsapp.phone") }}
							</a>
						@else
							<a href="https://web.whatsapp.com/send?phone=91{{ config("evibe.contact.customer.whatsapp.phone") }}&text=Hello Evibe.in, I am interested in your services for my party." target="_blank" class="whatsapp-wrap mar-t-8 pad-l-5 valign-mid mdl-color-text--white font-15">
								{{ config("evibe.contact.customer.whatsapp.phone") }}
							</a>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Categories section -->
	<div class="hide show-400-600 unhide__400">
		<div class="categories-wrap pos-abs">
			<ul class="categories-list no-mar ls-none no-pad">
				<!-- @see: do not close the li tag in same line to avoid mystery space -->
				<li>
					<div class="arrow-bottom"></div>
					<a href="{{ route('city.occasion.house-warming.decors.list', $cityUrl) }}?ref=header">
						Decor Styles
					</a>
				</li>
				<li>
					<div class="arrow-bottom"></div>
					<a href="{{ route('city.occasion.house-warming.food.list', $cityUrl) }}?ref=header">
						Food
					</a>
				</li>
				<li>
					<div class="arrow-bottom"></div>
					<a href="{{ route('city.occasion.house-warming.priest.list', $cityUrl) }}?ref=header">
						Priest
					</a>
				</li>
				<li>
					<div class="arrow-bottom"></div>
					<a href="{{ route('city.occasion.house-warming.tent.list', $cityUrl) }}?ref=header">
						Tent
					</a>
				</li>
				<li>
					<div class="arrow-bottom"></div>
					<a href="{{ route('city.occasion.house-warming.ent.list', $cityUrl) }}?ref=header">
						Add-ons
					</a>
				</li>
				<li class="hide">
					<div class="arrow-bottom"></div>
					<a href="{{ route('city.occasion.house-warming.cakes.list', $cityUrl) }}?ref=header">
						Cakes
					</a>
				</li>
			</ul>
		</div>
	</div>
</section>

<!-- top results in house warming -->
<section class="home-section padding-30 no-pad__400">
	<div class="container">
		@if(count($data['collections']) > 0)
			<div class="row trending-items pad-t-20 pad-b-20">
				<div class="head-title mar-b-40 pos-rel">
					<div class="col-md-8 col-sm-12 col-xs-12">
						<h5 class="section-title">House Warming Collections</h5>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 pull-right">
						<a href="{{ route('city.occasion.house-warming.collection.list', $cityUrl) }}?ref=top-all" class="btn-see-all pull-right"> See all Collections</a>
					</div>
					<div class="clearfix"></div>
				</div>
				@foreach($data['collections'] as $collection)
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
						<div class="collection-card-list">
							<a href="{{ $data['collectionBaseUrl'].$collection['url'] }}?ref=home-collections">
								<img src="{{ $collection['coverImg'] }}" alt="{{ $collection['name'] }}">
								<div class="collection-card-list-title">
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 collection-card-list-wrap">
										<img src="{{ $collection['profileImg'] }}" alt="{{ $collection['name'] }}">
									</div>
									<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 collection-card-list-title-wrap">
										<h3 class="mdl-color-text--white pull-left">{{ $collection['name'] }}</h3>
									</div>
									<h6 class="text-center mdl-color-text--white">{{ $collection['description'] }}</h6>
								</div>
							</a>
						</div>
					</div>
				@endforeach
				<div class="clearfix"></div>
			</div>
		@endif

		@if(count($data['topDecors']) > 0)
			<div class="row trending-items pad-t-20 pad-b-20">
				<div class="head-title mar-b-40 pos-rel">
					<div class="col-md-8 col-sm-12 col-xs-12">
						<h5 class="section-title">Popular Decor Styles</h5>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 pull-right">
						<a href="{{ route('city.occasion.house-warming.decors.list', $cityUrl) }}" class="btn-see-all pull-right"> See all Options</a>
					</div>
					<div class="clearfix"></div>
				</div>
				@foreach($data['topDecors'] as $decor)
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 trending">
						<a href="{{ route('city.occasion.house-warming.decors.profile', [$cityUrl, $decor->url]) }}?ref=top-image">
							<div class="img-container">
								<img src="{{ $decor->getProfileImg() }}" title="House warming decoration {{ $decor->name }} in {{ $cityName }}">
								@include('app.shortlist_results', [
																"mapId" => $decor->id,
																"mapTypeId" => config('evibe.ticket.type.decor'),
																"occasionId" => $data['occasionId'],
																"cityId" => $data['cityId']
															])
								<div class="price">
									@price($decor->min_price)
									<span class="star">*</span>
									@if ($decor->max_price && $decor->max_price > $decor->min_price)
										- &nbsp; @price($decor->max_price) <span class="star">*</span>
									@endif
								</div>
							</div>
							<h6 class="item-name">@truncateName($decor->name)</h6>
						</a>
					</div>
				@endforeach
				<div class="clearfix"></div>
			</div>
		@endif

		@if(count($data['topFoodOptions']) > 0)
			<div class="row trending-items pad-t-20 pad-b-20">
				<div class="head-title mar-b-40 pos-rel">
					<div class="col-md-8 col-sm-12 col-xs-12">
						<h5 class="section-title">Popular Food Packages</h5>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 pull-right">
						<a href="{{ route('city.occasion.house-warming.food.list', $cityUrl) }}" class="btn-see-all pull-right"> See All Food Packages</a>
					</div>
					<div class="clearfix"></div>
				</div>
				@foreach($data['topFoodOptions'] as $option)
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 trending">
						<a href="{{ route('city.occasion.house-warming.food.profile', [$cityUrl, $option->url]) }}?ref=top-image">
							<div class="img-container">
								<img src="{{ $option->getProfileImg('results') }}" title="House warming party food packages">
								@include('app.shortlist_results', [
															"mapId" => $option->id,
															"mapTypeId" => config('evibe.ticket.type.food'),
															"occasionId" => $data['occasionId'],
															"cityId" => $data['cityId']
														])
								<div class="price">
									@price($option->price)
									<span class="star">*</span>
									<span class="onwards">Onwards</span>
								</div>
							</div>
							<h6 class="item-name">@truncateName($option->name)</h6>
						</a>
					</div>
				@endforeach
				<div class="clearfix"></div>
			</div>
		@endif

		@if(count($data['topAddons']) > 0)
			<div class="row trending-items pad-t-20 pad-b-20">
				<div class="head-title mar-b-40 pos-rel">
					<div class="col-md-8 col-sm-12 col-xs-12">
						<h5 class="section-title">Popular House Warming Add-on Services</h5>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 pull-right">
						<a href="{{ route('city.occasion.house-warming.ent.list', $cityUrl) }}" class="btn-see-all pull-right"> See all Options</a>
					</div>
					<div class="clearfix"></div>
				</div>
				@foreach($data['topAddons'] as $addon)
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 trending">
						<a href="{{ route('city.occasion.house-warming.ent.profile', [$cityUrl, $addon->url]) }}?ref=top-image">
							<div class="img-container">
								<img src="{{ $addon->getProfilePic('results') }}" title="House Warming Add-on option {{ $addon->name }} in {{ $cityName }}">
								@include('app.shortlist_results', [
															"mapId" => $addon->id,
															"mapTypeId" => config('evibe.ticket.type.entertainment'),
															"occasionId" => $data['occasionId'],
															"cityId" => $data['cityId']
														])
								<div class="price">
									@price($addon->min_price)
									<span class="star">*</span>
								</div>
							</div>
							<h6 class="item-name">@truncateName($addon->name)</h6>
						</a>
					</div>
				@endforeach
				<div class="clearfix"></div>
			</div>
		@endif
	</div>
</section>
