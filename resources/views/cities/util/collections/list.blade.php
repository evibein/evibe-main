@extends('layout.base')

@section('page-title')
	<title>{{ $data['seo']['pageTitle'] }} | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="{{ $data['seo']['pageDescription'] }}"/>
@endsection

@section('meta-keywords')
	<meta name="keywords" content="{{ isset($data['seo']['keyWords']) ? $data['seo']['keyWords'] : ""}}">
@endsection

@section('og-title')
	<meta property="og:title" content="{{ $data['seo']['pageTitle'] }} | Evibe.in"/>
@endsection

@section('og-description')
	<meta property="og:description" content="{{ $data['seo']['pageDescription'] }}"/>
@endsection

@section('seo:schema')
	@parent
	@if(isset($data['seo']['schema']) && count($data['seo']['schema']))
		@foreach($data['seo']['schema'] as $schema)
			{!! $schema !!}
		@endforeach
	@endif
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-list.css') }}"/>
@endsection

@section("header")
	@include('base.home.header.header-home-city')
	<div class="header-border full-width mar-b-10"></div>
@endsection

@section("content")
	<div class="items-results">
		<div class="col-sm-12">
			<div class="top-panel">
				<div class="col-sm-10 col-sm-offset-1">
					<h1 title="{{ $data['seo']['pageHeader'] }}" class="text-cap">
						Showing {{ $data['seo']['pageHeader'] }}
					</h1>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="bottom-panel col-lg-10 col-lg-offset-1 col-sm-12 col-sm-offset-0">
				@if(count($data['collections']) > 0)
					<div>
						@foreach($data['collections'] as $collection)
							<div class="col-lg-4 col-md-6 col-sm-6 col-xs-10">
								<div class="collection-card-list">
									<a href="{{ $data['collectionBaseUrl'].$collection['url'] }}?ref=home-collections">
										<img src="{{ $collection['coverImg'] }}" alt="{{ $collection['name'] }}">
										<div class="collection-card-list-title">
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 collection-card-list-wrap">
												<img src="{{ $collection['profileImg'] }}" alt="{{ $collection['name'] }}">
											</div>
											<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 collection-card-list-title-wrap">
												<h3 class="mdl-color-text--white pull-left">{{ $collection['name'] }}</h3>
											</div>
											<h6 class="text-center mdl-color-text--white">{{ $collection['description'] }}</h6>
										</div>
									</a>
								</div>
							</div>
						@endforeach
						<div class="clearfix"></div>
					</div>
					<div class="pages text-center"> <!-- pagination begin -->
						<div>{{ $data['collections']->links() }}</div>
					</div>
				@else
					<div class="no-results-wrap text-center">
						<h4 class="text-col-gr no-mar">We are adding amazing party collections soon, keep checking this page.</h4>
					</div>
				@endif
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection

@section("footer")
	<div class="footer-wrap">
		@include('base.home.footer.footer-common')
	</div>
@endsection