<section class="home-section slides-wrap home-occasion ce-no-categories surprises-home-section">
	<div class="intro-header">
		<div class="img-slides">
			<ul class="rslides">
				<li>
					<div class="top-slide" style="background-image:url({{$galleryUrl}}/main/img/bg-couple-exp.jpg)"></div>
				</li>
				<li>
					<div class="top-slide" style="background-image:url({{ $galleryUrl }}/main/img/home/surprises-home-bg-1.jpg)"></div>
				</li>
				<li>
					<div class="top-slide" style="background-image:url({{ $galleryUrl }}/main/img/home/surprises-home-bg-2.jpg)"></div>
				</li>
			</ul>
			<div class="bg-overlay"></div>
			<div class="intro-message">
				<h1 class="text-bg">{{ $data['seo']['pageHeader'] }}</h1>
				<a class="btn btn-lg btn-evibe" href="{{ config("evibe.host") . "/" .$cityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?utm_source=occasion-home&utm_campaign=category-lists&utm_medium=website&utm_content=surprise-packages">
					Explore All Packages
				</a>
			</div>
			<div class="surprise-filter-bar hide">
				@include("occasion.util.list.d_components.surprises-filter-bar")
				<input type="hidden" id="surResultsUrl" data-ref="des-sur-home" value="{{ route('city.occasion.'.config("evibe.type-event-route-name.".$data['occasionId']).'.'.config('evibe.type-product-route-name.'.config('evibe.ticket.type.surprises')).'.list', $cityUrl) }}">
			</div>
		</div>
	</div>
</section>

@include('base.home.surprise-review')

<section class="home-section padding-30 no-pad__400" style="background:#ffffff;">
	<div class="container surprise-container">
		@if(count($data['collections']) > 0)
			<div class="row surprise-home-collections">
				<div class="">
					<div class="head-title pos-rel">
						<div class="col-md-8 col-sm-8 col-xs-8">
							<h4 class="mar-t-15 no-mar-b">Best Surprise Collections</h4>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-4 pull-right mar-t-15">
							<a href="{{ route('city.occasion.surprises.collection.list', $cityUrl) }}?ref=top-all" class="mdc-button mdc-button--raised pull-right">
								<button class="btn btn-default btn-see-all-surprises"><span class="valign-mid">See More</span>
								</button>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div>
					@foreach($data['collections'] as $collection)
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
							<div class="collection-card-list surprise-collection-card">
								<a class="surprise-collection-card-radius" href="{{ $data['collectionBaseUrl'].$collection['url'] }}?ref=home-collections">
									<img src="{{ $collection['coverImg'] }}" alt="{{ $collection['name'] }}">
									<div class="collection-card-list-title surprise-collection-card-radius">
										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 collection-card-list-wrap">
											<img src="{{ $collection['profileImg'] }}" alt="{{ $collection['name'] }}">
										</div>
										<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 collection-card-list-title-wrap">
											<h3 class="mdl-color-text--white pull-left">{{ $collection['name'] }}</h3>
										</div>
										<h6 class="text-center mdl-color-text--white">{{ $collection['description'] }}</h6>
									</div>
								</a>
							</div>
						</div>
					@endforeach
				</div>
				<div class="clearfix"></div>
			</div>
		@endif
		@foreach($data['topPackages'] as $key => $package)
			@include('occasion.util.list.d_components.package.top-packages-category', ["name" => $key])
		@endforeach
		<div class="clearfix"></div>
	</div>
</section>