<!-- Home page image -->
<section class="home-section slides-wrap home-occasion ce-no-categories">
	<div class="intro-header">
		<div class="img-slides">
			<ul class="rslides">
				<li>
					<div class="top-slide" style="background-image:url({{$galleryUrl}}/main/img/bg-couple-exp.jpg)"></div>
				</li>
			</ul>
			<div class="bg-overlay"></div>
			<div class="intro-message">
				<!---- Change the header for the Special Couple Experiences -->
				<h1 class="text-bg">{{ $data['seo']['pageHeader'] }}</h1>
			</div>
		</div>
	</div>

	<!-- Removed the Categories section for the Couple Experiences -->
</section>

<!-- Showcase popular listings -->
<section class="home-section padding-30 no-pad__400">
	<div class="container">
		@if(count($data['collections']) > 0)
			<div class="row trending-items pad-t-20 pad-b-20">
				<div class="head-title mar-b-40 pos-rel">
					<div class="col-md-8 col-sm-12 col-xs-12">
						<h5 class="section-title">Popular Collections</h5>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 pull-right">
						<a href="{{ route('city.occasion.surprises.collection.list', $cityUrl) }}?ref=top-all" class="btn-see-all pull-right"> See all Collections</a>
					</div>
					<div class="clearfix"></div>
				</div>
				@foreach($data['collections'] as $collection)
					<div class="col-sm-12 col-md-6 col-lg-4 no-pad-l">
						<div class="collection-card">
							<a href="{{ $data['collectionBaseUrl'].$collection['url'] }}?ref=list-image">
								<img src="{{ $collection['profileImg'] }}" alt="{{ $collection['name'] }}">
								<div class="collection-card-title">
									<h3 class="text-center mdl-color-text--white">{{ $collection['name'] }}</h3>
									<h6 class="text-center mdl-color-text--white">{{ $collection['description'] }}</h6>
								</div>
							</a>
						</div>
					</div>
				@endforeach
				<div class="clearfix"></div>
			</div>
		@endif
		@if(count($data['topPackages']) > 0)
			<div class="row trending-items pad-t-20 pad-b-20">
				<div class="head-title mar-b-40 pos-rel">
					<div class="col-md-8 col-sm-12 col-xs-12">
						<h5 class="section-title">Popular Packages</h5>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 pull-right">
						<a href="{{ route('city.occasion.surprises.package.list', $cityUrl) }}?ref=top-all" class="btn-see-all pull-right"> See all Packages</a>
					</div>
					<div class="clearfix"></div>
				</div>
				@foreach($data['topPackages'] as $package)
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 trending mar-b-30">
						<div class="img-container">
							<a href="{{ route('city.occasion.surprises.package.profile', [$cityUrl, $package->url]) }}?ref=top-image">
								<img src="{{ $package->getProfileImg('results') }}" title="romantic experience {{ $package->code }}">
							</a>
							@include('app.shortlist_results', [
																"mapId" => $package->id,
																"mapTypeId" => config('evibe.ticket.type.surprises'),
																"occasionId" => $data['occasionId'],
																"cityId" => $data['cityId']
															])
							<div class="price">
								@price($package->price)
								<span class="star">*</span>
							</div>
							@if(in_array($package->id, $data['autoBook']))
								<div class="booking">
									<a href="{{ route('city.occasion.surprises.package.profile', [$cityUrl, $package->url]) }}?ref=top-image#bookNowModal" class="btn btn-primary btn-view-more focused">Book Now</a>
								</div>
							@endif
						</div>
						<a href="{{ route('city.occasion.surprises.package.profile', [$cityUrl, $package->url]) }}?ref=top-image">
							<h6 class="item-name ov-text-no-wrap">@truncateName($package->name)</h6>
						</a>
					</div>
				@endforeach
				<div class="clearfix"></div>
			</div>
		@endif
		<div class="clearfix"></div>
	</div>
</section>

