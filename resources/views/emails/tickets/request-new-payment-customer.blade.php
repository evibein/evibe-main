<div>
	Hello {{ $data["name"] }},
	<div style="margin-top: 15px;">
		We have received your request for a new payment link for your party order on {{ $data["partyDateTime"] }}. We will re-check the availability of the service(s) and revert back in 2-4 business hours.
	</div>
	<div style="margin-top: 15px;">
		Should you need any further help, please feel free to reach us on the undersigned or reply to this email.
	</div>
	<div style="margin-top: 15px;">
		Thanks for choosing Evibe.in for your celebration.
	</div>
	<div style="margin-top: 15px;">
		<div>Thanks,</div>
		<div>Team Evibe.in</div>
		<div>Phone: +91 9640204000 (10 AM - 9 PM)</div>
	</div>
</div>