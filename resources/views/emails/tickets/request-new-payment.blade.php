<div>
	Hi Team,
	<div style="margin-top: 20px;">
		Customer requested for a new payment link, Since old payment link got expired.
	</div>
	<div style="margin-top: 10px">
		Please check the availability & send payment link again. <br>
		Dash Ticket Link (<a href="{{ config("evibe.dash_host") . "/tickets/" . $data["ticketId"] . "/bookings" }}">click here</a>)
	</div>
</div>