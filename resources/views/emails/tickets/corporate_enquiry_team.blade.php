<div style="padding:10px; border:15px solid #f2f2f2">
	<p>Hello Team,</p>
	<p>A new corporate enquiry has been raised. Please go through the below details.</p>
	<div style="margin-top:20px">
		<table style="border-collapse: collapse;" border="1">
			<tr>
				<td style="padding:8px">Name</td>
				<td style="padding:8px">{{ $data['name'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Email</td>
				<td style="padding: 8px">{{ $data['email'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Phone</td>
				<td style="padding: 8px">{{ $data['phone'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">City</td>
				<td style="padding: 8px">{{ $data['cityName'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Company Name</td>
				<td style="padding: 8px">{{ $data['companyName'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Party Date</td>
				<td style="padding: 8px">{{ $data['eventDate'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Requirements</td>
				<td style="padding: 8px">{!! $data['comments'] !!}</td>
			</tr>
			<tr>
				<td style="padding:8px">Source</td>
				<td style="padding: 8px">{{ $data['source'] }}</td>
			</tr>
		</table>
	</div>
	<div style="margin-top:10px">
		Best Regards,
		<div>Admin</div>
	</div>
</div>