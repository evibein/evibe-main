<p>Dear {{ $data['customerName'] }},</p>

<p>Thanks for uploading your ID proof.</p>

<p>
	Our team will validate the proof and will get in touch with you.
</p>

<p>You can track the validation progress here: {{ $data['tmoLink'] }}</p>

<div style="margin-top: 20px; margin-bottom: 15px;">
	<div>Cheers,</div>
	<div>Team Evibe.in</div>
</div>