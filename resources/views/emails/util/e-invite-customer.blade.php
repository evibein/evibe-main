<p>Dear {{ $data['customerName'] }},</p>

<p>Thanks for opting in for our invitation services.</p>

<p>
	Kindly fill the <a href="{{ $data['formUrl'] }}">form</a> provided to receive a specialised e-invitation for your party. You will receive the e-invitation card within 2 working days from your date of submission. You can use this to invite your guests to the party along with RSVP.
</p>

<p>Link to the form: {!! $data['formUrl'] !!}</p>

<div style="margin-top: 20px; margin-bottom: 15px;">
	<div>Cheers,</div>
	<div>Team Evibe.in</div>
</div>

<p><b>PS:</b> Kindly submit your details before tomorrow so that we will have more time to customise your invite. Ignore this email if already done.</p>