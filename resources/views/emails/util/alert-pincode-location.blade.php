<p>Hello Team,</p>

<p>
	A customer has entered party location, which is not in the list of areas form our Database.
	So, we have updated the customer area location based on the pin code that he/she has entered.
</p>

<p>
	Kindly cross check with customer regarding whether that is the correct location or not.
</p>

<table style="border-collapse: collapse;text-align: left;">
	<tr>
		<th colspan="2" style="border: 1px solid #dddddd; padding: 8px; text-align: center;">Available Details</th>
	</tr>
	@if(isset($data['name']) && $data['name'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Name</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['name'] }}</td>
		</tr>
	@endif
	@if(isset($data['phone']) && $data['phone'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Phone</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['phone'] }}</td>
		</tr>
	@endif
	@if(isset($data['email']) && $data['email'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Email</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['email'] }}</td>
		</tr>
	@endif
	@if(isset($data['partyLocation']) && $data['partyLocation'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Party Location updated by us</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['partyLocation'] }}</td>
		</tr>
	@endif
	@if(isset($data['pinCode']) && $data['pinCode'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer entered Pin Code</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['pinCode'] }}</td>
		</tr>
	@endif
	@if(isset($data['ticketId']) && $data['ticketId'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Ticket Id</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['ticketId'] }}</td>
		</tr>
	@endif
	@if(isset($data['dashLink']) && $data['dashLink'])
		<tr>
			<td colspan="2" style="border: 1px solid #dddddd; padding: 8px;"><a
						href="{{ $data['dashLink'] }}">Dash link to the ticket</a></td>
		</tr>
	@endif
</table>

<p>
	Best Regards,<br>
	Admin
</p>