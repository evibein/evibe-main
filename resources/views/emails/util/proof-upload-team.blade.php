<p>Hello Team,</p>

<p>
	A customer has uploaded ID proof for his party
</p>

<table style="border-collapse: collapse;text-align: left;">
	<tr>
		<th colspan="2" style="border: 1px solid #dddddd; padding: 8px; text-align: center;">Available Details</th>
	</tr>
	@if(isset($data['customerName']) && $data['customerName'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Name</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['customerName'] }}</td>
		</tr>
	@endif
	@if(isset($data['customerPhone']) && $data['customerPhone'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Phone</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['customerPhone'] }}</td>
		</tr>
	@endif
	@if(isset($data['customerEmail']) && $data['customerEmail'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Email</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['customerEmail'] }}</td>
		</tr>
	@endif
	@if(isset($data['partyDate']) && $data['partyDate'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Party Location updated by us</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['partyDate'] }}</td>
		</tr>
	@endif
	@if(isset($data['ticketId']) && $data['ticketId'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Ticket Id</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['ticketId'] }}</td>
		</tr>
	@endif
	@if(isset($data['dashLink']) && $data['dashLink'])
		<tr>
			<td colspan="2" style="border: 1px solid #dddddd; padding: 8px;"><a
						href="{{ $data['dashLink'] }}">Dash link to the ticket</a></td>
		</tr>
	@endif
</table>

<p>
	Best Regards,<br>
	Admin
</p>