<p>Hello Team,</p>

<p>Congrats, a new @if($data["isEnquiry"] != 2){{ "Auto Popup" }}@elseif(isset($data["imageUrl"]) && $data["imageUrl"] != ""){{ "delivery image" }}@endif enquiry has been raised. Please check the details below:</p>

<table style="border-collapse: collapse;text-align: left;">
	<tr>
		<th colspan="2" style="border: 1px solid #dddddd; padding: 8px; text-align: center;">Ticket Details</th>
	</tr>
	@if(isset($data["customerPreviousStatus"]) && $data["customerPreviousStatus"] && $data['customerPreviousStatus']!="")
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Previous Status</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["customerPreviousStatus"] }}</td>
		</tr>
	@endif
	@if(isset($data["surprisesLandingTheatreName"]) && $data["surprisesLandingTheatreName"] != "")
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Selected Theatre name</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{!! $data["surprisesLandingTheatreName"] !!}</td>
		</tr>
	@endif
	<tr>
		<td style="border: 1px solid #dddddd; padding: 8px;"><b>Occasion</b></td>
		<td style="border: 1px solid #dddddd; padding: 8px;">{{ isset($data["occasionName"]) ? $data['occasionName'] : "Default" }}</td>
	</tr>
	<tr>
		<td style="border: 1px solid #dddddd; padding: 8px;"><b>Call Preference</b></td>
		<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['slot'] }}</td>
	</tr>
	<tr>
		<td style="border: 1px solid #dddddd; padding: 8px;"><b>Booking Likeliness</b></td>
		<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['bookingLikeliness'] }}</td>
	</tr>
	<tr>
		<td style="border: 1px solid #dddddd; padding: 8px;"><b>Event Date</b></td>
		<td style="border: 1px solid #dddddd; padding: 8px;">{{ date("d/m/y", $data['date']) }}</td>
	</tr>
	<tr>
		<td style="border: 1px solid #dddddd; padding: 8px;"><b>Page Url</b></td>
		<td style="border: 1px solid #dddddd; padding: 8px;">Enquiry raised from this url (<a href="{{ $data['pageUrl'] }}">{{ $data['pageUrl'] }}</a>)
		</td>
	</tr>
	<tr>
		<td style="border: 1px solid #dddddd; padding: 8px;"><b>Name</b></td>
		<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["name"] }}</td>
	</tr>
	<tr>
		<td style="border: 1px solid #dddddd; padding: 8px;"><b>Email</b></td>
		<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["email"] }}</td>
	</tr>
	<tr>
		<td style="border: 1px solid #dddddd; padding: 8px;"><b>Phone</b></td>
		<td style="border: 1px solid #dddddd; padding: 8px;">
			@if(isset($data["callingCode"]) && $data["callingCode"])
				<span style="margin-right: 2px;">{{ $data["callingCode"] }}</span>
			@endif
			<span>{{ $data["phone"] }}</span>
		</td>
	</tr>
	<tr>
		<td style="border: 1px solid #dddddd; padding: 8px;"><b>City</b></td>
		<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["cityName"] }}</td>
	</tr>
	<tr>
		<td style="border: 1px solid #dddddd; padding: 8px;"><b>Comments / Requirements</b></td>
		<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['comments'] }}</td>
	</tr>
	@if(isset($data["optionDetails"]) && ($data["optionDetails"]["optionName"] || $data["optionDetails"]["optionPrice"] || $data["optionDetails"]["optionUrl"]))
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Option Details</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">
				Name: {{ $data["optionDetails"]["optionName"] }}<br>
				Price: {{ $data["optionDetails"]["optionPrice"] }}<br>
				<a href="{{ $data["optionDetails"]["optionUrl"] }}">Dash link to option</a>
			</td>
		</tr>
	@endif
	@if(isset($data["imageUrl"]) && $data["imageUrl"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Image Url</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{!! $data['imageUrl'] !!}</td>
		</tr>
	@endif
</table>

@if(isset($data["addOns"]) && count($data["addOns"]))
	<p>Customer has selected the following add-ons while making the enquiry</p>
	<table style="border-collapse: collapse;text-align: left;">
		@foreach($data["addOns"] as $addOn)
			<tr>
				<td style="border: 1px solid #dddddd; padding: 8px;">
					<div><b>{{ $addOn->name }}</b></div>
					<div>
						<span style="">Price: </span><span style="margin-left: 2px;">{{ $addOn->price_per_unit }}</span>
					</div>
					<div>{!! $addOn->info !!}</div>
				</td>
			</tr>
		@endforeach
	</table>
@endif

<p>
	<a href="{{ $data["dashLink"] }}" target="_blank">Dash link to ticket.</a>
</p>
<p>
	Let's make their party Evibe special! <br>
</p>