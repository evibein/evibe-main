<p>Hello Team,</p>

<p>Customer requested an update to the order from Track My Order page. Please check the requested details below & update the customer ASAP.</p>

<table style="border-collapse: collapse;text-align: left;">
	@if(isset($data["requestType"]) && $data['requestType']!="")
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Request Type</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["requestType"] }}</td>
		</tr>
	@endif
	@if(isset($data["requestReason"]) && $data["requestReason"] != "")
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Request Details</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{!! $data["requestReason"]  !!}</td>
		</tr>
	@endif
	@if(isset($data["userDeviceInfo"]) && $data["userDeviceInfo"] != "")
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Device Info</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["userDeviceInfo"] }}</td>
		</tr>
	@endif
</table>

<p>
	<a href="{{ config("evibe.dash_host") . "/tickets/" . $data["ticketId"] }}" target="_blank">Dash link to ticket.</a>
</p>
<p>
	Let's make their party Evibe special! <br>
</p>