<p>Namaste {{ $data['name'] }},</p>

<p>
	This is to confirm that we've have received your party enquiry. Our party planning expert shall contact you based on your preferred slot. However, if you need anything urgent, kindly reply to this email. We will respond to you ASAP.
</p>

<p>Please note your enquiry number <strong>#{{ $data['enquiryId'] }}</strong> for future reference.</p>

<p>We look forward to being part of your celebration.</p>

<p>
	Happy Partying,<br/>
	Team Evibe.in <br/>
	<a href="http://youtu.be/6lwEXs51qJw" target="_blank" rel="noopener">Why customers choose Evibe.in?</a>
</p>

<p style="padding-top: 10px;">
	{!! file_get_contents(base_path('resources/views/emails/enquiry/process.html')) !!}
</p>