<div style="padding:10px; border:15px solid #f2f2f2">
	<p>Hello Team,</p>
	@if(isset($data['fromRecos']) && $data['fromRecos'] == 1)
		<p>A customer has uploaded a custom menu from the recommendations page. Please go through the below details.</p>
	@else
		<p>Congrats, a new custom ticket has been raised. Please go through the below details.</p>
	@endif
	<div style="margin-top:20px">
		<table style="border-collapse: collapse;" border="1">
			<tr>
				<td style="padding:8px">Name</td>
				<td style="padding:8px"><a href="{{ $data['link'] }}">{{ $data['name'] }}</a></td>
			</tr>
			<tr>
				<td style="padding:8px">Email</td>
				<td style="padding: 8px">{{ $data['email'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Phone</td>
				<td style="padding: 8px">{{ $data['phone'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Party Date</td>
				<td style="padding: 8px">{{ $data['partyDate'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Party Location</td>
				<td style="padding: 8px">{{ $data['location'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Menu Type</td>
				@if( $data['foodType'] == 1)
					<td style="padding: 8px">Veg</td>
				@else
					<td style="padding: 8px">Non-Veg</td>
				@endif
			</tr>
			<tr>
				<td style="padding:8px">Budget per person</td>
				<td style="padding: 8px">{{ $data['budgetPerPerson'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Food Service Type</td>
				<td style="padding: 8px">{{ $data['foodServiceType'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Your Menu</td>
				@if($data['menuText'])
					<td style="padding: 8px">{{ $data['menuText'] }}</td>
				@else
					<td style="padding: 8px">---</td>
				@endif
			</tr>
			<tr>
				<td style="padding:8px">Guests Count</td>
				<td style="padding: 8px">{{ $data['venueMinGuests'] }}</td>
			</tr>
		</table>
	</div>
	@if(isset($data['image']))
		<p>Please find the attachment of image uploaded by customer</p>
	@endif
	<div style="margin-top:10px">
		Best Regards,
		<div>Admin</div>
	</div>
</div>