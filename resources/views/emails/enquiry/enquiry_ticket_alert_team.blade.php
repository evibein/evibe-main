<p>Hello Team,</p>

<p>Congrats, a new ticket has been raised. Please check the details below:</p>

<table style="border-collapse: collapse;text-align: left;">
	<tr>
		<th colspan="2" style="border: 1px solid #dddddd; padding: 8px; text-align: center;">Ticket Details</th>
	</tr>
	@if(isset($data["customerPreviousStatus"]) && $data["customerPreviousStatus"] && $data['customerPreviousStatus']!="")
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Previous Status</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["customerPreviousStatus"] }}</td>
		</tr>
	@endif
	@if(isset($data["customer_preferred_slot"]) && $data["customer_preferred_slot"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Call Preference</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["customer_preferred_slot"] }}</td>
		</tr>
	@endif
	@if(isset($data["booking_likeliness_id"]) && $data["booking_likeliness_id"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Booking Likeliness</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ config("evibe.ticket.bookingLikeliness." . $data["booking_likeliness_id"], "Not Set") }}</td>
		</tr>
	@endif
	@if(isset($data["occasion"]) && $data["occasion"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Occasion</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["occasion"] }}</td>
		</tr>
	@endif
	@if(isset($data["city"]) && $data["city"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>City</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["city"] }}</td>
		</tr>
	@endif
	@if(isset($data["productType"]) && $data["productType"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Type</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["productType"] }}</td>
		</tr>
	@endif
	@if(isset($data["productCode"]) && $data["productCode"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Code</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["productCode"] }}</td>
		</tr>
	@endif
	@if(isset($data["productName"]) && $data["productName"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Product Name</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["productName"] }}</td>
		</tr>
	@endif
	<tr>
		<td style="border: 1px solid #dddddd; padding: 8px;"><b>Party Date</b></td>
		<td style="border: 1px solid #dddddd; padding: 8px;">{{ date("d/m/y", $data['event_date']) }}</td>
	</tr>
	<tr>
		<td style="border: 1px solid #dddddd; padding: 8px;"><b>Name</b></td>
		<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["name"] }}</td>
	</tr>
	<tr>
		<td style="border: 1px solid #dddddd; padding: 8px;"><b>Email</b></td>
		<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["email"] }}</td>
	</tr>
	<tr>
		<td style="border: 1px solid #dddddd; padding: 8px;"><b>Phone</b></td>
		<td style="border: 1px solid #dddddd; padding: 8px;">
			@if(isset($data['calling_code']) && $data['calling_code'])
				<span class="mar-r-2">{{ $data['calling_code'] }}</span>
			@endif
			<span>{{ $data["phone"] }}</span>
		</td>
	</tr>
	@if(isset($data["party_location"]) && $data["party_location"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Party Location</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["party_location"] }}</td>
		</tr>
	@endif
	@if(isset($data["guests_count"]) && $data["guests_count"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Guests Count</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["guests_count"] }}</td>
		</tr>
	@endif
	@if(isset($data["comments"]) && $data["comments"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Requirements</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["comments"] }}</td>
		</tr>
	@endif
	@if(isset($data["source"]) && $data["source"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Enquiry Source</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["source"] }}</td>
		</tr>
	@endif
</table>

@if($data['type_ticket_id'] == config('evibe.ticket.type.party_bag'))
	<p></p>
	<table style="border-collapse: collapse;text-align: left;">
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;">Category</td>
			<td style="border: 1px solid #dddddd; padding: 8px;">Name</td>
		</tr>
		@foreach ($data['partyBagData'] as $mappingKey => $mappingValues)
			@for($count = 0 ; $count < count($mappingValues); $count++)
				<tr>
					<td style="border: 1px solid #dddddd; padding: 8px;">{{ $mappingKey }}</td>
					<td style="border: 1px solid #dddddd; padding: 8px;">{{ $mappingValues[$count]['title'] }}</td>
				</tr>
			@endfor
		@endforeach
	</table>
@endif

<p>
	Let's make their party Evibe special! <br>
</p>

