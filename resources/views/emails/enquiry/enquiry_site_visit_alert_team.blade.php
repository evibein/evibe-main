<p>Hello Team,</p>

<p>Congrats, a new site visit enquiry has been raised. Please check the details below:</p>

<table style="border-collapse: collapse;text-align: left;">
	<tr>
		<th colspan="2" style="border: 1px solid #dddddd; padding: 8px; text-align: center;">Ticket Details</th>
	</tr>
	<tr>
		<td style="border: 1px solid #dddddd; padding: 8px;"><b>Event</b></td>
		<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['eventName'] }} for {{ $data['eventFor'] }}</td>
	</tr>
	<tr>
		<td style="border: 1px solid #dddddd; padding: 8px;"><b>Event Date</b></td>
		<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['eventDate'] }}</td>
	</tr>
	<tr>
		<td style="border: 1px solid #dddddd; padding: 8px;"><b>Site Visit Date</b></td>
		<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['visitDate'] }} [{{ $data['visitSlot'] }}]</td>
	</tr>
	<tr>
		<td style="border: 1px solid #dddddd; padding: 8px;"><b>Booking Likeliness</b></td>
		<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['bookLikeliness'] }}</td>
	</tr>
	<tr>
		<td style="border: 1px solid #dddddd; padding: 8px;"><b>Name</b></td>
		<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["name"] }}</td>
	</tr>
	<tr>
		<td style="border: 1px solid #dddddd; padding: 8px;"><b>Email</b></td>
		<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["email"] }}</td>
	</tr>
	<tr>
		<td style="border: 1px solid #dddddd; padding: 8px;"><b>Phone</b></td>
		<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["phone"] }}</td>
	</tr>
	<tr>
		<td style="border: 1px solid #dddddd; padding: 8px;"><b>City</b></td>
		<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["cityName"] }}</td>
	</tr>
</table>

<p>
	Let's make their party Evibe special! <br>
</p>

