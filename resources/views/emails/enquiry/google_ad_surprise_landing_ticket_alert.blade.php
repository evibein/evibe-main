<p>Hello Team,</p>

<p>A ticket has been raised from {{ $data["source"] }} - surprises landing page. Kindly go through the details.</p>

<table style="border-collapse: collapse;text-align: left;">
	<tr>
		<th colspan="2" style="border: 1px solid #dddddd; padding: 8px; text-align: center;">Enquiry Details</th>
	</tr>
	@if(isset($data["name"]) && $data["name"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Name</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["name"] }}</td>
		</tr>
	@endif
	@if(isset($data["email"]) && $data["email"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Mail</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["email"] }}</td>
		</tr>
	@endif
	@if(isset($data["phone"]) && $data["phone"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Phone</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["phone"] }}</td>
		</tr>
	@endif
	@if(isset($data["event_date"]) && $data["event_date"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Party Date</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["event_date"] }}</td>
		</tr>
	@endif
	@if(isset($data["cityName"]) && $data["cityName"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>City</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["cityName"] }}</td>
		</tr>
	@endif
	@if(isset($data["eventName"]) && $data["eventName"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Occasion</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["eventName"] }}</td>
		</tr>
	@endif
	@if(isset($data["sourceUrl"]) && $data["sourceUrl"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Enquiry Raised From</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["sourceUrl"] }}</td>
		</tr>
	@endif
	@if(isset($data["referralCode"]) && $data["referralCode"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Coupon Code</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["referralCode"] }}</td>
		</tr>
		@if(isset($data["discountAmount"]) && $data["discountAmount"])
			<tr>
				<td style="border: 1px solid #dddddd; padding: 8px;"><b>Discount Amount</b></td>
				<td style="border: 1px solid #dddddd; padding: 8px;">Rs. {{ $data["discountAmount"] }} @if(isset($data["minOrder"]) && $data["minOrder"]) (For orders above Rs. {{ $data["minOrder"] }}) @endif</td>
			</tr>
		@endif
	@endif
</table>

<p><u>NOTE:</u> In case customer didn't receive any coupon code, Kindly share the above mentioned coupon code with them.
</p>

<p>
	regards,<br>
	Admin
</p>