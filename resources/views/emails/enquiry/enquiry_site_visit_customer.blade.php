<p>Namaste {{ $data['name'] }},</p>
<p>
	This is to confirm that we've have received your booking for site visit on {{ $data["visitDate"] }} between {{ $data["visitSlot"] }}.
</p>
<p>
	Here is the address you have given for the site visit: <br>
	{{ $data["addressLine1"] }},<br>
	{{ $data["addressLine2"] }},<br>
	{{ $data["location"] }},<br>
	{{ $data["zipCode"] }}.<br>
	Phone: {{ $data['phone'] }}
</p>
<p>
	Our team will get in touch with you shortly. Should you have any queries, kindly reply to this email, we will respond to you ASAP.
</p>
<div style="margin-top: 15px;">
	<div style="font-weight: bold; border-bottom: 1px solid #999999; margin-bottom: 10px; display: inline-block;">Next Steps</div>
	<div style="padding-left: 25px">
		1. You will receive a site visit appointment confirmation status within 1 business day.<br>
		2. Get all your questions clarified from Evibe wedding expert at your place.<br>
		3. Get a pre-negotiated price quote within 1 business day after site visit. No obligation to book.<br>
		4. If you are satisfied, book the services with 50% advance payment and get a detailed booking receipt.<br>
	</div>
</div>
<p>
	Look forward to being a part of your wedding celebrations.
</p>
<p>
	Cheers,<br>
	Team Evibe.in<br>
	<a href="http://youtu.be/6lwEXs51qJw" target="_blank" rel="noopener">Why customers choose Evibe.in?</a>
</p>