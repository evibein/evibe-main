<p>Hello Team,</p>

<p>A ticket has been raised from the google ad-words landing page. Kindly go through the details.</p>

<table style="border-collapse: collapse;text-align: left;">
	<tr>
		<th colspan="2" style="border: 1px solid #dddddd; padding: 8px; text-align: center;">Enquiry Details</th>
	</tr>
	@if(isset($data["name"]) && $data["name"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Name</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["name"] }}</td>
		</tr>
	@endif
	@if(isset($data["email"]) && $data["email"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Mail</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["email"] }}</td>
		</tr>
	@endif
	@if(isset($data["phone"]) && $data["phone"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Phone</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["phone"] }}</td>
		</tr>
	@endif
	@if(isset($data["event_date"]) && $data["event_date"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Party Date</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["event_date"] }}</td>
		</tr>
	@endif
	@if(isset($data["cityName"]) && $data["cityName"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>City</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["cityName"] }}</td>
		</tr>
	@endif
	@if(isset($data["eventName"]) && $data["eventName"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Occasion</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["eventName"] }}</td>
		</tr>
	@endif
</table>

<p>
	regards,<br>
	Admin
</p>

