<div style="padding:10px; border:15px solid #f2f2f2">
	<p>Dear {{ ucfirst($data['name']) }},</p>
	@if(isset($data['fromRecos']) && $data['fromRecos'] == 1)
		<p>We've received your custom menu. We will review your menu and get back to you with the availability of the menu along with the best quote from our verified partners.</p>
	@else
		<p>We've received your custom menu. We will review your menu and if feasible, we will get back to you within 48 hours with the best quote from our verified partners.</p>
	@endif
	<p>Please find below details that you've submitted:</p>
	<div style="margin-top:10px">
		<table style="border-collapse: collapse;" border="1">
			<tr>
				<td style="padding:8px">Name</td>
				<td style="padding:8px">{{ $data['name'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Email</td>
				<td style="padding: 8px">{{ $data['email'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Phone</td>
				<td style="padding: 8px">{{ $data['phone'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Party Date</td>
				<td style="padding: 8px">{{ $data['partyDate'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Party Location</td>
				<td style="padding: 8px">{{ $data['location'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Menu Type</td>
				@if( $data['foodType'] == 1)
					<td style="padding: 8px">Veg</td>
				@else
					<td style="padding: 8px">Non-Veg</td>
				@endif
			</tr>
			<tr>
				<td style="padding:8px">Budget per person</td>
				<td style="padding: 8px">{{ $data['budgetPerPerson'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Food Service Type</td>
				<td style="padding: 8px">{{ $data['foodServiceType'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Your Menu</td>
				@if($data['menuText'])
					<td style="padding: 8px">{{ $data['menuText'] }}</td>
				@else
					<td style="padding: 8px">---</td>
				@endif
			</tr>
			<tr>
				<td style="padding:8px">Guests Count</td>
				<td style="padding: 8px">{{ $data['venueMinGuests'] }}</td>
			</tr>
		</table>
	</div>
	<p>
		Should you have any queries, please reply to this email (or) call us on {{ config('evibe.contact.company.phone') }} (10 AM - 7 PM).
	</p>
	<div style="margin-top:10px">
		Best Regards,
		<div>Team Evibe.in</div>
	</div>
</div>