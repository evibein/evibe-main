@if(isset($data['customerName']) && $data['customerName'])
	<p>Hello {{ $data['customerName'] }},</p>
@else
	<p>Hello Customer,</p>
@endif

<p>We are sorry for the inconvenience caused. We've received your complaint and a representative from our team will get back to you here with a resolution within 24 - 48 hrs. If there is anything urgent, kindly reply to this email.</p>

@if(isset($data['requestReason']) && $data['requestReason'])
<p>
	<u>Issue:</u> {{ $data['requestReason'] }}
</p>
@endif

<p>
	Yours Sincerely,<br>
	Team Evibe
</p>