<div style="padding:20px; margin:15px; display:inline-block; border:10px solid #f2f2f2">
	<p>Hi CRM Team,</p>
	<p>
		<b>{{ $data['ticket']['name'] }}</b> did not like our recommendations. Please talk to him/her based on the consideration mentioned and recommend best options.
	</p>
	<table border="1" style="border-collapse: collapse; margin:15px 0" width="100%">
		<tr>
			<td style="padding:10px"><b>Customer Name:</b></td>
			<td colspan="2" style="padding: 10px">
				<a href="{{ $data['ticketLink'] }}">{{ $data['ticket']['name'] }}</a>({{ $data['ticket']['phone'] }})
			</td>
		</tr>
		<tr>
			<td style="padding:10px"><b>Consideration</b></td>
			<td colspan="2" style="padding:10px">
				@if(isset($data['nextStep']) && $data['nextStep'])
					{{ $data['nextStep'] }}
				@else
					--
				@endif
			</td>
		</tr>
		<tr>
			<td style="padding:10px"><b>Message</b></td>
			<td colspan="2" style="padding:10px">
				@if(isset($data['message']) && $data['message'])
					{{ $data['message'] }}
				@else
					--
				@endif
			</td>
		</tr>
		<tr>
			<td style="padding:10px"><b>Old Ticket</b></td>
			<td colspan="2" style="padding:10px">
				@if(isset($data['oldTicketLink']) && $data['oldTicketLink'])
					<a href="{{ $data['oldTicketLink'] }}" target="_blank">Link to old ticket</a>
				@else
					--
				@endif
			</td>
		</tr>
		@if($data['selectedRecos'])
			<tr>
				<td colspan="3" style="padding: 10px">Customer has shortlisted the following few options in old ticket.</td>
			</tr>
			<tr>
				<td style="padding:8px" width="20%">
					<b>Type</b>
				</td>
				<td style="padding:8px" width="60%">
					<b>Name</b>
				</td>

				<td style="padding:8px" width="10%">
					<b>code</b>
				</td>
			</tr>
			@foreach ($data['selectedRecos'] as $selectedReco)
				<tr>
					<td style="padding: 8px;">{{ $selectedReco['type'] }}</td>
					<td style="padding: 8px;"><a href="{{ $selectedReco['url'] }}">{{ $selectedReco['name'] }}</a>
					</td>
					<td style="padding: 8px;">{{ $selectedReco['code'] }}</td>
				</tr>
			@endforeach
		@else
			<tr>
				<td colspan="3" style="padding: 10px">Customer did not shortlist any option.</td>
			</tr>
		@endif
	</table>
	<div>
		Regards,<br>
		Tech Team
	</div>
</div>