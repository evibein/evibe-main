<div style="padding:20px; margin:15px; border:10px solid #f2f2f2">
	<p>Hi CRM Team,</p>
	<p>
		{{ $data['ticket']['name'] }} {{$data['msg']}}
	</p>
	<table border="1" style="border-collapse: collapse; margin:15px 0" width="100%">
		<tr>
			<td style="padding:10px"><b>Customer Name</b></td>
			<td colspan="2" style="padding: 10px"><a
						href="{{ $data['ticketLink'] }}">{{ $data['ticket']['name'] }}</a> ({{ $data['ticket']['phone'] }})
			</td>
		</tr>
		<tr>
			<td style="padding:10px"><b>Handler</b></td>
			<td colspan="2" style="padding: 10px">
				<span>{{ $data['handler'] }}</span>
			</td>
		</tr>
		<tr>
			<td style="padding:10px"><b>Recommendations</b></td>
			<td colspan="2" style="padding: 10px">
				<a href="{{ $data['recoLink'] }}">Reco Link</a>
			</td>
		</tr>
		@if(isset($data['ticket']['last_reco_rating']))
			<tr>
				<td style="padding:10px"><b>Recommendation Rating</b></td>
				<td colspan="2" style="padding: 10px">{{ $data['ticket']['last_reco_rating'] }} (out of 5)</td>
			</tr>
		@endif
		@if(isset($data['ticket']['last_reco_rated_at']))
			<tr>
				<td style="padding:10px"><b>Last Rated at</b></td>
				<td colspan="2" style="padding: 10px">{{ date('d M Y, h:i A', strtotime($data['ticket']['last_reco_rated_at'])) }}</td>
			</tr>
		@endif
		@if(isset($data['ticket']['last_reco_skipped_at']))
			<tr>
				<td style="padding:10px"><b>Last Skipped at</b></td>
				<td colspan="2" style="padding: 10px">{{ date('d M Y, h:i A', strtotime($data['ticket']['last_reco_skipped_at'])) }}</td>
			</tr>
		@endif
	</table>
	<div>
		Regards,<br>
		Tech Team
	</div>
</div>