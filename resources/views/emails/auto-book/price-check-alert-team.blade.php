<p>Hello Team,</p>

<p style="display: none;">A customer has done price check for the below product. A ticket has been created with the customer's details. Please check the details below to followup:</p>
<p>A customer has initiated an auto booking. If he/she doesn't proceed with the booking in 1hr, kindly call the customer an do the needful.</p>

<table style="border-collapse: collapse;text-align: left;">
	<tr>
		<th colspan="2" style="border: 1px solid #dddddd; padding: 8px; text-align: center;">Ticket Details</th>
	</tr>
	@if(isset($data["occasion"]) && $data["occasion"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Occasion</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["occasion"] }}</td>
		</tr>
	@endif
	@if(isset($data["city"]) && $data["city"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>City</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["city"] }}</td>
		</tr>
	@endif
	@if(isset($data["productType"]) && $data["productType"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Type</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["productType"] }}</td>
		</tr>
	@endif
	@if(isset($data["productCode"]) && $data["productCode"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Code</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["productCode"] }}</td>
		</tr>
	@endif
	@if(isset($data["productName"]) && $data["productName"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Product Name</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["productName"] }}</td>
		</tr>
	@endif
	@if(isset($data["event_date"]) && $data["event_date"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Party Date</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">
				{{ $data['event_date'] ? date("d M Y", $data['event_date']) : "--"}}
				@if(isset($data["unsurePartyDate"]) && $data["unsurePartyDate"])
					<div style="color: red; margin-top: 5px;">(The customer hasn't finalised the party date)</div>
				@endif
			</td>
		</tr>
	@endif
	@if(isset($data['name']) && $data['name'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Name</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["name"] }}</td>
		</tr>
	@endif
	@if(isset($data['email']) && $data['email'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Email</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["email"] }}</td>
		</tr>
	@endif
	@if(isset($data['phone']) && $data['phone'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Phone</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["phone"] }}</td>
		</tr>
	@endif
	@if(isset($data["party_location"]) && $data["party_location"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Party Location</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["party_location"] }}</td>
		</tr>
	@endif
	@if(isset($data["guests_count"]) && $data["guests_count"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Guests Count</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["guests_count"] }}</td>
		</tr>
	@endif
	@if(isset($data["comments"]) && $data["comments"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Requirements</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["comments"] }}</td>
		</tr>
	@endif
	@if(isset($data['baseBookingAmount']) && $data['baseBookingAmount'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Base Booking Amount</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">Rs. {{ $data["baseBookingAmount"] }}</td>
		</tr>
	@endif
	@if(isset($data['totalBookingAmount']) && $data['totalBookingAmount'])
		<tr style="display: none;">
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Total Booking Amount</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">Rs. {{ $data["totalBookingAmount"] }}</td>
		</tr>
	@endif
	@if(isset($data['partyPinCode']) && $data['partyPinCode'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Party PinCode</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['partyPinCode'] }}</td>
		</tr>
	@endif
	@if(isset($data['partnerPinCode']) && $data['partnerPinCode'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Partner PinCode</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['partnerPinCode'] }}</td>
		</tr>
	@endif
	@if(isset($data['feasibility']) && $data['feasibility'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Availability</b></td>
			@if($data['feasibility'] == 1)
				<td style="border: 1px solid #dddddd; padding: 8px;">Yes</td>
			@elseif($data['feasibility'] == 2)
				<td style="border: 1px solid #dddddd; padding: 8px;">Yes <span
							style="color: blue;">(Transportation charges applicable)</span></td>
			@else
				<td style="border: 1px solid #dddddd; padding: 8px; color: red;">No</td>
			@endif
		</tr>
	@endif
	@if(isset($data['transCharges']))
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Transportation Charges</b></td>
			@if($data['transCharges'] == 0)
				<td style="border: 1px solid #dddddd; padding: 8px; color: green">Free</td>
			@else
				<td style="border: 1px solid #dddddd; padding: 8px;">Rs. {{ $data['transCharges'] }}</td>
			@endif
		</tr>
	@endif
	@if(isset($data['partyDist']) && $data['partyDist'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Party Distance</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['partyDist'] }} kms</td>
		</tr>
	@endif
	@if(isset($data['typeDevice']) && $data['typeDevice'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Device</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['typeDevice'] }}</td>
		</tr>
	@endif
	@if(isset($data['typeBrowser']) && $data['typeBrowser'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Browser</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['typeBrowser'] }}</td>
		</tr>
	@endif
	@if(isset($data['ticketId']) && $data['ticketId'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Ticket Id</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['ticketId'] }}</td>
		</tr>
	@endif
	@if(isset($data['dashLink']) && $data['dashLink'])
		<tr>
			<td colspan="2" style="border: 1px solid #dddddd; padding: 8px;"><a
						href="{{ $data['dashLink'] }}">Dash link to the ticket</a></td>
		</tr>
	@endif
</table>

<p>
	Let's make their party Evibe special! <br>
</p>

