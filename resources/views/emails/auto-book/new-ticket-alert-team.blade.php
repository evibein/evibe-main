<p>Hello Team,</p>

<p>A customer has visited an auto-book checkout page. If the auto-booking doesn't progress in 1hr, please call the customer. Below are the details:</p>

<table style="border-collapse: collapse;text-align: left;">
	<tr>
		<th colspan="2" style="border: 1px solid #dddddd; padding: 8px; text-align: center;">Ticket Details</th>
	</tr>
	@if(isset($data['name']) && $data['name'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Name</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['name'] }}</td>
		</tr>
	@endif
	@if(isset($data['phone']) && $data['phone'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Mobile</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['phone'] }}</td>
		</tr>
	@endif
	@if(isset($data['partyDate']) && $data['partyDate'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Party Date</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['partyDate'] }}</td>
		</tr>
	@endif
	@if(isset($data['email']) && $data['email'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Email</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['email'] }}</td>
		</tr>
	@endif
	@if(isset($data['bookingId']) && $data['bookingId'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Booking Id</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['bookingId'] }}</td>
		</tr>
	@endif
	@if(isset($data['bookingInfo']) && $data['bookingInfo'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Booking Info</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{!! $data['bookingInfo'] !!}</td>
		</tr>
	@endif
	@if(isset($data['estimatedTransportCharges']) && $data['estimatedTransportCharges'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Estimated Transport Charges</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">Rs. {{ $data['estimatedTransportCharges'] }}</td>
		</tr>
	@endif
	@if(isset($data['typeTicket']) && $data['typeTicket'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Booking Type</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['typeTicket'] }}</td>
		</tr>
	@endif
	@if(isset($data['city']) && $data['city'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>City</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['city'] }}</td>
		</tr>
	@endif
	@if(isset($data['occasion']) && $data['occasion'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Occasion</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['occasion'] }}</td>
		</tr>
	@endif
	@if(isset($data['ticketId']) && $data['ticketId'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Ticket Id</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['ticketId'] }}</td>
		</tr>
	@endif
</table>

<p>
	Best Regards,<br>
	Admin
</p>