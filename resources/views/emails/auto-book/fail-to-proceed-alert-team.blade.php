<p>Hello Team,</p>

<p>A customer has checked the availability of below product. A ticket has been created with the customer's mobile number. Please check the details below to followup. Also
	<b>ask the reason</b> for drop off, if he/she doesn't proceed to book.
</p>

<table style="border-collapse: collapse;text-align: left;">
	<tr>
		<th colspan="2" style="border: 1px solid #dddddd; padding: 8px; text-align: center;">Available Details</th>
	</tr>
	@if(isset($data['phone']) && $data['phone'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Phone</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['phone'] }}</td>
		</tr>
	@endif
	@if(isset($data['optionName']) && $data['optionName'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Option</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['optionName'] }}</td>
		</tr>
	@endif
	@if(isset($data['optionCode']) && $data['optionCode'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Option Code</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['optionCode'] }}</td>
		</tr>
	@endif
	@if(isset($data['optionType']) && $data['optionType'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Option Type</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['optionType'] }}</td>
		</tr>
	@endif
	@if(isset($data['optionEvent']) && $data['optionEvent'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Occasion</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['optionEvent'] }}</td>
		</tr>
	@endif
	@if(isset($data['optionCity']) && $data['optionCity'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>City</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['optionCity'] }}</td>
		</tr>
	@endif
	@if(isset($data['partyPinCode']) && $data['partyPinCode'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Party PinCode</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['partyPinCode'] }}</td>
		</tr>
	@endif
	@if(isset($data['partnerPinCode']) && $data['partnerPinCode'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Partner PinCode</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['partnerPinCode'] }}</td>
		</tr>
	@endif
	@if(isset($data['feasibility']) && $data['feasibility'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Availability</b></td>
			@if($data['feasibility'] == 1)
				<td style="border: 1px solid #dddddd; padding: 8px;">Yes</td>
			@elseif($data['feasibility'] == 2)
				<td style="border: 1px solid #dddddd; padding: 8px;">Yes <span
							style="color: blue;">(Transportation charges applicable)</span></td>
			@else
				<td style="border: 1px solid #dddddd; padding: 8px; color: red;">No</td>
			@endif
		</tr>
	@endif
	@if(isset($data['transCharges']))
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Transportation Charges</b></td>
			@if($data['transCharges'] == 0)
				<td style="border: 1px solid #dddddd; padding: 8px; color: green">Free</td>
			@else
				<td style="border: 1px solid #dddddd; padding: 8px;">Rs. {{ $data['transCharges'] }}</td>
			@endif
		</tr>
	@endif
	@if(isset($data['partyDist']) && $data['partyDist'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Party Distance</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['partyDist'] }} kms</td>
		</tr>
	@endif
	@if(isset($data['typeDevice']) && $data['typeDevice'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Device</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['typeDevice'] }}</td>
		</tr>
	@endif
	@if(isset($data['typeBrowser']) && $data['typeBrowser'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Browser</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['typeBrowser'] }}</td>
		</tr>
	@endif
	@if(isset($data['ticketId']) && $data['ticketId'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Ticket Id</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['ticketId'] }}</td>
		</tr>
	@endif
	@if(isset($data['dashLink']) && $data['dashLink'])
		<tr>
			<td colspan="2" style="border: 1px solid #dddddd; padding: 8px;"><a
						href="{!! $data['dashLink'] !!}">Dash link to the ticket</a></td>
		</tr>
	@endif
</table>

<p>
	Best Regards,<br>
	Admin
</p>