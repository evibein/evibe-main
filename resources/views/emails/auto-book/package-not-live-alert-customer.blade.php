<p>
	Namaste {{ $data['customerName'] }},
</p>
<p>
	We are sorry to let you know that your selected service/package is not available at the moment.
</p>
<p>
	What’s next?<br>
	Our party planning expert shall contact you shortly. However, if you need anything urgent, kindly reply to this email. We will respond to you ASAP.
</p>
<p>
	We look forward to being part of your celebration.
</p>
<p>
	Happy Partying,<br/>
	Team Evibe.in <br/>
	<a href="http://youtu.be/6lwEXs51qJw" target="_blank" rel="noopener">Why customers choose Evibe.in?</a>
</p>
<p style="padding-top: 10px;">
	{!! file_get_contents(base_path('resources/views/emails/enquiry/process.html')) !!}
</p>