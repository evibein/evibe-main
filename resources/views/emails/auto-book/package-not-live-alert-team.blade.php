<div style="padding:20px; margin:15px; border:10px solid #f2f2f2">
	<p>Hello Team,</p>
	<p>
		<b>Customer</b> has checked for a {{$data['productType']}}. Please Check for details below.
	</p>

	<table border="1" style="border-collapse: collapse; margin:15px 0" width="100%">
		@if(isset($data['customerName']) && $data['customerName'])
			<tr>
				<td style="padding:10px"><b>Customer Name:</b></td>
				<td colspan="2" style="padding: 10px">
					{{ $data['customerName']}}
				</td>
			</tr>
		@endif
		@if(isset($data['customerEmail']) && $data['customerEmail'])
			<tr>
				<td style="padding:10px"><b>Customer Email:</b></td>
				<td colspan="2" style="padding: 10px">
					{{ $data['customerEmail']}}
				</td>
			</tr>
		@endif
		<tr>
			<td style="padding:10px"><b>Customer Contact:</b></td>
			<td colspan="2" style="padding: 10px">
				{{ $data['customerMobile']}}
			</td>
		</tr>
		<tr>
			<td style="padding:10px"><b>Party Date:</b></td>
			<td colspan="2" style="padding: 10px">
				{{ $data['partyDate']}}
			</td>
		</tr>
		<tr>
			<td style="padding:10px"><b>Link</b></td>
			<td colspan="2" style="padding:10px">
				@if(isset($data['pageLink']))
					<a href="{{$data['pageLink']}}">{{$data['productType']}} URL </a> <a>({{$data['pageLink']}})</a>
				@endif
			</td>
		</tr>
		<tr>
			<td style="padding:10px"><b>Status:</b></td>
			<td colspan="2" style="padding: 10px" class="error-message">
				Not Live
			</td>
		</tr>

	</table>
	<div>
		Regards,<br>
		Tech Team
	</div>
</div>