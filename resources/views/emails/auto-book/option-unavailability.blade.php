<p>Hello Team,</p>

<p>A customer tried to initiate Auto Booking for an option. But unfortunately, the option has been booked for that party date time. Below are the required details.
	Kindly call the customer and try to provide another option or timing.
</p>

<table style="border-collapse: collapse;text-align: left;">
	<tr>
		<th colspan="2" style="border: 1px solid #dddddd; padding: 8px; text-align: center;">Available Details</th>
	</tr>
	@if(isset($data['phone']) && $data['phone'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Phone</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['phone'] }}</td>
		</tr>
	@endif
	@if(isset($data['optionName']) && $data['optionName'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Option</b></td>
			@if(isset($data['optionUrl']) && $data['optionUrl'])
				<td style="border: 1px solid #dddddd; padding: 8px;">
					<a href="{{ $data['optionUrl'] }}" target="_blank">{{ $data['optionName'] }}</a>
				</td>
			@else
				<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['optionName'] }}</td>
			@endif
		</tr>
	@endif
	@if(isset($data['optionCode']) && $data['optionCode'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Option Code</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['optionCode'] }}</td>
		</tr>
	@endif
	@if(isset($data['optionType']) && $data['optionType'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Option Type</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['optionType'] }}</td>
		</tr>
	@endif
	@if(isset($data['partyDate']) && $data['partyDate'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Party Date</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['partyDate'] }}</td>
		</tr>
	@endif
	@if(isset($data['partyTime']) && $data['partyTime'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Party Time</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['partyTime'] }}</td>
		</tr>
	@endif
	@if(isset($data['optionEvent']) && $data['optionEvent'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Occasion</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['optionEvent'] }}</td>
		</tr>
	@endif
	@if(isset($data['optionCity']) && $data['optionCity'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>City</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['optionCity'] }}</td>
		</tr>
	@endif
</table>

<p>
	Best Regards,<br>
	Admin
</p>