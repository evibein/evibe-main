<div style="background-color:#F5F5f5;padding:10px 20px;width:700px;font-size:14px;line-height:22px;">
	<div>
		<div style="padding:20px;">
			<div style='float:left;'>
				<div>
					<a href='http://evibe.in'><img src='http://gallery.evibe.in/img/logo/logo_evibe.png' alt='evibe.in'></a>
				</div>
			</div>
			<div style='float:right;'>
				<div><i>Date:</i> {{ $data['date'] }}</div>
				<div style="padding-top:4px;"><i>Phone:</i> {{ $data['evibePhone'] }}</div>
			</div>
			<div style='clear:both;'></div>
		</div>
		<div style="padding:4px; background:#828282; text-align:center;">
			<h2 style="color:#FFFFFF">
				Are the following booking(s) Available For {{ $data['partyDateTime'] }} ?
			</h2>
		</div>
		<div style="font-size:14px;line-height:22px; padding: 20px 30px 20px; background-color:#FFFFFF;">
			<div style="margin-top:30px; text-align:center; ">
				<div style="font-size:16px;margin:15px 0;">Dear {{ $data['person'] }},</div>
				<div style="margin:15px 0;"> A customer is interested in the following services and has already paid amount of Rs.
					{{ $data['totalAdvancePaidStr'] }}. Please check the complete order details below.
					<div style="margin:15px auto;font-size:16px;">
						Click on <b>“Accept”</b> button to accept & confirm the order (or) click
						<b>“Reject”</b> button if it is not available.
					</div>
					<div style="margin:15px 0;">
						<ul style="list-style:none; padding:10px 0;">
							<li style="display:inline;">
								<a href="{{ $data['vendorAskUrl'] }}#confirm" style="color:#FFFFFF;text-transform: uppercase; text-decoration: none;padding: 10px 20px; background: #149814; ">Accept</a>
							</li>
							<li style="display:inline; margin-left:20px;">
								<a href="{{ $data['vendorAskUrl'] }}#cancel" style="color:#FFFFFF;text-transform: uppercase; text-decoration: none;padding: 10px 20px; background: #CC3F3F; ">Reject</a>
							</li>
						</ul>
					</div>
				</div>
				<div style="margin-top: 30px;">
					<table style="border-collapse: collapse; width:100%" border="1">
						<tr>
							<td style="padding:10px; width: 100%;vertical-align: top">
								<div style="text-align: left">
									<div style="text-transform: uppercase; color: #ED3E72"><b>Order Details</b></div>
									<div><strong>Customer Name:</strong> {{ $data['customer']['name'] }}</div>
									@if(isset($data['additional']['eventName']) && $data['additional']['eventName'])
										<div><strong>Occasion:</strong> {{ $data['additional']['eventName'] }}</div>
									@endif
									<div><strong>Party Date Time:</strong> {{ $data['partyDateTime'] }}</div>
									<div>
										<strong>Customer Special Notes* (Needs approval if not already discussed):</strong>
										@if($data['additional']['specialNotes'])
											{!! $data['additional']['specialNotes'] !!}
										@else
											--
										@endif
									</div>
									@if(isset($data['bookings']) && $data['bookings'])
										<div style="margin-top: 10px;">
											<table style="border-collapse: collapse; width:100%" border="1">
												@foreach($data['bookings'] as $booking)
													<tr>
														<td style="padding:10px; width: 60%;vertical-align: top">
															<div style="margin-bottom: 10px;">
																<span style="padding:3px 10px;background: #efefef;border-radius:2px;color:#333">{!! $booking['typeBookingDetails'] !!}</span>
															</div>
															<div>
																<strong>Booking Details:</strong> {!! $booking['bookingInfo'] !!}
															</div>
															@if(isset($booking['checkoutFields']))
																@foreach($booking['checkoutFields'] as $key => $value)
																	<div><strong>{{ $key }}:</strong> {{ $value }}</div>
																@endforeach
															@endif
														</td>
														<td style="padding:10px; width: 40%;vertical-align: top">
															@if(isset($data['typeTicketAddOn']) && isset($booking['itemMapTypeId']) && ($booking['itemMapTypeId'] == $data['typeTicketAddOn']))
																<div>
																	<strong>Booking Units:</strong> {!! $booking['bookingUnits'] !!}
																</div>
															@else
																<div>
																	<strong>Package Price:</strong> Rs. {{ $booking['productPriceStr'] }}
																</div>
																@if(isset($booking['itemMapTypeId']) && isset($data['decorTypeId']) && ($booking['itemMapTypeId'] == $data['decorTypeId']))
																	<div><strong>Delivery Charge:</strong>
																		@if($booking['transportCharges'] > 0)
																			Rs. {{ $booking['transportChargesStr'] }}
																		@else
																			<span style='color:rgba(0, 169, 12, 0.83)'> Free </span>
																		@endif
																	</div>
																@endif
															@endif
															<div>
																<strong>Booking Amount:</strong> Rs. {{ $booking['bookingAmountStr'] }}
															</div>
															@if(isset($booking['itemMapTypeId']) && isset($data['venueDealsTypeId']) && ($booking['itemMapTypeId'] == $data['venueDealsTypeId']))
																<div>
																	<strong>Advance Paid:</strong> Rs. {{ $booking['tokenAmountStr'] }}
																</div>
															@else
																<div>
																	<strong>Advance Paid:</strong> Rs. {{ $booking['advanceAmountStr'] }}
																</div>
															@endif
															<div>
																<strong>Balance Amount:</strong> Rs. {{ $booking['balanceAmountStr'] }}
															</div>
														</td>
													</tr>
												@endforeach
											</table>
										</div>
										@if(count($data['bookings']) > 1)
											<div style="margin-top: 10px;">
												<div>
													<strong>Total Booking Amount:</strong> Rs. {{ $data['totalBookingAmountStr'] }}
												</div>
												<div>
													<strong>Total Advance Paid:</strong> Rs. {{ $data['totalAdvancePaidStr'] }}
												</div>
												<div>
													<strong>Total Balance Amount:</strong> Rs. {{ $data['totalBalanceAmountStr'] }}
												</div>
											</div>
										@endif
									@endif
								</div>
							</td>
						</tr>
						@if(isset($data['venuePartnerTypeId']) && ($data['partnerTypeId'] == $data['venuePartnerTypeId']))
						@else
							<tr>
								<td style="padding:10px; width: 100%;vertical-align: top">
									<div style="text-align: left">
										<div style="text-transform: uppercase; color: #ED3E72">
											<b>Party Venue Details</b>
										</div>
										<div><strong>Location:</strong> {{ $data['additional']['venueLocation'] }}</div>
										<div><strong>Full Address:</strong> {{ $data['additional']['venueAddress'] }}
										</div>
									</div>
								</td>
							</tr>
						@endif
						@if(isset($data['gallery']) && count($data['gallery']))
							<tr>
								<td style="padding:10px; width: 100%;vertical-align: top">
									<div style="text-align: left">
										<div style="text-transform: uppercase; color: #ED3E72">
											<b>Images provided by Customer</b>
										</div>
										@foreach($data['gallery'] as $imageLink)
											<div style="height: 60px; width: 90px; display: inline-block; margin-top: 5px; margin-right: 5px;">
												<img style="height: 100%; width: 100%;" src="{{ $imageLink }}">
											</div>
										@endforeach
									</div>
								</td>
							</tr>
						@endif
						<tr>
							<td style="padding:10px; width: 100%;vertical-align: top">
								<div style="text-align: left">
									<div style="text-transform: uppercase; color: #ED3E72">
										<b>Cancellation Policy</b>
									</div>
									<div style="margin-top: 10px;">
										<div>In case of party cancellation by the customer, you will receive your fee based on the following policies:</div>
										<div>
											<table style="margin-top: 10px; border: 1px solid #DDDDDD; border-spacing: 0; border-collapse: collapse;">
												<tr>
													<th style="border: 1px solid #DDDDDD; padding: 5px;">Customer Cancellation Time</th>
													<th style="border: 1px solid #DDDDDD; padding: 5px;">You Receive*</th>
												</tr>
												<tr>
													<td style="border: 1px solid #DDDDDD; padding: 5px;">0 - 24 hours before party</td>
													<td style="border: 1px solid #DDDDDD; padding: 5px;">70% of Advance Paid By Customer</td>
												</tr>
												<tr>
													<td style="border: 1px solid #DDDDDD; padding: 5px;">1 day- 3 days before party</td>
													<td style="border: 1px solid #DDDDDD; padding: 5px;">45% of Advance Paid By Cusotmer</td>
												</tr>
												<tr>
													<td style="border: 1px solid #DDDDDD; padding: 5px;">4 days - 10 days before party</td>
													<td style="border: 1px solid #DDDDDD; padding: 5px;">25% of Advance Paid By Customer</td>
												</tr>
												<tr>
													<td style="border: 1px solid #DDDDDD; padding: 5px;">11 days or above before party</td>
													<td style="border: 1px solid #DDDDDD; padding: 5px;">15% of Advance Paid By Customer</td>
												</tr>
											</table>
										</div>
										<div style="margin-top: 10px;">* Fee Percentage will be calculated on Total Advance Amount paid by the customer</div>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</div>
				<div style="text-align:left">
					<div style="margin:10px 0 10px;">
						<u>Note:</u> If there is any change in product details, please reply to this email / call {{ $data['bizHead'] }} at the earliest. We will update the same on our website.
					</div>
					<div style="margin:15px 0;">
						<div>Best Regards,</div>
						<div>Your wellwishers at <a href="{{ $data['evibeLink'] }}" style='color:#333'>Evibe.in</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div style="padding-top:10px;font-size:12px;color:#999">If you are receiving the message in Spam or Junk folder, please mark it as 'not spam' and add senders id to contact list or safe list.</div>