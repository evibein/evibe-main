<div style="background-color:#F5F5f5;padding:10px 20px;width:700px;font-size:14px;line-height:22px;">
	<div>
		<div style="padding:20px;">
			<div style='float:left;'>
				<div>
					<a href='http://evibe.in'><img src='http://gallery.evibe.in/img/logo/logo_evibe.png' alt='evibe.in'></a>
				</div>
			</div>
			<div style='float:right;'>
				<div><i>Date:</i> {{ $data['date'] }}</div>
				<div style="padding-top:4px;"><i>Phone:</i> {{ $data['evibePhone'] }}</div>
			</div>
			<div style='clear:both;'></div>
		</div>
		<div style="padding:4px; background:#008000; text-align:center;">
			<h2 style="color:#FFFFFF">
				<img src='http://gallery.evibe.in/main/img/icons/check.png' height="20px" width="20px" alt='Success'> Payment Success
			</h2>
		</div>
		<div style="font-size:14px;line-height:22px; padding: 5px 30px 20px; background-color:#FFFFFF;">
			<div style="margin-top:30px; text-align:center; ">
				<div style="font-size:16px;margin:15px 0;">Dear {{ $data['customer']['name'] }},</div>
				<div style="margin:15px 0;">We have received your payment of
					<b>&#8377; {{ $data['totalAdvancePaidStr'] }}</b> for party on {{ $data['additional']['partyDate'] }}.
				</div>
				<div style="margin:15px 0;">
					Please check below for complete details & FAQs.
				</div>
				<h3 style="font-weight:bold;color:#444444;text-decoration:underline;margin-top:20px;margin-bottom:5px;">Important:</h3>
				<div style="margin:15px 0;">
					This is
					<span style="background: #F4FF97;">NOT BOOKING CONFIRMATION</span>. We will check the availability and confirm your booking/s within next 4 business hours.
				</div>
			</div>
			<div style='padding: 25px 0 25px;text-align:center;'>
				@if(isset($data['needProof']) && $data['needProof'] && isset($data['proofUpload']) && (!$data['proofUpload']))
					<h3 style="font-weight:bold;color:#444444;text-decoration:underline;margin-bottom:5px;">Action Needed:</h3>
					<div style="margin:15px 0;">
						A valid Photo ID proof needs to be submitted as early as possible.
					</div>
				@endif
				<a href='{{ route("track.orders") . "?ref=email&id=" . $data["ticketId"] . "&token=" . \Illuminate\Support\Facades\Hash::make($data["ticketId"] . "EVBTMO") }}' style='font-size:20px;text-decoration:none;background-color:#4584ee;color:#ffffff;padding:8px 20px;border-radius:4px;' target='_blank'>
					@if(isset($data['needProof']) && $data['needProof'] && isset($data['proofUpload']) && (!$data['proofUpload']))
						Upload ID Proof
					@else
						View &amp; Manage Order
					@endif
				</a>
			</div>
			<div style="margin-top:15px;">
				In case your service is not available for your party date, we will refund the total amount you paid immediately (or) help you locate closest
				alternate options based on your choice. Please check FAQ section below.
			</div>
			<div style="margin:15px 0;">
				<div>Best Regards,</div>
				<div>Team Evibe</div>
			</div>
		</div>
	</div>

	@if(isset($data['addOns']) && count($data['addOns']))
		<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
			<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Add-ons for your party</div>
			<a href="{{ route("track.orders") . "?ref=email&id=" . $data["ticketId"] . "&token=" . \Illuminate\Support\Facades\Hash::make($data["ticketId"] . "EVBTMO") }}">
				<table style="margin-top: 10px;">
					<tr>
					@foreach($data['addOns'] as $key => $addOn)
						<td style="width:30%; display: inline-block; vertical-align: top; @if($key < 2) padding-right: 4%; @endif">
							<div style="border: 1px solid #EFEFEF; padding: 10px; overflow: hidden;">
									<div style="">
										<div style="background-color: #F9F9F9; height: 120px; width: 140px;">
											<img data-src="{{ $galleryUrl}}/addons/{{ $addOn->id }}/{{ $addOn->image_url }}" alt="{{ $addOn->name }} profile picture" style="height: 100%; width: 100%;">
										</div>
									</div>
									<div>
										<div>
											<div style="font-size: 16px; line-height: 20px; font-weight: 500; margin-top: 5px;">
												{{ ucwords($addOn->name) }}
											</div>
											@if($addOn->price_per_unit)
												<div style="margin-top:5px;">
													@if($addOn->price_worth_per_unit)
														<span style="font-size: 14px; color: #878787; text-decoration: line-through;">Rs. {{ $addOn->price_worth_per_unit }}</span>
													@endif
													<span style="color: #ED3E72; font-size: 16px; font-weight: 500; margin-left: 4px;">Rs. {{ $addOn->price_per_unit }}</span>
													@if($addOn->price_worth_per_unit && ($addOn->price_per_unit < $addOn->price_worth_per_unit))
														<span style="font-size: 12px; font-weight: 500; margin-left: 4px;">(RS. {{ $addOn->price_worth_per_unit - $addOn->price_per_unit }} OFF)</span>
													@endif
												</div>
											@endif
											<div style="margin-top: 10px; margin-bottom: 5px;">
												<div style="padding: 0; text-align: left; width: 100%;" class="add-on-cta-list add-on-id-{{ $addOn->id }}" data-id="{{ $addOn->id }}" data-price="{{ $addOn->price_per_unit }}" data-price-worth="{{ $addOn->price_worth_per_unit }}" data-max-units="{{ $addOn->max_units }}" data-count-units="0" data-name="{{ $addOn->name }}">
													<div style="font-size: 14px; line-height: 20px; padding: 2px 12px 2px 10px; border-radius: 10px; display: inline-block; color: #30AC15; border: 1px solid #30AC15;">+ Add</div>
												</div>
											</div>
										</div>
									</div>
									@if($addOn->info)
										<div style="font-size: 13px; line-height: 18px; margin-top: 10px;">
											{!! $addOn->info !!}
										</div>
									@endif
							</div>
						</td>
						@if($key == 2) @break @endif
					@endforeach
					</tr>
				</table>
			</a>
		</div>
	@endif

	<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
		<div>
			<b>Customer Name: </b><span> {{ $data['customer']['name'] }} </span>
		</div>
		<div style="margin-top:15px;">
			<b>Phone Number: </b><span> {{ $data['customer']['phone'] }} </span>
		</div>
		<div style="margin-top:15px;">
			<b>Email Id: </b><span> {{ $data['customer']['email'] }} </span>
		</div>
		@if(isset($data['additional']['eventName']) && $data['additional']['eventName'])
			<div style="margin-top:15px;">
				<b>Occasion: </b><span> {{ $data['additional']['eventName'] }} </span>
			</div>
		@endif
		<div style="margin-top:15px;">
			<b>Party Date: </b><span> {{ $data['additional']['partyDate'] }} </span>
		</div>
		<div style="margin-top:15px;">
			<b>Party Time: </b><span> {{ $data['additional']['partyStartTime'] }} </span>
		</div>
	</div>

	<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
		<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Order Details</div>
		<div style="margin-top: 15px;">
			@foreach($data['bookings']['list'] as $booking)
				<div style="border: 1px solid #F5F5F5; padding: 10px; margin-bottom: 15px;">
					<div>
						<span style="padding:3px 10px;background: #efefef;border-radius:2px;color:#333">{!! $booking['typeBookingDetails'] !!}</span>
					</div>
					<ul style="display:table;table-layout:fixed;padding:0;margin:10px 0 0 0;list-style:none;width:100%;">
						<li style="display:table-cell;">
							<div><b>Booking ID</b></div>
							<div>{{ $booking['bookingId'] }}</div>
						</li>
						<li style="display:table-cell;">
							<div><b>Booking Amount</b></div>
							<div>&#8377; {{ $booking['bookingAmountStr'] }}</div>
						</li>
						@if(isset($data['typeTicketAddOn']) && isset($booking['itemMapTypeId']) && ($booking['itemMapTypeId'] == $data['typeTicketAddOn']))
							<li style="display:table-cell;">
								<div><b>Booking Units</b></div>
								<div>{!! $booking['bookingUnits'] !!}</div>
							</li>
						@else
							<li style="display:table-cell;">
								<div><b>Advance Paid</b></div>
								<div>&#8377; {{ $booking['advanceAmountStr'] }}</div>
							</li>
							<li style="display:table-cell;">
								<div><b>Balance Amount</b></div>
								<div>&#8377; {{ $booking['balanceAmountStr'] }}</div>
							</li>
						@endif
					</ul>
					@if(isset($booking['bookingInfo']) && $booking['bookingInfo'])
						<div style="margin-top:15px;">
							<div><b>Booking Details</b></div>
							<div>
								{!! $booking['bookingInfo'] !!}
							</div>
						</div>
					@endif
					@if(isset($booking['checkoutFields']))
						@foreach($booking['checkoutFields'] as $key => $value)
							<div style="margin-top:15px;">
								<b>{{ $key }}: </b><span> {{ $value }} </span>
							</div>
						@endforeach
					@endif
				</div>
			@endforeach
		</div>
		<div style="margin-top:15px; padding-bottom: 15px;">
			<b>Special Notes*: (Needs approval if not already discussed) </b>
			<span>
					@if($data['additional']['specialNotes'])
					{!! $data['additional']['specialNotes'] !!}
				@else
					--
				@endif
				</span>
		</div>
		<div style="text-align:center; background: #FFF6C4;padding:8px 15px; margin-top: 15px;">
			@if((isset($data['additional']['internetHandlingFee']) && ($data['additional']['internetHandlingFee'] > 0)) || (isset($data['couponDiscount']) && ($data['couponDiscount'] > 0)))
				<div>
					@if($data['additional']['internetHandlingFee'] > 0)
						<ul style="display:table;table-layout:fixed;padding: 0;margin: 5px 0 0 0;list-style:none;width:100%;/* border-bottom: 1px solid black; */">
							<li style="display:table-cell;text-align: left;">
								<div>Internet Handling Charges</div>
							</li>
							<li style="display:table-cell;text-align: right;">
								<div>&#8377; {{ $data['additional']['internetHandlingFee'] }}</div>
							</li>
						</ul>
					@endif
					@if($data['couponDiscount'] > 0)
						<ul style="display:table;table-layout:fixed;padding: 0;margin: 5px 0 0 0;list-style:none;width:100%;/* border-bottom: 1px solid black; */">
							<li style="display:table-cell;text-align: left;">
								<div>Coupon Discount</div>
							</li>
							<li style="display:table-cell;text-align: right;">
								<div>&#8377; {{ $data['couponDiscount'] }}</div>
							</li>
						</ul>
					@endif
					<hr>
				</div>
			@endif
			<ul style="display:table;table-layout:fixed;padding:0;margin:0;list-style:none;width:100%">
				<li style="display:table-cell">
					<div>Total Booking Amount</div>
					<div style="text-align: center; font-size:18px">
						<b>&#8377; {{ $data['totalBookingAmountStr'] }}</b></div>
				</li>
				<li style="display:table-cell">
					<div>Total Advance Paid</div>
					<div style="text-align: center; font-size:18px">
						<b>&#8377; {{ $data['totalAdvancePaidStr'] }}</b></div>
				</li>
				<li style="display:table-cell">
					<div>Total Balance Amount</div>
					<div style="text-align: center; font-size:18px">
						<b>&#8377; {{ $data['totalBalanceAmountStr'] }}</b></div>
				</li>
			</ul>
		</div>
	</div>

	<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
		<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Party Venue Details</div>
		@if(isset($data['ticketHasVenueBooking']) && $data['ticketHasVenueBooking'])
			<div style="margin-top:15px;">
				<b>Area: </b>
				<div> {{ $data['additional']['area'] }} ({{ $data['additional']['cityName'] }})</div>
			</div>
		@else
			<div style="margin-top:15px;">
				<b>Venue Address: </b>
				<div> {{ $data['additional']['venueAddress'] }} </div>
			</div>
		@endif
		@if(isset($data['additional']['venueLandmark']) && $data['additional']['venueLandmark'])
			<div style="margin-top:15px;">
				<b>Landmark: </b>
				<div> {{ $data['additional']['venueLandmark'] }} </div>
			</div>
		@endif
	</div>

	@if(isset($data['gallery']) && count($data['gallery']))
		<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
			<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Uploaded Images</div>
			<div style="margin-top: 5px;">
				@foreach($data['gallery'] as $imageLink)
					<div style="height: 60px; width: 90px; display: inline-block; margin-top: 5px; margin-right: 5px;">
						<img style="height: 100%; width: 100%;" src="{{ $imageLink }}">
					</div>
				@endforeach
			</div>
		</div>
	@endif

	<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
		<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">FAQ's</div>
		<ul style="list-style: none; position: relative;">
			<li style="color: #EF3E75;margin-top: 20px;padding-bottom: 10px;">
				<span style="position: absolute;left: 0px;">Q.</span> When will I get booking confirmation?
			</li>
			<li style="color: #000000;">
				Within next 4 business hours, you will receive a booking confirmation email & SMS with venue & event coordinator
				details based on availability of your package. In case of non-availability, you will be notified with next
				steps.
			</li>
			<li style="color: #EF3E75;margin-top: 20px;padding-bottom: 10px;">
				<span style="position: absolute;left: 0;">Q.</span> Whom should I contact after booking confirmation?
			</li>
			<li style="color: #000000;">
				Your will be assigned an event coordinator who will make sure all arrangements are done as per
				your order details. Contact information of event coordinator will be shared in booking confirmation email.
			</li>
			<li style="color: #EF3E75;margin-top: 20px;padding-bottom: 10px;">
				<span style="position: absolute;left: 0px;">Q.</span>What if my package is not available?
			</li>
			<li style="color: #000000;">
				Incase of non-availability of your package, we will try to match something similar. If you are
				not satisfied, we will refund 100% of the advance amount immediately thereafter.
			</li>
			<li style="color: #EF3E75;margin-top: 20px;padding-bottom: 10px;">
				<span style="position: absolute;left: 0;">Q.</span>
				How many days does it take to refund?
			</li>
			<li style="color: #000000;">
				We will initiate the refund process as soon as we get a go ahead from you. However, for the money
				to actually credit to your card / account, it depends on your payment choice at the time of making advance
				payment. Rest assured, you should receive the cash within 7 - 10 Business days.
			</li>
		</ul>
	</div>

	@include('emails.auto-book.pay-success.util.cancellation-policy-customer')

	<div style="background-color:#ffffff;margin-top:30px;padding:20px;">
		<div style="color:#ef3e75;text-transform:uppercase;font-size:14px;font-weight:bold;">Terms of booking</div>
		<ul style="list-style-type:decimal;padding:0 15px 0 20px;font-size:13px;">
			<li>All the order details and amount agreed are for direct clients only. Third party transactions are strictly not allowed and is subjected to high penalty.</li>
			<li style="padding-top:5px">All the services booked are valid for party duration of 3 - 4 hours until and unless it is explicitly mentioned.</li>
			<li style="padding-top:5px">Remaining balance payment needs to done in cash only, after your event is completed.</li>
			<li style="padding-top:5px">All the decoration items are on hire basis only. Extra charges are applicable if you want to own it.</li>
			<li style="padding-top:5px">Booking does not include any furniture and fixture.</li>
			<li style="padding-top:5px">Anything not mentioned in order details will cost extra accordingly.</li>
			<li style="padding-top:5px">It is your responsibility to ensure sufficient time is available to execute all the order details mentioned.</li>
			<li style="padding-top:5px">It is your responsibility to ensure that all necessary permissions / copyrights and authorizations are in place.</li>
			<li style="padding-top:5px">It is your responsibility to take care of all your belongings including gifts. We cannot be held liable for any of your assets.</li>
			<li style="padding-top:5px">Although we use all reasonable safety precautions, we cannot be held liable for any casualties arising at any stage.</li>
			<li style="padding-top:5px">Although we have a policy for execution team to not consume any of the food items served at your party, it is your responsibility to ensure and instruct the same on your party date.</li>
			<li style="padding-top:5px">We will use all reasonable endeavors to meet the obligations in a prompt and efficient manner, however we will not accept responsibility for failure or delay caused by circumstances beyond its control.</li>
		</ul>
	</div>

</div>

<div style="padding-top:10px;font-size:12px;color:#999">If you are receiving the message in Spam or Junk folder, please mark it as 'not spam' and add senders id to contact list or safe list.</div>
