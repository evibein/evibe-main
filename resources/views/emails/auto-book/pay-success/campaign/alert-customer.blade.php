<div style="background-color:#F5F5f5;padding:10px 20px;width:700px;font-size:14px;line-height:22px;">
	<div>
		<div style="padding:20px;">
			<div style='float:left;'>
				<div>
					<a href='http://evibe.in'><img src='http://gallery.evibe.in/img/logo/logo_evibe.png' alt='evibe.in'></a>
				</div>
			</div>
			<div style='float:right;'>
				<div><i>Date:</i> {{ $data['date'] }}</div>
				<div style="padding-top:4px;"><i>Phone:</i> {{ $data['evibePhone'] }}</div>
			</div>
			<div style='clear:both;'></div>
		</div>
		<div style="padding:4px; background:#008000; text-align:center;">
			<h2 style="color:#FFFFFF">
				<img src='http://gallery.evibe.in/main/img/icons/check.png' height="20px" width="20px" alt='Success'> Booking Successful #{{ $data['booking']['bookingId'] }}
			</h2>
		</div>

		<div style="line-height:22px; padding: 20px 30px 20px; background-color:#FFFFFF;">
			<div style="margin-top:30px;text-align:center;">
				<p>Namaste {{ $data['customer']['name'] }},</p>
				<p>
					We are pleased to inform that your order of {!! $data['booking']['bookingInfo'] !!} is successful. Please check below for complete order details.
				</p>
				<p>
					If you have any queries, please feel free to contact us on {{ $data['evibePhone'] }} ({{ $data['startTime'] }} - {{ $data['endTime'] }}) or write to us at {{ $data['evibeEmail'] }}.
				</p>
			</div>
			<div style="margin-top:10px">
				<div>Best Regards,</div>
				<div>Team Evibe.in</div>
			</div>
		</div>
	</div>

	<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
		<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Order Details</div>
		<ul style="display:table;table-layout:fixed;padding:0;margin:10px 0 0 0;list-style:none;width:100%;">
			<li style="display:table-cell;">
				<div><b>Customer Name</b></div>
				<div>{{ $data['customer']['name'] }}</div>
			</li>
			<li style="display:table-cell;">
				<div><b>Package</b></div>
				<div>{!! $data['booking']['bookingInfo'] !!}</div>
			</li>
			<li style="display:table-cell;">
				<div><b>Booking Id</b></div>
				<div>#{{ $data['booking']['bookingId'] }}</div>
			</li>
		</ul>
	</div>

	<!-- removed delivery date and time -->
	<!-- removed provider information -->
	<!-- todo: extend to a base when other campaigns are included -->

	<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
		<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Delivery Details</div>
		<div style="margin-top:5px;">
			<b>Location: </b><span>{{ $data['additional']['venueLocation'] }}</span>
		</div>
		<div style="margin-top:5px;">
			<b>Full Address: </b><span>{{ $data['additional']['venueAddress'] }}</span>
		</div>
	</div>

	<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
		<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Booking Details</div>
		<div style="margin-top:5px;">
			<b>Decor Price: </b><span>Rs. {{ $data['baseAmt'] }}</span>
		</div>
		<div style="margin-top:5px;">
			<b>Delivery Charge: </b>
			<span>
				@if($data['delivery'] > 0)
					Rs. {{ $data['deliveryAmt'] }}
				@else
					<span style='color:rgba(0, 169, 12, 0.83)'> Free </span>
				@endif
			</span>
		</div>
		<div style="margin-top:5px;">
			<b>Total Order Amount: </b><span>Rs. {{ $data['bookAmt'] }}</span>
		</div>
		<div style="margin-top:5px;">
			<b>Advance Paid: </b><span>Rs. {{ $data['advPd'] }}</span>
		</div>
		<div style="margin-top:5px;">
			<b>Balance Amount: </b><span>Rs. {{ $data['balAmt'] }}</span>
		</div>
	</div>

	@include('emails.auto-book.pay-success.util.cancellation-policy-customer')

	<div style="background-color:#ffffff;margin-top:30px;padding:20px;">
		<div style="color:#ef3e75;text-transform:uppercase;font-size:14px;font-weight:bold;">Terms of booking</div>
		<ul style="list-style-type:decimal;padding:0 15px 0 20px;font-size:13px;">
			<li>All the order details and amount agreed are for direct clients only. Third party transactions are strictly not allowed and is subjected to high penalty.</li>
			<li style="padding-top:5px">All the products are delivered with in the mentioned time span until and unless the circumstances are beyond our control.</li>
			<li style="padding-top:5px">Delivery is strictly limited to whatever is mentioned in the order.</li>
			<li style="padding-top:5px">It is your responsibility to ensure sufficient time is available for the delivery of the product, to be delivered on time.</li>
			<li style="padding-top:5px">It is your responsibility to ensure that all necessary permissions / copyrights and authorizations are in place.</li>
			<li style="padding-top:5px">We will use all reasonable endeavors to meet the obligations in a prompt and efficient manner, however we will not accept responsibility for failure or delay caused by circumstances beyond its control.</li>
		</ul>
	</div>

</div>

<div style="padding-top:10px;font-size:12px;color:#999">If you are receiving the message in Spam or Junk folder, please mark it as 'not spam' and add senders id to contact list or safe list.</div>