<div style="background-color:#F5F5f5;padding:10px 20px;width:700px;font-size:14px;line-height:22px;">
	<div>
		<div style="padding:20px;">
			<div style='float:left;'>
				<div>
					<a href='http://evibe.in'>
						<img src='http://gallery.evibe.in/img/logo/logo_evibe.png' alt='evibe.in'>
					</a>
				</div>
			</div>
			<div style='float:right;'>
				<div><i>Date:</i> {{ $data['date'] }}</div>
				<div style="padding-top:4px;"><i>Phone:</i> {{ $data['evibePhone'] }}</div>
			</div>
			<div style='clear:both;'></div>
		</div>
		<div style="padding:4px; background:#008000; text-align:center;">
			<h2 style="color:#FFFFFF">
				<img src='http://gallery.evibe.in/main/img/icons/check.png' height="20px" width="20px" alt='Success'> Payment Success (#{{ $data['booking']['bookingId'] }})
			</h2>
		</div>
		<div style="font-size:14px;line-height:22px; padding: 5px 30px 20px; background-color:#FFFFFF;">
			<div style="margin-top:30px; text-align:center; ">
				<div style="font-size:16px;margin:15px 0;">Dear {{ $data['customer']['name'] }},</div>
				<div style="margin:15px 0;">We have received your payment of
					<b>Rs. {{ $data['advPd'] }}</b> for {!! $data['booking']['bookingInfo'] !!}.
				</div>
				<div style="margin:15px 0;">
					Please check below for complete details & FAQs.
				</div>
				<h3 style="font-weight:bold;color:#444444;text-decoration:underline;margin-top:20px;margin-bottom:5px;">Important:</h3>
				<div style="margin:15px 0;">
					This is
					<span style="background: #F4FF97;">NOT BOOKING CONFIRMATION</span>. We will check the availability and confirm your booking within next 4 business hours.
				</div>
			</div>
			<div style='padding: 25px 0 25px;text-align:center;'>
				<a href='{{ route("track.orders") . "?ref=email&id=" . $data["ticketId"] . "&token=" . \Illuminate\Support\Facades\Hash::make($data["ticketId"] . "EVBTMO") }}' style='font-size:20px;text-decoration:none;background-color:#4584ee;color:#ffffff;padding:8px 20px;border-radius:4px;' target='_blank'>
					View &amp; Manage Order
				</a>
			</div>
			<div style="margin-top:15px;">
				In case your service is not available for your party date, we will refund the total amount you paid immediately (or) help you locate closest
				alternate options based on your choice. Please check FAQ section below.
			</div>
			<div style="margin:15px 0;">
				<div>Best Regards,</div>
				<div>Team Evibe</div>
			</div>
		</div>
	</div>

	<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
		<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Order Details</div>

		<div style="margin-top:15px;">
			<b>Customer Name: </b><span> {{ $data['customer']['name'] }} </span>
		</div>
		<div style="margin-top:15px;">
			<b>Phone Number: </b><span> {{ $data['customer']['phone'] }} </span>
		</div>
		<div style="margin-top:15px;">
			<b>Email Id: </b><span> {{ $data['customer']['email'] }} </span>
		</div>
		<div style="margin-top:15px;">
			<b>Total Booking Amount: </b><span>Rs. {{ $data['bookAmt'] }} </span>
		</div>
		<div style="margin-top:15px;">
			<b>Total Advance Paid: </b><span>Rs. {{ $data['advPd'] }} </span>
		</div>

		<div style="margin-top:15px;">
			<b>Balance Amount: </b><span>Rs. {{ $data['balAmt'] }} </span>
		</div>
	</div>

	<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
		<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Service Details</div>

		<div style="margin-top:15px;">
			<b>Booking Id: </b><span> #{{ $data['booking']['bookingId'] }} </span>
		</div>
		<div style="margin-top:15px;">
			<b>Service Name: </b><span> {!! $data['booking']['bookingInfo'] !!} </span>
		</div>
		<div style="margin-top:15px;">
			<b>Special Notes*: (Needs approval if not already discussed) </b>
			<span>
				@if($data['additional']['specialNotes'])
					{!! $data['additional']['specialNotes'] !!}
				@else
					--
				@endif
			</span>
		</div>
		@if(isset($data['checkoutFields']))
			@foreach($data['checkoutFields'] as $key => $value)
				<div style="margin-top:15px;">
					<b>{{ $key }}: </b><span> {{ $value }} </span>
				</div>
			@endforeach
		@endif
	</div>

	<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
		<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Party Details</div>

		<div style="margin-top:15px;">
			<b>Party Date: </b><span> {{ $data['additional']['checkInDate'] }} </span>
		</div>
		<div style="margin-top:15px;">
			<b>Party Time: </b><span> {{ $data['booking']['checkInTime'] }} </span>
		</div>
		<div style="margin-top:15px;">
			<b>Party Address: </b>
			<div> {{ $data['additional']['venueAddress'] }} </div>
		</div>
	</div>

	<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
		<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">FAQ's</div>
		<ul style="list-style: none; position: relative;">
			<li style="color: #EF3E75;margin-top: 20px;padding-bottom: 10px;">
				<span style="position: absolute;left: 0px;">Q.</span> When will I get booking confirmation?
			</li>
			<li style="color: #000000;">
				Within next 4 business hours, you will receive a booking confirmation email & SMS with venue & event coordinator
				details based on availability of your package. In case of non-availability, you will be notified with next
				steps.
			</li>
			<li style="color: #EF3E75;margin-top: 20px;padding-bottom: 10px;">
				<span style="position: absolute;left: 0;">Q.</span> Whom should I contact after booking confirmation?
			</li>
			<li style="color: #000000;">
				Your will be assigned an event coordinator who will make sure all arrangements are done as per
				your order details. Contact information of event coordinator will be shared in booking confirmation email.
			</li>
			<li style="color: #EF3E75;margin-top: 20px;padding-bottom: 10px;">
				<span style="position: absolute;left: 0px;">Q.</span>What if my package is not available?
			</li>
			<li style="color: #000000;">
				Incase of non-availability of your package, we will try to match something similar. If you are
				not satisfied, we will refund 100% of the advance amount immediately thereafter.
			</li>
			<li style="color: #EF3E75;margin-top: 20px;padding-bottom: 10px;">
				<span style="position: absolute;left: 0;">Q.</span>
				How many days does it take to refund?
			</li>
			<li style="color: #000000;">
				We will initiate the refund process as soon as we get a go ahead from you. However, for the money
				to actually credit to your card / account, it depends on your payment choice at the time of making advance
				payment. Rest assured, you should receive the cash within 7 - 10 Business days.
			</li>
		</ul>
	</div>

	@include('emails.auto-book.pay-success.util.cancellation-policy-customer')

	<div style="background-color:#ffffff;margin-top:30px;padding:20px;">
		<div style="color:#ef3e75;text-transform:uppercase;font-size:14px;font-weight:bold;">Terms of booking</div>
		<ul style="list-style-type:decimal;padding:0 15px 0 20px;font-size:13px;">
			<li>All the order details and amount agreed are for direct clients only. Third party transactions are strictly not allowed and is subjected to high penalty.</li>
			<li style="padding-top:5px">All the services booked are valid for party duration of 3 - 4 hours until and unless it is explicitly mentioned.</li>
			<li style="padding-top:5px">Remaining balance payment needs to done in cash only, after your event is completed.</li>
			<li style="padding-top:5px">All the service items are on hire basis only. Extra charges are applicable if you want to own it.</li>
			<li style="padding-top:5px">Booking does not include any furniture and fixture.</li>
			<li style="padding-top:5px">Anything not mentioned in order details will cost extra accordingly.</li>
			<li style="padding-top:5px">It is your responsibility to ensure sufficient time is available to execute all the order details mentioned.</li>
			<li style="padding-top:5px">It is your responsibility to ensure that all necessary permissions / copyrights and authorizations are in place.</li>
			<li style="padding-top:5px">It is your responsibility to take care of all your belongings including gifts. We cannot be held liable for any of your assets.</li>
			<li style="padding-top:5px">Although we use all reasonable safety precautions, we cannot be held liable for any casualties arising at any stage.</li>
			<li style="padding-top:5px">Although we have a policy for execution team to not consume any of the food items served at your party, it is your responsibility to ensure and instruct the same on your party date.</li>
			<li style="padding-top:5px">We will use all reasonable endeavors to meet the obligations in a prompt and efficient manner, however we will not accept responsibility for failure or delay caused by circumstances beyond its control.</li>
		</ul>
	</div>

</div>

<div style="padding-top:10px;font-size:12px;color:#999">If you are receiving the message in Spam or Junk folder, please mark it as 'not spam' and add senders id to contact list or safe list.</div>