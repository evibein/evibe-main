<div style="background-color:#FFFFFF;padding:20px;">
	<div>
		<div style='float:left;'>
			<div>
				<a href='http://evibe.in'>
					<img src='http://gallery.evibe.in/img/logo/logo_evibe.png' alt='evibe.in'>
				</a>
			</div>
		</div>
		<div style='float:right;'>
			<div><i>Date:</i> {{ $data['date'] }}</div>
			<div style="padding-top:4px;"><i>Phone:</i> {{ $data['evibePhone'] }} </div>
		</div>
		<div style='clear:both;'></div>
	</div>
	<hr>
	<div style="font-size:14px;line-height:22px;">
		<div style="margin-top:30px;">
			<p>Hi Team,</p>
			<p>We’ve received an auto booking for Service from a customer. Please check complete details below.</p>
			<div style="margin:15px auto;padding:5px;background-color:yellow">
				<b>Next, check for the availability, assign a partner and process the order accordingly</b>.
			</div>
		</div>
		<div style="background-color:#FFFFFF;margin-top:30px;">
			<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Customer Details</div>

			<table style="border-collapse:collapse; margin-top:15px" border="1">
				<tr>
					<td style="padding:8px"><b>Customer Name</b></td>
					<td style="padding:8px;">{{ $data['customer']['name'] }}</td>
				</tr>
				<tr>
					<td style="padding:8px"><b>Phone Number</b></td>
					<td style="padding:8px;">{{ $data['customer']['phone'] }}</td>
				</tr>
				<tr>
					<td style="padding:8px"><b>Email Id</b></td>
					<td style="padding:8px;">{{ $data['customer']['email'] }}</td>
				</tr>
			</table>
		</div>
		<div style="background-color:#FFFFFF;margin-top:30px;">
			<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Booking Details</div>

			<table border="1" style="border-collapse:collapse; margin-top:15px">
				<tr>
					<td style="padding:8px"><b>Booking Id</b></td>
					<td style="padding:8px;">{{ $data['booking']['bookingId'] }}</td>
				</tr>
				<tr>
					<td style="padding:8px"><b>Service Name</b></td>
					<td style="padding:8px;">{!! $data['booking']['bookingInfo'] !!}</td>
				</tr>
				<tr>
					<td style="padding:8px"><b>Customer Special Notes* (Needs approval if not already discussed)</b>
					</td>
					<td style="padding:8px;">
						@if($data['additional']['specialNotes'])
							{!! $data['additional']['specialNotes'] !!}
						@else
							--
						@endif
					</td>
				</tr>
				@if(isset($data['checkoutFields']))
					@foreach($data['checkoutFields'] as $key => $value)
						<tr>
							<td style="padding:8px"><b>{{ $key }}</b></td>
							<td style="padding:8px;">{{ $value }}</td>
						</tr>
					@endforeach
				@endif
			</table>
		</div>
		<div style="margin-top:30px">
			<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Party Details</div>
			<table border="1" style="border-collapse:collapse; margin-top:15px">
				<tr>
					<td style="padding:8px"><b>Party Date</b></td>
					<td style="padding:8px;">{{ $data['additional']['checkInDate'] }}</td>
				</tr>
				<tr>
					<td style="padding:8px"><b>Party Time</b></td>
					<td style="padding:8px;">{{ $data['booking']['checkInTime'] }}</td>
				</tr>
				<tr>
					<td style="padding:8px"><b>Party Address</b></td>
					<td style="padding:8px;">{{ $data['additional']['venueAddress'] }}</td>
				</tr>
				@if(isset($data['additional']['venueLocation']))
					<tr>
						<td style="padding:8px"><b>Location</b></td>
						<td style="padding:8px;">{{ $data['additional']['venueLocation'] }}</td>
					</tr>
				@endif
			</table>
		</div>
		<div style="margin-top:30px">
			<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Pricing Details</div>
			<table border="1" style="border-collapse:collapse; margin-top:15px">
				<tr>
					<td style="padding:8px"><b>Service Price</b></td>
					<td style="padding:8px;">Rs. {{ $data['baseAmt'] }}</td>
				</tr>
				<tr>
					<td style="padding:8px"><b>Total Booking Amount</b></td>
					<td style="padding:8px;">Rs. {{ $data['bookAmt'] }}</td>
				</tr>
				<tr>
					<td style="padding:8px"><b>Advance Paid</b></td>
					<td style="padding:8px;">Rs. {{ $data['advPd'] }}</td>
				</tr>
			</table>
		</div>
	</div>
	<div style="margin-top:15px">
		<div>Best,</div>
		<div>Admin</div>
	</div>
</div>