<div style="padding:10px; border:15px solid #f2f2f2">
	<div style="background-color:#FFFFFF;padding:20px;">
		<div>
			<div style='float:left;'>
				<div>
					<a href='http://evibe.in'><img src='http://gallery.evibe.in/img/logo/logo_evibe.png' alt='evibe.in'></a>
				</div>
			</div>
			<div style='float:right;'>
				<div><i>Date:</i> {{ $data['date'] }}</div>
				<div style="padding-top:4px;"><i>Phone:</i> {{ $data['evibePhone'] }} </div>
			</div>
			<div style='clear:both;'></div>
		</div>
		<hr>
		<div style="font-size:14px;line-height:22px;">
			<div style="margin-top:30px;">
				<p>Hi Team,</p>
				<p>
					We’ve received an auto booking from a customer. Please check complete details below. Next, check availability of the provider and
					send booking confirmation to customer (if available). If not available, call customer to provide alternate options.
				</p>
				<div style="margin:15px auto;padding:5px;background-color:yellow">
					<b><u>Note:</u></b> We need to respond to the customer within 4 business hrs from now.
				</div>
			</div>
			<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
				<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Order Details</div>

				<div style="margin-top:15px;">
					<b>Customer Name: </b><span> {{ $data['custName'] }} </span>
				</div>
				<div style="margin-top:15px;">
					<b>Phone Number: </b><span> {{ $data['custPhone'] }} </span>
				</div>
				<div style="margin-top:15px;">
					<b>Email Id: </b><span> {{ $data['custEmail'] }} </span>
				</div>
				<div style="margin-top:15px;">
					<b>Booking Id: </b><span> #{{ $data['bookId'] }} </span>
				</div>
				<div style="margin-top:15px;">
					<b>Package Name: </b><span> {!! $data['bookingInfo'] !!} </span>
				</div>
				<div style="margin-top:15px;">
					<b>Party Date / Check-In Date: </b><span> {{ $data['checkInDate'] }} ( {{ $data['checkInTime'] }} to {{ $data['checkOutTime'] }})</span>
				</div>
				<div style="margin-top:15px;">
					<b>Guests Count: </b><span> {{ $data['guestsCount'] }} </span>
				</div>
				<div style="margin-top:15px;">
					<b>Customer Special Notes* (Needs approval if not already discussed): </b><span> {!! $data['splNotes'] !!} </span>
				</div>
				<div style="margin-top:15px;">
					<b>Total Booking Amount: </b><span>Rs. {{ $data['bookAmt'] }} </span>
				</div>
				<div style="margin-top:15px;">
					<b>Total Advance Paid: </b><span>Rs. {{ $data['advPd'] }} </span>
				</div>

				<div style="margin-top:15px;">
					<b>Balance Amount: </b><span>Rs. {{ $data['balAmt'] }} </span>
				</div>
			</div>

			@if(isset($data['isVenue']) && ($data['isVenue']) == 0)
				<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
					<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Delivery Details</div>

					<div style="margin-top:15px;">
						<b>Delivery Date: </b><span> {{ $data['checkInDate'] }} </span>
					</div>
					<div style="margin-top:15px;">
						<b>Delivery Time: </b><span> {{ $data['checkInTime'] }} </span>
					</div>
					<div style="margin-top:15px;">
						<b>Delivery Address: </b>
						<div> {{ $data['deliveryAddress'] }} </div>
					</div>
				</div>
			@endif

			<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
				<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Provider Details</div>
				<div style="margin-top:15px;">
					<b>Provider Name: </b><span> {{ $data['providerName'] }} </span>
				</div>
				<div style="margin-top:15px;">
					<b>Provider Phone: </b><span> {{ $data['providerPhone'] }} </span>
				</div>
				<div style="margin-top:15px;">
					<b>Provider Alt. Phone: </b><span> {{ $data['providerPhone'] }} </span>
				</div>
				<div style="margin-top:15px;">
					<b>Provider Emails: </b><span> {{ $data['providerEmail'] }} </span>
				</div>
			</div>

		</div>
	</div>
</div>
<div style="padding-top:10px;font-size:12px;color:#999">If you are receiving the message in Spam or Junk folder, please mark it as 'not spam' and add senders id to contact list or safe list.</div>
