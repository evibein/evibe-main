<div style="background-color:#FFFFFF;padding:20px;">
	<div>
		<div style='float:left;'>
			<div>
				<a href='http://evibe.in'><img src='http://gallery.evibe.in/img/logo/logo_evibe.png' alt='evibe.in'></a>
			</div>
		</div>
		<div style='float:right;'>
			<div><i>Date:</i> {{ $data['date'] }}</div>
			<div style="padding-top:4px;"><i>Phone:</i> {{ $data['evibePhone'] }} </div>
		</div>
		<div style='clear:both;'></div>
	</div>
	<hr>
	<div style="font-size:14px;line-height:22px;">
		<div style="margin-top:30px;">
			<p>Hi Team,</p>
			<p>We’ve received an auto booking for Decor from a customer. Please check complete details below.</p>
			<p>Next, check availability of the provider and confirm the decor availability to the customer along with next steps to complete booking.
				If not available, please call the customer to provide alternate options.
			</p>
		</div>
		<div style="background-color:#FFFFFF;margin-top:30px;">
			<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Customer Details</div>

			<table style="border-collapse:collapse; margin-top:15px" border="1">
				<tr>
					<td style="padding:8px"><b>Customer Name</b></td>
					<td style="padding:8px;">{{ $data['customer']['name'] }}</td>
				</tr>
				<tr>
					<td style="padding:8px"><b>Phone Number</b></td>
					<td style="padding:8px;">{{ $data['customer']['phone'] }}</td>
				</tr>
				<tr>
					<td style="padding:8px"><b>Email Id</b></td>
					<td style="padding:8px;">{{ $data['customer']['email'] }}</td>
				</tr>
			</table>
		</div>
		<div style="background-color:#FFFFFF;margin-top:30px;">
			<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Booking Details</div>

			<table border="1" style="border-collapse:collapse; margin-top:15px">
				<tr>
					<td style="padding:8px"><b>Booking Id</b></td>
					<td style="padding:8px;">{{ $data['booking']['bookingId'] }}</td>
				</tr>
				<tr>
					<td style="padding:8px"><b>Decor Name</b></td>
					<td style="padding:8px;">{!! $data['booking']['bookingInfo'] !!}</td>
				</tr>
				<tr>
					<td style="padding:8px"><b>Customer Special Notes* (Needs approval if not already discussed)</b></td>
					<td style="padding:8px;">
						@if($data['additional']['specialNotes'])
							{!! $data['additional']['specialNotes'] !!}
						@else
							--
						@endif
					</td>
				</tr>
				@if(isset($data['checkoutFields']))
					@foreach($data['checkoutFields'] as $key => $value)
						<tr>
							<td style="padding:8px"><b>{{ $key }}</b></td>
							<td style="padding:8px;">{{ $value }}</td>
						</tr>
					@endforeach
				@endif
			</table>
		</div>
		<div style="margin-top:30px">
			<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Delivery Details</div>
			<table border="1" style="border-collapse:collapse; margin-top:15px">
				<tr>
					<td style="padding:8px"><b>Party Date</b></td>
					<td style="padding:8px;">{{ $data['additional']['checkInDate'] }}</td>
				</tr>
				<tr>
					<td style="padding:8px"><b>Party Time</b></td>
					<td style="padding:8px;">{{ $data['booking']['checkInTime'] }}</td>
				</tr>
				<tr>
					<td style="padding:8px"><b>Delivery Address</b></td>
					<td style="padding:8px;">{{ $data['additional']['venueAddress'] }}</td>
				</tr>
				@if(isset($data['additional']['venueLocation']))
					<tr>
						<td style="padding:8px"><b>Location</b></td>
						<td style="padding:8px;">{{ $data['additional']['venueLocation'] }}</td>
					</tr>
				@endif
			</table>
		</div>
		<div style="margin-top:30px">
			<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Pricing Details</div>
			<table border="1" style="border-collapse:collapse; margin-top:15px">
				<tr>
					<td style="padding:8px"><b>Decor Price</b></td>
					<td style="padding:8px;">Rs. {{ $data['baseAmt'] }}</td>
				</tr>
				<tr>
					<td style="padding:8px"><b>Delivery Charges</b></td>
					<td style="padding:8px;">Rs. {{ $data['deliveryAmt'] }}</td>
				</tr>
				<tr>
					<td style="padding:8px"><b>Total Amount</b></td>
					<td style="padding:8px;">Rs. {{ $data['bookAmt'] }}</td>
				</tr>
				<tr>
					<td style="padding:8px"><b>Total Advance Paid</b></td>
					<td style="padding:8px;">Rs. {{ $data['advPd'] }}</td>
				</tr>
			</table>
		</div>
		<div style="margin-top:30px">
			<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Provider Details</div>
			<table border="1" style="border-collapse:collapse; margin-top:15px">
				<tr>
					<td style="padding:8px"><b>Name</b></td>
					<td style="padding:8px;">{{ $data['booking']['provider']['person'] }}</td>
				</tr>
				<tr>
					<td style="padding:8px"><b>Phone</b></td>
					<td style="padding:8px;">{{ $data['booking']['provider']['phone'] }}</td>
				</tr>
				@if(isset($data['booking']['provider']['altPhone']))
					<tr>
						<td style="padding:8px"><b>Alt. Phone</b></td>
						<td style="padding:8px;">{{ $data['booking']['provider']['altPhone'] }}</td>
					</tr>
				@endif
				<tr>
					<td style="padding:8px"><b>Email</b></td>
					<td style="padding:8px;">{{ $data['booking']['provider']['email'] }}</td>
				</tr>
				@if(isset($data['booking']['provider']['altEmail']))
					<tr>
						<td style="padding:8px"><b>Alt. Email</b></td>
						<td style="padding:8px;">{{ $data['booking']['provider']['altEmail'] }}</td>
					</tr>
				@endif
			</table>
		</div>
	</div>
	<div style="margin-top:15px">
		<div>Best,</div>
		<div>Admin</div>
	</div>
</div>