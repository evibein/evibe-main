<div style="background-color:#F5F5f5;padding:10px 20px;width:700px;font-size:14px;line-height:22px;">
	<div>
		<div style="padding:20px;">
			<div style='float:left;'>
				<div>
					<a href='http://evibe.in'><img src='http://gallery.evibe.in/img/logo/logo_evibe.png' alt='evibe.in'></a>
				</div>
			</div>
			<div style='float:right;'>
				<div><i>Date:</i> {{ $data['date'] }}</div>
				<div style="padding-top:4px;"><i>Phone:</i> {{ $data['evibePhone'] }}</div>
			</div>
			<div style='clear:both;'></div>
		</div>
		<div style="padding:4px; background:#828282; text-align:center;">
			<h2 style="color:#FFFFFF">
				Is Decor Available For {{ $data['additional']['checkInDate'] }}, {{ $data['booking']['checkInTime'] }} ?
			</h2>
		</div>
		<div style="font-size:14px;line-height:22px; padding: 20px 30px 20px; background-color:#FFFFFF;">
			<div style="margin-top:30px; text-align:center; ">
				<div style="font-size:16px;margin:15px 0;">Dear {{ $data['booking']['provider']['person'] }},</div>
				<div style="margin:15px 0;"> A customer is interested in {!! $data['booking']['bookingInfo'] !!} and has already paid amount of Rs.
					{{ $data['advPd'] }}. Please check the complete order details below.
					<div style="margin:15px auto;font-size:16px;">
						Click on <b>“Accept”</b> button to accept & confirm the order (or) click
						<b>“Reject”</b> button if it is not available.
					</div>
					<div style="margin:15px 0;">
						<ul style="list-style:none; padding:10px 0;">
							<li style="display:inline;">
								<a href="{{ $data['vendorAskUrl'] }}#confirm" style="color:#FFFFFF;text-transform: uppercase; text-decoration: none;padding: 10px 20px; background: #149814; ">Accept</a>
							</li>
							<li style="display:inline; margin-left:20px;">
								<a href="{{ $data['vendorAskUrl'] }}#cancel" style="color:#FFFFFF;text-transform: uppercase; text-decoration: none;padding: 10px 20px; background: #CC3F3F; ">Reject</a>
							</li>
						</ul>
					</div>
				</div>
				<div style=" margin-top:30px;">
					<table style="border-collapse: collapse; width:100%" border="1">
						<tr>
							<td style="padding:10px; width: 50%;vertical-align: top">
								<div style="text-align: left">
									<div style="text-align: center"><b>Order Details</b></div>
									<hr>
									<div><strong>Customer Name:</strong> {{ $data['customer']['name'] }}</div>
									<div><strong>Decor:</strong> {!! $data['booking']['bookingInfo'] !!}</div>
									<div><strong>Customer Special Notes* (Needs approval if not already discussed):</strong>
										@if(isset($data['booking']['specialNotes']) && $data['booking']['specialNotes'])
											{!! $data['booking']['specialNotes'] !!}
										@else
											--
										@endif
									</div>
									@if(isset($data['checkoutFields']))
										@foreach($data['checkoutFields'] as $key => $value)
											<div><strong>{{ $key }}:</strong> {{ $value }}</div>
										@endforeach
									@endif
								</div>
							</td>
							<td style="padding:10px; width: 50%;vertical-align: top">
								<div style="text-align: left;">
									<div style="text-align: center"><b>Delivery Details</b></div>
									<hr>
									<div><strong>Date:</strong> {{ $data['additional']['checkInDate'] }}</div>
									<div><strong>Time:</strong> {{ $data['booking']['checkInTime'] }}</div>
									<div><strong>Location:</strong> {{ $data['additional']['venueLocation'] }}</div>
									<div><strong>Full Address:</strong> {{ $data['additional']['venueAddress'] }}</div>
								</div>
							</td>
						</tr>
						<tr>
							<td style="padding:10px; width: 50%">
								<div style="text-align: left">
									<div><strong>Decor Price:</strong> Rs. {{ $data['baseAmt'] }}</div>
									<div><strong>Delivery Charge:</strong>
										@if($data['delivery'] > 0)
											Rs. {{ $data['deliveryAmt'] }}
										@else
											<span style='color:rgba(0, 169, 12, 0.83)'> Free </span>
										@endif
									</div>
									<div><strong>Total Order Amount:</strong> Rs. {{ $data['bookAmt'] }}</div>
								</div>
							</td>
							<td style="padding:10px; width: 50%">
								<div style="text-align: left">
									<div><strong>Advance Paid:</strong> Rs. {{ $data['advPd'] }}</div>
									<div><strong>Balance Amount:</strong> Rs. {{ $data['balAmt'] }}</div>
								</div>
							</td>
						</tr>
					</table>
				</div>
				<div style="text-align:left">
					<div style="margin:10px 0 10px;">
						<u>Note:</u> If there is any change in decor details, please reply to this email / call {{ $data['bizHead'] }} at the earliest. We will update the same on our website.
					</div>
					<div style="margin:15px 0;">
						<div>Best Regards,</div>
						<div>Your wellwishers at <a href="{{ $data['evibeLink'] }}" style='color:#333'>Evibe.in</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div style="padding-top:10px;font-size:12px;color:#999">If you are receiving the message in Spam or Junk folder, please mark it as 'not spam' and add senders id to contact list or safe list.</div>