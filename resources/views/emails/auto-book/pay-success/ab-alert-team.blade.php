<div style="padding:10px; width: 900px; border:15px solid #f2f2f2">
	<div style="background-color:#FFFFFF;padding:20px;">
		<div>
			<div style='float:left;'>
				<div>
					<a href='http://evibe.in'><img src='http://gallery.evibe.in/img/logo/logo_evibe.png' alt='evibe.in'></a>
				</div>
			</div>
			<div style='float:right;'>
				<div><i>Date:</i> {{ $data['date'] }}</div>
				<div style="padding-top:4px;"><i>Phone:</i> {{ $data['evibePhone'] }} </div>
			</div>
			<div style='clear:both;'></div>
		</div>
		<hr>
		<div style="font-size:14px;line-height:22px;">
			<div style="margin-top:30px;">
				<p>Hi Team,</p>
				<p>
					We’ve received an auto booking from a customer. Please check complete details below. Next, check availability of the provider(s) and
					send booking confirmation to customer (if available). If not available, call customer to provide alternate options.
				</p>
				<div style="margin:15px auto;padding:5px;background-color:yellow">
					<b><u>Note:</u></b> We need to respond to the customer within 4 business hrs from now.
				</div>
				@if(isset($data['ticket']) && $data['ticket'] && $data['ticket']->enquiry_source_id && isset($data['pbCheckoutSourceId']) && $data['pbCheckoutSourceId'] && ($data['ticket']->enquiry_source_id == $data['pbCheckoutSourceId']))
					<div style="margin:15px auto;padding:5px;background-color:deepskyblue">
						<b><u>Note:</u></b> This is a party bag checkout auto booking
					</div>
					@endif
				@if(isset($data['gallery']) && count($data['gallery']))
					<div style="margin: 15px 0;">
						<b>Customer has uploaded few images</b>
					</div>
				@endif
			</div>
			<div style="background-color:#FFFFFF;margin-top:30px;">
				<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Customer Details</div>

				<table style="border-collapse:collapse; width:100%; margin-top:15px" border="1">
					<tr>
						<td style="padding:10px; width: 100%;vertical-align: top">
							<div>
								<b>Customer Name: </b><span> {{ $data['customer']['name'] }} </span>
							</div>
							<div style="margin-top:15px;">
								<b>Phone Number: </b><span> {{ $data['customer']['phone'] }} </span>
							</div>
							<div style="margin-top:15px;">
								<b>Email Id: </b><span> {{ $data['customer']['email'] }} </span>
							</div>
							<div style="margin-top:15px;">
								<b>Customer Special Notes* (Needs approval if not already discussed): </b>
								<span>
									@if($data['additional']['specialNotes'])
										{!! $data['additional']['specialNotes'] !!}
									@else
										--
									@endif
								</span>
							</div>
						</td>
					</tr>
				</table>
			</div>

			@if(isset($data['bookings']['list']) && count($data['bookings']['list']))
				<div style="background-color:#FFFFFF;margin-top:30px;">
					<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Order Details</div>

					@foreach($data['bookings']['list'] as $booking)
						<table style="border-collapse:collapse; width: 100%; margin-top:15px" border="1">
							<tr>
								<td style="padding:10px; width: 35%;vertical-align: top">
									<div>
										<span style="padding:3px 10px;background: #efefef;border-radius:2px;color:#333">{!! $booking['typeBookingDetails'] !!}</span>
									</div>
									<div style="margin-top:15px;">
										<b>Booking Id: </b><span> #{{ $booking['bookingId'] }} </span>
									</div>
									<div style="margin-top:15px;">
										<b>Product Name: </b><span> {!! $booking['bookingInfo'] !!} </span>
									</div>
									<div style="margin-top:15px;">
										<b>Booking Units: </b><span> {!! $booking['bookingUnits'] !!} </span>
									</div>
									@if(isset($booking['checkoutFields']) && count($booking['checkoutFields']))
										@foreach($booking['checkoutFields'] as $key => $value)
											<div style="margin-top:15px;">
												<b>{{ $key }}: </b><span> {{ $value }} </span>
											</div>
										@endforeach
									@endif
								</td>
								<td style="padding:10px; width: 30%;vertical-align: top">
									<div>
										<b>Product Price: </b><span>Rs. {{ $booking['productPriceStr'] }} </span>
									</div>
									<div style="margin-top:15px;">
										<b>Transportation Charges: </b><span>Rs. {{ $booking['transportChargesStr'] }} </span>
									</div>
									<div style="margin-top:15px;">
										<b>Booking Amount: </b><span>Rs. {{ $booking['bookingAmountStr'] }} </span>
									</div>
									<div style="margin-top:15px;">
										<b>Advance Amount: </b><span>Rs. {{ $booking['advanceAmountStr'] }} </span>
									</div>
									<div style="margin-top:15px;">
										<b>Balance Amount: </b><span>Rs. {{ $booking['balanceAmountStr'] }} </span>
									</div>
								</td>
								@if(isset($booking['provider']))
									<td style="padding:10px; width: 35%;vertical-align: top">
										<div>
											<span style="padding:3px 10px;background: #efefef;border-radius:2px;color:#333">{{ $booking['provider']['providerType'] }}</span>
										</div>
										<div style="margin-top:15px;">
											<b>Name: </b><span> {{ $booking['provider']['name'] }} </span>
										</div>
										<div style="margin-top:15px;">
											<b>Phone: </b><span> {{ $booking['provider']['phone'] }} </span>
										</div>
										<div style="margin-top:15px;">
											<b>Alt. Phone: </b><span> {{ $booking['provider']['altPhone'] }} </span>
										</div>
										<div style="margin-top:15px;">
											<b>Email: </b><span> {{ $booking['provider']['email'] }} </span>
										</div>
										<div style="margin-top:15px;">
											<b>Alt. Email: </b><span> {{ $booking['provider']['altEmail'] }} </span>
										</div>
									</td>
								@endif
							</tr>
						</table>
					@endforeach
				</div>

				@if(count($data['bookings']['list']) > 1)
					<div style="background-color:#FFFFFF;margin-top:30px;">
						<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Party Price Details</div>
						<div style="margin-top:15px;">
							<b>Total Booking Amount: </b><span>Rs. {{ $data['totalBookingAmountStr'] }} </span>
						</div>
						<div style="margin-top:15px;">
							<b>Total Advance Paid: </b><span>Rs. {{ $data['totalAdvancePaidStr'] }} </span>
						</div>
						<div style="margin-top:15px;">
							<b>Total Balance Amount: </b><span>Rs. {{ $data['totalBalanceAmountStr'] }} </span>
						</div>
					</div>
				@endif
			@endif

			<div style="background-color:#FFFFFF;margin-top:30px;">
				<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Venue Details</div>
				<table style="border-collapse:collapse; margin-top:15px" border="1">
					<tr>
						<td style="padding:10px; width: 100%;vertical-align: top">
							<div>
								<b>Venue Address: </b><span> {{ $data['additional']['venueAddress'] }} </span>
							</div>
							<div style="margin-top:15px;">
								<b>Area: </b><span> {{ $data['additional']['area'] }} ({{ $data['additional']['cityName'] }}) </span>
							</div>
							<div style="margin-top:15px;">
								<b>Landmark: </b><span> {{ $data['additional']['venueLandmark'] }} </span>
							</div>
						</td>
					</tr>
				</table>
			</div>

			@if(isset($data['gallery']) && count($data['gallery']))
				<div style="background-color:#FFFFFF;margin-top:30px;">
					<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Uploaded Images</div>
					<table style="border-collapse:collapse; margin-top:15px" border="1">
						<tr>
							<td style="padding:10px; width: 100%;vertical-align: top">
								@foreach($data['gallery'] as $imageLink)
									<div style="height: 60px; width: 90px; display: inline-block; margin-top: 5px; margin-right: 5px;">
										<img style="height: 100%; width: 100%;" src="{{ $imageLink }}">
									</div>
								@endforeach
							</td>
						</tr>
					</table>
				</div>
			@endif
		</div>
	</div>
</div>
<div style="padding-top:10px;font-size:12px;color:#999">If you are receiving the message in Spam or Junk folder, please mark it as 'not spam' and add senders id to contact list or safe list.</div>
