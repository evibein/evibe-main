<div style="padding:10px; border:15px solid #f2f2f2">
	<p>Hello Team,</p>
	<p>Someone reported an issue on one of our listings. Please go through the details below.</p>
	<div style="margin-top:10px">
		<table style="border-collapse: collapse;" border="1">
			<tr>
				<td style="padding:8px">Issue</td>
				<td style="padding:8px">{{ $data['issue'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Website link</td>
				<td style="padding: 8px"><a href="{{ $data['report_page_url'] }}">{{ $data['report_page_url'] }}</a>
				</td>
			</tr>
			<tr>
				<td style="padding:8px">Dash Link</td>
				<td style="padding: 8px">
					@if($data['map_type_id'] == config('evibe.ticket.type.decor'))
						{{ config('evibe.dash_host').'/decors/'.$data['option_id'].'/info' }}
					@elseif($data['map_type_id'] == config('evibe.ticket.type.cake'))
						{{ config('evibe.dash_host').'/cakes/'.$data['option_id'].'/info' }}
					@elseif($data['map_type_id'] == config('evibe.ticket.type.trend'))
						{{ config('evibe.dash_host').'/trends/view/'.$data['option_id'] }}
					@elseif($data['map_type_id'] == config('evibe.ticket.type.entertainment'))
						{{ config('evibe.dash_host').'/services/details/'.$data['option_id'] }}
					@elseif($data['map_type_id'] == config('evibe.ticket.type.halls'))
						{{ config('evibe.dash_host').'/venues/hall/'.$data['option_id']}}
					@elseif(!is_null($data['package_type_id']))
						@if($data['package_type_id'] == '2')
							{{ config('evibe.dash_host').'/packages/vendor/view/'.$data['option_id'] }}
						@elseif($data['package_type_id'] == '3')
							{{ config('evibe.dash_host').'/packages/venue/view/'.$data['option_id'] }}
						@else
							---
						@endif
					@else
						--
					@endif
				</td>
			</tr>
			@if(isset($data['username']) && $data['username'])
				<tr>
					<td style="padding:8px">User Name</td>
					<td style="padding: 8px">{{ $data['username'] }}</td>
				</tr>
			@endif
			@if(isset($data['reporter_name']) && $data['reporter_name'])
				<tr>
					<td style="padding:8px">Name</td>
					<td style="padding: 8px">{{ $data['reporter_name'] }}</td>
				</tr>
			@endif
			@if(isset($data['reporter_phone']) && $data['reporter_phone'])
				<tr>
					<td style="padding:8px">Phone Number</td>
					<td style="padding: 8px">{{ $data['reporter_phone'] }}</td>
				</tr>
			@endif
			@if(isset($data['reporter_email']) && $data['reporter_email'])
				<tr>
					<td style="padding:8px">Email</td>
					<td style="padding: 8px">{{ $data['reporter_email'] }}</td>
				</tr>
			@endif
		</table>
	</div>
	<div style="margin-top:10px">
		Best Regards,
		<div>Admin</div>
	</div>
</div>