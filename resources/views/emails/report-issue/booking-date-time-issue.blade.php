<div style="padding:10px; border:15px solid #f2f2f2">
	<p>Hello Team,</p>
	<p>A customer has tried making payment from checkout page to a party having party date time before the current time. Kindly call him and help in making the payment.</p>
	<p>Below is a dash link to the ticket.</p>
	<div style="margin-top:10px">
		<table style="border-collapse: collapse;" border="1">
			<tr>
				<td style="padding:8px">Dash link</td>
				<td style="padding:8px">{!! $data['dashLink'] !!}</td>
			</tr>
		</table>
	</div>
	<div style="margin-top:10px">
		Best Regards,
		<div>Admin</div>
	</div>
</div>