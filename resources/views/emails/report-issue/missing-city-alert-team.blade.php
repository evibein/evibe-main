<div style="padding:10px; border:15px solid #f2f2f2">
	<p>Hello Team,</p>
	<p>The following option doesn't belong to any city. Kindly verify and add appropriate city to the option.</p>
	<p>If the option already has a city, kindly notify TECH team.</p>
	<div style="margin-top:10px">
		<table style="border-collapse: collapse;" border="1">
			<tr>
				<td style="padding:8px">Option Name</td>
				<td style="padding:8px">{{ $data['name'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Option Code</td>
				<td style="padding: 8px">{{ $data['code'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Option Type</td>
				<td style="padding: 8px">{{ $data['type'] }}</td>
			</tr>
		</table>
	</div>
	<div style="margin-top:10px">
		Best Regards,
		<div>Admin</div>
	</div>
</div>