<div style="padding:10px; border:15px solid #f2f2f2">
	<p>Hello Team,</p>
	<p>A new applicant has applied. Please go through the below details.</p>
	<div style="margin-top:20px">
		<table style="border-collapse: collapse;" border="1">
			<tr>
				<td style="padding:8px">Name</td>
				<td style="padding:8px">{{ $data['name'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Email</td>
				<td style="padding: 8px">{{ $data['email'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Area interested in</td>
				<td style="padding: 8px">{!! $data['options'] !!}</td>
			</tr>
			<tr>
				<td style="padding:8px">Why join us?</td>
				<td style="padding: 8px">{!! $data['comment'] !!}</td>
			</tr>
		</table>
	</div>
	<p>Please find the attachment of resume uploaded by applicant</p>
	<div style="margin-top:10px">
		Best Regards,
		<div>Admin</div>
	</div>
</div>