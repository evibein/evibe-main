<div style="padding:10px; border:15px solid #f2f2f2">
	<p>Dear {{ ucfirst($data['name']) }},</p>
	<p>We've received your application. We will go through your resume and will get back to you as soon as we can.</p>
	<p>Please find below details that you've submitted:</p>
	<div style="margin-top:10px">
		<table style="border-collapse: collapse;" border="1">
			<tr>
				<td style="padding:8px">Name</td>
				<td style="padding:8px">{{ $data['name'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Email</td>
				<td style="padding: 8px">{{ $data['email'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Area interested in</td>
				<td style="padding: 8px">{!! $data['options'] !!}</td>
			</tr>
			<tr>
				<td style="padding:8px">Why join us?</td>
				<td style="padding: 8px">{!! $data['comment'] !!}</td>
			</tr>
		</table>
	</div>
	<p>
		Should you have any queries, please reply to this email (or) call us on {{ config('evibe.contact.company.phone') }}
	</p>
	<div style="margin-top:10px">
		Best Regards,
		<div>Team Evibe.in</div>
	</div>
</div>