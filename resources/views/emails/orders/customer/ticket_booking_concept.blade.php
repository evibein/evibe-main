<div style="padding:10px; border:15px solid #f2f2f2">
	<p>Hi Team,</p>
	<p>We've received a booking marked as:
		<b>@if(!is_null($data['booking']->bookingConcept)) {{  $data['booking']->bookingConcept->name }}@endif concept</b>.
	</p>
	<p>Please track this booking carefully to get all the required details (pictures, inclusions etc) to make a new activation.</p>
	<div style="margin-top:10px">
		<table style="border-collapse: collapse;" border="1">
			<tr>
				<td style="padding:8px">Partner name</td>
				<td style="padding:8px">
					@if(!is_null($data['person']))
						{{ $data['person'] }}
					@else ---
					@endif
				</td>
			</tr>
			<tr>
				<td style="padding:8px">Booking ID</td>
				<td style="padding: 8px">
					@if(!is_null($data['booking']['booking_id']))
						{{ $data['booking']['booking_id'] }}
					@else ---
					@endif
				</td>
			</tr>
			<tr>
				<td style="padding:8px">Customer name</td>
				<td style="padding: 8px">
					@if(!is_null($data['ticket']['name']))
						{{ $data['ticket']['name'] }}
					@else ---
					@endif
				</td>
			</tr>
			<tr>
				<td style="padding:8px">Party date</td>
				<td style="padding: 8px">
					@if(!is_null($data['partyDate']))
						{{ $data['partyDate'] }}
					@else ---
					@endif
				</td>
			</tr>
			<tr>
				<td style="padding:8px">Location</td>
				<td style="padding: 8px">
					@if($data['ticket']->area)
						{{ $data['ticket']->area->name }}
					@else ---
					@endif
				</td>
			</tr>
			<tr>
				<td style="padding:8px">Booking information</td>
				<td style="padding: 8px">
					@if(!is_null($data['booking']['booking_info']))
						{!! $data['booking']['booking_info'] !!}
					@else ---
					@endif
				</td>
			</tr>
		</table>
		<div class="pad-10">
			For further information <a href="{{ $data['url'] }}">click here.</a>
		</div>
	</div>
	<p class="pad-5" style="margin-top:10px">Please find the attachment of images uploaded by customer</p>
	<div style="margin-top:10px">
		Best Regards,
		<div>Team Evibe.in</div>
	</div>
</div>