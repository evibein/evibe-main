<p>Hello Team,</p>

<p>User entered details for premium invite. Kindly go through the details & send the payment link.</p>

<table style="border-collapse: collapse;text-align: left;">
	<tr>
		<th colspan="2" style="border: 1px solid #dddddd; padding: 8px; text-align: center;">Enquiry Details</th>
	</tr>
	@if(isset($data["name"]) && $data["name"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Name</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["name"] }}</td>
		</tr>
	@endif
	@if(isset($data["email"]) && $data["email"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Mail</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["email"] }}</td>
		</tr>
	@endif
	@if(isset($data["phone"]) && $data["phone"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Phone</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["phone"] }}</td>
		</tr>
	@endif
	@if(isset($data["link"]) && $data["link"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Invite Link</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["link"] }}</td>
		</tr>
	@endif
</table>
<p>
	regards,<br>
	Admin
</p>