<table class="body" style="Margin: 0; background: #F8F8F8; border-collapse: collapse; border-spacing: 0; color: #0A0A0A; font-size: 16px; font-weight: normal; height: 100%; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
	<tbody>
	<tr style="padding: 0; text-align: left; vertical-align: top">
		<td class="center" align="center" valign="top" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0A0A0A; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
			<center data-parsed="" style="min-width: 580px; width: 100%">
				<table align="center" class="container logo-block float-center" style="Margin: 0 auto; background: #F8F8F8; background-color: #F8F8F8; border-collapse: collapse; border-spacing: 0; float: none; margin: 0 auto; padding: 0; text-align: center; vertical-align: top; width: 580px">
					<tbody>
					<tr style="padding: 0; text-align: left; vertical-align: top">
						<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0A0A0A; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
							<table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
								<tbody>
								<tr style="padding: 0; text-align: left; vertical-align: top">
									<td height="30px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0A0A0A; font-size: 30px; font-weight: normal; hyphens: auto; line-height: 30px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&nbsp;</td>
								</tr>
								</tbody>
							</table>
							<table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%">
								<tbody>
								<tr style="padding: 0; text-align: left; vertical-align: top">
									<th class="small-12 large-12 columns first last" valign="middle" style="Margin: 0 auto; color: #0A0A0A; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 0; padding-left: 16px; padding-right: 16px; text-align: left; width: 564px">
										<table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
											<tbody>
											<tr style="padding: 0; text-align: left; vertical-align: top">
												<th style="Margin: 0; color: #0A0A0A; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
													<center data-parsed="" style="min-width: 532px; width: 100%">
														<img src="https://gallery.evibe.in/main/pages/refer_earn/logo_evibe.png" class="logo-image float-center" alt="Evibe.in" width="" align="center" style="-ms-interpolation-mode: bicubic; Margin: 0 auto; clear: both; display: block; float: none; height: auto; margin: 0 auto; max-width: 100%; outline: none; text-align: center; text-decoration: none; width: 210px">
													</center>
												</th>
												<th class="expander" style="Margin: 0; color: #0A0A0A; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th>
											</tr>
											</tbody>
										</table>
									</th>
								</tr>
								</tbody>
							</table>
							<table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
								<tbody>
								<tr style="padding: 0; text-align: left; vertical-align: top">
									<td height="30px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0A0A0A; font-size: 30px; font-weight: normal; hyphens: auto; line-height: 30px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&nbsp;</td>
								</tr>
								</tbody>
							</table>
						</td>
					</tr>
					</tbody>
				</table>
				<table align="center" class="container hero-block float-center" style="Margin: 0 auto; background: #F8F8F8; background-color: #F8F8F8; border-collapse: collapse; border-spacing: 0; float: none; margin: 0 auto; padding: 0; text-align: center; vertical-align: top; width: 580px">
					<tbody>
					<tr style="padding: 0; text-align: left; vertical-align: top">
						<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0A0A0A; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
							<table class="row collapse" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%">
								<tbody>
								<tr style="padding: 0; text-align: left; vertical-align: top">
									<th class="small-12 large-12 columns first last" valign="middle" style="Margin: 0 auto; color: #0A0A0A; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; text-align: left; width: 588px">
										<table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
											<tbody>
											<tr style="padding: 0; text-align: left; vertical-align: top">
												<th style="Margin: 0; color: #0A0A0A; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
													<center data-parsed="" style="min-width: 532px; width: 100%">
														<div align="center" class="float-center" style="Margin: 0; color: #ED3279; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left; text-decoration: none">
															<img src="https://gallery.evibe.in/main/pages/refer_earn/surprise.png" class="hero-image show-for-large" alt="Join the Evibe.in Referral Program!" width="580" style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: block; height: auto !important; max-width: 100% !important; outline: none; text-align: center; text-decoration: none; width: 100% !important">
														</div>
													</center>
												</th>
												<th class="expander" style="Margin: 0; color: #0A0A0A; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th>
											</tr>
											</tbody>
										</table>
									</th>
								</tr>
								</tbody>
							</table>
						</td>
					</tr>
					</tbody>
				</table><!-- BODY TEXT COMPONENT -->
				<table align="center" class="container text-block float-center" style="Margin: 0 auto; background: #F8F8F8; background-color: #FFFFFF; border-collapse: collapse; border-spacing: 0; float: none; margin: 0 auto; padding: 0; text-align: center; vertical-align: top; width: 580px">
					<tbody>
					<tr style="padding: 0; text-align: left; vertical-align: top">
						<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0A0A0A; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
							<table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%">
								<tbody>
								<tr style="padding: 0; text-align: left; vertical-align: top">
									<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0A0A0A; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 0; padding-left: 16px; padding-right: 16px; text-align: left; width: 564px">
										<table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
											<tbody>
											<tr style="padding: 0; text-align: left; vertical-align: top">
												<th style="Margin: 0; color: #0A0A0A; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
													<table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
														<tbody>
														<tr style="padding: 0; text-align: left; vertical-align: top">
															<td height="30px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0A0A0A; font-size: 30px; font-weight: normal; hyphens: auto; line-height: 30px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&nbsp;</td>
														</tr>
														</tbody>
													</table>
													<p class="text-header text-center rc-text" style="-moz-hyphens: none; -ms-hyphens: none; -webkit-hyphens: none; Margin: 0; Margin-bottom: 10px; color: #5B5B5B; font-size: 36px; font-weight: bold; hyphens: none; line-height: 1.25; margin: 0; margin-bottom: 10px; padding: 0; text-align: center">Thank you for Signing up!</p>
													<table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
														<tbody>
														<tr style="padding: 0; text-align: left; vertical-align: top">
															<td height="8px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0A0A0A; font-size: 8px; font-weight: normal; hyphens: auto; line-height: 8px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&nbsp;</td>
														</tr>
														</tbody>
													</table>
													<p class="text-subheader text-center rc-text" style="Margin: 0; Margin-bottom: 10px; color: #5B5B5B; font-size: 22px; font-weight: normal; line-height: 1.25; margin: 0; margin-bottom: 10px; padding: 0; text-align: center">
														As promised, here is your reward. Use the coupon code below to get &#8377; 200* OFF on your next party booking.
													</p>
													<table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
														<tbody>
														<tr style="padding: 0; text-align: left; vertical-align: top">
															<td height="24px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0A0A0A; font-size: 24px; font-weight: normal; hyphens: auto; line-height: 24px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&nbsp;</td>
														</tr>
														</tbody>
													</table>
													<center data-parsed="" style="min-width: 532px; width: 100%">
														<table class="button xl-cta-button float-center" style="Margin: 0; border-collapse: collapse; border-spacing: 0; float: none; margin: 0; padding: 0; text-align: center; vertical-align: top; width: auto; min-width: 400px; margin-bottom: 20px">
															<tbody>
															<tr style="padding: 0; text-align: left; vertical-align: top">
																<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0A0A0A; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
																	<table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
																		<tbody>
																		<tr style="padding: 0; text-align: left; vertical-align: top">
																			<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background: #ED3279; background-color: #FFFFFF; border: 2px dashed #BBBBBB; border-collapse: collapse !important; border-color: #ED3279; color: #ED3279; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: center; vertical-align: top; word-wrap: break-word">
																				<span href="#" style="Margin: 0; border: 0 solid; border-radius: 3px; color: #ED3279; display: inline-block; font-size: 18px; font-weight: normal; line-height: 1.25; margin: 0; padding: 10px 54px; text-align: center; text-decoration: none"><span class="rc-text">{{ $data["referralCode"] }}</span></span><br>
																				<span style="padding-top: 10px; color: #353535; font-size: 17px">&#8377; 200* OFF</span><br>
																				<small style="color: #7d7d7d; font-size: 10px;">* Valid upto {{ $data["validUpto"] }}, Minimum order Rs. 3,000, all T&C apply</small>
																			</td>
																		</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
															</tbody>
														</table>
													</center>
													<center data-parsed="" style="min-width: 532px; width: 100%">
														<table class="button xl-cta-button float-center" style="Margin: 0 0 16px 0; border-collapse: collapse; border-spacing: 0; float: none; margin: 0 0 16px 0; padding: 0; text-align: center; vertical-align: top; width: auto; min-width: 202px">
															<tbody>
															<tr style="padding: 0; text-align: left; vertical-align: top">
																<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0A0A0A; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
																	<table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
																		<tbody>
																		<tr style="padding: 0; text-align: left; vertical-align: top">
																			<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background: #ED3279; background-color: #ED3279; border: 2px solid #ED3279; border-collapse: collapse !important; border-color: #ED3279; color: #FFFFFF; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
																				<a href="http://evibe.in/" style="Margin: 0; border: 0 solid; border-radius: 3px; color: #FFFFFF; display: inline-block; font-size: 16px; font-weight: normal; line-height: 1.25; margin: 0; padding: 15px 57px; text-align: center; text-decoration: none"><span class="rc-text">PLAN NOW</span></a>
																			</td>
																		</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
															</tbody>
														</table>
													</center>
													<table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
														<tbody>
														<tr style="padding: 0; text-align: left; vertical-align: top">
															<td height="30px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0A0A0A; font-size: 30px; font-weight: normal; hyphens: auto; line-height: 30px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&nbsp;</td>
														</tr>
														</tbody>
													</table>
												</th>
												<th class="expander" style="Margin: 0; color: #0A0A0A; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th>
											</tr>
											</tbody>
										</table>
									</th>
								</tr>
								</tbody>
							</table>
						</td>
					</tr>
					</tbody>
				</table>
			</center>
		</td>
	</tr>
	</tbody>
</table>