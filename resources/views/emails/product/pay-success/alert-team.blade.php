<div style="padding:10px; border:15px solid #f2f2f2">
	<div style="background-color:#FFFFFF;padding:20px;">
		<div>
			<div style='float:left;'>
				<div>
					<a href='http://evibe.in'><img src='http://gallery.evibe.in/img/logo/logo_evibe.png' alt='evibe.in'></a>
				</div>
			</div>
			<div style='float:right;'>
				<div><i>Date:</i> {{ $data['date'] }}</div>
				<div style="padding-top:4px;"><i>Phone:</i> {{ $data['evibePhone'] }} </div>
			</div>
			<div style='clear:both;'></div>
		</div>
		<hr>
		<div style="font-size:14px;line-height:22px;">
			<div style="margin-top:30px;">
				<p>Hi Team,</p>
				<p>
					Hurray! We've receipt payment for <b>Party In A Box</b>
				</p>
				<div style="margin:15px auto;padding:5px;background-color:yellow">
					<b><u>Note:</u></b> We need to ship the product and update tracking code with customer
				</div>
			</div>
			<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
				<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Order Details</div>
				<div style="margin-top:15px;">
					<b>Customer Name: </b><span> {{ $data['customer']['name'] }} </span>
				</div>
				<div style="margin-top:15px;">
					<b>Phone Number: </b><span> {{ $data['customer']['phone'] }} </span>
				</div>
				<div style="margin-top:15px;">
					<b>Email Id: </b><span> {{ $data['customer']['email'] }} </span>
				</div>
				<div style="margin-top:15px;">
					<b>Booking Id: </b><span> #{{ $data['booking']['bookingOrderId'] }} </span>
				</div>
				<div style="margin-top:15px;">
					<b>Product: </b><span> {!! $data['booking']['bookingInfo'] !!} </span>
				</div>
				<div style="margin-top:15px;">
					<b>Total Order Amount: </b><span>Rs. {{ $data['totalBookingAmountStr'] }} </span>
				</div>
			</div>
			@if(isset($data['additionalFields']) && count($data['additionalFields']))
				<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
					<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Additional Details</div>
					@foreach($data['additionalFields'] as $additionalFiled)
						<div style="margin-top:15px;">
							<b>{{ $additionalFiled['identifier'] }}: </b><span> @if(isset($additionalFiled['value']) && $additionalFiled['value']) {{ $additionalFiled['value'] }} @else -- @endif </span>
						</div>
					@endforeach
				</div>
			@endif
			<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
				<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Delivery Details</div>
				@if(isset($data['delivery']['zipCode']) && $data['delivery']['zipCode'])
					<div style="margin-top:15px;">
						<b>Pincode: </b><span> {{ $data['delivery']['zipCode'] }} </span>
					</div>
				@endif
				@if(isset($data['delivery']['address']) && $data['delivery']['address'])
					<div style="margin-top:15px;">
						<b>Address: </b><span> {{ $data['delivery']['address'] }} </span>
					</div>
				@endif
				@if(isset($data['delivery']['landmark']) && $data['delivery']['landmark'])
					<div style="margin-top:15px;">
						<b>Landmark: </b><span> {{ $data['delivery']['landmark'] }} </span>
					</div>
				@endif
				@if(isset($data['delivery']['city']) && $data['delivery']['city'])
					<div style="margin-top:15px;">
						<b>City: </b><span> {{ $data['delivery']['city'] }} </span>
					</div>
				@endif
				@if(isset($data['delivery']['state']) && $data['delivery']['state'])
					<div style="margin-top:15px;">
						<b>State: </b><span> {{ $data['delivery']['state'] }} </span>
					</div>
				@endif
				@if(isset($data['delivery']['mapLink']) && $data['delivery']['mapLink'])
					<div style="margin-top:15px;">
						<b>Map Link: </b><span> {{ $data['delivery']['mapLink'] }} </span>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>
<div style="padding-top:10px;font-size:12px;color:#999">If you are receiving the message in Spam or Junk folder, please mark it as 'not spam' and add senders id to contact list or safe list.</div>
