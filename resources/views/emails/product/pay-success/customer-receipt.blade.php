<div style="background-color:#F5F5f5;padding:10px 20px;width:700px;font-size:14px;line-height:22px;">
	<div>
		<div style="padding:20px;">
			<div style='float:left;'>
				<div>
					<a href='http://evibe.in'><img src='http://gallery.evibe.in/img/logo/logo_evibe.png' alt='evibe.in'></a>
				</div>
			</div>
			<div style='float:right;'>
				<div><i>Date:</i> {{ $data['date'] }}</div>
			</div>
			<div style='clear:both;'></div>
		</div>
		<div style="padding:4px; background:#008000; text-align:center;">
			<h2 style="color:#FFFFFF">
				<img src='http://gallery.evibe.in/main/img/icons/check.png' height="20px" width="20px" alt='Success'> Order Placed
			</h2>
		</div>
		<div style="font-size:14px;line-height:22px; padding: 5px 30px 20px; background-color:#FFFFFF;">
			<div style="margin-top:30px; text-align:center; ">
				<div style="font-size:16px;margin:15px 0;">Dear {{ $data['customer']['name'] }},</div>
				<div style="text-align: left;">
					<div style="margin:15px 0;">We have received your payment of
						<b>&#8377; {{ $data['totalAmountPaidStr'] }}</b> for order {{ $data['booking']['bookingOrderId'] }}. You will receive your order by <b> @if(isset($data['delivery']['deliveryDate']) && $data['delivery']['deliveryDate']) {{ date('jS M Y', strtotime($data['delivery']['deliveryDate'])) }} @else {{ date('jS M Y', (time() + (3 * 24 * 60 * 60))) }} @endif</b>.
					</div>
				</div>
				@if(isset($data['tmoLink']) && $data['tmoLink'])
					<div style='padding: 25px 0 25px;text-align:center;'>
						<a href='{{ $data['tmoLink'] }}' style='font-size:20px;text-decoration:none;background-color:#4584ee;color:#ffffff;padding:8px 20px;border-radius:4px;' target='_blank'>
							Track My Order
						</a>
					</div>
				@endif
			</div>
			<div style="margin:15px 0;">
				<div>Happy Partying,</div>
				<div>Team Evibe</div>
			</div>
		</div>
	</div>

	<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
		<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Shipping Details</div>
		<div style="">
			@if(isset($data['delivery']['fullAddress']) && $data['delivery']['fullAddress'])
				<div style="margin-top:15px;">
					<b>Full Address: </b><span> {{ $data['delivery']['fullAddress'] }} </span>
				</div>
			@endif
			@if(isset($data['delivery']['landmark']) && $data['delivery']['landmark'])
				<div style="margin-top:15px;">
					<b>Landmark: </b><span> {{ $data['delivery']['landmark'] }} </span>
				</div>
			@endif
		</div>
	</div>

	<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
		<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Order Details</div>
		<div style="margin-top: 15px;">
			<div style="border: 1px solid #F5F5F5; padding: 10px; margin-bottom: 15px;">
				<div style="">
					<table style="width: 100%;">
						<tr>
							<td style="width: 30%;">
								<div style="height: 50px; text-align: center;">
									<img style="height: 100%; max-width: 100%;" src="{{ $data['booking']['productImgFullUrl'] }}">
								</div>
							</td>
							<td style="width: 50%; font-size: 18px;">
								<div>{!! $data['booking']['bookingInfo'] !!}</div>
							</td>
							<td style="width: 20%; font-size: 18px; text-align: right;">
								<div>&#8377; {{ $data['booking']['productPrice'] }}</div>
							</td>
						</tr>
					</table>
					<hr>
					<table style="width: 100%;">
						<tr style="border-top: 1px solid #EFEFEF;">
							<td style="width: 80%; text-align: right">
								<div>Product Price</div>
								<div>Shipping Fee</div>
								@if(isset($data['totalCouponDiscountAmount']) && $data['totalCouponDiscountAmount'])
									<div>Discount Amount</div>
								@endif
								<div style="margin-top:10px; font-size: 16px; font-weight: 600;">Total Order Amount</div>
							</td>
							<td style="width: 20%; text-align: right">
								<div>&#8377; {{ $data['totalProductAmountStr'] }}</div>
								<div>
									@if(isset($data['totalDeliveryCharge']) && $data['totalDeliveryCharge'])
										<span>
											&#8377; {{ $data['totalDeliveryChargeStr'] }}
										</span>
									@else
										<span style="color: #30AC15; font-size: 13px;">FREE</span>
									@endif
								</div>
								@if(isset($data['totalCouponDiscountAmount']) && $data['totalCouponDiscountAmount'])
									<div style="color: green;">- &#8377; {{ $data['totalCouponDiscountAmount'] }}</div>
								@endif
								<div style="margin-top:10px; font-size: 16px; font-weight: 600;">&#8377; {{ $data['totalAmountPaidStr'] }}</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
		<div style="margin:15px 0;">
			For any queries over refunds or cancellations, kindly write to support@evibe.in
		</div>
	</div>

</div>

<div style="padding-top:10px;font-size:12px;color:#999">If you are receiving the message in Spam or Junk folder, please mark it as 'not spam' and add senders id to contact list or safe list.</div>
