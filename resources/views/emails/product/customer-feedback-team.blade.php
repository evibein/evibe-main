<div style="padding:10px; border:15px solid #f2f2f2">
	<p>Hello Team,</p>

	<p>A customer has a query or gave feedback for our Party In A Box product. Kindly go through the details and do the necessary.</p>

	<div style="margin-top:20px">
		<table style="border-collapse: collapse;" border="1">
			@if(isset($data['feedbackComment']) && $data['feedbackComment'])
				<tr>
					<td style="padding:8px">Question/Feedback</td>
					<td style="padding:8px">{{ $data['feedbackComment'] }}</td>
				</tr>
			@endif
			@if(isset($data['name']) && $data['name'])
				<tr>
					<td style="padding:8px">Name</td>
					<td style="padding:8px">{{ $data['name'] }}</td>
				</tr>
			@endif
			@if(isset($data['phone']) && $data['phone'])
				<tr>
					<td style="padding:8px">Phone</td>
					<td style="padding:8px">{{ $data['phone'] }}</td>
				</tr>
			@endif
			@if(isset($data['email']) && $data['email'])
				<tr>
					<td style="padding:8px">Email</td>
					<td style="padding:8px">{{ $data['email'] }}</td>
				</tr>
			@endif
			@if(isset($data['productStyleCode']) && $data['productStyleCode'])
				<tr>
					<td style="padding:8px">Product Style Code</td>
					<td style="padding:8px">{{ $data['productStyleCode'] }}</td>
				</tr>
			@endif
			@if(isset($data['productInfo']) && $data['productInfo'])
				<tr>
					<td style="padding:8px">Product</td>
					<td style="padding:8px">{!! $data['productInfo'] !!}</td>
				</tr>
			@endif
		</table>
	</div>
	<div style="margin-top:10px">
		Best Regards,
		<div>Admin</div>
	</div>
</div>