<div style="background-color:#FFFFFF;padding:20px;">
	<div>
		<div style='float:left;'>
			<div>
				<a href='http://evibe.in'><img src='http://gallery.evibe.in/img/logo/logo_evibe.png' alt='evibe.in'></a>
			</div>
		</div>
		<div style='float:right;'>
			<div><i>Date:</i> {{ $data['date'] }}</div>
			<div style="padding-top:4px;"><i>Phone:</i> {{ $data['evibePhone'] }} </div>
		</div>
		<div style='clear:both;'></div>
	</div>
	<hr>
	<div style="font-size:14px;line-height:22px;">
		<div style="margin-top:30px;">
			<p>Hi Team,</p>
			<p>We’ve received an auto booking for Decor from a customer. Please check complete details below.</p>
			<p>Next, check availability of the provider and confirm the decor availability to the customer along with next steps to complete booking.
				If not available, please call the customer to provide alternate options.
			</p>
		</div>
	</div>
	<div style="margin-top:15px">
		<div>Best,</div>
		<div>Admin</div>
	</div>
</div>