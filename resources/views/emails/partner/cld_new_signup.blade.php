<div>
	<p style="margin-bottom: 20px;">Hello Team,</p>
	<p>Congrats, we have a CLD partner sign-up request. Check the details below</p>
	<div style="margin-top:10px">
		<table style="border-collapse: collapse;" border="1">
			<tr>
				<td style="padding:8px">Name</td>
				<td style="padding:8px">{{ $data['name'] }}</td>
			</tr>
			<tr>
				<td style="padding:8px">City</td>
				<td style="padding: 8px">{{ $data['city'] }}</td>
			</tr>
			@if($data["isFriend"] == 1)
				<tr>
					<td style="padding:8px">Friend Name</td>
					<td style="padding: 8px">{{ $data['friendName'] }}</td>
				</tr>
				<tr>
					<td style="padding:8px">Friend Phone</td>
					<td style="padding: 8px">{{ $data['phone'] }}</td>
				</tr>
			@else
				<tr>
					<td style="padding:8px">Phone</td>
					<td style="padding: 8px">{{ $data['phone'] }}</td>
				</tr>
				<tr>
					<td style="padding:8px">Restaurant Name</td>
					<td style="padding: 8px">{{ $data['restaurantName'] }}</td>
				</tr>
			@endif
		</table>
	</div>
	<div style="margin-top:10px">
		Best Regards,
		<div>Admin</div>
	</div>
</div>