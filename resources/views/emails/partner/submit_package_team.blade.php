<div style="padding:10px; border:15px solid #f2f2f2">
	<p>Hello Team,</p>
	<p>{{ $data['partner']->person }} has @if($data['is_update'] == 1){{"updated a package"}}@else{{"added a new package"}}@endif from partner portal. Please go through the details & review it as soon as possible.</p>
	<div style="margin-top:10px">
		<table style="border-collapse: collapse;" border="1">
			<tr>
				<td style="padding:8px">Name</td>
				<td style="padding:8px">{{ $data['package']->package_name }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Worth</td>
				<td style="padding: 8px">{{ $data['package']->worth }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Price</td>
				<td style="padding: 8px">{{ $data['package']->price }}</td>
			</tr>
			<tr>
				<td style="padding:8px">Inclusions</td>
				<td style="padding: 8px">{!! $data['package']->inclusions !!}</td>
			</tr>
			<tr>
				<td style="padding:8px">Dash Link</td>
				<td style="padding: 8px">{{ config('evibe.dash_host').'/partner/add/new-package/'.$data['package']->id }}</td>
			</tr>
		</table>
	</div>
	<div style="margin-top:10px">
		Best Regards,
		<div>Admin</div>
	</div>
</div>