<p>We've encountered an error while submitting an Ajax request. Check below for details and fix it immediately.</p>

<p><em>URL: </em> {{ $url }}</p>
<p><em>Text Status: </em> {{ $textStatus }}</p>
<p><em>Error Thrown: </em> {{ $errorThrown }}</p>
<p><em>Browser Info: </em> {{ $browserInfo }}</p>
<p><em>Previous Url: </em> {!! $previousUrl !!}</p>