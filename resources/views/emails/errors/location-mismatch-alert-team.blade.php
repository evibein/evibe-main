<p>Hello Team,</p>

<p>
	Party Location provided by us has been updated by the customer for the below ticket. Kindly check with the customer and confirm the party location details.
</p>

<table style="border-collapse: collapse;text-align: left;">
	<tr>
		<th colspan="2" style="border: 1px solid #dddddd; padding: 8px; text-align: center;">Ticket Details</th>
	</tr>
	@if(isset($data['enquiryId']) && $data['enquiryId'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Ticket Enquiry Id</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['enquiryId'] }}</td>
		</tr>
	@endif
	@if(isset($data['name']) && $data['name'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Name</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['name'] }}</td>
		</tr>
	@endif
	@if(isset($data['phone']) && $data['phone'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Mobile No</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['phone'] }}</td>
		</tr>
	@endif
	@if(isset($data['preDefinedArea']) && $data['preDefinedArea'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Location given by us</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['preDefinedArea'] }}</td>
		</tr>
	@endif
	@if(isset($data['preDefinedAreaId']) && $data['preDefinedAreaId'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Id of the location given by us</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['preDefinedAreaId'] }}</td>
		</tr>
	@endif
	@if(isset($data['modifiedArea']) && $data['modifiedArea'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Location updated by customer</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['modifiedArea'] }}</td>
		</tr>
	@endif
	@if(isset($data['modifiedAreaId']) && $data['modifiedAreaId'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Id of the location updated by customer</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['modifiedAreaId'] }}</td>
		</tr>
	@endif
</table>

<p>
	Best Regards,<br>
	Admin
</p>