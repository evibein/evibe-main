<p>Hello Team,</p>

<p>
	There is some problem while calculation transportation charges for the following option.
	Please check all the details below and make sure all the required fields like zip,
	kms_free, kms_max, trans_min, trans_max etc are updated.
</p>

<table style="border-collapse: collapse;text-align: left;">
	<tr>
		<th colspan="2" style="border: 1px solid #dddddd; padding: 8px; text-align: center;">Option Details</th>
	</tr>
	@if(isset($data['optionInfo']) && $data['optionInfo'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Option Info</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['optionInfo'] }}</td>
		</tr>
	@endif
	@if(isset($data['optionCode']) && $data['optionCode'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Option Code</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['optionCode'] }}</td>
		</tr>
	@endif
	@if(isset($data['typeTicket']) && $data['typeTicket'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Option Type</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['typeTicket'] }}</td>
		</tr>
	@endif
	@if(isset($data['providerName']) && $data['providerName'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Provider Name</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['providerName'] }}</td>
		</tr>
	@endif
	@if(isset($data['providerCode']) && $data['providerCode'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Provider Code</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['providerCode'] }}</td>
		</tr>
	@endif
	@if(isset($data['typeProvider']) && $data['typeProvider'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Type Provider</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['typeProvider'] }}</td>
		</tr>
	@endif
</table>

<p>
	Best Regards,<br>
	Admin
</p>