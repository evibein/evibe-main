<div style='padding:10px'>
	<p>Hi Team,</p>
	<p>please go through the details and fix it immediately.</p>
	<table border='1' style='border-collapse: collapse; margin-top:10px;width:100%'>
		<tr>
			<td style='padding:8px'>Url</td>
			<td style='padding:8px'>
				@if(isset($data['url']))
					{{ $data['url'] }}
				@else
					--
				@endif
			</td>
		</tr>
		<tr>
			<td style='padding:8px'>Code</td>
			<td style='padding:8px'>
				@if(isset($data['code']))
					{{ $data['code'] }}
				@else
					--
				@endif
			</td>
		</tr>
		<tr>
			<td style='padding:8px'>Previous Url</td>
			<td style='padding:8px'>
				@if(isset($data['previousUrl']))
					{!! $data['previousUrl'] !!}
				@else
					--
				@endif
			</td>
		</tr>
		<tr>
			<td style='padding:8px'>Method</td>
			<td style='padding:8px'>
				@if(isset($data['method']))
					{{ $data['method'] }}
				@else
					--
				@endif
			</td>
		</tr>
		<tr>
			<td style='padding:8px'>Message</td>
			<td style='padding:8px'>
				@if(isset($data['message']))
					{{ $data['message'] }}
				@else
					--
				@endif
			</td>
		</tr>
		<tr>
			<td style='padding:8px'>Trace</td>
			<td style='padding:8px'>
				@if(isset($data['trace']))
					{{ $data['trace'] }}
				@else
					--
				@endif
			</td>
		</tr>
	</table>
	<p>
	<div>Admin</div>
	</p>
</div>