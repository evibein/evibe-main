@extends('layout.base')

@section('page-title')
	<title>Error 404. Page not found | Evibe.in</title>
@endsection

@section("header")
	<header>
		@include('base.home.header.header-home-city')
		<div class="header-border"></div>
	</header>
@endsection

@include('app.modals.enquiry-form', [
  		"url" => route("city.header.enquire", "no-city")
  	])

@section("content")
	<article>
		<div class="col-sm-10 col-md-10 col-lg-10 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
			<section class="page-sec error-page row">
				<div class="sec-body col-md-12 no-pad">
					<div class="top-sec">
						<div class="in-blk col-md-4 text-center no-pad">
							<img src="{{ $galleryUrl }}/main/img/icons/error-404.png" alt="Error page">
						</div>
						<div class="in-blk side-message col-md-8 mar-t-40">
							<h1 class="error-title">Oops... page not found :(</h1>
							<p class="error-msg">Sorry, the page you are looking for could not be found. The page might have been moved, temporarily unavailalbe or never existed.</p>
						</div>
						<div class="bottom-sedc">
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="mid-sec text-left">
						<h1 class="sec-title">Never Mind</h1>
						<p class="never-message">Just tell us what you need, we will provide you with the best vendor recommendations ranging from party halls to individual artists, completely based on your preferences.</p>
						<div class="pad-t-10">
							<button class="btn btn-md btn-danger text-upr btn-error-page-post" data-formtype="{{ config('evibe.ticket.type.error') }}">Post Your Requirements
							</button>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="clearfix"></div>
	</article>
@endsection

@section("footer")
	<div class="footer-wrap">
		@include('base.home.footer.footer-common')
	</div>
@endsection