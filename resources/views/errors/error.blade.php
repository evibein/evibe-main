@extends('layout.base')

@section("header")
	@include("base.home.header.header-home-city")
	<div class="header-border"></div>
@endsection

@section("footer")
	@include("base.home.footer")
@endsection

@section("content")
	<div class="mar-10">
		<div class="alert alert-danger text-center col-xs-12 col-sm-4 col-sm-offset-4">
			<i class="glyphicon glyphicon-exclamation-sign"></i> Looks like something went wrong on our end.<br>
			<p class="pad-t-20">
				Please contact Evibe.in account manager.<br>
				<i class="glyphicon glyphicon-earphone"></i> {{ config("evibe.contact.company.phone") }} / <i class="glyphicon glyphicon-envelope"></i> {{ config("evibe.contact.company.email") }}.
			</p>
		</div>
	</div>
@endsection