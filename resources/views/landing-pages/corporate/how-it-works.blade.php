<div class="section-how-it-works section-how-it-works-corporate" style="background-color: #fff">
	<div class="pad-l-20 pad-r-20">
		<h4 class="sec-title title-text text-center top-0 mar-b-10">How it Works ?</h4>
		<div class="benefits-lists top-30 text-col-gr box-shadow">
			<div class="col-md-55 no-mar-b pos-rel benefit steps shadow-steps">
				<div class="ben-desc">
					<div class="text-e font-16 pad-b-5">Submit Enquiry</div>
					Just submit your high-level requirements with your contact details.
				</div>
			</div>
			<div class="col-md-55 no-mar-b pos-rel benefit steps " style="background:#383838;">
				<div class="arrow-right"></div>
				<div class="ben-desc">
					<div class="text-e font-16 pad-b-5">Free Site Visit</div>
					Our event expert will visit you at your place to discuss all your requirements and clarify all your questions. No obligation to book.
				</div>
			</div>
			<div class="col-md-55 no-mar-b pos-rel benefit steps shadow-steps">
				<div class="arrow-right arrow-black"></div>
				<div class="ben-desc">
					<div class="text-e font-16 pad-b-5">Get Best Quote</div>
					Based on your requirements, we will share the best quote within 1 Business day.
				</div>
			</div>
			<div class="col-md-55 no-mar-b pos-rel benefit steps" style="background:#383838;">
				<div class="arrow-right"></div>
				<div class="ben-desc">
					<div class="text-e font-16 pad-b-5">Book Seamlessly</div>
					If you are satisfied with our quote, you securely pay the booking amount to block your slot.
				</div>
			</div>
			<div class="col-md-55 no-mar-b pos-rel benefit steps shadow-steps">
				<div class="arrow-right arrow-black"></div>
				<div class="ben-desc">
					<div class="text-e font-16 pad-b-5">Relax & Party</div>
					A coordinator will be assigned to your event who will take care of the entire event.
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>