@if($agent->isMobile())
<div class="corporate-service-wrap mar-t-10" style="background-color: white">
				<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 single-service-wrap height-180-imp" >
					<div class="height-180-imp">
						<section class="text-center pad-t-10">
							<img class="in-blk" src="{{ $galleryUrl }}/main/pages/corporate/ballondecor.png" width="80px" height="80px">
						</section>
						<h6 class="text-bold text-center">Balloon Decor</h6>

					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 single-service-wrap height-180-imp" >
					<div class="height-180-imp">
						<section class="text-center pad-t-10">
							<img class="in-blk" src="{{ $galleryUrl }}/main/pages/corporate/activities.png" width="80px" height="80px">
						</section>
						<h6 class="text-bold text-center">Activities</h6>

					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 single-service-wrap height-180-imp" >
					<div class="height-180-imp">
						<section class="text-center pad-t-10">
							<img class="in-blk" src="{{ $galleryUrl }}/main/pages/corporate/funeats.png" width="80px" height="80px">
						</section>
						<h6 class="text-bold text-center">Fun Eats</h6>

					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 single-service-wrap height-180-imp" >
					<div class="height-180-imp">
						<section class="text-center pad-t-10">
							<img class="in-blk" src="{{ $galleryUrl }}/main/pages/corporate/artists.png" width="80px" height="80px">
						</section>
						<h6 class="text-bold text-center">Artists</h6>

					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 single-service-wrap height-180-imp" >
					<div class="height-180-imp">
						<section class="text-center pad-t-10">
							<img class="in-blk" src="{{ $galleryUrl }}/main/pages/corporate/snacks.png" width="80px" height="80px">
						</section>
						<h6 class="text-bold text-center">Snacks Pack</h6>

					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 single-service-wrap height-180-imp" >
					<div class="height-180-imp">
						<section class="text-center pad-t-10">
							<img class="in-blk" src="{{ $galleryUrl }}/main/pages/corporate/cakes.png" width="80px" height="80px">
						</section>
						<h6 class="text-bold text-center">Cake</h6>
					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 single-service-wrap height-180-imp" >
					<div class="height-180-imp">
						<section class="text-center pad-t-10">
							<img class="in-blk" src="{{ $galleryUrl }}/main/pages/corporate/returnGifts.png" width="80px" height="80px">
						</section>
						<h6 class="text-bold text-center">Return Gifts</h6>
					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 single-service-wrap height-180-imp" >
					<div class="height-180-imp">
						<section class="text-center pad-t-10">
							<img class="in-blk" src="{{ $galleryUrl }}/main/pages/corporate/rentals.png" width="80px" height="80px">
						</section>
						<h6 class="text-bold text-center">Rentals</h6>
					</div>
				</div>
				<div class="clearfix"></div>
</div>

@else
<div class="corporate-service-wrap mar-t-10" style="background-color: white">
				<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 single-service-wrap">
					<div class="inner-item-div">
						<section class="text-center pad-t-10">
							<img class="in-blk" src="{{ $galleryUrl }}/main/pages/corporate/ballondecor.png" width="80px" height="80px">
						</section>
						<h6 class="text-bold text-center">Balloon Decor</h6>
						<ul class="text-center">
							<li>Basic balloon Decor</li>
							<li>Balloon Decor with photos</li>
							<li>Balloon Decor with danglers</li>
							<li>Balloon Decor with pom poms</li>
						</ul>
					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 single-service-wrap">
					<div class="inner-item-div">
						<section class="text-center pad-t-10">
							<img class="in-blk" src="{{ $galleryUrl }}/main/pages/corporate/activities.png" width="80px" height="80px">
						</section>
						<h6 class="text-bold text-center">Activities</h6>
						<ul class="text-center">
							<li>Photo Booth</li>
							<li>Nail Art</li>
							<li>Caricature</li>
							<li>Hair Beading</li>
						</ul>
					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 single-service-wrap">
					<div class="inner-item-div">
						<section class="text-center pad-t-10">
							<img class="in-blk" src="{{ $galleryUrl }}/main/pages/corporate/funeats.png" width="80px" height="80px">
						</section>
						<h6 class="text-bold text-center">Fun Eats</h6>
						<ul class="text-center">
							<li>Popcorn Counter</li>
							<li>Cotton Candy</li>
							<li>Chocolate Fountain</li>
							<li>Ice Gola</li>
						</ul>
					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 single-service-wrap">
					<div class="inner-item-div">
						<section class="text-center pad-t-10">
							<img class="in-blk" src="{{ $galleryUrl }}/main/pages/corporate/artists.png" width="80px" height="80px">
						</section>
						<h6 class="text-bold text-center">Artists</h6>
						<ul class="text-center">
							<li>Emcee</li>
							<li>Magicians</li>
							<li>Dancers</li>
							<li>DJ</li>
						</ul>
					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 single-service-wrap">
					<div class="inner-item-div">
						<section class="text-center pad-t-10">
							<img class="in-blk" src="{{ $galleryUrl }}/main/pages/corporate/snacks.png" width="80px" height="80px">
						</section>
						<h6 class="text-bold text-center">Snacks Pack</h6>
						<ul class="text-center">
							<li>Mini pizza and juice pack</li>
							<li>Sandwich / Samosa and juice pack</li>
						</ul>
					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 single-service-wrap">
					<div class="inner-item-div">
						<section class="text-center pad-t-10">
							<img class="in-blk" src="{{ $galleryUrl }}/main/pages/corporate/cakes.png" width="80px" height="80px">
						</section>
						<h6 class="text-bold text-center">Cake</h6>
						<ul class="text-center">
							<li>Photo cake</li>
							<li>Inspirational theme cake</li>
						</ul>
					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 single-service-wrap">
					<div class="inner-item-div">
						<section class="text-center pad-t-10">
							<img class="in-blk" src="{{ $galleryUrl }}/main/pages/corporate/returnGifts.png" width="80px" height="80px">
						</section>
						<h6 class="text-bold text-center">Return Gifts</h6>
						<ul class="text-center">
							<li>Roses + Greeting Cards</li>
							<li>Chocolate box</li>
						</ul>
					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 single-service-wrap">
					<div class="inner-item-div">
						<section class="text-center pad-t-10">
							<img class="in-blk" src="{{ $galleryUrl }}/main/pages/corporate/rentals.png" width="80px" height="80px">
						</section>
						<h6 class="text-bold text-center">Rentals</h6>
						<ul class="text-center">
							<li>Sound System</li>
							<li>Lights</li>
						</ul>
					</div>
				</div>
				<div class="clearfix"></div>
</div>
@endif