@extends('layout.base')

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-landing-pages.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/app/jquery-mosaic.css') }}"/>
@endsection

@section('page-title')
	<title>{{ $data['seo']['pageTitle'] }} | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="{{ $data['seo']['pageDesc'] }}"/>
@endsection

@section('meta-keywords')
	<meta name="keywords" content="{{ $data['seo']['keyWords'] }}"/>
@endsection

@section('og-title')
	<meta property="og:title" content="{{ $data['seo']['pageTitle'] }} | Evibe.in"/>
@endsection

@section('og-description')
	<meta property="og:description" content="{{ $data['seo']['pageDesc'] }}"/>
@endsection

@section('og-url')
	<meta property="og:url" content="{{ route('city.occasion.corporate.home', [getCityUrl()]) }}"/>
@endsection

@section("header")
	@include('base.home.header.header-home-city')
@endsection

@section("footer")
	<div class="footer-wrap">
		@include('base.home.footer.footer-common')
	</div>
@endsection

@section("content")
	<div class="corporate-wrapper">
		<section class="corporate-sec corporate-carousel-wrap pos-rel" style="height: 380px">
			<div>
				<span class="corporate-punch-line-one" @if(!($agent->isMobile())) style="top:33%" @endif>Effortlessly plan your office events</span>
				<span class="corporate-punch-line-two @if($agent->isMobile()) pad-t-10 @endif">Send Requirements. Get Best Quote. Book. Relax & Party</span>
				<div class="corporate-top-enquire-btn">
					<a class="btn btn-evibe btn-lg corporate-enq-btn">
						Enquire Now
					</a>
				</div>
			</div>
			<div id="corporate-carousel" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#corporate-carousel" data-slide-to="0" class="active"></li>
					<li data-target="#corporate-carousel" data-slide-to="1"></li>
					<li data-target="#corporate-carousel" data-slide-to="2"></li>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner corporate-carousel-inner" role="listbox">
					<div class="item active">
						<img src="{{ $galleryUrl }}/main/pages/corporate/hero3.jpg" alt="corporate conference">
					</div>
					<div class="item">
						<img src="{{ $galleryUrl }}/main/pages/corporate/hero1.jpg" alt="corporate outing">
					</div>
					<div class="item">
						<img src="{{ $galleryUrl }}/main/pages/corporate/hero4.jpeg" alt="corporate decorations">
					</div>
				</div>

				<!-- Controls -->
				<a class="left carousel-control" href="#corporate-carousel" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#corporate-carousel" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</section>
			<section class="corporate-sec corporate-why-us-wrap">
			@include('landing-pages.corporate.why-us')
		</section>
		<section class="corporate-sec corporate-client-logo-wrap pad-t-30 pad-b-30">
			<h4 class="text-center">Our Happy Clients</h4>
			<div class="col-xs-10 col-sm-10 col-xs-offset-1 col-sm-offset-1 no-pad mar-t-10">
				<div class="col-md-3 col-sm-3 col-xs-6 corporate-client-logos">
					<div>
						<img src="{{ $galleryUrl }}/main/pages/corporate/amazon.png">
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 corporate-client-logos">
					<div>
						<img src="{{ $galleryUrl }}/main/pages/corporate/cipla.png">
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 corporate-client-logos">
					<div>
						<img src="{{ $galleryUrl }}/main/pages/corporate/ericsson.png">
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 corporate-client-logos">
					<div>
						<img src="{{ $galleryUrl }}/main/pages/corporate/expedia.png">
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 corporate-client-logos">
					<div>
						<img src="{{ $galleryUrl }}/main/pages/corporate/fujitsu.png">
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 corporate-client-logos">
					<div>
						<img src="{{ $galleryUrl }}/main/pages/corporate/hp.png">
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 corporate-client-logos">
					<div>
						<img src="{{ $galleryUrl }}/main/pages/corporate/hungrybox.png">
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 corporate-client-logos">
					<div>
						<img src="{{ $galleryUrl }}/main/pages/corporate/ilabs.png">
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 corporate-client-logos">
					<div>
						<img src="{{ $galleryUrl }}/main/pages/corporate/knoah.png">
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 corporate-client-logos">
					<div>
						<img src="{{ $galleryUrl }}/main/pages/corporate/morganstanley.png">
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 corporate-client-logos">
					<div>
						<img src="{{ $galleryUrl }}/main/pages/corporate/netgear.png">
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 corporate-client-logos">
					<div>
						<img src="{{ $galleryUrl }}/main/pages/corporate/netscout.png">
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</section>

			<div class="text-center no-pad-t pad-b-10">
				<a class="btn btn-evibe btn-lg corporate-enq-btn">
					Enquire Now
				</a>
			</div>


		<section class="corporate-sec corporate-reviews-wrap">
			@include('landing-pages.corporate.customer-reviews')
		</section>


		@if(!($agent->isMobile()))
			<div class="seperator-div"></div>
		@endif
		<section class="corporate-sec corporate-how-it-works-wrap">
			@include('landing-pages.corporate.how-it-works')
		</section>

		<section class="corporate-sec corporate-occasion-wrap">
			<div class="pos-rel">
				@if($agent->isMobile())
					<img class="full-width pos-abs" src="{{ $galleryUrl }}/main/pages/corporate/meeting/4.jpg">

				@else
					<img class="full-width pos-abs" src="{{ $galleryUrl }}/main/pages/corporate/occasion.jpg">
				@endif
			</div>
			<div>
				<h4 class="corporate-occasion-header text-center">Events Handled</h4>
				<p class="corporate-services-sub-heading pad-l-10 pad-r-10" >We understand the nuances of office events planning. Major firms of the country trust us for various events & celebration as we deliver nothing but excellence.</p>
				<div class="text-center corporate-occasion-big-wrap">
					<div class="col-md-55 ">
						<h6>Annual Day</h6>
					</div>
					<div class="col-md-55">
						<h6>Awards & Recognition</h6>
					</div>
					<div class="col-md-55">
						<h6>Product Launches</h6>
					</div>
					<div class="col-md-55">
						<h6>Seminar / Conferences</h6>
					</div>
					<div class="col-md-55">
						<h6>Family Day</h6>
					</div>
					<div class="clearfix"></div>
				</div>
				@if(!($agent->isMobile()))
				<div class="corporate-occasion-small-wrap text-center">
					<ul class="font-18">
						<li>Independence Day</li>
						<li>X-mas & New Year</li>
						<li>Diwali</li>
						<li>Women's Day</li>
						<li>Halloween</li>
						<li>Children's Day</li>
						<li>Sports Day</li>
						<li>IPL screening</li>
					</ul>
				</div>
				@endif
			</div>
		</section>
		<section class="corporate-sec corporate-services-wrap">

			@include('landing-pages.corporate.services')

		</section>
		<div class="text-center pad-t-20 pad-b-20">
				<a class="btn btn-evibe btn-lg corporate-enq-btn">
					Enquire Now
				</a>
		</div>
		<section class="corporate-sec corporate-gallery-wrap">
			<h4 class="text-center no-pad-t no-mar-t pad-b-10">Our Past Events</h4>
			@if($agent->isMobile())
				<div id="galleryCarousel" class="carousel slide hide" data-ride="carousel">
				  	<!-- Indicators -->
					<ol class="carousel-indicators">
							<li data-target="#galleryCarousel" data-slide-to="0" class="active"></li>

						    @for($i=1;$i<=7;$i++)
								<li data-target="#galleryCarousel" data-slide-to="{{$i}}"></li>
						    @endfor
					</ol>

				  <!-- Wrapper for slides -->
				  <div class="carousel-inner">
				    <div class="item active">
				      <img src="{{ $galleryUrl }}/main/pages/corporate/meeting/1.jpg" alt="Los Angeles">
				    </div>
					<div class="item">
				      <img src="{{ $galleryUrl }}/main/pages/corporate/meeting/2.jpg" alt="Chicago">
				    </div>
				    <div class="item">
				      <img src="{{ $galleryUrl }}/main/pages/corporate/meeting/3.jpg" alt="Los Angeles">
				    </div>
					<div class="item">
				      <img src="{{ $galleryUrl }}/main/pages/corporate/meeting/4.jpg" alt="Chicago">
				    </div>
				    <div class="item">
				      <img src="{{ $galleryUrl }}/main/pages/corporate/outing/1.jpg" alt="Chicago">
				    </div>
				    <div class="item">
				      <img src="{{ $galleryUrl }}/main/pages/corporate/outing/2.jpg" alt="Chicago">
				    </div>
				    <div class="item">
				      <img src="{{ $galleryUrl }}/main/pages/corporate/outing/3.jpg" alt="Chicago">
				    </div>
				    <div class="item">
				      <img src="{{ $galleryUrl }}/main/pages/corporate/outing/4.jpg" alt="Chicago">
				    </div>
				  </div>
				</div>

			@else
			<div class="pad-l-20 pad-r-20 pad-t-20 corporate-gallery-nav hide">
				<div id="myMosaic"></div>
			</div>
			@endif
			<div class="clearfix"></div>
		</section>
		<div class="text-center pad-b-20" style="background-color: #f6f6f6">
				<a class="btn btn-evibe btn-lg corporate-enq-btn">
					Enquire Now
				</a>
			</div>
		<section class="corporate-sec evibe-media">
			@include("base.home.press")
		</section>
	</div>
	<div class="hide corporate-loading-gallery-images">
		<div data-image-url="{{ $galleryUrl }}/main/pages/corporate/meeting/1.jpg" data-width="245" data-height="196"></div>
		<div data-image-url="{{ $galleryUrl }}/main/pages/corporate/meeting/2.jpg" data-width="208" data-height="125"></div>
		<div data-image-url="{{ $galleryUrl }}/main/pages/corporate/meeting/3.jpg" data-width="256" data-height="171"></div>
		<div data-image-url="{{ $galleryUrl }}/main/pages/corporate/meeting/4.jpg" data-width="256" data-height="125"></div>
		<div data-image-url="{{ $galleryUrl }}/main/pages/corporate/outing/1.jpg" data-width="256" data-height="171"></div>
		<div data-image-url="{{ $galleryUrl }}/main/pages/corporate/outing/2.jpg" data-width="171" data-height="256"></div>
		<div data-image-url="{{ $galleryUrl }}/main/pages/corporate/outing/3.jpg" data-width="256" data-height="192"></div>
		<div data-image-url="{{ $galleryUrl }}/main/pages/corporate/outing/4.jpg" data-width="216" data-height="239"></div>
		<div data-image-url="{{ $galleryUrl }}/main/pages/corporate/launches/1.jpg" data-width="192" data-height="144"></div>
		<div data-image-url="{{ $galleryUrl }}/main/pages/corporate/launches/2.jpg" data-width="208" data-height="156"></div>
		<div data-image-url="{{ $galleryUrl }}/main/pages/corporate/launches/3.jpg" data-width="256" data-height="192"></div>
		<div data-image-url="{{ $galleryUrl }}/main/pages/corporate/launches/4.jpeg" data-width="231" data-height="173"></div>
		<div data-image-url="{{ $galleryUrl }}/main/pages/corporate/launches/5.jpeg" data-width="231" data-height="173"></div>
		<div data-image-url="{{ $galleryUrl }}/main/pages/corporate/launches/6.jpeg" data-width="231" data-height="173"></div>
		<div data-image-url="{{ $galleryUrl }}/main/pages/corporate/launches/7.jpeg" data-width="231" data-height="173"></div>
	</div>
@endsection

@section("modals")
	@parent
	@include('app.modals.corporate-enquiry', ["url" => route('city.occasion.corporate.enquiry.save', [getCityUrl()])])
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			var mosaicJs = "<?php echo elixir('js/app/jquery-mosaic.js'); ?>";
			$.getScript(mosaicJs,function(){
				var imageTemplate = "";
				$('.corporate-loading-gallery-images div').each(function () {
					imageTemplate += "<img class='gallery-images lazy-loading' data-src='"+$(this).data("image-url")+"' width='"+$(this).data("width")+"' height='"+$(this).data("height")+"'>";

				});
				$("#myMosaic").append(imageTemplate);
				$("#galleryCarousel").removeClass("hide");
				$('.corporate-gallery-nav').removeClass('hide');
				$('#myMosaic').Mosaic();
				if ($(".gallery-images").length > 0) {
					$.getScript("/js/util/lazyLoadImages.js", function () {
						$(".gallery-images").removeClass("lazy-loading");
						new LazyLoad();
					});
				}


			});

			(function loadCarouselSwipe() {
				$.getScript("/js/util/bootstrapCarousalSwipe.js", function () {
					$('#galleryCarousel').carousel({
						interval: 5000,
						pause: true
					}).bcSwipe({threshold: 50});
				});
			})();
		});
	</script>
@endsection