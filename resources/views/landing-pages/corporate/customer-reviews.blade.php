<div class="section-feedback" style="background-color: #fff"> <!-- adding check inside so that background color will be restored -->
	<div id="cust-slide" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner" style="padding-top:0px">
			<div class="item row active">
				<div class="col-xs-10 col-sm-10 col-xs-offset-1 col-sm-offset-1">

					<div class="corporate-customer-review text-center pad-5">
						" We have already booked 3 times with Evibe. We celebrated our company anniversary, Children's' day and a product success. We are very happy with their services."
					</div>
					<div class="col-md-8 col-md-offset-2">
						<div class="col-md-12 text-center no-padding no-margin">
							<h6 class="cust-name no-margin">Mrs Punita Gupta</h6>
							<span>Tally Solutions, Bangalore</span>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="item row">
				<div class="col-xs-10 col-sm-10 col-xs-offset-1 col-sm-offset-1">

					<div class="corporate-customer-review text-center pad-5">
						" We hired Evibe.in for Republic Day 2018. We were completely impressed with the on-time delivery and the professionalism. Everyone appreciated the ambience and it was absolutely effortless."
					</div>
					<div class="col-md-8 col-md-offset-2">
						<div class="col-md-12 text-center no-padding no-margin">
							<h6 class="cust-name no-margin">Mr Ramesh</h6>
							<span>iLabs, Hyderabad</span>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="item row">
				<div class="col-xs-10 col-sm-10 col-xs-offset-1 col-sm-offset-1">

					<div class="corporate-customer-review text-center pad-5">
						" On behalf of HR team, I would like to extend our appreciation for the splendid work your team has done on the Halloween event, which was a huge success."
					</div>
					<div class="col-md-8 col-md-offset-2">
						<div class="col-md-12 text-center no-padding no-margin">
							<h6 class="cust-name no-margin">Mrs Sruthi Sundesh</h6>
							<span>Knoah Solutions, Hyderabad</span>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="item row">
				<div class="col-xs-10 col-sm-10 col-xs-offset-1 col-sm-offset-1">

					<div class="corporate-customer-review text-center pad-5">
						" It was a good experience with Evibe, we found a perfect solution to our problem. Everything was arranged on a short notice."
					</div>
					<div class="col-md-8 col-md-offset-2">
						<div class="col-md-12 text-center no-padding no-margin">
							<h6 class="cust-name no-margin">Mr Sumit Jain</h6>
							<span>Commonfloor, Bangalore</span>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<a class="carousel-control corporate-review-prev" href="#cust-slide" data-slide="prev">
			<img src="https://gallery.evibe.in/img/icons/icon_left_thin.png" alt="Prev">
		</a>
		<a class="carousel-control corporate-review-next" href="#cust-slide" data-slide="next">
			<img src="https://gallery.evibe.in/img/icons/icon_right_thin.png" alt="Next">
		</a>
	</div>

</div>

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			$('.cust-comments-text').each(function (i, el) {
				var $el = $(el);
				var story = $el.text();
				var maxLength = 400;
				if (story.length >= maxLength) {
					var lastSpaceIndex = story.substr(0, maxLength).lastIndexOf(' ');
					$el.text(story.substr(0, lastSpaceIndex) + ' ...');
				}
			});
		});
	</script>
@endsection