@if($agent->isMobile())
	<div class="section-why-us" style="background-color:white;padding-bottom: 0px;padding-top: 30px">
		<div class="col-xs-6 text-center usp-mbl-div">
				<img src="{{ $galleryUrl }}/main/img/icons/customers.png" class="usp-mbl-img"   alt="Office Events">
				<div class="text-center">
						<div class="title-text font-14">200+ successful office events done </div>
				</div>
		</div>
		<div class="col-xs-6 text-center usp-mbl-div">
				<img src="{{ $galleryUrl }}/main/img/icons/company.png" class="usp-mbl-img" alt="Top MNCs">
				<div class="text-center">
						<div class="title-text font-14">Worked with 50+ A list companies </div>
				</div>
		</div>
		<div class="col-xs-6 text-center usp-mbl-div">
				<img src="{{ $galleryUrl }}/main/img/icons/guarantee.png" class="usp-mbl-img" alt="Top Rated">
				<div class="text-center">
						<div class="title-text font-14">Top-rated online party planner of India </div>
				</div>
		</div>
		<div class="col-xs-6 text-center usp-mbl-div">
				<img src="{{ $galleryUrl }}/main/img/icons/packages.png" class="usp-mbl-img" alt="Best Quality">
				<div class="text-center">
						<div class="title-text font-14">Best Quality At Best Price </div>
				</div>
		</div>
		<div class="clearfix"></div>
	</div>
@else
<div class="section-why-us" style="background-color:white;">
	<div class="container">
		<div class="row">
			<div class="benefits-lists padding-30">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-6 text-center benefit">
					<img src="{{ $galleryUrl }}/main/img/icons/customers.png" alt="100% Delivery">
					<div class="text-center">
						<div class="ben-title why-us-title title-text">200+ successful office events done </div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-6 text-center benefit">
					<img src="{{ $galleryUrl }}/main/img/icons/company.png" alt="Best Offerings">
					<div class="text-center">
						<div class="ben-title why-us-title title-text">Worked with 50+ A list companies</div>

					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-6 text-center benefit">
					<img src="{{ $galleryUrl }}/main/img/icons/guarantee.png" alt="Easy Booking on Evibe">
					<div class="text-center">
						<div class="ben-title why-us-title title-text">Top-rated online party planner of India</div>

					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-6 text-center benefit">
					<img src="{{ $galleryUrl }}/main/img/icons/packages.png" alt="Total happy party hosts">
					<div class="text-center">
						<div class="ben-title why-us-title title-text">Best Quality At Best Price</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
@endif