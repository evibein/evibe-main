<div class="hide__400 hide-400-600">
	<div class="landing-body">
		<div class="landing-body-section-form">
			<div class="col-lg-6 col-md-6 no-pad">
				<div class="landing-body-sneak-peaks-wrap text-center">
					@foreach($data['sneakPeaks'] as $sneakPeak)

						@if($loop->index % 2== 0)
							<div class="landing-sneak-peaks-block">
								@endif
								<div class="col-md-6 col-lg-6 no-pad sneak-peak-item">
									<img class="landing-body-image-box" src="{{ $galleryUrl. $sneakPeak["img"] }}">
									<div class="landing-images-text">
										{{ $sneakPeak["title"] }}
									</div>
								</div>
								@if($loop->index % 2!= 0)
									<div class="clearfix"></div>
							</div>
						@endif
					@endforeach
					<div class="landing-sneak-peaks-block">
						<div class="landing-sneak-peak-more">
							& many more
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 no-pad">
				<div class="landing-body-form-wrap">

					<form class="landing-enquiry-form" id="landingEnquiryForm" data-url="{{ route('ga.landing.enquiry') }}">
						<div class="landing-form-head">
							<div class="landing-form-head-text text-e">
								<div class="in-blk"><i class="glyphicon glyphicon-chevron-right"></i></div>
								<div class="in-blk landing-form-head-right-text">{{ $data['formTitle'] }}</div>
							</div>
						</div>
						<div class="landing-form-body">
							<div id="landingEnquiryError" class="errors-cnt alert-danger text-center hide"></div>
							<div class="landing-form-body-block">
								<div class="col-lg-8 col-md-8 col-lg-offset-2 col-md-offset-2 no-pad">
									<div class="form-group">
										<label class="landing-form-label" for="enquiryName">Your Name*</label>
										<input id="enquiryName" name="enquiryName" type="text" class="form-control landing-form-input landing-from-name-input" placeholder="enter your full name" value="">
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="landing-form-body-block mar-t-5">
								<div class="col-lg-8 col-md-8 col-lg-offset-2 col-md-offset-2 no-pad">
									<div class="form-group">
										<label class="landing-form-label" for="enquiryPhone">Phone Number*</label>
										<input id="enquiryPhone" name="enquiryPhone" type="text" class="form-control landing-form-input" maxlength="10" placeholder="10 digit phone number" value="">
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="landing-form-body-block mar-t-5">
								<div class="col-lg-8 col-md-8 col-lg-offset-2 col-md-offset-2 no-pad">
									<div class="form-group">
										<label class="landing-form-label" for="enquiryEmail">Email Address (optional)</label>
										<input id="enquiryEmail" name="enquiryEmail" type="text" class="form-control landing-form-input" placeholder="enter your valid email address" value="">
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="landing-form-body-block mar-t-5">
								<div class="col-lg-8 col-md-8 col-lg-offset-2 col-md-offset-2 no-pad">
									<div class="form-group">
										<label class="landing-form-label" for="enquiryDate">Party Date (optional)</label>
										<input id="enquiryDate" name="enquiryDate" type="text" class="form-control landing-form-input" placeholder="when is your party?" value="">
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="landing-form-body-block mar-t-20 mar-b-10">
							<div class="col-xs-12 text-center">
								<div class="form-group">
									<button type="submit" id="btnSubmitFormLanding" class="btn btn-landing-submit"> {{ $data['ctaButton'] }}<i class="glyphicon glyphicon-chevron-right"></i>
									</button>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</form>

					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>