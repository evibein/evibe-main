@extends('layout.base')

@section('page-title')
	<title>Redirecting to your page</title>
@endsection

@section('app')
	<div>
		<div class="col-sm-6 col-sm-offset-3">
			<div class="alert alert-info text-center mar-t-30">
				<h3 class="no-mar">Thank You</h3>
				<div class="pad-t-10">
					<span><img src="{{ $galleryUrl }}/img/icons/loading.gif" alt="loading"></span>
					<span class="text-danger font-18">Redirecting to your page.. please wait...</span>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<input id='url' type='hidden' value="{{ $data['redirectUrl'] }}"/>
@endsection

@section("javascript")
	<script type="text/javascript">
		$(document).ready(function () {
			setTimeout(function () {
				location.href = $('#url').val();
			}, 3000);
		});
	</script>
@endsection