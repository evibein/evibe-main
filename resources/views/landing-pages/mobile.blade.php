<div class="hide show-400-600 unhide__400">
	<div class="landing-body-form-wrap">
		<form class="landing-enquiry-form">
			<div class="landing-form-head">
				<div class="landing-form-head-text">
					<div class="landing-form-head-text text-e">
						<div class="in-blk"><i class="glyphicon glyphicon-chevron-right"></i></div>
						<div class="in-blk landing-form-head-right-text">{{ $data['formTitle'] }}</div>
					</div>
				</div>
			</div>
			<div class="landing-form-body">
				<div id="landingEnquiryErrorMobile" class="errors-cnt alert-danger text-center hide"></div>
				<div class="landing-form-body-block mar-t-10">
					<div class="col-xs-6 no-pad-l">
						<div class="form-group">
							<label class="landing-form-label" for="enquiryName">Your Name*</label>
							<input id="enquiryNameMobile" name="enquiryName" type="text" class="form-control landing-form-input landing-from-name-input" placeholder="your full name" value="">
						</div>
					</div>
					<div class="col-xs-6 no-pad-l">
						<div class="form-group">
							<label class="landing-form-label" for="enquiryPhone">Phone Number*</label>
							<input id="enquiryPhoneMobile" name="enquiryPhone" type="text" class="form-control landing-form-input" maxlength="10" placeholder="your phone number" value="">
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="landing-form-body-block mar-t-10">
					<div class="col-xs-6 no-pad-l">
						<div class="form-group">
							<label class="landing-form-label" for="enquiryEmail">Email Id (optional)</label>
							<input id="enquiryEmailMobile" name="enquiryEmail" type="text" class="form-control landing-form-input" placeholder="your email address" value="">
						</div>
					</div>
					<div class="col-xs-6 no-pad-l">
						<div class="form-group">
							<label class="landing-form-label" for="enquiryDate">Party Date (optional)</label>
							<input id="eventDate" name="enquiryDate" type="text" class="form-control landing-form-input" placeholder="your party date" value="">
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="landing-form-body-block mar-t-10">
					<div class="col-xs-12 text-center">
						<div class="form-group">
							<button type="submit" id="btnSubmitFormLandingMobile" class="btn btn-landing-submit"> {{ $data['ctaButton'] }}<i class="glyphicon glyphicon-chevron-right"></i>
							</button>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</form>
	</div>
	<div class="landing-body-images-wrap text-center">
		<div>
			@foreach ($data['sneakPeaks'] as $sneakPeak)
				<div class="col-xs-3 no-pad sneak-peak-item">
					<img class="landing-body-image-box" src="{{ $galleryUrl . $sneakPeak['img'] }}">
					<div class="landing-images-text">
						{{ $sneakPeak["title"] }}
					</div>
				</div>
			@endforeach
			<div class="clearfix"></div>
		</div>
		<div class="landing-sneak-peak-more mar-b-20">
			& many more
		</div>
	</div>
</div>