@extends('layout.base')

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-landing-pages.css') }}"/>
@endsection

@section('page-title')
	<title>{{ $data['pageTitle'] }} | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="{{ $data['meta']['description'] }}"/>
@endsection

@section('meta-keywords')
	<meta name="keywords" content="{{ $data['meta']['key-words'] }}"/>
@endsection

@section('og-title')
	<meta property="og:title" content="{{ $data['meta']['og-title'] }}"/>
@endsection

@section('og-description')
	<meta property="og:description" content="{{ $data['meta']['og-description'] }}"/>
@endsection

@section('og-url')
	<meta property="og:url" content="{{ $data['meta']['og-url'] }}"/>
@endsection

@section('meta-canonical')
	<link rel="canonical" href="{{ $canonicalUrl }}{{ $data['meta']['canonical'] }}"/>
@endsection

@section('press:title')
@endsection

@section("header")
@endsection

@section("footer")
@endsection

@section("content")
	<div class="landing-header">
		<div class="col-xs-6 no-pad">
			<div class="landing-header-left pull-left pad-l-10 text-center mar-t-5">
				<div class="landing-header-logo-wrap">
					<img class="landing-header-logo" src="{{ $galleryUrl }}/img/logo/logo_evibe.png" alt="Evibe.in logo">
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="col-xs-6 no-pad">
			<div class="landing-header-right pull-right pad-r-10">
				<div class="landing-header-text-wrap">
					{{ $data['headerTag'] }}
				</div>
				<div class="landing-header-mobile pull-right">
					<a class="landing-mobile-link" href="tel:9640804000">
						<i class="glyphicon glyphicon-earphone landing-mobile"></i>
						9640804000</a>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="landing-body">
		<div class="landing-body-section-text">
			<img class="background-image" src="{{ $galleryUrl .  $data['heroImage'] }} ">
			<div class="landing-bg-overlay">
				<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
					<div class="text-1-warp text-center	">
						<h1 class="text-1 ">{{ $data['headline-1'] }}</h1>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-lg-10 col-lg-offset-1 col-md-12">
					<div class="text-2-warp text-justify">
						<h2 class="text-2 no-mar text-center">{{ $data['subHeading'] }}</h2>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="landing-body-section-form">
			@include("landing-pages.mobile")
			@include("landing-pages.desktop")
		</div>
		<div class="landing-body-section-customer-story">
			<div class="landing-customer-page-head">
				Why <span class="text-e">Customers Choose Evibe.in</span>?
			</div>
			<div class="landing-customer-page-body">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6 text-center">
					<div class="pad-t-15 pad-b-15">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/6lwEXs51qJw?html5=1"></iframe>
						</div>
					</div>
					<div class="pad-b-15">
						<div class="fb-like" data-href="https://www.facebook.com/evibe.in/" data-layout="button_count" data-action="like" data-size="large" data-show-faces="false" data-share="false">
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
					<ul class="landing-customer-benefits-list">
						@foreach($data['benefits'] as $benefit)
							<li class="landing-customer-list-option">{{ $benefit }}</li>
						@endforeach
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="landing-body-section-stats">
			@foreach($data['stats'] as $stat)
				<div class="col-xs-3 text-center no-pad">
					<div class="landing-stats-text-1 text-e">
						{{ $stat['number'] }}
					</div>
					<div class="landing-stats-text-2">
						{{ $stat['text'] }}
					</div>
				</div>
			@endforeach
			<div class="clearfix"></div>
		</div>
		<div class="landing-body-section-media">
			<div class="landing-media-head">
				<span class="text-e">Top Media</span> Mentioned About Us
			</div>
			<div class="landing-media-body text-center mar-t-10">
				@foreach ($data["media"] as $media)
					<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2 no-pad-l">
						<img class="landing-media-image" src="{{ $galleryUrl }}/main/landing/media/{{ $media["img"] }}" alt="{{ $media["title"] }}"/>
					</div>
				@endforeach
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div class="landing-footer">
		<div class="col-xs-6 no-pad">
			<div class="landing-footer-left">
				<div class="landing-copy-text">© 2014 - 2018 Evibe Technologies Pvt. Ltd.</div>
			</div>
		</div>
		<div class="col-xs-6 no-pad">
			<div class="landing-footer-right text-right pad-r-20">
				{{ $data['headerTag'] }}
			</div>
		</div>
		<div class="clearfix"></div>
	</div>

	<!-- hidden inputs same for both forms, values will be taken from top form -->
	<input type="hidden" id="eventId" value="{{ $data['eventId'] }}">
	<input type="hidden" id="cityId" value="{{ $data['cityId'] }}">
	<input type="hidden" id="redirectUrl" value="{{ $data['redirectUrl'] }}">
	<input type="hidden" id="intermediateUrl" value="{{ $data['intermediateUrl'] }}">
	<input type="hidden" id="formUrl" value="{{ route('ga.landing.enquiry') }}">
@endsection

@section("modals")
	@parent
	<section id="landingPageEnquiryModal" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<span class="text-muted text-center">Fill the simple form below. See 100s of decorations with real pictures and customer reviews.</span>
					<div id="lModalError" class="alert alert-danger no-mar mar-t-10 pad-10 hide"></div>
					<div class="pad-t-20">
						<form id="lModalForm" class="form l-modal-form" method="POST">
							<div class="form-group">
								<label for="lModalName">Name*</label>
								<input id="lModalName" type="text" class="form-control" placeholder="your full name">
							</div>
							<div class="form-group">
								<label for="lModalPhone">Phone*</label>
								<input id="lModalPhone" type="text" class="form-control" placeholder="your phone number">
							</div>
							<div class="form-group">
								<label for="lModalEmail">Email Id (optional)</label>
								<input id="lModalEmail" type="text" class="form-control" placeholder="your email">
							</div>
							<div class="form-group">
								<label for="partyDate">Party Date (optional)</label>
								<input id="partyDate" type="text" class="form-control" placeholder="your tentative party date">
							</div>
							<div class="form-group text-center">
								<button id="lModalSubmitBtn" type="submit" class="btn btn-landing-submit btn-l-modal-submit">Let's Explore Now >
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		/* facebook like button */
		(function (d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s);
			js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));

		var $landingModal = $("#landingPageEnquiryModal");
		$landingModal.on("show.bs.modal", function () {
			$("#lModalError").addClass("hide");
			$("#lModalForm").trigger("reset");
		});

		function showLandingModal(name, phone) {
			$("#lModalName").val(name);
			$("#lModalPhone").val(phone);

			$landingModal.modal("show");
		}

		(function handleFormSubmit() {

			var $intermediateUrl = $('#intermediateUrl').val();
			var formUrl = $('#formUrl').val();
			var cityId = $("#cityId").val();
			var eventId = $('#eventId').val();

			function postEnquiry(data, errorCnt, submitBtn) {

				errorCnt.addClass('hide');
				if (submitBtn) {
					submitBtn.attr("disabled", "disabled");
				} else {
					window.showLoading();
				}

				$.ajax({
					url: formUrl,
					type: 'POST',
					dataType: 'JSON',
					data: data,
					success: function (data) {

						if (submitBtn) {
							submitBtn.removeAttr("disabled");
						} else {
							window.hideLoading();
						}

						if (data.success === true) {
							window.location.href = $intermediateUrl;
						}
						else if (data.success === false) {
							errorCnt.text(data.errorMsg);
							errorCnt.removeClass('hide');
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {

						if (submitBtn) {
							submitBtn.removeAttr("disabled");
						} else {
							window.hideLoading();
						}

						window.notifyTeam({
							"url": url,
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			}

			$('#btnSubmitFormLanding').click(function (event) {
				event.preventDefault();

				var enquiryName = $('#enquiryName').val();
				var enquiryPhone = $('#enquiryPhone').val();

				if (!enquiryName || !enquiryPhone) {
					showLandingModal(enquiryName, enquiryPhone);
					return false;
				}

				var data = {
					'enquiryName': enquiryName,
					'enquiryPhone': enquiryPhone,
					'enquiryEmail': $('#enquiryEmail').val().toString().trim(),
					'enquiryDate': $('#enquiryDate').val(),
					'eventId': eventId,
					"cityId": cityId
				};
				postEnquiry(data, $('#landingEnquiryError'));
			});

			$('#btnSubmitFormLandingMobile').click(function (event) {
				event.preventDefault();

				var enquiryName = $('#enquiryNameMobile').val();
				var enquiryPhone = $('#enquiryPhoneMobile').val();

				if (!enquiryName || !enquiryPhone) {
					showLandingModal(enquiryName, enquiryPhone);
					return false;
				}

				var data = {
					enquiryName: enquiryName,
					enquiryPhone: enquiryPhone,
					enquiryEmail: $('#enquiryEmailMobile').val().toString().trim(),
					enquiryDate: $('#eventDate').val(),
					eventId: eventId,
					cityId: cityId
				};
				postEnquiry(data, $('#landingEnquiryErrorMobile'));
			});

			$("#lModalSubmitBtn").click(function (event) {
				event.preventDefault();

				var name = $("#lModalName").val();
				var phone = $("#lModalPhone").val();
				var $errorCnt = $("#lModalError");

				if (!name || !phone) {
					$errorCnt.text("Please enter both your name and phone number to continue.")
						.removeClass("hide");

					return false;
				}

				var data = {
					enquiryName: name,
					enquiryPhone: phone,
					enquiryEmail: $('#lModalEmail').val().toString().trim(),
					enquiryDate: $('#partyDate').val(),
					eventId: eventId,
					cityId: cityId
				};
				postEnquiry(data, $errorCnt, $(this));
			});

		})();

		(function showStartModal() {
			$(".sneak-peak-item").click(function () {
				showLandingModal("", "");
			});
		})();

	</script>
@endsection