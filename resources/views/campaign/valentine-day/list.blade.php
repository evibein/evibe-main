@extends("layout.occasion.birthday.base-birthday-list")

@section('page-title')
	<title>{{ $data['seo']['pageTitle'] }}</title>
@endsection

@section('meta-description')
	<meta name="description" content="{{ $data['seo']['pageDescription'] }}"/>
@endsection

@section('meta-keywords')
	<meta name="keywords" content="{{ $data['seo']['keywords'] }}">
@show

@section('og-title')
	<meta property="og:title" content="{{ $data['seo']['pageTitle'] }}"/>
@endsection

@section('og-description')
	<meta property="og:description" content="{{ $data['seo']['pageDescription'] }}"/>
@endsection

@section("header")
	@include('campaign.header')
@endsection

@section("content")
	<div class="items-results packages-results">
		<div class="col-sm-12">
			<div class="top-panel"> <!-- top section begin -->
				<div class="col-sm-12">
					<h4 class="display-1 text-cap mar-b-10" title="{{ $data['seo']['pageHeader'] }}">
						Showing {{ $data['seo']['pageHeader'] }}
					</h4>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="bottom-panel no-mar-t"> <!-- bottom section begin -->
				<!-- left section-->
			@if($data['packages']->count() > 0 || $data['filters']['clearFilter'])
				<!-- left section begin -->
					<div class="col-xs-12 col-sm-5 col-md-3 col-lg-3 bottom-left-panel">
						<!-- filters begin -->
						<div class="text-center hide unhide__400">
							<div class="in-blk mar-r-20">
								<button class="btn btn-md btn-default btn-show-filter to-toggle__400" data-target=".item-filters">
									<i class="material-icons valign-mid">&#xE152;</i>
									<span class="valign-mid">Filters</span>
								</button>
							</div>
							<div class="in-blk">
								<button class="btn btn-md btn-default btn-show-filter to-toggle__400" data-target=".item-sort-options">
									<i class="material-icons valign-mid">&#xE164;</i>
									<span class="valign-mid">Sort</span>
								</button>
							</div>
						</div>
						<div class="item-filters no-pad-b__400 hide__400">
							<div class="title">Filter Results</div>
							<ul class="filters-list">
								@if (count($data['filters']['cats']))
									<li class="filter categories-filter">
										<div class="super-cat">
											<div class="pull-left">
												<a class="all-styles" href="{{ route('city.c.valentine.experiences.list', $cityUrl) }}">All Categories</a>
											</div>
											<div class="clearfix"></div>
										</div>
										<ul class="no-mar no-pad ls-none item-sub-cats">
											@foreach ($data['filters']['cats'] as $key => $category)
												<li class="@if($data['filters']['active'] == $category['url']) active @endif">
													<div class="pull-left">
														<a class="cat-link" data-url="{{ $category['url'] }}" href="{{ route('city.c.valentine.experiences.category', [$cityUrl, $category['url']]) }}">{{ $category['name'] }}</a>
													</div>
													<div class="clearfix"></div>
												</li>
											@endforeach
										</ul>
									</li>
								@endif
								<li class="filter price-filter">
									<label>Price</label>

									<div class="price-slider price-slider__400">
										<div class="in-blk pos-rel mar-r-10">
											<span class="rupee-text"><span class='rupee-font'>&#8377;</span></span>
											<input id="priceMin" class="price-input" type="text" value="{{ $data['filters']['priceMin'] }}">

											<div class="text-muted text-center">From</div>
										</div>
										<div class="in-blk pos-rel mar-r-10">
											<span class="rupee-text"><span class='rupee-font'>&#8377;</span></span>
											<input id="priceMax" class="price-input" type="text" value="{{ $data['filters']['priceMax'] }}">

											<div class="text-muted text-center">To</div>
										</div>
										<div class="in-blk valign-top">
											<a id="filterPriceBtn" class="btn-price-go btn btn-xs btn-default">
												<i class="glyphicon glyphicon-chevron-right"></i>
											</a>
										</div>
									</div>
								</li>
								@if ($data['filters']['clearFilter'] || $data['filters']['active'])
									<li class="text-center reset-filters reset-filters__400">
										<a href="{{ route("city.c.valentine.experiences.list", $cityUrl) }}?ref=reset-filters" class="font-16">
											<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
										</a>
									</li>
								@endif
							</ul>
						</div>
						<!-- filters end -->
					</div>
					<!-- left section end -->
			@endif

			<!-- right section begins-->
				<div class="col-xs-12 @if($data['packages']->count() == 0 && !$data['filters']['clearFilter'] )
						col-lg-12 col-md-12 col-sm-12
					@else
						col-sm-7 col-md-9 col-lg-9 bottom-right-panel
					@endif">
				@if($data['packages']->count() > 0 || $data['filters']['clearFilter'])
					<!-- sort options begin -->
						<ul class="no-pad no-mar ls-none item-sort-options hide__400">
							<li class="text-muted font-13">Sort By:</li>
							<li class="@if($data['sort'] == 'popularity') active @endif">
								<a class="sort-option" data-value="popularity">Popularity</a>
							</li>
							<li class="@if($data['sort'] == 'plth' || $data['sort'] == 'phtl') active @endif">
								@if ($data['sort'] == 'phtl')
									<a class="sort-option" data-value="plth">Price <i
												class="glyphicon glyphicon-chevron-down"></i></a>
								@elseif ($data['sort'] == 'plth')
									<a class="sort-option" data-value="phtl">Price <i
												class="glyphicon glyphicon-chevron-up"></i></a>
								@else
									<a class="sort-option" data-value="plth">Price</a>
								@endif
							</li>
						</ul>
						<!-- sort options end -->

						<div class="items-list packages-list"> <!-- results begin -->
							@if (count($data['packages']))
								@foreach ($data['packages'] as $package)
									<div class="col-lg-4 col-md-4 col-sm-4 no-pad-l">
										<div class="item-result packages">
											<div class="img-wrap">
												<a href="{{ $package['fullPath'] }}">
													<img src="{{ $package['profilePic'] }}" alt="{{ $package['name'] }} profile picture">
												</a>
											</div>
											<div class="details">
												<div class="title" title="{{ $package['name'] }}">
													<a href="{{ $package['fullPath'] }}"> {{ $package['name'] }} </a>
												</div>

												<div class="col-md-8 col-sm-12 col-xs-12 no-pad-l">
													<div class="price-tag text-left">
															<span class="price line-height-18">
																@price($package['price'])
																@if ($package['priceMax'] && $package['priceMax'] > $package['price'])
																	- @price($package['priceMax'])
																@endif
															</span>
														@if ($package['worth'] && $package['worth'] > $package['price'])
															<span class="price-worth line-height-18">@price($package['worth'])</span>
														@endif
													</div>
												</div>
												<div class="col-md-4 no-padding hidden-xs hidden-sm">
													<a href="{{ $package['fullPath'] }}?ref=book-now#bookNowModal" class="btn btn-primary btn-view-more btn-venue-details focused" target="_blank" rel="noopener">Book Now</a>
												</div>
												<div class="clearfix"></div>

											</div>
										</div>
									</div>
								@endforeach
							@else
								<div class="no-results-wrap text-center">
									<h4 class="no-results-title no-mar">
										<i class="glyphicon glyphicon-warning-sign"></i>
										Oops, no packages were found matching your filters.
									</h4>

									<div class="pad-t-20">
										<a href="{{ route('city.c.valentine.experiences.list', $cityUrl) }}?ref=no-results" class="btn btn-danger btn-lg">
											<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
										</a>
									</div>
								</div>
							@endif
							<div class="clearfix"></div>
						</div>
						<div class="pages"> <!-- pagination begin -->
							<div>{{ $data['packages']->appends($data['filters']['queryParams'])->links() }}</div>
						</div>
					@else
						<div class="no-results-wrap text-center">
							<h4 class="text-col-gr no-mar">We are adding amazing packages soon, keep checking this page.</h4>
						</div>
					@endif
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection