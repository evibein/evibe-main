@php $cityUrl=getcityUrl(); $currentUrlPath = request()->path(); $currentTime = time(); @endphp

@if($currentTime >= config('evibe.decommission-time'))
<div  class="" >
	<div class="navbar-campaign  mob-promotional-header-wrap" role="navigation">
		<div class="header-campaign-wrap snow background" style="background-color: #DD4444;">
			<span class="in-blk">
				<div target="_blank">
					<!--
					<div class="header-campaign-title font-18 " style="color: #FFF;">
						<a href="{{ route('virtual-birthday-party.home') }}" target="_blank" class="a-no-decoration fc-white-imp">
								Make your #Lockdown occasions lifetime memorable with Evibe Virtual Parties.
						</a>
					</div>
					-->
					<div class="pad-5 font-18" style="color: #FFF;">
						Due to unforeseen reasons, we're not accepting new orders. Sorry for the inconvenience!
					</div>
				</div>
			</span>
			{{-- <a class="pull-right in-blk  mob-promotional-header-close-btn" href="#" style="margin-top: 8px;color:white;"><span class="glyphicon glyphicon-remove-circle"></span></a> --}}
		</div>
	</div>
</div>
@elseif ($currentTime <= config('evibe.campaign-limit.valentines-day') && ($cityUrl === "bangalore" || $cityUrl === "hyderabad") && !(strpos($currentUrlPath, 'valentines-day-2022-romantic-candlelight-dinner') !== false))
<div  class="" >
	<div class="navbar-campaign  mob-promotional-header-wrap" role="navigation">
		<div class="header-campaign-wrap snow background" style="background-color: #DD4444;">
			<span class="in-blk">
				<div target="_blank">
					<div class="header-campaign-title font-18 " style="color: #FFF;">
						<a href="{{ route('campaign.landingpage', config('evibe.valentines-day-cld-url.' . $cityUrl)) }}" target="_blank" class="a-no-decoration fc-white-imp">
							Valentines Day Romantic Candlelight Dinners
						</a>
					</div>
				</div>
			</span>
		</div>
	</div>
</div>
@endif