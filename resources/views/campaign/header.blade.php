<header>
	<?php
	$tracker = "utm_source=zoomcar&utm_campaign=dec2016&utm_medium=zoomcar&utm_content=experiences";
	$valentineTracker = "utm_source=valentine&utm_campaign=jan2017&utm_medium=valentine&utm_content=experiences";
	?>
	<div class="header-top pay-header">
		<nav class="navbar navbar-default navbar-home pos-rel navbar-top topnav pad-b-10 home" role="navigation">
			<div class="container topnav">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand topnav" href="/">
						<img src="https://gallery.evibe.in/img/logo/logo_evibe.png" alt="Evibe log">
					</a>
				</div>
				<div class="collapse navbar-collapse" id="collapse">
					<ul class="nav navbar-nav navbar-right top-header-list">
						<li>
							<a class="no-click">
								<span class="glyphicon glyphicon-envelope"></span>
								{{ config('evibe.contact.company.email') }}
							</a>
						</li>
						<li>
							<a class="no-click">
								<span class="glyphicon glyphicon-earphone"></span>
								{{ config('evibe.contact.company.plain_phone') }}
							</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	</div>
	@if(session()->has('cityId') && session('cityId') == 1)
		<div class="header-bottom">
			<div class="container">
				<div id='cssmenu' class="menu-links">
					<ul>
						<li>
							<a href="{{ route('city.occasion.bachelor.home', getCityUrl()) }}?@if( array_key_exists('valentineCampaign' , $data) && $data['valentineCampaign']){{ $valentineTracker }}@else{{ $tracker }}@endif">Youth Parties</a>
						</li>
						<li>
							<a href="{{ route('city.occasion.surprises.home', getCityUrl()) }}?@if( array_key_exists('valentineCampaign' , $data) && $data['valentineCampaign']){{ $valentineTracker }}@else{{ $tracker }}@endif">Surprises</a>
						</li>
						<li>
							<a href="{{ route('city.occasion.pre-post.home', getCityUrl()) }}?@if( array_key_exists('valentineCampaign' , $data) && $data['valentineCampaign']){{ $valentineTracker }}@else{{ $tracker }}@endif">Pre / Post Weddings</a>
						</li>
						<li class="active">
							<a href="{{ route('city.occasion.birthdays.home', getCityUrl()) }}?@if( array_key_exists('valentineCampaign' , $data) && $data['valentineCampaign']){{ $valentineTracker }}@else{{ $tracker }}@endif">Birthday Parties</a>
						</li>
						<li>
							<a href="{{ route('city.occasion.house-warming.home', getCityUrl()) }}?@if( array_key_exists('valentineCampaign' , $data) && $data['valentineCampaign']){{ $valentineTracker }}@else{{ $tracker }}@endif">House Warming</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	@elseif(session()->has('cityId') && session('cityId')==2)
		<div class="header-bottom">
			<div class="container">
				<div id='cssmenu' class="menu-links">
					<ul>
						<li>
							<a href="{{ route('city.occasion.bachelor.home', getCityUrl()) }}?@if( array_key_exists('valentineCampaign' , $data) && $data['valentineCampaign']){{ $valentineTracker }}@else{{ $tracker }}@endif">Youth Parties</a>
						</li>
						<li>
							<a href="{{ route('city.occasion.surprises.home', getCityUrl()) }}?@if( array_key_exists('valentineCampaign' , $data) && $data['valentineCampaign']){{ $valentineTracker }}@else{{ $tracker }}@endif">Surprises</a>
						</li>
						<li class="active">
							<a href="{{ route('city.occasion.birthdays.home', getCityUrl()) }}?@if( array_key_exists('valentineCampaign' , $data) && $data['valentineCampaign']){{ $valentineTracker }}@else{{ $tracker }}@endif">Birthday Parties</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	@endif
</header>