@extends("campaign.base")

@section("content")
	<div class="campaign-content-wrap">
		<div class="col-sm-12">
			<div class="top-panel text-center">
				<h4 class="display-1 text-cap" title="{{ $data['seo']['pageHeader'] }}">
					Showing {{ $data['seo']['pageHeader'] }}</h4>
				<div class="clearfix"></div>
			</div>
			<div class="bottom-panel">
				@if($data['packages']->count() > 0)
					<div class="items-list bachelor-pkgs-list">
						@if (count($data['packages']))
							@foreach ($data['packages'] as $package)
								<div class="col-sm-12 col-md-6 col-lg-3 pad-l-5 pad-r-6 no-pad__400-600">
									<div class="item-result bachelor-pkg">
										<div class="img-wrap">
											<a href="{{ $data['profileBaseUrl'] . $package->url }}?ref=list-image">
												<img src="{{ $package->getProfileImg() }}" alt="{{ $package->name }} profile picture">
											</a>
										</div>
										<div class="details">
											<div class="title">
												<a href="{{ $data['profileBaseUrl']. $package->url }}?ref=list-name">
													{{ $package->name }}
												</a>
											</div>
											<div class="action-btns">
												<div class="col-md-6 no-padding hidden-xs hidden-sm border-right">
													<div class="price-tag pad-t-5">
															<span class="price">
																@price($package->price)
															</span>
														@if ($package->price_worth && $package->price_worth > $package->price)
															<span class="price-worth">
																	@price($package->price_worth)
																</span>
														@endif
													</div>
												</div>
												<div class="col-md-6 no-padding hidden-xs hidden-sm">
													<a data-url="{{ route('auto-book.campaign.generic-campaign.pay.auto.init',[$package->id, config('evibe.occasion.'. $data['campaignUrl'].'.id'), $package->isRequiresBookingImage($data['campaignUrl'])]) }}"
															class="item-book-now btn-campaign-book-now"
															target="_blank" rel="noopener">Book Now</a>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="content-details">
												<div>
													<div class="col-md-8 col-sm-8 col-xs-8 no-pad">
														<div class="price-tag text-left">
															<span class="price">
																@price($package->price)
															</span>
															@if ($package->price_worth && $package->price_worth > $package->price)
																<span class="price-worth">
																	@price($package->price_worth)
																</span>
															@endif
														</div>
													</div>
													<div class="col-md-4 col-sm-4 col-xs-4 no-pad pad">
														<div class="rating-wrap">
															<a data-url="{{ route('auto-book.campaign.generic-campaign.pay.auto.init',[$package->id,config('evibe.occasion.'. $data['campaignUrl'].'.id'),$package->isRequiresBookingImage($data['campaignUrl'])]) }}"
																	class="item-book-now btn-campaign-book-now"
																	target="_blank" rel="noopener">Book Now</a>
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="additional-info"></div>
											</div>
										</div>
									</div>
								</div>
							@endforeach
						@else
							<div class="no-results-wrap text-center">
								<h4 class="no-results-title no-mar">
									<i class="glyphicon glyphicon-warning-sign"></i>
									Oops, no packages were found.
								</h4>
							</div>
						@endif
						<div class="clearfix"></div>
					</div>
				@else
					<div class="no-results-wrap text-center">
						<h4 class="text-col-gr no-mar">We are adding special surprise packages soon, keep checking this page.</h4>
					</div>
				@endif
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection