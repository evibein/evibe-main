@extends("campaign.base")

@section("content")
	<div class="campaign-content-wrap">
		@if($data['package'])
			<div class="items-list no-mar-t bachelor-pkgs-list">
				<div class="breadcrumb-section">
					<ol class="breadcrumb pad-l-15 pad-t-10 no-mar-b no-pad-b text-center__400"> <!-- breadcrumbs begin -->
						<li>
							<a href="{{ route('campaign.list', $data['campaignUrl']) }}">{{ ucwords(config('evibe.occasion.'.$data['campaignUrl'].'.name')) }}</a>
						</li>
						<li class="active">{{ $data['package']->code }}</li>
					</ol>
				</div>
				<div class="campaign-profile-wrap" data-url="{{ route('campaign.profile.similar',[$data["campaignUrl"], $data["package"]->id]) }}">
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="gallery-wrap" role="tabpanel">
						@if ($data['gallery']['showGalleryTabs']) <!-- nav tabs begin -->
							<ul class="nav nav-pills mar-l-20" role="tablist">
								<?php $count = 0; ?>
								@foreach($data['gallery']['images'] as $cat => $catImages)
									<li role="presentation" class="@if(!$count++) active @endif">
										<a href="#catGalId{{ $count }}" data-toggle="pill">
											{{ $cat }}
										</a>
									</li>
								@endforeach
							</ul>
							@endif
							@if (count($data['gallery']['images']))
								<div class="tab-content mar-t-15"> <!-- tab panes begin -->
									<?php $count = 0; ?>
									@foreach($data['gallery']['images'] as $cat => $catImages)
										<div role="tabpanel" class="tab-pane fade @if(!$count++) in active @endif" id="catGalId{{ $count }}">
											@if (count($catImages))
												<div class="fotorama" data-width="100%" data-ratio="900/600" data-maxheight="400" data-nav="thumbs" data-autoplay="true" data-thumbwidth="80" data-thumbmargin="10" data-keyboard="true" data-captions="true" @if($data['gallery']['showThumb']) data-enableifsingleframe="true" @endif>
													@foreach ($catImages as $key => $image)
														<a href="{{ $image['url'] }}">
															<img src="{{ $image['thumb'] }}" alt="{{ $image['title'] }}"/>
														</a>
													@endforeach
												</div>
											@else
												<i class="text-danger">No images found in this category.</i>
											@endif
										</div>
									@endforeach
								</div>
							@else
								<div>No images found</div>
							@endif
						</div>
					</div>
					<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
						<h4 class="no-mar mar-t-10">
							{{ ucwords($data['package']->name) }}
							<span id="verified" class="text-success"><i
										class="material-icons">&#xE8E8;</i></span>
							<div class="mdl-tooltip mdl-tooltip--large" for="verified">Verified Provider</div>
						</h4>
						#{{ $data['package']->code }}
						<div class="inclusions">
							<h6>Inclusions</h6>
							<div class="campaign-inclusions-wrap scrollbar" id="style-4">
								@if ($data['package']->info)
									<div class="card-info-text no-pad">
										{!! $data['package']->info !!}
									</div>
								@endif
								@if ($data['package']->prerequisites)
									<div class="text-center pad-b-10">
										<u><b>Prerequisites</b></u>
									</div>
									<div class="card-info-text no-pad">
										{!! $data['package']->prerequisites !!}
									</div>
								@endif
								@if ($data['package']->facts)
									<div class="text-center pad-b-10">
										<u><b>Facts</b></u>
									</div>
									<div class="card-info-text no-pad">
										{!! $data['package']->facts !!}
									</div>
								@endif
								@if ($data['package']->terms)
									<div class="text-center pad-b-10">
										<u><b>Terms of Booking</b></u>
									</div>
									<div class="card-info-text no-pad">
										{!! $data['package']->terms !!}
									</div>
								@endif
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
						<div class="item-profile highlights-card highlights-card-campaign"> <!-- highlights begin -->
							<div class="hs-section-campaign">
								<div class="text-center mar-b-5 border-b-1"> <!-- protection begin -->
									<img class="img-highlights-card" src="{{ $galleryUrl }}/img/app/decor_protection_260x66.jpg" alt="Evibe delivery guarantee"/>
								</div>
								<div class="card-item text-center price-item pad-t-10"> <!--- Price info --->
									@if ($data['package']->price_worth && $data['package']->price_worth > $data['package']->price)
										<div class="price-worth in-blk">
											@price ($data['package']->price_worth)
										</div>
									@endif
									<div class="price-val in-blk hide__400 hide">
										@price ($data['package']->price)
										@if ($data['package']->price_max && $data['package']->price_max > $data['package']->price)
											-
											@price( $data['package']->price_max )
										@endif
									</div>
									<div class="price-val in-blk unhide__400">
										<br>
										@price ($data['package']->price)
										@if ($data['package']->price_max && $data['package']->price_max > $data['package']->price)
											-
											@price( $data['package']->price_max )
										@endif
									</div>
								</div>
								<div class="card-item card-actions text-center pad-t-15 pad-b-10 hide__400">
									<a class="btn btn-default btn-view-more btn-more-details btn-campaign-book-now btn-campaign-book-now-profile" data-url="{{ route('auto-book.campaign.generic-campaign.pay.auto.init', [$data['package']->id, config('evibe.occasion.'. $data['campaignUrl'].'.id'), $data['package']->isRequiresBookingImage($data['campaignUrl'])]) }}">
										<i class="material-icons valign-mid font-18"></i>
										<b>BOOK NOW</b></a>
								</div>
							</div>
							<div class="hs-section social-section text-center">
								@include('app.social-share-plugin')
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<input type="hidden" id="packageId" value="{{ $data['package']->id }}">
					<input type="hidden" id="campaignUrl" value="{{ $data['campaignUrl'] }}">
				</div>
				<div>
					<div class="similar-items-wrap pad-t-30">
						<div class="text-center">
							<h4>Similar Packages</h4>
							<div class="similar-packages-list mdl-grid">
								<div class="loading-bar-campaign-profile col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 pad-l-20 pad-r-20">
										<div class="similar-loading-cards table table-bordered">
											<div class="loader">
												<div class="line"></div>
												<div class="line"></div>
												<div class="line"></div>
												<div class="line"></div>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 pad-l-20 pad-r-20">
										<div class="similar-loading-cards table table-bordered">
											<div class="loader">
												<div class="line"></div>
												<div class="line"></div>
												<div class="line"></div>
												<div class="line"></div>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 pad-l-20 pad-r-20">
										<div class="similar-loading-cards table table-bordered">
											<div class="loader">
												<div class="line"></div>
												<div class="line"></div>
												<div class="line"></div>
												<div class="line"></div>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 pad-l-20 pad-r-20">
										<div class="similar-loading-cards table table-bordered">
											<div class="loader">
												<div class="line"></div>
												<div class="line"></div>
												<div class="line"></div>
												<div class="line"></div>
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="hide unhide__400">
				<div class="no-pad btn-evibe btn-mobile-submit text-center">
					<a class="btn-campaign-book-now sticky-btn-campaign-book-now btn-submit-choices" data-url="{{ route('auto-book.campaign.generic-campaign.pay.auto.init', [$data['package']->id, config('evibe.occasion.'. $data['campaignUrl'].'.id'), $data['package']->isRequiresBookingImage($data['campaignUrl'])]) }}">
						<i class="material-icons valign-mid"></i>
						<div class="pad-t-10 pad-b-10 in-blk">BOOK NOW</div>
					</a>
				</div>
			</div>
		@else
			<div class="no-results-wrap text-center">
				<h4 class="no-results-title no-mar">
					<i class="glyphicon glyphicon-warning-sign"></i>
					Oops, Package not found.
				</h4>
			</div>
		@endif
	</div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			(function fetchSimilarPackages() {
				$.ajax({
					url: $('.campaign-profile-wrap').data("url"),
					dataType: 'html',
					type: 'POST',
					success: function (data) {
						$('.loading-bar-campaign-profile').addClass('hide');
						$('.similar-packages-list').append(data);
					},
					error: function (jqXHR, textStatus, errorThrown) {
						window.notifyTeam({
							"url": url,
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			})();
		});
	</script>
@endsection