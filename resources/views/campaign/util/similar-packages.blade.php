@foreach ($data['packages'] as $similarPackage)
	<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 pad-l-5 pad-r-6">
		<div class="item-result decor-style">
			<div class="img-wrap">
				<a href="{{ route('campaign.profile.show', [$data['campaignUrl'],$similarPackage->url]) }}">
					<img src="{{ $similarPackage->getProfileImg() }}" alt="{{ $similarPackage->getProfileImg() }} profile picture">
				</a>
			</div>
			<div class="details">
				<div class="title">
					<a href="{{ route('campaign.profile.show', [$data["campaignUrl"], $similarPackage->url]) }}">
						{{ $similarPackage->name }}
					</a>
				</div>
				<div class="item-info pad-t-5 in-blk text-center">
					<div class="price-tag text-left mar-t-5 col-md-12 pad-r-0">
							<span class="price">
								@price($similarPackage->price)
								@if ($similarPackage->price_max && $similarPackage->price_max > $similarPackage->price)
									-
									@price($similarPackage->price_max)
								@endif
								@if ($similarPackage->price_worth && $similarPackage->price_worth > $similarPackage->price)
									<span class="price-worth">
										@price($similarPackage->price_worth)
									</span>
								@endif
							</span>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
@endforeach
<div class="clearfix"></div>