@extends("partner.base")

@section("innerContent")
	<div class="right_col" role="main">
		<div class="">
			<div class="col-lg-8 col-md-8 col-sm-8 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">
				<div class="page-title">
					<div class="title_left">
						<h3> Package Information</h3>
						<div class="clearfix"></div>
					</div>
					@if(isset($data['newPackage']->status_id) && ($data['newPackage']->status_id == config("evibe.partner.package_status.draft")))
						<a href="{{ route("partner.dash.delete.package", $data['newPackage']->id) }}" class="btn btn-danger pull-right" onclick="return confirm('Package once deleted cannot be restored, Are you sure to continue?')"><i class="fa fa-times"></i> Delete</a>
					@endif
					@if(isset($data['newPackage']->status_id) && ($data['newPackage']->status_id != config("evibe.partner.package_status.rejected")))
						<a href="{{ route("partner.dash.add.package.third", ["packageId" => $data['newPackage']->id]) }}" class="btn btn-warning pull-right"><i class="fa fa-pencil"> </i> Edit</a>
					@endif
					@if(isset($data['newPackage']['fullUrl']))
						<a href="{{ $data['newPackage']['fullUrl'] }}" target="_blank" class="btn btn-warning pull-right"><i class="fa fa-eye"> </i> View on Evibe.in</a>
					@endif
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-8 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">
					<div class="x_panel">
						<div class="x_title">
							<h2>Basic Info</h2>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							@if(isset($data['newPackage']['fullUrl']))
								<table class="table table-striped table-hover text-left">
									<tr class="">
										<td class="table-top-row-remove-upper-border"><b>Package Name</b></td>
										<td class="table-top-row-remove-upper-border">{{ $data['livePackageData']['name'] }}</td>
									</tr>
									<tr class="">
										<td class="text-bold">Code</td>
										<td>{{ $data['livePackageData']['code'] }}</td>
									</tr>
									<tr class="">
										<td class="text-bold">Actual Price</td>
										<td><i class="fa fa-inr" aria-hidden="true"></i> {{ $data['livePackageData']['worth'] }}</td>
									</tr>
									<tr class="">
										<td class="text-bold">Discounted Price</td>
										<td><i class="fa fa-inr" aria-hidden="true"></i> {{ $data['livePackageData']['price'] }}</td>
									</tr>
									<tr class="">
										<td class="text-bold">Info</td>
										<td>{{ $data['livePackageData']['info'] }}</td>
									</tr>
									<tr class="">
										<td class="text-bold">Prerequisites</td>
										<td>{{ $data['livePackageData']['prerequisites'] }}</td>
									</tr>
									<tr class="">
										<td class="text-bold">Facts</td>
										<td>{{ $data['livePackageData']['facts'] }}</td>
									</tr>
								</table>
							@else
								<table class="table table-striped table-hover text-left">
									<tr class="">
										<td class="table-top-row-remove-upper-border"><b>Package Name</b></td>
										<td class="table-top-row-remove-upper-border">{{ $data['newPackage']->package_name }}</td>
									</tr>
									<tr class="">
										<td class="text-bold">Actual Price</td>
										<td><i class="fa fa-inr" aria-hidden="true"></i> {{ $data['newPackage']->worth }}</td>
									</tr>
									<tr class="">
										<td class="text-bold">Discounted Price</td>
										<td><i class="fa fa-inr" aria-hidden="true"></i> {{ $data['newPackage']->price }}</td>
									</tr>
									<tr class="">
										<td class="text-bold">Inclusions</td>
										<td>{{ $data['newPackage']->inclusions }}</td>
									</tr>
									<tr class="">
										@php $eventName = $data['event']->where('id', $data['newPackage']->event_id)->first(); @endphp
										<td class="text-bold">Event Supported</td>
										<td>{{ $eventName ? $eventName->name : "--" }}</td>
									</tr>
								</table>
							@endif
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="row">
				@if(isset($data['addValues']) && $data['addValues'])
					<div class="col-lg-8 col-md-8 col-sm-8 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">
						<div class="x_panel">
							<div class="x_title">
								<h2>Additional Info</h2>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								@php $count = 0; @endphp
								<table class="table table-striped table-hover text-left">
									@foreach($data['addValues'] as $key => $value)
										<tr class="">
											<td class="text-bold @if($count == 0) table-top-row-remove-upper-border @endif">{{ $key }}</td>
											<td class="@if($count == 0) table-top-row-remove-upper-border @endif">{{ $value }}</td>
										</tr>
										@php $count++; @endphp
									@endforeach
								</table>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				@endif
				@if(isset($data['newPackage']['fullUrl']))
					@if($data['mapTypeId'] == config('evibe.ticket.type.decor'))
						<div class="col-lg-8 col-md-8 col-sm-8 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">
							<div class="x_panel">
								<div class="x_title">
									<h2>Additional Info</h2>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<table class="table table-striped table-hover text-left">
										<tr class="">
											<td class="text-bold table-top-row-remove-upper-border">Setup Time(In hrs)</td>
											<td class="table-top-row-remove-upper-border">{{ $data['livePackageData']['self']->time_setup }}</td>
										</tr>
										<tr class="">
											<td class="text-bold table-top-row-remove-upper-border">Setup Duration(In hrs)</td>
											<td class="table-top-row-remove-upper-border">{{ $data['livePackageData']['self']->time_duration }}</td>
										</tr>
										<tr class="">
											<td class="text-bold table-top-row-remove-upper-border">Free Delivery KMs (in KMs)</td>
											<td class="table-top-row-remove-upper-border">{{ $data['livePackageData']['self']->kms_free }}</td>
										</tr>
										<tr class="">
											<td class="text-bold table-top-row-remove-upper-border">Maximum Delivery KMs (in KMs)</td>
											<td class="table-top-row-remove-upper-border">{{ $data['livePackageData']['self']->kms_max }}</td>
										</tr>
										<tr class="">
											<td class="text-bold table-top-row-remove-upper-border">Minimum Transport Charges (in Rupees)</td>
											<td class="table-top-row-remove-upper-border">{{ $data['livePackageData']['self']->trans_min }}</td>
										</tr>
										<tr class="">
											<td class="text-bold table-top-row-remove-upper-border">Maximum Transport Charges (in Rupees)</td>
											<td class="table-top-row-remove-upper-border">{{ $data['livePackageData']['self']->trans_max }}</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					@elseif($data['mapTypeId'] == config('evibe.ticket.type.package'))
						<div class="col-lg-8 col-md-8 col-sm-8 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">
							<div class="x_panel">
								<div class="x_title">
									<h2>Additional Info</h2>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<table class="table table-striped table-hover text-left">
										<tr class="">
											<td class="text-bold table-top-row-remove-upper-border">Setup Time(In hrs)</td>
											<td class="table-top-row-remove-upper-border">{{ intval($data['livePackageData']['self']->time_setup) }}</td>
										</tr>
										<tr class="">
											<td class="text-bold table-top-row-remove-upper-border">Setup Duration(In hrs)</td>
											<td class="table-top-row-remove-upper-border">{{ intval($data['livePackageData']['self']->time_duration) }}</td>
										</tr>
										<tr class="">
											<td class="text-bold table-top-row-remove-upper-border">Free Delivery KMs (in KMs)</td>
											<td class="table-top-row-remove-upper-border">{{ intval($data['livePackageData']['self']->kms_free) }}</td>
										</tr>
										<tr class="">
											<td class="text-bold table-top-row-remove-upper-border">Maximum Delivery KMs (in KMs)</td>
											<td class="table-top-row-remove-upper-border">{{ intval($data['livePackageData']['self']->kms_max) }}</td>
										</tr>
										<tr class="">
											<td class="text-bold table-top-row-remove-upper-border">Minimum Transport Charges (in Rupees)</td>
											<td class="table-top-row-remove-upper-border">{{ $data['livePackageData']['self']->trans_min }}</td>
										</tr>
										<tr class="">
											<td class="text-bold table-top-row-remove-upper-border">Maximum Transport Charges (in Rupees)</td>
											<td class="table-top-row-remove-upper-border">{{ $data['livePackageData']['self']->trans_max }}</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					@endif
				@endif
			</div>
			<div class="row">
				@if(isset($data['images']) && $data['images']->count() > 0)
					<div class="col-lg-8 col-md-8 col-sm-8 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">
						<div class="x_panel">
							<div class="x_title">
								<h2>Gallery</h2>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								@foreach($data['images'] as $image)
									@php $imageUrl = config("evibe.gallery.host")."/package/new/".$data['newPackage']->id."/". $image->url; @endphp
									<a href="{{ $imageUrl }}" target="_blank" class="mar-r-10">
										<img src="{{ $imageUrl }}" width="100" height="100">
									</a>
								@endforeach
							</div>
						</div>
					</div>
				@endif
				@if(isset($data['newPackage']['fullUrl']))
					@if($data['livePackageData']['self']->gallery() && $data['livePackageData']['self']->gallery()->count() > 0)
						<div class="col-lg-8 col-md-8 col-sm-8 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">
							<div class="x_panel">
								<div class="x_title">
									<h2>Gallery</h2>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									@foreach($data['livePackageData']['self']->gallery()->get() as $image)
										<a href="{{ $image->getPath() }}" target="_blank" class="mar-r-10">
											<img src="{{ $image->getPath() }}" width="100" height="100">
										</a>
									@endforeach
								</div>
							</div>
						</div>
					@endif
				@endif
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
@endsection