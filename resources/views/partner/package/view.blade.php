@extends("partner.base")

@section("innerContent")
	<div class="right_col" role="main">
		<div class="">
			<div class="page-title">
				<div class="title_left">
					<h3>
						@if($data['filter'] == "live")
							Packages Live on Evibe
						@elseif($data['filter'] == "submitted")
							Submitted Packages
						@elseif($data['filter'] == "draft")
							Packages Saved as Drafts
						@elseif($data['filter'] == "rejected")
							Rejected Packages
						@elseif($data['filter'] == "all")
							All Packages
						@endif
						{{ "(".count($data['packages']).")" }}
					</h3>
				</div>
				<div class="title_right">
					<a href="{{ route("partner.dash.add.package") }}" class="pull-right">
						<button type="button" class="btn btn-success"><i class="fa fa-plus"></i> <b>ADD NEW PACKAGE</b>
						</button>
					</a>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="row">
				<div class="x_panel">
					<div class="x_content">
						<div class="row packages-list">
							@if(session()->has('errorMsg'))
								<div class="alert alert-danger mar-l-10 mar-r-10 mar-b-10 pad-10">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									<i class="glyphicon glyphicon-remove-sign"></i>
									{{ session('errorMsg') }}
								</div>
							@endif
							@if(count($data['packages']) > 0)
								@foreach($data['packages'] as $card)
									<div class="col-md-55">
										<div class="thumbnail">
											@if(isset($card['isNewPackage'])) @php $isLivePackage = 0 @endphp
											@else @php $isLivePackage = $card['mapTypeId']."_".$card['mapId'] @endphp
											@endif
											<div class="image view view-first">
												<a href="{{ route('partner.dash.new.profile.view', [$card['id'],"is_live" => $isLivePackage]) }}" title="{{ $card['title'] }}">
													<img src="{{ $card['profileImg'] }}" alt="{{ $card['title'] }}" title="{{ $card['title'] }}" class="card-img-src" style="width: 100%; display: block;">
												</a>
												@if(isset($card['isNewPackage']) && $card['isNewPackage'] == 1)
													<div class="mask">
														<div class="tools tools-bottom font-12">
															<a href="{{ route("partner.dash.add.package.third", ["packageId" => $card['id']]) }}"><i class="fa fa-pencil"></i> Edit</a>
															@if(isset($card['isDelete']) && $card['isDelete'] == 1)
																<a href="{{ route("partner.dash.delete.package", $card['id']) }}" onclick="return confirm('Package once deleted cannot be restored, Are you sure to continue?')"><i class="fa fa-times"></i> Delete</a>
															@endif
														</div>
													</div>
												@endif
											</div>
											<div class="caption">
												<div class="card-detail-title title">
													<a href="{{ route('partner.dash.new.profile.view', [$card['id'],"is_live" => $isLivePackage]) }}"
															title="{{ $card['title'] }}">
														{{ $card['title'] }}
													</a>
												</div>
												@if ($card['price'] != 0)
													<span class="price card-price font-20"><i class="fa fa-inr"
																aria-hidden="true"></i> {{ $card['price'] }}
														@if($card['priceMax'] && $card['priceMax'] > $card['price'])
															- {{ $card['priceMax'] }}
														@endif
															</span>
													@if ($card['priceMax'] && $card['priceMax'] > $card['price'])
														*
													@endif
													@if ($card['worth'])
														<span class="price-worth">
																<i class="fa fa-inr" aria-hidden="true"></i><strike> {{ $card['worth'] }}</strike></span>
													@endif
													@if( $card['additionalPriceLine'])
														<span>{{ $card['additionalPriceLine'] }}</span>
													@endif
												@else
													<span>----</span>
												@endif
											</div>
										</div>
									</div>
								@endforeach
							@else
								<div class="alert alert-danger font-16 text-center mar-l-10 mar-r-10" style="line-height: 25px">
									<i class="glyphicon glyphicon-warning-sign"></i>
									<span>Sorry, you do not have any packages/services live on Evibe.in. Please contact {{ config('evibe.contact.company.phone') }} for more information.</span>
									<br>(OR)<br>
									<span>Click on ADD PACKAGE button at top right & submit details.</span>
								</div>
							@endif
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection