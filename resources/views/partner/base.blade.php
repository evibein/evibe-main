<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en"> <!--<![endif]-->

<head>
	<!-- GTM DataLayer -->
	@yield('gtm-data-layer')

	<!-- trackers -->
	@include('track.gtm')

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="google-site-verification" content="aAggmaMI_knVUeswYULXQSKD41sWlr2z3G21bdkPZoI"/>
	<meta name="sitelock-site-verification" content="7865"/>
	<meta name="msvalidate.01" content="31489A18F1AD59FC089CB9B9A7E5A85D"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	@section('page-title')
		<title>Partner Profile | Evibe.in</title>
	@show

	<link rel="icon" href="https://gallery.evibe.in/img/logo/favicon_evibe_v2.png" type="image/png" sizes="16x16">

	@section('custom-css')
		<link rel="stylesheet" href="{{ elixir('css/app/partner-profile.css') }}"/>
	@show

	@yield('push-notifications')
	@yield('custom-head')
</head>

<body class="nav-md">
@if(isset($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'], "wv") > 0)
	<div class="loading-screen-gif" style="position:fixed; background-color: #FFFFFF; width: 100%; height: 100%; display: table; z-index: 999;">
		<div style="display: table-cell; height: 100%; width: 100%; vertical-align: middle; text-align: center;">
			<img src="{{ config("evibe.gallery.host")."/img/gifs/blocks-loading-244.gif" }}" alt="loading..." width="120" height="120">
		</div>
	</div>
@endif
<div class="container body">
	<div class="main_container">
		@include('partner.util.left_nav_slider')

		@include('partner.util.top_nav')

		@yield("innerContent")

		@include('partner.util.bottom_nav')
	</div>
</div>
<input type="hidden" id="isLoginPage" value="2">

<script src="{{ elixir('js/partner/partner.js') }}"></script>

@yield('javascript')

</body>
</html>