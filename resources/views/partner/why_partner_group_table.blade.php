@extends('layout.base')

@section('page-title')
	<title>Partner Sign Up For Group Table Packages | Evibe.in</title>
@endsection

@section('og-url')
	<meta property="og:url" content="{{ route('partner.benefits') }}"/>
@endsection

@section('meta-canonical')
	<link rel="canonical" href="{{ $canonicalUrl }}/partner/signup"/>
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/partner-landing.css') }}">
@show

@section("header")
	<header>
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="navbar-brand">
					<img src="https://gallery.evibe.in/img/logo/logo_evibe.png" alt="Evibe logo">
				</div>
				<div class="pull-right">
					<a href="#cldSignUp">Sign Up</a>
				</div>
			</div>
		</nav>
	</header>
@endsection

@section("footer")
	<footer>
		<div class="text-center font-12 clr-black pad-10">
			&copy; 2014 - 2019 Evibe Technologies Pvt. Ltd.
		</div>
	</footer>
@endsection

@section("content")
	<section id="main-slider" class="no-mar">
		<div class="carousel slide">
			<div class="carousel-inner">
				<div class="item active" style="background-image: url(https://gallery.evibe.in/main/img/group_table_b.jpg)">
					<div class="pc-bg-overlay"></div>
				</div>
				<div class="container pos-abs" style="top: 0; text-align: left; z-index: 1;">
					<div class="row slide-margin">
						<div class="col-sm-10 col-sm-offset-1">
							<div class="carousel-content">
								<h3 class="animation animated-item-1">Do you have special packages for celebrations at your Restaurant?</h3>
								<p>Host group table packages and grow your business without extra efforts.</p>
								<a class="btn btn-evibe btn-lg text-bold animation animated-item-3 mar-t-20" href="#cldSignUp">SIGN UP NOW</a><br>
								<a class="btn btn-link no-pad-l frnd-sign-up-btn text-bold sign-up-friend-btn">(Sign Up For Friend)</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container">
			<h3 class="text-center pad-t-50">Key Benefits</h3>
			<div class="text-center">
				<div class="col-md-4">
					<div class="hi-icon-wrap hi-icon-effect wow fadeInDown">
						<i class="fa glyphicon glyphicon-list-alt"></i>
						<h2>Conceptualisation</h2>
						<p>We sold 1000s of special packages for celebrations. We will help you design a perfect package for you</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="hi-icon-wrap hi-icon-effect wow fadeInDown">
						<i class="fa glyphicon glyphicon-rupee"><span>₹</span></i>
						<h2>Pay as you earn</h2>
						<p>No joining fee, set-up costs, subscription or any other hidden costs. Pay only a small fee for booking</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="hi-icon-wrap hi-icon-effect wow fadeInDown">
						<i class="fa glyphicon glyphicon-time"></i>
						<h2>On Time Payment</h2>
						<p>We guarantee on-time payments, every time. You will receive the money straight in your bank, every week</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="hi-icon-wrap hi-icon-effect wow fadeInDown">
						<i class="fa glyphicon glyphicon-eye-close"></i>
						<h2>Zero Involvement</h2>
						<p>No more hassle of handling customer calls, managing bookings and everything else in-between.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="hi-icon-wrap hi-icon-effect wow fadeInDown">
						<i class="fa glyphicon glyphicon-phone"></i>
						<h2>Easy to Use partner app</h2>
						<p>Managing online orders was never this easy. Track all your booking, calendar and payouts using a single app</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="hi-icon-wrap hi-icon-effect wow fadeInDown">
						<i class="fa glyphicon glyphicon-paste"></i>
						<h2>Get back to what you care about</h2>
						<p>We use our brand to promote yours with online advertising and various channels</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="pos-rel landing-cld-signup-process">
			<div class="about pad-t-15 pad-b-30">
				<h3 class="text-center pad-t-50">Get Listed Today</h3>
				<div class="timeline">
					<div class="circle"></div>
					<ul>
						<li>
							<div class="signup-process-title">Sign up now</div>
							<strong>Please leave your contact details below</strong>
						</li>
						<li>
							<div class="signup-process-title">We visit your place</div>
							<strong>Our expert team will meet you in person to help you understand all the details</strong>
						</li>
						<li>
							<div class="signup-process-title">We list your package</div>
							<strong>If your package meets our standards, we list your package on our platform for free</strong>
						</li>
						<li>
							<div class="signup-process-title">Start getting paid every week</div>
							<strong>You are ready to take orders. Get payouts straight to your bank every week</strong>
						</li>
					</ul>
					<div class="circle"></div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container">
			<div class="row">
				<h3 class="text-center pad-t-50">What Partner's Are Saying</h3>
				<div class="col-md-12">
					<div class="carousel slide" data-ride="carousel" id="quote-carousel">
						<div class="carousel-inner text-center">
							<div class="item active">
								<blockquote>
									<div class="row">
										<div class="col-sm-8 col-sm-offset-2">
											<p class="customer-review-signup">Best thing I like about evibe.in is their every week on-time settlements & their quick support for any settlement queries. !</p>
											<small>Mr. Prakash</small>
										</div>
									</div>
								</blockquote>
							</div>
							<div class="item">
								<blockquote>
									<div class="row">
										<div class="col-sm-8 col-sm-offset-2">
											<p class="customer-review-signup">Evibe.in has always been there for us with different marketing strategies that have helped to boost my business. We were sold out for Valentine’s day!</p>
											<small>Mr. Jitendar</small>
										</div>
									</div>
								</blockquote>
							</div>
							<div class="item">
								<blockquote>
									<div class="row">
										<div class="col-sm-8 col-sm-offset-2">
											<p class="customer-review-signup">Being one of the preferred partners for evibe.in is a beautiful experience for me. I am delighted to have the convenience of their technology & services.</p>
											<small>Ms. Ramya</small>
										</div>
									</div>
								</blockquote>
							</div>
						</div>
						<a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa glyphicon glyphicon-menu-left"></i></a>
						<a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa glyphicon glyphicon-menu-right"></i></a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div id="partner" class="pad-b-50">
			<div class="center pad-t-25">
				<h3 class="text-center pad-t-30">Top Brands Love Us</h3>
			</div>
			<div class="container">
				<ul class="partner-logo">
					<li>
						<img src="https://gallery.evibe.in/main/img/logo/royalorchid.png" class="img-responsive img-centered">
					</li>
					<li>
						<img src="https://gallery.evibe.in/main/img/logo/taj-deccan.png" class="img-responsive img-centered" style="min-height: 70px; margin-top: 30px;">
					</li>
					<li>
						<img src="https://gallery.evibe.in/main/img/logo/radisson.png" class="img-responsive img-centered" style="min-height: 70px; margin-top: 30px;">
					</li>
					<li>
						<img src="https://gallery.evibe.in/main/img/logo/the-lalit.png" class="img-responsive img-centered" style="min-height: 70px; margin-top: 30px;">
					</li>
					<li>
						<img src="https://gallery.evibe.in/main/img/logo/aloft.jpg" class="img-responsive img-centered" style="min-height: 70px; margin-top: 30px;">
					</li>
				</ul>
			</div>
		</div>
	</section>

	<section>
		<div class="container mar-b-60">
			<h3 class="pad-t-30 text-center no-mar-b">Getting Started</h3>
			<p class="text-center">Frequently Asked Questions By Our Partners</p>
			<div class="panel-group" id="faqAccordion">
				<div class="panel panel-default">
					<div class="panel-heading cur-point accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question0">
						<h4 class="panel-title">Q: What is Evibe.in?</h4>
					</div>
					<div id="question0" class="panel-collapse collapse" style="height: 0;">
						<p class="pad-10 no-mar-b">
							Evibe.in is the largest platform for celebrations in India that helps customers to celebrate their special occasions like birthdays, wedding anniversary, get together, with a wide variety of options.
						</p>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading cur-point accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question1">
						<h4 class="panel-title">Q: Do you charge any sign-up or listing fee?</h4>
					</div>
					<div id="question1" class="panel-collapse collapse" style="height: 0;">
						<p class="pad-10 no-mar-b">
							No, you can sign-up & list your places for free. We only charge a small fee for a booking.
						</p>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading cur-point accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question2">
						<h4 class="panel-title">Q: When/How do I get paid?</h4>
					</div>
					<div id="question2" class="panel-collapse collapse" style="height: 0;">
						<p class="pad-10 no-mar-b">
							A weekly payment will be made to you. A direct transfer will be made to your bank account which is registered with us. You will also receive a detailed breakup on the payouts.
						</p>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading cur-point accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question3">
						<h4 class="panel-title">Q: What kind of growth can I expect?</h4>
					</div>
					<div id="question3" class="panel-collapse collapse" style="height: 0;">
						<p class="pad-10 no-mar-b">
							The revenue that restaurants/places see varies in regards to the city you are in, customer demand, place type, timings, etc., but our top restaurants/places have seen a 100% growth.
						</p>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading cur-point accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question4">
						<h4 class="panel-title">Q: How quickly can I begin accepting orders?</h4>
					</div>
					<div id="question4" class="panel-collapse collapse" style="height: 0;">
						<p class="pad-10 no-mar-b">
							You can start receiving orders as soon as you are on-boarded. We aim to have you on our platform as quickly as possible.
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div id="cldSignUp" class="bkgnd-grey pad-t-10">
			<h3 class="pad-t-30 text-center no-mar-b">Sign Up Now</h3>
			<p class="text-center mar-l-10 mar-t-10">Please leave your contact details below & we’ll be in touch within 24 hours</p>
			<div class="mar-l-10 mar-r-10">
				<div class="form-horizontal no-mar-b">
					<div class="form-group">
						<div class="col-md-4 col-md-offset-4 inputGroupContainer">
							<div class="input-group">
								<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
								<input name="userName" id="userName" placeholder="Your Name" class="form-control" type="text">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-4 col-md-offset-4 inputGroupContainer">
							<div class="input-group">
								<span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
								<input name="userCity" id="userCity" placeholder="City" class="form-control" type="text">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-4 col-md-offset-4 inputGroupContainer">
							<div class="input-group">
								<span class="input-group-addon"><i class="glyphicon glyphicon-tent"></i></span>
								<input name="restaurantName" id="restaurantName" placeholder="Restaurant Name" class="form-control" type="text">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-4 col-md-offset-4 inputGroupContainer">
							<div class="input-group">
								<span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
								<input name="userPhone" id="userPhone" placeholder="Your Phone" class="form-control" type="text">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-4 col-md-offset-4 text-center">
							<button class="btn btn-evibe btn-lg cld-partner-signup">SUBMIT<span class="glyphicon glyphicon-send"></span></button>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-md-offset-4 pad-b-20">
				<p class="divider-text">
					<span class="bg-light">OR</span>
				</p>

				<div class="form-group">
					<div class="text-center">
						<button class="btn btn-link sign-up-friend-btn">SIGN UP FOR FRIEND</button>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<hr>
			<div class="text-center pad-b-20">
				<h5>Want to know more?</h5>
				<p class="no-mar-b">Give us a missed call and we''ll be in touch with you!</p>
				<h5 class="no-mar-t mar-t-10 font-15">Call us at +91-8919926594</h5>
			</div>
		</div>
	</section>

	<div class="modal fade modal-custom-form modal-enquiry-form" tabindex="-1" role="dialog" id="modalEnquiryForm">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div class="modal-inner-header">
						<button type="button" class="close close-reset-form ga-btn-close-enquire-now-modal" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title text-center">Submit Details</h4>
					</div>
					<form id="enquiryForm">
						<div class="sticky-error-wrap sticky-error-wrap-enquiry hide pad-t-20">
							<span class="error-message">Please fill all the required details.</span>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-lg-6 col-sm-12">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ticket_modal_elements">
									<input class="mdl-textfield__input" type="text" name="name" id="name" value=""/>
									<label class="mdl-textfield__label" for="name">Your Name</label>
								</div>
							</div>
							<div class="col-md-6 col-lg-6 col-sm-12">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ticket_modal_elements">
									<input class="mdl-textfield__input email-typo-error" type="text" name="city" id="city" value=""/>
									<label class="mdl-textfield__label" for="city">City</label>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-lg-6 col-sm-12">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ticket_modal_elements">
									<input class="mdl-textfield__input" type="text" name="friendName" id="friendName" value=""/>
									<label class="mdl-textfield__label" for="friendName">Friend Name</label>
								</div>
							</div>
							<div class="col-md-6 col-lg-6 col-sm-12">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ticket_modal_elements">
									<input class="mdl-textfield__input email-typo-error" type="text" name="friendPhone" id="friendPhone" value=""/>
									<label class="mdl-textfield__label" for="friendPhone">Friend Phone</label>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="form-group">
							<div class="col-sm-12 text-center">
								<button class="btn btn-link mar-r-5 close-reset-form" data-dismiss="modal">
									<em>Cancel</em>
								</button>
								<button id="btnSubmitCLDSignup" class="btn btn-primary mar-l-5">
									<div class="default">
										<span class="glyphicon glyphicon-ok"></span>
										<span class="valign-mid">SUBMIT</span>
									</div>
									<div class="waiting hide">
										<span class="glyphicon glyphicon-ok"></span>
										<span class="valign-mid">SUBMITTING..</span>
									</div>
								</button>
							</div>
							<div class="clearfix"></div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('javascript')
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			(function signUp() {
				$(".cld-partner-signup").on("click", function () {
					showLoading();

					$.ajax({
						url: "/partner/signup/cld/submit",
						type: "POST",
						data: {
							name: $("#userName").val(),
							restaurantName: $("#restaurantName").val(),
							city: $("#userCity").val(),
							phone: $("#userPhone").val(),
							friendName: "",
							isFriend: 0
						},
						success: function (data) {
							hideLoading();
							if (data.success) {
								window.showNotySuccess("Thank you! We've received your request. We will get in touch with you shortly.");
								$("#userName").val("");
								$("#restaurantName").val("");
								$("#userCity").val("");
								$("#userPhone").val("");
							} else {
								showNotyError(data.errors);
							}
						},
						error: function (jqXHR, textStatus, errorThrown) {
							hideLoading();
							window.showNotyError("Some error occurred while submitting your request.");
							window.notifyTeam({
								"url": "/partner/signup/cld/submit",
								"textStatus": textStatus,
								"errorThrown": errorThrown
							});
						}
					});
				})
			})();

			(function friendSignUp() {
				$(".sign-up-friend-btn").on("click", function () {
					$("#enquiryForm")[0].reset();

					$("#modalEnquiryForm").modal({
						"backdrop": "static",
						"keyboard": false,
						"show": true
					});
				});

				var btnSubmit = $("#btnSubmitCLDSignup");
				btnSubmit.on("click", function () {
					hideSubmitButton();
					$.ajax({
						url: "/partner/signup/cld/submit",
						type: "POST",
						data: {
							name: $("#name").val(),
							restaurantName: "",
							city: $("#city").val(),
							phone: $("#friendPhone").val(),
							friendName: $("#friendName").val(),
							isFriend: 1
						},
						success: function (data) {
							showSubmitButton();
							if (data.success) {
								window.showNotySuccess("Thank you! We've received your request. We will get in touch with you shortly.");
								$("#modalEnquiryForm").modal("hide");
							} else {
								var errorObj = $(".modal-enquiry-form").find(".sticky-error-wrap").removeClass("hide").find(".error-message");
								errorObj.text(data.errors);
							}
						},
						error: function (jqXHR, textStatus, errorThrown) {
							showSubmitButton();
							window.showNotyError("Some error occurred while submitting your request.");
							window.notifyTeam({
								"url": "/partner/signup/cld/submit",
								"textStatus": textStatus,
								"errorThrown": errorThrown
							});
						}
					});
				});

				function showSubmitButton() {
					btnSubmit.attr("disabled", false);
					btnSubmit.find(".default").removeClass("hide");
					btnSubmit.find(".waiting").addClass("hide");
				}

				function hideSubmitButton() {
					btnSubmit.attr("disabled", true);
					btnSubmit.find(".default").addClass("hide");
					btnSubmit.find(".waiting").removeClass("hide");
				}
			})();
		});
	</script>
@stop