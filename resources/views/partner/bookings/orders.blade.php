@extends("partner.base")

@section("innerContent")
	<div class="right_col" role="main">
		<div class="">
			<div class="page-title">
				<div class="title_left">
					<h3> Orders</h3>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="row">
				@if(count($data['res']['list']) > 0)
					@foreach($data['res']['list'] as $order)
						<div class="partner-enquiry-wrap">
							<div class="col-md-10 col-sm-10 col-xs-10">
								<div class="col-md-3 col-sm-3 col-xs-3 no-pad-l"><i class="fa fa-user"></i>
									<span class="">{{ $order['customerName'] }}</span>
								</div>
								<div class="col-md-3 col-sm-3 col-xs-3"><i class="fa fa-calendar"></i>
									<span class="">{{ $order['formattedPartyDate']." / ".$order['formattedPartyTime'] }}</span>
								</div>
								<div class="col-md-3 col-sm-3 col-xs-3"><i class="fa fa-phone"></i>
									<span>{{ $order['customerPhone']." / ".$order['customerAltPhone'] }}</span>
								</div>
								<div class="col-md-3 col-sm-3 col-xs-3"><i class="fa fa-building"></i>
									<span>@if($order['partyLocation']){{ $order['partyLocation'] }}@else{{ "---" }}@endif{{ " (".$order['zipCode'].")" }}</span>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-3 col-sm-3 col-xs-3 no-pad-l mar-t-5">
									<span><b>Booking Amount : </b></span>
									<span><i class="fa fa-inr" aria-hidden="true"></i> {{ $order['formattedBookingAmount'] }}</span>
								</div>
								<div class="col-md-3 col-sm-3 col-xs-3 mar-t-5">
									<span><b>Advance Paid : </b></span>
									<span><i class="fa fa-inr" aria-hidden="true"></i> {{ $order['formattedAdvancePaid'] }}</span>
								</div>
								<div class="col-md-3 col-sm-3 col-xs-3 mar-t-5">
									<span><b>Balance Amount : </b></span>
									<span><i class="fa fa-inr" aria-hidden="true"></i> {{ $order['formattedBalanceAmount'] }}</span>
								</div>
								<div class="col-md-3 col-sm-3 col-xs-3 mar-t-5">
									<span><b>Reference Images : </b></span>
									@foreach($order['attachments']['url'] as $image)
										<a target="_blank" href="{{ $image['original'] }}" style="text-decoration: none">
											<img src="{{ $image['thumbs'] }}" style="width:40px">
										</a>
									@endforeach
									@if($order['attachments']['count'] == 0)
										---
									@endif
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="col-md-2 col-sm-2 col-xs-2 text-center mar-t-5">
								<a href="{{ route("partner.dash.order.details", $order['id']) }}" class="btn btn-success btn-xs">See More Details</a>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					@endforeach
				@endif
				@if(!$data['res']['list'])
					<div class="x_panel">
						<div class="x_content">
							<div class="alert alert-success no-mar-b font-14">No Orders found, Please stay updated with all enquiries to get more orders.</div>
						</div>
					</div>
				@endif
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
@endsection