@extends("partner.base")

@section("innerContent")
	<div class="right_col" role="main">
		<div class="">
			<div class="page-title">
				<div class="title_left">
					<h3> Deliveries</h3>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="row">
				@if($data['res']['delivery']['attachments']['count'] > 0)
					<div class="x_panel">
						<div class="x_title">
							<h2>Uploaded Images</h2>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							@foreach($data['res']['delivery']['attachments']['url'] as $image)
								<div class="in-blk">
									<a href="{{ $image['original'] }}" target="_blank" class="blk">
										<img class="mar-l-5 mar-r-4 mar-t-5 mar-b-5" src="{{ $image['profile'] }}" width="100" height="100">
									</a>
									<a href="{{ route("partner.dash.delivery.image.delete", [$data['res']['delivery']['id'], $image['imageId']]) }}" class="text-center blk text-danger" onclick="return confirm('Are you sure to delete this image?')">
										<i class="fa fa-minus-circle"></i> Delete
									</a>
								</div>
							@endforeach
							<div class="text-center">
								<a href="{{ route('partner.dash.delivery.done', $data['res']['delivery']['id']) }}" class="btn btn-primary esolitoi" type="reset"><i class="fa fa-check-circle"></i> I AM DONE</a>
							</div>
						</div>
					</div>
				@else
					"No Images uploades"
				@endif
				<div class="clearfix"></div>
			</div>
			<div class="row">
				@if(isset($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'], "wv") > 0)
					<div class="form-group mar-t-20">
						<div class="text-center">
							<a href="https://evibe.in/partner/deliveries/open-camera" class="btn btn-success col-xs-12 font-18">
								<i class="fa fa-camera"></i> Take a Photo
							</a>
							<div class="clearfix"></div>
						</div>
						<p class="text-center font-16 mar-t-20">OR</p>
						<div class="text-center">
							<a href="https://evibe.in/partner/deliveries/open-gallery" class="btn btn-success col-xs-12 font-18">
								<i class="fa fa-folder-o"></i> Choose from Photo Library
							</a>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</div>
				@else
					<div class="x_panel">
						<div class="x_content">
							<section id="" class="partner-signup-sec">
								@if(session()->has('errorMsg'))
									<div class="alert alert-danger mar-l-10 mar-r-10 mar-b-10 pad-10">
										<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
										<i class="glyphicon glyphicon-remove-sign"></i>
										{{ session('errorMsg') }}
									</div>
								@endif
								@if(session()->has('successMsg'))
									<div class="alert alert-success mar-l-10 mar-r-10 mar-b-10 pad-10">
										<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
										<i class="glyphicon glyphicon-remove-sign"></i>
										{{ session('successMsg') }}
									</div>
								@endif

								<form action="{{ route("partner.dash.delivery.upload", $data['res']['delivery']['id']) }}" method="POST" class="form-horizontal" enctype="multipart/form-data" accept-charset="UTF-8" id="uploadDeliveryImages">
									<div id="drop-zone">
										<p class="mar-t-20">Drop files here</p>
										<div id="clickHere">or click here to choose files
											<input type="file" name="attachments[]" id="attachments" multiple/>
										</div>
										<div id='filename'></div>
									</div>
									<div class="form-group mar-t-20">
										<div class="col-xs-12 text-center">
											<button type="submit" class="btn btn-success uploadDeliveryImagesSubmitButton">
												<i class="fa fa-upload"></i> Upload @if($data['res']['delivery']['attachments']['count'] > 0){{ "More" }}@endif Images
											</button>
										</div>
										<div class="clearfix"></div>
									</div>
								</form>
								<div class="in-blk pull-right">
									<span class="required-star">* </span>
									<span style='color:grey' class="font-12 pad-r-6">Maximum 10 Images can be uploaded at a time.</span>
								</div>
								<div class="clearfix"></div>
							</section>
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
@endsection