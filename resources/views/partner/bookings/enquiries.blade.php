@extends("partner.base")

@section("innerContent")
	<div class="right_col" role="main">
		<div class="">
			<div class="page-title">
				<div class="title_left">
					<h3> Enquiries</h3>
				</div>
			</div>
			<div class="clearfix"></div>

			<div class="row">
				@php $isAvlCheck = 1; @endphp
				@if(count($data['res']['list']) > 0)
					@foreach($data['res']['list'] as $enquiry)
						<div class="partner-enquiry-wrap">
							<div class="col-md-10 col-sm-10 col-xs-10">
								<div class="col-md-4 col-sm-4 col-xs-4 no-pad-l"><i class="fa fa-calendar"></i>
									<span class="">{{ $enquiry['formattedPartyDate']." / ".$enquiry['formattedPartyTime'] }}</span>
								</div>
								<div class="col-md-2 col-sm-2 col-xs-2"><i class="fa fa-inr"></i>
									@if($enquiry['typeId'] == config("evibe.enquiry.type.avlCheck"))
										{{ $enquiry['bookingAmount'] }}
									@else
										{{ $enquiry['budget'] }}
									@endif
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4"><i class="fa fa-building"></i>
									@if($enquiry['partyLocation']){{ $enquiry['partyLocation'] . " (". $enquiry['pinCode'].")" }}@else{{ "---" }}@endif
								</div>
								<div class="col-md-2 col-sm-2 col-xs-2"><i class="fa fa-birthday-cake"></i>
									{{ $enquiry['occasion'] }}
								</div>
								<div class="clearfix"></div>
								<div class="col-md-6 col-sm-6 col-xs-6 no-pad-l mar-t-5">
									<span><b>Order details : </b></span>
									<span>@if($enquiry['details']){!! $enquiry['details'] !!}@else{{ "---" }}@endif</span>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6 mar-t-5">
									<span><b>Reference Images : </b></span>
									@foreach($enquiry['attachments']['url'] as $image)
										<a target="_blank" href="{{ $image['original'] }}" style="text-decoration: none">
											<img src="{{ $image['thumbs'] }}" style="width:40px">
										</a>
									@endforeach
									@if($enquiry['attachments']['count'] == 0)
										---
									@endif
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="col-md-2 col-sm-2 col-xs-2 text-center mar-t-5">
								@if($enquiry['typeId'] == config("evibe.enquiry.type.avlCheck"))
									<a data-url="{{ route("partner.dash.enquiries.reply", $enquiry['id']) }}" class="btn btn-success btn-partner-enquiry-reply btn" data-reply="yes" data-text="Accept Order" data-class="btn-success">Accept</a>
									<a data-url="{{ route("partner.dash.enquiries.reply", $enquiry['id']) }}" class="btn btn-danger btn-partner-enquiry-reply btn" data-reply="no" data-text="Reject Order" data-class="btn-danger">Reject</a>
									@php $isAvlCheck = 1; @endphp
								@else
									<a data-url="{{ route("partner.dash.enquiries.reply", $enquiry['id']) }}" class="btn btn-success btn-partner-enquiry-quote-reply" data-text="Submit Quote" data-class="btn-success">Submit Quote</a>
									@php $isAvlCheck = 0; @endphp
								@endif
							</div>
							<div class="clearfix"></div>
						</div>
					@endforeach
				@endif
				@if(!$data['res']['list'])
					<div class="x_panel">
						<div class="x_content">
							<div class="alert alert-success no-mar-b font-14">No Enquiries found, Please check this webpage or app frequently to reply faster to enquiries & to get more bookings.</div>
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>

	<input type="hidden" name="partnerEnquiryType" value="{{ $isAvlCheck }}">
	<input type="hidden" id="avlCheckConfigValue" value="{{ config('evibe.enquiry.type.avlCheck') }}">
	<input type="hidden" id="customQuoteConfigValue" value="{{ config('evibe.enquiry.type.customQuote') }}">
	<div class="modal fade modal-submit-partner-enquiry-response" tabindex="-1" role="dialog" id="modalSubmitPartnerEnquiryResponse">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<form id="fromSubmitPartnerEnquiryResponse" data-url="">
						<div class="form-group">
							<div class="col-md-12 col-lg-12 col-sm-12 partner-enquiry-quote-input hide mar-b-20">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
									<label class="mdl-textfield__label" for="partnerQuote">Your best quote</label>
									<input class="mdl-textfield__input full-width" name="partnerQuote" id="partnerQuote">
								</div>
							</div>
							<div class="col-md-12 col-lg-12 col-sm-12">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
									<label class="mdl-textfield__label" for="partnerComments">Comments (Optional)</label>
									<textarea class="mdl-textfield__input full-width" name="partnerComments" id="partnerComments"></textarea>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="form-group">
							<div class="col-sm-12 text-center">
								<button class="btn btn-link mar-r-5 close-reset-form" data-dismiss="modal">
									<i class="glyphicon glyphicon-remove"></i> <em>Cancel</em>
								</button>
								<button id="formPartnerEnquiryReviewBtnSubmit" type="submit" class="btn btn-success btn-submit mar-l-5">
									<span class="default">SUBMIT</span>
									<span class="waiting hide">SUBMITTING..</span>
								</button>
							</div>
							<div class="clearfix"></div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection