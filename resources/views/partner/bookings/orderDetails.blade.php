@extends("partner.base")

@section("innerContent")
	<div class="right_col" role="main">
		<div class="">
			<a href="{{ route("partner.dash.orders") }}">
				<h6><< Back to Orders</h6>
			</a>
			<div class="clearfix"></div>

			<div class="row">
				<div class="col-md-6 col-xs-12">
					<div class="x_panel">
						<div class="x_title">
							<h2>Order Details</h2>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							@if(count($data['res']['order']['additionalFields']) > 0)
								<table class="table table-striped table-hover text-left">
									@foreach($data['res']['order']['additionalFields'] as $field)
										<tr class="">
											<td class="table-top-row-remove-upper-border"><b>{{ $field['name'] }}</b>
											</td>
											<td class="table-top-row-remove-upper-border">{{ $field['value'] }}</td>
										</tr>
									@endforeach
								</table>
							@endif
							<div class="mar-t-10 mar-b-10">
								<span class="text-bold">Order Type:&nbsp;</span>{{ $data['res']['order']['title'] }}
							</div>
							<span class="text-bold">Order Info:</span>
							<br>
							{!! $data['res']['order']['orderDetails'] !!}
							@if(count($data['res']['order']['attachments']['url']) > 0)
								<div class="mar-t-10">
									<span class="text-bold">Reference Images:</span>
									<div class="x_content">
										@foreach($data['res']['order']['attachments']['url'] as $image)
											<div class="pad-5 in-blk">
												<a href="{{ $image['original'] }}" target="_blank"><img src="{{ $image['profile'] }}" width="80" height="80"></a>
											</div>
										@endforeach
									</div>
								</div>
							@endif
						</div>
					</div>
				</div>

				<div class="col-md-6 col-xs-12">
					<div class="x_panel">
						<div class="x_title">
							<h2>Venue Location</h2>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<table class="table table-striped table-hover text-left">
								<tr class="">
									<td class="table-top-row-remove-upper-border"><b>Venue Info</b></td>
									<td class="table-top-row-remove-upper-border">{{ $data['res']['order']['venueFullAddress'] }}</td>
								</tr>
								<tr class="">
									<td class="text-bold">Landmark</td>
									<td>{{ $data['res']['order']['venueLandMark'] }}</td>
								</tr>
								<tr class="">
									<td class="text-bold">Location on Map</td>
									<td>
										<a href="{{ $data['res']['order']['mapLink'] }}" target="_blank">{{ $data['res']['order']['mapLink'] }}</a>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="x_panel">
						<div class="x_title">
							<h2>Customer Info</h2>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<table class="table table-striped table-hover text-left">
								<tr class="">
									<td class="table-top-row-remove-upper-border"><b>Customer Name</b></td>
									<td class="table-top-row-remove-upper-border">{{ $data['res']['order']['customerName'] }}</td>
								</tr>
								<tr class="">
									<td class="text-bold">Phone Number</td>
									<td>{{ $data['res']['order']['customerPhone']." / ".$data['res']['order']['customerAltPhone'] }}</td>
								</tr>
								<tr class="">
									<td class="text-bold">Party Date / Time</td>
									<td>{{ $data['res']['order']['formattedPartyDate']." / ".$data['res']['order']['formattedPartyTime'] }}</td>
								</tr>
								<tr class="">
									<td class="text-bold">Party Location</td>
									<td>{{ $data['res']['order']['partyLocation']." (". $data['res']['order']['zipCode'] .")" }}</td>
								</tr>
								<tr class="">
									<td class="text-bold">Occasion</td>
									<td>{{ $data['res']['order']['occasion'] }}</td>
								</tr>
							</table>
						</div>
						<div class="clearfix"></div>
						@if(isset($data['res']['order']['deliveryNote']) && $data['res']['order']['deliveryNote'])
							<div class="pad-l-5">
								<span class="text-bold">Note: </span><span>{{ $data['res']['order']['deliveryNote'] }}</span>
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection