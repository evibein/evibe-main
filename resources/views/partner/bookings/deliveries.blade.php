@extends("partner.base")

@section("innerContent")
	<div class="right_col" role="main">
		<div class="">
			<div class="page-title">
				<div class="title_left">
					<h3> Deliveries</h3>
				</div>
			</div>
			<div class="clearfix"></div>

			<div class="row">
				@if(count($data['res']['list']) > 0)
					@foreach($data['res']['list'] as $delivery)
						<div class="partner-enquiry-wrap">
							<div class="col-md-9 col-sm-9 col-xs-7 no-pad-l">
								<div class="col-md-3 col-sm-3 col-xs-12 no-pad-l"><i class="fa fa-calendar"></i>&nbsp;
									<span class="">{{ $delivery['formattedPartyDate']." / ".$delivery['formattedPartyTime'] }}</span>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-12 no-pad-l"><i class="fa fa-user"></i>&nbsp;
									{{ $delivery['customerName'] }}
								</div>
								<div class="col-md-3 col-sm-3 col-xs-12 no-pad-l"><i class="fa fa-building"></i>&nbsp;
									@if($delivery['partyLocation']){{ $delivery['partyLocation'] }}@else{{ "---" }}@endif
								</div>
								<div class="col-md-2 col-sm-2 col-xs-12 no-pad-l"><i class="fa fa-birthday-cake"></i>&nbsp;
									{{ $delivery['occasion'] }}
								</div>
								<div class="clearfix"></div>
								<div class="mar-t-5">
									<span><b>Delivered Images : </b></span>
									@foreach($delivery['attachments']['url'] as $image)
										<a target="_blank" href="{{ $image['original'] }}" style="text-decoration: none">
											<img src="{{ $image['thumbs'] }}" style="width:40px">
										</a>
									@endforeach
									@if($delivery['attachments']['count'] == 0)
										---
									@endif
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="col-md-3 col-sm-3 col-xs-5 text-center mar-t-5">
								@if($delivery['attachments']['count'] > 0)
									<a href="{{ route('partner.dash.delivery.done', $delivery['id']) }}" class="btn btn-primary" type="reset"><i class="fa fa-check-circle"></i> I AM DONE</a>
								@endif
								<a href="{{ route("partner.dash.delivery.details", $delivery['id']) }}" class="btn btn-success">Upload Images</a>
							</div>
							<div class="clearfix"></div>
						</div>
					@endforeach
				@endif
				@if(!$data['res']['list'])
					<div class="x_panel">
						<div class="x_content">
							<div class="alert alert-success no-mar-b font-14">No Deliveries left, Please upload the images as soon as the party is over so that our team will update them in website to gain more customers.</div>
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
@endsection