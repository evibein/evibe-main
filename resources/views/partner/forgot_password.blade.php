@extends('partner.base_login')

@section("innerContent")
	<div class="right_col" role="main">
		<div class="">
			<div class="container-fluid ">
				<div class="form-cnt col-md-6 col-sm-8 col-md-offset-3 col-sm-offset-2">
					@if(isset($userId) && isset($emailId) && isset($token) && $userId)
						<form action="{{ route('partner.dash.forgot.password.check.token',['id' => $userId, 'token' => $token]) }}"
								method="post">
							<div class="panel panel-default mar-t-30 mar-b-40">
								<div class="panel-heading text-center font-18 text-bold">
									Password assistance
								</div>
								<div class="panel-body">
									<div class="text text-info mar-b-15">
										<h3><i class="fa fa-envelope-o"></i> Email Verification</h3>
										<p>
											We've sent a code to the email
											<b>{{ $emailId }}</b> to verify your identity. Please enter it below.
										</p>
									</div>
									@if(session()->has('invalidCredential'))
										<div class="form-group">
											<div class="alert alert-danger pad-5">
												<i class="glyphicon glyphicon-info-sign"></i>
												<span class="mar-l-5">{{ session('invalidCredential') }}</span>
											</div>
										</div>
									@endif
									<div class="form-group">
										<label for="otp">Enter Code</label>
										<input type="text" name="otp" id="otp" class="form-control" value="{{ old('otp') ? old('otp'): (isset($email) ? $email :"")  }}" required>
									@if($errors->has('otp'))
											<div class="help-block">
									<span class="text-danger">
										{{ $errors->first('otp') }}
									</span>
											</div>
										@endif
									</div>
									<div class="form-group">
										<div class="text-center">
											<button type="submit" class="btn btn-danger">
												Continue
											</button>
										</div>
									</div>
								</div>
							</div>
						</form>
					@else
						<form action="{{ route('partner.dash.forgot.password.email') }}" method="post">
							<div class="panel panel-default mar-t-30 mar-b-40">
								<div class="panel-heading text-center font-18 text-bold">
									Password assistance
								</div>
								<div class="panel-body">
									<div class="text text-info mar-b-15 text-center">
										<i class="glyphicon glyphicon-info-sign"></i> Enter the email address associated with your Evibe.in account.
									</div>
									@if(session()->has('invalidCredential'))
										<div class="form-group">
											<div class="alert alert-danger pad-5">
												<i class="glyphicon glyphicon-info-sign"></i>
												<span class="mar-l-5">{{ session('invalidCredential') }}</span>
											</div>
										</div>
									@endif
									<div class="form-group">
										<label for="email">Email</label>
										<input type="text" name="email" class="form-control" value="{{ old('email') ? old('email'): (isset($email) ? $email :"")  }}" placeholder="xyz@example.com">
										@if($errors->has('email'))
											<div class="help-block">
									<span class="text-danger">
										{{ $errors->first('email') }}
									</span>
											</div>
										@endif
									</div>
									<div class="form-group">
										<div class="text-center">
											<button type="submit" class="btn btn-danger">
												Continue
											</button>
										</div>
									</div>
									<input type="hidden" value="123456">
								</div>
							</div>
						</form>
					@endif
				</div>
			</div>
		</div>
	</div>
@endsection