@extends('layout.base')

@section('page-title')
	<title>{{ $seo['pageTitle'] }} | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="{{ $seo['pageDescription'] }}"/>
@endsection

@section('meta-keywords')
	<meta name="keywords" content="evibe.in, partner sign up, Bangalore, Hyderabad party service providers partners sign up"/>
@endsection

@section('og-title')
	<meta property="og:title" content="{{ $seo['pageTitle'] }} | Evibe.in"/>
@endsection

@section('og-description')
	<meta property="og:description" content="{{ $seo['pageDescription'] }} "/>
@endsection

@section('og-url')
	<meta property="og:url" content="{{ route('partner.sign-up.show') }}"/>
@endsection

@section('meta-canonical')
	<link rel="canonical" href="{{ $canonicalUrl }}/partner/signup"/>
@endsection

@section("header")
	@include('base.home.header.header-home-city')
@endsection

@section("footer")
	@include('app.footer_noncity')
@endsection

@section("content")
	<article>
		<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
			<section id="vsignup-form" class="partner-signup-sec">
				<div class="partner-header">
					<div class="top-slide" style="background-image:url({{ $galleryUrl }}/img/icons/partner/why_partner_bkgnd.jpg); max-height:250px;"></div>
					<div class="bg-overlay"></div>
					<div class="partner-message partner-message-form no-mar__400 no-mar-400-600">
						<h2 class="text-center mar-t-10__400">Partner with evibe.in</h2>
						<div class="col-sm-3 col-md-3 col-lg-3 col-md-offset-7 col-sm-offset-7 col-lg-offset-7">
							<a href="{{ route('partner.benefits') }}?ref=signup" class="btn btn-default btn-explore no-mar__400 no-mar-400-600">
								<i class="glyphicon glyphicon-eye-open font-20"></i> SEE BENEFITS
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				@if (session('success'))
					<div class="submit-success">
						<div class="alert alert-success no-mar-b">
							Thanks for signing up :-). We will get in touch with you shortly.
						</div>
					</div>
					<div class="home-link">
						<a href="/" class="btn btn-default btn-explore">Go to Home page</a>
					</div>
				@else
					<div class="partner-sub-head">
						<div class="congrats-msg">
							Thanks for showing your interest. Please fill your contact details below..
						</div>
					</div>
					<div class="pad-b-30">
						@if ($errors->count() > 0)
							<div class="alert alert-danger alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
								</button>
								<div>{{ $errors->first() }}</div>
							</div>
						@endif
						{{ Form::open([
							'url' => route('partner.sign-up.save'),
							'method' => 'POST',
							'class' => 'form-horizontal',
							'role' => 'form',
							'id' => 'vendorSignUpForm'
						]) }}
						<div class="col-lg-10 col-sm-10 col-md-10 col-xs-12 ">
							<div class="form-group">
								<div class="col-lg-3 col-md-3 col-sm-3 no-pad text-left-400-600 text-right">
									{{ Form::label('vendorSignUpName', 'Name:', ['class' => 'control-label partner-signup-lbl']) }}
									{{ Form::label(null, '*', ['class' => 'required-star'])}}
								</div>
								<div class="col-lg-9 col-md-9 col-sm-9 no-pad__400-600 no-pad__400">
									{{ Form::text('vendorSignUpName', old('vendorSignUpName') , [
										'placeholder' => 'Enter your full name',
										'class' => 'form-control'
									]) }}
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-3 col-md-3 col-sm-3 no-pad text-left-400-600 text-right ">
									{{ Form::label('vendorSignUpCompanyName', 'Company:', ['class' => 'control-label partner-signup-lbl']) }}
									{{ Form::label(null, '*', ['class' => 'required-star'])}}
								</div>
								<div class="col-lg-9 col-md-9 col-sm-9 no-pad__400-600 no-pad__400">
									{{ Form::text('vendorSignUpCompanyName', old('vendorSignUpCompanyName') , [
										'placeholder' => 'Enter your company name',
										'class' => 'form-control'
									]) }}
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-3 col-md-3 col-sm-3 no-pad text-left-400-600 text-right ">
									{{ Form::label('vendorSignUpEmail', 'Email:', ['class' => 'control-label partner-signup-lbl']) }}
									{{ Form::label(null, '*', ['class' => 'required-star'])}}
								</div>
								<div class="col-lg-9 col-md-9 col-sm-9 no-pad__400-600 no-pad__400">
									{{ Form::text('vendorSignUpEmail', old('vendorSignUpEmail') , [
										'placeholder' => 'myname@example.com',
										'class' => 'form-control email-typo-error'
									]) }}
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-3 col-md-3 col-sm-3 no-pad text-left-400-600 text-right ">
									{{ Form::label('vendorSignUpPhone', 'Phone:', ['class' => 'control-label partner-signup-lbl']) }}
									{{ Form::label(null, '*', ['class' => 'required-star'])}}
								</div>
								<div class="col-lg-9 col-md-9 col-sm-9 no-pad__400-600 no-pad__400">
									{{ Form::text('vendorSignUpPhone', old('vendorSignUpPhone'), [
										'placeholder' => 'Enter 10 digits phone number',
										'class' => 'form-control',
										'maxlength' => 10
									]) }}
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3 col-md-3 col-sm-3 no-pad text-left-400-600 text-right ">
									{{ Form::label('vendorSignupCategory', 'Category:', ['class' => 'control-label partner-signup-lbl']) }}
									{{ Form::label(null, '*', ['class' => 'required-star'])}}
								</div>
								<div class="col-lg-9 col-md-9 col-sm-9 no-pad__400-600 no-pad__400">
									{{ Form::select('vendorSignupCategory',
										$partnerTypes,
										null,
										array( 'placeholder' => 'Select a category...', 'class' => 'form-control')
									) }}
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3 col-md-3 col-sm-3 no-pad text-left-400-600 text-right ">
									{{ Form::label('vendorSignupCity', 'City:', ['class' => 'control-label partner-signup-lbl']) }}
									{{ Form::label(null, '*', ['class' => 'required-star'])}}
								</div>
								<div class="col-lg-9 col-md-9 col-sm-9 no-pad__400-600 no-pad__400">
									{{ Form::select('vendorSignupCity',
										$cities,
										null,
										array( 'placeholder' => 'Select the city ...', 'class' => 'form-control')
									) }}
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-3 col-md-3 col-sm-3 no-pad text-left-400-600 text-right ">
									<label for="vendorSignUpMessage" class="control-label partner-signup-lbl">
										Message:&nbsp;
									</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-9 no-pad__400-600 no-pad__400">
									{{ Form::textarea('vendorSignUpMessage', old('vendorSignUpMessage') , [
										'placeholder' => 'Enter your message (if any)',
										'class' => 'form-control vertical-resize',
										'rows' => '4',
										'spellcheck' => 'true'
									]) }}
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="mar-t-20">
							<div id="captchaFieldPartnerSignup"></div>
						</div>
						<div class="form-group mar-t-15">
							<div class="text-center">
								{{ Form::input('submit', 'vendorSignUpSubmit', 'Submit Details', [
									'class' => 'btn btn-evibe disabled',
									'id' => 'vendorSignUpSubmit'
								]) }}
							</div>
							<div class="clearfix"></div>
						</div>
						{{ Form::close() }}
						<div class="text-left">
							<span>( </span>
							<span class="required-star">* </span>
							<span style='color:grey'>fields are required</span>
							<span> )</span>
						</div>
					</div>
					<div class="clearfix"></div>
				@endif
			</section>
		</div>
		<div class="clearfix"></div>
	</article>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		var CaptchaCallback = function () {
			grecaptcha.render('captchaFieldPartnerSignup', {
				'sitekey': '{{ config('evibe.google.re_cap_site') }}',
				'callback': function (response) {
					$('#vendorSignUpSubmit').removeClass('disabled');
				}
			});
		};
	</script>
	<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
@endsection