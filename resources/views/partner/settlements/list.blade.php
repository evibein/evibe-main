@extends("partner.base")

@section("innerContent")
	<div class="right_col" role="main">
		<div class="">
			<div class="page-title">
				<h3>List of Settlements</h3>
			</div>
			<div class="clearfix"></div>

			<div class="row">
				<div class="col-md-12 no-pad">
					<div class="row text-center mar-t-15">
						@if ($agent->isMobile() && !($agent->isTablet()))
							<div class="">Settlements Date:</div>
							<div class="mar-t-5">
								<div class=" mar-l-10">
									<input type="text" class="settlement-date form-control" id="minSettlementProcessDate"
											@if(isset($data['minSettlementProcessDate']) && $data['minSettlementProcessDate'])
											value="{{ $data['minSettlementProcessDate'] }}" @endif>
								</div>
								<div class="mar-l-10">TO</div>
								<div class="mar-l-10">
									<input type="text" class="settlement-date form-control" id="maxSettlementProcessDate"
											@if(isset($data['maxSettlementProcessDate']) && $data['maxSettlementProcessDate'])
											value="{{ $data['maxSettlementProcessDate'] }}" @endif>
								</div>
							</div>
							<div class="mar-t-10">
								<div id="getSettlementsBtn" class="btn btn-success btn-get-settlements" data-url="{{ route('partner.dash.settlements.list') }}">Get Settlements</div>
							</div>
						@else
							<div class="in-blk">Settlements Date:</div>
							<div class="in-blk mar-l-10">
								<input type="text" class="settlement-date form-control" id="minSettlementProcessDate"
										@if(isset($data['minSettlementProcessDate']) && $data['minSettlementProcessDate'])
										value="{{ $data['minSettlementProcessDate'] }}" @endif>
							</div>
							<div class="in-blk mar-l-10">TO</div>
							<div class="in-blk mar-l-10">
								<input type="text" class="settlement-date form-control" id="maxSettlementProcessDate"
										@if(isset($data['maxSettlementProcessDate']) && $data['maxSettlementProcessDate'])
										value="{{ $data['maxSettlementProcessDate'] }}" @endif>
							</div>
							<div class="in-blk mar-l-30">
								<div id="getSettlementsBtn" class="btn btn-success btn-get-settlements" data-url="{{ route('partner.dash.settlements.list') }}">Get Settlements</div>
							</div>
						@endif
					</div>
					@if(isset($data['errorMsg']) && $data['errorMsg'])
						<div class="alert alert-danger font-16 text-center row mar-t-15" style="line-height: 25px">
							{{ $data['errorMsg'] }}
						</div>
					@endif
					@if(isset($data['settlementsList']) && count($data['settlementsList']))
						<div class="row mar-t-15">
							@foreach($data['settlementsList'] as $settlement)
								<div class="top-sec">
									<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
										<div class="sub-sec-title">Settlement Date</div>
										<div class="sub-sec-value">
											@if(isset($settlement['settlementDoneAt']) && $settlement['settlementDoneAt'])
												{{ date('d M Y, h:i A', strtotime($settlement['settlementDoneAt'])) }}
											@else
												--
											@endif
										</div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
										<div class="sub-sec-title">Settlement Amount</div>
										<div class="sub-sec-value">
											@price( $settlement['settlementAmount'] )
										</div>
									</div>
									@if ($agent->isMobile() && !($agent->isTablet()))
										<div class="clearfix"></div>
									@endif
									<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 text-center pad-t-5 pad-b-5 mar-t-5__400-600">
										@if(isset($settlement['id']) && $settlement['id'])
											<a class="font-16" href="{{ route('partner.dash.settlements.info', $settlement['id']) }}">
												<i class="glyphicon glyphicon-info-sign"></i> View Settlement Details
											</a>
										@else
											--
										@endif
									</div>
									<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 text-center pad-t-5 pad-b-5">
										@if(isset($settlement['invoiceUrl']) && $settlement['invoiceUrl'])
											<a class="font-16" href="{{ config('evibe.gallery.host').'/invoices/'.$settlement['invoiceUrl'] }}"
													target="_blank" download>
												<i class="glyphicon glyphicon-download"></i> Download Invoice
											</a>
										@else
											--
										@endif
									</div>
									<div class="clearfix"></div>
								</div>
							@endforeach
						</div>
					@else
						<div class="row mar-t-15 text-center text-r">
							There are no settlements in the given dates range
						</div>
					@endif
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
@endsection