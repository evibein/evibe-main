@extends("partner.base")

@section("innerContent")
	<div class="right_col" role="main">
		<div class="">
			<div class="page-title">
				<h3>Settlement Details</h3>
			</div>
			<div class="clearfix"></div>

			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad">
					@if(isset($data['errorMsg']) && $data['errorMsg'])
						<div class="alert alert-danger font-16 text-center row mar-t-15" style="line-height: 25px">
							{{ $data['errorMsg'] }}
						</div>
					@endif
					@if(isset($data['settlementDetails']) && $data['settlementDetails'])
						<div class="settlement-wrap mar-t-15 mar-b-30">
							<div class="settlement-info-wrap">
								<div class="top-sec">
									<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
										<div class="sub-sec-title">Invoice Number</div>
										<div class="sub-sec-value">
											@if(isset($data['settlementDetails']['invoiceNumber']) && $data['settlementDetails']['invoiceNumber'])
												{{ $data['settlementDetails']['invoiceNumber'] }}
											@else
												--
											@endif
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 mar-t-5__400-600">
										<div class="sub-sec-title">Settlement Date</div>
										<div class="sub-sec-value">
											@if(isset($data['settlementDetails']['settlementDoneAt']) && $data['settlementDetails']['settlementDoneAt'] && (strtotime($data['settlementDetails']['settlementDoneAt']) > 0))
												{{  date('d M Y, h:i A', strtotime($data['settlementDetails']['settlementDoneAt'])) }}
											@else
												--
											@endif
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 mar-t-5__400-600">
										<div class="sub-sec-title">Settlement Amount</div>
										<div class="sub-sec-value">
											@price($data['settlementDetails']['settlementAmount'])
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 mar-t-5__400-600 pad-t-5 pad-b-5">
										@if(isset($data['settlementDetails']['invoiceUrl']) && $data['settlementDetails']['invoiceUrl'])
											<a class="font-16" href="{{ config('evibe.gallery.host').'/invoices/'.$data['settlementDetails']['invoiceUrl'] }}" target="_blank" download>
												<i class="glyphicon glyphicon-download"></i> Download Invoice
											</a>
										@else
											--
										@endif
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							@if(isset($data['settlementDetails']['settlementItemsList']) && count($data['settlementDetails']['settlementItemsList']))
								<div class="booking-settlements-wrap mar-t-15">
									<div class="font-14 text-bold mar-b-10">
										<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
											Settlement Items (Bookings + Adjustments)
										</div>
										<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
											<span class="pull-right mar-t-5__400-600">
												<span class="cancelled-info-box mar-r-5"></span>
												<span class="text-normal">Cancelled Booking</span>
											</span>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="settlement-items-wrap">
										<table class="table table-bordered table-hover">
											<tr>
												<th>Party Date</th>
												<th>Location</th>
												<th>Booking Id</th>
												<th>Booking Amount</th>
												<th>Advance Amount</th>
												<th>Evibe Service Fee</th>
												<th>CGST</th>
												<th>SGST</th>
												<th>IGST</th>
												<th>Refund Amount</th>
												<th>Partner Bear</th>
												<th>Reference</th>
												<th>Settlement Amount</th>
												<!-- @todo: booking settlement / adjustment with reference -->
											</tr>
											@foreach($data['settlementDetails']['settlementItemsList'] as $settlementItem)
												<tr @if(isset($settlementItem['cancelledAt']) && $settlementItem['cancelledAt']) class="cancelled-booking-settlement" @endif>
													<td>
														@if(isset($settlementItem['partyDateTime']) && $settlementItem['partyDateTime'])
															{{ date('d M Y, h:i A', strtotime($settlementItem['partyDateTime'])) }}
														@else
															--
														@endif
													</td>
													<td>
														@if(isset($settlementItem['partyLocation']) && $settlementItem['partyLocation'])
															{{ $settlementItem['partyLocation'] }}
														@else
															--
														@endif
													</td>
													<td>
														@if(isset($settlementItem['bookingId']) && $settlementItem['bookingId'])
															{{ $settlementItem['bookingId'] }}
														@else
															--
														@endif
													</td>
													@if(isset($settlementItem['adjustmentComment']) && $settlementItem['adjustmentComment'])
														<td colspan="8">
															<b>Adjustment Comments: </b>{{ $settlementItem['adjustmentComment'] }}
														</td>
													@else
														<td>
															@if(isset($settlementItem['bookingAmount']) && $settlementItem['bookingAmount'])
																@price($settlementItem['bookingAmount'])
															@else
																--
															@endif
														</td>
														<td>
															@if(isset($settlementItem['advanceAmount']) && $settlementItem['advanceAmount'])
																@price($settlementItem['advanceAmount'])
															@else
																--
															@endif
														</td>
														<td>
															@if(isset($settlementItem['evibeServiceFee']) && $settlementItem['evibeServiceFee'])
																@price($settlementItem['evibeServiceFee'])
															@else
																--
															@endif
														</td>
														<td>
															@if(isset($settlementItem['cgst']) && $settlementItem['cgst'])
																@price($settlementItem['cgst'])
															@else
																--
															@endif
														</td>
														<td>
															@if(isset($settlementItem['sgst']) && $settlementItem['sgst'])
																@price($settlementItem['sgst'])
															@else
																--
															@endif
														</td>
														<td>
															@if(isset($settlementItem['igst']) && $settlementItem['igst'])
																@price($settlementItem['igst'])
															@else
																--
															@endif
														</td>
														<td>
															@if(isset($settlementItem['refundAmount']) && $settlementItem['refundAmount'])
																@price($settlementItem['refundAmount'])
															@else
																--
															@endif
														</td>
														<td>
															@if(isset($settlementItem['partnerBear']) && $settlementItem['partnerBear'])
																@price($settlementItem['partnerBear'])
															@else
																--
															@endif
														</td>
													@endif
													<td>
														@if(isset($settlementItem['bookingSettlementLink']) && $settlementItem['bookingSettlementLink'])
															<a href="{{ $settlementItem['bookingSettlementLink'] }}"
																	target="_blank">Booking Settlement</a>
														@elseif(isset($settlementItem['adjustmentLink']) && $settlementItem['adjustmentLink'])
															<a href="{{ $settlementItem['adjustmentLink'] }}"
																	target="_blank">Adjustment</a>
														@endif
													</td>
													<td>
														@if(isset($settlementItem['settlementAmount']) && $settlementItem['settlementAmount'])
															@if(isset($settlementItem['isPayback']) && $settlementItem['isPayback'])
																<span class="text-green">- @price($settlementItem['settlementAmount'])</span>
															@else
																@price($settlementItem['settlementAmount'])
															@endif
														@else
															--
														@endif
													</td>
												</tr>
											@endforeach
										</table>
									</div>
								</div>
							@endif
						</div>
					@endif
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
@endsection