@extends("partner.base")

@section("innerContent")
	<div class="right_col" role="main">
		<div class="">
			<div class="row">
				<div class="col-md-12">
					<div class="" role="tabpanel" data-example-id="togglable-tabs">
						<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
							<li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Messages</a>
							</li>
						</ul>
						@php $newCount = $pastCount = 0; @endphp
						<div id="myTabContent" class="tab-content">
							<div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
								<ul class="messages">
									@foreach($data["messages"] as $message)
										@php $newCount++; @endphp
										<li>
											<div class="message_date">
												<h3 class="date text-info no-mar-b">{{ date('d',strtotime($message->created_at)) }}</h3>
												<p class="month">{{ date('M',strtotime($message->created_at)) }}</p>
											</div>
											<div class="message_wrapper">
												<a href="{{ route('partner.dash.add.package.third', [$message->partner_option_id, "ref" => "messages"]) }}">
													<h4 class="heading">{{ $message->title }}</h4>
												</a>
												<blockquote class="message">
													@if($message->user_comment)
														{{ $message->user_comment }}
													@else
														Our team has reviewed responded to the package you had submitted, Please go through the details.
													@endif
												</blockquote>
											</div>
										</li>
									@endforeach
									@if($newCount == 0)
										<div class="text-center pad-10">No new messages.</div>
									@endif
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection