<div class="top_nav no-mar-l">
	<div class="nav_menu">
		<nav class="text-center">
			<div class="nav in-blk">
				<a class="site_title left-nav-evibe-title-logo">
					<img src="{{ config('evibe.gallery.host') }}/img/logo/logo_evibe.png">
				</a>
			</div>
		</nav>
	</div>
</div>