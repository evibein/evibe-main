<div class="col-md-3 left_col">
	<div class="left_col scroll-view">
		<div class="navbar nav_title" style="border: 0;">
			<a class="site_title left-nav-evibe-title-logo">
				<img src="{{ config('evibe.gallery.host') }}/img/logo/logo_google_auth.png" width="40" height="40">
				<span class="">Evibe.in <b>PARTNER</b></span></a>
		</div>

		<div class="clearfix"></div>

		<!-- menu profile quick info -->
		<div class="profile clearfix">
			<div class="profile_pic">
				<div class="img-circle profile_img">
					<span class="partner-profile-icon">
						{{ ucwords(substr($data['user']->name,0,1)) }}
					</span>
				</div>
			</div>
			<div class="profile_info">
				<span>Welcome,</span>
				<h2>{{ ucwords($data['user']->name) }}</h2>
			</div>
		</div>
		<!-- /menu profile quick info -->

		<br/>

		<!-- sidebar menu -->
		<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
			<div class="menu_section">
				<ul class="nav side-menu">
					<li>
						<a href="{{ route("partner.dash.dashboard") }}"><i class="fa fa-tachometer"></i> DashBoard</a>
					</li>
					<li>
						<a href="{{ route("partner.dash.enquiries") }}"> <i class="fa fa-bell-o"></i> New Enquiries </a>
					</li>
					<li>
						<a href="{{ route("partner.dash.orders") }}"><i class="fa fa-truck"></i> Upcoming Orders</a>
					</li>
					<li>
						<a href="{{ route("partner.dash.deliveries") }}"><i class="fa fa-file-text"></i> Pending Deliveries</a>
					</li>
					<li><a><i class="fa fa-clone"></i> Packages <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="{{ route("partner.dash.view.packages", "live") }}">Live on Evibe</a></li>
							<li><a href="{{ route("partner.dash.view.packages", "submitted") }}">Submitted Packages</a>
							</li>
							<li><a href="{{ route("partner.dash.view.packages", "draft") }}">Saved as Drafts</a></li>
							<li class="hide"><a href="{{ route("partner.dash.view.packages", "rejected") }}">Rejected Packages</a>
							</li>
							<li class="hide"><a href="{{ route("partner.dash.view.packages", "all") }}">All Packages</a>
							</li>
						</ul>
					</li>
					<li><a href="{{ route("partner.dash.messages") }}"><i class="fa fa-envelope-o"></i> Messages</a>
					<li><a href="{{ route("partner.dash.settlements.list") }}"><i class="fa fa-money"></i> Settlements</a>
					</li>
					<li><a><i class="fa fa-user"></i> My Account <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="{{ route("partner.dash.view.profile") }}">Profile</a></li>
							<li class="hide"><a href="{{ route("partner.dash.change.password") }}">Change Password</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>

		</div>
		<!-- /sidebar menu -->

		<!-- /menu footer buttons -->
		<div class="sidebar-footer hidden-small">
			<a data-toggle="tooltip" data-placement="top" title="Contact Us" class="btnContactEvibe">
				<span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span>
			</a>
			<a data-toggle="tooltip" data-placement="top" title="Logout" href="{{ route('partner.dash.logout') }}">
				<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
			</a>
		</div>
		<!-- /menu footer buttons -->

		<!-- contact evibe -->
		<div class="modal" id="contactModal" data-url="{{ route('partner.dash.contact') }}">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<div>
							<div class="pull-left">
								<h4 class="no-mar no-pad">Contact Evibe.in</h4>
							</div>
							<div class="pull-right">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="modal-body">
						<div class="alert-danger mar-b-10 pad-5 hide" id="errorBox"></div>
						<div class="form-group">
							<label for="subject">Subject *</label>
							<input type="text" class="form-control" name="subject" id="subject" placeholder="Please write the subject of query. (maximum: 100 characters)">
						</div>
						<div class="form-group mar-t-10">
							<label for="message">Your Message</label>
							<textarea name="message" id="message" rows="5" class="form-control" placeholder="Please explain your query in brief"></textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="btnContactSubmit"><i
									class="glyphicon glyphicon-send"></i> Send
						</button>
					</div>
				</div>
			</div>
		</div>
		<section class="modal fade booking-form-success" id="partnerQuerySuccessModal" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title">Thank you :-)</h3>
					</div>
					<div class="modal-body">
					<span class="msg msg-gen">
						<p>Your query has been submitted successfully. Your business Manager at Evibe will get
							back to you shortly. If you need anything urgent, we recommend you to call us on
							<b>{{ config('evibe.contact.company.phone') }}</b> ({{ config('evibe.contact.company.working.start_day') }} to {{ config('evibe.contact.company.working.end_day') }}
							from {{ config('evibe.contact.company.working.start_time') }} to {{ config('evibe.contact.company.working.end_time') }}) (ext - 4).
						</p>
					</span>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary pull-right" data-dismiss="modal" aria-hidden="true">Okay
						</button>
					</div>
				</div>
			</div>
		</section>

		<!--loading modals -->
		<section id="waitingModal" class="modal fade submit-msg-modal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body text-center submitting-msg-wrap">
						<p class="submitting-msg">One moment please.</p>
						<p>
							<img src="{{ $galleryUrl }}/img/icons/fb_loading.png" alt="loading" class="rotate">
							<img src="{{ $galleryUrl }}/img/icons/fb_loading.png" alt="loading" class="reverse-rotate">
							<img src="{{ $galleryUrl }}/img/icons/fb_loading.png" alt="loading" class="rotate">
							<img src="{{ $galleryUrl }}/img/icons/fb_loading.png" alt="loading" class="reverse-rotate">
							<img src="{{ $galleryUrl }}/img/icons/fb_loading.png" alt="loading" class="rotate">
						</p>
					</div>
				</div>
			</div>
		</section>
		<section id="modalPartnerLoading" class="modal modal-loading" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-md">
				<div class="modal-content">
					<div class="modal-body">
						<h4>
							<img src="{{ $galleryUrl }}/img/icons/loading.gif" alt="Loading icon"> Loading, please wait...
						</h4>
						<div class="text-danger">(Please do not press refresh / back button)</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>