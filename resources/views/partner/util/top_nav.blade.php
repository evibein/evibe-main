<div class="top_nav top_nav_partner_package">
	<div class="nav_menu">
		<nav>
			<div class="nav toggle">
				<a id="menu_toggle"><i class="fa fa-bars"></i></a>
			</div>

			<ul class="nav navbar-nav navbar-right">
				<li class="">
					<a href="javascript:" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">{{ ucwords($data['user']->name) }}
						<span class=" fa fa-angle-down"></span>
					</a>
					<ul class="dropdown-menu dropdown-usermenu pull-right">
						<li><a href="{{ route("partner.dash.view.profile") }}"> Profile</a></li>
						<li><a class="btnContactEvibe">Contact Us</a></li>
						<li><a href="{{ route('partner.dash.logout') }}"><i
										class="fa fa-sign-out pull-right"></i> Log Out</a></li>
					</ul>
				</li>

				<li role="presentation" class="dropdown">
					<a href="javascript:" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
						<i class="fa fa-envelope-o"></i>
						<span class="badge bg-green top_nav_msg_count">0</span>
					</a>
					<ul id="menu1" class="dropdown-menu list-unstyled msg_list top_nav_msg_list" role="menu">

					</ul>
				</li>
			</ul>
		</nav>
	</div>

	<div class="clearfix"></div>

	@if(isset($data['pageName']) && $data['pageName'])
		<div class="partner-package-info-bar">
			<div class="alert alert-info">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<i class="fa fa-info-circle"></i> {{ config("evibe.partner.help_info.".$data['pageName']) }}
			</div>
		</div>
	@endif
</div>