@foreach($messages as $message)
	<li>
		<a href="{{ route('partner.dash.add.package.third', [$message->partner_option_id, "ref" => "messages"]) }}">
			<span>
				<span class="text-bold">{{ $message->title }}</span>
			</span>
			<span class="message">
				@if($message->user_comment)
					{{ $message->user_comment }}
				@else
					Our team has reviewed responded to the package you had submitted, Please go through the details.
				@endif
			</span>
		</a>
	</li>
@endforeach
@if($messages->count() == 0)
	<div class="text-center pad-10">No New Messages</div>
@endif
<li>
	<div class="text-center">
		<a href="{{ route("partner.dash.messages") }}">
			<strong>See All Messages</strong>
			<i class="fa fa-angle-right"></i>
		</a>
	</div>
</li>