<div class='col-lg-4 mar-b-20 '>
	<label>{{ ucfirst($field['field']['meta_name']) }} :</label>
	<select name="{{ $field['field']['name'] }}" class="form-control">
		@foreach($field['options'] as $option)
			<option value="{{ $option->id }}" @if(old($field['field']['name']) == $option->id || ($field['oldValueId'] && ($field['oldValueId'] == $option->id))) selected @endif>
				{{ $option->value }}</option>
		@endforeach
	</select>
</div>