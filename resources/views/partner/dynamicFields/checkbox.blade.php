<div class='col-lg-4 mar-b-20'>
	<label>{{ ucfirst($field['field']['meta_name']) }} :</label>
	<br>
	@foreach($field['options'] as $option)
		<span class='mar-r-20'>
			<input type='checkbox' name="{{ $field['field']['name'] }}[]" value="{{ $option->id }}" @if(old($field['field']['name']) == $option->id || ($field['checkBoxValues'] && in_array($option->id,$field['checkBoxValues']))) checked @endif> {{ $option->value }}
		</span>
	@endforeach
</div>