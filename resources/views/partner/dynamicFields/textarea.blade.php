<div class="form-group">
	<div class="col-lg-4 col-md-4 col-sm-4 no-pad text-left-400-600 text-right mar-t-20">
		<label for="{{ $field->original_field_name }}" class="control-label">{{ ucfirst($field->field_name) }} :</label>
		<label class="required-star">*</label>
	</div>
	<div class="col-lg-7 col-md-7 col-sm-7 no-pad__400-600 no-pad__400 mar-t-20">
		<textArea type=text class="form-control" name="{{ $field->original_field_name }}" id="{{ $field->original_field_name }}" rows="4" placeholder="@if($field->placeholder) {{ $field->placeholder }} @else Enter {{ $field->field_name }} @endif"> @if(old($field->original_field_name)){{ old($field->original_field_name) }} @endif</textArea>
	</div>
	<div class="clearfix"></div>
</div>