<div class="form-group">
	<div class="col-lg-4 col-md-4 col-sm-4 no-pad text-left-400-600 text-right mar-t-20">
		<label for="{{ $field->original_field_name }}" class="control-label">{{ ucfirst($field->field_name) }} : <label class="required-star">*</label>
		</label>
	</div>
	<div class="col-lg-7 col-md-7 col-sm-7 no-pad__400-600 no-pad__400 mar-t-20">
		<input type=text class="form-control" name="{{ $field->original_field_name }}" id="{{ $field->original_field_name }}" placeholder="@if($field->placeholder) {{ $field->placeholder }} @else Enter {{ $field->field_name }} @endif" value="@if(old($field->original_field_name)){{ old($field->original_field_name) }}@elseif(isset($data['packageValues']) && $data['packageValues']->where('type_partner_option_field_id', $field->id)->first()){{ $data['packageValues']->where('type_partner_option_field_id', $field->id)->first()->value }}@endif">
	</div>
	<div class="clearfix"></div>
</div>