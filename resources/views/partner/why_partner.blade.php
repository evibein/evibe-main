@extends('layout.base')

@section('page-title')
	<title>{{ $data['seo']['pageTitle'] }} | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="{{ $data['seo']['pageDescription'] }}"/>
@endsection

@section('og-title')
	<meta property="og:title" content="{{ $data['seo']['pageTitle'] }} | Evibe.in"/>
@endsection

@section('og-description')
	<meta property="og:description" content="{{ $data['seo']['pageDescription'] }}"/>
@endsection

@section('og-url')
	<meta property="og:url" content="{{ route('partner.benefits') }}"/>
@endsection

@section('meta-canonical')
	<link rel="canonical" href="{{ $canonicalUrl }}/partner/signup"/>
@endsection

@section("header")
	@include('base.home.header.header-top-city')
@endsection

@section("footer")
	@include('app.footer_noncity')
@endsection

@section("content")
	<section class="why-partner-sec">
		<div class="partner-header">
			<div class="bg-img-cnt">
				<div class="bg-img" style="background-image:url({{ $galleryUrl }}/img/icons/partner/why_partner_bkgnd.jpg);"></div>
				<div class="bg-overlay"></div>
			</div>
			<div class="partner-message no-mar__400 mar-t-20-400-600">
				<div class="pitch-lines no-mar__400 no-mar-400-600">
					<div>Grow your Business</div>
					<div class="line-2">without any extra efforts.</div>
				</div>
				<a href="{{ route('partner.sign-up.show') }}" class="btn btn-default btn-lg btn-partner-signup no-mar__400 no-mar-400-600">
					<i class="glyphicon glyphicon-ok-circle"></i> Sign Up Now
				</a>
			</div>
		</div>
		<div class="partner-menu">
			<div class="col-sm-12 col-md-12 col-lg-12 why-partner-menu partner-sticky hidden-xs">
				<ul id="partner-menu" class="col-sm-8 col-md-8 col-lg-8 col-md-offset-2 col-sm-offset-2 col-lg-offset-2">
					<li><a href="#benefits">Benefits</a></li>
					<li><a href="#howItWorks">How it works</a></li>
					<li><a href="#faqs">FAQs</a></li>
					<li><a href="#partnerStories">Partner Stories</a></li>
					<li class="header-signup-btn-cnt hide">
						<a href="{{ route('partner.sign-up.show') }}" class="btn btn-default btn-explore btn-partner-signup">
							<i class="glyphicon glyphicon-ok-circle"></i> Sign Up Now
						</a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div id="partnerContent" class="partner-content">
			<div class="col-sm-10 col-md-10 col-lg-10 col-md-offset-1 col-sm-offset-1 col-lg-offset-1">
				<div id="benefits" class="partner-data">
					<h4 class="data-title">Your Benefits</h4>
					<div class="partner-benefits">
						<div class="col-sm-3 col-md-3 col-lg-3 pad-t-20__400 pad-b-20__400">
							<img src="{{ $galleryUrl }}/img/icons/partner/benefits_hasslefree.png" alt="hassle free bookings" class="img-responsive">
							<h5 class="sub-sec-title">Hassle Free Bookings</h5>
							<div class="sub-sec-desc">Get confirmed bookings and not leads. We take care from customer enquiries to bookings. You can now just focus on the execution.</div>
						</div>
						<div class="col-sm-3 col-md-3 col-lg-3 pad-t-20__400 pad-b-20__400 pad-t-20-400-600">
							<img src="{{ $galleryUrl }}/img/icons/partner/benefits_earnpay.png" alt="pay when you earn" class="img-responsive">
							<h5 class="sub-sec-title">Pay When You Earn</h5>
							<div class="sub-sec-desc">There is no sign up cost or fixed costs or hidden costs. Pay us only when you earn.</div>
						</div>
						<div class="col-sm-3 col-md-3 col-lg-3 pad-t-20__400 pad-b-20__400 pad-t-20-400-600">
							<img src="{{ $galleryUrl }}/img/icons/partner/benefits-payments.png" alt="on-time payments" class="img-responsive">
							<h5 class="sub-sec-title">On-time Payments</h5>
							<div class="sub-sec-desc">Get on-time payment. Every time. It’s our promise. We also hate late payments.</div>
						</div>
						<div class="col-sm-3 col-md-3 col-lg-3 pad-t-20__400 pad-t-20-400-600">
							<img src="{{ $galleryUrl }}/img/icons/partner/benefits_support.png" alt="professional support" class="img-responsive">
							<h5 class="sub-sec-title">Professional Support</h5>
							<div class="sub-sec-desc">Get market insights and feedback to help you grow. We believe partner success is our success.</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div id="howItWorks" class="partner-data">
					<h4 class="data-title">How It Works</h4>
					<div class="partner-hiw">
						@foreach  ($data['howItWorks'] as $key => $hiw)
							<div class="hiw-item">
								<div class="col-sm-4 hiw-step-cnt hide-400-600 hide__400 @if($key % 2 == 0) pull-right @endif">
									<img src="{{ $galleryUrl }}/img/icons/partner/{{ $hiw['icon'] }}" alt="{{ $hiw['step'] }}" class="img-responsive in-blk">
									<div class="text-bold">{{ $hiw['step'] }}</div>
								</div>
								<div class="col-sm-8 hiw-content-cnt @if($key % 2 == 0) pull-left @endif">
									<div class="unhide__400 show-400-600 text-center hide text-bold pad-b-20__400 partner-step-txt">{{ $hiw['step'] }}</div>
									<div class="text-left">
										<h5 class="hiw-title">{{ $hiw['title'] }}</h5>
										<div class="hiw-data">{!! $hiw['desc'] !!}</div>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						@endforeach
					</div>
				</div>
				<div id="faqs" class="partner-data">
					<h4 class="data-title">Frequently Asked Questions (FAQs)</h4>
					<div class="partner-faqs">
						@foreach($data['faqs'] as $faq)
							<div class="faqs-item">
								<div class="faq-question">Q: {{ $faq['question'] }}</div>
								<div class="faq-answer">{!! $faq['answer'] !!}</div>
							</div>
						@endforeach
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div id="partnerStories" class="partner-stories">
			<h4 class="data-title">Partner Stories</h4>
			<div class="stories-cnt">
				<div class="col-sm-10 col-md-10 col-lg-10 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
					<div id="partner-slide" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner">
							<?php $count = 0 ?>
							@foreach($data['vendorStories'] as $vendor)
								<div class="item @if($count == 0) active @endif">
									<div class="col-sm-12 col-md-4 col-lg-4 mar-b-30-400-600">
										<div class="story-section">
											<div class="video-section">
												<?php if($vendor['type_id'] == 0) : ?>
												<img width="100%" src="{{ $galleryUrl }}/main/img/{{$vendor['url']}}" alt="partner stories" class="partner-story-img">
												<?php elseif($vendor['type_id'] == 1) : ?>
												<iframe id="ytplayer-{{$count}}" type="text/html" width="50%" height="300" src="https://youtube.com/embed/{{ $vendor['url']}}?enablejsapi=1&fs=0&showinfo=0&controls=0&rel=0&html5=1" frameborder="0" allowfullscreen>
												</iframe>
												<?php endif; ?>
											</div>
											<div class="story-txt-cnt">
												<div class="text-section">
													<div class="c-review"> {{ $vendor['story'] }} </div>
												</div>
												<div class="author-title">
													{{ $vendor['provider']['person'] }}
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php $count++; ?>
							@endforeach
						</div>
						<a class="carousel-control partner-slide-prev hidden-sm hidden-xs" href="#partner-slide" data-slide="prev">
							<img src="{{ $galleryUrl }}/img/icons/icon_left_thin.png" alt="Prev">
						</a>
						<a class="carousel-control partner-slide-next hidden-sm hidden-xs" href="#partner-slide" data-slide="next">
							<img src="{{ $galleryUrl }}/img/icons/icon_right_thin.png" alt="Next">
						</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</section>
@endsection
