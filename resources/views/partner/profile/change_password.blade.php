@extends("partner.base")

@section("innerContent")
	<div class="right_col" role="main">
		<div class="">
			<div class="page-title">
				<div class="title_left">
					<h3> Change Password</h3>
				</div>
			</div>
			<div class="clearfix"></div>

			<div class="row">
				<div class="x_panel">
					<div class="x_content">
						@if(session()->has('invalidCredential'))
							<div class="form-group">
								<div class="alert-danger pad-5">
									<i class="glyphicon glyphicon-info-sign"></i>
									<span class="mar-l-5">{{ session('invalidCredential') }}</span>
								</div>
							</div>
						@endif
						@if(session()->has('successMessage'))
							<div class="form-group">
								<div class="alert-success pad-5">
									<i class="glyphicon glyphicon-info-sign"></i>
									<span class="mar-l-5">{{ session('successMessage') }}</span>
								</div>
							</div>
						@endif
						<form id="changePasswordForm" data-parsley-validate class="form-horizontal form-label-left" method="post" action="{{ route("partner.dash.update.password") }}">
							<div class="item form-group">
								<label for="oldPassword" class="control-label col-md-3">Current Password</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input id="oldPassword" type="password" name="oldPassword" class="form-control col-md-7 col-xs-12" required="required" value="{{ old('oldPassword') }}">
								</div>
							</div>
							<div class="item form-group">
								<label for="password" class="control-label col-md-3">New Password</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input id="password" type="password" name="password" class="form-control col-md-7 col-xs-12" required="required" value="{{ old('password') }}">
								</div>
							</div>
							<div class="item form-group">
								<label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">Repeat New Password</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input id="password2" type="password" name="password2" data-validate-linked="password" class="form-control col-md-7 col-xs-12" required="required" value="{{ old('password2') }}">
								</div>
							</div>
							<div class="ln_solid"></div>
							<div class="form-group text-center">
								<a class="btn btn-primary" href="{{ route("partner.dash.view.profile") }}">Cancel</a>
								<button type="submit" class="btn btn-success">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection