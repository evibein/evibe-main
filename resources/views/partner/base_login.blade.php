<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en"> <!--<![endif]-->

<head>
	<!-- GTM DataLayer -->
	@yield('gtm-data-layer')

	<!-- trackers -->
	@include('track.gtm')

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width">
	<meta name="google-site-verification" content="aAggmaMI_knVUeswYULXQSKD41sWlr2z3G21bdkPZoI"/>
	<meta name="sitelock-site-verification" content="7865"/>
	<meta name="msvalidate.01" content="31489A18F1AD59FC089CB9B9A7E5A85D"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	@section('page-title')
		<title>Partner Profile | Evibe.in</title>
	@show

	<link rel="icon" href="https://gallery.evibe.in/img/logo/favicon_evibe_v2.png" type="image/png" sizes="16x16">

	@section('custom-css')
		<link rel="stylesheet" href="{{ elixir('css/app/partner-profile.css') }}"/>
	@show

	@yield('push-notifications')
	@yield('custom-head')
</head>

<body class="nav-md">
<div class="container body">
	<div class="main_container">

		@include('partner.util.top_nav_login')

		@yield("innerContent")
	</div>
</div>

<input type="hidden" id="isLoginPage" value="1">
<script src="{{ elixir('js/partner/partner.js') }}"></script>

@yield('javascript')

</body>
</html>