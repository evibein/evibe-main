@extends("partner.base")

@section("innerContent")
	<div class="right_col" role="main">
		<div class="col-lg-8 col-md-8 col-sm-8 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">
			<div class="page-title">
				<div class="title_left">
					<h3> Profile Information</h3>
				</div>
			</div>
			<div class="clearfix"></div>

			<div class="row">
				<div class="x_panel">
					<div class="x_title">
						<h2>Basic Info</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<table class="table table-striped table-hover text-left">
							<tr class="">
								<td class="table-top-row-remove-upper-border"><b>Company Name</b></td>
								<td class="table-top-row-remove-upper-border">{{ $data['partner']->person }}</td>
							</tr>
							<tr class="">
								<td class="text-bold">Contact Person</td>
								<td>{{ $data['partner']->name }}</td>
							</tr>
							<tr class="">
								<td class="text-bold">Phone Number</td>
								<td>{{ $data['partner']->phone }}</td>
							</tr>
							<tr class="">
								<td class="text-bold">Alternate Phone Number</td>
								<td>{{ $data['partner']->alt_phone }}</td>
							</tr>
							<tr class="">
								<td class="text-bold">Email</td>
								<td>{{ $data['partner']->email }}</td>
							</tr>
							<tr class="">
								<td class="text-bold">Alternate Email</td>
								<td>{{ $data['partner']->alt_email }}</td>
							</tr>
							<tr class="">
								<td class="text-bold">Area</td>
								<td>@if($data['partner']->area) {{ $data['partner']->area->name }} @else -- @endif</td>
							</tr>
							<tr class="">
								<td class="text-bold">City</td>
								<td>@if($data['partner']->city) {{ $data['partner']->city->name }} @else -- @endif</td>
							</tr>
						</table>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="x_panel">
					<div class="x_title">
						<h2>Bank Details</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="text-center">
							<b>Coming soon...</b>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection