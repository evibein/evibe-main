@extends("partner.base")

@section('innerContent')
	<div class="right_col" role="main">
		<div class="row mar-b-10">
			<div class="in-blk mar-t-5 text-bold">
				<span class="font-14">Week summary from
					<span class="dashboard-start-date">{{ $data['humanReadableStartDay'] }}</span> to
					<span class="dashboard-end-date">{{ $data['humanReadableEndDay'] }}</span>
				</span>
			</div>
		</div>
		<div class="dashboard-home-container"></div>
		<div class="row completed-orders-wrap">
			<div class="x_panel">
				<div class="x_title">
					<div class="col-sm-6 no-pad-l">
						<h2>
							Completed Orders
							<small>Summary of all the orders executed by you in the past week.</small>
						</h2>
						<div class="clearfix"></div>
					</div>
					<div class="col-sm-6">
						<input id="bookingRange" name="bookingRange" class="pull-right" type="text" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc" value="{{ $data['dateRange'] }}">
						<div class="in-blk pull-right mar-t-5 mar-r-4">
							<span class="">Change Date : </span>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="x_content no-mar-b">
					<div class="alert alert-danger font-14 text-center text-bold no-mar-b">
						There are no bookings found in the last week.
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" id="isDashboardHome" value="1">
	</div>
	<input id="postUrl" type="hidden" value="{{ route("partner.dash.dashboard.filter") }}">
@endsection