@if($data['response']['success'] && $data['response']['data']['total_bookings_count'] && $data['response']['data']['total_bookings_count'] > 0)
	<div class="row">
		<div class="x_panel">
			<div class="x_title">
				<h2>
					New Orders
					<small>Summary of all the orders you got in the past week.</small>
				</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content no-mar-b">
				<table id="datatable-responsive" class="table table-bordered dt-responsive nowrap no-mar-b" cellspacing="0" width="100%">
					<thead>
					<tr>
						<th>Booking date</th>
						<th>Party date</th>
						<th>Customer name</th>
						<th>Party Location</th>
						<th>Booking Amount</th>
						<th>Refernce Images</th>
					</tr>
					</thead>
					<tbody>
					@foreach($data['response']['data']['new_orders'] as $booking)
						@php $isBookingImages = $isDeliveryImages = $bookingReview = $areaCount = 0 @endphp
						<tr>
							<td>{{ date("d/m/y",$booking['paid_at']) }}</td>
							<td>{{ date("d/m/y",$booking['party_date_time']) }}</td>
							<td>@if(isset($booking['customer_name'])){{ $booking['customer_name'] }}@endif</td>
							<td>
								@foreach($data['response']['data']['areas'] as $area)
									@if(isset($booking['ticket_area_id']) && ($area['id'] == $booking['ticket_area_id']))
										@php $areaCount = 1 @endphp
										{{ $area['name'] }}
									@endif
								@endforeach
								@if($areaCount == 0)
									--
								@endif
							</td>
							<td>
								<i class="fa fa-inr"></i> {{ substr_replace(money_format('%!i',$booking['booking_amount']) ,"",-3) }}
							</td>
							<td>
								@foreach($data['response']['data']['bookingGallery'] as $bookingGallery)
									@if($bookingGallery['ticket_booking_id'] == $booking['id'])
										@php $isBookingImages++ @endphp
										<a target="_blank" href="{{ config("evibe.gallery.host")."/ticket/".$booking['ticket_id']."/".$booking['id']."/".$bookingGallery['url'] }}" style="text-decoration: none">
											<img src="{{ config("evibe.gallery.host")."/ticket/".$booking['ticket_id']."/".$booking['id']."/".$bookingGallery['url'] }}" style="width:40px">
										</a>
									@endif
								@endforeach
								@if($isBookingImages == 0)
									---
								@endif
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endif