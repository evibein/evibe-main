@php $pendingDeliveryCount = isset($data['res']) && isset($data['res']['list']) ? count($data['res']['list']) : 0; $currentDeliveryCount = 0; @endphp
@if($pendingDeliveryCount > 0)
	<div class="row">
		<div class="x_panel">
			<div class="x_title">
				<div class="col-sm-9 no-pad-l">
					<h2>
						Pending Deliveries
						<small>Deliveries that are pending from past 10days.</small>
					</h2>
					<div class="clearfix"></div>
				</div>
				<div class="col-sm-3">
					@if($pendingDeliveryCount >= 5)
						<div class="pull-right">
							<a href="{{ route("partner.dash.deliveries") }}" class="btn btn-primary btn-xs">See all Pending deliveries</a>
						</div>
					@endif
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="x_content no-mar-b">
				@if($pendingDeliveryCount > 0)
					@foreach($data['res']['list'] as $delivery)
						@php $currentDeliveryCount++ @endphp
						<div class="col-sm-10 col-xs-10">
							<div class="col-md-3 col-sm-3 col-xs-3 no-pad-l"><i class="fa fa-calendar"></i>
								<span class="">{{ $delivery['formattedPartyDate']." / ".$delivery['formattedPartyTime'] }}</span>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-4"><i class="fa fa-user"></i>
								{{ $delivery['customerName'] }}
							</div>
							<div class="col-md-3 col-sm-3 col-xs-3"><i class="fa fa-building"></i>
								@if($delivery['partyLocation']){{ $delivery['partyLocation'] }}@else{{ "---" }}@endif
							</div>
							<div class="col-md-2 col-sm-2 col-xs-2"><i class="fa fa-birthday-cake"></i>
								{{ $delivery['occasion'] }}
							</div>
							<div class="clearfix"></div>
							<div class="mar-t-5">
								<span><b>Delivered Images : </b></span>
								@foreach($delivery['attachments']['url'] as $image)
									<a target="_blank" href="{{ $image['original'] }}" style="text-decoration: none">
										<img src="{{ $image['thumbs'] }}" style="width:40px">
									</a>
								@endforeach
								@if($delivery['attachments']['count'] == 0)
									<span class="text-bold">N/A</span>
								@endif
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-2 text-center mar-t-5">
							@if($delivery['attachments']['count'] > 0)
								<a href="{{ route('partner.dash.delivery.done', $delivery['id']) }}" class="btn btn-primary" type="reset"><i class="fa fa-check-circle"></i> I AM DONE</a>
							@endif
							<a href="{{ route("partner.dash.delivery.details", $delivery['id']) }}" class="btn btn-success">Upload Images</a>
						</div>
						<div class="clearfix"></div>
						@if($currentDeliveryCount < $pendingDeliveryCount)
							<hr class="mar-t-10 mar-b-10">
						@endif
					@endforeach
				@endif
			</div>
		</div>
	</div>
@endif