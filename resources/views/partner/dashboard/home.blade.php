@extends("partner.base")

@section('innerContent')
	<div class="right_col" role="main">
		<div class="row mar-b-20">
			<div class="in-blk mar-t-5 text-bold">
				<span class="font-14">Week summary from
					<span class="dashboard-start-date">{{ $data['humanReadableStartDay'] }}</span> to
					<span class="dashboard-end-date">{{ $data['humanReadableEndDay'] }}</span>
				</span>
			</div>
		</div>
		<div class="row tile_count no-mar-t no-mar-b">
			<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count no-mar-b">
				<span class="count_top"><i class="fa fa-user"></i> New Orders</span>
				<div class="count">{{ $data['response']['data']['total_bookings_count'] }}</div>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count no-mar-b">
				<span class="count_top"><i class="	fa fa-money"></i> New Revenue</span>
				<div class="count">
					<?php setlocale(LC_MONETARY, 'en_IN'); ?>
					<i class="fa fa-inr" aria-hidden="true"></i> {{ substr_replace(money_format('%!i',$data['response']['data']['total_revenue']) ,"",-3) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count no-mar-b">
				<span class="count_top"><i class="fa fa-user"></i> Deliveries Done</span>
				<div class="count">{{ $data['response']['data']['total_delivered_count'] }}</div>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count no-mar-b">
				<span class="count_top"><i class="fa fa-user"></i> Completed Orders</span>
				<div class="count">{{ $data['response']['data']['completed_orders_count'] }}</div>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count no-mar-b">
				<span class="count_top"><i class="fa fa-user"></i> Top Rating</span>
				<div class="count">
					<?php $starCount = floor($data['response']['data']['top_review']);
					$partialStar = $data['response']['data']['top_review'] - $starCount;
					?>
					@for($count = 1; $count <= $starCount; $count++)
						<img src="{{ config("evibe.gallery.host").config("evibe.star_icon.full") }}" style="width:18px">
					@endfor
					@if($partialStar >= 0.15 && $partialStar < 0.4)
						<img src="{{ config("evibe.gallery.host").config("evibe.star_icon.quarter") }}" style="width:18px">
					@elseif($partialStar >= 0.4 && $partialStar < 0.6)
						<img src="{{ config("evibe.gallery.host").config("evibe.star_icon.half") }}" style="width:18px">
					@elseif($partialStar >= 0.6 && $partialStar < 0.85)
						<img src="{{ config("evibe.gallery.host").config("evibe.star_icon.34th") }}" style="width:18px">
					@elseif($partialStar >= 0.85)
						<img src="{{ config("evibe.gallery.host").config("evibe.star_icon.full") }}" style="width:18px">
					@endif
				</div>
				@if(!$data['response']['data']['top_review'])
					<div class="count">
						--
					</div>
				@endif
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count no-mar-b">
				<span class="count_top"><i class="fa fa-user"></i> Least Rating</span>
				<div class="count">
					<?php $starCount = floor($data['response']['data']['least_review']);
					$partialStar = $data['response']['data']['least_review'] - $starCount;?>
					@for($count = 1; $count <= $starCount; $count++)
						<img src="{{ config("evibe.gallery.host").config("evibe.star_icon.full") }}" style="width:18px">
					@endfor
					@if($partialStar >= 0.15 && $partialStar < 0.4)
						<img src="{{ config("evibe.gallery.host").config("evibe.star_icon.quarter") }}" style="width:18px">
					@elseif($partialStar >= 0.4 && $partialStar < 0.6)
						<img src="{{ config("evibe.gallery.host").config("evibe.star_icon.half") }}" style="width:18px">
					@elseif($partialStar >= 0.6 && $partialStar < 0.85)
						<img src="{{ config("evibe.gallery.host").config("evibe.star_icon.34th") }}" style="width:18px">
					@elseif($partialStar >= 0.85)
						<img src="{{ config("evibe.gallery.host").config("evibe.star_icon.full") }}" style="width:18px">
					@endif
				</div>
				@if(!$data['response']['data']['least_review'])
					<div class="count">
						--
					</div>
				@endif
			</div>
		</div>
		<div class="dashboard-home-container"></div>
		<div class="row completed-orders-wrap">
			<div class="x_panel">
				<div class="x_title">
					<div class="col-sm-6 no-pad-l">
						<h2>
							Completed Orders
							<small>Summary of all the orders executed by you in the past week.</small>
						</h2>
						<div class="clearfix"></div>
					</div>
					<div class="col-sm-6">
						<input id="bookingRange" name="bookingRange" class="pull-right" type="text" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc" value="{{ $data['dateRange'] }}">
						<div class="in-blk pull-right mar-t-5 mar-r-4">
							<span class="">Change Date : </span>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="x_content no-mar-b">
					@if($data['response']['data']['completed_orders_count'] && $data['response']['data']['completed_orders_count'] > 0)
						<table id="datatable-responsive" class="table table-bordered dt-responsive nowrap no-mar-b" cellspacing="0" width="100%">
							<thead>
							<tr>
								<th>Party date</th>
								<th>Customer name</th>
								<th>Location</th>
								<th>Price</th>
								<th>Customer Rating</th>
								<th>Delivery Images</th>
							</tr>
							</thead>
							<tbody>
							@foreach($data['response']['data']['completed_order'] as $booking)
								@php $isBookingImages = $isDeliveryImages = $bookingReview = $areaCount = 0 @endphp
								<tr>
									<td>{{ date("d/m/y",$booking['party_date_time']) }}</td>
									<td>{{ $booking['customer_name'] }}</td>
									<td>
										@foreach($data['response']['data']['areas'] as $area)
											@if($area['id'] == $booking['ticket_area_id'])@php $areaCount = 1 @endphp{{ $area['name'] }}@endif
										@endforeach
										@if($areaCount == 0)
											--
										@endif
									</td>
									<td>
										<i class="fa fa-inr"></i> {{ substr_replace(money_format('%!i',$booking['booking_amount']) ,"",-3) }}
									</td>
									<td>
										@foreach($data['response']['data']['reviews'] as $review)
											@if($review['ticket_booking_id'] == $booking['id'])
												@php $starCount = floor($review['rating']); $bookingReview = 1;
												$partialStar = $data['response']['data']['least_review'] - $starCount; @endphp
												@for($count = 1; $count <= $starCount; $count++)
													<img src="{{ config("evibe.gallery.host").config("evibe.star_icon.full") }}" style="width:18px">
												@endfor
												@if($partialStar >= 0.15 && $partialStar < 0.4)
													<img src="{{ config("evibe.gallery.host").config("evibe.star_icon.quarter") }}" style="width:18px">
												@elseif($partialStar >= 0.4 && $partialStar < 0.6)
													<img src="{{ config("evibe.gallery.host").config("evibe.star_icon.half") }}" style="width:18px">
												@elseif($partialStar >= 0.6 && $partialStar < 0.85)
													<img src="{{ config("evibe.gallery.host").config("evibe.star_icon.34th") }}" style="width:18px">
												@elseif($partialStar >= 0.85)
													<img src="{{ config("evibe.gallery.host").config("evibe.star_icon.full") }}" style="width:18px">
												@endif
												@break
											@endif
										@endforeach
										@if($bookingReview == 0)
											--
										@endif
									</td>
									<td>
										@foreach($data['response']['data']['deliveryGallery'] as $deliveryGallery)
											@if($deliveryGallery['ticket_booking_id'] == $booking['id'])
												@php $isDeliveryImages++ @endphp
												<a target="_blank" href="{{ config("evibe.gallery.host")."/ticket/".$booking['ticket_id']."/".$booking['id']."/partner/".$deliveryGallery['url'] }}" style="text-decoration: none">
													<img src="{{ config("evibe.gallery.host")."/ticket/".$booking['ticket_id']."/".$booking['id']."/partner/".$deliveryGallery['url'] }}" style="width:40px">
												</a>
											@endif
										@endforeach
										@if($isDeliveryImages == 0)
											---
										@endif
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					@else
						--
					@endif
				</div>
			</div>
		</div>
		<input type="hidden" id="isDashboardHome" value="1">
	</div>

	<div class="modal fade modal-submit-partner-response" tabindex="-1" role="dialog" id="modalSubmitPartnerResponse">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<form id="fromSubmitPartnerResponse" data-url="">
						<div class="form-group">
							<div class="col-md-12 col-lg-12 col-sm-12">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
									<textarea class="mdl-textfield__input" type="text" name="partnerStatus" id="partnerStatus"></textarea>
									<label class="mdl-textfield__label" for="partnerStatus">Enter Your response?</label>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="form-group">
							<div class="col-sm-12 text-center">
								<button class="btn btn-link mar-r-5 close-reset-form" data-dismiss="modal">
									<i class="glyphicon glyphicon-remove"></i> <em>Cancel</em>
								</button>
								<button id="formPartnerReviewBtnSubmit" type="submit" class="btn btn-primary btn-submit mar-l-5">
								<span class="default">
									<span class="glyphicon glyphicon-ok"></span>
									<span class="valign-mid">SUBMIT</span>
								</span>
									<span class="waiting hide">
									<span class="glyphicon glyphicon-ok"></span>
									<span class="valign-mid">SUBMITTING..</span>
								</span>
								</button>
							</div>
							<div class="clearfix"></div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade modal-delivery-more-details" tabindex="-1" role="dialog" id="modalDeliveryMoreDetails">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">More Info</h4>
				</div>
				<div class="modal-body">
					<div id="deliveryMoreDetails" data-url="">
						<div class="form-group">
							<div class="col-md-12 col-lg-12 col-sm-12 mar-b-10">
								<div class="in-blk"><b>Booking Type : </b></div>
								<div class="in-blk booking-type">Testing</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-md-12 col-lg-12 col-sm-12 mar-b-10">
								<div class="in-blk"><b>Your response : </b></div>
								<div class="in-blk partner-response">Testing</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-md-12 col-lg-12 col-sm-12 mar-b-10">
								<div class="in-blk"><b>Customer Feedback : </b></div>
								<div class="in-blk customer-feedback">Testing</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input id="postUrl" type="hidden" value="{{ route("partner.dash.dashboard.filter") }}">
@endsection