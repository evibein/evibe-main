@extends("partner.base")

@section("innerContent")
	<div class="right_col" role="main">
		<div class="">
			<div class="page-title">
				<div class="title_left">
					<h3> Add Package</h3>
				</div>
			</div>
			<div class="clearfix"></div>

			<div class="row">
				<div class="col-md-12">
					<div class="x_panel">
						<div class="x_content">
							<div class="row">
								<div id="wizard" class="form_wizard wizard_horizontal mar-t-10">
									<ul class="wizard_steps anchor">
										<li>
											<a class="selected" isdone="1" rel="1">
												<span class="step_no">1</span>
												<span class="step_descr">
													<b>Step 1</b><br>
													<small><b>Select type of package</b></small>
                                          </span>
											</a>
										</li>
										<li>
											<a class="disabled" isdone="0" rel="2">
												<span class="step_no">2</span>
												<span class="step_descr">
                                              Step 2<br>
                                              <small>Fill details about your package</small>
                                          </span>
											</a>
										</li>
										<li>
											<a class="disabled" isdone="0" rel="3">
												<span class="step_no">3</span>
												<span class="step_descr">
                                              Step 3<br>
                                              <small>Upload Images</small>
                                          </span>
											</a>
										</li>
										<li>
											<a class="disabled" isdone="0" rel="4">
												<span class="step_no">4</span>
												<span class="step_descr">
                                              Step 4<br>
                                              <small>Review & Submit</small>
                                          </span>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="x_panel">
						<div class="x_content">
							<div>
								@if(session()->has('errorMsg'))
									<div class="alert alert-danger mar-l-10 mar-r-10 mar-b-10 pad-10">
										<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
										<i class="glyphicon glyphicon-remove-sign"></i>
										{{ session('errorMsg') }}
									</div>
								@endif
								<form id="addPackageFirstForm" method="post" action="{{ route("partner.dash.add.package.next") }}">
									<div class="form-group pad-t-10">
										<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 col-md-offset-1 col-xs-offset-1 col-lg-offset-1 col-sm-offset-1">
											<label for="partnerPackageType" class="control-label">Package Type</label>
											<select id="partnerPackageType" name="partnerPackageType" class="form-control">
												<option value="0">-- Select type --</option>
												@foreach($data['typePackages'] as $packageType)
													<option value="{{ $packageType->id }}">{{ $packageType->type_package }}</option>
												@endforeach
											</select>
										</div>
										<div class="col-lg-2 col-md-2 col-sm-2 text-center mar-t-20">
											<div class="in-blk mar-t-3">
												<button type="submit" class="btn btn-success add-package-first-submit">Next <i class="glyphicon-arrow-right glyphicon"></i>
												</button>
											</div>
											<div class="in-blk">
												<button type="button" class="btn btn-success add-package-first-submit-waiting hide" disabled="disabled">Please Wait..!!
												</button>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								</form>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection