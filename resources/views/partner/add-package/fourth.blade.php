@extends("partner.base")

@section("innerContent")
	<div class="right_col" role="main">
		<div class="">
			<div class="page-title">
				@if(is_null($data['newPackage']->submitted_at))
					<div class="title_left">
						<h3> Add Package</h3>
					</div>
				@else
					<div class="col-lg-8 col-md-8 col-sm-8 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 font-14">
						<h3>Review Package</h3>
					</div>
				@endif
			</div>
			<div class="clearfix"></div>
			@if(!is_null($data['newPackage']->submitted_at) && $data['newPackage']->is_edit == 1)
				<div class="col-lg-8 col-md-8 col-sm-8 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 font-14">
					<div class="alert alert-info">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<i class="fa fa-info-circle"></i> Please click on "SUBMIT PACKAGE" to submit the updated details
					</div>
				</div>
			@endif
			@if(is_null($data['newPackage']->submitted_at))
				<div class="row">
					<div class="x_panel">
						<div class="x_content">
							<div class="row">
								<div id="wizard" class="form_wizard wizard_horizontal">
									<ul class="wizard_steps anchor no-mar-b">
										<li>
											<a class="selected" isdone="1" rel="1">
												<span class="step_no">1</span>
												<span class="step_descr">
                                              Step 1<br>
                                              <small>Select type of package</small>
                                          </span>
											</a>
										</li>
										<li>
											<a class="selected" isdone="1" rel="2">
												<span class="step_no">2</span>
												<span class="step_descr">
                                              Step 2<br>
                                              <small>Fill details about your package</small>
                                          </span>
											</a>
										</li>
										<li>
											<a class="selected" isdone="1" rel="3">
												<span class="step_no">3</span>
												<span class="step_descr">
                                              Step 3<br>
                                              <small>Upload Images</small>
                                          </span>
											</a>
										</li>
										<li>
											<a class="selected" isdone="1" rel="4">
												<span class="step_no">4</span>
												<span class="step_descr">
												<b>Step 4</b><br>
												<small><b>Review & Submit</b></small>
                                          </span>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			@endif
			@if($data['discussions']->count() > 0)
				<div class="row">
					<div class="col-lg-8 col-md-8 col-sm-8 col-lg-offset-2 col-md-offset-2 col-sm-offset-2 font-14">
						<div class="x_panel messages_content">
							<div class="x_title">
								<h2>Messages</h2>
								@if($data['newPackage']->is_edit == 1)
									<a class="btn btn-success pull-right review-package-submit">SUBMIT PACKAGE</a>
								@endif
								<div class="clearfix"></div>
							</div>
							@php $firstMessages = $data['discussions']->where('is_complete', '==', "")->first();
							$messages = $firstMessages ? explode(',', $firstMessages->upgrade_option_ids) : "" @endphp
							<div class="x_content well">
								<div class="pull-right font-12 pad-t-3"><i class="fa fa-clock-o"></i> {{ date('d M y, g:i A',strtotime($firstMessages->created_at)) }}
								</div>
								<ul>
									@foreach($messages as $message)
										@if(config("evibe.partner.discussion.".$message))
											<li>{{ config("evibe.partner.discussion.".$message) }}</li>
										@endif
									@endforeach
								</ul>
								@if($firstMessages)
									<div class="mar-l-10"><b>Comments :</b> {{ $firstMessages->user_comment }}</div>
								@endif
							</div>
							@if($data['discussions']->where('is_complete', 1)->count() > 0)
								<a class="btn btn-primary partner-see-old-msgs-btn" role="button" data-toggle="collapse" href="#collapseMessages" aria-expanded="false" aria-controls="collapseMessages">
									See old messages
								</a>
							@endif
							<div class="collapse" id="collapseMessages">
								@foreach($data['discussions']->where('is_complete', 1) as $discussion)
									@php $messages = $discussion ? explode(',', $discussion->upgrade_option_ids) : "" @endphp
									<div class="x_content well">
										<div class="pull-right font-12 pad-t-3"><i class="fa fa-clock-o"></i> {{ date('d M y, g:i A',strtotime($discussion->created_at)) }}
										</div>
										<ul>
											@foreach($messages as $message)
												@if(config("evibe.partner.discussion.".$message))
													<li>{{ config("evibe.partner.discussion.".$message) }}</li>
												@endif
											@endforeach
										</ul>
										@if($discussion->user_comment)
											<div class="mar-l-10"><b>Comments :</b> {{ $discussion->user_comment }}
											</div>
										@endif
									</div>
								@endforeach
							</div>
							<div class="collapse" id="collapseMessages">
								<div class="well">
									In built bootstrap css
								</div>
							</div>
						</div>
					</div>
				</div>
			@endif
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-8 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">
					<div class="x_panel">
						<div class="x_title">
							<h2>Package Details</h2>
							@if($data['newPackage']->is_edit == 1)
								<a href="{{ route("partner.dash.add.package.edit", $data['newPackage']->id) }}" class="btn btn-warning pull-right"><i class="fa fa-pencil"> </i> EDIT DETAILS</a>
							@endif
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<table class="table table-striped table-hover text-left">
								<tr class="">
									<td class="table-top-row-remove-upper-border"><b>Package Name</b></td>
									<td class="table-top-row-remove-upper-border">{{ $data['newPackage']->package_name }}</td>
								</tr>
								<tr class="">
									<td class="text-bold">Actual Price</td>
									<td><i class="fa fa-inr" aria-hidden="true"></i> {{ $data['newPackage']->worth }}</td>
								</tr>
								<tr class="">
									<td class="text-bold">Discounted Price</td>
									<td><i class="fa fa-inr" aria-hidden="true"></i> {{ $data['newPackage']->price }}</td>
								</tr>
								<tr class="">
									<td class="text-bold">Inclusions</td>
									<td>{{ $data['newPackage']->inclusions }}</td>
								</tr>
								<tr class="">
									@php $eventName = $data['event']->where('id', $data['newPackage']->event_id)->first(); @endphp
									<td class="text-bold">Event Supported</td>
									<td>{{ $eventName ? $eventName->name : "--" }}</td>
								</tr>
								@foreach($data['addValues'] as $key => $value)
									<tr class="">
										<td class="text-bold">{{ $key }}</td>
										<td>{{ $value }}</td>
									</tr>
								@endforeach
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-8 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">
					<div class="x_panel">
						<div class="x_title">
							<h2>Gallery</h2>
							@if($data['newPackage']->is_edit == 1)
								<a href="{{ route('partner.dash.add.package.second', ['newPackage' => $data['newPackage']->id]) }}" class="btn btn-warning pull-right"><i class="fa fa-plus"> </i> ADD MORE IMAGES</a>
								<div class="clearfix"></div>
							@endif
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							@foreach($data['images'] as $image)
								@php $imageUrl = config("evibe.gallery.host")."/package/new/".$data['newPackage']->id."/". $image->url; @endphp
								<div class="in-blk text-center mar-r-10">
									<a href="{{ $imageUrl }}" target="_blank" class="blk">
										<img src="{{ $imageUrl }}" width="100" height="100">
									</a>
									@if($data['newPackage']->is_edit == 1)
										<a href="{{ route("partner.dash.delete.image", [$data['newPackage']->id, $image->id]) }}" onclick="return confirm('Are you sure to delete this image?')" class="text-danger"><i class="fa fa-times"></i> Delete</a>
									@endif
								</div>
							@endforeach
							@if($data['images']->count() == 0)
								<div class="text-center">Please Click on Add more images button to upload images.</div>
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-8 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">
					<div class="x_panel">
						<div class="x_content text-center">
							@if($data['newPackage']->rejected_at)
								<div class="text-center text-danger no-mar-b">Rejeceted at {{ $data['newPackage']->rejected_at }}</div>
							@elseif($data['newPackage']->submitted_at && $data['newPackage']->is_edit != 1)
								<div class="text-center text-success no-mar-b">Submitted at {{ $data['newPackage']->submitted_at }}</div>
							@elseif($data['images']->count() == 0)
								<div class="text-center text-danger no-mar-b">Please add images to submit the details.</div>
							@elseif($data['discussions']->count() == 0)
								<a href="{{ route("partner.dash.add.package.next.third", $data['newPackage']->id) }}" class="btn btn-success no-mar-b no-mar-r" type="reset"><i class="fa fa-upload"></i> SUBMIT DETAILS</a>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade modal-submit-update-review" tabindex="-1" role="dialog" id="modalSubmitUpdateReview">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div id="deliveryMoreDetails" data-url="">
						<div class="form-group">
							<form id="" method="post" action="{{ route("partner.dash.add.package.next.third.upgrade", $data['newPackage']->id) }}">
								<div class="form-group">
									<div class="col-lg-2 col-md-2 col-sm-2 no-pad text-right mar-t-20">
										<label for="partnerUpgradeResponse" class="control-label partner-signup-lbl">Comments: (optional)</label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-9 no-pad__400-600 no-pad__400 mar-t-20">
													<textarea name="partnerUpgradeResponse" id="partnerUpgradeResponse" placeholder="Please enter any comments or suggestions regarding the upgrades." class="form-control" rows="4" spellcheck="true"></textarea>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="clearfix"></div>
								<div class="form-group pad-t-15">
									<div class="col-sm-12 text-center">
										<button type="submit" class="btn btn-success">
											<i class="fa fa-upload"></i> SUBMIT PACKAGE
										</button>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="clearfix"></div>
							</form>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection