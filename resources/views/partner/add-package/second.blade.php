@extends("partner.base")

@section("innerContent")
	<div class="right_col" role="main">
		<div class="">
			<div class="page-title">
				<div class="title_left">
					@if(isset($data['package']) && !is_null($data['package']->submitted_at))
						<h3>Update & Submit</h3>
					@else
						<h3> Add Package</h3>
					@endif
				</div>
			</div>
			<div class="clearfix"></div>

			<div class="row">
				<div class="col-md-12">
					<div class="x_panel">
						<div class="x_content">
							<div class="row">
								<div id="wizard" class="form_wizard wizard_horizontal">
									<ul class="wizard_steps anchor no-mar-b">
										<li>
											<a class="selected" isdone="1" rel="1">
												<span class="step_no">1</span>
												<span class="step_descr">
                                              Step 1<br>
                                              <small>Select type of package</small>
                                          </span>
											</a>
										</li>
										<li>
											<a class="selected" isdone="1" rel="2">
												<span class="step_no">2</span>
												<span class="step_descr">
													<b>Step 2</b><br>
													<small><b>Fill details about your package</b></small>
                                          </span>
											</a>
										</li>
										<li>
											<a class="disabled" isdone="0" rel="3">
												<span class="step_no">3</span>
												<span class="step_descr">
                                              Step 3<br>
                                              <small>Upload Images</small>
                                          </span>
											</a>
										</li>
										<li>
											<a class="disabled" isdone="0" rel="4">
												<span class="step_no">4</span>
												<span class="step_descr">
                                              Step 4<br>
                                              <small>Review & Submit</small>
                                          </span>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="x_panel">
						<div class="x_title">
							<h2>Please fill the details of your package</h2>
							<div class="clearfix"></div>
						</div>
						@if(session()->has('errorMsg'))
							<div class="alert alert-danger mar-l-10 mar-r-10 mar-b-10 pad-10">
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								<i class="glyphicon glyphicon-remove-sign"></i>
								{{ session('errorMsg') }}
							</div>
						@endif
						<div class="x_content">
							<div class="row">
								<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
									<form id="addPackageSecondForm" method="post" action="{{ route("partner.dash.add.package.next.first") }}">
										<section class="@if(!$data['packageFields']) partner-add-package-sec-elements-end @else partner-add-package-sec-elements @endif">
											<div class="form-group">
												<div class="col-lg-4 col-md-4 col-sm-4 no-pad text-right mar-t-20">
													<label for="vendorPackageName" class="control-label partner-signup-lbl">Package name:</label>
													<label class="required-star">*</label>
												</div>
												<div class="col-lg-7 col-md-7 col-sm-7 no-pad__400-600 no-pad__400 mar-t-20">
													<input type="text" name="vendorPackageName" id="vendorPackageName" placeholder="Enter the package name" class="form-control" value="@if(old("vendorPackageName")){{ old("vendorPackageName") }}@elseif(isset($data['package'])){{ $data['package']->package_name }}@endif">
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="form-group">
												<div class="col-lg-4 col-md-4 col-sm-4 no-pad text-right mar-t-20">
													<label for="vendorPackageActualPrice" class="control-label partner-signup-lbl">Actual Price:</label>
													<label class="required-star">*</label>
												</div>
												<div class="col-lg-7 col-md-7 col-sm-7 no-pad__400-600 no-pad__400 mar-t-20">
													<input type="text" name="vendorPackageActualPrice" id="vendorPackageActualPrice" placeholder="Enter the actual price" class="form-control" value="@if(old("vendorPackageActualPrice")){{ old("vendorPackageActualPrice") }}@elseif(isset($data['package'])){{ $data['package']->worth }}@endif">
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="form-group">
												<div class="col-lg-4 col-md-4 col-sm-4 no-pad text-right mar-t-20">
													<label for="vendorPackagePrice" class="control-label partner-signup-lbl">Discounted Price:</label>
													<label class="required-star">*</label>
												</div>
												<div class="col-lg-7 col-md-7 col-sm-7 no-pad__400-600 no-pad__400 mar-t-20">
													<input type="text" name="vendorPackagePrice" id="vendorPackagePrice" placeholder="Enter the discounted price" class="form-control" value="@if(old("vendorPackagePrice")){{ old("vendorPackagePrice") }}@elseif(isset($data['package'])){{ $data['package']->price }}@endif">
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="form-group">
												<div class="col-lg-4 col-md-4 col-sm-4 no-pad text-right mar-t-20">
													<label for="vendorPackageInclusions" class="control-label partner-signup-lbl">Inclusions:</label>
													<label class="required-star">*</label>
												</div>
												<div class="col-lg-7 col-md-7 col-sm-7 no-pad__400-600 no-pad__400 mar-t-20">
													<textarea name="vendorPackageInclusions" id="vendorPackageInclusions" placeholder="Please enter the inclusions of the package." class="form-control" rows="4" spellcheck="true">@if(old("vendorPackageInclusions")){{ old("vendorPackageInclusions") }}@elseif(isset($data['package'])){{ $data['package']->inclusions }}@endif</textarea>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="form-group">
												<div class="col-lg-4 col-md-4 col-sm-4 no-pad text-right mar-t-20">
													<label for="vendorPackageEventId" class="control-label partner-signup-lbl">Event Supported:</label>
													<label class="required-star">*</label>
												</div>
												<div class="col-lg-7 col-md-7 col-sm-7 no-pad__400-600 no-pad__400 mar-t-20">
													<select id="vendorPackageEventId" name="vendorPackageEventId" class="form-control">
														@foreach($data['typeEvents'] as $typeEvent)
															<option value="{{ $typeEvent->id }}" @if(old("vendorPackageEventId") && old("vendorPackageEventId") == $typeEvent->id) selected @elseif(isset($data['package']) && $data['package']->event_id == $typeEvent->id) selected @endif>{{ $typeEvent->name }}</option>
														@endforeach
													</select>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="clearfix"></div>
											@if(!$data['packageFields'])
												<div class="form-group pad-t-15">
													<div class="col-sm-12 text-center">
														<a class="btn btn-primary mar-r-5" href="{{ route("partner.dash.add.package") }}">
															<i class="glyphicon glyphicon-arrow-left"></i> <em>Back</em>
														</a>
														<button type="submit" class="btn btn-success add-package-second-submit">@if(isset($data['is_update']) && $data['is_update'] == 1) Update @else Next @endif
															<i class="glyphicon-arrow-right glyphicon"></i>
														</button>
														<button type="button" class="btn btn-success add-package-second-submit-waiting hide" disabled="disabled">Please Wait..!!
														</button>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="in-blk pull-right">
													<span class="required-star">* </span>
													<span style='color:grey' class="font-12 pad-r-6">fields are required</span>
												</div>
												<div class="clearfix"></div>
											@endif
										</section>
										@if($data['packageFields'] && $data['packageFields']->count() > 0)
											<section>
												@foreach($data['packageFields'] as $field)
													@if($field->type_field_id == config('evibe.input.text') || $field->type_field_id == config('evibe.input.number'))
														@include('partner.dynamicFields.text')
													@elseif($field->type_field_id == config('evibe.input.textarea'))
														@include('partner.dynamicFields.textarea')
													@elseif($field->type_field_id == config('evibe.input.option'))
														@include('partner.dynamicFields.option')
													@elseif($field->type_field_id == config('evibe.input.radio'))
														@include('partner.dynamicFields.radio')
													@elseif($field->type_field_id == config('evibe.input.checkbox'))
														@include('partner.dynamicFields.checkbox')
													@endif
												@endforeach
												<div class="clearfix"></div>
												<div class="form-group pad-t-15">
													<div class="col-sm-12 text-center">
														<a class="btn btn-primary mar-r-5" href="{{ route("partner.dash.add.package") }}">
															<i class="glyphicon glyphicon-arrow-left"></i> Back
														</a>
														<button type="submit" class="btn btn-success add-package-second-submit">@if(isset($data['is_update']) && $data['is_update'] == 1) Update @else Next
															<i class="glyphicon-arrow-right glyphicon"></i> @endif
														</button>
														<button type="button" class="btn btn-success add-package-second-submit-waiting hide" disabled="disabled">Please Wait..!!
														</button>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="in-blk pull-right">
													<span class="required-star">* </span>
													<span style='color:grey' class="font-12 pad-r-6">fields are required</span>
												</div>
												<div class="clearfix"></div>
											</section>
										@endif
										<input type="hidden" name="isUpdate" value="@if(isset($data['is_update'])){{"1"}}@else{{"0"}}@endif">
										<input type="hidden" name="newPackageId" value="@if(isset($data["package"])){{ $data["package"]->id }}@endif">
										<input type="hidden" id="loadPreventReloadModule" value="1">
										<input type="hidden" name="typePackageId" value="{{ $data['typePackageId'] }}">
									</form>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection