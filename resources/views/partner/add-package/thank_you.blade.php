@extends("partner.base")

@section("innerContent")
	<div class="right_col" role="main">
		<div class="">
			<div class="page-title">
				<div class="title_left">
					<h3> Add Package</h3>
				</div>
			</div>
			<div class="clearfix"></div>

			<div class="row">
				<div class="x_panel">
					<div class="x_content">
						<div class="row">
							<div class="submit-success">
								<div class="alert alert-success no-mar-b text-center">
									Thanks for adding your package :-). We will get in touch with you shortly.
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection