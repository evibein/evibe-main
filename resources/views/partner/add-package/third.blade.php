@extends("partner.base")

@section("innerContent")
	<div class="right_col" role="main">
		<div class="">
			<div class="page-title">
				<div class="title_left">
					@if(is_null($data['newPackage']->submitted_at))
						<h3> Add Package</h3>
					@else
						<h3>Update & Submit</h3>
					@endif
				</div>
			</div>
			<div class="clearfix"></div>

			@if(is_null($data['newPackage']->submitted_at))
				<div class="row">
					<div class="x_panel">
						<div class="x_content">
							<div class="row">
								<div id="wizard" class="form_wizard wizard_horizontal">
									<ul class="wizard_steps anchor no-mar-b">
										<li>
											<a class="selected" isdone="1" rel="1">
												<span class="step_no">1</span>
												<span class="step_descr">
                                              Step 1<br>
                                              <small>Select type of package</small>
                                          </span>
											</a>
										</li>
										<li>
											<a class="selected" isdone="1" rel="2">
												<span class="step_no">2</span>
												<span class="step_descr">
                                              Step 2<br>
                                              <small>Fill details about your package</small>
                                          </span>
											</a>
										</li>
										<li>
											<a class="selected" isdone="1" rel="3">
												<span class="step_no">3</span>
												<span class="step_descr">
												<b>Step 3</b><br>
												<small><b>Upload Images</b></small>
                                          </span>
											</a>
										</li>
										<li>
											<a class="disabled" isdone="0" rel="4">
												<span class="step_no">4</span>
												<span class="step_descr">
                                              Step 4<br>
                                              <small>Review & Submit</small>
                                          </span>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			@endif

			<div class="row">
				<div class="x_panel">
					<div class="x_content">
						<div class="row">
							<section id="" class="partner-signup-sec">
								@if(session()->has('errorMsg'))
									<div class="alert alert-danger mar-l-10 mar-r-10 mar-b-10 pad-10">
										<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
										<i class="glyphicon glyphicon-remove-sign"></i>
										{{ session('errorMsg') }}
									</div>
								@endif
								<form action="{{ route("partner.dash.add.package.next.second") }}?newPackage={{ $data["packageId"] }}" method="POST" class="form-horizontal" enctype="multipart/form-data" accept-charset="UTF-8" id="addPackageThirdForm">
									<div id="drop-zone">
										<p class="mar-t-20">Drop files here</p>
										<div id="clickHere">or click here to choose files
											<input type="file" name="vendorPackageInputFile[]" id="vendorPackageInputFile" multiple/>
										</div>
										<div id='filename'></div>
									</div>
									<div class="form-group mar-t-20">
										<div class="col-sm-12 text-center">
											<a href="{{ route("partner.dash.add.package.edit", $data["packageId"]) }}" class="btn btn-primary"><i class="fa fa-backward"></i> Edit Details</a>
											<button type="submit" class="btn btn-success add-package-third-submit">Upload Images
											</button>
											<a href="{{ route("partner.dash.add.package.third", $data["packageId"]) }}" class="btn btn-primary" type="reset">Preview <i class="fa fa-forward"></i></a>
											<button type="submit" class="btn btn-success add-package-third-submit-waiting hide" disabled="disabled">Please Wait..!!
											</button>
										</div>
										<div class="clearfix"></div>
									</div>
								</form>
								<div class="in-blk pull-right">
									<span class="required-star">* </span>
									<span style='color:grey' class="font-12 pad-r-6">Package saved as Draft, you can upload and submit details at any time.</span>
								</div>
								<div class="clearfix"></div>
							</section>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection