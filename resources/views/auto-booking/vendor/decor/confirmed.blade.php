@extends('pay.base')

@section('page-title')
	<title>Thanks for the confirmation | Evibe.in</title>
@endsection

@section('content')
	<div class="pay-body">
		<div class="pay-success-page">
			<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 auto-booking">
				<div class="checkout-wrap no-pad">
					<div class="co-data font-16 ask-confirm-wrap">
						<h4 class="text-center section-header confirmed">
							<img src='{{$galleryUrl}}/main/img/icons/check.png' height="20px" width="20px" alt='Success'> Order Confirmed - #{{ $data['booking']['bookingId'] }}
						</h4>
						<div class="main-content">
							You have confirmed the decor order. Please check email sent to {{ $data['booking']['provider']['email'] }} for complete order and delivery details
							<table class="table table-bordered mar-t-30">
								<tr>
									<td>Decor</td>
									<td>{{ $data['booking']['name'] }}</td>
								</tr>
								<tr>
									<td>Customer Special Notes* (Needs approval if not already discussed)</td>
									<td>
										@if($data['additional']['specialNotes'])
											{!! $data['additional']['specialNotes'] !!}
										@else
											--
										@endif
									</td>
								</tr>
								@if(isset($data['checkoutFields']))
									@foreach($data['checkoutFields'] as $key => $value)
										<tr>
											<td>{{ $key }}</td>
											<td>{{ $value }}</td>
										</tr>
									@endforeach
								@endif
								<tr>
									<td>Delivery Date & Time</td>
									<td>{{ $data['booking']['partyDate'] }}, {{ $data['booking']['checkInTime'] }}</td>
								</tr>
								<tr>
									<td>Delivery Location</td>
									<td>{{ $data['additional']['venueLocation'] }}</td>
								</tr>
								<tr>
									<td>Advance Paid</td>
									<td>@price($data['booking']['advanceAmount'])</td>
								</tr>
								<tr>
									<td>Balance Amount</td>
									<td>@if($data['balanceAmt']) @price($data['balanceAmt']) @else @price(0) @endif</td>
								</tr>
							</table>

							<div class="mar-t-10">
								<u>Important:</u> Please take a picture of the decor before the start of party and upload it in Evibe.in
							</div>

							<div class="mar-t-20">
								<a href="https://play.google.com/store/apps/details?id=in.evibe.evibe">Download our partner app</a><br> This will help us maintain your offerings up to date on our website that will fetch you more order & to protect you incase any issues.
							</div>
							<div class="mar-t-20">
								Should you have any queries, please reply to the email (or) contact your Evibe.in account manager / 9640204000. We will respond at the earliest.
							</div>

							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
@endsection

@section('javascript')
@endsection