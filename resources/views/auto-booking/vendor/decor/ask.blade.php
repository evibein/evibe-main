@extends('pay.base')

@section('page-title')
	<title>Accept / Reject Booking | Evibe.in</title>
@stop

@section('modals')
	@parent
	<section id="modalConfirmBooking" class="modal modal-confirm-booking" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-body">
					<form action="{{ $data['acceptUrl'] }}" method="POST" class="text-center">
						<h4>Are you sure to confirm the order?</h4>
						<div class="g-captcha mar-t-20">
							<div id="captchaField1"></div>
						</div>
						<div class="mar-t-30 mar-b-20">
							<div class="col-sm-8 col-sm-offset-2">
								<button type="submit" id="confirmBooking" class="btn btn-lg btn-reply confirm">YES, CONFIRM ORDER
								</button>
							</div>
							<div class="clearfix"></div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<section id="modalCancelBooking" class="modal modal-cancel-booking" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-body">
					<form action="{{ $data['rejectUrl'] }}" method="POST" class="text-center">
						<h4>Are you sure to cancel the order?</h4>
						<div class="g-captcha mar-t-20">
							<div id="captchaField2"></div>
						</div>
						<div class="mar-t-30 mar-b-20">
							<div class="col-sm-8 col-sm-offset-2">
								<button type="submit" id="cancelBooking" class="btn btn-lg btn-reply cancel">
									YES, CANCEL ORDER
								</button>
							</div>
							<div class="clearfix"></div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
@stop

@section('content')
	@parent
	<div class="ask-wrap">
		<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 auto-booking">
			<div class="ask-confirm-wrap no-pad">
				<h4 class="text-center section-header">Is Decor Available For {{ $data['booking']['partyDate'] }}, {{ $data['booking']['checkInTime'] }} ?</h4>
				<div class="main-content">
					<div class="text-center">
						A customer is interested in {{ $data['booking']['name'] }} and has already paid
						amount of Rs. {{ EvibeUtil::formatPriceWithoutChar($data['booking']['advanceAmount']) }}.Please check complete order details below.
						<div class="mar-t-15">
							Click on
							<b>“Accept”</b> button to accept to confirm the availability (or) click
							<b>“Reject”</b> button if you are not available.

						</div>
					</div>
					<div class="mar-t-30 text-normal">
						If there is any change in decor details, please call {{ config('evibe.contact.business.phone') }} at the earliest. We will update the same on our website.
					</div>
					<div class="mar-t-30" style="color:#ED2E71"><b><u>Booking Details</u></b></div>
					<div class="mar-t-10  border-black-no-pad">
						<table class="table table-bordered font-14 no-mar-b">
							<tr>
								<td width="50%">
									<div class="pad-10 text-center">
										<b>Order Details </b>
										<hr>
									</div>
									<div><b>Customer Name: </b> <span>{{ $data['customer']['name'] }}</span></div>
									<div><b>Decor: </b> <span>{{ $data['booking']['bookingInfo'] }}</span></div>
									<div><b>Customer Special Notes* (Needs approval if not already discussed): </b>
										<span>
											@if($data['additional']['specialNotes'])
												{!! $data['additional']['specialNotes'] !!}}
											@else
												--
											@endif
										</span>
									</div>
									@if(isset($data['checkoutFields']))
										@foreach($data['checkoutFields'] as $key => $value)
											<div><b>{{ $key }}: </b> <span>{{ $value }}</span></div>
										@endforeach
									@endif
								</td>
								<td width="60%">
									<div class="pad-10 text-center">
										<b>Delivery Details </b>
										<hr>
									</div>
									<div><b>Date: </b> <span>{{ $data['booking']['partyDate'] }}</span></div>
									<div><b>Time: </b> <span>{{ $data['booking']['checkInTime'] }}</span></div>
									<div><b>Location: </b><span>{{ $data['additional']['venueLocation'] }}</span>
									</div>
									<div><b>Full Address: </b><span>{{ $data['additional']['venueAddress'] }}</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div><b>Decor Price: </b> <span>@price($data['decorPrice'])</span></div>
									<div><b>Delivery Charge: </b>
										<span>@if($data['deliveryAmt']) @price($data['deliveryAmt']) @else
												<span class="text-success">Free</span>@endif</span>
									</div>
									<div><b>Total Order Amount: </b>
										<span>@price($data['booking']['bookingAmount']) </span>
									</div>
								</td>
								<td>
									<div><b>Advance Paid: </b>@price($data['booking']['advanceAmount'])</div>
									<div>
										<b>Balance Amount: </b> @if($data['balanceAmt']) @price($data['balanceAmt'])@else 0 @endif
									</div>
								</td>
							</tr>
						</table>
					</div>
					<div class="mar-b-20 mar-t-30">
						<div class="col-md-10 col-md-offset-1">
							<div class="col-md-6">
								<a href="#" class="btn btn-reply confirm" data-toggle="modal" data-target="#modalConfirmBooking">Accept</a>
							</div>
							<div class="col-md-6">
								<a href="#" class="btn btn-reply cancel" data-toggle="modal" data-target="#modalCancelBooking">Reject</a>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@stop

@section('javascript')
	<script type="text/javascript">
		$('#confirmBooking').prop('disabled', true);
		$('#cancelBooking').prop('disabled', true);

		var CaptchaCallback = function () {
			grecaptcha.render('captchaField1', {
				'sitekey': '{{ config('evibe.google.re_cap_site') }}',
				'callback': function (response) {
					$('#confirmBooking').prop('disabled', false);
				}
			});
			grecaptcha.render('captchaField2', {
				'sitekey': '{{ config('evibe.google.re_cap_site') }}',
				'callback': function (response) {
					$('#cancelBooking').prop('disabled', false);
				}
			});
		};

		/*==== Open the accept / reject Modal if the reference url has #confirm ====*/
		var pageUrl = window.location.href;
		if (pageUrl.indexOf('#confirm') != -1) {
			$('#modalConfirmBooking').modal('show');
			console.log("true");
		}
		if (pageUrl.indexOf('#cancel') != -1) {
			$('#modalCancelBooking').modal('show');
		}
	</script>
	<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
@endsection