@extends('pay.base')

@section('page-title')
	<title>Booking Action | Evibe.in</title>
@endsection

@section('content')
	<div class="pay-body">
		<div class="pay-success-page">
			`			<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 auto-booking">
				<div class="checkout-wrap no-pad">
					<div class="co-data font-16 ask-confirm-wrap">
						<h4 class="text-center section-header cancelled">
							<img src='{{$galleryUrl}}/main/img/icons/check.png' height="20px" width="20px" alt='Success'> Order Cancelled
						</h4>
						<div class="main-content">
							<div>
								Based on your consent, we have cancelled the order. There is no action required from your side.
								<div class="mar-t-20">
									Should you have any queries, please reply to this email (or) call you Evibe.in account manager / +91 9640204000. We will respond to you at the earliest.
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
@endsection