@extends('pay.base')

@section('page-title')
	<title>Thanks for the confirmation | Evibe.in</title>
@endsection

@section("content")
	<div class="pay-body">
		<div class="pay-success-page">
			<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 auto-booking">
				<div class="checkout-wrap no-pad">
					<div class="co-data font-16 ask-confirm-wrap">
						<h4 class="text-center section-header confirmed">
							<img src='{{$galleryUrl}}/main/img/icons/check.png' height="20px" width="20px" alt='Success'> Booking Confirmed
						</h4>
						<div class="main-content text-center">
							Based on your consent, we have confirmed your booking order for {{ $data['additional']['checkInDate'] }} from
							{{ $data['customer']['name'] }} @if($data['booking']['isVenue'] == 1) at {{ $data['booking']['venueName'] }}@endif for the
							package {{ $data['booking']['bookingInfo'] }}.
							<div class="mar-t-30">
								<b>Please check your email for more order details.</b>
							</div>

						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
@endsection