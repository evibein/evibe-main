@extends('pay.base')

@section('page-title')
	<title>Accept / Reject Booking | Evibe.in</title>
@endsection

@section("modals")
	@parent
	<section id="modalConfirmBooking" class="modal modal-confirm-booking" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-body">
					<form action="{{ $data['acceptUrl'] }}" method="POST" class="text-center">
						<h4>Are you sure to accept the booking?</h4>
						<div class="g-captcha mar-t-20">
							<div id="captchaField1"></div>
						</div>
						<div class="mar-t-30 mar-b-20">
							<div class="col-sm-8 col-sm-offset-2">
								<button type="submit" id="confirmBooking" class="btn btn-lg btn-reply confirm">Yes, I Accept Booking
								</button>
							</div>
							<div class="clearfix"></div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<section id="modalCancelBooking" class="modal modal-cancel-booking" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-body">
					<form action="{{ $data['rejectUrl'] }}" method="POST" class="text-center">
						<h4>Are you sure to reject the booking?</h4>
						<div class="g-captcha mar-t-20">
							<div id="captchaField2"></div>
						</div>
						<div class="mar-t-30 mar-b-20">
							<div class="col-sm-8 col-sm-offset-2">
								<button type="submit" id="cancelBooking" class="btn btn-lg btn-reply cancel">
									Yes, I Reject Booking
								</button>
							</div>
							<div class="clearfix"></div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
@endsection

@section("content")
	@parent
	<div class="ask-wrap">
		<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 auto-booking">
			<div class="ask-confirm-wrap no-pad">
				<h4 class="text-center section-header">Confirm Booking?</h4>
				<div class="main-content">
					<div class="text-center">
						We have an order booked for your property.
						<div class="mar-t-10">
							Click on <b>Accept</b> button to confirm booking or click
							<b> Reject</b> button to cancel booking.
						</div>
					</div>
					<div class="mar-t-5 booking-details">
						<div class="booking-title">Booking Details</div>
						<div class="mar-t-5 mar-b-10">
							<span class="ap-sub-title">Package:</span> {{ $data['booking']['bookingInfo'] }}
						</div>
						<div class="mar-t-5 mar-b-10">
							<span class="ap-sub-title">Customer Name:</span> {{ $data['customer']['name'] }}
						</div>
						@if(isset($data['additional']['eventName']) && $data['additional']['eventName'])
							<div class="mar-t-5 mar-b-10">
								<span class="ap-sub-title">Occasion:</span> {{ $data['additional']['eventName'] }}
							</div>
						@endif
						<div class="col-sm-3 no-pad-l mar-b-10">
							<div class="ap-sub-title">Guests Count:</div>
							<div>@if($data['additional']['guestsCount']) {{ $data['additional']['guestsCount'] }}@else N/A @endif</div>
						</div>
						<div class="col-sm-3 no-pad-l mar-b-10">
							<div class="ap-sub-title">Customer Special Notes* (Needs approval if not already discussed):</div>
							<div>@if($data['additional']['specialNotes']) {!! $data['additional']['specialNotes']  !!}@else -- @endif</div>
						</div>
						@if($data['booking']['isVenue'] == 1)
							<div class="col-sm-3 no-pad-l mar-b-10">
								<div class="ap-sub-title">Check In Date</div>
								<div>{{ $data['additional']['checkInDate'] }}</div>
							</div>
							@if($data['booking']['checkInTime'])
								<div class="col-sm-3 no-pad-l mar-b-10">
									<div class="ap-sub-title">Check In Time</div>
									<div>{{ $data['booking']['checkInTime'] }}</div>
								</div>
							@endif
							@if($data['booking']['checkOutTime'])
								<div class="col-sm-3 no-pad-l mar-b-10">
									<div class="ap-sub-title">Check Out Time</div>
									<div>{{ $data['booking']['checkOutTime'] }}</div>
								</div>
							@endif
						@else
							<div class="col-sm-3 no-pad-l mar-b-10">
								<div class="ap-sub-title">Party Date</div>
								<div>{{ $data['additional']['checkInDate'] }}</div>
							</div>
							@if($data['booking']['checkInTime'])
								<div class="col-sm-3 no-pad-l mar-b-10">
									<div class="ap-sub-title">Party Time</div>
									<div>{{ $data['booking']['checkInTime'] }}</div>
								</div>
							@endif
						@endif
						<div class="clearfix"></div>
					</div>
					<hr class="sep">
					<div class="mar-b-20">
						<div class="col-md-10 col-md-offset-1">
							<div class="col-md-6">
								<a href="#" class="btn btn-reply confirm" data-toggle="modal" data-target="#modalConfirmBooking">Accept</a>
							</div>
							<div class="col-md-6">
								<a href="#" class="btn btn-reply cancel" data-toggle="modal" data-target="#modalCancelBooking">Reject</a>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection

@section("javascript")
	<script type="text/javascript">
		$('#confirmBooking').prop('disabled', true);
		$('#cancelBooking').prop('disabled', true);

		var CaptchaCallback = function () {
			grecaptcha.render('captchaField1', {
				'sitekey': '{{ config('evibe.google.re_cap_site') }}',
				'callback': function (response) {
					$('#confirmBooking').prop('disabled', false);
				}
			});
			grecaptcha.render('captchaField2', {
				'sitekey': '{{ config('evibe.google.re_cap_site') }}',
				'callback': function (response) {
					$('#cancelBooking').prop('disabled', false);
				}
			});
		};

		/*==== Open the accept / reject Modal if the reference url has #confirm ====*/
		var pageUrl = window.location.href;
		if (pageUrl.indexOf('#confirm') != -1) {
			$('#modalConfirmBooking').modal('show');
			console.log("true");
		}
		if (pageUrl.indexOf('#cancel') != -1) {
			$('#modalCancelBooking').modal('show');
		}
	</script>
	<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
@endsection