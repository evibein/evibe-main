@extends('pay.base')

@section('page-title')
	<title>Accept / Reject Booking | Evibe.in</title>
@endsection

@section("modals")
	@parent
	<section id="modalConfirmBooking" class="modal modal-confirm-booking" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-body">
					<form action="{{ $data['acceptUrl'] }}" method="POST" class="text-center">
						<h4>Are you sure to accept the order?</h4>
						<div class="g-captcha mar-t-20">
							<div id="captchaField1"></div>
						</div>
						<div class="mar-t-30 mar-b-20">
							<div class="col-sm-8 col-sm-offset-2">
								<button type="submit" id="confirmBooking" class="btn btn-lg btn-reply confirm">Yes, I Accept Order
								</button>
							</div>
							<div class="clearfix"></div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<section id="modalCancelBooking" class="modal modal-cancel-booking" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-body">
					<form action="{{ $data['rejectUrl'] }}" method="POST" class="text-center">
						<h4>Are you sure to reject the order?</h4>
						<div class="g-captcha mar-t-20">
							<div id="captchaField2"></div>
						</div>
						<div class="mar-t-30 mar-b-20">
							<div class="col-sm-8 col-sm-offset-2">
								<button type="submit" id="cancelBooking" class="btn btn-lg btn-reply cancel">
									Yes, I Reject Order
								</button>
							</div>
							<div class="clearfix"></div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
@endsection

@section("content")
	@parent
	<div class="ask-wrap">
		<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 auto-booking">
			<div class="ask-confirm-wrap no-pad">
				<h4 class="text-center section-header">Are the following booking(s) available for {{ $data['partyDateTime'] }} ?</h4>
				<div class="main-content">
					<div class="text-center">
						A customer is interested in the following order and has already paid amount of Rs. {{ $data['totalAdvancePaidStr'] }}. Please check complete order details below.
						<div class="mar-t-10">
							Click on <b>Accept</b> button to confirm order or click
							<b> Reject</b> button to cancel order.
						</div>
					</div>
					<div class="mar-t-30 font-14">
						<table class="table table-bordered border-black" width="100%">
							<tr>
								<td class="pad-10 valign-top" width="100%">
									<div class="text-left">
										<div class="text-uppercase text-e mar-b-5"><b>Order Details</b></div>
										<div><strong>Customer Name:</strong> {{ $data['customer']['name'] }}</div>
										<div><strong>Party Date Time:</strong> {{ $data['partyDateTime'] }}</div>
										@if(isset($data['additional']['eventName']) && $data['additional']['eventName'])
											<div><strong>Occasion:</strong> {{ $data['additional']['eventName'] }}</div>
										@endif
										<div>
											<strong>Customer Special Notes* (Needs approval if not already discussed):</strong>
											@if($data['additional']['specialNotes'])
												{!! $data['additional']['specialNotes'] !!}
											@else
												--
											@endif
										</div>
										@if(isset($data['bookings']) && $data['bookings'])
											<div class="mar-t-10">
												<table class="table table-bordered border-black mar-b-10" width="100%">
													@foreach($data['bookings'] as $booking)
														<tr>
															<td class="pad-10 valign-top" width="60%">
																<div class="mar-b-10">
																	<span class="product-type-text">{{ $booking['typeBookingDetails'] }}</span>
																</div>
																<div>
																	<strong>Booking Details: </strong>{!! $booking['bookingInfo'] !!}
																</div>
																@if(isset($booking['checkoutFields']))
																	@foreach($booking['checkoutFields'] as $key => $value)
																		<div><strong>{{ $key }}: </strong>{{ $value }}
																		</div>
																	@endforeach
																@endif
															</td>
															<td class="pad-10 valign-top" width="40%">
																@if(isset($data['typeTicketAddOn']) && isset($booking['itemMapTypeId']) && ($booking['itemMapTypeId'] == $data['typeTicketAddOn']))
																	<div>
																		<strong>Booking Units:</strong> {{ $booking['bookingUnits'] }}
																	</div>
																@else
																	<div>
																		<strong>Product Price:</strong> Rs. {{ $booking['productPriceStr'] }}
																	</div>
																	@if(isset($booking['itemMapTypeId']) && isset($data['decorTypeId']) && ($booking['itemMapTypeId'] == $data['decorTypeId']))
																		<div><strong>Delivery Charge:</strong>
																			@if($booking['transportCharges'] > 0)
																				Rs. {{ $booking['transportChargesStr'] }}
																			@else
																				<span class="text-g"> Free </span>
																			@endif
																		</div>
																	@endif
																@endif
																<div>
																	<strong>Booking Amount:</strong> Rs. {{ $booking['bookingAmountStr'] }}
																</div>
																@if(isset($booking['itemMapTypeId']) && isset($data['venueDealsTypeId']) && ($booking['itemMapTypeId'] == $data['venueDealsTypeId']))
																	<div>
																		<strong>Advance Paid:</strong> Rs. {{ $booking['tokenAmountStr'] }}
																	</div>
																@else
																	<div>
																		<strong>Advance Paid:</strong> Rs. {{ $booking['advanceAmountStr'] }}
																	</div>
																@endif
																<div>
																	<strong>Balance Amount:</strong> Rs. {{ $booking['balanceAmountStr'] }}
																</div>
															</td>
														</tr>
													@endforeach
												</table>
											</div>
											@if(count($data['bookings']) > 1)
												<div class="mar-t-10">
													<div>
														<strong>Total Booking Amount:</strong> Rs. {{ $data['totalBookingAmountStr'] }}
													</div>
													<div>
														<strong>Total Advance Paid:</strong> Rs. {{ $data['totalAdvancePaidStr'] }}
													</div>
													<div>
														<strong>Total Balance Amount:</strong> Rs. {{ $data['totalBalanceAmountStr'] }}
													</div>
												</div>
											@endif
										@endif
									</div>
								</td>
							</tr>
							@if(isset($data['venuePartnerTypeId']) && ($data['partnerTypeId'] == $data['venuePartnerTypeId']))
							@else
								<tr>
									<td class="pad-10 valign-top" width="100%">
										<div class="text-left">
											<div class="text-uppercase text-e mar-b-5"><b>Delivery Details</b>
											</div>
											<div><strong>Location:</strong> {{ $data['additional']['venueLocation'] }}
											</div>
											<div>
												<strong>Full Address:</strong> {{ $data['additional']['venueAddress'] }}
											</div>
										</div>
									</td>
								</tr>
							@endif
							@if(isset($data['gallery']) && count($data['gallery']))
								<tr>
									<td class="pad-10 valign-top" width="100%">
										<div class="text-left">
											<div class="text-uppercase text-e mar-b-5">
												<b>Images provided by Customer</b>
											</div>
											@foreach($data['gallery'] as $imageLink)
												<div class="ap-ticket-image-wrap in-blk mar-t-5 mar-r-5">
													<img class="ap-ticket-image" src="{{ $imageLink }}">
												</div>
											@endforeach
										</div>
									</td>
								</tr>
							@endif
						</table>
					</div>
					<hr class="sep">
					<div class="mar-b-20">
						<div class="col-md-10 col-md-offset-1">
							<div class="col-md-6">
								<a href="#" class="btn btn-reply confirm" data-toggle="modal" data-target="#modalConfirmBooking">Accept</a>
							</div>
							<div class="col-md-6">
								<a href="#" class="btn btn-reply cancel" data-toggle="modal" data-target="#modalCancelBooking">Reject</a>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection

@section("javascript")
	<script type="text/javascript">
		$('#confirmBooking').prop('disabled', true);
		$('#cancelBooking').prop('disabled', true);

		var CaptchaCallback = function () {
			grecaptcha.render('captchaField1', {
				'sitekey': '{{ config('evibe.google.re_cap_site') }}',
				'callback': function (response) {
					$('#confirmBooking').prop('disabled', false);
				}
			});
			grecaptcha.render('captchaField2', {
				'sitekey': '{{ config('evibe.google.re_cap_site') }}',
				'callback': function (response) {
					$('#cancelBooking').prop('disabled', false);
				}
			});
		};

		/*==== Open the accept / reject Modal if the reference url has #confirm ====*/
		var pageUrl = window.location.href;
		if (pageUrl.indexOf('#confirm') != -1) {
			$('#modalConfirmBooking').modal('show');
			console.log("true");
		}
		if (pageUrl.indexOf('#cancel') != -1) {
			$('#modalCancelBooking').modal('show');
		}
	</script>
	<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
@endsection