@extends('pay.base')

@section('page-title')
	<title>Thanks for the confirmation | Evibe.in</title>
@endsection

@section('content')
	<div class="pay-body">
		<div class="pay-success-page">
			<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 auto-booking">
				<div class="checkout-wrap no-pad">
					<div class="co-data font-16 ask-confirm-wrap">
						<h4 class="text-center section-header confirmed">
							<img src='{{$galleryUrl}}/main/img/icons/check.png' height="20px" width="20px" alt='Success'> Order Confirmed
						</h4>
						<div class="main-content">
							@if(isset($data['email']) && $data['email'])
								You have confirmed the order. Please check email sent to {{ $data['email'] }} for complete order and delivery details
							@else
								You have confirmed the order. Please check your email for complete order and delivery details
							@endif

							<div class="mar-t-30 font-14">
								<table class="table table-bordered border-black" width="100%">
									<tr>
										<td class="pad-10 valign-top" width="100%">
											<div class="text-left">
												<div class="text-uppercase text-e mar-b-5"><b>Order Details</b></div>
												<div><strong>Customer Name:</strong> {{ $data['customer']['name'] }}
												</div>
												<div><strong>Occasion:</strong> {{ $data['additional']['eventName'] }}
												</div>
												<div><strong>Party Date Time:</strong> {{ $data['partyDateTime'] }}
												</div>
												<div>
													<strong>Customer Special Notes* (Needs approval if not already discussed):</strong>
													@if($data['additional']['specialNotes'])
														{!! $data['additional']['specialNotes'] !!}
													@else
														--
													@endif
												</div>
												@if(isset($data['bookings']) && $data['bookings'])
													<div class="mar-t-10">
														<table class="table table-bordered border-black mar-b-10" width="100%">
															@foreach($data['bookings'] as $booking)
																<tr>
																	<td class="pad-10 valign-top" width="60%">
																		<div class="mar-b-10">
																			<span class="product-type-text">{{ $booking['typeBookingDetails'] }}</span>
																		</div>
																		<div>
																			<strong>Booking Details: </strong> {!! $booking['bookingInfo'] !!}
																		</div>
																		@if(isset($booking['checkoutFields']))
																			@foreach($booking['checkoutFields'] as $key => $value)
																				<div>
																					<strong>{{ $key }}: </strong>{{ $value }}
																				</div>
																			@endforeach
																		@endif
																	</td>
																	<td class="pad-10 valign-top" width="40%">
																		@if(isset($data['typeTicketAddOn']) && isset($booking['itemMapTypeId']) && ($booking['itemMapTypeId'] == $data['typeTicketAddOn']))
																			<div>
																				<strong>Booking Units:</strong> Rs. {{ $booking['bookingUnits'] }}
																			</div>
																		@else
																			<div>
																				<strong>Product Price:</strong> Rs. {{ $booking['productPriceStr'] }}
																			</div>
																			@if(isset($booking['itemMapTypeId']) && isset($data['decorTypeId']) && ($booking['itemMapTypeId'] == $data['decorTypeId']))
																				<div><strong>Delivery Charge:</strong>
																					@if($booking['transportCharges'] > 0)
																						Rs. {{ $booking['transportChargesStr'] }}
																					@else
																						<span class="text-g"> Free </span>
																					@endif
																				</div>
																			@endif
																		@endif
																		<div>
																			<strong>Booking Amount:</strong> Rs. {{ $booking['bookingAmountStr'] }}
																		</div>
																		@if(isset($booking['itemMapTypeId']) && isset($data['venueDealsTypeId']) && ($booking['itemMapTypeId'] == $data['venueDealsTypeId']))
																			<div>
																				<strong>Advance Paid:</strong> Rs. {{ $booking['tokenAmountStr'] }}
																			</div>
																		@else
																			<div>
																				<strong>Advance Paid:</strong> Rs. {{ $booking['advanceAmountStr'] }}
																			</div>
																		@endif
																		<div>
																			<strong>Balance Amount:</strong> Rs. {{ $booking['balanceAmountStr'] }}
																		</div>
																	</td>
																</tr>
															@endforeach
														</table>
													</div>
													@if(count($data['bookings']) > 1)
														<div class="mar-t-10">
															<div>
																<strong>Total Booking Amount:</strong> Rs. {{ $data['totalBookingAmountStr'] }}
															</div>
															<div>
																<strong>Total Advance Paid:</strong> Rs. {{ $data['totalAdvancePaidStr'] }}
															</div>
															<div>
																<strong>Total Balance Amount:</strong> Rs. {{ $data['totalBalanceAmountStr'] }}
															</div>
														</div>
													@endif
												@endif
											</div>
										</td>
									</tr>
									<tr>
										<td class="pad-10 valign-top" width="100%">
											<div class="text-left">
												<div class="text-uppercase text-e mar-b-5"><b>Delivery Details</b>
												</div>
												<div>
													<strong>Location:</strong> {{ $data['additional']['venueLocation'] }}
												</div>
												<div>
													<strong>Full Address:</strong> {{ $data['additional']['venueAddress'] }}
												</div>
											</div>
										</td>
									</tr>
									@if(isset($data['gallery']) && count($data['gallery']))
										<tr>
											<td class="pad-10 valign-top" width="100%">
												<div class="text-left">
													<div class="text-uppercase text-e mar-b-5">
														<b>Images provided by Customer</b>
													</div>
													@foreach($data['gallery'] as $imageLink)
														<div class="ap-ticket-image-wrap in-blk mar-t-5 mar-r-5" style="height: 60px; width: 90px; display: inline-block; margin-top: 5px; margin-right: 5px;">
															<img class="ap-ticket-image" src="{{ $imageLink }}">
														</div>
													@endforeach
												</div>
											</td>
										</tr>
									@endif
								</table>
							</div>

							<div class="mar-t-10">
								<u>Important:</u> Please take a picture of the decor before the start of party and upload it in Evibe.in
							</div>

							<div class="mar-t-20">
								<a href="https://play.google.com/store/apps/details?id=in.evibe.evibe">Download our partner app</a><br> This will help us maintain your offerings up to date on our website that will fetch you more order & to protect you incase any issues.
							</div>
							<div class="mar-t-20">
								Should you have any queries, please reply to the email (or) contact your Evibe.in account manager / 9640204000. We will respond at the earliest.
							</div>

							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
@endsection

@section('javascript')
@endsection