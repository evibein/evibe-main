<section class="modal fade modal-pre-checkout" id="bookNowModal" tabindex="-1" role="dialog" aria-labelledby="bookNowModal" aria-hidden="true">
	<div class="modal-dialog no-pad">
		<div class="cross-button ga-btn-close-book-now-modal text-center" data-dismiss="modal">x</div>
		<div class="modal-content">
			<div class="modal-body no-pad">
				<div class="col-md-6 no-pad no-mar hidden-sm hidden-xs">
					<div class="pc-left-side pc-left-side-spl-exps no-pad" @if($occasionId == config('evibe.occasion.pre-post.id')) style="background: url('{{ $galleryUrl }}/main/img/bg-reception-ab.jpg') #000000;" @elseif($occasionId == config('evibe.occasion.bachelor.id')) style="background: url('{{ $galleryUrl }}/main/img/bg-bachelors.jpg') #000000;" @else style="background: url('{{ $galleryUrl }}/main/img/bg-birthday.jpg') #000000;" @endif>
						<div class="pc-bg-overlay"></div>
						<ul class="ls-none no-pad no-mar pc-booking-steps pc-spl-exps-booking-steps">
							<li>
								<div class="bs-title">
									<i class="material-icons">&#xE870;</i> Pay Advance
								</div>
								<div class="bs-desc">You securely pay {{config('evibe.ticket.advance.percentage')}}% of total booking amount for instant booking.</div>
							</li>
							<li>
								<div class="bs-title">
									<i class="material-icons">&#xE862;</i> Confirm Booking
								</div>
								<div class="bs-desc">We will check the availability for your order and confirm your booking within 4 business hours (based on their calendar).</div>
							</li>
							<li>
								<div class="bs-title">
									<i class="material-icons">&#xE01D;</i> Enjoy
								</div>
								<div class="bs-desc">You sit back, relax & enjoy. An event coordinator will be assigned who will make sure that all your order details are fulfilled.</div>
							</li>
						</ul>
						<div class="pc-refund-policy">
							<div class="pc-rp-title">100% Refund</div>
							<div class="pc-rp-desc">
								In case of non-availability of your order, we will initiate complete refund of the total advance paid within 2 business days.
								Otherwise, your total party is on us. No questions asked.
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12 no-pad">
					<div class="pc-right-side">
						<h5 class="pc-order-title">{{ $data['name'] }}</h5>
						<div class="pc-order-data">
							<div class="col-md-12 hide">
								<div class="pc-highlights-wrap">
									<div class="pc-total-order-price">
										<div class="col-md-6 no-pad-l text-center pad-b-10">
											<div class="pc-high-title pc-amount-title">Total Booking Amount</div>
											<div class="pc-amount pc-total-amount">
												<span class='rupee-font'>&#8377;</span>
												<span id="pcBookingPrice" data-price="{{ $data['minPrice'] }}">
													{{ $data['minPrice'] }}
												</span>
											</div>
										</div>
										<div class="col-md-6 no-pad-l text-center pad-b-10">
											<div class="pc-high-title pc-amount-title">Advance To Pay</div>
											<div class="pc-amount pc-total-amount">
												<span id="pcAdvanceAmount">
													@advAmount($data['minPrice'])
												</span>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="pc-adv-options-wrap mar-t-15 mar-b-15">
									<div class="pc-adv-radio" id="advanceSelect">
										<label class="btn-adv-option pc-adv-radio-option">
											<input type="radio" name="advSelectOption" class="adv-radio-btn pull-left" value="0">
											<div class="adv-radio-option-text">
												Pay <span class="text-bold text-e"> @advAmount($data['minPrice'])</span> ({{config('evibe.ticket.advance.percentage')}}%) as advance
											</div>
										</label>
										<label class="btn-adv-option pc-adv-radio-option">
											<input type="radio" name="advSelectOption" class="adv-radio-btn pull-left" value="1" checked="checked">
											<div class="adv-radio-option-text">
												Pay <span class="text-bold text-e">@price($data['minPrice'])</span> (100%) as advance
											</div>
										</label>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div id="availCheckFailMsg" class="avail-check-fail-msg hide">
									<i class="glyphicon glyphicon-remove-sign"></i> We regret to inform that this service is not available for
									<span id="availCheckRegretMsg" class="text-bold"></span>
								</div>
							</div>
							<div class="col-md-10 col-md-offset-1">
								<div id="autoBookingForm" data-url="{{ route('auto-book.ent.pay.auto.init', [$mapId, $occasionId]) }}">
									<div class="errors-msg pad-t-3 pad-b-4 hide alert-danger"></div>
									<div class="input-field">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
											<input id="phone" type="text" class="mdl-textfield__input" name="phone">
											<label class="mdl-textfield__label" for="phone">
												<span class="label-text">Phone Number</span>
											</label>
										</div>
									</div>
									<div class="input-field">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
											<input class="mdl-textfield__input" type="text" name="partyDate" id="partyDate"/>
											<label class="mdl-textfield__label" for="partyDate">Party Date</label>
										</div>
									</div>
									<div class="input-field">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
											<input class="mdl-textfield__input" type="text" name="partyTime" id="partyTime"/>
											<label class="mdl-textfield__label" for="partyTime">
												Party Time</label>
										</div>
									</div>
									<input type="hidden" id="forVenueDetails" class="forVenueDetails" value="{{ $mapTypeId }}">
									<input type="hidden" id="occasionId" name="occasionId" value="{{ $data['occasionId'] }}">
									<div class="text-center">
										<div class="input-field">
											<input id="agreePolicy" name="checkbox" type="checkbox">
											<label for="agreePolicy" class="text-normal mar-l-2">
												I have read and accept
												<a href="{{ route('terms') }}" target="_blank">terms of booking</a>.
											</label>
										</div>
										<button type="submit" id="btnPcSubmit" class="btn btn-primary btn-pc-proceed mar-t-20">PROCEED
										</button>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</section>

@include("app.generic-modal-loader")