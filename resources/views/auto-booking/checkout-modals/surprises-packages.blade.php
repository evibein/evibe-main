@extends('auto-booking.checkout-modals.generic-package')

@section("pre-checkout:left_advance")
	<div class="bs-desc">Pay securely using your preferred payment mode for instant booking.</div>
@endsection

@section('auto-book-modal-image')
	<div class="pc-left-side pc-left-side-spl-exps no-pad" style="background: url('{{ $galleryUrl }}/main/img/bg-couple-exp.jpg') #000000;">
		@endsection

		@section('auto-book-modal-right')
			<div class="pc-right-side">
				<div class="pc-order-data pad-t-10 pad-b-10">
					<div class="col-md-12">
						@if(($data['map_type_id']) == 3)
							<div class="pc-highlights-wrap mar-t-20">
								<div class="pc-total-order-price hide">
									<div class="col-md-6 no-pad-l text-center pad-b-10">
										<div class="pc-high-title pc-amount-title">Total Booking Amount</div>
										<div class="pc-amount pc-total-amount">
											<span class='rupee-font'>&#8377;</span>
											<span id="pcBookingPrice" data-price="{{ $data['price'] }}">
													{{ $data['price'] }}
												</span>
										</div>
									</div>
									<div class="col-md-6 no-pad-l text-center pad-b-10">
										<div class="pc-high-title pc-amount-title">Advance To Pay</div>
										<div class="pc-amount pc-total-amount">
											<span id="pcAdvanceAmount">@advAmount($data['price'])</span>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div>
									@if($data['price_per_extra_guest'])
										<div class="col-md-4 no-pad-l pad-b-10 text-center">
											<div class="pc-high-title">Extra / Guest</div>
											<div id="pcPricePerExtraGuest" data-value="{{ $data['price_per_extra_guest'] }}" class="pc-high-value">@price($data['price_per_extra_guest'])
											</div>
										</div>
									@endif
									<div class="col-md-4 no-pad-l pad-b-10 text-center">
										<div class="pc-high-title">Check In</div>
										<div class="pc-high-value">{{ $data['check_in'] }}</div>
									</div>
									<div class="col-md-4 no-pad-l pad-b-10 text-center">
										<div class="pc-high-title">Check Out</div>
										<div class="pc-high-value">{{ $data['check_out'] }}</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						@endif
						<div class="pc-adv-options-wrap mar-t-15 mar-b-15 hide">
							<div class="pc-adv-radio" id="advanceSelect">
								<label class="btn-adv-option pc-adv-radio-option">
									<input type="radio" name="advSelectOption" class="adv-radio-btn pull-left" value="0">
									<div class="adv-radio-option-text">
										Pay <span class="text-bold text-e"> @advAmount($data['price'])</span> ({{config('evibe.ticket.advance.percentage')}}%) as advance
									</div>
								</label>
								<label class="btn-adv-option pc-adv-radio-option">
									<input type="radio" name="advSelectOption" class="adv-radio-btn pull-left" value="1" checked="checked">
									<div class="adv-radio-option-text">
										Pay <span class="text-bold text-e">@price($data['price'])</span> (100%) as advance
									</div>
								</label>
							</div>
						</div>
					</div>
					<div class="col-md-10 col-md-offset-1 mar-t-10 text-center">
						<form id="autoBookingForm" data-url="{{ route('auto-book.surprises-packages.pay.auto.init', [$mapTypeId, $mapId, $typeId]) }}">
							<div class="errors-msg pad-t-3 pad-b-4 hide alert-danger"></div>
							<div class="input-field">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input id="phone" type="text" class="mdl-textfield__input" name="phone">
									<label class="mdl-textfield__label" for="phone">
										<span class="label-text">Phone Number</span>
									</label>
								</div>
							</div>
							<div class="input-field">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input class="mdl-textfield__input" type="text" name="checkInDate" id="checkInDate"/>
									<label class="mdl-textfield__label" for="checkInDate">
										@if(($data['map_type_id']) == 3) Check In Date @else Party Date @endif</label>
								</div>
							</div>
							@if(($data['map_type_id']) != 3)
								<div class="input-field">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<input class="mdl-textfield__input" type="text" name="partyTime" id="partyTime"/>
										<label class="mdl-textfield__label" for="partyTime">
											Party Time</label>
									</div>
								</div>
							@endif
							@if($data['guests_max'] && $data['guests_max'] > $data['group_count'])
								<div class="input-field">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<label class="mdl-textfield__label" for="pcGuestsCount">
											<span class="label-text">Guests Count</span>
										</label>
										<select id="pcGuestsCount" name="pcGuestsCount" class="mdl-textfield__input" data-groupcount="{{ $data['group_count'] }}">
											@for($i = $data['group_count']; $i <= $data['guests_max']; $i++)
												<option value="{{ $i }}"> {{ $i }}</option>
											@endfor
										</select>
									</div>
								</div>
							@endif
							<div class="text-center mar-b-20__400">
								<div class="input-field">
									<input id="agreePolicy" name="checkbox" type="checkbox">
									<label for="agreePolicy" class="text-normal mar-l-2">
										I have read and accept
										<a href="{{ route('terms') }}" target="_blank"> terms of booking</a>.
									</label>
								</div>
								<button type="submit" id="btnPcSubmit" class="btn btn-primary btn-pc-proceed mar-t-20">PROCEED
								</button>
							</div>
							<input type="hidden" id="occasionId" name="occasionId" value="{{ config('evibe.occasion.surprises.id') }}">
							<input type="hidden" id="forVenueDetails" class="forVenueDetails" value="{{ $data['map_type_id'] }}">
						</form>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
@endsection