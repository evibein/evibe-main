@extends("auto-booking.checkout-modals.util.base")

@section('modal-body')
	<div class="col-md-12 col-lg-12 no-pad">
		@include("auto-booking.checkout-modals.util.booking-steps")
	</div>
	@if(isset($addOns) && count($addOns))
		<div class="col-md-6 col-lg-6 no-pad-4">
			<div class="mar-t-15">
				<div class="pc-modal-addons-sec">
					<div class="pc-modal-addons-title">
						<b>Add Ons</b>
					</div>
					<div class="pc-modal-addons-wrap mar-t-5">
						@foreach($addOns as $addOn)
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad mar-b-15">
								@include('occasion.util.add-ons.card')
							</div>
						@endforeach
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	@endif
	<div class="col-md-6 col-lg-6 pad-t-25 @if(isset($addOns) && count($addOns)) no-pad-l @else col-md-offset-3 col-lg-offset-3 text-center @endif">
		@include("auto-booking.checkout-modals.ent-add-ons.util.highlights-price-section")
		@include("auto-booking.checkout-modals.ent-add-ons.util.input-section")
	</div>
	<div class="col-md-12 col-lg-12 no-pad mar-t-5">
		@include("auto-booking.checkout-modals.util.unavailability-refund")
	</div>
	<div class="clearfix"></div>
@endsection