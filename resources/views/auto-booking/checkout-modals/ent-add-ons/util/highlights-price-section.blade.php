<div class="pc-order-data">
	<div class="pc-highlights-wrap-2 mar-t-15">
		<div>
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad-l text-center pad-b-10">
				<div class="pc-high-title pc-amount-title">Service Price</div>
				<div class="pc-amount pc-total-amount">
					<input type="hidden" id="pcProductPrice" value="{{ $data['minPrice'] }}">
					<span class='rupee-font'>&#8377; </span><span id="pcBookingPrice" data-price="{{ $data['minPrice'] }}">@amount($data['minPrice'])</span>
				</div>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad-l text-center pad-b-10">
				<div class="pc-high-title pc-amount-title">Add-Ons Price</div>
				<div class="pc-amount pc-total-amount">
					<span class='rupee-font'>&#8377; </span><span id="pcAddOnsPrice" data-price="0">0</span>
				</div>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad-l text-center pad-b-10">
				<div class="pc-high-title pc-amount-title">Total Amount</div>
				<div class="pc-amount pc-total-amount">
					<span class='rupee-font'>&#8377; </span><span id="pcTotalBookingAmount" data-price="{{ $data['minPrice'] }}">@amount($data['minPrice'])</span>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		@if(isset($data['minPrice']) && isset($data['worth']) && ($data['minPrice'] < $data['worth']))
			<div class="text-center mar-t-10">
				<div class="pc-modal-save-text pc-modal-off-amount">
					<span>Congrats! You saved </span>
					<span class="text-bold">
						<span class='rupee-font'>&#8377; </span><span id="pcOffPrice" data-price="{{ $data['minPrice'] }}" data-price-worth="{{ $data['worth'] }}">
							@offPriceAmount($data['minPrice'], $data['worth'])
						</span>
					</span>
				</div>
			</div>
		@endif
	</div>
	<div class="pc-adv-options-wrap mar-t-15 mar-b-15">
		<div class="pc-adv-radio" id="advanceSelect">
			<label class="btn-adv-option pc-adv-radio-option">
				<input type="radio" name="advSelectOption" class="adv-radio-btn pull-left" value="0">
				<div class="adv-radio-option-text">
					Pay <span class="text-bold text-e"><span class="rupee-font">&#8377; </span><span class="adv-radio-advance-amount" data-adv-percent="{{config('evibe.ticket.advance.percentage')}}"> {{ round($data['minPrice'] / config('evibe.ticket.advance.percentage')) }}</span></span> as advance
					<div class="font-12 pc-adv-percent-text">({{config('evibe.ticket.advance.percentage')}}% of service <span class="add-ons-price-wrap no-mar-l"> + <span class="add-ons-selected-count"></span> add-ons</span>)</div>
				</div>
			</label>
			<label class="btn-adv-option pc-adv-radio-option">
				<input type="radio" name="advSelectOption" class="adv-radio-btn pull-left" value="1" checked="checked">
				<div class="adv-radio-option-text">
					Pay <span class="text-bold text-e"><span class="rupee-font">&#8377; </span><span class="adv-radio-total-amount" data-adv-percent="100">{{ $data['minPrice'] }}</span></span> as advance
					<div class="font-12 pc-adv-percent-text">(100% of service <span class="add-ons-price-wrap no-mar-l"> + <span class="add-ons-selected-count"></span> add-ons</span>)</div>
				</div>
			</label>
		</div>
	</div>
</div>