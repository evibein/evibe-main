<div class="pc-highlights-wrap-2 mar-t-15">
	<div class="col-md-12">
		<div id="availCheckFailMsg" class="avail-check-fail-msg hide">
			<i class="glyphicon glyphicon-remove-sign"></i> We regret to inform that this service is not available for
			<span id="availCheckRegretMsg" class="text-bold"></span>
		</div>
	</div>
	<div class="col-md-10 col-md-offset-1">
		<div id="autoBookingForm" data-url="{{ route('auto-book.ent.pay.auto.init', [$mapId, $occasionId]) }}">
			<div class="errors-msg pad-t-3 pad-b-4 hide alert-danger"></div>
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad">
				@if(isset($data['countries']) && count($data['countries']))
					<select id="checkoutModalCallingCode" name="checkoutModalCallingCode" class="form-control country-calling-code">
						@foreach($data['countries'] as $country)
							<option value="{{ $country->calling_code }}">{{ $country->calling_code }}</option>
						@endforeach
					</select>
				@else
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
						<input class="mdl-textfield__input mar-b-5" type="text" name="checkoutModalCallingCode" id="checkoutModalCallingCode" value="+91"/>
						<label class="mdl-textfield__label" for="checkoutModalCallingCode">Code</label>
					</div>
				@endif
			</div>
			<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-pad-r">
				<div class="input-field">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="phone" type="text" class="mdl-textfield__input" name="phone">
						<label class="mdl-textfield__label" for="phone">
							<span class="label-text">Phone Number</span>
						</label>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="input-field">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					<input class="mdl-textfield__input" type="text" name="partyDate" id="partyDate"/>
					<label class="mdl-textfield__label" for="partyDate">Party Date</label>
				</div>
			</div>
			<div class="input-field">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					<input class="mdl-textfield__input" type="text" name="partyTime" id="partyTime"/>
					<label class="mdl-textfield__label" for="partyTime">
						Party Time</label>
				</div>
			</div>
			<input type="hidden" id="forVenueDetails" class="forVenueDetails" value="{{ $mapTypeId }}">
			<input type="hidden" id="occasionId" name="occasionId" value="{{ $data['occasionId'] }}">
			<div class="text-center">
				<div class="input-field">
					<input id="agreePolicy" name="checkbox" type="checkbox">
					<label for="agreePolicy" class="text-normal mar-l-2">
						I have read and accept
						<a href="{{ route('terms') }}" target="_blank">terms of booking</a>.
					</label>
				</div>
				<button type="submit" id="btnPcSubmit" class="btn btn-primary btn-pc-proceed mar-t-20">PROCEED
				</button>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>