@extends("auto-booking.checkout-modals.util.base")

@section('modal-body')
	@if(isset($addOns) && count($addOns))
		<div class="col-xs-12 col-sm-12">
			<div class="pc-highlights-wrap-2 mar-t-15">
				<div class="pc-modal-addons-sec">
					<div class="pc-modal-addons-title">
						<b>Add Ons</b>
						<span class="add-ons-price-wrap">
							<b><span class="add-ons-selected-count"></span></b> add-ons selected - <b><span><span class="rupee-font">&#8377; </span><span class="add-ons-price">0</span></span></b>
						</span>
					</div>
					<div class="modal-content-overflow mar-t-5">
						@foreach($addOns as $addOn)
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad mar-b-15">
								@include('occasion.util.add-ons.card')
							</div>
						@endforeach
						<div class="clearfix"></div>
					</div>
					<div class="text-center mar-t-10">
						<a class="btn-modal-overflow" href="#">+ Show more add-ons</a>
					</div>
				</div>
			</div>
		</div>
	@endif
	<div class="col-xs-12 col-ms-12">
		@include("auto-booking.checkout-modals.cake-add-ons.util.input-section")
	</div>
	<div class="clearfix"></div>
@endsection