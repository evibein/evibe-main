<div class="pc-highlights-wrap-2 mar-t-15">
	<div class="modal-body no-pad">
		<div class="col-md-12 col-sm-12 col-xs-12 no-pad">
			<div class="pc-right-side">
				<div class=" vd-highlights-wrap mar-t-10">
					<div class="pull-left">
						<h5 class="pc-order-title"> {{ $data['name'] }}</h5>
					</div>
					<div class="pull-right">
						<div class="pc-high-title pc-amount-title mar-r-8 in-blk">@if(isset($data['advanceAmount']) && $data['advanceAmount']>0) Booking Amount: @else Amount: @endif</div>
						<div class="card-item price-item cake-price-cnt in-blk">
							<div class="price-content hide">
								<div class="price-worth font-16 in-blk">
									<span class='rupee-font'>&#8377;</span>
									<del>
										<span class="displayWorth">{{ $data['price_worth'] }}</span>
									</del>
								</div>
								<div class=" font-16 pc-total-amount in-blk mar-l-5">
											<span class="text-e">
												<span class='rupee-font'>&#8377;</span>
											<span class="displayPrice">{{ $data['base_price'] }}</span>
											</span>
								</div>
							</div>
							<div class="loading-cnt">
								<img src="{{ $galleryUrl }}/img/icons/fb_loading.png" alt="loading..." class="rotate">
							</div>
						</div>
						@if(isset($data['advanceAmount']) && $data['advanceAmount']>0)
							<div class="pc-high-title  pc-amount-title in-blk">Advance Amount:</div>
							<div class="pc-amount pc-total-amount in-blk">
								<span class="text-e"> @price($data['advanceAmount']) </span>
							</div>
						@endif
						<div class="text-muted cake-extra-price hide">
							<span class="pc-amount-title pc-high-title mar-r-8">Delivery charge: </span>
							<span class="text-e font-16">
										<span class='rupee-font'>&#8377;</span>
										<span id="deliveryCharge">10</span>
									</span>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<hr class="no-mar-b">
				<div class="cake-extra-price hide">
					<div class="pull-right">
						<div class="text-muted font-11">
							TOTAL AMOUNT : <span class="text-e font-18"> <span class='rupee-font'>&#8377;</span>
									<span id="totalPrice"></span>
									</span>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div>
				<div class="col-sm-12 col-md-12 col-xs-12">
					<div class="col-sm-12 col-md-12 col-xs-12">
						<div class="mar-t-20 selected-field">
							<!-- Don't remove this field. this is being used for filling the cake price by javascript !-->
						</div>
					</div>
					<div class="col-sm-12 col-md-12 col-xs-12">
						<div class="pc-order-form pc-vd-order-form">
							<div id="cakeAutoBookingForm" data-url="{{ route('auto-book.cake.pay.auto.init', [$data['id'], $data['occasionId']]) }}">
								<div class="errors-msg pad-t-3 pad-b-4 hide alert-danger"></div>
								<div>
									<div class="col-md-10 col-sm-10 col-md-offset-1 col-sm-offset-1">
										<div class="input-field">
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<input id="name" type="text" class="mdl-textfield__input" name="name" value="{{ old('name') }}">
												<label class="mdl-textfield__label" for="name">
													<span class="label-text">Name</span>
												</label>
											</div>
										</div>
									</div>
									<div class="col-md-10 col-sm-10 col-md-offset-1 col-sm-offset-1">
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad">
											@if(isset($data['countries']) && count($data['countries']))
												<select id="checkoutModalCallingCode" name="checkoutModalCallingCode" class="form-control country-calling-code">
													@foreach($data['countries'] as $country)
														<option value="{{ $country->calling_code }}">{{ $country->calling_code }}</option>
													@endforeach
												</select>
											@else
												<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
													<input class="mdl-textfield__input mar-b-5" type="text" name="checkoutModalCallingCode" id="checkoutModalCallingCode" value="+91"/>
													<label class="mdl-textfield__label" for="checkoutModalCallingCode">Code</label>
												</div>
											@endif
										</div>
										<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-pad-r">
											<div class="input-field">
												<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
													<input id="phone" type="text" class="mdl-textfield__input" name="phone" value="{{ old('phone') }}" maxlength="10">
													<label class="mdl-textfield__label" for="phone">
														<span class="label-text">Phone Number</span>
													</label>
												</div>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="col-md-10 col-sm-10 col-md-offset-1 col-sm-offset-1">
										<div class="input-field">
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<input id="email" type="text" class="mdl-textfield__input email-typo-error" name="email" value="{{ old('email') }}">
												<label class="mdl-textfield__label" for="email">
													<span class="label-text">Email</span>
												</label>
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="pad-t-10">
									<div class="col-md-10 col-sm-10 col-md-offset-1 col-sm-offset-1">
										<div class="input-field">
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<label class="mdl-textfield__label" for="cakeDeliveryDate">Delivery Date</label>
												<input class="mdl-textfield__input" type="text" name="cakeDeliveryDate" autocomplete="off" id="cakeDeliveryDate" value="{{ old('cakeDeliveryDate') }}"/>
											</div>
										</div>
									</div>
									<div class="col-md-10 col-sm-10 col-md-offset-1 col-sm-offset-1">
										<div class="input-field">
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<label class="mdl-textfield__label" for="cakeSlot">
													<span class="label-text">Delivery Slot</span>
												</label>
												<select name="cakeSlot" id="cakeSlot" class="mdl-textfield__input ">
													@foreach($data['slots'] as $slot)
														<option value="{{ $slot->id }}" data-price="{{ $slot->price }}">{{ $slot->formattedSlot() }}</option>
													@endforeach
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-10 col-sm-10 col-md-offset-1 col-sm-offset-1">
										<div class="input-field">
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<label class="mdl-textfield__label" for="message">Message on cakes (Max 30 chars)</label>
												<textarea class="mdl-textfield__input" name="message" id="message">{{ old('message') }}</textarea>
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="text-center">
									<button id="btnCakeSubmit" class="btn btn-primary btn-pc-proceed mar-t-20">PROCEED
									</button>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div class="hide">
		<div class="mar-t-10" id="selectedTypeMarkup">
			<div class="col-sm-12 col-xs-12 mar-t-10 text-center">
				<div class="font-14 text-muted title in-blk">
					Weights
				</div>
				<div class="font-16 mar-t-5 mar-l-15 value in-blk">
					1 KG
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>