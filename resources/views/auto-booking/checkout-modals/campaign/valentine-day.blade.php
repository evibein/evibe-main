<section class="modal fade modal-pre-checkout" id="bookNowModal" tabindex="-1" role="dialog" aria-labelledby="bookNowModal" aria-hidden="true">
	<div class="modal-dialog no-pad">
		<div class="modal-content">
			<div class="modal-body no-pad">
				<div class="col-md-6 no-pad no-mar hidden-sm hidden-xs">
					<div class="pc-left-side pc-left-side-bachelors no-pad" style="background: url('{{ $galleryUrl }}/main/img/bg-bachelors.jpg') #000000;">
						<div class="pc-bg-overlay"></div>
						<ul class="ls-none no-pad no-mar pc-booking-steps pc-bachelor-booking-steps">
							<li>
								<div class="bs-title">
									<i class="material-icons">&#xE870;</i> Book Now
								</div>
								<div class="bs-desc">You securely pay for instant booking.</div>
							</li>
							<li>
								<div class="bs-title">
									<i class="material-icons">&#xE862;</i> Confirm Booking
								</div>
								<div class="bs-desc">We will check the availability for your order and confirm your booking within 24 hours.</div>
							</li>
							<li>
								<div class="bs-title">
									<i class="material-icons">&#xE01D;</i> Delivered
								</div>
								<div class="bs-desc">Your order will be delivered on 14 February 2017.</div>
							</li>
						</ul>
						<div class="pc-refund-policy">
							<div class="pc-rp-title">100% Refund</div>
							<div class="pc-rp-desc">
								In case of non-availability of your order, we will initiate complete refund of the total advance paid within 2 business days.
								Otherwise, your total party is on us. No questions asked.
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12 no-pad">
					<div class="pc-right-side">
						<h5 class="pc-order-title">{{ $data['name'] }}</h5>
						<div class="pc-order-data">
							<div class="col-md-12">
								<div class="pc-highlights-wrap">
									<div class="pc-total-order-price">
										<div class="col-md-6 no-pad-l text-center pad-b-10">
											<div class="pc-high-title pc-amount-title">Total Booking Amount</div>
											<div class="pc-amount pc-total-amount">
												<span class='rupee-font'>&#8377;</span>
												<span id="pcBookingPrice" data-price="{{ $price }}">
													{{ $price }}
												</span>
											</div>
										</div>
										<div class="col-md-6 no-pad-l text-center pad-b-10">
											<div class="pc-high-title pc-amount-title">Advance To Pay</div>
											<div class="pc-amount pc-total-amount">
												<span class='rupee-font'>&#8377;</span>
												<span id="pcAdvanceAmountValentineDay">
													{{ round($price) }}
												</span>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div>
										@if($data['price_per_extra_guest'])
											<div class="col-md-4 no-pad-l pad-b-10 text-center">
												<div class="pc-high-title">Extra / Guest</div>
												<div id="pcPricePerExtraGuest" data-value="{{ $data['price_per_extra_guest'] }}" class="pc-high-value">@price($data['price_per_extra_guest'])</div>
											</div>
										@endif
										@if($data['check_in'])
											<div class="col-md-4 no-pad-l pad-b-10 text-center">
												<div class="pc-high-title">Check In</div>
												<div class="pc-high-value">{{ $data['check_in'] }}</div>
											</div>
										@endif
										@if($data['check_out'])
											<div class="col-md-4 no-pad-l pad-b-10 text-center">
												<div class="pc-high-title">Check Out</div>
												<div class="pc-high-value">{{ $data['check_out'] }}</div>
											</div>
										@endif
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
							<div class="col-md-10 col-md-offset-1">
								<div id="autoBookingForm" data-url="{{ route('auto-book.campaign.valentine-day.pay.auto.init', [$mapTypeId, $mapId]) }}">
									<div class="errors-msg pad-t-3 pad-b-4 hide alert-danger"></div>
									<div class="input-field">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
											<input id="phone" type="text" class="mdl-textfield__input" name="phone">
											<label class="mdl-textfield__label" for="phone">
												<span class="label-text">Phone Number</span>
											</label>
										</div>
									</div>
									<div class="input-field">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
											<input class="mdl-textfield__input" type="text" name="checkInDate" id="checkInDate" value="{{ date("Y/m/d",1487030401) }}" disabled/>
											<label class="mdl-textfield__label" for="checkInDate">Delivery Date</label>
										</div>
									</div>
									@if($data['price_per_extra_guest'] && $data['guests_max'] && $data['guests_max'] > $data['group_count'])
										<div class="input-field">
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<label class="mdl-textfield__label" for="pcGuestsCount">
													<span class="label-text">Guests Count</span>
												</label>
												<select id="pcGuestsCount" name="pcGuestsCount" class="mdl-textfield__input" data-groupcount="{{ $data['group_count'] }}">
													@for($i = $data['group_count']; $i <= $data['guests_max']; $i++)
														<option value="{{ $i }}"> {{ $i }}</option>
													@endfor
												</select>
											</div>
										</div>
									@endif
									<div class="text-center">
										<div class="input-field">
											<input id="agreePolicy" name="checkbox" type="checkbox">
											<label for="agreePolicy" class="text-normal mar-l-2">
												I have read and accept <a href="{{ route('terms') }}" target="_blank" rel="noopener">terms of booking</a>.
											</label>
										</div>
										<button type="submit" id="btnPcSubmit" class="btn btn-primary btn-pc-proceed mar-t-20">PROCEED
										</button>
									</div>
									<input type="hidden" id="occasionId" name="occasionId" value="{{ config('evibe.occasion.bachelor.id') }}">
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</section>
