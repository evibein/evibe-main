<div class="pc-order-data">
	<div class="pc-highlights-wrap-2 mar-t-15">
		@if(($data['map_type_id']) == 3 && !(isset($tags) && count($tags) && isset($tags[config('evibe.surprise-relation-tags.candle-light-dinner')])))
			<div>
				<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pad-b-10 text-center">
						<div class="pc-high-title">Check In</div>
						<div class="pc-high-value">{{ $data['check_in'] }}</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pad-b-10 text-center">
						<div class="pc-high-title">Check Out</div>
						<div class="pc-high-value">{{ $data['check_out'] }}</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<hr>
		@endif
		<div>
			@if(isset($data['price_per_extra_guest']) && $data['price_per_extra_guest'])
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad-l text-center pad-b-10">
					<div class="pc-high-title pc-amount-title">Package Price</div>
					<div class="pc-amount pc-total-amount">
						<input type="hidden" id="pcProductPrice" value="{{ $data['price'] }}">
						<span class='rupee-font'>&#8377; </span><span id="pcBookingPrice" data-price="{{ $data['price'] }}">@amount($data['price'])</span>
					</div>
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad-l text-center pad-b-10 hide">
					<div class="pc-high-title pc-amount-title">Add-Ons Price</div>
					<div class="pc-amount pc-total-amount">
						<span class='rupee-font'>&#8377; </span><span id="pcAddOnsPrice" data-price="0">0</span>
					</div>
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad-l text-center pad-b-10">
					<div class="pc-high-title pc-amount-title">Extra Guests</div>
					<div class="pc-amount pc-total-amount">
						<span class='rupee-font'>&#8377; </span><span id="pcExtraGuestsPrice" data-price="0">0</span>
					</div>
					<div class="pc-high-text">
					<span>
						<span id="pcExtraGuestsCount">0</span>
					</span>
						<span> * </span>
						<span>
						<span class='rupee-font'>&#8377; </span><span id="pcPricePerExtraGuest" data-value="{{ $data['price_per_extra_guest'] }}">@amount($data['price_per_extra_guest'])</span>
						<span> / guest</span>
					</span>
					</div>
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad-l text-center pad-b-10">
					<div class="pc-high-title pc-amount-title">Total Amount</div>
					<div class="pc-amount pc-total-amount">
						<span class='rupee-font'>&#8377; </span><span id="pcTotalBookingAmount" data-price="{{ $data['price'] }}">@amount($data['price'])</span>
					</div>
				</div>
				<div class="clearfix"></div>
			@else
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center pad-b-10">
					<div class="pc-high-title pc-amount-title">Package Price</div>
					<div class="pc-amount pc-total-amount">
						<input type="hidden" id="pcProductPrice" value="{{ $data['price'] }}">
						<span class='rupee-font'>&#8377; </span><span id="pcBookingPrice" data-price="{{ $data['price'] }}">@amount($data['price'])</span>
					</div>
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center pad-b-10">
					<div class="pc-high-title pc-amount-title">Add-Ons Price</div>
					<div class="pc-amount pc-total-amount">
						<span class='rupee-font'>&#8377; </span><span id="pcAddOnsPrice" data-price="0">0</span>
					</div>
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center pad-b-10">
					<div class="pc-high-title pc-amount-title">Total Amount</div>
					<div class="pc-amount pc-total-amount">
						<span class='rupee-font'>&#8377; </span><span id="pcTotalBookingAmount" data-price="{{ $data['price'] }}">@amount($data['price'])</span>
					</div>
				</div>
				<div class="clearfix"></div>
			@endif
		</div>
		@if(isset($data['price']) && isset($data['price_worth']) && ($data['price'] < $data['price_worth']))
			<div class="text-center mar-t-10">
				<div class="pc-modal-save-text pc-modal-off-amount">
					<span>Congrats! You saved </span>
					<span class="text-bold">
						<span class='rupee-font'>&#8377; </span><span id="pcOffPrice" data-price="{{ $data['price'] }}" data-price-worth="{{ $data['price_worth'] }}">
							@offPriceAmount($data['price'], $data['price_worth'])
						</span>
					</span>
				</div>
			</div>
		@endif
	</div>
	<div class="pc-adv-options-wrap mar-t-15 mar-b-15 @if(isset($data['type_ticket_id']) && ($data['type_ticket_id'] == config('evibe.ticket.type.package'))) @else hide @endif">
		<div class="pc-adv-radio" id="advanceSelect">
			<label class="btn-adv-option pc-adv-radio-option">
				<input type="radio" name="advSelectOption" class="adv-radio-btn pull-left" value="0">
				<div class="adv-radio-option-text">
					Pay
					<span class="text-bold text-e"><span class="rupee-font">&#8377; </span><span class="adv-radio-advance-amount" data-adv-percent="{{config('evibe.ticket.advance.percentage')}}"> {{ round($data['price'] / config('evibe.ticket.advance.percentage')) }}</span></span> as advance
					<div class="font-12 pc-adv-percent-text">({{config('evibe.ticket.advance.percentage')}}% of service
						<span class="add-ons-price-wrap no-mar-l"> + <span class="add-ons-selected-count"></span> add-ons</span>)
					</div>
				</div>
			</label>
			<label class="btn-adv-option pc-adv-radio-option">
				<input type="radio" name="advSelectOption" class="adv-radio-btn pull-left" value="1" checked="checked">
				<div class="adv-radio-option-text">
					Pay
					<span class="text-bold text-e"><span class="rupee-font">&#8377; </span><span class="adv-radio-total-amount" data-adv-percent="100">{{ $data['price'] }}</span></span> as advance
					<div class="font-12 pc-adv-percent-text">(100% of service <span class="add-ons-price-wrap no-mar-l"> + <span class="add-ons-selected-count"></span> add-ons</span>)
					</div>
				</div>
			</label>
		</div>
	</div>
</div>