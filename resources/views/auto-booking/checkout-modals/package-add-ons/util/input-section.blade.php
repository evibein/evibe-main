<div class="pc-highlights-wrap-2 pc-highlights-package-wrap mar-t-15">
	<div class="col-md-10 col-md-offset-1 mar-t-10 text-center mar-b-10">
		<form id="autoBookingForm" data-url="{{ route('auto-book.surprises-packages.pay.auto.init', [$mapTypeId, $mapId, $typeId]) }}">
			<div class="errors-msg pad-t-3 pad-b-4 hide alert-danger"></div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad">
					@if(isset($countries) && count($countries))
						<select id="checkoutModalCallingCode" name="checkoutModalCallingCode" class="form-control country-calling-code">
							@foreach($countries as $country)
								<option value="{{ $country->calling_code }}">{{ $country->calling_code }}</option>
							@endforeach
						</select>
					@else
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
							<input class="mdl-textfield__input mar-b-5" type="text" name="checkoutModalCallingCode" id="checkoutModalCallingCode" value="+91"/>
							<label class="mdl-textfield__label" for="checkoutModalCallingCode">Code</label>
						</div>
					@endif
				</div>
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-pad-r">
					<div class="input-field">
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
							<input id="phone" type="text" class="mdl-textfield__input" name="phone">
							<label class="mdl-textfield__label" for="phone">
								<span class="label-text">Phone Number</span>
							</label>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="input-field">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input class="mdl-textfield__input @if(isset($data['type_ticket_id']) && ($data['type_ticket_id'] == config('evibe.ticket.type.villas'))) bachelor-checkout-date @endif" type="text" name="checkInDate" id="checkInDate" @if(isset($campaignDate) && $campaignDate) value="{{ $campaignDate }}" disabled @endif/>
						<label class="mdl-textfield__label" for="checkInDate">
							@if(($data['map_type_id']) == 3) Check In Date @else Party Date @endif</label>
					</div>
				</div>
			</div>
			@if(($data['map_type_id']) == 3)
				@if(isset($checkInSlots) && count($checkInSlots))
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="input-field">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<label class="mdl-textfield__label" for="checkInTime">
									<span class="label-text">Check In Time</span>
								</label>
								<select name="checkInTime" id="checkInTime" class="mdl-textfield__input ">
									@foreach($checkInSlots as $checkInSlot)
										<option value="{{ $checkInSlot['checkInTime'] }}" data-check-out-time="{{ $checkInSlot['checkOutTime'] }}">{{ $checkInSlot['checkInTime'] }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
				@else
				@endif
			@else
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="input-field">
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
							<input class="mdl-textfield__input" type="text" name="partyTime" id="partyTime"/>
							<label class="mdl-textfield__label" for="partyTime">Party Time</label>
						</div>
					</div>
				</div>
			@endif
			@if($data['guests_max'] && $data['guests_max'] > $data['group_count'])
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="input-field">
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
							<label class="mdl-textfield__label" for="pcGuestsCount">
								<span class="label-text">Guests Count</span>
							</label>
							<select id="pcGuestsCount" name="pcGuestsCount" class="mdl-textfield__input" data-groupcount="{{ $data['group_count'] }}">
								@for($i = $data['group_count']; $i <= $data['guests_max']; $i++)
									<option value="{{ $i }}"> {{ $i }}</option>
								@endfor
							</select>
						</div>
					</div>
				</div>
			@endif
			<div class="clearfix"></div>
			<div class="text-center mar-b-20__400">
				<div class="input-field">
					<input id="agreePolicy" name="checkbox" type="checkbox" checked>
					<label for="agreePolicy" class="text-normal mar-l-2">
						<span class="mar-r-4">I have read and accept</span>
						<a href="{{ route('terms') }}" target="_blank">terms of booking</a>.
					</label>
				</div>
				<button type="submit" id="btnPcSubmit" class="btn btn-primary btn-pc-proceed mar-t-20">PROCEED</button>
			</div>
			<input type="hidden" id="occasionId" name="occasionId" value="{{ config('evibe.occasion.surprises.id') }}">
			<input type="hidden" id="forVenueDetails" class="forVenueDetails" value="{{ $data['map_type_id'] }}">
		</form>
	</div>
	<div class="clearfix"></div>
</div>