<div class="des-book-now-form-wrap hide">
	<div class="des-product-book-now-form">
		<div class="modal-header hide">
			<button type="button" class="close feasibility-modal-close hide" data-dismiss="modal">&times;
			</button>
			<div class="modal-title text-center feasibility-modal-title">
				Book Now
				<div class="feasibility-modal-text sticky-form-text text-center">
					<div id="formNormalText" class="form-normal-text">
						Book Now
						<div class="feasibility-modal-title-help">
							Please enter following details to book the decor.
						</div>
					</div>
					<div id="feasibilitySuccessText" class="feasibility-success-text hide">
						<i class="glyphicon glyphicon-ok-circle feasibility-success-icon mar-r-2"></i>
						Delivery is available at
						<span id="upPinCodeSuccess" class="updated-pincode text-bold mar-l-4"></span>
						<a class="des-feasibility-change-pincode mar-r-2">(change)</a>
					</div>
					<div id="feasibilityFailText" class="text-r feasibility-fail-text hide">
						<i class="glyphicon glyphicon-remove-circle feasibility-fail-icon mar-l-2"></i>
						<span>
							Sorry, delivery is not available at
						</span>
						<span id="upPinCodeFail" class="updated-pincode text-bold mar-l-4"></span>. Please try with different pin code.
					</div>
				</div>
			</div>
		</div>
		<div class="modal-body">
			<div class="feasibility-modal-content pad-b-10">
				<div class="feasibility-modal-product-wrap">
					<div class="form-normal-text">
						<div class="feasibility-modal-product mar-b-15">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad">
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-pad">
									<div class="feasibility-modal-product-title text-center">
										{{ $data['decor']->name }}
									</div>
									<div class="feasibility-modal-product-price">
										<div class="text-center mar-t-10">
																<span class="book-now-price-worth">
																	@price($data['decor']->worth)
																</span>
											<span class="mar-l-5 book-now-price-off">
																	(@offPrice($data['decor']->min_price, $data['decor']->worth) OFF)
																</span>
											<div class="mar-t-10">
																	<span class="book-now-base-price">
																		@price($data['decor']->min_price)
																	</span>
												<span class="book-now-price-tag mar-l-5">
																		<i class="glyphicon glyphicon-thumbs-up"></i> Best Price
																	</span>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad">
									<div class="feasibility-modal-image-wrap text-right">
										<img class="feasibility-modal-image" src="@if(isset($data['gallery']) && $data['gallery']){{ $data['gallery'][0]['url'] }}@endif" alt="{{ $data['decor']->name }} {{ config('evibe.type-event-route-name.'.$data['occasionId']) }} party decoration" onError="this.src='{{ $galleryUrl }}/img/icons/balloons.png'; this.style='padding: 30px 30px'">
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="feasibility-success-text"></div>
					<div class="feasibility-fail-text">
						<div class="feasibility-modal-product mar-b-15">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad">
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-pad">
									<div class="feasibility-modal-product-title text-center">
										{{ $data['decor']->name }}
									</div>
									<div class="feasibility-modal-product-price">
										<div class="text-center mar-t-10">
																<span class="book-now-price-worth">
																	@price($data['decor']->worth)
																</span>
											<span class="mar-l-5 book-now-price-off">
																	(@offPrice($data['decor']->min_price, $data['decor']->worth) OFF)
																</span>
											<div class="mar-t-10">
																	<span class="book-now-base-price">
																		@price($data['decor']->min_price)
																	</span>
												<span class="book-now-price-tag mar-l-5">
																		<i class="glyphicon glyphicon-thumbs-up"></i> Best Price
																	</span>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad">
									<div class="feasibility-modal-image-wrap text-right">
										<img class="feasibility-modal-image" src="@if(isset($data['gallery']) && $data['gallery']){{ $data['gallery'][0]['url'] }}@endif" alt="{{ $data['decor']->name }} {{ config('evibe.type-event-route-name.'.$data['occasionId']) }} party decoration" onError="this.src='{{ $galleryUrl }}/img/icons/balloons.png'; this.style='padding: 30px 30px'">
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<div class="feasibility-modal-text pad-t-5 pad-b-5 sticky-form-text text-center">
					<div id="formNormalText" class="form-normal-text">
						<div class="modal-title text-center feasibility-modal-result-msg hide">
							Book Now
						</div>
						<div class="feasibility-modal-title-help">
							Please enter following details to book the decor.
						</div>
						<div class="mar-t-10">
							Advance To Pay
							<span class="book-now-advance mar-l-10">
														@advAmount($data['decor']->min_price)
													</span>
						</div>
					</div>
					<div id="feasibilitySuccessText" class="feasibility-success-text feasibility-modal-result-msg hide">
						<i class="glyphicon glyphicon-ok-circle feasibility-success-icon mar-r-2"></i>
						Delivery is available at
						<span id="upPinCodeSuccess" class="updated-pincode text-bold mar-l-4"></span>
						<a class="des-feasibility-change-pincode mar-l-2">(change)</a>
					</div>
					<div id="feasibilityFailText" class="text-r feasibility-fail-text feasibility-modal-result-msg hide">
						<i class="glyphicon glyphicon-remove-circle feasibility-fail-icon mar-r-2"></i>
						<span>
													Sorry, delivery is not available at
												</span>
						<span id="upPinCodeFail" class="updated-pincode text-bold mar-l-4"></span>. Please try with different pin code.
					</div>
				</div>
				<div class="feasibility-modal-price ">
					<div class="feasibility-success-text hide">
						<div class="feasibility-modal-price-content sticky-price-content mar-t-10 hide">
							<div class="sticky-price-splitUp hide pad-b-5 pad-r-15">
								<ul class="price-split-wrap">
									<li>
										<div>Decor Price</div>
										<div>
											<span class="book-now-price-worth">@price($data['decor']->worth)</span>
											<span class="book-now-split-base-price mar-l-5">@price($data['decor']->min_price)</span>
										</div>
									</li>
									<li>
										<div>Transportation Charges</div>
										<div id="transportFree" class="transport-free text-free">Free
										</div>
										<div id="transportPriceContent" class="transport-price-content book-now-transport-price hide">
											<span class='rupee-font'>&#8377;</span>
											<span id="transportPrice" class="transport-price"></span>
										</div>
									</li>
									<li>
										<div>Total Amount</div>
										<div class="book-now-total-price">
											<span class='rupee-font'>&#8377;</span>
											<span id="updatedPrice" class="updated-price"></span>
										</div>
									</li>
									<li class="hide">
										<div>Advance To Pay</div>
										<div>
											<span class='rupee-font'>&#8377;</span>
											<span id="tokenAmount" class="token-amount book-now-advance"></span>
										</div>
									</li>
								</ul>
								<div class="advance-msg mar-t-20 text-center">
									Advance To Pay
									<span class="book-now-advance mar-l-10">
												<span class='rupee-font'>&#8377;</span>
												<span id="tokenAmount" class="token-amount"></span>
											</span>
								</div>
							</div>
							<div class="sticky-price-splitUp des-price-splitUp">
								<div class="product-split-price-worth">
									<span class="pull-left">Decor Price</span>
									<span class="pull-right">@price($data['decor']->worth)</span>
									<div class="clearfix"></div>
								</div>
								<div class="product-split-base-price hide">
									<span class="pull-left">Decor Price</span>
									<span class="pull-right">@price($data['decor']->min_price)</span>
									<div class="clearfix"></div>
								</div>
								<div class="product-split-price-off">
									<span class="pull-left">Evibe discount</span>
									<span class="pull-right text-g">- @offPrice($data['decor']->min_price, $data['decor']->worth)</span>
									<div class="clearfix"></div>
								</div>
								<div class="product-split-transport-price">
									<span class="pull-left">Transportation Charges</span>
									<span id="transportFree" class="transport-free pull-right text-free">Free</span>
									<span id="transportPriceContent" class="transport-price-content pull-right hide">
																	<span class='rupee-font'>&#8377;</span>
																	<span id="transportPrice" class="transport-price"></span>
																</span>
									<div class="clearfix"></div>
								</div>
								<hr class="sticky-mobile-profile-hr-2">
								<div class="product-split-total-price">
									<span class="pull-left">Total Price</span>
									<span class="pull-right">
																	<span class='rupee-font'>&#8377;</span>
																	<span class="updated-price"></span>
																</span>
									<div class="clearfix"></div>
								</div>
								<hr class="sticky-mobile-profile-hr-2">
								<div class="product-split-advance text-bold">
									<span class="pull-left">Advance To Pay</span>
									<span class="pull-right text-e">
																	<span class='rupee-font'>&#8377;</span>
																	<span id="tokenAmount" class="token-amount"></span>
																</span>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="feasibility-modal-old-price hide">
					<div class="form-normal-text text-center">
						<div class="text-center mar-t-10">
													<span class="book-now-price-worth">
														@price($data['decor']->worth)
													</span>
							<span class="book-now-base-price mar-l-5">
														@price($data['decor']->min_price)
													</span>
							<div class="mar-t-10">
								Advance To Pay
								<span class="book-now-advance mar-l-10">
															@advAmount($data['decor']->min_price)
														</span>
							</div>
						</div>
					</div>
					<div class="feasibility-success-text hide">
						<div class="feasibility-modal-price-content sticky-price-content mar-t-10 hide">
							<div class="sticky-updated-price product-updated-price text-e hide text-center">
								<span class='rupee-font'>&#8377;</span>
								<span id="updatedPrice" class="updated-price"></span>
							</div>
							<div class="sticky-form-token-msg text-center hide mar-t-10">
								Pay
								<span class="sticky-form-token-msg-price">
											<span class='rupee-font'>&#8377;</span>
											<span id="msgTokenAmount"></span>
										</span>
								as advance to book
							</div>
							<div class="sticky-price-splitUp pad-b-5 pad-r-15">
								<ul class="price-split-wrap">
									<li>
										<div>Decor Price</div>
										<div>
											<span class="book-now-price-worth">@price($data['decor']->worth)</span>
											<span class="book-now-base-price mar-l-5">@price($data['decor']->min_price)</span>
										</div>
									</li>
									<li>
										<div>Transportation Charges</div>
										<div id="transportFree" class="transport-free text-free">Free
										</div>
										<div id="transportPriceContent" class="transport-price-content book-now-transport-price hide">
											<span class='rupee-font'>&#8377;</span>
											<span id="transportPrice" class="transport-price"></span>
										</div>
									</li>
									<li>
										<div>Total Amount</div>
										<div class="book-now-total-price">
											<span class='rupee-font'>&#8377;</span>
											<span id="updatedPrice" class="updated-price"></span>
										</div>
									</li>
									<li class="hide">
										<div>Advance To Pay</div>
										<div>
											<span class='rupee-font'>&#8377;</span>
											<span id="tokenAmount" class="token-amount book-now-advance"></span>
										</div>
									</li>
								</ul>
								<div class="advance-msg mar-t-20 text-center">
									Advance To Pay
									<span class="book-now-advance mar-l-10">
												<span class='rupee-font'>&#8377;</span>
												<span id="tokenAmount" class="token-amount"></span>
											</span>
								</div>
							</div>
						</div>
					</div>
					<div class="feasibility-fail-text hide text-center">
						<div class="text-center mar-t-10">
									<span class="book-now-price-worth">
										@price($data['decor']->worth)
									</span>
							<span class="book-now-base-price mar-l-5">
										@price($data['decor']->min_price)
									</span>
						</div>
					</div>
				</div>
				<div class="feasibility-modal-form sticky-form mar-t-5 mar-b-5">
					<form id="desBookNowForm" class="product-user-input">
						<div id="feasibilityErrorMsg" class="alert-danger form-error-message text-center hide"></div>
						<div class="col-xs-6 col-sm-6 text-center hide">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input id="bookNowName" type="text" class="mdl-textfield__input" name="bookNowName">
								<label class="mdl-textfield__label font-13">
									<span class="label-text">Name*</span>
								</label>
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 text-center hide">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input id="bookNowEmail" type="text" class="mdl-textfield__input" name="bookNowEmail">
								<label class="mdl-textfield__label font-13">
									<span class="label-text">Email*</span>
								</label>
							</div>
						</div>
						<div class="col-xs-10 col-sm-10 col-md-8 col-lg-8 col-xs-offset-1 col-sm-offset-1 col-md-offset-2 col-lg-offset-2">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad">
								@if(isset($data['countries']) && count($data['countries']))
									<select id="checkoutModalCallingCode" name="checkoutModalCallingCode" class="form-control country-calling-code">
										@foreach($data['countries'] as $country)
											<option value="{{ $country->calling_code }}">{{ $country->calling_code }}</option>
										@endforeach
									</select>
								@else
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
										<input class="mdl-textfield__input mar-b-5" type="text" name="checkoutModalCallingCode" id="checkoutModalCallingCode" value="+91"/>
										<label class="mdl-textfield__label" for="checkoutModalCallingCode">Code</label>
									</div>
								@endif
							</div>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-pad-r">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input id="bookNowPhone" type="text" class="mdl-textfield__input" name="bookNowPhone" maxlength="10">
									<label class="mdl-textfield__label font-13">
										<span class="label-text">Mobile Number*</span>
									</label>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-xs-6 col-sm-6 text-center hide">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input id="bookNowPartyArea" type="text" class="mdl-textfield__input google-auto-complete" name="bookNowPartyArea" placeholder="">
								<label class="mdl-textfield__label font-13">
									<span class="label-text">Party Area*</span>
								</label>
								<input type="hidden" class="google-location-details" name="locationDetails" value="">
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 text-center">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input id="bookNowPartyDate" type="text" class="mdl-textfield__input" name="bookNowPartyDate">
								<label class="mdl-textfield__label font-13">
									<span class="label-text">Party Date*</span>
								</label>
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 text-center">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input id="bookNowPartyPinCode" type="text" class="mdl-textfield__input" name="bookNowPartyPinCode" maxlength="6" data-url="{{ route('auto-book.transport-price-calculate', [$data["decor"]->id, config("evibe.ticket.type.decor")]) }}">
								<label class="mdl-textfield__label font-13">
									<span class="label-text">Party Area Pin Code*</span>
								</label>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 text-center hide">
							<div><i>By submitting this form, I agree to the <a
											href="{{ route('terms') }}"
											target="_blank">terms of service</a>.</i></div>
						</div>
						<div class="clearfix"></div>
					</form>
				</div>
			</div>
			<div class="feasibility-modal-cta sticky-form-cta">
				<div id="formNormalCta" class="form-normal-cta text-center">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hide">
						<div class="product-cta-message text-center pad-b-5">
							<i class="glyphicon glyphicon-info-sign"></i> You can
							<b>Book</b> the decor after checking availability.
						</div>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad hide">
						<button class="btn btn-submit product-btn-grey des-btn-back">Close</button>
					</div>
					<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 no-pad">
						<button class="btn btn-submit product-btn-book-now btn-book-now-submit" data-url="{{ route('auto-book.booking-price', [$data['decor']->id, config('evibe.ticket.type.decor'), $data['occasionId']]) }}">
							Submit
						</button>
					</div>
					<div class="clearfix"></div>
				</div>
				<div id="feasibilitySuccessCta" class="feasibility-success-cta mar-t-10 hide">
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad hide">
						<button class="btn btn-submit product-btn-grey des-btn-back">Close</button>
					</div>
					<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 no-pad">
						<button class="btn btn-submit product-btn-book-now-2 des-btn-book-now">Continue To Book</button>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-loader product-profile-form-loader text-center hide">
		<div class="profile-form-loader-image">
			<img src="/images/loading.gif" alt="">
		</div>
		<div class="profile-form-loader-text mar-t-5">Please wait..</div>
	</div>
</div>