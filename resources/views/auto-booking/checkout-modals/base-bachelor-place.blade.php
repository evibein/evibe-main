@extends('auto-booking.checkout-modals.generic-package')

@section("pre-checkout:left_advance")
	<div class="bs-desc">Pay securely using your preferred payment mode for instant booking.</div>
@endsection

@section('auto-book-modal-image')
	<div class="pc-left-side pc-left-side-bachelors no-pad" style="background: url('{{ $galleryUrl }}/main/img/bg-bachelors.jpg') #000000;">
		@endsection

		@section('auto-book-modal-right')
			<div class="pc-right-side">
				<h5 class="pc-order-title">{{ $data['name'] }}</h5>
				<div class="pc-order-data">
					<div class="col-md-12">
						<div class="pc-highlights-wrap">
							<div class="pc-total-order-price hide">
								<div class="col-md-6 no-pad-l text-center pad-b-10">
									<div class="pc-high-title pc-amount-title">Total Booking Amount</div>
									<div class="pc-amount pc-total-amount">
										<span class='rupee-font'>&#8377;</span>
										<span id="pcBookingPrice" data-price="{{ $data['price'] }}">
													{{ $data['price'] }}
												</span>
									</div>
								</div>
								<div class="col-md-6 no-pad-l text-center pad-b-10">
									<div class="pc-high-title pc-amount-title">Advance To Pay</div>
									<div class="pc-amount pc-total-amount">
										<span id="pcAdvanceAmount">
											@advAmount($data['price'])
												</span>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div>
								@if($data['price_per_extra_guest'])
									<div class="col-md-4 no-pad-l pad-b-10 text-center">
										<div class="pc-high-title">Extra / Guest</div>
										<div id="pcPricePerExtraGuest" data-value="{{ $data['price_per_extra_guest'] }}" class="pc-high-value">@price($data['price_per_extra_guest'])
										</div>
									</div>
								@endif
								<div class="col-md-4 no-pad-l pad-b-10 text-center">
									<div class="pc-high-title">Check In</div>
									<div class="pc-high-value">{{ $data['check_in'] }}</div>
								</div>
								<div class="col-md-4 no-pad-l pad-b-10 text-center">
									<div class="pc-high-title">Check Out</div>
									<div class="pc-high-value">{{ $data['check_out'] }}</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div id="availCheckFailMsg" class="avail-check-fail-msg text-center">
							<i class="glyphicon glyphicon-remove-sign"></i> This property is unavailable.
						</div>
					</div>
					<label class="btn-adv-option pc-adv-radio-option hide">
						<input type="radio" name="advSelectOption" class="adv-radio-btn pull-left" value="1" checked="checked">
						<div class="adv-radio-option-text">
							Pay <span class="text-bold text-e">@price($data['price'])</span> (100%) as advance
						</div>
					</label>
					<div class="col-md-10 col-md-offset-1">
						<div id="autoBookingForm" data-url="{{ route('auto-book.villas.pay.auto.init', [$mapTypeId, $mapId, $typeId]) }}">
							<div class="errors-msg pad-t-3 pad-b-4 hide alert-danger"></div>
							<div class="input-field">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input id="phone" type="text" class="mdl-textfield__input" name="phone">
									<label class="mdl-textfield__label" for="phone">
										<span class="label-text">Phone Number</span>
									</label>
								</div>
							</div>
							<div class="input-field">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input class="mdl-textfield__input" type="text" name="checkInDate" id="checkInDate"/>
									<label class="mdl-textfield__label" for="checkInDate">Check In Date</label>
								</div>
							</div>
							@if($data['guests_max'] && $data['guests_max'] > $data['group_count'])
								<div class="input-field">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<label class="mdl-textfield__label" for="pcGuestsCount">
											<span class="label-text">Guests Count</span>
										</label>
										<select id="pcGuestsCount" name="pcGuestsCount" class="mdl-textfield__input" data-groupcount="{{ $data['group_count'] }}">
											@for($i = $data['group_count']; $i <= $data['guests_max']; $i++)
												<option value="{{ $i }}"> {{ $i }}</option>
											@endfor
										</select>
									</div>
								</div>
							@endif
							<div class="text-center">
								<div class="input-field">
									<input id="agreePolicy" name="checkbox" type="checkbox">
									<label for="agreePolicy" class="text-normal mar-l-2">
										I have read and accept <a href="{{ route('terms') }}" target="_blank">terms of booking</a>.
									</label>
								</div>
								<button type="submit" id="btnPcSubmit" class="btn btn-primary btn-pc-proceed mar-t-20">PROCEED
								</button>
							</div>
							<input type="hidden" id="occasionId" name="occasionId" value="{{ config('evibe.occasion.bachelor.id') }}">
							<input type="hidden" id="forVenueDetails" class="forVenueDetails" value="{{ $data['map_type_id'] }}">
							<input type="hidden" id="hidMapTypeId" value="{{ $mapTypeId }}">
							<input type="hidden" id="hidMapId" value="{{ $mapId }}">
							<input type="hidden" id="hidTypeId" value="{{ $typeId }}">
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		@endsection

		@if(time() <= 1514744999) <!-- 31st Dec 2017 11:59:59 PM -->
		@section("javascript")
			@parent
			<script type="text/javascript">
                $(document).ready(function () {

                    var today = new Date();
                    var oneDay = new Date(today.getTime() + 24 * 60 * 60 * 1000);

                    $("#checkInDate").datetimepicker({
                        timepicker: false,
                        minDate: today,
                        format: 'Y/m/d',
                        scrollInput: false,
                        scrollMonth: false,
                        scrollTime: false,
                        closeOnDateSelect: true,
                        disabledDates: ['2017/12/31'],
                        onSelectDate: function (ct, $i) {
                            $i.parent().addClass('is-dirty');
                            /* for venue, decor forms */
                        }
                    });

                });
			</script>
@endsection
@endif