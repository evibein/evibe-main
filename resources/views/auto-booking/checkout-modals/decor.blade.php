<section class="modal fade modal-pre-checkout" id="bookNowModal" tabindex="-1" role="dialog" aria-labelledby="bookNowModal" aria-hidden="true">
	<div class="modal-dialog no-pad">
		<div class="cross-button text-center" data-dismiss="modal">x</div>
		<div class="modal-content decor-modal-content">
			<div class="modal-body no-pad">
				<div class="col-md-12 col-sm-12 col-xs-12 no-pad">
					<div class="ab-decor-top">
						<div class="ab-decor-profile-pic">
							<div class="col-md-12 col-sm-12 col-xs-12 no-pad">
								<div class="ab-decor-name">
									@truncateName($data['decor']->name)
								</div>
							</div>
							<div class="ab-decor-profile-pic-wrap">
								<img class="ab-decor-profile-img" src="{{ $data['decor']->getProfileImg() }}" alt="{{ $data['decor']->name }} birthday party decoration">
							</div>
							<div class="ab-decor-profile-amt">
								<span id="baseBookingAmount" data-price="{{ $data['decor']->min_price }}">@price($data['decor']->min_price)</span>
							</div>
						</div>
						<div class="ab-decor-info">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div id="verifyPincode" class="ab-decor-provider-wrap mar-t-5 ab-decor-extra-space">
									<span>Provider Area:</span>
									<span class="provider-text">{{ $data['providerArea']->name }} ({{ $data['decor']->provider->zip }})</span>
								</div>
								<div id="partyPinCodeSection" class="ab-decor-price mar-t-15">
									<div class="col-md-12 col-xs-12 no-pad">
										<div class="col-md-8 col-xs-10 col-xs-offset-1 col-md-offset-0 text-center">
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<input id="partyPincode" type="text" class="mdl-textfield__input" name="partyPincode" maxlength="6" data-url="{{ route('auto-book.transport-price-calculate', [$mapId, $mapTypeId]) }}">
												<label class="mdl-textfield__label">
													<span class="label-text">Party Location Pincode*</span>
												</label>
											</div>
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<input id="decorPhone" type="text" class="mdl-textfield__input" name="decorPhone" maxlength="10">
												<label class="mdl-textfield__label">
													<span class="label-text">Mobile Number*</span>
												</label>
											</div>
										</div>
										<div class="col-md-4 col-xs-10 col-xs-offset-1 col-md-offset-0 no-pad__400-600">
											<div class="text-center">
												<button type="submit" id="btnPincodeSubmit" class="btn btn-decor-verify">Check<br>Availability
												</button>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div id="abLoading" class="ab-decor-load-wrap mar-t-10 text-center hide">
									<img src="{{ $galleryUrl }}/img/icons/loading.gif" alt="loading">
									Please wait while we are checking the availability of this decor
								</div>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div id="ab-transport-success" class="hide">
									<div class="ab-decor-price mar-t-15">
										<div class="ab-decor-available">
											<i class="material-icons valign-mid">&#xE86C;</i>
											<span class="ad-decor-available-text">Decor is available in your area</span>
										</div>
										<div class="ab-decor-price-element mar-t-5">
											<div class="ab-decor-price-title-1 ab-decor-trans-title pull-left">
												Transportation Charges
												<span class="customer-pincode-wrap text-muted">
													(for: <span id="customerPinCode"></span>, <button type="submit"
															id="btnChangePinCode"
															class="btn btn-link btn-change-pincode no-pad">change</button>)
												</span>
											</div>
											<div class="ab-decor-amt mar-t-n-20__400 pull-right">
												<span class="transport-free">Free</span>
												<span id="transport-free-2" class="hide">Free*</span>
												<span class="hide"> <!-- @todo: remove it after solving trans issue -->
													<span id="transportRupeeFont" class='rupee-font'>&#8377;</span>
													<span id="transportAmount"></span>
												</span>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="ab-decor-price-element mar-t-5">
											<div class="ab-decor-price-title-1 pull-left">Total Booking Amount</div>
											<div class="ab-decor-amt pull-right">
												<span class='rupee-font'>&#8377;</span>
												<span id="totalBookingAmount"></span>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="ab-decor-price-element mar-t-5">
											<div class="ab-decor-price-title-2 pull-left">Advance To Pay</div>
											<div class="ab-decor-amt pull-right">
												<span class='rupee-font'>&#8377;</span>
												<span id="tokenAmount"></span>
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
									<div id="transportNotifyText" class="ab-decor-notice-text hide">
										* Transportation charges might be applicable based on the exact location. We will notify you accordingly.
									</div>
									<form id="decorAutoBookingForm" data-url="{{ route('auto-book.decor.pay.auto.init', [$mapId, $occasionId]) }}">
										<input type="hidden" id="forVenueDetails" class="forVenueDetails" value="{{ $data['mapTypeId'] }}">
										<input type="hidden" id="providerPincode" class="providerPincode" value="{{ $data['decor']['provider']->zip }}">
										<input type="hidden" id="distFree" class="distFree" value="{{ $data['decor']->kms_free }}">
										<input type="hidden" id="distMax" class="distMax" value="{{ $data['decor']->kms_max }}">
										<input type="hidden" id="failedToProceedData" name="failedToProceedData" value="{{ route('ajax.auto-book.failed-to-proceed', [$mapId, $mapTypeId, $occasionId]) }}">

										<div class="text-center">
											<button type="submit" id="btnDecorSubmit" class="btn btn-primary btn-pc-proceed ab-decor-btn-pc-proceed">PROCEED
											</button>
										</div>
										<div class="errors-msg pad-t-3 pad-b-4 hide text-center alert-danger"></div>
									</form>
								</div>
								<div id="ab-transport-fail" class="hide">
									<div class="mar-t-15">
										<div class="ab-decor-unavailable">
											<i class="material-icons valign-mid">&#xE5C9;</i>
											We regret to inform that this decoration is not available in your area
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</section>

@include("app.generic-modal-loader")