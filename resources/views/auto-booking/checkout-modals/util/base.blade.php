<section class="modal fade modal-pre-checkout" id="bookNowModal" tabindex="-1" role="dialog" aria-labelledby="bookNowModal" aria-hidden="true">
	<div class="modal-dialog no-pad">
		<div class="cross-button ga-btn-close-book-now-modal text-center" data-dismiss="modal">x</div>
		<div class="modal-content">
			<div class="modal-body no-pad">
				@yield('modal-body')
			</div>
		</div>
	</div>
</section>

@include("app.generic-modal-loader")