<div class="checkout-modal-hiw-section">
	<ul class="how-it-works-list">
		<li class="width-30">
			<div class="how-it-works-text">
				<b>Pay Advance</b>
			</div>
			<div class="how-it-works-content">
				Pay securely using your preferred payment mode for instant booking.
			</div>
		</li>
		<li class="how-it-works-arrow">
			<i class="material-icons">&#xE037;</i>
		</li>
		<li class="width-30">
			<div class="how-it-works-text">
				<b>Confirm Booking</b>
			</div>
			<div class="how-it-works-content">
				We will check the availability for your order and confirm your booking within 4 business hours.
			</div>
		</li>
		<li class="how-it-works-arrow">
			<i class="material-icons">&#xE037;</i>
		</li>
		<li class="width-30">
			<div class="how-it-works-text">
				<b>Relax & Enjoy</b>
			</div>
			<div class="how-it-works-content">
				Our event coordinator will ensure that all your order details are fulfilled.
			</div>
		</li>
	</ul>
</div>