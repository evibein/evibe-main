<div class="checkout-modal-hiw-section">
	<ul class="how-it-works-list">
		<li class="valign-mid width-10">
			<div class="how-it-works-image-wrap pc-evibe-shield-wrap">
				<img src="{{ config('evibe.gallery.host') }}/img/app/evibe_shield.png" alt="Evibe guarantee" class="how-it-works-image">
			</div>
		</li>
		<li class="valign-mid">
			<div class="how-it-works-text font-16">
				<b>100% Refund</b>
			</div>
			<div class="how-it-works-content">
				In case of non-availability of your order, we will initiate complete refund of the total advance paid within 2 business days.Otherwise, your total party is on us. No questions asked.
			</div>
		</li>
	</ul>
</div>