@extends('layout.base')

@section('page-title')
	<title>Evibe - Wallet</title>
@endsection

@section('page-meta-description')
	@yield("meta-description")
@endsection

@section('meta-keywords')
	@yield("meta-kw")
@endsection

@section('og-title')
	<meta property="og:title" content="Evibe - Wallet"/>
@endsection

@section('og-description')
	@yield('og-description')
@endsection

@section('og-url')
	<meta property="og:url" content="https://evibe.in/wallet"/>
@endsection

@section("header")
	@include("base.home.header.header-home-city")
	<div class="header-border"></div>
@endsection

@section("footer")
	@include("base.home.footer")
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/wallet.css') }}"/>
@endsection

@section("content")
	<div class="mar-10 max-width-500">
		<div style="border: 1px solid #DFE1E5; border-radius: 20px; padding: 10px;">
			<div class="text-center mar-t-5">
				<img src="{{ $galleryUrl }}/main/img/icons/friend-share-green.png" alt="evibe wallet share" width="75">
				<p class="font-16 mar-t-10 user-display-msg">
					{{ ucfirst($data["user"]->name) }} cares for your celebrations<br>
				</p>
				<p>
					{{ $data["user"]->name }} sent you <b>&#8377;{{ $data["walletShare"]->amount }}</b> Evibe Cash for you to have a fantastic party like they did with Evibe. So sweet!
				</p>
				<div class="btn btn-evibe friend-claim-reward">
					CLAIM MY GIFT
				</div>
			</div>
			<hr class="hr-text">
			<div class="text-muted font-12">
				What is Evibe.in?
				<ul class="font-12">
					<li style="line-height: 20px;">Evibe is your go-to platform to book trusted party services like decors, artists, candle-light dinner, cake-cutting places & more at best prices with a quality service guaranteed.</li>
				</ul>
				In which cities is Evibe available?
				<ul class="font-12">
					<li style="line-height: 20px;">You can currently use Evibe to host a party in Bangalore, Hyderabad, Mumbai, Delhi, Pune, Chennai</li>
				</ul>
			</div>
		</div>
	</div>
	@include("base.home.why-us-vertical")
	<input type="hidden" id="walletShareId" value="{{ $data["walletShare"]->url }}">
	<input type="hidden" id="walletShareTkn" value="{{ request("tkn") }}">
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			(function claimReward() {
				$(".friend-claim-reward").on("click", function () {
					showLoading();

					$.ajax({
						url: "/wallet/claim-my-reward/" + $("#walletShareId").val() + "?tkn=" + $("#walletShareTkn").val(),
						dataType: 'json',
						type: 'POST',
						data: {},
						success: function (data) {
							hideLoading();
							if (data.success) {
								window.location.href = data.url;
							} else if (data.loginError) {
								$('#loginModal').modal('show');
							} else {
								window.showNotyError(data.error);
							}
						},
						error: function (jqXHR, textStatus, errorThrown) {
							hideLoading();
							window.notifyTeam({
								"url": "/ajax/checkout/add-ons/" + $ticketId,
								"textStatus": textStatus,
								"errorThrown": errorThrown
							});
						}
					});
				});
			})();
		});
	</script>
@endsection