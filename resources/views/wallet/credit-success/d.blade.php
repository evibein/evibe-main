@extends('layout.base')

@section('page-title')
	<title>Evibe - Wallet</title>
@endsection

@section('page-meta-description')
	@yield("meta-description")
@endsection

@section('meta-keywords')
	@yield("meta-kw")
@endsection

@section('og-title')
	<meta property="og:title" content="Evibe - Wallet"/>
@endsection

@section('og-description')
	@yield('og-description')
@endsection

@section('og-url')
	<meta property="og:url" content="https://evibe.in/wallet"/>
@endsection

@section("header")
	@include("base.home.header.header-home-city")
	<div class="header-border"></div>
@endsection

@section("footer")
	@include("base.home.footer")
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/wallet.css') }}"/>
@endsection

@section("content")
	@if(request("redirct") != 1 && ($data["transaction"] != ""))
		<div class="alert alert-success text-center no-mar-b" style="border-radius: 0">&#8377;{{ $data["transaction"]->amount }} successfully credited to your Wallet</div>
	@endif
	<div class="w-1366">
		<div class="mar-t-20 mar-b-20 col-sm-6 col-sm-offset-3">
			<div style="border: 1px solid #DFE1E5; border-radius: 20px; text-align: center;">
				@if((request("redirct") != 1 && ($data["transaction"] == "")) || $data["wallet"]->balance == 0)
					<p class="mar-t-20 no-mar-b">Hello <b>{{ ucfirst($data["user"]->name) }}</b>, here is your wallet summary</p>
				@endif
				@php
					if($data["transaction"] != "")
					{$walletAmount = $data["transaction"]->claimed_on > 0 ? $data["wallet"]->balance : $data["transaction"]->amount;}
					else
					{$walletAmount = 0;}
				@endphp
				@if($walletAmount > 0)
					<img src="{{ $galleryUrl }}/main/img/gifs/success.gif" class="mar-t-25 mar-b-15" width="75px" alt="credit success">
					<div>
						<span class="font-26 text-bold">&#8377; {{ $walletAmount }}</span><br>{{ $data["transaction"]->transaction_name }}
						<div class="font-10 text-muted mar-t-10 mar-b-10">Expires on {{ date("d M Y, H:i A",$data["transaction"]->expires_on) }}</div>
					</div>
				@else
					<div class="mar-t-30">
						<span class="font-26 text-bold">&#8377; {{ $walletAmount }}</span><br>Evibe Cash
					</div>
				@endif
				<div class="clearfix"></div>
				<hr class="desktop-profile-hr">
				@php $cityUrl = !is_null(getCityUrl()) ? getCityUrl() : "bangalore"; @endphp
				<div class="text-center">
					<h6>Start Planning Now</h6>
					<div class="pad-b-20 pad-t-20">
						<div class="col-sm-4">
							<a href="{{ route('city.occasion.birthdays.home', $cityUrl) }}?utm_source=wallet&utm_campaign=wallet&utm_medium=wallet" class="a-no-decoration-black update-opacity">
								<img class="home-cat-img" src="{{ $galleryUrl }}/main/img/home/v1/icons/birthday.png" alt="Birthday">
								<h6 class="home-icon-title mar-t-10  no-mar-b">Birthday</h6>
							</a>
						</div>
						<div class="col-sm-4">
							<a href="{{ route("city.cld.list", $cityUrl) }}?utm_source=wallet&utm_campaign=wallet&utm_medium=wallet" class="a-no-decoration-black update-opacity">
								<img class="home-cat-img" src="{{ $galleryUrl }}/main/img/home/v1/icons/cld.png" alt="Candle light dinner">
								<h6 class="home-icon-title mar-t-10 no-mar-b">Candle Light Dinner</h6>
							</a>
						</div>
						<div class="col-sm-4">
							<a href="{{ route('city.occasion.surprises.package.list', $cityUrl) }}?utm_source=wallet&utm_campaign=wallet&utm_medium=wallet" class="a-no-decoration-black update-opacity">
								<img class="home-cat-img" src="{{ $galleryUrl }}/main/img/home/v1/icons/surprise.png" alt="Surprises">
								<h6 class="home-icon-title mar-t-10 no-mar-b">Surprises</h6>
							</a>
						</div>
						<div class="clearfix"></div>
						<div class="col-sm-4 mar-t-20">
							<a href="{{ route('city.occasion.house-warming.home', $cityUrl) }}?utm_source=wallet&utm_campaign=wallet&utm_medium=wallet" class="a-no-decoration-black update-opacity">
								<img class="home-cat-img" src="{{ $galleryUrl }}/main/img/home/v1/icons/house_warming.png" lt="House warming">
								<h6 class="home-icon-title mar-t-10 no-mar-b">House Warming</h6>
							</a>
						</div>
						<div class="col-sm-4 mar-t-20">
							<a href="{{ route('city.occasion.ncdecors.list', $cityUrl) }}?utm_source=wallet&utm_campaign=wallet&utm_medium=wallet" class="a-no-decoration-black update-opacity">
								<img class="home-cat-img" src="{{ $galleryUrl }}/main/img/home/v1/icons/naming-cermony.png" lt="Naming Ceremony">
								<h6 class="home-icon-title mar-t-10 no-mar-b">Naming Ceremony</h6>
							</a>
						</div>
						<div class="col-sm-4 mar-t-20">
							<a href="{{ route('city.occasion.bsdecors.list', $cityUrl) }}?utm_source=wallet&utm_campaign=wallet&utm_medium=wallet" class="a-no-decoration-black update-opacity">
								<img class="home-cat-img" src="{{ $galleryUrl }}/main/img/home/v1/icons/baby-shower.png" lt="Baby Shower">
								<h6 class="home-icon-title mar-t-10 no-mar-b">Baby Shower</h6>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		@if($walletAmount > 0)
			<div class="mar-b-20 col-sm-6 col-sm-offset-3" style="color: #3c3c3c;">
				<div style="border: 1px solid #DFE1E5; border-radius: 20px; text-align: center; overflow: hidden;" class="pos-rel">
					<div class="text-center pad-10">
						<h4 class="mar-t-15 mar-b-10">No Upcoming Parties?</h4>
						<h4 class="no-mar-t">Gift Evibe Cash to your friend!</h4>
						<p>Help Out Your Friends & Relatives</p>
						<p>Share this link with your friends & relatives, they will get <b>&#8377;{{ $walletAmount }}</b> off on their next booking.</p>
						<div class="mar-b-20 wallet-share-now-btn-wrap">
							<button class="btn btn-evibe wallet-share-now-btn">Gift Now</button>
						</div>
						<div class="mar-b-20 hide url-section">
							<div class="wallet-copy-btn-text in-blk init-width">
								http://evibe.in
							</div>
							<button class="wallet-copy-btn in-blk init-width">
								Copy Link
							</button>
							<div class="copy-notification mar-t-"></div>
						</div>
					</div>
				</div>
			</div>
		@endif
		<div class="clearfix"></div>
	</div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			(function getShareUrl() {
				$(".wallet-share-now-btn").on("click", function () {
					showLoading();

					$.ajax({
						url: "/wallet/share-url",
						dataType: 'json',
						type: 'POST',
						data: {},
						success: function (data) {
							hideLoading();
							if (data.success) {
								$(".wallet-share-now-btn-wrap").addClass("hide");
								$(".wallet-copy-btn-text").text(data.url);
								$(".url-section").removeClass("hide");
							} else {
								window.showNotyError(data.error);
							}
						},
						error: function (jqXHR, textStatus, errorThrown) {
							hideLoading();
							window.notifyTeam({
								"url": "/wallet/share-url",
								"textStatus": textStatus,
								"errorThrown": errorThrown
							});
						}
					});
				});
			})();

			$(".wallet-copy-btn").on("click", function () {
				let $temp = $("<input>");
				$("body").append($temp);
				$temp.val($(".wallet-copy-btn-text").text()).select();
				document.execCommand("copy");
				$temp.remove();

				let copyNotification = $("div.copy-notification");
				copyNotification.text("Copied!").fadeIn("slow", function () {
					setTimeout(function () {
						copyNotification.fadeOut("slow", function () {
							copyNotification.empty();
						});
					}, 2000);
				});
			});
		});
	</script>
@endsection