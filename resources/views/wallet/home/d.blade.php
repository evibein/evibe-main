@extends('layout.base')

@section('page-title')
	<title>Evibe - Wallet</title>
@endsection

@section('page-meta-description')
	@yield("meta-description")
@endsection

@section('meta-keywords')
	@yield("meta-kw")
@endsection

@section('og-title')
	<meta property="og:title" content="Evibe - Wallet"/>
@endsection

@section('og-description')
	@yield('og-description')
@endsection

@section('og-url')
	<meta property="og:url" content="https://evibe.in/wallet"/>
@endsection

@section("header")
	@include("base.home.header.header-home-city")
	<div class="header-border"></div>
@endsection

@section("footer")
	@include("base.home.footer")
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/wallet.css') }}"/>
@endsection

@section("content")
	<div class="w-1366">
		<div class="mar-t-20 mar-b-20 col-sm-6 col-sm-offset-3">
			<div style="border: 1px solid #DFE1E5; border-radius: 20px;">
				<div style="display: flex; justify-content: center; margin-top: 20px;">
					@if(is_null($data["user"]->name) || ($data["user"]->name == ""))
						<div class="user-profile-icon">E</div>
					@else
						<div class="user-profile-icon">{{ substr($data["user"]->name, 0, 1) }}</div>
					@endif
					<div class="mar-l-15 @if($data["wallet"]->balance == 0) pad-t-10 @endif">
						Available Balance<br>
						<span class="font-18 text-bold">&#8377; {{ $data["wallet"]->balance }}</span><br>
						@if($data["wallet"]->balance > 0)
							<span class="font-11 text-muted">Expires on {{ date("d M Y, H:i A",$data["wallet"]->expires_on) }}</span>
						@endif
					</div>
				</div>
				<hr class="hr-text">
				<div class="mar-l-20 mar-r-20">
					<div class="pull-left">Transactions</div>
					@if($data["transactionsCount"] > 3)
						<div class="pull-right text-e font-12">VIEW ALL</div>
					@endif
					<div class="clearfix"></div>
					<div class="mar-b-15">
						@if($data["transactionsCount"] > 0)
							@foreach($data["transactions"] as $transaction)
								<div style="display: flex; margin-top: 15px;">
									<div style="flex-grow: 1">
										<div class="user-profile-icon user-profile-icon-credit">{{ substr($transaction->transaction_name, 0, 1) }}</div>
									</div>
									<div style="flex-grow: 9" class="font-12">
										<div class="pull-left">
											<span class="text-bold">{{ $transaction->transaction_name }}</span><br>
											{{ date("d M Y, H:i A", strtotime($transaction->created_at)) }}
										</div>
										<div class="pull-right text-right">
											@if($transaction->type == "withdraw")
												- &#8377;{{ $transaction->amount }}<br>
												Paid
											@else
												+ &#8377;{{ $transaction->amount }}<br>
												Received
											@endif
										</div>
									</div>
								</div>
							@endforeach
						@else
							<div class="text-center">
								<div class="pad-l-15 pad-r-15 mar-t-15 text-muted font-13">
									No Transactions found<br>Continue shopping to earn more
								</div>
								<div class="btn btn-evibe btn-xs mar-t-10">Shop Now</div>
								<div class="clearfix"></div>
							</div>
						@endif
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		@if($data["wallet"]->balance > 0)
			<div class="mar-b-20 col-sm-6 col-sm-offset-3" style="color: #3c3c3c;">
				<div style="border: 1px solid #DFE1E5; border-radius: 20px; text-align: center; overflow: hidden;" class="pos-rel">
					<div class="text-muted font-12 pad-t-10">Updated Wallet Balance: &#8377; {{ $data["wallet"]->balance }}</div>
					<div class="text-center">
						<h4 class="mar-t-15 mar-b-5">No Upcoming Parties?</h4>
						<h4 class="no-mar-t">No Worries!</h4>
						<p>Help Out Your Friends & Relatives</p>
						<p>Share this link with your friends & relatives, they will get <b>&#8377;{{ $data["wallet"]->balance }}</b> off on their next booking.</p>
						<div class="mar-b-20 wallet-share-now-btn-wrap">
							<button class="btn btn-evibe wallet-share-now-btn">Share Now</button>
						</div>
						<div class="mar-b-20 hide url-section">
							<div class="wallet-copy-btn-text in-blk init-width">
								http://evibe.in
							</div>
							<button class="wallet-copy-btn in-blk init-width">
								Copy Link
							</button>
							<div class="copy-notification mar-t-"></div>
						</div>
					</div>
				</div>
			</div>
		@endif
		<div class="clearfix"></div>
	</div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			(function getShareUrl() {
				$(".wallet-share-now-btn").on("click", function () {
					showLoading();

					$.ajax({
						url: "/wallet/share-url",
						dataType: 'json',
						type: 'POST',
						data: {},
						success: function (data) {
							hideLoading();
							if (data.success) {
								$(".wallet-share-now-btn-wrap").addClass("hide");
								$(".wallet-copy-btn-text").text(data.url);
								$(".url-section").removeClass("hide");
							} else {
								window.showNotyError(data.error);
							}
						},
						error: function (jqXHR, textStatus, errorThrown) {
							hideLoading();
							window.notifyTeam({
								"url": "/wallet/share-url",
								"textStatus": textStatus,
								"errorThrown": errorThrown
							});
						}
					});
				});
			})();

			$(".wallet-copy-btn").on("click", function () {
				let $temp = $("<input>");
				$("body").append($temp);
				$temp.val($(".wallet-copy-btn-text").text()).select();
				document.execCommand("copy");
				$temp.remove();

				let copyNotification = $("div.copy-notification");
				copyNotification.text("Copied!").fadeIn("slow", function () {
					setTimeout(function () {
						copyNotification.fadeOut("slow", function () {
							copyNotification.empty();
						});
					}, 2000);
				});
			});
		});
	</script>
@endsection