<div id="modalOfferLandingSignUp" class="modal fade no-pad-r-imp" role="dialog" tabindex="-1">
	<div class="modal-dialog modal-md">
		<div class="modal-content" style="background-color: #262c3b">
			<div class="col-xs-12 no-pad text-center">
				<div>
					@if ($agent->isMobile() && !($agent->isTablet()))
						<div>
							<div style="color: #ed3e72; margin-top: 30px;">@if($data["offer"]->offer_title != ""){{ $data["offer"]->offer_title }}@else{{ "ENTER YOUR DETAILS AND GET" }}@endif</div>
							<div style="font-size: 50px; margin-top: 20px; color: #ffffff; height: 45px; line-height: 50px">
								<img style="height: 40px; margin: 5px 10px 0 0; vertical-align: top;" src="{{ config("evibe.gallery.host") }}/img/icons/gift-box-white.png">&#8377;{{ $data["offer"]->offer_amount }} off
							</div>
							<div style="color: #ffffff; padding-bottom: 20px">On your first order</div>
							@if($data["offer"]->offer_min_booking_amount > 0)
								<div class="text-muted">ON AN ORDER OF &#8377; {{ $data["offer"]->offer_min_booking_amount }} OR MORE</div>
							@endif
						</div>
						<div style="margin-top: 40px;">
							<div class="col-xs-8 col-xs-offset-2 mar-b-10">
								<input type="text" placeholder="Enter your phone number" class="offer-landing-signup-phone" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px">
								@if($data["occasionId"] == config("evibe.occasion.surprises.id") || (isset($data['isPiab']) && $data['isPiab']))
									<input type="text" @if(isset($data['isPiab']) && $data['isPiab']) placeholder="Your party date" @else placeholder="Your special date" @endif class="offer-landing-party-date" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px">
								@endif
							</div>
							<div class="clearfix"></div>
							<a class="btn btn-evibe pad-t-10 pad-b-10 mar-b-20 offer-signup-submit-btn" data-url="{{ route("of.landing.submit") }}">
								GET MY &#8377; {{ $data["offer"]->offer_amount }} OFF
							</a>
							<a class="btn btn-evibe pad-t-10 pad-b-10 mar-b-20 submitting-offer-signup hide">SUBMITTING...</a>
						</div>
					@else
						<div>
							<div style="color: #ed3e72; margin-top: 40px; padding-top: 45px">@if($data["offer"]->offer_title != ""){{ $data["offer"]->offer_title }}@else{{ "ENTER YOUR DETAILS AND GET" }}@endif</div>
							<div style="font-size: 80px; margin-top: 10px; color: #ffffff; height: 75px; line-height: 80px">
								<img style="height: 65px; margin: 10px 20px 0 0; vertical-align: top;" src="{{ config("evibe.gallery.host") }}/img/icons/gift-box-white.png">&#8377;{{ $data["offer"]->offer_amount }} off
							</div>
							<div style="color: #ffffff; padding-bottom: 10px">On your first order</div>
							@if($data["offer"]->offer_min_booking_amount > 0)
								<div class="text-muted">ON AN ORDER OF &#8377; {{ $data["offer"]->offer_min_booking_amount }} OR MORE</div>
							@endif
						</div>
						<div style="margin-top: 40px;">
							<div class="col-xs-4 mar-b-10 @if($data["occasionId"] == config("evibe.occasion.surprises.id")){{ "col-xs-offset-2" }}@else{{ "col-xs-offset-4" }}@endif">
								<input type="text" placeholder="Enter your phone number" class="offer-landing-signup-phone" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px">
							</div>
							@if($data["occasionId"] == config("evibe.occasion.surprises.id") || (isset($data['isPiab']) && $data['isPiab']))
								<div class="col-xs-4 mar-b-10 no-pad-l">
									<input type="text" @if(isset($data['isPiab']) && $data['isPiab']) placeholder="Your party date" @else placeholder="Your special date" @endif class="offer-landing-party-date" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px">
								</div>
							@endif
							<div class="clearfix"></div>
							<div class="col-sm-4 col-xs-offset-4 col-xs-offset-4">
								<a class="btn btn-evibe full-width pad-t-10 pad-b-10 offer-signup-submit-btn" data-url="{{ route("of.landing.submit") }}">
									GET MY &#8377; {{ $data["offer"]->offer_amount }} OFF
								</a>
								<a class="btn btn-evibe full-width pad-t-10 pad-b-10 submitting-offer-signup hide">SUBMITTING...</a>
							</div>
							<div class="clearfix"></div>
						</div>
					@endif
					<div>
						<div class="text-muted text-underline mar-t-15 cur-point offer-signup-close-btn in-blk" data-dismiss="modal">No Thanks, I don't need this discount</div>
					</div>
					<div>
						<h6 style="margin: 25px 0; color: #ffffff;">
							<img src="{{ $galleryUrl }}/main/img/icons/trust_w.png" height="35px" width="35px">
							<span class="pad-l-5">TRUSTED BY {{ config("evibe.stats.parties_done") }} CUSTOMERS</span>
						</h6>
					</div>
					<div>
						<div class="pull-right font-12 text-muted pad-r-10 pad-b-5">*coupon code will be sent to your phone number</div>
					</div>
				</div>
			</div>
			<div class="offer-signup-thankyou text-center hide">
				<div>
					<div style="font-size: 40px; margin-top: 30px; color: #ffffff; height: 35px; line-height: 40px">THANK YOU</div>
					<div style="color: #ffffff; margin-top: 25px;">
						<div>Here is you coupon code</div>
						<div style="margin-top: 10px; font-size:25px; padding: 10px; border: 1px dotted; display: inline-block;" class="offer-landing-coupon-code"></div>
					</div>
					<div style="color: #ffffff; margin: 25px 0;">*Your coupon code has been sent to your phone number</div>
				</div>
				<div>
					<div style="color: #ffffff; margin-bottom: 25px; font-size: 16px;" class="text-underline cur-point in-blk" data-dismiss="modal">CONTINUE PLANNING</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>