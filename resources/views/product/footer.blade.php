@if(isset($data['customerReviews']) && count($data['customerReviews']))
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mar-b-60">
		<div class="footer-component pad-l-15 pad-r-15">
			<div class="footer-piab-reviews-title text-bold font-18">
				Customer Stories
			</div>
			<div class="footer-piab-reviews-wrap mar-t-5">
				@if(isset($data['customerReviews']))
					@if ($agent->isMobile() && !($agent->isTablet()))
						<div id="slickCarouser_customerReviews">
						@foreach($data['customerReviews'] as $customerReview)
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mar-t-10 pad-l-5 pad-r-5">
								<div class="footer-piab-review-wrap hide">
									<div class="pull-left mar-r-15">
										<div class="footer-piab-review-photo-wrap">
											<img src="{{ config("evibe.gallery.host") }}/main/customer/reviews/{{ $customerReview['photo'] }}" class="footer-piab-review-photo" alt="Evibe.in Surprises Review by {{ $customerReview['name']  }}">
										</div>
									</div>
									<div>
										<div class="">
											<b>{{ $customerReview['name'] }}</b>
										</div>
									</div>
									<div class="mar-t-10 footer-piab-review-text-wrap font-13">
										{{ $customerReview['review'] }}
									</div>
								</div>
							</div>
						@endforeach
						</div>
					@else
						@foreach($data['customerReviews'] as $customerReview)
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mar-t-10 pad-l-5 pad-r-5">
								<div class="footer-piab-review-wrap">
									<div class="pull-left mar-r-15">
										<div class="footer-piab-review-photo-wrap">
											<img src="{{ config("evibe.gallery.host") }}/main/customer/reviews/{{ $customerReview['photo'] }}" class="footer-piab-review-photo" alt="Evibe.in Surprises Review by {{ $customerReview['name']  }}">
										</div>
									</div>
									<div>
										<div class="">
											<b>{{ $customerReview['name'] }}</b>
										</div>
									</div>
									<div class="mar-t-10 footer-piab-review-text-wrap font-13">
										<div>{{ $customerReview['review'] }}</div>
									</div>
								</div>
							</div>
						@endforeach
					@endif
				@endif
				<div class="clearfix"></div>
			</div>
			<div class="footer-piab-reviews-wrap-loading hide">
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
@endif

@if(isset($data['productCategories']) && count($data['productCategories']))
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="footer-component">
			<div class="mar-l-15 font-18 text-bold">Other Categories</div>
			<div class="mar-t-15">
			@php $i = 0; @endphp
			@foreach($data['productCategories'] as $productCategory)
				@if(isset($data['partyKitData']['categoryId']) && $data['partyKitData']['categoryId'] && ($data['partyKitData']['categoryId'] == $productCategory['id']))
					<!-- Do not show already existing product category -->
					@else
						@php $i++; @endphp
						<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
							<div class="footer-piab-option-wrap">
								<a class="footer-piab-option-link @if(!$productCategory['isLive']) footer-piab-option-link-disabled @endif" href="{{ route('party-kits.list', $productCategory['url']) }}?ref=footer#card">
									@if(isset($productCategory['imgFullUrl']) && $productCategory['imgFullUrl'])
										<img class="" src="{{ $productCategory['imgFullUrl'] }}">
									@else
										<img class="" src="{{ $galleryUrl }}/img/icons/default-piab.jpg">
									@endif
									<div class="footer-piab-option-bg"></div>
									<div class="footer-piab-option-title">
										{{ $productCategory['name'] }}
									</div>
								</a>
							</div>
						</div>
						@if ($agent->isMobile() && !($agent->isTablet()))
							@if(($i % 2) == 0)
								<div class="clearfix"></div>
							@endif
						@else
							@if(($i % 4) == 0)
								<div class="clearfix"></div>
							@endif
						@endif
					@endif
				@endforeach
				@if ($agent->isMobile() && !($agent->isTablet()))
					@if(($i % 2) != 0)
						<div class="clearfix"></div>
					@endif
				@else
					@if(($i % 4) != 0)
						<div class="clearfix"></div>
					@endif
				@endif
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
@endif