@extends('product.base')

@section("header")
	@include('product.header')
@endsection

@section('push-notifications')
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-profile.css') }}"/>
@endsection

@section("modals")
	@parent
	<div class="modal fade modal-piab-feedback" tabindex="-1" role="dialog" id="modalPIABFeedbackForm">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div class="modal-inner-header">
						<button type="button" class="close close-reset-form ga-btn-close-enquire-now-modal" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title text-center">Post Your Questions / Feedback</h4>
					</div>
					<div class="modal-piab-feedback-form">
						<form id="piabFeedbackForm" data-url="{{ route('party-kits.feedback') }}">
							<div class="error-message-wrap  text-center text-danger hide pad-t-10">
								<span class="error-message">Please fill all the required details.</span>
							</div>
							<div class="form-group pad-t-20">
								<div class="col-lg-12 col-md-12 col-lg-12 col-sm-12">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
										<textarea class="mdl-textfield__input" name="feedbackComment" id="feedbackComment"></textarea>
										<label class="mdl-textfield__label" for="feedbackComment">Your question / feedback</label>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="form-group">
								<div class="col-md-12 col-lg-12 col-sm-12">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
										<input class="mdl-textfield__input ticket_modal_elements" type="text" name="feedbackName" id="feedbackName" value=""/>
										<label class="mdl-textfield__label bottom-5-inverse" for="feedbackName">Name</label>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="form-group">
								<div class="col-md-6 col-lg-6 col-sm-12">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ticket_modal_elements">
										<input class="mdl-textfield__input" type="text" name="feedbackPhone" id="feedbackPhone" value=""/>
										<label class="mdl-textfield__label bottom-5-inverse" for="feedbackPhone">Phone Number</label>
									</div>
								</div>
								<div class="col-md-6 col-lg-6 col-sm-12">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ticket_modal_elements">
										<input class="mdl-textfield__input email-typo-error" type="text" name="feedbackEmail" id="feedbackEmail" value=""/>
										<label class="mdl-textfield__label bottom-5-inverse" for="feedbackEmail">Email</label>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="form-group">
								<div class="col-sm-12 text-center">
									<button class="btn btn-link mar-r-5 close-reset-form ga-btn-close-enquire-now-modal" data-dismiss="modal">
										<i class="glyphicon glyphicon-remove"></i> <em>Cancel</em>
									</button>
									<button id="btnPIABFeedbackSubmit" type="submit" class="btn btn-primary btn-submit mar-l-5">
										SUBMIT
									</button>
								</div>
								<div class="clearfix"></div>
							</div>
						</form>
					</div>
					<div class="modal-piab-feedback-ty text-g text-center font-18 pad-t-10 pad-b-10 hide">
						Thank you for sharing with us
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			setTimeout(function loadFotoramaForPackageProfile() {
				var fotoramaSection = $("#fotorama");
				if (fotoramaSection.length > 0) {
					fotoramaSection.fotorama();

					removeGalleryLoadingAnimation();
				} else {
					removeGalleryLoadingAnimation();
				}

				$('.lazy-loading').each(function () {
					var img = $(this);
					img.attr('src', img.data('src'));
				});
			}, 500);

			function removeGalleryLoadingAnimation() {
				$('.loading-img-wrap').remove();
				$('.gallery-wrap').removeClass('hide');
			}

			var $productStyleId = $('#hidProductStyleId').val();
			var $productStylePrice = $('#hidProductStylePrice').val();

			/* Set delivery pin code check box width */
			var inputWrapWidth = 0;
			inputWrapWidth += $('.profile-piab-del-icon').outerWidth();
			inputWrapWidth += $('.profile-piab-del-input').outerWidth();
			inputWrapWidth += $('.piab-del-check-btn').outerWidth();
			inputWrapWidth += 12; /* just in case */
			$('.profile-piab-del-input-box').css('width', inputWrapWidth);
			$('.profile-piab-del-input-box').css('margin', 'auto');

			/* Buy Now btn */
			$('#piabBuyBtn').on('click', function (event) {
				event.preventDefault();
				showProductCheckout();
			});

			$('.piab-feedback-btn').on('click', function (event) {
				event.preventDefault();

				$('.modal-piab-feedback-ty').addClass('hide');
				$('.modal-piab-feedback-form').removeClass('hide');
				$('#modalPIABFeedbackForm').modal('show');
			});

			$('#btnPIABFeedbackSubmit').on('click', function (event) {
				event.preventDefault();

				$('.error-message-wrap').addClass('hide');

				$.ajax({
					url: $('#piabFeedbackForm').data('url'),
					type: "POST",
					dataType: "JSON",
					data: {
						'feedbackComment': $('#feedbackComment').val(),
						'name': $('#feedbackName').val(),
						'email': $('#feedbackEmail').val(),
						'phone': $('#feedbackPhone').val(),
						'productStyleId': $productStyleId
					},
					success: function (data) {
						if (data.success) {
							$('.modal-piab-feedback-ty').removeClass('hide');
							$('.modal-piab-feedback-form').addClass('hide');
						} else {
							var $errorMsg = "Some error occurred while submitting your data. Kindly refresht he page and try again.";
							if (data.errorMsg) {
								$errorMsg = data.errorMsg;
							}
							$('.error-message').html($errorMsg);
							$('.error-message-wrap').removeClass('hide');
						}
					},
					error: function () {
						var $errorMsg = "Some error occurred while submitting your data. Kindly refresht he page and try again.";
						$('.error-message').html($errorMsg);
						$('.error-message-wrap').removeClass('hide');
					}
				});
			});

			$('.piab-del-check-btn').on('click', function () {
				$('.profile-piab-del-error').addClass('hide');
				$('.profile-piab-del-success').addClass('hide');
				$('.profile-piab-del-text').addClass('hide');
				$('#delCheckLocationPin').data('is-available', 0);

				var $delPin = $('#delCheckLocationPin').val();

				if ($delPin.length != 6) {
					$('.profile-piab-del-error').html("Kindly enter a valid pincode");
					$('.profile-piab-del-error').removeClass('hide');
				} else {
					var $ajaxUrl = $(this).data('url');

					$('.profile-piab-del-loader').removeClass('hide');
					$('.profile-piab-del-wrap').addClass('hide');

					$.ajax({
						type: 'POST',
						dataType: 'JSON',
						url: $ajaxUrl,
						data: {
							'deliveryPincode': $delPin
						},
						success: function (data) {
							if (data.success) {
								if (data.deliveryDateText) {
									$('.profile-piab-del-success-date').text(data.deliveryDateText);
								}
								$('#delCheckLocationPin').data('is-available', 1);
								$('.profile-piab-del-pin').text($delPin);
								$('.profile-piab-del-success').removeClass('hide');

								$('.profile-piab-del-wrap').removeClass('hide');
								$('.profile-piab-del-loader').addClass('hide');
							} else {
								var $errorMsg = "Some error occurred, please refresh the page and try again";
								if (data.errorMsg) {
									$errorMsg = data.errorMsg;
								}
								$('.profile-piab-del-error').html($errorMsg);
								$('.profile-piab-del-error').removeClass('hide');

								$('.profile-piab-del-wrap').removeClass('hide');
								$('.profile-piab-del-loader').addClass('hide');
							}
						},
						error: function (jqXHR, textStatus, errorThrown) {
							var $errorMsg = "Some error occurred, please refresh the page and try again";
							$('.profile-piab-del-error').html($errorMsg);
							$('.profile-piab-del-error').removeClass('hide');

							$('.profile-piab-del-wrap').removeClass('hide');
							$('.profile-piab-del-loader').addClass('hide');

							window.notifyTeam({
									"url": $ajaxUrl,
									"textStatus": textStatus,
									"errorThrown": errorThrown
								}
							);
						}
					});
				}

			});

			function showProductCheckout() {
				if (!$productStyleId || !$productStylePrice) {
					window.showNotyError("Some error occurred while fetching data to buy the product. Kindly refresh the page and try again.");
					return false;
				}

				var acceptedPinCode = null;
				if ($('#delCheckLocationPin').data('is-available')) {
					acceptedPinCode = $('#delCheckLocationPin').val();
				}

				var $ajaxUrl = "/party-kits/buy/init/" + $productStyleId;

				$.ajax({
					type: 'POST',
					dataType: 'JSON',
					url: $ajaxUrl,
					data: {
						'productStylePrice': $productStylePrice,
						'delPinCode': acceptedPinCode
					},
					success: function (data) {
						if (data.success && data.redirectUrl) {
							location.href = data.redirectUrl;
						} else {
							var $errorMsg = "Some error occurred, please refresh the page and try again";
							if (data.errorMsg) {
								$errorMsg = data.errorMsg;
							}
							window.showNotyError($errorMsg);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						window.showNotyError("Some error occurred, please refresh the page and try again");
						window.notifyTeam({
								"url": $ajaxUrl,
								"textStatus": textStatus,
								"errorThrown": errorThrown
							}
						);
					}
				});
			}

			function priceFormat($num) {
				$num = $num.toString();
				var lastThree = $num.substring($num.length - 3);
				var otherNumbers = $num.substring(0, $num.length - 3);
				if (otherNumbers != '')
					lastThree = ',' + lastThree;
				var $res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

				return $res;
			}
		});
	</script>
@endsection

@section("content")
	@parent
	<div class="profile-piab-container">
		@if(isset($data['partyKitData']) && $data['partyKitData'])
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hide">
				<div class="profile-piab-breadcrumbs mar-t-5">
					<ol class="breadcrumb profile-piab-breadcrumb no-pad-l">
						<li>
							<a href="{{ route('party-kits.home') }}">
								<span class="glyphicon glyphicon-arrow-left"></span>
								<span>View All Party Kits</span>
							</a>
						</li>
					</ol>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
				<div class="profile-piab-gallery mar-t-15">
					@yield('profile-gallery')
				</div>
				@if ($agent->isMobile() && !($agent->isTablet()))
					@include('product.profile.util.title-section')
					@include('product.profile.util.price-card')
				@else
				@endif
				@if(isset($data['partyKitData']) && $data['partyKitData'])
					<div class="profile-piab-info-wrap mar-t-30">
						<div class="profile-piab-inclusions-wrap mar-t-15">
							@if((isset($data['partyKitData']['info']) && $data['partyKitData']['info']) || ((isset($data['partyKitData']['inclusions'])) && ($data['partyKitData']['inclusions'])) )
								<div class="profile-piab-info-card profile-piab-style-inclusions" data-product-style-id="{{ $data['partyKitData']['id'] }}">
									<div class="profile-piab-info-card-title">
										What Is Included?
									</div>
									<div class="profile-piab-info-card-text mar-t-5">
										@if(isset($data['partyKitData']['info']) && $data['partyKitData']['info'])
											<div class="mar-t-10">
												{{ $data['partyKitData']['info'] }}
											</div>
										@endif
										@if(isset($data['partyKitData']['inclusions']) && $data['partyKitData']['inclusions'])
											<div class="mar-t-10">
												{!! $data['partyKitData']['inclusions'] !!}
											</div>
										@endif
									</div>
								</div>
							@endif
						</div>
						<div class="profile-piab-exclusions-wrap mar-t-15">
							@if(isset($data['partyKitData']['exclusions']) && $data['partyKitData']['exclusions'])
								<div class="profile-piab-info-card profile-piab-style-exclusions" data-product-style-id="{{ $data['partyKitData']['id'] }}">
									<div class="profile-piab-info-card-title">
										Exclusions
									</div>
									<div class="profile-piab-info-card-text mar-t-5">
										{!! $data['partyKitData']['exclusions'] !!}
									</div>
								</div>
							@endif
						</div>
					</div>
				@endif
			</div>
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				@if ($agent->isMobile() && !($agent->isTablet()))
				@else
					@include('product.profile.util.title-section')
					@include('product.profile.util.price-card')
				@endif
			</div>
			<div class="clearfix"></div>
			<div class="hide">
				<input type="hidden" id="hidProductTypeId" value="{{ $data['partyKitData']['productType']['id'] }}">
				<input type="hidden" id="hidProductStyleId" value="{{ $data['partyKitData']['id'] }}">
				<input type="hidden" id="hidProductStylePrice" value="{{ $data['partyKitData']['price'] }}">
			</div>
		@else
			<div class="text-center font-24 mar-t-30 text-r">Uhu! Somethings not right. Kindly refresh the page and try again</div>
		@endif
	</div>
	@if(isset($data['similarPartyKits']) && count($data['similarPartyKits']))
		<div class="similar-piab-kits-container">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="similar-piab-styles-title font-18 text-bold">
					Similar {{ $data['productCategoryName'] }} Party Kits
				</div>
				<div class="similar-piab-styles-wrap hide mar-t-10 pad-l-15 pad-r-15">
					<div id="slickCarouser_similarPartyKits">
						@foreach($data['similarPartyKits'] as $partyKit)
							<div class="similar-piab-style-wrap text-center">
								<a class="similar-piab-style-link" href="{{ route('party-kits.profile', ['categoryUrl' => $partyKit['productCategory']['url'], 'typeUrl' => $partyKit['productType']['url'], 'styleUrl' => $partyKit['url']]) }}?ref=pp#similarKits">
									<div class="similar-piab-style-img-wrap">
										@if(isset($partyKit['profileImgFullUrl']) && $partyKit['profileImgFullUrl'])
											<img class="similar-piab-style-img" src="{{ $partyKit['profileImgFullUrl'] }}">
										@else
											<img class="similar-piab-style-img" src="{{ $galleryUrl }}/img/icons/default-piab.jpg">
										@endif
									</div>
									<div class="similar-piab-style-title mar-t-10">
										{{ $partyKit['productType']['name'] }} {{ $partyKit['name'] }} Theme
									</div>
									<div class="similar-piab-style-price-wrap mar-t-5">
										<div class="similar-piab-style-price-worth in-blk">
											@price($partyKit['priceWorth'])
										</div>
										<div class="similar-piab-style-price in-blk mar-l-5">
											@price($partyKit['price'])
										</div>
									</div>
								</a>
							</div>
						@endforeach
					</div>
				</div>
				<div class="similar-piab-styles-wrap-loading">

				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	@endif
@endsection

@section("footer")
	@include('product.footer')
	<div class="footer-wrap">
		@include('base.home.footer.footer-non-city')
	</div>
@endsection