<div class="profile-piab-title text-center">
	<span>{{ $data['partyKitData']['productCategory']['name'] }} {{ $data['partyKitData']['productType']['name'] }} {{ $data['partyKitData']['name'] }} Theme</span>
	<span class="profile-piab-title-style-tag"></span>
</div>
@if(isset($data['similarTypePartyKits']) && count($data['similarTypePartyKits']))
	<div class="profile-piab-style-selection-wrap mar-t-15 mar-b-5">
		<div class="profile-piab-style-selection-title in-blk valign-mid">
			Other Color Variations:
		</div>
		<div class="profile-piab-style-selection-colours mar-t-10 valign-mid">
			@foreach($data['similarTypePartyKits'] as $similarTypePartyKit)
				<div class="profile-piab-style-selection-style-wrap in-blk">
					<div class="profile-piab-style-selection-img-wrap text-center">
						<a href="{{ route('party-kits.profile', ['categoryUrl' => $similarTypePartyKit['productCategory']['url'], 'typeUrl' => $similarTypePartyKit['productType']['url'], 'styleUrl' => $similarTypePartyKit['url']]) }}" class="">
							@if(isset($similarTypePartyKit['profileThumbImgFullUrl']) && $similarTypePartyKit['profileThumbImgFullUrl'])
								<img class="profile-piab-style-selection-img" src="{{ $similarTypePartyKit['profileThumbImgFullUrl'] }}">
							@else
								<img class="profile-piab-style-selection-img" src="{{ $galleryUrl }}/img/icons/default-piab.jpg">
							@endif
						</a>
					</div>
					<div class="profile-piab-style-selection-style-title text-center">
						{{ $similarTypePartyKit['name'] }}
					</div>
				</div>
			@endforeach
			<div class="clearfix"></div>
		</div>
	</div>
@endif