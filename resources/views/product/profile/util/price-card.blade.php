<div class="profile-piab-price-card-wrap">
	<div class="profile-piab-price-card">
		<div class="text-center">
			@if(isset($data['partyKitData']['priceWorth']) && $data['partyKitData']['priceWorth'])
				<div class="mar-t-5">
					<div class="profile-piab-price-worth in-blk">
						<span class="rupee-font">&#8377;</span>
						<span class="price">@amount($data['partyKitData']['priceWorth'])</span>
					</div>
				</div>
			@endif
			@if(isset($data['partyKitData']['price']) && $data['partyKitData']['price'])
				<div class="mar-t-5">
					<div class="profile-piab-price in-blk">
						<span class="rupee-font">&#8377;</span>
						<span class="price">@amount($data['partyKitData']['price'])</span>
					</div>
					<div class="profile-piab-price-tag mar-l-5 valign-top in-blk">
						<i class="glyphicon glyphicon-thumbs-up"></i> Best Price
					</div>
				</div>
			@endif
			<div class="mar-t-10 hide">
				<div class="profile-piab-price-delivery text-center">
					<div>
						Standard delivery by <b>{{ date('jS M Y', (time() + (3 * 24 * 60 * 60))) }}</b>
					</div>
				</div>
			</div>
		</div>
		<div class="profile-piab-del-wrap mar-t-30">
			<div class="text-center profile-piab-del-input-box">
				<div class="profile-piab-del-input-title text-left">
					<span class="glyphicon glyphicon-map-marker"></span><span>Deliver To</span>
				</div>
				<div class="mar-t-5 text-center profile-piab-del-input-wrap">
					<input type="number" name="delCheckLocationPin" id="delCheckLocationPin" data-is-available="0" class="profile-piab-del-input" placeholder="Enter Pincode">
					<span class="btn-piab-del-check piab-del-check-btn" data-url="{{ route('party-kits.delivery.check') }}">
						<span class="btn-piab-del-check-text">Check</span>
					</span>
				</div>
			</div>
			<div class="text-center mar-t-10">
				<div class="profile-piab-del-text">
					Standard delivery by <b>{{ date('jS M Y', (time() + (3 * 24 * 60 * 60))) }}</b>
				</div>
				<div class="profile-piab-del-error hide"></div>
				<div class="profile-piab-del-success hide">
					<span>Delivery by <span class="text-bold profile-piab-del-success-date">{{ date('jS M Y', (time() + (3 * 24 * 60 * 60))) }}</span></span>
					<span class="mar-l-4">for the location</span>
					<span class="mar-l-4 profile-piab-del-pin"></span>
					<span class="profile-piab-del-success-line hide"></span>
					<span class="profile-piab-del-success-free hide">FREE</span>
				</div>
			</div>
			<div class="profile-piab-del-options-wrap">

			</div>
		</div>
		<div class="profile-piab-del-loader mar-t-30 text-center hide">
			<img src="{{ $galleryUrl }}/img/gifs/filter-loading.gif" alt="">
		</div>
		<div class="mar-t-30">
			<div id="piabBuyBtn" class="btn btn-primary btn-piab-buy-now">
				<span><i class="material-icons valign-mid">&#xE8CC;</i></span>
				<span class="mar-l-5 valign-mid">Buy Now</span>
			</div>
		</div>
		<hr class="profile-piab-hr">
		<div class="mar-t-20">
			<div class="profile-piab-coupon-offer hide">
				<div class="profile-piab-offer-title valign-bottom text-i">
					<span><img class="profile-piab-offer-icon" src="{{ $galleryUrl }}/img/icons/discount.png"></span>
					<span class="text-bold mar-l-5">Offer</span>
				</div>
				<div class="profile-piab-offer-content mar-t-5">
					<span>Apply coupon code</span>
					<span><b> EVBFREESHIP </b></span>
					<span>at checkout for FREE shipping.</span>
				</div>
			</div>
			@include("product.usp", ["isProfile" => 1])
		</div>
		<hr class="profile-piab-hr">
		<div class="mar-t-10 text-center">
			<div class="profile-piab-feedback-text">
				Have questions or feedback?
			</div>
			<div class="btn btn-default btn-piab-feedback piab-feedback-btn mar-t-10">
				<span class="glyphicon glyphicon-comment mar-r-2"></span> Ask Now
			</div>
		</div>
	</div>
</div>