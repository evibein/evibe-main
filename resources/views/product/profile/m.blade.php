@extends('product.profile.c')

@section('profile-gallery')
	@if (isset($data['partyKitData']['gallery']) && count($data['partyKitData']['gallery']))
		<div class="profile-piab-style-gallery" data-product-style-id="{{ $data['partyKitData']['id'] }}">
			<div class="loading-img-wrap">
				<img src="/images/loading.gif" alt="">
				<div class="pad-t-10">loading gallery...</div>
			</div>
			<div class="gallery-wrap mobile-gallery-wrap no-mar hide">
				<div class="fotorama" data-width="100%" data-ratio="900/600" data-maxheight="400" data-autoplay="true" data-thumbwidth="80" data-thumbmargin="10" data-keyboard="true">
					@foreach ($data['partyKitData']['gallery'] as $gallery)
						<a href="{{ $gallery['mobileUrl'] }}">
							<img src="{{ $gallery['thumb'] }}" alt="{{ $gallery['title'] }}"/>
						</a>
					@endforeach
					<a href="{{ $galleryUrl }}/main/img/explainer-image_profile.png">
						<img src="{{ $galleryUrl }}/main/img/explainer-image_thumb.png" alt="Lifelong Memories Guaranteed"/>
					</a>
				</div>
			</div>
		</div>
	@else
		<div><img class="des-profile-no-image" src="{{ $galleryUrl }}/img/icons/balloons.png"></div>
	@endif
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			var slickJs = "<?php echo elixir('js/util/slick.js'); ?>";
			$.getScript(slickJs, function () {

				$('#slickCarouser_similarPartyKits').slick({
					dots: false,
					infinite: false,
					speed: 300,
					slidesToShow: 1,
					centerMode: true,
					variableWidth: false,
					arrows: true
				});

				$('.similar-piab-styles-wrap').removeClass('hide');
				$('.similar-piab-styles-wrap-loading').addClass('hide');

				$('#slickCarouser_customerReviews').slick({
					dots: false,
					infinite: false,
					speed: 300,
					slidesToShow: 1,
					arrows: true
				});

				$('.footer-piab-review-wrap').removeClass('hide');

				/*
				$('.footer-piab-reviews-wrap').removeClass('hide');
				$('.footer-piab-reviews-wrap-loading').addClass('hide');
				*/


				var $maxHeight = 0;
				var $eleHeight = 0;
				$('.footer-piab-review-text-wrap').each(function (i, val) {
					$eleHeight = $(this).height();
					if ($eleHeight > $maxHeight) {
						$maxHeight = $eleHeight;
					}
				});

				$('.footer-piab-review-wrap').css('height', $maxHeight + 80);


			});

		});
	</script>
@endsection