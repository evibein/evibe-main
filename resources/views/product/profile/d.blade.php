@extends('product.profile.c')

@section('profile-gallery')

	@if (isset($data['partyKitData']['gallery']) && count($data['partyKitData']['gallery']))
		<div class="profile-piab-style-gallery" data-product-style-id="{{ $data['partyKitData']['id'] }}">
			<div id="slickCarouser">
				@foreach ($data['partyKitData']['gallery'] as $gallery)
					<img class="profile-piab-profile-img mar-r-10 hide" data-product-style-id="{{ $data['partyKitData']['id'] }}" src="{{ $gallery['url'] }}" alt="{{ $gallery['title'] }}"/>
				@endforeach
				<img class="profile-piab-profile-img mar-r-10 hide" data-product-style-id="{{ $data['partyKitData']['id'] }}" src="{{ $galleryUrl }}/main/img/explainer-image_profile.png" alt="Lifelong Memories Guaranteed"/>
			</div>
		</div>
		<div class="profile-piab-style-gallery-loading">

		</div>
	@else
		<div class="profile-piab-style-gallery">
			<img class="profile-piab-style-no-image" src="{{ $galleryUrl }}/img/icons/balloons.png">
		</div>
	@endif

@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			var slickJs = "<?php echo elixir('js/util/slick.js'); ?>";
			$.getScript(slickJs, function () {

				$('#slickCarouser').slick({
					dots: false,
					infinite: false,
					speed: 300,
					slidesToShow: 1,
					centerMode: false,
					variableWidth: true,
					arrows: true
				});

				$('.profile-piab-profile-img').removeClass('hide');
				$('.profile-piab-style-gallery-loading').addClass('hide');

				$('#slickCarouser_similarPartyKits').slick({
					dots: false,
					infinite: false,
					speed: 300,
					slidesToShow: 4,
					centerMode: false,
					variableWidth: true,
					arrows: true
				});

				if($(".slick-arrow").parents(".similar-piab-styles-wrap").length) {
					/* slick arrows exist - do nothing */
				}
				else {
					/* no slick arrows - alter css to remove spacing */
					$('.similar-piab-styles-wrap .slick-list').css('margin', '0');
					$('.similar-piab-styles-wrap .slick-slider').css('padding', '0');
					$('.similar-piab-styles-wrap').css('padding', '0');
					$('.similar-piab-styles-wrap').css('margin-left', '-15px');
				}

				$('.similar-piab-styles-wrap').removeClass('hide');
				$('.similar-piab-styles-wrap-loading').addClass('hide');

			});

			/*
			 $('.footer-piab-reviews-wrap').removeClass('hide');
			 $('.footer-piab-reviews-wrap-loading').addClass('hide');
			 */

			var $priceCardTop = $('.profile-piab-price-card-wrap').offset().top;
			var $priceCardHeight = $('.profile-piab-price-card-wrap').height();
			var $priceCardWidth = $('.profile-piab-price-card-wrap').width();
			var $piabContainerHeight = $('.profile-piab-container').height();
			$(window).scroll(function (e) {
				var $el = $('.profile-piab-price-card-wrap');
				var isPositionFixed = ($el.css('position') == 'fixed');
				if ($(this).scrollTop() > $priceCardTop && !isPositionFixed) {
					$el.css({'position': 'fixed', 'top': '0px', 'width': $priceCardWidth});
				}
				if ($(this).scrollTop() < $priceCardTop && isPositionFixed) {
					$el.css({'position': 'static', 'top': '0px', 'width': $priceCardWidth});
				}
				if ($(this).scrollTop() > ($piabContainerHeight - $priceCardHeight)) {
					$el.addClass('hide');
				}
				if ($(this).scrollTop() < ($piabContainerHeight - $priceCardHeight)) {
					$el.removeClass('hide');
				}
			});

		});
	</script>
@endsection