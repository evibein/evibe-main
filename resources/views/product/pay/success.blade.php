@extends('product.pay.base')

@section('page-title')
	<title>Payment Success | Evibe.in</title>
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/refer-earn.css') }}">
@endsection

@section('gtm-data-layer')
	@php
		$itemsData = [];
		if(isset($data['booking'])) {
			foreach ($data['booking'] as $booking) {
				array_push($itemsData, [
					'name' => isset($booking['sku']) ? $booking['sku'] : 'DEC_DEFAULT_NAME',
					'sku' => isset($booking['sku']) ? $booking['sku'] : 'DEC_DEFAULT_CODE',
					'price' => isset($booking['productPrice']) ? $booking['productPrice'] : 0,
					'category' => isset($booking['categoryId']) ? $booking['categoryId'] : 'DEFAULT',
					'quantity' => 1,
					'currency' => 'INR'
				]);
			}
		}
	@endphp

	@include('track.gtm-dl-ecom', [
		'trans' => [
			'id' => isset($data['booking']['id']) ? $data['booking']['id'] : time(),
			'affiliation' => "PIAB",
			'revenue' => isset($data['totalBookingAmount']) ? $data['totalBookingAmount'] : 0,
			'shipping' => 0,
			'tax' => isset($data['totalBookingAmount']) ? $data['totalBookingAmount'] / (0.18 / 1.18) : 0,
			'currency' => 'INR'
		],

		'transId' => isset($data['booking']['id']) ? $data['booking']['id'] : time(),

		'items' => $itemsData
	])
@endsection

@section('content')
	<div class="pay-body">
		<div class="pay-success-page mar-b-20">
			<div class="ps-message-container mar-t-20">
				<div class="col-xs-12 col-sm12 col-md-12 col-lg-12">
					<div class="ps-message-card-wrap">
						<i class="glyphicon glyphicon-ok-circle valign-text-top"></i><span class="mar-l-10">Payment Successful</span>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="ps-details-container mar-t-20">
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<div class="ps-card-wrap ps-details-card-wrap text-center">
						<div class="ps-card-header text-left">
							<div class="ps-card-header-text mar-l-15">
								Order Details
							</div>
						</div>
						<div class="ps-card-body pad-b-15-imp">
							<div class="ps-products-list-wrap mar-t-15 mar-b-15">
								<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
									<div class="ps-product-img-wrap">
										<img class="ps-product-img" src="{{ $data['booking']['productImgFullUrl'] }}">
									</div>
								</div>
								<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 text-left">
									<div class="ps-product-title">
										{!! $data['booking']['bookingInfo'] !!}
									</div>
								</div>
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
									<div class="ps-product-price">
										<span class="rupee-font">&#8377; </span>
										<span>@amount($data['booking']['productPrice'])</span>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="ps-price-split-wrap mar-t-10">
								<div class="ps-price-split-component mar-t-10">
									<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 text-right">
										<div>
											Products Price
										</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<div class="">
											<span class="rupee-font">&#8377; </span>
											<span>@amount($data['totalProductsPrice'])</span>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="ps-price-split-component">
									<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 text-right">
										<div>
											Delivery Charges
										</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<div class="">
											@if(isset($data['totalDeliveryCharges']) && $data['totalDeliveryCharges'])
												<span class="rupee-font">&#8377; </span>
												<span>@amount($data['totalDeliveryCharges'])</span>
											@else
												<span class="text-g font-12">FREE</span>
											@endif
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								@if(isset($data['totalCouponDiscountAmount']) && $data['totalCouponDiscountAmount'])
									<div class="ps-price-split-component">
										<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 text-right">
											<div>
												Coupon Discount
											</div>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
											<div class="text-g">
												<span>- </span>
												<span class="rupee-font">&#8377; </span>
												<span>@amount($data['totalCouponDiscountAmount'])</span>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								@endif
								<div class="ps-price-split-component mar-t-5">
									<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 text-right">
										<div class="text-bold">
											Total Order Amount
										</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<div class="text-e text-bold">
											<span class="rupee-font">&#8377; </span>
											@if(isset($data['totalCouponDiscountAmount']) && $data['totalCouponDiscountAmount'])
												<span>@amount($data['totalBookingAmount'] - $data['totalCouponDiscountAmount'])</span>
											@else
												<span>@amount($data['totalBookingAmount'])</span>
											@endif
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 no-pad-l pad-l-15__400-600 mar-t-20__400-600">
					<div class="ps-card-wrap ps-details-card-wrap text-center">
						<div class="ps-card-body text-left pad-b-15-imp">
							<div class="pad-t-15 ps-card-body-title">
								Shipping Details
							</div>
							@if(isset($data['delivery']['fullAddress']) && $data['delivery']['fullAddress'])
								<div class="mar-t-10">
									<span class="text-888">Full Address:</span><span class="mar-l-5">{{ $data['delivery']['fullAddress'] }}</span>
								</div>
							@endif
							@if(isset($data['delivery']['landmark']) && $data['delivery']['landmark'])
								<div class="mar-t-5">
									<span class="text-888">Landmark:</span><span class="mar-l-5">{{ $data['delivery']['landmark'] }}</span>
								</div>
							@endif
							@if(isset($data['tmoLink']) && $data['tmoLink'])
								<div class="text-center mar-t-15">
									<a href="{{ $data['tmoLink'] }}" target="_blank" class="btn btn-primary btn-checkout-tmo ps-tmo-btn">Track My Order</a>
								</div>
							@endif
							@if(isset($data['delivery']['deliveryDate']) && $data['delivery']['deliveryDate'])
								<div class="mar-t-15 text-center">
									Your order will be delivered by <b>{{ date('jS M Y', strtotime($data['delivery']['deliveryDate'])) }}</b>
								</div>
							@endif
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
@endsection