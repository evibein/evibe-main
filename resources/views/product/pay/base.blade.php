@extends('product.base')

@section('page-title')
	<title>Checkout Party In A Box | Evibe.in</title>
@endsection

@section("header")
	@include('product.header')
@endsection

@section('custom-head')
	<link rel="icon" href="{{ $galleryUrl }}/img/logo/favicon_evibe_v2.png" type="image/png" sizes="16x16"/>
	<link rel="apple-touch-icon" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="57x57" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-57x57.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="72x72" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-72x72.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="76x76" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-76x76.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="114x114" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-114x114.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="120x120" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-120x120.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="144x144" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-144x144.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="152x152" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-152x152.png" type="image/png"/>
@endsection

@section('body-attrs')class="pay-body"@endsection

@section("content")
	<div class="hide">
		<form id="payUMoneyForm" action="{{ config('payumoney.end_point') }}" method="POST">
			<input id="key" name="key" type="hidden" value="">
			<input id="hash" name="hash" type="hidden" value="">
			<input id="txnid" name="txnid" type="hidden" value="">
			<input id="amount" name="amount" type="input" value="">
			<input id="productinfo" name="productinfo" type="input" value="">
			<input id="firstname" name="firstname" type="input" value="">
			<input id="email" name="email" type="input" value="">
			<input id="phone" name="phone" type="input" value="">
			<input id="surl" name="surl" type="input" value="">
			<input id="furl" name="furl" type="input" value="">
			<input id="udf1" name="udf1" type="input" value="{{ request('pbId') }}">
			<input id="service_provider" name="service_provider" type="input" value="payu_paisa">
		</form>
		<form id="razorPayForm">
			<input id="razorPayKey" type="hidden" value="{{ config('razorpay.key') }}">
			<input id="razorPayMerchant" type="hidden" value="{{ config('razorpay.merchant') }}">
			<input id="razorPayLogo" type="hidden" value="{{ $galleryUrl .'/img/logo/logo_evibe.png' }}">
			<input id="razorPaySuccessUrl" type="hidden" value="{{ route('pay.pgs.razor.success') }}">
		</form>
		<form enctype='application/json' action="https://​checkout.freecharge.in​/api/v1/co/pay/init"
				id="freeChargeForm" method="POST">
			<input id="fcChannel" name="channel" type="text" value="">
			<input id="fcChecksum" name="checksum" type="text" value="">
			<input id="fcFurl" name="furl" type="text" value="">
			<input id="fcSurl" name="surl" type="text" value="">
			<input id="fcCurrency" name="currency" type="text" value="">
			<input id="fcAmount" name="amount" value="1000" type="number">
			<input id="fcMerchantId" name="merchantId" value="" type="text">
			<input id="fcMerchantTxnId" name="merchantTxnId" value="" type="text">
			<input id="fcProductInfo" name="productInfo" type="text" value="auth">
			<input id="fcEmail" value="example@domain.com" name="email" type="email" required>
			<input id="fcMobile" name="mobile" type="text" value="" required>
			<input id="fcCustomerName" value="" name="customerName" type="text">
			<input id="fcCustomNote" name="customNote" type="text" value="">
			<input id="fcOs" name="os" type="text" value="">
		</form>
		<form enctype='application/json' method="post" action="https://securegw.paytm.in/theia/processTransaction" id="paytmForm">
			<input type="text" name="MID" id="paytmMId" value="">
			<input type="text" name="ORDER_ID" id="paytmOrderId" value="">
			<input type="text" name="CUST_ID" id="paytmCustomerId" value="">
			<input type="text" name="INDUSTRY_TYPE_ID" id="paytmIndustryTypeId" value="">
			<input type="text" name="CHANNEL_ID" id="paytmChannelId" value="">
			<input type="text" name="TXN_AMOUNT" id="paytmTransactionAmount" value="">
			<input type="text" name="WEBSITE" id="paytmWebsite" value="">
			<input type="text" name="CHECKSUMHASH" id="paytmChecksumHash" value="">
			<input type="text" name="CALLBACK_URL" id="paytmSuccessUrl" value="">
		</form>
		<form id="redirectToIntermediateForm" method="POST" action=""></form>
	</div>
@endsection

@section("javascript")
	<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
@endsection

@section("footer")
	<div class="footer-wrap">
		@include('base.home.footer.footer-non-city')
	</div>
@endsection