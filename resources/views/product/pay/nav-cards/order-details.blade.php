<div class="checkout-details-wrap">
	<div class="checkout-card-header">
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 pad-r-15__400-600 text-left valign-mid">
			<div class="checkout-card-header-text">
				<span class="mar-r-5">Additional Details</span>
				<span class="checkout-card-header-info">(Kindly review your order and proceed to pay)</span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="checkout-card-body">
		<div class="form-row mar-t-10 hide">
			<div class="col-sm-6 col-md-4 col-lg-4">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					<select class="mdl-textfield__input" id="specialOccasion" name="specialOccasion" style="height: 29px;">
						<option></option>
						<option value="birthday">Birthday</option>
						<option value="baby-shower">Baby Shower</option>
						<option value="anniversary">Anniversary</option>
					</select>
					<label class="mdl-textfield__label" for="specialOccasion">What is the special occasion?
						<small> (optional)</small>
					</label>
				</div>
			</div>
		</div>
		<div class="mar-t-20 pad-b-15__400-600">
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class="checkout-product-img-wrap">
					<img class="checkout-product-img" src="{{ $data['booking']['productImgFullUrl'] }}">
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 mar-t-10-400-600">
				<div class="checkout-product-img-title">
					{!! $data['booking']['bookingInfo'] !!}
				</div>
				<div class="checkout-product-img-quantity mar-t-5">
					<span>Quantity: </span>
					<span>1</span>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		@if ($agent->isMobile() && !($agent->isTablet()))
			<div class="mar-t-10">
				<div class="payment-gateways-wrap mar-l-10 mar-r-10">
					@include('product.pay.nav-cards.price-details')
				</div>
				<div class="mar-t-10">
					@include('product.pay.nav-cards.coupon-details')
				</div>
			</div>
		@else
		@endif
		<div class="mar-t-10">
			<div class="details pad-t-10">
				<div class="form-info text-center mar-b-10">
					@if(!$data['booking']['paidAt'])
						<div>
							<div class="pad-20 pad-t-10__400">
								<button id="payBtn" class="btn btn-evibe btn-large btn-pay text-center">
									<span>Proceed to Pay </span>
									<span class='rupee-font'>&#8377;</span>
									<span class="checkout-advance-amount mar-l-4">{{ $data['booking']['bookingAmount'] }}</span>
								</button>
							</div>
						</div>
						<small class="text-muted text-center blk font-11">
							<i>
								By proceeding further you accept to all our
								<a class="mar-l-4" href="{{ route('terms') }}" target="_blank" rel="noopener">terms of booking.</a>
							</i>
						</small>
					@else
						<span class='text-success text-upr text-bold font-18'>Payment Received</span>
					@endif
				</div>
			</div>
		</div>
	</div>
	<div class="checkout-card-footer">

	</div>
	<input type="hidden" id="paymentMethodPayU" value="payU" data-url="{{ route('pay.pgs.payU.init', request('pbId')) }}">

</div>