<div class="checkout-details-wrap">
	<div class="checkout-card-header">
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 pad-r-15__400-600 text-left valign-mid">
			<div class="checkout-card-header-text">
				<span class="mar-r-5">Additional Details</span>
				<span class="checkout-card-header-info">(Kindly help us better understand your party and proceed to pay)</span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="checkout-card-body">
		@if(isset($data['additionalFields']) && count($data['additionalFields'])> 0)
			<div class="form-row mar-t-10">
				@foreach($data['additionalFields'] as $additionalField)
					@if($additionalField['typeFieldId'] == config('evibe.input.checkbox'))
						<div class="col-sm-12 col-md-12 col-lg-12 input-field mar-b-10 mar-t-30">
							<div id="{{ $additionalField['name'] }}">
								<div>
									<div class="text-bold">{{ $additionalField['identifier'] }}</div>
									@if($additionalField['hintMessage'])
										<div class="hint-msg font-12 text-info">({{ ucfirst($additionalField['hintMessage']) }})</div>
									@endif
								</div>
								@php
									$valueOptionsArray = [];
									$valueString = $additionalField['value'];
								@endphp
								@if($valueString)
									@php $valueOptionsArray = explode(', ', $valueString); @endphp
								@endif
								@foreach($additionalField['options'] as $checkoutOption)
									<div class="md-checkbox in-blk pad-r-20">
										<input type="checkbox" class="product-booking-additional-field" data-additional-field-id="{{ $additionalField['id'] }}" data-type-field-id="{{ $additionalField['typeFieldId'] }}" id="{{ $checkoutOption['id'] }}" name="{{ $additionalField['name'] }}" value="{{ $checkoutOption['name'] }}" @if(count($valueOptionsArray) && in_array($checkoutOption['name'], $valueOptionsArray)) checked @endif>
										<label for="{{ $checkoutOption['id'] }}">{{ ucwords($checkoutOption['name']) }}</label>
									</div>
								@endforeach
							</div>
						</div>
					@elseif($additionalField['typeFieldId'] == config('evibe.input.option'))
						@if(count($additionalField['options']))
							<div class="col-sm-6 col-md-4 col-lg-4">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<select class="mdl-textfield__input product-booking-additional-field" data-additional-field-id="{{ $additionalField['id'] }}" data-type-field-id="{{ $additionalField['typeFieldId'] }}" id="{{ $additionalField['id'] }}" name="{{ $additionalField['name'] }}" style="height: 29px;">
										<option></option>
										<option value="N/A">N/A</option>
										@foreach($additionalField['options'] as $checkoutOption)
											<option value="{{ $checkoutOption['name'] }}" @if($additionalField['value'] == $checkoutOption['name'])) selected @endif>{{ ucwords($checkoutOption['name']) }}</option>
										@endforeach
									</select>
									<label class="mdl-textfield__label" for="{{ $additionalField['id'] }}">{{ ucwords($additionalField['identifier']) }}*</label>
									@if($additionalField['hintMessage'])
										<div class="hint-msg checkout-hint-msg font-12 text-info">({{ ucfirst($additionalField['hintMessage']) }})</div>
									@endif
								</div>
							</div>
						@endif
					@elseif($additionalField['typeFieldId'] == config('evibe.input.date'))
						<div class="col-sm-6 col-md-4 col-lg-4">
							<div class="input-field">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input id="{{ $additionalField['id'] }}" data-additional-field-id="{{ $additionalField['id'] }}" data-type-field-id="{{ $additionalField['typeFieldId'] }}" type="text" class="mdl-textfield__input additional-field-input-date product-booking-additional-field" value="@if(request()->has($additionalField['name'])){{ old($additionalField['name']) }}@elseif($additionalField['value']){{ $additionalField['value'] }}@endif">
									<label class="mdl-textfield__label">
										<span class="label-text">{{ ucwords($additionalField['identifier']) }}</span>
									</label>
									@if($additionalField['hintMessage'])
										<div class="hint-msg checkout-hint-msg font-12 text-info">({{ ucfirst($additionalField['hintMessage']) }})</div>
									@endif
								</div>
							</div>
						</div>
					@else
						<div class="col-sm-6 col-md-4 col-lg-4">
							<div class="input-field">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input id="{{ $additionalField['id'] }}" data-additional-field-id="{{ $additionalField['id'] }}" data-type-field-id="{{ $additionalField['typeFieldId'] }}" type="text" class="mdl-textfield__input product-booking-additional-field" value="@if(request()->has($additionalField['name'])){{ old($additionalField['name']) }}@elseif($additionalField['value']){{ $additionalField['value'] }}@endif">
									<label class="mdl-textfield__label">
										<span class="label-text">{{ ucwords($additionalField['identifier']) }}*</span>
									</label>
									@if($additionalField['hintMessage'])
										<div class="hint-msg checkout-hint-msg font-12 text-info">({{ ucfirst($additionalField['hintMessage']) }})</div>
									@endif
								</div>
							</div>
						</div>
					@endif
				@endforeach
				<div class="clearfix"></div>

				<div class="clearfix"></div>
			</div>
		@else
			<div class="mar-t-20 pad-b-15__400-600">
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="checkout-product-img-wrap">
						<img class="checkout-product-img" src="{{ $data['booking']['productImgFullUrl'] }}">
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 mar-t-10-400-600">
					<div class="checkout-product-img-title">
						{!! $data['booking']['bookingInfo'] !!}
					</div>
					<div class="checkout-product-img-quantity mar-t-5">
						<span>Quantity: </span>
						<span>1</span>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		@endif
		@if ($agent->isMobile() && !($agent->isTablet()))
			<div class="mar-t-10">
				<div class="payment-gateways-wrap mar-l-10 mar-r-10">
					@include('product.pay.nav-cards.price-details')
				</div>
				<div class="mar-t-10">
					@include('product.pay.nav-cards.coupon-details')
				</div>
			</div>
		@else
		@endif
		<div class="mar-t-10">
			<div class="details pad-t-10">
				<div class="form-info text-center mar-b-10">
					@if(!$data['booking']['paidAt'])
						<div>
							<div class="pad-20 pad-t-10__400">
								<button id="payBtn" class="btn btn-evibe btn-large btn-pay text-center">
									<span>Proceed to Pay </span>
									<span class='rupee-font'>&#8377;</span>
									<span class="checkout-advance-amount mar-l-4">{{ $data['booking']['bookingAmount'] }}</span>
								</button>
							</div>
						</div>
						<small class="text-muted text-center blk font-11">
							<i>
								By proceeding further you accept to all our
								<a class="mar-l-4" href="{{ route('terms') }}" target="_blank" rel="noopener">terms of booking.</a>
							</i>
						</small>
					@else
						<span class='text-success text-upr text-bold font-18'>Payment Received</span>
					@endif
				</div>
			</div>
		</div>
	</div>
	<div class="checkout-card-footer">

	</div>
	<input type="hidden" id="paymentMethodPayU" value="payU" data-url="{{ route('pay.pgs.payU.init', request('pbId')) }}">

</div>