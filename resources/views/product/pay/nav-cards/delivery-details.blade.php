<div class="checkout-details-wrap">
	<div class="checkout-card-header">
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 pad-r-15__400-600 text-left valign-mid">
			<div class="checkout-card-header-text">
				<span class="mar-r-5">Delivery Details</span>
				<span class="checkout-card-header-info">(Kindly provide the delivery address)</span>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-right text-center-400-600 mar-t-10-400-600 valign-mid">
			<div class="btn btn-primary btn-checkout-card-continue checkout-card-details-submit-btn btn-checkout-delivery-continue">
				<span class="">Continue</span>
				<span class="glyphicon glyphicon-arrow-right mar-l-5 no-mar-r"></span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="checkout-card-body">
		<div class="form-row mar-t-10">
			<div class="col-sm-6 col-md-4 col-lg-4 input-field">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					<input id="zipCode" type="number" class="mdl-textfield__input" value="@if(old('zipCode')){{ old('zipCode') }}@elseif($data['delivery']['zipCode']){{ $data['delivery']['zipCode'] }}@endif">
					<label class="mdl-textfield__label" for="zipCode">
						<span class="label-text">Pin Code*</span>
					</label>
				</div>
			</div>
			<div class="col-sm-6 col-md-4 col-lg-4 input-field">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					<input id="address" type="text" class="mdl-textfield__input" value="@if(request()->has('address')){{ old('address') }}@else{{ $data['delivery']['address'] }}@endif">
					<label class="mdl-textfield__label" for="address">
						<span class="label-text">Full Address*</span>
					</label>
				</div>
			</div>
			<div class="col-sm-6 col-md-4 col-lg-4 input-field">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					<input id="landmark" type="text" class="mdl-textfield__input" value="@if(request()->has('landmark')){{ old('landmark') }}@else{{ $data['delivery']['landmark'] }}@endif">
					<label class="mdl-textfield__label" for="landmark">
						<span class="label-text">Landmark*</span>
					</label>
				</div>
			</div>
			<div class="col-sm-6 col-md-4 col-lg-4 input-field">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					<input id="city" type="text" class="mdl-textfield__input" value="@if(request()->has('city')){{ old('city') }}@else{{ $data['delivery']['city'] }}@endif">
					<label class="mdl-textfield__label" for="city">
						<span class="label-text">City*</span>
					</label>
				</div>
			</div>
			<div class="col-sm-6 col-md-4 col-lg-4 input-field">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					<input id="state" type="text" class="mdl-textfield__input" value="@if(request()->has('state')){{ old('state') }}@else{{ $data['delivery']['state'] }}@endif">
					<label class="mdl-textfield__label" for="state">
						<span class="label-text">State*</span>
					</label>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="checkout-card-footer">
		<div class="btn btn-primary btn-checkout-card-continue checkout-card-details-submit-btn btn-checkout-delivery-continue">
			<span class="">Continue</span>
			<span class="glyphicon glyphicon-arrow-right mar-l-5 no-mar-r"></span>
		</div>
	</div>
</div>