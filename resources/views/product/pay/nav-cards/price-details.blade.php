<div class="checkout-details-wrap">
	<div class="checkout-card-header">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left valign-mid">
			<div class="checkout-card-header-text">
				Order Summary
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="checkout-card-body">
		<div class="mar-t-20 pad-b-10 font-14">
			<div class="checkout-price-split-up-wrap"></div>
		</div>
	</div>
</div>