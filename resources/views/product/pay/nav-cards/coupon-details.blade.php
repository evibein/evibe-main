<div class="checkout-details-wrap">
	<div class="checkout-card-body hide">
		<div class="in-blk valign-bottom mar-b-20">
			<img class="checkout-piab-coupon-img" src="{{ $galleryUrl }}/img/icons/discount.png">
		</div>
		<div class="in-blk mar-l-10 mar-t-15 mar-b-10">
			<div class="checkout-piab-coupon-title">EVBSHIPFREE</div>
			<div class="checkout-piab-coupon-hint">Coupon auto applied</div>
		</div>
	</div>

	<div class="checkout-card-body no-pad__400-600">
		<div>
			<div class="coupon-code-wrap mar-l-10 mar-r-10 pad-b-10 mar-b-20 pad-l-10 pad-r-10 pad-t-10 text-left">
				<div class="coupon-code-input-wrap text-center">
					<div class="mar-l-10 mar-r-10 coupon-error-msg-wrap pad-10 hide">
						<div class="text text-danger">
							<i class="glyphicon glyphicon-exclamation-sign"></i>
							<span class="coupon-error-msg mar-l-4"></span>
						</div>
					</div>
					<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label pad-l-5 mar-r-5 in-blk">
							<input type="text" id="couponCode" class="mdl-textfield__input" value="">
							<label class="mdl-textfield__label pad-l-5" for="couponCode">
								<span class="label-text">Enter Coupon Code</span>
							</label>
						</div>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 mar-t-15 no-pad">
						<div class="in-blk">
							<a class="btn btn-default coupon-code-apply-button mar-b-15__400 mar-b-15-400-600">APPLY</a>
							<a class="btn btn-default coupon-code-remove-button hide  mar-b-15__400 mar-b-15-400-600">
								<i class="glyphicon glyphicon-remove"></i>
								<span> REMOVE</span>
							</a>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="mar-l-10 mar-r-10 coupon-success-msg-wrap alert alert-success pad-10 hide">
						<div class="text text-success text-center">
															<span class="coupon-Success-msg">
																<i class="glyphicon glyphicon-ok-circle"></i>
																<span> Congratulations! Coupon applied successfully. You have saved <span class='rupee-font'>&#8377;</span></span>
																<span class="coupon-discount-amount"></span>.</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
