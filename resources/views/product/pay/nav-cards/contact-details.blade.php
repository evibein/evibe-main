<div class="checkout-details-wrap">
	<div class="checkout-card-header">
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 pad-r-15__400-600 text-left valign-mid">
			<div class="checkout-card-header-text">
				<span class="mar-r-5">Contact Details</span>
				<span class="checkout-card-header-info">(Kindly provide your contact details)</span>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-right text-center-400-600 mar-t-10-400-600 valign-mid">
			<div class="btn btn-primary btn-checkout-card-continue checkout-card-details-submit-btn btn-checkout-contact-continue">
				<span class="">Continue</span>
				<span class="glyphicon glyphicon-arrow-right mar-l-5 no-mar-r"></span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="checkout-card-body">
		<div class="form-row mar-t-10">
			<div class="col-md-6">
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad">
					@if(isset($data['countries']) && count($data['countries']))
						<select id="inpCallingCode" name="inpCallingCode" class="form-control country-calling-code">
							@foreach($data['countries'] as $country)
								<option value="{{ $country->calling_code }}" @if(isset($data['customer']['callingCode']) && ($data['customer']['callingCode'] == $country->calling_code)) selected @endif>{{ $country->calling_code }}</option>
							@endforeach
						</select>
					@else
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
							<input id="inpCallingCode" type="text" class="mdl-textfield__input" value="@if(request()->has('inpCallingCode')){{ old('inpCallingCode') }}@else{{ $data['customer']['callingCode'] }}@endif">
							<label class="mdl-textfield__label" for="inpCallingCode">
								<span class="label-text">Calling Code</span>
							</label>
						</div>
					@endif
				</div>
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-pad-r">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="inpPhone" type="text" class="mdl-textfield__input" value="@if(request()->has('inpPhone')){{ old('inpPhone') }}@else{{ $data['customer']['phone'] }}@endif">
						<label class="mdl-textfield__label" for="inpPhone">
							<span class="label-text">Phone Number*</span>
						</label>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="col-md-6 ">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					<input id="inpEmail" type="text" class="mdl-textfield__input email-typo-error" value="@if(request()->has('inpEmail')){{ old('inpEmail') }}@else{{ $data['customer']['email'] }}@endif">
					<label class="mdl-textfield__label" for="inpEmail">
						<span class="label-text">Email id*</span>
					</label>
				</div>
			</div>
			<div class="col-md-6">
				<div class="col-xs-2 col-md-3 no-pad-l no-pad-r">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<select class="mdl-textfield__input" id="title" name="title" style="height: 29px;">
							<option></option>
							<option value="1" @if(isset($data['customer']['customerGender']) && ($data['customer']['customerGender'] == 1)) selected @endif>Mr</option>
							<option value="2" @if(isset($data['customer']['customerGender']) && ($data['customer']['customerGender'] == 2)) selected @endif>Ms</option>
						</select>
						<label class="mdl-textfield__label" for="title">Title</label>
					</div>
				</div>
				<div class="col-xs-10 col-md-9 no-pad-r pad-l-5">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="name" type="text" class="mdl-textfield__input" value="@if(request()->has('name')){{ old('name') }}@else{{ $data['customer']['name'] }}@endif">
						<label class="mdl-textfield__label" for="name">
							<span class="label-text">Full Name*</span>
						</label>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="checkout-card-footer">
		<div class="btn btn-primary btn-checkout-card-continue checkout-card-details-submit-btn btn-checkout-contact-continue">
			<span class="glyphicon glyphicon-arrow-right"></span>
			<span class="mar-l-4">Continue</span>
		</div>
	</div>
</div>