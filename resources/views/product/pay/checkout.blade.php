@extends('product.pay.base')

@section('content')
	@parent
	<div class="checkout-page-2 auto-checkout-page">
		<div class="checkout-hidden-details-container hide">
			<input id="productBookingId" type="hidden" value="{{ request('id') }}">
			<input id="productBookingPaymentId" type="hidden" value="{{ request('pbId') }}">
			<input type="hidden" id="hidValidateContactDetailsUrl" value="{{ route('party-kits.buy.validate.contact-details', ['productBookingId' => request('id')]) }}">
			<input type="hidden" id="hidValidateDeliveryDetailsUrl" value="{{ route('party-kits.buy.validate.delivery-details', ['productBookingId' => request('id')]) }}">
			<input type="hidden" id="hidValidateOrderDetailsUrl" value="{{ route('party-kits.buy.validate.order-details', ['productBookingId' => request('id')]) }}">
			<input type="hidden" id="hidUpdatePriceDetailsUrl" value="{{ route('party-kits.buy.update.price-details', ['productBookingId' => request('id')]) }}">
			<input type="hidden" id="advanceAmount" value="{{ $data['booking']['bookingAmount'] }}">
			<input type="hidden" id="couponSecretKey" value="@if(isset($data["couponToken"])){{ $data["couponToken"] }}@endif">
			<input type="hidden" id="isValidStatus" @if(isset($data['booking']['statusId']) && $data['booking']['statusId'] && ($data['booking']['statusId'] == config('evibe.product-booking-status.cancelled'))) value="0" @else value="1" @endif>
			<input type="hidden" id="hidInputTypeCheckbox" value="{{ config("evibe.input.checkbox") }}">
			<input type="hidden" id="hidInputTypeOption" value="{{ config("evibe.input.option") }}">
		</div>
		@if($data['booking']['paidAt'])
			<div class="checkout-tmo-container mar-t-15">
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2">
					<div class="checkout-tmo-card-wrap text-center">
						<div class="checkout-tmo-text">
							You have successfully made payment for this order
						</div>
						<div class="checkout-tmo-cta-wrap mar-t-15">
							<a href="{{ $data['tmoLink'] }}" class="btn btn-primary btn-checkout-tmo">
								<span class="glyphicon glyphicon-screenshot"></span>
								<span class="mar-l-4">Track My Order</span>
							</a>
							<div class="checkout-tmo-cta-text mar-t-5">
								( View your order and provider details )
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		@else
			<div class="checkout-navigation-container">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="checkout-navigation-card-wrap">
						<div id="wizard" class="checkout-navigation-list form_wizard wizard_horizontal pad-l-5 pad-r-5">
							<ul class="wizard_steps anchor no-pad no-mar-t no-mar-b">
								<li>
									<a data-ref="contactDetails" data-checkout-card-count="" class="checkout-navigation-btn btn-checkout-navigation-contact disabled">
										<span class="step_no"></span>
										<span class="step_descr">Contact Details<br></span>
									</a>
								</li>
								<li>
									<a data-ref="deliveryDetails" data-checkout-card-count="" class="checkout-navigation-btn btn-checkout-navigation-delivery disabled">
										<span class="step_no"></span>
										<span class="step_descr">Delivery Details<br></span>
									</a>
								</li>
								<li>
									<a data-ref="additionalDetails" data-checkout-card-count="" class="checkout-navigation-btn btn-checkout-navigation-additional disabled">
										<span class="step_no"></span>
										<span class="step_descr">Additional Details<br></span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="checkout-details-container mar-t-15">
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<div id="contactDetails" class="checkout-card-wrap" data-ref="contactDetails">
						@include('product.pay.nav-cards.contact-details')
					</div>
					<div id="deliveryDetails" class="checkout-card-wrap" data-ref="deliveryDetails">
						@include('product.pay.nav-cards.delivery-details')
					</div>
					<div id="additionalDetails" class="checkout-card-wrap" data-ref="additionalDetails">
						@include('product.pay.nav-cards.additional-details')
					</div>
				</div>
				@if ($agent->isMobile() && !($agent->isTablet()))
				@else
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 no-pad-l">
						<div id="priceDetails" class="checkout-price-card-wrap">
							@include('product.pay.nav-cards.price-details')
						</div>
						<div id="couponDetails" class="checkout-coupon-card-wrap mar-t-15">
							@include('product.pay.nav-cards.coupon-details')
						</div>
					</div>
				@endif
				<div class="clearfix"></div>
			</div>
		@endif
	</div>
@endsection

@section("modals")
	@parent
@endsection

@section('javascript')
	@parent

	<script type="text/javascript">
		$(document).ready(function () {

			var productBookingId = $('#productBookingId').val();
			var userInput = {};
			var presentCheckoutCardCount = 0;
			var nextCheckoutCardCount = 0;
			var maxCheckoutCardCount = 0;
			var totalBookingAmount = 0;
			var totalAdvanceAmount = 0;
			var orderPriceDetails = {};
			var couponCodeVal = '';
			var couponDiscountAmount = 0;

			function scrollToTop() {
				window.scrollTo(0, 0);
			}

			(function CouponInits() {
				/* Declaring outside because customer has the access to edit these input values, while will be problem while fetching */
				var productId = $('#productBookingPaymentId').val();
				var totalAdvance = $('#advanceAmount').val();

				$('.coupon-code-apply-button').on('click', function () {
					/* Hide all the existing notifications */
					$('.coupon-success-msg-wrap').addClass('hide');
					$('.coupon-error-msg-wrap').addClass('hide');
					couponDiscountAmount = 0;
					updatePriceDetails();

					/* Reset the amount if they try for another coupon */
					$text = "PAY <span class='rupee-font'>&#8377;</span> " + parseFloat(totalAdvance, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
					$('#payBtn').html($text);

					var name = $('#name').val();
					var phone = $('#inpPhone').val();

					/* For normal booking tickets, customer do not have access to modify the email address */
					if ($("#inpEmail")[0]) {
						var email = $('#inpEmail').val();
					} else {
						email = $('#ticketEmailIdForNormalBooking').val();
					}

					var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
					var couponCode = $('#couponCode').val();

					if (!name || !phone || !email) {
						showNotyError("Please enter your name, phone & email to apply the coupon code");
					} else if (!regex.test(email)) {
						showNotyError("Please enter a valid Email Address");
					} else if (phone.length != 10) {
						showNotyError("Please enter a valid Phone number");
					} else if (!couponCode && couponCode.length < 6) {
						showNotyError("Please enter valid coupon code");
					} else {
						window.showLoading();

						var data = {
							'name': name,
							'phone': phone,
							'email': email,
							'couponCode': couponCode,
							'productId': productId,
							'totalAdvance': totalAdvance,
							'couponToken': $('#couponSecretKey').val(),
							'isVenueDeal': $('#isVenueDeal').length ? $('#isVenueDeal').val() : 0
						};
						$.ajax({
							url: '/coupon/validate/piab',
							type: 'POST',
							dataType: 'json',
							data: data
						}).done(function (data, textStatus, jqXHR) {
							if (data.success == "yes") {
								window.hideLoading();
								updateCouponCodeVal('');

								if (data.couponExist == "no") {
									$('.coupon-error-msg').text("Sorry, the coupon code is not valid.");
									$('.coupon-success-msg-wrap').addClass('hide');
									$('.coupon-error-msg-wrap').removeClass('hide');

									couponDiscountAmount = 0;
									updatePriceDetails();
								} else {
									couponDiscountAmount = data.discountAmount;
									updatePriceDetails();

									if (data.discountAmount <= 0) {
										$('.coupon-error-msg').text("Sorry, the coupon code is not valid.");
										$('.coupon-success-msg-wrap').addClass('hide');
										$('.coupon-error-msg-wrap').removeClass('hide');
									} else {
										$('.coupon-code-apply-button').addClass('hide');
										$('.coupon-code-remove-button').removeClass('hide');
										$('#couponCode').attr('disabled', true).css({cursor: "not-allowed"});
										$('.coupon-error-msg-wrap').addClass('hide');
										$('.coupon-success-msg-wrap').removeClass('hide');

										if (data.discountAmount > 0) {
											/* Updating the discount amount in the success notification */
											$('.coupon-discount-amount').text(data.discountAmount);

											updateCouponCodeVal($('#couponCode').val());
											$modifiedAmount = totalAdvance - data.discountAmount;
											$text = "PAY <span class='rupee-font'>&#8377;</span> " + parseFloat($modifiedAmount, 10).toFixed(0).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();

											$('#payBtn').html($text);

											/* Update the total price by striking the old price & showing the new price. */
											$priceText = "<strike><span class='rupee-font'>&#8377;</span> " + totalAdvance +
												"</strike> <span class='pad-l-10 rupee-font'>&#8377;</span> " + parseFloat($modifiedAmount, 10).toFixed(0).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString() +
												"<br><span class='font-12 text-success'><span class='rupee-font'>&#8377;</span> " + data.discountAmount + " Coupon Discount</span>";

											if ($(".checkout-price")[0]) {
												$('.checkout-price').html($priceText);
											} else if ($(".checkout-price-total-amount")[0]) {
												$('.checkout-price-total-amount').html($priceText);
											}
										}
									}
								}
							} else if (data.success == "no") {
								window.hideLoading();
								showNotyError(data.error);
							}
						}).fail(function (jqXHR, textStatus, errorThrown) {
							window.notifyTeam({
								"url": "/coupon/validate",
								"textStatus": textStatus,
								"errorThrown": errorThrown
							});
						});
					}
				});

				$('.coupon-code-remove-button').on('click', function () {
					/* Hide all the existing notifications */
					$('.coupon-success-msg-wrap').addClass('hide');
					$('.coupon-error-msg-wrap').addClass('hide');
					couponDiscountAmount = 0;
					updatePriceDetails();

					$('.coupon-code-remove-button').addClass('hide');
					$('.coupon-code-apply-button').removeClass('hide');
					$('#couponCode').attr('disabled', false).css({cursor: "default"}).val('');

					$modifiedAmount = totalAdvance;
					$text = "PAY <span class='rupee-font'>&#8377;</span> " + parseFloat($modifiedAmount, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();

					$('#payBtn').html($text);

					/* Update the total price by striking the old price & showing the new price. */
					$priceText = "<span class='rupee-font'>&#8377;</span> " + totalAdvance;

					if ($(".checkout-price")[0]) {
						$('.checkout-price').html($priceText);
					} else if ($(".checkout-price-total-amount")[0]) {
						$('.checkout-price-total-amount').html($priceText);
					}
				})
			})();

			function updateCouponCodeVal(val) {
				couponCodeVal = val;
			}

			function reportAjaxFail(that, error) {
				if (that) that.removeClass('hide');
				error = error ? error : "";
				window.hideLoading();
				window.showNotyError(error);
			}

			function razorCapturePayment(response) {
				window.showLoading();
				var url = $('#razorPaySuccessUrl').val();
				var data = {
					'productBookingId': productBookingId,
					'razorpay_payment_id': response.razorpay_payment_id
				};

				$.ajax({
					url: url,
					type: 'POST',
					dataType: 'json',
					data: data
				}).done(function (data, textStatus, jqXHR) {
					if (data.success) {
						/* form POST to intermediate page */
						$('#redirectToIntermediateForm').attr('action', data.redirectTo).submit();
					} else reportAjaxFail(null, data.error);
				}).fail(function (jqXHR, textStatus, errorThrown) {
					reportAjaxFail(null, null)
				});
			}

			function processRazorPostInit(data, paymentType) {
				var razorPayKey = $("#razorPayKey").val();
				var amount = parseInt(data.amount, 10);
				var merchant = $("#razorPayMerchant").val();
				var logo = $("#razorPayLogo").val();
				var $inpEmail = $('#inpEmail');
				var email = parseInt($inpEmail.length, 10) && $inpEmail.val() ? $inpEmail.val() : $('#custEmail').val();

				/* razorPay modal */
				var options = {
					key: razorPayKey,
					amount: amount,
					name: merchant,
					description: "Ticket #" + ticketId,
					image: logo,
					handler: function (response) {
						razorCapturePayment(response);
						/* capture response */
					},
					prefill: {
						name: $('#inpName').val(),
						email: email,
						contact: $('#inpPhone').val(),
						method: paymentType ? paymentType : "card"
					},
					modal: {
						ondismiss: function () {
							/* show make payment button */
							$('#payBtn').removeClass('hide');
						}
					}
				};

				window.hideLoading();
				var rzp1 = new Razorpay(options);
				rzp1.open();
			}

			function processPayUPostInit(data) {
				var payU = data.payu;

				/* submit PayUMoney form */
				if (payU) {
					$('#key').attr('value', payU.key);
					$('#txnid').attr('value', payU.txnid);
					$('#amount').attr('value', payU.amount);
					$('#productinfo').attr('value', payU.productinfo);
					$('#firstname').attr('value', payU.firstname);
					$('#email').attr('value', payU.email);
					$('#phone').attr('value', payU.phone);
					$('#surl').attr('value', payU.surl);
					$('#furl').attr('value', payU.furl);
					$('#hash').attr('value', payU.hash);
					$('#payUMoneyForm').submit();
				}
			}

			function processPaytmInit(data) {
				var paytm = data.paytm;

				/* submit Paytm form */
				if (paytm) {
					$('#paytmMId').attr('value', paytm.MID);
					$('#paytmOrderId').attr('value', paytm.ORDER_ID);
					$('#paytmCustomerId').attr('value', paytm.CUST_ID);
					$('#paytmIndustryTypeId').attr('value', paytm.INDUSTRY_TYPE_ID);
					$('#paytmChannelId').attr('value', paytm.CHANNEL_ID);
					$('#paytmTransactionAmount').attr('value', paytm.TXN_AMOUNT);
					$('#paytmWebsite').attr('value', paytm.WEBSITE);
					$('#paytmChecksumHash').attr('value', paytm.checksum);
					$('#paytmSuccessUrl').attr('value', paytm.CALLBACK_URL);
					$('#paytmForm').submit();
				}
			}

			function showNavTargetCheckoutCard($targetNavElement) {
				/*
				 * if (targetCount < presentCount) {
				 *      directly navigate;
				 * }
				 * else {
				 *      validate present card;
				 *      proceed to target card;
				 * }
				 *
				 * */
				var targetCheckoutCardCount = $targetNavElement.data('checkout-card-count');
				var presentCheckoutCardRef = location.hash.substr(1);

				if ($targetNavElement.hasClass('done')) {
					if (targetCheckoutCardCount < presentCheckoutCardCount) {
						/* no validation, as there is not a valid use case */
						/* @see: should implement validation if such case happens */
						showCheckoutCardFromRef($targetNavElement.data('ref'));
					} else {
						nextCheckoutCardCount = targetCheckoutCardCount;
						validateCheckoutCard(presentCheckoutCardRef);
					}
				}
			}

			function showNextCheckoutCard() {
				/*
				 * present checkout card count is updated whenever a card is shown
				 * This indicated that nav bar should be updated whenever a change happens
				 * */

				$('.checkout-navigation-btn').each(function (i, el) {
					if ($(el).data('checkout-card-count') == nextCheckoutCardCount) {
						showCheckoutCardFromRef($(el).data('ref'));
					}
				});
			}

			function showFirstCheckoutCard() {
				nextCheckoutCardCount = 1;

				$('.checkout-navigation-btn').each(function (i, el) {
					if ($(el).data('checkout-card-count') == nextCheckoutCardCount) {
						showCheckoutCardFromRef($(el).data('ref'));
					}
				});
			}

			function showCheckoutCardFromRef($cardRef) {
				var $cardId = '#' + $cardRef;
				$('.checkout-card-wrap').addClass('hide');

				if (!$($cardId).length) {
					showFirstCheckoutCard();
				} else {
					$($cardId).removeClass('hide');
					updateCheckoutCardNavigation($cardRef);
					scrollToTop();

					window.location.hash = "#" + $cardRef;
				}
			}

			function validateContactDetails() {
				window.showLoading();

				userInput.name = $('#name').val();
				userInput.phone = $('#inpPhone').val();
				userInput.callingCode = $('#inpCallingCode').val();
				userInput.email = $('#inpEmail').val();
				userInput.title = $('#title').val();

				var $errorMsg = "Some error occurred while submitting your data. Kindly refresh the page and try again.";
				var $ajaxUrl = $('#hidValidateContactDetailsUrl').val();

				$.ajax({
					url: $ajaxUrl,
					dataType: 'json',
					data: userInput,
					type: 'POST',
					success: function (data) {
						window.hideLoading();
						if (data.success) {
							/* showPaymentDetails(); */
							showNextCheckoutCard();
						} else {
							if (data.errorMsg) {
								$errorMsg = data.errorMsg;
							}
							window.showNotyError($errorMsg);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						window.hideLoading();
						window.showNotyError($errorMsg);
						window.notifyTeam({
							"url": $ajaxUrl,
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			}

			function validateDeliveryDetails() {
				window.showLoading();

				/* add area id of the ticket */
				userInput.preDefinedAreaId = $('#preDefinedAreaId').val();

				/* add venue info only for not venue booking */
				var $address = $('#address');
				if (parseInt($address.length, 10) === 1) {
					userInput.zipCode = $('#zipCode').val();
					userInput.address = $address.val();
					userInput.landmark = $('#landmark').val();
					userInput.city = $('#city').val();
					userInput.state = $('#state').val();
				}

				var $errorMsg = "Some error occurred while submitting your data. Kindly refresh the page and try again.";
				var $ajaxUrl = $('#hidValidateDeliveryDetailsUrl').val();

				$.ajax({
					url: $ajaxUrl,
					dataType: 'json',
					data: userInput,
					type: 'POST',
					success: function (data) {
						window.hideLoading();
						if (data.success) {
							/* showPartyDetails(); */
							showNextCheckoutCard();
						} else {
							if (data.errorMsg) {
								$errorMsg = data.errorMsg;
							}
							window.showNotyError($errorMsg);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						window.hideLoading();
						window.showNotyError($errorMsg);
						window.notifyTeam({
							"url": $ajaxUrl,
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			}

			function validateOrderDetails() {
				window.showLoading();

				userInput.uploadImage = $('.checkout-page-2 #uploadImage').val();
				userInput.uploadSuccess = $('#hidUploadSuccess').val();

				/* get the dynamic field value */
				var $dynamicFields = $('#dynamicFieldsUser');
				if ($dynamicFields.length > 0) {
					var dynamicFields = JSON.parse($dynamicFields.val());
					$.each(dynamicFields, function (key, value) {
						var name = value['name'];
						var fieldType = value['type_field_id'];
						if (fieldType == $('#hidInputTypeCheckbox').val()) {
							var checkedFields = $('input[name="' + name + '"]:checked');
							var checkboxValue = '';
							$.each(checkedFields, function (key, value) {
								if (checkboxValue !== '') {
									checkboxValue = checkboxValue + ', ';
								}
								checkboxValue = checkboxValue + $(this).val();
							});
							userInput[name] = checkboxValue;
						} else {
							userInput[name] = $('#' + name).val();
						}
					});
				}

				var $errorMsg = "Some error occurred while submitting your data. Kindly refresh the page and try again.";
				var $ajaxUrl = $('#hidValidateOrderDetailsUrl').val();

				$.ajax({
					url: $ajaxUrl,
					dataType: 'json',
					data: userInput,
					type: 'POST',
					success: function (data) {
						window.hideLoading();
						if (data.success) {
							/* showContactDetails(); */
							showNextCheckoutCard();
						} else {
							if (data.errorMsg) {
								$errorMsg = data.errorMsg;
							}
							window.showNotyError($errorMsg);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						window.hideLoading();
						window.showNotyError($errorMsg);
						window.notifyTeam({
							"url": $ajaxUrl,
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			}

			function validateAdditionalDetails() {

			}

			function updatePriceDetails() {
				var $ajaxUrl = $('#hidUpdatePriceDetailsUrl').val();

				$.ajax({
					url: $ajaxUrl,
					dataType: 'json',
					type: 'POST',
					data: {},
					success: function (data) {
						if (data.success) {
							if (data.bookingAmount && data.advanceAmount) {
								totalBookingAmount = data.bookingAmount;
								totalAdvanceAmount = data.advanceAmount;
								/* do not update discount amount with ticket_booking->prepay_discount_amount */
								/* couponDiscountAmount = data.couponDiscountAmount; */
								orderPriceDetails = data.orderPriceDetails;

								updatePriceDetailsMarkup();
							} else {
								window.showNotyError("Some error occurred while submitting your data. Kindly refresh the page and try again");
							}
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						window.showNotyError("Some error occurred while submitting your data. Kindly refresh the page and try again");
						window.notifyTeam({
							"url": $ajaxUrl,
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			}

			function updatePriceDetailsMarkup() {
				var totalSavedAmount = 0;
				$('#advanceAmount').val(totalAdvanceAmount - couponDiscountAmount);
				$('.checkout-advance-amount').text(priceFormat(totalAdvanceAmount - couponDiscountAmount));
				$('.checkout-price-split-up-wrap').empty();

				var $element = "";
				if (orderPriceDetails) {
					$element += '<div class="checkout-price-split-up-orders">';
					orderPriceDetails.forEach(function (item) {
						/* @see: if transport price exists, then product price will definitely exist */
						/* added default in controller to fetch booking amount in the absence of product price */
						if (item['productBookingAmount']) {
							$element += '<div class="checkout-price-split-up-component checkout-price-split-up-order" data-product-id="' + item['productId'] + '" data-product-type-id="' + item['productTypeId'] + '">';
							$element += '<div class="col-xs-8 col-sm-8 text-left no-pad-l">';
							$element += '<span class="checkout-price-product-title">' + item['productTitle'] + '</span>';
							$element += '</div>';
							$element += '<div class="col-xs-4 col-sm-4 no-pad-l__400">';
							$element += '<span class="">';
							if (item['productWorthAmount']) {
								$element += '<div class="checkout-price-split-up-order-worth">';
								$element += '<span class="rupee-font">&#8377; </span>';
								$element += '<span class="">' + priceFormat(item['productWorthAmount']) + '</span>';
								$element += '</div>';
							}
							$element += '<div>';
							$element += '<span class="rupee-font">&#8377; </span>';
							$element += '<span class="">' + priceFormat(item['productPrice']) + '</span>';
							$element += '</div>';
							$element += '</span>';
							$element += '</div>';
							$element += '<div class="clearfix"></div>';
							if (item['isVenueBooking']) {
								/* No need to show delivery charges for venue bookings */
							} else {
								$element += '<div class="font-12">';
								$element += '<div class="col-xs-8 col-sm-8 text-left no-pad-l">';
								$element += '<span class="checkout-price-product-transport">Delivery Charges</span>';
								$element += '</div>';
								$element += '<div class="col-xs-4 col-sm-4 no-pad-l__400">';
								/* @see: if delivery charges needs to be fixed */
								/*
								 $element += '<span class="rupee-font">&#8377; </span>';
								 $element += '<span class="">' + priceFormat(100) + '</span>';
								 */
								if (item['productTransportCharges']) {
									$element += '<span class="rupee-font">&#8377; </span>';
									$element += '<span class="">' + priceFormat(item['productTransportCharges']) + '</span>';
								} else {
									$element += '<span class="text-g">Free</span>';
								}
								$element += '</div>';
								$element += '<div class="clearfix"></div>';
								$element += '</div>';
							}
							$element += '</div>';

							/* calculate total saved amount */
							var productSavedAmount = 0;
							if (item['productWorthAmount'] && ((item['productWorthAmount'] - item['productBookingAmount']) > 0)) {
								productSavedAmount = item['productWorthAmount'] - item['productBookingAmount'];
							}
							totalSavedAmount = totalSavedAmount + productSavedAmount;
						}
					});
					$element += '</div>';
				}

				/* @see: if coupon discount need to be fixed */
				/*
				 $element += '<div class="checkout-price-split-up-component checkout-price-split-up-coupon">';
				 $element += '<div class="col-xs-8 col-sm-8 text-left no-pad-l">';
				 $element += '<div class="checkout-price-product-title">Coupon Discount</div>';
				 $element += '</div>';
				 $element += '<div class="col-xs-4 col-sm-4 no-pad-l__400">';
				 $element += '<span class="text-g">';
				 $element += '- <span class="rupee-font">&#8377; </span>';
				 $element += '<span class="">' + priceFormat(100) + '</span>';
				 $element += '</span>';
				 $element += '</div>';
				 $element += '<div class="clearfix"></div>';
				 $element += '</div>';
				 */

				if (couponDiscountAmount) {
					$element += '<div class="checkout-price-split-up-component checkout-price-split-up-coupon">';
					$element += '<div class="col-xs-8 col-sm-8 text-left no-pad-l">';
					$element += '<span class="checkout-price-product-title">Coupon Discount</span>';
					$element += '</div>';
					$element += '<div class="col-xs-4 col-sm-4 no-pad-l__400">';
					$element += '<span class="text-g">';
					$element += '- <span class="rupee-font">&#8377; </span>';
					$element += '<span class="">' + priceFormat(couponDiscountAmount) + '</span>';
					$element += '</span>';
					$element += '</div>';
					$element += '<div class="clearfix"></div>';
					$element += '</div>';
				}

				$element += '<hr class="checkout-price-split-up-hr">';

				$element += '<div class="checkout-price-split-up-component text-bold">';
				$element += '<div class="col-xs-8 col-sm-8 text-left no-pad-l">';
				$element += '<span class="checkout-price-product-title">Total Order Amount</span>';
				$element += '</div>';
				$element += '<div class="col-xs-4 col-sm-4 no-pad-l__400">';
				$element += '<span class="">';
				$element += '<span class="rupee-font">&#8377; </span>';
				$element += '<span class="">' + priceFormat(totalBookingAmount - couponDiscountAmount) + '</span>';
				$element += '</span>';
				$element += '</div>';
				$element += '<div class="clearfix"></div>';
				$element += '</div>';

				/* @see: if ever coupon discount is fixed */
				/*
				 if ((totalSavedAmount) > 0) {
				 $element += '<div class="checkout-price-split-up-save text-center">';
				 $element += '<span class="">(You saved </span>';
				 $element += '<span class="">';
				 $element += '<span class="rupee-font">&#8377; </span>';
				 $element += '<span class="">' + priceFormat(totalSavedAmount + 100) + '</span>';
				 $element += '</span>';
				 $element += '<span class=""> on this purchase)</span>';
				 $element += '</div>';
				 }
				 */

				if ((totalSavedAmount + couponDiscountAmount) > 0) {
					$element += '<div class="checkout-price-split-up-save text-center">';
					$element += '<span class="">(You saved </span>';
					$element += '<span class="">';
					$element += '<span class="rupee-font">&#8377; </span>';
					$element += '<span class="">' + priceFormat(totalSavedAmount + couponDiscountAmount) + '</span>';
					$element += '</span>';
					$element += '<span class=""> on this purchase)</span>';
					$element += '</div>';
				}

				if (totalAdvanceAmount && (totalAdvanceAmount !== totalBookingAmount)) {
					$element += '<hr class="checkout-price-split-up-hr">';

					$element += '<div class="checkout-price-split-up-component text-bold">';
					$element += '<div class="col-xs-8 col-sm-8 text-left no-pad-l">';
					$element += '<span class="checkout-price-product-title">Advance To Pay</span>';
					$element += '</div>';
					$element += '<div class="col-xs-4 col-sm-4 no-pad-l__400">';
					$element += '<span class="text-e">';
					$element += '<span class="rupee-font">&#8377; </span>';
					$element += '<span class="">' + priceFormat(totalAdvanceAmount - couponDiscountAmount) + '</span>';
					$element += '</span>';
					$element += '</div>';
					$element += '<div class="clearfix"></div>';
					$element += '</div>';

					$element += '<div class="checkout-price-split-up-info text-center">';
					$element += '<span class="glyphicon glyphicon-info-sign"></span>';
					$element += '<span class="">Balance amount needs to be paid to our event co-ordinator(s)</span>';
					$element += '</div>';
				}

				$('.checkout-price-split-up-wrap').prepend($element);
			}

			function validateCheckoutCard($ref) {
				switch ($ref) {
					case 'contactDetails':
						validateContactDetails();
						break;
					case 'deliveryDetails':
						validateDeliveryDetails();
						break;
					case 'additionalDetails':
						validateAdditionalDetails();
						break;
					default:
						break;
				}
			}

			function updateCheckoutCardNavigation(elementRef) {
				$('.checkout-navigation-btn').removeClass('selected');
				$('.checkout-navigation-btn').removeClass('done');
				$('.checkout-navigation-btn').addClass('disabled');
				$('.checkout-navigation-btn').each(function (i, el) {
					if ($(el).data('ref') == elementRef) {
						presentCheckoutCardCount = $(el).data('checkout-card-count');

						if (presentCheckoutCardCount >= maxCheckoutCardCount) {
							maxCheckoutCardCount = presentCheckoutCardCount;
						}
					}
				});

				$('.checkout-navigation-btn').each(function (i, el) {
					var navCheckoutCardCount = $(el).data('checkout-card-count');

					if (navCheckoutCardCount === presentCheckoutCardCount) {
						$(el).addClass('selected');
					}

					/* rest of the nav links are any way disabled */
					if (navCheckoutCardCount < maxCheckoutCardCount) {
						$(el).addClass('done');
						$(el).removeClass('disabled');
					}
				});
			}

			function priceFormat($num) {
				$num = $num.toString();
				var lastThree = $num.substring($num.length - 3);
				var otherNumbers = $num.substring(0, $num.length - 3);
				if (otherNumbers != '')
					lastThree = ',' + lastThree;
				var $res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

				return $res;
			}

			$('.section-why-us').css('background', '#FFFFFF');

			$('.lazy-loading').each(function () {
				var img = $(this);
				img.attr('src', img.data('src'));
			});

			$('.checkout-navigation-btn').on('click', function () {
				var $targetNavElement = $(this);
				showNavTargetCheckoutCard($targetNavElement);
			});

			$('.checkout-card-details-submit-btn').on('click', function () {
				var $ref = $(this).parent().closest('.checkout-card-wrap').attr('id');

				nextCheckoutCardCount = +presentCheckoutCardCount + 1;
				validateCheckoutCard($ref);
			});

			var tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
			$(".additional-field-input-date").datetimepicker({
				timepicker: false,
				defaultDate: tomorrow,
				format: 'd M Y',
				scrollInput: false,
				scrollMonth: false,
				scrollTime: false,
				closeOnDateSelect: true,
				onSelectDate: function (ct, $i) {
					$i.parent().addClass('is-dirty');
				}
			});

			/* process payment */
			$('#payBtn').click(function (event) {
				event.preventDefault();

				window.showLoading();
				var that = $(this);
				that.addClass('hide');

				var selectedPaymentMethod = $("#paymentMethodPayU");
				var initUrl = $(selectedPaymentMethod).data('url');
				var gateway = $(selectedPaymentMethod).val();

				userInput.advanceAmount = $('#advanceAmount').val();

				userInput.name = $('#name').val();
				userInput.phone = $('#inpPhone').val();
				userInput.callingCode = $('#inpCallingCode').val();
				userInput.acceptsTerms = 1;
				userInput.title = $('#title').val();

				/* if auto booking, add email id */
				var $email = $('#inpEmail');
				if (parseInt($email.length, 10) === 1) {
					userInput.email = $email.val();
				}

				/* add venue info only for not venue booking */
				var $address = $('#address');
				if (parseInt($address.length, 10) === 1) {
					userInput.zipCode = $('#zipCode').val();
					userInput.address = $address.val();
					userInput.landmark = $('#landmark').val();
					userInput.city = $('#city').val();
					userInput.state = $('#state').val();
				}

				/* add additional details */
				var $additionalFields = {};
				$('.product-booking-additional-field').each(function (i, el) {
					var $typeFieldId = $(el).data('type-field-id');
					var $fieldId = $(el).data('additional-field-id');
					if ($typeFieldId == $('#hidInputTypeCheckbox').val()) {
						if (!$additionalFields[$fieldId]) {
							var checkedFields = $('input[data-additional-field-id="' + $fieldId + '"]:checked');
							var checkboxValue = '';
							$.each(checkedFields, function (key, value) {
								if (checkboxValue !== '') {
									checkboxValue = checkboxValue + ', ';
								}
								checkboxValue = checkboxValue + $(this).val();
							});
							$additionalFields[$fieldId] = checkboxValue;
						}
					} else {
						$additionalFields[$fieldId] = $('#' + $fieldId).val();
					}
				});
				console.log($additionalFields);
				userInput.additionalFields = $additionalFields;

				userInput.couponCode = couponCodeVal;

				$.ajax({
					url: initUrl,
					type: 'POST',
					dataType: 'json',
					data: userInput
				}).done(function (data, textStatus, jqXHR) {
					if (data.success) {
						if (gateway == "razor") {
							var paymentType = selectedPaymentMethod.data("paymentType");
							processRazorPostInit(data, paymentType);
						}
						if (gateway == "payU") {
							processPayUPostInit(data);
						}
						if (gateway == "freeCharge") {
							showNotyError("Selected freecharge payment option is not active right now, Please use other payment options to complete your payment");
						}
						if (gateway == "paytm") {
							processPaytmInit(data);
						}
					} else {
						reportAjaxFail(that, data.error);
					}
				}).fail(function (jqXHR, textStatus, errorThrown) {

					reportAjaxFail(that, null);
				});
			});

			/* update checkout cards with count */
			var checkoutStepCount = 1;
			$('.checkout-card-wrap').addClass('hide');
			$('.checkout-navigation-btn').each(function (i, el) {
				/* $(el).children('.step_no').text(checkoutStepCount); */
				$(el).data('checkout-card-count', checkoutStepCount);

				checkoutStepCount++;
			});

			/* using hashbangs to navigate through checkout page */
			window.onhashchange = function () {
				showCheckoutCardFromRef(location.hash.substr(1));
			};

			/* by this time, present checkout card count is 'o' */
			/* card change based on hashbang should only be attempted if it is checkout page */
			if (window.location.href.indexOf("checkout") > -1) {
				if (location.hash) {
					showCheckoutCardFromRef(location.hash.substr(1));
				} else {
					showFirstCheckoutCard();
				}
			}

			/* updating price card to be safe */
			if ($('#isValidStatus').val() == 1) {
				updatePriceDetails();
			}

			/* auto fill city and state on entering pin code */
			$('#zipCode').on('change', function () {

				if ($(this).val().length != 6) {
					return false;
				}

				updateDeliveryPinData($(this).val());
			});

			function updateDeliveryPinData($pinCode) {
				/* window.showLoading(); */
				/* make API call to fetch and compute data */

				var $ajaxUrl = '/party-kits/delivery/pin-data/' + $pinCode;
				$.ajax({
					url: $ajaxUrl,
					type: 'GET',
					dataType: 'json',
					data: {}
				}).done(function (data, textStatus, jqXHR) {
					if (data.success) {
						if (data.city) {
							$('#city').val(data.city);
							$('#city').parent().addClass('is-dirty');
						}
						if (data.state) {
							$('#state').val(data.state);
							$('#state').parent().addClass('is-dirty');
						}
						window.hideLoading();
					} else {
						$('#city').val('');
						$('#city').parent().removeClass('is-dirty');
						$('#state').val('');
						$('#state').parent().removeClass('is-dirty');
						window.hideLoading();
					}
				}).fail(function (jqXHR, textStatus, errorThrown) {
					window.hideLoading();
					window.notifyTeam({
						"url": $ajaxUrl,
						"textStatus": textStatus,
						"errorThrown": errorThrown
					});
				});
			}

			/* check if pin code already exists when the page loads */
			if ($('#zipCode').val().length == 6) {
				updateDeliveryPinData($('#zipCode').val());
			}
		});
	</script>
@endsection