@extends('layout.base')

@section('page-title')
	<title>Redirecting</title>
@endsection

@section('app')
	<div>
		<div class="col-sm-6 col-sm-offset-3">
			<div class="alert alert-info text-center mar-t-30">
				<h3 class="no-mar">Redirecting to Evibe.in, please wait...</h3>
				<div class="pad-t-10">
					<span class="text-danger font-18">(Please do not press refresh / back button)</span>
					<div class="pad-t-10">
						<img src="{{ $galleryUrl }}/img/icons/loading.gif" alt="loading">
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<input id='url' type='hidden' value="{{ $data['redirectTo'] }}"/>
@endsection

@section("javascript")
	<script type="text/javascript">
		$(document).ready(function () {
			$.ajax({
				url: $('#url').val(),
				type: 'POST',
				dataType: 'json',
				data: {}
			}).done(function (data, textStatus, jqXHR) {
				window.location.href = data.redirectTo;
			}).fail(function (jqXHR, textStatus, errorThrown) {
				window.showNotyError();
			});
		});
	</script>
@endsection