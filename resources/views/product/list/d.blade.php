@extends('product.list.c')

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			var slickJs = "<?php echo elixir('js/util/slick.js'); ?>";
			$.getScript(slickJs, function () {

				$('.list-piab-type-wrap').each(function (i, val) {
					var $productTypeId = $(this).data('product-type-id');

					$('#slickCarouser_' + $productTypeId).slick({
						dots: false,
						infinite: false,
						speed: 300,
						slidesToShow: 1,
						arrows: true
					});

					$('.list-piab-style-wrap[data-product-type-id=' + $productTypeId + ']').removeClass('hide');
					$('.list-piab-type-styles-loading[data-product-type-id=' + $productTypeId + ']').addClass('hide');
				});

			});

			/*
			 $('.footer-piab-reviews-wrap').removeClass('hide');
			 $('.footer-piab-reviews-wrap-loading').addClass('hide');
			 */

		});
	</script>
@endsection