@extends('product.list.c')

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			var slickJs = "<?php echo elixir('js/util/slick.js'); ?>";
			$.getScript(slickJs, function () {

				$('.list-piab-type-wrap').each(function (i, val) {
					var $productTypeId = $(this).data('product-type-id');

					$('#slickCarouser_' + $productTypeId).slick({
						dots: false,
						infinite: false,
						speed: 300,
						slidesToShow: 1,
						arrows: true
					});

					$('.list-piab-style-wrap[data-product-type-id=' + $productTypeId + ']').removeClass('hide');
					$('.list-piab-type-styles-loading[data-product-type-id=' + $productTypeId + ']').addClass('hide');
				});

				$('#slickCarouser_customerReviews').slick({
					dots: false,
					infinite: false,
					speed: 300,
					slidesToShow: 1,
					arrows: true
				});

				$('.footer-piab-review-wrap').removeClass('hide');

				/*
				 $('.footer-piab-reviews-wrap').removeClass('hide');
				 $('.footer-piab-reviews-wrap-loading').addClass('hide');
				 */


				var $maxHeight = 0;
				var $eleHeight = 0;
				$('.footer-piab-review-text-wrap').each(function (i, val) {
					$eleHeight = $(this).height();
					if ($eleHeight > $maxHeight) {
						$maxHeight = $eleHeight;
					}
				});

				$('.footer-piab-review-wrap').css('height', $maxHeight + 80);


			});

		});
	</script>
@endsection