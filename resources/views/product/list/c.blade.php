@extends('product.base')

@section("header")
	@include('product.header')
@endsection

@section('push-notifications')
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-list.css') }}"/>
@endsection

@section("content")
	@parent
	<div class="list-piab-container mar-t-15">
		@if(isset($data['partyKitsData']) && $data['partyKitsData'])
			<div class="list-piab-category-title hide">
				{{ $data['categoryName'] }} Party Kits
			</div>
			@foreach($data['partyKitsData'] as $productType)
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="list-piab-type-wrap mar-t-20" data-product-type-id="{{ $productType['id'] }}">
						<div class="pad-l-15 pad-r-15 text-center">
							<div class="list-piab-type-title mar-t-10">
								{{ $productType['name'] }} {{ $data['categoryName'] }} Party Kits
							</div>
							<div class="list-piab-type-text mar-t-10">
								{{ $productType['info'] }}
							</div>
						</div>
						<div class="list-piab-type-styles ">
							@if(isset($productType['styles']) && count($productType['styles']))
								<div id="slickCarouser_{{ $productType['id'] }}">
									@foreach($productType['styles'] as $productStyle)
										<div class="list-piab-style-wrap text-center mar-t-15 mar-b-10 hide" data-product-type-id="{{ $productType['id'] }}">
											<a class="list-piab-style-link" href="{{ route('party-kits.profile', ['categoryUrl' => $data['categoryUrl'], 'typeUrl' => $productType['url'], 'styleUrl' => $productStyle['url']]) }}?ref=mlp#productStyle">
												<div class="list-piab-style-img-wrap">
													@if(isset($productStyle['profileImgFullUrl']) && $productStyle['profileImgFullUrl'])
														<img class="list-piab-style-img" src="{{ $productStyle['profileImgFullUrl'] }}">
													@else
														<img class="list-piab-style-img" src="{{ $galleryUrl }}/img/icons/default-piab.jpg">
													@endif
												</div>
												<div class="mar-t-10">
													<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 no-pad-l">
														<div class="list-piab-style-title">
															{{ $productStyle['name'] }} Theme
														</div>
													</div>
													<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 no-pad">
														<div class="list-piab-style-price-wrap">
															<div class="list-piab-style-price-worth in-blk">
																@price($productStyle['priceWorth'])
															</div>
															<div class="list-piab-style-price in-blk mar-l-5">
																@price($productStyle['price'])
															</div>
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
											</a>
										</div>
									@endforeach
								</div>
							@else
								<div class="text-center font-16 text-e">
									Amazing party kits coming soon!
								</div>
							@endif
						</div>
						<div class="list-piab-type-styles-loading" data-product-type-id="{{ $productType['id'] }}">

						</div>
					</div>
				</div>
			@endforeach
			<div class="clearfix"></div>
		@else
			<div class="text-center font-24 mar-t-30 text-r">Uhu! Somethings not right. Kindly refresh the page and try again</div>
		@endif
	</div>
@endsection

@section("footer")
	@include('product.footer')
	<div class="no-mar-t pad-b-20">
		@include("product.usp")
	</div>
	<div class="footer-wrap">
		@include('base.home.footer.footer-non-city')
	</div>
@endsection