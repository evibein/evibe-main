<header>
	<div class="header-top header-piab" style="height: 74px; border-bottom: 1px solid #E0E0E0 !important;">
		<div class="in-blk mar-l-30 mar-t-15">
			<a class="" href="#">
				<img src="{{ $galleryUrl }}/img/logo/logo_evibe.png" alt="Evibe logo">
			</a>
		</div>
		@if(isset($data['productCategories']) && count($data['productCategories']) && !($agent->isMobile() && !($agent->isTablet())))
			<div class="header-piab-options-wrap in-blk mar-r-30 mar-l-60 mar-t-25">
				@foreach($data['productCategories'] as $productCategory)
					<a class="header-piab-option-text in-blk" href="{{ route('party-kits.list', $productCategory['url']) }}?ref=topHeader#link">
						{{ $productCategory['name'] }}
					</a>
				@endforeach
			</div>
		@else
			<div class="in-blk pull-right mar-r-30 mar-t-15">
				<div class="header-secure-img-wrap in-blk">
					<img class="header-secure-img secure-image" src="{{ config('evibe.gallery.host') }}/img/app/evibe_shield.png" alt="Evibe guarantee">
				</div>
				<div class="in-blk mar-l-5 text-bold">100% Secure</div>
			</div>
		@endif
		<div class="clearfix"></div>
	</div>
</header>