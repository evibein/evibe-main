@if (isset($isProfile) && $isProfile == 1)
	<div class="text-center">
		<div class="col-xs-4 no-pad-l no-pad-r">
			<img src="{{ $galleryUrl }}/img/icons/quality.png" style="border-radius: 50%; height: 50px; box-shadow: #4040401f 0 6px 12px;" alt="quality">
			<div class="mar-t-10 font-12">Quality<br> Supplies</div>
		</div>
		<div class="col-xs-4">
			<img src="{{ $galleryUrl }}/img/icons/deals.png" style="border-radius: 50%; height: 50px; box-shadow: #4040401f 0 6px 12px;" alt="best deals">
			<div class="mar-t-10 font-12">Unbeatable<br> Prices</div>
		</div>
		<div class="col-xs-4 no-pad-l no-pad-r">
			<img src="{{ $galleryUrl }}/img/icons/delivery.png" style="border-radius: 50%; height: 50px; box-shadow: #4040401f 0 6px 12px;" alt="free shipping">
			<div class="mar-t-10 font-12">FREE<br> Shipping</div>
		</div>
		<div class="clearfix"></div>
	</div>
@elseif ($agent->isMobile() && !($agent->isTablet()))
	<div class="text-center">
		<div class="in-blk" style="padding: 0 5%;">
			<img src="{{ $galleryUrl }}/img/icons/quality.png" style="border-radius: 50%; height: 60px; box-shadow: #4040401f 0 6px 12px;" alt="quality">
			<div class="mar-t-10 font-12">Quality Supplies</div>
		</div>
		<div class="in-blk" style="padding: 0 5%;">
			<img src="{{ $galleryUrl }}/img/icons/deals.png" style="border-radius: 50%; height: 60px; box-shadow: #4040401f 0 6px 12px;" alt="best deals">
			<div class="mar-t-10 font-12">Unbeatable Prices</div>
		</div>
		<div class="in-blk" style="padding: 0 5%;">
			<img src="{{ $galleryUrl }}/img/icons/delivery.png" style="border-radius: 50%; height: 60px; box-shadow: #4040401f 0 6px 12px;" alt="free shipping">
			<div class="mar-t-10 font-12">FREE Shipping</div>
		</div>
	</div>
@else
	<div class="text-center">
		<div class="in-blk" style="padding: 0 5%;">
			<img src="{{ $galleryUrl }}/img/icons/quality.png" style="border-radius: 50%; height: 80px; box-shadow: #4040401f 0 6px 12px;" alt="quality">
			<div class="mar-t-10">Quality Supplies</div>
		</div>
		<div class="in-blk" style="padding: 0 5%;">
			<img src="{{ $galleryUrl }}/img/icons/deals.png" style="border-radius: 50%; height: 80px; box-shadow: #4040401f 0 6px 12px;" alt="best deals">
			<div class="mar-t-10">Unbeatable Prices</div>
		</div>
		<div class="in-blk" style="padding: 0 5%;">
			<img src="{{ $galleryUrl }}/img/icons/delivery.png" style="border-radius: 50%; height: 80px; box-shadow: #4040401f 0 6px 12px;" alt="free shipping">
			<div class="mar-t-10">FREE Shipping</div>
		</div>
	</div>
@endif