@extends('product.home.c')

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			$('.home-piab-option-link').on('click', function (event) {
				event.preventDefault();

				if ($(this).data('is-live')) {
					window.location.href = $(this).data('url');
				} else {
					/* do nothing */
				}
			});
		});
	</script>
@endsection