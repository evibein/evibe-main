@extends('product.base')

@section("header")
	@include('product.header')
@endsection

@section('push-notifications')
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-home.css') }}"/>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			$('.home-piab-option-link').on('click', function (event) {
				event.preventDefault();

				if ($(this).data('is-live')) {
					window.location.href = $(this).data('url');
				} else {
					/* do nothing */
				}
			});
		});
	</script>
@endsection

@section("content")
	<div class="bg-white home-piab-container mar-b-30">
		<div class="">
			<div class="home-piab-intro-wrap" style="background-image: url({{ $galleryUrl }}/main/img/hero-piab.jpg)">
				<div class="home-piab-intro-text-wrap">
					<div class="home-piab-intro-text">
						<div>Latest party kits</div>
						<div>Delivered to your doorstep</div>
					</div>
				</div>
			</div>
			<div class="mar-t-30">
				@include("product.usp")
			</div>
			@if(isset($data['productCategoriesList']) && count($data['productCategoriesList']))
				<div class="container">
					<div class="list-options-section-wrap mar-t-30">
						@php $i = 0; @endphp
						@foreach($data['productCategoriesList'] as $productCategory)
							@php $i++; @endphp
							<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 pad-l-10 pad-r-10">
								<div class="home-piab-option-wrap">
									<a class="home-piab-option-link @if(!$productCategory['isLive']) home-piab-option-link-disabled @endif" href="#" data-is-live="{{ $productCategory['isLive'] }}" data-url="{{ route('party-kits.list', $productCategory['url']) }}?ref=lp#image">
										@if(isset($productCategory['imgFullUrl']) && $productCategory['imgFullUrl'])
											<img class="" src="{{ $productCategory['imgFullUrl'] }}">
										@else
											<img class="" src="{{ $galleryUrl }}/img/icons/default-piab.jpg">
										@endif
										<div class="home-piab-option-bg"></div>
										<div class="home-piab-option-title">
											{{ $productCategory['name'] }}
										</div>
										<div class="home-piab-option-text">
											{{ $productCategory['info'] }}
										</div>
									</a>
								</div>
							</div>
							@if(($i % 3) == 0)
								<div class="clearfix"></div>
							@endif
						@endforeach
						@if(($i % 3) != 0)
							<div class="clearfix"></div>
						@endif
					</div>
				</div>
			@else
				<div class="list-content-empty-wrap text-center mar-t-30">
					<div class="list-content-empty-img">
						<div class="no-results-wrap text-center">
							<h4 class="text-col-gr no-mar">We are adding amazing party sets soon, keep checking this page.</h4>
						</div>
					</div>
				</div>
			@endif
		</div>
	</div>
@endsection

@section("footer")
	@include('product.footer')
	<div class="footer-wrap">
		@include('base.home.footer.footer-non-city')
	</div>
@endsection