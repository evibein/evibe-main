@extends('product.home.c')

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			var slickJs = "<?php echo elixir('js/util/slick.js'); ?>";
			$.getScript(slickJs, function () {

				$('#slickCarouser_customerReviews').slick({
					dots: false,
					infinite: false,
					speed: 300,
					slidesToShow: 1,
					arrows: true
				});

				$('.footer-piab-review-wrap').removeClass('hide');

				var $maxHeight = 0;
				var $eleHeight = 0;
				$('.footer-piab-review-text-wrap').each(function (i, val) {
					$eleHeight = $(this).height();
					if ($eleHeight > $maxHeight) {
						$maxHeight = $eleHeight;
					}
				});

				$('.footer-piab-review-wrap').css('height', $maxHeight + 80);

			});

		});
	</script>
@endsection