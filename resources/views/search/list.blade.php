@extends('layout.base')

@section('page-title')
	<title>{{ $data['seo']['pageTitle'] }} | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="{{ $data['seo']['pageDescription'] }}"/>
@endsection

@section('meta-keywords')
	<meta name="keywords" content="evibe.in, partner sign up, {{ $cityName }} kids party partners sign up"/>
@endsection

@section('og-title')
	<meta property="og:title" content="{{ $data['seo']['pageTitle'] }} | Evibe.in"/>
@endsection

@section('og-description')
	<meta property="og:description" content="{{ $data['seo']['pageDescription'] }} "/>
@endsection

@section("header")
	@include('base.home.header.header-home-city')
@endsection

@section('og-url')
	<meta property="og:url" content="{{ route("city.search", $cityUrl) }}"/>
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-list.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/app/page-gs.css') }}"/>
@endsection

@section("content") 
	<div class="w-1366 header-hr-line"></div>
	<div class="w-1366 items-results pad-t-10 no-pad-b">
		<div class="full-width">
			@if(count($data['results']) > 0 || $data['filters']['clearFilter'])
				<div class="clearfix"></div>
				<div class="in-blk no-pad-t pad-l-15 mar-b-5"> <!-- for aligning with bottom panel -->
					<h1 class="text-cap no-mar-t no-mar-b search-result-title font-16 ls-normal">
						@if($data['searchProductResults']->total())
							@if($data['searchProductResults']->total() > 48)
								<b><span>{{ $data['searchProductResults']->total() }}</span></b>
							@else
								<b><span>{{ count($data['results']) }}</span></b>
							@endif
							<span> Results for "{{ $data['query'] }}" in <a class="select-city clr-black global-search-city-a">{{ getCityName() }}</a></span>
						@endif
					</h1>
				</div>
				@if($agent->isMobile())
					<hr class="hr-text no-mar-t" style="height: 0.5em; margin-bottom: 7px;">
				@endif
				<div class="in-blk pull-right pad-r-15">
					<div class="mar-b-5 clr-black global-search-price-filter-wrap">
						<span class="text-muted">Sort By: </span>
						<a class="pad-l-5 global-search-price-filter-a @if(request("price") == "") global-search-price-filter-active @endif">Popular</a>
						<a class="pad-l-10 global-search-price-filter-a @if(request("price") == "asc") global-search-price-filter-active @endif" data-url="asc">
							<span>Price</span>
							<i class="glyphicon glyphicon-arrow-up"></i>
						</a>
						<a class="pad-l-10 global-search-price-filter-a @if(request("price") == "desc") global-search-price-filter-active @endif" data-url="desc">
							<span>Price</span>
							<i class="glyphicon glyphicon-arrow-down"></i>
						</a>
					</div>
				</div>
			@endif
			<div class="bottom-panel">
				@if(count($data['results']) > 0)
					<div class="results-list">
						@foreach($data['results'] as $card)
							<div class="col-md-3 col-lg-2 col-sm-4 col-xs-6 no-pad">
								<div class="des-list-card-wrap pad-5">
									<div class="list-option-card des-list-option-card search-list-option-card">
										<div class="list-option-image-wrap text-left">
											<a class="list-option-link option-profile-image " href="{{ $card['fullUrl'] . "?utm_source=search&utm_campaign=search&utm_medium=website&utm_term=" . $data['query'] }}">
												<img data-src="{{ $card['profileImg'] }}" alt="{{ $card['title']. "-background thumbnail image" }}" title="{{ $card['title'] }}" class="list-option-image search-list-option-image lazy-loading" style="width:auto; z-index:3;position:absolute;left:50%;transform:translate(-50%)">
												<img data-src="{{ $card['profileImg'] }}" alt="{{ $card['title'] }}" title="{{ $card['title'] }}" class="list-option-image search-list-option-image lazy-loading" style="width:100%; z-index:2;position:absolute;filter:blur(5px);left:0;height:auto">

											</a>
											<div class="hide">
												@if (isset($card['occasionId']))
													@include('app.shortlist_results', [
																"mapId" => $card['id'],
																"mapTypeId" => $card['mapTypeId'],
																"occasionId" => $card['occasionId'],
																"cityId" => $data['cityId']
															])
												@endif
											</div>
										</div>
										<div class="list-option-content-wrap">
											<div class="list-option-title">
												<a class="list-option-link" href="{{ $card['fullUrl'] }}"> {{ $card['title'] }}</a>
											</div>
											<div class="list-option-cta-wrap text-center">
												<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-pad">
													<a href="{{ $card['fullUrl'] }}?#bookNowModal" class="item-book-now" target="_blank">
														<div class="list-cta-btn">Book Now</div>
													</a>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="list-option-price price-tag">
												@if ($card['price'] != 0)
													@if ($card['worth'])
														<div class="in-blk list-price-worth">@price($card['worth'])</div>
													@endif
													<div class="in-blk list-main-price pad-l-5">@price($card['price'])</div>
												@else
													<span>----</span>
												@endif
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						@endforeach
						<div class="clearfix"></div>
					</div>
					<div class="pages text-center"> <!-- pagination begin -->
						<div>{{ $data['searchProductResults']->appends($data['filters']['queryParams'])->links() }}</div>
					</div>
				@else
					<div class="global-no-results-wrap text-center">
						@if ($data['filters']['clearFilter'])
							<h4 class="text-col-gr no-mar">
								<i class="glyphicon glyphicon-warning-sign"></i>
								Oops, no results were found matching your filters
							</h4>

							<div class="pad-t-20">
								<a href="{{route("city.search", $cityUrl)}}?q={{ $data['query'] }}&ref=gs-no-results" class="btn btn-danger btn-lg">
									<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
								</a>
							</div>
						@else
							<h4 class="text-col-gr no-mar-t">
								Sorry, we couldn't find any matches for <i><b>{{$data['query']}}</b></i>, modify your query a bit and try again.
							</h4>

							<div class="mar-t-20">
								<form method="get" action="{{ route('city.search', $cityUrl) }}" autocomplete="off" class="form-horizontal" role="search" id="no-results-global-search-form">
									<div class="input-group input-group-lg col-lg-10 col-lg-offset-1">
										<input type="text" name="q" class="form-control no-results-search" placeholder="Search for Theme Decors, Cakes, Resorts etc." onfocus="noResultsInputFocusedDesktop()" onblur="noResultsInputBlurredDesktop()">
										<span class="input-group-btn">
												<button class="btn btn-default no-results-submit" type="submit">
													<i class="glyphicon glyphicon-search"></i>
												</button>
											</span>
									</div>
								</form>
							</div>
							<div class="mar-t-20 popular-search">
								<!-- Get these from backend -->
								<span>Popular Searches: </span>
								<span>
											<a href="{{route("city.search", $cityUrl)}}?q=Theme+Decor&ref=gs-ps-no-results">Theme Decor</a>,
										</span>
								<span>
											<a href="{{route("city.search", $cityUrl)}}?q=Cakes&ref=gs-ps-no-results">&nbsp;Cakes</a>,
										</span>
								<span>
											<a href="{{route("city.search", $cityUrl)}}?q=Entertainment&ref=gs-ps-no-results"> &nbsp;Entertainment</a>,
										</span>
								<span>
											<a href="{{route("city.search", $cityUrl)}}?q=Pool+Side&ref=gs-ps-no-results">&nbsp;Pool Side</a>,
										</span>
								<span>
											<a href="{{route("city.search", $cityUrl)}}?q=Candle+Light+Dinners&ref=gs-ps-no-results">Candle Light Dinners</a>,
										</span>
								<span>
											<a href="{{route("city.search", $cityUrl)}}?q=Snacks&ref=gs-ps-no-results">&nbsp;Snacks</a>,
										</span>
								<span>
											<a href="{{route("city.search", $cityUrl)}}?q=Villas&ref=gs-ps-no-results">&nbsp;Villas</a>,
										</span>
								<span>
											<a href="{{route("city.search", $cityUrl)}}?q=Resorts&ref=gs-ps-no-results">&nbsp;Resorts</a>,
										</span>
								<span>
											<a href="{{route("city.search", $cityUrl)}}?q=Flowers&ref=gs-ps-no-results">&nbsp;Flowers</a>,
										</span>
								<span>
											<a href="{{route("city.search", $cityUrl)}}?q=Banquet+Halls&ref=gs-ps-no-results">&nbsp;Banquet Halls</a>
										</span>
							</div>
							<div style="margin-top:100px">
								@include("base.home.enquiry-form");
							</div>

						@endif
					</div>
				@endif
			</div>
			<input type="hidden" id="globalSearchKeyword" value="{{ $data['query'] }}">
			@include('app.modals.auto-popup',
			[
			"cityId" => $data['cityId'],
			"occasionId" => "",
			"mapTypeId" => "",
			"mapId" => "",
			"optionName" => ""
			])
		</div>
	</div>
@endsection

@section("footer")
	@include('base.home.footer.footer-common')
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			var listJs = "<?php echo elixir('js/app/results_util.js'); ?>";
			$.getScript(listJs);

			$.getScript("/js/util/lazyLoadImages.js", function () {
				$(".search-list-option-image").removeClass("lazy-loading");
				new LazyLoad();
			});

			(function () {
				var globalSearchForm = $('#no-results-global-search-form');
				if (globalSearchForm.length) {
					globalSearchForm.on('submit', function (e) {
						var globalSearchInput = e.currentTarget[0].value;
						if (globalSearchInput.trim() !== '') {
							e.currentTarget[0].value = globalSearchInput.trim();
						}
					});
				}
			})();

			(function filters() {
				var currentUrl = 'https://' + window.location.hostname + window.location.pathname;
				currentUrl = currentUrl + "?q=" + $("#globalSearchKeyword").val();

				$(".global-search-category-link").on("click", function () {
					if ($(this).data("url") > 0) {
						currentUrl = currentUrl + "&category=" + $(this).data("url");
					}
					if (($(".global-search-price-filter-active").data("url") === "asc") || ($(".global-search-price-filter-active").data("url") === "desc")) {
						currentUrl = currentUrl + "&price=" + $(".global-search-price-filter-active").data("url");
					}

					location.href = currentUrl;
				});

				$(".global-search-price-filter-a").on("click", function () {
					if ($(".global-search-category-wrap-active").find(".global-search-category-link").data("url") > 0) {
						currentUrl = currentUrl + "&category=" + $(".global-search-category-wrap-active").find(".global-search-category-link").data("url");
					}
					if (($(this).data("url") === "asc") || ($(this).data("url") === "desc")) {
						currentUrl = currentUrl + "&price=" + $(this).data("url");
					}
					location.href = currentUrl;
				})
			})();
		});

		/* display borders */
		function noResultsInputFocusedDesktop() {
			this.event.currentTarget.classList.add('border-gs-input');
			this.event.currentTarget.nextElementSibling.children[0].classList.add('border-gs-input-group-btn');
		}

		/* hide borders */
		function noResultsInputBlurredDesktop() {
			this.event.currentTarget.classList.remove('border-gs-input');
			this.event.currentTarget.nextElementSibling.children[0].classList.remove('border-gs-input-group-btn');
		}
	</script>
@endsection