@extends('invites.home')
@section("title")
	@if(isset($data['inviteTemplate']) && isset($data['inviteTemplate']->name))
		Download {{ $data['inviteTemplate']->name }} Theme For Free | Evibe.in
	@else
		Download ecard For Free | Evibe.in
	@endif
@endsection

@section("meta-description")
	@if(isset($data['inviteTemplate']) && isset($data['inviteTemplate']->name))
		<meta name="description" content="Download {{ $data['inviteTemplate']->name }} Theme And Share With Your Loved one's.">
	@else
		<meta name="description" content="Download ecard And Share With Your Loved one's.">
	@endif
@endsection

@section('meta-kw')
	@if(isset($data['inviteTemplate']) && isset($data['inviteTemplate']->name))
		<meta name="keywords" content="{{ $data['inviteTemplate']->name }}, Free Invite templates, Easy to customize "/>
	@else
		<meta name="keywords" content="Free Invite templates, Easy to customize"/>
	@endif
@endsection

@section('og-description')
	@if(isset($data['inviteTemplate']) && isset($data['inviteTemplate']->name))
		<meta property="og:description" content="Download {{ $data['inviteTemplate']->name }} Theme And Share With Your Loved one's."/>
	@else
		<meta property="og:description" content="Download ecards And Share With Your Loved one's."/>
	@endif
@endsection

@section('invites-step')
	<div style="max-width: 1366px; margin: 0 auto;">
		@if(isset($data['designUrl']) && $data['designUrl'])
			<div class="invite-design-wrap">
				@if ($agent->isMobile() && !($agent->isTablet()))
					@if(!is_null($data["inviteTemplate"]->type_card_category_id))
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<div id="inviteDesignTemplate" class="invite-design-template mar-t-20">
								<div class="invite-design-img-wrap">
									<img class="invite-design-img" alt="invite design" src="{{ $data['designUrl'] . "?ref=" . substr(time(), -5) }}">
								</div>
							</div>
							<div class="invite-design-steps mar-t-15">
								<div class="mar-t-30 font-15">
									<div class="invite-design-step-wrap">
										<div>
											<div class="in-blk pull-left col-xs-6" style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
												1 x Premium {{ $data["inviteTemplate"]->name }}<br><span class="font-12">(without watermark)</span>
											</div>
											<div class="in-blk col-xs-6">
												<span style="font-size: 22px; color: #ED3E72; float: right;">@price($data["inviteTemplate"]->cardCategory->price)</span>
												<span style="color: #878787; text-decoration: line-through; padding-right: 10px; float: right;">@price($data["inviteTemplate"]->cardCategory->worth)</span>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="text-center">
											<button type="submit" id="payInviteBtn" class="btn btn-evibe mar-t-15">
												Pay and Download
											</button>
											<form id="inviteDownloadForm" action="{{ route('iv.create.download') }}?imagePath={{ $data['designPath'] }}" method="POST">
												<button type="submit" id="inviteDownloadBtn" class="btn btn-link text-muted invite-download-btn font-12 no-pad-t mar-t-5 no-pad-b">
													Download a watermarked E-Card
												</button>
											</form>
										</div>
									</div>
								</div>
							</div>
							<div class="text-center mar-t-20 mar-b-15">
								<div class="in-blk mar-r-10">
									<a class="btn btn-default" href="{{ route("iv.create.party-details", [$data['inviteTemplate']->seo_url ,"id" => $data['ticketInvite']->id, "tkn" => $data['editHashTkn']]) }}">Edit E-Card</a>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					@else
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<div id="inviteDesignTemplate" class="invite-design-template mar-t-20">
								<div class="invite-design-img-wrap">
									<img class="invite-design-img" alt="invite design" src="{{ $data['designUrl'] . "?ref=" . substr(time(), -5) }}">
								</div>
							</div>
							<div class="text-center mar-t-20 mar-b-15">
								<div class="in-blk mar-r-10">
									<a class="btn btn-default" href="{{ route("iv.create.party-details", [$data['inviteTemplate']->seo_url ,"id" => $data['ticketInvite']->id, "tkn" => $data['editHashTkn']]) }}">Edit E-Card</a>
								</div>
								<form id="inviteDownloadForm" action="{{ route('iv.create.download') }}?imagePath={{ $data['designPath'] }}" class="in-blk" method="POST">
									<button type="submit" id="inviteDownloadBtn" class="btn btn-evibe invite-download-btn">Download E-Card</button>
								</form>
							</div>
						</div>
						<div class="clearfix"></div>
					@endif
				@else
					@if(!is_null($data["inviteTemplate"]->type_card_category_id))
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<div id="inviteDesignTemplate" class="invite-design-template mar-t-20">
								<div class="invite-design-img-wrap">
									<img class="invite-design-img" alt="invite design" src="{{ $data['designUrl'] . "?ref=" . substr(time(), -5) }}">
								</div>
							</div>
							@if(isset($data['RSVPLink']) && ($data['RSVPLink']))
								<div class="">
									<!-- show RSVP link -->
									<!-- copy RSVP link -->
								</div>
							@endif
							<div class="text-center mar-t-20 mar-b-30">
								<div class="in-blk">
									<a class="btn btn-default" href="{{ route("iv.create.party-details", [$data['inviteTemplate']->seo_url ,"id" => $data['ticketInvite']->id, "tkn" => $data['editHashTkn']]) }}">Edit E-Card</a>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<div class="invite-design-steps mar-t-20">
								<div class="mar-t-30 font-15">
									<div class="invite-design-step-wrap">
										<div class="text-center">
											<div class="col-md-10 col-md-offset-1 mar-t-5">
												<div class="in-blk pull-left col-md-6" style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
													1 x Premium {{ $data["inviteTemplate"]->name }}<br><span class="font-12">(without watermark)</span>
												</div>
												<div class="in-blk pull-right col-md-6">
													<span style="color: #878787; text-decoration: line-through; padding-right: 10px;">@price($data["inviteTemplate"]->cardCategory->worth)</span>
													<span style="font-size: 22px; color: #ED3E72;">@price($data["inviteTemplate"]->cardCategory->price)</span>
												</div>
											</div>
											<div class="clearfix"></div>
											<button type="submit" id="payInviteBtn" class="btn btn-evibe mar-t-15">
												Pay and Download
											</button>
											<form id="inviteDownloadForm" action="{{ route('iv.create.download') }}?imagePath={{ $data['designPath'] }}" method="POST">
												<button type="submit" id="inviteDownloadBtn" class="btn btn-link text-muted invite-download-btn font-12 no-pad-t mar-t-10 no-pad-b">
													Download a watermarked E-Card
												</button>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					@else
						<div class="col-xs-6 col-xs-offset-3">
							<div id="inviteDesignTemplate" class="invite-design-template mar-t-20">
								<div class="invite-design-img-wrap">
									<img class="invite-design-img" alt="invite design" src="{{ $data['designUrl'] . "?ref=" . substr(time(), -5) }}">
								</div>
							</div>
							<div class="text-center mar-t-20 mar-b-30">
								<div class="in-blk mar-r-10">
									<a class="btn btn-default" href="{{ route("iv.create.party-details", [$data['inviteTemplate']->seo_url ,"id" => $data['ticketInvite']->id, "tkn" => $data['editHashTkn']]) }}">Edit E-Card</a>
								</div>
								<form id="inviteDownloadForm" action="{{ route('iv.create.download') }}?imagePath={{ $data['designPath'] }}" class="in-blk" method="POST">
									<button type="submit" id="inviteDownloadBtn" class="btn btn-evibe invite-download-btn">
										Download E-Card
									</button>
								</form>
							</div>
						</div>
						<div class="clearfix"></div>
					@endif
				@endif
			</div>
		@else
			<div class="">
				Oops! Some error occurred while generating the e-card. Kindly refresh the page and try again. If the issue still persists, kindly write to {{ config('evibe.contact.invitations.group') }}
			</div>
		@endif
	</div>
@endsection