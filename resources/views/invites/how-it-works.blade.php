<div class="container">
	<div class="col-sm-4 col-xs-12 no-pad-l no-pad-r">
		<div class="col-xs-2 no-pad-r">
			<div class="how-it-works-number">01</div>
		</div>
		<div class="col-xs-10 pad-l-5">
			<p class="how-it-works-title">Choose the perfect design</p>
			<div class="how-it-works-description">Find a free or premium invitation that’s perfect for your event.</div>
		</div>
	</div>
	<div class="col-sm-4 col-xs-12 no-pad-l no-pad-r">
		<div class="col-xs-2 no-pad-r">
			<div class="how-it-works-number">02</div>
		</div>
		<div class="col-xs-10 pad-l-5">
			<p class="how-it-works-title">Make it your own</p>
			<div class="how-it-works-description">Put your personal touch on the design by adding your event’s details.</div>
		</div>
	</div>
	<div class="col-sm-4 col-xs-12 no-pad-l no-pad-r">
		<div class="col-xs-2 no-pad-r">
			<div class="how-it-works-number">03</div>
		</div>
		<div class="col-xs-10 pad-l-5">
			<p class="how-it-works-title">START SPREADING THE NEWS</p>
			<div class="how-it-works-description">Download & share it with your guests on the go.</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>