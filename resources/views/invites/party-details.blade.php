@extends('invites.home')

@section("title")
	@if(isset($data['inviteTemplate']) && isset($data['inviteTemplate']->name))
		Customize {{$data['inviteTemplate']->name}} Theme To Download | Evibe.in
	@else
		Customize your own ecard | Evibe.in
	@endif
@endsection

@section("meta-description")
	@if(isset($data['inviteTemplate']) && isset($data['inviteTemplate']->name))
		<meta name="description" content="Get your {{$data['inviteTemplate']->name}} in 3 steps. Easy To Customize.">
	@else
		<meta name="description" content="Get your ecard in 3 steps. Easy To Customize.">
	@endif
@endsection
@section('meta-kw')
	@if(isset($data['inviteTemplate']) && isset($data['inviteTemplate']->name))
		<meta name="keywords" content="{{$data['inviteTemplate']->name}}, Download Free Invite, Easy to customize"/>
	@else
		<meta name="keywords" content="Download Free Invite, Easy to customize"/>
	@endif
@endsection

@section('og-description')
	@if(isset($data['inviteTemplate']) && isset($data['inviteTemplate']->name))
		<meta property="og:description" content="Get your {{$data['inviteTemplate']->name}} in 3 steps. Easy To Customize."/>
	@else
		<meta property="og:description" content="Get your ecard in 3 steps. Easy To Customize."/>
	@endif
@endsection

@section('invites-step')
	@if ($agent->isMobile() && !($agent->isTablet()))
		<div class="mar-t-10">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				@if(isset($data['inviteTemplate']) && isset($data['inviteTemplate']->name) && isset($data['inviteTemplate']->type_card_id))
					<ol class="breadcrumb product-breadcrumb no-pad-t no-pad-b">
						@if($data['inviteTemplate']->type_card_id == config("evibe.type-card.invite"))
							<li><a href="{{ route('e-cards.invites') }}?ref=ivscreen2">Templates</a></li>
						@elseif($data['inviteTemplate']->type_card_id == config("evibe.type-card.wish-card"))
							<li><a href="{{ route('e-cards.wish-cards') }}?ref=ivscreen2">Templates</a></li>
						@elseif($data['inviteTemplate']->type_card_id == config("evibe.type-card.thank-you-card"))
							<li><a href="{{ route('e-cards.thank-you-cards') }}?ref=ivscreen2">Templates</a></li>
						@endif
						<li class="active">{{$data['inviteTemplate']->name}}</li>
					</ol>
				@endif
				<div class="invite-step-title no-mar-t">
					<h1 class="no-mar-t mar-b-15 font-20">
						Enter your party details
					</h1>
				</div>
				@if (isset($data['errorMsg']) && $data['errorMsg'])
					<div class="errors-cnt alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<ul class="erros-list list-unstyled">
							<li>{{ $data['errorMsg'] }}</li>
						</ul>
					</div>
				@endif
				@if(isset($data['inviteTemplateFields']) && count($data['inviteTemplateFields']))
					<div class="invite-party-details-wrap mar-b-30">
						<form id="inviteDetailsForm" method="GET" action="">
							<input type="hidden" id="inviteTemplateId" name="inviteTemplateId" value="{{ $data['inviteTemplateId'] }}">
							<div class="form-fields">
								@foreach($data['inviteTemplateFields'] as $dataField)
									<div class="pad-t-20">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad">
											<div class="form-group table">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad-l no-pad-r text-right">
													<label class="invite-details-field-name" for="{{ $dataField->name }}">{{ $dataField->identifier }}*</label>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-pad-r">
													@php $value = "";
														if(isset($data['fieldValues']) && count($data['fieldValues']))
														foreach($data['fieldValues'] as $fieldValue){
															if($fieldValue->invite_template_field_id == $dataField->id)
															$value = $fieldValue->value;
														}
													@endphp
													@if($dataField->type_field_id == config('evibe.input.text'))
														<input id="{{ $dataField->name }}" name="{{ $dataField->name }}" type="text" class="form-control invite-party-detail" data-party-field-name="{{ $dataField->name }}" placeholder="{{ $dataField->hint_message }}" value="{{ $value }}" maxlength="{{ $dataField->character_limit }}">
														@if($dataField->character_limit)
															<div class="invite-details-field-hint">(You can enter maximum up to {{ $dataField->character_limit }} characters)</div>
														@endif
													@elseif($dataField->type_field_id == config('evibe.input.textarea'))
													@elseif($dataField->type_field_id == config('evibe.input.number'))
														<input id="{{ $dataField->name }}" name="{{ $dataField->name }}" type="number" class="form-control invite-party-detail" data-party-field-name="{{ $dataField->name }}" placeholder="{{ $dataField->hint_message }}" value="{{str_replace(" ", "",$value)}}" maxlength="{{ $dataField->character_limit }}">
													@elseif($dataField->type_field_id == config('evibe.input.date'))
														<input id="{{ $dataField->name }}" name="{{ $dataField->name }}" type="text" class="party-date-time form-control invite-party-detail" data-party-field-name="{{ $dataField->name }}" value="{{ $value }}">
													@endif
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								@endforeach
							</div>
							<div class="clearfix"></div>
							<div class="text-center mar-t-20 mar-b-20">
								<button type="submit" class="btn-party-details-next btn">
									<span class="mar-r-5">Get My E-Card</span>
								</button>
							</div>
						</form>
						<hr class="hr-text">
						@if(isset($data['inviteTemplate']) && $data['inviteTemplate']->sample_url)
							<div class="invite-sample-text font-italic text-muted font-12">
								<span class="glyphicon glyphicon-info-sign"></span> This is a sample invitation and you will receive your invitation with your party details in the next step.
							</div>
							<div class="invite-sample-template text-center mar-b-5 mar-t-10">
								<div class="invite-sample-img-wrap">
									<img class="invite-sample-img" src="{{ $galleryUrl }}/invibe/templates/{{ $data['inviteTemplate']->sample_url }}">
								</div>
								<div class="clearfix"></div>
							</div>
						@endif
					</div>
				@else
					<div class="">
						Oops! Some error occurred while fetching input details for your invitation. Kindly refresh the page and try again. If the issue still persists, kindly write to {{ config('evibe.contact.invitations.group') }}
					</div>
				@endif
			</div>
			<div class="clearfix"></div>
		</div>
	@else
		<div class="container">
			<div class="mar-t-15 mar-b-15">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="product-breadcrumb-section pad-b-5">
						@if(isset($data['inviteTemplate']) && isset($data['inviteTemplate']->name) && isset($data['inviteTemplate']->type_card_id))
							<ol class="breadcrumb product-breadcrumb text-center__400">
								@if($data['inviteTemplate']->type_card_id == config("evibe.type-card.invite"))
									<li><a href="{{ route('e-cards.invites') }}?ref=ivscreen2">Templates</a></li>
								@elseif($data['inviteTemplate']->type_card_id == config("evibe.type-card.wish-card"))
									<li><a href="{{ route('e-cards.wish-cards') }}?ref=ivscreen2">Templates</a></li>
								@elseif($data['inviteTemplate']->type_card_id == config("evibe.type-card.thank-you-card"))
									<li><a href="{{ route('e-cards.thank-you-cards') }}?ref=ivscreen2">Templates</a></li>
								@endif
								<li class="active">{{$data['inviteTemplate']->name}}</li>
							</ol>
						@endif
					</div>
					<div class="invite-step-title no-mar-t">
						<h1 class="no-mar-t mar-b-15 font-22">
							Enter your party details
						</h1>
					</div>
					@if (isset($data['errorMsg']) && $data['errorMsg'])
						<div class="errors-cnt alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<ul class="erros-list list-unstyled">
								<li>{{ $data['errorMsg'] }}</li>
							</ul>
						</div>
					@endif
					@if(isset($data['inviteTemplateFields']) && count($data['inviteTemplateFields']))
						<div class="invite-party-details-wrap mar-b-30">
							<form id="inviteDetailsForm" method="GET" action="">
								<input type="hidden" id="inviteTemplateId" name="inviteTemplateId" value="{{ $data['inviteTemplateId'] }}">
								<div class="form-fields">
									@foreach($data['inviteTemplateFields'] as $dataField)
										<div class="pad-t-20">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad">
												<div class="form-group table">
													<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad-l no-pad-r text-right">
														<label class="invite-details-field-name" for="{{ $dataField->name }}">{{ $dataField->identifier }}*</label>
													</div>
													<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-pad-r">
														@php $value = "";
														if(isset($data['fieldValues']) && count($data['fieldValues']))
														foreach($data['fieldValues'] as $fieldValue){
															if($fieldValue->invite_template_field_id == $dataField->id)
															$value = $fieldValue->value;
														}
														@endphp
														@if($dataField->type_field_id == config('evibe.input.text'))
															<input id="{{ $dataField->name }}" name="{{ $dataField->name }}" type="text" class="form-control invite-party-detail" data-party-field-name="{{ $dataField->name }}" placeholder="{{ $dataField->hint_message }}" value="{{ $value }}" maxlength="{{ $dataField->character_limit }}">
															@if($dataField->character_limit)
																<div class="invite-details-field-hint">(You can enter maximum up to {{ $dataField->character_limit }} characters)</div>
															@endif
														@elseif($dataField->type_field_id == config('evibe.input.textarea'))
														@elseif($dataField->type_field_id == config('evibe.input.number'))
															<input id="{{ $dataField->name }}" name="{{ $dataField->name }}" type="number" class="form-control invite-party-detail" data-party-field-name="{{ $dataField->name }}" placeholder="{{ $dataField->hint_message }}" value="{{ str_replace(" ", "",$value) }}" maxlength="{{ $dataField->character_limit }}">
														@elseif($dataField->type_field_id == config('evibe.input.date'))
															<input id="{{ $dataField->name }}" name="{{ $dataField->name }}" type="text" class="party-date-time form-control invite-party-detail" data-party-field-name="{{ $dataField->name }}" value="{{ $value }}">
														@endif
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
									@endforeach
								</div>
								<div class="clearfix"></div>
								<div class="text-center mar-t-20 mar-b-20">
									<button type="submit" class="btn-party-details-next btn">
										<span class="mar-r-5">Get My E-Card</span>
									</button>
								</div>
							</form>
						</div>
					@else
						<div class="">
							Oops! Some error occurred while fetching input details for your invitation. Kindly refresh the page and try again. If the issue still persists, kindly write to {{ config('evibe.contact.invitations.group') }}
						</div>
					@endif
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					@if(isset($data['inviteTemplate']) && $data['inviteTemplate']->sample_url)
						<div class="invite-sample-text font-italic text-muted pad-l-30 pad-r-30">
							<span class="glyphicon glyphicon-info-sign"></span> This is a sample invitation and you will receive your invitation with your party details in the next step.
						</div>
						<div class="invite-sample-template text-center mar-b-5 mar-t-10">
							<div class="invite-sample-img-wrap">
								<img class="invite-sample-img" src="{{ $galleryUrl }}/invibe/templates/{{ $data['inviteTemplate']->sample_url }}">
							</div>
							<div class="clearfix"></div>
						</div>
					@else
					<!-- Show a default sample template -->
					@endif
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	@endif
@endsection