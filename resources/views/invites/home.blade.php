@extends('layout.base')

@section('page-title')
	<title>@yield("title")</title>
@endsection

@section('page-meta-description')
	@yield("meta-description")
@endsection

@section('meta-keywords')
	@yield("meta-kw")
@endsection

@section('og-title')
	<meta property="og:title" content="@yield('title')"/>
@endsection

@section('og-description')
	@yield('og-description')
@endsection

@section('og-url')
	<meta property="og:url" content="https://evibe.in/e-cards"/>
@endsection

@section('press:title')
@endsection

@section("header")
	@include("base.home.header.header-home-city")
	<div class="header-border"></div>
@endsection

@section("footer")
	@include("base.home.footer")
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/invite.css') }}"/>
@endsection

@section("content")
	<div class="checkout-navigation-container no-pad-l no-pad-r">
		<div class="invites-steps-wrap">
			@yield('invites-step')
		</div>
		<input type="hidden" id="hidInviteTemplateId" value="@if(isset($data['inviteTemplateId']) && $data['inviteTemplateId']) {{ $data['inviteTemplateId'] }} @endif">
		<input type="hidden" id="hidTicketId" value="@if(isset($data['ticketId']) && $data['ticketId']) {{ $data['ticketId'] }} @endif">
		<input type="hidden" id="hidTicketInviteId" value="@if(isset($data['ticketInviteId']) && $data['ticketInviteId']){{ $data['ticketInviteId'] }}@endif">
		<input type="hidden" id="hidEvibeHost" value="{{ config('evibe.host') }}">
		<input type="hidden" id="hidGalleryHost" value="{{ config('evibe.gallery.host') }}">
	</div>

	<div id="modalOfferLandingSignUp" class="modal fade no-pad-r-imp" role="dialog" tabindex="-1">
		<div class="modal-dialog modal-md">
			<div class="modal-content" style="background-color: #262c3b">
				<div class="col-xs-12 no-pad text-center">
					@if ($agent->isMobile() && !($agent->isTablet()))
						<div>
							<div>
								<img style="height: 65px; margin-top: 20px; vertical-align: top;" src="{{ config("evibe.gallery.host") }}/img/icons/gift-box-white.png">
							</div>
							<div style="color: #ffffff; padding: 10px 0 20px 0">Enter your details</div>
						</div>
						<div>
							<div class="col-xs-12">
								<input type="text" placeholder="Enter your full name" class="invite-details-name" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
								<input type="text" placeholder="Enter your email address" class="invite-details-email" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
								<input type="text" placeholder="Enter your phone number" class="invite-details-phone" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
							</div>
							<div class="col-xs-6 col-xs-offset-3 mar-t-30 mar-b-30">
								<a class="btn btn-evibe full-width pad-t-10 pad-b-10 invite-details-submit" data-url="{{ route("iv.create.store-details") }}">
									SUBMIT
								</a>
								<a class="btn btn-evibe full-width pad-t-10 pad-b-10 invite-details-submitting hide">SUBMITTING...</a>
							</div>
							<div class="clearfix"></div>
						</div>
					@else
						<div>
							<div>
								<img style="height: 65px; margin-top: 20px; vertical-align: top;" src="{{ config("evibe.gallery.host") }}/img/icons/gift-box-white.png">
							</div>
							<div style="color: #ffffff; padding: 10px 0 20px 0">Enter your details</div>
						</div>
						<div>
							<div class="col-xs-4 col-xs-offset-4">
								<input type="text" placeholder="Enter your full name" class="invite-details-name" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
								<input type="text" placeholder="Enter your email address" class="invite-details-email" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
								<input type="text" placeholder="Enter your phone number" class="invite-details-phone" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
							</div>
							<div class="col-sm-4 col-xs-offset-4 mar-t-30 mar-b-30">
								<a class="btn btn-evibe full-width pad-t-10 pad-b-10 invite-details-submit" data-url="{{ route("iv.create.store-details") }}">
									SUBMIT
								</a>
								<a class="btn btn-evibe full-width pad-t-10 pad-b-10 invite-details-submitting hide">SUBMITTING...</a>
							</div>
							<div class="clearfix"></div>
						</div>
					@endif
				</div>
				<div class="offer-signup-thankyou text-center hide">
					<div>
						<div style="font-size: 40px; margin-top: 30px; color: #ffffff; height: 35px; line-height: 40px">THANK YOU</div>
						<div style="color: #ffffff; margin: 25px 0; padding: 0 20px;">Our team will get in touch with you shortly. Should you have any queries, kindly email to {{ config("evibe.contact.tech.group") }}, we will respond to you ASAP.</div>
					</div>
					<div>
						<div style="color: #ffffff; margin-bottom: 25px; font-size: 16px;" class="text-underline cur-point in-blk" data-dismiss="modal">Continue Browsing</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			var $ticketInviteId = $('#hidTicketInviteId').val();

			(function loadInvitePreviewCarousel() {
				$.getScript("/js/util/bootstrapCarousalSwipe.js", function () {
					$('#carousel-invite').carousel({
						interval: false
					}).bcSwipe({threshold: 50});
				});
			})();

			(function loadTemplateSelectionPintrestModal() {
				if ($(".grid").length > 0) {
					var mosaicJs = "<?php echo elixir('js/app/pt.js'); ?>";
					$.getScript(mosaicJs, function () {
						var $grid = $('.grid').masonry({
							itemSelector: '.grid-item',
							percentPosition: true,
							columnWidth: '.grid-sizer'
						});

						$grid.imagesLoaded().progress(function () {
							$grid.masonry();
						});
					});
				}
			})();

			(function templateSelectionScreenBTNActions() {
				/* Selecting design */
				$('.btn-invite-template-select').on('click', function () {
					inviteTemplateId = $(this).data('id');

					showTemplatePartyDetails();
				});

				/* Selecting design on modal*/
				$('.btn-modal-invite-template-select').on('click', function () {
					showTemplatePartyDetails();
				});

				/* Previewing image*/
				$('.btn-invite-template-preview').on('click', function () {
					$(".invite-item").removeClass("active");
					inviteTemplateId = $(this).data('id');
					$('.invite-item-' + inviteTemplateId).addClass("active");

					$('#inviteTemplatePreviewModal').modal('show');
				});
			})();

			(function partyDetailsScreenBTNActions() {
				/* Submitting details */
				$('.btn-party-details-next').on('click', function (event) {
					event.preventDefault();

					validateAndStoreValues();
				});

				/* Date time picker */
				$(".party-date-time").datetimepicker({
					"minDate": 0,
					"format": 'Y/m/d h:i A',
					"scrollInput": false,
					"scrollMonth": false,
					"scrollTime": false,
					"closeOnDateSelect": true,
					"disabledDates": window.disableDates(),
					"validateOnBlur": true,
					onSelectDate: function (ct, $i) {
						$i.parent().addClass("is-dirty");
					}
				});
			})();

			(function paymentButtonClick() {
				$("#payInviteBtn").on("click", function () {
					var biddingModal = $('#modalOfferLandingSignUp');

					biddingModal.modal({
						'show': true,
						'keyboard': true
					});
				});

				$('.invite-details-submit').on('click', function (e) {
					e.preventDefault();
					disableSubmitButton();

					var formUrl = $(this).data('url');

					var data = {
						"inviteDetailsName": $('.invite-details-name').val(),
						"inviteDetailsEmail": $('.invite-details-email').val(),
						"inviteDetailsPhone": $('.invite-details-phone').val(),
						"sourceUrl": window.location.href,
					};

					$.ajax({
						url: formUrl,
						type: "POST",
						data: data,
						success: function (data) {
							if (data.success) {
								$("#modalOfferLandingSignUp").find(".col-xs-12").empty();
								$(".offer-signup-thankyou").removeClass("hide");
							} else {
								showSubmitButton();
								window.showNotyError(data.errorMsg);
							}
						},
						error: function (jqXHR, textStatus, errorThrown) {
							showSubmitButton();
							window.showNotyError("An error occurred while submitting your request. Please try again later.");
							reportError("url=" + formUrl + "&textStatus=" + textStatus + "&errorThrown=" + errorThrown);
						}
					});
				});

				function showSubmitButton() {
					$('.invite-details-submit').removeClass('hide');
					$('.invite-details-submitting').addClass('hide');
				}

				function disableSubmitButton() {
					$('.invite-details-submit').addClass('hide');
					$('.invite-details-submitting').removeClass('hide');
				}
			})();

			function showTemplatePartyDetails() {
				var inviteTemplateUrl = $("#carousel-invite").find(".active").data("url");

				if ((inviteTemplateId === undefined) || (inviteTemplateId === '')) {
					window.showNotyError('Kindly select a design to proceed');
					return false;
				}

				window.location.href = $('#hidEvibeHost').val() + '/i/edit/' + inviteTemplateUrl;
			}

			function validateAndStoreValues() {
				if (($ticketInviteId > 0) && ($ticketInviteId !== undefined) && ($ticketInviteId !== '')) {
					window.showLoading();

					var $storeValuesUrl = $('#hidEvibeHost').val() + '/i/edit/store-values';
					var $data = {
						ticketInviteId: $ticketInviteId
					};

					/* get the invite dynamic field value */
					var $dynamicFields = $('.invite-party-detail');
					if ($dynamicFields.length > 0) {
						$.each($dynamicFields, function (key, value) {
							var name = $(this).data('party-field-name');
							$data[name] = $('#' + name).val();
						});
					}

					$.ajax({
						url: $storeValuesUrl,
						dataType: 'JSON',
						tryCount: 0,
						retryLimit: 3,
						type: 'POST',
						data: $data,
						success: function (data) {
							window.hideLoading();
							if (data.success) {
								var $inviteHashId = "";

								if (data.ticketInviteId) {
									$ticketInviteId = data.ticketInviteId;
									$inviteHashId = data.inviteHashId;
								}

								window.showLoading();
								window.location.href = $('#hidEvibeHost').val() + '/i/download-card/' + $ticketInviteId + '?tkn=' + $inviteHashId;
							} else {
								var $errorMsg = 'Some error occurred while preparing your invite. Please try again after sometime (or) Kindly write to invites@evibe.in';
								if (data.error) {
									$errorMsg = data.error;
								}
								if (typeof data.login != "undefined" && data.login) {
									$('#loginModal').modal('show');
								}
								window.showNotyError($errorMsg);
							}
						},
						error: function (jqXHR, textStatus, errorThrown) {
							this.tryCount++;
							if (this.tryCount <= this.retryLimit) {
								$.ajax(this);
							} else {
								window.hideLoading();
								window.notifyTeam({
									"url": $storeValuesUrl,
									"textStatus": textStatus,
									"errorThrown": errorThrown
								});
							}
						}
					});
				} else {
					/* error occurred */
				}
			}
		});
	</script>
@endsection