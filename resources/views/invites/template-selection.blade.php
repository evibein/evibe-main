@extends('invites.home')

@section("title")
	@if(isset($data['seo']) && isset($data['seo']['title']))
		{{ $data['seo']['title'] }}
	@else
		Customize your own invites | Evibe.in
	@endif
@endsection

@section("meta-description")
	@if(isset($data['seo']) && isset($data['seo']['description']))
		<meta name="description" content="{{ $data['seo']['description'] }}">
	@else
		<meta name="description" content="Get your own e-card for free. Easy to customize. ">
	@endif

@endsection

@section('meta-kw')
	@if(isset($data['seo']) && isset($data['seo']['kw']))
		<meta name="keywords" content="{{ $data['seo']['kw'] }}"/>
	@else
		<meta name="keywords" content="Free Invites, Wishcards, Thank you cards, Easy to customize, Downloadable and sharable cards"/>

	@endif

@endsection

@section('og-description')
	@if(isset($data['seo']) && isset($data['seo']['description']))
		<meta property="og:description" content="{{ $data['seo']['description'] }}"/>
	@else
		<meta name="description" content="Get your own e-card for free. Easy to customize. ">
	@endif

@endsection

@section('invites-step')
	@php
		$imageUrl = "";
		if(isset($data['cardTypeId'])) {
			$imageUrl = ($data['cardTypeId'] == config('evibe.type-card.invite')) ? "invite-hero" : ($data['cardTypeId'] == config('evibe.type-card.wish-card') ? "wish-cards-hero" : "thank-you-hero");
		}
	@endphp
	<div class="invite-hero-image-wrapper">
		@if($agent->isMobile() && !($agent->isTablet()))
			<div class="mobile-res">
				<div>
					<img src="{{ $galleryUrl }}/main/img/{{ $imageUrl . "-mobile.jpg" }}" style="width: 100%;" alt="e-cards header image">
				</div>
			</div>
			<div>
				@if(isset($data["trending"]) && (count($data["trending"]) > 0))
					<div class="mar-t-20 pad-l-10 pad-r-10">
						<h6 class="no-mar-t mar-b-15">
							TRENDING INVITES
						</h6>
						<div class="trending-designs">
							@foreach($data["trending"] as $template)
								<div class="col-sm-3 col-xs-6 mar-b-10">
									<div style="height: 200px; padding: 20px; background-color: #f7f7f7; text-align: center">
										<a href="{{ route("iv.create.party-details", $template["seo_url"]) }}" class="pos-rel">
											@if($template["type_card_category_id"] == config("evibe.type-card-category.pro"))
												<i class="glyphicon glyphicon-star trending-invite-premium-icon-mobile"></i>
											@endif
											<img src="{{ $galleryUrl }}/invibe/templates/thumbs/{{ $template["sample_url"] }}?ref=trending" alt="{{ $template["name"] }}" style="box-shadow: 1px 4px 8px rgba(0,0,0,0.15); max-height: 100%;">
										</a>
									</div>
									<p class="invite-link-title text-center">{{ ucwords($template["name"]) }}</p>
								</div>
							@endforeach
							<div class="clearfix"></div>
						</div>
					</div>
					<hr class="hr-text no-mar">
				@endif
				@include("invites.how-it-works")
			</div>
			<div>
				<div class="mar-t-20">
					@if(isset($data['inviteTemplates']) && count($data['inviteTemplates']))
						@if($data['cardTypeId'] == config('evibe.type-card.invite'))
							<h6 class="no-pad-t mar-l-10 mar-r-10">
								BIRTHDAY INVITES
							</h6>
						@endif
						<div class="grid mar-l-5 mar-r-5" style="min-height: 400px;">
							<div class="grid-sizer"></div>
							@foreach($data['inviteTemplates'] as $inviteTemplate)
								@php $gradientColor = !is_null($inviteTemplate->gradient_color) ? $inviteTemplate->gradient_color : "#dddddd"; @endphp
								<div class="grid-item hero-grid__item btn-invite-template-preview" data-id="{{ $inviteTemplate->id }}" data-sample-url="{{ $inviteTemplate->sample_url }}">
									<div style="padding: 12px; border: 1px solid #f7f7f7; background-image: linear-gradient(to bottom right, {{ $gradientColor . "40" }}, {{ $gradientColor . "66" }}); border-radius: 5px;">
										@if($inviteTemplate->type_card_category_id == config("evibe.type-card-category.pro"))
											<i class="glyphicon glyphicon-star invite-premium-icon-mobile"></i>
										@endif
										<img class="invite-link__img invite-template-img" src="{{ $galleryUrl }}/invibe/templates/thumbs/{{ $inviteTemplate->sample_url }}" data-id="{{ $inviteTemplate->id }}" alt="{{ $inviteTemplate->name }}">
									</div>
									<p class="invite-link-title">{{ ucwords($inviteTemplate->name) }}</p>
								</div>
							@endforeach
						</div>
					@else
						<div class="e-cards-add-msg">
							We are adding amazing templates soon. Stay tuned!
						</div>
					@endif
				</div>
			</div>
		@else
			<div class="desktop-res">
				<div style="height: 300px; overflow: hidden;">
					<a href="#" class="a-no-decoration" target="_blank">
						<img src="{{ $galleryUrl }}/main/img/{{ $imageUrl . ".png" }}" alt="e-cards header image">
					</a>
				</div>
			</div>
			<div class="container">
				<div class="mar-t-30" style="margin-left: -15px;">
					@include("invites.how-it-works")
				</div>
				@if(isset($data["trending"]) && (count($data["trending"]) > 0))
					<div class="mar-t-30">
						<h6 class="no-pad-t">
							TRENDING INVITES
						</h6>
						<div class="trending-designs">
							@foreach($data["trending"] as $template)
								<div class="col-sm-3 col-xs-6">
									<div style="height: 230px; padding: 20px; background-color: #f7f7f7; text-align: center">
										<a href="{{ route("iv.create.party-details", $template["seo_url"]) }}" class="pos-rel">
											@if($template["type_card_category_id"] == config("evibe.type-card-category.pro"))
												<i class="glyphicon glyphicon-star trending-invite-premium-icon"></i>
											@endif
											<img src="{{ $galleryUrl }}/invibe/templates/thumbs/{{ $template["sample_url"] }}?ref=trending" alt="{{ $template["name"] }}" style="box-shadow: 1px 4px 8px rgba(0,0,0,0.15); max-height: 100%;">
										</a>
									</div>
									<p class="invite-link-title text-center">{{ ucwords($template["name"]) }}</p>
								</div>
							@endforeach
							<div class="clearfix"></div>
						</div>
					</div>
				@endif
				<div style="max-width: 1366px; margin: 0 auto;">
					<div class="mar-t-30">
						@if(isset($data['inviteTemplates']) && count($data['inviteTemplates']))
							@if($data['cardTypeId'] == config('evibe.type-card.invite'))
								<h6 class="no-pad-t">
									BIRTHDAY INVITES
								</h6>
							@endif
							<div class="grid" style="min-height: 400px; margin-left: -1%; margin-right: -1%;">
								<div class="grid-sizer"></div>
								@foreach($data['inviteTemplates'] as $inviteTemplate)
									@php $gradientColor = !is_null($inviteTemplate->gradient_color) ? $inviteTemplate->gradient_color : "#dddddd"; @endphp
									<div class="grid-item hero-grid__item btn-invite-template-preview" data-id="{{ $inviteTemplate->id }}" data-sample-url="{{ $inviteTemplate->sample_url }}">
										<div style="padding: 20px; border: 1px solid #f7f7f7; background-image: linear-gradient(to bottom right, {{ $gradientColor . "40" }}, {{ $gradientColor . "66" }}); border-radius: 5px;">
											@if($inviteTemplate->type_card_category_id == config("evibe.type-card-category.pro"))
												<i class="glyphicon glyphicon-star invite-premium-icon"></i>
											@endif
											<img class="invite-link__img invite-template-img" src="{{ $galleryUrl }}/invibe/templates/thumbs/{{ $inviteTemplate->sample_url }}" data-id="{{ $inviteTemplate->id }}" alt="{{ $inviteTemplate->name }}">
										</div>
										<p class="invite-link-title">{{ ucwords($inviteTemplate->name) }}</p>
									</div>
								@endforeach
							</div>
						@else
							<div class="e-cards-add-msg">
								We are adding amazing templates soon. Stay tuned!
							</div>
						@endif
					</div>
				</div>
			</div>
		@endif
	</div>
@endsection

@section("modals")
	<section class="modal fade" id="inviteTemplatePreviewModal" tabindex="-1" role="dialog" aria-labelledby="inviteTemplatePreviewModal" aria-hidden="true">
		<div class="modal-dialog no-pad">
			<div class="cross-button text-center" data-dismiss="modal">x</div>
			<div class="modal-content">
				<div class="modal-body text-center">
					@if(isset($data['inviteTemplates']) && count($data['inviteTemplates']))
						<div class="btn btn-modal-invite-template-select mar-b-15">Use This Design</div>
						<div id="carousel-invite" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner" role="listbox">
								@foreach($data['inviteTemplates'] as $inviteTemplate)
									<div class="item invite-item invite-item-{{ $inviteTemplate->id }}" data-id="{{ $inviteTemplate->id }}" data-url="{{$inviteTemplate->seo_url}}">
										<img data-src="{{ $galleryUrl }}/invibe/templates/{{ $inviteTemplate->sample_url }}" src="/" alt="{{ $inviteTemplate->name }}" class="full-width lazy-loading-invite-carousel-img">
									</div>
								@endforeach
							</div>
							<a class="left carousel-control" href="#carousel-invite" role="button" data-slide="prev">
								<i class="glyphicon glyphicon-triangle-left" aria-hidden="true"></i>
							</a>
							<a class="right carousel-control" href="#carousel-invite" role="button" data-slide="next">
								<i class="glyphicon glyphicon-triangle-right" aria-hidden="true"></i>
							</a>
						</div>
					@else
						<div class="e-cards-add-msg">
							We are adding amazing templates soon. Stay tuned!
						</div>
					@endif
				</div>
			</div>
		</div>
	</section>
@endsection