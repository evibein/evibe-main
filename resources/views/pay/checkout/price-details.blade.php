<div class="price-total mar-l-10 mar-r-10 mar-b-20">
	<div>
		<div class="col-sm-8 text-left">
			<h5 class="mar-t-20">Total Booking Amount:</h5>
		</div>
		<div class="col-sm-4">
			<h5 class="mar-t-20 checkout-price-total-amount">@price($bookingAmount)</h5>
		</div>
		<div class="clearfix"></div>
	</div>
	@if($advanceToPay < $bookingAmount)
		<div class="seperator">
			<div class="col-sm-8 text-left font-16">
				<h5 class="mar-t-20">
					Total Advance To Pay:
				</h5>
			</div>
			<div class="col-sm-4">
				<h5 class="mar-t-20 checkout-price">
					@price($advanceToPay)
				</h5>
			</div>
			<div class="clearfix"></div>
		</div>
	@endif
</div>