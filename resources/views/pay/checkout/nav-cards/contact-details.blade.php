<div class="checkout-details-wrap">
	<div class="checkout-card-header">
		<div class="checkout-card-header-text">
			Contact Details
		</div>
	</div>
	<div class="checkout-card-body">
		<div class="form-row mar-t-10">
			<div class="col-md-6 ">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					<input id="inpPhone" type="text" class="mdl-textfield__input" value="@if(request()->has('inpPhone')){{ old('inpPhone') }}@else{{ $data['customer']['phone'] }}@endif" data-old-phone="" data-url="{{ route('ajax.auto-book.alert-team', $data['ticketId']) }}">
					<label class="mdl-textfield__label" for="inpPhone">
						<span class="label-text">Phone Number</span>
					</label>
				</div>
			</div>
			<div class="col-md-6 ">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					<input id="altPhone" type="text" class="mdl-textfield__input" value="@if(request()->has('altPhone')){{ old('altPhone') }}@else{{ $data['customer']['altPhone'] }}@endif">
					<label class="mdl-textfield__label" for="altPhone">
						<span class="label-text">Alt. Phone Number</span>
					</label>
				</div>
			</div>
			<div class="col-md-6">
				<div class="col-xs-2 col-md-3 no-pad-l no-pad-r">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<select class="mdl-textfield__input" id="inpTitle" name="inpTitle" style="height: 29px;">
							<option></option>
							<option value="1">Mr</option>
							<option value="2">Ms</option>
						</select>
						<label class="mdl-textfield__label" for="inpTitle">Title</label>
					</div>
				</div>
				<div class="col-xs-10 col-md-9 no-pad-r pad-l-5">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="inpName" type="text" class="mdl-textfield__input" value="@if(request()->has('inpName')){{ old('inpName') }}@else{{ $data['customer']['name'] }}@endif">
						<label class="mdl-textfield__label" for="inpName">
							<span class="label-text">Full Name</span>
						</label>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			@if($data['isAutoBooking'])
				<div class="col-md-6 ">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="inpEmail" type="text" class="mdl-textfield__input email-typo-error" value="@if(request()->has('inpEmail')){{ old('inpEmail') }}@else{{ $data['customer']['email'] }}@endif">
						<label class="mdl-textfield__label" for="inpEmail">
							<span class="label-text">Email id</span>
						</label>
					</div>
				</div>
			@endif
			@php $sources = \App\Models\Ticket\TypeTicketSource::select("id", "name")->where("show_customer", 1)->get(); @endphp
			@if($sources->count() > 0)
				<div class="col-md-6">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<select class="mdl-textfield__input" id="inpCustomerSource" name="inpCustomerSource" style="height: 29px;">
							<option></option>
							@foreach($sources as $source)
								<option value="{{ $source->id }}">{{ $source->name }}</option>
							@endforeach
						</select>
						<label class="mdl-textfield__label" for="inpCustomerSource">How did you find Evibe.in?
							<small> (optional)</small>
						</label>
					</div>
				</div>
			@endif
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="checkout-card-footer">
		<div class="btn btn-primary btn-checkout-card-continue checkout-card-details-submit-btn btn-checkout-contact-continue">
			<span class="glyphicon glyphicon-arrow-right"></span>
			<span class="mar-l-4">Continue</span>
		</div>
	</div>
</div>