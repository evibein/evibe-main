<div class="checkout-details-wrap">
	<div class="checkout-card-header">
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 pad-r-15__400-600 text-left valign-mid">
			<div class="checkout-card-header-text">
				<span class="mar-r-5">Party Info</span>
				<span class="checkout-card-header-info">(Kindly provide your party details)</span>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-right text-center-400-600 mar-t-10-400-600 valign-mid">
			<div class="btn btn-primary btn-checkout-card-continue checkout-card-details-submit-btn btn-checkout-party-continue">
				<span class="">Continue</span>
				<span class="glyphicon glyphicon-arrow-right mar-l-5 no-mar-r"></span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="checkout-card-body">
		<div class="form-row mar-t-10">
			<div>
				<div class="col-sm-6 col-md-4 col-lg-4">
					<div class="input-field">
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
							<input class="mdl-textfield__input" type="text" name="inpPartyDate" id="inpPartyDate" @if(isset($data['additional']['partyDate']) && $data['additional']['partyDate'] && ($data['additional']['partyDate'] != '01 Jan 1970')) value="{{ $data['additional']['partyDate'] }}" disabled @endif/>
							<label class="mdl-textfield__label" for="partyDate">
								Party Date*</label>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-lg-4">
					<div class="input-field">
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
							<input class="mdl-textfield__input" type="text" name="inpPartyTime" id="inpPartyTime" @if(isset($data['additional']['partyStartTime']) && $data['additional']['partyStartTime']) value="{{ $data['additional']['partyStartTime'] }}" @endif @if(isset($data['additional']['PartyStartTimeDisabled']) && $data['additional']['PartyStartTimeDisabled']) disabled @endif/>
							<label class="mdl-textfield__label" for="partyTime">
								Party Start Time*</label>
						</div>
					</div>
				</div>
				@if(isset($data['additional']['checkoutFieldsCustomerData']) && count($data['additional']['checkoutFieldsCustomerData'])> 0)
					@foreach($data['additional']['checkoutFieldsCustomerData'] as $checkoutField)
						@if($checkoutField['type_field_id'] == config('evibe.input.checkbox'))
							<div class="col-sm-12 col-md-12 col-lg-12 input-field mar-b-10 mar-t-30">
								<div id="{{ $checkoutField['name'] }}">
									<div>
										<div class="text-bold">{{ $checkoutField['identifier'] }}</div>
										@if($checkoutField['hint_message'])
											<div class="hint-msg font-12 text-info">({{ ucfirst($checkoutField['hint_message']) }})</div>
										@endif
									</div>
									@php
										$valueOptionsArray = [];
										$valueString = $data['additional'][$checkoutField['name']];
									@endphp
									@if($valueString)
										@php $valueOptionsArray = explode(', ', $valueString); @endphp
									@endif
									@foreach($checkoutField->checkoutOptions as $checkoutOption)
										<div class="md-checkbox in-blk pad-r-20">
											<input type="checkbox" id="{{ $checkoutOption->id }}" name="{{ $checkoutField['name'] }}" value="{{ $checkoutOption->name }}" @if(count($valueOptionsArray) && in_array($checkoutOption->name, $valueOptionsArray)) checked @endif>
											<label for="{{ $checkoutOption->id }}">{{ ucwords($checkoutOption->name) }}*</label>
										</div>
									@endforeach
								</div>
							</div>
						@elseif($checkoutField['type_field_id'] == config('evibe.input.option'))
							@if(count($checkoutField->checkoutOptions))
								<div class="col-sm-6 col-md-4 col-lg-4">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<select class="mdl-textfield__input" id="{{ $checkoutField['name'] }}" name="{{ $checkoutField['name'] }}" style="height: 29px;">
											<option></option>
											<option value="N/A">N/A</option>
											@foreach($checkoutField->checkoutOptions as $checkoutOption)
												<option value="{{ $checkoutOption->name }}" @if(isset($data['additional'][$checkoutField['name']]) && ($data['additional'][$checkoutField['name']] == $checkoutOption->name)) selected @endif>{{ ucwords($checkoutOption->name) }}</option>
											@endforeach
										</select>
										<label class="mdl-textfield__label" for="{{ $checkoutField['id'] }}">{{ ucwords($checkoutField['identifier']) }}*</label>
										@if($checkoutField['hint_message'])
											<div class="hint-msg checkout-hint-msg font-12 text-info">({{ ucfirst($checkoutField['hint_message']) }})</div>
										@endif
									</div>
								</div>
							@endif
						@elseif($checkoutField['type_field_id'] == config('evibe.input.date'))
							<div class="col-sm-6 col-md-4 col-lg-4">
								<div class="input-field">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<input id="{{ $checkoutField['name'] }}" type="text" class="mdl-textfield__input checkout-input-date" value="@if(request()->has($checkoutField['name'])){{ old($checkoutField['name']) }}@elseif(isset($data['additional'][$checkoutField['name']])){{ $data['additional'][$checkoutField['name']] }}@endif">
										<label class="mdl-textfield__label">
											<span class="label-text">{{ ucwords($checkoutField['identifier']) }}</span>
										</label>
										@if($checkoutField['hint_message'])
											<div class="hint-msg checkout-hint-msg font-12 text-info">({{ ucfirst($checkoutField['hint_message']) }})</div>
										@endif
									</div>
								</div>
							</div>
						@else
							<div class="col-sm-6 col-md-4 col-lg-4">
								<div class="input-field">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<input id="{{ $checkoutField['name'] }}" type="text" class="mdl-textfield__input" value="@if(request()->has($checkoutField['name'])){{ old($checkoutField['name']) }}@elseif(isset($data['additional'][$checkoutField['name']])){{ $data['additional'][$checkoutField['name']] }}@endif">
										<label class="mdl-textfield__label">
											<span class="label-text">{{ ucwords($checkoutField['identifier']) }}*</span>
										</label>
										@if($checkoutField['hint_message'])
											<div class="hint-msg checkout-hint-msg font-12 text-info">({{ ucfirst($checkoutField['hint_message']) }})</div>
										@endif
									</div>
								</div>
							</div>
						@endif
					@endforeach
					<div class="clearfix"></div>
				@endif
				<div class="clearfix"></div>
			</div>
			@if(isset($data['hasDecor']) && $data['hasDecor'])
				<div class="text-12 mar-l-15 mar-t-10 mar-b-10 text-i text-left">
					<span class="glyphicon glyphicon-info-sign valign-mid"></span>
					<span class="">Party decoration will be completed by party start time</span>
				</div>
			@endif
		</div>
	</div>
	<div class="checkout-card-footer">
		<div class="btn btn-primary btn-checkout-card-continue checkout-card-details-submit-btn btn-checkout-party-continue">
			<span class="">Continue</span>
			<span class="glyphicon glyphicon-arrow-right mar-l-5 no-mar-r"></span>
		</div>
	</div>
</div>