<div class="checkout-details-wrap">
	<div class="checkout-card-header">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pad-r-15__400-600 text-left valign-mid">
			<div class="checkout-card-header-text">
				<span class="mar-r-5">Contact details</span>
				<span class="checkout-card-header-info">(Kindly provide your contact details and proceed to payment)</span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="checkout-card-body">
		<div class="mar-t-10">
			<div class="col-sm-6 col-md-4 col-lg-4 ">
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad">
					@if(isset($data['countries']) && count($data['countries']))
						<select id="inpCallingCode" name="inpCallingCode" class="form-control country-calling-code">
							@foreach($data['countries'] as $country)
								<option value="{{ $country->calling_code }}" @if(isset($data['customer']['callingCode']) && ($data['customer']['callingCode'] == $country->calling_code)) selected @endif>{{ $country->calling_code }}</option>
							@endforeach
						</select>
					@else
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
							<input id="inpCallingCode" type="text" class="mdl-textfield__input" value="@if(request()->has('inpCallingCode')){{ old('inpCallingCode') }}@else{{ $data['customer']['callingCode'] }}@endif">
							<label class="mdl-textfield__label" for="inpCallingCode">
								<span class="label-text">Calling Code</span>
							</label>
						</div>
					@endif
				</div>
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-pad-r">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="inpPhone" type="text" class="mdl-textfield__input" value="@if(request()->has('inpPhone')){{ old('inpPhone') }}@else{{ $data['customer']['phone'] }}@endif" data-old-phone="" data-url="{{ route('ajax.auto-book.alert-team', $data['ticketId']) }}">
						<label class="mdl-textfield__label" for="inpPhone">
							<span class="label-text">Phone Number*</span>
						</label>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="col-sm-6 col-md-4 col-lg-4 ">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					<input id="altPhone" type="text" class="mdl-textfield__input" value="@if(request()->has('altPhone')){{ old('altPhone') }}@else{{ $data['customer']['altPhone'] }}@endif">
					<label class="mdl-textfield__label" for="altPhone">
						<span class="label-text">Alt. Phone Number*</span>
					</label>
				</div>
			</div>
			<div class="col-sm-6 col-md-4 col-lg-4">
				<div class="col-xs-2 col-md-3 no-pad-l no-pad-r">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<select class="mdl-textfield__input" id="inpTitle" name="inpTitle" style="height: 29px;">
							<option></option>
							<option value="1">Mr</option>
							<option value="2">Ms</option>
						</select>
						<label class="mdl-textfield__label" for="inpTitle">Title</label>
					</div>
				</div>
				<div class="col-xs-10 col-md-9 no-pad-r pad-l-5">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="inpName" type="text" class="mdl-textfield__input" value="@if(request()->has('inpName')){{ old('inpName') }}@else{{ $data['customer']['name'] }}@endif">
						<label class="mdl-textfield__label" for="inpName">
							<span class="label-text">Full Name*</span>
						</label>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			@if($data['isAutoBooking'])
				<div class="col-sm-6 col-md-4 col-lg-4 ">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="inpEmail" type="text" class="mdl-textfield__input email-typo-error" value="@if(request()->has('inpEmail')){{ old('inpEmail') }}@else{{ $data['customer']['email'] }}@endif">
						<label class="mdl-textfield__label" for="inpEmail">
							<span class="label-text">Email Id*</span>
						</label>
					</div>
				</div>
			@endif
			@php $sources = \App\Models\Ticket\TypeTicketSource::select("id", "name")->where("show_customer", 1)->get(); @endphp
			@if($sources->count() > 0)
				<div class="col-sm-6 col-md-4 col-lg-4">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<select class="mdl-textfield__input" id="inpCustomerSource" name="inpCustomerSource" style="height: 29px;">
							<option></option>
							@foreach($sources as $source)
								<option value="{{ $source->id }}">{{ $source->name }}</option>
							@endforeach
						</select>
						<label class="mdl-textfield__label" for="inpCustomerSource">How did you find Evibe.in?*
						</label>
					</div>
				</div>
			@endif
			<div class="clearfix"></div>
		</div>
		@if ($agent->isMobile() && !($agent->isTablet()))
			<div class="mar-t-10">
				<div class="payment-gateways-wrap mar-l-10 mar-r-10">
					@include('pay.checkout.nav-cards.price-details')
				</div>
				<div class="mar-t-10">
					@include('pay.checkout.nav-cards.coupon-details')
				</div>
			</div>
		@else
		@endif
		<div class="mar-t-10 text-center">
			<div class="pad-10 mar-l-20 mar-r-20 in-blk text-left" style="background-color: #fafafa;">
				<input type="checkbox" name="additionalCharges" id="additionalCharges" class="valign-mid" value="clicked" style="margin-top: -2px;">
				<label for="additionalCharges" class="cur-point">&nbsp;Pay through International Cards</label><br>
				<span class="font-11">
					Additional charges will be added to your transaction.
					<a href="JavaScript:Void(0);" data-toggle="tooltip" title="3.5% Extra charges will be added to your booking amount if you are paying through any International cards." class="a-no-decoration-black">
						&nbsp;<i class="glyphicon glyphicon-question-sign"></i>
					</a>
				</span>
			</div>
		</div>
		<div>
			<div class="details">
				<div class="form-info text-center mar-b-10">
					@if($data['bookings']['totalAdvanceToPay'] > 0)
						<div>
							<div class="pad-20 pad-t-10__400">
								<button id="payBtn" class="btn btn-evibe btn-large btn-pay text-center">
									<span>Proceed to Pay </span>
									<span class='rupee-font'>&#8377;</span>
									<span class="checkout-advance-amount mar-l-4">{{ $data['bookings']['totalAdvanceToPay'] }}</span>
								</button>
							</div>
						</div>
						<small class="text-muted text-center blk font-11">
							<i>
								By proceeding further you accept to all our
								<a class="mar-l-4" href="{{ route('terms') }}" target="_blank" rel="noopener">terms of booking.</a>
							</i>
						</small>
					@else
						<span class='text-success text-upr text-bold font-18'>Total Advance Received</span>
					@endif
				</div>
			</div>
		</div>
	</div>
	<div class="checkout-card-footer">
	</div>
	<div class="payment-txn-inputs">
		@if(request("txnCnt") > 2)
			<input type="hidden" id="paymentMethod" value="razor" data-url="{{ route('pay.pgs.razor.init', request('id')) }}">
			<input type="hidden" id="paymentMethodPayU" value="razor" data-url="{{ route('pay.pgs.razor.init', request('id')) }}">
		@else
			<input type="hidden" id="paymentMethod" value="payU" data-url="{{ route('pay.pgs.payU.init', request('id')) }}">
			<input type="hidden" id="paymentMethodPayU" value="payU" data-url="{{ route('pay.pgs.payU.init', request('id')) }}">
		@endif
		<input type="hidden" id="retryPaymentMethod" value="razor" data-url="{{ route('pay.pgs.razor.init', request('id')) }}">
		<input type="hidden" id="retryPaymentMethodPayu" value="payU" data-url="{{ route('pay.pgs.payU.init', request('id')) }}">
	</div>
</div>
<div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 pad-t-15 pad-b-15 mar-t-15" style="background-color: white; border-radius: 10px">
	<h6 class="no-mar-t no-mar-b">Payment options</h6>
	@if ($agent->isMobile() && !($agent->isTablet()))
		<img src="{{ $galleryUrl }}/main/img/payment_modes.png" alt="evibe-payment-modes" style="max-width: 100%; height: 55px">
		<span>and more</span>
	@else
		<img src="{{ $galleryUrl }}/main/img/payment_modes_2.png" alt="evibe-payment-modes" style="max-width: 100%; height: 55px">
		<span>and more</span>
	@endif
</div>
