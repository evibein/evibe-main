<div class="checkout-details-wrap">
	<div class="checkout-card-header hide">
		<div class="checkout-card-header-text">
			Add Ons
		</div>
	</div>
	<div class="checkout-card-body">
		<div class="checkout-add-ons-container">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad-r">
				<div class="mar-t-15">
					<div class="checkout-add-ons-sec">
						<div class="checkout-add-ons-title">
							<div class="coll-xs-12 col-sm-12 col-md-8 col-lg-8 pad-r-15__400-600 text-left no-pad-l valign-mid">
								<div class="checkout-add-ons-info">
									<ul class="checkout-add-ons-info-list">
										<li class="text-center valign-mid">
											<div class="checkout-ao-info-img-wrap">
												<img class="checkout-ao-info-img" src="{{ $galleryUrl }}/main/img/icons/add-on-gift.png">
											</div>
										</li>
										<li class="valign-mid">
											<div class="mar-l-15 text-bold lh-20">Make your day extra special by adding these hand picked add-ons</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="coll-xs-12 col-sm-12 col-md-4 col-lg-4 text-right text-center-400-600 valign-mid">
								<div class="btn btn-primary btn-checkout-card-continue btn-checkout-add-ons-continue">
									<span class="">Continue</span>
									<span class="glyphicon glyphicon-arrow-right mar-l-5 no-mar-r"></span>
								</div>
								<div class="checkout-add-ons-continue-hint">
									<span class="glyphicon glyphicon-info-sign"></span>
									<span class="">You can proceed to make payment even without selecting any add-ons</span>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="checkout-add-ons-title mar-t-15">
							<b class="hide">Add On Services</b>
							<span class="co-ao-modal-price-wrap hide no-mar-l">
								<b><span class="co-ao-modal-count"></span></b> add-ons selected - <b><span><span class="rupee-font">&#8377; </span><span class="co-ao-modal-price">0</span></span></b>
								<a href="#" class="checkout-add-ons-remove-btn mar-l-10">(remove all)</a>
							</span>
						</div>
						<div class="checkout-add-ons-store-promotion pad-r-15 mar-t-20">
							@if(isset($data['storePromotion']) && $data['storePromotion'])
								<a href="@if(isset($data['storePromotionUrl']) && $data['storePromotionUrl']) {{ $data['storePromotionUrl'] }} @else # @endif" style="color:black; text-decoration: none;" target="_blank">
									<div class="add-on-card">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-pad">
											<div class="pull-left mar-r-15">
												<div class="add-on-image-wrap">
													<img data-src="@if(isset($data['storePromotionImage']) && $data['storePromotionImage']) {{ $data['storePromotionImage'] }} @else {{ config('evibe.gallery.host') }}/main/img/home/shop-hero-m2.png @endif" alt="profile picture" class="add-on-image lazy-loading package-list-profile-image">
												</div>
											</div>
											<div class="">
												<div class="add-on-content">
													<div class="add-on-name">
														@if(isset($data['storePromotionTitle']) && $data['storePromotionTitle'])
															{{ $data['storePromotionTitle'] }}
														@else
															Party Props
														@endif
													</div>
												</div>
											</div>
											<div class="add-on-cta-wrap mar-t-10 mar-b-5">
												<div class="add-on-cta-list">
													<a href="@if(isset($data['storePromotionUrl']) && $data['storePromotionUrl']) {{ $data['storePromotionUrl'] }} @else # @endif" class="" style="color: #ED3E72; border: 1px solid #ED3E72;padding: 2px 12px 2px 10px; border-radius: 10px; text-decoration: none;" target="_blank">Browse</a>
												</div>
											</div>
											<div class="add-on-info mar-t-10">
												@if(isset($data['storePromotionTagLine']) && $data['storePromotionTagLine'])
													{{ $data['storePromotionTagLine'] }}
												@else
													Unique party stuff delivered to your doorstep.
												@endif
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								</a>
							@endif
						</div>
						<div class="checkout-add-ons-wrap mar-t-20">
							@php $i = 1; @endphp
							@foreach($data['addOns'] as $addOn)
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 no-pad-l mar-b-15">
									@if((isset($data['selectedAddOnIds'])) && (count($data['selectedAddOnIds'])) && (isset($data['selectedAddOnIds'][$addOn->id])))
										<div class="checkout-selected-add-on" data-id="{{ $addOn->id }}" data-booking-units="{{ $data['selectedAddOnIds'][$addOn->id]['bookingUnits'] }}"></div>
									@endif
									@include('occasion.util.add-ons.card')
								</div>
								@if(($i % 2) == 0)
									<div class="clearfix"></div>
								@endif
								@php $i++; @endphp
							@endforeach
							<div class="clearfix"></div>
						</div>
						<div class="checkout-add-ons-cta text-center mar-t-15 mar-b-15">

						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="checkout-card-footer">
		<div class="btn btn-primary btn-checkout-card-continue btn-checkout-add-ons-continue">
			<span class="">Continue</span>
			<span class="glyphicon glyphicon-arrow-right mar-l-5 no-mar-r"></span>
		</div>
	</div>
</div>