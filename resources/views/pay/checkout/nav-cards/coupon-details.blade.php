<div class="checkout-details-wrap">
	<div class="checkout-card-body no-pad__400-600">
		<div class="coupon-code-wrap mar-l-10 mar-r-10 mar-b-20 pad-l-15 pad-r-15 text-left">
			<div class="coupon-code-input-wrap text-center">
				<div class="mar-l-10 mar-r-10 coupon-error-msg-wrap pad-10 hide">
					<div class="text text-danger">
						<i class="glyphicon glyphicon-exclamation-sign"></i>
						<span class="coupon-error-msg mar-l-4"></span>
					</div>
				</div>
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-pad-l no-pad-r">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label in-blk">
						<input type="text" id="couponCode" class="mdl-textfield__input" value="">
						<label class="mdl-textfield__label pad-l-5" for="couponCode">
							<span class="label-text">Enter Coupon Code</span>
						</label>
					</div>
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 mar-t-15 pull-right">
					<div class="in-blk">
						<a class="btn btn-default coupon-code-apply-button mar-b-15__400 mar-b-15-400-600">APPLY</a>
						<a class="btn btn-default coupon-code-remove-button hide  mar-b-15__400 mar-b-15-400-600">
							<i class="glyphicon glyphicon-remove"></i>
							<span> REMOVE</span>
						</a>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="mar-l-10 mar-r-10 coupon-success-msg-wrap alert alert-success pad-10 hide">
					<div class="text text-success text-center">
							<span class="coupon-Success-msg">
								<i class="glyphicon glyphicon-ok-circle"></i>
								<span> Congratulations! Coupon applied successfully. You have saved <span class='rupee-font'>&#8377;</span></span>
								<span class="coupon-discount-amount"></span>.
							</span>
					</div>
				</div>
			</div>
			@if(isset($data["walletBalance"]) && ($data["walletBalance"] >= config("wallet.default.amount"))
			&& isset($data["walletExpiry"]) && ($data["walletExpiry"] > time()))
				<hr class="checkout-price-split-up-hr no-mar-t">
				<div class="pad-t-5 pad-b-10">
					<div class="pull-left in-blk">
						<input type="checkbox" name="walletAmount" id="walletAmount" class="valign-mid" value="clicked" style="margin-top: -2px;">
						<label for="walletAmount" class="cur-point">&nbsp;Use Evibe Wallet</label>
						<a href="JavaScript:Void(0);" data-toggle="tooltip" title="Max ₹{{ config("wallet.default.amount") }} wallet money can be redeemed from cashbacks & coupons." class="a-no-decoration-black">
							&nbsp;<i class="glyphicon glyphicon-question-sign"></i>
						</a>
					</div>
					<div class="pull-right text-bold">
						&#8377; {{ config("wallet.default.amount") }}
					</div>
					<div class="clearfix"></div>
				</div>
			@endif
			<input type="hidden" id="walletMaxUsage" value="{{ config("wallet.default.amount") }}">
		</div>
	</div>
</div>
