<div class="checkout-details-wrap">
	<div class="checkout-card-header">
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 pad-r-15__400-600 text-left valign-mid">
			<div class="checkout-card-header-text">
				<span class="mar-r-5">ID Proofs</span>
				<span class="checkout-card-header-info">(Kindly upload your ID proof and click "continue" to proceed)</span>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-right text-center-400-600 mar-t-10-400-600 valign-mid">
			<div class="btn btn-primary btn-checkout-card-continue checkout-card-details-submit-btn btn-checkout-proof-continue">
				<span class="">Continue</span>
				<span class="glyphicon glyphicon-arrow-right mar-l-5 no-mar-r"></span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="checkout-card-body">
		<div class="form-row mar-t-20 text-center">
			<div class="checkout-proofs-wrap mar-t-20">
				<div class="checkout-proofs-upload-wrap @if(isset($data['customerOrderProofs']) && $data['customerOrderProofs']) hide @endif">
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<div class="text-left mar-l-15">
							<div class="text-bold">Upload your ID proof</div>
							<div class="mar-t-10 @if(isset($data['ticket']) && $data['ticket'] && $data['ticket']->event_date && (($data['ticket']->event_date - time()) >= (24*60*60))) @else hide @endif">
								<label class="btn-checkout-proofs-choice btn-checkout-proof-left btn-checkout-proofs-toggle">
									<input type="radio" name="proofUploadChoice" value="1" class="checkout-proofs-choice hide" checked>
									<div class="mar-l-5 valign-top">
										<div>Now</div>
									</div>
								</label>
								<label class="btn-checkout-proofs-choice btn-checkout-proof-right">
									<input type="radio" name="proofUploadChoice" value="2" class="checkout-proofs-choice hide">
									<div class="mar-l-5 valign-top">
										<div>Later</div>
									</div>
								</label>
							</div>
							<div class="mar-t-5 font-10">
								<span class="glyphicon glyphicon-info-sign"></span>
								<span>According to Govt. regulations, a valid Photo ID proof has to be carried by every person above the age of 18 staying at the venue.</span>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
						<input type="hidden" id="needCustomerProof" value="{{ $data['needCustomerProof'] }}">
						<div id="proofUploadWrap" class="">
							@include('app.customer-order-proofs')
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="checkout-proofs-success-wrap text-g @if(isset($data['customerOrderProofs']) && $data['customerOrderProofs']) @else hide @endif">
					Your ID proofs have been uploaded successfully
				</div>
			</div>
			<div class="mar-t-20">
				<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1 text-left">
					<div>
						<label>
							<input type="checkbox" name="accountabilityCheck" value="1"><span class="mar-l-5">I take complete accountability for all the guests accompanying me.</span>
						</label>
					</div>
					<div>
						<label id="proofCancellationCheck" class="hide">
							<input type="checkbox" name="cancellationCheck" value="1"><span class="mar-l-5">I accept the non-refundable cancellation of the party if I fail to upload valid ID proof at least 4 hrs before check-in time.</span>
						</label>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div class="checkout-card-footer">
		<div class="btn btn-primary btn-checkout-card-continue checkout-card-details-submit-btn btn-checkout-proof-continue">
			<span class="">Continue</span>
			<span class="glyphicon glyphicon-arrow-right mar-l-5 no-mar-r"></span>
		</div>
	</div>
</div>