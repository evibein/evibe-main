<div class="form-row mar-t-20">
	<div>
		<input type="hidden" id="dynamicFieldsUser" value='{{ $data['additional']['checkoutFieldsUser'] }}'>
		<div class="checkout-bookings-wrap">
			@foreach ($data['bookings']['list'] as $booking)
				<div class="booking-data">
					<div class="billing">
						@if($booking['bookingAmount'] == $booking['advanceAmount'])
							<div class="col-sm-6 billing-item">
								<div><b>Booking Id</b></div>
								<div>#{{ $booking['bookingId'] }}</div>
							</div>
							<div class="col-sm-6 billing-item">
								<div>
									@if ($booking['advancePaid'] == 1)
										<b>Booking Amount Paid</b>
									@else
										<b>Booking Amount</b>
									@endif
								</div>
								<div class="price">@price($booking['bookingAmount'])</div>
							</div>
						@else
							<div class="col-sm-4 billing-item">
								<div><b>Booking Id</b></div>
								<div>#{{ $booking['bookingId'] }}</div>
							</div>
							<div class="col-sm-4 billing-item">
								<div><b>Booking Amount</b></div>
								<div class="price">@price($booking['bookingAmount'])</div>
							</div>
							<div class="col-sm-4 billing-item">
								<div>
									@if ($booking['advancePaid'] == 1)
										<b>Advance Paid</b>
									@else
										<b>Advance Amount</b>
									@endif
								</div>
								<div class="price">@price($booking['advanceAmount'])</div>
							</div>
						@endif
						<div class="clearfix"></div>
					</div>
					@if(isset($booking['partyDateTime']) && $booking['partyDateTime'])
						<div class="mar-t-10">
							<label>Party Time: </label>
							<span class="mar-l-5">{{ date("d/m/y g:i A", $booking['partyDateTime']) }}</span>
						</div>
					@endif
					<div class="mar-t-10">
						<label>Description: </label>
						<div>{!! $booking['bookingInfo'] !!}</div>
					</div>
					@if(count($booking['checkoutFieldsCrm'])> 0)
						@foreach($booking['checkoutFieldsCrm'] as $checkoutFieldCrm)
							<div class="mar-t-10">
								@if(!empty($data['additional'][$checkoutFieldCrm['name']]) && $checkoutFieldCrm['type_ticket_booking_id'] == $booking['typeTicketBookingId'])
									<label>{{ ucwords($checkoutFieldCrm['identifier']) }}: </label>
									@if($checkoutFieldCrm['type_field_id'] == config('evibe.input.textarea'))
										<div> {{ $data['additional'][$checkoutFieldCrm['name']] }}  </div>
									@else
										<span class="mar-l-5">{{ $data['additional'][$checkoutFieldCrm['name']] }}</span>
									@endif
								@endif
							</div>
						@endforeach
					@endif
					@if ($booking['prerequisites'])
						<div class="mar-t-10">
							<label>Prerequisites: </label>
							<div>{!!  $booking['prerequisites'] !!}</div>
						</div>
					@endif
					@if ($booking['facts'])
						<div class="mar-t-10">
							<label>Facts: </label>
							<div>{!! $booking['facts'] !!}</div>
						</div>
					@endif
					@if(count($booking['gallery']) > 0)
						<div class="mar-t-10">
							<div>
								<label>Reference Image(s):</label>
							</div>
							@foreach($booking['gallery'] as $gallery)
								<a href="{{ $gallery->getOriginalImagePath() }}" target="_blank" rel="noopener" class="booking-gallery">
									<img src="{{ $gallery->getResultsImagePath() }}" alt="{{ $gallery->title }}" class="booking-image">
								</a>
							@endforeach
						</div>
					@endif
					@if(count($booking['checkoutFieldsUser'])> 0)
						@foreach($booking['checkoutFieldsUser'] as $checkoutFieldUser)
							@if($checkoutFieldUser['type_ticket_booking_id'] == $booking['typeTicketBookingId'])
								@if($checkoutFieldUser['type_field_id'] == config('evibe.input.checkbox'))
									<div class="col-sm-12 col-md-12 col-lg-12 input-field mar-b-10 mar-t-30">
										<div id="{{ $checkoutFieldUser['name'] }}">
											<div>
												<div class="text-bold">{{ $checkoutFieldUser['identifier'] }}</div>
												@if($checkoutFieldUser['hint_message'])
													<div class="hint-msg font-12 text-info">({{ ucfirst($checkoutFieldUser['hint_message']) }})</div>
												@endif
											</div>
											@php
												$valueOptionsArray = [];
												$valueString = $data['additional'][$checkoutFieldUser['name']];
											@endphp
											@if($valueString)
												@php $valueOptionsArray = explode(', ', $valueString); @endphp
											@endif
											@foreach($checkoutFieldUser->checkoutOptions as $checkoutOption)
												<div class="md-checkbox in-blk pad-r-20">
													<input type="checkbox" id="{{ $checkoutOption->id }}" name="{{ $checkoutFieldUser['booking_checkout_unique_id'] }}" value="{{ $checkoutOption->name }}" @if(count($valueOptionsArray) && in_array($checkoutOption->name, $valueOptionsArray)) checked @endif>
													<label for="{{ $checkoutOption->id }}">{{ ucwords($checkoutOption->name) }}*</label>
												</div>
											@endforeach
										</div>
									</div>
								@elseif($checkoutFieldUser['type_field_id'] == config('evibe.input.option'))
									@if(count($checkoutFieldUser->checkoutOptions))
										<div class="col-sm-6 col-md-6 col-lg-6 input-field mar-b-10 mar-t-10">
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<select class="mdl-textfield__input mar-b-n-5" id="{{ $checkoutFieldUser['booking_checkout_unique_id'] }}" name="{{ $checkoutFieldUser['booking_checkout_unique_id'] }}" style="height: 29px;">
													<option></option>
													@foreach($checkoutFieldUser->checkoutOptions as $checkoutOption)
														<option value="{{ $checkoutOption->id }}">{{ ucwords($checkoutOption->name) }}</option>
													@endforeach
												</select>
												<label class="mdl-textfield__label" for="{{ $checkoutFieldUser['id'] }}">{{ ucwords($checkoutFieldUser['identifier']) }}*</label>
												@if($checkoutFieldUser['hint_message'])
													<div class="hint-msg checkout-hint-msg font-12 text-info">({{ ucfirst($checkoutFieldUser['hint_message']) }})</div>
												@endif
											</div>
										</div>
									@endif
								@elseif($checkoutFieldUser['type_field_id'] == config('evibe.input.date'))
									<div class="col-sm-6 col-md-4 col-lg-4">
										<div class="input-field">
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<input id="{{ $checkoutFieldUser['booking_checkout_unique_id'] }}" type="text" class="mdl-textfield__input checkout-input-date" value="@if(request()->has($checkoutFieldUser['name'])){{ old($checkoutFieldUser['name']) }}@elseif(isset($data['additional'][$checkoutFieldUser['name']])){{ $data['additional'][$checkoutFieldUser['name']] }}@endif">
												<label class="mdl-textfield__label">
													<span class="label-text">{{ ucwords($checkoutFieldUser['identifier']) }}</span>
												</label>
												@if($checkoutFieldUser['hint_message'])
													<div class="hint-msg checkout-hint-msg font-12 text-info">({{ ucfirst($checkoutFieldUser['hint_message']) }})</div>
												@endif
											</div>
										</div>
									</div>
								@else
									<div class="col-sm-6 col-md-6 col-lg-6 input-field mar-b-10 mar-t-10">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
											<input id="{{ $checkoutFieldUser['booking_checkout_unique_id'] }}" type="text" class="mdl-textfield__input" value="@if(request()->has($checkoutFieldUser['name'])){{ old($checkoutFieldUser['name']) }}@else{{$data['additional'][$checkoutFieldUser['name']]  }}@endif">
											<label class="mdl-textfield__label checkout-issue-label">
												<span class="label-text">{{ ucwords($checkoutFieldUser['identifier']) }}*</span>
											</label>
											@if($checkoutFieldUser['hint_message'])
												<div class="hint-msg checkout-hint-msg font-12 text-info">({{ ucfirst($checkoutFieldUser['hint_message']) }})</div>
											@endif
										</div>
									</div>
								@endif
							@endif
						@endforeach
						<div class="clearfix"></div>
					@endif
				</div>
			@endforeach
		</div>
		@if(isset($data["bookings"]["list"]) && (count($data["bookings"]["list"]) > 0))
			@foreach($data["bookings"]["list"] as $bookingList)
				<div class="col-sm-12 input-field mar-t-10 mar-b-15 special-notes-wrap">
					<span class="hide special-notes-title">{{ $bookingList["typeBookingDetails"] }} - #{{ $bookingList["bookingId"] }}</span>
					<label>Special Notes for {{ ucwords($bookingList["typeBookingDetails"]) }}
						<span class="text-normal text-muted"><small> (optional)</small> #{{ $bookingList["bookingId"] }}</span>
					</label><br>
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="margin-top: -12px">
						<input type="text" class="special-notes mdl-textfield__input" value="@if(request()->has('splNotes')){{ old('splNotes') }}@else{!! $data['additional']['specialNotes'] !!}@endif">
						<label class="mdl-textfield__label">
							<span class="label-text">Special Order Notes</span>
						</label>
						<div class="hint-msg checkout-hint-msg font-13 text-info">(Needs approval if not already discussed)</div>
					</div>
				</div>
			@endforeach
		@endif
		@if(isset($data['uploadImage']) && $data['uploadImage'])
			<div class="col-sm-12 col-md-12 col-lg-12 mar-b-20">
				<div class="upload-details">
					<div class="" id="uploadDetails">
						<div class="form-row">
							<form id="uploadImagesForm" data-url="{{ route('ajax.auto-book.upload-customer-images', $data['ticketId']) }}">
								<div class="image-upload-text mar-b-5">
									<b>Upload images</b>
								</div>
								<div class="image-upload-rules text-info">
									( Valid Formats: JPG, jpeg, jpg and png. Maximum file size:
									<b><u>1 MB</u></b> )
								</div>
								<div class="form-group mar-t-10">
									<input type="hidden" id="uploadImage" value="{{ $data['uploadImage'] }}">
									<input type="hidden" id="uploadTicketId" value="{{ $data['ticketId'] }}">
									<div class="in-blk">
										<input type="file" name="checkoutImages[]" multiple/>
									</div>
									<div class="in-blk">
										<button type="submit" id="imageUploadBtn" class="btn btn-warning btn-sm">
											<i class="glyphicon glyphicon-upload"></i> Upload
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="hide" id="uploadCompleted">
						<div class="upload-successful-text">
							Your upload was successful
						</div>
					</div>
				</div>
			</div>
		@endif
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
</div>