<div class="checkout-details-wrap">
	<div class="checkout-card-header">
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 pad-r-15__400-600 text-left valign-mid">
			<div class="checkout-card-header-text">
				<span class="mar-r-5">Order Details</span>
				<span class="checkout-card-header-info">(Kindly provide necessary details for your order and click "continue")</span>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-right text-center-400-600 mar-t-10-400-600 valign-mid">
			<div class="btn btn-primary btn-checkout-card-continue checkout-card-details-submit-btn btn-checkout-order-continue">
				<span class="">Continue</span>
				<span class="glyphicon glyphicon-arrow-right mar-l-5 no-mar-r"></span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="checkout-card-body">
		@include('pay.checkout.nav-cards.order-details-content')
	</div>
	@if(isset($data['invite']) && $data['invite'])
		<div class="checkout-invite-promo-wrap mar-t-20">
			<div class="checkout-invite-promo-title mar-t-15">
				Invite Services
				<span class="checkout-invite-promo-label">Exclusive</span>
			</div>
			<div class="mar-t-15 mar-b-15">
				<div>
					<label>
						<input type="checkbox" id="eInvite" name="eInvite" class="pull-left" @if(isset($data['additional']['eInvite']) && $data['additional']['eInvite']) checked @endif>
						<div class="checkout-invite-service-text mar-l-20">
							Whatsapp downloadable premium invites for you party. Opt-in for this service and receive premium invite samples from us.
						</div>
					</label>
				</div>
				<a class="valign-mid checkout-invite-know-more mar-l-20 font-12" id="btnInviteKnowMore" data-toggle="modal" data-target="#checkoutEInviteBenefitsModal">[Know More]</a>
			</div>
		</div>
	@endif
	<div class="checkout-card-footer">
		<div class="btn btn-primary btn-checkout-card-continue checkout-card-details-submit-btn btn-checkout-order-continue">
			<span class="">Continue</span>
			<span class="glyphicon glyphicon-arrow-right mar-l-5 no-mar-r"></span>
		</div>
	</div>
</div>