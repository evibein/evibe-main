@extends('pay.base')

@section("content")
	@parent
	<div class="checkout-page">
		<div class="col-md-12 no-pad__400-600">
			<div class="col-md-12 no-pad__400-600">
				<div class="alert alert-info text-center">
					<i class="glyphicon glyphicon-info-sign mar-r-4"></i>
					<span>Please cross check all your order details below. "Reply All" to our email if anything is missing to get an updated order details before you make any payment.</span>
				</div>
				<div class="col-md-6 checkout-form">
					<input id="ticketId" type="hidden" value="{{ request('id') }}">
					<input id="custEmail" type="hidden" value="{{ $data['customer']['email'] }}">
					<input id="advanceAmount" type="hidden" value="{{ $data['bookings']['totalAdvanceToPay'] }}">
					<div class="cards">
						@include('pay.checkout.cards.customer-info')
						@include('pay.checkout.cards.party-details')
						@if(isset($data['hasAddOn']) && $data['hasAddOn'])
							@include('pay.checkout.cards.auto-add-on-details')
						@endif
						@if (!$data['hasVenueBooking'])
							@include('pay.checkout.cards.venue-info', ['isShowVenueType' => true])
						@endif
					</div>
				</div>
				<div class="col-md-6 item-checkout">
					<div class="payment-section mar-b-20">
						<div class="title">
							<h5 class="co-title">Order Details</h5>
						</div>
						<div class="details pad-t-10 mar-b-10">
							@include('pay.checkout.cards.auto-order-details', ['viewData' => ['email' => true]])
						</div>
					</div>
					@if(isset($data['extraBenefits']) && $data['extraBenefits'])
						<div class="payment-section hide">
							<div>
								@include('pay.checkout.cards.service-selection')
							</div>
						</div>
					@endif
					<div class="payment-section">
						<div class="details pad-t-10">
							<div class="form-info text-center mar-b-10">
								@if($data['bookings']['totalAdvanceToPay'] > 0)
									<div>
										<div>
											@include('pay.checkout.price-details',[
												'bookingAmount' => $data['bookings']['totalBookingAmount'],
												'advanceToPay' => $data['bookings']['totalAdvanceToPay']])
											<div class="coupon-code-wrap mar-l-10 mar-r-10 mar-b-20 text-left">
												<h6 class="coupon-code-text-plus no-mar pad-t-15 pad-b-15 pad-l-10">Have Coupon Code?</h6>
												<div class="coupon-code-input-wrap text-center">
													<div class="mar-l-10 mar-r-10 coupon-error-msg-wrap pad-10 hide">
														<div class="text text-danger">
															<i class="glyphicon glyphicon-exclamation-sign"></i>
															<span class="coupon-error-msg mar-l-4"></span>
														</div>
													</div>
													<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label pad-l-5 mar-r-5">
														<input type="text" id="couponCode" class="mdl-textfield__input" value="">
														<label class="mdl-textfield__label pad-l-5" for="couponCode">
															<span class="label-text">Enter Code</span>
														</label>
													</div>
													<a class="btn btn-default coupon-code-apply-button mar-b-15__400 mar-b-15-400-600">APPLY</a>
													<a class="btn btn-default coupon-code-remove-button hide  mar-b-15__400 mar-b-15-400-600">
														<i class="glyphicon glyphicon-remove"></i>
														<span> REMOVE</span>
													</a>
													<div class="mar-l-10 mar-r-10 coupon-success-msg-wrap alert alert-success pad-10 hide">
														<div class="text text-success text-center">
															<span class="coupon-Success-msg">
																<i class="glyphicon glyphicon-ok-circle"></i>
																<span> Congratulations! Coupon applied successfully. You have saved <span class='rupee-font'>&#8377;</span></span>
																<span class="coupon-discount-amount"></span>.</span>
														</div>
													</div>
												</div>
											</div>
											<div class="payment-gateways-wrap mar-l-10 mar-r-10">
												<h6 class="coupon-code-text-plus no-mar pad-t-15 pad-l-10 text-left">Select Payment Method</h6>
												<div class="text-center">
													<div class="pull-left text-left pad-t-5 pad-b-15">
														<div class="col-xs-12 col-sm-6">
															<div class="default-payment-option">
																<input type="radio" id="paymentMethodRazorCredit" name="paymentMethod" value="razor" checked data-url="{{ route('pay.pgs.razor.init', request('id')) }}" class="custom-radio-animation" data-payment-type="card">
																<label for="paymentMethodRazorCredit" class="custom-radio-animation-label mar-t-10 mar-l-10">
																	<span class="font-20 default-payment-label-text">Credit Card</span>
																</label>
															</div>
														</div>
														<div class="col-xs-12 col-sm-6">
															<div class="default-payment-option">
																<input type="radio" id="paymentMethodRazorDebit" name="paymentMethod" value="razor" data-url="{{ route('pay.pgs.razor.init', request('id')) }}" class="custom-radio-animation" data-payment-type="card">
																<label for="paymentMethodRazorDebit" class="custom-radio-animation-label mar-t-10 mar-l-10">
																	<span class="font-20 default-payment-label-text">Debit Card</span>
																</label>
															</div>
														</div>
														<div class="col-xs-12 col-sm-6">
															<div class="default-payment-option">
																<input type="radio" id="paymentMethodRazorNetBanking" name="paymentMethod" value="razor" data-url="{{ route('pay.pgs.razor.init', request('id')) }}" class="custom-radio-animation" data-payment-type="netbanking">
																<label for="paymentMethodRazorNetBanking" class="custom-radio-animation-label mar-t-10 mar-l-10">
																	<span class="font-20 default-payment-label-text">Net Banking</span>
																</label>
															</div>
														</div>
														<div class="col-xs-12 col-sm-6">
															<div class="default-payment-option">
																<input type="radio" id="paymentMethodRazorUPI" name="paymentMethod" value="razor" data-url="{{ route('pay.pgs.razor.init', request('id')) }}" class="custom-radio-animation" data-payment-type="upi">
																<label for="paymentMethodRazorUPI" class="custom-radio-animation-label mar-t-10 mar-l-10">
																	<span class="font-20 default-payment-label-text">UPI</span>
																</label>
															</div>
														</div>
														<div class="clearfix"></div>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="payment-gateways-hr"></div>
												<div class="text-center">
													<div class="pad-b-15 pad-t-15">
														<div class="col-xs-12 col-sm-6">
															<div class="default-payment-option">
																<input type="radio" id="paymentMethodPaytm" name="paymentMethod" value="paytm" data-url="{{ route('pay.pgs.paytm.init', request('id')) }}" class="custom-radio-animation checkout-custom-radio-animation">
																<label for="paymentMethodPaytm" class="custom-radio-animation-label mar-t-10">
																	<img src="{{ $galleryUrl }}/img/logo/logo_paytm.png" alt="paytm" height="30px">
																</label>
															</div>
														</div>
														<div class="col-xs-12 col-sm-6">
															<div class="default-payment-option">
																<input type="radio" id="paymentMethodPayU" name="paymentMethod" value="payU" data-url="{{ route('pay.pgs.payU.init', request('id')) }}" class="custom-radio-animation checkout-custom-radio-animation">
																<label for="paymentMethodPayU" class="custom-radio-animation-label mar-t-10">
																	<img src="{{ $galleryUrl }}/img/logo/logo_payU.png" alt="PayUMoney" height="30px">
																</label>
															</div>
														</div>
														<div class="col-xs-12 col-sm-6">
															<div class="default-payment-option">
																<input type="radio" id="paymentMethodFreeCharge" name="paymentMethod" value="freeCharge" data-url="{{ route('pay.pgs.freeCharge.init', request('id')) }}" class="custom-radio-animation checkout-custom-radio-animation">
																<label for="paymentMethodFreeCharge" class="custom-radio-animation-label mar-t-10">
																	<img src="{{ $galleryUrl }}/img/logo/logo_freecharge.png" alt="freecharge" height="30px">
																</label>
															</div>
														</div>
														<div class="clearfix"></div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-sm-8 col-sm-offset-2 hide">
											<div class="payment-offer-wrap">
												<span>
													Pay using Freecharge and get &#8377;100 instant cash back for purchase above &#8377;4,000. *T&C
												</span>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="pad-20 pad-t-10__400">
											<button id="payBtn" class="btn btn-evibe btn-large btn-pay text-center">
												<span>Pay </span> @price($data['bookings']['totalAdvanceToPay'])
											</button>
										</div>
									</div>
									<small class="text-muted text-right blk font-11">
										<i>
											By proceeding further you accept to all our
											<a href="{{ route('terms') }}" target="_blank" rel="noopener">terms of booking.</a>
										</i>
									</small>
								@else
									<span class='text-success text-upr text-bold font-18'>Total Advance Received</span>
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection

@section("modals")
	@parent
	@include('pay.checkout.modals.add-ons', [
		"addOns" => isset($data['addOns']) ? $data['addOns'] : []
	])
	@include('pay.checkout.modals.e-invite-benefits')
	@include('pay.checkout.modals.e-thank-you-card-benefits')
@endsection

