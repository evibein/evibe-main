@extends('pay.base')

@section("content")
	@if(isset($data['ticket']) && $data['ticket']->status_id && ($data['ticket']->status_id == config('evibe.ticket.status.followup')))
		<div class="col-xs-10 col-sm-6 col-sm-offset-3 col-xs-offset-1 mar-t-30 mar-b-30 text-center">
			<img src="{{ $galleryUrl }}/img/icons/tick-icon.png" alt="success icon">
			<div class="ty-msg-txt mar-t-30">We have received your request. We will get in touch with you shortly.</div>
			<div class="mar-t-20">Need any help? please write to us at {{ config('evibe.contact.customer.group') }} or call us at +91 9640204000 (Mon - Sat 10 AM - 8 PM)</div>
		</div>
		<div class="clearfix"></div>
	@else
		<div class="col-xs-10 col-sm-6 col-sm-offset-3 col-xs-offset-1 mar-t-30 mar-b-30 text-center request-payment-link-wrap">
			<img src="{{ $galleryUrl }}/main/img/icons/sad_face.png">
			<div class="mar-t-30 font-16"> Sorry, the payment link has expired on
				<b> {{ date('F j, Y, g:i A',$data['eventDate']) }}</b></div>
			<div class="pad-t-10 text-center">
				<button class="btn btn-lg btn-evibe request-new-payment-link">Request New Payment Link</button>
			</div>
			<div class="mar-t-30">
				Need any help? please write to us at {{ config('evibe.contact.customer.email') }} or call us at<br> +91 9640204000 (Mon - Sat 10 AM - 8 PM)
			</div>
		</div>
		<div class="col-xs-10 col-sm-6 col-sm-offset-3 col-xs-offset-1 mar-t-30 mar-b-30 text-center request-payment-link-success hide">
			<img src="{{ $galleryUrl }}/img/icons/tick-icon.png" alt="success icon">
			<div class="ty-msg-txt mar-t-30">We have received your request. We will get in touch with you shortly.</div>
			<div class="mar-t-20">Need any help? please write to us at {{ config('evibe.contact.customer.group') }} or call us at +91 9640204000 (Mon - Sat 10 AM - 8 PM)</div>
		</div>
		<div class="clearfix"></div>
	@endif
	<input type="hidden" value="{{$data['ticket']['id']}}" id="ticketId">
	<input type="hidden" id="isValidStatus" @if(isset($data['ticket']) && $data['ticket']->status_id && ($data['ticket']->status_id == config('evibe.ticket.status.cancelled'))) value="0" @else value="1" @endif>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			/* Opened the expiry page */
			$('.request-new-payment-link').on('click', function () {

				/* Clicked on generate new payment link button */
				let ticketId = $('#ticketId').val();
				$('.request-new-payment-link').prop('onclick', null).off('click').addClass('disabled');

				$.ajax({
					url: 'request-new-payment-link/' + ticketId,
					type: 'GET',
					dataType: 'json'
				}).done(function (data) {
					if (data.success) {
						$(".request-payment-link-wrap").addClass("hide");
						$(".request-payment-link-success").removeClass("hide");
					}
					else {
						window.hideLoading();
						window.showNotyError(data.error);
					}
				}).fail(function () {
					window.hideLoading();
					window.showNotyError();
				});
			});
		});
	</script>
@endsection