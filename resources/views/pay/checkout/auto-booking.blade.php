@extends('pay.base')

@section('content')
	@parent
	<div class="checkout-page-2 auto-checkout-page">
		<div class="checkout-hidden-details-container hide">
			<input id="ticketId" type="hidden" value="{{ request('id') }}">
			<input id="custEmail" type="hidden" value="{{ $data['customer']['email'] }}">
			<input id="advanceAmount" type="hidden" value="{{ $data['bookings']['totalAdvanceToPay'] }}">
			<input type="hidden" id="dynamicFieldsUser" value='{{ $data['additional']['checkoutFieldsUser'] }}'>
			<input type="hidden" id="hidInputTypeCheckbox" value="{{ config("evibe.input.checkbox") }}">
			<input type="hidden" id="hidValidateVenueDetailsUrl" value="{{ route('auto-book.pay.auto.validate.venue-details', ['ticketId' => request('id')]) }}">
			<input type="hidden" id="hidValidatePartyDetailsUrl" value="{{ route('auto-book.pay.auto.validate.party-details', ['ticketId' => request('id')]) }}">
			<input type="hidden" id="hidValidateOrderDetailsUrl" value="{{ route('auto-book.pay.auto.validate.order-details', ['ticketId' => request('id')]) }}">
			<input type="hidden" id="hidValidateContactDetailsUrl" value="{{ route('auto-book.pay.auto.validate.contact-details', ['ticketId' => request('id')]) }}">
		</div>
		<div class="checkout-navigation-container">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="checkout-navigation-card-wrap">
					<div id="wizard" class="checkout-navigation-list form_wizard wizard_horizontal pad-l-5 pad-r-5">
						<ul class="wizard_steps anchor no-pad">
							@if (!$data['hasVenueBooking'])
								<li>
									<a data-ref="venueDetails" data-checkout-card-count="" class="checkout-navigation-btn btn-checkout-navigation-venue disabled">
										<span class="step_no"></span>
										<span class="step_descr">Venue Details<br></span>
									</a>
								</li>
							@endif
							<li>
								<a data-ref="partyDetails" data-checkout-card-count="" class="checkout-navigation-btn btn-checkout-navigation-party disabled">
									<span class="step_no"></span>
									<span class="step_descr">Party Details<br></span>
								</a>
							</li>
							@if(isset($data['addOns']) && count($data['addOns']))
								<li>
									<a data-ref="addOnDetails" data-checkout-card-count="" class="checkout-navigation-btn btn-checkout-navigation-add-on disabled">
										<span class="step_no"></span>
										<span class="step_descr">Add On Details<br></span>
									</a>
								</li>
							@endif
							<li>
								<a data-ref="orderDetails" data-checkout-card-count="" class="checkout-navigation-btn btn-checkout-navigation-order disabled">
									<span class="step_no"></span>
									<span class="step_descr">Order Details<br></span>
								</a>
							</li>
							<li>
								<a data-ref="contactDetails" data-checkout-card-count="" class="checkout-navigation-btn btn-checkout-navigation-contact disabled">
									<span class="step_no"></span>
									<span class="step_descr">Contact Details<br></span>
								</a>
							</li>
							<li>
								<a data-ref="paymentDetails" data-checkout-card-count="" class="checkout-navigation-btn btn-checkout-navigation-payment disabled">
									<span class="step_no"></span>
									<span class="step_descr">Payment Details<br></span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="checkout-details-container mar-t-15">
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
				@if (!$data['hasVenueBooking'])
					<div id="venueDetails" class="checkout-card-wrap" data-ref="venueDetails" data-card-count="">
						@include('pay.checkout.nav-cards.venue-details', ['isShowVenueType' => $data['isShowVenueType']])
					</div>
				@endif
				<div id="partyDetails" class="checkout-card-wrap" data-card-count="">
					@include('pay.checkout.nav-cards.party-details')
				</div>
				@if(isset($data['addOns']) && count($data['addOns']))
					<div id="addOnDetails" class="checkout-card-wrap" data-card-count="">
						@include('pay.checkout.nav-cards.add-on-details')
					</div>
				@endif
				<div id="orderDetails" class="checkout-card-wrap" data-card-count="">
					@include('pay.checkout.nav-cards.order-details', ['viewData' => ['email' => true]])
				</div>
				<div id="contactDetails" class="checkout-card-wrap" data-card-count="">
					@include('pay.checkout.nav-cards.contact-details')
				</div>
				<div id="" class="checkout-card-wrap" data-card-count="">
					@if(isset($data['hasAddOn']) && $data['hasAddOn'])
						@include('pay.checkout.cards.auto-add-on-details')
					@endif
				</div>
				<div id="paymentDetails" class="checkout-card-wrap" data-ref="paymentDetails" data-card-count="">
					@if(isset($data['extraBenefits']) && $data['extraBenefits'])
						<div class="payment-section hide">
							<div>
								@include('pay.checkout.cards.service-selection')
							</div>
						</div>
					@endif
					@include('pay.checkout.nav-cards.payment-details')
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 no-pad-l">
				<div id="priceDetails" class="checkout-price-card-wrap">
					@include('pay.checkout.nav-cards.price-details')
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
@endsection

@section("modals")
	@parent
	@include('pay.checkout.modals.add-ons', [
		"addOns" => isset($data['addOns']) ? $data['addOns'] : []
	])
	@include('pay.checkout.modals.e-invite-benefits')
	@include('pay.checkout.modals.e-thank-you-card-benefits')
@endsection

@section('javascript')
	@parent

	<script type="text/javascript">
		$(document).ready(function () {

			$("#inpPartyTime").datetimepicker({
				datepicker: false,
				formatTime: 'g:i A',
				format: 'g:i A',
				step: 30,
				onSelectTime: function (ct, $i) {
					$i.parent().addClass('is-dirty');
				}
			});

			var tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);

			$("#inpPartyDate").datetimepicker({
				timepicker: false,
				minDate: 0,
				startDate: tomorrow,
				defaultDate: tomorrow,
				format: 'Y/m/d',
				scrollInput: false,
				scrollMonth: false,
				scrollTime: false,
				closeOnDateSelect: true,
				disabledDates: window.disableDates(),
				onSelectDate: function (ct, $i) {
					$i.parent().addClass('is-dirty');
				}
			});
		});

	</script>
@endsection