@extends('pay.base')

@section("content")
	@if(isset($data['redirect']) && $data['redirect'])
		<input type="hidden" id="hidRedirect" value="{{ $data['redirect'] }}">
	@endif
	<input type="hidden" id="hidTicketId" value="{{ $data['ticketId'] }}">
	@if(isset($data['checkoutUrl']) && $data['checkoutUrl'])
		<input type="hidden" id="hidCheckoutUrl" value="{{ $data['checkoutUrl'] }}">
		@if(isset($data['addOns']) && count($data['addOns']))
			<div class="checkout-add-ons-container">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad-r">
					<div class="mar-t-15">
						<div class="checkout-add-ons-sec">
							<div class="checkout-add-ons-title">
								<div class="coll-xs-12 col-sm-12 col-md-8 col-lg-8 pad-r-15__400-600 text-left no-pad-l valign-mid">
									<div class="checkout-add-ons-info">
										<ul class="checkout-add-ons-info-list">
											<li class="text-center valign-mid">
												<div class="checkout-ao-info-img-wrap">
													<img class="checkout-ao-info-img" src="{{ $galleryUrl }}/main/img/icons/add-on-gift.png">
												</div>
											</li>
											<li class="valign-mid">
												<div class="mar-l-15__400-600 text-bold lh-20">Make your day extra special by adding these hand picked add-ons</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="coll-xs-12 col-sm-12 col-md-4 col-lg-4 text-right text-center-400-600 pad-t-10 pad-r-30 valign-mid">
									<a href="" class="btn-skip-add-ons">Skip To Payment Without Add-ons</a>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="checkout-add-ons-title mar-t-15">
								<b class="hide">Add On Services</b>
								<span class="co-ao-modal-price-wrap hide no-mar-l">
									<b><span class="co-ao-modal-count"></span></b> add-ons selected - <b><span><span class="rupee-font">&#8377; </span><span class="co-ao-modal-price">0</span></span></b>
								</span>
							</div>
							<div class="checkout-add-ons-wrap mar-t-20">
								@php $i = 1; @endphp
								@foreach($data['addOns'] as $addOn)
									<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 no-pad-l mar-b-15">
										@if((isset($data['selectedAddOnIds'])) && (count($data['selectedAddOnIds'])) && (isset($data['selectedAddOnIds'][$addOn->id])))
											<div class="checkout-selected-add-on" data-id="{{ $addOn->id }}" data-booking-units="{{ $data['selectedAddOnIds'][$addOn->id]['bookingUnits'] }}"></div>
										@endif
										@include('occasion.util.add-ons.card')
									</div>
									@if(($i % 3) == 0)
										<div class="clearfix"></div>
									@endif
									@php $i++; @endphp
								@endforeach
								<div class="clearfix"></div>
							</div>
							<div class="checkout-add-ons-cta text-center mar-t-15 mar-b-15">
								<div class="text-center hidden-md hidden-lg">
									<a href="" class="btn-skip-add-ons">Skip To Payment Without Add-ons</a>
								</div>
								<div class="">
									<span class="text-center mar-r-20 hidden-xs hidden-sm">
										<a href="" class="btn-skip-add-ons">Skip To Payment Without Add-ons</a>
									</span>
									<span class="btn btn-primary mar-t-10-400-600" id="btnUpdateCheckoutAddOns">Proceed To Make Payment</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		@endif
	@else
		<div class="error-wrap">Some error occurred while fetching your data. Please refresh the page and try again. If the problem still persists, contact {{ config('evibe.contact.customer.group') }}</div>
	@endif
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			/* Use Loading */
			var $ticketId = $('#hidTicketId').val();

			if ($('#hidRedirect').length) {
				window.showLoading();
				window.location.href = $('#hidCheckoutUrl').val();
			}

		});
	</script>
@endsection