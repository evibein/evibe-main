<section class="modal fade modal-pre-checkout" id="checkoutAddOnsModal" tabindex="-1" role="dialog" aria-labelledby="checkoutAddOnsModal" aria-hidden="true">
	<div class="modal-dialog no-pad">
		<div class="cross-button text-center" data-dismiss="modal">x</div>
		<div class="modal-content">
			<div class="modal-body">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					@if(count($addOns))
						<div class="co-ao-modal-head">
							<span class="co-ao-modal-title"><b>Manage Add Ons</b></span>
							<span class="hidden-xs hidden-sm">
							<span class="co-ao-modal-price-wrap hide">
								<b><span class="co-ao-modal-count"></span></b> add-ons selected - <b><span><span class="rupee-font">&#8377; </span><span class="co-ao-modal-price">0</span></span></b>
							</span>
								</span>
							<div class="hidden-md hidden-lg text-center pad-t-10">
								<span class="co-ao-modal-price-wrap">
									<span>Total add-ons amount: </span>
									<span class="text-e text-bold"><span class="rupee-font">&#8377; </span><span class="co-ao-modal-price">0</span></span>
								</span>
							</div>
						</div>
						<div class="co-ao-modal-content mar-t-10">
							@foreach($addOns as $key => $addOn)
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 no-pad__400-600 mar-t-10">
									@include('occasion.util.add-ons.card')
								</div>
								@if(($key % 2) == 1)
									<div class="clearfix"></div>
								@endif
							@endforeach
							<div class="clearfix"></div>
						</div>
						<div class="co-ao-modal-cta text-center mar-t-20">
							<a class="btn btn-primary" id="updateAddOns">UPDATE</a>
						</div>
					@else
						<div class="text-center">
							We regret to inform that there are no add-ons available for this booking
						</div>
					@endif
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</section>

@include("app.generic-modal-loader")