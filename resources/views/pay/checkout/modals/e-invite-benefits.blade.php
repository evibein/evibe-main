<section class="modal fade" id="checkoutEInviteBenefitsModal" tabindex="-1" role="dialog" aria-labelledby="checkoutEInviteBenefitsModal" aria-hidden="true">
	<div class="modal-dialog no-pad">
		<div class="cross-button text-center" data-dismiss="modal">x</div>
		<div class="modal-content">
			<div class="modal-body">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="benefit-title">
						What is this?
					</div>
					<div class="benefit-text">
						Customised e-invitation card
					</div>
					<hr>
					<div class="benefit-title">
						How it works?
					</div>
					<div class="benefit-text">
						Download the image and share it with your friends and family on whatsapp / facebook / email / sms or any other medium, inviting them to your party.
					</div>
					<hr>
					<div class="benefit-title">
						Sample Image
					</div>
					<div class="benefit-img-wrap mar-t-10 mar-b-15">
						<img class="benefit-img" src="{{ $galleryUrl }}/img/app/invite_sample.png">
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</section>