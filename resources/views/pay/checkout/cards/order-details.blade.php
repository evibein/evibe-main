<div class="co-order-details form-info">
	<div class="col-md-12 mar-t-10">
		@foreach ($data['bookings']['list'] as $booking)
			<div class="booking-data">
				<div class="billing">
					<div class="col-sm-4 billing-item">
						<div><b>Booking Id</b></div>
						<div>#{{ $booking['bookingId'] }}</div>
					</div>
					<div class="col-sm-4 billing-item">
						<div><b>Booking Amount</b></div>
						<div class="price">@price($booking['bookingAmount'])</div>
					</div>
					<div class="col-sm-4 billing-item">
						<div>
							@if ($booking['advancePaid'] == 1)
								<b>Advance Paid</b>
							@else
								<b>Advance Amount</b>
							@endif
						</div>
						<div class="price">@price($booking['advanceAmount'])</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="mar-t-10">
					<label>Party Time: </label>
					<span class="mar-l-5">{{ date("d/m/y g:i A", $booking['partyDateTime']) }}</span>
				</div>
				<div class="mar-t-10">
					<label>Description: </label>
					<div>{!! $booking['bookingInfo'] !!}</div>
				</div>
				@if(count($data['additional']['checkoutFieldsCrm'])> 0)
					@foreach($data['additional']['checkoutFieldsCrm'] as $checkoutFieldCrm)
						<div class="mar-t-10">
							@if(!empty($data['additional'][$checkoutFieldCrm['name']]) && $checkoutFieldCrm['type_ticket_booking_id'] == $booking['typeTicketBookingId'])
								<label>{{ ucwords($checkoutFieldCrm['identifier']) }}: </label>
								@if($checkoutFieldCrm['type_field_id'] == config('evibe.input.textarea'))
									<div> {{ $data['additional'][$checkoutFieldCrm['name']] }}  </div>
								@else
									<span class="mar-l-5">{{ $data['additional'][$checkoutFieldCrm['name']] }}</span>
								@endif
							@endif
						</div>
					@endforeach
				@endif
				@if ($booking['prerequisites'])
					<div class="mar-t-10">
						<label>Prerequisites: </label>
						<div>{{ $booking['prerequisites'] }}</div>
					</div>
				@endif
				@if ($booking['facts'])
					<div class="mar-t-10">
						<label>Facts: </label>
						<div>{{ $booking['facts'] }}</div>
					</div>
				@endif
				@if(count($booking['gallery']) > 0)
					<div class="mar-t-10">
						<div>
							<label>Reference Image(s):</label>
						</div>
						@foreach($booking['gallery'] as $gallery)
							<a href="{{ $gallery->getOriginalImagePath() }}" target="_blank" rel="noopener" class="booking-gallery">
								<img src="{{ $gallery->getResultsImagePath() }}" alt="{{ $gallery->title }}" class="booking-image">
							</a>
						@endforeach
					</div>
				@endif
			</div>
		@endforeach
	</div>
	<div class="clearfix"></div>
</div>

