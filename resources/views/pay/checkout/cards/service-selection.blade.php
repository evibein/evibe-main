<div class="co-service-selection">
	<div class="co-service-title">
		Complementary Services
	</div>
	@if(isset($data['invite']) && $data['invite'])
		<div class="col-md-12 no-pad hide">
			<div class="co-service-wrap">
				<div>
					<label>
						<input type="checkbox" id="eInvite" name="eInvite" class="pull-left">
						<div class="co-service-text mar-l-20">
							e-invitation with RSVP
						</div>
					</label>
				</div>
				<a class="valign-mid mar-l-20 font-12" id="btnInviteKnowMore" data-toggle="modal" data-target="#checkoutEInviteBenefitsModal">[Know More]</a>
			</div>
		</div>
	@endif
	<div class="col-md-12 no-pad hide">
		<div class="co-service-wrap">
			<div>
				<label>
					<input type="checkbox" id="eTYCard" name="eTYCard" class="pull-left">
					<div class="co-service-text mar-l-20">
						e-thank you card
					</div>
				</label>
			</div>
			<a class="valign-mid mar-l-20 font-12" id="btnTYCKnowMore" data-toggle="modal" data-target="#checkoutETYCBenefitsModal">[Know More]</a>
		</div>
	</div>
	<div class="clearfix"></div>
</div>