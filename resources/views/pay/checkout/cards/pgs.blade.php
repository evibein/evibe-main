<div class="form-info text-center mar-b-10">
	@if($totalAdvanceToPay > 0)
		<div class="pad-10 font-16 hide">
			<label class="text-normal">
				<input type="checkbox" id="acceptsTerms" name="acceptsTerms" value="1">
				I have read and accept to all <a href="{{ route('terms') }}"
						target="_blank" rel="noopener">terms of booking.</a>
			</label>
		</div>
		<div>
			<div>
				<div class="coupon-code-wrap mar-l-10 mar-r-10 mar-b-20 text-left">
					<h6 class="coupon-code-text-plus no-mar pad-t-15 pad-b-15 pad-l-10">Have Coupon Code?</h6>
					<div class="coupon-code-input-wrap text-center">
						<div class="mar-l-10 mar-r-10 coupon-error-msg-wrap pad-10 hide">
							<div class="text text-danger">
								<i class="glyphicon glyphicon-exclamation-sign"></i>
								<span class="coupon-error-msg mar-l-4"></span>
							</div>
						</div>
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label pad-l-5 mar-r-5">
							<input type="text" id="couponCode" class="mdl-textfield__input" value="">
							<label class="mdl-textfield__label pad-l-5" for="couponCode">
								<span class="label-text">Enter Code</span>
							</label>
						</div>
						<a class="btn btn-default coupon-code-apply-button mar-b-15__400 mar-b-15-400-600">APPLY</a>
						<a class="btn btn-default coupon-code-remove-button hide mar-b-15__400 mar-b-15-400-600">
							<i class="glyphicon glyphicon-remove"></i>
							<span> REMOVE</span>
						</a>
						<div class="mar-l-10 mar-r-10 coupon-success-msg-wrap alert alert-success pad-10 hide">
							<div class="text text-success text-center">
								<span class="coupon-Success-msg">
									<i class="glyphicon glyphicon-ok-circle"></i>
									<span> Congratulations! you got </span>
									<b>
										<span class='rupee-font'>&#8377;</span>
										<span class="coupon-discount-amount"></span>
									</b> discount.
								</span>
							</div>
						</div>
					</div>
				</div>
				<div class="payment-gateways-wrap mar-l-10 mar-r-10">
					<h6 class="coupon-code-text-plus no-mar pad-t-15 pad-l-10 text-left">Select Payment Method</h6>
					<div class="text-center">
						<div class="pull-left text-left pad-t-5 pad-b-15">
							<div class="col-xs-12 col-sm-6">
								<div class="default-payment-option">
									<input type="radio" id="paymentMethodRazorCredit" name="paymentMethod" value="razor" checked data-url="{{ route('pay.pgs.razor.init', request('id')) }}" class="custom-radio-animation" data-payment-type="card">
									<label for="paymentMethodRazorCredit" class="custom-radio-animation-label mar-t-10 mar-l-10">
										<span class="font-20 default-payment-label-text">Credit Card</span>
									</label>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="default-payment-option">
									<input type="radio" id="paymentMethodRazorDebit" name="paymentMethod" value="razor" data-url="{{ route('pay.pgs.razor.init', request('id')) }}" class="custom-radio-animation" data-payment-type="card">
									<label for="paymentMethodRazorDebit" class="custom-radio-animation-label mar-t-10 mar-l-10">
										<span class="font-20 default-payment-label-text">Debit Card</span>
									</label>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="default-payment-option">
									<input type="radio" id="paymentMethodRazorNetBanking" name="paymentMethod" value="razor" data-url="{{ route('pay.pgs.razor.init', request('id')) }}" class="custom-radio-animation" data-payment-type="netbanking">
									<label for="paymentMethodRazorNetBanking" class="custom-radio-animation-label mar-t-10 mar-l-10">
										<span class="font-20 default-payment-label-text">Net Banking</span>
									</label>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="default-payment-option">
									<input type="radio" id="paymentMethodRazorUPI" name="paymentMethod" value="razor" data-url="{{ route('pay.pgs.razor.init', request('id')) }}" class="custom-radio-animation" data-payment-type="upi">
									<label for="paymentMethodRazorUPI" class="custom-radio-animation-label mar-t-10 mar-l-10">
										<span class="font-20 default-payment-label-text">UPI</span>
									</label>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="payment-gateways-hr"></div>
					<div class="text-center">
						<div class="pad-b-15 pad-t-15">
							<div class="col-xs-12 col-sm-6">
								<div class="default-payment-option">
									<input type="radio" id="paymentMethodPaytm" name="paymentMethod" value="paytm" data-url="{{ route('pay.pgs.paytm.init', request('id')) }}" class="custom-radio-animation checkout-custom-radio-animation">
									<label for="paymentMethodPaytm" class="custom-radio-animation-label mar-t-10">
										<img src="{{ $galleryUrl }}/img/logo/logo_paytm.png" alt="paytm" height="30px">
									</label>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="default-payment-option">
									<input type="radio" id="paymentMethodPayU" name="paymentMethod" value="payU" data-url="{{ route('pay.pgs.payU.init', request('id')) }}" class="custom-radio-animation checkout-custom-radio-animation">
									<label for="paymentMethodPayU" class="custom-radio-animation-label mar-t-10">
										<img src="{{ $galleryUrl }}/img/logo/logo_payU.png" alt="PayUMoney" height="30px">
									</label>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="default-payment-option">
									<input type="radio" id="paymentMethodFreeCharge" name="paymentMethod" value="freeCharge" data-url="{{ route('pay.pgs.freeCharge.init', request('id')) }}" class="custom-radio-animation checkout-custom-radio-animation">
									<label for="paymentMethodFreeCharge" class="custom-radio-animation-label mar-t-10">
										<img src="{{ $galleryUrl }}/img/logo/logo_freecharge.png" alt="freecharge" height="30px">
									</label>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-8 col-sm-offset-2 hide">
				<div class="payment-offer-wrap">
					<span>
						Pay using Freecharge and get &#8377;100 instant cash back for purchase above &#8377;4,000. *T&C
					</span>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="pad-20 pad-t-10__400">
				<button id="payBtn" class="btn btn-evibe btn-large btn-pay text-center">
					<span>Pay </span>
					<span class='rupee-font'>&#8377;</span>
					<span class="checkout-advance-amount mar-l-2">{{ $totalAdvanceToPay }}</span>
				</button>
			</div>
		</div>
		<small class="text-muted text-center blk font-11">
			<i>
				By proceeding further you accept to all our
				<a href="{{ route('terms') }}" target="_blank" rel="noopener">terms of booking.</a>
			</i>
		</small>
	@else
		<span class='text-success text-upr text-bold font-18'>Total Advance Received</span>
	@endif
</div>