<div class="form-card-details party-details">
	<a class="form-category-title" data-toggle="collapse" href="#partyDetails" aria-expanded="true" aria-controls="partyDetails">
		<h4>
			Party Details
			<span class="pull-right mar-t-5 ">
				<span class="arrow-up glyphicon glyphicon-chevron-down"></span>
				<span class="arrow-down glyphicon glyphicon-chevron-up"></span>
			</span>
			<span class="clearfix"></span>
		</h4>
	</a>
	<div class="form-info collapse in mob-checkout-form-card" id="partyDetails">
		<div class="form-row">
			<div>
				<div class="col-sm-6">
					<div class="input-field">
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
							<input class="mdl-textfield__input" type="text" name="inpPartyDate" id="inpPartyDate" @if(isset($data['additional']['partyDate']) && $data['additional']['partyDate'] && ($data['additional']['partyDate'] != '01 Jan 1970')) value="{{ $data['additional']['partyDate'] }}" disabled @endif/>
							<label class="mdl-textfield__label" for="partyDate">
								Party Date</label>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="input-field">
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
							<input class="mdl-textfield__input" type="text" name="inpPartyTime" id="inpPartyTime" @if(isset($data['additional']['partyStartTime']) && $data['additional']['partyStartTime']) value="{{ $data['additional']['partyStartTime'] }}" disabled @endif/>
							<label class="mdl-textfield__label" for="partyTime">
								Party Start Time</label>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-6 col-lg-6 input-field mar-b-20">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="splNotes" type="text" class="mdl-textfield__input" value="@if(request()->has('splNotes')){{ old('splNotes') }}@else{!!  $data['additional']['specialNotes'] !!}@endif">
						<label class="mdl-textfield__label">
							<span class="label-text">Special Order Notes (optional)</span>
						</label>
						<div class="hint-msg checkout-hint-msg font-13 text-info">(Needs approval if not already discussed)</div>
					</div>
				</div>
				@if(count($data['additional']['checkoutFieldsUser'])> 0)
					@foreach($data['additional']['checkoutFieldsUser'] as $checkoutFieldUser)
						@if(isset($data['addOnTypeTicketBookingIds']) && count($data['addOnTypeTicketBookingIds']) && in_array($checkoutFieldUser['type_ticket_booking_id'], $data['addOnTypeTicketBookingIds']))
						@else
							@if($checkoutFieldUser['type_field_id'] == config('evibe.input.checkbox'))
								<div class="col-sm-12 col-md-12 col-lg-12 input-field mar-b-10 mar-t-30">
									<div id="{{ $checkoutFieldUser['name'] }}">
										<div>
											<div class="text-bold">{{ $checkoutFieldUser['identifier'] }}</div>
											@if($checkoutFieldUser['hint_message'])
												<div class="hint-msg font-12 text-info">({{ ucfirst($checkoutFieldUser['hint_message']) }})</div>
											@endif
										</div>
										@php
											$valueOptionsArray = [];
											$valueString = $data['additional'][$checkoutFieldUser['name']];
										@endphp
										@if($valueString)
											@php $valueOptionsArray = explode(', ', $valueString); @endphp
										@endif
										@foreach($checkoutFieldUser->checkoutOptions as $checkoutOption)
											<div class="md-checkbox in-blk pad-r-20">
												<input type="checkbox" id="{{ $checkoutOption->id }}" name="{{ $checkoutFieldUser['name'] }}" value="{{ $checkoutOption->name }}" @if(count($valueOptionsArray) && in_array($checkoutOption->name, $valueOptionsArray)) checked @endif>
												<label for="{{ $checkoutOption->id }}">{{ ucwords($checkoutOption->name) }}</label>
											</div>
										@endforeach
									</div>
								</div>
							@else
								<div class="col-sm-6 col-md-6 col-lg-6 input-field mar-b-10 mar-t-10">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<input id="{{ $checkoutFieldUser['name'] }}" type="text" class="mdl-textfield__input" value="@if(request()->has($checkoutFieldUser['name'])){{ old($checkoutFieldUser['name']) }}@else{{$data['additional'][$checkoutFieldUser['name']]  }}@endif">
										<label class="mdl-textfield__label">
											<span class="label-text">{{ ucwords($checkoutFieldUser['identifier']) }}</span>
										</label>
										@if($checkoutFieldUser['hint_message'])
											<div class="hint-msg checkout-hint-msg font-12 text-info">({{ ucfirst($checkoutFieldUser['hint_message']) }})</div>
										@endif
									</div>
								</div>
							@endif
						@endif
					@endforeach
					<input type="hidden" id="dynamicFieldsUser" value='{{ $data['additional']['checkoutFieldsUser'] }}'>
					<input type="hidden" id="hidInputTypeCheckbox" value="{{ config("evibe.input.checkbox") }}">
				@endif
				<div class="clearfix"></div>
			</div>

			<div class="clearfix"></div>
		</div>
	</div>
</div>