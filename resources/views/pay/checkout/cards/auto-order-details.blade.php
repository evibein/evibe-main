<div class="co-order-details form-info mar-b-10">
	<div class="col-md-12 mar-t-10">
		@foreach ($data['bookings']['list'] as $booking)
			@if(isset($booking['itemMapTypeId']) && ($booking['itemMapTypeId'] != config('evibe.ticket.type.add-on')))
				<div class="booking-data">
					<div class="billing">
						@if($booking['bookingAmount'] == $booking['advanceAmount'])
							<div class="col-sm-6 billing-item">
								<div><b>Booking Id</b></div>
								<div>#{{ $booking['bookingId'] }}</div>
							</div>
							<div class="col-sm-6 billing-item">
								<div>
									@if ($booking['advancePaid'] == 1)
										<b>Booking Amount Paid</b>
									@else
										<b>Booking Amount</b>
									@endif
								</div>
								<div class="price">@price($booking['bookingAmount'])</div>
							</div>
						@else
							<div class="col-sm-4 billing-item">
								<div><b>Booking Id</b></div>
								<div>#{{ $booking['bookingId'] }}</div>
							</div>
							<div class="col-sm-4 billing-item">
								<div><b>Booking Amount</b></div>
								<div class="price">@price($booking['bookingAmount'])</div>
							</div>
							<div class="col-sm-4 billing-item">
								<div>
									@if ($booking['advancePaid'] == 1)
										<b>Advance Paid</b>
									@else
										<b>Advance Amount</b>
									@endif
								</div>
								<div class="price">@price($booking['advanceAmount'])</div>
							</div>
						@endif
						<div class="clearfix"></div>
					</div>
					@if(isset($booking['partyDateTime']) && $booking['partyDateTime'])
						<div class="mar-t-10">
							<label>Party Time: </label>
							<span class="mar-l-5">{{ date("d/m/y g:i A", $booking['partyDateTime']) }}</span>
						</div>
					@endif
					<div class="mar-t-10">
						<label>Description: </label>
						<div>{!! $booking['bookingInfo'] !!}</div>
					</div>
					@if(count($data['additional']['checkoutFieldsCrm'])> 0)
						@foreach($data['additional']['checkoutFieldsCrm'] as $checkoutFieldCrm)
							<div class="mar-t-10">
								@if(!empty($data['additional'][$checkoutFieldCrm['name']]) && $checkoutFieldCrm['type_ticket_booking_id'] == $booking['typeTicketBookingId'])
									<label>{{ ucwords($checkoutFieldCrm['identifier']) }}: </label>
									@if($checkoutFieldCrm['type_field_id'] == config('evibe.input.textarea'))
										<div> {{ $data['additional'][$checkoutFieldCrm['name']] }}  </div>
									@else
										<span class="mar-l-5">{{ $data['additional'][$checkoutFieldCrm['name']] }}</span>
									@endif
								@endif
							</div>
						@endforeach
					@endif
					@if ($booking['prerequisites'])
						<div class="mar-t-10">
							<label>Prerequisites: </label>
							<div>{{ $booking['prerequisites'] }}</div>
						</div>
					@endif
					@if ($booking['facts'])
						<div class="mar-t-10">
							<label>Facts: </label>
							<div>{{ $booking['facts'] }}</div>
						</div>
					@endif
					@if(count($booking['gallery']) > 0)
						<div class="mar-t-10">
							<div>
								<label>Reference Image(s):</label>
							</div>
							@foreach($booking['gallery'] as $gallery)
								<a href="{{ $gallery->getOriginalImagePath() }}" target="_blank" rel="noopener" class="booking-gallery">
									<img src="{{ $gallery->getResultsImagePath() }}" alt="{{ $gallery->title }}" class="booking-image">
								</a>
							@endforeach
						</div>
					@endif
				</div>
			@endif
		@endforeach
	</div>
	<div class="clearfix"></div>
</div>

@if(isset($data['addOns']) && count($data['addOns']))
	<div class="co-add-on-details">
		<!-- show already selected add-on details here. if not, show link (don't confuse user thinking that any add-on has been added) -->
		<div class="col-md-12 pad-t-10">
			<div>
				<div class="mar-b-5">
					@if(isset($data['hasAddOn']) && $data['hasAddOn'])
						<div class="co-add-ons-title in-blk mar-t-10">
							<b>Add Ons:</b>
						</div>
						@if(isset($data['ticket']) && $data['ticket'] && !(
						($data['ticket']->status_id == config('evibe.ticket.status.booked')) ||
						($data['ticket']->status_id == config('evibe.ticket.status.auto_paid')) ||
						($data['ticket']->status_id == config('evibe.ticket.status.service_auto_pay'))))
							<div class="co-add-ons-edit in-blk">
								<a class="btn btn-warning" id="checkoutAddOns">
									Add/Remove Add-Ons
								</a>
							</div>
						@endif
						<div class="clearfix"></div>
					@else
						@if(isset($data['ticket']) && $data['ticket'] && !(
						($data['ticket']->status_id == config('evibe.ticket.status.booked')) ||
						($data['ticket']->status_id == config('evibe.ticket.status.auto_paid')) ||
						($data['ticket']->status_id == config('evibe.ticket.status.service_auto_pay'))))
							<div class="text-center">
								<a class="btn btn-warning" id="checkoutAddOns">
									Add Add-Ons
								</a>
							</div>
						@endif
					@endif
				</div>
				<div class="co-add-ons-wrap">
					@foreach ($data['bookings']['list'] as $booking)
						@if(isset($booking['itemMapTypeId']) && ($booking['itemMapTypeId'] == config('evibe.ticket.type.add-on')))
							<div class="co-add-on-wrap mar-t-5" data-id="{{ $booking['itemMapId'] }}" data-booking-units="{{ $booking['bookingUnits'] }}" data-product-price="{{ $booking['productPrice'] }}">
								<div class="col-md-4 col-sm-4 col-xs-4 no-pad-l">
									@if(count($booking['gallery']) > 0)
										@php $profileImage = $booking['gallery'][0] @endphp
										<div class="co-add-on-img-wrap">
											<a href="{{ $profileImage->getOriginalImagePath() }}" target="_blank" rel="noopener" class="booking-gallery">
												<img src="{{ $profileImage->getResultsImagePath() }}" alt="{{ $profileImage->title }}" class="co-add-on-img no-mar-t">
											</a>
										</div>
									@endif
								</div>
								<div class="col-md-8 col-sm-8 col-xs-8 no-pad">
									<div class="co-add-on-title">
										<b>{{ $booking['name'] }}</b>
									</div>
									<div class="co-add-on-price mar-t-5">
										<span>@price($booking['productPrice'])</span>
										@if(isset($booking['bookingUnits']) && ($booking['bookingUnits'] > 1))
											<span> X {{ $booking['bookingUnits'] }}</span>
										@endif
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						@endif
					@endforeach
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endif
