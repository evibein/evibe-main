<div class="form-card-details party-details">
	<a class="form-category-title" data-toggle="collapse" href="#partyDetails" aria-expanded="true" aria-controls="partyDetails">
		<h4>
			Order Details
			<span class="arrow-up mar-t-5 pull-right glyphicon glyphicon-chevron-down"></span>
			<span class="arrow-down mar-t-5 pull-right glyphicon glyphicon-chevron-up"></span>
		</h4>
	</a>
	<div class="form-info collapse in mob-checkout-form-card" id="partyDetails">
		<div class="form-row">
			<div class="col-sm-12 mar-t-10 mar-b-15">
				<div class="booking-info font-18">
					{{ $data['booking']['bookingInfo'] }}
					@if($data['isVenueBooking'] == 0 && isset($data["additional"]["cityName"]) && $data["additional"]["cityName"])
						({{ $data["additional"]["cityName"] }})
					@endif
				</div>
			</div>
			@if(!empty($data['booking']['checkInDate']))
				<div class="col-sm-4">
					<label for="partyDate">@if ($data['isVenueBooking'] == 0) Delivery Date @else Check in: @endif</label>
					<div class="center-block font-14">
						{{ date('d M Y ',strtotime($data['booking']['checkInDate']))}}
						({{  date('D', strtotime($data['booking']['checkInDate'])) }})
					</div>
				</div>
				@if($data['isVenueBooking'] == 1)
					<div class="col-sm-4">
						<label for="partyDate">Check In Time</label>
						<div class="center-block font-14">
							{{ $data['booking']['checkInTime'] }}
						</div>
					</div>
					<div class="col-sm-4">
						<label for="partyDate">Check Out Time</label>
						<div class="center-block font-14">
							{{ $data['booking']['checkOutTime'] }}
						</div>
					</div>
				@else
					<div class="col-sm-4">
						<label for="partyDate">Party Time</label>
						<div class="center-block font-14">
							{{ $data['booking']['checkInTime'] }}
						</div>
					</div>
				@endif
			@endif
			@if (!empty($data['additional']['guestsCount']))
				<div class="col-sm-12 mar-t-15">
					<label for="partyDate">Guests Count: </label>
					<span>{{ $data['additional']['guestsCount'] }}</span>
				</div>
			@endif
			<div class="col-sm-6 col-md-6 col-lg-6 input-field mar-b-20">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					<input id="splNotes" type="text" class="mdl-textfield__input" value="@if(request()->has('splNotes')){{ old('splNotes') }}@else{!!$data['additional']['specialNotes']!!}@endif">
					<label class="mdl-textfield__label">
						<span class="label-text">Special Order Notes (optional)</span>
					</label>
					<div class="hint-msg checkout-hint-msg font-13 text-info">(Needs approval if not already discussed)</div>
				</div>
			</div>
			@if(count($data['additional']['checkoutFieldsUser'])> 0)
				@foreach($data['additional']['checkoutFieldsUser'] as $checkoutFieldUser)
					<div class="col-sm-6 col-md-6 col-lg-6 input-field mar-b-10 mar-t-10">
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
							<input id="{{ $checkoutFieldUser['name'] }}" type="text" class="mdl-textfield__input" value="@if(request()->has($checkoutFieldUser['name'])){{ old($checkoutFieldUser['name']) }}@else{{$data['additional'][$checkoutFieldUser['name']]  }}@endif">
							<label class="mdl-textfield__label">
								<span class="label-text">{{ ucwords($checkoutFieldUser['identifier']) }}</span>
							</label>
							@if($checkoutFieldUser['hint_message'])
								<div class="hint-msg checkout-hint-msg font-13 text-info">({{ ucfirst($checkoutFieldUser['hint_message']) }})</div>
							@endif
						</div>
					</div>
				@endforeach
				<input type="hidden" id="dynamicFieldsUser" value='{{ $data['additional']['checkoutFieldsUser'] }}'>
			@endif
			<div class="clearfix"></div>
		</div>
	</div>
</div>