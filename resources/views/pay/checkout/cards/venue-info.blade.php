<div class="form-card-details venue-details">
	<a class="form-category-title" data-toggle="collapse" href="#venueDetails" aria-expanded="true" aria-controls="venueDetails">
		<h4>
			@if(isset($isChristmas) && $isChristmas) Delivery Address @else @yield('address-card-title', 'Venue Details') @endif
			<span class="pull-right mar-t-5 ">
				<span class="arrow-up glyphicon glyphicon-chevron-down"></span>
				<span class="arrow-down glyphicon glyphicon-chevron-up"></span>
			</span>
			<span class="clearfix"></span>
		</h4>
	</a>
	<div class="form-info collapse in mob-checkout-form-card" id="venueDetails">
		<div class="form-row">
			<div class="col-sm-6 col-md-6 col-lg-6 input-field">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					<input id="venueAddressLine1" type="text" class="mdl-textfield__input" value="@if(request()->has('venueAddressLine1')){{ old('venueAddressLine1') }}@else{{ $data['additional']['venueAddressLine1'] }}@endif">
					<label class="mdl-textfield__label" for="venueAddressLine1">
						<span class="label-text">@yield('address-card-delivery-line', 'Venue') Address Line 1</span>
					</label>
				</div>
			</div>
			<div class="col-sm-6 col-md-6 col-lg-6 input-field">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					<input id="venueAddressLine2" type="text" class="mdl-textfield__input" value="@if(request()->has('venueAddressLine2')){{ old('venueAddressLine2') }}@else{{ $data['additional']['venueAddressLine2'] }}@endif">
					<label class="mdl-textfield__label" for="venueAddressLine2">
						<span class="label-text">@yield('address-card-delivery-line', 'Venue') Address Line 2</span>
					</label>
				</div>
			</div>
			<div class="col-sm-6 col-md-6 col-lg-6 input-field">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					<input id="venueLocation" type="text" class="mdl-textfield__input google-auto-complete" placeholder="" value="@if(old('venueLocation')){{ old('venueLocation') }}@else{{ $data['additional']['venueLocation'] }}@endif">
					<label class="mdl-textfield__label" for="venueLocation">
						<span class="label-text">@yield('address-card-delivery-line', 'Venue') Location</span>
					</label>
					<input type="hidden" class="google-location-details" name="locationDetails" value="">
				</div>
			</div>
			<div class="col-sm-6 col-md-6 col-lg-6 input-field">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					<input id="venueLandmark" type="text" class="mdl-textfield__input" value="@if(request()->has('venueLandmark')){{ old('venueLandmark') }}@else{{ $data['additional']['venueLandmark'] }}@endif">
					<label class="mdl-textfield__label" for="venueLandmark">
						<span class="label-text">@yield('address-card-delivery-line', 'Venue') Landmark</span>
					</label>
				</div>
			</div>
			<div class="col-sm-6 col-md-6 col-lg-6 input-field">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					<input id="venueZipCode" type="text" class="mdl-textfield__input" @if($data['additional']['venueZipCode']) disabled @endif value="@if(old('venueZipCode')){{ old('venueZipCode') }}@elseif($data['additional']['venueZipCode']){{ $data['additional']['venueZipCode'] }}@endif">
					<label class="mdl-textfield__label" for="venueZipCode">
						<span class="label-text">Pin Code</span>
					</label>
				</div>
			</div>
			@if(!empty($isShowVenueType) && $isShowVenueType)
				<div class="col-sm-6 col-md-6 col-lg-6 input-field">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<label class="mdl-textfield__label" for="typeVenue">
							<span class="label-text">Venue Type</span>
						</label>
						<select id="typeVenue" name="typeVenue" class="mdl-textfield__input mar-t-10">
							<option value="-1">--- Select your venue type ---</option>
							@foreach($data['additional']['typeVenues'] as $venueType)
								<option value="{{ $venueType->id }}"
										@if($data['additional']['ticketVenueType'] && $data['additional']['ticketVenueType']->id == $venueType->id) selected @endif>{{ $venueType->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
			@endif
			<div class="clearfix"></div>
		</div>
		<div class="google-map-warp">
			<div class="map-label pad-l-10__400-600 pad-r-10__400-600">Locate your @yield('map-location-text', 'venue') on map
				<div class="hint-msg font-13 text-info">Drag the marker to the closest landmark of your @yield('map-location-text', 'party venue'). (< 1 KM)</div>
			</div>
			<div id="googleMap">
				<input id="pac-input" class="controls" type="text" placeholder="Search your location">
				<div class="input-field no-pad" id="map"></div>
				<div id="warningMapMessage" class="hide">
					<i class="glyphicon glyphicon-warning-sign mar-r-4"></i> Marked location's pin code does not match with given pin code.
				</div>
			</div>
			<div class="hide"> <!-- saving the map Data -->
				<div id="mapAddress" data-address=""></div>
				<div id="mapLat" data-lat="@if(!is_null($data['additional']['lat'])){{ $data['additional']['lat'] }}@else 0 @endif"></div>
				<div id="mapLng" data-lng="@if(!is_null($data['additional']['lng'])){{ $data['additional']['lng'] }}@else 0 @endif"></div>
				<div id="cityName" data-city="{{ $data['additional']['cityName'] }}"></div>
				<input type="text" id="mapMarker" value="{{ config('evibe.gallery.host') }}/img/icons/map_marker.png">
			</div>
		</div>
	</div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key={{ config('evibe.google.map_key') }}&libraries=places"></script>
@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			function showGoogleMap() {

				var allAddress = [];

				function initializeMap(latLng) {

					/* initializing the map */
					var mapDiv = document.getElementById('map');
					var map = new google.maps.Map(mapDiv, {
						center: latLng,
						zoom: 16,
						disableDefaultUI: true,
						zoomControl: true,
						zoomControlOptions: {
							style: google.maps.ZoomControlStyle.LARGE
						}
					});

					/* initializing the marker */
					var marker = new google.maps.Marker({
						map: map,
						position: latLng,
						draggable: true,
						icon: $('#mapMarker').val()
					});

					/* initializing the info window */
					var infoWindow = new google.maps.InfoWindow;

					/* Create the search box and link it to the UI element. */
					var input = document.getElementById('pac-input');
					var searchBox = new google.maps.places.SearchBox(input);
					map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

					/* Bias the SearchBox results towards current map's viewport. */
					map.addListener('bounds_changed', function () {
						searchBox.setBounds(map.getBounds());
					});
					searchBox.addListener('places_changed', function () {
						var places = searchBox.getPlaces();

						if (places.length == 0) {
							return;
						}

						/* For each place, get the icon, name and location. */
						var bounds = new google.maps.LatLngBounds();
						places.forEach(function (place) {
							if (!place.geometry) {
								console.log("Returned place contains no geometry");
								return;
							}

							/* clear the old marker */
							marker.setMap(null);

							/* overriding the existing marker to show at user searched location */
							marker = new google.maps.Marker({
								map: map,
								title: place.name,
								position: place.geometry.location,
								draggable: true,
								icon: $('#mapMarker').val()
							});
							/* update teh marker address of a marker by default or after dragging */
							updateMarkerAddress(map, marker, infoWindow);
							dragMarker(map, marker, infoWindow);

							if (place.geometry.viewport) {
								/* Only geocodes have viewport. */
								bounds.union(place.geometry.viewport);
							} else {
								bounds.extend(place.geometry.location);
							}
						});
						map.fitBounds(bounds);
					});

					/* update teh marker address of a marker by default or after dragging */
					updateMarkerAddress(map, marker, infoWindow);
					dragMarker(map, marker, infoWindow);
				}

				function getUpdatedAddress(map, lat, lng, marker, infoWindow, showContent) {

					var geocoder = new google.maps.Geocoder;
					var latLng = {'lat': lat, 'lng': lng};

					geocoder.geocode({'location': latLng}, function (results, status) {
						if (status === 'OK') {
							if (results[0]) {
								var address = results[0].formatted_address;
								$('#mapAddress').data('address', address);
								$('#mapLat').data('lat', lat);
								$('#mapLng').data('lng', lng);

								if (showContent) {
									infoWindow.setContent(address);
									infoWindow.open(map, marker);
								}
								checkPostalCode(results);
							}
						}
					});
				}

				/* loading the map if map address is available otherwise generating */
				if ($('#mapLat').data('lat') != 0 && $('#mapLng').data('lng') != 0) {
					var latLng = {'lat': $('#mapLat').data('lat'), 'lng': $('#mapLng').data('lng')};
					initializeMap(latLng);
				}
				else {
					var cityName = $('#cityName').data('city');
					var zip = $('#venueZipCode').val();
					var location = $('#venueLocation').val();
					var address = $('#venueAddressLine2').val() + ', ' + location + ', ' + cityName + ', ' + zip;
					var landmark = $('#venueLandmark').val() + ', ' + location + ', ' + cityName + ', ' + zip;
					var zipCode = zip;

					if (address.length) {
						allAddress.push(address);
					}
					allAddress.push(landmark);
					allAddress.push(zipCode);

					$.each(allAddress, function (key, value) {
						var geocoder = new google.maps.Geocoder();
						geocoder.geocode({'address': value}, function (results, status) {
							if (status == google.maps.GeocoderStatus.OK) {
								showGoogleMap(results);
								return false;
							}
						});
					});

					function showGoogleMap(results) {
						var lat = results[0].geometry.location.lat();
						var lng = results[0].geometry.location.lng();
						var latLng = {'lat': lat, 'lng': lng};
						initializeMap(latLng);
					}
				}

				/* function will handle the draggable event and update the input box. */
				function dragMarker(map, marker, infoWindow) {
					google.maps.event.addListener(marker, 'dragend', function (evt) {
						var lat = this.position.lat();
						var lng = this.position.lng();
						getUpdatedAddress(map, lat, lng, marker, infoWindow, true);
					});
				}

				/* check for the postal code */
				function checkPostalCode(results) {
					var $warningMapMessage = $('#warningMapMessage');
					var postalCode;
					$.each(results[0].address_components, function (key, value) {
						if (value['types'][0] == "postal_code") {
							postalCode = value['long_name'];
							return true;
						}
					});

					var venueZipCode = $('#venueZipCode').val();
					if (postalCode && venueZipCode && (postalCode !== $.trim(venueZipCode))) {
						$warningMapMessage.removeClass('hide');
					}
					else {
						$warningMapMessage.addClass('hide');
					}
				}

				function updateMarkerAddress(map, marker, infoWindow) {
					var lat = marker.position.lat();
					var lng = marker.position.lng();
					getUpdatedAddress(map, lat, lng, marker, infoWindow, false);
				}
			}

			showGoogleMap();
		});
	</script>
@endsection