@if(isset($data['uploadImage']) && $data['uploadImage'])
	<div class="form-card-details upload-details">
		<a class="form-category-title" data-toggle="collapse" href="#uploadDetails" aria-expanded="true" aria-controls="uploadDetails">
			<h4>
				Upload Images
				<span class="arrow-up mar-t-5 pull-right glyphicon glyphicon-chevron-down"></span>
				<span class="arrow-down mar-t-5 pull-right glyphicon glyphicon-chevron-up"></span>
			</h4>
		</a>
		<div class="form-info collapse in" id="uploadDetails">
			<div class="form-row">
				<form id="uploadImagesForm" data-url="{{ route('ajax.auto-book.upload-customer-images', $data['ticketId']) }}">
					<div class="image-upload-text mar-b-5">
						Kindly upload images for the applicable add-ons added
					</div>
					<div class="image-upload-rules text-info">
						( Valid Formats: JPG, jpeg, jpg and png. Maximum file size: <b><u>1 MB</u></b> )
					</div>
					<div class="form-group mar-t-10">
						<input type="hidden" id="uploadImage" value="{{ $data['uploadImage'] }}">
						<input type="hidden" id="uploadTicketId" value="{{ $data['ticketId'] }}">
						<div class="in-blk">
							<input type="file" name="checkoutImages[]" multiple/>
						</div>
						<div class="in-blk">
							<button type="submit" id="imageUploadBtn" class="btn btn-warning btn-sm">
								<i class="glyphicon glyphicon-upload"></i> Upload
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="form-info collapse in hide" id="uploadCompleted">
			<div class="upload-successful-text">
				Your upload was successful
			</div>
		</div>
	</div>
@endif