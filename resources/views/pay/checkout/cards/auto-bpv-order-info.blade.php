<div class="form-card-details party-details">
	<a class="form-category-title" data-toggle="collapse" href="#partyDetails" aria-expanded="true" aria-controls="partyDetails">
		<h4>
			Order Details
			<span class="arrow-up mar-t-5 pull-right glyphicon glyphicon-chevron-down"></span>
			<span class="arrow-down mar-t-5 pull-right glyphicon glyphicon-chevron-up"></span>
		</h4>
	</a>
	<div class="form-info collapse in mob-checkout-form-card" id="partyDetails">
		<div class="form-row">
			<div class="col-sm-12 mar-t-10 mar-b-15">
				<div class="booking-info font-18">
					{{ $data['booking']['bookingInfo'] }}
				</div>
			</div>
			@if(!empty($data['booking']['checkInDate']))
				<div class="col-sm-4">
					<label for="partyDate">@if(isset($isChristmas)) Delivery Date @else Check In Date @endif</label>
					<div class="center-block font-14">
						{{ date('d M Y ',strtotime($data['booking']['checkInDate']))}}
						({{  date('D', strtotime($data['booking']['checkInDate'])) }})
					</div>
				</div>
				@if(empty($isChristmas))
					<div class="col-sm-4">
						<label for="partyDate">Check In Time</label>
						<div class="center-block font-14">
							{{ $data['booking']['checkInTime'] }}
						</div>
					</div>
					<div class="col-sm-4">
						<label for="partyDate">Check Out Time</label>
						<div class="center-block font-14">
							{{ $data['booking']['checkOutTime'] }}
						</div>
					</div>
				@endif
			@endif
			@if (!empty($data['additional']['guestsCount']))
				<div class="col-sm-12 mar-t-15">
					<label for="partyDate">Guests Count: </label>
					<span>{{ $data['additional']['guestsCount'] }}</span>
				</div>
			@endif
			<div class="col-sm-6 col-md-6 col-lg-6 input-field mar-b-20">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
					<input id="splNotes" type="text" class="mdl-textfield__input" value="@if(request()->has('splNotes')){{ old('splNotes') }}@else{!!  $data['additional']['specialNotes'] !!}@endif">
					<label class="mdl-textfield__label">
						<span class="label-text">Special Order Notes (optional)</span>
					</label>
					<div class="hint-msg checkout-hint-msg font-13 text-info">(Needs approval if not already discussed)</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>