<div class="form-card-details party-details">
	<a class="form-category-title" data-toggle="collapse" href="#partyDetails" aria-expanded="true" aria-controls="partyDetails">
		<h4>
			Order Details
			<span class="arrow-up mar-t-5 pull-right glyphicon glyphicon-chevron-down"></span>
			<span class="arrow-down mar-t-5 pull-right glyphicon glyphicon-chevron-up"></span>
		</h4>
	</a>
	<div class="form-info collapse in mob-checkout-form-card" id="partyDetails">
		<div class="form-row">
			<div class="col-sm-12 mar-t-10 mar-b-15">
				<div class="booking-info font-18">
					{{ $data['booking']['bookingInfo'] }}
				</div>
			</div>
			<div>
				<div class="@if($data['booking']['partyDate']) hide @endif">
					<div class="col-sm-6">
						<div class="input-field">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="inpPartyDate" id="inpPartyDate" @if($data['booking']['partyDate']) disabled @endif value="@if($data['booking']['partyDate']) {{ $data['booking']['partyDate'] }} @endif"/>
								<label class="mdl-textfield__label" for="partyDate">
									Delivery Date
								</label>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>