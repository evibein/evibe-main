<div class="co-order-details form-info">
	<div class="col-md-12 mar-t-10">

		<div class="booking-data">
			<div class="billing">
				<div class="col-sm-4 billing-item">
					<div><b>Booking Id</b></div>
					<div>#{{ $data['booking']['bookingId'] }}</div>
				</div>
				<div class="col-sm-4 billing-item">
					<div><b>Decor Price</b></div>
					<div class="price">@price($data['booking']['baseBookingAmount'])</div>
				</div>
				<div class="col-sm-4 billing-item">
					<div>
						<b>Transportation Charges</b>
					</div>
					@if($data['additional']['delivery'] > 0)
						<div class="price">@price($data['additional']['delivery'])</div>
					@else
						@if(isset($data['booking']['estimatedTransportCharges']) && $data['booking']['estimatedTransportCharges'] > 0)
							<div class="">Free*</div>
						@else
							<span class="text-success">Free</span>
						@endif
					@endif
				</div>
				<div class="clearfix"></div>
				<div class="clearfix"></div>
			</div>
			@if(isset($data['booking']['estimatedTransportCharges']) && $data['booking']['estimatedTransportCharges'] > 0)
				<div class="notice-transport-message mar-t-5 mar-b-10">* Transportation charges might be applicable based on the exact location. We will notify you accordingly.</div>
			@endif
			@if($data['additional']['itemInfo'])
				<div class="mar-t-10">
					<label>Description: </label>
					<div>{!! $data['additional']['itemInfo'] !!}</div>
				</div>
			@endif
			@if(count($data['additional']['checkoutFieldsCrm'])> 0)
				@foreach($data['additional']['checkoutFieldsCrm'] as $checkoutFieldCrm)
					<div class="mar-t-10">
						@if(!empty($data['additional'][$checkoutFieldCrm['name']]) && $checkoutFieldCrm['type_ticket_booking_id'] == $data['booking']['typeTicketBookingId'])
							<label>{{ ucwords($checkoutFieldCrm['identifier']) }}: </label>
							@if($checkoutFieldCrm['type_field_id'] == config('evibe.input.textarea'))
								<div> {{ $data['additional'][$checkoutFieldCrm['name']] }}  </div>
							@else
								<span>{{ $data['additional'][$checkoutFieldCrm['name']] }}</span>
							@endif
						@endif
					</div>
				@endforeach
			@endif
			@if ($data['booking']['prerequisites'])
				<div class="mar-t-10">
					<label>Prerequisites: </label>
					<div>{{ $data['booking']['prerequisites'] }}</div>
				</div>
			@endif
			@if ($data['booking']['facts'])
				<div class="mar-t-10">
					<label>Facts: </label>
					<div>{{ $data['booking']['facts'] }}</div>
				</div>
			@endif
			@if(count($data['booking']['gallery']) > 0)
				<div class="mar-t-10">
					<div>
						<label>Reference Image(s):</label>
					</div>
					@foreach($data['booking']['gallery'] as $gallery)

						<a href="{{ $gallery->getOriginalImagePath() }}" target="_blank" rel="noopener" class="booking-gallery">
							<img src="{{ $gallery->getResultsImagePath() }}" alt="{{ $gallery->title }}" class="booking-image">
						</a>
					@endforeach
				</div>
			@endif
		</div>
	</div>
	<div class="clearfix"></div>
</div>

