<div class="form-card-details addd-on-details">
	<a class="form-category-title" data-toggle="collapse" href="#addOnDetails" aria-expanded="true" aria-controls="addOnDetails">
		<h4>
			Add On Details
			<span class="pull-right mar-t-5 ">
				<span class="arrow-up glyphicon glyphicon-chevron-down"></span>
				<span class="arrow-down glyphicon glyphicon-chevron-up"></span>
			</span>
			<span class="clearfix"></span>
		</h4>
	</a>
	<div class="form-info collapse in mob-checkout-form-card" id="addOnDetails">
		<div class="form-row">
			<div>
				@if(count($data['additional']['checkoutFieldsUser'])> 0)
					@foreach($data['additional']['checkoutFieldsUser'] as $checkoutFieldUser)
						@if(isset($data['addOnTypeTicketBookingIds']) && count($data['addOnTypeTicketBookingIds']) && in_array($checkoutFieldUser['type_ticket_booking_id'], $data['addOnTypeTicketBookingIds']))
							@if($checkoutFieldUser['type_field_id'] == config('evibe.input.checkbox'))
								<div class="col-sm-12 col-md-12 col-lg-12 input-field mar-b-10 mar-t-30">
									<div id="{{ $checkoutFieldUser['name'] }}">
										<div>
											<div class="text-bold">{{ $checkoutFieldUser['identifier'] }}</div>
											@if($checkoutFieldUser['hint_message'])
												<div class="hint-msg font-12 text-info">({{ ucfirst($checkoutFieldUser['hint_message']) }})</div>
											@endif
										</div>
										@php
											$valueOptionsArray = [];
											$valueString = $data['additional'][$checkoutFieldUser['name']];
										@endphp
										@if($valueString)
											@php $valueOptionsArray = explode(', ', $valueString); @endphp
										@endif
										@foreach($checkoutFieldUser->checkoutOptions as $checkoutOption)
											<div class="md-checkbox in-blk pad-r-20">
												<input type="checkbox" id="{{ $checkoutOption->id }}" name="{{ $checkoutFieldUser['name'] }}" value="{{ $checkoutOption->name }}" @if(count($valueOptionsArray) && in_array($checkoutOption->name, $valueOptionsArray)) checked @endif>
												<label for="{{ $checkoutOption->id }}">{{ ucwords($checkoutOption->name) }}</label>
											</div>
										@endforeach
									</div>
								</div>
							@else
								<div class="col-sm-6 col-md-6 col-lg-6 input-field mar-b-10 mar-t-10">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<input id="{{ $checkoutFieldUser['name'] }}" type="text" class="mdl-textfield__input" value="@if(request()->has($checkoutFieldUser['name'])){{ old($checkoutFieldUser['name']) }}@else{{$data['additional'][$checkoutFieldUser['name']]  }}@endif">
										<label class="mdl-textfield__label">
											<span class="label-text">{{ ucwords($checkoutFieldUser['identifier']) }}</span>
										</label>
										@if($checkoutFieldUser['hint_message'])
											<div class="hint-msg checkout-hint-msg font-13 text-info">({{ ucfirst($checkoutFieldUser['hint_message']) }})</div>
										@endif
									</div>
								</div>
							@endif
						@else
						@endif
					@endforeach
				@endif
				@if(isset($data['uploadImage']) && $data['uploadImage'])
					<div class="col-sm-12 col-md-12 col-lg-12 mar-b-20 mar-t-30">
						<div class="upload-details">
							<div class="" id="uploadDetails">
								<div class="form-row">
									<form id="uploadImagesForm" data-url="{{ route('ajax.auto-book.upload-customer-images', $data['ticketId']) }}">
										<div class="image-upload-text mar-b-5">
											<b>Upload images</b>
										</div>
										<div class="image-upload-rules text-info">
											( Valid Formats: JPG, jpeg, jpg and png. Maximum file size:
											<b><u>1 MB</u></b> )
										</div>
										<div class="form-group mar-t-10">
											<input type="hidden" id="uploadImage" value="{{ $data['uploadImage'] }}">
											<input type="hidden" id="uploadTicketId" value="{{ $data['ticketId'] }}">
											<div class="in-blk">
												<input type="file" name="checkoutImages[]" multiple/>
											</div>
											<div class="in-blk">
												<button type="submit" id="imageUploadBtn" class="btn btn-warning btn-sm">
													<i class="glyphicon glyphicon-upload"></i> Upload
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
							<div class="hide" id="uploadCompleted">
								<div class="upload-successful-text">
									Your upload was successful
								</div>
							</div>
						</div>
					</div>
				@endif
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>