<div class="co-order-details form-info">
	<div class="col-md-12 mar-t-10">

		<div class="booking-data">
			<div class="billing">
				<div class="col-sm-6 billing-item">
					<div><b>Booking Id</b></div>
					<div>#{{ $data['booking']['bookingId'] }}</div>
				</div>
				<div class="col-sm-6 text-center billing-item">
					<div><b>Price</b></div>
					<div class="price">@price($data['booking']['totalBookingAmount'])</div>
				</div>
				<div class="clearfix"></div>
			</div>
			@if(isset($data['booking']['estimatedTransportCharges']) && $data['booking']['estimatedTransportCharges'] > 0)
				<div class="notice-transport-message mar-t-5 mar-b-10">* Transportation charges might be applicable based on the exact location. We will notify you accordingly.</div>
			@endif
			@if($data['additional']['itemInfo'])
				<div class="mar-t-10">
					<label>Description: </label>
					<div>{!! $data['additional']['itemInfo'] !!}</div>
				</div>
			@endif
			@if ($data['booking']['prerequisites'])
				<div class="mar-t-10">
					<label>Prerequisites: </label>
					<div>{{ $data['booking']['prerequisites'] }}</div>
				</div>
			@endif
			@if ($data['booking']['facts'])
				<div class="mar-t-10">
					<label>Facts: </label>
					<div>{{ $data['booking']['facts'] }}</div>
				</div>
			@endif
		</div>
	</div>
	<div class="clearfix"></div>
</div>

