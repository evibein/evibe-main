@extends('pay.base')

@section('content')
	@parent
	<div class="checkout-page-2 auto-checkout-page">
		<div class="checkout-hidden-details-container hide">
			<input id="ticketId" type="hidden" value="{{ request('id') }}">
			<input id="custEmail" type="hidden" value="{{ $data['customer']['email'] }}">
			<input id="advanceAmount" type="hidden" value="{{ $data['bookings']['totalAdvanceToPay'] }}">
			<input id="addOnTypeId" type="hidden" value="{{ config('evibe.ticket.type.add-on') }}">
			<!-- Dynamic checkout fields should be in orders page as they will be refreshed each time -->
			<input type="hidden" id="dynamicFieldsCustomerData" value='{{ $data['additional']['checkoutFieldsCustomerData'] }}'>
			<input type="hidden" id="hidUploadSuccess" value="">
			<input type="hidden" id="hidProofUploadSuccess" value="@if(isset($data['customerOrderProofs'])) {{ $data['customerOrderProofs'] }} @endif">
			<input type="hidden" id="hidInputTypeCheckbox" value="{{ config("evibe.input.checkbox") }}">
			<input type="hidden" id="hidInputTypeOption" value="{{ config("evibe.input.option") }}">
			<input type="hidden" id="hidValidateVenueDetailsUrl" value="{{ route('auto-book.pay.auto.validate.venue-details', ['ticketId' => request('id')]) }}">
			<input type="hidden" id="hidValidatePartyDetailsUrl" value="{{ route('auto-book.pay.auto.validate.party-details', ['ticketId' => request('id')]) }}">
			<input type="hidden" id="hidValidateOrderDetailsUrl" value="{{ route('auto-book.pay.auto.validate.order-details', ['ticketId' => request('id')]) }}">
			<input type="hidden" id="hidValidateProofDetailsUrl" value="{{ route('auto-book.pay.auto.validate.proof-details', ['ticketId' => request('id')]) }}">
			<input type="hidden" id="hidValidateContactDetailsUrl" value="{{ route('auto-book.pay.auto.validate.contact-details', ['ticketId' => request('id')]) }}">
			<input type="hidden" id="hidUpdateOrderDetailsUrl" value="{{ route('auto-book.pay.auto.update.order-details', ['ticketId' => request('id')]) }}">
			<input type="hidden" id="hidUpdatePriceDetailsUrl" value="{{ route('auto-book.pay.auto.update.price-details', ['ticketId' => request('id')]) }}">
		</div>
		@if($data['bookings']['totalAdvanceToPay'] > 0)
			<div class="checkout-navigation-container">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="checkout-navigation-card-wrap">
						<div id="wizard" class="checkout-navigation-list form_wizard wizard_horizontal pad-l-5 pad-r-5">
							<ul class="wizard_steps anchor no-pad no-mar-t no-mar-b">
								@if (!$data['hasVenueBooking'])
									<li>
										<a data-ref="venueDetails" data-checkout-card-count="" class="checkout-navigation-btn btn-checkout-navigation-venue disabled">
											<span class="step_no"></span>
											<span class="step_descr">Venue Address<br></span>
										</a>
									</li>
								@endif
								<li>
									<a data-ref="partyDetails" data-checkout-card-count="" class="checkout-navigation-btn btn-checkout-navigation-party disabled">
										<span class="step_no"></span>
										<span class="step_descr">Party Info<br></span>
									</a>
								</li>
								@if(isset($data['addOns']) && count($data['addOns']))
									<li>
										<a data-ref="addOnDetails" data-checkout-card-count="" class="checkout-navigation-btn btn-checkout-navigation-add-on disabled">
											<span class="step_no"></span>
											<span class="step_descr">Add Ons<br></span>
										</a>
									</li>
								@endif
								<li>
									<a data-ref="orderDetails" data-checkout-card-count="" class="checkout-navigation-btn btn-checkout-navigation-order disabled">
										<span class="step_no"></span>
										<span class="step_descr">Order Details<br></span>
									</a>
								</li>
								@if (isset($data['needCustomerProof']) && $data['needCustomerProof'])
									<li>
										<a data-ref="proofDetails" data-checkout-card-count="" class="checkout-navigation-btn btn-checkout-navigation-proof disabled">
											<span class="step_no"></span>
											<span class="step_descr">ID Proofs<br></span>
										</a>
									</li>
								@endif
								<!-- Contact Details has been merged with Payment Details -->
								<li>
									<a data-ref="paymentDetails" data-checkout-card-count="" class="checkout-navigation-btn btn-checkout-navigation-payment disabled">
										<span class="step_no"></span>
										<span class="step_descr">Contact Details<br></span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="checkout-details-container mar-t-15">
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					@if (!$data['hasVenueBooking'])
						<div id="venueDetails" class="checkout-card-wrap hide" data-ref="venueDetails">
							@include('pay.checkout.nav-cards.venue-details', ['isShowVenueType' => $data['isShowVenueType']])
						</div>
					@endif
					<div id="partyDetails" class="checkout-card-wrap hide">
						@include('pay.checkout.nav-cards.party-details')
					</div>
					@if(isset($data['addOns']) && count($data['addOns']))
						<div id="addOnDetails" class="checkout-card-wrap hide">
							@include('pay.checkout.nav-cards.add-on-details')
						</div>
					@endif
					<div id="orderDetails" class="checkout-card-wrap hide">
						@include('pay.checkout.nav-cards.order-details', ['viewData' => ['email' => true]])
					</div>
					@if (isset($data['needCustomerProof']) && $data['needCustomerProof'])
						<div id="proofDetails" class="checkout-card-wrap hide">
							@include('pay.checkout.nav-cards.proof-details')
						</div>
					@endif
					<div id="paymentDetails" class="checkout-card-wrap hide" data-ref="paymentDetails">
						@if(isset($data['extraBenefits']) && $data['extraBenefits'])
							<div class="payment-section hide">
								<div>
									@include('pay.checkout.cards.service-selection')
								</div>
							</div>
						@endif
						@include('pay.checkout.nav-cards.payment-details')
					</div>
				</div>
				@if ($agent->isMobile() && !($agent->isTablet()))
				@else
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 no-pad-l">
						<div id="priceDetails" class="checkout-price-card-wrap">
							@include('pay.checkout.nav-cards.price-details')
						</div>
						<div id="couponDetails" class="checkout-coupon-card-wrap mar-t-15">
							@include('pay.checkout.nav-cards.coupon-details')
						</div>
					</div>
				@endif
				<div class="clearfix"></div>
			</div>
		@else
			<div class="checkout-tmo-container mar-t-15">
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2">
					<div class="checkout-tmo-card-wrap text-center">
						<div class="checkout-tmo-text">
							You have successfully made payment for this order
						</div>
						<div class="checkout-tmo-cta-wrap mar-t-15">
							<a href="{{ $data['tmoLink'] }}" class="btn btn-primary btn-checkout-tmo">
								<span class="glyphicon glyphicon-screenshot"></span>
								<span class="mar-l-4">Track My Order</span>
							</a>
							<div class="checkout-tmo-cta-text mar-t-5">
								( View your order and provider details )
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		@endif
	</div>
@endsection

@section("modals")
	@parent
	@include('pay.checkout.modals.e-invite-benefits')
	@include('pay.checkout.modals.e-thank-you-card-benefits')
@endsection

@section('javascript')
	@parent

	<script type="text/javascript">
		$(document).ready(function () {

			$("#inpPartyTime").datetimepicker({
				datepicker: false,
				formatTime: 'g:i A',
				format: 'g:i A',
				step: 30,
				onSelectTime: function (ct, $i) {
					$i.parent().addClass('is-dirty');
				}
			});

			var tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);

			$("#inpPartyDate").datetimepicker({
				timepicker: false,
				minDate: 0,
				startDate: tomorrow,
				defaultDate: tomorrow,
				format: 'Y/m/d',
				scrollInput: false,
				scrollMonth: false,
				scrollTime: false,
				closeOnDateSelect: true,
				disabledDates: window.disableDates(),
				onSelectDate: function (ct, $i) {
					$i.parent().addClass('is-dirty');
				}
			});

			$(".checkout-input-date").datetimepicker({
				timepicker: false,
				defaultDate: tomorrow,
				format: 'd M Y',
				scrollInput: false,
				scrollMonth: false,
				scrollTime: false,
				closeOnDateSelect: true,
				onSelectDate: function (ct, $i) {
					$i.parent().addClass('is-dirty');
				}
			});
		});

	</script>
@endsection