@extends('pay.base')

@section("content")
	@parent
	<div class="checkout-page auto-checkout-page">
		<div class="col-md-12 no-pad__400-600">
			<div class="col-md-12 no-pad__400-600">
				<div class="col-md-6 checkout-form">
					<input id="ticketId" type="hidden" value="{{ request('id') }}">
					<input id="custEmail" type="hidden" value="{{ $data['customer']['email'] }}">
					<input id="advanceAmount" type="hidden" value="{{ $data['booking']['totalAdvanceToPay'] }}">
					<div class="cards">
						@include('pay.checkout.cards.customer-info')
						@include('pay.checkout.cards.auto-bpv-order-info')
					</div>
				</div>
				<div class="col-md-6 item-checkout">
					<div class="payment-section mar-b-20">
						<div class="title">
							<h5 class="co-title">#{{ $data['booking']['bookingId']}}</h5>
						</div>
						<div class="details pad-t-10 mar-b-10">
							@if (isset($data["additional"]["venueLocation"]) || isset($data["additional"]["cityName"]))
								<div class="text-center font-18 pad-t-10">
									<span class="text-bold">Location:</span>
									<span>
										@if(isset($data["additional"]["venueLocation"]))
											{{ $data["additional"]["venueLocation"] }}
											@if(isset($data["additional"]["cityName"]) && $data["additional"]["cityName"])
												<span>, </span>
											@endif
										@endif</span>
									<span>@if(isset($data["additional"]["cityName"])){{ $data["additional"]["cityName"] }}@endif</span>
								</div>
							@endif
							@if(!empty($data['booking']['checkInDate']))
								<div class="checkout-timing panel-item-body text-center">
									<div class="col-md-12">
										<h5 class="no-mar no-pad">
											<b> Check in: </b>
											{{ date('d M Y ',strtotime($data['booking']['checkInDate']))}}
											({{  date('D', strtotime($data['booking']['checkInDate'])) }})
										</h5>
									</div>

									<div class="col-md-6 mar-t-10 checkin-time">
										<h5 class="no-mar no-pad"> Check In Time:</h5>
										<h6 class="no-mar mar-t-10"> {{ $data['booking']['checkInTime'] }}</h6>
									</div>
									<div class="col-md-6 mar-t-10 checkin-time">
										<h5 class="no-mar no-pad"> Check Out Time:</h5>
										<h6 class="no-mar mar-t-10">{{ $data['booking']['checkOutTime'] }} </h6>
									</div>

									<div class="clearfix"></div>
								</div>
							@endif
							@if(!empty($data['booking']['groupCount']))
								<div class="panel-item-body">
									<div class="cal-price">
										<div class="col-sm-8 text-left">
											Price (for {{$data['booking']['groupCount']}} Guests)
										</div>
										<div class="col-sm-4">
											@price($data['booking']['packagePrice'])
										</div>
										<div class="clearfix"></div>
									</div>
									@if(!empty($data['additional']['guestsCount']) && $data['additional']['guestsCount'] > $data['booking']['groupCount'])
										<div class="cal-price">
											<div class="col-sm-8 text-left">
												Extra ({{ $data['additional']['extraGuests'] }} Guests x @price($data['booking']['pricePerExtraGuest']))
											</div>
											<div class="col-sm-4">
												@price($data['booking']['pricePerExtraGuest'] * $data['additional']['extraGuests'])
											</div>
											<div class="clearfix"></div>
										</div>
									@endif
								</div>
							@endif
						</div>
					</div>
					<div class="payment-section">
						<div class="details pad-t-10">
							@include('pay.checkout.price-details',[
								'bookingAmount' => $data['booking']['totalBookingAmount'],
								'advanceToPay' => $data['booking']['totalAdvanceToPay']
							])
							@include('pay.checkout.cards.pgs', ['totalAdvanceToPay' => $data['booking']['totalAdvanceToPay']])
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection