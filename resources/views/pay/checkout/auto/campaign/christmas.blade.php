@extends('pay.base')

@section("content")
	@parent
	<?php $isChristmas = isset($data['campaignChristmas']) ? $data['campaignChristmas'] : false ?>
	<div class="checkout-page auto-checkout-page">
		<div class="col-md-12">
			<div class="col-md-12">
				<div class="col-md-6 checkout-form">
					<input id="ticketId" type="hidden" value="{{ request('id') }}">
					<input id="custEmail" type="hidden" value="{{ $data['customer']['email'] }}">
					<input id="advanceAmount" type="hidden" value="{{ $data['booking']['totalAdvanceToPay'] }}">
					<div class="cards">
						@include('pay.checkout.cards.customer-info')
						@include('pay.checkout.cards.auto-bpv-order-info')
					</div>
				</div>
				<div class="col-md-6 item-checkout">
					<div class="payment-section mar-t-20-400-600">
						<div class="title">
							<h5 class="co-title">#{{ $data['booking']['bookingId']}}</h5>
						</div>
						<div class="details pad-t-10">
							@if(!empty($data['booking']['checkInDate']))
								<div class="checkout-timing panel-item-body text-center">
									<div class="col-md-12">
										<h5 class="no-mar no-pad">
											<b> @if($isChristmas) Delivery Date @else Check in: @endif</b>
											{{ date('d M Y ',strtotime($data['booking']['checkInDate']))}}
											({{  date('D', strtotime($data['booking']['checkInDate'])) }})
										</h5>
									</div>
									@if(!$isChristmas)
										<div class="col-md-6 mar-t-10 checkin-time">
											<h5 class="no-mar no-pad"> Check In Time:</h5>
											<h6 class="no-mar mar-t-10"> {{ $data['booking']['checkInTime'] }}</h6>
										</div>
										<div class="col-md-6 mar-t-10 checkin-time">
											<h5 class="no-mar no-pad"> Check Out Time:</h5>
											<h6 class="no-mar mar-t-10">{{ $data['booking']['checkOutTime'] }} </h6>
										</div>
									@endif
									<div class="clearfix"></div>
								</div>
							@endif
							@if(!empty($data['booking']['groupCount']))
								<div class="panel-item-body">
									<div class="cal-price">
										<div class="col-sm-8 text-left">
											@if($isChristmas)
												{{ $data['booking']['name'] }}
											@else
												Price (for {{$data['booking']['groupCount']}} Guests)
											@endif
										</div>
										<div class="col-sm-4">
											@price($data['booking']['packagePrice'])
										</div>
										<div class="clearfix"></div>
									</div>
									@if(!empty($data['additional']['guestsCount']) && $data['additional']['guestsCount'] > $data['booking']['groupCount'])
										<div class="cal-price">
											<div class="col-sm-8 text-left">
												Extra ({{ $data['additional']['extraGuests'] }} Guests x @price($data['booking']['pricePerExtraGuest']))
											</div>
											<div class="col-sm-4">
												@price($data['booking']['pricePerExtraGuest'] * $data['additional']['extraGuests'])
											</div>
											<div class="clearfix"></div>
										</div>
									@endif
								</div>
							@endif
							<div class="price-total">
								<div>
									<div class="col-xs-12 col-sm-8 text-left">
										<h5 class="font-16">Total Booking Amount:</h5>
									</div>
									<div class="col-sm-4">
										<h5 class="font-16">@price($data['booking']['totalBookingAmount'])</h5>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="seperator">
									<div class="col-xs-12 col-sm-8 text-left font-16">
										<h5 class="no-mar no-pad font-16">
											Total Advance To Pay:
										</h5>
									</div>
									<div class="col-xs-12 col-sm-4">
										<h5 class="no-mar no-pad mar-t-10__400 mar-t-10-400-600 font-16">
											@price($data['booking']['totalAdvanceToPay'])
										</h5>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							@include('pay.checkout.cards.pgs', ['totalAdvanceToPay' => $data['booking']['totalAdvanceToPay']])
						</div>
						<input type="hidden" id="christmasCampaign" value="@if($isChristmas) 1 @else 0 @endif">
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection