@extends('pay.base')

@section('content')
	@parent
	<div class="checkout-page auto-checkout-page">
		<div class="col-md-12">
			<div class="col-md-12">
				@yield('product-delivery-info')
				<div class="col-md-6 checkout-form">
					<input id="ticketId" type="hidden" value="{{ request('id') }}">
					<input id="custEmail" type="hidden" value="{{ $data['customer']['email'] }}">
					<input id="advanceAmount" type="hidden" value="{{ $data['booking']['totalAdvanceToPay'] }}">
					<div class="cards">
						@include('pay.checkout.cards.customer-info')
						@include('pay.checkout.cards.auto-campaign-party-details')
						@include('pay.checkout.cards.upload-images')
						@include('pay.checkout.cards.venue-info', ['isShowVenueType' => false])
					</div>
				</div>
				<div class="col-md-6 item-checkout">
					<div class="payment-section mar-b-20">
						<div class="title">
							<h5 class="co-title">#{{ $data['booking']['bookingId']}}</h5>
						</div>
						<div class="details pad-t-10 mar-b-10">
							@include('pay.checkout.cards.auto-campaign-order-details')
						</div>
					</div>
					<div class="payment-section">
						<div class="details pad-t-10">
							<div class="price-total mar-l-10 mar-r-10 mar-b-20">
								<div>
									<div class="col-sm-8 text-left">
										<h5 class="mar-t-20">Total Price:</h5>
									</div>
									<div class="col-sm-4">
										<h5 class="mar-t-20 checkout-price">@price($data['booking']['totalBookingAmount'])</h5>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							@include('pay.checkout.cards.pgs', ['totalAdvanceToPay' => $data['booking']['totalAdvanceToPay']])
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection

@section('javascript')
	@parent

	<script type="text/javascript">
		$(document).ready(function () {

			var tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);

			$("#inpPartyDate").datetimepicker({
				timepicker: false,
				minDate: 0,
				startDate: tomorrow,
				defaultDate: tomorrow,
				format: 'Y/m/d',
				scrollInput: false,
				scrollMonth: false,
				scrollTime: false,
				closeOnDateSelect: true,
				disabledDates: window.disableDates(),
				onSelectDate: function (ct, $i) {
					$i.parent().addClass('is-dirty');
				}
			});
		});

	</script>
@endsection