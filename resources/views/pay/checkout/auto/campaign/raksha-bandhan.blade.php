@extends('pay.checkout.auto.campaign.base')

@section('product-delivery-info')
	<div class="alert alert-info text-center">
		<i class="glyphicon glyphicon-info-sign"></i>
		<span>Your product will be delivered within 4 - 5 business days.</span>
	</div>
@endsection

@section('address-card-title')
	Delivery Address
@endsection

@section('address-card-delivery-line')
	Delivery
@endsection

@section('map-location-text')
	delivery location
@endsection