@extends('layout.base')

@section('page-title')
	<title>Payment Modes | Evibe.in</title>
@endsection

@section("header")
	@include('pay.header')
@endsection

@section("custom-css")
	@parent
	<style>
		blockquote:after, blockquote:before {
			content: none;
		}
	</style>
@endsection

@section("content")
	<div class="container" style="line-height: 1.3;">
		<h3>Payment Modes</h3>
		<p>
			This page describes the various payment modes supported by Evibe.
		</p>
		<div>
			<h4>Credit Card</h4>
			<p>We accepts all major credit cards that are available in India. The following card schemes of any bank are available to you on the checkout page.</p>
			<ul>
				<li>AMEX</li>
				<li>Cards</li>
				<li>Visa</li>
				<li>MasterCard</li>
				<li>Diners</li>
				<li>Rupay</li>
			</ul>
			<p>We also support international credit cards. Please contact our support team to enable international transactions.</p>
		</div>
		<div>
			<h4>Debit Card</h4>
			<p>We accepts all debit cards that are available in India. The following card schemes of any bank are available to you on the checkout page.</p>
			<ul>
				<li>Visa</li>
				<li>MasterCard</li>
				<li>Rupay</li>
				<li>Maestro</li>
			</ul>
			<p>We also support international debit cards. Please contact our support team to enable international transactions.</p>
		</div>
		<div>
			<h4>Net Banking</h4>
			<p>We supports 50 online banking options available in India. These Net Banking options are listed below:</p>
			@php  $banks=['Kotak Mahindra Bank','Andhra Bank','Airtel Payments Bank','AXIS Bank NetBanking','The Bharat Co-op. Bank Ltd','Bank of India','Bank of Maharashtra','Canara Bank','Central Bank Of India','Citibank Netbanking','Punjab National Bank – Corporate Banking','Corporation Bank','Catholic Syrian Bank','Cosmos Bank','City Union Bank','DCB Bank','Dena Bank','Dhanlaxmi Bank','Deutsche Bank','Federal Bank','HDFC Bank','ICICI Netbanking','IDBI Bank','IDFC Netbanking','Indian Bank','IndusInd Bank','Indian Overseas Bank','Jammu and Kashmir Bank','Janata Sahakari Bank Pune','Karnataka Bank','Karur Vysya – Retail Netbanking','Karur Vysya – Corporate Netbanking','Lakshmi Vilas Bank – Corporate Netbanking','Lakshmi Vilas Bank – Retail Netbanking','Oriental Bank of Commerce','Punjab And Maharashtra Co-operative Bank Limited','Punjab National Bank – Retail Banking','Punjab And Sind Bank','State Bank of India','South Indian Bank','Saraswat Bank','Shamrao Vithal Co-operative Bank Ltd.','Syndicate Bank','The Nainital Bank','Tamilnad Mercantile Bank','Union Bank – Retail Netbanking','Union Bank – Corporate Netbanking','UCO Bank','United Bank Of India','Vijaya Bank','Yes Bank']; @endphp
			<ul>
				@foreach($banks as $bank)
					<li>{{$bank}}</li>
				@endforeach
			</ul>
		</div>
		<div>
			<h4>UPI</h4>
			<p>UPI or Unified Payment Interface is a new mobile first payment mode for making payments to friends or businesses. This is launched by National Payments Corporation of India (NPCI) and regulated by RBI.</p>
			<p>UPI is built over Immediate Payment Service (IMPS) for transferring funds using Virtual Payment Address(VPA). VPA is a unique ID provided by the bank to a customer. A customer requires an MPIN (Mobile banking Personal Identification number) to confirm each payment.</p>
		</div>
		<div>
			<h4>Wallets</h4>
			<p>We now supports 7 different Wallets on all checkout experiences. As a merchant, you can pay using either of the below Wallets.</p>
			@php  $wallets=['FreeCharge','OlaMoney','Oxigen','ItzCash','JioMoney','PayCash','Yes Pay']; @endphp
			<ul>
				@foreach($wallets as $wallet)
					<li>{{$wallet}}</li>
				@endforeach
			</ul>
		</div>
		<div>
			<h4>Tez</h4>
			<p>Tez is a mobile payments service by Google, targeted at users in India. It operates on top of the Unified Payments Interface, developed by the National Payments Corporation of India.</p>
			<p>This option on our checkout products allows consumer to pay using Tez App. Once consumer enters his Tez handle on checkout, he will get a notification on Tez app on his mobile to complete the transaction. He can open that notification, enter his pin and complete the transaction.
			</p>
		</div>
	</div>
@endsection

@section("footer")
	<div class="footer-wrap">
		@include('base.home.footer.footer-non-city')
	</div>
@endsection