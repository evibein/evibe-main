<div class="text-center">
	<img src="{{ $galleryUrl }}/main/pages/refer_earn/gift-box.png" width="200" height="130">
	<div class="text-center">
		<div>
			<h3>Refer Friends. Get Rewards.</h3>
			<h4>Get &#8377;200* OFF when your friends book from evibe.in. Your friends get &#8377;200* OFF on signup.</h4>
			<div class="referral-content-wrapper mar-t-30 pos-rel">
				<div class="loader">
					<div class="line"></div>
					<div class="line"></div>
					<div class="line"></div>
					<div class="line"></div>
				</div>
				<div class="mar-t-10">Loading....</div>
			</div>
			<div class="referral-copy-btns hide">
				<div>
					<div class="referral-copy-btn-text in-blk init-width">
						http://evibe.in
					</div>
					<button class="referral-copy-btn in-blk init-width">
						Copy Link
					</button>
				</div>
				<div class="copy-notification"></div>
				<div class="mar-t-30 text-center">
					<div class="mar-l-5 mar-r-5 mar-b-5 in-blk">
						@if ($agent->isMobile() && !($agent->isTablet()))
							<a id="referralMobWhatsappShare" href="" class="referral-share-btns" data-source-id="{{ config("evibe.referral.source.whatsapp") }}" data-value="whatsapp" target="_blank"><img src="{{ $galleryUrl }}/main/pages/refer_earn/whatsapp.png" width="60" height="60"></a>
							<a id="referralMobMessengerShare" href="" class="mar-l-10 referral-share-btns" data-source-id="{{ config("evibe.referral.source.messenger") }}" data-value="messenger" target="_blank"><img src="{{ $galleryUrl }}/main/pages/refer_earn/messenger.png" width="60" height="60"></a>
						@else
							<a id="referralDesWhatsappShare" href="" class="referral-share-btns" data-source-id="{{ config("evibe.referral.source.whatsapp") }}" data-value="whatsapp" target="_blank"><img src="{{ $galleryUrl }}/main/pages/refer_earn/whatsapp.png" width="60" height="60"></a>
							<a id="referralDesMessengerShare" href="" class="mar-l-10 referral-share-btns" data-source-id="{{ config("evibe.referral.source.messenger") }}" data-value="messenger" target="_blank"><img src="{{ $galleryUrl }}/main/pages/refer_earn/messenger.png" width="60" height="60"></a>
						@endif
					</div>
					<div class="mar-l-5 mar-r-5 mar-b-5 in-blk">
						@if ($agent->isMobile() && !($agent->isTablet()))
							<a id="referralMobSMSShare" href="" class="referral-share-btns" data-source-id="{{ config("evibe.referral.source.sms") }}" data-value="sms" target="_blank"><img src="{{ $galleryUrl }}/main/pages/refer_earn/sms.png" width="60" height="60"></a>
						@else
							<a id="referralDesSMSShare" href="" class="referral-share-btns" data-source-id="{{ config("evibe.referral.source.sms") }}" data-value="sms" target="_blank"><img src="{{ $galleryUrl }}/main/pages/refer_earn/sms.png" width="60" height="60"></a>
						@endif
						<a id="referralEmailShare" href="" class="mar-l-10 referral-share-btns" data-source-id="{{ config("evibe.referral.source.email") }}" data-value="email" target="_blank"><img src="{{ $galleryUrl }}/main/pages/refer_earn/email.png" width="60" height="60"></a>
					</div>
				</div>
				<p class="text-muted">
					<span>Go to this </span>
					<a class="customer-referral-link" target="_blank" href="https://evibe.in">link</a>
					<span> to know about your referrals</span>
				</p>
			</div>
		</div>
	</div>
	<div>
		@include("refer-earn.how-it-works")
	</div>
	<input type="hidden" id="referralGetUniqueLink" value="{{ route("re.get.unique.link", $ticketId) }}">
	<input type="hidden" id="friendReferralSource" value="{{ request("id") }}">
</div>

@section("javascript")
	@parent
	<script type="text/javascript">
        $(".referral-copy-btn").on("click", function () {
            let $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(".referral-copy-btn-text").text()).select();
            document.execCommand("copy");
            $temp.remove();

            let copyNotification = $("div.copy-notification");
            copyNotification.text("Copied!").fadeIn("slow", function () {
                setTimeout(function () {
                    copyNotification.fadeOut("slow", function () {
                        copyNotification.empty();
                    });
                }, 2000);
            });
        });

        $.ajax({
            url: $("#referralGetUniqueLink").val(),
            type: 'post',
            dataType: 'json',
            data: {}
        }).done(function (data) {
            if (data.success) {
                $(".referral-content-wrapper").addClass("hide");
                $(".referral-copy-btns").removeClass("hide");
                $(".referral-copy-btn-text").text(data["res"]["referralUrl"]);
                $("#friendReferralSource").val(data["res"]["userId"]);

                /*encodeURIComponent() for encoding the URL through JS*/
                $("#referralDesWhatsappShare").attr("href", 'https://api.whatsapp.com/send?text=I found Evibe.in to be the best party planners. Make your next event planning effortless and stress-free with Evibe. Register now with Evibe.in to get Rs 200* OFF on your next party booking. Click ' + data["res"]["referralUrl"] + "?src=whatsapp");
                $("#referralDesMessengerShare").attr("href", 'https://www.facebook.com/v2.9/dialog/send?app_id=521736568283788&link=' + data["res"]["referralUrl"] + "&src=messenger&redirect_uri=https://evibe.in");
                $("#referralMobWhatsappShare").attr("href", 'whatsapp://send?text=I found Evibe.in to be the best party planners. Make your next event planning effortless and stress-free with Evibe. Register now with Evibe.in to get Rs 200* OFF on your next party booking. Click ' + data["res"]["referralUrl"] + "?src=whatsapp");
                $("#referralMobMessengerShare").attr("href", 'fb-messenger://share?app_id=521736568283788&link=' + data["res"]["referralUrl"] + "?src=messenger");
                $("#referralMobSMSShare").attr("href", 'sms:?&body=I+found+Evibe.in+to+be+the+best+party+planners.+Make+your+next+event+planning+effortless+and+stress-free+with+Evibe.+Register+now+with+Evibe.in+to+get+Rs+200*+OFF+on+your+next+party+booking.+Click+' + data["res"]["referralUrl"] + "?src=sms");
                $("#referralDesSMSShare").attr("href", 'sms:?&body=I found Evibe.in to be the best party planners. Make your next event planning effortless and stress-free with Evibe. Register now with Evibe.in to get Rs 200* OFF on your next party booking. Click ' + data["res"]["referralUrl"] + "?src=sms");
                $("#referralEmailShare").attr("href", 'mailto:?subject=Get Rs 200* OFF at Evibe.in&body=I found Evibe.in to be the best party planners. Make your next event planning effortless and stress-free with Evibe. Register now with Evibe.in to get Rs 200* OFF on your next party booking. Click ' + (data["res"]["referralUrl"]) + "?src=email");
                $(".customer-referral-link").attr("href", data["res"]["userAutoLoginUrl"]);

                $(".referral-copy-btn").on("click", function () {
                    let $temp = $("<input>");
                    $("body").append($temp);
                    $temp.val($(".referral-copy-btn-text").text()).select();
                    document.execCommand("copy");
                    $temp.remove();

                    let copyNotification = $("div.copy-notification");
                    copyNotification.text("Copied!").fadeIn("slow", function () {
                        setTimeout(function () {
                            copyNotification.fadeOut("slow", function () {
                                copyNotification.empty();
                            });
                        }, 2000);
                    });
                });
            } else {
                showNotyError("Error occurred while fetching your data, Please reload the page & try again")
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            window.notifyTeam({
                "url": $("#referralGetUniqueLink").val(),
                "textStatus": textStatus + ", errorThrown: " + errorThrown,
                "errorThrown": $.parseJSON(jqXHR.responseText)
            });
        });

        $(".referral-share-btns").on("click", function () {
            let userId = $("#friendReferralSource").val();
            if (userId > 0) {
                $.ajax({
                    url: "/re/update/share/" + userId + "/" + $(this).data("value"),
                    type: 'post',
                    dataType: 'json'
                }).done(function (data) {
                    if (!data.success) {
                        showNotyError("Some error occurred while loading your data, Please reload and try again.")
                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    window.notifyTeam({
                        "url": "/re/update/share/" + userId + "/" + $(this).data("value"),
                        "textStatus": textStatus + ", errorThrown: " + errorThrown,
                        "errorThrown": $.parseJSON(jqXHR.responseText)
                    });
                });
            }
        });
	</script>
@endsection