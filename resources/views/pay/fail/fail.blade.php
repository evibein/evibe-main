@extends('layout.base')

@section('page-title')
	<title>Payment Failed | Evibe.in</title>
@endsection

@section('custom-head')
	<link rel="icon" href="{{ $galleryUrl }}/img/logo/favicon_evibe_v2.png" type="image/png" sizes="16x16"/>
	<link rel="apple-touch-icon" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="57x57" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-57x57.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="72x72" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-72x72.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="76x76" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-76x76.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="114x114" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-114x114.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="120x120" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-120x120.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="144x144" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-144x144.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="152x152" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-152x152.png" type="image/png"/>
@endsection

@section('body-attrs')class="pay-body"@endsection

@section('app')
	<div class='pay-fail-page bg-white pad-t-20' style="height: 100%">
		<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
			<div class="co-header">
				<div class="pull-left">
					<img src="{{ $galleryUrl }}/img/logo/logo_evibe.png" alt="Evibe.in logo" title="Evibe.in">
				</div>
				<div class="pull-right">
					<div>
						<i class="glyphicon glyphicon-phone-alt"></i><b> Phone: </b> {{ config('evibe.contact.company.phone')}}
					</div>
					<div class="pad-t-5">
						<i class="glyphicon glyphicon-envelope"></i><b> Email: </b> {{ config('evibe.contact.company.email')}}
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="co-data font-16">
				<h3 class="text-danger no-pad-l">Transaction Failed.</h3>
				<div>Sorry, your transaction has failed. Please try again.</div>
				@if(isset($data["ticketId"]) && isset($data["retryPaymentUrl"]) && !is_null($data["ticketId"]) && !is_null($data["retryPaymentUrl"]) && ($data["ticketId"] != "") && ($data["ticketId"] > 0) && ($data["retryPaymentUrl"] != ""))
					<div class="text-center mar-t-30 mar-b-20">
						<a class="btn btn-lg btn-evibe" href="{{ $data["retryPaymentUrl"] }}">RETRY PAYMENT</a>
					</div>
				@endif
				<div class="pad-t-10">
					<b><u>Note:</u></b> If the amount has been deducted from your bank account, please drop an email to
					<a href="mailto:{{ config('evibe.contact.company.email') }}" target="_top"> {{ config('evibe.contact.company.email') }}</a> with SMS / transaction screenshot. We will get in touch with you.
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection