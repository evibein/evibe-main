@extends('layout.base')

@section('page-title')
	<title>Payment failure | Evibe.in</title>
@endsection

@section('app')
	<div class="container">
		<div class="col-sm-6 col-sm-offset-3">
			<div class="alert alert-danger text-center mar-t-30">
				<h3 class="no-mar">Failed to receive payment.</h3>
				<p>
					An error has occurred while receiving your payment. If the amount has been debited from your account,
					please call {{ config('evibe.contact.company.phone') }} / email <a
							href="mailto:{{ config('evibe.contact.company.email') }}"
							target="_top">{{ config('evibe.contact.company.email') }}</a> at the earliest.
					Our team will assist you.
				</p>
			</div>
		</div>
	</div>
@endsection