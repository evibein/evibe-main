@extends('layout.base')

@section('page-title')
	<title>Payment Success | Evibe.in</title>
@endsection

@section("header")
	@include('pay.header')
@endsection

@section('custom-head')
	<link rel="icon" href="{{ $galleryUrl }}/img/logo/favicon_evibe_v2.png" type="image/png" sizes="16x16"/>
	<link rel="apple-touch-icon" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="57x57" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-57x57.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="72x72" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-72x72.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="76x76" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-76x76.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="114x114" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-114x114.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="120x120" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-120x120.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="144x144" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-144x144.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="152x152" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-152x152.png" type="image/png"/>
@endsection

@section('body-attrs')class="pay-body"@endsection

@section('gtm-data-layer')
	@php
		$itemsData = [];
		if(isset($data['bookings'])) {
			foreach ($data['bookings'] as $booking) {
				array_push($itemsData, [
					'name' => isset($booking['name']) ? $booking['name'] : 'DEC_DEFAULT_NAME',
					'sku' => isset($booking['code']) ? $booking['code'] : 'DEC_DEFAULT_CODE',
					'price' => isset($booking['productPrice']) ? $booking['productPrice'] : 0,
					'category' => isset($booking['itemMapTypeId']) ? $booking['itemMapTypeId'] : 'Default',
					'quantity' => 1,
					'currency' => 'INR'
				]);
			}
		}
	@endphp

	@include('track.gtm-dl-ecom', [
		'trans' => [
			'id' => isset($data['ticketId']) ? $data['ticketId'] : time(),
			'affiliation' => "Evibe",
			'revenue' => isset($data['totalBookingAmount']) ? $data['totalBookingAmount'] * 0.15 : 0,
			'shipping' => 0,
			'tax' => isset($data['totalBookingAmount']) ? $data['totalBookingAmount'] * 0.15 * 0.18 : 0,
			'currency' => 'INR'
		],

		'transId' => isset($data['ticketId']) ? $data['ticketId'] : time(),

		'items' => $itemsData
	])
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/refer-earn.css') }}">
@endsection

@section("content")
	<div class="pay-body">
		<div class="pay-success-page">
			<div class="col-sm-12 col-xs-12 @if(isset($data["ticketId"]) && ($data["ticketId"] > 0)) col-md-8 col-lg-8 @else col-md-12 col-lg-12 @endif auto-booking no-pad__400-600">
				<div class="checkout-wrap no-pad">
					<div class="co-data font-16">
						<h3 class="text-center success">
							<i class="material-icons">check_circle</i> Payment Success
						</h3>
						@if(isset($data["ticketId"]) && ($data["ticketId"] > 0))
							<div class="hidden-lg hidden-md text-center success-re-pro-section pad-t-20">
								<div class="success-re-pro-wrap pad-10 mar-l-20 mar-r-20">
									<a class="scroll-item" data-scroll="reSection">
										<ul class="success-re-pro-items no-mar-b">
											<li class="valign-mid">
												<div class="success-re-pro-image-wrap mar-l-10">
													<img src="{{ $galleryUrl }}/img/icons/gift_white.png" class="success-re-pro-image">
												</div>
											</li>
											<li class="valign-mid">
												<div class="in-blk pad-l-15 pad-r-10 font-20 line-22 text-left">
													<b>Refer Friends. Earn Rewards</b>
												</div>
											</li>
										</ul>
										<div class="text-center mar-t-5 font-14">[Learn More]</div>
									</a>
								</div>
							</div>
						@endif
						@if(isset($data['eInviteFormUrl']) && $data['eInviteFormUrl'])
							<div class="success-e-invite-section pad-t-20">
								<div class="success-e-invite-wrap ps-e-invite-wrap mar-t-5__400-600 text-center mar-l-20 mar-r-20">
									<div class="text-bold text-left">Action Required - E Invitation:</div>
									<div class="text-left mar-t-5">
										Kindly provide the details required to receive customised e-invitation to share with your guests and RSVP
									</div>
									<div class="mar-t-15">
										<a class="invite-form-btn" href="{{ $data['eInviteFormUrl'] }}" target="_blank">
											Show Form
										</a>
									</div>
									<input type="hidden" id="hidTicketId" value="{{ $data['ticketId'] }}">
								</div>
							</div>
						@endif
						@if(isset($data['invitePromotionLink']) && $data['invitePromotionLink'])
							<div class="ps-card-wrap over-hide mar-t-20 mar-l-20 mar-r-20 mar-b-10 text-center">
								<div class="invite-promo-img-wrap">
									<img class="invite-promo-img" src="{{ $galleryUrl }}/main/img/invite-hero.png">
								</div>
								<div class="mar-t-15 pad-l-15 pad-r-15">Download the image and share it with your friends and family on whatsapp / facebook / email / sms or any other medium, inviting them to your party.</div>
								<a href="{{ $data['invitePromotionLink'] }}" target="_blank" class="btn ps-invite-promo-btn">Get My Invite</a>
							</div>
						@endif
						@if(isset($data["ticketId"]) && ($data["ticketId"] > 0))
							@if(isset($data['additional']))
								<div class="payment-success-order-wrap">
									<div class="order-wrap no-mar-t no-mar-b">
										@include("order.masked_order_details")
									</div>
								</div>
							@endif
						@endif
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-4 col-lg-4 col-xs-12 no-pad-l pad-l-15__400-600">
				@if(isset($data["ticketId"]) && ($data["ticketId"] > 0))
					<div class="checkout-wrap refer-earn-wrap no-mar-t__400-600" id="reSection">
						@include("pay.referral-post-purchase-widget", ["ticketId" => $data["ticketId"]])
					</div>
				@endif
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	@if(isset($data['ticketId']))
		<input type="hidden" id="ticketId" value="{{ $data['ticketId'] }}">
	@else
		<input type="hidden" id="ticketId" value="{{ "t-" . time() }}">
	@endif
	<div class="hide">
		@if(isset($data['ticketBookings']))
			@php $data['totalBookingAmount'] = 0; @endphp
			@foreach($data['ticketBookings'] as $ticketBooking)
				@php $mapping = $ticketBooking->mapping; $data['totalBookingAmount'] += $ticketBooking->booking_amount; @endphp
				<span class="eec-ticket-booking" data-product-id="{{ $mapping->map_type_id . "_" . $mapping->map_id }}"
						data-product-category="{{ $mapping->map_type_id }}"
						data-product-amount="{{ $ticketBooking->booking_amount }}">
			</span>
			@endforeach
			<span class="eec-purchase-r">{{ $data['totalBookingAmount'] * 0.15 }}</span>
			<span class="eec-purchase-t">{{ $data['totalBookingAmount'] * 0.15 * 0.18 }}</span>
		@endif
	</div>
@endsection

@section("google-places-api")
@endsection

@section("javascript")

@endsection

@section("footer")
	@include('base.home.why-us')
	<div class="footer-wrap">
		@include('base.home.footer.footer-non-city')
	</div>
@endsection