@extends('pay.base')

@section('page-title')
	<title>Payment Success | Evibe.in</title>
@endsection

@section('gtm-data-layer')
	@include('track.gtm-dl-ecom', [
		'trans' => [
			'id' => isset($data['ticketId']) ? $data['ticketId'] : time(),
			'affiliation' => "AutoBook_Default",
			'revenue' => 0,
			'shipping' => 0,
			'tax' => 0,
			'currency' => 'INR'
		],

		'transId' => isset($data['ticketId']) ? $data['ticketId'] : time(),

		'item' => [
			'name' => 'AUTO_DEFAULT_NAME',
			'sku' => 'AUTO_DEFAULT_CODE',
			'price' => 0,
			'category' => 'AUTO_DEFAULT',
			'quantity' => 1,
			'currency' => 'INR'
		]
	])
@endsection

@section("header")
	@include('pay.header')
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/refer-earn.css') }}">
@endsection

@section('content')
	<div class="pay-body">
		<div class="pay-success-page">
			<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 auto-booking">
				<div class="checkout-wrap no-pad">
					<div class="co-data font-16">
						<h3 class="text-center success">
							<i class="material-icons">check_circle</i><span class="mar-l-2">Payment Successful</span>
						</h3>
						<div class="main-content">
							<div class="text-center">
								Thank you for choosing Evibe.in. Please check your email sent to
								<span class="mar-l-4"><b>{{ $data['customer']['email'] }}</b></span> for complete details.
							</div>
							@if(isset($data['eInviteFormUrl']) && $data['eInviteFormUrl'])
								<div class="checkout-wrap ps-e-invite-wrap mar-t-20__400-600 text-center">
									<div class="text-bold text-left">Action Required - E Invitation:</div>
									<div class="text-left mar-t-5">
										Kindly provide the details required to receive customised e-invitation to share with your guests and RSVP
									</div>
									<div class="mar-t-15">
										<a class="invite-form-btn" href="{{ $data['eInviteFormUrl'] }}" target="_blank">
											Submit Details
										</a>
									</div>
								</div>
								<input type="hidden" id="hidTicketId" value="{{ $data['ticketId'] }}">
							@endif
							@if(isset($data["ticketId"]) && ($data["ticketId"] > 0))
								<div class="checkout-wrap refer-earn-wrap no-mar-t__400-600">
									@include("pay.referral-post-purchase-widget", ["ticketId" => $data["ticketId"]])
								</div>
							@endif
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
@endsection

<!-- There is some issue with pay.js that is triggering success page request. Need to debug. Removing @parent for now -->
@section('javascript')

@endsection