@extends('pay.base')

@section('page-title')
	<title>Payment Success | Evibe.in</title>
@endsection

@section('gtm-data-layer')
	@include('track.gtm-dl-ct', ['transactionId' => $data["ticketId"]])
@endsection

@section("header")
	@include('pay.header')
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/refer-earn.css') }}">
@endsection

@section("content")
	<div class="pay-body">
		<div class="pay-success-page">
			<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 auto-booking">
				<div class="checkout-wrap no-pad">
					<div class="co-data font-16">
						<h3 class="text-center success">
							<i class="material-icons">check_circle</i>
							Payment Success #{{$data['bookingId']}}
						</h3>
						<div class="main-content">
							<div class="text-center">
								@if(array_key_exists('isVenueDeals',$data) && $data['isVenueDeals'])
									We have received your token amount of <b>@price($data['advanceAmount'])</b> for
									{{ $data['bookingInfo'] }}. Please check your email sent to
									<b>{{ $data['email'] }}</b> for complete details.
								@else
									We have received your advance payment of <b>@price($data['advanceAmount'])</b> for
									{{ $data['bookingInfo'] }}. Please check your email sent to
									<b>{{ $data['email'] }}</b> for complete details.
								@endif
							</div>
							<div class="mar-t-20 text-center">
								<h6><u><b>Important: </b></u></h6>
								This is <span class="highlight-text"> NOT BOOKING CONFIRMATION</span>.
								We will check the availability and confirm your booking within next 4 business hours.
							</div>
							<hr class="sep">
							@if(isset($data["ticketId"]) && ($data["ticketId"] > 0))
								@include("pay.referral-post-purchase-widget", ["ticketId" => $data["ticketId"]])
								<hr class="sep">
							@endif
							<h4> FAQs</h4>
							<ul class="faq">
								<li class="ques"><span class="qa-symbol">Q.</span> When will I get booking confirmation?
								</li>
								<li class="ans">
									Within next 4 business hours, you will receive a booking confirmation email & SMS with venue & event coordinator
									details based on availability of your package. In case of non-availability, you will be notified with next
									steps.
								</li>
								<li class="ques">
									<span class="qa-symbol">Q.</span> Whom should I contact after booking confirmation?
								</li>
								<li class="ans">
									Your will be assigned an event coordinator who will make sure all arrangements are done as per
									your order details. Contact information of event coordinator will be shared in booking confirmation email.
								</li>
								<li class="ques"><span class="qa-symbol">Q.</span> What if my package is not available?
								</li>
								<li class="ans">
									Incase of non-availability of your package, we will try to match something similar. If you are
									not satisfied, we will refund 100% of the advance amount immediately thereafter.
								</li>
								<li class="ques">
									<span class="qa-symbol">Q.</span>
									How many days does it take to refund?
								</li>
								<li class="ans">
									We will initiate the refund process as soon as we get a go ahead from you. However, for the money
									to actually credit to your card / account, it depends on your payment choice at the time of making advance
									payment. Rest assured, you should receive the cash within 7 - 10 Business days.
								</li>

							</ul>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
@endsection

@section("javascript")
@endsection