@extends('layout.base')

@section('page-title')
	<title>Payment Success | Evibe.in</title>
@endsection

@section("header")
	@include('pay.header')
@endsection

@section('custom-head')
	<link rel="icon" href="{{ $galleryUrl }}/img/logo/favicon_evibe_v2.png" type="image/png" sizes="16x16"/>
	<link rel="apple-touch-icon" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="57x57" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-57x57.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="72x72" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-72x72.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="76x76" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-76x76.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="114x114" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-114x114.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="120x120" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-120x120.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="144x144" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-144x144.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="152x152" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-152x152.png" type="image/png"/>
@endsection

@section('body-attrs')class="pay-body"@endsection

@section('gtm-data-layer')
	@php
		$itemsData = []; $data['totalBookingAmount'] = 0;
		if(isset($data['ticketBookings']))
		{
			foreach ($data['ticketBookings'] as $booking) {
				$data['totalBookingAmount'] += $booking->booking_amount;
				$mapping = $booking->mapping;
				array_push($itemsData, [
					'name' => isset($mapping['name']) ? $mapping['name'] : 'DEC_DEFAULT_NAME',
					'sku' => isset($mapping['map_type_id']) && isset($mapping['map_id']) ? $mapping->map_type_id . "_" . $mapping->map_id : 'DEC_DEFAULT_CODE',
					'price' => isset($booking['product_price']) ? $booking['product_price'] : 0,
					'category' => isset($mapping['map_type_id']) ? $mapping['map_type_id'] : 'Default',
					'quantity' => 1,
					'currency' => 'INR'
				]);
			}
		}
	@endphp

	@include('track.gtm-dl-ecom', [
		'trans' => [
			'id' => isset($data['ticketId']) ? $data['ticketId'] : time(),
			'affiliation' => "Evibe",
			'revenue' => isset($data['totalBookingAmount']) ? $data['totalBookingAmount'] * 0.15 : 0,
			'shipping' => 0,
			'tax' => isset($data['totalBookingAmount']) ? $data['totalBookingAmount'] * 0.15 * 0.18 : 0,
			'currency' => 'INR'
		],

		'transId' => isset($data['ticketId']) ? $data['ticketId'] : time(),

		'items' => $itemsData
	])
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/refer-earn.css') }}">
@endsection

@section('content')
	<div class="pay-body">
		<div class="pay-success-page mar-b-20">
			<div class="ps-message-container mar-t-20">
				<div class="col-xs-12 col-sm12 col-md-12 col-lg-12">
					<div class="ps-message-card-wrap">
						<i class="glyphicon glyphicon-ok-circle valign-text-top"></i><span class="mar-l-10">Payment Successful</span>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="ps-details-container mar-t-20">
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<div class="ps-card-wrap ps-details-card-wrap text-center">
						<div class="ps-tmo-wrap">
							<div class="ps-details-msg-wrap pad-t-20">
								@if(isset($data['totalAdvanceAmount']))
									<div>Thank you for choosing Evibe. We have received your payment of
										<b>Rs. @amount($data['totalAdvanceAmount'])</b> for party on
										<b>{{ $data['additional']['partyDate'] }}</b></div>
								@endif
								@if(isset($data['ticket']) && $data['ticket']->is_auto_booked)
									<div class="mar-t-20">
										<div class="text-bold text-underline">Important:</div>
										<div class="mar-t-10">
											<span>This is </span>
											<span class="bg-yellow">NOT BOOKING CONFIRMATION</span>. We will check the availability and confirm your booking/s within next 4 business hours.
											@if(isset($data['invitePromotionLink']) && $data['invitePromotionLink'])
												<span> You can track your order details here: </span>
												<span><a href="{{ $data['tmoLink'] }}" target="_blank">Track My Order</a></span>
											@endif
										</div>
									</div>
								@endif
							</div>
							<div class="checkout-tmo-cta-wrap mar-t-20 pad-b-20">
								<a href="{{ $data['tmoLink'] }}" class="btn btn-primary btn-checkout-tmo">
									<span class="glyphicon glyphicon-screenshot"></span>
									<span class="mar-l-4">Track My Order</span>
								</a>
								<div class="checkout-tmo-cta-text mar-t-5">
									( View your order and provider details )
								</div>
							</div>
						</div>
					</div>
					@if(isset($data['invitePromotionLink']) && $data['invitePromotionLink'])
						<div class="ps-card-wrap over-hide mar-t-15 text-center">
							<div class="invite-promo-img-wrap">
								<img class="invite-promo-img" src="{{ $galleryUrl }}/main/img/invite-hero.png">
							</div>
							<div class="mar-t-15 pad-l-15 pad-r-15">Download the image and share it with your friends and family on whatsapp / facebook / email / sms or any other medium, inviting them to your party.</div>
							<a href="{{ $data['invitePromotionLink'] }}" target="_blank" class="btn ps-invite-promo-btn">Get My Invite</a>
						</div>
					@else
						@if(isset($data["ticketId"]) && ($data["ticketId"] > 0))
							<div class="ps-card-wrap ps-re-card-wrap text-center mar-t-20">
								@include("pay.referral-post-purchase-widget", ["ticketId" => $data["ticketId"]])
							</div>
						@endif
					@endif
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 no-pad-l pad-l-15__400-600 mar-t-20__400-600">
					@if(isset($data['cancellationData']) && count($data['cancellationData']))
						<div class="ps-card-wrap ps-refunds-card-wrap">
							<div class="ps-card-header">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left valign-mid">
									<div class="ps-card-header-text">
										Cancellations And Refunds
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="ps-card-body">
								<div class="mar-t-20 pad-b-10 font-14">
									<div class="mar-t-10">In case of cancellation, refundable amount is calculated based on the following policies:</div>
									<table class="table table-condensed table-bordered mar-t-10">
										<tr>
											<th>Cancellation Time Before Party</th>
											<th>Refund Percentage*</th>
										</tr>
										@foreach($data['cancellationData'] as $cd)
											<tr>
												<td>@if($cd->min_day == 0)
														@if($cd->max_day == 0)
															0 - 24 hours before party
														@else
															{{ $cd->max_day }} days or below before party
														@endif
													@elseif($cd->max_day > 30)
														{{ $cd->min_day }} days or above before party
													@else
														{{ $cd->min_day }} days - {{ $cd->max_day }} days before party
													@endif
												</td>
												<td>{{ round($cd->refund_percent) }} %</td>
											</tr>
										@endforeach
									</table>
									<div class="mar-t-5">* Refund Percentage will be calculated on
										<b>Total Advance Amount</b></div>
								</div>
							</div>
						</div>
					@endif
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	@if(isset($data['ticketId']))
		<input type="hidden" id="ticketId" value="{{ $data['ticketId'] }}">
	@else
		<input type="hidden" id="ticketId" value="{{ "t-" . time() }}">
	@endif
	<div class="hide">
		@if(isset($data['ticketBookings']))
			@php $data['totalBookingAmount'] = 0; @endphp
			@foreach($data['ticketBookings'] as $ticketBooking)
				@php $mapping = $ticketBooking->mapping; $data['totalBookingAmount'] += $ticketBooking->booking_amount; @endphp
				<span class="eec-ticket-booking" data-product-id="{{ $mapping->map_type_id . "_" . $mapping->map_id }}"
						data-product-category="{{ $mapping->map_type_id }}"
						data-product-amount="{{ $ticketBooking->booking_amount }}">
			</span>
			@endforeach
			<span class="eec-purchase-r">{{ $data['totalBookingAmount'] * 0.15 }}</span>
			<span class="eec-purchase-t">{{ $data['totalBookingAmount'] * 0.15 * 0.18 }}</span>
		@endif
	</div>
@endsection

@section("javascript")

@endsection

@section("footer")
	@include('base.home.why-us')
	<div class="footer-wrap">
		@include('base.home.footer.footer-non-city')
	</div>
@endsection


