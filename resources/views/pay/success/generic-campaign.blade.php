@extends('pay.base')

@section('page-title')
	<title>Booking Confirmed | Evibe.in</title>
@endsection

@section('gtm-data-layer')
	@include('track.gtm-dl-ecom', [
		'trans' => [
			'id' => isset($data['booking']['bookingId']) ? $data['booking']['bookingId'] : time(),
			'affiliation' => "Evibe",
			'revenue' => isset($data['booking']['bookingAmount']) ? $data['booking']['bookingAmount'] * 0.15 : 0,
			'shipping' => 0,
			'tax' => isset($data['booking']['bookingAmount']) ? $data['booking']['bookingAmount'] * 0.15 * 0.18 : 0,
			'currency' => 'INR'
		],

		'transId' => isset($data['booking']['bookingId']) ? $data['booking']['bookingId'] : time(),

		'item' => [
			'name' => isset($data['booking']['name']) ? $data['booking']['name'] : 'CAMP_DEFAULT_NAME',
			'sku' => isset($data['booking']['code']) ? $data['booking']['code'] : 'CAMP_DEFAULT_CODE',
			'price' => isset($data['booking']['productPrice']) ? $data['booking']['productPrice'] : 0,
			'category' => isset($data['booking']['itemMapTypeId']) ? $data['booking']['itemMapTypeId'] : 'CAMP_DEFAULT',
			'quantity' => 1,
			'currency' => 'INR'
		]
	])
@endsection

@section('content')
	<div class="pay-body">
		<div class="pay-success-page">
			<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 auto-booking">
				<div class="checkout-wrap no-pad">
					<div class="co-data font-16">
						<h3 class="text-center success">
							<i class="material-icons">check_circle</i>
							Booking Successful #{{ $data['booking']['bookingId'] }}
						</h3>
						<div class="main-content">
							<div class="text-center">
								We have received your advance payment of
								<span class="mar-l-4"><b>@price($data['booking']['advanceAmount'])</b></span> for
								<span class="mar-l-4">{{ $data['booking']['bookingInfo'] }}</span>. Thank you for choosing Evibe.in. Please check your email sent to
								<span class="mar-l-4"><b>{{ $data['customer']['email'] }}</b></span> for complete details.
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
@endsection

@section('javascript')
@endsection