<header>
	<div class="header-top pay-header">
		<div class="hide__400 hide-400-600">
			<nav class="navbar navbar-default navbar-home pos-rel navbar-top topnav home" role="navigation">
				<div class="col-xs-10 col-sm-10 col-md-12 col-lg-12 col-xs-offset-1 col-sm-offset-1 col-md-offset-0 col-lg-offset-0 topnav">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle navbar-toggler collapsed mar-t-20" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand topnav" href="/">
							<img src="https://gallery.evibe.in/img/logo/logo_evibe.png" alt="Evibe log">
						</a>
					</div>
					<div class="collapse navbar-collapse navbar-collapse-dropdown navbar-collapse-dropdown-static-page" id="navbarNav">
						<ul class="nav navbar-nav navbar-right top-header-list top-header-list-background" v-cloak>
							<li>
								<a class="no-click">
									<span class="glyphicon glyphicon-envelope"></span>
									<span class="text-lower">{{ config('evibe.contact.customer.group') }}</span>
								</a>
							</li>
							<li>
								<a class="no-click">
									<span class="glyphicon glyphicon-earphone"></span>
									{{ config('evibe.contact.company.plain_phone') }}
								</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
		<div class="hide show-400-600 unhide__400">
			<div class="container no-pad pad-b-15__400-600 pos-rel">
				<div class="col-xs-12 col-sm-12 text-center">
					<div class="checkout-page-logo-wrap in-blk mar-b-10 pad-t-10">
						<a class="" href="/">
							<img class="checkout-page-logo" src="{{ $galleryUrl }}/img/logo/logo_evibe.png" alt="Evibe.in logo">
						</a>
					</div>
				</div>
				@php
					$iPhoneBackButton = 1;
					if(isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], "iPhoneWebView") > 0)) { $iPhoneBackButton = 2; }
				@endphp
				@if($iPhoneBackButton == "2")
					<div class="pos-abs">
						<i class="material-icons pull-left mar-l-10 mar-t-20" id="headerBackButton" style="z-index: 2">&#xe317;</i>
					</div>
				@endif
				<div class="col-xs-12 col-sm-12 pad-t-10">
					<span class="pull-left">
						<span class="glyphicon glyphicon-earphone"></span>{{ config('evibe.contact.company.plain_phone') }}
					</span>
					<span class="pull-right">
						<span class="glyphicon glyphicon-envelope no-mar-t"></span>{{ strtolower(config('evibe.contact.customer.group')) }}
					</span>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</header>