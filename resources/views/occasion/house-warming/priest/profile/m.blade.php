@extends("layout.product.package.profile.m")

@section("header")
	@include('occasion.house-warming.header.m_profile')
@endsection

@section('push-notifications')
	@include('plugins.onesignal', ['pushOccasion' => 'house-warming'])
@endsection

@section("content")
	<div class="col-xs-12 col-sm-12 col-md-12 no-pad bg-white mobile-pro-page">
		@include("occasion.util.profile.m_components.package.gallery")

		@include("occasion.util.profile.m_components.package.title-section")

		@include("occasion.util.profile.m_components.package.product-description-section")

		@include("occasion.util.profile.m_components.loading-similar-products")
		<div id="showSimilarProducts"></div>

		@include("occasion.util.profile.m_components.package.hidden-data-section")

		@include("occasion.util.profile.m_components.package.call-to-action")

		<hr class="mobile-header-phone">
		@include('base.home.why-us-vertical')

	</div>
	<div class="clearfix"></div>
@endsection