@extends("layout.occasion.house-warming.base-house-warming-profile")

@section("content")
	<div class="col-md-12 bg-white">
		<a href="#" class="scrollup">Scroll</a>
		<div class="col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0 package-profile"> <!-- Decors profile begin -->
			<div class="col-md-12 no-pad mar-b-15">
				<div class="breadcrumb-section">
					<ol class="breadcrumb text-center__400"> <!-- breadcrumbs begin -->
						<li><a href="{{ route('city.home', $cityUrl) }}">{{$cityName}}</a></li>
						<li><a href="{{ route('city.occasion.house-warming.home', $cityUrl) }}">House Warming</a></li>
						<li>
							<a href="{{ route('city.occasion.house-warming.priest.list', $cityUrl) }}">Priest Packages</a>
						</li>
						<li class="active">{{ $data['package']->code }}</li>
					</ol>
				</div>
				<div class="header-wrap"> <!-- header begin -->
					<div class="col-md-12 no-pad">
						<div class="col-md-8"> <!-- title begin -->
							<h4 class="no-mar mar-t-10 text-center__400">
								[#{{ $data['package']->code }}] {{ $data['package']->name }}
								<span id="verified" class="text-success"><i class="material-icons">&#xE8E8;</i></span>
								<div class="mdl-tooltip mdl-tooltip--large" for="verified">Verified Provider</div>
							</h4>
						</div>
						<div class="col-md-4 text-center mar-t-10__400 no-pad-r"> <!-- actions begin -->
							@include('app.shortlist_detail')
							<a class="btn btn-default btn-view-more btn-best-deal btn-book-now scroll-item" data-toggle="modal" data-target="#bookNowModal">
								<i class="material-icons valign-mid">&#xE8CC;</i>
								<span class="valign-mid">Book Now</span>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="info-panel">
				<div class="row">
					<div class="col-md-8 mar-t-10">
						<!-- gallery begin -->
						<div class="gallery-wrap hide" role="tabpanel">
						@if ($data['gallery']['showGalleryTabs']) <!-- nav tabs begin -->
							<ul class="nav nav-pills mar-l-20" role="tablist">
								<?php $count = 0; ?>
								@foreach($data['gallery']['images'] as $cat => $catImages)
									<li role="presentation" class="@if(!$count++) active @endif">
										<a href="#catGalId{{ $count }}" data-toggle="pill">
											{{ $cat }}
										</a>
									</li>
								@endforeach
							</ul>
							@endif
							@if (count($data['gallery']['images']))
								<div class="tab-content mar-t-15"> <!-- tab panes begin -->
									<?php $count = 0; ?>
									@foreach($data['gallery']['images'] as $cat => $catImages)
										<div role="tabpanel" class="tab-pane fade @if(!$count++) in active @endif" id="catGalId{{ $count }}">
											@if (count($catImages))
												<div class="fotorama" data-width="100%" data-ratio="900/600" data-maxheight="400" data-nav="thumbs" data-autoplay="true" data-thumbwidth="80" data-thumbmargin="10" data-keyboard="true" data-captions="true" @if($data['gallery']['showThumb']) data-enableifsingleframe="true" @endif>
													@foreach ($catImages as $key => $image)
														<a href="{{ $image['url'] }}" data-caption="{{ $image['title'] }}">
															<img src="{{ $image['thumb'] }}" alt="{{ $image['title'] }}"/>
														</a>
													@endforeach
												</div>
											@else
												<i class="text-danger">No images found in this category.</i>
											@endif
										</div>
									@endforeach
								</div>
							@else
								<div>No images found</div>
							@endif
						</div>
						<!-- gallery end -->
					</div>
					<div class="col-md-4 mar-t-10">
						<!-- highlights begin -->
						<div class="item-profile highlights-card">
							<div class="hs-section">
								<!-- protection begin -->
								<div class="text-center mar-b-5 border-b-1">
									<img src="{{ $galleryUrl }}/img/app/decor_protection_260x66.jpg" alt="Evibe delivery guarantee"/>
								</div>
								<!-- protection end -->
								<!--- Price info --->
								<div class="card-item text-center price-item pad-t-10">
									@if ($data['package']->price_worth && $data['package']->price_worth > $data['package']->price)
										<div class="price-worth in-blk">
											@price ($data['package']->price_worth)
										</div>
									@endif
									<div class="price-val in-blk">
										@price ($data['package']->price)
										@if ($data['package']->price_max && $data['package']->price_max > $data['package']->price)
											-
											@price( $data['package']->price_max )
										@endif
									</div>
								</div>
								<div class="card-item card-supporting-text">
									<div class="card-item">
										<div class="card-item__title"><i
													class="glyphicon glyphicon-map-marker"></i> Location:
										</div>
										<div class="card-item__body">{{ $data['package']->provider->area->name }}</div>
									</div>
									@if ($data['package']->time_setup)
										<div class="card-item">
											<div class="card-item__title"><i
														class="glyphicon glyphicon-time"></i> Setup Time:
											</div>
											<div class="card-item__body">{{ $data['package']->time_setup }} Hrs</div>
										</div>
									@endif
									@if ($data['package']->time_duration > 0)
										<div class="card-item">
											<div class="card-item__title"><i
														class="glyphicon glyphicon-time"></i> Party Duration:
											</div>
											<div class="card-item__body">{{ $data['package']->time_duration }} Hrs</div>
										</div>
									@endif

									@if ($data['package']->kms_free || $data['package']->kms_max)
										<div class="card-item">
											<div class="card-item__title blk valign-top">
												<i class="glyphicon glyphicon-retweet"></i> Transport
												<span>(from {{ $data['package']->provider->area->name}}):</span>
											</div>
											<div class="card-item__body valign-top blk mar-t-3">
												@if (!$data['package']->kms_free && $data['package']->trans_min && $data['package']->kms_max)
													@price($data['package']->trans_min)
													@if ($data['package']->trans_max && $data['package']->trans_max > $data['package']->trans_min)
														- @price($data['package']->trans_max)
													@endif
													<span> up to {{ $data['package']->kms_max }} KMs</span>
												@elseif ($data['package']->kms_free)
													@if (!$data['package']->trans_min)
														Free up to {{ max($data['package']->kms_free, $data['package']->kms_max) }} KMs
													@else
														@if ($data['package']->kms_free == $data['package']->kms_max)
															@price($data['package']->trans_min)
															@if ($data['package']->trans_max && $data['package']->trans_max > $data['package']->trans_min)
																- @price($data['package']->trans_max)
															@endif
															for max. {{ $data['package']->kms_max }} KMs
														@elseif ($data['package']->kms_free < $data['package']->kms_max)
															Free up to {{ $data['package']->kms_free }} KMs
															<span>
															(add @price($data['package']->trans_min)
																@if ($data['package']->trans_max && $data['package']->trans_max > $data['package']->trans_min)
																	- @price($data['package']->trans_max)
																@endif
																for max. {{ $data['package']->kms_max }} KMs)
														</span>
														@else
														@endif
													@endif
												@else
												@endif
											</div>
										</div>
									@endif
								</div>
								<div class="card-item card-actions text-center hide__400">
									<a class="btn btn-default btn-view-more btn-more-details scroll-item" data-scroll="info-container">
										View More Details
									</a>
								</div>
							</div>
							<div class="hs-section social-section text-center">
								@include('app.social-share-plugin')
							</div>
						</div>
						<!-- highlights end -->
						<!-- why Evibe? begin -->
						<div class="why-us-card hide">
							<div class="card-item">
								<div class="card-item__title">
									<h6 class="card-title-text mdl-typography--title title">Why Book With Evibe?</h6>
								</div>
								<div class="card-item__body">
									<ul class="no-mar no-pad ls-none">
										<li>
											<span class="text-e"><i class="glyphicon glyphicon-star"></i></span>
											<span><b>100% Delivery</b> Guarantee</span>
										</li>
										<li>
											<i class="glyphicon glyphicon-star"></i>
											<span>Hosted <b>1000+ Events</b></span>
										</li>
										<li>
											<i class="glyphicon glyphicon-star"></i>
											<span>Best <b>Price &amp; Quality</b></span>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<!-- why Evibe? end -->
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="col-md-12 menu-top-b bg-white sticky hidden-sm hidden-xs">
		<div class="col-lg-10 col-md-12 col-lg-offset-1 col-md-offset-0 tabs-scroll">
			<div class="head-scroll-fixed border-b-1 col-md-12">
				<div class="col-md-8">
					<h5 class="mar-t-10 mar-b-10 text-center__400 ov-text-no-wrap">
						[#{{ $data['package']->code }}] {{ $data['package']->name }}
						<span id="verified" class="text-success"><i
									class="material-icons">&#xE8E8;</i></span>
						<div class="mdl-tooltip mdl-tooltip--large" for="verified">Verified Provider</div>
					</h5>
				</div>
				<div class="col-md-4 mar-t-15">
					<div class="card-item item-profile price-item">
						@if ($data['package']->price_worth && $data['package']->price_worth > $data['package']->price)
							<div class="price-worth in-blk">
								<strike> @price ($data['package']->price_worth) </strike>
							</div>
						@endif
						<div class="price-val in-blk">
							@price ($data['package']->price)
							@if ($data['package']->price_max && $data['package']->price_max > $data['package']->price)
								-
								@price( $data['package']->price_max )
							@endif
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="col-md-12">
				<ul class="menu-scroll col-md-10" id="top-menu">
					@if ($data['package']->info || count($data['package']->services))
						<li><a href="#inclusions" data-scroll="inclusions"> Inclusions </a></li>
					@endif
					@if ($data['package']->prerequisites)
						<li><a href="#prerequisites"> Prerequisites </a></li>
					@endif
					@if ($data['package']->facts)
						<li><a href="#facts"> Facts </a></li>
					@endif
					@if ($data['ratings']['total']['count'])
						<li><a href="#reviews"> Reviews </a></li>
					@endif
					@if ($data['package']->terms)
						<li><a href="#terms"> Terms </a></li>
					@endif
				</ul>
				<div class="col-md-2 interested">
					<a class="btn btn-default btn-view-more btn-best-deal btn-book-now scroll-item" data-toggle="modal" data-target="#bookNowModal">
						<i class="material-icons valign-mid">&#xE8CC;</i>
						<span class="valign-mid">Book Now</span>
					</a>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="col-md-12 bg-light-gray pad-t-20" id="info-container">
		<div class="col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0">
			<div id="decorDetails" class="info-more-panel">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-8">
							@if ($data['package']->info || count($data['package']->services))
								<div id="inclusions" class="main-card info-card"> <!-- inclusions begin -->
									<div class="card-title">
										<h5 class="card-title-text">Inclusions</h5>
									</div>
									<div class="card-info-text no-pad-t">
										<div class="mar-b-10">{!! $data['package']->info !!}</div>
										@if ($data['services']->count())
											<ul class="pkg-services-list">
												@foreach($data['services'] as $service)
													<li>
														<div class="service-title">{{ $service->title }}</div>
														<div class="service-info">{!! $service->info !!}</div>
													</li>
												@endforeach
											</ul>
										@endif
										@if (count($data['tags']))
											<ul class="tags-lists">
												@foreach ($data['tags'] as $key => $tag)
													<li><i class="glyphicon glyphicon-tag"></i> {{ $tag }}
													</li>
												@endforeach
											</ul>
										@endif
									</div>
								</div>
							@endif

							@if ($data['package']->prerequisites)
								<div id="prerequisites" class="main-card more-info-card"> <!-- prerequisites begin -->
									<div class="card-title">
										<h5 class="card-title-text">Prerequisites</h5>
									</div>
									<div class="card-info-text no-pad-t">
										{!! $data['package']->prerequisites !!}
									</div>
								</div>
							@endif

							@if ($data['package']->facts)
								<div id="facts" class="main-card more-info-card"> <!-- facts begin -->
									<div class="card-title">
										<h5 class="card-title-text">Facts</h5>
									</div>
									<div class="card-info-text no-pad-t">
										{!! $data['package']->facts !!}
									</div>
								</div>
							@endif

							@if (!empty($data['itinerary'])) <!-- itinerary -->
							<div id="itinerary" class="main-card more-info-card">
								<div class="card-title">
									<h5 class="card-title-text">Itinerary</h5>
								</div>
								<div class="card-info-text no-pad-t">
									<div>Sample itinerary for dinner party starting at 7.00 PM</div>
									<div class="package-itinerary-wrap">
										<div class="pad-t-10">
											<?php $itineraryCount = 0; ?>
											@foreach ($data['itinerary'] as $key => $value)
												@if($key == 2)
													<div class="itinerary-data itinerary-{{ $key }}">
														<table class="table table-bordered">
															<tbody>
															@foreach ($value['items'] as $itineraryItem)
																<tr class="type-{{ $itineraryItem['type'] }}">
																	<td class="w-30pc">{{ $itineraryItem['time'] }}</td>
																	<td>{{ $itineraryItem['info'] }}</td>
																</tr>
															@endforeach
															</tbody>
														</table>
													</div>
												@endif
												<?php $itineraryCount++; ?>
											@endforeach
										</div>
									</div>
								</div>
							</div>
							@endif
							@include("app.review.partner", [
								"reviews"=> $data["ratings"],
								"showAllReviewsUrl" => route("provider-reviews:all", [
									getSlugFromText($data["package"]->name),
									config("evibe.ticket.type.planner"),
									$data["package"]->provider->id
								])
							])
							@if ($data['package']->terms)
								<div id="terms" class="main-card terms-card">
									<div class="card-title">
										<h5 class="card-title-text">Terms of Booking</h5>
									</div>
									<div class="card-info-text no-pad-t">
										{!! $data['package']->terms !!}
									</div>
								</div>
							@endif
						</div>
						<div class="col-md-4">
							<div class="main-card provider-rating-card"> <!-- provider rating card begin -->
								<div class="card-info-text">
									<div class="font-16">Provider Code:
										<b>{{ $data['package']->provider->code }}</b>
									</div>
									<div class="font-18 pad-t-10">
										<span>
											<input type="number" class="avg-rating hide" value="{{ $data['ratings']['total']['avg'] }}" title="{{ $data['ratings']['total']['avg'] }} average rating for provider of {{ $data['package']->name }}"/>
										</span>
										@if($data['ratings']['total']['count'])
											<span id="reviews" class="card-item__title-hint scroll-item cur-point" data-scroll="reviews"> ({{ $data['ratings']['total']['count'] }} reviews) </span>
										@else
											<span class="card-item__title-hint cur-point"> (0 reviews) </span>
										@endif
									</div>
								</div>
							</div>
							@include("app.forms.profile_enquiry_form", [
								"enquiryUrl" => route("ticket.package", [$data['occasionId'], $data['package']->id])
							])
							@include('app.process.non-venue')
							@include('app.process.report-issue')
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="col-md-12 bg-white similar-results">
		<div class="col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0">
			@if (count($data['similarPackages']))
				<div class="col-md-12">
					<div class="similar-items-wrap similar-decors-wrap">
						<h4 class=" text-center">Similar Packages</h4>
						<div class="similar-decors-list mdl-grid">
							@foreach ($data['similarPackages'] as $package)
								<div class="col-xs-12 col-sm-6 col-md-4 no-pad-l">
									<div class="item-result decor-style">
										<div class="img-wrap">
											<a href="{{ route('city.occasion.house-warming.priest.profile', [$cityUrl, $package->url]) }}">
												<img src="{{ $package->getProfileImg() }}" alt="{{ $package->getProfileImg() }} profile picture">
											</a>
											@include('app.shortlist_results', [
															"mapId" => $package->id,
															"mapTypeId" => $data['mapTypeId'],
															"occasionId" => $data['occasionId'],
															"cityId" => $data['cityId']
														])
										</div>
										<div class="details">
											<div class="title">
												<a href="{{ route('city.occasion.house-warming.priest.profile', [$cityUrl, $package->url]) }}">
													{{ $package->name }}
												</a>
											</div>
											<div class="col-md-6 col-sm-12 col-xs-12 pad-r-0">
												<div class="rating-wrap">
													<input type="number" value="{{ $package->getProviderAvgRating() }}" class="provider-avg-rating hide" title="{{ $package->getProviderAvgRating() }} average rating for package {{ $package->name }}"/>
												</div>
											</div>
											<div class="col-md-6 no-padding hidden-xs hidden-sm">
												<a href="{{ route('city.occasion.house-warming.priest.profile', [$cityUrl, $package->url]) }}"
														class="btn btn-primary btn-view-more btn-venue-details pull-right">View Details</a>
											</div>
											<div class="clearfix"></div>

											<div class="item-info pad-t-5">
												<div class="price-tag text-left mar-t-5 col-md-12 pad-r-0">
													<span class="price">
														@price($package->price)
														@if ($package->price_max && $package->price_max > $package->price)
															-
															@price($package->price_max)
														@endif
														@if ($package->price_worth && $package->price_worth > $package->price)
															<span class="price-worth">
																@price($package->price_worth)
															</span>
														@endif
													</span>
												</div>

												<div class="clearfix"></div>
											</div>
										</div>
									</div>
								</div>
							@endforeach
							<div class="clearfix"></div>
						</div>
					</div>
				</div> <!-- similar decors begin -->
				<div class="clearfix"></div>
			@endif
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
@endsection

@section("modals")
	@parent
	@include('auto-booking/checkout-modals/generic-package', [
		"mapId" => $data['package']->id,
		"mapTypeId" => config('evibe.ticket.type.package'),
		"data" => $data['package']
	])
@endsection