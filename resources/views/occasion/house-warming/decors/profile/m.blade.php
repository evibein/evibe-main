@extends("layout.product.decor.profile.m")

@section("header")
	@include('occasion.house-warming.header.m_profile')
@endsection

@section('push-notifications')
	@include('plugins.onesignal', ['pushOccasion' => 'house-warming'])
@endsection

@section('content')
	<div class="col-xs-12 col-sm-12 no-pad bg-white mobile-pro-page">
		@include("occasion.util.profile.m_components.gallery")

		@include("occasion.util.profile.m_components.decor.title-section")

		@include("occasion.util.profile.m_components.decor.product-description-section")

		<hr class="mobile-profile-hr">
		@include("occasion.util.profile.m_components.decor.custom-design-section")

		@include("occasion.util.profile.m_components.loading-similar-products")
		<div id="showSimilarProducts"></div>

		@include("occasion.util.profile.m_components.call-to-action")

		@include("occasion.util.profile.m_components.decor.hidden-data-section")

		@include("occasion.util.profile.m_components.loading-wrap")

		@include("occasion.util.profile.m_components.decor.sticky-form-section")
		<hr class="mobile-header-phone">
		@include('base.home.why-us-vertical')
	</div>
@endsection

@section("modals")
	@parent
	@include('auto-booking/checkout-modals/decor-add-ons/m', [
		"mapId" => $data['decor']->id,
		"mapTypeId" => config('evibe.ticket.type.decor'),
		"typeId" => config('evibe.ticket.type.decor'),
		"data" => $data,
		"addOns" => isset($data['addOns']) ? $data['addOns'] : []
	])
@endsection

@section("footer")
	@include('base.home.why-us')
	<div class="footer-wrap">
		@include('base.home.footer.footer-common')
	</div>
@endsection
