@extends("layout.occasion.house-warming.base-house-warming-list")

@section("content")
	<div class="items-results decors-results">
		<div class="col-sm-12">
			<div class="top-panel"> <!-- top section begin -->
				<div class="col-sm-12">
					<h4 class="display-1 text-cap" title="{{ $data['seo']['pageHeader'] }}">
						Showing {{ $data['seo']['pageHeader'] }}</h4>
				</div>
				<div class="clearfix"></div>
			</div>

			<div class="bottom-panel"> <!-- bottom section begin -->
				@if($data['decors']->count() > 0 || $data['filters']['clearFilter'])
					<div class="col-xs-12 col-sm-5 col-md-3 col-lg-3 bottom-left-panel"> <!-- left section begin -->
						<div class="text-center hide unhide__400"> <!-- filters begin -->
							<div class="in-blk mar-r-20">
								<button class="btn btn-md btn-default btn-show-filter to-toggle__400" data-target=".item-filters">
									<i class="material-icons valign-mid">&#xE152;</i>
									<span class="valign-mid">Filters</span>
								</button>
							</div>
							<div class="in-blk">
								<button class="btn btn-md btn-default btn-show-filter to-toggle__400" data-target=".item-sort-options">
									<i class="material-icons valign-mid">&#xE164;</i>
									<span class="valign-mid">Sort</span>
								</button>
							</div>
						</div>
						<div class="item-filters decor-filters no-pad-b__400 hide__400">
							<ul class="filters-list">
								<li class="filter search-filter">
									<label>Search</label>
									<div class="text-center">
										<div class="in-blk search-box">
											<input id="searchInput" type="text" placeholder="By name, code" value="{{ $data['filters']['search'] }}" class="form-control search-input" data-old="{{ $data['filters']['search'] }}"/>
										</div>
										<div class="in-blk valign-mid">
											<a id="filterSearchBtn" class="btn-price-go btn btn-xs btn-default">
												<i class="glyphicon glyphicon-chevron-right"></i>
											</a>
										</div>
									</div>
								</li>
								<li class="filter link-filter categories-filter" data-type="style">
									<div class="super-cat">
										<div class="pull-left">
											<a href="{{ route('city.occasion.house-warming.decors.list', $cityUrl) }}">All Decorations</a>
										</div>
										<div class="clearfix"></div>
									</div>
									<ul class="no-mar no-pad ls-none item-sub-cats decor-sub-cats">
										@foreach ($data['filters']['allCategories'] as $catId => $catData)
											@if (array_key_exists($catId, $data['filters']['catCounts']))
												<li class="@if ($data['filters']['active'] == $catData['url']) active @endif">
													<div>
														<div class="pull-left">
															@if($data['filters']['catCounts'][$catId])
																<a class="decor-cat-link" data-url="{{ $catData['url'] }}" href="{{ route('city.occasion.house-warming.decors.category', [$cityUrl, $catData['url']]) }}">{{ $catData['name'] }}</a>
															@else
																<span class="disabled">{{ $catData['name'] }}</span>
															@endif
														</div>
														<div class="pull-right font-12 text-muted count">
														<span @if(!$data['filters']['catCounts'][$catId]) class="disabled" @endif>
															({{ $data['filters']['catCounts'][$catId] }})
														</span>
														</div>
														<div class="clearfix"></div>
													</div>
													@if (count($catData['child']))
														<ul class="no-mar no-pad item-child-cats">
															@foreach($catData['child'] as $childCatId => $childCatData)
																<li class="@if ($data['filters']['active'] == $childCatData['url']) active @endif">
																	<div class="pull-left">
																		@if($data['filters']['catCounts'][$childCatId])
																			<a class="decor-cat-link" data-url="{{ $childCatData['url'] }}" href="{{ route('city.occasion.house-warming.decors.category', [$cityUrl, $childCatData['url']]) }}">{{ $childCatData['name'] }}</a>
																		@else
																			<span class="disabled">{{ $childCatData['name'] }}</span>
																		@endif
																	</div>
																	<div class="pull-right font-12 text-muted count">
																	<span @if(!$data['filters']['catCounts'][$childCatId]) class="disabled" @endif>
																		({{ $data['filters']['catCounts'][$childCatId] }})
																	</span>
																	</div>
																	<div class="clearfix"></div>
																</li>
															@endforeach
														</ul>
													@endif
												</li>
											@endif
										@endforeach
									</ul>
								</li>
								@include("occasion.util.list-filters.price-filter")
								@if ($data['filters']['clearFilter'])
									<li class="text-center reset-filters reset-filters__400">
										<a href="{{ route("city.occasion.house-warming.decors.list", $cityUrl) }}?ref=reset-filters" class="font-16">
											<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
										</a>
									</li>
								@endif
							</ul>
						</div>
						@include('app.bidding.custom_ticket',["url" => route('ticket.custom-ticket.decor', $data['occasionId'])])
						<div class="hide__400">
							@include('app.evibe_guarantee')
						</div>
					</div>
				@endif

				<div class="col-xs-12 @if($data['decors']->count() == 0 && !$data['filters']['clearFilter'] )
						col-lg-12 col-md-12 col-sm-12
					@else
						col-sm-7 col-md-9 col-lg-9 bottom-right-panel
					@endif"> <!-- right section begin -->

					@if($data['decors']->count() > 0 || $data['filters']['clearFilter'])
						<ul class="no-mar ls-none item-sort-options decor-sort-options no-pad hide__400"> <!-- sort options begin -->
							<li class="text-muted font-13">Sort By:</li>
							<li class="@if($data['sort'] == 'popularity') active @endif">
								<a class="sort-option" data-value="popularity">Popularity</a>
							</li>
							<li class="@if($data['sort'] == 'plth' || $data['sort'] == 'phtl') active @endif">
								@if ($data['sort'] == 'phtl')
									<a class="sort-option" data-value="plth">Price <i
												class="glyphicon glyphicon-chevron-down"></i></a>
								@elseif ($data['sort'] == 'plth')
									<a class="sort-option" data-value="phtl">Price <i
												class="glyphicon glyphicon-chevron-up"></i></a>
								@else
									<a class="sort-option" data-value="plth">Price</a>
								@endif
							</li>
							<li class="@if($data['sort'] == 'new-arrivals') active @endif">
								<a class="sort-option" data-value="new-arrivals">New Arrivals</a>
							</li>
						</ul>
						<div class="items-list decors-list"> <!-- results begin -->
							@if ($data['decors']->count())
								@foreach ($data['decors'] as $decor)
									@include('app.custom_design_card',[ "currentPage" => $data['decors']->currentPage(),
																		"loopIteration" => $loop->iteration])
									<div class="col-sm-12 col-md-6 col-lg-4 no-pad-l no-pad__400-600">
										<div class="item-result decor-style">
											<div class="img-wrap">
												<a href="{{ route('city.occasion.house-warming.decors.profile', [$cityUrl, $decor->url]) }}">
													<img src="{{ $decor->getProfileImg() }}" alt="{{ $decor->name }} house warming decoration">
												</a>
												@include('app.shortlist_results', [
															"mapId" => $decor->id,
															"mapTypeId" => $data['mapTypeId'],
															"occasionId" => $data['occasionId'],
															"cityId" => $data['cityId']
														])
											</div>
											<div class="details">
												<div class="title">
													<a href="{{ route('city.occasion.house-warming.decors.profile', [$cityUrl, $decor->url]) }}">
														{{ $decor->name }}
													</a>
												</div>
												<div class="action-btns">
													<div class="col-md-6 no-padding hidden-xs hidden-sm border-right">
														@include('app.shortlist_results_link', [
															"mapId" => $decor->id,
															"mapTypeId" => $data['mapTypeId'],
															"occasionId" => $data['occasionId'],
															"cityId" => $data['cityId']
														])
													</div>
													<a href="{{ route('city.occasion.house-warming.decors.profile', [$cityUrl, $decor->url]) }}?ref=book-now#bookNowModal"
															class="item-book-now"
															target="_blank">Book Now</a>
													<div class="clearfix"></div>
												</div>
												<div class="content-details">
													<div class="col-md-8 col-sm-12 col-xs-12 no-pad">
														<div class="price-tag text-left">
															<span class="price">
																<span class='rupee-font'>&#8377;</span> {{ EvibeUtil::formatPrice($decor->min_price) }}
																@if ($decor->max_price && $decor->max_price > $decor->min_price)
																	-
																	<span class='rupee-font'>&#8377;</span> {{ EvibeUtil::formatPrice($decor->max_price) }}
																	@if ($decor->range_info)
																		<span class="text-muted hide" data-toggle="tooltip" data-placement="left" title="{{ $decor->range_info }}">
																			<i class="glyphicon glyphicon-info-sign"></i>
																		</span>
																	@endif
																@endif
																@if ($decor->worth && $decor->worth > $decor->min_price)
																	<span class="price-worth">
																	<span class='rupee-font'>&#8377;</span> {{ EvibeUtil::formatPrice($decor->worth) }}
																</span>
																@endif
															</span>
														</div>
													</div>
													<div class="col-md-4 hidden-xs hidden-sm no-pad">
														<div class="rating-wrap hide">
															<input type="number" value="{{ $decor->getProviderAvgRating() }}" class="provider-avg-rating hide" title="{{ $decor->getProviderAvgRating() }} average rating for decor style {{ $decor->name }}"/>
														</div>
													</div>
													<div class="clearfix"></div>
													<div class="additional-info"></div>
												</div>
											</div>
										</div>
									</div>
								@endforeach
							@else
								<div class="no-results-wrap text-center">
									<h4 class="no-results-title no-mar">
										<i class="glyphicon glyphicon-warning-sign"></i>
										Oops, no Decoration Styles were found matching your filters.
									</h4>

									<div class="pad-t-20">
										<a href="{{ route("city.occasion.house-warming.decors.list", $cityUrl) }}?ref=no-results" class="btn btn-danger btn-lg">
											<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
										</a>
									</div>
								</div>
							@endif
							<div class="clearfix"></div>
						</div>
						<div class="pages"> <!-- pagination begin -->
							<div>{{ $data['decors']->appends($data['filters']['queryParams'])->links() }}</div>
						</div>
					@else
						<div class="no-results-wrap text-center">
							<h4 class="text-col-gr no-mar">We are adding amazing decorations soon, keep checking this page.</h4>
						</div>
					@endif
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection