@extends("layout.product.decor.list.d")

@section("header")
	{{-- Showing mobile view for desktop  --}}
	@if($agent->isTablet())	
			@include('occasion.house-warming.header.m_list')
	@else

			@include('occasion.house-warming.header.header')
	@endif
@endsection

@section('push-notifications')
	@include('plugins.onesignal', ['pushOccasion' => 'house-warming'])
@endsection

@section("content")
	<div class="bg-white desktop-list-page">
		<div class="col-md-12 col-lg-12">
			@include("occasion.util.list.d_components.decor.top-bar-section")
		</div>

		@include("occasion.util.list.d_components.decor.hidden-data-section")

		<div class="col-md-12 col-lg-12 pad-r-20">
			@include("occasion.util.list.d_components.decor.content-section")
		</div>
		<div class="clearfix"></div>
	</div>
@endsection

@section("footer")
	@parent
	@include('base.home.why-us-list')
	<div class="footer-wrap">
		@if (isset($data['views']['footer']) && view()->exists($data['views']['footer']))
			@include($data['views']['footer'])
		@endif
	</div>
@endsection