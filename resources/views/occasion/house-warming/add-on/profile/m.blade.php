@extends("layout.product.ent.profile.m")

@section("header")
	@include('occasion.house-warming.header.m_profile')
@endsection

@section('push-notifications')
	@include('plugins.onesignal', ['pushOccasion' => 'house-warming'])
@endsection

@section('content')
	<div class="col-xs-12 col-sm-12 no-pad bg-white mobile-pro-page">
		@include("occasion.util.profile.m_components.gallery")

		@include("occasion.util.profile.m_components.ent.title-section")

		@include("occasion.util.profile.m_components.ent.product-description-section")

		@include("occasion.util.profile.m_components.loading-similar-products")
		<div id="showSimilarProducts"></div>

		@include("occasion.util.profile.m_components.ent.call-to-action")

		@include("occasion.util.profile.m_components.ent.hidden-data-section")
		<hr class="mobile-header-phone">
		@include('base.home.why-us-vertical')
	</div>
@endsection

@section("modals")
	@parent
	@include('auto-booking/checkout-modals/ent', [
		"mapId" => $data['id'],
		"mapTypeId" => $data['mapTypeId'],
		"occasionId" => $data['occasionId'],
		"data" => $data
	])
@endsection
