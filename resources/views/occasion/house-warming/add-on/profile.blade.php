@extends("layout.occasion.house-warming.base-house-warming-profile")

@section("content")
	<div class="col-md-12 bg-white">
		<a href="#" class="scrollup">Scroll</a>
		<div class="col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0"> <!-- Decors profile begin -->
			<div class="col-md-12 no-pad mar-b-15">
				<div class="breadcrumb-section">
					<ol class="breadcrumb text-center__400"> <!-- breadcrumbs begin -->
						<li><a href="{{ route('city.home', $cityUrl) }}">{{$cityName}}</a></li>
						<li><a href="{{ route('city.occasion.house-warming.home', $cityUrl) }}">House Warming</a></li>
						<li><a href="{{ route('city.occasion.house-warming.ent.list', $cityUrl) }}">Add-ons</a></li>
						<li class="active">{{ $data['code'] }}</li>
					</ol>
				</div>
				<div class="header-wrap"> <!-- header begin -->
					<div class="col-md-12 no-pad">
						<div class="col-md-8"> <!-- title begin -->
							<h4 class="no-mar mar-t-10 text-center__400">
								[{{ $data['code'] }}] {{ $data['name'] }}
							</h4>
						</div>
						<div class="col-md-4 text-center mar-t-10__400 no-pad-r"> <!-- actions begin -->
							@include('app.shortlist_detail')
							<a class="btn btn-default btn-view-more btn-best-deal btn-book-now scroll-item" data-toggle="modal" data-target="#bookNowModal">
								<i class="material-icons valign-mid">&#xE8CC;</i>
								<span class="valign-mid">Book Now</span>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="info-panel">
				<div class="row">
					<div class="col-md-8 mar-t-10">
						<!-- gallery begin -->
						@if (count($data['gallery']))
							<div class="loading-img-wrap">
								<img src="/images/loading.gif" alt="loading">
								<div class="pad-t-10">loading gallery...</div>
							</div>
							<div class="gallery-wrap  hide">
								<div class="fotorama" data-width="100%" data-ratio="900/600" data-maxheight="400" data-nav="thumbs" data-autoplay="true" data-thumbwidth="80" data-thumbmargin="10" data-keyboard="true">
									@foreach ($data['gallery'] as $gallery)
										<a href="{{ $gallery['url'] }}"><img src="{{ $gallery['thumb'] }}" alt="{{ $gallery['title'] }}"/></a>
									@endforeach
								</div>
							</div>
						@else
							<div>No images found</div>
					@endif
					<!-- gallery end -->
					</div>
					<div class="col-md-4 mar-t-10">
						<!-- highlights begin -->
						<div class="item-profile highlights-card">
							<div class="hs-section">
								<!-- protection begin -->
								<div class="text-center mar-b-5 border-b-1">
									<img src="{{ $galleryUrl }}/img/app/decor_protection_260x66.jpg" alt="Evibe delivery guarantee"/>
								</div>
								<!-- protection end -->
								<div class="card-item text-center price-item pad-t-10">
									@if ($data['worth'])
										<div class="price-worth blk">@price($data['worth'])</div>
									@endif
									<div class="price-val in-blk mar-t-15 mar-b-20">
										@price($data['minPrice'])
										@if ($data['minPrice'] && $data['maxPrice'] > $data['minPrice'])
											- @price($data['maxPrice'])
										@endif
									</div>
									@if ($data['rangeInfo'])
										<div class="price-range-info"><i
													class="glyphicon glyphicon-info-sign range-info"
													data-toggle="tooltip"
													data-placement="auto" title=""></i>
											{{ $data['rangeInfo'] }}</div>
									@endif
								</div>
								<div class="card-item card-actions text-center hide__400">
									<a class="btn btn-default btn-view-more btn-more-details scroll-item" data-scroll="info-container">
										View More Details
									</a>
								</div>
							</div>
							<div class="hs-section social-section text-center">
								@include('app.social-share-plugin')
							</div>
						</div>
						<!-- highlights end -->
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="col-md-12 menu-top-b bg-white sticky hidden-sm hidden-xs">
		<div class="col-lg-10 col-md-12 col-lg-offset-1 col-md-offset-0 tabs-scroll">
			<div class="head-scroll-fixed border-b-1 col-md-12">
				<div class="col-md-8">
					<h5 class="mar-t-10 mar-b-10 text-center__400 ov-text-no-wrap">
						[{{ $data['code'] }}] {{ $data['name'] }}
					</h5>
				</div>
				<div class="col-md-4 mar-t-15">
					<div class="card-item item-profile price-item">
						@if ($data['worth'])
							<div class="price-worth in-blk">
								<strike>@price($data['worth'])</strike>
							</div>
						@endif
						<div class="price-val in-blk">
							@price($data['minPrice'])
							@if ($data['minPrice'] && $data['maxPrice'] > $data['minPrice'])
								- @price($data['maxPrice'])
							@endif
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="col-md-12">
				<ul class="menu-scroll col-md-9" id="top-menu">
					@if ($data['info'])
						<li><a href="#inclusions" data-scroll="inclusions"> Inclusions </a></li>
					@endif
					@if ($data['preq'])
						<li><a href="#prerequisites"> Prerequisites </a></li>
					@endif
					@if ($data['facts'])
						<li><a href="#facts"> Facts </a></li>
					@endif
					@if ($data['terms'])
						<li><a href="#terms"> Terms </a></li>
					@endif
				</ul>
				<div class="col-md-3 interested">
					<a href="#book" class="btn btn-default btn-view-more btn-best-deal scroll-item mar-t-5" data-scroll="provider-rating">
						<i class="material-icons valign-mid">&#xE8DC;</i>
						<span class="valign-mid">I'm Interested</span>
					</a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="col-md-12 bg-light-gray pad-t-20" id="info-container">
		<div class="col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0">
			<div id="mainDetails" class="info-more-panel">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-8">
							@if ($data['info'])
								<div id="inclusions" class="main-card info-card"> <!-- inclusions begin -->
									<div class="card-title">
										<h5 class="card-title-text">Inclusions</h5>
									</div>
									<div class="card-info-text no-pad-t">
										{!! $data['info'] !!}
									</div>
								</div>
							@endif

							@if ($data['preq'])
								<div id="prerequisites" class="main-card more-info-card"> <!-- prerequisites begin -->
									<div class="card-title">
										<h5 class="card-title-text">Prerequisites</h5>
									</div>
									<div class="card-info-text no-pad-t">
										{!! $data['preq'] !!}
									</div>
								</div>
							@endif

							@if ($data['facts'])
								<div id="facts" class="main-card more-info-card"> <!-- facts begin -->
									<div class="card-title">
										<h5 class="card-title-text">Facts</h5>
									</div>
									<div class="card-info-text no-pad-t">
										{!! $data['facts'] !!}
									</div>
								</div>
							@endif

							@if ($data['terms'])
								<div id="terms" class="main-card terms-card">
									<div class="card-title">
										<h5 class="card-title-text">Terms of Booking</h5>
									</div>
									<div class="card-info-text no-pad-t">{!! $data['terms'] !!}</div>
								</div>
							@endif
						</div>
						<div class="col-md-4">
							@include("app.forms.profile_enquiry_form", [
								"enquiryUrl" => route("ticket.service", [$data['occasionId'], $data['id']])
							])
							@include('app.process.non-venue')
							@include('app.process.report-issue')
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>

		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
@endsection

@section("modals")
	@parent
	@include('auto-booking/checkout-modals/ent', [
		"mapId" => $data['id'],
		"mapTypeId" => config('evibe.ticket.type.entertainment'),
		"occasionId" => $data['occasionId'],
		"data" => $data
	])
@endsection