@extends("layout.occasion.house-warming.base-house-warming-list")

@section("content")
	<div class="e-options-wrap">
		<div class="col-sm-12">
			<div class="top-panel">
				<div class="col-sm-10 col-sm-offset-1">
					<h4 class="header-title display-1 text-cap no-mar__400 no_pad__400 pad-tb-20__400 text-center__400" title="{{ $data['seo']['pageHeader'] }}">Showing {{ $data['seo']['pageHeader'] }}</h4>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="bottom-panel col-lg-10 col-lg-offset-1 col-sm-12 col-sm-offset-0">
				@if ($data['options'])
					@foreach ($data['options'] as $option)
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="item-result ent-pkg">
								<div class="img-wrap">
									<a href="{{ $option['fullPath'] }}">
										<img src="{{ $option['profileImg'] }}" alt="{{ $option['name'] }}">
									</a>
									@include('app.shortlist_results', [
															"mapId" => $option['id'],
															"mapTypeId" => $data['mapTypeId'],
															"occasionId" => $data['occasionId'],
															"cityId" => $data['cityId']
														])
								</div>
								<div class="details">
									<div class="title">
										<a href="{{ $option['fullPath'] }}">{{ $option['name'] }}</a>
									</div>
									<div class="action-btns">
										<div class="col-md-6 no-padding hidden-xs hidden-sm border-right">
											@include('app.shortlist_results_link', [
												"mapId" => $option['id'],
												"mapTypeId" => $data['mapTypeId'],
												"occasionId" => $data['occasionId'],
												"cityId" => $data['cityId']
											])
										</div>
										<div class="col-md-6 no-padding hidden-xs hidden-sm">
											<a href="{{ $option['fullPath'] }}?ref=book-now#bookNowModal" data-url="{{ $option['fullPath'] }}?ref=book-now#bookNowModal" class="item-book-now" target="_blank">Book Now</a>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="content-details">
										<div>
											<div class="col-md-8 col-sm-12 col-xs-12 no-pad">
												<div class="price-tag text-left">
												<span class="price">
													@price($option['minPrice'])
												</span>
													@if (array_key_exists('maxPrice', $option) && $option['maxPrice'] > $option['minPrice'])
														- <span class="price">
														@price($option['maxPrice'])
													</span>
													@endif
													@if (array_key_exists('rangeInfo', $option) && $option['rangeInfo'])
														*
													@endif
												</div>
											</div>
											<div class="col-md-4 hidden-xs hidden-sm no-pad">
												<div class="rating-wrap">
													<a href="{{ $option['fullPath'] }}?ref=book-now#bookNowModal" data-url="{{ $option['fullPath'] }}?ref=book-now#bookNowModal" class="item-book-now" target="_blank">Book Now</a>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="additional-info"></div>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				@else
					<div class="no-results-wrap text-center">
						<h4 class="text-col-gr no-mar">We are adding amazing add-on options soon, keep checking this page.</h4>
					</div>
				@endif
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection