@extends("layout.product.package.profile.d")

@section("header")
	@if($agent->isTablet())
			@include('occasion.house-warming.header.m_profile')
	@else
		@include('occasion.house-warming.header.header')
	@endif
@endsection

@section('push-notifications')
	@include('plugins.onesignal', ['pushOccasion' => 'house-warming'])
@endsection

@section('content')
	<div class="col-md-12 col-lg-12 bg-white desktop-pro-page">
		<div class="des-top-section">
			<div class="col-md-12 col-lg-12 no-pad">
				@include("occasion.util.profile.d_components.package.breadcrumb-section")
			</div>
			<div class="col-md-12 col-lg-12 no-pad">
				<div class="col-md-4 col-lg-4 no-pad-l">
					@include("occasion.util.profile.d_components.package.gallery")
					<div class="text-center">
						@include('app.social-share-plugin')
					</div>
				</div>
				<div class="col-md-4 col-lg-4 no-pad-l no-pad-r">
					@include("occasion.util.profile.d_components.package.title-section")
				</div>
				<div class="col-md-4 col-lg-4 no-pad-r">
					@include("occasion.util.profile.d_components.package.price-section")
					<div class="mar-t-20">
						@include("occasion.util.profile.d_components.enquiry-card-section")
					</div>
					<div class="pad-t-5">
						@include('app.modals.auto-popup',
						[
						"cityId" => $data['shortlistData']['cityId'],
						"occasionId" => $data['shortlistData']['occasionId'],
						"mapTypeId" => $data['shortlistData']['mapTypeId'],
						"mapId" => $data['shortlistData']['mapId'],
						"optionName" => $data['package']['name'],
						"countries" => (isset($data['countries']) && count($data['countries'])) ? $data['countries'] : []
						])
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<hr class="desktop-profile-hr">
		<div class="des-down-section">
			<div class="col-md-12 col-lg-12 no-pad">
				<div class="col-md-8 col-lg-8">
					@include("occasion.util.profile.d_components.package.product-detailed-info")
				</div>
				<div class="col-md-4 col-lg-4 no-pad">
					<div class="right-panel-cards">
						<div class="right-panel-wrap pad-l-15 pad-r-15">
							<div class="hide">
								@include("occasion.util.profile.d_components.package.partner-card-section")
								<div class="mar-t-5"></div>
								<hr class="desktop-profile-sub-hr">
							</div>
							@include("occasion.util.profile.d_components.how-it-works-section")
							<hr class="desktop-profile-sub-hr">
							@include('base.home.why-us-vertical')
							<hr class="desktop-profile-sub-hr">
							@include('app.process.report-issue')
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="col-md-12 col-lg-12 no-pad">
				@include("occasion.util.profile.d_components.loading-similar-products")
				<div id="showSimilarProducts"></div>
			</div>
			<div class="col-md-12 col-lg-12 no-pad">
				@include("occasion.util.profile.d_components.loading-most-viewed-products")
				<div id="showMostViewedProducts"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		@include("occasion.util.profile.d_components.package.hidden-data-section")
	</div>
@endsection

@section("footer")
	@parent
	<div class="footer-wrap">
		@if (isset($data['views']['footer']) && view()->exists($data['views']['footer']))
			@include($data['views']['footer'])
		@endif
	</div>
@endsection