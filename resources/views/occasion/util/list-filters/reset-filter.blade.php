@if ($data['filters']['clearFilter'] || $data['filters']['active'])
	<li class="text-center reset-filters reset-filters__400 pad-t-20">
		<a href="{{ route($routeName, $cityUrl) }}?ref=reset-filters" class="font-16">
			<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
		</a>
	</li>
@endif