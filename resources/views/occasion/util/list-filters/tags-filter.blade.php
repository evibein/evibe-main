@if(isset($data['filters']['allCategories']) && count($data['filters']['allCategories']))
	<div class="loader-filter-list mar-t-20 text-center">
		<img src="{{ $galleryUrl }}/img/gifs/filter-loading.gif" alt="">
	</div>
	<div class="loader-filter-list-wrap hide">
		@foreach ($data['filters']['relevantParentTags'] as $name => $tagId)
			@php
				$tagFilters = $data['filters']['allCategories']->where("parent_id", $tagId);
				$minifiedName = strtolower(str_replace(' ', '-', $name));
				$selectedTagFilters = $minifiedName == "category" ? explode("__", $data["filters"]["active"]) : explode("__", request($minifiedName));
			@endphp
			@if($tagFilters->count() > 0)
				<li class="filter link-filter categories-filter no-pad-t mar-t-10" data-type="category">
					<label class="pull-left">{{ $name }}</label>
					<br>
					<ul class="no-mar no-pad ls-none item-sub-cats packages-sub-cats">
						@foreach ($tagFilters as $catData)
							<input data-url="{{ $catData['url'] }}" type="checkbox" class="{{ $minifiedName . "-input" }} list-page-filters-input" @if(in_array($catData['url'], $selectedTagFilters)) checked @endif id="{{ $catData['url'] }}" value="{{ $catData['url'] }}">
							<label for="{{ $catData['url'] }}" class="{{ $minifiedName . "-label" }}" data-url="{{ $catData['url'] }}">{{ $catData['identifier'] ?: $catData['name'] }}</label>
						@endforeach
					</ul>
				</li>
			@endif
		@endforeach
	</div>
	<!-- Below Conditions meant for naming ceremony and baby  shower -->
	@if($data['occasionId']==config("evibe.occasion.naming_ceremony.id"))
		<input type="hidden" id="categoryFilterUrl" value="{{ route('city.occasion.ncdecors.category', [$cityUrl, "replaceFilters"]) }}">
	@elseif($data['occasionId']==config("evibe.occasion.baby-shower.id"))
		<input type="hidden" id="categoryFilterUrl" value="{{ route('city.occasion.bsdecors.category', [$cityUrl, "replaceFilters"]) }}">
	@elseif($data['occasionId']==config("evibe.occasion.store-opening.id"))
		<input type="hidden" id="categoryFilterUrl" value="{{ route('city.occasion.sodecors.category', [$cityUrl, "replaceFilters"]) }}">
	@else
		<input type="hidden" id="categoryFilterUrl" value="{{ route('city.occasion.'.config("evibe.type-event-route-name.".$data['occasionId']).'.decors.category', [$cityUrl, "replaceFilters"]) }}">
	@endif
@endif