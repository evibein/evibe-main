<ul class="no-pad no-mar ls-none item-sort-options hide__400">
	<li class="text-muted font-13">Sort By:</li>
	<li class="@if($data['sort'] == 'popularity') active @endif">
		<a class="sort-option" data-value="popularity">Popularity</a>
	</li>
	<li class="@if($data['sort'] == 'plth' || $data['sort'] == 'phtl') active @endif">
		@if ($data['sort'] == 'phtl')
			<a class="sort-option" data-value="plth">Price
				<i class="glyphicon glyphicon-chevron-down"></i></a>
		@elseif ($data['sort'] == 'plth')
			<a class="sort-option" data-value="phtl">Price
				<i class="glyphicon glyphicon-chevron-up"></i></a>
		@else
			<a class="sort-option" data-value="plth">Price</a>
		@endif
	</li>
	<li class="@if($data['sort'] == 'new-arrivals') active @endif">
		<a class="sort-option" data-value="new-arrivals">New Arrivals</a>
	</li>
</ul>