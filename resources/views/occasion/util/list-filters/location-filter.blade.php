@if(isset($data['filters']['allLocations']['all']) && count($data['filters']['allLocations']['all']))
	@php
		if(request("price_min") > 0)
			$urlParams =  "?price_min=" . request("price_min") . (request("price_max") > 0 ? "&price_max=" . request("price_max") : "");
		else
			$urlParams = request("price_max") > 0 ? "?price_max=" . request("price_max") : "";
	@endphp
	<li class="filter location-filter" data-type="location">
		<label>Locations</label>
		<div class="location-filter-options-wrap price-categories-wrap">
			<?php $locationCount = 0 ?>
			<div class="content-overflow">
				@if (request()->is('*/candle-light-dinner*'))
					@foreach($data['filters']['allLocations']['all'] as $location)
						<a class="a-grey @if(isset($data['filters']['location']) && ($data['filters']['location'] == $location['url'])) text-e @endif @if(isset($location['count']) && ($location['count'] == 0)) disabled @endif" href="{{ route("city.cld.list", [$cityUrl, $location['url']]) . $urlParams }}">{{ $location['name'] }}</a>
						<div class="clearfix"></div>
						<?php $locationCount++ ?>
					@endforeach
				@else
					@foreach($data['filters']['allLocations']['all'] as $location)
						<a class="location-filter-option price-category-option @if(isset($data['filters']['location']) && ($data['filters']['location'] == $location['url'])) text-e @endif @if(isset($location['count']) && ($location['count'] == 0)) disabled @endif" href="#" data-location-url="{{ $location['url'] }}">{{ $location['name'] }}</a>
						<div class="clearfix"></div>
						<?php $locationCount++ ?>
					@endforeach
				@endif
			</div>
			<div class="pad-t-10">
				<div class="text-left">
					<a class="btn-overflow" href="#">+ Show More Locations</a>
				</div>
			</div>
		</div>
	</li>
@endif