@if(isset($data['filters']['allCategories']) && count($data['filters']['allCategories']))
	<li class="filter link-filter categories-filter" data-type="category">
		<div class="super-cat">
			<div class="pull-left">
				<a href="{{ route('city.occasion.'.config("evibe.type-event-route-name.".$data['occasionId']).'.'.config("evibe.type-product-route-name.".$data['mapTypeId']).'.list', $cityUrl) }}?#allCategories">
					All Categories</a>
			</div>
			<div class="clearfix"></div>
		</div>
		<ul class="no-mar no-pad ls-none item-sub-cats packages-sub-cats">
			@if(isset($data['mapTypeId']) && ($data['mapTypeId'] == config('evibe.ticket.type.decor')))
				@foreach ($data['filters']['allCategories'] as $catData)
					<li class="@if($data['filters']['active'] == $catData['url']) active @endif">
						<div class="pull-left">
							<a class="des-list-cat-link" data-url="{{ $catData['url'] }}" href="{{ route('city.occasion.'.config("evibe.type-event-route-name.".$data['occasionId']).'.decors.category', [$cityUrl, $catData['url']]) }}">{{ $catData['identifier'] ?: $catData['name'] }}</a>
						</div>
						<div class="clearfix"></div>
					</li>
				@endforeach
			@elseif(isset($data['mapTypeId']) && ($data['mapTypeId'] == config('evibe.ticket.type.entertainment')))
				@foreach ($data['filters']['allCategories'] as $catData)
					<li class="@if($data['filters']['active'] == $catData['url']) active @endif">
						<div class="pull-left">
							<a class="des-list-cat-link" data-url="{{ $catData['url'] }}" href="{{ route('city.occasion.' . config("evibe.type-event-route-name." . $data['occasionId']) . '.ent.list.category', [$cityUrl, $catData['url']]) }}?ref=category-filters">{{ $catData['name'] }}</a>
						</div>
						<div class="clearfix"></div>
					</li>
				@endforeach
			@else
				@foreach ($data['filters']['allCategories'] as $catId => $catData)
					@if (array_key_exists($catId, $data['filters']['catCounts']))
						<li class="@if (!count($catData['child']) && $data['filters']['active'] == $catData['url']) active @endif">
							<div>
								<div class="pull-left">
									@if(!count($catData['child']))
										<a class="decor-cat-link" data-url="{{ $catData['url'] }}" href="?category={{ $catData['url'] }}">{{ $catData['name'] }}</a>
									@elseif(count($catData['child']))
										<span>{{ $catData['name'] }}</span>
									@else
										<span class="disabled">{{ $catData['name'] }}</span>
									@endif
								</div>
								@if (!count($catData['child']))
								@endif
								<div class="clearfix"></div>
							</div>
							@if (count($catData['child']))
								<ul class="no-mar no-pad item-child-cats">
									@foreach($catData['child'] as $childCatId => $childCatData)
										<li class="@if ($data['filters']['active'] == $childCatData['url']) active @endif">
											<div class="pull-left">
												<a class="decor-cat-link" data-url="{{ $childCatData['url'] }}" href="?category={{ $childCatData['url'] }}">{{ $childCatData['name'] }}</a>
											</div>
											<div class="clearfix"></div>
										</li>
									@endforeach
								</ul>
							@endif
						</li>
					@endif
				@endforeach
			@endif
		</ul>
	</li>
@endif