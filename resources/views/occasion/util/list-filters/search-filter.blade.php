<li class="filter search-filter">
	<label>Search</label>
	<div class="text-center">
		<div class="in-blk search-box">
			<input id="searchInput" type="text" placeholder="By name, option code" value="{{ $data['filters']['search'] }}" class="form-control search-input" data-old="{{ $data['filters']['search'] }}"/>
		</div>
		<div class="in-blk valign-mid mar-l-4">
			<a id="filterSearchBtn" class="btn-price-go btn btn-xs btn-default">
				<i class="glyphicon glyphicon-chevron-right"></i>
			</a>
		</div>
	</div>
</li>