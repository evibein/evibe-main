<li class="filter search-filter">
	<label>Search</label>
	<div class="text-center">
		<div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 no-pad">
			<div class="search-box des-search-box">
				<input id="searchInput" type="text" placeholder="By name, option code" value="{{ $data['filters']['search'] }}" class="form-control search-input" data-old="{{ $data['filters']['search'] }}"/>
			</div>
		</div>
		<div class="col-md-2 col-lg-2 col-sm-2 col-xs-2 no-pad-r">
			<div class="des-search-btn">
				<a id="filterSearchBtn" class="btn-price-go btn btn-xs btn-default">
					<i class="glyphicon glyphicon-chevron-right"></i>
				</a>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</li>