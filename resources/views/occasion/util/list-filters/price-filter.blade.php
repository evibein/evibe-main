@if(isset($data['filters']['priceMin']) && isset($data['filters']['priceMax']))
	<li class="filter price-filter">
		<label>Price</label>
		<div class="price-filter-input-wrap mar-b-5">
			<div class="in-blk">
				<label class="price-filter-input-label">
					<span class="rupee-font mar-r-5">&#8377;</span>
					<input type="text" class="price-filter-input-min-price price-filter-input-field" @if(isset($data['filters']['priceFilterApplied']) && $data['filters']['priceFilterApplied']) value="{{ $data['filters']['priceMin'] }}" @endif>
				</label>
			</div>
			<div class="in-blk">
				<span class="mar-l-5 mar-r-5 price-filter-input-text">to</span>
			</div>
			<div class="in-blk">
				<label class="price-filter-input-label">
					<span class="rupee-font mar-r-5">&#8377;</span>
					<input type="text" class="price-filter-input-max-price price-filter-input-field" @if(isset($data['filters']['priceFilterApplied']) && $data['filters']['priceFilterApplied']) value="{{ $data['filters']['priceMax'] }}" @endif>
				</label>
			</div>
			<div class="in-blk">
				<div class="btn btn-link price-filter-submit-btn">GO</div>
			</div>
		</div>
		<div class="price-categories-wrap @if(isset($data['filters']['hidePriceCategories']) && $data['filters']['hidePriceCategories']) hide @endif">
			@if(isset($data['filters']['priceCategories']) && count($data['filters']['priceCategories']) > 0)
				@foreach($data['filters']['priceCategories'] as $priceCategory)
					<a class="price-category-option in-blk @if(($data['filters']['priceMin'] == $priceCategory['minPrice']) && ($data['filters']['priceMax'] == $priceCategory['maxPrice'])) text-e @endif @if($priceCategory['count'] == 0) disabled @endif" data-price-cat-min="{{ $priceCategory['minPrice'] }}" data-price-cat-max="{{ $priceCategory['maxPrice'] }}" href="#">
						@if($priceCategory['bound'] == config('evibe.price-category.bounds.low'))
							Below @price($priceCategory['maxPrice'])
						@elseif($priceCategory['bound'] == config('evibe.price-category.bounds.high'))
							Above @price($priceCategory['minPrice'])
						@else
							@price($priceCategory['minPrice']) - @price($priceCategory['maxPrice'])
						@endif
					</a>
					<div class="clearfix"></div>
				@endforeach
			@endif
		</div>
	</li>
@endif