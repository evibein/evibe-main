@php $sessionCityUrl = is_null(getCityUrl()) ? 'bangalore' : getCityUrl(); $currentUrlPath = request()->path(); $currentTime = time(); @endphp

<div class="mobile-header" style="position: fixed; width: 100%; z-index: 101; background-color: #FFFFFF;">
<!-- Header height is fixed. Be careful while working with this -->
	@if($currentTime >= config('evibe.decommission-time'))
	<div class="" >
		<div class="header-campaign-wrap mob-promotional-header-wrap font-13 " style="background-color:red">
			{{-- <a href="{{ route('virtual-birthday-party.home') }}" class="a-no-decoration-black" target="_blank" style="color:#fff;line-height:1.3;"> Make your #Lockdown occasions lifetime memorable with Evibe Virtual Parties.
			</a> --}}
			{{-- <a class="pull-right no-mar-t mob-promotional-header-close-btn" href="#"><span class="glyphicon glyphicon-remove-circle"></span></a> --}}
			<span style="color:#fff;line-height:1.3;">Due to unforeseen reasons, we're not accepting new orders. Sorry for the inconvenience!</span>
		</div>
	</div>
	@elseif ($currentTime <= config('evibe.campaign-limit.valentines-day') && ($sessionCityUrl === "bangalore" || $sessionCityUrl === "hyderabad") && !(strpos($currentUrlPath, 'valentines-day-2022-romantic-candlelight-dinner') !== false))
	<div class="" >
		<div class="header-campaign-wrap mob-promotional-header-wrap font-13 " style="background-color:red">
			<a href="{{ route('campaign.landingpage', config('evibe.valentines-day-cld-url.' . $sessionCityUrl)) }}" class="a-no-decoration-black" target="_blank" style="color:#fff;line-height:1.3;">
				Valentines Day Romantic Candlelight Dinners
			</a>
			<a class="pull-right no-mar-t mob-promotional-header-close-btn" href="#"><span class="glyphicon glyphicon-remove-circle"></span></a>
		</div>
	</div>
	@endif
	<div class="mobile-header-wrap" @if((getCityUrl() == "hyderabad") || (getCityUrl() == "bangalore")) style='box-shadow: 0 2px 2px 0 #b9b9b9; border-top:1px solid #d7cdcd5e' @endif>
		@php
			$iPhoneBackButton = 1;
			if(isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], "iPhoneWebView") > 0) && !(isset($data["isHomePage"]) && $data["isHomePage"])) { $iPhoneBackButton = 2; }
		@endphp
		@if($iPhoneBackButton == "2")
			<div class="mar-l-10 mobile-header-menu in-blk">
				<i class="material-icons mobile-menu-icon mar-r-15" id="headerBackButton">&#xe317;</i>
				<i class="material-icons mobile-menu-icon" id="mobileHeaderMenuBtn">&#xE5D2;</i>
			</div>
		@else
			<div id="mobileHeaderMenuBtn" class="mar-l-15 mobile-header-menu in-blk">
				<i class="material-icons mobile-menu-icon">&#xE5D2;</i>
			</div>
		@endif
		<div class="mobile-header-logo-wrap in-blk @if($iPhoneBackButton == "2") mar-l-15 @endif">
			<a href="/" class="enable-link">
				<img class="mobile-header-logo" src="{{ $galleryUrl }}/img/logo/logo_evibe.png" alt="Evibe.in logo">
			</a>
		</div>
		@if(!(isset($data["passiveCity"]) && isset($data["passiveCity"])))
			<div class="mobile-header-cta in-blk pull-right mar-t-5">
				<div class="mar-r-20">
					<a href="{{ route('partybag.list') }}">
						<img height="22" class="mar-r-2" src="{{ $galleryUrl }}/main/img/icons/basket-icon.png" alt="partybag icon">
						<span class="partybag-badge" v-cloak>@{{ pbCount }}</span>
					</a>
				</div>
			</div>
			<div class="mar-r-20 mar-t-3 in-blk pull-right hide" id="mobile-header-search-icon"> <!-- see: mobile search icon -->
				<img height="22" class="mar-t-3" src="{{ $galleryUrl }}/main/img/icons/search-icon-header1.png" alt="search icon">
			</div>
			<div class="mar-r-20 mar-t-3 in-blk pull-right" id="mobileHeaderCallIcon">
				<a href="tel:{{ config('evibe.contact.company.mobile') }}" class="ga-mobile-header-customer-support">
					<img height="22" class="mar-t-3" src="{{ $galleryUrl }}/main/img/icons/support_solid_black.png" alt="search icon">
				</a>
			</div>
			@include("app.mobile.search-bar")
		@endif
	</div>
</div>
<div class="mobile-header-additional-space" style="height: 55px;"></div>
<div>
	<div class="scroll-hor-div font-13" style="height: 40px; width:auto;overflow-x:scroll;white-space: nowrap;scrollbar-width:none;background-color: white;">

		

		<a href="{{ route('virtual-birthday-party.home') }}?ref=horizontal-navbar" class="a-no-decoration-black in-blk text-center pad-l-15 pad-r-15 ">
			<div class="home-icon-title mar-t-7 header-scroll-icons-wrap" data-id='direct-link' style="color:#ed3e72;border:0.5px solid #ed3e7299" >Virtual Celebrations</div>
		</a>
	
		<a class="a-no-decoration-black in-blk text-center pad-r-15">
			<div class="home-icon-title mar-t-7 header-scroll-icons-wrap" data-id='for-kids'>For Kids</div>
		</a>
		<a class="a-no-decoration-black  text-center pad-r-15  in-blk">
			<div class="home-icon-title mar-t-7 header-scroll-icons-wrap" data-id='for-couple'>For Couple</div>
		</a>
		<a class="a-no-decoration-black in-blk text-center pad-r-15">
			<div class="home-icon-title mar-t-7 header-scroll-icons-wrap" data-id='for-occasion'>For Occasion</div>
		</a>
		<a href="{{ route('store-redirect', ['redirect' => config('evibe.store_host').'?utm_source=CategoryHeader&utm_medium=Website&utm_campaign=StorePromotion']) }}" class="a-no-decoration-black in-blk text-center pad-r-15 hide">
			<span class=" home-icon-title mar-t-7 header-scroll-icons-wrap" data-id='direct-link'>Party Props</span>
		</a>
		<a href="{{route('city.occasion.corporate.home',getCityUrl())}}?ref=horizontal-navbar" class="a-no-decoration-black in-blk text-center pad-r-15">
			<span class=" home-icon-title mar-t-7 header-scroll-icons-wrap" data-id='direct-link'>For Companies</span>
		</a>
		<a class="a-no-decoration-black in-blk text-center pad-r-15">
			<div class="home-icon-title mar-t-7 header-scroll-icons-wrap" data-id='invites'>E-Invitations</div>
		</a>
	</div>
	<hr style="margin:0; border-top: 0px; background-color: #f3f3f3;">
</div>