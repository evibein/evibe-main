@php $sessionCityUrl = getCityUrl(); $sessionCityUrl = is_null($sessionCityUrl) ? 'bangalore' : $sessionCityUrl; @endphp
@if($sessionCityUrl == "bangalore" || $sessionCityUrl == "hyderabad")
	@php $urlSlug = "ganesh-chaturthi-traditional-decorations-in-".$sessionCityUrl; @endphp
@endif
<div id="listSliderSection" class="list-slider-section">
	<div class="list-slider-wrap">
		<div class="col-xs-12 col-sm-12 no-pad">
			<div class="list-slide-menu-wrap">
				<div id="listSlideMenu" class="list-slide-menu">
					<div id="listSlideContent" class="list-slide-content hide">
						<div class="list-slide-category-wrap pad-l-20 pad-r-20 pad-t-10 pad-b-15" style="background-image: linear-gradient(141deg, #9fb8ad 0%, #1fc8db 51%, #2cb5e8 75%);">
							<div class="list-slide-category-title">
								<span class="hide" id="username">{{ getAuthUserName() }}</span>
								<div v-if="user == ''" class="mar-t-5">
									<a href="#" class="enable-link" v-on:click.stop.prevent="userLogin" data-letters="e" style="text-decoration: none; color: #FFFFFF;">
										Login / Signup
									</a>
								</div>
								<div v-else class="mar-b-10 mar-t-5">
									<div class="pull-left text-bold" data-letters="{{ substr(getAuthUserName(), 0, 1) }}" style="color: #FFFFFF;">
										{{ getAuthUserName() }}
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<hr class="no-mar-t">
						<div class="list-slide-category-wrap pad-l-20">
							<div class="list-slide-category-title mar-b-10">
								<i class="glyphicon glyphicon-home mar-r-5"></i><a href="{{ route("city.home", getCityUrl()) }}" style="text-decoration: none; color: #5A5A5A;"> Home</a>
							</div>
							<div class="list-slide-category-title mar-b-10">
								@if(isset($data["passiveCity"]) && $data["passiveCity"])
									<i class="glyphicon glyphicon-map-marker pad-r-5"></i> {{ $data["passiveCityUrl"] }}
								@else
									<i class="glyphicon glyphicon-map-marker pad-r-5"></i> {{ ucfirst(getCityName()) }}
								@endif
								<a href="javascript:void(0)" class="select-city enable-link" style="color: #448aff; font-size: 11px; padding-left: 5px;">
									(change)
								</a>
							</div>
							<div class="list-slide-category-title mar-b-10 hide" id="listSlideEnquire">
								<a style="text-decoration: none; color: #5A5A5A;" href="#">
									<i class="glyphicon glyphicon-envelope pad-r-5"></i> Enquire Now
								</a>
							</div>
							{{-- <div class="list-slide-category-title mar-b-10 text-ip">
								<img class=" mar-r-5" src="https://gallery.evibe.in/main/img/icons/video-call.png" style="height:15px"></i>
								<a href="" style="text-decoration: none; color: #5A5A5A;"> Introducing Virtual Celebrations
									<span class="nav-highlight-text-mob valign-mid">New</span> 
								</a>
							</div> --}}
						</div>
						<hr>
						<div class="list-sslide-category-wrap pad-l-20 pad-r-20">
							<div class="list-slide-category-title mar-b-10">
								<a class="dropdown-btn dropdown-head" id='for-kids' style="text-decoration: none; color: #5A5A5A;  display: block;">
									<img src="https://gallery.evibe.in/main/img/icons/decoration2_g.png" width="16px" height="16px" class="mar-r-8" alt="Birthday party decorations">
									For Kids
										<span class="pull-right"><i class="glyphicon glyphicon-menu-right  dropdown-icon mar-t-3"></i></span>
								</a>
								<div class="dropdown-container">
									<a href="{{ route('virtual-birthday-party.home') }}?ref=nav-bar" class="dropdown-head">Virtual Celebrations</a>
									<a class='dropdown-btn dropdown-head'>Decorations
										<span class="pull-right"><i class="glyphicon glyphicon-menu-right mar-t-3 dropdown-icon"></i></span>
									</a>
									<div class="dropdown-container nested-dropdown-container">
										<a href="{{ route('city.occasion.birthdays.decors.list',['cityUrl'=>$sessionCityUrl]) }}?ref=navbar" class="fw-300">All Decorations</a>
										<a href="{{ route('city.occasion.birthdays.decors.category',['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'simple-balloon-decorations']) }}ref=navbar" class="fw-300">Simple Balloon Decorations</a>

										<a href="{{ route('city.occasion.birthdays.decors.category',['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'customised-balloon-decorations']) }}?ref=navbar" class="fw-300 no-pad-r-imp">Customised Balloon Decorations</a>

										<a href="{{ route('city.occasion.birthdays.decors.category',['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'basic-theme-decorations']) }}?ref=navbar" class="fw-300 no-pad-l no-pad-r-imp">Basic Theme Decorations</a>

										<a href="{{ route('city.occasion.birthdays.decors.category',['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'medium-theme-decorations']) }}?ref=navbar" class="fw-300 no-pad-l no-pad-r-imp">Medium Theme Decorations</a>

										<a href="{{ route('city.occasion.birthdays.decors.category',['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'high-end-theme-decorations']) }}?ref=navbar" class="fw-300 no-pad-l no-pad-r-imp">High-End Theme Decorations</a>


										<a href="{{ route('city.occasion.birthdays.decors.category',['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'flower-decorations']) }}?ref=navbar" class="fw-300 no-pad-l no-pad-r-imp">Flower Decorations</a>

										<a href="{{ route('city.occasion.birthdays.decors.category',['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'drapes-decorations']) }}?ref=navbar" class="fw-300 no-pad-l no-pad-r-imp">Drapes Decorations</a>
									</div>
									<a class='dropdown-btn dropdown-head'>Cakes
										<span class="pull-right"><i class="glyphicon glyphicon-menu-right mar-t-3 dropdown-icon"></i></span>
									</a>
									<div class="dropdown-container nested-dropdown-container">
										<a href="{{ route('city.occasion.birthdays.cakes.list',$sessionCityUrl) }}?ref=navbar" class="fw-300">All</a>
										<a href="{{ route('city.occasion.birthdays.cakes.list', $sessionCityUrl) }}?category=girl-themes&ref=navbar" class="fw-300">Girl Themes</a>
										<a href="{{ route('city.occasion.birthdays.cakes.list', $sessionCityUrl) }}?category=boy-themes&ref=navbar" class="fw-300 no-pad-r-imp">Boy Themes</a>
										<a href="{{ route('city.occasion.birthdays.cakes.list', $sessionCityUrl) }}?category=neutral-themes&ref=navbar" class="fw-300 no-pad-l no-pad-r-imp">Neutral Themes</a>
									</div>
									<a class='dropdown-btn dropdown-head'>Food
										<span class="pull-right"><i class="glyphicon glyphicon-menu-right mar-t-3 dropdown-icon"></i></span>
									</a>
									<div class="dropdown-container nested-dropdown-container">
										<a href="{{ route('city.occasion.birthdays.food.list', $sessionCityUrl) }}?category=food-combo&ref=navbar" class="fw-300">All</a>
										<a href="{{ route('city.occasion.birthdays.food.list', $sessionCityUrl) }}?category=food-combo&ref=navbar" class="fw-300">Food Combo</a>

										<a href="{{ route('city.occasion.birthdays.food.list', $sessionCityUrl) }}?category=snack-combo&ref=navbar" class="fw-300 no-pad-r-imp">Snack Combo</a>

									</div>
									<a class='dropdown-btn dropdown-head'>Entertainment
										<span class="pull-right"><i class="glyphicon glyphicon-menu-right mar-t-3 dropdown-icon"></i></span>
									</a>
									<div class="dropdown-container nested-dropdown-container">
										<a href="{{ route('city.occasion.birthdays.ent.list', $sessionCityUrl) }}?ref=navbar" class="fw-300">All</a>
										<a href="{{ route('city.occasion.birthdays.ent.list.category', ['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'eateries']) }}?ref=navbar" class="fw-300">Eateries</a>
										<a href="{{ route('city.occasion.birthdays.ent.list.category', ['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'game-stalls']) }}?ref=navbar" class="fw-300">Game Stalls</a>
										<a href="{{ route('city.occasion.birthdays.ent.list.category', ['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'entertainers']) }}?ref=navbar" class="fw-300">Entertainers</a>
										<a href="{{ route('city.occasion.birthdays.ent.list.category', ['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'artists']) }}?ref=navbar" class="fw-300">Artists</a>
										<a href="{{ route('city.occasion.birthdays.ent.list.category', ['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'rentals']) }}?ref=navbar" class="fw-300">Rentals</a>
										<a href="{{ route('city.occasion.birthdays.ent.list.category', ['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'photo-video']) }}?ref=navbar" class="fw-300">Photo / Video</a>
										<a href="{{ route('city.occasion.birthdays.ent.list.category', ['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'sound-lights']) }}?ref=navbar" class="fw-300">Sound / Lights</a>
										<a href="{{ route('city.occasion.birthdays.ent.list.category', ['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'new-trends']) }}?ref=navbar" class="fw-300">New Trends</a>
										<a href="{{ route('city.occasion.birthdays.ent.list.category', ['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'mascots']) }}?ref=navbar" class="fw-300">Mascots</a>
									</div>
									<a class='dropdown-btn dropdown-head'>Packages
										<span class="pull-right"><i class="glyphicon glyphicon-menu-right mar-t-3 dropdown-icon"></i></span>
									</a>
									<div class="dropdown-container nested-dropdown-container">
										<a href="{{ route('city.occasion.birthdays.packages.list',$sessionCityUrl) }}?ref=navbar" class="fw-300">All</a>
										<a href="{{ route('city.occasion.birthdays.packages.list', $sessionCityUrl) }}?category=basic-combos&ref=navbar" class="fw-300">Basic</a>
										<a href="{{ route('city.occasion.birthdays.packages.list', $sessionCityUrl) }}?category=moderate-combos&ref=navbar" class="fw-300 no-pad-r-imp">Moderate</a>
										<a href="{{ route('city.occasion.birthdays.packages.list', $sessionCityUrl) }}?category=premium-combos&ref=navbar" class="fw-300 no-pad-r-imp">Premium</a>

									</div>
									<a href="{{ route('city.occasion.birthdays.party-halls.list', getCityUrl()) }}?ref=navbar" class="dropdown-head">Party Halls</a>
									<a href="{{ route('store-redirect', ['redirect' => config('evibe.store_host').'/collections/kids-birthday-party-supplies?utm_source=Sidebar_Kids&utm_medium=Website&utm_campaign=StorePromotion']) }}" class="dropdown-head hide">Party Props</a>
								</div>
							</div>
							<div class="list-slide-category-title mar-b-10">
								<a class="dropdown-btn dropdown-head" id='for-couple' style="text-decoration: none; color: #5A5A5A; display: block;">
									<i class="glyphicon glyphicon-heart mar-r-8"></i>
									For Couple
									<span class="pull-right"><i class="glyphicon glyphicon-menu-right  dropdown-icon mar-t-3"></i></span>
								</a>
								<div class="dropdown-container">
									<a href="{{ route('city.cld.list', getCityUrl()) }}?ref=navbar" class="dropdown-head">
										Candle light dinner</a>
									<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=cake-cutting-places&ref=navbar" class="dropdown-head">
										Cake Cutting Places</a>
									<a class='dropdown-btn dropdown-head'>Surprises
										<span class="pull-right"><i class="glyphicon glyphicon-menu-right mar-t-3 dropdown-icon"></i></span>
									</a>
									<div class="dropdown-container nested-dropdown-container">
										<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/h/couple-surprises?ref=navbar" class="fw-300">All surprises</a>
										<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=romantic-stay&ref=navbar" class="fw-300">Romantic Stay</a>
										<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=home-decorations&ref=navbar" class="fw-300">Home decorations</a>
										<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=regular-surprise&ref=navbar" class="fw-300">Regular surprises</a>
										<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=customised&ref=navbar" class="fw-300">Customised surprises</a>
										<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&ref=navbar" class="fw-300">Unique </a>
										<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=romantic&ref=navbar" class="fw-300">Romantic</a>
										<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=adventure&ref=navbar" class="fw-300">Adventure</a>
									</div>
									<a href="{{ route('store-redirect', ['redirect' => config('evibe.store_host').'/collections/youth-birthday-party-supplies?utm_source=Sidebar_Couple&utm_medium=Website&utm_campaign=StorePromotion" class="dropdown-head']) }}" class="hide">Party Props</a>
								</div>
							</div>
							<div class="list-slide-category-title mar-b-10">
								<a class="dropdown-btn dropdown-head" id='for-occasion' style="text-decoration: none; color: #5A5A5A;  display: block;">
									<img src="https://gallery.evibe.in/main/img/icons/lounge_g.png" width="16px" height="16px" class="mar-r-8" alt="House warming">
									For Occasion
									<span class="pull-right"><i class="glyphicon glyphicon-menu-right dropdown-icon mar-t-3"></i></span>
								</a>
								<div class="dropdown-container">
									@php $city = getCityUrl()  @endphp
									@if(getCityUrl() == "hyderabad" || getCityUrl() == "bangalore")
										@php $campaignUrl = config('evibe.host')."/u/wedding-decors-in-".$city  @endphp
									{{-- 	<a href="{{ $campaignUrl }}?ref=nav-bar" class="dropdown-head">Home Weddings</a> --}}
										<a class="dropdown-btn dropdown-head" href="#">Home Weddings
											<span class="pull-right"><i class="glyphicon glyphicon-menu-right dropdown-icon mar-t-3"></i></span>
										</a>
											<div class="dropdown-container nested-dropdown-container">
												<a href="{{ route('city.occasion.pre-post.decors.list', $sessionCityUrl) }}?ref=navbar" class="fw-300">Decorations</a>
												<a href="{{ route('city.occasion.pre-post.cakes.list', $sessionCityUrl) }}?ref=navbar" class="fw-300">Cakes</a>
												<a href="{{route('city.occasion.pre-post.ent.list', $sessionCityUrl) }}?ref=navbar" class="fw-300">Entertainment</a>
											</div>
									@endif
									<a class="dropdown-btn dropdown-head" href="#">House Warming
										<span class="pull-right"><i class="glyphicon glyphicon-menu-right dropdown-icon mar-t-3"></i></span>
									</a>
									<div class="dropdown-container nested-dropdown-container">
										<a href="{{ route('city.occasion.house-warming.decors.list',$sessionCityUrl) }}?ref=navbar" class="fw-300">All decorations</a>
										<a href="{{ route('city.occasion.house-warming.food.list', $sessionCityUrl) }}?ref=navbar" class="fw-300">Food</a>
										<a href="{{ route('city.occasion.house-warming.ent.list',$sessionCityUrl) }}?ref=navbar" class="fw-300">Addons</a>
										<a href="{{ route('city.occasion.house-warming.priest.list', $sessionCityUrl) }}?utm_source=top-header&utm_campaign=category-lists&utm_medium=website&utm_content=house-warming-Priest" class="fw-300">Priest</a>
										<a href="{{ route('city.occasion.house-warming.tent.list', $sessionCityUrl) }}?utm_source=top-header&utm_campaign=category-lists&utm_medium=website&utm_content=house-warming-Tent" class="fw-300">Tent</a>
										<a href="{{ route('city.occasion.house-warming.cakes.list', $sessionCityUrl) }}?utm_source=top-header&utm_campaign=category-lists&utm_medium=website&utm_content=house-warming-Cakes" class="fw-300">Cakes</a>
										<a href="{{ route('city.occasion.house-warming.collection.list', $sessionCityUrl) }}?utm_source=top-header&utm_campaign=category-lists&utm_medium=website&utm_content=house-warming-Collections" class="fw-300">Collections</a>
									</div>
									<a class="dropdown-btn dropdown-head" href="#">Store Openings
										<span class="pull-right"><i class="glyphicon glyphicon-menu-right dropdown-icon mar-t-3"></i></span>
									</a>
									<div class="dropdown-container nested-dropdown-container">
										<a href="{{ route('city.occasion.sodecors.list',$sessionCityUrl) }}?ref=navbar" class="fw-300">Decorations</a>
									</div>
									<a class="dropdown-btn dropdown-head" href="#">Naming Ceremony
										<span class="pull-right"><i class="glyphicon glyphicon-menu-right dropdown-icon mar-t-3"></i></span>
									</a>
									<div class="dropdown-container nested-dropdown-container">
										<a href="{{ route('city.occasion.ncdecors.list',$sessionCityUrl) }}?ref=navbar" class="fw-300">Decorations</a>
									</div>
									<a class="dropdown-btn dropdown-head" href="#">Baby Shower
										<span class="pull-right"><i class="glyphicon glyphicon-menu-right dropdown-icon mar-t-3"></i></span>
									</a>
									<div class="dropdown-container nested-dropdown-container">
										<a href="{{route('inspirations.feed','baby-shower-decor-ideas') }}?ref=navbar" class="fw-300">Decorations</a>
									</div>
								</div>
							</div>
							<div class="list-slide-category-title mar-b-10">
								<a class="dropdown-btn dropdown-head enable-link" id="invites" style="text-decoration: none; color: #5A5A5A;  display: block;">
									<i class="glyphicon glyphicon-picture pad-r-5"></i> Invites
									<span class="pull-right"><i class="glyphicon glyphicon-menu-right dropdown-icon mar-t-3"></i></span>
								</a>
								<div class="dropdown-container">
									<a href="{{ route('e-cards.invites') }}?ref=navbar" class="dropdown-head enable-link">Invites</a>
									<a href="{{ route('e-cards.wish-cards') }}?ref=navbar" class="dropdown-head enable-link">Wish Cards</a>
									<a href="{{ route('e-cards.thank-you-cards') }}?ref=navbar" class="dropdown-head enable-link">Thank You Cards</a>
								</div>
							</div>
							<div class="list-slide-category-title mar-b-10">
								<a class="" href="{{route('city.occasion.corporate.home',$sessionCityUrl)}}?ref=navbar" style="text-decoration: none; color: #5A5A5A;  display: block;">
									<img src="{{$galleryUrl}}/main/img/icons/three-buildings.png" width="16px" height="16px" class="mar-r-8" alt="corporate events">
									For Companies
								</a>
							</div>
							<div class="list-slide-category-title pad-b-5 hide">
								<a class="" href="{{ route('store-redirect', ['redirect' => config('evibe.store_host').'?utm_source=Sidebar&utm_medium=Website&utm_campaign=StorePromotion']) }}" style="text-decoration: none; color: #5A5A5A;  display: block;">
									<img src="{{$galleryUrl}}/main/img/icons/confetti-nav.png" width="16px" height="16px" class="mar-r-8" alt="party props">
									Party Props
								</a>
							</div>
						</div>
						<hr>
						<div class="list-slide-category-wrap pad-l-20">
							<div class="list-slide-category-title mar-b-10">
								<a href="{{ route("track.orders") }}" style="text-decoration: none; color: #5A5A5A;">
									<i class="glyphicon glyphicon-shopping-cart pad-r-5"></i> My Orders
								</a>
							</div>
							<div class="list-slide-category-title pad-b-10">
								<a href="{{ route("wishlist.list") }}" style="text-decoration: none; color: #5A5A5A;">
									<i class="glyphicon glyphicon-bookmark pad-r-5"></i> My Wishlist
								</a>
							</div>
							<div class="list-slide-category-title pad-b-10">
								<a href="{{ route("wallet.home") }}?ref=mobile-nav" style="text-decoration: none; color: #5A5A5A;">
									<i class="glyphicon glyphicon-credit-card pad-r-5"></i> My Wallet
								</a>
							</div>
							<div class="list-slide-category-title pad-b-5">
								<a href="{{route('user.browsing.history.list.new')}}" style="text-decoration: none; color: #5A5A5A;">
									<i class="glyphicon glyphicon-time pad-r-5"></i> My Browsing History
								</a>
							</div>
						</div>
						<hr>
						<div class="list-slide-category-wrap pad-l-20 mar-b-15">
							<div class="list-slide-category-title mar-b-10 hide">
								<a href="tel:{{ config('evibe.contact.company.plain_phone') }}" class="" style="text-decoration: none; color: #5A5A5A;">
									<i class="glyphicon glyphicon-earphone pad-r-5"></i> +91 {{ config('evibe.contact.company.plain_phone') }}
								</a>
							</div>
							<div class="list-slide-category-title mar-b-10">
								<a class="lowercase" style="text-decoration: none; color: #5A5A5A;" href="mailto:{{ config('evibe.contact.customer.group') }}" target="_top">
									<i class="glyphicon glyphicon-envelope pad-r-5"></i> {{ config('evibe.contact.customer.group') }}
								</a>
							</div>
							<div v-if="user != ''" class="mar-t-5">
								<a href="/user/logout" class="in-blk" style="text-decoration: none; color: #5A5A5A;">
									<i class="glyphicon glyphicon-off pad-r-5"></i>
									<span class="list-logout-text text-uppercase">Logout</span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-3 col-sm-3 no-pad">
			<div class="list-slide-empty" id="listSlideEmpty">
				<!-- @see: should be empty, do not remove -->
			</div>
		</div>
	</div>
</div>

<div id="listSliderBackground" class="list-slider-background" style="display: none; background-color: rgba(0,0,0,0.50);"></div>