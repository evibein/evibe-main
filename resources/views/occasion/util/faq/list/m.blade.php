@if(isset($data['occasionId']) && ($data['occasionId'] == config('evibe.occasion.surprises.id')))
	<div>
		@if(isset($data['tags']) && count($data['tags']) && isset($data['tags'][config('evibe.surprise-relation-tags.candle-light-dinner')]))
			<div class="product-faq-wrap">
				<div class="product-faq-question">
					<div class="product-faq-question-text">Can we choose lunch instead of dinner?</div>
				</div>
				<div class="product-faq-answer">
					<div class="product-faq-answer-text">
						Yes, you can choose lunch instead of dinner if it's not poolside or rooftop area subject to availability. Lunch slot can be anytime between 12.30 PM and 3 PM.
					</div>
				</div>
			</div>
			<div class="product-faq-wrap">
				<div class="product-faq-question">
					<div class="product-faq-question-text">Can we extend the timing to midnight for cake cutting?</div>
				</div>
				<div class="product-faq-answer">
					<div class="product-faq-answer-text">
						Most of the places do not have permission to run in the midnight. If the package has midnight provision, it will be mentioned in the package details.					</div>
				</div>
			</div>
			<div class="product-faq-wrap">
				<div class="product-faq-question">
					<div class="product-faq-question-text">Can we make changes to the menu?</div>
				</div>
				<div class="product-faq-answer">
					<div class="product-faq-answer-text">
						You can see which meal course is available at the Hotel in the package details. You can choose the specific items at the place directly based on the chef menu offered on your special day.
					</div>
				</div>
			</div>
			<div class="product-faq-wrap">
				<div class="product-faq-question">
					<div class="product-faq-question-text">What should I do after making the payment?</div>
				</div>
				<div class="product-faq-answer">
					<div class="product-faq-answer-text">
						<ol>
							<li>You will receive a booking receipt once the payment is made.</li>
							<li>The receipt has concerned coordinator name and phone number along with place details.</li>
							<li>On your event date, You can directly reach the place and show this at the reception/entry.</li>
						</ol>
					</div>
				</div>
			</div>
			<div class="product-faq-wrap">
				<div class="product-faq-question">
					<div class="product-faq-question-text">I need other customisations to this package, what should I do?</div>
				</div>
				<div class="product-faq-answer">
					<div class="product-faq-answer-text">You can write to enquiry@evibe.in with your party date and request, you will get an update in 2 business hours.</div>
				</div>
			</div>
			<div class="product-faq-wrap">
				<div class="product-faq-question">
					<div class="product-faq-question-text">Can we change from wine to any other mocktail/cocktail/wine/juice?</div>
				</div>
				<div class="product-faq-answer">
					<div class="product-faq-answer-text">
						Yes, you can change based on the options available at the place only if the wine/cocktail is included in this package.
					</div>
				</div>
			</div>
			<div class="product-faq-wrap">
				<div class="product-faq-question">
					<div class="product-faq-question-text">What cake flavours are available?</div>
				</div>
				<div class="product-faq-answer">
					<div class="product-faq-answer-text">
						If cake is provided as a complimentary in this package, then it will be in chocolate flavour or a cream cake with occasion name. If you need any specific preferences with the cake, you can book this separately from add-ons section.
					</div>
				</div>
			</div>
			<div class="product-faq-wrap">
				<div class="product-faq-question">
					<div class="product-faq-question-text">Are cabs available from this place?</div>
				</div>
				<div class="product-faq-answer">
					<div class="product-faq-answer-text">
						Location and landmark are mentioned in the package, you can check the availability from the cab booking app which you use.
					</div>
				</div>
			</div>
		@else
			<div class="product-faq-wrap">
				<div class="product-faq-question">
					<div class="product-faq-question-text">I need other customisations to this package, what should I do?</div>
				</div>
				<div class="product-faq-answer">
					<div class="product-faq-answer-text">You can write to enquiry@evibe.in with your party date and request, you will get an update in 2 business hours.</div>
				</div>
			</div>
			<div class="product-faq-wrap">
				<div class="product-faq-question">
					<div class="product-faq-question-text">What should I do after making the payment?</div>
				</div>
				<div class="product-faq-answer">
					<div class="product-faq-answer-text">
						<ol>
							<li>You will receive a booking receipt once the payment is made.</li>
							<li>The receipt has concerned coordinator name and phone number along with booking details.</li>
							<li>The coordinator will call you a day before with reporting time and does the needful.</li>
							<li>If you made the payment in the last minute, you will receive a call from the coordinator before starting to your location.</li>
							<li>You need to keep the balance amount ready in cash.</li>
						</ol>
					</div>
				</div>
			</div>
		@endif
	</div>
@else
	<div class="text-center">
		Sorry! There are no customer questions for this product yet.
	</div>
@endif