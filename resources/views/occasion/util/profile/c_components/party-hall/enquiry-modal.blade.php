<div id="hallEnquiryModal" class="modal modal-md fade text-center modal-hall-enquiry" role="dialog">
	<div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content">
			<form class="enquiry-form-profile-pages">
				<div class="cross-button ga-btn-close-book-now-modal text-center" data-dismiss="modal">x</div>
				<div class="card-info-text">
					<div class="mar-t-15">
						<div class="font-18 text-bold">Get Best Deal</div>
						<div class="mar-t-10">
							@if(isset($formDescription))
								{{ $formDescription }}
							@else
								Please submit your details. We will contact you at the earliest.
							@endif
						</div>
					</div>
					<div class="errors-cnt alert-danger hide mar-t-5"></div>
					<div class="form-fields">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="enquiryProfileName" id="enquiryProfileName" value=""/>
								<label class="mdl-textfield__label" for="enquiryProfileName">Name</label>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="enquiryProfilePhone" id="enquiryProfilePhone" value=""/>
								<label class="mdl-textfield__label" for="enquiryProfilePhone">Phone Number</label>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="enquiryProfileEmail" id="enquiryProfileEmail" value=""/>
								<label class="mdl-textfield__label" for="enquiryProfileEmail">Email</label>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="enquiryProfileDate" id="enquiryProfileDate" value=""/>
								<label class="mdl-textfield__label" for="enquiryProfileDate">Party Date</label>
							</div>
						</div>
						@if(!isset($hidePartyLocation) || !$hidePartyLocation)
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input class="mdl-textfield__input google-auto-complete" type="text" name="enquiryProfilePartyLocation" id="enquiryProfilePartyLocation" placeholder="" value=""/>
									<label class="mdl-textfield__label" for="enquiryProfilePartyLocation">
										@if(isset($locationTitle))
											{{ $locationTitle }}
										@else
											Party Location
										@endif
									</label>
									<input type="hidden" class="google-location-details" name="locationDetails" value="">
								</div>
							</div>
						@endif
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="mdl-card__actions">
					<button class="btn btn-default btn-view-more btn-hall-enquiry-submit" id="btnSubmitForm" data-url="{{ $enquiryUrl }}">
						<i class="material-icons valign-mid">&#xE8DC;</i>
						<span class="valign-mid">Submit Details</span>
					</button>
				</div>
				<div class="mar-t-10 mar-b-20 mar-r-15 mar-l-15 text-muted text-center low-font">
					Submitting the form above implies that you accept to our
					<a target="_blank" rel="noopener" href="{{ route('terms') }}" class="text-muted text-underline mar-l-3">terms and conditions.</a>
				</div>
			</form>
		</div>

	</div>
</div>