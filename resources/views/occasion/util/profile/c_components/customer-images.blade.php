<div id="reviewSection" class="desc-component-wrap mar-b-20">
	<div class="desc-component-title">
		Customer Images
	</div>
	<div class="full-width">
		@if($agent->isMobile() && !($agent->isTablet()))
			<div class="pad-l-10 pad-r-10" style="height: 160px;width: 100%;overflow-x:scroll;white-space: nowrap;">
				@php $counter=1; @endphp
				@foreach($deliveryImages as $img)
					@if($counter==5)
						@break
					@endif
					<div class="in-blk pad-5 w-200-imp customer-images-wrap">
						<a class="thumbnail no-pad" href="#" data-image-id="@php echo $counter; @endphp" data-toggle="modal"
								data-image="{{$img->getPath()}}"
								data-target="#image-gallery" style="border:none">
							<img class="img-thumbnail img-thumb" src="{{$img->getPath()}}" alt="">
						</a>
					</div>
					@php $counter++; @endphp
				@endforeach
			</div>
		@else
			<div class="row pad-l-10 pad-r-10">
				@php $counter=1; @endphp
				@foreach($deliveryImages as $img)
					@if($counter==5)
						@break
					@endif
					<div class=" customer-images-wrap col-sm-3 col-lg-3 col-md-3 col-xs-4 pad-5">
						<a class="thumbnail no-pad" href="#" data-image-id="@php echo $counter; @endphp" data-toggle="modal"
								data-image="{{$img->getPath()}}"
								data-target="#image-gallery" style="border:none">
							<img class="img-thumbnail img-thumb " src="{{$img->getPath()}}" alt="">
						</a>
					</div>
					@php $counter++; @endphp
				@endforeach
			</div>
		@endif
		<div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="image-gallery" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title text-center">Customer Images</h5>
					</div>
					<div class="modal-body customer-images-div-modal">
						<img src="{{ $galleryUrl }}/img/logo/logo_evibe.png" id="image-gallery-image" class="img-responsive customer-review-image">
						<button type="button" class="btn customer-review-control-left" id="show-previous-image">
							<span class="glyphicon glyphicon-chevron-left"></span>
						</button>
						<button type="button" id="show-next-image" class="btn customer-review-control-right">
							<span class="glyphicon glyphicon-chevron-right"></span>
						</button>
						<button class="btn btn-md view-gallery-btn-style view-gallery-modal view-gallery-btn" >
							<span class="glyphicon glyphicon-th"></span><span class="font-14">VIEW ALL IMAGES</span>
						</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="image-library" tabindex="-1" role="dialog" aria-labelledby="image-library" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header text-center full-width">
						<span class="font-18"> Customer Image Gallery</span>
					</div>
					<div class="modal-body " style="height: 350px;overflow-y: scroll">
						@if(count($deliveryImages))
							@php $counter=1; @endphp
							@foreach($deliveryImages as $img)
								<div class="thumb col-sm-3 col-md-3 col-xs-4 pad-5 " style="padding-top:0;height: 100px">
									<a class="thumbnail no-mar-b" href="#" data-image-id="@php echo $counter; @endphp" data-toggle="modal"
											data-image="{{$img->getPath()}}"
											data-target="#image-gallery" style="border:none">
										<img class="img-thumbnail" src="{{$img->getPath()}}" style="min-height: 95px;width:auto;object-fit: cover">
									</a>
								</div>
								@php $counter++; @endphp
							@endforeach
							<input type="hidden" id="imagecounter" value="@php echo $counter-1 @endphp">
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
	@if(count($deliveryImages)>4)
		<div class="full-width  view-gallery view-gallery-btn">
			<span style="color:#0066c0;text-decoration: underline;">View All Images</span>
		</div>
	@endif
	<div class="clearfix"></div>
</div>