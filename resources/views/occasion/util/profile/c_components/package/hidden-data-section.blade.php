@if(isset($data['package']))
	@if(isset($data['validMinBookingDate']))
		<input type="hidden" id="hidMinBookingDate" value="{{ $data['validMinBookingDate'] }}">
	@endif

	<input type="hidden" id="hidOptionId" value="{{ $data['package']->id }}">
	<input type="hidden" id="hidOptionTypeId" value="{{ $data['package']->type_ticket_id }}">
@endif