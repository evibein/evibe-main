<div class="product-input-wrap pc-highlights-package-wrap mar-t-15">
	@php
		$typeId = $data['package']->type_ticket_id;
		$typeId = ($typeId == config('evibe.ticket.type.package')) ? config('evibe.ticket.type.generic-package') : $typeId;
	@endphp
	<div class="col-md-12">
		<div id="availCheckFailMsg" class="avail-check-fail-msg hide">
			<i class="glyphicon glyphicon-remove-sign"></i> We regret to inform that this service is not available for
			<span id="availCheckRegretMsg" class="text-bold mar-l-4"></span>
		</div>
		@if(isset($data['campaignText']) && $data['campaignText'] && isset($data['campaignFocusDate']) && $data['campaignFocusDate'])
			<div id="campaignTextMessage" class="hide text-o mar-b-10" data-campaign-focus-date="{{ $data['campaignFocusDate'] }}">{{ $data['campaignText'] }}</div>
		@endif
	</div>
	<form id="autoBookingForm" data-url="{{ route('auto-book.surprises-packages.pay.auto.init', [config('evibe.ticket.type.package'), $data['package']->id, $typeId]) }}">
		<div class="product-book-form @if ($agent->isMobile() && !($agent->isTablet())) product-book-form-hidden @endif">
			<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
				<div class="errors-msg pad-t-3 pad-b-4 hide alert-danger"></div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="input-field">
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
							<input class="mdl-textfield__input mar-b-5 @if(isset($data['package']->type_ticket_id) && ($data['package']->type_ticket_id == config('evibe.ticket.type.villas'))) bachelor-checkout-date @endif" type="text" name="checkInDate" id="checkInDate"/>
							<label class="mdl-textfield__label" for="checkInDate">
								@if(($data['package']->map_type_id) == 3) Check In Date @else Party Date @endif</label>
						</div>
					</div>
				</div>
				@if(($data['package']->map_type_id) == 3)
					@if(isset($data['checkInSlots']) && count($data['checkInSlots']))
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="input-field">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
									<label class="mdl-textfield__label" for="checkInTime">
										<span class="label-text">Check In Time</span>
									</label>
									<select name="checkInTime" id="checkInTime" class="mdl-textfield__input mar-b-5">
										@foreach($data['checkInSlots'] as $checkInSlot)
											<option value="{{ $checkInSlot['checkInTime'] }}" data-check-out-time="{{ $checkInSlot['checkOutTime'] }}">{{ $checkInSlot['checkInTime'] }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
					@else
					@endif
				@else
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="input-field">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
								<input class="mdl-textfield__input mar-b-5" type="text" name="partyTime" id="partyTime"/>
								<label class="mdl-textfield__label" for="partyTime">Party Time</label>
							</div>
						</div>
					</div>
				@endif
				@if($data['package']->guests_max && $data['package']->guests_max > $data['package']->group_count)
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="input-field">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
								<label class="mdl-textfield__label" for="pcGuestsCount">
									<span class="label-text">Guests Count</span>
								</label>
								<select id="pcGuestsCount" name="pcGuestsCount" class="mdl-textfield__input mar-b-5" data-groupcount="{{ $data['package']->group_count }}">
									@for($i = $data['package']->group_count; $i <= $data['package']->guests_max; $i++)
										<option value="{{ $i }}"> {{ $i }}</option>
									@endfor
								</select>
							</div>
						</div>
					</div>
				@endif
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad">
						@if(isset($data['countries']) && count($data['countries']))
							<select id="checkoutModalCallingCode" name="checkoutModalCallingCode" class="form-control country-calling-code">
								@foreach($data['countries'] as $country)
									<option value="{{ $country->calling_code }}">{{ $country->calling_code }}</option>
								@endforeach
							</select>
						@else
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
								<input class="mdl-textfield__input mar-b-5" type="text" name="checkoutModalCallingCode" id="checkoutModalCallingCode" value="+91"/>
								<label class="mdl-textfield__label" for="checkoutModalCallingCode">Code</label>
							</div>
						@endif
					</div>
					<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-pad-r">
						<div class="input-field">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input id="phone" type="text" class="mdl-textfield__input mar-b-5" name="phone">
								<label class="mdl-textfield__label" for="phone">
									<span class="label-text">Phone Number</span>
								</label>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
			<div class="pc-adv-options-wrap @if(isset($data['package']->map_type_id) && ($data['package']->map_type_id == config('evibe.ticket.type.venue'))) hide @else @endif">
				<div class="pc-adv-radio" id="advanceSelect">
					<label class="btn-adv-option pc-adv-radio-option">
						<input type="radio" name="advSelectOption" class="adv-radio-btn pull-left" value="0">
						<div class="adv-radio-option-text">
							Pay
							<span class="text-bold text-e mar-l-4"><span class="rupee-font">&#8377; </span><span class="adv-radio-advance-amount" data-adv-percent="{{config('evibe.ticket.advance.percentage')}}"> {{ round($data['package']->price / config('evibe.ticket.advance.percentage')) }}</span></span> as advance
							<div class="font-12 pc-adv-percent-text">({{config('evibe.ticket.advance.percentage')}}% of service
								<span class="add-ons-price-wrap no-mar-l"> + <span class="add-ons-selected-count"></span> add-ons</span>)
							</div>
						</div>
					</label>
					<label class="btn-adv-option pc-adv-radio-option">
						<input type="radio" name="advSelectOption" class="adv-radio-btn pull-left" value="1" checked="checked">
						<div class="adv-radio-option-text">
							Pay
							<span class="text-bold text-e mar-l-4"><span class="rupee-font">&#8377; </span><span class="adv-radio-total-amount" data-adv-percent="100">{{ $data['package']->price }}</span></span> as advance
							<div class="font-12 pc-adv-percent-text">(100% of service
								<span class="add-ons-price-wrap no-mar-l"> + <span class="add-ons-selected-count"></span> add-ons</span>)
							</div>
						</div>
					</label>
				</div>
			</div>
		</div>
		<div class="product-book-price-split-wrap hide">
			<div class="product-book-price-split">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<div class="product-book-price-title">Product Price</div>
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<div>
						<span class="rupee-font">&#8377; </span>
						<span class="product-book-price">@amount($data['package']->price)</span>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			@if($data['package']->guests_max && $data['package']->guests_max > $data['package']->group_count)
				<div class="product-book-price-split">
					<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
						<div class="product-book-price-title">Extra Guests Price</div>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
						<div>
							<span class="rupee-font">&#8377; </span>
							<span class="product-book-extra-guests-price">0</span>
						</div>
						<div class="font-10 text-light-grey">
							(<span class="product-book-extra-guests-count"></span>
							<span> * </span>
							<span class="rupee-font">&#8377; </span>
							<span class="product-book-price-per-extra-guest">0</span>)
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			@endif
			@if(isset($data['addOns']) && count($data['addOns']))
				<div class="product-book-price-split">
					<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
						<div class="product-book-price-title">Add Ons Price</div>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
						<div>
							<span class="rupee-font">&#8377; </span>
							<span class="product-book-addons-price add-ons-price"></span>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			@endif
			<div class="product-book-price-split">
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<div class="product-book-price-title">Total Booking Amount</div>
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<div class="text-e">
						<span class="rupee-font">&#8377; </span>
						<span class="product-book-total-booking-amount"></span>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="text-center mar-t-15 mar-b-20__400">
			<div class="input-field hide">
				<input id="agreePolicy" name="checkbox" type="checkbox" checked>
				<label for="agreePolicy" class="text-normal mar-l-2">
					<span class="mar-r-4">I have read and accept</span>
					<a href="{{ route('terms') }}" target="_blank">terms of booking</a>.
				</label>
			</div>
			<a id="btnPcSubmit" class="btn btn-default product-btn-book-now ga-btn-book-now">
				<i class="material-icons valign-mid">&#xE8CC;</i>
				<span class="valign-mid mar-l-4">Book Now</span>
			</a>
		</div>
		<input type="hidden" id="occasionId" name="occasionId" value="{{ config('evibe.occasion.surprises.id') }}">
		<input type="hidden" id="forVenueDetails" class="forVenueDetails" value="{{ $data['package']->map_type_id }}">
	</form>
</div>

@include("app.generic-modal-loader")