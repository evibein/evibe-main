@php $cityUrl=is_null(getCityUrl())?'bangalore':getCityUrl(); @endphp
@if($data)
	<hr class="desktop-profile-hr">
	<div class="similar-products-section w-1366">
		<div class="similar-products-wrap">
			<div class="product-description-tab-title similar-products-title text-center">
				You Might Also Like
			</div>
			<div class="similar-products-content mar-t-15">
				<div class="col-xs-12 col-sm-12 bg-white no-pad">
					<div class="full-width">
						<div class="owl-carousel owl-theme categoryCards">
							@foreach ($data['cards'] as $card)
								<div class="item text-center">
									<div class="product-similar-img-wrap " style="height: auto">
										<a href="{{ config('evibe.host') }}/{{ $cityUrl }}/{{ $card['cta'] }}">
											<img class="product-similar-img" src="{{ $galleryUrl }}/{{config('evibe.occasion-landingpages.surprises.gallery-path')}}/{{$card['img_url_mbl']}}" alt="{{$card['alt_text'] }}" onError="this.src=''; this.style='padding: 30px 30px'" style="height: auto;width:100% ">
										</a>
									</div>
								</div>
							@endforeach
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
@endif