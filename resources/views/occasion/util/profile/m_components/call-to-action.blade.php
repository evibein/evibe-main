<div class="hide">
	@if($data['occasionId'] == config("evibe.occasion.pre-post.id"))
		<div class="cta-section text-center product-btn-mobile-submit hide">
			<a class="btn btn-default product-btn-book-now btn-profile-book-free-site-visit">
				<span class="valign-mid">BOOK FREE SITE VISIT</span>
			</a>
		</div>
	@else
		<div class="cta-section text-center product-btn-mobile-submit">
			<div class="col-xs-6 col-sm-6 no-pad">
				<a class="btn btn-default product-btn-grey pre-check-feasibility-button">
					<span class="valign-mid">Check Availability</span>
				</a>
			</div>
			<div class="col-xs-6 col-sm-6 no-pad">
				<a class="btn btn-default product-btn-book-now pre-check-book-now-button">
					<span class="valign-mid">Book Now</span>
				</a>
			</div>
			<div class="clearfix"></div>
		</div>
	@endif
</div>