<div class="images-section">
	@if (count($data['gallery']))
		<div class="loading-img-wrap no-mar-t">
			<div class="animated-background" style="min-height: 210px; margin: 15px;"></div>
		</div>
		<div class="gallery-wrap mobile-gallery-wrap no-mar hide">
			<div class="fotorama" data-width="100%" data-ratio="900/600" data-maxheight="400" data-autoplay="true" data-thumbwidth="80" data-thumbmargin="10" data-keyboard="true">
				@foreach ($data['gallery'] as $gallery)
					<a href="{{ $gallery['url'] }}">
						<img src="{{ $gallery['thumb'] }}" alt="{{ $gallery['title'] }} in {{ getCityUrl() }}"/>
					</a>
				@endforeach
				<a href="{{ $galleryUrl }}/main/img/explainer-image_profile.png">
					<img src="{{ $galleryUrl }}/main/img/explainer-image_thumb.png" alt="Lifelong Memories Guaranteed"/>
				</a>
			</div>
		</div>
	@else
		<div><img class="mob-profile-no-image" src="{{ $galleryUrl }}/img/icons/balloons.png"></div>
	@endif
</div>