<div class="title-section">
	<div class="title-section-wrap mar-t-5">
		<div class="product-title-text">
			{{ $data['package']->name }}
		</div>
		<div class="product-rating @if(!($data['ratings']['total']['count'])) hide @endif">
			<div class="font-16">
				<div class="in-blk">
					<input type="number" class="avg-rating hide" value="{{ $data['ratings']['total']['avg'] }}" title="{{ $data['ratings']['total']['avg'] }} average rating for provider of {{ $data['package']->name }}"/>
				</div>
				@if($data['ratings']['total']['count'])
					<div id="reviews" class="product-rating-reviews scroll-item cur-point in-blk scroll-to-reviews" data-scroll="reviews"> ({{ $data['ratings']['total']['count'] }} reviews)
					</div>
				@else
					<div class="product-rating-reviews cur-point in-blk"> (0 reviews)</div>
				@endif
			</div>
		</div>
		<div class="product-price mar-t-10">
			<div class="product-price-wrap text-center">
				<div class="product-price-old cake-profile">
					<div class="card-item price-item cake-price-cnt landing-surprise-cnt text-center">
						<div class="price-content">
							<div class="pad-t-10">
								<div class="product-main-price price-val in-blk">
									<span class='rupee-font'>&#8377;</span>
									<span class="displayPrice"> {{ config("landing.ads.base_price") }}*</span>
								</div>
								<div class="product-price-tag mar-l-5 in-blk">
									<i class="glyphicon glyphicon-thumbs-up"></i> Best Price
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="mar-t-5 font-12">
							<i>(price may vary based on <b>Location</b> & <b>Theater</b>)</i>
						</div>
					</div>
					<div class="loading-cnt product-price-load-wrap hide">
						<img src="{{ $galleryUrl }}/img/icons/fb_loading.png" alt="loading..." class="rotate">
					</div>
					<div class="mar-t-15">
						@foreach(config("landing.ads.options") as $name => $addOn)
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="card-item cake-add-on-wrap mar-t-10" data-catid="{{ $name }}" data-name="{{ $name }}">
									<div class="col-xs-5 col-sm-5 col-md-5">
										<div class="product-property-name mar-t-5">
											{{ $name }}
										</div>
									</div>
									<div class="col-xs-7 col-sm-7 col-md-7">
										<div class="product-property-value">
											<select class="form-control landing-surprises-options landing-surprises-{{ $name }}">
												@foreach($addOn as $optionName => $value)
													<option value="{{ $value }}">
														{{ $optionName }}
													</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						@endforeach
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="product-cta pad-l-15 mar-t-15">
			<div class="col-md-12 col-lg-12 no-pad-l">
				<div class="pad-t-5">
					<a class="btn btn-default product-btn-book-now btn-cake-book-now ga-btn-book-now landing-surprises-enquire-now">
						<i class="material-icons valign-mid">&#xE0CB;</i>
						<span class="valign-mid"> Enquire Now</span>
					</a>
					@include('app.modals.auto-popup',
					[
					"cityId" => $data['shortlistData']['cityId'],
					"occasionId" => $data['shortlistData']['occasionId'],
					"mapTypeId" => $data['shortlistData']['mapTypeId'],
					"mapId" => "",
					"optionName" => ""
					])
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
@section("javascript")
	@parent
	<script type="text/javascript">
        $(document).ready(function () {
            $(".landing-surprises-options").on("change", function () {
                $(".product-price-load-wrap").removeClass("hide");
                $(".landing-surprise-cnt").addClass("hide");
                $.ajax({
                    url: '/surprises/theatre-movie-wishes/get-price',
                    type: "POST",
                    tryCount: 0,
                    retryLimit: 3,
                    data: {"cityName": $(".landing-surprises-City").val(), "theatreName": $(".landing-surprises-Theatre").val()},
                    success: function (data) {
                        $(".product-price-load-wrap").addClass("hide");

                        if (data > 0) {
                            $(".displayPrice").empty().text(" " + data + "*");
                            $(".landing-surprise-cnt").removeClass("hide");
                        }
                        else {
                            window.showNotyError("Selected theatre not available in the current city, Please choose another theatre.");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $(".product-price-load-wrap").addClass("hide");

                        this.tryCount++;
                        if (this.tryCount <= this.retryLimit) {
                            $.ajax(this);
                        }
                        else {
                            window.showNotyError("An error occurred while fetching price data. Please reload the page.");
                            window.notifyTeam({
                                "url": '/surprises/theatre-movie-wishes/get-price',
                                "textStatus": textStatus,
                                "errorThrown": errorThrown
                            });
                        }
                    }
                });
            });

            $(".landing-surprises-enquire-now").on("click", function (event) {
                event.preventDefault();

                var popUpEnquiryForm = $(".popup-need-help-btn");
                if (popUpEnquiryForm.length && (parseInt(popUpEnquiryForm.data("loaded"), 10) === 2)) {
                    popUpEnquiryForm.click();
                } else {
                    $("#modalEnquiryForm").modal({
                        "backdrop": "static",
                        "keyboard": false,
                        "show": true
                    });
                }
            });
        });
	</script>
@endsection