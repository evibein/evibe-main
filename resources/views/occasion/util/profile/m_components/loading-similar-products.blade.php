<div id="loadingSimilarProducts">
	<hr class="mobile-profile-hr">
	<div class="pad-l-15 loading-similar-title-wrap">
		<div class="loading-similar-title">

		</div>
	</div>
	@php $j = 0 @endphp
	@while($j < 3)
		<div class="col-xs-4 col-sm-4 mar-t-15 mar-b-15">
			<div class="product-similar-img-wrap">
				<div class="loading-similar-image"></div>
			</div>
			<div class="product-similar-title">
				<div class="loading-similar-text-wrap">
					<div class="loading-similar-text">

					</div>
				</div>
			</div>
			<div class="product-similar-price">
				<div class="loading-similar-worth-wrap">
					<div class="loading-similar-worth">

					</div>
				</div>
				<div class="loading-similar-price-wrap">
					<div class="loading-similar-price">

					</div>
				</div>
			</div>
		</div>
		@php $j++; @endphp
	@endwhile
	<div class="clearfix"></div>
</div>