<div class="product-description-section">
	<div class="product-description-wrap mar-t-20">
		<div class="hide">
			<div class="mar-l-5 mar-r-5 product-info-tabs">
				@if ($data['info'] || (isset($data['highlights']) && count($data['highlights'])) || (isset($data['addOns']) && count($data['addOns'])))
					<a class="scroll-item btn btn-default mar-5" data-scroll="inclusions">Inclusions</a>
				@endif
				@if ($data['preq'] || $data['facts'])
					<a class="scroll-item btn btn-default mar-5" data-scroll="thingsToRemember">Things To Remember</a>
				@endif
				@if(isset($data['addOns']) && count($data['addOns']))
					<a class="scroll-item btn btn-default mar-5" data-scroll="addOns">Add Ons</a>
				@endif
				@if ($data['terms'])
					<a class="scroll-item btn btn-default mar-5" data-scroll="terms">Terms</a>
				@endif
				@if(isset($data['cancellationData']) && !is_null($data['cancellationData']))
					<a class="scroll-item btn btn-default mar-5" data-scroll="cancellationsAndRefunds">Cancellations & Refunds</a>
				@endif
			</div>
		</div>
		<div class="product-description-content">
			<div class="" id="tabs">
				<div class="tab-pane product-description-tab mar-t-10 mar-l-10 mar-r-15" id="inclusions">
					<div class="product-description-tab-title">
						Service Inclusions
					</div>
					<div class="product-highlights-wrap mar-b-15">
						@if($data['code'])
							<div class="product-highlight-item">
								<div class="product-highlight-item-title in-blk">
									<i class="glyphicon glyphicon-th-list"></i> Service Code:
								</div>
								<div class="product-highlight-item-text in-blk mar-l-5">{{ $data['code'] }}</div>
								<div class="clearfix"></div>
							</div>
						@endif
					</div>
					@if ($data['info'])
						{!! $data['info'] !!}
					@endif
					<div class="text-center pad-t-10 pad-b-5">
						@include('app.social-share-plugin')
					</div>
				</div>
				@if(isset($data['addOns']) && count($data['addOns']))
					<div class="tab-pane product-description-tab mar-t-10 mar-l-10 mar-r-15" id="addOns">
						@include('occasion.util.add-ons.list', ['addOns' => $data['addOns']])
					</div>
				@endif
				@if ($data['preq'] || $data['facts'])
					<div class="tab-pane product-description-tab mar-t-10 mar-l-10 mar-r-15" id="thingsToRemember">
						<div class="product-description-tab-title">
							Things To Remember
						</div>
						@if($data['preq'])
							{!! $data['preq'] !!}
						@endif
						@if($data['facts'])
							{!! $data['facts'] !!}
						@endif
					</div>
				@endif
				@if ($data['terms'])
					<div class="tab-pane product-description-tab mar-t-10 mar-l-10 mar-r-15" id="terms">
						<div class="product-description-tab-title">
							Terms Of Booking
						</div>
						{!! $data['terms'] !!}
					</div>
				@endif
				@if(isset($data['cancellationData']) && !is_null($data['cancellationData']))
					@include('occasion.util.profile.m_components.cancellation-policy')
				@endif
			</div>
		</div>
		<div class="mar-t-30 mar-b-30 text-center">
			<span>Need help?</span>
			<span><a class="product-link-enquire-now ga-btn-enquire-now popup-need-help-btn mar-l-5 ga-mobile-profile-enquire-link">Enquire Now</a></span>
		</div>
		@include('app.modals.auto-popup',
				[
				"cityId" => $data['cityId'],
				"occasionId" => $data['occasionId'],
				"mapTypeId" => $data['mapTypeId'],
				"mapId" => $data['id'],
				"optionName" => $data['name'],
				"countries" => (isset($data['countries']) && count($data['countries'])) ? $data['countries'] : []
				])
	</div>
</div>