<div class="title-section">
	<div class="title-section-wrap mar-t-5">
		<h1 class="product-title-text no-mar-b no-mar-t ls-normal">
			{{ $data['package']->name }}
		</h1>
		<div class="product-rating @if(!($data['ratings']['total']['count'])) hide @endif">
			<div class="font-16">
				<div class="in-blk">
					<input type="number" class="avg-rating hide" value="{{ $data['ratings']['total']['avg'] }}" title="{{ $data['ratings']['total']['avg'] }} average rating for provider of {{ $data['package']->name }}"/>
				</div>
				@if($data['ratings']['total']['count'])
					<div id="reviews" class="product-rating-reviews scroll-item cur-point in-blk scroll-to-reviews" data-scroll="reviews"> ({{ $data['ratings']['total']['count'] }} reviews)
					</div>
				@else
					<div class="product-rating-reviews cur-point in-blk"> (0 reviews)</div>
				@endif
			</div>
		</div>
		<div class="product-price product-price-wrap mar-t-10">
			<div>
				<div class="in-blk">
					@if ($data['package']->price_worth && $data['package']->price_worth > $data['package']->price)
						<input type="hidden" id="pcProductPriceWorth" value="{{ $data['package']->price_worth }}">
						<div class="product-price-worth in-blk mar-l-2">
							<span class='rupee-font'>&#8377; </span><span id="pcBookingWorth" data-price="{{ $data['package']->price_worth }}">@amount($data['package']->price_worth)</span>
						</div>
					@endif
					@if($data['package']->type_ticket_id != config('evibe.ticket.type.surprises'))
						@if($data['package']->price && $data['package']->price_worth)
							<div class="product-price-off mar-l-5 in-blk @if($data['package']->type_ticket_id == config('evibe.ticket.type.villas')) hide @endif">
								(<span class='rupee-font'>&#8377; </span><span id="pcProductPriceOff" data-price="{{ $data['package']->price_worth - $data['package']->price }}">@amount($data['package']->price_worth - $data['package']->price)</span> OFF)
							</div>
						@endif
					@endif
					<div class="mar-l-5 product-main-price in-blk">
						<input type="hidden" id="pcProductPrice" value="{{ $data['package']->price }}">
						<span class='rupee-font'>&#8377; </span><span id="pcBookingPrice" data-price="{{ $data['package']->price }}">@amount($data['package']->price)</span>
						@if ($data['package']->price_max && $data['package']->price_max > $data['package']->price)
							- @price($data['package']->price_max)
						@endif
					</div>
					@if($data['package']->type_ticket_id != config('evibe.ticket.type.surprises'))
						<div class="product-price-tag mar-l-5 in-blk">
							<i class="glyphicon glyphicon-thumbs-up"></i> Best Price
						</div>
					@endif
					<div class="clearfix"></div>
					@if ($data['package']->price_per_extra_guest)
						<span class="product-price-per-guest mar-l-2 mar-t-5 hide">
					(<span class='rupee-font'>&#8377; </span><span id="pcPricePerExtraGuest" data-value="{{ $data['package']->price_per_extra_guest }}">@amount($data['package']->price_per_extra_guest)</span> / extra guest)
				</span>
					@endif
					<div class="product-price-msg mar-t-5">
						@if(!$data['package']->propertyType)
							<span class="product-price-msg">
						(price may vary based on <b>party location</b>)
					</span>
						@endif
						@if(isset($data['tags']) && count($data['tags']) && isset($data['tags'][config('evibe.surprise-relation-tags.candle-light-dinner')]))
							<div class="product-price-msg">
								(price is <b>inclusive</b> of all taxes)
							</div>
						@endif
					</div>
				</div>
				@if($data['package']->type_ticket_id == config('evibe.ticket.type.surprises'))
					<div class="in-blk valign-top mar-l-15">
						<div class="product-lowest-price-img-wrap">
							<img src="{{ $galleryUrl }}/main/img/icons/lowest-price.png">
						</div>
					</div>
				@endif
			</div>
			<div class="mar-t-5">
				<span class="add-ons-price-wrap no-mar-l">
					<b><span class="add-ons-selected-count"></span></b> add-ons selected - <b><span><span class="rupee-font">&#8377; </span><span id="pcAddOnsPrice" data-price="0">0</span></span></b>
				</span>
			</div>
		</div>
		<div class="product-price-loading-wrap text-left hide">
			<img src="/images/loading.gif" alt="">
		</div>
		<div class="product-input mar-t-10">
			@include('occasion.util.profile.c_components.package.booking-input-section')
		</div>
		<div class="product-cta">
			<div>
				<div class="mar-t-15 mar-b-15 hide">
					<a class="btn btn-default product-btn-book-now ga-btn-book-now scroll-item" data-toggle="modal" data-target="#bookNowModal">
						<i class="material-icons valign-mid">&#xE8CC;</i>
						<span class="valign-mid mar-l-4">Book Now</span>
					</a>
				</div>
				@if(isset($data['mapTypeId']) && ($data['mapTypeId'] == config('evibe.ticket.type.surprises')))
					<div class="mar-t-15 no-pad-l no-pad-r">
						<a id="" class="btn btn-default product-btn-shortlist mobile-profile-enquire-button ga-btn-enquire-now popup-need-help-btn ga-mobile-profile-enquire-button">
							<i class="glyphicon glyphicon-envelope product-shortlist-icon"></i>
							<span class="mar-l-2">Enquire Now</span>
						</a>
					</div>
				@endif
				@if(isset($data['mapTypeId']) && (($data['mapTypeId'] != config('evibe.ticket.type.surprises')) && ($data['mapTypeId'] != config('evibe.ticket.type.villas'))))
					<div class="col-xs-6 mar-t-15 no-pad-l">
						<div class="">
							<a id="shortlistBtn" class="btn btn-default product-btn-shortlist" v-on:click="shortlistOptionFromProfilePage">
								<span class="hideShortlistData" data-cityId="{{ $data['shortlistData']['cityId'] }}" data-occasionId="{{ $data['shortlistData']['occasionId'] }}" data-mapId="{{ $data['shortlistData']['mapId'] }}" data-mapTypeId="{{ $data['shortlistData']['mapTypeId'] }}" data-urlAdd="{{ route('partybag.add') }}" data-urlDelete="{{ route('partybag.delete') }}" data-plainImgWhite="{{ $galleryUrl }}/main/img/icons/party-bag_empty_w.png" data-removeImgGrey="{{ $galleryUrl }}/main/img/icons/party-bag_empty_g.png"></span>
								<i class="icon-bagicon5 product-shortlist-icon"></i>
							<!--<img src="{{ $galleryUrl }}/main/img/icons/party-bag_empty_w.png" alt="Party Bag List" class="partybag-detail-img">-->
								<span class="mar-l-2">Add To Cart</span>
							</a>
						</div>
					</div>
					<div class="col-xs-6 mar-t-15 no-pad-l no-pad-r">
						<a id="" class="btn btn-default product-btn-shortlist mobile-profile-enquire-button ga-btn-enquire-now popup-need-help-btn ga-mobile-profile-enquire-button">
							<i class="glyphicon glyphicon-envelope product-shortlist-icon"></i>
							<span class="mar-l-2">Enquire Now</span>
						</a>
					</div>
					<div class="col-xs-5 mar-t-15 no-pad-r text-center font-16 hide" style="border: solid 1px #D4D5D9; padding: 8px 0">
						<div data-city-id="{{ $data['shortlistData']['cityId'] }}" data-occasion-id="{{ $data['shortlistData']['occasionId'] }}" data-map-id="{{ $data['shortlistData']['mapId'] }}" data-map-type-id="{{ $data['shortlistData']['mapTypeId'] }}">
							<a class="wishlist-option fw-normal a-no-decoration-black" data-url="{{ route("wishlist.add") }}" data-status-url="{{ route("wishlist.status") }}"><i class="glyphicon glyphicon-bookmark" style="top: 2px;"></i> WISHLIST</a>
							<a class="wishlisted-option fw-normal a-no-decoration-black hide a-no-decoration-default"><i class="glyphicon glyphicon-bookmark" style="top: 2px;"></i> WISHLISTED</a>
						</div>
					</div>
					<div class="clearfix"></div>
				@endif
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>