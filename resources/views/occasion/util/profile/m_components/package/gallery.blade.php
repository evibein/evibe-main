<div class="loading-img-wrap no-mar-t">
	<div class="animated-background" style="min-height: 210px; margin: 15px;"></div>
</div>
<div class="gallery-wrap hide mobile-gallery-wrap" role="tabpanel">
	@if ($data['gallery']['showGalleryTabs'])
		<ul class="nav nav-pills mar-l-20" role="tablist">
			<?php $count = 0; ?>
			@foreach($data['gallery']['images'] as $cat => $catImages)
				<li role="presentation" class="@if(!$count++) active @endif">
					<a href="#catGalId{{ $count }}" data-toggle="pill">
						{{ $cat }}
					</a>
				</li>
			@endforeach
		</ul>
	@endif
	@if (count($data['gallery']['images']))
		<div class="tab-content mar-t-15"> <!-- tab panes begin -->
			@php
				$count = 0;
				$explainerImage = false;
			@endphp
			@foreach($data['gallery']['images'] as $cat => $catImages)
				<div role="tabpanel" class="tab-pane fade @if(!$count++) in active @endif" id="catGalId{{ $count }}">
					@if (count($catImages))
						<div id="fotorama" data-width="100%" data-ratio="900/600" data-maxheight="400" data-autoplay="true" data-thumbwidth="80" data-thumbmargin="10" data-keyboard="true" data-captions="true" @if($data['gallery']['showThumb']) data-enableifsingleframe="true" @endif>
							@foreach ($catImages as $key => $image)
								<a href="{{ $image['url'] }}">
									<img src="{{ $image['thumb'] }}" alt="{{ $image['title'] }} in {{ getCityUrl() }}"/>
								</a>
							@endforeach
							@if(!$explainerImage)
								<a href="{{ $galleryUrl }}/main/img/explainer-image_profile.png">
									<img src="{{ $galleryUrl }}/main/img/explainer-image_thumb.png" alt="Lifelong Memories Guaranteed"/>
								</a>
								@php $explainerImage = true; @endphp
							@endif
						</div>
					@else
						<i class="text-danger">No images found in this category.</i>
					@endif
				</div>
			@endforeach
		</div>
	@else
		<div><img class="mob-profile-no-image" src="{{ $galleryUrl }}/img/icons/gift.png"></div>
	@endif
</div>