<div class="product-description-section">
	<div class="product-description-wrap mar-t-20">

		<div class="hide">
			<div class="scroll-anchor"></div>
			<div class="mob-pro-into-section">
				<div class="mar-l-5 mar-r-5 product-info-tabs">
					@if ($data['package']->about_package)
						<a class="scroll-item btn btn-default mar-5" data-scroll="aboutPackage">@if($data['package']->map_type_id == config('evibe.ticket.type.venue')) About Venue @else About Experience @endif</a>
					@endif
					@if ($data['package']->info || (isset($data['highlights']) && count($data['highlights'])) || (isset($data['addOns']) && count($data['addOns'])))
						<a class="scroll-item btn btn-default mar-5" data-scroll="inclusions">Inclusions</a>
					@endif
					@if(isset($data['occasionId']) && ($data['occasionId'] == config('evibe.occasion.surprises.id')))
						<a class="scroll-item btn btn-default mar-5" data-scroll="faqs">FAQs</a>
					@endif
					@if ($data['package']->prerequisites || $data['package']->facts)
						@if(isset($data['occasionId']) && ($data['occasionId'] == config('evibe.occasion.surprises.id')) && isset($data['tags']) && count($data['tags']) && !isset($data['tags'][config('evibe.surprise-relation-tags.candle-light-dinner')]))

						@else
							<a class="scroll-item btn btn-default mar-5" data-scroll="thingsToRemember">Things To Remember</a>
						@endif
					@endif
					@if($data['ratings']['total']['count'])
						<a class="scroll-item btn btn-default mar-5" data-scroll="productReviews">Customer Reviews</a>
					@endif
					@if(isset($data['addOns']) && count($data['addOns']))
						<a class="scroll-item btn btn-default mar-5" data-scroll="addOns">Add Ons</a>
					@endif
					@if ($data['package']->terms)
						<a class="scroll-item btn btn-default mar-5" data-scroll="terms">Terms</a>
					@endif
					@if(isset($data['cancellationData']) && !is_null($data['cancellationData']))
						<a class="scroll-item btn btn-default mar-5" data-scroll="cancellationsAndRefunds">Cancellations & Refunds</a>
					@endif
				</div>
			</div>
			<div class="scroll-empty-div"></div>
		</div>
		<div class="product-description-content">
			<div class="" id="tabs">
				@if ($data['package']->about_package)
					<div class="tab-pane product-description-tab mar-t-10 mar-l-10 mar-r-15" id="aboutPackage">
						<div class="product-description-tab-title">
							@if($data['package']->map_type_id == config('evibe.ticket.type.venue')) About Venue @else About Experience @endif
						</div>
						{!! $data['package']->about_package !!}
					</div>
				@endif
				<div class="tab-pane product-description-tab mar-t-10 mar-l-10 mar-r-15" id="highlights">
					<div class="product-description-tab-title">
						Highlights
					</div>
					<div class="product-description-highlights-wrap">
						<ul>
							@if($data['package']->package_tag_line && isset($data['occasionId']) && ($data['occasionId'] == config('evibe.occasion.surprises.id')))
								<li>
									{{ $data['package']->package_tag_line }}
								</li>
							@endif
							@if ($data['package']->time_setup)
								<li>Setup Time: {{ $data['package']->time_setup }} Hrs</li>
							@endif
							@if((isset($data['mapTypeId']) && $data['mapTypeId'] != config('evibe.ticket.type.food')) &&
								((isset($data['occasionId'])) && ($data['occasionId'] != config('evibe.occasion.surprises.id'))))
								@if ($data['package']->time_duration > 0)
									<li>Rent Duration: {{ $data['package']->time_duration }} Hrs</li>
								@endif
							@endif
							@if(isset($data['mapTypeId']) && $data['mapTypeId'] != config('evibe.ticket.type.food'))
								@if($data['package']->kms_free || $data['package']->kms_max)
									<li>
										<span>Transport: </span>
										<span>
											@if (!$data['package']->kms_free && $data['package']->trans_min && $data['package']->kms_max)
												@price($data['package']->trans_min)
												@if ($data['package']->trans_max && $data['package']->trans_max > $data['package']->trans_min)
													<span class="mar-l-2 mar-r-2">-</span> @price($data['package']->trans_max)
												@endif
												<span class="mar-l-2"> up to {{ $data['package']->kms_max }} KMs</span>
											@elseif ($data['package']->kms_free)
												@if (!$data['package']->trans_min)
													Free up to {{ max($data['package']->kms_free, $data['package']->kms_max) }} KMs
												@else
													@if ($data['package']->kms_free == $data['package']->kms_max)
														@price($data['package']->trans_min)
														@if ($data['package']->trans_max && $data['package']->trans_max > $data['package']->trans_min)
															<span class="mar-l-2 mar-r-2">-</span> @price($data['package']->trans_max)
														@endif
														<span class="mar-l-2">for max. {{ $data['package']->kms_max }} KMs</span>
													@elseif ($data['package']->kms_free < $data['package']->kms_max)
														Free up to {{ $data['package']->kms_free }} KMs
														<span class="mar-l-2">
															(add @price($data['package']->trans_min)
															@if ($data['package']->trans_max && $data['package']->trans_max > $data['package']->trans_min)
																<span class="mar-l-2 mar-r-2">-</span> @price($data['package']->trans_max)
															@endif
															<span class="mar-l-2">for max. {{ $data['package']->kms_max }} KMs)</span>
														</span>
													@else
													@endif
												@endif
											@else
											@endif
										</span>
									</li>
								@endif
							@endif
							@if($data['package']->propertyType)
								@if ($data['package']->area)
									<li>
										<div>
											Located at {{ $data['package']->area->name }}
											@if( $data['package']->landmark)
												<span class="mar-l-5">({{ $data['package']->landmark }})</span>
											@endif
										</div>
										@if(isset($data['occasionId']) &&
										 ($data['occasionId'] == config('evibe.occasion.surprises.id')) &&
										 ($data['package']->map_type_id == config('evibe.ticket.type.venue')) &&
										  $data['package']->area->zip_code)
											<div class="btn btn-link font-12 no-pad btn-init-dist-check">(check distance from your location)</div>
										@endif
										@if(isset($data['occasionId']) && ($data['occasionId'] == config('evibe.occasion.surprises.id')) &&
											($data['package']->map_type_id == config('evibe.ticket.type.venue')))
											<div class="text-center pad-b-10 dist-check-wrap pad-r-5 hide">
												<div class="dist-check-action-wrap">
													<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-pad-r">
														<div class="in-blk dist-check-input-wrap">
															<input type="number" name="distCheckLocationPin" id="distCheckLocationPin" class="dist-check-input" placeholder="Enter Your Pincode">
														</div>
													</div>
													<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 mar-b-5">
														<div class="in-blk dist-check-btn-wrap">
															<div class="btn btn-primary btn-sm dis-check-btn" data-url="{{ route('ajax.profile.calculate-distance') }}" data-pin="{{ $data['package']->area->zip_code }}">Check</div>
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="product-dist-check-msg mar-t-5 hide">
													This venue is approximately
													<span class="text-bold mar-l-3"><span class="dist-check-number"></span> kms</span> from the entered location
												</div>
												<div class="product-dist-check-error mar-t-5 hide text-r">

												</div>
											</div>
										@endif
									</li>
								@endif
								@if($data['package']->propertyType->identifier || $data['package']->table_setup)
									<li>
										@if(isset($data['isCLD']) && $data['isCLD'])
											<span class="mar-r-5">Experience setup:</span>
										@endif
										@if($data['package']->table_setup)
											<span>{{ $data['package']->table_setup }} </span>
										@endif
										@if($data['package']->propertyType->identifier)
											<span>At {{ $data['package']->propertyType->identifier }} </span>
										@endif
									</li>
								@endif
								@if(isset($data['isCLD']) && $data['isCLD'])
									<li>
										Limited reservations
									</li>
								@endif
								@if($data['package']->check_in && $data['package']->check_out && isset($data['tags']) && count($data['tags']) && isset($data['tags'][config('evibe.surprise-relation-tags.candle-light-dinner')]))
									<li>
										Experience is for 2 hours
									</li>
								@else
									@if($data['package']->check_in)
										<li>Check In: {{ $data['package']->check_in }}</li>
									@endif
									@if($data['package']->check_out)
										<li>Check Out: {{ $data['package']->check_out }}</li>
									@endif
								@endif
							@endif
							@if(isset($data['highlights']) && count($data['highlights']))
								@foreach($data['highlights'] as $highlightItem)
									<li>{{ $highlightItem['name'] }}: {{ $highlightItem['value'] }}</li>
								@endforeach
							@endif
							@if($data['package']->code)
								<li>
									<span>
										@if(isset($data['isCLD']) && $data['isCLD'])
											Experience Code:
										@else
											Package Code:
										@endif
									</span>
									<span class="mar-l-5">
										{{ $data['package']->code }}
									</span>
								</li>
							@endif
							@if(isset($data['isCLD']) && $data['isCLD'])
								<li>
									<span class="mar-r-5">For enquiries, contact hotel/restaurant:</span>
									<span>{{ config('evibe.contact.company.vday_cld') }}</span>
								</li>
							@endif
						</ul>
					</div>
				</div>
				<div class="tab-pane product-description-tab mar-t-10 mar-l-10 mar-r-15" id="inclusions">
					<div class="product-description-tab-title">
						Package Inclusions
					</div>
					<div class="product-highlights-wrap mar-b-15 hide">
						@if($data['package']->code)
							<div class="product-highlight-item">
								<div class="product-highlight-item-title in-blk">
									<i class="glyphicon glyphicon-th-list"></i> Product Code:
								</div>
								<div class="product-highlight-item-text in-blk mar-l-5">{{ $data['package']->code }}</div>
								<div class="clearfix"></div>
							</div>
						@endif
						@if($data['package']->package_tag_line && isset($data['occasionId']) && ($data['occasionId'] == config('evibe.occasion.surprises.id')))
							<div class="product-highlight-item">
								<div class="product-highlight-item-title in-blk">
									<i class="glyphicon glyphicon-list-alt"></i> Product Highlights:
								</div>
								<div class="product-highlight-item-text in-blk mar-l-5">{{ $data['package']->package_tag_line }}</div>
								<div class="clearfix"></div>
							</div>
						@endif
						@if ($data['package']->time_setup)
							<div class="product-highlight-item">
								<div class="product-highlight-item-title in-blk">
									<i class="glyphicon glyphicon-time"></i> Setup Time:
								</div>
								<div class="product-highlight-item-text in-blk mar-l-5">
									{{ $data['package']->time_setup }} Hrs
								</div>
								<div class="clearfix"></div>
							</div>
						@endif
						@if((isset($data['mapTypeId']) && $data['mapTypeId'] != config('evibe.ticket.type.food')) &&
						((isset($data['occasionId'])) && ($data['occasionId'] != config('evibe.occasion.surprises.id'))))
							@if ($data['package']->time_duration > 0)
								<div class="product-highlight-item">
									<div class="product-highlight-item-title in-blk">
										<i class="glyphicon glyphicon-time"></i> Rent Duration:
									</div>
									<div class="product-highlight-item-text in-blk mar-l-5">
										{{ $data['package']->time_duration }} Hrs
									</div>
									<div class="clearfix"></div>
								</div>
							@endif
						@endif
						@if(isset($data['mapTypeId']) && $data['mapTypeId'] != config('evibe.ticket.type.food'))
							@if ($data['package']->area)
								<div class="product-highlight-item hide">
									<div class="product-highlight-item-title in-blk">
										<i class="glyphicon glyphicon-map-marker"></i> Location:
									</div>
									<div class="product-highlight-item-text in-blk mar-l-5">
										{{ $data['package']->area->name }}
									</div>
									@if( $data['package']->landmark)
										<div class="highlights-landmark">({{ $data['package']->landmark }})</div>
									@endif
									<div class="clearfix"></div>
								</div>
							@endif
							@if ($data['package']->kms_free || $data['package']->kms_max)
								<div class="product-highlight-item hide">
									<div class="product-highlight-item-title in-blk">
										<i class="glyphicon glyphicon-retweet"></i> Transport:
									</div>
									<div class="product-highlight-item-text in-blk mar-l-5">
										@if (!$data['package']->kms_free && $data['package']->trans_min && $data['package']->kms_max)
											@price($data['package']->trans_min)
											@if ($data['package']->trans_max && $data['package']->trans_max > $data['package']->trans_min)
												<span class="mar-l-2 mar-r-2">-</span> @price($data['package']->trans_max)
											@endif
											<span class="mar-l-2"> up to {{ $data['package']->kms_max }} KMs</span>
										@elseif ($data['package']->kms_free)
											@if (!$data['package']->trans_min)
												Free up to {{ max($data['package']->kms_free, $data['package']->kms_max) }} KMs
											@else
												@if ($data['package']->kms_free == $data['package']->kms_max)
													@price($data['package']->trans_min)
													@if ($data['package']->trans_max && $data['package']->trans_max > $data['package']->trans_min)
														<span class="mar-l-2 mar-r-2">-</span> @price($data['package']->trans_max)
													@endif
													<span class="mar-l-2">for max. {{ $data['package']->kms_max }} KMs</span>
												@elseif ($data['package']->kms_free < $data['package']->kms_max)
													Free up to {{ $data['package']->kms_free }} KMs
													<span class="mar-l-2">
															(add @price($data['package']->trans_min)
														@if ($data['package']->trans_max && $data['package']->trans_max > $data['package']->trans_min)
															<span class="mar-l-2 mar-r-2">-</span> @price($data['package']->trans_max)
														@endif
														<span class="mar-l-2">for max. {{ $data['package']->kms_max }} KMs)</span>
														</span>
												@else
												@endif
											@endif
										@else
										@endif
									</div>
									<div class="clearfix"></div>
								</div>
							@endif
						@endif
						@if($data['package']->propertyType)
							@if ($data['package']->area)
								<div class="product-highlight-item">
									<div class="product-highlight-item-title in-blk">
										<i class="glyphicon glyphicon-map-marker"></i> Location:
									</div>
									<div class="product-highlight-item-text in-blk mar-l-5">
										{{ $data['package']->area->name }}
									</div>
									@if( $data['package']->landmark)
										<div class="highlights-landmark">({{ $data['package']->landmark }})</div>
									@endif
									<div class="clearfix"></div>
									@if(isset($data['occasionId']) &&
									 ($data['occasionId'] == config('evibe.occasion.surprises.id')) &&
									 ($data['package']->map_type_id == config('evibe.ticket.type.venue')) &&
									  $data['package']->area->zip_code)
										<div class="btn btn-link font-12 no-pad btn-init-dist-check">(check distance from your location)</div>
									@endif
								</div>
								@if(isset($data['occasionId']) && ($data['occasionId'] == config('evibe.occasion.surprises.id')) &&
								($data['package']->map_type_id == config('evibe.ticket.type.venue')))
									<div class="text-center pad-b-10 dist-check-wrap hide">
										<div class="dist-check-action-wrap">
											<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-pad-r">
												<div class="in-blk dist-check-input-wrap">
													<input type="number" name="distCheckLocationPin" id="distCheckLocationPin" class="dist-check-input" placeholder="Enter Your Pincode">
												</div>
											</div>
											<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 mar-b-5">
												<div class="in-blk dist-check-btn-wrap">
													<div class="btn btn-primary btn-sm dis-check-btn" data-url="{{ route('ajax.profile.calculate-distance') }}" data-pin="{{ $data['package']->area->zip_code }}">Check</div>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="product-dist-check-msg mar-t-5 hide">
											This venue is approximately
											<span class="text-bold mar-l-3"><span class="dist-check-number"></span> kms</span> from the entered location
										</div>
										<div class="product-dist-check-error mar-t-5 hide text-r">

										</div>
									</div>
								@endif
							@endif
							@if ($data['package']->propertyType->identifier)
								<div class="product-highlight-item">
									<div class="product-highlight-item-title in-blk">
										<i class="glyphicon glyphicon-home"></i> Property Type:
									</div>
									<div class="product-highlight-item-text in-blk mar-l-5">
										{{ $data['package']->propertyType->identifier }}
									</div>
									<div class="clearfix"></div>
								</div>
							@endif
							@if ($data['package']->table_setup)
								<div class="product-highlight-item">
									<div class="product-highlight-item-title in-blk">
										<i class="glyphicon glyphicon-modal-window"></i> Table Setup:
									</div>
									<div class="product-highlight-item-text in-blk mar-l-5">
										{{ $data['package']->table_setup }}
									</div>
									<div class="clearfix"></div>
								</div>
							@endif
							@if($data['package']->check_in && $data['package']->check_out && isset($data['tags']) && count($data['tags']) && isset($data['tags'][config('evibe.surprise-relation-tags.candle-light-dinner')]))
								<div class="product-highlight-item">
									<div class="product-highlight-item-title in-blk">
										<i class="glyphicon glyphicon-time"></i> Timings:
									</div>
									<div class="product-highlight-item-text in-blk mar-l-5">
										@if(strpos($data['package']->check_out, '(Next day)') !== false)
											{{ $data['package']->check_in }} - {{ $data['package']->check_out }}
										@else
											7 PM - 11 PM [Choose any 2 hours]
										@endif
									</div>
									<div class="clearfix"></div>
								</div>
							@else
								@if ($data['package']->check_in)
									<div class="product-highlight-item">
										<div class="product-highlight-item-title in-blk">
											<i class="glyphicon glyphicon-time"></i> Check In:
										</div>
										<div class="product-highlight-item-text in-blk mar-l-5">
											{{ $data['package']->check_in }}
										</div>
										<div class="clearfix"></div>
									</div>
								@endif
								@if ($data['package']->check_out)
									<div class="product-highlight-item">
										<div class="product-highlight-item-title in-blk">
											<i class="glyphicon glyphicon-time"></i> Check Out:
										</div>
										<div class="product-highlight-item-text in-blk mar-l-5">
											{{ $data['package']->check_out }}
										</div>
										<div class="clearfix"></div>
									</div>
								@endif
							@endif
						@endif
						@if($data['package']->book_before_hrs)
							<div class="product-highlight-item">
								<div class="product-highlight-item-title in-blk">
									<i class="glyphicon glyphicon-dashboard"></i> Book Before:
								</div>
								<div class="product-highlight-item-text in-blk mar-l-5 text-lower-imp">
									@if($data['package']->book_before_hrs > 24) {{ round($data['package']->book_before_hrs / 24) + 1 }} Days @else {{ $data['package']->book_before_hrs }} Hrs @endif in advance to your event
								</div>
								<div class="clearfix"></div>
							</div>
						@endif
						@if (isset($data['highlights']) && count($data['highlights']))
							@foreach($data['highlights'] as $highlightItem)
								<div class="product-highlight-item">
									<div class="product-highlight-item-title in-blk">
										<i class="{{ $highlightItem['icon'] }}"></i> {{ $highlightItem['name'] }}:
									</div>
									<div class="product-highlight-item-text in-blk mar-l-5">
										{{ $highlightItem['value'] }} Hrs
									</div>
									<div class="clearfix"></div>
								</div>
							@endforeach
						@endif
					</div>
					@if ($data['package']->info)
						{!! $data['package']->info !!}
					@endif
					<div class="text-center pad-t-10 pad-b-5">
						@include('app.social-share-plugin')
					</div>
				</div>
				@if(isset($data['addOns']) && count($data['addOns']))
					<div class="tab-pane product-description-tab mar-t-10 mar-l-10 mar-r-15" id="addOns">
						@include('occasion.util.add-ons.list', ['addOns' => $data['addOns'], 'occasionId' => $data['occasionId']])
					</div>
				@endif
				@if(isset($data['occasionId']) && ($data['occasionId'] == config('evibe.occasion.surprises.id')))
					<div class="tab-pane product-description-tab mar-t-10 mar-l-10 mar-r-15" id="faqs">
						<div class="product-description-tab-title">
							Frequently Asked Questions
						</div>
						<div class="pad-10">
							@include('occasion.util.faq.list.m')
						</div>
					</div>
				@endif
				@if ($data['package']->prerequisites || $data['package']->facts)
					@if(isset($data['occasionId']) && ($data['occasionId'] == config('evibe.occasion.surprises.id')) && isset($data['tags']) && count($data['tags']) && !isset($data['tags'][config('evibe.surprise-relation-tags.candle-light-dinner')]))

					@else
						<div class="tab-pane product-description-tab mar-t-10 mar-l-10 mar-r-15" id="thingsToRemember">
							<div class="product-description-tab-title">
								Things To Remember
							</div>
							@if($data['package']->prerequisites)
								{!! $data['package']->prerequisites !!}
							@endif
							@if($data['package']->facts)
								{!! $data['package']->facts !!}
							@endif
						</div>
					@endif
				@endif
				<div class="tab-pane product-description-tab pad-l-15 pad-r-15 reviews-card" id="productReviews">
					@if(isset($data['deliveryImages']) && (count($data['deliveryImages'])))
						@include("occasion.util.profile.c_components.customer-images",[
							"deliveryImages" => $data['deliveryImages'],
						])

					@endif
					@include("app.review.partial", [
				"reviews"=> $data["ratings"],
				"isShowLocation" => ($data["package"]->map_type_id == config("evibe.ticket.type.venue")) ? false : true,
				"showAllReviewsUrl" => route("provider-reviews:all", [
									getSlugFromText($data["package"]->name),
									($data["package"]->map_type_id == config("evibe.ticket.type.venue") ? config("evibe.ticket.type.venue") : config("evibe.ticket.type.planner")),
									$data["package"]->provider->id
								])
			])
				</div>
				@if ($data['package']->terms)
					<div class="tab-pane product-description-tab mar-t-10 mar-l-10 mar-r-15" id="terms">
						<div class="product-description-tab-title">
							Terms Of Booking
						</div>
						{!! $data['package']->terms !!}
					</div>
				@endif
				@if(isset($data['cancellationData']) && !is_null($data['cancellationData']))
					@include('occasion.util.profile.m_components.cancellation-policy')
				@endif
			</div>
		</div>
		<div class="mar-t-30 mar-b-30 text-center">
			<span>Need help?</span>
			<span><a class="product-link-enquire-now ga-btn-enquire-now popup-need-help-btn mar-l-5 ga-mobile-profile-enquire-link">Enquire Now</a></span>
		</div>
		@include('app.modals.auto-popup',
			[
			"cityId" => $data['cityId'],
			"occasionId" => $data['occasionId'],
			"mapTypeId" => $data['mapTypeId'],
			"mapId" => $data['package']['id'],
			"optionName" => $data['package']['name'],
			"countries" => (isset($data['countries']) && count($data['countries'])) ? $data['countries'] : []
			])
	</div>
</div>