<div class="product-description-section">
	<div class="product-description-wrap mar-t-20">
		<div class="hide">
			<div class="mar-l-5 mar-r-5 product-info-tabs">
				@if ($data['decor']->info || (isset($data['highlights']) && count($data['highlights'])) || (isset($data['addOns']) && count($data['addOns'])))
					<a class="scroll-item btn btn-default mar-5" data-scroll="inclusions">Inclusions</a>
				@endif
				@if ($data['decor']->more_info || $data['decor']->facts)
					<a class="scroll-item btn btn-default mar-5" data-scroll="thingsToRemember">Things To Remember</a>
				@endif
				@if($data['ratings']['total']['count'])
					<a class="scroll-item btn btn-default mar-5" data-scroll="productReviews">Customer Reviews</a>
				@endif
				@if(isset($data['addOns']) && count($data['addOns']))
					<a class="scroll-item btn btn-default mar-5" data-scroll="addOns">Add Ons</a>
				@endif
				@if ($data['decor']->terms)
					<a class="scroll-item btn btn-default mar-5" data-scroll="terms">Terms</a>
				@endif
				@if(isset($data['cancellationData']) && !is_null($data['cancellationData']))
					<a class="scroll-item btn btn-default mar-5" data-scroll="cancellationsAndRefunds">Cancellations & Refunds</a>
				@endif
			</div>
		</div>
		<div class="product-description-content">
			<div class="" id="tabs">
				<div class="tab-pane product-description-tab mar-t-10 mar-l-10 mar-r-15" id="inclusions">
					<div class="product-description-tab-title">
						Decor Inclusions
					</div>
					<div class="product-highlights-wrap mar-b-15">
						@if($data['decor']->code)
							<div class="product-highlight-item">
								<div class="product-highlight-item-title in-blk">
									<i class="glyphicon glyphicon-th-list"></i> Style Code:
								</div>
								<div class="product-highlight-item-text in-blk mar-l-5">{{ $data['decor']->code }}</div>
								<div class="clearfix"></div>
							</div>
						@endif
						@if ($data['decor']->time_setup)
							<div class="product-highlight-item">
								<div class="product-highlight-item-title in-blk">
									<i class="glyphicon glyphicon-time"></i> Style Setup Time:
								</div>
								<div class="product-highlight-item-text in-blk mar-l-5">
									{{ $data['decor']->time_setup }} Hrs
								</div>
								<div class="clearfix"></div>
							</div>
						@endif
						@if ($data['decor']->time_duration > 0 && (isset($occasionId) && $occasionId != config("evibe.occasion.house-warming.id")))
							<div class="product-highlight-item">
								<div class="product-highlight-item-title in-blk">
									<i class="glyphicon glyphicon-time"></i> Style Rent Duration:
								</div>
								<div class="product-highlight-item-text in-blk mar-l-5">
									{{ $data['decor']->time_duration }} Hrs
									<small>(not applicable for balloons, flowers)</small>
								</div>
								<div class="clearfix"></div>
							</div>
						@endif
					</div>
					@if ($data['decor']->info)
						{!! $data['decor']->info !!}
					@endif
					<div class="text-center pad-t-10 pad-b-5">
						@include('app.social-share-plugin')
					</div>
				</div>
				@if(isset($data['addOns']) && count($data['addOns']))
					<div class="tab-pane product-description-tab mar-t-10 mar-l-10 mar-r-15" id="addOns">
						@include('occasion.util.add-ons.list', ['addOns' => $data['addOns']])
					</div>
				@endif
				@if ($data['decor']->more_info || $data['decor']->facts)
					<div class="tab-pane product-description-tab mar-t-10 mar-l-10 mar-r-15" id="thingsToRemember">
						<div class="product-description-tab-title">
							Things To Remember
						</div>
						@if($data['decor']->more_info)
							{!! $data['decor']->more_info !!}
						@endif
						@if($data['decor']->facts)
							{!! $data['decor']->facts !!}
						@endif
					</div>
				@endif
				<div class="tab-pane product-description-tab pad-l-15 pad-r-15 reviews-card" id="productReviews">
					@if(isset($data['deliveryImages']) && (count($data['deliveryImages'])))
						@include("occasion.util.profile.c_components.customer-images",[
							"deliveryImages" => $data['deliveryImages'],
						])

					@endif
					@include("app.review.partial", [
				"reviews"=> $data["ratings"],
				"showAllReviewsUrl" => route("provider-reviews:all", [
					getSlugFromText($data["decor"]->name),
					config("evibe.ticket.type.planner"),
					$data["decor"]->provider->id
				])
			])
				</div>
				@if ($data['decor']->terms)
					<div class="tab-pane product-description-tab mar-t-10 mar-l-10 mar-r-15" id="terms">
						<div class="product-description-tab-title">
							Terms Of Booking
						</div>
						{!! $data['decor']->terms !!}
					</div>
				@endif
				@if(isset($data['cancellationData']) && !is_null($data['cancellationData']))
					@include('occasion.util.profile.m_components.cancellation-policy')
				@endif
			</div>
		</div>
		<div class="mar-t-30 mar-b-30 text-center">
			<span>Need help?</span>
			<span><a class="product-link-enquire-now ga-btn-enquire-now popup-need-help-btn mar-l-5 ga-mobile-profile-enquire-link">Enquire Now</a></span>
		</div>
		@include('app.modals.auto-popup',
			[
			"cityId" => $data['cityId'],
			"occasionId" => $data['occasionId'],
			"mapTypeId" => $data['mapTypeId'],
			"mapId" => $data['decor']['id'],
			"optionName" => $data['decor']['name'],
			"countries" => (isset($data['countries']) && count($data['countries'])) ? $data['countries'] : []
			])
	</div>
</div>

