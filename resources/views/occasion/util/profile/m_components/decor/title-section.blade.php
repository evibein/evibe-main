<div class="title-section">
	<div class="title-section-wrap mar-t-5">
		<h1 class="product-title-text no-mar-b no-mar-t ls-normal">
			{{ $data['decor']->name }}
		</h1>
		<div class="product-rating @if(!($data['ratings']['total']['count'])) hide @endif">
			<div class="font-16">
				<div class="in-blk">
					<input type="number" class="avg-rating hide" value="{{ $data['ratings']['total']['avg'] }}" title="{{ $data['ratings']['total']['avg'] }} average rating for provider of {{ $data['decor']->name }}"/>
				</div>
				@if($data['ratings']['total']['count'])
					<div id="reviews" class="product-rating-reviews scroll-item cur-point in-blk scroll-to-reviews" data-scroll="reviews"> ({{ $data['ratings']['total']['count'] }} reviews)
					</div>
				@else
					<div class="product-rating-reviews cur-point in-blk"> (0 reviews)</div>
				@endif
			</div>
		</div>
		@include("occasion.util.profile.d_components.product-variations")
		<div class="product-price mar-t-10">
			@if ($data['decor']->worth && $data['decor']->worth > $data['decor']->min_price)
				<div class="product-price-worth in-blk mar-l-2">
					@price($data['decor']->worth)
				</div>
			@endif
			@if($data['decor']->min_price && $data['decor']->worth)
				<div class="product-price-off mar-l-5 in-blk">
					(@offPrice($data['decor']->min_price, $data['decor']->worth) OFF)
				</div>
			@endif
			<div class="product-main-price mar-l-5 in-blk">
				@price($data['decor']->min_price)
				@if ($data['decor']->max_price && $data['decor']->max_price > $data['decor']->min_price)
					- @price($data['decor']->max_price)
				@endif
				@if ($data['decor']->range_info)
					<i class="glyphicon glyphicon-info-sign product-price-info font-12" data-toggle="tooltip" data-placement="left" title="{{ $data['decor']->range_info }}"></i>
				@endif
			</div>
			<div class="product-price-tag mar-l-5 in-blk">
				<i class="glyphicon glyphicon-thumbs-up"></i> Best Price
			</div>
			<div class="clearfix"></div>
			<div class="product-price-msg">
				(price may vary based on <b>party location</b>)
			</div>
			<div class="mar-t-5">
				<span class="add-ons-price-wrap no-mar-l">
					<b><span class="add-ons-selected-count"></span></b> add-ons selected - <b><span><span class="rupee-font">&#8377; </span><span class="add-ons-price">0</span></span></b>
				</span>
			</div>
		</div>
		<div class="product-cta mar-t-15">
			@if($data["occasionId"] == config("evibe.occasion.pre-post.id"))
				<div>
					<a class="btn btn-default product-btn-book-now ga-btn-book-now des-btn-pre-book-now">
						<i class="material-icons valign-mid">&#xE8CC;</i>
						<span class="valign-mid mar-l-4">Book Now</span>
					</a>
				</div>
			@else
				@if($data["occasionId"] == config("evibe.occasion.kids_birthdays.id"))
					<div>
						<a class="btn btn-default product-btn-book-now ga-btn-book-now des-btn-pre-book-now">
							<i class="material-icons valign-mid">&#xE8CC;</i>
							<span class="valign-mid mar-l-4">Book Now</span>
						</a>
					</div>
				@else
					<div>
						<a class="btn btn-default product-btn-book-now pre-check-book-now-button ga-btn-book-now">
							<i class="material-icons valign-mid">&#xE8CC;</i>
							<span class="valign-mid mar-l-4">Book Now</span>
						</a>
					</div>
				@endif
				<div class="mar-t-10 hide">
					<a class="btn btn-default product-btn-availability pre-check-feasibility-button ga-btn-availability">
						Check Availability
					</a>
				</div>
			@endif
			@if(isset($data['shortlistData']) && $data['shortlistData'])
				@if($data["occasionId"] != config("evibe.occasion.pre-post.id"))
					<div class="col-xs-6 mar-t-15 no-pad-l">
						<a id="shortlistBtn" class="btn btn-default product-btn-shortlist" @click="shortlistOptionFromProfilePage">
						<span class="hideShortlistData" data-cityId="{{ $data['shortlistData']['cityId'] }}" data-occasionId="{{ $data['shortlistData']['occasionId'] }}" data-mapId="{{ $data['shortlistData']['mapId'] }}" data-mapTypeId="{{ $data['shortlistData']['mapTypeId'] }}" data-urlAdd="{{ route('partybag.add') }}" data-urlDelete="{{ route('partybag.delete') }}" data-plainImgWhite="{{ $galleryUrl }}/main/img/icons/party-bag_empty_w.png" data-removeImgGrey="{{ $galleryUrl }}/main/img/icons/party-bag_empty_g.png">
						</span>
							<i class="icon-bagicon5 product-shortlist-icon"></i>
						<!--<img src="{{ $galleryUrl }}/main/img/icons/party-bag_empty_w.png" alt="Party Bag List" class="partybag-detail-img">-->
							<span class="mar-l-2">Add To Cart</span>
						</a>
					</div>
					<div class="col-xs-6 mar-t-15 no-pad-l no-pad-r">
						<a id="" class="btn btn-default product-btn-shortlist mobile-profile-enquire-button ga-btn-enquire-now popup-need-help-btn ga-mobile-profile-enquire-button">
							<i class="glyphicon glyphicon-envelope product-shortlist-icon"></i>
							<span class="mar-l-2">Enquire Now</span>
						</a>
					</div>
					<div class="col-xs-5 mar-t-15 no-pad-r text-center font-16 hide" style="border: solid 1px #D4D5D9; padding: 8px 0">
						<div data-city-id="{{ $data['shortlistData']['cityId'] }}" data-occasion-id="{{ $data['shortlistData']['occasionId'] }}" data-map-id="{{ $data['shortlistData']['mapId'] }}" data-map-type-id="{{ $data['shortlistData']['mapTypeId'] }}">
							<a class="wishlist-option fw-normal a-no-decoration-black" data-url="{{ route("wishlist.add") }}" data-status-url="{{ route("wishlist.status") }}"><i class="glyphicon glyphicon-bookmark" style="top: 2px;"></i> WISHLIST</a>
							<a class="wishlisted-option fw-normal a-no-decoration-black hide a-no-decoration-default"><i class="glyphicon glyphicon-bookmark" style="top: 2px;"></i> WISHLISTED</a>
						</div>
					</div>
					<div class="clearfix"></div>
				@endif
			@endif
		</div>
		<div class="clearfix"></div>
	</div>
</div>