<div class="sticky-form-wrap">
	<div class="sticky-form-title hide">
		<div class="sticky-form-title-text text-center">
			Check Final Price
			<i class="glyphicon glyphicon-remove sticky-form-close pull-right in-blk mar-r-10"></i>
			<div class="clearfix"></div>
			<div class="sticky-form-msg">(the final price depends on your <b>party location</b>)</div>
		</div>
		<div class="sticky-form-no-title-text hide text-center">
			<i class="glyphicon glyphicon-remove sticky-form-close pull-right in-blk mar-r-10"></i>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="sticky-form-text text-center pad-l-15 pad-r-15">
		<div id="formNormalText" class="form-normal-text">
			Please enter following details to check availability.
		</div>
		<div id="feasibilitySuccessText" class="feasibility-success-text hide">
			<i class="glyphicon glyphicon-ok-circle feasibility-success-icon mar-r-2"></i>
			Delivery is available at <span id="upPinCodeSuccess" class="updated-pincode text-bold mar-l-4"></span>
			<a class="change-party-location mar-l-2">(change)</a>
		</div>
		<div id="feasibilityFailText" class="feasibility-fail-text hide">
			<i class="glyphicon glyphicon-remove-circle feasibility-fail-icon mar-r-2"></i>
			<span>
				Sorry, delivery is not available at
			</span>
			<span id="upPinCodeFail" class="updated-pincode text-bold mar-l-4"></span>. Please try with different pin code.
		</div>
	</div>
	<div class="sticky-form-content pad-l-15 pad-r-15">
		<div class="sticky-price-content mar-t-10 hide">
			<div class="sticky-updated-price price-check-sticky-price text-e text-center">
				<span class='rupee-font'>&#8377;</span>
				<span id="updatedPrice" class="updated-price"></span>
			</div>
			<div class="sticky-form-token-msg text-center hide mar-t-10">
				Pay
				<span class="sticky-form-token-msg-price">
					<span class='rupee-font'>&#8377;</span>
					<span id="msgTokenAmount"></span>
				</span>
				as advance to book
			</div>
			<hr class="sticky-mobile-profile-hr-1">
			<div class="sticky-price-splitUp">
				<div class="product-split-price-worth">
					<span class="pull-left">Decor Price</span>
					<span class="pull-right">@price($data['decor']->worth)</span>
					<div class="clearfix"></div>
				</div>
				<div class="product-split-base-price hide">
					<span class="pull-left">Decor Price</span>
					<span class="pull-right">@price($data['decor']->min_price)</span>
					<div class="clearfix"></div>
				</div>
				<div class="product-split-price-off">
					<span class="pull-left">Evibe discount</span>
					<span class="pull-right text-g">- @offPrice($data['decor']->min_price, $data['decor']->worth)</span>
					<div class="clearfix"></div>
				</div>
				<div class="product-split-transport-price">
					<span class="pull-left">Transportation Charges</span>
					<span id="transportFree" class="transport-free pull-right text-free">Free</span>
					<span id="transportPriceContent" class="transport-price-content pull-right hide">
						<span class='rupee-font'>&#8377;</span>
						<span id="transportPrice" class="transport-price"></span>
					</span>
					<div class="clearfix"></div>
				</div>
				<hr class="sticky-mobile-profile-hr-2">
				<div class="product-split-total-price">
					<span class="pull-left">Total Price</span>
					<span class="pull-right">
						<span class='rupee-font'>&#8377;</span>
						<span class="updated-price"></span>
					</span>
					<div class="clearfix"></div>
				</div>
				<hr class="sticky-mobile-profile-hr-2">
				<div class="product-split-advance text-bold">
					<span class="pull-left">Advance To Pay</span>
					<span class="pull-right text-e">
						<span class='rupee-font'>&#8377;</span>
						<span id="tokenAmount" class="token-amount"></span>
					</span>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="sticky-form mar-t-5">
			<form id="mobileStickyForm" class="product-user-input">
				<div id="feasibilityErrorMsg" class="alert-danger form-error-message text-center hide"></div>
				<div class="col-xs-6 col-sm-6 text-center hide">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="customerName" type="text" class="mdl-textfield__input mar-b-5" name="customerName">
						<label class="mdl-textfield__label font-13">
							<span class="label-text">Name*</span>
						</label>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 text-center hide">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="customerEmail" type="text" class="mdl-textfield__input mar-b-5" name="customerEmail">
						<label class="mdl-textfield__label font-13">
							<span class="label-text">Email*</span>
						</label>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 text-center">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="mobilePhone" type="text" class="mdl-textfield__input mar-b-5" name="mobilePhone" maxlength="10">
						<label class="mdl-textfield__label font-13">
							<span class="label-text">Mobile Number*</span>
						</label>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 text-center hide">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="partyArea" type="text" class="mdl-textfield__input google-auto-complete mar-b-5" name="partyArea" placeholder="">
						<label class="mdl-textfield__label font-13">
							<span class="label-text">Party Area*</span>
						</label>
						<input type="hidden" class="google-location-details" name="locationDetails" value="">
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 text-center hide">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="profilePartyDate" type="text" class="mdl-textfield__input mar-b-5" name="profilePartyDate">
						<label class="mdl-textfield__label font-13">
							<span class="label-text">Party Date*</span>
						</label>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 text-center">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="partyPincode" type="text" class="mdl-textfield__input mar-b-5" name="partyPincode" maxlength="6" data-url="{{ route('auto-book.transport-price-calculate', [$data["decor"]->id, config("evibe.ticket.type.decor")]) }}">
						<label class="mdl-textfield__label font-13">
							<span class="label-text">Party Area Pin Code*</span>
						</label>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 hide">
					<label class="fw-normal">
						{{ Form::checkbox('unsurePartyDate', '1', false, ['id' => 'unsurePartyDate']) }}
						My party date is not finalised.
					</label>
				</div>
				<div class="clearfix"></div>
			</form>
		</div>
	</div>
	<div class="sticky-form-cta">
		<div id="formNormalCta" class="form-normal-cta text-center">
			<div class="col-xs-12 col-sm-12">
				<div class="product-cta-message text-center pad-b-5">
					<i class="glyphicon glyphicon-info-sign"></i> You can
					<b>Book</b> the decor after checking availability.
				</div>
			</div>
			<div class="col-xs-4 col-sm-4 no-pad">
				<button class="btn btn-submit mob-cta-text mob-btn-close-form full-width">Close</button>
			</div>
			<div class="col-xs-8 col-sm-8 no-pad">
				<button class="btn btn-submit mob-btn-check mob-cta-text full-width check-feasibility-button">Check Now</button>
			</div>
			<div class="clearfix"></div>
		</div>
		<div id="feasibilitySuccessCta" class="feasibility-success-cta mar-t-10 hide">
			<div class="col-xs-4 col-sm-4 no-pad hide"><!-- need to rethink after headers are changed -->
				<button class="btn btn-submit mob-cta-text mob-btn-close-form full-width">Close</button>
			</div>
			<div class="col-xs-12 col-sm-12 no-pad">
				<button class="btn btn-submit mob-cta-text full-width mob-btn-proceed mob-btn-proceed-2">Book Now</button>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<div class="sticky-book-now-form-wrap">
	<div class="sticky-form-text text-center pad-l-15 pad-r-15">
		<div id="formNormalText" class="form-normal-text">
			Please enter following details to book decor.
			<div class="text-center mar-t-10">
				<span class="book-now-price-worth">@price($data['decor']->worth)</span>
				<span class="book-now-base-price mar-l-5">@price($data['decor']->min_price)</span>
			</div>
			<div class="mar-t-10">
				Advance To Pay
				<span class="book-now-advance mar-l-10">
					@advAmount($data['decor']->min_price)
				</span>
			</div>
			<div class="book-now-advance-msg"><i>(price may vary based on <b>party location</b>)</i></div>
		</div>
		<div id="feasibilitySuccessText" class="feasibility-success-text hide">
			<i class="glyphicon glyphicon-ok-circle feasibility-success-icon mar-r-2"></i>
			Delivery is available at <span id="upPinCodeSuccess" class="updated-pincode text-bold mar-l-4"></span>
			<a class="change-party-location mar-l-2">(change)</a>
		</div>
		<div id="feasibilityFailText" class="feasibility-fail-text hide">
			<i class="glyphicon glyphicon-remove-circle feasibility-fail-icon mar-r-2"></i>
			<span>
				Sorry, delivery is not available at
			</span>
			<span id="upPinCodeFail" class="updated-pincode text-bold mar-l-4"></span>. Please try with different pin code.
		</div>
	</div>
	<div class="sticky-form-content pad-l-15 pad-r-15">
		<div class="sticky-price-content mar-t-10 hide">
			<div class="sticky-updated-price price-check-sticky-price text-e text-center">
				<span class='rupee-font'>&#8377;</span>
				<span id="updatedPrice" class="updated-price"></span>
			</div>
			<div class="sticky-form-token-msg text-center hide mar-t-10">
				Pay
				<span class="sticky-form-token-msg-price">
					<span class='rupee-font'>&#8377;</span>
					<span id="msgTokenAmount"></span>
				</span>
				as advance to book
			</div>
			<hr class="sticky-mobile-profile-hr-1">
			<div class="sticky-price-splitUp">
				<div class="product-split-price-worth">
					<span class="pull-left">Decor Price</span>
					<span class="pull-right">@price($data['decor']->worth)</span>
					<div class="clearfix"></div>
				</div>
				<div class="product-split-base-price hide">
					<span class="pull-left">Decor Price</span>
					<span class="pull-right">@price($data['decor']->min_price)</span>
					<div class="clearfix"></div>
				</div>
				<div class="product-split-price-off">
					<span class="pull-left">Evibe discount</span>
					<span class="pull-right text-g">- @offPrice($data['decor']->min_price, $data['decor']->worth)</span>
					<div class="clearfix"></div>
				</div>
				<div class="product-split-transport-price">
					<span class="pull-left">Transportation Charges</span>
					<span id="transportFree" class="transport-free pull-right text-free">Free</span>
					<span id="transportPriceContent" class="transport-price-content pull-right hide">
						<span class='rupee-font'>&#8377;</span>
						<span id="transportPrice" class="transport-price"></span>
					</span>
					<div class="clearfix"></div>
				</div>
				<hr class="sticky-mobile-profile-hr-2">
				<div class="product-split-total-price">
					<span class="pull-left">Total Price</span>
					<span class="pull-right">
						<span class='rupee-font'>&#8377;</span>
						<span class="updated-price"></span>
					</span>
					<div class="clearfix"></div>
				</div>
				<hr class="sticky-mobile-profile-hr-2">
				<div class="product-split-advance text-bold">
					<span class="pull-left">Advance To Pay</span>
					<span class="pull-right text-e">
						<span class='rupee-font'>&#8377;</span>
						<span id="tokenAmount" class="token-amount"></span>
					</span>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="sticky-form mar-t-5 mar-b-5">
			<form id="mobileStickyForm" class="product-user-input">
				<div id="bookNowErrorMsg" class="alert-danger form-error-message text-center hide"></div>
				<div class="col-xs-6 col-sm-6 text-center hide">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="bookNowName" type="text" class="mdl-textfield__input mar-b-5" name="bookNowName">
						<label class="mdl-textfield__label font-13">
							<span class="label-text">Name*</span>
						</label>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 text-center hide">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="bookNowEmail" type="text" class="mdl-textfield__input mar-b-5" name="bookNowEmail">
						<label class="mdl-textfield__label font-13">
							<span class="label-text">Email*</span>
						</label>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 text-center">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="bookNowPhone" type="text" class="mdl-textfield__input mar-b-5" name="bookNowPhone" maxlength="10">
						<label class="mdl-textfield__label font-13">
							<span class="label-text">Mobile Number*</span>
						</label>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 text-center hide">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="bookNowPartyArea" type="text" class="mdl-textfield__input google-auto-complete mar-b-5" name="bookNowPartyArea" placeholder="">
						<label class="mdl-textfield__label font-13">
							<span class="label-text">Party Area*</span>
						</label>
						<input type="hidden" class="google-location-details" name="locationDetails" value="">
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 text-center">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="bookNowPartyDate" type="text" class="mdl-textfield__input mar-b-5" name="bookNowPartyDate">
						<label class="mdl-textfield__label font-13">
							<span class="label-text">Party Date*</span>
						</label>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 text-center">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="bookNowPartyPinCode" type="text" class="mdl-textfield__input mar-b-5" name="bookNowPartyPinCode" maxlength="6" data-url="{{ route('auto-book.transport-price-calculate', [$data["decor"]->id, config("evibe.ticket.type.decor")]) }}">
						<label class="mdl-textfield__label font-13">
							<span class="label-text">Party Area Pin Code*</span>
						</label>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 hide">
					<div><i>By submitting this form, I agree to the <a href="{{ route('terms') }}"
									target="_blank">terms of service</a>.</i></div>
				</div>
				<div class="clearfix"></div>
			</form>
		</div>
	</div>
	<div class="sticky-form-cta">
		<div id="formNormalCta" class="form-normal-cta text-center">
			<div class="col-xs-12 col-sm-12 hide">
				<div class="product-cta-message text-center pad-b-5">
					<i class="glyphicon glyphicon-info-sign"></i> You can
					<b>Book</b> the decor after checking availability.
				</div>
			</div>
			<div class="col-xs-4 col-sm-4 no-pad">
				<button class="btn btn-submit mob-cta-text mob-btn-close-book-now-form full-width ga-btn-close-book-now-modal">Close</button>
			</div>
			<div class="col-xs-8 col-sm-8 no-pad">
				<button class="btn btn-submit mob-btn-check mob-cta-text full-width btn-book-now-submit" data-url="{{ route('auto-book.booking-price', [$data['decor']->id, config('evibe.ticket.type.decor'), $data['occasionId']]) }}">Submit
				</button>
			</div>
			<div class="clearfix"></div>
		</div>
		<div id="feasibilitySuccessCta" class="feasibility-success-cta mar-t-10 hide">
			<div class="col-xs-4 col-sm-4 no-pad hide"><!-- need to rethink after headers are changed -->
				<button class="btn btn-submit mob-cta-text mob-btn-close-form full-width ga-btn-close-availability-modal">Close</button>
			</div>
			<div class="col-xs-12 col-sm-12 no-pad">
				<button class="btn btn-submit mob-cta-text full-width mob-btn-proceed mob-btn-proceed-2">Continue To Book</button>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<div class="sticky-enquiry-form-wrap">
	<div class="sticky-enquiry-title">
		<div class="sticky-form-title-text text-center">
			Enquire with us
		</div>
	</div>
	<div class="sticky-enquiry-content pad-l-15 pad-r-15">
		<div class="sticky-form mar-t-5">
			<form id="mobileStickyEnquiryForm" class="">
				<div class="sticky-form-error">
					<div id="stickyEnquiryFormErrorMsg" class="alert-danger text-center"></div>
				</div>
				<div class="col-xs-6 col-sm-6 text-center">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="enquiryCustomerName" type="text" class="mdl-textfield__input mar-b-5" name="enquiryCustomerName">
						<label class="mdl-textfield__label font-13">
							<span class="label-text">Name*</span>
						</label>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 text-center">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="enquiryCustomerPhone" type="text" class="mdl-textfield__input mar-b-5" name="enquiryCustomerPhone" maxlength="10">
						<label class="mdl-textfield__label font-13">
							<span class="label-text">Mobile Number*</span>
						</label>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 text-center">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="enquiryCustomerEmail" type="text" class="mdl-textfield__input mar-b-5" name="enquiryCustomerEmail">
						<label class="mdl-textfield__label font-13">
							<span class="label-text">Email*</span>
						</label>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 text-center">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="enquiryProfileDate" type="text" class="mdl-textfield__input mar-b-5" name="enquiryProfileDate">
						<label class="mdl-textfield__label font-13">
							<span class="label-text">Party Date*</span>
						</label>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 text-center hide">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="partyArea" type="text" class="mdl-textfield__input google-auto-complete mar-b-5" name="partyArea" placeholder="">
						<label class="mdl-textfield__label font-13">
							<span class="label-text">Party Area*</span>
						</label>
						<input type="hidden" class="google-location-details" name="locationDetails" value="">
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 text-center hide">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="partyPincode" type="text" class="mdl-textfield__input mar-b-5" name="partyPincode" maxlength="6" data-url="{{ route('auto-book.transport-price-calculate', [$data["decor"]->id, config("evibe.ticket.type.decor")]) }}">
						<label class="mdl-textfield__label font-13">
							<span class="label-text">Party Area Pin Code*</span>
						</label>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 text-center">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input id="enquiryComments" type="text" class="mdl-textfield__input mar-b-5" name="enquiryComments">
						<label class="mdl-textfield__label font-13">
							<span class="label-text">Your Requirements</span>
						</label>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12">
					<div><i>By submitting this form, I agree to the <a href="{{ route('terms') }}"
									target="_blank">terms of service</a>.</i></div>
				</div>
				<div class="clearfix"></div>
			</form>
		</div>
	</div>
	<div class="sticky-enquiry-cta mar-t-10 text-center">
		<div class="col-xs-4 col-sm-4 no-pad">
			<button class="btn btn-submit mob-cta-text mob-profile-enquiry-close-button full-width">Close</button>
		</div>
		<div class="col-xs-8 col-sm-8 no-pad">
			<button class="btn btn-submit mob-btn-check mob-cta-text full-width mob-post-enquiry-button" data-url="{{ route('ticket.decor', [$data['occasionId'], $data['decor']->id]) }}">Submit
			</button>
		</div>
		<div class="clearfix"></div>
	</div>
</div>