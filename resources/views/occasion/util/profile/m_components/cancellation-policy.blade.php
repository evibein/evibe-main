<div class="tab-pane product-description-tab mar-t-10 mar-l-10 mar-r-15" id="cancellationsAndRefunds">
	<div class="product-description-tab-title">
		Cancellations & Refunds
	</div>
	<div class="mar-t-10">In case of cancellation, refundable amount is calculated based on the following policies:</div>
	<table class="table table-condensed table-bordered mar-t-10">
		<tr>
			<th>Cancellation Time Before Party</th>
			<th>Refund Percentage*</th>
		</tr>
		@foreach($data['cancellationData'] as $cd)
			<tr>
				<td>@if($cd->min_day ==0)
						{{ $cd->max_day }} days or below before party
					@elseif($cd->max_day > 30)
						{{ $cd->min_day }} days or above before party
					@else
						{{ $cd->min_day }} days - {{ $cd->max_day }} days before party
					@endif
				</td>
				<td>{{ round($cd->refund_percent) }} %</td>
			</tr>
		@endforeach
	</table>
	<div class="mar-t-5">* Refund Percentage will be calculated on <b>Total Advance Amount</b></div>
</div>