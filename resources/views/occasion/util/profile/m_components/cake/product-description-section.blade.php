<div class="product-description-section">
	<div class="product-description-wrap mar-t-20">
		<div class="hide">
			<div class="mar-l-5 mar-r-5 product-info-tabs">
				@if ($data['info'] || (isset($data['highlights']) && count($data['highlights'])) || (isset($data['addOns']) && count($data['addOns'])))
					<a class="scroll-item btn btn-default mar-5" data-scroll="inclusions">Inclusions</a>
				@endif
				@if($data['ratings']['total']['count'])
					<a class="scroll-item btn btn-default mar-5" data-scroll="productReviews">Customer Reviews</a>
				@endif
				<a class="scroll-item btn btn-default mar-5" data-scroll="deliveryCharges">Delivery Charges</a>
				@if(isset($data['addOns']) && count($data['addOns']))
					<a class="scroll-item btn btn-default mar-5" data-scroll="addOns">Add Ons</a>
				@endif
				@if(isset($data['cancellationData']) && !is_null($data['cancellationData']))
					<a class="scroll-item btn btn-default mar-5" data-scroll="cancellationsAndRefunds">Cancellations & Refunds</a>
				@endif
			</div>
		</div>
		<div class="product-description-content">
			<div class="" id="tabs">
				<div class="tab-pane product-description-tab mar-t-10 mar-l-10 mar-r-15" id="inclusions">
					<div class="product-description-tab-title">
						Cake Inclusions
					</div>
					<div class="product-highlights-wrap mar-b-15">
						@if(isset($data['code']) && $data['code'])
							<div class="product-highlight-item">
								<div class="product-highlight-item-title in-blk">
									<i class="glyphicon glyphicon-th-list"></i> Cake Code:
								</div>
								<div class="product-highlight-item-text in-blk mar-l-5">{{ $data['code'] }}</div>
								<div class="clearfix"></div>
							</div>
						@endif
						@if(isset($data['flavour']) && $data['flavour'])
							<div class="product-highlight-item">
								<div class="product-highlight-item-title in-blk">
									<i class="glyphicon glyphicon-ice-lolly"></i> Best Flavour:
								</div>
								<div class="product-highlight-item-text in-blk mar-l-5">{{ $data['flavour'] }}</div>
								<div class="clearfix"></div>
							</div>
						@endif
						@if (isset($data['highlights']) && count($data['highlights']))
							@foreach($data['highlights'] as $highlightItem)
								<div class="product-highlight-item">
									<div class="product-highlight-item-title in-blk">
										<i class="{{ $highlightItem['icon'] }}"></i> {{ $highlightItem['name'] }}:
									</div>
									<div class="product-highlight-item-text in-blk mar-l-5">
										{{ $highlightItem['value'] }} Hrs
									</div>
									<div class="clearfix"></div>
								</div>
							@endforeach
						@endif
					</div>
					@if (isset($data['info']) && $data['info'])
						{!! $data['info'] !!}
					@endif
					<div class="text-center pad-t-10 pad-b-5">
						@include('app.social-share-plugin')
					</div>
				</div>
				<div class="tab-pane product-description-tab pad-l-15 pad-r-15 reviews-card" id="productReviews">
						@if(isset($data['deliveryImages']) && (count($data['deliveryImages'])))
							@include("occasion.util.profile.c_components.customer-images",[
								"deliveryImages" => $data['deliveryImages'],
							])

						@endif
					@include("app.review.partial", [
													"reviews"=> $data["ratings"],
													"showAllReviewsUrl" => route("provider-reviews:all", [
														getSlugFromText($data["name"]),
														config("evibe.ticket.type.planner"),
														$data["provider"]['id']
													])
												])
				</div>
				<div class="tab-pane product-description-tab mar-t-10 mar-l-10 mar-r-15" id="deliveryCharges">
					<div class="product-description-tab-title">
						Delivery Charges
					</div>
					<table class="table table-condensed table-bordered mar-t-10">
						<tr>
							<th>Slot (any day)</th>
							<th>Price</th>
						</tr>
						<tr>
							<td>7:00 AM - 10:00 AM</td>
							<td>@price(150)</td>
						</tr>
						<tr>
							<td>10:00 AM - 11:00 PM</td>
							<td>Free</td>
						</tr>
						<tr>
							<td>11:00 PM - 11:50 PM</td>
							<td>@price(150)</td>
						</tr>
					</table>
				</div>
				@if(isset($data['addOns']) && count($data['addOns']))
					<div class="tab-pane product-description-tab mar-t-10 mar-l-10 mar-r-15" id="addOns">
						@include('occasion.util.add-ons.list', ['addOns' => $data['addOns']])
					</div>
				@endif
				@if(isset($data['cancellationData']) && !is_null($data['cancellationData']))
					@include('occasion.util.profile.m_components.cancellation-policy')
				@endif
			</div>
		</div>
		<div class="mar-t-30 mar-b-30 text-center">
			<span>Need help?</span>
			<span><a class="product-link-enquire-now ga-btn-enquire-now popup-need-help-btn mar-l-5 ga-mobile-profile-enquire-link">Enquire Now</a></span>
		</div>
		@include('app.modals.auto-popup',
			[
			"cityId" => getCityId(),
			"occasionId" => $data['occasionId'],
			"mapTypeId" => $data['mapTypeId'],
			"mapId" => $data['id'],
			"optionName" => $data['name'],
			"countries" => (isset($data['countries']) && count($data['countries'])) ? $data['countries'] : []
			])
	</div>
</div>