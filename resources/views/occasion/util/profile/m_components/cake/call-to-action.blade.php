<div class="cta-section text-center btn-mobile-submit hide">
	<div class="col-xs-12 col-sm-12 no-pad">
		<a class="btn btn-default product-btn-book-now btn-cake-book-now">
			<i class="material-icons valign-mid">&#xE8CC;</i>
			<span class="valign-mid">Book Now</span>
		</a>
	</div>
	<div class="clearfix"></div>
</div>