@if (!is_null($data['similarProducts']) && (count($data['similarProducts']) > 0) && isset($data['cityUrl']) && isset($data['occasion']))
	<hr class="desktop-profile-hr">
	<div class="similar-products-section">
		<div class="similar-products-wrap">
			<div class="similar-products-title mar-l-15">
				@if(isset($data['title']))
					{{ $data['title'] }}
				@else
					Similar Products
				@endif
			</div>
			<div class="similar-products-content mar-t-15 mar-b-15">
				<div class="col-xs-12 col-sm-12 bg-white no-pad">
					<div id="sProductsCaruosel" class="carousel slide" data-ride="carousel">

						<!-- Wrapper for slides -->
						<div class="carousel-inner pad-l-15">
							@php $i = 1;
							$count = count($data['similarProducts']);
							@endphp
							@foreach ($data['similarProducts'] as $product)
								@php
									$occasionId = \App\Models\Types\TypeEvent::where("name", "LIKE", "%" . $product["occasion_name"] . "%")->first();
								@endphp
								@if(!is_null($product["profile_url"]) && $product["profile_url"] != "")
									@if($i % 3 == 1)
										<div class="item @if($i == 1) active @endif">
											@endif
											<div class="col-xs-4 col-sm-4 mar-b-15 no-pad-l">
												<div class="product-similar-img-wrap bg-black">
													<a href="{{ $product["profile_url"] }}?utm_source=image&utm_campaign=similar-options&utm_medium=website&utm_content={{$product['code']}}" target="_blank">
														<img class="product-similar-img" src="{{ $product['profile_image'] }}" alt="{{ substr($product['name'], 0, 15) . "..." }} profile picture" onError="this.src='{{ $data['defaultImage'] }}'; this.style='padding: 30px 30px'">
													</a>
												</div>
												<div class="product-similar-title">
													<a href="{{ $product["profile_url"] }}?utm_source=title&utm_campaign=similar-options&utm_medium=website&utm_content={{$product['code']}}" target="_blank">
														{{ $product['name'] }}
													</a>
												</div>
												<div class="product-similar-content">
													@if(isset($product['ratings']) && $product['ratings'])
														<div class="col-md-6 col-lg-6 no-pad-r">
															<div class="product-similar-ratings">
																@if($product['ratings']['total']['count'])
																	<div>
																		<input type="number" class="avg-rating hide" value="{{ $product['ratings']['total']['avg'] }}" title="{{ $product['ratings']['total']['avg'] }} average rating for provider of {{ $product['name'] }}"/>
																	</div>
																	<div class="product-rating-reviews cur-point in-blk">({{ $product['ratings']['total']['count'] }} reviews)</div>
																@else
																	<div class="product-rating-reviews in-blk">(<i>new arrival</i>)
																	</div>
																@endif
															</div>
														</div>
													@endif
													<div class="@if(isset($product['ratings']) && $product['ratings']) col-md-6 col-lg-6 @else col-md-12 col-lg-12 text-center @endif">
														<div class="product-similar-price">
															<div class="product-main-price">
																@price($product['price_min'])*
															</div>
															@if ($product['price_max'] && $product['price_max'] > $product['price_min'])
																<div class="product-price-worth price-check-product-price-worth">
																	@price($product['price_max'])
																</div>
															@endif
															@if (isset($product['rangeInfo']) && $product['rangeInfo'])
																<i class="glyphicon glyphicon-info-sign product-price-info font-12" data-toggle="tooltip" data-placement="left" title="{{ $product['rangeInfo'] }}"></i>
															@endif
															@if($product['price_min'] && $product['price_max'])
																<div class="product-price-off hide in-blk">
																	(@offPrice($product['price_min'], $product['price_max']) OFF)
																</div>
															@endif
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
											@if($i % 3 == 0)
										</div>
									@endif

									@php $i++; @endphp
								@endif
							@endforeach
							@if(count($data['similarProducts']) % 3 != 0)</div>@endif
					</div>

					<!-- Left and right controls -->
					<a class="left carousel-control product-carousel-slider" href="#sProductsCaruosel" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left font-20-imp"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control product-carousel-slider" href="#sProductsCaruosel" data-slide="next">
						<span class="glyphicon glyphicon-chevron-right font-20-imp"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
@endif