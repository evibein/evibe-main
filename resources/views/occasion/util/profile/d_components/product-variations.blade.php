@if(isset($data["variations"]) && isset($data["variations"]["productVariations"]) && (count($data["variations"]["productVariations"]) > 0))
	@if ($agent->isMobile() && !($agent->isTablet()))
		<div class="variation-wrap mar-b-15">
			<h6 class="mar-t-10 no-mar-b font-12">Other Variations:</h6>
			<div class="variation-selection-img-loader text-center">
				<img src="{{ $galleryUrl }}/img/gifs/filter-loading.gif" alt="">
			</div>
			<div class="mar-t-5 variation-selection-wrap hide">
				@foreach($data["variations"]["productVariations"] as $variation)
					<div class="variation-selection-img-wrap in-blk text-center" data-map-id="{{ $variation["map_id"] }}" data-map-type-id="{{ $variation["map_type_id"] }}">
						<a href="javascript:void(0)" class="{{ $variation["map_type_id"] . "_" . $variation["map_id"] }}">
							<img src="{{ $galleryUrl }}/img/logo/logo_evibe.png" alt="variation-{{ $variation["map_id"] }}" class="variation-selection-img">
						</a>
					</div>
				@endforeach
				<div class="clearfix"></div>
			</div>
		</div>
	@else
		<div class="variation-wrap mar-b-15">
			<h6 class="no-mar-t no-mar-b">Other Variations:</h6>
			<div class="variation-selection-img-loader text-center">
				<img src="{{ $galleryUrl }}/img/gifs/filter-loading.gif" alt="">
			</div>
			<div class="mar-t-10 variation-selection-wrap hide">
				@foreach($data["variations"]["productVariations"] as $variation)
					<div class="variation-selection-img-wrap in-blk text-center" data-map-id="{{ $variation["map_id"] }}" data-map-type-id="{{ $variation["map_type_id"] }}">
						<a href="javascript:void(0)" class="{{ $variation["map_type_id"] . "_" . $variation["map_id"] }}">
							<img src="{{ $galleryUrl }}/img/logo/logo_evibe.png" alt="variation-{{ $variation["map_id"] }}" class="variation-selection-img">
						</a>
					</div>
				@endforeach
				<div class="clearfix"></div>
			</div>
		</div>
	@endif
	<input type="hidden" id="optionVariationId" value="{{ $data["variations"]["variationId"] }}">
@endif