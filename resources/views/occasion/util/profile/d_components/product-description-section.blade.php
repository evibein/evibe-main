<div class="product-description-section">
	<div id="fullDescription" class="product-description-wrap">
		@if($data['info'])
			<div class="desc-component-wrap">
				<div class="desc-component-title">
					Inclusions
				</div>
				<div class="desc-component-content">
					{!! $data['info'] !!}
				</div>
			</div>
		@endif
		@if($data['moreInfo'])
			<div class="desc-component-wrap">
				<div class="desc-component-title">
					Prerequisites
				</div>
				<div class="desc-component-content">
					{!! $data['moreInfo'] !!}
				</div>
			</div>
		@endif
		@if($data['facts'])
			<div class="desc-component-wrap">
				<div class="desc-component-title">
					Facts
				</div>
				<div class="desc-component-content">
					{!! $data['facts'] !!}
				</div>
			</div>
		@endif
		@if($data['ratings']['total']['count'])
			<div id="reviewSection" class="desc-component-wrap">
				<div class="desc-component-title">
					Reviews
				</div>
				<input type="hidden" id="hidProductAvgRating" value="{{ $data['ratings']['total']['avg'] }}">
				<input type="hidden" id="hidProductReviewCount" value="{{ $data['ratings']['total']['count'] }}">
				<input type="hidden" id="hidProductName" value="{{ $data['name'] }}">
				<div class="desc-component-content reviews-card">
					@include("app.review.partial", [
			"reviews"=> $data["ratings"],
			"showAllReviewsUrl" => route("provider-reviews:all", [
				getSlugFromText($data['productName']),
				config("evibe.ticket.type.planner"),
				$data['providerId']
			])
		])
				</div>
			</div>
		@endif
		@if($data['terms'])
			<div class="desc-component-wrap">
				<div class="desc-component-title">
					Terms of Booking
				</div>
				<div class="desc-component-content">
					{!! $data['terms'] !!}
				</div>
			</div>
		@endif
	</div>
</div>