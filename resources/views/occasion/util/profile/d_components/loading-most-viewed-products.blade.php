<div id="loadingMostViewedProducts">
	<hr class="desktop-profile-hr">
	<div class="loading-similar-title-wrap mar-l-15"> <!-- copy of 'loading-similar-products' -->
		<div class="loading-similar-title">

		</div>
	</div>
	@php $j = 0 @endphp
	@while($j < 6)
		<div class="col-xs-2 col-sm-2 mar-t-15 mar-b-30">
			<div class="product-similar-img-wrap bg-loading">
				<div class="loading-similar-image"></div>
			</div>
			<div class="product-similar-title">
				<div class="loading-similar-text-wrap">
					<div class="loading-similar-text">

					</div>
				</div>
			</div>
			<div class="product-similar-price">
				<div class="loading-similar-price-wrap">
					<div class="loading-similar-price">

					</div>
				</div>
			</div>
		</div>
		@php $j++; @endphp
	@endwhile
	<div class="clearfix"></div>
</div>