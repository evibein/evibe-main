<div class="how-it-works-section">
	<div class="how-it-works-wrap">
		<div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset1 text-center">
			<div class="desc-component-title ">How It Works?</div>
			<div class="how-it-works-list mar-t-30">
				<div class="how-it-works-step mar-b-10">
					<div class="col-md-4">
						<div class="how-it-works-image-wrap">
							<img class="how-it-works-image" src="{{ $galleryUrl}}/img/icons/d-cart.png" alt="finalize">
						</div>
					</div>
					<div class="col-md-8">
						<div class="how-it-works-text">
							Finalise
						</div>
						<div class="how-it-works-content">
							Choose from 1000s of options with real customer reviews, inclusions & images.
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="how-it-works-arrow mar-b-10">
					<i class="material-icons">arrow_downward</i>
				</div>
				<div class="how-it-works-step mar-b-10">
					<div class="col-md-4">
						<div class="how-it-works-image-wrap">
							<img class="how-it-works-image" src="{{ $galleryUrl}}/img/icons/d-credit-card.png" alt="book">
						</div>
					</div>
					<div class="col-md-8">
						<div class="how-it-works-text">
							Book
						</div>
						<div class="how-it-works-content">
							Securely pay advance amount to block your slot (based on availability).
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="how-it-works-arrow mar-b-10">
					<i class="material-icons">arrow_downward</i>
				</div>
				<div class="how-it-works-step mar-b-10">
					<div class="col-md-4">
						<div class="how-it-works-image-wrap">
							<img class="how-it-works-image" src="{{ $galleryUrl}}/img/icons/d-confetti.png" alt="party">
						</div>
					</div>
					<div class="col-md-8">
						<div class="how-it-works-text">
							Relax & Enjoy
						</div>
						<div class="how-it-works-content">
							A coordinator will be assigned for your party. Be a guest to your own party.
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="col-md-12 col-lg-12 no-pad">
			<div class="refund-wrap mar-t-20">
				<div class="col-md-3 valign-mid">
					<div class="how-it-works-image-wrap pc-evibe-shield-wrap">
						<img src="{{ config('evibe.gallery.host') }}/img/app/evibe_shield.png" alt="Evibe guarantee" class="how-it-works-image">
					</div>
				</div>
				<div class="col-md-9">
					<div class="how-it-works-text">
						100% Refund
					</div>
					<div class="how-it-works-content">
						In case of non-availability of your order, we will initiate complete refund of the total advance paid within 2 business days. Otherwise, your total party is on us. No questions asked.
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>