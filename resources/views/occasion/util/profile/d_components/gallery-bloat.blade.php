<div class="gallery-section">
	@if (isset($data['gallery']) && count($data['gallery']))
		<div class="loading-img-wrap">
			<img src="/images/loading.gif" alt="">
			<div class="pad-t-10">loading gallery...</div>
		</div>
		<div class="gallery-wrap desktop-gallery hide">
			<img class="xzoom des-back-color" width="300" height="200" src="{{ $data['gallery'][0]['preview'] }}" xoriginal="{{ $data['gallery'][0]['url'] }}" onError="this.src= '/images/loading.gif'; this.style='padding: 80px 130px'">
			<div class="xzoom-thumbs scrollmenu des-description-scroll mar-t-5">
			@php $i = 1; @endphp
			@foreach ($data['gallery'] as $gallery)
					<a href="{{ $gallery['url'] }}">
						<img class="xzoom-gallery des-back-color" width="60" height="60" src="{{ $gallery['thumb'] }}" @if($i == 1) xpreview="{{ $gallery['preview'] }} @endif" onError="this.src= '/images/loading.gif'; this.style='padding: 20px 20px'">
					</a>
			@endforeach
			</div>
		</div>
	@else
		<div>No images found</div>
	@endif
</div>