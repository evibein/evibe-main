<div class="product-breadcrumb-section mar-t-5">
	<ol class="breadcrumb product-breadcrumb text-center__400">
		<li><a href="{{ route('city.home', $cityUrl) }}">{{$cityName}}</a></li>
		<li><a href="{{ route('city.occasion.birthdays.home', $cityUrl) }}">Birthday Party</a></li>
		<li><a href="{{ route('city.occasion.birthdays.venues.list', $cityUrl) }}">Venues</a>
		<li class="active">{{ $data['hall']['code'] }}</li>
	</ol>
</div>