<div class="product-description-section">
	<div id="fullDescription" class="product-description-wrap">
		<div class="desc-component-wrap mar-b-20" id="facilities">
			<div class="desc-component-title">
				Facilities
			</div>
			<div class="desc-component-content">
				<ul class="clearfix product-details-item-list">
					<li class="item">
						<i class="material-icons">&#xE7FB;</i>
						<span class="title">Capacity: </span>
						<span> {{ $data['hall']['capacityMin'] }} - {{ $data['hall']['capacityMax'] }}</span>
						@if ($data['hall']['capacityFloat'])
							<span> (floating {{ $data['hall']['capacityFloat'] }})</span>
						@endif
					</li>
					<li class="item @if (!$data['hall']['hasAc']) disabled @endif">
						<i class="material-icons">&#xE332;</i>
						<span>AC</span>
					</li>
					<li class="item @if (!$data['venue']['hasValetParking']) disabled @endif">
						<i class="material-icons">&#xE54F;</i>
						<span>Valet Parking</span>
					</li>
					<li class="item @if (!$data['venue']['carParkCount']) disabled @endif">
						<i class="material-icons">&#xE531;</i>
						@if ($data['venue']['carParkCount'])
							<span>Car Parking:</span>
							<span> {{ $data['venue']['carParkCount'] }}</span>
						@else
							<span> No Car Parking</span>
						@endif
					</li>
					<li class="item @if (!$data['venue']['hasLift']) disabled @endif">
						<i class="material-icons">&#xE89D;</i>
						<span>Lift</span>
					</li>
					<li class="item @if (!$data['venue']['hasSoundSystem']) disabled @endif">
						<i class="material-icons">&#xE049;</i>
						<span>Sound System</span>
						@if ($data['venue']['hasSoundSystem'] && $data['venue']['priceSoundSystem'])
							<span> (add @price($data['venue']['priceSoundSystem']))</span>
						@endif
					</li>
					<li class="item @if (!$data['venue']['hasMike']) disabled @endif">
						<i class="material-icons">&#xE029;</i>
						<span>Mike</span>
						@if ($data['venue']['hasMike'] && $data['venue']['priceMike'])
							<span> (add @price($data['venue']['priceMike']))</span>
						@endif
					</li>
					<li class="item @if (!$data['venue']['hasProjector']) disabled @endif">
						<i class="material-icons">&#xE04B;</i>
						<span>Projector</span>
						@if ($data['venue']['hasProjector'] && $data['venue']['priceProjector'])
							<span>(add @price($data['venue']['priceProjector']))</span>
						@endif
					</li>
					<li class="item @if (!$data['venue']['isOutsideDecorsAllowed']) disabled @endif">
						<i class="material-icons">&#xE545;</i>
						<span>Decorators Allowed</span>
						@if ($data['venue']['isOutsideDecorsAllowed'] && $data['venue']['priceOutsideDecorsAllowance'])
							<span>(add @price($data['venue']['priceOutsideDecorsAllowance']))</span>
						@endif
					</li>
				</ul>
			</div>
		</div>
		<hr class="desktop-profile-sub-hr">
		<div class="desc-component-wrap mar-b-20 pad-t-10" id="food">
			<div class="desc-component-title">
				Food &amp; Beverages
			</div>
			<div class="desc-component-content">
				<ul class="clearfix product-details-item-list">
					<li class="item @if (!$data['venue']['isAlcoholServed']) disabled @endif">
						<i class="material-icons">&#xE540;</i>
						@if ($data['venue']['isAlcoholServed'])
							<span>Alcohol Served</span>
						@else
							<span>Alcohol Not Served</span>
						@endif
					</li>
					<li class="item @if ($data['venue']['taxPercent'] == 0.00) disabled @endif">
						<i class="material-icons">&#xE227;</i>
						<span>Tax Extra: </span>
						@if ($data['venue']['taxPercent']) <span>Yes</span>
						@else <span>No</span> @endif
					</li>
					@if($data['hall']['priceMinRent'])
						<li class="item">
							<i class="material-icons">&#xE870;</i>
							<span>Rent: </span>
							@if ($data['hall']['worthRent'] && $data['hall']['worthRent'] > $data['hall']['priceMinRent'])
								<span class="mar-r-4"><del>@price($data['hall']['worthRent'])</del></span>
							@endif
							<span>@price($data['hall']['priceMinRent'])</span>
							@if ($data['hall']['priceMaxRent'] && $data['hall']['priceMaxRent'] >  $data['hall']['priceMinRent'])
								- @price($data['hall']['priceMaxRent'])*
							@else
								* onwards
							@endif
							<span>({{ $data['hall']['minRentDuration'] }} Hrs)</span>
						</li>
					@elseif (!$data['hall']['priceMinRent'] &&  $data['venue']['priceMinRent'])
						<li class="item">
							<i class="material-icons">&#xE870;</i>
							<span>Rent: </span>
							@if ($data['venue']['worthRent'] && $data['venue']['worthRent'] > $data['venue']['priceMinRent'])
								<span class="mar-r-4"><del>@price($data['venue']['worthRent'])</del></span>
							@endif
							<span>@price($data['venue']['priceMinRent'])</span>
							@if ($data['venue']['priceMaxRent'] && $data['venue']['priceMaxRent'] >  $data['venue']['priceMinRent'])
								- @price($data['venue']['priceMaxRent'])*
							@else
								* onwards
							@endif
							<span>({{ $data['venue']['minRentDuration'] }} Hrs)</span>
						</li>
					@endif
					@if($data['venue']['priceMinVeg'])
						<li class="item">
							<i class="material-icons">&#xE870;</i>
							<span>Veg: </span>
							@if ($data['venue']['worthVeg'] && $data['venue']['worthVeg'] > $data['venue']['priceMinVeg'])
								<span class="mar-r-4"><del>@price($data['venue']['worthVeg'])</del></span>
							@endif
							<span>@price($data['venue']['priceMinVeg'])* onwards</span>
						</li>
						<li class="item @if ($data['venue']['isPureVeg']) disabled @endif">
							<i class="material-icons">&#xE870;</i>
							<span>Non-Veg: </span>
							@if (!$data['venue']['isPureVeg'])
								@if ($data['venue']['worthNonVeg'] && $data['venue']['priceMinNonveg'])
									<span class="mar-r-4"><del>@price($data['venue']['worthNonVeg'])</del></span>
								@endif
								<span>@price($data['venue']['priceMinNonveg'])* onwards</span>
							@else
								<span>N/A</span>
							@endif
						</li>
					@endif
					@if($data['venue']['priceKid'])
						<li class="item">
							<i class="material-icons">&#xEB41;</i>
							<span>Per Kid: </span>
							@if ($data['venue']['worthKid'] && $data['venue']['worthKid'] > $data['venue']['priceKid'])
								<span class="mar-r-4">
													<del>@price($data['venue']['worthKid'])</del>
												</span>
							@endif
							<span>@price($data['venue']['priceKid'])*</span>
						</li>
						@if($data['venue']['priceAdult'])
							<li class="item">
								<i class="material-icons">&#xE87C;</i>
								<span>Per Adult: </span>
								@if ($data['venue']['worthAdult'] && $data['venue']['worthAdult'] > $data['venue']['priceAdult'])
									<span class="mar-r-4"><del>@price($data['venue']['worthAdult'])</del></span>
								@endif
								<span>@price($data['venue']['priceAdult'])*</span>
							</li>
						@endif
					@endif
					<li class="item item-66 w-100__400">
						<i class="material-icons">&#xE561;</i>
						<span>Cuisines: </span>
						<span>{{ $data['venue']['cuisines'] }}</span>
					</li>
				</ul>
			</div>
			@if($data['menus'])
				<hr class="card-separator">
				<div class="card-title">
					<h5 class="card-title-text">Food Menu</h5>
				</div>
				<div class="card-supporting-text clearfix no-pad-t">
					<ul class="clearfix menus-list gallery-images">
						@foreach($data['menus'] as $menu)
							<li class="menu gallery-image">
								<a href="{{ $menu['url'] }}" rel="slider[img]" title="{{ $menu['name'] }} @if ($menu['minGuests']) (mininum {{ $menu['minGuests'] }} guests) @endif">
									<img src="{{ $menu['url'] }}" alt="{{ $menu['name'] }} @if ($menu['minGuests']) (mininum {{ $menu['minGuests'] }} guests) @endif">
								</a>
								<div class="title">{{ $menu['name'] }}</div>
								@if ($menu['minGuests'])
									<div class="sub-text">(min. {{ $menu['minGuests'] }} guests)</div>
								@endif
							</li>
						@endforeach
					</ul>
				</div>
			@endif
		</div>
		@if ($data['venue']['mapLat'] && $data['venue']['mapLong'])
			<div class="desc-component-wrap mar-b-20" id="map">
				<div class="desc-component-title">
					On Map
				</div>
				<div class="desc-component-content">
					<div id="map-canvas"></div>
				</div>
			</div>
		@endif
		@if (count($data['extRatings']['ratings']))
			<div class="desc-component-wrap mar-b-20 pad-t-10" id="ratings">
				<div class="desc-component-title">
					Ratings across web
				</div>
				<div class="desc-component-content">
					<div class="card-supporting-text no-pad-t w-100__400">
						<table class="table table-striped table-bordered table-ratings no-mar-b">
							<thead>
							<tr class="text-center">
								<th class="first"></th>
								<th class="text-center">Rating</th>
								<th class="text-center">Mentions</th>
							</tr>
							</thead>
							<tbody class="text-bold">
							@foreach ($data['extRatings']['ratings'] as $rating)
								<tr>
									<td>
										<a class="no-pad__400" href="{{ $rating['type']['url'] }}" target="_blank" rel="noopener">
											<img class="w-100__400" src="{{ $galleryUrl }}/img/logo/{{ $rating['type']['image_url'] }}" alt="ratings on {{ $rating['type']['name'] }}"/>
										</a>
									</td>
									<td class="value">
										<span>{{ $rating['rating_value']}} / {{ $rating['rating_max'] }}</span>
									</td>
									<td class="value">
										<span>{{ $rating['rating_count'] }}</span>
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
						<div class="text-muted mar-t-15">
							<u>Important:</u>
							<span> These ratings were last updated <b>1 day ago</b>. All the ratings are mentioned for the sole purpose of information only.</span>
						</div>
					</div>
				</div>
			</div>
		@endif
	</div>
</div>