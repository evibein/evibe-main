<div class="price-section">
	<div class="price-section-wrap">
		<div class="product-profile-form-wrap">
			<div id="productProfileForm" class="product-profile-form pad-t-5 pad-b-15">
				<div class="product-price mar-t-10">
					<div class="product-price-wrap-2 text-center">
						<div class="product-price-old">
							@if ($data['decor']->worth && $data['decor']->worth > $data['decor']->min_price)
								<div class="product-price-worth in-blk mar-l-2 mar-r-5">
									@price($data['decor']->worth)
								</div>
							@endif
							@if($data['decor']->min_price && $data['decor']->worth)
								<div class="product-price-off in-blk">
									(@offPrice($data['decor']->min_price, $data['decor']->worth) OFF)
								</div>
							@endif
							<div class="pad-t-10">
								<div class="mar-l-5 product-main-price in-blk">
									@price($data['decor']->min_price)
									@if ($data['decor']->max_price && $data['decor']->max_price > $data['decor']->min_price)
										- @price($data['decor']->max_price)
									@endif
									@if ($data['decor']->range_info)
										<i class="glyphicon glyphicon-info-sign product-price-info gfont-12" data-toggle="tooltip" data-placement="left" title="{{ $data['decor']->range_info }}"></i>
									@endif
								</div>
								<div class="product-price-tag mar-l-5 in-blk">
									<i class="glyphicon glyphicon-thumbs-up"></i> Best Price
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="mar-t-5 font-12">
								<i>(price may vary based on <b>party location</b>)</i>
							</div>
							@if($data["occasionId"] != config("evibe.occasion.pre-post.id"))
								<div class="mar-t-5">
								<span class="add-ons-price-wrap no-mar-l">
									<b><span class="add-ons-selected-count"></span></b> add-ons selected - <b><span><span class="rupee-font">&#8377; </span><span class="add-ons-price">0</span></span></b>
								</span>
								</div>
							@endif
						</div>
					</div>
				</div>
				@if($data["occasionId"] == config("evibe.occasion.pre-post.id"))
					<div class="product-cta mar-t-15 pad-l-10 pad-r-10">
						<div class="col-md-12 col-lg-12">
							<a class="btn btn-default product-btn-book-now des-btn-pre-book-now ga-btn-book-now">
								<i class="material-icons valign-mid">&#xE8CC;</i>
								<span class="valign-mid mar-l-4">Book Now</span>
							</a>
						</div>
						<div class="clearfix"></div>
						<a class="btn btn-default product-btn-book-now btn-profile-book-free-site-visit hide">
							<div class="valign-mid">BOOK FREE SITE VISIT</div>
						</a>
					</div>
				@else
					<div class="product-cta mar-t-15">
						<div class="col-md-12 col-lg-12">
							<a class="btn btn-default product-btn-book-now des-btn-pre-book-now ga-btn-book-now">
								<i class="material-icons valign-mid">&#xE8CC;</i>
								<span class="valign-mid mar-l-4">Book Now</span>
							</a>
						</div>
						<div class="col-md-12 col-lg-12 mar-t-15 hide">
							<a class="btn des-btn-pre-feasibility product-btn-availability ga-btn-availability">
								<div class="valign-mid">Check Availability</div>
							</a>
						</div>
						<div class="col-md-12 col-lg-12">
							<div class="col-xs-7 mar-t-15 no-pad-l">
								<a id="shortlistBtn" class="btn btn-default product-btn-shortlist" v-on:click="shortlistOptionFromProfilePage">
									<span class="hideShortlistData" data-cityId="{{ $data['shortlistData']['cityId'] }}" data-occasionId="{{ $data['shortlistData']['occasionId'] }}" data-mapId="{{ $data['shortlistData']['mapId'] }}" data-mapTypeId="{{ $data['shortlistData']['mapTypeId'] }}" data-urlAdd="{{ route('partybag.add') }}" data-urlDelete="{{ route('partybag.delete') }}" data-plainImgWhite="{{ $galleryUrl }}/main/img/icons/party-bag_empty_w.png" data-removeImgGrey="{{ $galleryUrl }}/main/img/icons/party-bag_empty_g.png"></span>
									<i class="icon-bagicon5 product-shortlist-icon"></i>
								<!--<img src="{{ $galleryUrl }}/main/img/icons/party-bag_empty_w.png" alt="Party Bag List" class="partybag-detail-img">-->
									<span class="mar-l-2">Add To Cart</span>
								</a>
							</div>
							<div class="col-xs-5 mar-t-15 no-pad-r text-center font-16" style="border: solid 1px #D4D5D9; padding: 8px 0">
								<div data-city-id="{{ $data['shortlistData']['cityId'] }}" data-occasion-id="{{ $data['shortlistData']['occasionId'] }}" data-map-id="{{ $data['shortlistData']['mapId'] }}" data-map-type-id="{{ $data['shortlistData']['mapTypeId'] }}">
									<a class="wishlist-option fw-normal a-no-decoration-black cur-point" data-url="{{ route("wishlist.add") }}" data-status-url="{{ route("wishlist.status") }}"><i class="glyphicon glyphicon-bookmark" style="top: 2px;"></i> WISHLIST</a>
									<a class="wishlisted-option fw-normal a-no-decoration-black hide a-no-decoration-default"><i class="glyphicon glyphicon-bookmark" style="top: 2px;"></i> WISHLISTED</a>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>

<div class="mar-t-30 mar-b-30 text-center">
	<span>Need help?</span>
	<span><a class="product-link-enquire-now ga-btn-enquire-now popup-need-help-btn mar-l-5 ga-desktop-profile-enquire-link">Enquire Now</a></span>
</div>