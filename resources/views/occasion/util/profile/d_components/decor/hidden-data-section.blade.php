@if(isset($data['decor']))
	<input type="hidden" id="hidBaseBookingAmount" value="{{ $data['decor']->min_price }}">
	<input type="hidden" id="hidDistMax" value="{{ $data['decor']->kms_max }}">
	<input type="hidden" id="hidDistFree" value="{{ $data['decor']->kms_free }}">
	<input type="hidden" id="hidPriceCheckUrl" value="{{ route('auto-book.fetch-price', [$data['decor']->id, config('evibe.ticket.type.decor'), $data['occasionId']]) }}">

	@if(isset($data['decor']['provider']))
		<input type="hidden" id="hidProviderPincode" value="{{ $data['decor']['provider']->zip }}">
	@endif

	@if(isset($data['occasionId']))
		<input type="hidden" id="hidAutoBookUrl" value="{{ route('auto-book.decor.pay.auto.init', [$data['decor']->id, $data['occasionId']]) }}">
		<input type="hidden" id="hidFailedToProceedData" name="failedToProceedData" value="{{ route('ajax.auto-book.failed-to-proceed', [$data['decor']->id, $data['mapTypeId'], $data['occasionId']]) }}">
		@if(isset($data['decor']) && $data['decor']->id)
			<input type="hidden" id="hidProfileEnqUrl" value="{{ route('ticket.decor', [$data['occasionId'], $data['decor']->id]) }}">
		@endif
	@endif

	@if(isset($data['typeDevice']))
		<input type="hidden" id="hidTypeDevice" value="{{ $data['typeDevice'] }}">
	@endif

	@if(isset($data['typeBrowser']))
		<input type="hidden" id="hidTypeBrowser" value="{{ $data['typeBrowser'] }}">
	@endif

	@if(isset($data['pageName']))
		<input type="hidden" id="hidPageName" value="{{ $data['pageName'] }}">
	@endif

	@if($data['decor']->min_price)
		<input type="hidden" id="hidProductPrice" value="{{ $data['decor']->min_price }}">
	@endif

	@if($data['decor']->code)
		<input type="hidden" id="hidProductCode" value="{{ $data['decor']->code }}">
	@endif

	@if(isset($data['mapTypeId']))
		<input type="hidden" id="hidForVenueDetails" value="{{ $data['mapTypeId'] }}">
	@endif

	@if(isset($data['mapTypeId']) && isset($data['occasionId']))
		<input type="hidden" id="hidSimilarProductsUrl" value="{{ route('ajax.profile.similar-products-global', [$data['occasionId'], $data['mapTypeId'], $data['decor']->id]) }}">
		<input type="hidden" id="hidMostViewedProductsUrl" value="{{ route('ajax.profile.most-viewed-products', [$data['decor']->id, $data['mapTypeId'], $data['occasionId']]) }}">
		<input type="hidden" id="hidProductReviewsUrl" value="{{ route('ajax.profile.product-reviews', [$data['decor']->id, $data['mapTypeId']]) }}">
	@endif
@endif