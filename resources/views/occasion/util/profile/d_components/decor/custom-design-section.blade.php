@if (!request()->is('*/engagement-wedding-reception*') && !request()->is('*/bachelor-party*'))
	<div class="custom-design-section">
		<div class="custom-design-wrap text-center">
			<div class="custom-design-title">
				@if(isset($title)) {{ $title }} @else Have your own designs?@endif
			</div>
			<div class="custom-design-content mar-t-10">
				<div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
					@if(isset($content))
						{{ $content }}
					@else
						You can now upload your customised designs. Based on the availability, we will get the best quote from our verified partners.
					@endif
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="custom-design-btn mar-t-15 mar-b-20">
				<button class="btn btn-default btn-bidding">
					<i class="material-icons valign-mid">&#xE2C6;</i>
					<span class="valign-mid">Submit My Designs</span>
				</button>
			</div>
		</div>
		@include("app.bidding.modals.design", ["url" => route('ticket.custom-ticket.decor', $data['occasionId'])])
		@include("app.bidding.bidding_common")
	</div>
@endif