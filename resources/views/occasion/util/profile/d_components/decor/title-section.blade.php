<div class="title-section">
	<div class="title-section-wrap">
		<h1 class="product-title-text no-mar-b no-mar-t ls-normal">
			{{ $data['decor']->name }}
		</h1>
		<div class="product-rating mar-t-10 in-blk">
			<div class="in-blk font-16">
				<div id="defaultRating" class="product-default-rating in-blk"></div>
				<div id="upProductAvgRating"></div>
			</div>
			<div id="defaultReview" class="product-default-reviews in-blk"></div>
			<div id="upProductReviewCount" class="in-blk"></div>
		</div>
		<div class="clearfix"></div>
		<div class="description-top des-description-scroll mar-t-15">
			<div class="product-highlights-wrap mar-b-15">
				@if($data['decor']->code)
					<div class="product-highlight-item">
						<div class="product-highlight-item-title in-blk">
							<i class="glyphicon glyphicon-th-list"></i> Style Code:
						</div>
						<div class="product-highlight-item-text in-blk mar-l-5">
							{{ $data['decor']->code }}
						</div>
						<div class="clearfix"></div>
					</div>
				@endif
				@if ($data['decor']->time_setup)
					<div class="product-highlight-item">
						<div class="product-highlight-item-title in-blk">
							<i class="glyphicon glyphicon-time"></i> Style Setup Time:
						</div>
						<div class="product-highlight-item-text in-blk mar-l-5">
							{{ $data['decor']->time_setup }} Hrs
						</div>
						<div class="clearfix"></div>
					</div>
				@endif
				@if ($data['decor']->time_duration > 0 && (isset($occasionId) && $occasionId != config("evibe.occasion.house-warming.id")))
					<div class="product-highlight-item">
						<div class="product-highlight-item-title in-blk">
							<i class="glyphicon glyphicon-time"></i> Style Rent Duration:
						</div>
						<div class="product-highlight-item-text in-blk mar-l-5">
							{{ $data['decor']->time_duration }} Hrs <small>(not applicable for balloons, flowers)</small>
						</div>
						<div class="clearfix"></div>
					</div>
				@endif
			</div>
			<div class="description-learn-more">
				Learn More:
				@if ($data['decor']->info)
					<a class="scroll-item text-underline mar-l-5" data-scroll="inclusions">Inclusions,</a>
				@endif
				@if(($data['decor']->more_info) || ($data['decor']->facts))
					<a class="scroll-item text-underline mar-l-5" data-scroll="thingsToRemember">Things To Remember,</a>
				@endif
				<a class="scroll-item text-underline mar-l-5 customer-reviews-ref-link hide" data-scroll="reviewSection">Customer Reviews,</a>
				@if(isset($data['addOns']) && count($data['addOns']))
					<a class="scroll-item text-underline mar-l-5" data-scroll="addOns">Add Ons,</a>
				@endif
				@if($data['decor']->terms)
					<a class="scroll-item text-underline mar-l-5" data-scroll="terms">Terms,</a>
				@endif
				@if(isset($data['cancellationData']) && !is_null($data['cancellationData']))
					<a class="scroll-item text-underline mar-l-5" data-scroll="cancellationsAndRefunds">Cancellations & Refunds</a>
				@endif
			</div>
		</div>
		<div class="description-show-more mar-t-5">
			<a class="scroll-item hide" data-scroll="fullDescription">
				<i class="material-icons">&#xE5DB;</i> <span class="text-underline">see more</span>
			</a>
		</div>
	</div>
</div>