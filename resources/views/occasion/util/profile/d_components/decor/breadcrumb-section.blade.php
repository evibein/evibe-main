@if (!request()->is('*/engagement-wedding-reception*') && !request()->is('*/bachelor-party*'))
	<div class="product-breadcrumb-section mar-t-5">
		<ol class="breadcrumb product-breadcrumb text-center__400">
			<li><a href="{{ route('city.home', $cityUrl) }}">{{$cityName}}</a></li>
			<li>
				<a href="{{ route('city.occasion.'.config('evibe.type-event-route-name.'.$data['occasionId']).'.home', $cityUrl) }}">{{ config('evibe.type-event-breadcrumb.'.$data['occasionId']) }}</a>
			</li>
			<li>
				<a href="{{ route('city.occasion.'.config('evibe.type-event-route-name.'.$data['occasionId']).'.'.config('evibe.type-product-route-name.'.$data['mapTypeId']).'.list', [$cityUrl]) }}">Decor Styles</a>
			</li>
			<li class="active">{{ $data['decor']->code }}</li>
		</ol>
	</div>
@else
	<div class="product-breadcrumb-section mar-t-5">
		<ol class="breadcrumb product-breadcrumb text-center__400">
			<li><a>{{$cityName}}</a></li>
			<li>
				<a>{{ config('evibe.type-event-breadcrumb.'.$data['occasionId']) }}</a>
			</li>
			<li>
				<a>Decor Styles</a>
			</li>
			<li class="active">{{ $data['decor']->code }}</li>
		</ol>
	</div>
@endif