<div class="partner-card-section">
	<div class="partner-card-wrap pad-l-15 pad-r-15">
		<div class="partner-code-text text-center">
			Partner Code: <span class="partner-code">{{ $data["decor"]->provider->code }}</span>
		</div>
		<div class="partner-attributes-wrap mar-t-5">
			<div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
				<ul class="partner-attributes">
					<li><img class="partner-attributes-image" src="{{ $galleryUrl }}/img/icons/phone-verified.png" data-toggle="tooltip" data-placement="left" title="mobile verified"></li>
					<li><img class="partner-attributes-image" src="{{ $galleryUrl }}/img/icons/email-verified.png" data-toggle="tooltip" data-placement="left" title="email verified"></li>
					<li><img class="partner-attributes-image" src="{{ $galleryUrl }}/img/icons/address-verified.png" data-toggle="tooltip" data-placement="left" title="address verified"></li>
					<li><img class="partner-attributes-image" src="{{ $galleryUrl }}/img/icons/quality-checked.png" data-toggle="tooltip" data-placement="left" title="quality checked"></li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>