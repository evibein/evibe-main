@if (isset($data['mVProducts']) && count($data['mVProducts']) && isset($data['cityUrl']) && isset($data['occasion']))
	<hr class="desktop-profile-hr">
	<div class="similar-products-section"> <!-- copy of 'similar-products' -->
		<div class="similar-products-wrap">
			<div class="similar-products-title mar-l-15">
				@if(isset($data['title']))
					{{ $data['title'] }}
				@else
					Most Viewed Products
				@endif
			</div>
			<div class="similar-products-content mar-t-15 mar-b-30">
				<div class="col-xs-12 col-sm-12 bg-white no-pad">
					<div id="mVProductsCarousel" class="carousel slide" data-ride="carousel">

						<!-- Wrapper for slides -->
						<div class="carousel-inner pad-l-15">
							@php $i = 1;
							$count = count($data['mVProducts'])
							@endphp
							@foreach ($data['mVProducts'] as $product)
								@if($i % 6 == 1)
									<div class="item @if($i == 1) active @endif">
										@endif
										<div class="col-xs-2 col-sm-2 mar-b-15 no-pad-l">
											<div class="product-similar-img-wrap bg-black">
												<a href="{{ route('city.occasion.'.config('evibe.type-event-route-name.'.$data['occasion']->id).'.'.config('evibe.type-product-route-name.'.$data['mapTypeId']).'.profile', [$data['cityUrl'], $product['url']]) }}">
													<img class="product-similar-img" src="{{ $product['profileImg'] }}" alt="{{ substr($product['name'], 0, 15) . "..." }} profile picture" onError="this.src='{{ $data['defaultImage'] }}'; this.style='padding: 30px 30px'">
												</a>
											</div>
											<div class="product-similar-title">
												<a href="{{ route('city.occasion.'.config('evibe.type-event-route-name.'.$data['occasion']->id).'.'.config('evibe.type-product-route-name.'.$data['mapTypeId']).'.profile', [$data['cityUrl'], $product['url']]) }}">
													{{ $product['name'] }}
												</a>
											</div>
											<div class="product-similar-content">
												@if(isset($product['ratings']) && $product['ratings'])
													<div class="col-md-6 col-lg-6 no-pad-r">
														<div class="product-similar-ratings">
															@if($product['ratings']['total']['count'])
																<div>
																	<input type="number" class="avg-rating hide" value="{{ $product['ratings']['total']['avg'] }}" title="{{ $product['ratings']['total']['avg'] }} average rating for provider of {{ $product['name'] }}"/>
																</div>
																<div class="product-rating-reviews cur-point in-blk">({{ $product['ratings']['total']['count'] }} reviews)</div>
															@else
																<div class="product-rating-reviews in-blk">(<i>new arrival</i>)
																</div>
															@endif
														</div>
													</div>
												@endif
												<div class="@if(isset($product['ratings']) && $product['ratings']) col-md-6 col-lg-6 @else col-md-12 col-lg-12 text-center @endif">
													<div class="product-similar-price">
														@if(isset($data['mapTypeId']) && $data['mapTypeId'] == config('evibe.ticket.type.cake'))
															<div class="product-main-price">
																@price($product['minPrice'])
																<span class="product-price-kg"> / KG</span>
															</div>
															<div class="product-min-order">
																(min. order: {{ $product['minOrder'] }} kg)
															</div>
														@else
															<div class="product-main-price">
																@price($product['minPrice'])
																@if ($product['maxPrice'] && $product['maxPrice'] > $product['minPrice'])
																	- @price($product['maxPrice'])
																@endif
															</div>
															@if ($product['worth'] && $product['worth'] > $product['minPrice'])
																<div class="product-price-worth price-check-product-price-worth">
																	@price($product['worth'])
																</div>
															@endif
															@if (isset($product['rangeInfo']) && $product['rangeInfo'])
																<i class="glyphicon glyphicon-info-sign product-price-info font-12" data-toggle="tooltip" data-placement="left" title="{{ $product['rangeInfo'] }}"></i>
															@endif
															@if($product['minPrice'] && $product['worth'])
																<div class="product-price-off hide in-blk">
																	(@offPrice($product['minPrice'], $product['worth']) OFF)
																</div>
															@endif
														@endif
													</div>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
										@if($i % 6 == 0)
									</div>
								@endif

								@php $i++; @endphp
							@endforeach
							@if(count($data['mVProducts']) % 6 != 0)</div>@endif
					</div>

					<!-- Left and right controls -->
					<a class="left carousel-control product-carousel-slider" href="#mVProductsCarousel" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left font-20-imp"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control product-carousel-slider" href="#mVProductsCarousel" data-slide="next">
						<span class="glyphicon glyphicon-chevron-right font-20-imp"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
@endif