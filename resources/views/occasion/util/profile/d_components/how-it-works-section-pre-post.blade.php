<div class="how-it-works-section">
	<div class="how-it-works-wrap">
		<ul class="how-it-works-list">
			<li>
				<div class="col-md-4">
					<div class="how-it-works-image-wrap">
						<img class="how-it-works-image" src="{{ $galleryUrl}}/img/icons/d-cart.png" alt="finalize">
					</div>
				</div>
				<div class="col-md-8">
					<div class="how-it-works-text">
						Browse
					</div>
					<div class="how-it-works-content">
						Choose from 1000s of options with best prices, reviews, inclusions & images
					</div>
				</div>
				<div class="clearfix"></div>
			</li>
			<li class="how-it-works-arrow">
				<i class="material-icons">&#xE037;</i>
			</li>
			<li>
				<div class="col-md-4">
					<div class="how-it-works-image-wrap">
						<img class="how-it-works-image" src="{{ $galleryUrl}}/main/img/icons/meeting.png" alt="book">
					</div>
				</div>
				<div class="col-md-8">
					<div class="how-it-works-text">
						Site Visit
					</div>
					<div class="how-it-works-content">
						Evibe wedding expert will clarify all your queries. Free site visit. Get price estimate. No obligation to book.
					</div>
				</div>
				<div class="clearfix"></div>
			</li>
			<li class="how-it-works-arrow">
				<i class="material-icons">&#xE037;</i>
			</li>
			<li>
				<div class="col-md-4">
					<div class="how-it-works-image-wrap">
						<img class="how-it-works-image" src="{{ $galleryUrl}}/img/icons/d-credit-card.png" alt="book">
					</div>
				</div>
				<div class="col-md-8">
					<div class="how-it-works-text">
						Book
					</div>
					<div class="how-it-works-content">
						If satisfied with the quote, securely pay 50% of total order amount as advance to block your slot.
					</div>
				</div>
				<div class="clearfix"></div>
			</li>
			<li class="how-it-works-arrow">
				<i class="material-icons">&#xE037;</i>
			</li>
			<li>
				<div class="col-md-4">
					<div class="how-it-works-image-wrap">
						<img class="how-it-works-image" src="{{ $galleryUrl}}/img/icons/d-confetti.png" alt="party">
					</div>
				</div>
				<div class="col-md-8">
					<div class="how-it-works-text">
						Relax & Enjoy
					</div>
					<div class="how-it-works-content">
						A coordinator will be assigned for your wedding. Be a guest to your own wedding.
					</div>
				</div>
				<div class="clearfix"></div>
			</li>
		</ul>
		<div class="clearfix"></div>
	</div>
</div>