<div class="product-description-section">
	<div id="fullDescription" class="product-description-wrap font-14">
		<div class="desc-component-wrap mar-b-20" id="aboutVenue">
			<div class="desc-component-title">
				About Venue
			</div>
			<div class="hall-desc-component-content mar-t-15">
				<div>
					@if(isset($data['venue']['aboutVenue']) && $data['venue']['aboutVenue'])
						{{ $data['venue']['aboutVenue'] }}
					@else
						Super luxurious party hall with great hospitality and service. Perfect for all of your events celebration, dining, social gatherings and meetings.
					@endif
				</div>
				<ul class="clearfix product-details-item-list hall-product-details-item-list">
					<li class="item @if(!$data['venue']['isAlcoholServed']) disabled @endif">
							<span class="material-icons hall-product-details-item-icon">
								@if($data['venue']['isAlcoholServed'])
									&#xE540;
								@else
									block
								@endif
							</span>
						<span class="hall-product-details-item-text">
							@if ($data['venue']['isAlcoholServed'])
								<span>Alcohol Served</span>
							@else
								<span>Alcohol Not Served</span>
							@endif
							</span>
					</li>
					<li class="item @if ($data['venue']['taxPercent'] == 0.00) disabled @endif">
						<span class="material-icons hall-product-details-item-icon">&#xE227;</span>
						<span class="hall-product-details-item-text">
							<span>Tax Extra: </span>
							@if ($data['venue']['taxPercent']) <span>Yes</span>
							@else <span>No</span> @endif
							</span>
					</li>
					@if($data['hall']['priceMinRent'])
						<li class="item">
							<span class="material-icons hall-product-details-item-icon">&#xE870;</span>
							<span class="hall-product-details-item-text">
								<span>Rent: </span>
								@if ($data['hall']['worthRent'] && $data['hall']['worthRent'] > $data['hall']['priceMinRent'])
									<span class="mar-r-4"><del>@price($data['hall']['worthRent'])</del></span>
								@endif
								<span>@price($data['hall']['priceMinRent'])</span>
								@if ($data['hall']['priceMaxRent'] && $data['hall']['priceMaxRent'] >  $data['hall']['priceMinRent'])
									- @price($data['hall']['priceMaxRent'])*
								@else
									* onwards
								@endif
								<span>({{ $data['hall']['minRentDuration'] }} Hrs)</span>
								</span>
						</li>
					@elseif (!$data['hall']['priceMinRent'] &&  $data['venue']['priceMinRent'])
						<li class="item">
							<span class="material-icons hall-product-details-item-icon">&#xE870;</span>
							<span class="hall-product-details-item-text">
								<span>Rent: </span>
								@if ($data['venue']['worthRent'] && $data['venue']['worthRent'] > $data['venue']['priceMinRent'])
									<span class="mar-r-4"><del>@price($data['venue']['worthRent'])</del></span>
								@endif
								<span>@price($data['venue']['priceMinRent'])</span>
								@if ($data['venue']['priceMaxRent'] && $data['venue']['priceMaxRent'] >  $data['venue']['priceMinRent'])
									- @price($data['venue']['priceMaxRent'])*
								@else
									* onwards
								@endif
								<span>({{ $data['venue']['minRentDuration'] }} Hrs)</span>
								</span>
						</li>
					@endif
					@if($data['venue']['priceMinVeg'])
						<li class="item">
							<span class="material-icons hall-product-details-item-icon">&#xE870;</span>
							<span class="hall-product-details-item-text">
								<span>Veg: </span>
								@if ($data['venue']['worthVeg'] && $data['venue']['worthVeg'] > $data['venue']['priceMinVeg'])
									<span class="mar-r-4"><del>@price($data['venue']['worthVeg'])</del></span>
								@endif
								<span>@price($data['venue']['priceMinVeg'])* onwards</span>
								</span>
						</li>
						<li class="item @if ($data['venue']['isPureVeg']) disabled @endif">
							<span class="material-icons hall-product-details-item-icon">&#xE870;</span>
							<span class="hall-product-details-item-text">
								<span>Non-Veg: </span>
								@if (!$data['venue']['isPureVeg'])
									@if ($data['venue']['worthNonVeg'] && $data['venue']['priceMinNonveg'])
										<span class="mar-r-4"><del>@price($data['venue']['worthNonVeg'])</del></span>
									@endif
									<span>@price($data['venue']['priceMinNonveg'])* onwards</span>
								@else
									<span>N/A</span>
								@endif
								</span>
						</li>
					@endif
					@if($data['venue']['priceKid'])
						<li class="item">
							<span class="material-icons hall-product-details-item-icon">&#xEB41;</span>
							<span class="hall-product-details-item-text">
								<span>Per Kid: </span>
								@if ($data['venue']['worthKid'] && $data['venue']['worthKid'] > $data['venue']['priceKid'])
									<span class="mar-r-4">
													<del>@price($data['venue']['worthKid'])</del>
												</span>
								@endif
								<span>@price($data['venue']['priceKid'])*</span>
								</span>
						</li>
						@if($data['venue']['priceAdult'])
							<li class="item">
								<span class="material-icons hall-product-details-item-icon">&#xE87C;</span>
								<span class="hall-product-details-item-text">
									<span>Per Adult: </span>
									@if ($data['venue']['worthAdult'] && $data['venue']['worthAdult'] > $data['venue']['priceAdult'])
										<span class="mar-r-4"><del>@price($data['venue']['worthAdult'])</del></span>
									@endif
									<span>@price($data['venue']['priceAdult'])*</span>
									</span>
							</li>
						@endif
					@endif
					<li class="item item-66 w-100__400">
						<span class="material-icons hall-product-details-item-icon">&#xE561;</span>
						<span class="hall-product-details-item-text">
							<span>Cuisines: </span>
							<span>{{ $data['venue']['cuisines'] }}</span>
							</span>
					</li>
				</ul>
			</div>
		</div>
		<hr class="desktop-profile-sub-hr">
		<div class="desc-component-wrap mar-b-20 pad-t-10" id="amenities">
			<div class="desc-component-title">
				Amenities
			</div>
			<div class="hall-desc-component-content">
				<ul class="clearfix product-details-item-list hall-product-details-item-list mar-t-15">
					<li class="item">
						<span class="material-icons hall-product-details-item-icon">&#xE7FB;</span>
						<span class="hall-product-details-item-text">
								<span class="title">Capacity: </span>
								<span> {{ $data['hall']['capacityMin'] }} - {{ $data['hall']['capacityMax'] }}</span>
								@if ($data['hall']['capacityFloat'])
								<span> (floating {{ $data['hall']['capacityFloat'] }})</span>
							@endif
							</span>
					</li>
					<li class="item @if (!$data['hall']['hasAc']) disabled @endif">
							<span class="material-icons hall-product-details-item-icon">
								@if ($data['hall']['hasAc'])
									&#xE332;
								@else
									block
								@endif
							</span>
						<span class="hall-product-details-item-text">
							<span>
								@if ($data['hall']['hasAc'])
									AC
								@else
									No AC
								@endif
							</span>
							</span>
					</li>
					<li class="item @if (!$data['venue']['hasValetParking']) disabled @endif">
							<span class="material-icons hall-product-details-item-icon">
								@if($data['venue']['hasValetParking'])
									&#xE54F;
								@else
									block
								@endif
							</span>
						<span class="hall-product-details-item-text">
							@if($data['venue']['hasValetParking'])
								<span>Valet Parking</span>
							@else
								<span>No Valet Parking</span>
							@endif
							</span>
					</li>
					<li class="item @if (!$data['venue']['carParkCount']) disabled @endif">
							<span class="material-icons hall-product-details-item-icon">
								@if($data['venue']['carParkCount'])
									&#xE531;
								@else
									block
								@endif
							</span>
						<span class="hall-product-details-item-text">
							@if($data['venue']['carParkCount'])
								<span>Car Parking:</span>
								<span> {{ $data['venue']['carParkCount'] }}</span>
							@else
								<span> No Car Parking</span>
							@endif
							</span>
					</li>
					<li class="item @if (!$data['venue']['hasLift']) disabled @endif">
							<span class="material-icons hall-product-details-item-icon">
								@if($data['venue']['hasLift'])
									&#xE89D;
								@else
									block
								@endif
							</span>
						<span class="hall-product-details-item-text">
							<span>
								@if($data['venue']['hasLift'])
									Lift
								@else
									No Lift
								@endif
							</span>
							</span>
					</li>
					<li class="item @if (!$data['venue']['hasSoundSystem']) disabled @endif">
							<span class="material-icons hall-product-details-item-icon">
								@if($data['venue']['hasSoundSystem'])
									&#xE049;
								@else
									block
								@endif
							</span>
						<span class="hall-product-details-item-text">
								@if($data['venue']['hasSoundSystem'])
								<span>Sound System</span>
								@if ($data['venue']['hasSoundSystem'] && $data['venue']['priceSoundSystem'])
									<span> (add @price($data['venue']['priceSoundSystem']))</span>
								@endif
							@else
								<span>No Sound System</span>
							@endif
							</span>
					</li>
					<li class="item @if (!$data['venue']['hasMike']) disabled @endif">
							<span class="material-icons hall-product-details-item-icon">
								@if($data['venue']['hasMike'])
									&#xE029;
								@else
									block
								@endif
							</span>
						<span class="hall-product-details-item-text">
								@if ($data['venue']['hasMike'])
								<span>Mike</span>
								@if ($data['venue']['hasMike'] && $data['venue']['priceMike'])
									<span> (add @price($data['venue']['priceMike']))</span>
								@endif
							@else
								<span>No Mike</span>
							@endif
							</span>
					</li>
					<li class="item @if(!$data['venue']['hasProjector']) disabled @endif">
							<span class="material-icons hall-product-details-item-icon">
								@if($data['venue']['hasProjector'])
									&#xE04B;
								@else
									block
								@endif
							</span>
						<span class="hall-product-details-item-text">
								@if($data['venue']['hasProjector'])
								<span>Projector</span>
								@if ($data['venue']['hasProjector'] && $data['venue']['priceProjector'])
									<span>(add @price($data['venue']['priceProjector']))</span>
								@endif
							@else
								<span>No Projector</span>
							@endif
							</span>
					</li>
					<li class="item @if(!$data['venue']['isOutsideDecorsAllowed']) disabled @endif">
							<span class="material-icons hall-product-details-item-icon">
								@if($data['venue']['isOutsideDecorsAllowed'])
									&#xE545;
								@else
									block
								@endif
							</span>
						<span class="hall-product-details-item-text">
								@if($data['venue']['isOutsideDecorsAllowed'])
								<span>Decorators Allowed</span>
								@if ($data['venue']['isOutsideDecorsAllowed'] && $data['venue']['priceOutsideDecorsAllowance'])
									<span>(add @price($data['venue']['priceOutsideDecorsAllowance']))</span>
								@endif
							@else
								<span>No Outside Decorators Allowed</span>
							@endif
							</span>
					</li>
				</ul>
			</div>
		</div>
		<hr class="desktop-profile-sub-hr">
		<div class="desc-component-wrap mar-b-20" id="location">
			<div class="desc-component-title">
				Location
			</div>
			<div class="hall-desc-component-content mar-t-15 pad-b-10">
				@if(isset($data['venue']['area']) && $data['venue']['area'])
					<div class="product-description-tab-location">
						Neighbourhood: {{ $data['venue']['area'] }}
					</div>
				@endif
				@if(isset($data['venue']['landmark']) && $data['venue']['landmark'])
					<div class="product-description-tab-landmark mar-t-5">
						Landmark: {{ $data['venue']['landmark'] }}
					</div>
				@endif
				<div class="flex-box mar-t-5">
					@if(isset($data['venue']['fullAddress']) && $data['venue']['fullAddress'])
						<div class="product-description-tab-address">
							{{ $data['venue']['fullAddress'] }}
						</div>
					@endif
					@if(isset($data['venue']['mapLat']) && $data['venue']['mapLat'] && isset($data['venue']['mapLong']) && $data['venue']['mapLong'] && isset($data['venue']['mapLink']) && $data['venue']['mapLink'])
						<a href="{{ $data['venue']['mapLink'] }}" class="product-description-tab-direction-btn">
							<span class="material-icons">map</span>
						</a>
					@endif
				</div>
			</div>
		</div>
		@if (count($data['extRatings']['ratings']))
			<hr class="desktop-profile-sub-hr">
			<div class="desc-component-wrap mar-b-20 pad-t-10" id="ratings">
				<div class="desc-component-title">
					Ratings across web
				</div>
				<div class="hall-desc-component-content mar-t-15">
					<div class="product-description-tab-rating-wrap flex-box product-rating">
						<div class="rating-number">{{ round($data['extRatings']['avgRating'], 1) }}</div>
						<div class="">
							<input type="number" class="avg-rating hide" value="{{ $data['extRatings']['avgRating'] }}" title="{{ $data['extRatings']['avgRating'] }} average rating"/>
							<div class="rating-help-text font-12">(across web)</div>
						</div>
					</div>
					<div class="product-description-tab-others-rating mar-t-15">
						@foreach ($data['extRatings']['ratings'] as $rating)
							<div class="product-description-tab-others-rating-warp">
								<a href="{{ $rating['type']['url'] }}" target="_blank" rel="noopener">
									<div class="flex-box">
										<div class="rating-website-img-wrap">
											<img src="{{ $galleryUrl }}/img/logo/{{ $rating['type']['image_url'] }}" alt="ratings on {{ $rating['type']['name'] }}"/>
										</div>
										<div>
											<div class="rating-text">
												<span class="rating-number">{{ $rating['rating_value']}} / {{ $rating['rating_max'] }}</span>
												<span class="ratings-count"> - {{ $rating['rating_count'] }} mentions</span>
											</div>
											<div class="rating-website-text">{{ $rating['type']['name'] }}</div>
										</div>
									</div>
								</a>
							</div>
						@endforeach
					</div>
					<div class="text-muted mar-t-15 font-12 line-16">
						<u>Important:</u>
						<span> These ratings were last updated <b>1 day ago</b>. All the ratings are mentioned for the sole purpose of information only.</span>
					</div>
				</div>
			</div>
		@endif
	</div>
</div>