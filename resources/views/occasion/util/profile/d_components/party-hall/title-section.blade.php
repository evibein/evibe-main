<div class="title-section">
	<div class="title-section-wrap hall-title-section-wrap">
		<div class="product-title-text hall-product-title-text no-mar-b ls-normal">
			{{ $data['hall']['type'] }} at {{ $data['venue']['area'] }}
		</div>
		<div class="product-rating mar-t-10 in-blk hide">
			<div class="in-blk font-16">
				<div id="defaultRating" class="product-default-rating in-blk"></div>
				<div id="upProductAvgRating"></div>
			</div>
			<div id="defaultReview" class="product-default-reviews in-blk"></div>
			<div id="upProductReviewCount" class="in-blk"></div>
		</div>
		<div class="clearfix"></div>
		<div class="description-top des-description-scroll mar-t-5">
			<div class="product-highlights-wrap mar-b-15 hall-product-highlights-wrap">
				@if ($data['extRatings']['avgRating'])
					<div class="product-highlight-item font-14">
						<div class="in-blk product-rating-number">{{ round($data['extRatings']['avgRating'],1) }}</div>
						<div class="in-blk product-rating mar-l-5 valign-bottom">
							<input type="number" class="avg-rating hide" value="{{ $data['extRatings']['avgRating'] }}" title="{{ $data['extRatings']['avgRating'] }} average rating"/>
						</div>
						<div class="in-blk mar-l-5">(across web)</div>
						<div class="clearfix"></div>
					</div>
				@endif
				@if($data['hall']['code'])
					<div class="product-highlight-item hide">
						<div class="product-highlight-item-title in-blk">
							<i class="glyphicon glyphicon-th-list mar-r-3"></i> Party Hall Code:
						</div>
						<div class="product-highlight-item-text in-blk mar-l-5">
							{{ $data['hall']['code'] }}
						</div>
						<div class="clearfix"></div>
					</div>
				@endif
				@if ($data['hall']['capacityMin'] && $data['hall']['capacityMax'])
					<div class="product-highlight-item">
						<div class="product-highlight-item-title in-blk">
							<i class="material-icons valign-bottom font-20-imp mar-r-2">&#xE7FB;</i> Capacity:
						</div>
						<div class="product-highlight-item-text in-blk mar-l-5">
							{{ $data['hall']['capacityMin'] }} - {{ $data['hall']['capacityMax'] }}
							@if ($data['hall']['capacityFloat'] && $data['hall']['capacityMax'] != $data['hall']['capacityFloat'])
								<span class="mar-l-5">(floating {{ $data['hall']['capacityFloat'] }})</span>
							@endif
						</div>
						<div class="clearfix"></div>
					</div>
				@endif
				<div class="product-highlight-item">
					@if($data['venue']['priceMinRent'] || $data['hall']['priceMinRent'])
						<div class="product-highlight-item-title in-blk">
							@if($data['hall']['minRentDuration'])
								<i class="material-icons valign-bottom font-20-imp mar-r-2">&#xE870;</i> Rent ({{ $data['hall']['minRentDuration'] }} Hrs):
							@elseif(!$data['hall']['minRentDuration'] && $data['venue']['minRentDuration'])
								<i class="material-icons valign-bottom font-20-imp mar-r-2">&#xE870;</i> Rent ({{ $data['venue']['minRentDuration'] }} Hrs):
							@else
							@endif
						</div>
						<div class="product-highlight-item-text in-blk mar-l-5">
							@if ($data['hall']['priceMinRent'])
								@if ($data['hall']['worthRent'] && $data['hall']['worthRent'] > $data['hall']['priceMinRent'])
									<span class="mar-r-4 text-price-worth"><del>@price($data['hall']['worthRent'])</del></span>
								@endif
								<span class="text-price">@price($data['hall']['priceMinRent'])</span>
								@if ($data['hall']['priceMaxRent'] && $data['hall']['priceMaxRent'] > $data['hall']['priceMinRent'])
									<span> - </span>
									<span class="text-price">@price($data['hall']['priceMaxRent'])*</span>
								@else
									<span class=""> * onwards</span>
								@endif
							@elseif (!$data['hall']['priceMinRent'] && $data['venue']['priceMinRent'])
								@if ($data['venue']['worthRent'] && $data['venue']['worthRent'] > $data['venue']['priceMinRent'])
									<span class="mar-r-4 text-price-worth"><del>@price($data['venue']['worthRent'])</del></span>
								@endif
								<span class="text-price">@price($data['venue']['priceMinRent'])</span>
								@if ($data['venue']['priceMaxRent'] && $data['venue']['priceMaxRent'] > $data['venue']['priceMinRent'])
									<span> - </span>
									<span class="text-price">@price($data['venue']['priceMaxRent'])*</span>
								@else
									<span class=""> * onwards</span>
								@endif
							@endif
						</div>
					@elseif($data['hall']['priceMinVeg'])
						<div class="in-blk"> <!-- serves food -->
							<div class="product-highlights-img-wrap valign-bottom in-blk">
								<img src="{{ $galleryUrl }}/img/icons/venue-facilities/green_veg.png" alt="veg price">
							</div>
							<div class="in-blk mar-l-5">
								@if ($data['hall']['worthVeg'] && $data['hall']['worthVeg'] > $data['hall']['priceMinVeg'])
									<span class="mar-r-4 text-price-worth"><del>@price($data['hall']['worthVeg'])</del></span>
								@endif
								<span class="text-price">@price($data['hall']['priceMinVeg'])</span>
								<span class=""><span class="text-e">*</span> onwards</span>
							</div>
						</div>
						@if ($data['hall']['priceMinNonveg'])
							<div class="mar-t-15"> <!-- serves food -->
								<div class="product-highlights-img-wrap valign-bottom in-blk">
									<img src="{{ $galleryUrl }}/img/icons/venue-facilities/red_nonveg.png" alt="non veg price">
								</div>
								<div class="in-blk mar-l-5">
									@if ($data['hall']['worthVeg'] && $data['hall']['worthNonVeg'] > $data['hall']['priceMinNonveg'])
										<span class="mar-r-4 text-price-worth"><del>@price($data['hall']['worthNonVeg'])</del></span>
									@endif
									<span class="text-price">@price($data['hall']['priceMinNonveg'])</span>
									<span class=""><span class="text-e">*</span> onwards</span>
								</div>
							</div>
						@else
							<div class="in-blk mar-l-5">
								<span class="">(Pure Vegetarian)</span>
							</div>
						@endif
					@elseif($data['venue']['priceMinVeg'])
						<div class="in-blk"> <!-- serves food -->
							<div class="product-highlights-img-wrap valign-bottom in-blk">
								<img src="{{ $galleryUrl }}/img/icons/venue-facilities/green_veg.png" alt="veg price">
							</div>
							<div class="in-blk mar-l-5">
								@if ($data['venue']['worthVeg'] && $data['venue']['worthVeg'] > $data['venue']['priceMinVeg'])
									<span class="mar-r-4 text-price-worth"><del>@price($data['venue']['worthVeg'])</del></span>
								@endif
								<span class="text-price">@price($data['venue']['priceMinVeg'])</span>
								<span class=""><span class="text-e">*</span> onwards</span>
							</div>
						</div>
						@if ($data['venue']['priceMinNonveg'])
							<div class="mar-t-15"> <!-- serves food -->
								<div class="product-highlights-img-wrap valign-bottom in-blk">
									<img src="{{ $galleryUrl }}/img/icons/venue-facilities/red_nonveg.png" alt="non veg price">
								</div>
								<div class="in-blk mar-l-5">
									@if ($data['venue']['worthVeg'] && $data['venue']['worthNonVeg'] > $data['venue']['priceMinNonveg'])
										<span class="mar-r-4 text-price-worth"><del>@price($data['venue']['worthNonVeg'])</del></span>
									@endif
									<span class="text-price">@price($data['venue']['priceMinNonveg'])</span>
									<span class=""><span class="text-e">*</span> onwards</span>
								</div>
							</div>
						@else
							<div class="in-blk mar-l-5">
								<span class="">(Pure Vegetarian)</span>
							</div>
						@endif
					@elseif($data['venue']['priceKid'])
						<div class="in-blk"> <!-- serves food -->
							@if ($data['venue']['worthKid'] && $data['venue']['worthKid'] > $data['venue']['priceKid'])
								<span class="mar-r-4 text-price-worth"><del>@price($data['venue']['worthKid'])</del></span>
							@endif
							<span class="text-price">@price($data['venue']['priceKid'])</span>
							<span class=""><span class="text-e">*</span> (per Kid)</span>
						</div>
					@else
					@endif
				</div>
			</div>
			<div class="text-center mar-t-10 mar-b-25">
				<div class="btn btn-hall-best-deal" data-toggle="modal" data-target="#hallEnquiryModal">Get Best Deal</div>
			</div>
			<div class="description-learn-more">
				Learn More:
				<a class="scroll-item text-underline mar-l-5" data-scroll="aboutVenue">About Venue,</a>
				<a class="scroll-item text-underline mar-l-5" data-scroll="amenities">Amenities,</a>
				@if ($data['venue']['mapLat'] && $data['venue']['mapLong'])
					<a class="scroll-item text-underline mar-l-5" data-scroll="location">Location,</a>
				@endif
				@if (count($data['extRatings']['ratings']))
					<a class="scroll-item text-underline mar-l-5" data-scroll="ratings">Ratings</a>
				@endif
			</div>
		</div>
		<div class="description-show-more mar-t-5">
			<a class="scroll-item hide" data-scroll="fullDescription">
				<i class="material-icons">&#xE5DB;</i> <span class="text-underline">see more</span>
			</a>
		</div>
	</div>
</div>