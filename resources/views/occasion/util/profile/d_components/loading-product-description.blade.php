<div id="loadingProductDescription">
	@php $i = 0; @endphp
	@while($i < 3)
		<div class="loading-component-title-wrap">
			<div class="loading-component-title">

			</div>
		</div>
		<div class="loading-component-content-wrap mar-b-10">
			<div class="loading-component-content">

			</div>
		</div>
		@php $i++; @endphp
	@endwhile
</div>