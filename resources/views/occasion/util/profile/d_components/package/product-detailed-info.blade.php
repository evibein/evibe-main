<div class="product-description-section">
	<div id="fullDescription" class="product-description-wrap">
		@if ($data['package']->about_package)
			<div class="desc-component-wrap mar-b-20" id="aboutPackage">
				<div class="desc-component-title">
					@if($data['package']->map_type_id == config('evibe.ticket.type.venue'))
						About Venue
					@else
						About Experience
					@endif
				</div>
				<div class="desc-component-content">
					{!! $data['package']->about_package !!}
				</div>
			</div>
		@endif
		@if ($data['package']->info)
			<div class="desc-component-wrap mar-b-20" id="inclusions">
				<div class="desc-component-title">
					Inclusions
				</div>
				<div class="desc-component-content">
					{!! $data['package']->info !!}
				</div>
			</div>
		@endif
		@if(isset($data['addOns']) && count($data['addOns']))
			<div id="addOns">
				@include('occasion.util.add-ons.desktop-list', ['addOns' => $data['addOns'], 'occasionId' => $data['occasionId']])
			</div>
		@endif
		@if(isset($data['occasionId']) && ($data['occasionId'] == config('evibe.occasion.surprises.id')))
			<div class="desc-component-wrap mar-b-20" id="faqs">
				<div class="desc-component-title">
					Frequently Asked Questions
				</div>
				<div class="desc-component-content">
					<div class="product-faq-content-overflow">
						@include('occasion.util.faq.list.d')
					</div>
					<div class="text-center mar-t-20">
						<a class="product-faq-btn-overflow" href="#">+ Show More FAQs</a>
					</div>
				</div>
			</div>
		@endif
		@if($data['package']->prerequisites || $data['package']->facts)
			@if(isset($data['occasionId']) && ($data['occasionId'] == config('evibe.occasion.surprises.id')) && isset($data['tags']) && count($data['tags']) && !isset($data['tags'][config('evibe.surprise-relation-tags.candle-light-dinner')]))

			@else
				<div class="desc-component-wrap mar-b-20" id="thingsToRemember">
					<div class="desc-component-title">
						Things To Remember
					</div>
					<div class="desc-component-content">
						@if($data['package']->prerequisites)
							{!! $data['package']->prerequisites !!}
						@endif
						@if($data['package']->facts)
							{!! $data['package']->facts !!}
						@endif
					</div>
				</div>
			@endif
		@endif
		@include("occasion.util.profile.d_components.loading-reviews-section")
		<div id="showProductReviews"></div>
		@if($data['package']->terms)
			<div class="desc-component-wrap mar-b-20 pad-t-10 pad-b-10" id="terms">
				<div class="desc-component-title">
					Terms of Booking
				</div>
				<div class="desc-component-content desc-terms-content">
					{!! $data['package']->terms !!}
				</div>
			</div>
		@endif
		@if(isset($data['cancellationData']) && !is_null($data['cancellationData']))
			@include('occasion.util.profile.d_components.cancellation-policy')
		@endif
	</div>
</div>