<div class="product-reviews-section">
	<div id="fullDescription" class="product-reviews-wrap">
		@if(count($data['deliveryImages']))
			@include("occasion.util.profile.c_components.customer-images",[
				"deliveryImages" => $data['deliveryImages'],
			])

		@endif
		@if($data['ratings']['total']['count'])
			<div id="reviewSection" class="desc-component-wrap mar-b-20">
				<div class="desc-component-title">
					Customer Reviews
					<small><i class="hide">(also caters to other occasions)</i></small>
				</div>
				<input type="hidden" id="hidProductAvgRating" value="{{ $data['ratings']['total']['avg'] }}">
				<input type="hidden" id="hidProductReviewCount" value="{{ $data['ratings']['total']['count'] }}">
				<input type="hidden" id="hidProductName" value="{{ $data['name'] }}">
				<div class="desc-component-content reviews-card">
					@include("app.review.partial", [
				"reviews"=> $data["ratings"],
				"isShowLocation" => ($data["product"]->map_type_id == config("evibe.ticket.type.venue")) ? false : true,
				"showAllReviewsUrl" => route("provider-reviews:all", [
									getSlugFromText($data["product"]->name),
									($data["product"]->map_type_id == config("evibe.ticket.type.venue") ? config("evibe.ticket.type.venue") : config("evibe.ticket.type.planner")),
									$data["product"]->provider->id
								])
			])
				</div>
			</div>
		@endif
	</div>
</div>

@section('seo:schema')
	@parent
	@if(isset($data['seo']['schema']) && count($data['seo']['schema']))
		@foreach($data['seo']['schema'] as $schema)
			{!! $schema !!}
		@endforeach
	@endif
@endsection