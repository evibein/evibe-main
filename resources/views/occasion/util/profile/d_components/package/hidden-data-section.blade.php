@if(isset($data['package']))
	@if(isset($data['mapTypeId']) && isset($data['occasionId']))
		<input type="hidden" id="hidSimilarProductsUrl" value="{{ route('ajax.profile.similar-products', [$data['package']->id, $data['mapTypeId'], $data['occasionId']]) }}">
		<input type="hidden" id="hidMostViewedProductsUrl" value="{{ route('ajax.profile.most-viewed-products', [$data['package']->id, $data['mapTypeId'], $data['occasionId']]) }}">
		<input type="hidden" id="hidProductReviewsUrl" value="{{ route('ajax.profile.product-reviews', [$data['package']->id, $data['mapTypeId']]) }}">
		@if(!is_null(request()->get('cat')))
			<input type="hidden" id="hidOtherCategoryCardsUrl" value="{{ route('ajax.profile.other-profile-cards', [$data['package']->id, $data['mapTypeId'], $data['occasionId'],request()->get('cat')]) }}">
		@endif
	@endif

	@if($data['package']->price)
		<input type="hidden" id="hidProductPrice" value="{{ $data['package']->price }}">
	@endif

	@if($data['package']->code)
		<input type="hidden" id="hidProductCode" value="{{ $data['package']->code }}">
	@endif

	@if($data['package']->id)
		<input type="hidden" id="hidProductId" value="{{ $data['package']->id }}">
	@endif

	@if($data['package']->type_ticket_id)
		<input type="hidden" id="hidProductTypeId" value="{{ $data['package']->type_ticket_id }}">
	@endif
@endif

@if(isset($data['occasionId']))
	@if(isset($data['package']) && $data['package']->id)
		<input type="hidden" id="hidProfileEnqUrl" value="{{ route('ticket.package', [$data['occasionId'], $data['package']->id]) }}">
	@endif
@endif

@if(isset($data['typeDevice']))
	<input type="hidden" id="hidTypeDevice" value="{{ $data['typeDevice'] }}">
@endif

@if(isset($data['typeBrowser']))
	<input type="hidden" id="hidTypeBrowser" value="{{ $data['typeBrowser'] }}">
@endif

@if(isset($data['pageName']))
	<input type="hidden" id="hidPageName" value="{{ $data['pageName'] }}">
@endif

@if(isset($data['mapTypeId']))
	<input type="hidden" id="hidForVenueDetails" value="{{ $data['mapTypeId'] }}">
@endif

@if(isset($data['unavailableDates']) && count($data['unavailableDates']))
	@foreach($data['unavailableDates'] as $date)
		<input type="hidden" class="package-unavailable-date unavailable-date-{{ $date }}" value="{{ $date }}">
	@endforeach
@endif

@if(isset($data['campaignDates']) && count($data['campaignDates']))
	@foreach($data['campaignDates'] as $date)
		<input type="hidden" class="package-campaign-date campaign-date-{{ $date }}" value="{{ $date }}">
	@endforeach
@endif

@if(isset($data['cityId']))
	<input type="hidden" id="hidCityId" value="{{ $data['cityId'] }}">
@endif

@if(isset($data['occasionId']))
	<input type="hidden" id="hidOccasionId" value="{{ $data['occasionId'] }}">
@endif

@include('occasion.util.profile.c_components.package.hidden-data-section')