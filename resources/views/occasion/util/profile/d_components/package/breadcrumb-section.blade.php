@if (request()->is('*/candle-light-dinner*'))
	<div class="product-breadcrumb-section mar-t-5">
		<ol class="breadcrumb product-breadcrumb text-center__400">
			<li><a href="{{ route('city.home', $cityUrl) }}">{{$cityName}}</a></li>
			<li><a href="{{ route('city.cld.list', $cityUrl) }}">Candle Light Dinners</a></li>
			<li class="active">{{ $data['package']->code }}</li>
		</ol>
	</div>
@elseif (request()->is('*/bachelor-party*'))
	<div class="product-breadcrumb-section mar-t-5">
		<ol class="breadcrumb product-breadcrumb text-center__400">
			<li><a href="{{ route('city.home', $cityUrl) }}">{{$cityName}}</a></li>
			<li><a>{{ config('evibe.type-event-title.'.$data['occasionId']) }}</a></li>
			<li>
				<a href="{{ route('city.occasion.'.config('evibe.type-event-route-name.'.$data['occasionId']).'.'.config('evibe.type-product-route-name.'.$data['mapTypeId']).'.list', [$cityUrl]) }}">{{ config('evibe.type-product-title.'.$data['mapTypeId']) }}</a>
			</li>
			<li class="active">{{ $data['package']->code }}</li>
		</ol>
	</div>
@elseif (!request()->is('*/engagement-wedding-reception*'))
	<div class="product-breadcrumb-section mar-t-5">
		<ol class="breadcrumb product-breadcrumb text-center__400">
			<li><a href="{{ route('city.home', $cityUrl) }}">{{$cityName}}</a></li>
			<li><a href="{{ route('city.occasion.'.config('evibe.type-event-route-name.'.$data['occasionId']).'.home', $cityUrl) }}">{{ config('evibe.type-event-title.'.$data['occasionId']) }}</a></li>
			<li>
				<a href="{{ route('city.occasion.'.config('evibe.type-event-route-name.'.$data['occasionId']).'.'.config('evibe.type-product-route-name.'.$data['mapTypeId']).'.list', [$cityUrl]) }}">{{ config('evibe.type-product-title.'.$data['mapTypeId']) }}</a>
			</li>
			<li class="active">{{ $data['package']->code }}</li>
		</ol>
	</div>
@else
	<div class="product-breadcrumb-section mar-t-5">
		<ol class="breadcrumb product-breadcrumb text-center__400">
			<li><a>{{$cityName}}</a></li>
			<li><a>{{ config('evibe.type-event-title.'.$data['occasionId']) }}</a></li>
			<li>
				<a>{{ config('evibe.type-product-title.'.$data['mapTypeId']) }}</a>
			</li>
			<li class="active">{{ $data['package']->code }}</li>
		</ol>
	</div>
@endif