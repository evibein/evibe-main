<div class="gallery-wrap desktop-gallery hide" role="tabpanel">
@if (isset($data['gallery']['showGalleryTabs']) && $data['gallery']['showGalleryTabs']) <!-- nav tabs begin -->
	<ul class="nav nav-pills mar-l-20" role="tablist">
		<?php $count = 0; ?>
		@foreach($data['gallery']['images'] as $cat => $catImage)
			<li role="presentation" class="@if(!$count++) active @endif">
				<a href="#catGalId{{ $count }}" data-toggle="pill">
					{{ $cat }}
				</a>
			</li>
		@endforeach
	</ul>
	@endif
	@if (count($data['gallery']['images']))
		<div class="tab-content mar-t-15"> <!-- tab panes begin -->
			@php
				$count = 0;
				$explainerImage = false;
			@endphp
			@foreach($data['gallery']['images'] as $cat => $catImage)
				<div role="tabpanel" class="tab-pane fade @if(!$count++) in active @endif" id="catGalId{{ $count }}">
					@if (count($catImage))
						<div class="fotorama" data-width="100%" data-allowfullscreen="true" data-ratio="900/600" data-maxheight="400" data-nav="thumbs" data-autoplay="true" data-thumbwidth="80" data-thumbmargin="10" data-keyboard="true" data-captions="true" @if(isset($data['gallery']['showThumb']) && $data['gallery']['showThumb']) data-enableifsingleframe="true" @endif>
							<a href="{{ $catImage['url'] }}">
								<img src="{{ $catImage['thumb'] }}" alt="{{ $catImage['title'] }}"/>
							</a>
							@if(!$explainerImage)
								<a href="{{ $galleryUrl }}/main/img/explainer-image_profile.png">
									<img src="{{ $galleryUrl }}/main/img/explainer-image_thumb.png" alt="Lifelong Memories Guaranteed"/>
								</a>
								@php $explainerImage = true; @endphp
							@endif
						</div>
					@else
						<i class="text-danger">No images found in this category.</i>
					@endif
				</div>
			@endforeach
		</div>
	@else
		<div><img class="des-profile-no-image" src="{{ $galleryUrl }}/img/icons/gift.png"></div>
	@endif
</div>
