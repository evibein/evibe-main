<div class="title-section">
	<div class="title-section-wrap">
		<h1 class="product-title-text no-mar-b no-mar-t ls-normal">
			{{ $data['name'] }}
		</h1>
		<div class="product-rating mar-t-10 in-blk">
			<div class="in-blk font-16">
				<div id="defaultRating" class="product-default-rating in-blk"></div>
				<div id="upProductAvgRating"></div>
			</div>
			<div id="defaultReview" class="product-default-reviews in-blk"></div>
			<div id="upProductReviewCount" class="in-blk"></div>
		</div>
		<div class="clearfix"></div>
		<div class="description-top des-description-scroll mar-t-15">
			<div class="product-highlights-wrap mar-b-15">
				@if(isset($data['code']) && $data['code'])
					<div class="product-highlight-item">
						<span class="product-highlight-item-title">
							<i class="glyphicon glyphicon-th-list"></i> Cake Code:
						</span>
						<span class="product-highlight-item-text mar-l-5">
							{{ $data['code'] }}
						</span>
						<div class="clearfix"></div>
					</div>
				@endif
				@if(isset($data['flavour']) && $data['flavour'])
					<div class="product-highlight-item">
						<span class="product-highlight-item-title">
							<i class="glyphicon glyphicon-ice-lolly"></i> Best Flavour:
						</span>
						<span class="product-highlight-item-text mar-l-5">
							{{ $data['flavour'] }}
						</span>
						<div class="clearfix"></div>
					</div>
				@endif
				@if (isset($data['highlights']) && count($data['highlights']))
					@foreach($data['highlights'] as $highlightItem)
						<div class="product-highlight-item">
						<span class="product-highlight-item-title">
							<i class="{{ $highlightItem['icon'] }}"></i> {{ $highlightItem['name'] }}:
						</span>
							<span class="product-highlight-item-text mar-l-5">
								{{ $highlightItem['value'] }}
							</span>
							<div class="clearfix"></div>
						</div>
					@endforeach
				@endif
			</div>
			<div class="description-learn-more">
				Learn More:
				@if ($data['info'])
					<a class="scroll-item text-underline mar-l-5" data-scroll="inclusions">Inclusions,</a>
				@endif
				<a class="scroll-item text-underline mar-l-5" data-scroll="deliveryCharges">Delivery Charges,</a>
				<a class="scroll-item text-underline mar-l-5 customer-reviews-ref-link hide" data-scroll="reviewSection">Customer Reviews,</a>
				@if(isset($data['addOns']) && count($data['addOns']))
					<a class="scroll-item text-underline mar-l-5" data-scroll="addOns">Add Ons,</a>
				@endif
				@if(isset($data['cancellationData']) && !is_null($data['cancellationData']))
					<a class="scroll-item text-underline mar-l-5" data-scroll="cancellationsAndRefunds">Cancellations & Refunds</a>
				@endif
			</div>
		</div>
		<div class="description-show-more mar-t-5">
			<a class="scroll-item hide" data-scroll="fullDescription">
				<i class="material-icons">&#xE5DB;</i> <span class="text-underline">see more</span>
			</a>
		</div>
	</div>
</div>