@if($data)
	@if(isset($data['mapTypeId']) && isset($data['occasionId']))
		<input type="hidden" id="hidSimilarProductsUrl" value="{{ route('ajax.profile.similar-products', [$data['id'], $data['mapTypeId'], $data['occasionId']]) }}">
		<input type="hidden" id="hidMostViewedProductsUrl" value="{{ route('ajax.profile.most-viewed-products', [$data['id'], $data['mapTypeId'], $data['occasionId']]) }}">
		<input type="hidden" id="hidProductReviewsUrl" value="{{ route('ajax.profile.product-reviews', [$data['id'], $data['mapTypeId']]) }}">
	@endif
@endif

@if(isset($data['occasionId']))
	@if(isset($data['id']))
		<input type="hidden" id="hidProfileEnqUrl" value="{{ route('ticket.cake', [$data['occasionId'], $data['id']]) }}">
	@endif
@endif

@if(isset($data['mapTypeId']))
	<input type="hidden" id="hidForVenueDetails" value="{{ $data['mapTypeId'] }}">
@endif

@if(isset($data['typeDevice']))
	<input type="hidden" id="hidTypeDevice" value="{{ $data['typeDevice'] }}">
@endif

@if(isset($data['typeBrowser']))
	<input type="hidden" id="hidTypeBrowser" value="{{ $data['typeBrowser'] }}">
@endif

@if(isset($data['pageName']))
	<input type="hidden" id="hidPageName" value="{{ $data['pageName'] }}">
@endif

@if(isset($data['code']))
	<input type="hidden" id="hidProductCode" value="{{ $data['code'] }}">
@endif

@if(isset($data['base_price']))
	<input type="hidden" id="hidProductPrice" value="{{ $data['base_price'] }}">
@endif
