<div class="price-section">
	<div class="price-section-wrap">
		<div class="product-profile-form-wrap">
			<div id="productProfileForm" class="product-profile-form pad-t-5 pad-b-15">
				<div class="product-price mar-t-10">
					<div class="product-price-wrap-2 text-center">
						<div class="product-price-old cake-profile">
							<div class="card-item price-item cake-price-cnt text-center">
								<div class="price-content hide">
									<div class="product-price-worth price-worth price-worth-cake in-blk">
										<span class='rupee-font'>&#8377;</span>
										<span class="displayWorth">{{ $data['price_worth'] }}</span>
									</div>
									<div class="product-price-off price-off mar-l-5 in-blk">
										(<span class='rupee-font'>&#8377;</span>
										<span class="displayPriceOff">{{ $data['price_worth'] - $data['base_price'] }}</span> OFF)
									</div>
									<div class="clearfix"></div>
									<div class="pad-t-10">
										<div class="product-main-price price-val in-blk">
											<span class='rupee-font'>&#8377;</span>
											<span class="displayPrice">{{ $data['base_price'] }}</span>
										</div>
										<div class="product-price-tag mar-l-5 in-blk">
											<i class="glyphicon glyphicon-thumbs-up"></i> Best Price
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
								<div class="loading-cnt product-price-load-wrap">
									<img src="{{ $galleryUrl }}/img/icons/fb_loading.png" alt="loading..." class="rotate">
								</div>
							</div>
							<div class="mar-t-5 font-12">
								<i>(price may vary based on <b>party location</b>)</i>
							</div>
							@if(isset($data['cakeAddOns']) && count($data['cakeAddOns']) > 0)
								<div class="mar-t-15">
									@foreach($data['cakeAddOns'] as $catId => $addOn)
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<div class="card-item cake-add-on-wrap mar-t-10" data-catid="{{ $catId }}" data-name="{{ $addOn['name'] }}">
												<div class="col-xs-5 col-sm-5 col-md-5">
													<div class="product-property-name mar-t-5">
														{{ $addOn['name'] }}
													</div>
												</div>
												<div class="col-xs-7 col-sm-7 col-md-7">
													<div class="product-property-value">
														<select name="addOnCat" class="form-control add-on-cat">
															@foreach($addOn['values'] as $categoryId => $addOnValue)
																<option value="{{ $addOnValue['id'] }}" data-price="{{ $addOnValue['price'] }}" data-worth="{{ $addOnValue['priceWorth'] }}" data-isfixed="{{ $addOnValue['isFixed'] }}" data-weight="{{ $addOnValue['weight'] }}" data-name="{{ $addOnValue['name'] }}" data-displayoption="@if(isset($addOnValue['identifier'])) {{ $addOnValue['identifier'] }}@endif">
																	@if(isset($addOnValue['identifier'])){{ $addOnValue['identifier'] }}@else{{ $addOnValue['name'] }}@endif
																</option>
															@endforeach
														</select>
													</div>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									@endforeach
									<div class="clearfix"></div>
								</div>
							@endif
						</div>
					</div>
				</div>
				<div class="product-cta pad-l-15 mar-t-15">
					<div class="col-md-12 col-lg-12 no-pad-l">
						<a class="btn btn-default product-btn-book-now btn-cake-book-now ga-btn-book-now">
							<i class="material-icons valign-mid">&#xE8CC;</i>
							<span class="valign-mid mar-l-4">Book Now</span>
						</a>
					</div>
					<div class="col-md-12 col-lg-12 no-pad-l">
						<div class="col-xs-7 mar-t-15 no-pad-l">
							<a id="shortlistBtn" class="btn btn-default product-btn-shortlist" v-on:click="shortlistOptionFromProfilePage">
								<span class="hideShortlistData" data-cityId="{{ $data['shortlistData']['cityId'] }}" data-occasionId="{{ $data['shortlistData']['occasionId'] }}" data-mapId="{{ $data['shortlistData']['mapId'] }}" data-mapTypeId="{{ $data['shortlistData']['mapTypeId'] }}" data-urlAdd="{{ route('partybag.add') }}" data-urlDelete="{{ route('partybag.delete') }}" data-plainImgWhite="{{ $galleryUrl }}/main/img/icons/party-bag_empty_w.png" data-removeImgGrey="{{ $galleryUrl }}/main/img/icons/party-bag_empty_g.png"></span>
								<i class="icon-bagicon5 product-shortlist-icon"></i>
							<!--<img src="{{ $galleryUrl }}/main/img/icons/party-bag_empty_w.png" alt="Party Bag List" class="partybag-detail-img">-->
								<span class="mar-l-2">Add To Cart</span>
							</a>
						</div>
						<div class="col-xs-5 mar-t-15 no-pad-r text-center font-16" style="border: solid 1px #D4D5D9; padding: 8px 0">
							<div data-city-id="{{ $data['shortlistData']['cityId'] }}" data-occasion-id="{{ $data['shortlistData']['occasionId'] }}" data-map-id="{{ $data['shortlistData']['mapId'] }}" data-map-type-id="{{ $data['shortlistData']['mapTypeId'] }}">
								<a class="wishlist-option fw-normal a-no-decoration-black cur-point" data-url="{{ route("wishlist.add") }}" data-status-url="{{ route("wishlist.status") }}"><i class="glyphicon glyphicon-bookmark" style="top: 2px;"></i> WISHLIST</a>
								<a class="wishlisted-option fw-normal a-no-decoration-black hide a-no-decoration-default"><i class="glyphicon glyphicon-bookmark" style="top: 2px;"></i> WISHLISTED</a>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div id="pPFormLoader" class="product-profile-form-loader text-center hide">
				<div class="profile-form-loader-image">
					<img src="/images/loading.gif" alt="">
				</div>
				<div class="profile-form-loader-text mar-t-5">Please wait..</div>
			</div>
		</div>
	</div>
</div>

<div class="mar-t-30 mar-b-30 text-center">
	<span>Need help?</span>
	<span><a class="product-link-enquire-now ga-btn-enquire-now popup-need-help-btn mar-l-5 ga-desktop-profile-enquire-link">Enquire Now</a></span>
</div>