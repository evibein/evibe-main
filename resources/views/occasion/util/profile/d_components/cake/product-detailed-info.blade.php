<div class="product-description-section">
	<div id="fullDescription" class="product-description-wrap">
		@if ($data['info'])
			<div class="desc-component-wrap mar-b-20" id="inclusions">
				<div class="desc-component-title">
					Inclusions
				</div>
				<div class="desc-component-content">
					{!! $data['info'] !!}
				</div>
			</div>
		@endif
		@include("occasion.util.profile.d_components.loading-reviews-section")
		<div id="showProductReviews"></div>
		<div class="desc-component-wrap mar-b-20 pad-t-10 pad-b-10" id="deliveryCharges">
			<div class="desc-component-title">
				Delivery Charges
			</div>
			<div class="desc-component-content desc-terms-content mar-t-10">
				<table class="table table-condensed table-bordered">
					<tr>
						<th>Slot (any day)</th>
						<th>Price</th>
					</tr>
					<tr>
						<td>7:00 AM - 10:00 AM</td>
						<td>@price(150)</td>
					</tr>
					<tr>
						<td>10:00 AM - 11:00 PM</td>
						<td>Free</td>
					</tr>
					<tr>
						<td>11:00 PM - 11:50 PM</td>
						<td>@price(150)</td>
					</tr>
				</table>
			</div>
		</div>
		@if(isset($data['addOns']) && count($data['addOns']))
			<div id="addOns">
				@include('occasion.util.add-ons.desktop-list', ['addOns' => $data['addOns'], 'occasionId' => $data['occasionId']])
			</div>
		@endif
		@if(isset($data['cancellationData']) && !is_null($data['cancellationData']))
			@include('occasion.util.profile.d_components.cancellation-policy')
		@endif
	</div>
</div>