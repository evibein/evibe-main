<div class="enquiry-card-section hide">
	<div class="enquiry-card-wrap text-center">
		<div class="product-enquiry-title">
			Have any questions?
		</div>
		<div class="product-enquiry-content mar-t-10 hide">
			<div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
				Let us know your queries and we will get back to you within 4 Business hours.
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="product-enquiry-btn mar-t-5">
			<button id="desProfileEnquiry" class="btn btn-default btn-enquiry ga-btn-enquire-now">
				<i class="material-icons valign-mid mar-r-5">&#xE0CB;</i>
				<span class="valign-mid">Enquire Now</span>
			</button>
		</div>
	</div>
</div>