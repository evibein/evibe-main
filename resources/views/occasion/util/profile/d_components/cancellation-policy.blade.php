<div class="desc-component-wrap mar-b-20 pad-t-10 pad-b-10" id="cancellationsAndRefunds">
	<div class="desc-component-title">
		Cancellations & Refunds
	</div>
	<div class="desc-component-content desc-terms-content mar-t-10">
		<div class="mar-t-10">In case of cancellation, refundable amount is calculated based on the following policies:</div>
		<table class="table table-condensed table-bordered mar-t-10">
			<tr>
				<th>Cancellation Time Before Party</th>
				<th>Refund Percentage*</th>
			</tr>
			@foreach($data['cancellationData'] as $cd)
				<tr>
					<td>@if($cd->min_day == 0)
							@if($cd->max_day == 0)
								0 - 24 hours before party
							@else
								{{ $cd->max_day }} days or below before party
							@endif
						@elseif($cd->max_day > 30)
							{{ $cd->min_day }} days or above before party
						@else
							{{ $cd->min_day }} days - {{ $cd->max_day }} days before party
						@endif
					</td>
					<td>{{ round($cd->refund_percent) }} %</td>
				</tr>
			@endforeach
		</table>
		<div class="mar-t-5">* Refund Percentage will be calculated on <b>Total Advance Amount</b></div>
	</div>
</div>