<div class="gallery-section">
	@if (isset($data['gallery']) && count($data['gallery']))
		<div class="loading-img-wrap">
			<img src="/images/loading.gif" alt="">
			<div class="pad-t-10">loading gallery...</div>
		</div>
		<div class="gallery-wrap desktop-gallery hide">
			<div class="fotorama" data-width="100%" data-allowfullscreen="true" data-ratio="900/600" data-maxheight="400" data-nav="thumbs" data-autoplay="true" data-thumbwidth="80" data-thumbmargin="10" data-keyboard="true">
				@foreach ($data['gallery'] as $gallery)
					<a href="{{ $gallery['url'] }}">
						<img src="{{ $gallery['thumb'] }}" alt="{{ $gallery['title'] }} in {{ getCityUrl() }}"/>
					</a>
				@endforeach
				<a href="{{ $galleryUrl }}/main/img/explainer-image_profile.png">
					<img src="{{ $galleryUrl }}/main/img/explainer-image_thumb.png" alt="Lifelong Memories Guaranteed"/>
				</a>
			</div>
		</div>
	@else
		<div><img class="des-profile-no-image" src="{{ $galleryUrl }}/img/icons/balloons.png"></div>
	@endif
</div>