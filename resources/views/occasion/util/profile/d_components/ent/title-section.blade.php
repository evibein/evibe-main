<div class="title-section">
	<div class="title-section-wrap">
		<h1 class="product-title-text no-mar-b no-mar-t ls-normal">
			{{ $data['name'] }}
		</h1>
		<div class="product-rating mar-t-10 in-blk hide">
			<div class="in-blk font-16">
				<div id="defaultRating" class="product-default-rating in-blk"></div>
				<div id="upProductAvgRating"></div>
			</div>
			<div id="defaultReview" class="product-default-reviews in-blk"></div>
			<div id="upProductReviewCount" class="in-blk"></div>
		</div>
		<div class="clearfix"></div>
		<div class="description-top des-description-scroll mar-t-15">
			<div class="product-highlights-wrap mar-b-15">
				@if(isset($data['code']) && $data['code'])
					<div class="product-highlight-item">
						<div class="product-highlight-item-title in-blk">
							<i class="glyphicon glyphicon-th-list"></i> Service Code:
						</div>
						<div class="product-highlight-item-text in-blk mar-l-5">
							{{ $data['code'] }}
						</div>
						<div class="clearfix"></div>
					</div>
				@endif
			</div>
			<div class="description-learn-more">
				Learn More:
				@if ($data['info'])
					<a class="scroll-item text-underline mar-l-5" data-scroll="inclusions">Inclusions,</a>
				@endif
				@if((isset($data['preq']) && $data['preq']) || (isset($data['facts']) && $data['facts']))
					<a class="scroll-item text-underline mar-l-5" data-scroll="thingsToRemember">Things To Remember,</a>
				@endif
				@if(isset($data['addOns']) && count($data['addOns']))
					<a class="scroll-item text-underline mar-l-5" data-scroll="addOns">Add Ons,</a>
				@endif
				@if(isset($data['terms']) && $data['terms'])
					<a class="scroll-item text-underline mar-l-5" data-scroll="terms">Terms,</a>
				@endif
				@if(isset($data['cancellationData']) && !is_null($data['cancellationData']))
					<a class="scroll-item text-underline mar-l-5" data-scroll="cancellationsAndRefunds">Cancellations & Refunds</a>
				@endif
			</div>
		</div>
		<div class="description-show-more mar-t-5">
			<a class="scroll-item hide" data-scroll="fullDescription">
				<i class="material-icons">&#xE5DB;</i> <span class="text-underline">see more</span>
			</a>
		</div>
	</div>
</div>