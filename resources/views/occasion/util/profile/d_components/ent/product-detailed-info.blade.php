<div class="product-description-section">
	<div id="fullDescription" class="product-description-wrap">
		@if ($data['info'])
			<div class="desc-component-wrap mar-b-20" id="inclusions">
				<div class="desc-component-title">
					Inclusions
				</div>
				<div class="desc-component-content">
					{!! $data['info'] !!}
				</div>
			</div>
		@endif
		@if(isset($data['addOns']) && count($data['addOns']))
			<div id="addOns">
				@include('occasion.util.add-ons.desktop-list', ['addOns' => $data['addOns'], 'occasionId' => $data['occasionId']])
			</div>
		@endif
		@if((isset($data['preq']) && $data['preq']) || (isset($data['facts']) && $data['facts']))
			<div class="desc-component-wrap mar-b-20" id="thingsToRemember">
				<div class="desc-component-title">
					Things To Remember
				</div>
				<div class="desc-component-content">
					@if(isset($data['preq']) && $data['preq'])
						{!! $data['preq'] !!}
					@endif
					@if(isset($data['facts']) && $data['facts'])
						{!! $data['facts'] !!}
					@endif
				</div>
			</div>
		@endif
		@include("occasion.util.profile.d_components.loading-reviews-section")
		<div id="showProductReviews"></div>
		@if(isset($data['terms']) && $data['terms'])
			<div class="desc-component-wrap mar-b-20 pad-t-10 pad-b-10" id="terms">
				<div class="desc-component-title">
					Terms of Booking
				</div>
				<div class="desc-component-content desc-terms-content">
					{!! $data['terms'] !!}
				</div>
			</div>
		@endif
		@if(isset($data['cancellationData']) && !is_null($data['cancellationData']))
			@include('occasion.util.profile.d_components.cancellation-policy')
		@endif
	</div>
</div>