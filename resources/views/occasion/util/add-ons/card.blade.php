@if($addOn)
	<div class="add-on-card">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-pad">
			<div class="shortlisted-label hide">Selected</div>
			<div class="pull-left mar-r-15">
				<div class="add-on-image-wrap">
					<img data-src="{{ $galleryUrl}}/addons/{{ $addOn->id }}/{{ $addOn->image_url }}" alt="{{ $addOn->name }} profile picture" class="add-on-image lazy-loading package-list-profile-image">
				</div>
			</div>
			<div class="">
				<div class="add-on-content">
					<div class="add-on-name">
						{{ ucwords($addOn->name) }}
					</div>
					@if($addOn->price_per_unit)
						<div class="add-on-price-wrap mar-t-5">
							@if($addOn->price_worth_per_unit)
								<span class="add-on-price-worth">@price($addOn->price_worth_per_unit)</span>
							@endif
							<span class="mar-l-4 add-on-price">@price($addOn->price_per_unit)</span>
							@if($addOn->price_worth_per_unit && ($addOn->price_per_unit < $addOn->price_worth_per_unit))
								<span class="mar-l-4 add-on-price-off">(@offPrice($addOn->price_per_unit, $addOn->price_worth_per_unit) OFF)</span>
							@endif
						</div>
					@endif
					<div class="add-on-cta-wrap mar-t-10 mar-b-5">
						<div class="add-on-cta-list add-on-id-{{ $addOn->id }}" data-id="{{ $addOn->id }}" data-price="{{ $addOn->price_per_unit }}" data-price-worth="{{ $addOn->price_worth_per_unit }}" data-max-units="{{ $addOn->max_units }}" data-count-units="0" data-name="{{ $addOn->name }}">
							<a href="" class="add-on-remove-unit hide">Remove</a>
							<a href="" class="add-on-add-unit">+ Add</a>
						</div>
					</div>
				</div>
			</div>
			@if($addOn->info)
				<div class="add-on-info mar-t-10">
					{!! $addOn->info !!}
				</div>
			@endif
		</div>
		<div class="clearfix"></div>
	</div>
@endif