@if(count($addOns))
	<div class="desc-component-wrap mar-b-20">
		<div class="desc-component-title">
			<span>
				@if($occasionId && ($occasionId == config('evibe.occasion.surprises.id')))
					Make your experience more memorable with these Add-ons
				@else
					Add-ons
				@endif
			</span>
			<span class="add-ons-price-wrap">
				<b><span class="add-ons-selected-count"></span></b> add-ons selected - <b><span><span class="rupee-font">&#8377; </span><span class="add-ons-price">0</span></span></b>
			</span>
		</div>
		<div class="desc-component-content">
			<div class="mar-t-20">
				@if(isset($addOns) && count($addOns))
					<div class="add-ons-content content-overflow no-pad-l">
						@foreach($addOns as $key => $addOn)
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mar-b-15 no-pad-r pad-l-5__400-600">
								@include('occasion.util.add-ons.card')
							</div>
							@if(($key % 2) == 1)
								<div class="clearfix"></div>
							@endif
						@endforeach
						<div class="clearfix"></div>
					</div>
					<div class="text-center mar-t-10">
						<a class="btn-overflow" href="#">+ Show more add-ons</a>
					</div>
				@endif
			</div>
		</div>
	</div>
@endif