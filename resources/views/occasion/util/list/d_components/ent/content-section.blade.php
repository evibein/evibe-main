@if(count($data['options']) > 0 || $data['filters']['clearFilter'])
	<div class="list-bottom-section">
		<div class="list-bottom-wrap">
			<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 no-pad-l">
				<div class="list-left-section">
					<div class="list-left-section-warp">
						<div class="item-filters">
							<ul class="filters-list">
								@include('occasion.util.list-filters.search-filter-new')
								@if(isset($data['filters']['priceMin']) && isset($data['filters']['priceMax']))
									<hr class="des-list-filter-hr">
								@endif
								@include('occasion.util.list-filters.price-filter')
								@if(isset($data['filters']['allCategories']) && count($data['filters']['allCategories']))
									<hr class="des-list-filter-hr">
								@endif
								@include("occasion.util.list-filters.category-filter")
								@if ($data['filters']['clearFilter'] || $data['filters']['active'])
									<li class="text-center reset-filters reset-filters__400 pad-t-20">
										<a href="{{ route('city.occasion.'.config("evibe.type-event-route-name.".$data['occasionId']).'.'.config('evibe.type-product-route-name.'.$data['mapTypeId']).'.list', $cityUrl) }}?ref=reset-filter" class="font-16">
											<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
										</a>
									</li>
								@endif
							</ul>
						</div>
						<div class="hide__400">
							@include('app.evibe_guarantee')
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-9 col-lg-9 col-sm-9 col-xs-12 no-pad">
				<div class="list-right-section">
					<div class="list-right-section-wrap">
						@if (count($data['options']))
							@php $ids = []; $count = 1; @endphp
							@foreach ($data['options'] as $option)
								@php $ids[] = $option['id'] @endphp
								<div class="col-md-3 col-lg-3 col-sm-4 col-xs-6 no-pad-r pad-b-20 pad-l-20">
									<div class="des-list-card-wrap">
										<div class="list-option-card des-list-option-card">
											<div class="list-option-image-wrap text-left">
												<a class="list-option-link option-profile-image option-profile-image-{{ $option['id'] }}" data-option-id="{{ $option['id'] }}" data-image-alt-text="{{ $option['name'] }} {{ config('evibe.type-event-route-name.'.$data['occasionId']) }} party service in {{ getCityUrl() }} @ {{ $option['minPrice'] }}" href="{{ $option['fullPath'] }}?ref=dlpg#image">
													<div class="content-wrapper">
														<div class="placeholder-option">
															<div class="animated-background"></div>
														</div>
													</div>
												</a>
											</div>
											<div class="list-option-content-wrap">
												<div class="list-option-title">
													<a class="list-option-link" href="{{ $option['fullPath'] }}?ref=dlpg#title">
														{{ $option['name'] }}
													</a>
												</div>
												<div class="list-option-cta-wrap text-center">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-pad">
														<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no-pad">
															<a class="item-shortlist" v-on:click="shortlistOptionFromListPage">
																<div class="hideShortlistResultsData list-cta-btn list-btn-shortlist" data-mapId="{{ $option['id'] }}" data-mapTypeId="{{ $data['mapTypeId'] }}" data-cityId="{{ $data['cityId'] }}" data-occasionId="{{ $data['occasionId'] }}" data-urlAdd="{{ route('partybag.add') }}" data-urlDelete="{{ route('partybag.delete') }}">
																	Add To Cart
																</div>
															</a>
														</div>
														<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no-pad">
															<a href="{{ $option['fullPath'] }}?ref=book-now#bookNowModal" class="item-book-now" target="_blank">
																<div class="list-cta-btn list-btn-book-now">
																	Book Now
																</div>
															</a>
														</div>
														<div class="clearfix"></div>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="list-option-price">
													@if ($option['worth'] && $option['worth'] > $option['minPrice'])
														<div class="list-price-worth mar-l-2 mar-r-5 in-blk">
															@price($option['worth'])
														</div>
													@endif
													<div class="list-main-price in-blk mar-r-5">
														@price($option['minPrice'])
														@if ($option['maxPrice'] && $option['maxPrice'] > $option['minPrice'])
															- @price($option['maxPrice'])
														@endif
													</div>
													@if($option['minPrice'] && $option['worth'])
														<div class="list-price-off in-blk mar-r-5">
															(@offPrice($option['minPrice'], $option['worth']) OFF)
														</div>
													@endif
													<div class="list-price-tag hide in-blk">
														<i class="glyphicon glyphicon-thumbs-up"></i> Best Price
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
								@php $count++; @endphp
							@endforeach
							<input type="hidden" id="hidPaginatedIds" data-ids="{{ json_encode($ids) }}"> <!-- array has already been declared as empty -->
							<div class="clearfix"></div>
						@else
							<div class="no-results-wrap mar-l-15 text-center">
								<h4 class="no-results-title no-mar">
									<i class="glyphicon glyphicon-warning-sign"></i>
									Oops, no Entertainment options were found matching your filters.
								</h4>

								<div class="pad-t-20">
									<a href="{{ route('city.occasion.'.config("evibe.type-event-route-name.".$data['occasionId']).'.'.config('evibe.type-product-route-name.'.$data['mapTypeId']).'.list', $cityUrl) }}?ref=reset-filter" class="btn btn-danger btn-lg">
										<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
									</a>
								</div>
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
@else
	<div class="list-content-empty-wrap text-center">
		<div class="list-content-empty-img">
			<div class="no-results-wrap text-center">
				<h4 class="text-col-gr no-mar">We are adding amazing entertainment options soon, keep checking this page.</h4>
			</div>
		</div>
	</div>
@endif
<div>
	@include('app.modals.auto-popup',
	[
	"cityId" => $data['cityId'],
	"occasionId" => $data['occasionId'],
	"mapTypeId" => $data['mapTypeId'],
	"mapId" => "",
	"optionName" => "",
	"countries" => (isset($data['countries']) && count($data['countries'])) ? $data['countries'] : []
	])
</div>