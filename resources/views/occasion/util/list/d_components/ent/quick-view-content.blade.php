<div class="quick-view-content">
	<div class="col-lg-12 col-md-12 col-sm-12 no-pad">

		<div class="quick-view-gallery">
			@if (count($data['gallery']))
				<div class="loading-img-wrap">
					<img src="/images/loading.gif" alt="">
					<div class="pad-t-10">loading gallery...</div>
				</div>
				<div class="gallery-wrap des-slide-gallery-wrap no-mar hide">
					<div class="quick-view-guarantee-wrap text-center">
						<img src="{{ $galleryUrl }}/img/app/eg_rounded_7.png" alt="Evibe delivery guarantee"/>
					</div>
					<div class="fotorama" data-width="100%" data-ratio="900/600" data-maxheight="400" data-autoplay="true" data-thumbwidth="80" data-thumbmargin="10" data-keyboard="true">
						@foreach ($data['gallery'] as $gallery)
							<a href="{{ $gallery['url'] }}">
								<img src="{{ $gallery['thumb'] }}" alt="{{ $gallery['title'] }}"/>
							</a>
						@endforeach
					</div>
				</div>
			@else
				<div>No images found</div>
			@endif
		</div>
		<div class="quick-view-title">
			@if(isset($data['option']))
				<div class="title-section">
					<div class="title-section-wrap mar-t-15">
						<div class="product-title-text">
							{{ $data['option']->name }}
						</div>
						@if(isset($data['ratings']))
							<div class="product-rating @if(!($data['ratings']['total']['count'])) hide @endif">
								<div class="font-16">
									<div class="in-blk">
										<input type="number" class="avg-rating hide" value="{{ $data['ratings']['total']['avg'] }}" title="{{ $data['ratings']['total']['avg'] }} average rating for provider of {{ $data['option']->name }}"/>
									</div>
									@if($data['ratings']['total']['count'])
										<div id="reviews" class="product-rating-reviews scroll-item cur-point in-blk scroll-to-reviews"> ({{ $data['ratings']['total']['count'] }} reviews)
										</div>
									@else
										<div class="product-rating-reviews in-blk"> (<i>new arrival</i>)
										</div>
									@endif
								</div>
							</div>
						@endif
						<div class="product-price mar-t-10">
							@if ($data['option']->price_worth && $data['option']->price_worth > $data['option']->min_price)
								<div class="product-price-worth in-blk mar-l-5">
									@price($data['option']->price_worth)
								</div>
							@endif
							@if($data['option']->min_price && $data['option']->price_worth)
								<div class="product-price-off in-blk mar-l-5">
									(@offPrice($data['option']->min_price, $data['option']->price_worth) OFF)
								</div>
							@endif
							<div class="mar-l-5 product-main-price in-blk">
								@price($data['option']->min_price)
								@if ($data['option']->max_price && $data['option']->max_price > $data['option']->min_price)
									- @price($data['option']->max_price)
								@endif
								@if (isset($data['option']->range_info) && $data['option']->range_info)
									<i class="glyphicon glyphicon-info-sign product-price-info font-12" data-toggle="tooltip" data-placement="left" title="{{ $data['option']->range_info }}"></i>
								@endif
							</div>
							<div class="product-price-tag mar-l-5 in-blk">
								<i class="glyphicon glyphicon-thumbs-up"></i> Best Price
							</div>
							@if (isset($data['option']->price_per_extra_guest) && $data['option']->price_per_extra_guest)
								<div class="product-price-per-guest mar-t-5 mar-l-5">
									<i>(@price($data['option']->price_per_extra_guest) / extra guest)</i>
								</div>
							@endif
							<div class="clearfix"></div>
							<div class="mar-t-5 font-12">
								<i>(price may vary based on <b>party location</b>)</i>
							</div>
						</div>
						@if (!request()->is('*/engagement-wedding-reception*') && !request()->is('*/bachelor-party*'))
							<div class="product-enquiry mar-t-10 hide">
								<a class="product-enquiry-link quick-view-enquire-button">Have Questions? Enquire Now</a>
							</div>
						@endif
					</div>
				</div>
			@endif
		</div>
		<div class="quick-view-cta-2 mar-t-15">
			<div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
				<div>
					<a href="{{ route('city.occasion.'.config("evibe.type-event-route-name.".$data['occasionId']).'.'.config("evibe.type-product-route-name.".$data['mapTypeId']).'.profile', [$data['cityUrl'], $data['option']->url]) }}?ref=dlpg#bookNowModal">
						<div class="btn btn-default quick-view-btn-book-now-3 ga-qv-btn-book-now">
							BOOK NOW
						</div>
					</a>
				</div>
				@if($data['occasionId'] != config("evibe.occasion.pre-post.id") && $data['occasionId'] != config("evibe.occasion.bachelor.id"))
					<div class="product-enquiry mar-t-10">
						<a class="product-enquiry-link quick-view-enquire-button">
							<div class="btn btn-default quick-view-btn-enquire ga-qv-btn-enquire-now">
								Enquire Now
							</div>
						</a>
					</div>
				@endif
			</div>
			<div class="clearfix"></div>

		</div>
		<div class="quick-view-description ">
			@if(isset($data['option']))
				<div class="product-description-wrap mar-t-20">
					<div class="scrollmenu product-info-tabs">
						@if ($data['option']->info)
							<a class="scrollmenu-element" href="#inclusions" data-toggle="tab">Inclusions</a>
						@endif
						@if ($data['option']->prerequisites)
							<a class="scrollmenu-element" href="#prerequisites" data-toggle="tab">Prerequisites</a>
						@endif
						@if ($data['option']->facts)
							<a class="scrollmenu-element" href="#facts" data-toggle="tab">Facts</a>
						@endif
						@if ($data['option']->terms)
							<a class="scrollmenu-element" href="#terms" data-toggle="tab">Terms</a>
						@endif
					</div>
					<div class="product-description-content pad-b-50">
						<div class="tab-content" id="tabs">
							@if ($data['option']->info)
								<div class="tab-pane mar-t-10 mar-l-10 mar-r-15" id="inclusions">{!! $data['option']->info !!}</div>
							@endif
							@if ($data['option']->prerequisites)
								<div class="tab-pane mar-t-10 mar-l-10 mar-r-15" id="prerequisites">{!! $data['option']->prerequisites !!}</div>
							@endif
							@if ($data['option']->facts)
								<div class="tab-pane mar-t-10 mar-l-10 mar-r-15" id="facts">{!! $data['option']->facts !!}</div>
							@endif
							@if ($data['option']->terms)
								<div class="tab-pane mar-t-10 mar-l-10 mar-r-15" id="terms">{!! $data['option']->terms !!}</div>
							@endif
						</div>
					</div>
				</div>
			@endif
		</div>
		<div class="quick-view-cta hide">
			<div class="quick-view-cta-wrap">
				<a href="{{ route('city.occasion.'.config("evibe.type-event-route-name.".$data['occasionId']).'.'.config("evibe.type-product-route-name.".$data['mapTypeId']).'.profile', [$data['cityUrl'], $data['option']->url]) }}?ref=dlpg#bookNowModal">
					<div class="list-view-full-details text-center">
						<i class="material-icons valign-mid">&#xE8CC;</i>
						<span class="valign-mid">BOOK NOW</span>
					</div>
				</a>
			</div>
		</div>
		<div class="quick-view-hidden">
			@if(@isset($data['option']))
				@if(@isset($data['occasionId']))
					<input type="hidden" id="hidProductEnquiryUrl" value="{{ route('ticket.service', [$data['occasionId'], $data['option']->id]) }}">
				@endif

				@if($data['option']->code)
					<input type="hidden" id="hidProductCode" value="{{ $data['option']->code }}">
				@endif

				@if($data['option']->min_price)
					<input type="hidden" id="hidProductPrice" value="{{ $data['option']->min_price }}">
				@endif
			@endif
		</div>
	</div>
	<div class="clearfix"></div>
</div>

@include('occasion.util.list.d_components.quick-view-content-common')
