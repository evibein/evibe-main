@if(isset($data['mapTypeId']))
	<input type="hidden" id="hidRatingsUrl" value="{{ route('ajax.list.options-ratings', ['optionTypeId' => $data['mapTypeId']]) }}">
@endif

@if(isset($data['cityId']))
	<input type="hidden" id="hidCityId" value="{{ $data['cityId'] }}">
@endif

@if(isset($data['occasionId']))
	<input type="hidden" id="hidOccasionId" value="{{ $data['occasionId'] }}">
@endif