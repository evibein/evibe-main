@php $currentCategory = request()->get('category'); @endphp
<div class="list-bottom-section">
	<div class="list-bottom-wrap">
		<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 no-pad-l">
			<div class="list-left-section">
				<div class="list-left-section-warp">
					<div class="item-filters">
						<ul class="filters-list">
							@include('occasion.util.list-filters.search-filter-new')
							@if(isset($data['filters']['priceMin']) && isset($data['filters']['priceMax']))
								<hr class="des-list-filter-hr">
							@endif
							@include('occasion.util.list-filters.price-filter')
							@if(isset($data['filters']['allCategories']) && count($data['filters']['allCategories']))
								<hr class="des-list-filter-hr">
							@endif
							@include('occasion.util.list-filters.category-filter')
							@if(isset($data['filters']['allLocations']['all']) && count($data['filters']['allLocations']['all']))
								<hr class="des-list-filter-hr">
								@include('occasion.util.list-filters.location-filter')
							@endif
							@if ($data['filters']['clearFilter'] || $data['filters']['active'])
								<li class="text-center reset-filters reset-filters__400 pad-t-20">
									@if (request()->is('*/candle-light-dinner*'))
										<a href="{{ route('city.cld.list', $cityUrl) }}?ref=reset-filter" class="font-16">
											<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
										</a>
									@else
										<a href="{{ route('city.occasion.'.config("evibe.type-event-route-name.".$data['occasionId']).'.'.config('evibe.type-product-route-name.'.$data['mapTypeId']).'.list', $cityUrl) }}?ref=reset-filter" class="font-16">
											<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
										</a>
									@endif
								</li>
							@endif
						</ul>
					</div>
					@if(isset($data['mapTypeId']) && $data['mapTypeId'] == config('evibe.ticket.type.food'))
						@include('app.bidding.custom_ticket_food', ["url" => route('ticket.custom-ticket.food',$data['occasionId'])])
					@endif
					<div class=" hide__400">
						@include('app.evibe_guarantee')
					</div>
					<div>
						@include('app.modals.auto-popup',
						[
						"cityId" => $data['cityId'],
						"occasionId" => $data['occasionId'],
						"mapTypeId" => $data['mapTypeId'],
						"mapId" => "",
						"optionName" => "",
						"countries" => (isset($data['countries']) && count($data['countries'])) ? $data['countries'] : []
						])
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-9 col-lg-9 col-sm-9 col-xs-12 no-pad">
			<div class="list-right-section">
				<div class="list-right-section-wrap">
					@if ($data['packages']->count() > 0)
						@php $ids = []; $count = 1; @endphp
						@foreach ($data['packages'] as $package)
							@php
								$ids[] = $package->id;
								if(request()->is('*/candle-light-dinner*'))
								$profileUrl = route('city.cld.profile', [$cityUrl, $package->url]);
							else
								$profileUrl = route('city.occasion.'.config('evibe.type-event-route-name.'.$data['occasionId']).'.'.config('evibe.type-product-route-name.'.$data['mapTypeId']).'.profile', [$cityUrl, $package->url]);
							@endphp
							@if(isset($data['mapTypeId']) && $data['mapTypeId'] == config('evibe.ticket.type.food'))
								@include('occasion.util.list.d_components.package.custom-design', [
											"currentPage" => $data['packages']->currentPage(),
											"loopIteration" => $loop->iteration,
											"noOfCards" => 4
										])
							@endif
							@if(isset($data['isTaggingLive']) && $data['isTaggingLive'] && $count <=4)
								@if($loop->first)
									<h5 style="color: #777;margin-top: 5px; padding-left: 20px;font-size: 16px;">Best Selling <span class="glyphicon glyphicon-heart"></span></h5>
								@endif
							@endif
							@if(isset($data['isTaggingLive']) && $data['isTaggingLive'] && $count == 5)
								<div class="clearfix"></div>
								<hr class="des-list-filter-hr" style="margin:0; margin-bottom: 15px">
							@endif
							@php if(!is_null($currentCategory)) {$catfilter = "&cat=" . $currentCategory;} else {$catfilter="";} @endphp
							<div class="col-md-3 col-lg-3 col-sm-4 col-xs-6 no-pad-r pad-b-20 pad-l-20">
								<div class="list-option-card des-list-option-card @if(($package->type_ticket_id == config('evibe.ticket.type.surprises')) || ($package->type_ticket_id == config('evibe.ticket.type.villas')) && ($package->type_ticket_id != config('evibe.ticket.type.villas'))) surprises-list-option-card @endif">
									<div class="list-option-image-wrap text-center" style="overflow: hidden;position:relative; ">
										<a class="list-option-link lazyload-listpage-image" data-profile-url="{{ $package->getProfileImg() }}" data-option-id="{{ $package->id }}" data-image-alt-text="{{ $package->name }} {{ config('evibe.type-event-route-name.'.$data['occasionId']) }} package in {{ getCityUrl() }} @ {{ $package->price }}" href="{{ $profileUrl }}?ref=dlpgImage{{ $catfilter }}">
											<div class="content-wrapper">
												<div class="placeholder-option">
													<div class="animated-background"></div>
												</div>
											</div>
										</a>
									</div>
									<div class="list-option-content-wrap">
										<div class="list-option-title">
											<a class="list-option-link" href="{{ $profileUrl }}?ref=dlpg#title">
												{{ $package->name }}
											</a>
										</div>
										<div class="list-option-cta-wrap text-center">
											<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-pad">
												@if($data['mapTypeId'] && (($data['mapTypeId'] == config('evibe.ticket.type.surprises')) || ($data['mapTypeId'] == config('evibe.ticket.type.villas'))))
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-pad">
														<a href="{{ $profileUrl }}?ref=book-now#bookNowModal" class="item-book-now" target="_blank">
															<div class="list-cta-btn list-btn-book-now no-border">Book Now</div>
														</a>
													</div>
												@elseif($data['mapTypeId'] && ($data['mapTypeId'] == config('evibe.ticket.type.villas')))
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-pad hide">
														<a href="{{ $profileUrl }}?ref=des-list-view-package" class="item-book-now" target="_blank">
															<div class="list-cta-btn list-btn-book-now no-border">View Package</div>
														</a>
													</div>
												@else
													<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no-pad">
														<a class="item-shortlist" v-on:click="shortlistOptionFromListPage">
															<div class="hideShortlistResultsData list-cta-btn list-btn-shortlist" data-mapId="{{ $package->id }}" data-mapTypeId="{{ $data['mapTypeId'] }}" data-cityId="{{ $data['cityId'] }}" data-occasionId="{{ $data['occasionId'] }}" data-urlAdd="{{ route('partybag.add') }}" data-urlDelete="{{ route('partybag.delete') }}">
																Add To Cart
															</div>
														</a>
													</div>
													<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no-pad">
														<a href="{{ $profileUrl }}?ref=book-now#bookNowModal" class="item-book-now" target="_blank">
															<div class="list-cta-btn list-btn-book-now">Book Now</div>
														</a>
													</div>
												@endif
												<div class="clearfix"></div>
											</div>
											<div class="clearfix"></div>
										</div>
										@if($package->type_ticket_id == config('evibe.ticket.type.villas'))
											<div class="list-option-price">
												<div class="list-price-onwards in-blk hide">
													from
												</div>
												<div class="list-main-price in-blk">
													@price($package->price) +
												</div>
												<div class="list-price-onwards in-blk mar-l-5 hide">
													onwards
												</div>
												<div class="clearfix"></div>
											</div>
										@else
											<div class="list-option-price">
												@if ($package->price_worth && $package->price_worth > $package->price)
													<div class="list-price-worth mar-l-2 mar-r-5 in-blk">
														@price($package->price_worth)
													</div>
												@endif

												@if($package->price && $package->price_worth)
													<div class="list-price-off in-blk mar-r-5">
														(@offPrice($package->price, $package->price_worth) OFF)
													</div>
												@endif
												@if ($package->range_info)
													<i class="glyphicon glyphicon-info-sign list-price-info font-12" data-toggle="tooltip" data-placement="left" title="{{ $package->range_info }}"></i>
												@endif
												<div class="list-price-tag hide in-blk">
													<i class="glyphicon glyphicon-thumbs-up"></i> Best Price
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="list-main-price in-blk mar-r-5">
												@price($package->price)
												@if ($package->price_max && $package->price_max > $package->price)
													- @price($package->price_max)
												@endif
											</div>
										@endif
										@php $providerRating = $package->provider->rating; @endphp
										<div class="list-option-rating in-blk pull-right">
											@if($providerRating && $providerRating->review_count > 0)
												<span>
													<input type="number" class="avg-rating hide" value="{{ $providerRating->avg_rating }}" title="{{ $providerRating->avg_rating }} average rating for provider of {{ $package->name }}"/>
												</span>
												<span class="list-rating-count in-blk">({{ $providerRating->review_count }})</span>
											@else
												<span class="list-rating-count no-mar-l in-blk">(<i>new arrival</i>)</span>
											@endif
										</div>
										@if($package->map_type_id && ($package->map_type_id == config('evibe.ticket.type.venue')) && $package->area && ($package->type_ticket_id != config('evibe.ticket.type.villas')))
											<div class="list-option-location">
												<span class="glyphicon glyphicon-map-marker"></span><span>{{ ucwords($package->area->name) }}</span>
											</div>
										@endif
									</div>
									<div class="list-option-cta-overlay"></div>
								</div>
							</div>
							@php $count++; @endphp
						@endforeach
						<input type="hidden" id="hidPaginatedIds" data-ids="{{ json_encode($ids) }}"> <!-- array has already been declared as empty -->
						<div class="clearfix"></div>
						<div class="pages text-center des-list-pagination-wrap"> <!-- pagination begin -->
							<div>{{ $data['packages']->appends($data['filters']['queryParams'])->links() }}</div>
						</div>
					@else
						<div class="no-results-wrap mar-l-15 text-center">
							<h4 class="no-results-title no-mar">
								<i class="glyphicon glyphicon-warning-sign"></i>
								Oops, no Packages were found matching your filters.
							</h4>

							<div class="pad-t-20">
								@if (request()->is('*/candle-light-dinner*'))
									<a href="{{ route('city.cld.list', $cityUrl) }}?ref=reset-filter" class="font-16">
										<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
									</a>
								@else
									<a href="{{ route('city.occasion.'.config("evibe.type-event-route-name.".$data['occasionId']).'.'.config('evibe.type-product-route-name.'.$data['mapTypeId']).'.list', $cityUrl) }}?ref=reset-filter" class="btn btn-danger btn-lg">
										<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
									</a>
								@endif
							</div>
						</div>
					@endif
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
