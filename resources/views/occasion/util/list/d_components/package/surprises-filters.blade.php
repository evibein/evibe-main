@if($data['surCategories'])
	<div class="scroll-anchor"></div>
	<div class="surprise-filter-bar des-list-sur-filter-bar">
		@include("occasion.util.list.d_components.surprises-filter-bar")
		<input type="hidden" id="surResultsUrl" data-ref="des-sur-list-filters" value="{{ route('city.occasion.'.config("evibe.type-event-route-name.".$data['occasionId']).'.'.config('evibe.type-product-route-name.'.config('evibe.ticket.type.surprises')).'.list', $cityUrl) }}">
	</div>
	<div class="scroll-empty-div"></div>
@endif