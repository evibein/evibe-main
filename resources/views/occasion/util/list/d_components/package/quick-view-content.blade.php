<div class="quick-view-content">
	<div class="col-lg-12 col-md-12 col-sm-12 no-pad">
		<div class="quick-view-gallery">
			@if (isset($data['gallery']['images']['Others']) && count($data['gallery']['images']['Others']))
				<div class="loading-img-wrap">
					<img src="/images/loading.gif" alt="">
					<div class="pad-t-10">loading gallery...</div>
				</div>
				<div class="gallery-wrap des-slide-gallery-wrap no-mar hide">
					<div class="quick-view-guarantee-wrap text-center">
						<img src="{{ $galleryUrl }}/img/app/eg_rounded_7.png" alt="Evibe delivery guarantee"/>
					</div>
					<div class="fotorama" data-width="100%" data-ratio="900/600" data-maxheight="400" data-autoplay="true" data-thumbwidth="80" data-thumbmargin="10" data-keyboard="true">
						@foreach ($data['gallery']['images']['Others'] as $image)
							<a href="{{ $image['url'] }}">
								<img src="{{ $image['thumb'] }}" alt="{{ $image['title'] }}"/>
							</a>
						@endforeach
					</div>
				</div>
			@elseif(isset($data['gallery']['images']['Food']) && count($data['gallery']['images']['Food']))
				<div class="loading-img-wrap">
					<img src="/images/loading.gif" alt="">
					<div class="pad-t-10">loading gallery...</div>
				</div>
				<div class="gallery-wrap des-slide-gallery-wrap no-mar hide">
					<div class="quick-view-guarantee-wrap text-center">
						<img src="{{ $galleryUrl }}/img/app/eg_rounded_7.png" alt="Evibe delivery guarantee"/>
					</div>
					<div class="fotorama" data-width="100%" data-ratio="900/600" data-maxheight="400" data-autoplay="true" data-thumbwidth="80" data-thumbmargin="10" data-keyboard="true">
						@foreach ($data['gallery']['images']['Food'] as $image)
							<a href="{{ $image['url'] }}">
								<img src="{{ $image['thumb'] }}" alt="{{ $image['title'] }}"/>
							</a>
						@endforeach
					</div>
				</div>
			@else
				<i class="text-danger">No images found</i>
			@endif
		</div>
		<div class="quick-view-title">
			@if(isset($data['package']))
				<div class="title-section">
					<div class="title-section-wrap mar-t-15">
						<div class="product-title-text">
							{{ $data['package']->name }}
						</div>
						@if(isset($data['ratings']))
							<div class="product-rating @if(!($data['ratings']['total']['count'])) hide @endif">
								<div class="font-16">
									<div class="in-blk">
										<input type="number" class="avg-rating hide" value="{{ $data['ratings']['total']['avg'] }}" title="{{ $data['ratings']['total']['avg'] }} average rating for provider of {{ $data['package']->name }}"/>
									</div>
									@if($data['ratings']['total']['count'])
										<div id="reviews" class="product-rating-reviews scroll-item cur-point in-blk scroll-to-reviews"> ({{ $data['ratings']['total']['count'] }} reviews)
										</div>
									@else
										<div class="product-rating-reviews in-blk"> (<i>new arrival</i>)
										</div>
									@endif
								</div>
							</div>
						@endif
						<div class="product-price mar-t-10">
							@if ($data['package']->	price_worth && $data['package']->price_worth > $data['package']->price)
								<div class="product-price-worth in-blk mar-l-5">
									@price($data['package']->price_worth)
								</div>
							@endif
							@if($data['package']->price && $data['package']->price_worth)
								<div class="product-price-off in-blk mar-l-5">
									(@offPrice($data['package']->price, $data['package']->price_worth) OFF)
								</div>
							@endif
							<div class="mar-l-5 product-main-price in-blk">
								@price($data['package']->price)
								@if ($data['package']->price_max && $data['package']->price_max > $data['package']->price)
									- @price($data['package']->price_max)
								@endif
								@if (isset($data['package']->range_info) && $data['package']->range_info)
									<i class="glyphicon glyphicon-info-sign product-price-info font-12" data-toggle="tooltip" data-placement="left" title="{{ $data['package']->range_info }}"></i>
								@endif
							</div>
							<div class="product-price-tag mar-l-5 in-blk">
								<i class="glyphicon glyphicon-thumbs-up"></i> Best Price
							</div>
							<div class="clearfix"></div>
							<div class="mar-t-5">
								@if (isset($data['package']->price_per_extra_guest) && $data['package']->price_per_extra_guest)
									<span class="product-price-per-guest mar-l-5 mar-r-10">
										<i>(@price($data['package']->price_per_extra_guest) / extra guest)</i>
									</span>
								@endif
								@if(!$data['package']->propertyType)
									<span class="font-12">
										<i>(price may vary based on <b>party location</b>)</i>
									</span>
								@endif
							</div>
						</div>
					</div>
				</div>
			@endif
		</div>
		<div class="quick-view-cta-2 mar-t-15">
			<div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
				<div>
					<a href="{{ route('city.occasion.'.config("evibe.type-event-route-name.".$data['occasionId']).'.'.config("evibe.type-product-route-name.".$data['mapTypeId']).'.profile', [$data['cityUrl'], $data['package']->url]) }}?ref=dlpg#bookNowModal">
						<div class="btn btn-default quick-view-btn-book-now-3 ga-qv-btn-book-now">
							BOOK NOW
						</div>
					</a>
				</div>
				@if($data['occasionId'] != config("evibe.occasion.pre-post.id") && $data['occasionId'] != config("evibe.occasion.bachelor.id"))
					<div class="product-enquiry mar-t-10">
						<a class="product-enquiry-link quick-view-enquire-button">
							<div class="btn btn-default quick-view-btn-enquire ga-qv-btn-enquire-now">
								Enquire Now
							</div>
						</a>
					</div>
				@endif
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="quick-view-description ">
			@if(isset($data['package']))
				<div class="product-description-wrap mar-t-20">
					<div class="scrollmenu product-info-tabs">
						@if ($data['package']->info)
							<a class="scrollmenu-element" href="#inclusions" data-toggle="tab">Inclusions</a>
						@endif
						@if ($data['package']->prerequisites)
							<a class="scrollmenu-element" href="#prerequisites" data-toggle="tab">Prerequisites</a>
						@endif
						@if ($data['package']->facts)
							<a class="scrollmenu-element" href="#facts" data-toggle="tab">Facts</a>
						@endif
						@if($data['ratings']['total']['count'])
							<a class="scrollmenu-element" href="#productReviews" data-toggle="tab">Reviews</a>
						@endif
						@if ($data['package']->terms)
							<a class="scrollmenu-element" href="#terms" data-toggle="tab">Terms</a>
						@endif
					</div>
					<div class="product-description-content pad-b-50">
						<div class="tab-content" id="tabs">
							@if ($data['package']->info)
								<div class="tab-pane mar-t-10 mar-l-10 mar-r-15" id="inclusions">{!! $data['package']->info !!}</div>
							@endif
							@if ($data['package']->prerequisites)
								<div class="tab-pane mar-t-10 mar-l-10 mar-r-15" id="prerequisites">{!! $data['package']->prerequisites !!}</div>
							@endif
							@if ($data['package']->facts)
								<div class="tab-pane mar-t-10 mar-l-10 mar-r-15" id="facts">{!! $data['package']->facts !!}</div>
							@endif
							<div class="tab-pane pad-l-15 pad-r-15 pad-t-15 reviews-card" id="productReviews">
								@include("app.review.quick-view-partial", [
							"reviews"=> $data["ratings"],
							"showAllReviewsUrl" => route("provider-reviews:all", [
								getSlugFromText($data["package"]->name),
								config("evibe.ticket.type.planner"),
								$data["package"]->provider->id
							])
						])
							</div>

							@if ($data['package']->terms)
								<div class="tab-pane mar-t-10 mar-l-10 mar-r-15" id="terms">{!! $data['package']->terms !!}</div>
							@endif
						</div>
					</div>
				</div>
			@endif
		</div>
		<div class="quick-view-cta hide">
			<div class="quick-view-cta-wrap">
				<a href="{{ route('city.occasion.'.config("evibe.type-event-route-name.".$data['occasionId']).'.'.config("evibe.type-product-route-name.".$data['mapTypeId']).'.profile', [$data['cityUrl'], $data['package']->url]) }}?ref=dlpg#bookNowModal">
					<div class="list-view-full-details text-center">
						<i class="material-icons valign-mid">&#xE8CC;</i>
						<span class="valign-mid">BOOK NOW</span>
					</div>
				</a>
			</div>
		</div>
		<div class="quick-view-hidden">
			@if(@isset($data['package']))
				@if(@isset($data['occasionId']))
					<input type="hidden" id="hidProductEnquiryUrl" value="{{ route('ticket.package', [$data['occasionId'], $data['package']->id]) }}">
				@endif

				@if($data['package']->code)
					<input type="hidden" id="hidProductCode" value="{{ $data['package']->code }}">
				@endif

				@if($data['package']->price)
					<input type="hidden" id="hidProductPrice" value="{{ $data['package']->price }}">
				@endif
			@endif
		</div>
	</div>
	<div class="clearfix"></div>
</div>

@include('occasion.util.list.d_components.quick-view-content-common')
