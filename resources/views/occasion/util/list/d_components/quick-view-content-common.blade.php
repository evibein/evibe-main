<div class="quick-view-enquire text-center">
	<div class="quick-view-enquire-wrap">
		<div class="quick-view-enquire-title">
			Enquire with us
		</div>
		<div class="quick-view-enquire-content">
			<form id="quickViewEnquiryForm">
				<div id="quickViewEnquiryError" class="alert-danger quick-view-enquire-error hide"></div>
				<div class="form-group mar-t-5">
					<div class="col-lg-10 col-md-10 col-sm-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
						<div class="col-md-12 col-lg-12 col-sm-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input ticket_modal_elements" type="text" name="quickViewEnquiryName" id="quickViewEnquiryName" value=""/>
								<label class="mdl-textfield__label no-mar-b" for="quickViewEnquiryName">Name</label>
							</div>
						</div>
						<div class="col-md-12 col-lg-12 col-sm-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ticket_modal_elements">
								<input class="mdl-textfield__input" type="text" name="quickViewEnquiryPhone" id="quickViewEnquiryPhone" value="" maxlength="10"/>
								<label class="mdl-textfield__label no-mar-b" for="quickViewEnquiryPhone">Phone Number</label>
							</div>
						</div>
						<div class="col-md-12 col-lg-12 col-sm-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ticket_modal_elements">
								<input class="mdl-textfield__input" type="text" name="quickViewEnquiryEmail" id="quickViewEnquiryEmail" value=""/>
								<label class="mdl-textfield__label no-mar-b" for="quickViewEnquiryEmail">Email</label>
							</div>
						</div>
						<div class="col-md-12 col-lg-12 col-sm-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ticket_modal_elements">
								<input class="mdl-textfield__input" type="text" name="quickViewEnquiryDate" id="quickViewEnquiryDate" value=""/>
								<label class="mdl-textfield__label no-mar-b" for="quickViewEnquiryDate">Party Date</label>
							</div>
						</div>
						<div class="col-md-12 col-lg-12 col-sm-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ticket_modal_elements">
								<input class="mdl-textfield__input" type="text" name="quickViewEnquiryComments" id="quickViewEnquiryComments" value=""/>
								<label class="mdl-textfield__label no-mar-b" for="quickViewEnquiryComments">Your Requirements</label>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 no-pad">
							<div><i>By submitting this form, I agree to the <a href="{{ route('terms') }}"
											target="_blank">terms of service</a>.</i></div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 hide">
							<label class="fw-normal">
								{{ Form::checkbox('quickViewAccepts', '1', false, ['id' => 'quickViewAccepts']) }}
								I
								<span class="text-lower">have read and accept the <a href="{{ route('terms') }}" target="_blank" rel="noopener">terms of service</a>.</span>
							</label>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</form>
		</div>
		<div class="quick-view-enquire-cta">
			<div class="col-xs-4 col-sm-4 no-pad">
				<button class="btn quick-view-cta-text quick-view-enquire-close-button full-width ga-qv-btn-close-enquire-now">Cancel</button>
			</div>
			<div class="col-xs-8 col-sm-8 no-pad">
				<button class="btn quick-view-cta-text quick-view-btn-submit quick-view-enquire-submit-button full-width">Submit
				</button>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="quick-view-enquire-loader text-center">
		<div class="similar-loading-cards">
			<div class="loader">
				<div class="mar-b-15">
					<div class="line"></div>
					<div class="line"></div>
					<div class="line"></div>
					<div class="line"></div>
				</div>
				Please wait
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {

		var $quickViewEnquireCard = $('.quick-view-enquire');
		var $quickViewEnquireButton = $('.quick-view-enquire-button');
		var $quickViewEnquireSubmit = $('.quick-view-enquire-submit-button');
		var $quickViewEnquireClose = $('.quick-view-enquire-close-button');
		var $quickViewEnquiryError = $('#quickViewEnquiryError');
		var $quickViewEnquireLoader = $('.quick-view-enquire-loader');

		(function initViews() {
			/* gallery */
			$('.fotorama').fotorama();
			$('.loading-img-wrap').remove();
			$('.gallery-wrap').removeClass('hide');

			/* profile images */
			if (slides = $(".rslides").length) {
				slides.responsiveSlides({
					speed: 500,
					timeout: 5000
				});
			}

			activateTab('inclusions');
		})();

		function activateTab(tab) {

			var $activatedTab = $('.product-info-tabs a[href="#' + tab + '"]');
			$activatedTab.tab('show');
			$('.product-info-tabs a').removeClass('scrollmenu-element-active');
			$activatedTab.addClass('scrollmenu-element-active');
		}

		(function setDatePicker() {
			var today = new Date();
			var oneDay = new Date(today.getTime() + 24 * 60 * 60 * 1000);

			$("#quickViewEnquiryDate, #quickViewPriceCheckPartyDate, #bookNowPartyDate, #feasibilityPartyDate").datetimepicker({
				timepicker: false,
				minDate: oneDay,
				format: 'Y/m/d',
				scrollInput: false,
				scrollMonth: false,
				scrollTime: false,
				closeOnDateSelect: true,
				disabledDates: window.disableDates(),
				onSelectDate: function (ct, $i) {
					$i.parent().addClass('is-dirty');
				}
			});

			window.disableDates = function (date) {
				if (date) {
					return date;
				}
			};
		})();

		(function eventsHandler() {
			$('.scroll-to-reviews').click(function (event) {
				event.preventDefault();

				activateTab('productReviews');
			});

			$('.product-info-tabs a').click(function (event) {
				event.preventDefault();

				$('.product-info-tabs a').removeClass('scrollmenu-element-active');
				$(this).addClass('scrollmenu-element-active');
			});

		})();

		function displayEnquireCard() {
			$quickViewEnquiryError.addClass('hide');
			$quickViewEnquireCard.animate({
				height: 'show'
			}, "fast");
		}

		function hideEnquireCard() {
			$quickViewEnquireCard.animate({
				height: 'hide'
			}, "fast");
		}

		function showEnquireLoader() {
			var enquiryFormHeight = $('.quick-view-enquire-wrap').outerHeight();
			$quickViewEnquireLoader.css({"height": enquiryFormHeight, "display": "block"});
			$('.quick-view-enquire-wrap').css("display", "none");
		}

		function hideEnquireLoader() {
			$quickViewEnquireLoader.css("display", "none");
			$('.quick-view-enquire-wrap').css("display", "block");
		}

		function postEnquiry() {
			/* var $enquiryForm = $('#quickViewEnquiryForm'); */
			showEnquireLoader();
			$quickViewEnquiryError.addClass('hide');

			$.ajax({
				url: $('#hidProductEnquiryUrl').val(),
				dataType: 'json',
				type: 'POST',
				data: {
					'name': $('#quickViewEnquiryName').val(),
					'phone': $('#quickViewEnquiryPhone').val(),
					'email': $('#quickViewEnquiryEmail').val(),
					'partyDate': $('#quickViewEnquiryDate').val(),
					'comments': $('#quickViewEnquiryComments').val(),
					'accepts': 'yes'
					/* 'accepts': $('#quickViewAccepts').is(':checked') ? 'yes' : 'no' */
				},
				success: function (data) {
					if (data.success === true) {
						if (data.redirectTo) {
							window.location = data.redirectTo;
						}
						else {
							hideEnquireLoader();
							window.showNotySuccess('Your response has been submitted successfully. Expect a call from us in 4 business hours');
							hideEnquireCard();
							$('#quickViewEnquiryForm')[0].reset();
						}
					}
					else if (data.errors) {
						setTimeout(function () {
							hideEnquireLoader();
							$quickViewEnquiryError.text(data.errors);
							$quickViewEnquiryError.removeClass('hide');
						}, 200);
					}
					else {
						hideEnquireLoader();
						window.showNotyError("Some error occurred while submitting your response. Kindly refresh the page and try again.")
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					hideEnquireLoader();
					window.showNotyError("Some error occurred while submitting your response. Kindly refresh the page and try again.");
					window.notifyTeam({
						"url": url,
						"textStatus": textStatus,
						"errorThrown": errorThrown
					});
				}

			});
		}

		(function quickViewFormHandlers() {
			$quickViewEnquireButton.click(function (event) {
				event.preventDefault();

				displayEnquireCard();
			});

			$quickViewEnquireClose.click(function (event) {
				event.preventDefault();

				hideEnquireCard();
			});

			$quickViewEnquireSubmit.click(function (event) {
				event.preventDefault();

				postEnquiry();
			});
		})();

		/* including 'material.min.js' does not work */
		if (!(typeof(componentHandler) == 'undefined')) {
			componentHandler.upgradeAllRegistered();
		}

	});
</script>