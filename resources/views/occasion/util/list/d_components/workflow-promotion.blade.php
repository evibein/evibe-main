<div style="background-color: #ddf9ff; color: #5a5a5a; padding: 7px 15px; text-align: center; border-bottom: 1px solid #efefef;" class="hide">
	<span class="font-15">Party Planning Made Simpler & Easier</span>
	<a href="{{ route("city.workflow.home", [getCityUrl()]) }}" target="_blank" class="workflow-promotion-plan-now-btn mar-l-10">TRY NOW <i class="glyphicon glyphicon-arrow-right"></i></a>
	<a class="pull-right workflow-promotion-close" style="cursor: pointer; color: #5A5A5A; font-size: 15px"><i class="glyphicon glyphicon-remove"></i></a>
</div>