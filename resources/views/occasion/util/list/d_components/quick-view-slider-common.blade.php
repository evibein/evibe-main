<div class="des-list-slider">
	<div class="des-list-slider-wrap">
		<div class="col-lg-8 col-md-8 col-sm-8 no-pad">
			<div class="des-list-empty-wrap">
				<div class="pull-right quick-view-close">
					<i class="material-icons valign-mid">&#xE5CD;</i>
				</div>
				<div class="clearfix"></div>
			<!-- @see: should be empty -->
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 no-pad">
			<div class="quick-view-wrap">
				<div class="quick-view-content-wrap">
					<div class="quick-view-loader text-center hide">
						<div class="quick-view-load-animation">
							<div class="loader">
								<div class="line"></div>
								<div class="line"></div>
								<div class="line"></div>
								<div class="line"></div>
							</div>
						</div>
						<div class="quick-view-load-text">Please wait</div>
					</div>
				</div>
			</div>
			<div class="quick-view-background"></div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>