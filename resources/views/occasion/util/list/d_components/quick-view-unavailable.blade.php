<div class="quick-view-content">
	<div class="col-lg-12 col-md-12 col-sm-12 text-center">
		<div class="text-bold mar-t-30">
			We regret to inform that quick view is not available for this option. Kindly try with a different option
		</div>
		<div class="mar-t-20">
			<img src="{{ $galleryUrl }}/img/icons/sad-face.png" alt="sorry for the inconvenience">
		</div>
	</div>
	<div class="clearfix"></div>
</div>
