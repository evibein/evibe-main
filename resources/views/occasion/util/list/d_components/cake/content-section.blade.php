@if($data['cakes']->count() > 0 || $data['filters']['clearFilter'])
	<div class="list-bottom-section">
		<div class="list-bottom-wrap">
			<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 no-pad-l">
				<div class="list-left-section">
					<div class="list-left-section-warp">
						<div class="item-filters">
							<ul class="filters-list">
								@include("occasion.util.list-filters.search-filter-new")
								@if(isset($data['filters']['priceMin']) && isset($data['filters']['priceMax']))
									<hr class="des-list-filter-hr">
								@endif
								@include("occasion.util.list-filters.price-filter")
								@if(isset($data['filters']['allCategories']) && count($data['filters']['allCategories']))
									<hr class="des-list-filter-hr">
								@endif
								@include("occasion.util.list-filters.tags-filter")
								@if ($data['filters']['clearFilter'])
									<li class="text-center reset-filters reset-filters__400 pad-t-20">
										<a href="{{ route('city.occasion.'.config("evibe.type-event-route-name.".$data['occasionId']).'.cakes.list', $cityUrl) }}?ref=reset-filters" class="font-16">
											<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
										</a>
									</li>
								@endif
							</ul>
						</div>
						@include('app.bidding.custom_ticket', ["url" => route('ticket.custom-ticket.cake',$data['occasionId'])])
						<div class="hide__400">
							@include('app.evibe_guarantee')
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-9 col-lg-9 col-sm-9 col-xs-12 no-pad">
				<div class="list-right-section">
					<div class="list-right-section-wrap">
						@if ($data['cakes']->count())
							@php $ids = []; $count = 1; @endphp
							@foreach ($data['cakes'] as $cake)
								@php $ids[] = $cake['id'] @endphp
								@include('occasion.util.list.d_components.cake.custom-design', [
									"currentPage" => $data['cakes']->currentPage(),
									"loopIteration" => $loop->iteration,
									"noOfCards" => 4
								])
								<div class="col-md-3 col-lg-3 col-sm-4 col-xs-6 no-pad-r pad-b-20 pad-l-20">
									<div class="des-list-card-wrap">
										<div class="list-option-card des-list-option-card">
											<div class="list-option-image-wrap text-left">
												<a class="list-option-link option-profile-image option-profile-image-{{ $cake['id'] }}" data-option-id="{{ $cake['id'] }}" data-image-alt-text="{{ $cake['name'] }} {{ config('evibe.type-event-route-name.'.$data['occasionId']) }} party cake in {{ getCityUrl() }} @ {{ $cake['price'] }}" href="{{ route('city.occasion.'.config('evibe.type-event-route-name.'.$data['occasionId']).'.cakes.profile', [$cityUrl, $cake['url']]) }}?ref=dlpg#image">
													<div class="content-wrapper">
														<div class="placeholder-option">
															<div class="animated-background"></div>
														</div>
													</div>
												</a>
											</div>
											<div class="list-option-content-wrap">
												<div class="list-option-title">
													<a class="list-option-link" href="{{ route('city.occasion.'.config('evibe.type-event-route-name.'.$data['occasionId']).'.cakes.profile', [$cityUrl, $cake['url']]) }}?ref=dlpg#title">
														{{ $cake['name'] }}
													</a>
												</div>
												<div class="list-option-cta-wrap text-center">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-pad">
														<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no-pad">
															<a class="item-shortlist" v-on:click="shortlistOptionFromListPage">
																<div class="hideShortlistResultsData list-cta-btn list-btn-shortlist" data-mapId="{{ $cake['id'] }}" data-mapTypeId="{{ $data['mapTypeId'] }}" data-cityId="{{ $data['cityId'] }}" data-occasionId="{{ $data['occasionId'] }}" data-urlAdd="{{ route('partybag.add') }}" data-urlDelete="{{ route('partybag.delete') }}">
																	Add To Cart
																</div>
															</a>
														</div>
														<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no-pad">
															<a href="{{ route('city.occasion.'.config('evibe.type-event-route-name.'.$data['occasionId']).'.cakes.profile', [$cityUrl, $cake['url']]) }}?ref=book-now#bookNowModal" class="item-book-now" target="_blank">
																<div class="list-cta-btn list-btn-book-now">
																	Book Now
																</div>
															</a>
														</div>
														<div class="clearfix"></div>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="list-option-price">
													@if(isset($cake['price_per_kg']) && $cake['price_per_kg'])
														<div class="list-main-price in-blk">
															@price($cake['price_per_kg'])
															<span class="mar-l-4 list-price-kg">/ KG</span>
														</div>
														@if(isset($cake['min_order']) && $cake['min_order'])
															<div class="mar-l-2 list-min-order in-blk">
																(min. order:
																<span class="mar-l-2 mar-r-2">{{ $cake['min_order'] }}</span> KG)
															</div>
														@endif
													@endif
													<div class="list-price-tag hide in-blk">
														<i class="glyphicon glyphicon-thumbs-up"></i> Best Price
													</div>
													<div class="clearfix"></div>
												</div>
												@if(isset($cake['provider']['avgRating']) && isset($cake['provider']['reviewCount']))
													<div class="list-option-rating">
														@if($cake['provider']['reviewCount'] > 0)
															<span class="mar-r-4">
															<input type="number" class="avg-rating hide" value="{{ $cake['provider']['avgRating'] }}" title="{{ $cake['provider']['avgRating'] }} average rating for provider of {{ $cake['name'] }}"/>
														</span>
															<span class="list-rating-count in-blk">({{ $cake['provider']['reviewCount'] }})</span>
														@else
															<span class="list-rating-count no-mar-l in-blk">(<i>new arrival</i>)</span>
														@endif
													</div>
												@endif
											</div>
										</div>
									</div>
								</div>
								@php $count++; @endphp
							@endforeach
							<input type="hidden" id="hidPaginatedIds" data-ids="{{ json_encode($ids) }}"> <!-- array has already been declared as empty -->
							<div class="clearfix"></div>
							<div class="pages text-center des-list-pagination-wrap"> <!-- pagination begin -->
								<div>{{ $data['cakes']->appends($data['filters']['queryParams'])->links() }}</div>
							</div>
						@else
							<div class="no-results-wrap mar-l-15 text-center">
								<h4 class="no-results-title no-mar">
									<i class="glyphicon glyphicon-warning-sign"></i>
									Oops, no Cakes were found matching your filters.
								</h4>

								<div class="pad-t-20">
									<a href="{{ route('city.occasion.'.config('evibe.type-event-route-name.'.$data['occasionId']).'.cakes.list', $cityUrl) }}?ref=no-results" class="btn btn-danger btn-lg">
										<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
									</a>
								</div>
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
@else
	<div class="list-content-empty-wrap text-center">
		<div class="list-content-empty-img">
			<div class="no-results-wrap text-center">
				<h4 class="text-col-gr no-mar">We are adding amazing cakes soon, keep checking this page.</h4>
			</div>
		</div>
	</div>
@endif
<div>
	@include('app.modals.auto-popup',
	[
	"cityId" => $data['cityId'],
	"occasionId" => $data['occasionId'],
	"mapTypeId" => $data['mapTypeId'],
	"mapId" => "",
	"optionName" => "",
	"countries" => (isset($data['countries']) && count($data['countries'])) ? $data['countries'] : []
	])
</div>