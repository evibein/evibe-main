@if (!request()->is('*/engagement-wedding-reception*') && !request()->is('*/bachelor-party*'))
	@if($currentPage % 2 == 1 && $loopIteration == 27 || $currentPage % 2 == 0 && $loopIteration == 18)
		<div class="@if(isset($noOfCards) && $noOfCards == 4) col-md-3 col-lg-3 col-sm-4 col-xs-6 no-pad-r pad-b-20 pad-l-20 @else col-sm-12 col-md-6 col-lg-4 no-pad-l no-pad__400-600 @endif">
			<div class="des-list-option-card list-custom-card-wrap">
				<div class="list-custom-card-header">
					<div class="list-custom-card-title">
						Have your own designs?
					</div>
					<div class="list-custom-card-content">
						You can now upload your customized designs. Based on the availability, we will get the best quote from our verified partners.
					</div>
				</div>
				<div class="list-custom-card-footer">
					<button class="btn btn-default btn-bidding">
						<i class="material-icons valign-mid">&#xE2C6;</i>
						<span class="valign-mid">Submit Designs</span>
					</button>
				</div>
			</div>
		</div>
	@endif
@endif