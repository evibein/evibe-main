<div class="quick-view-content">
	<div class="col-lg-12 col-md-12 col-sm-12 no-pad">

		<div class="quick-view-gallery">
			@if (count($data['gallery']))
				<div class="loading-img-wrap">
					<img src="/images/loading.gif" alt="">
					<div class="pad-t-10">loading gallery...</div>
				</div>
				<div class="gallery-wrap des-slide-gallery-wrap no-mar hide">
					<div class="quick-view-guarantee-wrap text-center">
						<img src="{{ $galleryUrl }}/img/app/eg_rounded_7.png" alt="Evibe delivery guarantee"/>
					</div>
					<div class="fotorama" data-width="100%" data-ratio="900/600" data-maxheight="400" data-autoplay="true" data-thumbwidth="80" data-thumbmargin="10" data-keyboard="true">
						@foreach ($data['gallery'] as $gallery)
							<a href="{{ $gallery['url'] }}">
								<img src="{{ $gallery['thumb'] }}" alt="{{ $gallery['title'] }}"/>
							</a>
						@endforeach
					</div>
				</div>
			@else
				<div>No images found</div>
			@endif
		</div>
		<div class="quick-view-title">
			@if(isset($data['cake']))
				<div class="title-section">
					<div class="title-section-wrap mar-t-15">
						<div class="product-title-text">
							{{ $data['cake']->name }}
						</div>
						@if(isset($data['ratings']))
							<div class="product-rating @if(!($data['ratings']['total']['count'])) hide @endif">
								<div class="font-16">
									<div class="in-blk">
										<input type="number" class="avg-rating hide" value="{{ $data['ratings']['total']['avg'] }}" title="{{ $data['ratings']['total']['avg'] }} average rating for provider of {{ $data['cake']->name }}"/>
									</div>
									@if($data['ratings']['total']['count'])
										<div id="reviews" class="product-rating-reviews scroll-item cur-point in-blk scroll-to-reviews"> ({{ $data['ratings']['total']['count'] }} reviews)
										</div>
									@else
										<div class="product-rating-reviews in-blk"> (<i>new arrival</i>)
										</div>
									@endif
								</div>
							</div>
						@endif
						<div class="product-price mar-t-10">
							@if($data['cake']->price_per_kg)
								<div class="product-main-price in-blk">
									@price($data['cake']->price_per_kg)
									<span class="mar-l-4 product-price-kg">/ KG</span>
								</div>
								@if($data['cake']->min_order)
									<div class="mar-l-5 product-min-order in-blk">
										(min. order:
										<span class="mar-l-2 mar-r-2">{{ $data['cake']->min_order }}</span> KG)
									</div>
								@endif
							@endif
							<div class="product-price-tag mar-l-5 in-blk">
								<i class="glyphicon glyphicon-thumbs-up"></i> Best Price
							</div>
							<div class="clearfix"></div>
							<div class="mar-t-5 font-12">
								<i>(price may vary based on <b>party location</b>)</i>
							</div>
						</div>
					</div>
				</div>
			@endif
		</div>
		<div class="quick-view-cta-2 mar-t-15">
			<div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
				<div>
					<a href="{{ route('city.occasion.'.config("evibe.type-event-route-name.".$data['occasionId']).'.'.config("evibe.type-product-route-name.".$data['mapTypeId']).'.profile', [$data['cityUrl'], $data['cake']->url]) }}?ref=dlpg#bookNowModal">
						<div class="btn btn-default quick-view-btn-book-now-3 ga-qv-btn-book-now">
							BOOK NOW
						</div>
					</a>
				</div>
				@if($data['occasionId'] != config("evibe.occasion.pre-post.id") && $data['occasionId'] != config("evibe.occasion.bachelor.id"))
					<div class="product-enquiry mar-t-10">
						<a class="product-enquiry-link quick-view-enquire-button">
							<div class="btn btn-default quick-view-btn-enquire ga-qv-btn-enquire-now">
								Enquire Now
							</div>
						</a>
					</div>
				@endif
			</div>
			<div class="clearfix"></div>

		</div>
		<div class="quick-view-description ">
			@if(isset($data['cake']))
				<div class="product-description-wrap mar-t-20">
					<div class="scrollmenu product-info-tabs">
						@if ($data['cake']->info)
							<a class="scrollmenu-element" href="#inclusions" data-toggle="tab">Inclusions</a>
						@endif
						@if($data['ratings']['total']['count'])
							<a class="scrollmenu-element" href="#productReviews" data-toggle="tab">Reviews</a>
						@endif
						<a class="scrollmenu-element" href="#deliveryCharges" data-toggle="tab">Delivery Charges</a>
					</div>
					<div class="product-description-content pad-b-50">
						<div class="tab-content" id="tabs">
							@if ($data['cake']->info)
								<div class="tab-pane mar-t-10 mar-l-10 mar-r-15" id="inclusions">{!! $data['cake']->info !!}</div>
							@endif
							<div class="tab-pane pad-l-15 pad-r-15 pad-t-15 reviews-card" id="productReviews">
								@include("app.review.quick-view-partial", [
							"reviews"=> $data["ratings"],
							"showAllReviewsUrl" => route("provider-reviews:all", [
								getSlugFromText($data["cake"]->name),
								config("evibe.ticket.type.planner"),
								$data["cake"]->provider->id
							])
						])
							</div>
							<div class="tab-pane mar-t-15 mar-l-15 mar-r-15" id="deliveryCharges">
								<table class="table table-condensed table-bordered">
									<tr>
										<th>Slot (any day)</th>
										<th>Price</th>
									</tr>
									<tr>
										<td>7:00 AM - 10:00 AM</td>
										<td>@price(150)</td>
									</tr>
									<tr>
										<td>10:00 AM - 11:00 PM</td>
										<td>Free</td>
									</tr>
									<tr>
										<td>11:00 PM - 11:50 PM</td>
										<td>@price(150)</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			@endif
		</div>
		<div class="quick-view-cta hide">
			<div class="quick-view-cta-wrap">
				<a href="{{ route('city.occasion.'.config("evibe.type-event-route-name.".$data['occasionId']).'.'.config("evibe.type-product-route-name.".$data['mapTypeId']).'.profile', [$data['cityUrl'], $data['cake']->url]) }}?ref=dlpg#bookNowModal">
					<div class="list-view-full-details text-center">
						<i class="material-icons valign-mid">&#xE8CC;</i>
						<span class="valign-mid">BOOK NOW</span>
					</div>
				</a>
			</div>
		</div>
		<div class="quick-view-hidden">
			@if(@isset($data['cake']))
				@if(@isset($data['occasionId']))
					<input type="hidden" id="hidProductEnquiryUrl" value="{{ route('ticket.cake', [$data['occasionId'], $data['cake']->id]) }}">
				@endif
				@if($data['cake']->code)
					<input type="hidden" id="hidProductCode" value="{{ $data['cake']->code }}">
				@endif

				@if($data['cake']->base_price)
					<input type="hidden" id="hidProductPrice" value="{{ $data['cake']->base_price }}">
				@endif
			@endif
		</div>
	</div>
	<div class="clearfix"></div>
</div>

@include('occasion.util.list.d_components.quick-view-content-common')
