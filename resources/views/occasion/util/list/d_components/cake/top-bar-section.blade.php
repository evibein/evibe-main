<div class="list-top-bar-section">
	<div class="list-top-bar-wrap">
		<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12 no-pad">
			<div class="list-options-count-wrap in-blk">
				<h1 class="list-results-count no-mar-t no-mar-b" style="letter-spacing: normal;">
					@if(isset($data['seo']['pageHeader']) && $data['seo']['pageHeader'])
						Showing {{ $data['seo']['pageHeader'] }}
						@if(isset($data['totalCount']) && $data['totalCount'] > 0)
							<span class="mar-l-4">({{ $data['totalCount'] }})</span>
						@endif
					@else
						Showing
						@if(isset($data['totalCount']) && $data['totalCount'] > 0)
							<span class="mar-l-4">({{ $data['totalCount'] }})</span>
						@endif products
					@endif
				</h1>
			</div>
		</div>
		<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 no-pad-l no-pad-4 pad-t-15">
			<div class="list-options-sort">
				@if($data['cakes']->count() > 0 || $data['filters']['clearFilter'])
					<ul class="no-mar ls-none item-sort-options no-pad hide__400"> <!-- sort options begin -->
						<li class="text-muted font-13">Sort By:</li>
						<li class="@if($data['sort'] == 'popularity') active @endif">
							<a class="sort-option" data-value="popularity">Popularity</a>
						</li>
						<li class="@if($data['sort'] == 'plth' || $data['sort'] == 'phtl') active @endif">
							@if ($data['sort'] == 'phtl')
								<a class="sort-option" data-value="plth">Price <i class="glyphicon glyphicon-chevron-down"></i></a>
							@elseif ($data['sort'] == 'plth')
								<a class="sort-option" data-value="phtl">Price <i class="glyphicon glyphicon-chevron-up"></i></a>
							@else
								<a class="sort-option" data-value="plth">Price</a>
							@endif
						</li>
						<li class="@if($data['sort'] == 'new-arrivals') active @endif">
							<a class="sort-option" data-value="new-arrivals">New Arrivals</a>
						</li>
					</ul>
				@endif
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>