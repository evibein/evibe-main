@if($data['decors']->count() > 0 || $data['filters']['clearFilter'])
	<div class="list-bottom-section">
		<div class="list-bottom-wrap">
			<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 no-pad-l">
				<div class="list-left-section">
					<div class="list-left-section-warp">
						<div class="item-filters">
							<ul class="filters-list">
								@include("occasion.util.list-filters.search-filter-new")
								@if(isset($data['filters']['priceMin']) && isset($data['filters']['priceMax']))
									<hr class="des-list-filter-hr">
								@endif
								@include("occasion.util.list-filters.price-filter")
								@if(isset($data['filters']['allCategories']) && count($data['filters']['allCategories']))
									<hr class="des-list-filter-hr">
								@endif
								@include("occasion.util.list-filters.tags-filter")
								@if ($data['filters']['clearFilter'])
									<li class="text-center reset-filters reset-filters__400 pad-t-20">
										<!-- Below Conditions meant for naming ceremony and baby  shower -->
										@if($data['occasionId'] == config("evibe.occasion.naming_ceremony.id"))
											<a href="{{ route('city.occasion.ncdecors.list', $cityUrl) }}?ref=reset-filters" class="font-16">
												<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
											</a>
										@elseif($data['occasionId'] == config("evibe.occasion.baby-shower.id"))
											<a href="{{ route('city.occasion.bsdecors.list', $cityUrl) }}?ref=reset-filters" class="font-16">
												<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
											</a>
										@elseif($data['occasionId'] == config("evibe.occasion.store-opening.id"))
											<a href="{{ route('city.occasion.sodecors.list', $cityUrl) }}?ref=reset-filters" class="font-16">
												<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
											</a>
										@else
											<a href="{{ route('city.occasion.'.config("evibe.type-event-route-name.".$data['occasionId']).'.decors.list', $cityUrl) }}?ref=reset-filters" class="font-16">
												<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
											</a>
										@endif
									</li>
								@endif
							</ul>
						</div>
						@include('app.bidding.custom_ticket', ["url" => route('ticket.custom-ticket.decor',$data['occasionId'])])
						<div class="hide__400">
							@include('app.evibe_guarantee')
						</div>
						<div>
							@include('app.modals.auto-popup',
													[
													"cityId" => $data['cityId'],
													"occasionId" => $data['occasionId'],
													"mapTypeId" => $data['mapTypeId'],
													"mapId" => "",
													"optionName" => "",
													"countries" => (isset($data['countries']) && count($data['countries'])) ? $data['countries'] : []
													])
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-9 col-lg-9 col-sm-9 col-xs-12 no-pad">
				<div class="list-right-section">
					<div class="list-right-section-wrap">
						@if ($data['decors']->count())
							@php $ids = []; $count = 1; @endphp
							@foreach ($data['decors'] as $decor)
								@php $ids[] = $decor->id @endphp
								@include('occasion.util.list.d_components.decor.custom-design', [
									"currentPage" => $data['decors']->currentPage(),
									"loopIteration" => $loop->iteration,
									"noOfCards" => 4
								])
								<div class="col-md-3 col-lg-3 col-sm-4 col-xs-6 no-pad-r pad-b-20 pad-l-20">
									<div class="des-list-card-wrap">
										<div class="list-option-card des-list-option-card">
											<div class="list-option-image-wrap text-left">
												@php
													if($data['occasionId'] == config("evibe.occasion.naming_ceremony.id"))
													{
														$renderedProfileUrl = route('city.occasion.ncdecors.profile', [$cityUrl, $decor->url]);
													}
													elseif($data['occasionId'] == config("evibe.occasion.baby-shower.id"))
													{
														$renderedProfileUrl = route('city.occasion.bsdecors.profile', [$cityUrl, $decor->url]);
													}
													elseif($data['occasionId'] == config("evibe.occasion.store-opening.id"))
													{
														$renderedProfileUrl = route('city.occasion.sodecors.profile', [$cityUrl, $decor->url]);
													}
													else
													{
														$renderedProfileUrl = route('city.occasion.'.config('evibe.type-event-route-name.'.$data['occasionId']).'.decors.profile', [$cityUrl, $decor->url]);
													}
												@endphp
												<a class="list-option-link option-profile-image option-profile-image-{{ $decor->id }}" data-option-id="{{ $decor->id }}" data-image-alt-text="{{ $decor->name }} {{ config('evibe.type-event-route-name.'.$data['occasionId']) }} party decoration in {{ getCityUrl() }} @ {{ $decor->min_price }}" href="{{ $renderedProfileUrl }}?ref=dlpg#image">
													<div class="content-wrapper">
														<div class="placeholder-option">
															<div class="animated-background"></div>
														</div>
													</div>
												</a>
											</div>
											<div class="list-option-content-wrap">
												<div class="list-option-title">
													<a class="list-option-link" href="{{ $renderedProfileUrl }}?ref=dlpg#title">
														{{ $decor->name }}
													</a>
												</div>
												<div class="list-option-cta-wrap text-center">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-pad">
														@if($data['occasionId']  ==  config("evibe.occasion.pre-post.id"))
															<a href="{{ $renderedProfileUrl }}?ref=book-now#bookNowModal" class="item-book-now" target="_blank">
																<div class="list-cta-btn">
																	Book Now
																</div>
															</a>
														@else
															<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no-pad">
																<a class="item-shortlist" v-on:click="shortlistOptionFromListPage">
																	<div class="hideShortlistResultsData list-cta-btn list-btn-shortlist" data-mapId="{{ $decor->id }}" data-mapTypeId="{{ $data['mapTypeId'] }}" data-cityId="{{ $data['cityId'] }}" data-occasionId="{{ $data['occasionId'] }}" data-urlAdd="{{ route('partybag.add') }}" data-urlDelete="{{ route('partybag.delete') }}">
																		Add To Cart
																	</div>
																</a>
															</div>
															<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no-pad">
																<a href="{{ $renderedProfileUrl }}?ref=book-now#bookNowModal" class="item-book-now" target="_blank">
																	<div class="list-cta-btn list-btn-book-now">
																		Book Now
																	</div>
																</a>
															</div>
														@endif
														<div class="clearfix"></div>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="list-option-price">
													@if ($decor->worth && $decor->worth > $decor->min_price)
														<div class="list-price-worth mar-l-2 mar-r-5 in-blk">
															@price($decor->worth)
														</div>
													@endif
													<div class="list-main-price in-blk mar-r-5">
														@price($decor->min_price)
														@if ($decor->max_price && $decor->max_price > $decor->min_price)
															- @price($decor->max_price)
														@endif
													</div>
													@if($decor->min_price && $decor->worth)
														<div class="list-price-off in-blk mar-r-5">
															(@offPrice($decor->min_price, $decor->worth) OFF)
														</div>
													@endif
													<div class="list-price-tag hide in-blk">
														<i class="glyphicon glyphicon-thumbs-up"></i> Best Price
													</div>
													<div class="clearfix"></div>
												</div>
												@php $providerRating = $decor->provider->rating; @endphp
												<div class="list-option-rating">
													@if($providerRating && $providerRating->review_count > 0)
														<span class="mar-r-4">
															<input type="number" class="avg-rating hide" value="{{ $providerRating->avg_rating }}" title="{{ $providerRating->avg_rating }} average rating for provider of {{ $decor->name }}"/>
														</span>
														<span class="list-rating-count in-blk">({{ $providerRating->review_count }})</span>
													@else
														<span class="list-rating-count no-mar-l in-blk">(<i>new arrival</i>)</span>
													@endif
												</div>
											</div>
										</div>
									</div>
								</div>
								@php $count++; @endphp
							@endforeach
							<input type="hidden" id="hidPaginatedIds" data-ids="{{ json_encode($ids) }}"> <!-- array has already been declared as empty -->
							<div class="clearfix"></div>
							<div class="pages text-center des-list-pagination-wrap"> <!-- pagination begin -->
								<div>{{ $data['decors']->appends($data['filters']['queryParams'])->links() }}</div>
							</div>
						@else
							<div class="no-results-wrap mar-l-15 text-center">
								<h4 class="no-results-title no-mar">
									<i class="glyphicon glyphicon-warning-sign"></i>
									Oops, no Decoration Styles were found matching your filters.
								</h4>

								<div class="pad-t-20">
									<!-- Below Conditions meant for naming ceremony,baby  shower and Store Opening -->
									@if($data['occasionId'] == config("evibe.occasion.naming_ceremony.id"))
										<a href="{{ route('city.occasion.ncdecors.list', $cityUrl) }}?ref=no-results" class="btn btn-danger btn-lg">
											<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
										</a>
									@elseif($data['occasionId'] == config("evibe.occasion.baby-shower.id"))
										<a href="{{ route('city.occasion.bsdecors.list', $cityUrl) }}?ref=no-results" class="btn btn-danger btn-lg">
											<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
										</a>
									@elseif($data['occasionId'] == config("evibe.occasion.store-opening.id"))
										<a href="{{ route('city.occasion.sodecors.list', $cityUrl) }}?ref=no-results" class="btn btn-danger btn-lg">
											<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
										</a>
									@else
										<a href="{{ route('city.occasion.'.config('evibe.type-event-route-name.'.$data['occasionId']).'.decors.list', $cityUrl) }}?ref=no-results" class="btn btn-danger btn-lg">
											<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
										</a>
									@endif
								</div>
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
@else
	<div class="list-content-empty-wrap text-center">
		<div class="list-content-empty-img">
			<div class="no-results-wrap text-center">
				<h4 class="text-col-gr no-mar">We are adding amazing decorations soon, keep checking this page.</h4>
			</div>
		</div>
	</div>
@endif