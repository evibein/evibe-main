<div class="quick-view-content">
	<div class="col-lg-12 col-md-12 col-sm-12 no-pad">
		<div class="quick-view-gallery">
			@if (count($data['gallery']))
				<div class="loading-img-wrap">
					<img src="/images/loading.gif" alt="">
					<div class="pad-t-10">loading gallery...</div>
				</div>
				<div class="gallery-wrap des-slide-gallery-wrap no-mar hide">
					<div class="quick-view-guarantee-wrap text-center">
						<img src="{{ $galleryUrl }}/img/app/eg_rounded_7.png" alt="Evibe delivery guarantee"/>
					</div>
					<div class="fotorama" data-width="100%" data-ratio="900/600" data-maxheight="400" data-autoplay="true" data-thumbwidth="80" data-thumbmargin="10" data-keyboard="true">
						@foreach ($data['gallery'] as $gallery)
							<a href="{{ $gallery['url'] }}">
								<img src="{{ $gallery['thumb'] }}" alt="{{ $gallery['title'] }}"/>
							</a>
						@endforeach
					</div>
				</div>
			@else
				<div>No images found</div>
			@endif
		</div>
		<div class="quick-view-title">
			@if(isset($data['decor']))
				<div class="title-section">
					<div class="title-section-wrap mar-t-15">
						<div class="product-title-text">
							{{ $data['decor']->name }}
						</div>
						@if(isset($data['ratings']))
							<div class="product-rating @if(!$data['ratings']['total']['count']) hide @endif">
								<div class="font-16">
									<div class="in-blk">
										<input type="number" class="avg-rating hide" value="{{ $data['ratings']['total']['avg'] }}" title="{{ $data['ratings']['total']['avg'] }} average rating for provider of {{ $data['decor']->name }}"/>
									</div>
									@if($data['ratings']['total']['count'])
										<div id="reviews" class="product-rating-reviews scroll-item cur-point in-blk scroll-to-reviews"> ({{ $data['ratings']['total']['count'] }} reviews)
										</div>
									@else
										<div class="product-rating-reviews in-blk"> (<i>new arrival</i>)
										</div>
									@endif
								</div>
							</div>
						@endif
						<div class="product-price mar-t-10">
							@if ($data['decor']->worth && $data['decor']->worth > $data['decor']->min_price)
								<div class="product-price-worth in-blk mar-l-5">
									@price($data['decor']->worth)
								</div>
							@endif
							@if($data['decor']->min_price && $data['decor']->worth)
								<div class="product-price-off in-blk mar-l-5">
									(@offPrice($data['decor']->min_price, $data['decor']->worth) OFF)
								</div>
							@endif
							<div class="mar-l-5 product-main-price in-blk">
								@price($data['decor']->min_price)
								@if ($data['decor']->max_price && $data['decor']->max_price > $data['decor']->min_price)
									- @price($data['decor']->max_price)
								@endif
								@if ($data['decor']->range_info)
									<i class="glyphicon glyphicon-info-sign product-price-info font-12" data-toggle="tooltip" data-placement="left" title="{{ $data['decor']->range_info }}"></i>
								@endif
							</div>
							<div class="product-price-tag mar-l-5 in-blk">
								<i class="glyphicon glyphicon-thumbs-up"></i> Best Price
							</div>
							<div class="mar-t-5 font-12">
								<i>(The final price depends on your <b>party location</b>)</i>
							</div>
						</div>
					</div>
				</div>
			@endif
		</div>
		<div class="quick-view-cta-2 mar-t-15">
			<div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
				@if($data['occasionId'] == config("evibe.occasion.pre-post.id"))
					@php $decorUrl = isset($data['decor']) ? $data['decor']->url : ""; @endphp
					<div class="hide">
						<a class="btn quick-view-btn-book-site-visit" href="{{ route('city.occasion.'.config('evibe.type-event-route-name.'.$data['occasionId']).'.decors.profile', [getCityUrl(), $decorUrl]) }}?ref=quickview#bookSiteVisitModal">
							Book FREE Site Visit
						</a>
					</div>
				@else
					<div>
						<a class="btn btn-default quick-view-btn-book-now-3 quick-view-book-now-button ga-qv-btn-book-now">
							BOOK NOW
						</a>
					</div>
					<div class="mar-t-10 hide">
						<a class="btn btn-default quick-view-btn-enquire quick-view-feasibility-button ga-qv-btn-availability">
							Check Availability
						</a>
					</div>
				@endif
				@if($data['occasionId'] != config("evibe.occasion.pre-post.id") && $data['occasionId'] != config("evibe.occasion.bachelor.id"))
					<div class="product-enquiry mar-t-10 text-center">
						<a class="btn btn-default quick-view-btn-enquire quick-view-enquire-button ga-qv-btn-enquire-now">Enquire Now</a>
					</div>
				@endif
			</div>
			<div class="clearfix"></div>

		</div>
		<div class="quick-view-description ">
			@if(isset($data['decor']))
				<div class="product-description-wrap mar-t-20">
					<div class="scrollmenu product-info-tabs">
						@if ($data['decor']->info)
							<a class="scrollmenu-element" href="#inclusions" data-toggle="tab">Inclusions</a>
						@endif
						@if ($data['decor']->more_info)
							<a class="scrollmenu-element" href="#prerequisites" data-toggle="tab">Prerequisites</a>
						@endif
						@if ($data['decor']->facts)
							<a class="scrollmenu-element" href="#facts" data-toggle="tab">Facts</a>
						@endif
						@if($data['ratings']['total']['count'])
							<a class="scrollmenu-element" href="#productReviews" data-toggle="tab">Reviews</a>
						@endif
						@if ($data['decor']->terms)
							<a class="scrollmenu-element" href="#terms" data-toggle="tab">Terms</a>
						@endif
					</div>
					<div class="product-description-content quick-view-product-desc">
						<div class="tab-content" id="tabs">
							<div class="tab-pane mar-t-10 mar-l-10 mar-r-15" id="inclusions">
								<div class="product-highlights-wrap mar-b-10 mar-t-10">
									@if($data['decor']->code)
										<div class="product-highlight-item">
											<div class="product-highlight-item-title in-blk"><i
														class="glyphicon glyphicon-th-list"></i> Style Code:
											</div>
											<div class="product-highlight-item-text in-blk mar-l-5">{{ $data['decor']->code }}</div>
											<div class="clearfix"></div>
										</div>
									@endif
									@if ($data['decor']->time_setup)
										<div class="product-highlight-item">
											<div class="product-highlight-item-title in-blk"><i
														class="glyphicon glyphicon-time"></i> Style Setup Time:
											</div>
											<div class="product-highlight-item-text in-blk mar-l-5">{{ $data['decor']->time_setup }} Hrs</div>
											<div class="clearfix"></div>
										</div>
									@endif
									@if ($data['decor']->time_duration > 0)
										<div class="product-highlight-item">
											<div class="product-highlight-item-title in-blk"><i
														class="glyphicon glyphicon-time"></i> Style Rent Duration:
											</div>
											<div class="product-highlight-item-text in-blk mar-l-5">{{ $data['decor']->time_duration }} Hrs <small>(not applicable for balloons, flowers)</small></div>
											<div class="clearfix"></div>
										</div>
									@endif
								</div>
								@if ($data['decor']->info)
									{!! $data['decor']->info !!}
								@endif
							</div>
							@if ($data['decor']->more_info)
								<div class="tab-pane mar-t-10 mar-l-10 mar-r-15" id="prerequisites">{!! $data['decor']->more_info !!}</div>
							@endif
							@if ($data['decor']->facts)
								<div class="tab-pane mar-t-10 mar-l-10 mar-r-15" id="facts">{!! $data['decor']->facts !!}</div>
							@endif
							<div class="tab-pane pad-l-15 pad-r-15 pad-t-15 reviews-card" id="productReviews">
								@include("app.review.quick-view-partial", [
							"reviews"=> $data["ratings"],
							"showAllReviewsUrl" => route("provider-reviews:all", [
								getSlugFromText($data["decor"]->name),
								config("evibe.ticket.type.planner"),
								$data["decor"]->provider->id
							])
						])
							</div>

							@if ($data['decor']->terms)
								<div class="tab-pane mar-t-10 mar-l-10 mar-r-15" id="terms">{!! $data['decor']->terms !!}</div>
							@endif
						</div>
					</div>
				</div>
			@endif
		</div>
		<div class="quick-view-cta hide">
			<div class="quick-view-cta-wrap">
				@if($data['occasionId'] == config("evibe.occasion.pre-post.id"))
					@php $decorUrl = isset($data['decor']) ? $data['decor']->url : ""; @endphp
					<a class="btn quick-view-btn-book-site-visit hide" href="{{ route('city.occasion.'.config('evibe.type-event-route-name.'.$data['occasionId']).'.decors.profile', [getCityUrl(), $decorUrl]) }}?ref=quickview#bookSiteVisitModal">
						Book FREE Site Visit
					</a>
				@else
					<div class="col-lg-6 col-md-6 col-sm-6 no-pad">
						<a class="btn quick-view-btn-grey quick-view-feasibility-button">
							Check Availability
						</a>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 no-pad">
						<a class="btn quick-view-btn-book-now quick-view-book-now-button">
							Book Now
						</a>
					</div>
					<div class="clearfix"></div>
				@endif
			</div>
		</div>
		<div class="quick-view-hidden">
			@if(isset($data['decor']))
				<input type="hidden" id="hidBaseBookingAmount" value="{{ $data['decor']->min_price }}">
				<input type="hidden" id="hidDistMax" value="{{ $data['decor']->kms_max }}">
				<input type="hidden" id="hidAdvPercentage" value="{{config('evibe.ticket.advance.percentage')}}">
				<input type="hidden" id="hidDistFree" value="{{ $data['decor']->kms_free }}">

				@if($data['decor']->code)
					<input type="hidden" id="hidProductCode" value="{{ $data['decor']->code }}">
				@endif

				@if($data['decor']->min_price)
					<input type="hidden" id="hidProductPrice" value="{{ $data['decor']->min_price }}">
				@endif

				@if(isset($data['decor']['provider']))
					<input type="hidden" id="hidProviderPincode" value="{{ $data['decor']['provider']->zip }}">
				@endif

				@if(isset($data['occasionId']))
					<input type="hidden" id="hidAutoBookUrl" value="{{ route('auto-book.decor.pay.auto.init', [$data['decor']->id, $data['occasionId']]) }}">
					<input type="hidden" id="hidFailedToProceedData" name="failedToProceedData" value="{{ route('ajax.auto-book.failed-to-proceed', [$data['decor']->id, $data['mapTypeId'], $data['occasionId']]) }}">
					<input type="hidden" id="hidProductEnquiryUrl" value="{{ route('ticket.decor', [$data['occasionId'], $data['decor']->id]) }}">
				@endif

				@if(isset($data['typeDevice']))
					<input type="hidden" id="hidTypeDevice" value="{{ $data['typeDevice'] }}">
				@endif

				@if(isset($data['typeBrowser']))
					<input type="hidden" id="hidTypeBrowser" value="{{ $data['typeBrowser'] }}">
				@endif

				@if(isset($data['mapTypeId']))
					<input type="hidden" id="hidForVenueDetails" value="{{ $data['mapTypeId'] }}">
				@endif

				@if(isset($data['mapTypeId']) && isset($data['occasionId']))
					<input type="hidden" id="hidSimilarProductsUrl" value="{{ route('ajax.profile.similar-products', [$data['decor']->id, $data['mapTypeId'], $data['occasionId']]) }}">
				@endif
			@endif
		</div>
		<div class="quick-view-feasibility">
			<div class="quick-view-form-text text-center pad-l-5 pad-r-5">
				<div class="form-normal-text">
					Please enter following details to check availability.
				</div>
				<div class="feasibility-success-text hide">
					<i class="glyphicon glyphicon-ok-circle feasibility-success-icon mar-r-2"></i>
					Decor is available at
					<span id="upPinCodeSuccess" class="updated-pincode text-bold mar-l-4"></span>
					<a class="change-party-location">(change)</a>
				</div>
				<div class="feasibility-fail-text hide">
					<i class="glyphicon glyphicon-remove-circle feasibility-fail-icon mar-r-2"></i>
					<span>Sorry, delivery is not available at</span>
					<span id="upPinCodeFail" class="updated-pincode text-bold mar-l-4"></span>. Please try with different pin code.
				</div>
			</div>
			<div class="quick-view-form-content">
				<div class="sticky-price-content pad-l-15 pad-r-15 mar-t-5 hide">
					<div class="sticky-updated-price text-e text-center">
						<span class='rupee-font'>&#8377;</span>
						<span id="updatedPrice" class="updated-price"></span>
					</div>
					<hr class="quick-view-form-hr-1">
					<div class="sticky-price-splitUp">
						<div class="product-split-price-worth">
							<span class="pull-left">Decor Price</span>
							<span class="pull-right">@price($data['decor']->worth)</span>
							<div class="clearfix"></div>
						</div>
						<div class="product-split-base-price hide">
							<span class="pull-left">Decor Price</span>
							<span class="pull-right">@price($data['decor']->min_price)</span>
							<div class="clearfix"></div>
						</div>
						<div class="product-split-price-off">
							<span class="pull-left">Evibe discount</span>
							<span class="pull-right text-g">- @offPrice($data['decor']->min_price, $data['decor']->worth)</span>
							<div class="clearfix"></div>
						</div>
						<div class="product-split-transport-price">
							<span class="pull-left">Transportation Charges</span>
							<span id="transportFree" class="transport-free pull-right text-free">Free</span>
							<span id="transportPriceContent" class="transport-price-content pull-right hide">
											<span class='rupee-font'>&#8377;</span>
											<span id="transportPrice" class="transport-price"></span>
										</span>
							<div class="clearfix"></div>
						</div>
						<hr class="quick-view-form-hr-2">
						<div class="product-split-total-price">
							<span class="pull-left">Total Price</span>
							<span class="pull-right">
								<span class='rupee-font'>&#8377;</span>
								<span class="updated-price"></span>
							</span>
							<div class="clearfix"></div>
						</div>
						<hr class="quick-view-form-hr-2">
						<div class="product-split-advance text-bold">
							<span class="pull-left">Advance To Pay</span>
							<span class="pull-right text-e">
								<span class='rupee-font'>&#8377;</span>
								<span id="tokenAmount" class="token-amount"></span>
							</span>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<div class="sticky-form mar-t-5">
					<form id="mobileStickyForm" class="product-user-input">
						<div id="quickViewFeasibilityError" class="alert-danger quick-view-feasibility-error text-center hide"></div>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="col-md-6 col-lg-6 col-sm-6 text-center hide">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input id="partyArea" type="text" class="mdl-textfield__input google-auto-complete mar-b-5" name="partyArea" placeholder=""/>
									<label for="partyArea" class="mdl-textfield__label font-13">
										<span class="label-text">Party Area*</span>
									</label>
									<input type="hidden" class="google-location-details" name="locationDetails" value="">
								</div>
							</div>
							<div class="col-md-6 col-lg-6 col-sm-6 text-center">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input id="mobilePhone" type="text" class="mdl-textfield__input mar-b-5" name="mobilePhone" maxlength="10"/>
									<label for="mobilePhone" class="mdl-textfield__label font-13">
										<span class="label-text">Mobile Number*</span>
									</label>
								</div>
							</div>
							<div class="col-md-6 col-lg-6 col-sm-6 text-center">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input id="partyPincode" type="text" class="mdl-textfield__input mar-b-5 google-auto-pincode" name="partyPincode" maxlength="6" data-url="{{ route('auto-book.transport-price-calculate', [$data["decor"]->id, config("evibe.ticket.type.decor")]) }}"/>
									<label for="partyPincode" class="mdl-textfield__label font-13">
										<span class="label-text">Party Area Pin Code*</span>
									</label>
								</div>
							</div>
							<div class="col-md-6 col-lg-6 col-sm-6 text-center hide">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input id="feasibilityPartyDate" type="text" class="mdl-textfield__input mar-b-5" name="feasibilityPartyDate"/>
									<label for="feasibilityPartyDate" class="mdl-textfield__label font-13">
										<span class="label-text">Party Date*</span>
									</label>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</form>
				</div>
			</div>
			<div class="quick-view-form-cta">
				<div class="form-normal-cta text-center">
					<div class="col-xs-12 col-sm-12">
						<div class="product-cta-message text-center pad-b-5">
							<i class="glyphicon glyphicon-info-sign"></i> You can
							<b>Book</b> the decor after checking feasibility.
						</div>
					</div>
					<div class="col-xs-4 col-sm-4 no-pad">
						<button class="btn btn-submit quick-view-cta-text quick-view-form-close-btn full-width">Close</button>
					</div>
					<div class="col-xs-8 col-sm-8 no-pad">
						<button class="btn btn-submit quick-view-btn-check quick-view-cta-text full-width check-feasibility-button">Check Now</button>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="feasibility-success-cta mar-t-10 hide">
					<div class="col-xs-4 col-sm-4 no-pad hide"><!-- need to change after revamping headers -->
						<button class="btn btn-submit quick-view-cta-text quick-view-form-close-btn full-width">Close</button>
					</div>
					<div class="col-xs-12 col-sm-12 no-pad">
						<button class="btn btn-submit quick-view-cta-text quick-view-btn-proceed full-width quick-view-btn-proceed-2">Book Now</button>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="quick-view-book-now">
			<div class="quick-view-book-now-wrap">
				<div class="quick-view-book-now-text text-center"></div>
				<div class="quick-view-book-now-content">
					<div class="book-now-price-content pad-l-15 pad-r-15 mar-t-5">
						<div class="quick-view-form-text text-center">
							<div class="form-normal-text">
								Please enter following details to book decor.
								<div class="text-center mar-t-10">
									<span class="book-now-price-worth">@price($data['decor']->worth)</span>
									<span class="book-now-base-price mar-l-5">@price($data['decor']->min_price)</span>
								</div>
								<div class="mar-t-10">
									Advance To Pay
									<span class="book-now-advance mar-l-10">
										@advAmount($data['decor']->min_price)
									</span>
								</div>
								<div class="book-now-advance-msg"><i>(price may vary based on <b>party location</b>)</i>
								</div>
							</div>
							<div class="feasibility-success-text hide">
								<i class="glyphicon glyphicon-ok-circle feasibility-success-icon mar-r-2"></i>
								Decor is available at
								<span id="upPinCodeSuccess" class="updated-pincode text-bold mar-l-4"></span>
								<a class="book-now-change-party-location mar-l-2">(change)</a>
							</div>
							<div class="feasibility-fail-text hide">
								<i class="glyphicon glyphicon-remove-circle feasibility-fail-icon mar-r-2"></i>
								<span>Sorry, delivery is not available at</span>
								<span id="upPinCodeFail" class="updated-pincode text-bold mar-l-4"></span>. Please try with different pin code.
								<a class="book-now-change-party-location mar-l-2">(change)</a>
							</div>
						</div>
						<div class="quick-view-form-content">
							<div class="sticky-price-content pad-l-15 pad-r-15 mar-t-10 pad-b-10 hide">
								<div class="sticky-updated-price text-e text-center">
									<span class='rupee-font'>&#8377;</span>
									<span id="bookNowUpdatedPrice" class="updated-price"></span>
								</div>
								<hr class="quick-view-form-hr-1">
								<div class="sticky-price-splitUp">
									<div class="product-split-price-worth">
										<span class="pull-left">Decor Price</span>
										<span class="pull-right">@price($data['decor']->worth)</span>
										<div class="clearfix"></div>
									</div>
									<div class="product-split-base-price hide">
										<span class="pull-left">Decor Price</span>
										<span class="pull-right">@price($data['decor']->min_price)</span>
										<div class="clearfix"></div>
									</div>
									<div class="product-split-price-off">
										<span class="pull-left">Evibe discount</span>
										<span class="pull-right text-g">- @offPrice($data['decor']->min_price, $data['decor']->worth)</span>
										<div class="clearfix"></div>
									</div>
									<div class="product-split-transport-price">
										<span class="pull-left">Transportation Charges</span>
										<span id="transportFree" class="transport-free pull-right text-free">Free</span>
										<span id="transportPriceContent" class="transport-price-content pull-right hide">
											<span class='rupee-font'>&#8377;</span>
											<span id="transportPrice" class="transport-price"></span>
										</span>
										<div class="clearfix"></div>
									</div>
									<hr class="quick-view-form-hr-2">
									<div class="product-split-total-price">
										<span class="pull-left">Total Price</span>
										<span class="pull-right">
											<span class='rupee-font'>&#8377;</span>
											<span class="updated-price"></span>
										</span>
										<div class="clearfix"></div>
									</div>
									<hr class="quick-view-form-hr-2">
									<div class="product-split-advance text-bold">
										<span class="pull-left">Advance To Pay</span>
										<span class="pull-right text-e">
											<span class='rupee-font'>&#8377;</span>
											<span id="tokenAmount" class="token-amount"></span>
										</span>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="book-now-form mar-t-10">
						<form id="quickViewBookNowForm" class="product-user-input">
							<div id="quickViewBookNowError" class="alert-danger quick-view-book-now-error text-center hide"></div>
							<div class="form-group mar-t-5">
								<div class="col-lg-12 col-md-12 col-sm-12 mar-b-5">
									<div class="col-md-6 col-lg-6 col-sm-6 text-center hide">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
											<input id="bookNowName" type="text" class="mdl-textfield__input mar-b-5" name="bookNowName"/>
											<label for="bookNowName" class="mdl-textfield__label font-13">
												<span class="label-text">Name*</span>
											</label>
										</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 text-center">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
											<input id="bookNowPhone" type="text" class="mdl-textfield__input mar-b-5" name="bookNowPhone" maxlength="10"/>
											<label for="bookNowPhone" class="mdl-textfield__label font-13">
												<span class="label-text">Mobile Number*</span>
											</label>
										</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 text-center hide">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
											<input id="bookNowEmail" type="text" class="mdl-textfield__input mar-b-5" name="bookNowEmail"/>
											<label for="bookNowEmail" class="mdl-textfield__label font-13">
												<span class="label-text">Email*</span>
											</label>
										</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 text-center">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
											<input id="bookNowPartyDate" type="text" class="mdl-textfield__input mar-b-5" name="bookNowPartyDate"/>
											<label for="bookNowPartyDate" class="mdl-textfield__label font-13">
												<span class="label-text">Party Date*</span>
											</label>
										</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 text-center hide">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
											<input id="bookNowPartyArea" type="text" class="mdl-textfield__input mar-b-5 google-auto-complete" name="bookNowPartyArea" placeholder=""/>
											<label for="bookNowPartyArea" class="mdl-textfield__label font-13">
												<span class="label-text">Party Area*</span>
											</label>
											<input type="hidden" class="google-location-details" name="locationDetails" value="">
										</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 text-center">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
											<input id="bookNowPartyPinCode" type="text" class="mdl-textfield__input mar-b-5 google-auto-pincode" name="bookNowPartyPinCode" maxlength="6"/>
											<label for="bookNowPartyPinCode" class="mdl-textfield__label font-13">
												<span class="label-text">Party Area Pin Code*</span>
											</label>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 text-center hide">
										<div><i>By submitting this form, I agree to the <a href="{{ route('terms') }}"
														target="_blank">terms of service</a>.</i></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="clearfix"></div>
							</div>
						</form>
					</div>
				</div>
				<div class="quick-view-book-now-cta">
					<div id="bookNowNormalCta" class="form-normal-cta">
						<div class="col-xs-4 col-sm-4 no-pad">
							<button class="btn quick-view-cta-text quick-view-book-now-close-button full-width ga-btn-close-book-now-modal">Cancel</button>
						</div>
						<div class="col-xs-8 col-sm-8 no-pad">
							<button class="btn quick-view-cta-text quick-view-btn-submit quick-view-book-now-submit-button full-width" data-url="{{ route('auto-book.booking-price', [$data['decor']->id, config('evibe.ticket.type.decor'), $data['occasionId']]) }}">Submit
							</button>
						</div>
						<div class="clearfix"></div>
					</div>
					<div id="bookNowSuccessCta" class="feasibility-success-cta hide">
						<div class="col-xs-12 col-sm-12 no-pad">
							<button class="btn quick-view-cta-text quick-view-btn-proceed-2 quick-view-book-now-continue-btn full-width">Continue To Book</button>
						</div>
					</div>
					<div id="bookNowFailCta" class="feasibility-fail-cta hide">
						<div class="col-xs-12 col-sm-12 no-pad">
							<button class="btn quick-view-cta-text quick-view-book-now-cancel-btn full-width">Cancel</button>
						</div>
					</div>
				</div>
			</div>
			<div class="quick-view-book-now-loader text-center">
				<div class="similar-loading-cards">
					<div class="loader">
						<div class="mar-b-15">
							<div class="line"></div>
							<div class="line"></div>
							<div class="line"></div>
							<div class="line"></div>
						</div>
						Please wait
					</div>
				</div>
			</div>
		</div>
		<div class="quick-view-form-loader text-center">
			<div class="loading-content text-center">
				<div class="similar-loading-cards">
					<div class="loader">
						<div class="line"></div>
						<div class="line"></div>
						<div class="line"></div>
						<div class="line"></div>
					</div>
				</div>
				Please wait
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>

@include('occasion.util.list.d_components.quick-view-content-common')

<script type="text/javascript">

    $(document).ready(function () {

        var $quickViewFormWrap = $('.quick-view-feasibility');
        var $loadingSectionWrap = $('.quick-view-form-loader');
        var $closeQuickViewForm = $('.quick-view-form-close-btn');
        var $preCheckFeasibilityButton = $('.quick-view-feasibility-button');
        var $preCheckBookNowButton = $('.quick-view-book-now-button');
        var $checkFeasibilityButton = $('.check-feasibility-button');
        var $proceedButton = $('.quick-view-btn-proceed');
        var $changePartyLocation = $('.change-party-location');
        var $quickViewBookNowCard = $('.quick-view-book-now');
        var $quickViewBookNowLoader = $('.quick-view-book-now-loader');
        var $quickViewBookNowClose = $('.quick-view-book-now-close-button');
        var $quickViewBookNowSubmit = $('.quick-view-book-now-submit-button');
        var $quickViewBookNowError = $('#quickViewBookNowError');
        var $bookNowChangePartyLocation = $('.book-now-change-party-location');
        var $quickViewFeasibilityError = $('#quickViewFeasibilityError');
        var $quickViewContinueToBook = $('.quick-view-book-now-continue-btn');

        var distMax = parseInt($('#hidDistMax').val(), 10);
        var distFree = parseInt($('#hidDistFree').val(), 10);
        var basePrice = parseInt($('#hidBaseBookingAmount').val(), 10);
        var customerPinCode = 0;
        var feasibilityCheck = false;
        var productFeasibility = 0;
        var oldPhone = 0;
        var partyDist = 0;
        var transCharges = 0;
        var formatTransCharges = 0;
        var formatTokenAmount = 0;
        var formatBaseBookingAmount = 0;
        var formatTotalBookingAmount = 0;
        var distMaxMeters = distMax * 1000;
        var distFreeMeters = distFree * 1000;
        var percentage = $('#hidAdvPercentage').val();
        var totalBookingAmount = parseInt(basePrice + transCharges);
        var tokenAmount = parseInt(totalBookingAmount * percentage * 0.01);

        var upName = null;
        var upPhone = null;
        var upEmail = null;
        var upPartyDate = null;
        var upPartyArea = null;
        var upPartyPinCode = null;

        function calculateRoundedAdvance($bookingAmount) {
            var $advanceAmount = $bookingAmount * percentage * 0.01;
            var $balAmount = $bookingAmount - $advanceAmount;
            if (($balAmount % 100) != 0 && $balAmount > 100) {
                var $round = parseInt($balAmount % 100);
                var $roundedAdvanceAmount = parseInt($bookingAmount) - parseInt($balAmount) + $round;
                return $roundedAdvanceAmount;
            }

            return parseInt($advanceAmount);
        }

        function displayLoadingDiv() {
            $loadingSectionWrap.animate({
                height: 'show'
            }, "fast");
        }

        function hideLoadingDiv() {
            $loadingSectionWrap.animate({
                height: 'hide'
            }, "fast");
        }

        function checkFeasibilityOfTheProduct() {
            var $partyPincode = $('#partyPincode');
            var $mobilePhone = $('#mobilePhone');
            var $partyDate = $('#feasibilityPartyDate');

            upPhone = $mobilePhone.val();
            upPartyPinCode = $partyPincode.val();
            upPartyDate = $partyDate.val();

            $quickViewFeasibilityError.addClass('hide');

            var $gUrl = $partyPincode.data('url');
            var dataToController = {
                'providerPinCode': $('#hidProviderPincode').val(),
                'partyPinCode': $partyPincode.val(),
                'phone': $mobilePhone.val()
            };

            displayLoadingDiv();
            returnToBasicState();

            $.ajax({
                url: $gUrl,
                dataType: 'json',
                type: 'POST',
                data: dataToController,
                success: function (data) {
                    if (data.success === true) {
                        totalBookingAmount = data.totalBookingAmount;
                        partyDist = data.partyDist;
                        distFreeMeters = data.distFreeMeters;
                        transCharges = data.transCharges;
                        tokenAmount = data.tokenAmount;
                        formatTokenAmount = data.formatTokenAmount;
                        formatBaseBookingAmount = data.formatBaseBookingAmount;
                        formatTotalBookingAmount = data.formatTotalBookingAmount;
                        formatTransCharges = data.formatTransCharges;
                        customerPinCode = $partyPincode.val();
                        if (partyDist <= distFreeMeters) {
                            feasibilityCheck = true;
                            productFeasibility = 1;
                            ticketAlertToTeam();
                            /* see: don not change order */
                            calculateAndShowAmount();
                            $('.sticky-form').removeClass('hide');
                            $('#feasibilityPartyDate').parent().parent().removeClass('hide');
                            $('#partyPincode').parent().parent().addClass('hide');
                            $('#mobilePhone').parent().parent().addClass('hide');
                            hideLoadingDiv();
                        }
                        else if ((partyDist > distFreeMeters) && (partyDist <= distMaxMeters)) {
                            feasibilityCheck = true;
                            productFeasibility = 1;
                            ticketAlertToTeam();
                            calculateAndShowAmount();
                            $('.sticky-form').removeClass('hide');
                            $('#feasibilityPartyDate').parent().parent().removeClass('hide');
                            $('#partyPincode').parent().parent().addClass('hide');
                            $('#mobilePhone').parent().parent().addClass('hide');
                            hideLoadingDiv();
                        }
                        else if (partyDist > distMaxMeters) {
                            productFeasibility = 0;
                            $('.form-normal-text').addClass('hide');
                            $('.feasibility-fail-text').removeClass('hide');
                            $('.updated-pincode').text(customerPinCode);
                            hideLoadingDiv();
                            ticketAlertToTeam();
                        }
                    }
                    else if (data.success === false) {
                        if (data.error) {
                            productFeasibility = 0;
                            ticketAlertToTeam();
                            $quickViewFeasibilityError.text(data.error);
                            $quickViewFeasibilityError.removeClass('hide');
                            var isLive = data.is_live;
                            if (isLive === 0) {
                                var $submitButton = $('.quick-view-btn-submit');
                                var $checkButton = $('.check-feasibility-button');
                                $submitButton.text('Option Unavailable');
                                $submitButton.attr('disabled', true);
                                $checkButton.text('Option Unavailable');
                                $checkButton.attr('disabled', true);
                            }
                            hideLoadingDiv();
                        }
                        else {
                            productFeasibility = 0;
                            hideLoadingDiv();
                            ticketAlertToTeam();
                            window.showNotyError('Some error occurred while processing your data. Please try again after refreshing the page');
                        }
                    }
                    else {
                        productFeasibility = 0;
                        hideLoadingDiv();
                        ticketAlertToTeam();
                        window.showNotyError('Some error occurred while processing your data. Please try again after refreshing the page');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    productFeasibility = 0;
                    ticketAlertToTeam();
                    hideLoadingDiv();
                    window.notifyTeam({
                        "url": url,
                        "textStatus": textStatus,
                        "errorThrown": errorThrown
                    });
                }
            });
        }

        function calculateAndShowAmount() {
            $('.sticky-price-content').removeClass('hide');

            $('.form-normal-text').addClass('hide');
            $('.feasibility-success-text').removeClass('hide');

            $('.book-now-form').addClass('hide');

            if (transCharges > 0) {
                $('.transport-free').addClass('hide');
                $('.transport-price-content').removeClass('hide');
                $('.transport-price').text(formatTransCharges);
                /*				transCharges = 0; */
                /*				totalBookingAmount = basePrice; */
                tokenAmount = calculateRoundedAdvance(parseInt(totalBookingAmount));
            }
            $('.updated-price').text(formatTotalBookingAmount);
            $('.token-amount').text(formatTokenAmount);
            $('#msgTokenAmount').text(formatTokenAmount);
            $('.updated-pincode').text(customerPinCode);

            $('.sticky-form').addClass('hide');
            $('.form-normal-cta').addClass('hide');
            $('.feasibility-success-cta').removeClass('hide');
        }

        function returnToBasicState() {
            $('.sticky-form').removeClass('hide');
            $('.book-now-form').removeClass('hide');
            $('.form-normal-cta').removeClass('hide');
            $('.sticky-price-content').addClass('hide');
            $('.feasibility-success-cta').addClass('hide');
            $('.feasibility-fail-cta').addClass('hide');
            $('.transport-free').removeClass('hide');
            $('.transport-price-content').addClass('hide');
            $('.form-normal-text').removeClass('hide');
            $('.feasibility-success-text').addClass('hide');
            $('.feasibility-fail-text').addClass('hide');

            $quickViewBookNowError.addClass('hide');
            $quickViewFeasibilityError.addClass('hide');
            $('#feasibilityPartyDate').parent().parent().addClass('hide');
            /*			$('#partyPincode').prop('disabled', false); */
            /*			$('#mobilePhone').prop('disabled', false); */
            $('#partyPincode').parent().parent().removeClass('hide');
            $('#mobilePhone').parent().parent().removeClass('hide');
        }

        function showCheckoutPage() {
            var url = $('#hidAutoBookUrl').val();

            $quickViewFeasibilityError.addClass('hide');

            $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: url,
                data: {
                    'phone': upPhone,
                    'forVenueDetails': $('#hidForVenueDetails').val(),
                    'partyPinCode': upPartyPinCode,
                    'partyDate': upPartyDate,
                    'transportCharges': transCharges,
                    'totalBookAmount': totalBookingAmount,
                    'tokenAdvance': tokenAmount
                }, success: function (data) {
                    if
                    (data.success && data.redirectUrl) {
                        location.href = data.redirectUrl;
                    } else if (data.error) {
                        $quickViewFeasibilityError.text(data.error);
                        $quickViewFeasibilityError.removeClass('hide');
                        hideLoadingDiv();
                    } else {
                        window.showNotyError("Some error occurred while we are collecting your details, Please refresh the page and try again");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    window.showNotyError("Some error occurred, please try again after refreshing the page");
                    window.notifyTeam({
                            "url": url,
                            "textStatus": textStatus,
                            "errorThrown": errorThrown
                        }
                    );
                }
            });
        }

        function changePinCodeState() {
            returnToBasicState();
            $('#partyPincode').focus();
            feasibilityCheck = false;
            hideLoadingDiv();
        }

        function changeBookNowPinCodeState() {
            returnToBasicState();
            $('#bookNowPartyPinCode').focus();
            feasibilityCheck = false;
            hideBookNowLoader();
        }

        function updateUserData() {
            updateInput($('#partyPincode'), upPartyPinCode);
            updateInput($('#bookNowPartyPinCode'), upPartyPinCode);

            updateInput($('#mobilePhone'), upPhone);
            updateInput($('#bookNowPhone'), upPhone);

            /*			$('#partyDate').datetimepicker('setDate', upPartyDate); */
            /*			updateInput($('#partyDate'), upPartyDate); */
            /*			updateInput($('#bookNowPartyDate'), upPartyDate); */
        }

        function updateInput($varId, $value) {
            if ($value !== null) {
                $varId.val($value);
                if ($value !== "") {
                    $varId.parent().addClass('is-dirty');
                }
                else {
                    $varId.parent().removeClass('is-dirty');
                }
            }
        }

        function displayStickyForm() {
            returnToBasicState();
            $quickViewFormWrap.animate({
                height: 'show'
            }, "fast");
        }

        function hideStickyForm() {
            $quickViewFormWrap.animate({
                height: 'hide'
            }, "fast");
        }

        function displayBookNowCard() {
            returnToBasicState();
            $quickViewBookNowCard.animate({
                height: 'show'
            }, "fast");
        }

        function hideBookNowCard() {
            $quickViewBookNowCard.animate({
                height: 'hide'
            }, "fast");
        }

        function ticketAlertToTeam() {
            var otherCityDistStatus = "no";
            if (productFeasibility === 0) {
                otherCityDistStatus = window.checkFeasibilityInOtherCities();
            }

            var $mobilePhone = $('#mobilePhone');
            if ($mobilePhone.val().length === 10 && $mobilePhone.val() !== oldPhone) {
                oldPhone = $mobilePhone.val();

                $.ajax({
                    url: $('#hidFailedToProceedData').val(),
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        'phone': $mobilePhone.val(),
                        'partnerPinCode': $('#hidProviderPincode').val(),
                        'partyPinCode': $('#partyPincode').val(),
                        'productFeasibility': productFeasibility,
                        'typeDevice': $('#hidTypeDevice').val(),
                        'typeBrowser': $('#hidTypeBrowser').val(),
                        'partyDist': partyDist,
                        'distFreeMeters': distFreeMeters,
                        'transCharges': transCharges,
                        'otherCitiesLimitStatus': otherCityDistStatus
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        window.notifyTeam({
                            "url": url,
                            "textStatus": textStatus,
                            "errorThrown": errorThrown
                        });
                    }
                });
            }
        }

        function checkBookingFeasibility() {

            showBookNowLoader();
            $quickViewBookNowError.addClass('hide');

            var $partyPinCode = $('#bookNowPartyPinCode');
            var $providerPinCode = $('#hidProviderPincode');
            var $phone = $('#bookNowPhone');
            var $partyDate = $('#bookNowPartyDate');

            upPhone = $phone.val();
            upPartyPinCode = $partyPinCode.val();
            upPartyDate = $partyDate.val();

            returnToBasicState();

            $.ajax({
                url: $quickViewBookNowSubmit.data('url'),
                dataType: 'json',
                type: 'POST',
                data: {
                    /*					'name': $('#bookNowName').val(), */
                    'phone': $phone.val(),
                    /*					'email': $('#bookNowEmail').val(), */
                    'partyDate': $partyDate.val(),
                    /*					'partyArea': $('#bookNowPartyArea').val(), */
                    /*					'locationDetails': $('.google-location-details').val(), */
                    'partyPinCode': $partyPinCode.val(),
                    'providerPinCode': $providerPinCode.val()
                },
                success: function (data) {
                    if (data.success === true) {
                        totalBookingAmount = data.totalBookingAmount;
                        partyDist = data.partyDist;
                        distFreeMeters = data.distFreeMeters;
                        transCharges = data.transCharges;
                        tokenAmount = data.tokenAmount;
                        formatTokenAmount = data.formatTokenAmount;
                        formatBaseBookingAmount = data.formatBaseBookingAmount;
                        formatTotalBookingAmount = data.formatTotalBookingAmount;
                        formatTransCharges = data.formatTransCharges;
                        customerPinCode = $partyPinCode.val();
                        if (partyDist <= distFreeMeters) {
                            feasibilityCheck = true;
                            productFeasibility = 1;
                            calculateAndShowAmount();
                            /* see: don not change order */
                            showCheckoutPage();
                            /* todo: redirect to checkout page */
                        }
                        else if ((partyDist > distFreeMeters) && (partyDist <= distMaxMeters)) {
                            feasibilityCheck = true;
                            productFeasibility = 1;
                            /*							ticketAlertToTeam(); */
                            calculateAndShowAmount();
                            /*							$('#bookNowNormalCta').addClass('hide'); */
                            /*							$('#bookNowSuccessCta').removeClass('hide'); */
                            hideBookNowLoader();
                        }
                        else if (partyDist > distMaxMeters) {
                            productFeasibility = 0;
                            $('.form-normal-text').addClass('hide');
                            $('.feasibility-fail-text').removeClass('hide');
                            $('.updated-pincode').text(customerPinCode);
                            $('.book-now-form').removeClass('hide');
                            /*							$('#bookNowNormalCta').removeClass('hide'); */
                            /*							$('#bookNowSuccessCta').addClass('hide'); */
                            hideBookNowLoader();
                            /*							ticketAlertToTeam(); */
                        }
                    }
                    else if (data.success === false) {
                        hideBookNowLoader();
                        $quickViewBookNowError.text(data.error);
                        $quickViewBookNowError.removeClass('hide');
                        var isLive = data.is_live;
                        if (isLive === 0) {
                            var $submitButton = $('.quick-view-btn-submit');
                            var $checkButton = $('.check-feasibility-button');
                            $submitButton.text('Option Unavailable');
                            $submitButton.attr('disabled', true);
                            $checkButton.text('Option Unavailable');
                            $checkButton.attr('disabled', true);
                        }
                    }
                    else if (data.error) {
                        hideBookNowLoader();
                        $quickViewBookNowError.text(data.error);
                        $quickViewBookNowError.removeClass('hide');
                    }
                    else {
                        hideBookNowLoader();
                        window.showNotyError("Some error occurred while submitting your response. Kindly refresh the page and try again.")
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    hideBookNowLoader();
                    window.showNotyError("Some error occurred while submitting your response. Kindly refresh the page and try again.");
                    window.notifyTeam({
                        "url": url,
                        "textStatus": textStatus,
                        "errorThrown": errorThrown
                    });
                }

            });
        }

        /* todo: remove this */

        function showBookNowLoader() {
            var bookNowFormHeight = $('.quick-view-book-now-wrap').outerHeight();
            $quickViewBookNowLoader.css({"height": bookNowFormHeight, "display": "block"});
            $('.quick-view-book-now-wrap').css("display", "none");
        }

        function hideBookNowLoader() {
            $quickViewBookNowLoader.css("display", "none");
            $('.quick-view-book-now-wrap').css("display", "block");
        }

        function resetCommonData() {
            $('#partyArea').removeClass('google-auto-complete');
            $('#bookNowPartyArea').removeClass('google-auto-complete');
        }

        (function quickViewFormHandlers() {

            $closeQuickViewForm.click(function (event) {
                event.preventDefault();

                hideStickyForm();
            });

            $preCheckFeasibilityButton.click(function (event) {
                event.preventDefault();

                updateUserData();
                displayStickyForm();
                /*				resetCommonData(); */
                /*				$('#partyArea').addClass('google-auto-complete'); */
            });

            $proceedButton.click(function (event) {
                event.preventDefault();

                displayLoadingDiv();
                upPartyDate = $('#feasibilityPartyDate').val();
                showCheckoutPage();
            });

            $changePartyLocation.click(function (event) {
                event.preventDefault();

                displayLoadingDiv();
                setTimeout(changePinCodeState, 1000);

            });

            $preCheckBookNowButton.click(function (event) {
                event.preventDefault();

                updateUserData();
                displayBookNowCard();
                /*				resetCommonData(); */
                /*				$('#bookNowPartyArea').addClass('google-auto-complete'); */
            });

            $quickViewBookNowClose.click(function (event) {
                event.preventDefault();

                hideBookNowCard();
            });

            $quickViewBookNowSubmit.click(function (event) {
                event.preventDefault();

                checkBookingFeasibility();
            });

            $checkFeasibilityButton.click(function (event) {
                event.preventDefault();

                checkFeasibilityOfTheProduct();
            });

            $bookNowChangePartyLocation.click(function (event) {
                event.preventDefault();

                showBookNowLoader();
                setTimeout(changeBookNowPinCodeState, 1000);
            });

            $quickViewContinueToBook.click(function (event) {
                event.preventDefault();

                showBookNowLoader();
                showCheckoutPage();
            });

        })();

        (function attachGooglePlacesToPartyLocation() {

            /* see: do not change to $() notation */
            /* see: duplicate of similar function in 'app.js' */
            /* see: in order to initialize for after load elements, change 'google.maps.event.addDomListener' */
            var preventFormSubmit = function (event) {
                if (event.keyCode === 13) {
                    event.preventDefault();
                }
            };

            var locationInputs = document.getElementsByClassName('google-auto-complete');
            for (var i = 0; i < locationInputs.length; i++) {
                var input = locationInputs[i];
                if (input) {
                    google.maps.event.addDomListener(input, 'keydown', preventFormSubmit);
                }
            }

            var initialize = function () {
                var options = {
                    types: ['(regions)'],
                    componentRestrictions: {country: 'in'}
                };

                for (var i = 0; i < locationInputs.length; i++) {
                    var input = locationInputs[i];
                    if (input) {
                        new google.maps.places.Autocomplete(input, options);
                        var autoComplete = new google.maps.places.Autocomplete(input, options);
                        autoComplete.addListener('place_changed', function () {
                            var place = autoComplete.getPlace();
                            var addressComponents = place.address_components;
                            var postalCode = null;

                            $.each(addressComponents, function (key, component) {
                                if (component.types[0] == 'postal_code') {
                                    postalCode = component.long_name;
                                }
                            });

                            var $quickViewPinCode = $('.google-auto-pincode');
                            if (postalCode && $quickViewPinCode[0]) {
                                $quickViewPinCode.val(postalCode);
                                $quickViewPinCode.parent().addClass('is-dirty');
                                $quickViewPinCode.prop('disabled', true);
                            }
                            else {
                                $quickViewPinCode.val('');
                                $quickViewPinCode.parent().removeClass('is-dirty');
                                $quickViewPinCode.prop('disabled', false);
                            }

                            $('.google-location-details').val(JSON.stringify(addressComponents));
                        });
                    }
                }
            };
            google.maps.event.addDomListener($preCheckBookNowButton, 'click', initialize());

        })();

    });

</script>