<div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 no-pad in-blk">
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 no-pad pad-r-15">
		<label for="surRelations" name="surRelations" class="pull-left no-mar-b surprise-filter-bar-label">Surprise For</label>
		<div class="surprise-filter">
			<select id="surRelations" name="surRelations" class="surprise-filter-drop-down">
				<option value="0">All</option>
				@php $surRelCats = $data['surCategories']['rel']; @endphp
				@foreach($surRelCats as $surRelCat)
					<option value="{{ $surRelCat['id'] }}" @if(isset($data['surFilters']) && $data['surFilters']['surRelTagId'] == $surRelCat['id']) selected @endif>{{ $surRelCat['name'] }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 no-pad pad-r-15">
		<label for="surOccasions" class="pull-left no-mar-b surprise-filter-bar-label">Occasion</label>
		<div class="surprise-filter">
			<select id="surOccasions" name="surOccasions" class="surprise-filter-drop-down">
				<option data-rid="-1" value="0">All</option>
				@php $surOccCats = $data['surCategories']['occ']; @endphp
				@foreach($surOccCats as $surOccCat)
					<option data-rid="{{ $surOccCat['parent_id'] }}" value="{{ $surOccCat['id'] }}" @if(isset($data['surFilters']) && $data['surFilters']['surOccTagId'] == $surOccCat['id']) selected @endif>{{ $surOccCat['name'] }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 no-pad pad-r-15">
		<label for="surAgeGroups" class="pull-left no-mar-b surprise-filter-bar-label">Age Group <span class="text-muted">(optional)</span></label>
		<div class="surprise-filter">
			<select id="surAgeGroups" name="surAgeGroups" class="surprise-filter-drop-down">
				<option data-rid="-1" value="0">All</option>
				@php $surAgeCats = $data['surCategories']['age']; @endphp
				@foreach($surAgeCats as $surAgeCat)
					<option data-rid="{{ $surOccCat['parent_id'] }}" value="{{ $surAgeCat['id'] }}" @if(isset($data['surFilters']) && $data['surFilters']['surAgeTagId'] == $surAgeCat['id']) selected @endif>{{ $surAgeCat['name'] }}</option>
				@endforeach
			</select>
		</div>
	</div>

	@if(isset($data['surFilters']))
		<input type="hidden" id="surOccasionOption" value="{{ $data['surFilters']['surOccTagId'] }}">
		<input type="hidden" id="surAgeGroupOption" value="{{ $data['surFilters']['surAgeTagId'] }}">
	@endif
	<select class="hide" id="OccasionMappingOptions">
		@foreach($data['surCategories']['rel-occ'] as $surOccCat)
			<option data-sr-id="{{ $surOccCat['sr_id'] }}" value="{{ $surOccCat['so_id'] }}"></option>
		@endforeach
	</select>
	<select class="hide" id="AgeMappingOptions">
		@foreach($data['surCategories']['rel-age'] as $surAgeCat)
			<option data-sr-id="{{ $surAgeCat['sr_id'] }}" value="{{ $surAgeCat['sa_id'] }}"></option>
		@endforeach
	</select>
	<div class="clearfix"></div>
</div>
<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 no-pad-l in-blk">
	<button id="surMainFilterBtn" class="surprise-filter-bar-go-btn">GO</button>
</div>
<div class="clearfix"></div>