<div id="listSortWrap" class="list-sort-wrap">
	<div id="listSortEmpty" class="list-sort-empty">
	<!-- @see: leave the space empty, do not remove -->
	</div>
	<div class="list-sort-options-wrap">
		<ul class="list-sort-options">
			<li class="@if($data['sort'] == 'popularity') active @endif">
				<a class="sort-option" data-value="popularity">
					<i class="glyphicon glyphicon-star valign-mid"></i>Popularity
				</a>
			</li>
			<li class="@if($data['sort'] == 'plth') active @endif">
				<a class="sort-option" data-value="plth">
					<i class="glyphicon glyphicon-sort-by-attributes valign-mid"></i>Price: Low - High</a>
			</li>
			<li class="@if($data['sort'] == 'phtl') active @endif">
				<a class="sort-option" data-value="phtl">
					<i class="glyphicon glyphicon-sort-by-attributes-alt valign-mid"></i>Price: High - Low
				</a>
			</li>
			<li class="@if($data['sort'] == 'new-arrivals') active @endif">
				<a class="sort-option" data-value="new-arrivals">
					<i class="glyphicon glyphicon-fire valign-mid"></i>New Arrivals
				</a>
			</li>
		</ul>
	</div>
</div>