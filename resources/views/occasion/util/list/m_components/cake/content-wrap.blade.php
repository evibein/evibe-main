<div class="list-content-wrap">
	@if($data['cakes']->count() > 0 || $data['filters']['clearFilter'])
		<div class="list-options-wrap pad-t-15">
			@if ($data['cakes']->count())
				<div class="col-xs-12 col-sm-12 no-pad-r">
					@php $ids = []; $count = 1; @endphp
					@foreach ($data['cakes'] as $cake)
						@php $ids[] = $cake['id'] @endphp
						<div class="col-xs-6 col-sm-6 no-pad-l pad-b-20">
							<div class="list-option-card">
								<div class="list-option-image-wrap text-left">
									<a class="list-option-link option-profile-image option-profile-image-{{ $cake['id'] }}" data-option-id="{{ $cake['id'] }}" data-image-alt-text="{{ $cake['name'] }} {{ config('evibe.type-event-route-name.'.$data['occasionId']) }} party cake in {{ getCityUrl() }} @ {{ $cake['price'] }}" href="{{ route('city.occasion.'.config('evibe.type-event-route-name.'.$data['occasionId']).'.cakes.profile', [$cityUrl, $cake['url']]) }}?ref=mlp#image">
										<div class="content-wrapper">
											<div class="placeholder-option-mobile">
												<div class="animated-background"></div>
											</div>
										</div>
									</a>
								</div>
								<div class="list-option-content-wrap">
									<div class="list-option-title">
										<a class="list-option-link" href="{{ route('city.occasion.'.config('evibe.type-event-route-name.'.$data['occasionId']).'.cakes.profile', [$cityUrl, $cake['url']]) }}?ref=mlp#title">
											{{ $cake['name'] }}
										</a>
									</div>
									<div class="list-option-price">
										@if(isset($cake['price_per_kg']) && $cake['price_per_kg'])
											<div class="list-main-price">
												@price($cake['price_per_kg'])
												<span class="mar-l-4 list-price-kg">/ KG</span>
											</div>
										@endif
									</div>
									<div class="in-blk">
										<div class="list-option-price">
											@if(isset($cake['min_order']) && $cake['min_order'])
												<div class="list-min-order">
													(minimum order:
													<span class="mar-l-2 mar-r-2">{{ $cake['min_order'] }}</span> KG)
												</div>
											@endif
										</div>
										<div class="list-price-tag hide">
											<i class="glyphicon glyphicon-thumbs-up"></i> Best Price
										</div>
										@if(isset($cake['provider']['avgRating']) && isset($cake['provider']['reviewCount']))
											<div class="list-option-rating">
												@if($cake['provider']['reviewCount'] > 0)
													<span class="mar-r-4">
															<input type="number" class="avg-rating hide" value="{{ $cake['provider']['avgRating'] }}" title="{{ $cake['provider']['avgRating'] }} average rating for provider of {{ $cake['name'] }}"/>
														</span>
													<span class="list-rating-count in-blk">({{ $cake['provider']['reviewCount'] }})</span>
												@else
													<span class="list-rating-count no-mar-l in-blk">(<i>new arrival</i>)</span>
												@endif
											</div>
										@endif
									</div>
									<div class="in-blk hide">
										@include('app.shortlist_results', [
															"mapId" => $cake['id'],
															"mapTypeId" => $data['mapTypeId'],
															"occasionId" => $data['occasionId'],
															"cityId" => $data['cityId']
														])
									</div>
								</div>
							</div>
						</div>
						@if(($count % 2) == 0)
							<div class="clearfix"></div> @endif
						@include("occasion.util.list.m_components.cake.custom-design", ["currentPage" => $data['cakes']->currentPage(), "loopIteration" => $count, "perPage" => $data['cakes']->perPage()])
						@php $count++; @endphp
					@endforeach
					@if(($count % 2) == 0)
						<div class="clearfix"></div>
					@endif
					<input type="hidden" id="hidPaginatedIds" data-ids="{{ json_encode($ids) }}"> <!-- array has already been declared as empty -->
				</div>
				<div class="clearfix"></div>
			@else
				<div class="list-no-results-wrap text-center">
					<div class="list-no-results-title no-mar">
						<i class="glyphicon glyphicon-warning-sign"></i>
						Oops, no Cakes were found matching your filters.
					</div>
					<div class="pad-t-20">
						<a href="{{ route('city.occasion.'.config('evibe.type-event-route-name.'.$data['occasionId']).'.cakes.list', $cityUrl) }}?ref=no-results" class="btn btn-danger btn-lg">
							<i class="glyphicon glyphicon-remove-circle valign-mid"></i> Reset All Filters
						</a>
					</div>
				</div>
			@endif
			@include('app.modals.auto-popup',
			[
			"cityId" => $data['cityId'],
			"occasionId" => $data['occasionId'],
			"mapTypeId" => $data['mapTypeId'],
			"mapId" => "",
			"optionName" => "",
			"countries" => (isset($data['countries']) && count($data['countries'])) ? $data['countries'] : []
			])
		</div>
		@if(isset($data['totalCount']) && ($data['totalCount'] > config('evibe.paginate-options.generic-mobile')))
			<div class="pages mobile-list-pagination text-center mar-b-30 text-center"> <!-- pagination begin -->
				<ul class="pagination">
					<li class="page-item @if(($data['cakes']->appends($data['filters']['queryParams'])->currentPage()) == 1) disabled @endif">
						<a class="page-link btn-smp-pgn-nav-mob btn-left-pgn-nav-mob" @if(($data['cakes']->appends($data['filters']['queryParams'])->currentPage()) != 1) href="{{ $data['cakes']->appends($data['filters']['queryParams'])->previousPageUrl() }}" @endif aria-label="Previous">
							<span aria-hidden="true">&laquo; previous</span>
							<span class="sr-only">Previous</span>
						</a>
					</li>
					<li class="page-item @if(($data['cakes']->appends($data['filters']['queryParams'])->currentPage()) == ($data['cakes']->appends($data['filters']['queryParams'])->lastPage())) disabled @endif">
						<a class="page-link btn-smp-pgn-nav-mob btn-right-pgn-nav-mob no-mar-r" @if(($data['cakes']->appends($data['filters']['queryParams'])->currentPage()) != ($data['cakes']->appends($data['filters']['queryParams'])->lastPage())) href="{{ $data['cakes']->appends($data['filters']['queryParams'])->nextPageUrl() }}" @endif aria-label="Next">
							<span aria-hidden="true">next &raquo;</span>
							<span class="sr-only">Next</span>
						</a>
					</li>
				</ul>
			</div>
		@endif
	@else
		<div class="list-content-empty-wrap text-center">
			<div class="list-content-empty-img">
				<img src="{{ $galleryUrl }}/img/icons/floating-balloons.png">
			</div>
			<div class="list-content-empty-text">
				We are adding yummy cakes soon, keep checking this page.
			</div>
		</div>
	@endif
</div>