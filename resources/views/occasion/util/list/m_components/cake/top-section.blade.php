<div class="list-top-section" id="listTopSection">
	@if($data['cakes']->count() > 0 || $data['filters']['clearFilter'])
		<div class="list-top-section-wrap mar-t-20"> <!-- included in content section -->
			<div class="col-xs-12 col-sm-12">
				<h1 class="list-results-count no-mar-t no-mar-b" style="letter-spacing: normal;">
					@if(isset($data['seo']['pageHeader']) && $data['seo']['pageHeader'])
						Showing {{ $data['seo']['pageHeader'] }}
						@if(isset($data['totalCount']))
							<span class="mar-l-4">({{ $data['totalCount'] }})</span>
						@endif
					@else
						Showing
						@if(isset($data['totalCount']))
							<span class="mar-l-4">({{ $data['totalCount'] }})</span>
						@endif products
					@endif
				</h1>
			</div>
			<div class="clearfix"></div>
		</div>
	@endif
</div>