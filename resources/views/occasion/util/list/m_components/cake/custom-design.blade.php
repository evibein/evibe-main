@if (!request()->is('*/engagement-wedding-reception*') && !request()->is('*/bachelor-party*'))
	@if($currentPage % 2 == 1 && $loopIteration == 30 || $currentPage % 2 == 0 && $loopIteration == 20)
		<div class="col-xs-12 col-sm-12 no-pad-l">
			<div class="list-custom-upload-wrap pad-b-20 mar-b-20 text-center">
				<div class="list-custom-upload-title">
					Have Your Own Designs?
				</div>
				<div class="list-custom-upload-text">
					You can now upload your customised designs. Based on the availability, we will get the best quote from our verified partners.
				</div>
				<div class="list-custom-upload-btn in-blk mar-t-15">
					<i class="material-icons valign-mid">&#xE2C6;</i>
					<span>Submit My Design</span>
				</div>
			</div>

			<div class="btn-bidding">
				<!-- @see: required to trigger the class -->
			</div>
			@include("app.bidding.modals.design", ["url" => route('ticket.custom-ticket.cake', $data['occasionId'])])
			@include("app.bidding.bidding_common")
		</div>
		<div class="clearfix"></div>
	@endif
@endif