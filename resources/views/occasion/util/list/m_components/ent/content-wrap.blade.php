<div class="list-content-wrap">
	@if(count($data['options']) > 0 || $data['filters']['clearFilter'])
		<div class="list-options-wrap pad-t-15">
			@if (count($data['options']))
				<div class="col-xs-12 col-sm-12 no-pad-r">
					@php $ids = []; $count = 1; @endphp
					@foreach ($data['options'] as $option)
						@php $ids[] = $option['id'] @endphp
						<div class="col-xs-6 col-sm-6 no-pad-l pad-b-20">
							<div class="list-option-card">
								<div class="list-option-image-wrap text-left">
									<a class="list-option-link option-profile-image option-profile-image-{{ $option['id'] }}" data-option-id="{{ $option['id'] }}" data-image-alt-text="{{ $option['name'] }} {{ config('evibe.type-event-route-name.'.$data['occasionId']) }} party service in {{ getCityUrl() }} @ {{ $option['minPrice'] }}" href="{{ $option['fullPath'] }}?ref=mlp#image">
										<div class="content-wrapper">
											<div class="placeholder-option-mobile">
												<div class="animated-background"></div>
											</div>
										</div>
									</a>
								</div>
								<div class="list-option-content-wrap">
									<div class="list-option-title">
										<a class="list-option-link" href="{{ $option['fullPath'] }}?ref=mlp#title">
											{{ $option['name'] }}
										</a>
									</div>
									<div class="list-option-price">
										@if ($option['worth'] && $option['worth'] > $option['minPrice'])
											<div class="list-price-worth in-blk mar-r-5">
												@price($option['worth'])
											</div>
										@endif
										@if($option['minPrice'] && $option['worth'])
											<div class="list-price-off in-blk">
												(@offPrice($option['minPrice'], $option['worth']) OFF)
											</div>
										@endif
									</div>
									<div class="in-blk">
										<div class="list-option-price">
											<div class="list-main-price in-blk">
												@price($option['minPrice'])
												@if ($option['maxPrice'] && $option['maxPrice'] > $option['minPrice'])
													- @price($option['maxPrice'])
												@endif
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="list-price-tag hide">
											<i class="glyphicon glyphicon-thumbs-up"></i> Best Price
										</div>
									</div>
									<div class="in-blk hide">
										@include('app.shortlist_results', [
															"mapId" => $option['id'],
															"mapTypeId" => $data['mapTypeId'],
															"occasionId" => $data['occasionId'],
															"cityId" => $data['cityId']
														])
									</div>
								</div>
							</div>
						</div>
						@if(($count % 2) == 0)
							<div class="clearfix"></div> @endif
						@php $count++; @endphp
					@endforeach
					@if(($count % 2) == 0)
						<div class="clearfix"></div>
					@endif
					<input type="hidden" id="hidPaginatedIds" data-ids="{{ json_encode($ids) }}"> <!-- array has already been declared as empty -->
				</div>
				<div class="clearfix"></div>
			@else
				<div class="list-no-results-wrap text-center">
					<div class="list-no-results-title no-mar">
						<i class="glyphicon glyphicon-warning-sign"></i>
						Oops, no Entertainment options were found matching your filters.
					</div>
					<div class="pad-t-20">
						<a href="{{ route('city.occasion.'.config("evibe.type-event-route-name.".$data['occasionId']).'.'.config('evibe.type-product-route-name.'.$data['mapTypeId']).'.list', $cityUrl) }}?ref=reset-filter" class="btn btn-danger btn-lg">
							<i class="glyphicon glyphicon-remove-circle valign-mid"></i> Reset All Filters
						</a>
					</div>
				</div>
			@endif
		</div>
	@else
		<div class="list-content-empty-wrap text-center">
			<div class="list-content-empty-img">
				<img src="{{ $galleryUrl }}/img/icons/floating-balloons.png">
			</div>
			<div class="list-content-empty-text">
				We are adding entertainment option soon, keep checking this page.
			</div>
		</div>
	@endif
	<div>
		@include('app.modals.auto-popup',
		[
		"cityId" => $data['cityId'],
		"occasionId" => $data['occasionId'],
		"mapTypeId" => $data['mapTypeId'],
		"mapId" => "",
		"optionName" => "",
		"countries" => (isset($data['countries']) && count($data['countries'])) ? $data['countries'] : []
		])
	</div>
</div>