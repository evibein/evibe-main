<div id="listBottomSectionWrap" class="list-bottom-section-wrap text-center">
	<div class="list-bottom-cta-wrap">
		<div class="col-xs-4 col-sm-4 no-pad">
			<div id="listSortBtn" class="list-sort-btn-wrap list-float-btn @if(request()->has('sort')) text-blue-imp @endif">
				<i class="material-icons valign-mid">&#xE164;</i> Sort
			</div>
		</div>
		<div class="col-xs-4 col-sm-4 no-pad">
			<div id="listFilterBtn" class="list-filters-btn-wrap list-float-btn @if(isset($data['filters']['clearFilter']) && $data['filters']['clearFilter']) text-blue-imp @endif">
				<i class="material-icons valign-mid">&#xE152;</i> Filter
			</div>
		</div>
		<div class="col-xs-4 col-sm-4 no-pad">
			<div class="list-fab-space-wrap">

			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>