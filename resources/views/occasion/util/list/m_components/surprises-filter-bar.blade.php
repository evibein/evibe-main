<div class="col-xs-6 no-pad">
	<label for="surRelations" name="surRelations" class="pull-left no-mar-b surprise-filter-bar-label surprise-filter-bar-label-m">SURPRISE FOR</label>
	<div class="surprise-filter surprise-filter-m">
		<select id="surRelations" name="surRelations" class="">
			<option value="0">All</option>
			@php $surRelCats = $data['surCategories']['rel']; @endphp
			@foreach($surRelCats as $surRelCat)
				<option value="{{ $surRelCat['id'] }}" @if(isset($data['surFilters']) && $data['surFilters']['surRelTagId'] == $surRelCat['id']) selected @endif>{{ $surRelCat['name'] }}</option>
			@endforeach
		</select>
	</div>
</div>
<div class="col-xs-6 no-pad">
	<label for="surOccasions" class="pull-left no-mar-b surprise-filter-bar-label surprise-filter-bar-label-m">OCCASION</label>
	<div class="surprise-filter surprise-filter-m">
		<select id="surOccasions" name="surOccasions" class="">
			<option data-rid="-1" value="0">All</option>
			@php $surOccCats = $data['surCategories']['occ']; @endphp
			@foreach($surOccCats as $surOccCat)
				<option data-rid="{{ $surOccCat['parent_id'] }}" value="{{ $surOccCat['id'] }}" @if(isset($data['surFilters']) && $data['surFilters']['surOccTagId'] == $surOccCat['id']) selected @endif>{{ $surOccCat['name'] }}</option>
			@endforeach
		</select>
	</div>
</div>
<div class="col-xs-6 no-pad mar-t-10">
	<label for="surAgeGroups" class="pull-left no-mar-b surprise-filter-bar-label surprise-filter-bar-label-m">AGE GROUP <span class="text-muted">(Optional)</span></label>
	<div class="surprise-filter surprise-filter-m">
		<select id="surAgeGroups" name="surAgeGroups" class="">
			<option value="0">All</option>
			@php $surAgeCats = $data['surCategories']['age']; @endphp
			@foreach($surAgeCats as $surAgeCat)
				<option data-rid="{{ $surAgeCat['parent_id'] }}" value="{{ $surAgeCat['id'] }}" @if(isset($data['surFilters']) && $data['surFilters']['surAgeTagId'] == $surAgeCat['id']) selected @endif>{{ $surAgeCat['name'] }}</option>
			@endforeach
		</select>
	</div>
</div>
<div class="col-xs-6 no-pad pad-r-10">
	<button id="surMainFilterBtn" class="surprise-filter-bar-go-btn">GO</button>
</div>
@if(isset($data['surFilters']))
	<input type="hidden" id="surOccasionOption" value="{{ $data['surFilters']['surOccTagId'] }}">
	<input type="hidden" id="surAgeGroupOption" value="{{ $data['surFilters']['surAgeTagId'] }}">
@endif
<select class="hide" id="OccasionMappingOptions">
	@foreach($data['surCategories']['rel-occ'] as $surOccCat)
		<option data-sr-id="{{ $surOccCat['sr_id'] }}" value="{{ $surOccCat['so_id'] }}"></option>
	@endforeach
</select>
<select class="hide" id="AgeMappingOptions">
	@foreach($data['surCategories']['rel-age'] as $surAgeCat)
		<option data-sr-id="{{ $surAgeCat['sr_id'] }}" value="{{ $surAgeCat['sa_id'] }}"></option>
	@endforeach
</select>
<div class="clearfix"></div>