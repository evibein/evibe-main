@extends("occasion.util.list.m_components.filters-section")

@section('surprise-filters')
	@if(isset($data['surCategories']) && $data['surCategories'])
		<div class="surprise-filter-bar mob-list-sur-filter-bar no-pad-b hide">
			@include("occasion.util.list.m_components.surprises-filter-bar")
			<input type="hidden" id="surResultsUrl" data-ref="mob-sur-list-filters" value="{{ route('city.occasion.'.config("evibe.type-event-route-name.".$data['occasionId']).'.'.config('evibe.type-product-route-name.'.config('evibe.ticket.type.surprises')).'.list', $cityUrl) }}">
		</div>
	@endif
@endsection