@if(!(isset($data['filters']['hidePriceCategories']) && $data['filters']['hidePriceCategories']))
	<div class="price-filter-wrap">
		<div id="mblPriceFilterSticky" class="sticky-top price-categories-wrap hide pad-r-15" style="z-index:6;white-space: nowrap;overflow: scroll;height: 30px;-webkit-scrollbar:none">
			@if(isset($data['filters']['priceCategories']) && count($data['filters']['priceCategories']) > 0)
				@foreach($data['filters']['priceCategories'] as $priceCategory)
					<a class="price-category-option pad-l-15 pad-r-15 a-no-decoration-black in-blk @if(($data['filters']['priceMin'] == $priceCategory['minPrice']) && ($data['filters']['priceMax'] == $priceCategory['maxPrice'])) text-e @endif @if($priceCategory['count'] == 0) disabled @endif" data-price-cat-min="{{ $priceCategory['minPrice'] }}" data-price-cat-max="{{ $priceCategory['maxPrice'] }}" href="#" style="border-radius: 5px;padding: 3px 5px;border: 0.5px solid #f3f3f3;margin-left: 15px">
						@if($priceCategory['bound'] == config('evibe.price-category.bounds.low'))
							Below @price($priceCategory['maxPrice'])
						@elseif($priceCategory['bound'] == config('evibe.price-category.bounds.high'))
							Above @price($priceCategory['minPrice'])
						@else
							@price($priceCategory['minPrice']) - @price($priceCategory['maxPrice'])
						@endif
					</a>
				@endforeach
			@endif
		</div>
	</div>
@endif
@php $currentCategory = request()->get('category'); @endphp
<div class="list-content-wrap">
	<div class="list-options-wrap pad-t-5">
		@if ($data['packages']->count())
			<div class="col-xs-12 col-sm-12 no-pad-r">
				@php $ids = []; $count = 1; @endphp
				@foreach ($data['packages'] as $package)
					@php
						$ids[] = $package->id;
						if(request()->is('*/candle-light-dinner*'))
						$profileUrl = route('city.cld.profile', [$cityUrl, $package->url]);
					else
						$profileUrl = route('city.occasion.'.config('evibe.type-event-route-name.'.$data['occasionId']).'.'.config('evibe.type-product-route-name.'.$data['mapTypeId']).'.profile', [$cityUrl, $package->url]);
					@endphp
					<div class="col-xs-6 col-sm-6 no-pad-l pad-b-20" style="position: relative;">
						@php $pId=$package->id @endphp
						@if(isset($data['isTaggingLive']) && $data['isTaggingLive'] && in_array($package->id,array_keys($data['taggedItems'])))
							@if($data['taggedItems'][$pId]== config('evibe.custom-tags-list-page.1'))
								<span class="custom-tag" style="background:#FFBF00B3;"><span class="glyphicon glyphicon-heart font-12"></span>{{$data['taggedItems'][$pId]}}</span>
							@elseif($data['taggedItems'][$pId]== config('evibe.custom-tags-list-page.2'))
								<span class="custom-tag" style="background:#B12704B3;"><span class="glyphicon glyphicon-time font-12"></span>{{$data['taggedItems'][$pId]}}</span>
							@elseif($data['taggedItems'][$pId]== config('evibe.custom-tags-list-page.3'))
								<span class="custom-tag" style="background:#E68A6FB3;"><span class="glyphicon glyphicon-star font-12"></span>{{$data['taggedItems'][$pId]}}</span>
							@elseif($data['taggedItems'][$pId]== config('evibe.custom-tags-list-page.4'))
								<span class="custom-tag" style="background:#00CD00B3;"><span class="glyphicon glyphicon-flash font-12"></span>{{$data['taggedItems'][$pId]}}</span>
							@endif
						@endif
						@php if(!is_null($currentCategory)) {$catfilter = "&cat=" . $currentCategory;} else {$catfilter="";} @endphp
						<div class="list-option-card">
							<div class="list-option-image-wrap text-left">
								<a class="list-option-link" href="{{ $profileUrl }}?ref=mlpImage{{ $catfilter }}">
									<!-- In below two images, First image z-index must be greater than second image. And note that this z-index will affect other floating buttons,links,navbar,filters -->
									<img class="list-option-image @if($package->type_ticket_id == config('evibe.ticket.type.villas')) villa-list-option-img @endif" src="{{ $package->getProfileImg() }}" alt="{{ $package->name }} {{ config('evibe.type-event-route-name.'.$data['occasionId']) }} package in {{ getCityUrl() }} @ {{ $package->price }}" onError="this.src='{{ $galleryUrl }}/img/icons/gift.png'; this.style='padding: 10px 10px;z-index:3;position:absolute;left:0;top:50%;transform:translateY(-50%);width:100%; height:auto;min-height: 100px'" style="z-index:3;position:absolute;left:0;top:50%;transform:translateY(-50%);width:100%; height:auto;min-height: 100px">
									<img class="list-option-image @if($package->type_ticket_id == config('evibe.ticket.type.villas')) villa-list-option-img @endif" src="{{ $package->getProfileImg() }}" alt="{{ $package->name }} {{ config('evibe.type-event-route-name.'.$data['occasionId']) }} package in {{ getCityUrl() }} @ {{ $package->price }}" onError="this.src='{{ $galleryUrl }}/img/icons/gift.png'; this.style='padding: 10px 10px z-index:2;position:absolute;filter:blur(20px);left:0;width:100%; height:auto;width:auto;height: 100%'" style="z-index:2;position:absolute;filter:blur(5px);left:0;width:auto;height: 100%">
								</a>
							</div>
							<div class="list-option-content-wrap">
								<div class="list-option-title">
									<a class="list-option-link" href="{{ $profileUrl }}?ref=mlp#title">
										{{ $package->name }}
									</a>
								</div>
								@if($package->type_ticket_id == config('evibe.ticket.type.villas'))
									<div class="list-option-price">
										<div class="list-price-onwards in-blk hide">
											from
										</div>
										<div class="list-main-price in-blk">
											@price($package->price) +
										</div>
										<div class="list-price-onwards in-blk mar-l-5 hide">
											onwards
										</div>
									</div>
								@else
									<div class="list-option-price">
										@if ($package->price_worth && $package->price_worth > $package->price)
											<div class="list-price-worth in-blk mar-r-5">
												@price($package->price_worth)
											</div>
										@endif
										@if($package->price && $package->price_worth)
											<div class="list-price-off in-blk">
												(@offPrice($package->price, $package->price_worth) OFF)
											</div>
										@endif
									</div>
								@endif
								<div class="">
									@if($package->type_ticket_id == config('evibe.ticket.type.villas'))
									@else
										<div>
											<div class="list-option-price pull-left">
												<div class="list-main-price in-blk">
													@price($package->price)
													<span class="hide">
												@if ($package->price_max && $package->price_max > $package->price)
															- @price($package->price_max)
														@endif
												</span>
												</div>
												@if (isset($package->range_info) && $package->range_info)
													<i class="glyphicon glyphicon-info-sign list-price-info font-12" data-toggle="tooltip" data-placement="left" title="{{ $package->range_info }}"></i>
												@endif
											</div>
											@php $providerRating = $package->provider->rating; @endphp
											<div class="list-option-rating pull-right">
												@if($providerRating && $providerRating->review_count > 0)
													<span class="mar-r-4">
														<input type="number" class="avg-rating hide" value="{{ $providerRating->avg_rating }}" title="{{ $providerRating->avg_rating }} average rating for provider of {{ $package->name }}"/>
													</span>
													<span class="list-rating-count in-blk">({{ $providerRating->review_count }})</span>
												@else
													<span class="list-rating-count no-mar-l in-blk">(<i>new arrival</i>)</span>
												@endif
											</div>
											<div class="clearfix"></div>
											<div class="list-price-tag hide">
												<i class="glyphicon glyphicon-thumbs-up"></i> Best Price
											</div>
										</div>
									@endif
								</div>
								@if(($data['mapTypeId'] != config('evibe.ticket.type.surprises')) && ($data['mapTypeId'] != config('evibe.ticket.type.villas')))
									<div class="in-blk hide">
										@include('app.shortlist_results', [
																"mapId" => $package->id,
																"mapTypeId" => $data['mapTypeId'],
																"occasionId" => $data['occasionId'],
																"cityId" => $data['cityId']
															])
									</div>
								@endif
								@if($package->map_type_id && ($package->map_type_id == config('evibe.ticket.type.venue')) && $package->area && ($package->type_ticket_id != config('evibe.ticket.type.villas')))
									<div class="list-option-location">
										<span class="glyphicon glyphicon-map-marker"></span><span>{{ ucwords($package->area->name) }}</span>
									</div>
								@endif
							</div>
						</div>
					</div>
					@if(($count % 2) == 0)
						<div class="clearfix"></div> @endif
					@php $count++; @endphp
				@endforeach
				@if(($count % 2) == 0)
					<div class="clearfix"></div>
				@endif
				<input type="hidden" id="hidPaginatedIds" data-ids="{{ json_encode($ids) }}"> <!-- array has already been declared as empty -->
			</div>
			<div class="clearfix"></div>
		@else
			<div class="list-no-results-wrap text-center">
				<div class="list-no-results-title no-mar">
					<i class="glyphicon glyphicon-warning-sign"></i>
					Oops, no Packages were found matching your filters.
				</div>
				<div class="pad-t-20">
					@if (request()->is('*/candle-light-dinner*'))
						<a href="{{ route('city.cld.list', $cityUrl) }}?ref=reset-filter" class="font-16">
							<i class="glyphicon glyphicon-remove-circle valign-mid"></i> Reset All Filters
						</a>
					@else
						<a href="{{ route('city.occasion.'.config('evibe.type-event-route-name.'.$data['occasionId']).'.'.config('evibe.type-product-route-name.'.$data['mapTypeId']).'.list', $cityUrl) }}?ref=no-results" class="btn btn-danger btn-lg">
							<i class="glyphicon glyphicon-remove-circle valign-mid"></i> Reset All Filters
						</a>
					@endif
				</div>
			</div>
		@endif
		@include('app.modals.auto-popup',
		[
		"cityId" => $data['cityId'],
		"occasionId" => $data['occasionId'],
		"mapTypeId" => $data['mapTypeId'],
		"mapId" => "",
		"optionName" => "",
		"countries" => (isset($data['countries']) && count($data['countries'])) ? $data['countries'] : []
		])
	</div>
	@if(isset($data['totalCount']) && ($data['totalCount'] > config('evibe.paginate-options.generic')))
		<div class="pages mobile-list-pagination text-center mar-b-30 text-center"> <!-- pagination begin -->
			<ul class="pagination">
				<li class="page-item @if(($data['packages']->appends($data['filters']['queryParams'])->currentPage()) == 1) disabled @endif">
					<a class="page-link btn-smp-pgn-nav-mob btn-left-pgn-nav-mod" @if(($data['packages']->appends($data['filters']['queryParams'])->currentPage()) != 1) href="{{ $data['packages']->appends($data['filters']['queryParams'])->previousPageUrl() }}" @endif aria-label="Previous">
						<span aria-hidden="true">&laquo; previous</span>
						<span class="sr-only">Previous</span>
					</a>
				</li>
				<li class="page-item @if(($data['packages']->appends($data['filters']['queryParams'])->currentPage()) == ($data['packages']->appends($data['filters']['queryParams'])->lastPage())) disabled @endif">
					<a class="page-link btn-smp-pgn-nav-mob btn-right-pgn-nav-mod no-mar-r" @if(($data['packages']->appends($data['filters']['queryParams'])->currentPage()) != ($data['packages']->appends($data['filters']['queryParams'])->lastPage())) href="{{ $data['packages']->appends($data['filters']['queryParams'])->nextPageUrl() }}" @endif aria-label="Next">
						<span aria-hidden="true">next &raquo;</span>
						<span class="sr-only">Next</span>
					</a>
				</li>
			</ul>
		</div>
	@endif
</div>