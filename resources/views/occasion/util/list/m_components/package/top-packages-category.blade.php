@if(isset($data['topPackages'][$name]) && (count($data['topPackages'][$name]) > 0))
	<div class="row trending-items pad-t-20 pad-b-20">
		<div class="head-title pos-rel">
			<div class="col-md-8 col-sm-8 col-xs-12">
				<h4 class="">Best {{ ucfirst($name) }} Packages</h4>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-4 pull-right mar-t-15 hide">
				<a href="{{ route('city.occasion.surprises.package.list', $cityUrl) }}?ref=top-all" class="pull-right">
					<button class="btn btn-default btn-see-all-surprises btn-see-all-surprises-m"><span
								class="valign-mid">See More</span>
					</button>
				</a>
			</div>
			<div class="clearfix"></div>
		</div>
		<div id="surprisePackageCarousel-{{ str_replace(" ", "", $name) }}" class="carousel slide" data-ride="carousel">
			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				@php $i = 1;
							$count = count($data['topPackages'][$name])
				@endphp
				@foreach($data['topPackages'][$name] as $package)
					@if($i % 2 == 1)
						<div class="item @if($i == 1) active @endif">
							@endif
							<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 trending mar-b-30">
								<div class="img-container">
									<a href="{{ route('city.occasion.surprises.package.profile', [$cityUrl, $package->url]) }}?ref=top-image" title="Popular surprise package {{ $package->code }} in {{ $cityUrl }}">
										<img src="{{ $package->getProfileImg('results') }}" title="romantic experience {{ $package->code }}">
									</a>
									@include('app.shortlist_results', [
																		"mapId" => $package->id,
																		"mapTypeId" => config('evibe.ticket.type.surprises'),
																		"occasionId" => $data['occasionId'],
																		"cityId" => $data['cityId']
																	])
								</div>
								<a href="{{ route('city.occasion.surprises.package.profile', [$cityUrl, $package->url]) }}?ref=top-image">
									<h6 class="ov-text-no-wrap mar-t-15 mar-b-5 font-14">@truncateName($package->name)</h6>
								</a>
								<div class="price-tag text-left">
															<span class="surprise-home-packages-list-price">
																@price($package->price)
															</span>
									@if ($package->price_worth && $package->price_worth > $package->price)
										<span class="surprise-home-packages-list-worth">
																	@price($package->price_worth)
																</span>
									@endif
								</div>
							</div>
							@if($i % 2 == 0)
						</div>
					@endif

					@php $i++; @endphp
				@endforeach
				@if(count($data['topPackages'][$name]) % 2 != 0)</div>@endif

			@if(count($data['topPackages'][$name]) > 2)
				<a class="left carousel-control product-carousel-slider carousel-control-m" href="#surprisePackageCarousel-{{ str_replace(" ", "", $name) }}" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left font-20-imp"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control product-carousel-slider carousel-control-m" href="#surprisePackageCarousel-{{ str_replace(" ", "", $name) }}" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right font-20-imp"></span>
					<span class="sr-only">Next</span>
				</a>
			@endif
		</div>
		<div class="clearfix"></div>
	</div>
@endif