<div style="background-color: #ddf9ff; color: #5a5a5a; padding: 7px 15px; text-align: left; border-bottom: 1px solid #efefef;" class="hide">
	<div class="col-xs-10 no-pad-l">
		<div class="font-15">Party Planning Made Simpler & Faster</div>
		<a href="{{ route("city.workflow.home", [getCityUrl()]) }}" target="_blank" class="workflow-promotion-plan-now-btn mar-t-5">PLAN NOW <i class="glyphicon glyphicon-arrow-right"></i></a>
	</div>
	<a class="mar-t-5 workflow-promotion-close col-xs-2" style="cursor: pointer; color: #5A5A5A; font-size: 17px; margin-top: 5px"><i class="glyphicon glyphicon-remove pull-right"></i></a>
	<div class="clearfix"></div>
</div>