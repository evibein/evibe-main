<div id="listFiltersWrap" class="list-filters-wrap">
	<div class="list-filters-top text-center">
		<div class="col-xs-2 col-sm-2 no-pad">
			<div id="listFilterClose" class="list-filter-close in-blk pull-left">
				<i class="material-icons">&#xE5CD;</i>
			</div>
		</div>
		<div class="col-xs-6 col-sm-6 no-pad-l text-left">
			<div class="list-filter-title in-blk">
				All Filters
			</div>
		</div>
		<div class="col-xs-4 col-sm-4 no-pad">
			<div class="list-filter-clear in-blk pull-right">
				@if ($data['filters']['clearFilter'] && isset($data['occasionId']))
					@if (request()->is('*/candle-light-dinner*'))
						<a href="{{ route('city.cld.list', $cityUrl) }}?ref=reset-filter" class="font-16">
							<i class="glyphicon glyphicon-remove-circle"></i> Clear All
						</a>
					@elseif($data['occasionId']==config("evibe.occasion.naming_ceremony.id"))
						<a href="{{ route('city.occasion.ncdecors.list', $cityUrl) }}?ref=reset-filter" class="font-16">
							<i class="glyphicon glyphicon-remove-circle"></i> Clear All
						</a>
					@elseif($data['occasionId']==config("evibe.occasion.baby-shower.id"))
						<a href="{{ route('city.occasion.bsdecors.list', $cityUrl) }}?ref=reset-filter" class="font-16">
							<i class="glyphicon glyphicon-remove-circle"></i> Clear All
						</a>
					@elseif($data['occasionId']==config("evibe.occasion.store-opening.id"))
						<a href="{{ route('city.occasion.sodecors.list', $cityUrl) }}?ref=reset-filter" class="font-16">
							<i class="glyphicon glyphicon-remove-circle"></i> Clear All
						</a>
					@else
						<a href="{{ route('city.occasion.'.config('evibe.type-event-route-name.'.$data['occasionId']).'.'.config('evibe.type-product-route-name.'.$data['mapTypeId']).'.list', $cityUrl) }}?ref=reset-filters" class="font-14">
							<i class="glyphicon glyphicon-remove-circle"></i> Clear All
						</a>
					@endif
				@endif
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="list-filters-content">
		@yield('surprise-filters')
		<div class="item-filters">
			<ul class="filters-list">
				@include("occasion.util.list-filters.search-filter-new")
				@include("occasion.util.list-filters.price-filter")
				@if(isset($data['filters']['allCategories']) && count($data['filters']['allCategories']))
					@if(isset($data['mapTypeId']) && (in_array($data['mapTypeId'], [config('evibe.ticket.type.decor'), config("evibe.ticket.type.cake")])))
						@include('occasion.util.list-filters.tags-filter')
					@elseif(isset($data['mapTypeId']) && ($data['mapTypeId'] == config('evibe.ticket.type.entertainment')))
						<li class="filter link-filter categories-filter" data-type="category">
							<div class="super-cat">
								<div class="pull-left">
									<a class="all-cats" href="#">All Categories</a>
								</div>
								<div class="clearfix"></div>
							</div>
							<ul class="no-mar no-pad ls-none item-sub-cats packages-sub-cats">
								@foreach ($data['filters']['allCategories'] as $catData)
									@if (array_key_exists($catData->id, $data['filters']['catCounts']))
										<li class="@if($data['filters']['active'] == $catData['url']) active @endif">
											<div class="pull-left">
												@if($data['filters']['catCounts'][$catData->id])
													<a data-url="{{ $catData['url'] }}" href="{{ route("city.occasion.birthdays.ent.list.category", [$cityUrl, $catData['url']]) }}?ref=category-filters">{{ $catData['name'] }}</a>
												@else
													<span class="disabled">{{ $catData['identifier'] }}</span>
												@endif
											</div>
											<div class="pull-right font-12 text-muted count">
												<span @if(!$data['filters']['catCounts'][$catData->id]) class="disabled" @endif>({{ $data['filters']['catCounts'][$catData->id] }})</span>
											</div>
											<div class="clearfix"></div>
										</li>
									@endif
								@endforeach
							</ul>
						</li>
					@else
						<li class="filter link-filter categories-filter" data-type="category">
							<div class="super-cat">
								<div class="pull-left">
									<a class="all-cats" href="#">All Categories</a>
								</div>
								<div class="clearfix"></div>
							</div>
							<ul class="no-mar no-pad ls-none item-sub-cats packages-sub-cats">
								@foreach ($data['filters']['allCategories'] as $catId => $catData)
									@if (array_key_exists($catId, $data['filters']['catCounts']))
										<li class="@if (!count($catData['child']) && $data['filters']['active'] == $catData['url']) active @endif">
											<div>
												<div class="pull-left">
													@if(!count($catData['child']) && $data['filters']['catCounts'][$catId])
														<a class="decor-cat-link" data-url="{{ $catData['url'] }}" href="?category={{ $catData['url'] }}">{{ $catData['name'] }}</a>
													@elseif(count($catData['child']))
														<span>{{ $catData['name'] }}</span>
													@else
														<span class="disabled">{{ $catData['name'] }}</span>
													@endif
												</div>
												@if (!count($catData['child']))
												@endif
												<div class="clearfix"></div>
											</div>
											@if (count($catData['child']))
												<ul class="no-mar no-pad item-child-cats">
													@foreach($catData['child'] as $childCatId => $childCatData)
														<li class="@if ($data['filters']['active'] == $childCatData['url']) active @endif">
															<div class="pull-left">
																@if($data['filters']['catCounts'][$childCatId])
																	<a class="decor-cat-link" data-url="{{ $childCatData['url'] }}" href="?category={{ $childCatData['url'] }}">{{ $childCatData['name'] }}</a>
																@else
																	<span class="disabled">{{ $childCatData['name'] }}</span>
																@endif
															</div>
															<div class="clearfix"></div>
														</li>
													@endforeach
												</ul>
											@endif
										</li>
									@endif
								@endforeach
							</ul>
						</li>
					@endif
				@endif
				@if(isset($data['filters']['allLocations']['all']) && count($data['filters']['allLocations']['all']))
					@include('occasion.util.list-filters.location-filter')
				@endif
			</ul>
		</div>
	</div>
	<div class="list-filters-bottom">

	</div>
</div>