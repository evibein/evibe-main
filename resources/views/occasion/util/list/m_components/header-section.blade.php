@include("occasion.util.common.m_header-section")

<!-- Need to check if there are multiple options to show filter and sort options -->
<div class="scroll-anchor"></div>
@if(!($agent->isTablet()))
	<div class="mobile-list-sort-filter">
		@include("occasion.util.list.m_components.bottom-section-wrap")
	</div>
@endif
<div class="scroll-empty-div"></div>

<div class="clearfix"></div>