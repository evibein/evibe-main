@if(isset($data['mapTypeId']))
	<input type="hidden" id="hidRatingsUrl" value="{{ route('ajax.list.options-ratings', ['optionTypeId' => $data['mapTypeId']]) }}">

	<input type="hidden" id="hidOptionTypeId" value="{{ $data['mapTypeId'] }}">
@endif