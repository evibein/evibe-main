<div class="list-content-wrap">
	@if($data['decors']->count() > 0 || $data['filters']['clearFilter'])
		<div class="list-options-wrap pad-t-15">
			@if ($data['decors']->count())
				<div class="col-xs-12 col-sm-12 no-pad-r">
					@php $ids = []; $count = 1; @endphp
					@foreach ($data['decors'] as $decor)
						@php
							$ids[] = $decor->id;
							if($data['occasionId']==config("evibe.occasion.naming_ceremony.id"))
							{
								$renderedProfileUrl = route('city.occasion.ncdecors.profile', [$cityUrl, $decor->url]);
							}
							elseif($data['occasionId']==config("evibe.occasion.baby-shower.id"))
							{
								$renderedProfileUrl = route('city.occasion.bsdecors.profile', [$cityUrl, $decor->url]);
							}
							elseif($data['occasionId']==config("evibe.occasion.store-opening.id"))
							{
								$renderedProfileUrl = route('city.occasion.sodecors.profile', [$cityUrl, $decor->url]);
							}
							else
							{
								$renderedProfileUrl = route('city.occasion.'.config('evibe.type-event-route-name.'.$data['occasionId']).'.decors.profile', [$cityUrl, $decor->url]);
							}
						@endphp
						<div class="col-xs-6 col-sm-6 no-pad-l pad-b-20">
							<div class="list-option-card">
								<div class="list-option-image-wrap text-left">
									<a class="list-option-link option-profile-image option-profile-image-{{ $decor->id }}" data-option-id="{{ $decor->id }}" data-image-alt-text="{{ $decor->name }} {{ config('evibe.type-event-route-name.'.$data['occasionId']) }} party decoration in {{ getCityUrl() }} @ {{ $decor->min_price }}" href="{{ $renderedProfileUrl }}?ref=mlp#image">
										<div class="content-wrapper">
											<div class="placeholder-option-mobile">
												<div class="animated-background"></div>
											</div>
										</div>
									</a>
								</div>
								<div class="list-option-content-wrap">
									<div class="list-option-title">
										<a class="list-option-link" href="{{ $renderedProfileUrl }}?ref=mlp#title">
											{{ $decor->name }}
										</a>
									</div>
									<div class="list-option-price">
										@if ($decor->worth && $decor->worth > $decor->min_price)
											<div class="list-price-worth in-blk mar-r-5">
												@price($decor->worth)
											</div>
										@endif
										@if($decor->min_price && $decor->worth)
											<div class="list-price-off in-blk">
												(@offPrice($decor->min_price, $decor->worth) OFF)
											</div>
										@endif
									</div>
									<div class="">
										<div class="list-option-price pull-left">
											<div class="list-main-price in-blk">
												@price($decor->min_price)
												<span class="hide">
												@if ($decor->max_price && $decor->max_price > $decor->min_price)
													- @price($decor->max_price)
												@endif
												</span>
											</div>
											<div class="clearfix"></div>
										</div>
										@php $providerRating = $decor->provider->rating; @endphp
										<div class="list-option-rating pull-right">
											@if($providerRating && $providerRating->review_count > 0)
												<span class="mar-r-4">
												<input type="number" class="avg-rating hide" value="{{ $providerRating->avg_rating }}" title="{{ $providerRating->avg_rating }} average rating for provider of {{ $decor->name }}"/>
											</span>
												<span class="list-rating-count in-blk">({{ $providerRating->review_count }})</span>
											@else
												<span class="list-rating-count no-mar-l in-blk">(<i>new arrival</i>)</span>
											@endif
										</div>
										<div class="clearfix"></div>
										<div class="list-price-tag hide">
											<i class="glyphicon glyphicon-thumbs-up"></i> Best Price
										</div>
									</div>
									@if($data['occasionId']  !=  config("evibe.occasion.pre-post.id"))
										<div class="in-blk hide">
											@include('app.shortlist_results', [
																"mapId" => $decor->id,
																"mapTypeId" => $data['mapTypeId'],
																"occasionId" => $data['occasionId'],
																"cityId" => $data['cityId']
															])
										</div>
									@endif
								</div>
							</div>
						</div>
						@if(($count % 2) == 0)
							<div class="clearfix"></div> @endif
						@include("occasion.util.list.m_components.decor.custom-design", ["currentPage" => $data['decors']->currentPage(), "loopIteration" => $count, "perPage" => $data['decors']->perPage()])
						@php $count++; @endphp
					@endforeach
					@if(($count % 2) == 0)
						<div class="clearfix"></div>
					@endif
					<input type="hidden" id="hidPaginatedIds" data-ids="{{ json_encode($ids) }}"> <!-- array has already been declared as empty -->
				</div>
				<div class="clearfix"></div>
			@else
				<div class="list-no-results-wrap text-center">
					<div class="list-no-results-title no-mar">
						<i class="glyphicon glyphicon-warning-sign"></i>
						Oops, no Decoration Styles were found matching your filters.
					</div>
					<div class="pad-t-20">
						@if($data['occasionId']==8)
							<a href="{{ route('city.occasion.ncdecors.list', $cityUrl) }}?ref=no-results" class="btn btn-danger btn-lg">
								<i class="glyphicon glyphicon-remove-circle valign-mid"></i> Reset All Filters
							</a>
						@elseif($data['occasionId']==19)
							<a href="{{ route('city.occasion.bsdecors.list', $cityUrl) }}?ref=no-results" class="btn btn-danger btn-lg">
								<i class="glyphicon glyphicon-remove-circle valign-mid"></i> Reset All Filters
							</a>
						@else

							<a href="{{ route('city.occasion.'.config('evibe.type-event-route-name.'.$data['occasionId']).'.decors.list', $cityUrl) }}?ref=no-results" class="btn btn-danger btn-lg">
								<i class="glyphicon glyphicon-remove-circle valign-mid"></i> Reset All Filters
							</a>
						@endif
					</div>
				</div>
			@endif
		</div>
		<div>
			@include('app.modals.auto-popup',
									[
									"cityId" => $data['cityId'],
									"occasionId" => $data['occasionId'],
									"mapTypeId" => $data['mapTypeId'],
									"mapId" => "",
									"optionName" => "",
									"countries" => (isset($data['countries']) && count($data['countries'])) ? $data['countries'] : []
									])
		</div>
		@if(isset($data['totalCount']) && ($data['totalCount'] > config('evibe.paginate-options.generic-mobile')))
			<div class="pages mobile-list-pagination text-center mar-b-30 text-center"> <!-- pagination begin -->
				<ul class="pagination">
					<li class="page-item @if(($data['decors']->appends($data['filters']['queryParams'])->currentPage()) == 1) disabled @endif">
						<a class="page-link btn-smp-pgn-nav-mob btn-left-pgn-nav-mob" @if(($data['decors']->appends($data['filters']['queryParams'])->currentPage()) != 1) href="{{ $data['decors']->appends($data['filters']['queryParams'])->previousPageUrl() }}" @endif aria-label="Previous">
							<span aria-hidden="true">&laquo; previous</span>
							<span class="sr-only">Previous</span>
						</a>
					</li>
					<li class="page-item @if(($data['decors']->appends($data['filters']['queryParams'])->currentPage()) == ($data['decors']->appends($data['filters']['queryParams'])->lastPage())) disabled @endif">
						<a class="page-link btn-smp-pgn-nav-mob btn-right-pgn-nav-mob no-mar-r" @if(($data['decors']->appends($data['filters']['queryParams'])->currentPage()) != ($data['decors']->appends($data['filters']['queryParams'])->lastPage())) href="{{ $data['decors']->appends($data['filters']['queryParams'])->nextPageUrl() }}" @endif aria-label="Next">
							<span aria-hidden="true">next &raquo;</span>
							<span class="sr-only">Next</span>
						</a>
					</li>
				</ul>
			</div>
		@endif
	@else
		<div class="list-content-empty-wrap text-center">
			<div class="list-content-empty-img">
				<img src="{{ $galleryUrl }}/img/icons/floating-balloons.png">
			</div>
			<div class="list-content-empty-text">
				We are adding amazing decorations soon, keep checking this page.
			</div>
		</div>
	@endif
</div>