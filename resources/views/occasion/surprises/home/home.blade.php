@extends('layout.base-surprises')

@section("header")
	@include('base.home.header.header-home-city')
@endsection

@section("content")
	<div class="home-screen">
		<a href="#" class="scrollup">Scroll</a>

		@if (isset($data['views']['home']) && view()->exists($data['views']['home']))
			@include($data['views']['home'])
		@endif

		@include('base.home.why-us')

		@include("base.home.enquiry-form")

		<div class="occasion-customer-reviews"></div>

		@if (isset($data['views']['press']) && view()->exists($data['views']['press']))
			@include($data['views']['press'])
		@endif

		@include('base.home.vendor-comment')
	</div>
@endsection

@include("layout.product.package.list.c")

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			(function init() {
				$(".rslides").responsiveSlides({
					speed: 500,
					timeout: 5000
				});

				$('#surprisePackageCarousel').carousel('pause');
			})();

		});
	</script>
@endsection