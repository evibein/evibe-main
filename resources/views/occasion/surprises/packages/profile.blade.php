@extends("layout.occasion.surprises.base-surprise-profile")

@section("content")
	<div class="col-md-12 bg-white">
		<a href="#" class="scrollup">Scroll</a>
		<div class="col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0 package-profile"> <!-- package profile begin -->
			<div class="col-md-12 no-pad mar-b-15">
				<div class="breadcrumb-section">
					<ol class="breadcrumb text-center__400"> <!-- breadcrumbs begin -->
						<li><a href="{{ route('city.home', $cityUrl) }}">{{$cityName}}</a></li>
						<li>
							<a href="{{ route('city.occasion.surprises.home', $cityUrl) }}">Surprises</a>
						</li>
						<li>
							<a href="{{ route('city.occasion.surprises.package.list', $cityUrl) }}">Packages</a>
						</li>
						<li class="active">{{ $data['package']->code }}</li>
					</ol>
				</div>
				<div class="header-wrap"> <!-- header begin -->
					<div class="col-md-12 no-pad">
						<div class="col-md-8"> <!-- title begin -->
							<h4 class="no-mar mar-t-10 text-center__400">
								[#{{ $data['package']->code }}] {{ $data['package']->name }}
								<span id="verified" class="text-success"><i class="material-icons">&#xE8E8;</i></span>
								<div class="mdl-tooltip mdl-tooltip--large" for="verified">Verified Provider</div>
							</h4>
						</div>

						<div class="col-md-4 text-center mar-t-10__400 no-pad-r"> <!-- actions begin -->
							@include('app.shortlist_detail')
							<a class="btn btn-default btn-view-more btn-best-deal btn-book-now scroll-item" data-toggle="modal" data-target="#bookNowModal">
								<i class="material-icons valign-mid">&#xE8CC;</i>
								<span class="valign-mid">Book Now</span>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="info-panel">
				<div class="row">
					<div class="col-md-8 mar-t-10">
						<!-- gallery begin -->
						<div class="gallery-wrap hide" role="tabpanel">
						@if ($data['gallery']['showGalleryTabs']) <!-- nav tabs begin -->
							<ul class="nav nav-pills mar-l-20" role="tablist">
								<?php $count = 0; ?>
								@foreach($data['gallery']['images'] as $cat => $catImages)
									<li role="presentation" class="@if(!$count++) active @endif">
										<a href="#catGalId{{ $count }}" data-toggle="pill">
											{{ $cat }}
										</a>
									</li>
								@endforeach
							</ul>
							@endif
							@if (count($data['gallery']['images']))
								<div class="tab-content mar-t-15"> <!-- tab panes begin -->
									<?php $count = 0; ?>
									@foreach($data['gallery']['images'] as $cat => $catImages)
										<div role="tabpanel" class="tab-pane fade @if(!$count++) in active @endif" id="catGalId{{ $count }}">
											@if (count($catImages))
												<div class="fotorama" data-width="100%" data-ratio="900/600" data-maxheight="400" data-nav="thumbs" data-autoplay="true" data-thumbwidth="80" data-thumbmargin="10" data-keyboard="true" data-captions="true" @if($data['gallery']['showThumb']) data-enableifsingleframe="true" @endif>
													@foreach ($catImages as $key => $image)
														<a href="{{ $image['url'] }}" data-caption="{{ $image['title'] }}">
															<img src="{{ $image['thumb'] }}" alt="{{ $image['title'] }}"/>
														</a>
													@endforeach
												</div>
											@else
												<i class="text-danger">No images found in this category.</i>
											@endif
										</div>
									@endforeach
								</div>
							@else
								<div>No images found</div>
							@endif
						</div>
						<!-- gallery end -->
					</div>
					<div class="col-md-4 mar-t-10">
						<div class="item-profile highlights-card"> <!-- highlights begin -->
							<div class="hs-section">
								<div class="text-center mar-b-5 border-b-1"> <!-- protection begin -->
									<img src="{{ $galleryUrl }}/img/app/decor_protection_260x66.jpg" alt="Evibe delivery guarantee"/>
								</div>
								<div class="card-item text-center price-item pad-t-10"> <!--- Price info --->
									@if ($data['package']->price_worth && $data['package']->price_worth > $data['package']->price)
										<div class="price-worth in-blk">
											@price ($data['package']->price_worth)
										</div>
									@endif
									<div class="price-val in-blk">
										@price ($data['package']->price)
										@if ($data['package']->price_max && $data['package']->price_max > $data['package']->price)
											-
											@price( $data['package']->price_max )
										@endif
									</div>
									@if ($data['package']->price_per_extra_guest)
										<div class="pad-t-8">
											(@price($data['package']->price_per_extra_guest) / extra guest)
										</div>
									@endif
								</div>
								<div class="card-item card-supporting-text">
									@if($data['package']->map_type_id == config("evibe.ticket.type.venue"))
										@if($data['package']->provider->area)
											<div class="card-item"> <!-- location -->
												<div class="card-item__title">
													<i class="glyphicon glyphicon-map-marker"></i> Location:
												</div>
												<div class="card-item__body">{{ $data['package']->provider->area->name }}</div>
												@if( $data['package']->provider->landmark)
													<div class="highlights-landmark">({{ $data['package']->provider->landmark }})</div>
												@endif
											</div>
										@endif
										@if($data['package']->propertyType)
											<div class="card-item"> <!-- property type -->
												<div class="card-item__title">
													<i class="glyphicon glyphicon-home"></i> Property Type:
												</div>
												<div class="card-item__body">{{ $data['package']->propertyType->identifier }}</div>
											</div>
											@if($data['package']->check_in)
												<div class="card-item"> <!-- check in -->
													<div class="card-item__title">
														<i class="glyphicon glyphicon-time"></i> Check In:
													</div>
													<div class="card-item__body">{{ $data['package']->check_in }}</div>
												</div>
											@endif
											@if($data['package']->check_out)
												<div class="card-item"> <!-- check out -->
													<div class="card-item__title">
														<i class="glyphicon glyphicon-time"></i> Check Out:
													</div>
													<div class="card-item__body">{{ $data['package']->check_out }}</div>
												</div>
											@endif
										@endif
									@endif
									@if (isset($data['highlights']) && count($data['highlights']))
										@foreach($data['highlights'] as $highlightItem)
											<div class="card-item"> <!-- dynamic highlights fields -->
												<div class="card-item__title">
													<i class="{{ $highlightItem['icon'] }}"></i> {{ $highlightItem['name'] }}:
												</div>
												<div class="card-item__body">{{ $highlightItem['value'] }}</div>
											</div>
										@endforeach
									@endif
								</div>
								<div class="card-item card-actions text-center hide__400">
									<a class="btn btn-default btn-view-more btn-more-details scroll-item" data-scroll="info-container">
										View More Details
									</a>
								</div>
							</div>
							<div class="hs-section social-section text-center">
								@include('app.social-share-plugin')
							</div>
						</div>
						<div class="why-us-card hide"> <!-- why Evibe? begin -->
							<div class="card-item">
								<div class="card-item__title">
									<h6 class="card-title-text mdl-typography--title title">Why Book With Evibe?</h6>
								</div>
								<div class="card-item__body">
									<ul class="no-mar no-pad ls-none">
										<li>
											<span class="text-e"><i class="glyphicon glyphicon-star"></i></span>
											<span><b>100% Delivery</b> Guarantee</span>
										</li>
										<li>
											<i class="glyphicon glyphicon-star"></i>
											<span>Hosted <b>1000+ Events</b></span>
										</li>
										<li>
											<i class="glyphicon glyphicon-star"></i>
											<span>Best <b>Price &amp; Quality</b></span>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="col-md-12 menu-top-b bg-white sticky hidden-sm hidden-xs">
		<div class="col-lg-10 col-md-12 col-lg-offset-1 col-md-offset-0 tabs-scroll">
			<div class="head-scroll-fixed border-b-1 col-md-12">
				<div class="col-md-8">
					<h5 class="mar-t-10 mar-b-10 text-center__400 ov-text-no-wrap">
						[#{{ $data['package']->code }}] {{ $data['package']->name }}
						<span id="verified" class="text-success"><i
									class="material-icons">&#xE8E8;</i></span>
						<div class="mdl-tooltip mdl-tooltip--large" for="verified">Verified Provider</div>
					</h5>
				</div>
				<div class="col-md-4 mar-t-15">
					<div class="card-item item-profile price-item">
						@if ($data['package']->price_worth && $data['package']->price_worth > $data['package']->price)
							<div class="price-worth in-blk">
								<strike> @price ($data['package']->price_worth) </strike>
							</div>
						@endif
						<div class="price-val in-blk">
							@price ($data['package']->price)
							@if ($data['package']->price_max && $data['package']->price_max > $data['package']->price)
								-
								@price( $data['package']->price_max )
							@endif
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="col-md-12">
				<ul class="menu-scroll col-md-10" id="top-menu">
					@if ($data['package']->info)
						<li><a href="#inclusions" data-scroll="inclusions">Inclusions</a></li>
					@endif
					@if ($data['package']->prerequisites)
						<li><a href="#prerequisites">Prerequisites</a></li>
					@endif
					@if ($data['package']->facts)
						<li><a href="#facts">Facts</a></li>
					@endif
					@if ($data['ratings']['total']['count'])
						<li><a href="#reviews"> Reviews </a></li>
					@endif
					@if ($data['package']->terms)
						<li><a href="#terms">Terms</a></li>
					@endif
				</ul>
				<div class="col-md-2 interested">
					<div class="col-md-2 interested">
						<a class="btn btn-default btn-view-more btn-best-deal btn-book-now scroll-item" data-toggle="modal" data-target="#bookNowModal">
							<i class="material-icons valign-mid">&#xE8CC;</i>
							<span class="valign-mid">Book Now</span>
						</a>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div> <!-- sticky header -->
	<div class="col-md-12 bg-light-gray pad-t-20" id="info-container">
		<div class="col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0">
			<div class="info-more-panel">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-8">
							@if ($data['package']->info)
								<div id="inclusions" class="main-card info-card"> <!-- inclusions begin -->
									<div class="card-title">
										<h5 class="card-title-text">Inclusions</h5>
									</div>
									<div class="card-info-text no-pad-t">
										<div class="mar-b-10">{!! $data['package']->info !!}</div>
										@if (isset($data['moreInclusions']) && count($data['moreInclusions']))
											@foreach($data['moreInclusions'] as $inclusionItem)
												<div class="pkg-inc-sec"> <!-- show all dynamic fields in inclusions section -->
													<div class="pkg-inc-sec-title">{{ $inclusionItem['name'] }}</div>
													<div class="pad-t-10">{{ $inclusionItem['value'] }}</div>
												</div>
											@endforeach
										@endif
										@if (!empty($data['itinerary']))
											<div class="pkg-inc-sec"> <!-- Itinerary -->
												<div class="pkg-inc-sec-title">Itinerary</div>
												<div class="pad-t-10">
													@foreach ($data['itinerary'] as $id => $itinerary)
														@if(isset($itinerary['values']) && count($itinerary['values']) > 0)
															<div class="itinerary-data">
																<table class="table table-bordered">
																	<tbody>
																	@foreach ($itinerary['values'] as $itineraryValue)
																		<tr>
																			<td class="w-30pc">{{ $itineraryValue['time'] }}</td>
																			<td>{{ $itineraryValue['info'] }}</td>
																		</tr>
																	@endforeach
																	</tbody>
																</table>
															</div>
														@endif
													@endforeach
												</div>
											</div>
										@endif
										@if (count($data['tags']))
											<ul class="tags-lists">
												@foreach ($data['tags'] as $key => $tag)
													<li><i class="glyphicon glyphicon-tag"></i> {{ $tag }}
													</li>
												@endforeach
											</ul>
										@endif
									</div>
								</div>
							@endif

							@if ($data['package']->prerequisites)
								<div id="prerequisites" class="main-card more-info-card"> <!-- prerequisites begin -->
									<div class="card-title">
										<h5 class="card-title-text">Prerequisites</h5>
									</div>
									<div class="card-info-text no-pad-t">
										{!! $data['package']->prerequisites !!}
									</div>
								</div>
							@endif

							@if ($data['package']->facts)
								<div id="facts" class="main-card more-info-card"> <!-- facts -->
									<div class="card-title">
										<h5 class="card-title-text">Facts</h5>
									</div>
									<div class="card-info-text no-pad-t">
										{!! $data['package']->facts !!}
									</div>
								</div>
							@endif
							@include("app.review.partner", [
								"reviews"=> $data["ratings"],
								"isShowLocation" => ($data["package"]->map_type_id == config("evibe.ticket.type.venue")) ? false : true,
								"showAllReviewsUrl" => route("provider-reviews:all", [
									getSlugFromText($data["package"]->name),
									($data["package"]->map_type_id == config("evibe.ticket.type.venue") ? config("evibe.ticket.type.venue") : config("evibe.ticket.type.planner")),
									$data["package"]->provider->id
								])
							])
							@if ($data['package']->terms)
								<div id="terms" class="main-card terms-card"> <!-- terms of booking -->
									<div class="card-title">
										<h5 class="card-title-text">Terms of Booking</h5>
									</div>
									<div class="card-info-text no-pad-t">
										{!! $data['package']->terms !!}
									</div>
								</div>
							@endif
						</div>
						<div class="col-md-4">
							<div class="main-card provider-rating-card"> <!-- provider rating card begin -->
								<div class="card-info-text">
									<div class="font-16">Provider Code:
										<b>{{ $data['package']->provider->code }}</b>
									</div>
									<div class="font-18 pad-t-10">
										<span>
											<input type="number" class="avg-rating hide" value="{{ $data['ratings']['total']['avg'] }}" title="{{ $data['ratings']['total']['avg'] }} average rating for provider of {{ $data['package']->name }}"/>
										</span>
										@if($data['ratings']['total']['count'])
											<span id="reviews" class="card-item__title-hint scroll-item cur-point" data-scroll="reviews"> ({{ $data['ratings']['total']['count'] }} reviews) </span>
										@else
											<span class="card-item__title-hint cur-point"> (0 reviews) </span>
										@endif
									</div>
								</div>
							</div>
							@if ($data['package']->map_type_id === config("evibe.ticket.type.venue"))
								@include("app.forms.profile_enquiry_form", [
									"enquiryUrl" => route("ticket.package", [$data['occasionId'], $data['package']->id]),
									"hidePartyLocation" => true
								])
							@else
								@include("app.forms.profile_enquiry_form", [
									"enquiryUrl" => route("ticket.package", [$data['occasionId'], $data['package']->id])
								])
							@endif
							@include('app.process.venue')
							@include('app.process.report-issue')
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="col-md-12 bg-white similar-results">
		<div class="col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0">
			@if (count($data['similarPackages']))
				<div class="col-md-12">
					<div class="similar-items-wrap similar-decors-wrap">
						<h4 class=" text-center">Similar Packages</h4>
						<div class="similar-decors-list mdl-grid">
							@foreach ($data['similarPackages'] as $package)
								<div class="col-xs-12 col-sm-6 col-md-4 no-pad-l">
									<div class="item-result decor-style">
										<div class="img-wrap">
											<a href="{{ route('city.occasion.surprises.package.profile', [$cityUrl, $package->url]) }}">
												<img src="{{ $package->getProfileImg() }}" alt="{{ $package->getProfileImg() }} profile picture">
											</a>
											@include('app.shortlist_results', [
															"mapId" => $package->id,
															"mapTypeId" => $data['mapTypeId'],
															"occasionId" => $data['occasionId'],
															"cityId" => $data['cityId']
														])
										</div>
										<div class="details">
											<div class="title">
												<a href="{{ route('city.occasion.surprises.package.profile', [$cityUrl, $package->url]) }}">
													{{ $package->name }}
												</a>
											</div>
											<div class="col-md-6 col-sm-12 col-xs-12 pad-r-0">
												<div class="rating-wrap">
													<input type="number" value="{{ $package->getProviderAvgRating() }}" class="provider-avg-rating hide" title="{{ $package->getProviderAvgRating() }} average rating for package {{ $package->name }}"/>
												</div>
											</div>
											<div class="col-md-6 no-padding hidden-xs hidden-sm">
												<a href="{{ route('city.occasion.surprises.package.profile', [$cityUrl, $package->url]) }}"
														class="btn btn-primary btn-view-more btn-venue-details pull-right">View Details</a>
											</div>
											<div class="clearfix"></div>

											<div class="item-info pad-t-5">
												<div class="price-tag text-left mar-t-5 col-md-12 pad-r-0">
													<span class="price">
														@price($package->price)
														@if ($package->price_max && $package->price_max > $package->price)
															-
															@price($package->price_max)
														@endif
														@if ($package->price_worth && $package->price_worth > $package->price)
															<span class="price-worth">
																@price($package->price_worth)
															</span>
														@endif
													</span>
												</div>

												<div class="clearfix"></div>
											</div>
										</div>
									</div>
								</div>
							@endforeach
							<div class="clearfix"></div>
						</div>
					</div>
				</div> <!-- similar decors begin -->
				<div class="clearfix"></div>
			@endif
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
@endsection

@section("modals")
	@parent
	@include('auto-booking/checkout-modals/surprises-packages', [
		"mapId" => $data['package']->id,
		"mapTypeId" => config('evibe.ticket.type.package'),
		"typeId" => config('evibe.ticket.type.surprises'),
		"data" => $data['package']
	])
@endsection