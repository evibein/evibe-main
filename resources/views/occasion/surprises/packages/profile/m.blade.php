@extends("layout.product.package.profile.m")

@section("header")
	@include('occasion.surprises.header.m_profile')
@endsection

@section('custom-head')
	@if(isset($data['shortlistData']['mapTypeId']) && isset($data['shortlistData']['mapId']) && ($data['shortlistData']['mapTypeId'] == "21") && ($data['shortlistData']['mapId'] == "2410"))
		<meta name="DC.title" content="EVIBECLD0001"/>
		<meta name="geo.region" content="IN"/>
		<meta name="geo.placename" content="Hyderabad"/>
		<meta name="geo.position" content="17.418151;78.450695"/>
		<meta name="ICBM" content="17.418151, 78.450695"/>
	@endif
@endsection

@section('push-notifications')
	@include('plugins.onesignal', ['pushOccasion' => 'surprises'])
@endsection

@section("content")
	<div class="col-xs-12 col-sm-12 col-md-12 no-pad bg-white mobile-pro-page">
		@include("occasion.util.profile.m_components.package.gallery")

		@if((request()->is('*surprises/theatre-movie-wishes*')))
			@include("occasion.util.profile.m_components.landing-profile-title-section")
		@else
			@include("occasion.util.profile.m_components.package.title-section")
		@endif

		@include("occasion.util.profile.m_components.package.product-description-section")

		@include("occasion.util.profile.m_components.loading-similar-products")
		<div id="showSimilarProducts"></div>
		
		@include("occasion.util.profile.d_components.loading-category-cards")
		<div id="showOtherCategoryCards"></div>

		@include("occasion.util.profile.m_components.package.hidden-data-section")

		@include("occasion.util.profile.m_components.package.call-to-action")

		<hr class="mobile-header-phone">
		@include('base.home.why-us-vertical')
	</div>
	<div class="clearfix"></div>
@endsection