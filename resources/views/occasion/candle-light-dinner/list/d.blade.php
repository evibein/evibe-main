@extends("layout.product.package.list.d")

@section("header")
	@if($agent->isTablet())	
		@include('occasion.surprises.header.m_list')
	@else
		@include('occasion.surprises.header.header')
	@endif
@endsection

@section('push-notifications')
	@include('plugins.onesignal', ['pushOccasion' => 'surprises'])
@endsection

@section("content")
	<div class="bg-white desktop-list-page">
		<div class="col-md-12 col-lg-12 no-pad hide">
		</div>
		<div class="header-border full-width mar-b-10"></div>
		<div class="col-md-12 col-lg-12">
			@include("occasion.util.list.d_components.package.top-bar-section")
		</div>

		@include("occasion.util.list.d_components.package.hidden-data-section")

		<div class="col-md-12 col-lg-12 pad-r-20">
			@include("occasion.util.list.d_components.package.content-section")
		</div>
		<div class="clearfix"></div>
	</div>
@endsection

@section("footer")
	@parent
	@include('base.home.why-us-list')
	<div class="footer-wrap">
		@if (isset($data['views']['footer']) && view()->exists($data['views']['footer']))
			@include($data['views']['footer'])
		@endif
	</div>
@endsection