@extends("layout.product.package.list.m")

@section("header")
	@include('occasion.surprises.header.m_list')
@endsection

@section('push-notifications')
	@include('plugins.onesignal', ['pushOccasion' => 'surprises'])
@endsection

@section("content")
	<div class="bg-white mobile-list-page">

		<div class="list-content-section" id="listContentSection">

			@include("occasion.util.list.m_components.package.top-section")

			@include("occasion.util.list.m_components.package.content-wrap")

			@include("occasion.util.list.m_components.package.hidden-data-section")

		</div>

		<div class="mobile-list-footer"></div>

	</div>

	@include("occasion.util.list.m_components.package.surprise-filters")

	@include("occasion.util.list.m_components.sort-section")

@endsection

@section("footer")
	@parent
	@include('base.home.why-us')
	<div class="footer-wrap">
		@if (isset($data['views']['footer']) && view()->exists($data['views']['footer']))
			@include($data['views']['footer'])
		@endif
	</div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			if ($("#mblPriceFilterSticky").length > 0) {
				var divHeaderTop = $('.price-filter-wrap').offset().top;
				$(window).scroll(function () {
					var window_top = $(window).scrollTop() + 55;
					if (typeof Cookies.get('mobPromoVDayHeaderClose') === 'undefined') {
						window_top = $(window).scrollTop() + 95;
					}

					if (window_top > divHeaderTop) {
						if (!$('#mblPriceFilterSticky').is('.sticky')) {
							$("#mblPriceFilterSticky").addClass('sticky-behave').removeClass('hide');
						}

						if (typeof Cookies.get('mobPromoVDayHeaderClose') === 'undefined') {
							$('.sticky-behave').css('top', '55px');
						} else {
							$('.sticky-behave').css('top', '55px');
						}
					} else {
						$('#mblPriceFilterSticky').removeClass('sticky-behave').addClass('hide');
					}
				});
			}
		});
	</script>
@endsection