@extends("layout.occasion.pre-post.base-pre-post-profile")

@section("content")
	<div class="col-md-12 bg-white">
		<a href="#" class="scrollup">Scroll</a>
		<div class="col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0"> <!-- venue profile begin -->
			<div class="col-md-12 no-pad mar-b-15">
				<div class="breadcrumb-section"> <!-- breadcrumbs begin -->
					<ol class="breadcrumb text-center__400">
						<li><a href="{{ route('city.home', $cityUrl) }}">{{$cityName}}</a></li>
						<li>
							<a href="{{ route('city.occasion.pre-post.home', $cityUrl) }}">Engagement | Wedding Reception</a>
						</li>
						<li><a href="{{ route('city.occasion.pre-post.venues.list', $cityUrl) }}">Venues</a></li>
						<li class="active">{{ $data['hall']['code'] }}</li>
					</ol>
				</div>
				<div class="header"> <!-- header begin -->
					<div class="col-md-12 no-pad">
						<div class="col-md-8">
							<h4 class="no-mar mar-t-10 text-center__400">
								[#{{ $data['hall']['code'] }}] {{ $data['hall']['type'] }} at {{ $data['venue']['area'] }}
							</h4>
							<div class="mdl-typography--subhead mdl-typography--subhead-light hide"></div>
						</div>
						<div class="col-md-4 no-pad text-right mar-tb-15__400 no-pad-r">
							@include('app.shortlist_detail')
							<a class="btn btn-default btn-view-more btn-best-deal scroll-item" data-scroll="info-container">
								<i class="material-icons valign-mid">&#xE8DC;</i>
								<span class="valign-mid">Get Best Deal</span>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="venue-snapshot"> <!-- snapshot begin -->
				<div class="row ">
					<div class="col-md-8 mar-t-10"> <!-- gallery images begin -->
						@if (count($data['gallery']) > 0)
							<div class="loading-img-wrap">
								<img src="/images/loading.gif" alt="">
								<div class="pad-t-10">loading images...</div>
							</div>
							<div class="venue-gallery-wrap hide">
								<div class="fotorama" data-width="100%" data-ratio="900/600" data-maxheight="400" data-nav="thumbs" data-autoplay="true" data-thumbwidth="80" data-thumbmargin="10" data-keyboard="true">
									@foreach ($data['gallery'] as $image)
										<a href="{{ $image['url'] }}"><img src="{{ $image['thumb'] }}" alt="{{ $image['title'] }}"/></a>
									@endforeach
								</div>
							</div>
						@else
							<div>No images found</div>
						@endif
					</div>
					<div class="col-md-4 mar-t-10 no-padding">
						<div class="item-profile highlights-card"> <!-- highlights begin -->
							<div class="hs-section">
								<div class="card-item">
									<div class="card-item__title pad-r-6 in-blk">
										<i class="material-icons">&#xE7FB;</i> Capacity:
									</div>
									<div class="card-item__body in-blk">
										<div class="in-blk card-item__sub">
											<span class="card-item__val">
												{{ $data['hall']['capacityMin'] }} - {{ $data['hall']['capacityMax'] }}
												@if ($data['hall']['capacityFloat'] && $data['hall']['capacityMax'] != $data['hall']['capacityFloat'])
													(floating {{ $data['hall']['capacityFloat'] }})
												@endif
											</span>
										</div>
									</div>
								</div>
								<div class="card-item">
									<div class="card-item__body"> <!-- pricing info -->
										@if($data['venue']['priceMinRent'] || $data['hall']['priceMinRent'])
											<div class="card-item">
												<div class="card-item__title pad-r-6 in-blk">
													@if($data['hall']['minRentDuration'])
														<i class="material-icons">&#xE870;</i> Rent ({{ $data['hall']['minRentDuration'] }} Hrs):
													@elseif(!$data['hall']['minRentDuration'] && $data['venue']['minRentDuration'])
														<i class="material-icons">&#xE870;</i> Rent ({{ $data['venue']['minRentDuration'] }} Hrs):
													@else
													@endif
												</div>
												<div class="card-item__body in-blk">
													<div class="in-blk card-item__sub">
												<span class="card-item__val">
													@if ($data['hall']['priceMinRent'])
														@if ($data['hall']['worthRent'] && $data['hall']['worthRent'] > $data['hall']['priceMinRent'])
															<span class="mar-r-4"><del>@price($data['hall']['worthRent'])</del></span>
														@endif
														@price($data['hall']['priceMinRent'])
														@if ($data['hall']['priceMaxRent'] && $data['hall']['priceMaxRent'] > $data['hall']['priceMinRent'])
															- @price($data['hall']['priceMaxRent'])*
														@else
															* <span class="card-item__subtitle">onwards</span>
														@endif
													@elseif (!$data['hall']['priceMinRent'] && $data['venue']['priceMinRent'])
														@if ($data['venue']['worthRent'] && $data['venue']['worthRent'] > $data['venue']['priceMinRent'])
															<span class="mar-r-4"><del>@price($data['venue']['worthRent'])</del></span>
														@endif
														@price($data['venue']['priceMinRent'])
														@if ($data['venue']['priceMaxRent'] && $data['venue']['priceMaxRent'] > $data['venue']['priceMinRent'])
															- @price($data['venue']['priceMaxRent'])*
														@else
															* <span class="card-item__subtitle">onwards</span>
														@endif
													@endif
												</span>
													</div>
												</div>
											</div>
										@elseif($data['hall']['priceMinVeg'])
											<div class="in-blk card-item__sub"> <!-- serves food -->
												<span class="card-item__val">
													<img src="{{ $galleryUrl }}/img/icons/venue-facilities/green_veg.png" alt="veg price">
													@if ($data['hall']['worthVeg'] && $data['hall']['worthVeg'] > $data['hall']['priceMinVeg'])
														<span class="mar-r-4">
															<del>@price($data['hall']['worthVeg'])</del>
														</span>
													@endif
													@price($data['hall']['priceMinVeg'])*
												</span>
												<span class="card-item__subtitle">onwards</span>
											</div>
											@if ($data['hall']['priceMinNonveg'])
												<div class="in-blk card-item__sub">
													<span class="card-item__val">
														<img src="{{ $galleryUrl }}/img/icons/venue-facilities/red_nonveg.png" alt="non veg price">
														@if ($data['hall']['worthVeg'] && $data['hall']['worthNonVeg'] > $data['hall']['priceMinNonveg'])
															<span class="mar-r-4"><del>@price($data['hall']['worthNonVeg'])</del></span>
														@endif
														@price($data['hall']['priceMinNonveg'])*
													</span>
													<span class="card-item__subtitle">onwards</span>
												</div>
											@else
												<div class="in-blk card-item__sub">
													<span class="card-item__val">(Pure Vegetarian)</span>
												</div>
											@endif
										@elseif($data['venue']['priceMinVeg'])
											<div class="in-blk card-item__sub"> <!-- serves food -->
												<span class="card-item__val">
													<img src="{{ $galleryUrl }}/img/icons/venue-facilities/green_veg.png" alt="veg price">
													@if ($data['venue']['worthVeg'] && $data['venue']['worthVeg'] > $data['venue']['priceMinVeg'])
														<span class="mar-r-4">
															<del>@price($data['venue']['worthVeg'])</del>
														</span>
													@endif
													@price($data['venue']['priceMinVeg'])*
												</span>
												<span class="card-item__subtitle">onwards</span>
											</div>
											@if ($data['venue']['priceMinNonveg'])
												<div class="in-blk card-item__sub mar-t-10">
													<span class="card-item__val">
														<img src="{{ $galleryUrl }}/img/icons/venue-facilities/red_nonveg.png" alt="non veg price">
														@if ($data['venue']['worthVeg'] && $data['venue']['worthNonVeg'] > $data['venue']['priceMinNonveg'])
															<span class="mar-r-4"><del>@price($data['venue']['worthNonVeg'])</del></span>
														@endif
														@price($data['venue']['priceMinNonveg'])*
													</span>
													<span class="card-item__subtitle">onwards</span>
												</div>
											@else
												<div class="in-blk card-item__sub">
													<span class="card-item__val">(Pure Vegetarian)</span>
												</div>
											@endif
										@elseif($data['venue']['priceKid'])
											<div class="card-item">
												<div class="card-item__body">
													<div class="in-blk card-item__sub">
														<span class="card-item__val">
															@if ($data['venue']['worthKid'] && $data['venue']['worthKid'] > $data['venue']['priceKid'])
																<span class="mar-r-4"><del>@price($data['venue']['worthKid'])</del></span>
															@endif
															@price($data['venue']['priceKid'])*
														</span>
														<span class="card-item__title-hint"> (per Kid) </span>
													</div>
												</div>
											</div>
										@else
										@endif
									</div>
								</div>
								<div class="card-item"> <!--- Rating accross the web ---->
									<div class="card-item__body">
										<div class="in-blk card-item__sub">
									<span class="card-item__val font-18">
										<input type="number" class="avg-rating hide" value="{{ $data['extRatings']['avgRating'] }}" title="{{ $data['extRatings']['avgRating'] }} average rating"/>
									</span>
											<span class="card-item__title-hint"> (across web) </span>
										</div>
									</div>
								</div>
								<div class="card-item">
									<div class="card-item__body">
										<div class="in-blk card-item__sub  mar-r-15"> <!---- Alcohol served or not --->
											<span class="card-item__val @if (!$data['venue']['isAlcoholServed']) disabled @endif">
												<i class="material-icons">&#xE540;</i>
												<span class="valign-mid">
													@if ($data['venue']['isAlcoholServed'])
														Alcohol Served
													@else
														Alcohol Not Served
													@endif
												</span>
											</span>
										</div>
										<div class="in-blk card-item__sub mar-t-15"> <!---- Car Pakring or not --->
											<span class="card-item__val @if (!$data['venue']['carParkCount']) disabled @endif">
												<i class="material-icons">&#xE531;</i>
												<span class="valign-mid">
													@if ($data['venue']['carParkCount'])
														{{ $data['venue']['carParkCount'] }}
														<span class="card-item"> Car Parking</span>
													@else
														<span class="card-item"> No Car Parking</span>
													@endif
												</span>
											</span>
										</div>
									</div>
								</div>
								<div class="card-item">
								@if ($data['venue']['isVerified'] || $data['venue']['isPartner']) <!-- badges begin -->
									<ul class="no-mar no-pad ls-none badges-wrap">
										@if ($data['venue']['isVerified'])
											<li class="in-blk"><img
														src="{{ $galleryUrl }}/img/icons/verified.png"
														alt="verified venue"/></li>
										@endif
										@if ($data['venue']['isPartner'])
											<li class="in-blk"><img
														src="{{ $galleryUrl }}/img/icons/best_price.jpg"
														alt="price guaranteed"/></li>
										@endif
									</ul>
									@endif
								</div>
							</div>
							<div class="mdl-card__actions mdl-card--border text-center hide__400">
								<a class="btn btn-default btn-view-more btn-more-details scroll-item" data-scroll="venueDetails">
									View More Details
								</a>
							</div>
							<div class="hs-section social-section text-center">
								@include('app.social-share-plugin')
							</div>
						</div>

					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="col-md-12 menu-top-b bg-white sticky hidden-sm hidden-xs">
		<div class="col-lg-10 col-md-12 col-lg-offset-1 col-md-offset-0 tabs-scroll">
			<div class="head-scroll-fixed border-b-1 col-md-12">
				<div class="col-md-10">
					<h5 class="mar-t-10 mar-b-10 text-center__400 ov-text-no-wrap">
						[#{{ $data['hall']['code'] }}] {{ $data['hall']['type'] }} at {{ $data['venue']['area'] }}
						<span id="verified" class="text-success"><i
									class="material-icons">&#xE8E8;</i></span>
						<div class="mdl-tooltip mdl-tooltip--large" for="verified">Verified Provider</div>
					</h5>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="col-md-12">
				<ul class="menu-scroll col-md-9" id="top-menu">
					<li><a href="#facilities" data-scroll="inclusions"> Facilities </a></li>
					<li><a href="#food" data-scroll="inclusions"> Food </a></li>
					@if ($data['venue']['mapLat'] && $data['venue']['mapLong'])
						<li><a href="#venue-map" data-scroll="inclusions"> Map </a></li>
					@endif
					@if (count($data['extRatings']['ratings']))
						<li><a href="#rating" data-scroll="rating"> Ratings </a></li>
					@endif
				</ul>
				<div class="col-md-3 interested">
					<a class="btn btn-default btn-view-more btn-best-deal scroll-item mar-t-5" data-scroll="info-container">
						<i class="material-icons valign-mid">&#xE8DC;</i>
						<span class="valign-mid">Get Best Deal</span>
					</a>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="col-md-12 bg-light-gray pad-t-20" id="info-container">
		<div class="col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0">
			<div id="venueDetails" class="venue-details"> <!-- bottom begin -->
				<div class="col-md-8">
					<div class="main-card venue-details-card facilities-card" id="facilities"> <!-- facilities begin -->
						<div class="card-title">
							<h5 class="card-title-text">Facilities</h5>
						</div>
						<div class="card-supporting-text no-pad-t">
							<ul class="clearfix items-list">
								<li class="item">
									<i class="material-icons">&#xE7FB;</i>
									<span class="title">Capacity: </span>
									<span> {{ $data['hall']['capacityMin'] }} - {{ $data['hall']['capacityMax'] }}</span>
									@if ($data['hall']['capacityFloat'])
										(floating {{ $data['hall']['capacityFloat'] }})
									@endif
								</li>
								<li class="item @if (!$data['hall']['hasAc']) disabled @endif">
									<i class="material-icons">&#xE332;</i>
									<span>AC</span>
								</li>
								<li class="item @if (!$data['venue']['hasValetParking']) disabled @endif">
									<i class="material-icons">&#xE54F;</i>
									<span>Valet Parking</span>
								</li>
								<li class="item @if (!$data['venue']['carParkCount']) disabled @endif">
									<i class="material-icons">&#xE531;</i>
									@if ($data['venue']['carParkCount'])
										<span>Car Parking:</span>
										<span> {{ $data['venue']['carParkCount'] }}</span>
									@else
										<span> No Car Parking</span>
									@endif
								</li>
								<li class="item @if (!$data['venue']['hasLift']) disabled @endif">
									<i class="material-icons">&#xE89D;</i>
									<span>Lift</span>
								</li>
								<li class="item @if (!$data['venue']['hasSoundSystem']) disabled @endif">
									<i class="material-icons">&#xE049;</i>
									<span>Sound System</span>
									@if ($data['venue']['hasSoundSystem'] && $data['venue']['priceSoundSystem'])
										<span> (add @price($data['venue']['priceSoundSystem']))</span>
									@endif
								</li>
								<li class="item @if (!$data['venue']['hasMike']) disabled @endif">
									<i class="material-icons">&#xE029;</i>
									<span>Mike</span>
									@if ($data['venue']['hasMike'] && $data['venue']['priceMike'])
										<span> (add @price($data['venue']['priceMike']))</span>
									@endif
								</li>
								<li class="item @if (!$data['venue']['hasProjector']) disabled @endif">
									<i class="material-icons">&#xE04B;</i>
									<span>Projector</span>
									@if ($data['venue']['hasProjector'] && $data['venue']['priceProjector'])
										<span>(add @price($data['venue']['priceProjector']))</span>
									@endif
								</li>
								<li class="item @if (!$data['venue']['isOutsideDecorsAllowed']) disabled @endif">
									<i class="material-icons">&#xE545;</i>
									<span>Decorators Allowed</span>
									@if ($data['venue']['isOutsideDecorsAllowed'] && $data['venue']['priceOutsideDecorsAllowance'])
										<span>(add @price($data['venue']['priceOutsideDecorsAllowance']))</span>
									@endif
								</li>
							</ul>
						</div>
					</div>
					<div class="main-card venue-details-card" id="food"> <!-- menu begin -->
						<div class="card-title">
							<h5 class="card-title-text">Food &amp; Beverages</h5>
						</div>
						<div class="card-supporting-text no-pad-t">
							<ul class="clearfix items-list">
								<li class="item @if (!$data['venue']['isAlcoholServed']) disabled @endif">
									<i class="material-icons">&#xE540;</i>
									@if ($data['venue']['isAlcoholServed'])
										<span>Alcohol Served</span>
									@else
										<span>Alcohol Not Served</span>
									@endif
								</li>
								<li class="item @if ($data['venue']['taxPercent'] == 0.00) disabled @endif">
									<i class="material-icons">&#xE227;</i>
									<span>Tax Extra: </span>
									@if ($data['venue']['taxPercent']) <span>Yes</span>
									@else <span>No</span> @endif
								</li>
								@if($data['hall']['priceMinRent'])
									<li class="item">
										<i class="material-icons">&#xE870;</i>
										<span>Rent: </span>
										@if ($data['hall']['worthRent'] && $data['hall']['worthRent'] > $data['hall']['priceMinRent'])
											<span class="mar-r-4"><del>@price($data['hall']['worthRent'])</del></span>
										@endif
										<span>@price($data['hall']['priceMinRent'])</span>
										@if ($data['hall']['priceMaxRent'] && $data['hall']['priceMaxRent'] >  $data['hall']['priceMinRent'])
											- @price($data['hall']['priceMaxRent'])*
										@else
											* onwards
										@endif
										<span>({{ $data['hall']['minRentDuration'] }} Hrs)</span>
									</li>
								@elseif (!$data['hall']['priceMinRent'] &&  $data['venue']['priceMinRent'])
									<li class="item">
										<i class="material-icons">&#xE870;</i>
										<span>Rent: </span>
										@if ($data['venue']['worthRent'] && $data['venue']['worthRent'] > $data['venue']['priceMinRent'])
											<span class="mar-r-4"><del>@price($data['venue']['worthRent'])</del></span>
										@endif
										<span>@price($data['venue']['priceMinRent'])</span>
										@if ($data['venue']['priceMaxRent'] && $data['venue']['priceMaxRent'] >  $data['venue']['priceMinRent'])
											- @price($data['venue']['priceMaxRent'])*
										@else
											* onwards
										@endif
										<span>({{ $data['venue']['minRentDuration'] }} Hrs)</span>
									</li>
								@endif
								@if($data['venue']['priceMinVeg'])
									<li class="item">
										<i class="material-icons">&#xE870;</i>
										<span>Veg: </span>
										@if ($data['venue']['worthVeg'] && $data['venue']['worthVeg'] > $data['venue']['priceMinVeg'])
											<span class="mar-r-4"><del>@price($data['venue']['worthVeg'])</del></span>
										@endif
										<span>@price($data['venue']['priceMinVeg'])* onwards</span>
									</li>
									<li class="item @if ($data['venue']['isPureVeg']) disabled @endif">
										<i class="material-icons">&#xE870;</i>
										<span>Non-Veg: </span>
										@if (!$data['venue']['isPureVeg'])
											@if ($data['venue']['worthNonVeg'] && $data['venue']['priceMinNonveg'])
												<span class="mar-r-4"><del>@price($data['venue']['worthNonVeg'])</del></span>
											@endif
											<span>@price($data['venue']['priceMinNonveg'])* onwards</span>
										@else
											<span>N/A</span>
										@endif
									</li>
								@endif
								@if($data['venue']['priceKid'])
									<li class="item">
										<i class="material-icons">&#xEB41;</i>
										<span>Per Kid: </span>
										@if ($data['venue']['worthKid'] && $data['venue']['worthKid'] > $data['venue']['priceKid'])
											<span class="mar-r-4">
													<del>@price($data['venue']['worthKid'])</del>
												</span>
										@endif
										<span>@price($data['venue']['priceKid'])*</span>
									</li>
									@if($data['venue']['priceAdult'])
										<li class="item">
											<i class="material-icons">&#xE87C;</i>
											<span>Per Adult: </span>
											@if ($data['venue']['worthAdult'] && $data['venue']['worthAdult'] > $data['venue']['priceAdult'])
												<span class="mar-r-4"><del>@price($data['venue']['worthAdult'])</del></span>
											@endif
											<span>@price($data['venue']['priceAdult'])*</span>
										</li>
									@endif
								@endif
								<li class="item item-66 w-100__400">
									<i class="material-icons">&#xE561;</i>
									<span>Cuisines: </span>
									<span>{{ $data['venue']['cuisines'] }}</span>
								</li>
							</ul>
						</div>
						@if($data['menus'])
							<hr class="card-separator">
							<div class="card-title">
								<h5 class="card-title-text">Food Menu</h5>
							</div>
							<div class="card-supporting-text clearfix no-pad-t">
								<ul class="clearfix menus-list gallery-images">
									@foreach($data['menus'] as $menu)
										<li class="menu gallery-image">
											<a href="{{ $menu['url'] }}" rel="slider[img]" title="{{ $menu['name'] }} @if ($menu['minGuests']) (mininum {{ $menu['minGuests'] }} guests) @endif">
												<img src="{{ $menu['url'] }}" alt="{{ $menu['name'] }} @if ($menu['minGuests']) (mininum {{ $menu['minGuests'] }} guests) @endif">
											</a>
											<div class="title">{{ $menu['name'] }}</div>
											@if ($menu['minGuests'])
												<div class="sub-text">(min. {{ $menu['minGuests'] }} guests)</div>
											@endif
										</li>
									@endforeach
								</ul>
							</div>
						@endif
					</div>

					@if ($data['venue']['mapLat'] && $data['venue']['mapLong'])
						<div class="main-card venue-details-card no-padding" id="venue-map"> <!-- menu begin -->
							<div class="card-title pad-t-15 pad-l-20">
								<h5 class="card-title-text">On Map</h5>
							</div>

							<div id="map-canvas"></div>
						</div>
					@endif

					@if (count($data['extRatings']['ratings']))
						<div class="main-card venue-details-card ratings-card" id="rating"> <!-- ext ratings begin -->
							<div class="card-title">
								<h5 class="card-title-text">Ratings across web</h5>
							</div>
							<div class="card-supporting-text no-pad-t w-100__400">
								<table class="table table-striped table-bordered table-ratings no-mar-b">
									<thead>
									<tr class="text-center">
										<th class="first"></th>
										<th class="text-center">Rating</th>
										<th class="text-center">Mentions</th>
									</tr>
									</thead>
									<tbody class="text-bold">
									@foreach ($data['extRatings']['ratings'] as $rating)
										<tr>
											<td>
												<a class="no-pad__400" href="{{ $rating['type']['url'] }}" target="_blank" rel="noopener">
													<img class="w-100__400" src="{{ $galleryUrl }}/img/logo/{{ $rating['type']['image_url'] }}" alt="ratings on {{ $rating['type']['name'] }}"/>
												</a>
											</td>
											<td class="value">
												<span>{{ $rating['rating_value']}} / {{ $rating['rating_max'] }}</span>
											</td>
											<td class="value">
												<span>{{ $rating['rating_count'] }}</span>
											</td>
										</tr>
									@endforeach
									</tbody>
								</table>
								<div class="text-muted mar-t-15">
									<u>Important:</u>
									<span> These ratings were last updated <b>1 day ago</b>. All the ratings are mentioned for the sole purpose of information only.</span>
								</div>
							</div>
						</div>
					@endif
				</div>
				<div class="col-md-4">
					@include("app.forms.profile_enquiry_form", [
						"enquiryUrl" => route("ticket.venue", [$data['occasionId'], $data['hall']['id']]),
						"hidePartyLocation" => true
					])
					@include('app.process.venue')
					@include('app.process.report-issue')
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="col-md-12 bg-white pad-t-20 similar-results">
		<div class="col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0">
			@if (count($data['similarHalls']))
				<div class="row"> <!-- similar listing begin -->
					<div class="col-md-12">
						<div class="similar-venues-wrap text-center__400 mar-tb-20__400">
							<h5 class="text-center pad-l-15">Similar Venues</h5>
							<div class="similar-venues-list row">
								@foreach ($data['similarHalls'] as $similarHall)
									<div class="col-sm-12 col-md-6 col-lg-4 no-pad-l">
										<div class="item-result venue-result">
											<div class="img-wrap">
												<a href="{{ $similarHall->getLink($data['occasionUrl']) }}">
													<img src="{{ $similarHall->getProfileImg() }}" alt="{{ $similarHall->name }}"/>
												</a>
												@include('app.shortlist_results', [
															"mapId" => $similarHall->id,
															"mapTypeId" => $data['mapTypeId'],
															"occasionId" => $data['occasionId'],
															"cityId" => $data['cityId']
														])
											</div>
											<div class="details">
												<div class="item-info pad-r-5">
													<div class="title">
														<a href="{{ $similarHall->getLink($data['occasionUrl']) }}">
															@if ($similarHall->type_id == 1)
																Party space
															@else
																{{ $similarHall->type->name }}
															@endif
															<span> at {{ $similarHall->venue->area->name }}</span>
														</a>
													</div>
													<div class="col-xs-12 col-sm-12 col-md-8 no-pad pad-l-10">
														<div class="rating-wrap pull-left hide">
															<?php $extRatings = $similarHall->venue->prepareExtRatings(); ?>
															<input type="number" class="avg-rating hide" value="{{ $extRatings['avgRating'] }}" title="{{ $extRatings['avgRating'] }} average rating"/>
														</div>
														<ul class="fact-list pull-left icons-list no-mar no-pad ls-none">
															<li class="in-blk mar-r-10">
																<i class="material-icons valign-mid">&#xE7FB;</i>
																<span class="valign-mid">{{ $similarHall->cap_min }} - {{ $similarHall->cap_float }}</span>
															</li>
														</ul>
													</div>
													<div class="col-md-4 hidden-xs hidden-sm no-padding">
														<a href="{{ $similarHall->getLink($data['occasionUrl']) }}"
																class="btn btn-primary btn-view-more btn-venue-details">View Details</a>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="item-info text-left">
													<div class="price-tag mar-t-5 col-md-12 pad-r-0">
														@if($similarHall->rent_min)
															<span class="price">@price($similarHall->rent_min) </span> *
															<span class="low-font">rent onwards</span>
															@if($similarHall->rent_worth)
																<span class="price-worth"> <del>@price($similarHall->rent_worth)</del> </span>
															@endif
														@elseif(!$similarHall->rent_min && $similarHall->venue->price_min_rent)
															<span class="price"> @price($similarHall->venue->price_min_rent) </span>*
															<span class="low-font">rent onwards</span>
															@if($similarHall->venue->worth_rent)
																<span class="price-worth"> <del>@price($similarHall->venue->worth_rent)</del> </span>
															@endif
														@elseif($similarHall->venue->min_veg_worth)
															<span class="price"> @price($similarHall->venue->price_min_veg) </span>*
															<span class="low-font">onwards</span>
															<span class="price-worth"> <del>@price($similarHall->venue->min_veg_worth)</del> </span>

														@elseif($similarHall->venue->price_kid)
															<span class="price"> @price($similarHall->venue->price_kid) </span>*
															<span class="low-font">onwards</span>
															@if($similarHall->venue->worth_kid)
																<span class="price-worth"> <del>@price($similarHall->venue->worth_kid)</del> </span>
															@endif
														@endif
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
									</div>
								@endforeach
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			@endif
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			/*  */
			$('.loading-img-wrap').remove();
			$('.venue-gallery-wrap').removeClass('hide');

			$('.avg-rating').rating({
				showClear: 0,
				readonly: 'true',
				showCaption: 0,
				size: 'xl',
				min: 0,
				max: 5,
				step: 0.1,
			});

			$('.avg-rating').removeClass('hide');

			/* show Google map */
			function initialize() {

				var myLatlng = new google.maps.LatLng(12.9944049, 77.6787036);
				var mapOptions = {
					zoom: 16,
					center: myLatlng,
					clickable: false,
					scrollwheel: false,
					mapTypeControl: false,
					mapMaker: false
				};

				var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

				var marker = new google.maps.Marker({
					position: myLatlng,
					map: map
				});
			}

			google.maps.event.addDomListener(window, 'load', initialize);

		});
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
@endsection