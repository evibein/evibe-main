@extends('layout.base-pre-post')

@section('page-title')
	<title>Best Venues in {{ $cityName }} - Quick Link | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="List of all best venues for birthday parties in {{ $cityName }} based on venue type, hall type, pricing and location.">
@endsection

@section('meta-keywords')
	<meta name="keywords" content="List of venues, {{ $cityName }}, party hall list, pricing details, pictures">
@endsection

@section('og-title')
	<meta property="og:title" content="Best Venues in {{ $cityName }} - Quick Link | Evibe.in"/>
@endsection

@section('og-description')
	<meta property="og:description" content="List of all best venues for birthday parties in {{ $cityName }} based on venue type, hall type, pricing and location."/>
@endsection

@section("header")
	@include('occasion.pre-post.header.header')
@endsection

@section("content")
	<?php
	$occasionUrl = config('evibe.occasion.reception.url');
	$venueBase = "/$cityUrl/$occasionUrl/" . config('evibe.results_url.venues');
	?>
	<div class="items-results venue-quick-links">
		<div class="col-sm-12">
			<div class="top-panel">
				<div class="col-sm-12">
					<h4 class="display-1 text-cap">
						Quick Links for Venues in {{ $cityName }}
					</h4>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="bottom-panel">
				<div class="col-sm-12">
					<div class="col-sm-3">
						<div class="links-title">By Venue Type</div>
						<ul class="links-list">
							<li class="a-link">
								<a href="{{ $venueBase }}?type=5-star-hotels&ref=quick-links">5 Star Hotels in {{ $cityName }}</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?type=4-star-hotels&ref=quick-links">4 Star Hotels in {{ $cityName }}</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?type=3-star-hotels&ref=quick-links">3 Star Hotels in {{ $cityName }}</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?type=party-halls&ref=quick-links">Party Halls in {{ $cityName }}</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?type=resorts&ref=quick-links">Resorts in {{ $cityName }}</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?type=restaurants&ref=quick-links">Restaurants in {{ $cityName }}</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?type=general-hotels&ref=quick-links">Hotels in {{ $cityName }}</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?type=boutique-hotels&ref=quick-links">Boutique Hotels in {{ $cityName }}</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?type=play-zones&ref=quick-links">Play Zones in {{ $cityName }}</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?type=convention-halls&ref=quick-links">Convention Halls in {{ $cityName }}</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?type=wedding-halls&ref=quick-links">Wedding Halls in {{ $cityName }}</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?type=open-lawns&ref=quick-links">Open Lawns in {{ $cityName }}</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?type=pubs&ref=quick-links">Pubs in {{ $cityName }}</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?type=clubs&ref=quick-links">Clubs in {{ $cityName }}</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?type=auditoriums&ref=quick-links">Auditoriums in {{ $cityName }}</a>
							</li>
						</ul>
					</div>
					<div class="col-sm-3">
						<div class="links-title">By Hall Type</div>
						<ul class="links-list">
							<li class="a-link">
								<a href="{{ $venueBase }}?category=banquet-halls&ref=quick-links">Banquet Halls in {{ $cityName }}</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?category=party-spaces&ref=quick-links">Party Spaces in {{ $cityName }}</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?category=restaurants&ref=quick-links">Restaurant Spaces in {{ $cityName }}</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?category=open-air&ref=quick-links">Open Air Halls in {{ $cityName }}</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?category=semi-open-air&ref=quick-links">Semi-Open Air Spaces in {{ $cityName }}</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?category=open-air-pool-side&ref=quick-links">Pool Side Spaces in {{ $cityName }}</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?category=terrace-halls&ref=quick-links">Terrace Halls in {{ $cityName }}</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?category=terrace-pool-side&ref=quick-links">Terrace Pool Side in {{ $cityName }}</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?category=board-rooms&ref=quick-links">Board Rooms in {{ $cityName }}</a>
							</li>
						</ul>
					</div>
					<div class="col-sm-3">
						<div class="links-title">By Location</div>
						<ul class="links-list">
							<li class="a-link">
								<a href="{{ $venueBase }}?location=Indiranagar&ref=footer">Best Venues in Indira Nagar</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?location=Koramangala&ref=footer">Best Venues in Koramangala</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?location=Whitefield&ref=footer">Best Venues in Whitefield</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?location=Jayanagar&ref=footer">Best Venues in Jayanagar</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?location=J.P%20Nagar&ref=footer">Best Venues in J. P. Nagar</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?location=Marathahalli%20Outer%20Ring%20Road&ref=footer">Best Venues in Marathahalli</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?location=Old%20Airport%20Road&ref=footer">Best Venues on Old Airport Road</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?location=Domlur&ref=footer">Best Venues in Domlur</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?location=Hebbal&ref=footer">Best Venues in Hebbal</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?location=BTM%20Layout&ref=footer">Best Venues in BTM Layout</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?location=Electronic%20City&ref=footer">Best Venues in Electronic City</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?location=Yeswantpur&ref=footer">Best Venues in Yeswantpur</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?location=Yelhanka&ref=footer">Best Venues in Yelhanka</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?location=Kammanahalli&ref=footer">Best Venues in Kammanahalli</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?location=Sarjapur%20Road&ref=footer">Best Venues in Sarjapur Road</a>
							</li>
						</ul>
					</div>
					<div class="col-sm-3">
						<div class="links-title">By Pricing</div>
						<ul class="links-list">
							<li class="a-link">
								<a href="{{ $venueBase }}?price_min=0&price_max=300&ref=footer">
									Upto <span class='rupee-font'>&#8377; </span> 300 (per person)
								</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?price_min=300&price_max=500&ref=footer">
									From <span class='rupee-font'>&#8377; </span> 300 to <span
											class='rupee-font'>&#8377; </span> 500
								</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?price_min=500&price_max=750&ref=footer">
									From <span class='rupee-font'>&#8377; </span> 500 to <span
											class='rupee-font'>&#8377; </span> 750
								</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?price_min=750&price_max=1000&ref=footer">
									From <span class='rupee-font'>&#8377; </span> 750 to <span
											class='rupee-font'>&#8377; </span> 1,000
								</a>
							</li>
							<li class="a-link">
								<a href="{{ $venueBase }}?price_min=1000&price_max=5000&ref=footer">
									Above <span class='rupee-font'>&#8377; </span> 1,000
								</a>
							</li>
						</ul>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection