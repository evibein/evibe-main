@extends("layout.occasion.pre-post.base-pre-post-list")

@section("content")
	<div class="items-results e-options-wrap">
		<a href="#" class="scrollup">Scroll</a>
		<div class="col-sm-12">
			<div class="top-panel">
				<div class="col-lg-12  col-sm-12 col-sm-offset-0">
					<h4 class="header-title display-1 text-cap no-mar__400 no_pad__400 pad-tb-20__400 text-center__400" title="{{ $data['seo']['pageHeader'] }}">{{ $data['seo']['pageHeader'] }}</h4>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="bottom-panel">
				<div class="col-xs-12 col-sm-5 col-md-3 col-lg-3 bottom-left-panel">
					@include('occasion.util.list-filters.cake-filters', ['filterRoute'=>route("city.occasion.pre-post.cakes.list", $cityUrl)])
				</div>
				<div class="col-xs-12 col-sm-7 col-md-9 col-lg-9 bottom-right-panel">
					@include('occasion.util.list-filters.sort-filter')
					<div class="items-list cakes-results-wrap mar-b-40">
						@if (count($data['cakes']) > 0)
							<div class="cakes-list">
								@foreach ($data['cakes'] as $cake)
									@include('app.custom_design_card', [
										"currentPage" => $data['cakes']->currentPage(),
										"loopIteration" => $loop->iteration
									])
									<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 no-pad-l no-pad__400-600">
										<div class="item-result cake-pkg">
											<div class="img-wrap">
												<a href="{{ $cake['fullUrl'] }}" title="{{ $cake['name'] }}">
													<img src="{{ $cake['profileImg'] }}" alt="{{ $cake['name'] }}" title="{{ $cake['name'] }}">
												</a>
												@include('app.shortlist_results', [
															"mapId" => $cake['id'],
															"mapTypeId" => $data['mapTypeId'],
															"occasionId" => $data['occasionId'],
															"cityId" => $data['cityId']
														])
											</div>
											<div class="details">
												<div class="title">
													<a href="{{ $cake['fullUrl'] }}" title="{{ $cake['name'] }}">
														{{ $cake['name'] }}
													</a>
												</div>
												<div class="action-btns">
													<div class="col-md-6 no-padding hidden-xs hidden-sm border-right">
														@include('app.shortlist_results_link', [
														"mapId" => $cake['id'],
														"mapTypeId" => $data['mapTypeId'],
														"occasionId" => $data['occasionId'],
														"cityId" => $data['cityId']
														])
													</div>
													<div class="col-md-6 no-padding hidden-xs hidden-sm">
														<a href="{{ route('city.occasion.pre-post.cakes.profile', [$cityUrl, $cake['url']]) }}?ref=book-now#bookNowModal" class="item-book-now" target="_blank" rel="noopener">Book Now</a>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="content-details">
													<div>
														<div class="col-md-8 col-sm-12 col-xs-12 no-pad">
															<div class="price-tag text-left">
																<span class="price"> @price( $cake['price_per_kg']) </span>
																<span> / KG </span>
															</div>
														</div>
														<div class="col-md-4 hidden-xs hidden-sm no-pad">
															<div class="rating-wrap">
																<a href="{{ route('city.occasion.birthdays.cakes.profile', [$cityUrl, $cake['url']]) }}?ref=book-now#bookNowModal" class="btn-view-more" target="_blank" rel="noopener">Book Now</a>
															</div>
														</div>
														<div class="clearfix"></div>
													</div>
													<div class="additional-info font-italic">
														Minimum Order: {{ $cake['min_order'] }} KG
													</div>
												</div>
											</div>
										</div>
									</div>
								@endforeach
								<div class="clearfix"></div>
							</div>
							<div class="pages text-center"> <!-- pagination begin -->
								<div>{{ $data['cakes']->appends($data['filters']['queryParams'])->links() }}</div>
							</div>
						@elseif(request('category') || request('search') || request('price_min') || request('price_max'))
							<div class="no-results-wrap text-center">
								<h4 class="no-results-title no-mar">
									<i class="glyphicon glyphicon-warning-sign"></i>
									Oops, no cakes were found matching your filters.
								</h4>

								<div class="pad-t-20">
									<a href="{{ route("city.occasion.pre-post.cakes.list", $cityUrl) }}?ref=no-results" class="btn btn-danger btn-lg">
										<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
									</a>
								</div>
							</div>
						@else
							<div class="no-results-wrap text-center">
								<h4 class="no-results-title">{{ config('evibe.cake.no_results')}}</h4>
							</div>
						@endif
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
@endsection