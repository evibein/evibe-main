@extends("layout.occasion.pre-post.base-pre-post-profile")

@section("content")
	<div class="cake-profile-wrap">
		<div class="col-md-12 bg-white">
			<a href="#" class="scrollup">Scroll</a>
			<div class="col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0"> <!-- Decors profile begin -->
				<div class="col-md-12 no-pad mar-b-15">
					<div class="breadcrumb-section">
						<ol class="breadcrumb text-center__400">
							<li><a href="{{ route('city.home', $cityUrl) }}">{{$cityName}}</a></li>
							<li>
								<a href="{{ route('city.occasion.pre-post.home', $cityUrl) }}">Engagement | Wedding Reception</a>
							</li>
							<li><a href="{{ route('city.occasion.pre-post.cakes.list', $cityUrl) }}">Cakes</a></li>
							<li class="active">{{ $data['name'] }}</li>
						</ol>
					</div>
					<div class="header-wrap"> <!-- header begin -->
						<div class="col-md-12 no-pad">
							<div class="col-md-8"> <!-- title begin -->
								<h4 class="no-mar mar-t-10 text-center__400">
									[#{{ $data['code'] }}] {{ $data['name'] }}
								</h4>
							</div>

							<div class="col-md-4 text-center mar-t-10__400"> <!-- actions begin --->
								@include('app.shortlist_detail')
								<a class="btn btn-default btn-view-more btn-best-deal btn-book-now">
									<i class="material-icons valign-mid">&#xE8CC;</i>
									<span class="valign-mid">Book Now</span>
								</a>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<div class="info-panel">
					<div class="row">
						<div class="col-md-8 mar-t-10 pad-b-15">
							@if (count($data['gallery']['images']))
								<div class="loading-img-wrap">
									<img src="/images/loading.gif" alt="">
									<div class="pad-t-10">loading images...</div>
								</div>
								<div class="gallery-wrap decor-gallery-wrap mar-b-10 hide">
									<div class="fotorama" data-width="100%" data-ratio="900/600" data-maxheight="400" data-nav="thumbs" data-autoplay="true" data-thumbwidth="80" data-thumbmargin="10" data-keyboard="true">
										@foreach ($data['gallery']['images'] as $image)
											<a href="{{ $image['url'] }}">
												<img src="{{ $image['thumb'] }}" alt="{{ $image['title'] }}"/>
											</a>
										@endforeach
									</div>
								</div>
								<div>
									<i class="low-font gallery-bt-text">(These picture(s) are only for your reference. Click on an image to enlarge.)</i>
								</div>
							@else
								<div>No images found</div>
							@endif
							<div class="text-center">
								@include('app.social-share-plugin')
							</div>
						</div>
						<div class="col-md-4 mar-t-10">
							<!-- highlights begin -->
							<div class="item-profile cake-profile highlights-card">
								<div class="hs-section">
									<div class="text-center mar-b-20 border-b-1">
										<img src="{{ $galleryUrl }}/img/app/decor_protection_260x66.jpg" alt="Evibe delivery guarantee"/>
									</div>
									<div class="card-item price-item cake-price-cnt text-center">
										<div class="price-content hide">
											<div class="price-worth price-worth-cake in-blk">
												<span class='rupee-font'>&#8377;</span>
												<span class="displayWorth">{{ $data['price_worth'] }}</span>
											</div>
											<div class="price-val in-blk">
												<span class='rupee-font'>&#8377;</span>
												<span class="displayPrice">{{ $data['base_price'] }}</span>
											</div>
										</div>
										<div class="loading-cnt">
											<img src="{{ $galleryUrl }}/img/icons/fb_loading.png" alt="loading..." class="rotate">
										</div>
									</div>
									@if($data['flavour'])
										<div class="card-item text-center">
											<div class="card-item__title">
												<i class="glyphicon glyphicon-ice-lolly"></i> Best Flavour:
											</div>
											<div class="card-item__body"> {{ $data['flavour'] }}</div>
										</div>
									@endif
									@include('base.cake.price')
									<div class="card-item card-actions text-center hide__400">
										<a class="btn btn-default btn-view-more btn-more-details scroll-item" data-scroll="book">
											View Details
										</a>
									</div>
								</div>
							</div>
							<!-- highlights end -->
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12 bg-light-gray pad-t-20">
			<div class="col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0">
				<div id="moreDetails" class="info-more-panel">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-8">
								@if ($data['info'])
									<div class="main-card info-card"> <!-- inclusions begin -->
										<div class="card-title">
											<h5 class="card-title-text">Description</h5>
										</div>
										<div class="card-info-text no-pad-t">
											{!! $data['info'] !!}
											<div class="mar-t-10">
												@if (count($data['tags']))
													<ul class="ls-none in-blk tags-lists">
														@foreach ($data['tags'] as $tag)
															<li class="in-blk tag ">
																<i class="in-blk glyphicon glyphicon-tags"></i>
																{{ $tag['tag_name'] }}
															</li>
														@endforeach
													</ul>
												@endif
											</div>
										</div>
									</div>
								@endif

								<div id="pricing" class="main-card info-card"> <!-- delivery -->
									<div class="head">
										<div class="card-title">
											<h5 class="card-title-text">Delivery Charges</h5>
										</div>
									</div>
									<div class="body">
										<table class="table table-condensed table-bordered table-striped table-45 w-100__400">
											<thead>
											<tr>
												<th>Slot (any day)</th>
												<th>Price</th>
											</tr>
											</thead>
											<tbody>
											<tr>
												<td>7:00 AM - 10:00 AM</td>
												<td><span class='rupee-font'>&#8377;</span> 150</td>
											</tr>
											<tr>
												<td>10:00 AM - 11:00 PM</td>
												<td>Free</td>
											</tr>
											<tr>
												<td>11:00 PM - 11:50 PM</td>
												<td><span class='rupee-font'>&#8377;</span> 150</td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
								@include("app.review.partner", [
									"reviews"=> $data["partnerReview"],
									"showAllReviewsUrl" => route("provider-reviews:all", [
										getSlugFromText($data["name"]),
										config("evibe.ticket.type.planner"),
										$data["provider"]["id"]
									])
								])
							</div>
							<div class="col-md-4">
								<div class="main-card provider-rating-card">
									<div class="card-info-text">
										<div class="font-16">Provider Code:
											<b>{{ $data['provider']['code'] }}</b>
										</div>
										<div class="font-18 pad-t-10">
											<span>
												<input type="number" class="avg-rating hide" value="{{ $data['partnerReview']['total']['avg'] }}" title="{{ $data['partnerReview']['total']['avg'] }} average rating for provider of {{ $data['name'] }}"/>
											</span>
											@if($data['partnerReview']['total']['count'])
												<span id="reviews" class="card-item__title-hint scroll-item cur-point" data-scroll="reviews"> ({{ $data['partnerReview']['total']['count'] }} reviews)</span>
											@else
												<span class="card-item__title-hint cur-point"> (0 reviews) </span>
											@endif
										</div>
									</div>
								</div>
								<div class="main-card">
									@include('app.bidding.custom_ticket',["url" => route('ticket.custom-ticket.cake', $data['occasionId'])])
								</div>
								@include("app.forms.profile_enquiry_form", [
									"enquiryUrl" => route("ticket.cake", [$data['occasionId'], $data['id']]),
									"locationTitle" => "Delivery Location"
								])
								<div class="mar-t-15">
									@include('app.process.report-issue')
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section("modals")
	@parent
	@include('auto-booking/checkout-modals/cake', [
		"data" => $data,
		"mapId" => $data['id'],
		"mapTypeId" => config('evibe.ticket.type.cake'),
		"occasionId" => $data['occasionId']
	])
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			var cakeJs = "<?php echo elixir('js/cakes/cake_profile.js'); ?>";
			$.getScript(cakeJs);
		});
	</script>
@endsection