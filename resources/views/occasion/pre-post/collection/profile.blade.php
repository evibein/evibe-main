@extends("layout.occasion.pre-post.base-pre-post-list")

@section("header")
	@include('occasion.pre-post.header.header')
@endsection

@section("content")
	<div class="collection-profile">
		<div class="col-sm-12">
			<div class="top-panel mar-b-30">
				<div class="breadcrumb-section text-center__400">
					<ol class="breadcrumb text-center__400">
						<li><a href="{{ route('city.home', $cityUrl) }}">{{$cityName}}</a></li>
						<li><a href="{{ route('city.collection.list', $cityUrl) }}">Collections</a></li>
					</ol>
				</div>
				<div class="collection-cover-content-wrap-2">
					<div class="col-sm-2 no-pad-r valign-mid hide__400">
						<div class="collection-profile-img-wrap">
							<div class="collection-profile-img">
								@if($data['collection']['profile_image'] != null)
									<img src="{{ $data['collection']->getProfilePageProfileImg() }}">
								@endif
							</div>
						</div>
					</div>
					<div class="col-sm-10 no-pad-l pad-l-15__400 valign-mid">
						<div class="collection-title-wrap">
							@if ( $data['collection']->name)
								<div class="collection-title">
									{{  $data['collection']->name }}
								</div>
							@endif
							<div class="collection-stats hide">
								@if(($data['totalOptionsCount']) > 0)
									@if(($data['totalOptionsCount']) > 1)
										{{ ($data['totalOptionsCount']) }} options
									@else
										{{ ($data['totalOptionsCount']) }} option
									@endif
								@endif
							</div>
							@if ($data['collection']->description)
								<div class="collection-cover-desc-wrap">
									{{$data['collection']->description}}
								</div>
							@endif
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="bottom-panel">
				@if(count($data['uniqueMapTypes']) > 1)
					<div class="col-xs-12 col-sm-5 col-md-3 col-lg-3 bottom-left-panel">
						<!-- filters begin -->
						<div class="item-filters no-pad-b__400 hide__400">
							<div class="title">Filter Results</div>
							<ul class="filters-list">
								@include('occasion.birthday.collection.profile-category-filter')
								@if ($data['clearFilter'])
									<li class="text-center reset-filters reset-filters__400 pad-t-20">
										<a href="?ref=reset-filters" class="font-16">
											<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
										</a>
									</li>
								@endif
							</ul>
						</div>
						<div class="collection-list-wrap">
							@include('app.evibe_guarantee')
						</div>
						<!-- filters end -->
					</div>
				@endif
				<div class="col-xs-12 @if((count($data['collectionOptions']) == 0 && !(request('category'))) || count($data['uniqueMapTypes']) < 2) col-lg-12 col-md-12 col-sm-12 mar-l-10 @else col-sm-7 col-md-9 col-lg-9 bottom-right-panel @endif">

					@if(count($data['collectionOptions']) > 0)
						@foreach($data['collectionOptions'] as $card)
							<div class="col-xs-12 @if((count($data['collectionOptions']) == 0 && !(request('category'))) || count($data['uniqueMapTypes']) < 2) col-sm-6 col-md-4 col-lg-3 @else col-sm-12 col-md-6 col-lg-4 @endif no-pad-l no-pad__400-600">
								<div class="item-result collection-item-result">
									<div class="img-wrap collection-option-tag">
										<div class="ribbon-wrap left-edge color">
											<span>{{ $card['displayName'] }}</span>
										</div>
										<a href="{{ $card['fullUrl'] }}?ref=cs_img" title="{{ $card['title'] }}">
											<img src="{{ $card['profileImg'] }}" alt="{{ $card['title'] }}" title="{{ $card['title'] }}" class="card-img-src">
										</a>
										@include('app.shortlist_results', [
													"mapId" => $card['id'],
													"mapTypeId" => $card['mapTypeId'],
													"occasionId" => $card['occasionId'],
													"cityId" => $data['cityId']
												])

									</div>
									<div class="details">
										<div class="card-detail-title title">
											<a href="{{ $card['fullUrl'] }}?ref=cs_title" title="{{ $card['title'] }}">
												{{ $card['title'] }}
											</a>
										</div>
										<div class="action-btns">
											@if($card['occasionId'] == config("evibe.occasion.pre-post.id"))
												<div class="no-padding hidden-xs hidden-sm hide">
													<a href="{{ $card['fullUrl'] }}?ref=cs_detail_link#bookSiteVisitModal" title="{{ $card['title'] }}" target="_blank" class="item-view-details">Book Free Site Visit</a>
												</div>
												<div class="clearfix"></div>
											@else
												<div class="col-md-6 no-padding hidden-xs hidden-sm border-right">
													@include('app.shortlist_results_link', [
													"mapId" => $card['id'],
													"mapTypeId" => $card['mapTypeId'],
													"occasionId" => $card['occasionId'],
													"cityId" => $card['cityId']
													])
												</div>
												<div class="col-md-6 no-padding hidden-xs hidden-sm">
													<a href="{{ $card['fullUrl'] }}?ref=cs_detail_link#bookNowModal" title="{{ $card['title'] }}" target="_blank" class="item-view-details">Book Now</a>
												</div>
												<div class="clearfix"></div>
											@endif
										</div>
										<div class="content-details">
											<div>
												<div class="col-md-6 col-sm-12 col-xs-12 no-pad">
													<div class="price-tag text-left">
														@if ($card['price'] != 0)
															<span class="price">
															@price($card['price'])
														</span>

															@if ($card['priceMax'] && $card['priceMax'] > $card['price'])
																- <span class="price">@price($card['priceMax'])</span>
															@endif
															@if ($card['worth'])
																<span class="price-worth">@price($card['worth'])</span>
															@endif
														@else
															<span>----</span>
														@endif
													</div>
												</div>
												<div class="col-md-6 hidden-xs hidden-sm no-pad">
													<div class="rating-wrap">
														@if($card['occasionId'] == config("evibe.occasion.pre-post.id"))
															<a href="{{ $card['fullUrl'] }}?#bookSiteVisitModal" title="{{ $card['title'] }}" target="_blank" class="item-view-details hide">Book Free Site Visit</a>
														@else
															<a href="{{ $card['fullUrl'] }}?#bookNowModal" title="{{ $card['title'] }}" target="_blank" class="btn-view-more">Book Now</a>
														@endif
													</div>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="additional-info font-italic">
												@if ($card['additionalLine'])
													<span>{{ $card['additionalLine'] }}</span>
												@endif
											</div>
										</div>
									</div>
								</div>
							</div>
						@endforeach
					@else
						<div class="no-results-wrap text-center">
							<h4 class="text-col-gr no-mar">We are adding amazing party options to collections soon, keep checking this page.</h4>
						</div>
					@endif
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection
@section("footer")
	@include('base.home.why-us')
	@if (isset($data['views']['footer']) && view()->exists($data['views']['footer']))
		@include($data['views']['footer'])
	@endif
@endsection