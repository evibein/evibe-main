@extends('layout.base-pre-post')

@section("header")
	@include('base.home.header.header-home-city')
@endsection

@section("content")
	<div class="home-screen">

		<a href="#" class="scrollup">Scroll</a>

		@if (isset($data['views']['home']) && view()->exists($data['views']['home']))
			@include($data['views']['home'])
		@endif

		@include('base.home.why-us')

		<div class="occasion-customer-reviews"></div>

		@if (isset($data['views']['press']) && view()->exists($data['views']['press']))
			@include($data['views']['press'])
		@endif

		@include('base.home.vendor-comment')
	</div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			function change() {
				$("#changeText").html(text[counter]).fadeIn(100);
				counter++;
				if (counter >= text.length) {
					counter = 0;
				}
			}

			$(".rslides").responsiveSlides({
				speed: 500,
				timeout: 5000
			});

			var text = ["Engagement", "Reception"];
			var counter = 0;
			setInterval(change, 2000);
		});
	</script>
@endsection