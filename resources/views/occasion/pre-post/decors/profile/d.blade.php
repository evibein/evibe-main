@extends("layout.product.decor.profile.d")

@section("header")
	@include('occasion.pre-post.header.header')
@endsection

@section('push-notifications')
	@include('plugins.onesignal', ['pushOccasion' => 'pre-post'])
@endsection

@section('content')
	<div class="col-md-12 col-lg-12 bg-white desktop-pro-page">
		<div class="des-top-section">
			<div class="col-md-12 col-lg-12 no-pad">
				@include("occasion.util.profile.d_components.decor.breadcrumb-section")
			</div>
			<div class="col-md-12 col-lg-12 no-pad">
				<div class="col-md-4 col-lg-4 no-pad-l">
					@include("occasion.util.profile.d_components.gallery")
					<div class="text-center">
						@include('app.social-share-plugin')
					</div>
				</div>
				<div class="col-md-4 col-lg-4 no-pad-l no-pad-r">
					@include("occasion.util.profile.d_components.decor.title-section")
				</div>
				<div class="col-md-4 col-lg-4 no-pad-r">
					@include("occasion.util.profile.d_components.product-variations")
					@include("occasion.util.profile.d_components.decor.price-section")
					<div class="mar-t-20">
						@include("occasion.util.profile.d_components.enquiry-card-section")
					</div>
					<div class="pad-t-5">
						@include('app.modals.auto-popup',
						[
						"cityId" => $data['shortlistData']['cityId'],
						"occasionId" => $data['shortlistData']['occasionId'],
						"mapTypeId" => $data['shortlistData']['mapTypeId'],
						"mapId" => $data['shortlistData']['mapId'],
						"optionName" => $data['decor']['name'],
						"countries" => (isset($data['countries']) && count($data['countries'])) ? $data['countries'] : []
						])
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="col-md-12 col-lg-12 no-pad hide">
				<hr class="desktop-profile-hr">
				@include("occasion.util.profile.d_components.how-it-works-section-pre-post")
			</div>
			<div class="clearfix"></div>
		</div>
		<hr class="desktop-profile-hr">
		<div class="des-down-section">
			<div class="col-md-12 col-lg-12 no-pad">
				<div class="col-md-8 col-lg-8">
					@include("occasion.util.profile.d_components.decor.product-detailed-info")
				</div>
				<div class="col-md-4 col-lg-4 no-pad">
					<div class="right-panel-cards">
						<div class="right-panel-wrap pad-l-15 pad-r-15">
							@include("occasion.util.profile.d_components.how-it-works-section")
							<hr class="desktop-profile-sub-hr">
							<div class="hide">
								@include("occasion.util.profile.d_components.decor.partner-card-section")
								<div class="mar-t-5"></div>
								<hr class="desktop-profile-sub-hr">
							</div>
							@include('base.home.why-us-vertical')
							<hr class="desktop-profile-sub-hr">
							@include('app.process.report-issue')
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="col-md-12 col-lg-12 no-pad">
				@include("occasion.util.profile.d_components.loading-similar-products")
				<div id="showSimilarProducts"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		@include("occasion.util.profile.d_components.decor.hidden-data-section")
	</div>
@endsection

@section("modals")
	@parent
	@include('auto-booking/checkout-modals/decor-add-ons/d', [
		"mapId" => $data['decor']->id,
		"mapTypeId" => config('evibe.ticket.type.decor'),
		"typeId" => config('evibe.ticket.type.decor'),
		"data" => $data,
		"addOns" => isset($data['addOns']) ? $data['addOns'] : []
	])
@endsection

@section("footer")
	@parent
	<div class="footer-wrap">
		@if (isset($data['views']['footer']) && view()->exists($data['views']['footer']))
			@include($data['views']['footer'])
		@endif
	</div>
@endsection