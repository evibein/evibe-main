@extends("layout.product.package.list.d")

@section("header")
	@if($agent->isTablet())
		@include('occasion.birthday.header.m_list')
	@else
		@include('occasion.birthday.header.header')
	@endif
@endsection

@section('push-notifications')
	@include('plugins.onesignal', ['pushOccasion' => 'birthday'])
@endsection

@section("content")
	<div class="bg-white desktop-list-page">
		<div class="col-md-12 col-lg-12">
			@include("occasion.util.list.d_components.package.top-bar-section")
		</div>

		@include("occasion.util.list.d_components.package.hidden-data-section")

		<div class="col-md-12 col-lg-12 pad-r-20">
			@include("occasion.util.list.d_components.package.content-section")
		</div>
		<div class="clearfix"></div>
	</div>
@endsection

@section("footer")
	@parent
	@include('base.home.why-us-list')
	<div class="footer-wrap">
		@if (isset($data['views']['footer']) && view()->exists($data['views']['footer']))
			@include($data['views']['footer'])
		@endif
		@include('base.home.footer.footer-common')
	</div>
@endsection