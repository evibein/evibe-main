@extends("layout.product.package.list.m")

@section("header")
	@include('occasion.birthday.header.m_list')
@endsection

@section('push-notifications')
	@include('plugins.onesignal', ['pushOccasion' => 'birthday'])
@endsection

@section("content")
	<div class="bg-white mobile-list-page">

		<div class="list-content-section" id="listContentSection">

			@include("occasion.util.list.m_components.package.top-section")

			@include("occasion.util.list.m_components.package.content-wrap")

			@include("occasion.util.list.m_components.package.hidden-data-section")

		</div>

		<div class="mobile-list-footer"></div>

	</div>

	@include("occasion.util.list.m_components.filters-section")

	@include("occasion.util.list.m_components.sort-section")

@endsection

@section("footer")
	@parent
	@include('base.home.why-us')
	<div class="footer-wrap">
		@if (isset($data['views']['footer']) && view()->exists($data['views']['footer']))
			@include($data['views']['footer'])
		@endif
		@include('base.home.footer.footer-common')
	</div>
@endsection