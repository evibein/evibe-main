@extends("layout.occasion.birthday.base-birthday-list")

@section("content")
	<div class="items-results packages-results">
		<div class="col-sm-12">
			<div class="top-panel">
				<div class="col-sm-12">
					<h4 class="display-1 text-cap" title="{{ $data['seo']['pageHeader'] }}">
						Showing {{ $data['seo']['pageHeader'] }}</h4>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="bottom-panel">
				@if($data['packages']->count() > 0 || $data['filters']['clearFilter'])
					<div class="col-xs-12 col-sm-5 col-md-3 col-lg-3 bottom-left-panel">
						<div class="text-center hide unhide__400">
							<div class="in-blk mar-r-20">
								<button class="btn btn-md btn-default btn-show-filter to-toggle__400" data-target=".item-filters">
									<i class="material-icons valign-mid">&#xE152;</i>
									<span class="valign-mid">Filters</span>
								</button>
							</div>
							<div class="in-blk">
								<button class="btn btn-md btn-default btn-show-filter to-toggle__400" data-target=".item-sort-options">
									<i class="material-icons valign-mid">&#xE164;</i>
									<span class="valign-mid">Sort</span>
								</button>
							</div>
						</div>
						<div class="item-filters package-filters no-pad-b__400 hide__400">
							<div class="title">Filter Results</div>
							<ul class="filters-list">
								@include('occasion.util.list-filters.search-filter')
								@include('occasion.util.list-filters.category-filter')
								@include('occasion.util.list-filters.price-filter')
								@include('occasion.util.list-filters.reset-filter', ["routeName" => "city.occasion.birthdays.packages.list"])
							</ul>
						</div>
						<div class="hide__400">
							@include('app.evibe_guarantee')
						</div>
					</div>
				@endif

				<div class="col-xs-12 @if($data['packages']->count() == 0 && !$data['filters']['clearFilter'] ) col-lg-12 col-md-12 col-sm-12 @else col-sm-7 col-md-9 col-lg-9 bottom-right-panel @endif">
					@if($data['packages']->count() > 0 || $data['filters']['clearFilter'])
						@include('occasion.util.list-filters.sort-filter')
						<div class="items-list packages-list">
							@if (count($data['packages']))
								@foreach ($data['packages'] as $package)
									<div class="col-sm-12 col-md-6 col-lg-4 no-pad-l no-pad__400-600">
										<div class="item-result packages">
											<div class="img-wrap">
												<a href="{{ $data['profileBaseUrl'] . $package->url }}?ref=list-image">
													<img data-src="{{ $package->getProfileImg() }}"
															alt="{{ $package->name }} profile picture"
															class="lazy-loading package-list-profile-image">
												</a>
												@include('app.shortlist_results', [
															"mapId" => $package->id,
															"mapTypeId" => $data['mapTypeId'],
															"occasionId" => $data['occasionId'],
															"cityId" => $data['cityId']
														])
											</div>
											<div class="details">
												<div class="title" title="{{ $package->name }}">
													<a href="{{ $data['profileBaseUrl'] . $package->url }}?ref=list-name">
														{{ $package->name }}
													</a>
												</div>
												<div class="action-btns">
													<div class="col-md-6 no-padding hidden-xs hidden-sm border-right">
														@include('app.shortlist_results_link', [
															"mapId" => $package->id,
															"mapTypeId" => $data['mapTypeId'],
															"occasionId" => $data['occasionId'],
															"cityId" => $data['cityId']
														])
													</div>
													<div class="col-md-6 no-padding hidden-xs hidden-sm">
														<a href="{{ $data['profileBaseUrl'] . $package->url . "?ref=book-now#bookNowModal" }}"
																class="item-book-now"
																target="_blank" rel="noopener">Book Now</a>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="content-details">
													<div class="col-md-8 col-sm-12 col-xs-12 no-pad">
														<div class="price-tag text-left">
														<span class="price">
														@price($package['price'])
															@if ($package->price_max && $package->price_max > $package->price)
																- @price($package->price_max)
															@endif
														</span>
															@if ($package->worth && $package->worth > $package->price)
																<span class="price-worth">@price($package->worth)</span>
															@endif
														</div>
													</div>
													<div class="col-md-4 hidden-xs hidden-sm no-pad">
														<div class="rating-wrap hide">
															<input type="number" value="{{ $package->provider->avgRating() }}" class="avg-rating provider-avg-rating hide" title="{{ $package->provider->avgRating() }} - average provider rating for package {{ $package->name }}"/>
														</div>
													</div>
													<div class="clearfix"></div>
													<div class="additional-info"></div>
												</div>
											</div>
										</div>
									</div>
								@endforeach
							@else
								<div class="no-results-wrap text-center">
									<h4 class="no-results-title no-mar">
										<i class="glyphicon glyphicon-warning-sign"></i>
										Oops, no packages were found matching your filters.
									</h4>

									<div class="pad-t-20">
										<a href="{{ route('city.occasion.birthdays.packages.list', $cityUrl) }}?ref=no-results" class="btn btn-danger btn-lg">
											<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
										</a>
									</div>
								</div>
							@endif
							<div class="clearfix"></div>
						</div>
						<div class="pages">
							<div>{{ $data['packages']->appends($data['filters']['queryParams'])->links() }}</div>
						</div>
					@else
						<div class="no-results-wrap text-center">
							<h4 class="text-col-gr no-mar">We are adding amazing packages soon, keep checking this page.</h4>
						</div>
					@endif
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection