@php $sessionCityUrl = is_null(getCityUrl()) ? 'bangalore' : getCityUrl(); @endphp
@php $carouselItemCount=6 @endphp

<div class="section-cat mar-t-20">
	<h4 class="sec-title title-text text-center">Customer Testimonials</h4>
	<div class="" style="margin-left: 50px;margin-right: 50px">
		<div id="g-reviews-carousel-wrap" class="carousel slide  header-wrap" data-ride="carousel">
			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				@if(isset($reviews) && count($reviews))
					@php $reviewCount = 0 @endphp
					@foreach($reviews as $review)
						@if(($reviewCount++)%3 == 0 || $agent->isMobile())
							<div class="item @if($loop->first) active @endif full-width clr-black">
								@endif
								<div class="@if(count($reviews)<3) col-sm-12 col-md-12 col-lg-12 @else col-sm-12 col-md-4 col-lg-4 @endif">
									<div class="google-reviews-card">
										@if(isset($review['profile_pic']) && $review['profile_pic'])
											<img src="{{$review['profile_pic']}}" height="60">
										@else
											<img src="https://lh4.googleusercontent.com/-gZl_LpT5sQU/AAAAAAAAAAI/AAAAAAAAAAA/X_Rq2u1nWiQ/s40-c-rp-mo-br100/photo.jpg">
										@endif
										<p class="fw-500 font-18">{{$review['author']}}<br>
											<span class="list-option-rating in-blk" style="color: #FFCE00;">
													<input type="number" class="google-avg-rating hide" value="{{ $review['rating'] }}" title="{{ $review['rating'] }} google rating "/>
												</span>
										</p>
										<p style="padding-left: 15px;padding-right: 15px;color: #657781">
											{{$review['review'] }}
										</p>
									</div>
								</div>
								@if($reviewCount % 3 == 0 || $agent->isMobile() || $loop->last)
							</div>
						@endif
					@endforeach
				@endif
			</div>
			<!-- Controls -->
			<a class="left carousel-control" href="#g-reviews-carousel-wrap" role="button" data-slide="prev">
				<img src="{{ $galleryUrl }}/main/img/icons/left-arrow-carousel.svg" class="custom-controls-style control-left" style="margin-left: -10px" alt="carousel control left">
			</a>
			<a class="right carousel-control" href="#g-reviews-carousel-wrap" role="button" data-slide="next">
				<img src="{{ $galleryUrl }}/main/img/icons/right-arrow-carousel.svg" class="custom-controls-style control-right" style="margin-left: 10px" alt="carousel control right">
			</a>
		</div>
	</div>
</div>
<div class="clearfix"></div>