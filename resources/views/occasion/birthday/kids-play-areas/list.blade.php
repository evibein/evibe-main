@extends('layout.base')

@section('page-title')
	<title>{{$data['seo']['pt']}}</title>
@endsection

@section('meta-description')
	<meta name="description" content="{{$data['seo']['pd']}}">
@endsection

@section('meta-keywords')
	<meta name="keywords" content="{{$data['seo']['keywords']}}"/>
@endsection

@section("header")
	@include('base.home.header.header-home-city')
	<div class="header-border"></div>
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-list.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/app/page-party-bag-list.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/util/star-rating.css') }}"/>
@endsection

@section("content")
	<div class="w-1366">
		<!-- Header Image -->
		<div class="header-wrap" style="height:auto">
			@if($agent->isMobile()||$agent->isTablet() )
				<img src="https://gallery.evibe.in/main/pages/play-area/play-area-m.png" style="width: 100%;">
			@else
				<img src="https://gallery.evibe.in/main/pages/play-area/play-area-d.png" style="width: 100%;">
			@endif
		</div>
		<section style="padding-top: 20px ">
			<div class="col-xs-12">
				<h1 style="font-size:16px;color:#777;letter-spacing: 1px">Play Areas In {{getCityName()}}</h1>

				@foreach($data['venueData'] as $venue)
					@if($agent->isMobile()||$agent->isTablet())
						<a href="{{route('city.occasion.birthdays.kids_play_areas.profile',[getCityUrl(),'placeId' => $venue['placeId']])}}" class="a-no-decoration-black">
							@endif
							<div class="des-list-card-wrap col-md-3 col-lg-3 col-xs-12 col-sm-6 mar-b-20">
								<div class="des-list-option-card" style="height: 260px">
									<div class="thumb-wraper text-center" style="height: 180px;overflow: hidden;position: relative;">
										<img src="{{$venue['imgUrl']}}" style="height:100%;width:auto;max-width:100%;z-index:3;position:absolute;left:50%;	transform:translate(-50%)">
										<img src="{{$venue['imgUrl']}}" style="z-index:2;position:absolute;filter:blur(6px);	left:0;height:auto;width: 100%">
									</div>
									<div class="list-option-content-wrap no-pad-r no-pad-l">
										<div class="list-campaign-option-title list-option-title col-sm-12" style="height: 30px !important;overflow: hidden;">
											<a href="{{route('city.occasion.birthdays.kids_play_areas.profile',[getCityUrl(),'placeId' => $venue['placeId']])}}" class="list-option-link" href="" style="text-overflow: ellipsis;padding-top:5px">
												{{ $venue['vName'] }}
											</a>
										</div>
										<div class="list-option-cta-wrap text-center">
											<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-pad">
												<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no-pad">
													<a href="{{route('city.occasion.birthdays.kids_play_areas.profile',[getCityUrl(),'placeId' => $venue['placeId']])}}" class="item-shortlist">
														<div class=" list-cta-btn ">
															View Details
														</div>
													</a>
												</div>
												<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no-pad">
													<div class="list-cta-btn item-book-now" id="btn-bestDeal">
														Get Best Deal
													</div>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="clearfix"></div>
										</div>
										@if(isset($venue['rating']))
											<div class="list-option-rating col-xs-7 ">
														<span class="mar-r-4">
															<input type="number" class="product-avg-rating hide" value="{{ $venue['rating'] }}" title=" average rating"/>
														</span>
											</div>
											<div class="clearfix"></div>
										@endif
										@if(isset($venue['address']))
											<div class="col-xs-12" style="height: 20px;text-overflow: ellipsis;overflow: hidden;font-size:13px">
												<span class=" glyphicon glyphicon-map-marker"></span>{{$venue['address']}}
											</div>
										@endif
									</div>
								</div>
							</div>
							@if($agent->isMobile()||$agent->isTablet())
						</a>
					@endif
				@endforeach
			</div>
		</section>
		<div class="clearfix"></div>
	@include('occasion.birthday.kids-play-areas.enquiry-modal')
	<!-- hidden Data -->
		<input type="hidden" id="cityUrl" value="@if(is_null(getCityUrl())){{"bangalore"}}@else{{getCityUrl()}}@endif">
		<input type="hidden" id="passiveCity" value="NA">
	</div>
@endsection
@section("footer")
	<div class="footer-wrap">
		@include('app.footer_noncity')
	</div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			$(".item-book-now").on("click", function () {
				var targetModal = $('#book-item-modal');
				targetModal.modal({
					'show': true,
					'keyboard': true
				});
			});

		});
		$.getScript("/js/util/star-rating.js", function () {
			function applyRatingStar(to) {
				$(to).rating({
					showClear: 0,
					readonly: 'true',
					showCaption: 0,
					size: 'xl',
					min: 0,
					max: 5,
					step: 0.1
				});
				$(to).removeClass('hide');
			}

			applyRatingStar('.product-avg-rating');

		});

		$('.enquiry-details-submit').click(function (e) {
			e.preventDefault();

			var passiveCity = $("#passiveCity").val();
			if (passiveCity == "NA") {
				var cityUrl = $("#cityUrl").val();
			} else {
				var cityUrl = passiveCity;
			}
			console.log("entered");
			var url = '/' + cityUrl + '/book';
			var data = {
				'name': $('.lead-details-name').val(),
				'phone': $('.lead-details-phone').val(),
				'callingCode': '+91',
				'email': $('.lead-details-email').val(),
				'partyDate': $('.lead-details-party-date').val(),
				'comments': $('.lead-details-comments').val(),
				'accepts': 'yes',
				'passiveCity': passiveCity,
				'isPlayAreaBooking': $("#isPlayAreaBooking").val()
			};
			$('#btnSubmitFormHome').prop('disabled', true);
			$('.errors-cnt').addClass('hide');
			window.showLoading();
			if (location.hash) {
				if (location.hash.substr(1) === "fbEnquiry") {
					data["typeSource"] = $('#hidTypeSourceFb').val();
				}
			}
			$.ajax({
				url: url,
				type: 'POST',
				dataType: 'json',
				data: data,
				success: function (data) {
					window.hideLoading();
					$("#btnSubmitFormHome").removeAttr('disabled');
					if (data.success) {
						/* redirect to thank you page */
						if (data.redirectTo) {
							window.location = data.redirectTo;
						} else {
							window.showNotySuccess("Thank you! We've received your request. We will get in touch with you shortly.");
						}
					} else {
						var errors = data.errors;
						window.showNotyError(errors);
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					$("#btnSubmitFormHome").removeAttr('disabled');
					$('.errors-cnt').removeClass('hide').text("Sorry, your request could not be saved. Please try again later.");
					window.hideLoading();
					window.notifyTeam({
						"url": url,
						"textStatus": textStatus,
						"errorThrown": errorThrown
					});
				}
			});

		});

	</script>

@endsection