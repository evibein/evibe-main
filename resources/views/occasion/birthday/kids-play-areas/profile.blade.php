@extends('layout.base')

@section('page-title')
	<title>{{$data['seo']['pt']}}</title>
@endsection

@section('meta-description')
	<meta name="description" content="{{$data['seo']['pd']}}">
@endsection

@section('meta-keywords')
	<meta name="keywords" content="{{$data['seo']['keywords']}}"/>
@endsection

@section("header")
	@include('base.home.header.header-home-city')
	<div class="header-border"></div>
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-profile.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/app/page-party-bag-list.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/util/star-rating.css') }}"/>
@endsection

@section("content")
	<div class="w-1366">
		<div class="col-md-12 col-lg-12 bg-white desktop-pro-page des-top-section">
			@if(!($agent->isMobile() || $agent->isTablet()))
				<div class="col-md-12 col-lg-12 no-pad">
					<div class="product-breadcrumb-section mar-t-5">
						<ol class="breadcrumb product-breadcrumb text-center__400">
							<li><a href="{{ route('city.home', $cityUrl) }}">{{getCityName()}}</a></li>
							<li><a href="{{ route('city.occasion.birthdays.home', $cityUrl) }}">Birthday Party</a></li>
							<li><a href="{{ route('city.occasion.birthdays.venues.list', $cityUrl) }}">Kids Play Areas</a></li>
						</ol>
					</div>
				</div>
			@endif
			<div class="col-md-12 col-lg-12 no-pad">
				<div class="col-md-8 col-lg-8 no-pad-l">
					<div class="gallery-section">
						@if(isset($data['gallery']) && count($data['gallery']))
							<div class="loading-img-wrap">
								<img src="/images/loading.gif" alt="">
								<div class="pad-t-10">loading gallery...</div>
							</div>
							<div class="gallery-wrap desktop-gallery hide" style="border: none;">
								<div class="fotorama" data-width="100%" data-allowfullscreen="true" data-ratio="900/600" data-maxheight="400" data-nav="thumbs" data-autoplay="true" data-thumbwidth="80" data-thumbmargin="10" data-keyboard="true">
									@if(!($agent->isMobile()||$agent->isTablet()))
										@foreach ($data['gallery'] as $gallery)
											<a href="{{ $gallery['image_url'] }}">
												<img src="{{ $gallery['image_url'] }}" alt="Kids Venue"/>
											</a>
										@endforeach

										<img src="{{ $gallery['image_url'] }}" alt="Lifelong Memories Guaranteed"/>
									@endif
								</div>
							</div>
						@else
							<div><img class="des-profile-no-image" src="{{ $galleryUrl }}/img/icons/balloons.png"></div>
						@endif
					</div>
					<div class="text-center">
						@include('app.social-share-plugin')
					</div>
				</div>
				<div class="col-md-4 col-lg-4" style="border: 1px #F3F3F3 solid; padding-left: 24px;">
					<h1 class="product-title-text no-mar-b" style="color:#212121;font-size: 20px;font-weight: 400;line-height: 28px;">{{$data['venueDetails']->name}}</h1>
					@if(isset($data['venueDetails']->rating))
						<div class="list-option-rating col-xs-12 no-pad-l ">
							<span style="color:#afafaf;text-transform: uppercase;font-weight: 500;"></span>
							<span class="mar-r-4">
								<input type="number" class="product-avg-rating hide" value="{{ $data['venueDetails']->rating }}" title=" average rating"/>
								<span class="font-11 text-muted">(Avg. Rating By Google)</span>
							</span>
						</div>
						<div class="clearfix"></div>
					@endif
					<div>
						<div class="in-blk pad-10 mar-t-15 font-14" style="background-color: #f3f3f3; border-radius: 10px;">
							#NewArrival
						</div>
						<div class="in-blk pad-10 mar-t-15 font-14 mar-l-10" style="background-color: #f3f3f3; border-radius: 10px;">
							#PocketFriendly
						</div>
					</div>
					@php $address = explode(',',$data['venueDetails']->address,-1) @endphp
					<div class="mar-t-25">
						<span style="color:#333333; text-transform: uppercase; font-weight: bold;"><span class="glyphicon glyphicon-map-marker"></span>Address</span><br>
						<div class="pad-l-15">
							@foreach($address as $aLine)
								{{$aLine}},
							@endforeach
						</div>
					</div>
					<div class="text-center mar-t-20 mar-b-25">
						<div class="btn btn-hall-best-deal item-book-now" style="text-transform: uppercase;font-size: 18px;font-weight: 600;color:#fff;background-color:#ed3e72;border: 1px solid #ed3e72;padding: 6px 32px;border-radius: 20px;">Get Best Deal</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	@include("occasion.birthday.kids-play-areas.reviews",['reviews' => $data['venueReviews']])

	<!--USP-->
		@include("base.home.why-us")
		<div class="clearfix"></div>
	@include('occasion.birthday.kids-play-areas.enquiry-modal')

	<!-- hidden Data -->
		<input type="hidden" id="cityUrl" value="{{is_null(getCityUrl())?bangalore:getCityUrl()}}">
		<input type="hidden" id="passiveCity" value="NA">
	</div>
@endsection

@section("footer")
	<div class="footer-wrap">
		@include('app.footer_noncity')
	</div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			$(".item-book-now").on("click", function () {
				console.log("create");
				var targetModal = $('#book-item-modal');
				targetModal.modal({
					'show': true,
					'keyboard': true
				});
			});

		});
		var profileJs = "<?php echo elixir('js/profile/common.js'); ?>";
		$.getScript(profileJs);
		$.getScript("/js/util/star-rating.js", function () {
			function applyRatingStar(to) {
				$(to).rating({
					showClear: 0,
					readonly: 'true',
					showCaption: 0,
					size: 'xl',
					min: 0,
					max: 5,
					step: 0.1
				});
				$(to).removeClass('hide');
			}

			applyRatingStar('.product-avg-rating');

			$('.enquiry-details-submit').click(function (e) {
				e.preventDefault();

				var passiveCity = $("#passiveCity").val();
				if (passiveCity == "NA") {
					var cityUrl = $("#cityUrl").val();
				} else {
					var cityUrl = passiveCity;
				}
				console.log("entered");
				var url = '/' + cityUrl + '/book';
				var data = {
					'name': $('.lead-details-name').val(),
					'phone': $('.lead-details-phone').val(),
					'callingCode': '+91',
					'email': $('.lead-details-email').val(),
					'partyDate': $('.lead-details-party-date').val(),
					'comments': $('.lead-details-comments').val(),
					'accepts': 'yes',
					'passiveCity': passiveCity,
					'isPlayAreaBooking': $("#isPlayAreaBooking").val()
				};
				$('#btnSubmitFormHome').prop('disabled', true);
				$('.errors-cnt').addClass('hide');
				window.showLoading();
				if (location.hash) {
					if (location.hash.substr(1) === "fbEnquiry") {
						data["typeSource"] = $('#hidTypeSourceFb').val();
					}
				}
				$.ajax({
					url: url,
					type: 'POST',
					dataType: 'json',
					data: data,
					success: function (data) {
						window.hideLoading();
						$("#btnSubmitFormHome").removeAttr('disabled');
						if (data.success) {
							/* redirect to thank you page */
							if (data.redirectTo) {
								window.location = data.redirectTo;
							} else {
								window.showNotySuccess("Thank you! We've received your request. We will get in touch with you shortly.");
							}
						} else {
							var errors = data.errors;
							window.showNotyError(errors);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						$("#btnSubmitFormHome").removeAttr('disabled');
						$('.errors-cnt').removeClass('hide').text("Sorry, your request could not be saved. Please try again later.");
						window.hideLoading();
						window.notifyTeam({
							"url": url,
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});

			});

		});


	</script>

@endsection