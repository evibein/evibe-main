<div id="book-item-modal" class="modal fade no-pad-r-imp" role="dialog" tabindex="-1">
	<div class="modal-dialog modal-md">
		<div class="modal-content" style="background-color: #262c3b">
			<div class="col-xs-12 no-pad text-center">
				@if ($agent->isMobile() && !($agent->isTablet()))
					<div>
						<div>
							<img style="height: 65px; margin-top: 20px; vertical-align: top;" src="{{ config("evibe.gallery.host") }}/img/icons/gift-box-white.png">
						</div>
						<div style="color: #ffffff; padding: 10px 0 20px 0">Enter your details</div>
					</div>
					<div>
						<div class="col-xs-12">
							<input type="text" placeholder="Enter your full name" class="lead-details-name" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
							<input type="text" placeholder="Enter your email address" class="lead-details-email" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
							<input type="text" placeholder="Enter your phone number" class="lead-details-phone" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
							<input type="hidden" class="lead-details-comments" value="Enquiry is created for kidsPlayArea,check at {{Request::fullUrl()}}" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
							<input id="enquiryFormDate" name="enquiryFormDate" type="text" class="lead-details-party-date" placeholder="when is your party?" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
							<input type="hidden" id="isPlayAreaBooking" value="{{Request::fullUrl()}}">
						</div>
						<div class="col-xs-6 col-xs-offset-3 mar-t-30 mar-b-30">
							<a class="btn btn-evibe full-width pad-t-10 pad-b-10 enquiry-details-submit">
								SUBMIT
							</a>
							<a class="btn btn-evibe full-width pad-t-10 pad-b-10 enquiry-details-submitting hide">SUBMITTING...</a>
						</div>
						<div class="clearfix"></div>
					</div>
				@else
					<div>
						<div>
							<img style="height: 65px; margin-top: 20px; vertical-align: top;" src="{{ config("evibe.gallery.host") }}/img/icons/gift-box-white.png">
						</div>
						<div style="color: #ffffff; padding: 10px 0 20px 0">Enter your details</div>
					</div>
					<div>
						<div class="col-xs-6 col-xs-offset-3">
							<input type="text" placeholder="Enter your full name" class="lead-details-name" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
							<input type="text" placeholder="Enter your email address" class="lead-details-email" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
							<input type="text" placeholder="Enter your phone number" class="lead-details-phone" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
							<input type="hidden" placeholder="Enter your phone number" class="lead-details-comments" value="Enquiry is created for kidsPlayArea,check at {{Request::fullUrl()}}" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
							<input id="enquiryFormDate" name="enquiryFormDate" type="text" class="lead-details-party-date" placeholder="when is your party?" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
							<input type="hidden" id="isPlayAreaBooking" value="{{Request::fullUrl()}}">
						</div>
						<div class="col-sm-4 col-xs-offset-4 mar-t-30 mar-b-30">
							<a class="btn btn-evibe full-width pad-t-10 pad-b-10 enquiry-details-submit">
								SUBMIT
							</a>
							<a class="btn btn-evibe full-width pad-t-10 pad-b-10 enquiry-details-submitting hide">SUBMITTING...</a>
						</div>
						<div class="clearfix"></div>
					</div>
				@endif
			</div>
			<div class="offer-signup-thankyou text-center hide">
				<div>
					<div style="font-size: 40px; margin-top: 30px; color: #ffffff; height: 35px; line-height: 40px">THANK YOU</div>
					<div style="color: #ffffff; margin: 25px 0; padding: 0 20px;">Our team will get in touch with you shortly. Should you have any queries, kindly email to {{ config("evibe.contact.tech.group") }}, we will respond to you ASAP.</div>
				</div>
				<div>
					<div style="color: #ffffff; margin-bottom: 25px; font-size: 16px;" class="text-underline cur-point in-blk" data-dismiss="modal">Continue Browsing</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>