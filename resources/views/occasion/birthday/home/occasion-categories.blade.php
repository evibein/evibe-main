@php $sessionCityUrl = getCityUrl() @endphp
@php $sessionCityUrl = is_null($sessionCityUrl) ? 'bangalore' : $sessionCityUrl; @endphp

@if ($agent->isMobile() || ($agent->isTablet()))
	<div class="text-center full-width row col-sm-12 col-md-12" style="background-color: #fff;margin-top: -40px">
		<div class="col-sm-6 col-xs-6 col-md-2 pad-20">
			<a href="{{route('city.occasion.birthdays.decors.list',['cityUrl'=>$sessionCityUrl]) }}?ref=occasion-home" class="a-no-decoration-black">
				<img src="{{ $galleryUrl }}/main/pages/occasion-home/garland.png" style=" height: 50px; " alt="Decorations">
				<div class="mar-t-10 home-icon-title">Decorations</div>
			</a>
		</div>
		<div class="col-sm-6 col-xs-6 col-md-2 pad-20">
			<a href="{{ route('city.occasion.birthdays.food.list',$sessionCityUrl) }}?ref=occasion-home" class="a-no-decoration-black">
				<img src="{{ $galleryUrl }}/main/pages/occasion-home/fork.png" style=" height: 50px; " alt="Food">
				<div class="home-icon-title mar-t-10">Food</div>
			</a>
		</div>
		<div class="col-sm-6 col-xs-6 col-md-2 pad-20">
			<a href="{{ route('city.occasion.birthdays.cakes.list',$sessionCityUrl) }}?ref=occasion-home" class="a-no-decoration-black">
				<img src="{{ $galleryUrl }}/main/pages/occasion-home/cake.png" style=" height: 50px; " alt="Cakes">
				<div class="home-icon-title mar-t-10">Cakes</div>
			</a>
		</div>
		<div class="col-sm-6 col-xs-6 col-md-2 pad-20">
			<a href="{{ route('city.occasion.birthdays.packages.list',$sessionCityUrl) }}?ref=occasion-home" class="a-no-decoration-black">
				<img src="{{ $galleryUrl }}/main/pages/occasion-home/gift.png" style=" height: 50px;" alt="Packages">
				<div class="home-icon-title mar-t-10">Packages</div>
			</a>
		</div>
		<div class="col-sm-6 col-xs-6 col-md-2 pad-20">
			<a href="{{ route('e-cards.home') }}?ref=occasion-home" class="a-no-decoration-black">
				<img src="{{ $galleryUrl }}/main/pages/occasion-home/invite.png" style=" height: 50px;" alt="E-cards">
				<div class="home-icon-title mar-t-10">E-Cards</div>
			</a>
		</div>
		<div class="col-sm-6 col-xs-6 col-md-2 pad-20">
			<a href="{{ route('city.occasion.birthdays.ent.list',$sessionCityUrl) }}?ref=occasion-home" class="a-no-decoration-black">
				<img src="{{ $galleryUrl }}/main/pages/occasion-home/wand.png" style=" height: 50px;" alt="Entertainment">
				<div class="home-icon-title mar-t-10">Entertainment</div>
			</a>
		</div>
	</div>
@else
	<div class="text-center no-pad">
		<div style="background-color: #fff;margin-top: -30px" class="pad-b-20 pad-t-20">
			<div class="in-blk occasion-home-icon-wrap ">
				<a href="{{route('city.occasion.birthdays.decors.list',['cityUrl'=>$sessionCityUrl]) }}?ref=occasion-home" class="a-no-decoration-black update-opacity">
					<img class="home-cat-img" src="{{ $galleryUrl }}/main/pages/occasion-home/garland.png" alt="Decorations">
					<h6 class="home-icon-title mar-t-10  no-mar-b">Decorations</h6>
				</a>
			</div>
			<div class="in-blk occasion-home-icon-wrap">
				<a href="{{ route('city.occasion.birthdays.food.list',$sessionCityUrl) }}?ref=occasion-home" class="a-no-decoration-black update-opacity">
					<img class="home-cat-img" src="{{ $galleryUrl }}/main/pages/occasion-home/fork.png" alt="Food">
					<h6 class="home-icon-title mar-t-10 no-mar-b">Food</h6>
				</a>
			</div>
			<div class="in-blk occasion-home-icon-wrap">
				<a href="{{ route('city.occasion.birthdays.cakes.list',$sessionCityUrl) }}?ref=occasion-home" class="a-no-decoration-black update-opacity">
					<img class="home-cat-img" src="{{ $galleryUrl }}/main/pages/occasion-home/cake.png" alt="Cakes">
					<h6 class="home-icon-title mar-t-10 no-mar-b">Cakes</h6>
				</a>
			</div>
			<div class="in-blk occasion-home-icon-wrap">
				<a href="{{ route('city.occasion.birthdays.packages.list',$sessionCityUrl) }}?ref=occasion-home" class="a-no-decoration-black update-opacity">
					<img class="home-cat-img" src="{{ $galleryUrl }}/main/pages/occasion-home/gift.png" alt="Packages">
					<h6 class="home-icon-title mar-t-10 no-mar-b">Packages</h6>
				</a>
			</div>
			<div class="in-blk occasion-home-icon-wrap hide">
				<a href="{{ route('store-redirect', ['redirect' => config('evibe.store_host').'/collections/kids-birthday-party-supplies?utm_source=OccasionCategory&utm_medium=Website&utm_campaign=StorePromotion']) }}" class="a-no-decoration-black update-opacity">
					<img class="home-cat-img" src="{{ $galleryUrl }}/main/pages/occasion-home/shop-icon.png" alt="Party Props">
					<h6 class="home-icon-title mar-t-10 no-mar-b">Party Props</h6>
				</a>
			</div>
			<div class="in-blk occasion-home-icon-wrap">
				<a href="{{ route('e-cards.home')}}?ref=occasion-home" class="a-no-decoration-black update-opacity">
					<img class="home-cat-img" src="{{ $galleryUrl }}/main/pages/occasion-home/invite.png" alt="E-Cards">
					<h6 class="home-icon-title mar-t-10 no-mar-b">E-Cards</h6>
				</a>
			</div>
			<div class="in-blk occasion-home-icon-wrap">
				<a href="{{ route('city.occasion.birthdays.ent.list',$sessionCityUrl) }}?ref=occasion-home" class="a-no-decoration-black update-opacity">
					<img class="home-cat-img" src="{{ $galleryUrl }}/main/pages/occasion-home/wand.png" alt="Entertainment">
					<h6 class="home-icon-title mar-t-10 no-mar-b">Entertainment</h6>
				</a>
			</div>
		</div>
	</div>
@endif