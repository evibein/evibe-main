@extends("layout.base-birthdays")

@section("header")
	@include('base.home.header.header-home-city')
@endsection

@section("content")
	<style>
		.home-bg-color {
			background-color: #F8F8F8;
		}
	</style>
	<div class="home-screen w-1366" style="background-color: #F8F8F8;">
		<a href="#" class="scrollup">Scroll</a>

		@if (isset($data['views']['home']) && view()->exists($data['views']['home']))
			@include($data['views']['home'])
		@endif

		@include("base.home.enquiry-form")

		<div class="occasion-customer-reviews"></div>
	</div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			(function carouselSwipe() {
				$.getScript("/js/util/bootstrapCarousalSwipe.js", function () {
					$('#header-carousel-wrap').carousel({
						interval: 5000,
						pause: true
					}).bcSwipe({threshold: 50});
				});
			})();
		});
	</script>
@endsection