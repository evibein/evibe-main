@php $sessionCityUrl = is_null(getCityUrl()) ? 'bangalore' : getCityUrl(); @endphp
@php $carouselItemCount=1 @endphp

@if ($agent->isMobile() && !($agent->isTablet()))
	<div class="intro-header" style="height: 250px">
		<div id="header-carousel-wrap" class="carousel slide " data-ride="carousel" style="height: 250px">
			<ol class="carousel-indicators" style="bottom: -7px;">
				@for($i=0;$i<=$carouselItemCount;$i++)
					<li data-target="#header-carousel-wrap" data-slide-to="{{ $i }}"></li>
				@endfor
			</ol>
			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
			<!--
				<div class="item">
					<a href="{{ config('evibe.store_host') }}/collections/kids-birthday-party-supplies?utm_source=OccasionCarousel&utm_medium=Website&utm_campaign=StorePromotion" class="a-no-decoration" target="_blank">
						<img src="{{ $galleryUrl }}/main/img/home/shop-hero-m2.png" class="full-width" alt="Evibe Store Poster"></a>
					<a class="carousel-caption carousel-caption-style-mobile a-no-decoration" href="{{ config('evibe.store_host') }}/collections/kids-birthday-party-supplies?utm_source=OccasionCarousel&utm_medium=Website&utm_campaign=StorePromotion" target="_blank">
						<h1 class="fc-white no-mar pad-b-15">Party Props<br>
							<span class="fw-400 font-16">Delivered to your doorstep</span>
						</h1>
						<div class="btn btn-home-carousel-cta btn-sm">Shop Now</div>
					</a>
				</div>
				-->
				<div class="item active">
					<a href="{{ route('city.occasion.birthdays.decors.list',['cityUrl'=>$sessionCityUrl]) }}?ref=carousel" class="a-no-decoration">
						<img src="{{ $galleryUrl }}/main/pages/occasion-home/cakes-m.png" class="full-width" alt="Birthday Party Carousel Poster"></a>
					<a href="{{ route('city.occasion.birthdays.decors.list',['cityUrl'=>$sessionCityUrl]) }}?ref=carousel" class="carousel-caption carousel-caption-style-mobile a-no-decoration">
						<h1 class="fc-white no-mar pad-b-15">Cakes!<br><span class="fw-400 font-16">Special Occasion Needs Special Cakes</span>
						</h1>
						<div class="btn btn-home-carousel-cta btn-sm btn-post">Enquire Now</div>
					</a>
				</div>
				<div class="item">
					<a href="{{ route('city.cld.list', $sessionCityUrl) }}?ref=carousel" class="a-no-decoration">
						<img src="{{ $galleryUrl }}/main/pages/occasion-home/decor-m.png" class="full-width" alt="Candle Light Dinner Carousel Poster"></a>
					<a class="carousel-caption carousel-caption-style-mobile" href="{{ route('city.occasion.birthdays.decors.list', $sessionCityUrl) }}?ref=carousel">
						<h1 class="fc-white no-mar pad-b-15"> Decorations!<br>
							<span class="fw-400 font-16">Not Just Decor,But Memories</span>
						</h1>
						<div class="btn btn-home-carousel-cta btn-sm">Browse All</div>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class=" mar-l-15 mar-r-15 mar-t-15 mar-b-30 home-tiles-medium hide" style="background-image: url('{{ $galleryUrl }}/main/img/home/shop-hero-2.png');background-size:cover;">
		<div class="pad-l-20 pad-r-20 pad-b-15 text-center">
			<a href="{{ route('store-redirect', ['redirect' => config('evibe.store_host').'/collections/kids-birthday-party-supplies?utm_source=OccasionCard&utm_medium=Website&utm_campaign=StorePromotion']) }}" class="a-no-decoration">
				<div class="font-24 text-grey">Party Props</div>
				<div class="font-14 mar-t-10 text-grey">Delivered to your doorstep</div>
				<div class="font-14 lh-24 mar-t-15 pad-l-15 pad-r-15 in-blk text-grey" style="border: 1px solid #000000;">Shop Now</div>
			</a>
		</div>
	</div>
@else
	<div class="intro-header" style="height: 400px">
		<div id="header-carousel-wrap" class="carousel slide  header-wrap" data-ride="carousel">
			<ol class="carousel-indicators">
				@for($i=0;$i<=$carouselItemCount;$i++)
					<li data-target="#header-carousel-wrap" data-slide-to="{{ $i }}"></li>
				@endfor
			</ol>
			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="hide">
					<div class="item">
						<a href="{{ route('store-redirect', ['redirect' => config('evibe.store_host').'/collections/kids-birthday-party-supplies?utm_source=OccasionCarousel&utm_medium=Website&utm_campaign=StorePromotion']) }}" class="a-no-decoration">
							<img src="{{ $galleryUrl }}/main/img/home/shop-hero-2.png" alt="Carousel banner of Evibe Store" class="carousel-hero-img">
						</a>
						<div class="carousel-caption carousel-caption-style">
							<h1 class="carousel-header white-text-shadow no-mar">Kids Birthday Party Props</h1>
							<p class="font-26 ls-1-imp" style="text-shadow: -3px 8px 9px rgba(244, 238, 236, 0.4)">Get latest party props delivered to your doorstep</p>
							<a class="btn btn-home-carousel-cta btn-md" href="{{ route('store-redirect', ['redirect' => config('evibe.store_host').'/collections/kids-birthday-party-supplies?utm_source=OccasionCarousel&utm_medium=Website&utm_campaign=StorePromotion']) }}">Shop Now</a>
						</div>
					</div>
				</div>
				<div class="item active">
					<a href="{{ route('city.occasion.birthdays.home', getCityUrl()) }}" class="a-no-decoration">
						<img src="{{ $galleryUrl }}/main/pages/occasion-home/cakes.png" alt="carousel banner of Birthday party" class="carousel-hero-img">
					</a>
					<div class="carousel-caption carousel-caption-style">
						<h1 class="carousel-header black-text-shadow no-mar">Special Occasions Deserve Special Cakes</h1>
						<p class="font-26 ls-1-imp" style="text-shadow: -3px 8px 9px rgba(101,44,27,0.4)">Order Customised Theme Cakes</p>
						<a class="btn btn-home-carousel-cta btn-md btn-post" href="#">Enquire Now</a>
					</div>
				</div>
				<div class="item">
					<a href="{{ route('city.cld.list', $sessionCityUrl) }}?ref=carousel" class="a-no-decoration">
						<img src="{{ $galleryUrl }}/main/pages/occasion-home/decor.png" alt="Carousel banner of Candle light dinners" class="carousel-hero-img">
					</a>
					<div class="carousel-caption carousel-caption-style">
						<h1 class="carousel-header fc-white white-text-shadow no-mar">Not Just Decor, But An Experience!</h1>
						<p class=" fc-white font-26 ls-1-imp" style="text-shadow: -3px 8px 9px rgba(244, 238, 236, 0.4)">Choose From 100s Of Decorations</p>
						<a class="btn btn-home-carousel-cta btn-md" href="{{ route('city.occasion.birthdays.decors.list', $sessionCityUrl) }}?ref=carousel">Browse All</a>
					</div>
				</div>
			</div>
			<!-- Controls -->
			<a class="left carousel-control" href="#header-carousel-wrap" role="button" data-slide="prev">
				<img src="{{ $galleryUrl }}/main/img/icons/left-arrow-carousel.svg" class="custom-controls-style control-left" alt="carousel control left">
			</a>
			<a class="right carousel-control" href="#header-carousel-wrap" role="button" data-slide="next">
				<img src="{{ $galleryUrl }}/main/img/icons/right-arrow-carousel.svg" class="custom-controls-style control-right" alt="carousel control right">
			</a>
		</div>
	</div>
@endif

