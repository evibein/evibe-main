@extends("layout.product.cake.profile.m")

@section("header")
	@include('occasion.birthday.header.m_profile')
@endsection

@section('push-notifications')
	@include('plugins.onesignal', ['pushOccasion' => 'birthday'])
@endsection

@section("content")
	<div class="col-xs-12 col-sm-12 col-md-12 no-pad bg-white mobile-pro-page">
		@include("occasion.util.profile.m_components.cake.gallery")

		@include("occasion.util.profile.m_components.cake.title-section")

		@include("occasion.util.profile.m_components.cake.product-description-section")

		@include("occasion.util.profile.m_components.loading-similar-products")
		<div id="showSimilarProducts"></div>

		@include("occasion.util.profile.m_components.cake.hidden-data-section")

		@include("occasion.util.profile.m_components.cake.call-to-action")

		<hr class="mobile-header-phone">
		@include("base.home.why-us-vertical")
	</div>
	<div class="clearfix"></div>
@endsection

@section("modals")
	@parent
	@include('auto-booking/checkout-modals/cake-add-ons/m', [
		"mapId" => $data['id'],
		"mapTypeId" => config('evibe.ticket.type.cake'),
		"occasionId" => $data['occasionId'],
		"data" => $data,
		"addOns" => isset($data['addOns']) ? $data['addOns'] : []
	])
@endsection