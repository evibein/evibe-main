@extends("layout.base-birthdays")

@section('page-title')
	<title>Best Venues in {{ $cityName }} - Quick Link | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="List of all best venues for birthday parties in {{ $cityName }} based on venue type, hall type, pricing and location.">
@endsection

@section('meta-keywords')
	<meta name="keywords" content="List of venues, {{ $cityName }}, party hall list, pricing details, pictures">
@endsection

@section('og-title')
	<meta property="og:title" content="Best Venues in {{ $cityName }} - Quick Link | Evibe.in"/>
@endsection

@section('og-description')
	<meta property="og:description" content="List of all best venues for birthday parties in {{ $cityName }} based on venue type, hall type, pricing and location."/>
@endsection

@section("header")
	@include('occasion.birthday.header.header')
@endsection

@section("content")
	<?php
	$occasionUrl = config('evibe.occasion.kids_birthdays.url');
	$link = EvibeUtil::getBirthdayFooterLink();
	?>
	<div class="items-results venue-quick-links">
		<div class="col-sm-12">
			<div class="top-panel">
				<div class="col-sm-12">
					<h4 class="display-1 text-cap">
						Quick Links for Venues in {{ $cityName }}
					</h4>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="bottom-panel">
				<div class="col-sm-12">
					@if(count($link['categories']['all']) > 0)
						<div class="col-sm-4">
							<div class="links-title">By Type</div>
							<ul class="links-list">
								@foreach($link['categories']['all'] as $category)
									<li class="a-link">
										<a href="{{ route('city.occasion.birthdays.venue_deals.filterList', [$cityUrl, 'all', $category->url]) }}?ref=quick-links">{{ $category->identifier }} in {{ $cityName }}</a>
									</li>
								@endforeach
							</ul>
						</div>
					@endif
					@if(count($link['locations']['all']))
						<div class="col-sm-4">
							<div class="links-title">By Location</div>
							<ul class="links-list">
								@foreach($link['locations']['all'] as $location)
									<li class="a-link">
										<a href="{{ route('city.occasion.birthdays.venue_deals.filterList', [$cityUrl, $location['url'],'all']) }}?ref=footer">Best Deals in {{ $location['name'] }}</a>
									</li>
								@endforeach
							</ul>
						</div>
					@endif
					<div class="col-sm-3">
						<div class="links-title">By Pricing</div>
						<ul class="links-list">
							<li class="a-link">
								<a href="{{ route('city.occasion.birthdays.venue_deals.list', [$cityUrl]) }}?price_min=0&price_max=300&ref=footer">
									<span class="mar-r-8">Upto</span><span class='rupee-font'>&#8377; </span> 300 (per person)
								</a>
							</li>
							<li class="a-link">
								<a href="{{ route('city.occasion.birthdays.venue_deals.list', [$cityUrl]) }}?price_min=300&price_max=500&ref=footer">
									<span class="mar-r-4">From</span>
									<span class='rupee-font'>&#8377; </span> 300
									<span class="mar-lr-4">to</span>
									<span class='rupee-font'>&#8377; </span> 500
								</a>
							</li>
							<li class="a-link">
								<a href="{{ route('city.occasion.birthdays.venue_deals.list', [$cityUrl]) }}?price_min=500&price_max=750&ref=footer">
									<span class="mar-r-4">From</span>
									<span class='rupee-font'>&#8377; </span> 500
									<span class="mar-lr-4">to</span>
									<span class='rupee-font'>&#8377; </span> 750
								</a>
							</li>
							<li class="a-link">
								<a href="{{ route('city.occasion.birthdays.venue_deals.list', [$cityUrl]) }}?price_min=750&price_max=1000&ref=footer">
									<span class="mar-r-4">From</span>
									<span class='rupee-font'>&#8377; </span> 750
									<span class="mar-lr-4">to</span>
									<span class='rupee-font'>&#8377; </span> 1,000
								</a>
							</li>
							<li class="a-link">
								<a href="{{ route('city.occasion.birthdays.venue_deals.list', [$cityUrl]) }}?price_min=1000&price_max=5000&ref=footer">
									<span class="mar-r-8">Above</span><span class='rupee-font'>&#8377; </span> 1,000
								</a>
							</li>
						</ul>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection