@extends("layout.occasion.birthday.base-birthday-list")

@section("content")
	<div class="items-results venue-results">
		<div class="col-sm-12">
			<div class="top-panel">
				<div class="col-sm-12">
					<h4 class="display-1 text-cap" title="{{ $data['seo']['pageHeader'] }}">
						Showing {{ $data['seo']['pageHeader'] }}</h4>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="bottom-panel">
				<div class="col-xs-12 col-sm-5 col-md-3 col-lg-3 bottom-left-panel"> <!-- left section begin -->
					<div class="text-center hide unhide__400"> <!-- filters begin -->
						<div class="in-blk mar-r-20">
							<button class="btn btn-md btn-default btn-show-filter to-toggle__400" data-target=".item-filters">
								<i class="material-icons valign-mid">&#xE152;</i>
								<span class="valign-mid">Filters</span>
							</button>
						</div>
						<div class="in-blk">
							<button class="btn btn-md btn-default btn-show-filter to-toggle__400" data-target=".item-sort-options">
								<i class="material-icons valign-mid">&#xE164;</i>
								<span class="valign-mid">Sort</span>
							</button>
						</div>
					</div>
					<div class="item-filters venue-filters no-pad-b__400 hide__400">
						<div class="title">Filter Results</div>
						<ul class="filters-list">
							<li class="filter link-filter type-filter" data-type="type" data-more="{{ $data['filters']['typeShowMore'] }}">
								<div class="super-cat">
									<div class="pull-left">
										<a class="all-cats" href="#">All Venue Types</a>
									</div>
									<div class="clearfix"></div>
								</div>
								<ul class="no-mar no-pad ls-none item-sub-cats venue-sub-cats">
									<?php $count = 0; ?>
									@foreach ($data['filters']['allTypes'] as $venueType)
										<li class="@if ($data['filters']['activeType'] == $venueType->url) active @endif
										@if (++$count > 5 ) hide non-top @endif">
											<div class="pull-left">
												@if ($data['filters']['typeCounts'][$venueType->id])
													<a class="cat-link" data-url="{{ $venueType->url }}"
															href="?type={{ $venueType->url }}">{{ $venueType->identifier }}</a>
												@else
													<span class="disabled">{{ $venueType->identifier }}</span>
												@endif
											</div>
											<div class="pull-right font-12 text-muted count">
												<span @if(!$data['filters']['typeCounts'][$venueType->id]) class="disabled" @endif>
													({{ $data['filters']['typeCounts'][$venueType->id] }})
												</span>
											</div>
											<div class="clearfix"></div>
										</li>
									@endforeach
									@if ($count > 5)
										<li class="link-filter-actions">
											<a href="#" class="btn-show-more">
												<i class="glyphicon glyphicon-plus"></i> Show More
											</a>
											<a href="#" class="btn-show-less hide">
												<i class="glyphicon glyphicon-minus"></i> Show Less
											</a>
										</li>
									@endif
								</ul>
							</li>
							<li class="filter link-filter categories-filter" data-type="category" data-more="{{ $data['filters']['catShowMore'] }}">
								<div class="super-cat">
									<div class="pull-left">
										<a class="all-cats" href="#">All Hall Types</a>
									</div>
									<div class="clearfix"></div>
								</div>
								<ul class="no-mar no-pad ls-none item-sub-cats venue-sub-cats">
									<?php $count = 0; ?>
									@foreach ($data['filters']['allCategories'] as $category)
										<li class="@if ($data['filters']['activeCat'] == $category->url) active @endif
										@if (++$count > 5 ) hide non-top @endif">
											<div class="pull-left">
												@if ($data['filters']['catCounts'][$category->id])
													<a class="cat-link" data-url="{{ $category->url }}"
															href="?category={{ $category->url }}">{{ $category->identifier }}</a>
												@else
													<span class="disabled">{{ $category->identifier }}</span>
												@endif
											</div>
											<div class="pull-right font-12 text-muted count">
												<span @if(!$data['filters']['catCounts'][$category->id]) class="disabled" @endif>
													({{ $data['filters']['catCounts'][$category->id] }})
												</span>
											</div>
											<div class="clearfix"></div>
										</li>
									@endforeach
									@if ($count > 5)
										<li class="link-filter-actions">
											<a href="#" class="btn-show-more">
												<i class="glyphicon glyphicon-plus"></i> Show More
											</a>
											<a href="#" class="btn-show-less hide">
												<i class="glyphicon glyphicon-minus"></i> Show Less
											</a>
										</li>
									@endif
								</ul>
							</li>
							<li class="filter price-filter">
								<label>Location</label>
								<select id="filterLocations" class="form-control locations" autocomplete="off">
									<option value="all" @if(empty($data['filters']['location'])) selected="selected" @endif>Across {{ $cityName }}</option>
									@foreach ($data['areas'] as $area)
										<option value="{{ $area->name }}"
												@if (!empty($data['filters']['location']) && $data['filters']['location'] == $area->name) selected="selected" @endif>{{ $area->name }}</option>
									@endforeach
								</select>
							</li>
							<li class="filter price-filter">
								<label>Price (per person)</label>

								<div class="price-slider price-slider__400 no-pad-b">
									<div class="in-blk pos-rel mar-r-10">
										<span class="rupee-text"><span class='rupee-font'>&#8377;</span></span>
										<input id="priceMin" class="price-input" type="text" value="{{ $data['filters']['priceMin'] }}">
										<div class="text-muted text-center">From</div>
									</div>
									<div class="in-blk pos-rel mar-r-10">
										<span class="rupee-text"><span class='rupee-font'>&#8377;</span></span>
										<input id="priceMax" class="price-input" type="text" value="{{ $data['filters']['priceMax'] }}">

										<div class="text-muted text-center">To</div>
									</div>
									<div class="in-blk valign-top">
										<a id="filterPriceBtn" class="btn-price-go btn btn-xs btn-default">
											<i class="glyphicon glyphicon-chevron-right"></i>
										</a>
									</div>
								</div>
							</li>
							<li class="filter capacity-filter">
								<label>Capacity</label>

								<div class="cap-slider price-slider cap-slider__400 price-slider__400">
									<div class="in-blk pos-rel mar-r-10">
										<input id="capMin" class="price-input" type="text" value="{{ $data['filters']['capMin'] }}">
										<div class="text-muted text-center">From</div>
									</div>
									<div class="in-blk pos-rel mar-r-10">
										<input id="capMax" class="price-input" type="text" value="{{ $data['filters']['capMax'] }}">
										<div class="text-muted text-center">To</div>
									</div>
									<div class="in-blk valign-top">
										<a id="filterCapacityBtn" class="btn-price-go btn btn-xs btn-default">
											<i class="glyphicon glyphicon-chevron-right"></i>
										</a>
									</div>
								</div>
							</li>
							@if ($data['filters']['hasFilter'])
								<li class="text-center reset-filters reset-filters__400">
									<a href="{{ route("city.occasion.birthdays.venue_deals.list", $cityUrl) }}?ref=reset-filters" class="font-16">
										<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
									</a>
								</li>
							@endif
						</ul>
					</div>
					<div class="hide__400">
						@include('app.evibe_guarantee')
					</div>
				</div>
				<div class="col-xs-12 col-sm-7 col-md-9 col-lg-9 bottom-right-panel">
					<ul class="no-pad no-mar ls-none item-sort-options hide__400"> <!-- sort options begin -->
						<li class="text-muted font-13">Sort By:</li>
						<li class="@if($data['sort'] == 'popularity') active @endif">
							<a class="sort-option" data-value="popularity">Popularity</a>
						</li>
						<li class="@if($data['sort'] == 'plth' || $data['sort'] == 'phtl') active @endif">
							@if ($data['sort'] == 'phtl')
								<a class="sort-option" data-value="plth">Price <i
											class="glyphicon glyphicon-chevron-down"></i></a>
							@elseif ($data['sort'] == 'plth')
								<a class="sort-option" data-value="phtl">Price <i
											class="glyphicon glyphicon-chevron-up"></i></a>
							@else
								<a class="sort-option" data-value="plth">Price</a>
							@endif
						</li>
						<li class="@if($data['sort'] == 'new-arrivals') active @endif">
							<a class="sort-option" data-value="new-arrivals">New Arrivals</a>
						</li>
					</ul>
					<div class="items-list venues-list"> <!-- results begin -->
						@if (count($data['halls']))
							@foreach ($data['halls'] as $hall)
								<div class="col-sm-12 col-md-6 col-lg-4 no-pad-l no-pad__400-600">
									<div class="item-result venue-result">
										<div class="img-wrap">
											<a href="{{ $hall->getLink($data['occasionUrl']) }}">
												<img src="{{ $hall->getProfileImg() }}" alt="{{ $hall->name }}"/>
											</a>
											<div class="hide">
												@include('app.shortlist_results', [
																"mapId" => $hall->id,
																"mapTypeId" => $data['mapTypeId'],
																"occasionId" => $data['occasionId'],
																"cityId" => $data['cityId']
															])
											</div>
										</div>
										<div class="details">
											@if ($hall->venue->is_partner)
												<div class="badges-wrap">
													<img src="{{ $galleryUrl }}/img/icons/best_price.png" alt="{{ $hall->code }} best deal"/>
												</div>
											@endif
											<div class="title">
												<a href="{{ $hall->getLink($data['occasionUrl']) }}">
													@if ($hall->type_id == 1)
														Party space
													@else
														{{ $hall->type->name }}
													@endif
													<span> at {{ $hall->venue->area->name }}</span>
												</a>
											</div>
											<div class="action-btns">
												<div class="col-md-6 no-padding hidden-xs hidden-sm border-right hide">
													@include('app.shortlist_results_link', [
														"mapId" => $hall->id,
														"mapTypeId" => $data['mapTypeId'],
														"occasionId" => $data['occasionId'],
														"cityId" => $data['cityId']
													])
												</div>
												<div class="col-md-12 no-padding hidden-xs hidden-sm">
													<a href="{{ $hall->getLink($data['occasionUrl']) }}" class="item-view-details">View Details</a>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="content-details">
												<div class="col-md-8 col-sm-12 col-xs-12 no-pad">
													<div class="price-tag text-left">
														@if($hall->rent_min)
															<span class="price">@price($hall->rent_min)</span>
															@if ($hall->rent_max)
																-
																<span class="price">@price($hall->rent_max)</span>*
															@else
																* onwards
															@endif
															@if($hall->rent_worth)
																<span class="price-worth">@price($hall->rent_worth)</span>
															@endif
														@elseif ($hall->price_min_veg)
															<span class="price-worth">
																	@price($hall->price_worth_veg)
																</span>
															<span class="price">
																	@price($hall->price_min_veg)*
																</span>
														@elseif ($hall->venue->price_min_rent)
															<span class="price">@price($hall->venue->price_min_rent)</span>
															@if ($hall->venue->price_max_rent)
																-
																<span class="price">@price($hall->venue->price_max_rent)</span>*
															@else
																* onwards
															@endif
															@if($hall->venue->worth_rent)
																<span class="price-worth">@price($hall->venue->worth_rent)</span>
															@endif
														@elseif ($hall->venue->price_min_veg)
															<span class="price">
																	@price($hall->venue->price_min_veg)*
																</span>
															<span class="price-worth">
																	@price($hall->venue->min_veg_worth)
																</span>
														@elseif ($hall->venue->price_kid)
															<span class="price">
																	@price($hall->venue->price_kid)*
																</span>
															<span class="price-worth">
																	@price($hall->venue->worth_kid)
																</span>
														@else
														@endif
													</div>
												</div>
												<div class="col-md-4 hidden-xs hidden-sm no-pad">
													<div class="rating-wrap hide">
														<?php $extRatings = $hall->venue->prepareExtRatings(); ?>
														<input type="number" class="provider-avg-rating hide" value="{{ $extRatings['avgRating'] }}" title="{{ $extRatings['avgRating'] }} average rating"/>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="additional-info">
													<div class="pull-left">
														@if($hall->rent_min)
															<div>
																<div class="font-12 font-italic">Rent ({{ $hall->min_rent_duration }} Hrs)</div>
															</div>
														@elseif ($hall->price_min_veg)
															<div>

																<div class="font-12 font-italic">Price (per person)</div>
															</div>
														@elseif ($hall->venue->price_min_rent)
															<div>

																<div class="font-12 font-italic">Rent ({{ $hall->venue->min_rent_duration }} Hrs)</div>
															</div>
														@elseif ($hall->venue->price_min_veg)
															<div>

																<div class="font-12 font-italic">Price (per person)</div>
															</div>
														@elseif ($hall->venue->price_kid)
															<div>
																<div class="font-12 font-italic">Price (per kid)</div>
															</div>
														@else
														@endif
													</div>
													<div class="pull-right text-right">
														<div class="guests-count">
															<i class="material-icons valign-mid">&#xE7FB;</i>
															<span class="valign-mid">
																@number( $hall->cap_min ) - @number( $hall->cap_float )
															</span>
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							@endforeach
						@else
							<div class="no-results">
								<div class="alert alert-danger text-center no-results-inner">
									<h3 class="no-mar">
										<i class="material-icons valign-mid">&#xE002;</i>
										<span class="valign-mid">Ooops!</span>
									</h3>
									<div class="pad-t-20 font-20">
										Sorry, no matching venues were found for your search.
									</div>
									<div class="pad-t-20 text-center">
										<a href="/{{ $cityUrl }}/venues?ref=no-results" class="mdl-button mdl-js-button mdl-button--raised">Reset Filters</a>
									</div>
								</div>
							</div>
						@endif
						<div class="clearfix"></div>
					</div>
					<div class="pages"> <!-- pagination begin -->
						<div>{{ $data['halls']->appends($data['filters']['queryParams'])->links() }}</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			var $selectizeObj = $('#filterLocations').selectize();

			$(".btn-show-more").click(function (event) {
				event.preventDefault();
				$(this).parents(".link-filter").find("li.non-top").removeClass("hide");
				$(this).addClass("hide");
				$(this).parent().find(".btn-show-less").removeClass("hide");
			});

			$(".btn-show-less").click(function (event) {
				event.preventDefault();
				$(this).parents(".link-filter").find("li.non-top").addClass("hide");
				$(this).addClass("hide");
				$(this).parent().find(".btn-show-more").removeClass("hide");
			});

			$(".link-filter").each(function (i, item) {
				if ($(item).data('more')) {
					$(this).find(".btn-show-more").click();
				}
			});

			/* capacity filters */
			$("#filterCapacityBtn").click(function (event) {
				event.preventDefault();
				var capMin = parseInt($("#capMin").val(), 10);
				var capMax = parseInt($("#capMax").val(), 10);
				if (capMax < capMin) return false;
				refreshUrl(formUrl(formUrl(formUrl(null, "page", null, true), 'capacity_min', capMin), 'capacity_max', capMax));
			});

			/* location filter */
			var selectize = $selectizeObj[0].selectize;
			selectize.on("item_add", function (value, item) {
				/* remove page number when location changed */
				refreshUrl(formUrl(formUrl(null, "page", null, true), 'location', value));
			});
		});
	</script>
@endsection