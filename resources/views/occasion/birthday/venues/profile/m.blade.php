@extends("layout.product.venue.profile.m")

@section("header")
	@include('occasion.birthday.header.m_profile')
@endsection

@section('content')
	<div class="col-xs-12 col-sm-12 no-pad bg-white mobile-pro-page">
		@include("occasion.util.profile.m_components.gallery")

		@include("occasion.util.profile.m_components.venue.title-section")

		@include("occasion.util.profile.m_components.venue.enquiry-section", [
							"enquiryUrl" => route("ticket.venue", [$data['occasionId'], $data['hall']['id']]),
							"hidePartyLocation" => true
						])

		@include("occasion.util.profile.m_components.venue.product-description-section")

		@include("occasion.util.profile.m_components.venue.hidden-data-section")

		<hr class="mobile-profile-hr">
		@include("base.home.why-us-vertical")
	</div>
@endsection
