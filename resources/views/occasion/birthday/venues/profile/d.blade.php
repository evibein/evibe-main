@extends("layout.product.venue.profile.d")

@section("header")
	@if($agent->isTablet())
		@include('occasion.birthday.header.m_profile')
	@else
		@include('occasion.birthday.header.header')
	@endif
@endsection

@section('content')
	<div class="col-md-12 col-lg-12 bg-white desktop-pro-page">
		<div class="des-top-section">
			<div class="col-md-12 col-lg-12 no-pad">
				@include("occasion.util.profile.d_components.venue.breadcrumb-section")
			</div>
			<div class="col-md-12 col-lg-12 no-pad">
				<div class="col-md-4 col-lg-4 no-pad-l">
					@include("occasion.util.profile.d_components.gallery")
					<div class="text-center">
						@include('app.social-share-plugin')
					</div>
				</div>
				<div class="col-md-4 col-lg-4 no-pad-l no-pad-r">
					@include("occasion.util.profile.d_components.venue.title-section")
				</div>
				<div class="col-md-4 col-lg-4 no-pad-r">
					@include("occasion.util.profile.d_components.venue.enquiry-section", [
							"enquiryUrl" => route("ticket.venue", [$data['occasionId'], $data['hall']['id']]),
							"hidePartyLocation" => true
						])
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<hr class="desktop-profile-hr">
		<div class="des-down-section">
			<div class="col-md-12 col-lg-12 no-pad">
				<div class="col-md-8 col-lg-8">
					@include("occasion.util.profile.d_components.venue.product-detailed-info")
				</div>
				<div class="col-md-4 col-lg-4 no-pad">
					<div class="right-panel-cards">
						<div class="right-panel-wrap pad-l-15 pad-r-15">
							@include("base.home.why-us-vertical")
							<div class="mar-t-5"></div>
							<hr class="desktop-profile-sub-hr">
							@include('app.process.report-issue')
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		@include("occasion.util.profile.d_components.venue.hidden-data-section")
	</div>
@endsection

@section("footer")
	@parent
	<div class="footer-wrap">
		@if (isset($data['views']['footer']) && view()->exists($data['views']['footer']))
			@include($data['views']['footer'])
		@endif
		@include('base.home.footer.footer-common')
	</div>
@endsection