@section("content")
	<div class="items-results venue-results">
		<div class="col-sm-12 no-pad__400-600">
			<div class="venue-list-options-count-wrap" title="{{ $data['seo']['pageHeader'] }}">
				Showing {{ $data['seo']['pageHeader'] }}</div>
			<div class="">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad-l">
					<div class="mar-t-15"> <!-- results begin -->
						@if (count($data['halls']))
							@foreach ($data['halls'] as $hall)
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3 no-pad-r">
									<div class="venue-list-option-wrap">
										<div class="venue-list-option-img-wrap">
											<a href="{{ $hall->getLink($data['occasionUrl']) }}">
												<img src="{{ $hall->getProfileImg() }}" alt="{{ $hall->name }}"/>
											</a>
										</div>
										<div class="venue-list-option-content">
											<div class="venue-list-option-title text-center">
												<a href="{{ $hall->getLink($data['occasionUrl']) }}">
													@if ($hall->type_id == 1)
														Party space
													@else
														{{ $hall->type->name }}
													@endif
													<span> at {{ $hall->venue->area->name }}</span>
												</a>
											</div>
											<div class="venue-list-option-details">
												<div class="venue-list-option-details-1">
													<div class="venue-list-option-price-wrap pull-left">
														@if($hall->rent_min)
															@if ($hall->rent_max)
																<span class="venue-list-option-price">@price($hall->rent_min)</span>
																<span> - </span>
																<span class="venue-list-option-price">@price($hall->rent_max)*</span>
															@else
																@if($hall->rent_worth)
																	<span class="venue-list-option-price-worth mar-l-4">@price($hall->rent_worth)</span>
																@endif
																<span class="venue-list-option-price mar-l-4">@price($hall->rent_min)*</span>
															@endif
														@elseif ($hall->price_min_veg)
															<span class="venue-list-option-price-worth">
																	@price($hall->price_worth_veg)
																</span>
															<span class="venue-list-option-price mar-l-4">
																	@price($hall->price_min_veg)*
																</span>
														@elseif ($hall->venue->price_min_rent)
															@if ($hall->venue->price_max_rent)
																<span class="venue-list-option-price">@price($hall->venue->price_min_rent)</span>
																<span> - </span>
																<span class="venue-list-option-price">@price($hall->venue->price_max_rent)*</span>
															@else
																@if($hall->venue->worth_rent)
																	<span class="venue-list-option-price-worth mar-l-4">@price($hall->venue->worth_rent)</span>
																@endif
																<span class="venue-list-option-price mar-l-4">@price($hall->venue->price_min_rent)*</span>
															@endif

														@elseif ($hall->venue->price_min_veg)
															<span class="venue-list-option-price-worth">
																	@price($hall->venue->min_veg_worth)
																</span>
															<span class="venue-list-option-price mar-l-4">
																	@price($hall->venue->price_min_veg)*
																</span>
														@elseif ($hall->venue->price_kid)
															<span class="venue-list-option-price-worth">
																	@price($hall->venue->worth_kid)
																</span>
															<span class="venue-list-option-price mar-l-4">
																	@price($hall->venue->price_kid)*
																</span>
														@else
														@endif
													</div>
													<div class="venue-list-option-rating-wrap pull-right">
														<div class="rating-wrap hide in-blk">
															<?php $extRatings = $hall->venue->prepareExtRatings(); ?>
															<input type="number" class="provider-avg-rating hide" value="{{ $extRatings['avgRating'] }}" title="{{ $extRatings['avgRating'] }} average rating"/>
														</div>
														@if(isset($extRatings['reviewCount']) && $extRatings['reviewCount'])
															<div class="venue-list-option-rating-count in-blk hide">({{ $extRatings['reviewCount'] }})</div>
														@endif
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="venue-list-option-details-2">
													<div class="venue-list-option-additional-info pull-left">
														@if($hall->rent_min)
															<div>
																<div class="font-12 font-italic">Rent ({{ $hall->min_rent_duration }} Hrs)</div>
															</div>
														@elseif ($hall->price_min_veg)
															<div>

																<div class="font-12 font-italic">Price (per person)</div>
															</div>
														@elseif ($hall->venue->price_min_rent)
															<div>

																<div class="font-12 font-italic">Rent ({{ $hall->venue->min_rent_duration }} Hrs)</div>
															</div>
														@elseif ($hall->venue->price_min_veg)
															<div>

																<div class="font-12 font-italic">Price (per person)</div>
															</div>
														@elseif ($hall->venue->price_kid)
															<div>
																<div class="font-12 font-italic">Price (per kid)</div>
															</div>
														@else
														@endif
													</div>
													<div class="venue-list-option-guest-count pull-right">
														<i class="material-icons valign-mid">&#xE7FB;</i>
														<span class="valign-mid">@number( $hall->cap_min ) - @number( $hall->cap_float )</span>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
											<div class="venue-list-option-cta-wrap">
												<a href="{{ $hall->getLink($data['occasionUrl']) }}" class="venue-list-option-cta">
													<div class="venue-list-option-cta">View Details</div>
												</a>
											</div>
										</div>
									</div>
								</div>
							@endforeach
						@else
							<div class="no-results">
								<div class="alert alert-danger text-center no-results-inner">
									<h3 class="no-mar">
										<i class="material-icons valign-mid">&#xE002;</i>
										<span class="valign-mid">Ooops!</span>
									</h3>
									<div class="pad-t-20 font-20">
										Sorry, no matching venues were found for your search.
									</div>
									<div class="pad-t-20 text-center">
										<a href="/{{ $cityUrl }}/venues?ref=no-results" class="mdl-button mdl-js-button mdl-button--raised">Reset Filters</a>
									</div>
								</div>
							</div>
						@endif
						<div class="clearfix"></div>
					</div>
					<div class="pages pad-l-20"> <!-- pagination begin -->
						<div>{{ $data['halls']->appends($data['filters']['queryParams'])->links() }}</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			var $selectizeObj = $('#filterLocations').selectize();

			$(".btn-show-more").click(function (event) {
				event.preventDefault();
				$(this).parents(".link-filter").find("li.non-top").removeClass("hide");
				$(this).addClass("hide");
				$(this).parent().find(".btn-show-less").removeClass("hide");
			});

			$(".btn-show-less").click(function (event) {
				event.preventDefault();
				$(this).parents(".link-filter").find("li.non-top").addClass("hide");
				$(this).addClass("hide");
				$(this).parent().find(".btn-show-more").removeClass("hide");
			});

			$(".link-filter").each(function (i, item) {
				if ($(item).data('more')) {
					$(this).find(".btn-show-more").click();
				}
			});

			/* capacity filters */
			$("#filterCapacityBtn").click(function (event) {
				event.preventDefault();
				var capMin = parseInt($("#capMin").val(), 10);
				var capMax = parseInt($("#capMax").val(), 10);
				if (capMax < capMin) return false;
				refreshUrl(formUrl(formUrl(formUrl(null, "page", null, true), 'capacity_min', capMin), 'capacity_max', capMax));
			});
		});
	</script>
@endsection