@extends("layout.occasion.birthday.base-birthday-profile")

@section("content")
	<div class="col-md-12 bg-white">
		<a href="#" class="scrollup">Scroll</a>
		<div class="col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0"> <!-- Decors profile begin -->
			<div class="col-md-12 no-pad mar-b-15">
				<div class="breadcrumb-section">
					<ol class="breadcrumb text-center__400">
						<li><a href="{{ route('city.home', $cityUrl) }}">{{$cityName}}</a></li>
						<li><a href="{{ route('city.occasion.birthdays.home', $cityUrl) }}">Birthday Party</a></li>
						<li><a href="{{ route('city.occasion.birthdays.trends.list', $cityUrl) }}">Trends</a></li>
						<li class="active">{{ $data['name'] }}</li>
					</ol>
				</div>
				<div class="header-wrap"> <!-- header begin -->
					<div class="col-md-12 no-pad">
						<div class="col-md-8"> <!-- title begin -->
							<h4 class="no-mar mar-t-10 text-center__400">
								{{ $data['name'] }}
							</h4>
						</div>
						<div class="col-md-4 text-center mar-t-10__400 no-pad-r hide"> <!-- actions begin -->
							@include('app.shortlist_detail')
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="info-panel">
				<div class="row">
					<div class="col-md-8 mar-t-10 pad-b-15">
						<!-- gallery begin -->
						@if (count($data['images']) > 0)
							<div class="loading-img-wrap">
								<img src="/images/loading.gif" alt="">
								<div class="pad-t-10">loading images...</div>
							</div>
							<div class="gallery-wrap decor-gallery-wrap mar-b-10 hide">
								<div class="fotorama" data-width="100%" data-ratio="900/600" data-maxheight="400" data-nav="thumbs" data-autoplay="true" data-thumbwidth="80" data-thumbmargin="10" data-keyboard="true">
									@foreach ($data['images'] as $key => $image)
										<a href="{{ $image->getPath("profile") }}"><img src="{{ $image->getPath("profile") }}" alt="{{ $image['title'] }}"/></a>
									@endforeach

								</div>
							</div>
							<div>
								<i class="low-font gallery-bt-text">(These picture(s) are only for your reference. Click on an image to enlarge.)</i>
							</div>
						@else
							<div>No images found</div>
						@endif
						<div class="text-center">
							@include('app.social-share-plugin')
						</div>
					</div>
					<div class="col-md-4 mar-t-10">
						<!-- highlights begin -->
						<div class="item-profile highlights-card pad-t-20 pad-b-20">
							<div class="hs-section">
								<div class="card-item text-center price-item">
									<div class="highlight-item">
										<div class="highlight-title">Price</div>
										<div class="price-val mar-t-5">
											@if ($data['price'] != 0)
												@price($data['price'])*
											@else
												<span>----</span>
											@endif
										</div>
									</div>
								</div>
								<div class="card-item card-actions text-center hide">
									<a id="orderBtn" href="#" data-scroll="book" class="btn btn-default btn-view-more btn-more-details btn-best-deal btn-order-now scroll-item">
										Order Now
									</a>
								</div>
								<div class="no-mar-t mar-b-10">
									@include('app.modals.auto-popup',
									[
									"cityId" => $data['shortlistData']['cityId'],
									"occasionId" => $data['shortlistData']['occasionId'],
									"mapTypeId" => $data['shortlistData']['mapTypeId'],
									"mapId" => $data['shortlistData']['mapId'],
									"optionName" => $data['name']
									])
								</div>
								<div class="card-item card-actions text-center">
									<div class="contact-sec">
										<div class="phone">
											<i class="glyphicon glyphicon-phone-alt"></i>
											<span>{{ config('evibe.contact.company.phone') }}</span>
										</div>
										<div class="email">
											<i class="glyphicon glyphicon-envelope"></i>
											<span>ping@evibe.in</span>
										</div>
										<div class="text-timings">
											<span>(Mon - Sat; 10 AM - 7 PM)</span>
										</div>
									</div>
								</div>

							</div>
						</div>
						<!-- highlights end -->
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="col-md-12 bg-light-gray pad-t-20">
		<div class="col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0">
			<div id="moreDetails" class="info-more-panel">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-8">
							@if ($data['info'])
								<div class="main-card info-card"> <!-- inclusions begin -->
									<div class="card-title">
										<h5 class="card-title-text">Description</h5>
									</div>
									<div class="card-info-text no-pad-t">
										{!! $data['info'] !!}
									</div>
								</div>
							@endif
							@if ($data['prereq'])
								<div id="prerequisites" class="main-card more-info-card"> <!-- prerequisites begin -->
									<div class="card-title">
										<h5 class="card-title-text">Prerequisites</h5>
									</div>
									<div class="card-info-text no-pad-t">
										{{ $data['prereq'] }}
									</div>
								</div>
							@endif
							@if ($data['facts'])
								<div id="facts" class="main-card more-info-card"> <!-- facts begin -->
									<div class="card-title">
										<h5 class="card-title-text">Facts</h5>
									</div>
									<div class="card-info-text no-pad-t">
										{!! $data['facts'] !!}
									</div>
								</div>
							@endif
							@include("app.review.partner", [
								"reviews"=> $data["ratings"],
								"showAllReviewsUrl" => route("provider-reviews:all", [
									getSlugFromText($data["name"]),
									config("evibe.ticket.type.planner"),
									$data["providerId"]
								])
							])
							@if ($data['terms'])
								<div class="main-card info-card"> <!-- inclusions begin -->
									<div class="card-title">
										<h5 class="card-title-text">Terms of Booking</h5>
									</div>
									<div class="card-info-text no-pad-t">
										{!! $data['terms'] !!}
									</div>
								</div>
							@endif
						</div>
						<div class="col-md-4">
							@if($data['ratings']['total']['count'])
								<div class="main-card provider-rating-card"> <!-- provider rating card begin -->
									<div class="card-info-text">
										<div class="font-16">Provider Code:
											<b>{{ $data['providerCode'] }}</b>
										</div>
										<div class="font-18 pad-t-10">
											<span>
												<input type="number" class="avg-rating hide" value="{{ $data['ratings']['total']['avg'] }}" title="{{ $data['ratings']['total']['avg'] }} average rating for provider of {{ $data['name'] }}"/>
											</span>
											@if($data['ratings']['total']['count'])
												<span id="reviews" class="card-item__title-hint scroll-item cur-point" data-scroll="reviews"> ({{ $data['ratings']['total']['count'] }} reviews) </span>
											@else
												<span class="card-item__title-hint cur-point"> (0 reviews) </span>
											@endif
										</div>
										<hr class="desktop-profile-sub-hr">

									</div>
								</div>
							@endif
							@include('app.process.non-venue')
							@include('app.process.report-issue')
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
@endsection
@section("footer")
	@parent
	@include('base.home.why-us')
	<div class="footer-wrap">
		@if (isset($data['views']['footer']) && view()->exists($data['views']['footer']))
			@include($data['views']['footer'])
		@endif
		@include('base.home.footer.footer-common')
	</div>
@endsection