@extends("layout.occasion.birthday.base-birthday-list")

@section("content")
	<div class="vendor-results-wrap e-options-wrap">

		<a href="#" class="scrollup">Scroll</a>

		<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 col-md-offset-1 col-sm-offset-1 col-lg-offset-1">
			<h4 class="header-title display-1 text-cap no-mar__400 no_pad__400 pad-tb-20__400 text-center__400">
				{{ $data['seo']['pageHeader'] }}
			</h4>
			<div class="results">
				<div class="results-bottom">
					<div class="vendors-list-wrap">
						@if ($data['hasTrends'])
							@foreach ($data['trends'] as $trend)
								<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
									<div class="item-result ">
										<div class="img-wrap">
											<a href="{{ route('city.occasion.birthdays.trends.profile', [$cityUrl, $trend->url]) }}">
												<img src="{{ $trend->getProfileImg() }}" alt="{{ $trend->name }}" class="gallery-pic" title="{{ $trend->name }}">
											</a>
											<div class="hide">
												@include('app.shortlist_results', [
																"mapId" => $trend['id'],
																"mapTypeId" => $data['mapTypeId'],
																"occasionId" => $data['occasionId'],
																"cityId" => $data['cityId']
															])
											</div>
										</div>
										<div class="details">
											<div class="title">
												<a href="{{ route('city.occasion.birthdays.trends.profile', [$cityUrl, $trend->url]) }}?ref=list-name">
													{{ $trend->name }}
												</a>
											</div>
											<div class="action-btns">
												<div class="col-md-6 no-padding hidden-xs hidden-sm border-right hide">
													@include('app.shortlist_results_link', [
														"mapId" => $trend['id'],
														"mapTypeId" => $data['mapTypeId'],
														"occasionId" => $data['occasionId'],
														"cityId" => $data['cityId']
													])
												</div>
												<div class="col-md-12 no-padding hidden-xs hidden-sm">
													<a href="{{ route('city.occasion.birthdays.trends.profile', [$cityUrl, $trend->url]) }}"
															class="item-view-details">View Details</a>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="content-details">
												<div>
													<div class="col-md-8 col-sm-12 col-xs-12 no-pad">
														<div class="price-tag text-left">
															@if ($trend->price != 0)
																<span class="price">
																@price($trend->price)*
															</span>
															@else
																<span>----</span>
															@endif
														</div>
													</div>
													<div class="col-md-4 hidden-xs hidden-sm no-pad">
														<div class="rating-wrap">
															<a href="{{ route('city.occasion.birthdays.trends.profile', [$cityUrl, $trend->url]) }}"
																	class="btn-view-more">View Details</a>
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="additional-info"></div>
											</div>
										</div>
									</div>
								</div>
							@endforeach
							<div class="clearfix"></div>
						@else
							<div class="no-results-wrap text-center">
								<h4 class="text-col-gr no-mar">We are adding amazing trends soon, keep checking this page.</h4>
							</div>
						@endif
					</div>
					<div class="clearfix"></div>
				</div>
				<div>
					@include('app.modals.auto-popup',
					[
					"cityId" => $data['cityId'],
					"occasionId" => $data['occasionId'],
					"mapTypeId" => $data['mapTypeId'],
					"mapId" => "",
					"optionName" => ""
					])
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection
@section("footer")
	@include('base.home.why-us-list')
	<div class="footer-wrap">
		@if (isset($data['views']['footer']) && view()->exists($data['views']['footer']))
			@include($data['views']['footer'])
		@endif
		@include('base.home.footer.footer-common')
	</div>
@endsection