@extends("layout.product.party-hall.profile.m")

@section("header")
	@include('occasion.birthday.header.m_profile')
@endsection

@section('content')
	<div class="col-xs-12 col-sm-12 no-pad bg-white mobile-pro-page">
		@include("occasion.util.profile.m_components.gallery")

		@include("occasion.util.profile.m_components.party-hall.title-section")

		@include("occasion.util.profile.m_components.party-hall.enquiry-section", [
							"enquiryUrl" => route("ticket.venue", [$data['occasionId'], $data['hall']['id']]),
							"hidePartyLocation" => true
						])

		@include("occasion.util.profile.m_components.party-hall.product-description-section")

		@include("occasion.util.profile.m_components.party-hall.hidden-data-section")

		<hr class="mobile-profile-hr">
		@include("base.home.why-us-vertical")

		<!-- enquiry modal -->
		@include("occasion.util.profile.c_components.party-hall.enquiry-modal", [
							"enquiryUrl" => route("ticket.venue", [$data['occasionId'], $data['hall']['id']]),
							"hidePartyLocation" => true
						])
		<!-- ------------- -->
	</div>
@endsection
