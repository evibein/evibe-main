@extends("layout.product.party-hall.list.m")

@section("header")
	@include("base.home.header.header-home-city")
@endsection

@include('occasion.birthday.party-halls.list.c')

@section("footer")
	@parent
	@include('base.home.why-us')
	<div class="footer-wrap">
		@if (isset($data['views']['footer']) && view()->exists($data['views']['footer']))
			@include($data['views']['footer'])
		@endif
		@include('base.home.footer.footer-common')
	</div>
@endsection