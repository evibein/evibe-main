@extends("layout.product.party-hall.list.d")

@section("header")
	@if($agent->isTablet())
			@include("base.home.header.header-home-city")
	@else
			@include('occasion.birthday.header.header')
	@endif
@endsection

@include('occasion.birthday.party-halls.list.c')

@section("footer")
	@parent
	@include('base.home.why-us-list')
	<div class="footer-wrap">
		@if (isset($data['views']['footer']) && view()->exists($data['views']['footer']))
			@include($data['views']['footer'])
		@endif
		@include('base.home.footer.footer-common')
	</div>
@endsection