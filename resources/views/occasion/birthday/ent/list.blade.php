@extends("layout.occasion.birthday.base-birthday-list")

@section("content")
	<div class="e-options-wrap">
		<div class="col-sm-12">
			<div class="top-panel">
				<div class="col-sm-12">
					<h4 class="header-title display-1 text-cap no-mar__400 no_pad__400 pad-tb-20__400" title="{{ $data['seo']['pageHeader'] }}">Showing {{ $data['seo']['pageHeader'] }}</h4>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="bottom-panel">
				@if($data['options'] || $data['filters']['clearFilter'])
					<div class="col-xs-12 col-sm-5 col-md-3 col-lg-3 bottom-left-panel">
						<div class="text-center hide unhide__400 mar-b-20">
							<div class="in-blk">
								<button class="btn btn-md btn-default btn-show-filter to-toggle__400" data-target=".item-filters">
									<i class="material-icons valign-mid">&#xE152;</i>
									<span class="valign-mid">Filters</span>
								</button>
							</div>
						</div>
						<div class="item-filters package-filters no-pad-b__400 hide__400 mar-b-20">
							<div class="title">Filter Results</div>
							<ul class="filters-list">
								@if(isset($data['filters']['allCategories']) && count($data['filters']['allCategories']))
									<li class="filter link-filter categories-filter" data-type="category">
										<div class="super-cat">
											<div class="pull-left">
												<a href="{{ route("city.occasion.birthdays.ent.list", $cityUrl) }}">All Categories</a>
											</div>
											<div class="clearfix"></div>
										</div>
										<ul class="no-mar no-pad ls-none item-sub-cats packages-sub-cats">
											@foreach ($data['filters']['allCategories'] as $catData)
												@if (array_key_exists($catData->id, $data['filters']['catCounts']))
													<li class="@if($data['filters']['active'] == $catData['url']) active @endif">
														<div class="pull-left">
															@if($data['filters']['catCounts'][$catData->id])
																<a data-url="{{ $catData['url'] }}" href="{{ route("city.occasion.birthdays.ent.list.category", [$cityUrl, $catData['url']]) }}?ref=category-filters">{{ $catData['name'] }}</a>
															@else
																<span class="disabled">{{ $catData['identifier'] }}</span>
															@endif
														</div>
														<div class="pull-right font-12 text-muted count">
															<span @if(!$data['filters']['catCounts'][$catData->id]) class="disabled" @endif>({{ $data['filters']['catCounts'][$catData->id] }})</span>
														</div>
														<div class="clearfix"></div>
													</li>
												@endif
											@endforeach
										</ul>
									</li>
								@endif
							</ul>
						</div>
						<div class="hide__400">
							@include('app.evibe_guarantee')
						</div>
					</div>
				@endif

				<div class="col-xs-12 col-sm-7 col-md-9 col-lg-9 bottom-right-panel ent-list-panel">
					@if ($data['options'])
						@foreach ($data['options'] as $option)
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 pad-l-5 pad-r-5">
								<div class="item-result ent-pkg">
									<div class="img-wrap">
										<a href="{{ $option['fullPath'] }}">
											<img src="{{ $option['profileImg'] }}" alt="{{ $option['name'] }}">
										</a>
										@include('app.shortlist_results', [
																"mapId" => $option['id'],
																"mapTypeId" => $data['mapTypeId'],
																"occasionId" => $data['occasionId'],
																"cityId" => $data['cityId']
															])
									</div>
									<div class="details">
										<div class="title">
											<a href="{{ $option['fullPath'] }}">{{ $option['name'] }}</a>
										</div>
										<div class="action-btns">
											<div class="no-padding hidden-xs in-blk text-center">
												<a href="{{ $option['fullPath'] }}?ref=book-now#bookNowModal" class="item-book-now" target="_blank">Book Now</a>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="content-details">
											<div>
												<div class="no-pad in-blk pull-left pad-t-2">
													<div class="price-tag text-left">
														<span class="price">
															@price($option['minPrice'])
														</span>
														@if (array_key_exists('maxPrice', $option) && $option['maxPrice'] > $option['minPrice'])
															- <span class="price">
																@price($option['maxPrice'])
															</span>
														@endif
														@if (array_key_exists('rangeInfo', $option) && $option['rangeInfo'])
															<span class="glyphicon glyphicon-info-sign font-11 valign-top no-mar-r pad-t-3"></span>
														@endif
													</div>
												</div>
												<div class="no-pad in-blk pull-right hide show-400-600 unhide__400">
													<a href="{{ $option['fullPath'] }}?ref=book-now#bookNowModal" class="btn-view-more" target="_blank">Book Now</a>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						@endforeach
						<div class="pull-right">
							<span class="glyphicon glyphicon-info-sign"></span>
							Final price depends on party location, duration, guests count & provider.
						</div>
					@else
						<div class="no-results-wrap text-center">
							<h4 class="text-col-gr no-mar">We are adding amazing entertainment options soon, keep checking this page.</h4>
						</div>
					@endif
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection