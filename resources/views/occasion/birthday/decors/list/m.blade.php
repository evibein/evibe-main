@extends("layout.product.decor.list.m")

@section("header")
	@include('occasion.birthday.header.m_list')
@endsection

@section('push-notifications')
	@include('plugins.onesignal', ['pushOccasion' => 'birthday'])
@endsection

@section("content")
	<div class="bg-white mobile-list-page">

		<div class="list-content-section" id="listContentSection">

			@include("occasion.util.list.m_components.decor.top-section")

			@include("occasion.util.list.m_components.decor.content-wrap")

			@include("occasion.util.list.m_components.decor.hidden-data-section")

		</div>

		<!--
		<div class="list-bottom-section">
			@if($data['decors']->count() > 0)
				@include("occasion.util.list.m_components.bottom-section-wrap")
			@endif
		</div>
		-->

		<div class="mobile-list-footer"></div>

	</div>
	
		@include("occasion.util.list.m_components.filters-section")

		@include("occasion.util.list.m_components.sort-section")
	

@endsection

@section("footer")
	@parent
	@include('base.home.why-us')
	<div class="footer-wrap">
		@if (isset($data['views']['footer']) && view()->exists($data['views']['footer']))
			@include($data['views']['footer'])
		@endif
		@include('base.home.footer.footer-common')
	</div>
@endsection