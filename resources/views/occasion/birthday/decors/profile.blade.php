@extends("layout.occasion.birthday.base-birthday-profile")

@section('content')
	<div class="col-md-12 bg-white">
		<a href="#" class="scrollup">Scroll</a>
		<div class="col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0 decor-profile"> <!-- Decors profile begin -->
			<div class="col-md-12 no-pad mar-b-15">
				<div class="breadcrumb-section">
					<ol class="breadcrumb text-center__400"> <!-- breadcrumbs begin -->
						<li><a href="{{ route('city.home', $cityUrl) }}">{{$cityName}}</a></li>
						<li><a href="{{ route('city.occasion.birthdays.home', $cityUrl) }}">Birthday Party</a></li>
						<li><a href="{{ route('city.occasion.birthdays.decors.list', [$cityUrl]) }}">Decor Styles</a>
						</li>
						<li class="active">{{ $data['decor']->code }}</li>
					</ol>
				</div>
				<div class="header-wrap"> <!-- header begin -->
					<div class="col-md-12 no-pad">
						<div class="col-md-8"> <!-- title begin -->
							<h4 class="no-mar mar-t-10 text-center__400">
								[#{{ $data['decor']->code }}] {{ $data['decor']->name }}
								<span id="verified" class="text-success">
									<i class="material-icons">&#xE8E8;</i></span>
								<div class="mdl-tooltip mdl-tooltip--large" for="verified">Verified Provider</div>
							</h4>
						</div>
						<div class="col-md-4 text-center mar-t-10__400 no-pad-r"> <!-- actions begin -->
							@include('app.shortlist_detail')
							<a class="btn btn-default btn-view-more btn-best-deal btn-book-now scroll-item" data-toggle="modal" data-target="#bookNowModal">
								<i class="material-icons valign-mid">&#xE8CC;</i>
								<span class="valign-mid">Book Now</span>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="info-panel">
				<div class="row">
					<div class="col-md-8 mar-t-10">
						@if (count($data['gallery']))
							<div class="loading-img-wrap">
								<img src="/images/loading.gif" alt="">
								<div class="pad-t-10">loading gallery...</div>
							</div>
							<div class="gallery-wrap decor-gallery-wrap hide">
								<div class="fotorama" data-width="100%" data-ratio="900/600" data-maxheight="400" data-nav="thumbs" data-autoplay="true" data-thumbwidth="80" data-thumbmargin="10" data-keyboard="true">
									@foreach ($data['gallery'] as $gallery)
										<a href="{{ $gallery['url'] }}">
											<img src="{{ $gallery['thumb'] }}" alt="{{ $gallery['title'] }}"/>
										</a>
									@endforeach
								</div>
							</div>
						@else
							<div>No images found</div>
						@endif
					</div>
					<div class="col-md-4 mar-t-10">
						<!-- highlights begin -->
						<div class="item-profile highlights-card">
							<div class="hs-section">
								<!-- protection begin -->
								<div class="text-center mar-b-5 border-b-1">
									<img src="{{ $galleryUrl }}/img/app/decor_protection_260x66.jpg" alt="Evibe delivery guarantee"/>
								</div>
								<!-- protection end -->
								<div class="card-item text-center price-item pad-t-10">
									@if ($data['decor']->worth && $data['decor']->worth > $data['decor']->min_price)
										<div class="price-worth in-blk">
											@price($data['decor']->worth)
										</div>
									@endif
									<div class="price-val in-blk">
										@price($data['decor']->min_price)
										@if ($data['decor']->max_price && $data['decor']->max_price > $data['decor']->min_price)
											- @price($data['decor']->max_price)
										@endif
									</div>
									@if ($data['decor']->range_info)
										<i class="glyphicon glyphicon-info-sign font-16" data-toggle="tooltip" data-placement="left" title="{{ $data['decor']->range_info }}"></i>
									@endif
								</div>
								<div class="card-item card-supporting-text">
									<div class="card-item">
										<div class="card-item__title"><i
													class="glyphicon glyphicon-map-marker"></i> Location:
										</div>
										<div class="card-item__body">{{ $data['decor']->provider->area->name }}</div>
									</div>
									<div class="card-item">
										<div class="card-item__title"><i
													class="glyphicon glyphicon-time"></i> Style Setup Time:
										</div>
										<div class="card-item__body">
											@if ($data['decor']->time_setup)
												{{ $data['decor']->time_setup }} Hrs
											@else
												N/A
											@endif
										</div>
									</div>
									<div class="card-item">
										<div class="card-item__title"><i
													class="glyphicon glyphicon-time"></i> Style Rent Duration:
										</div>
										<div class="card-item__body">
											@if ($data['decor']->time_duration > 0)
												{{ $data['decor']->time_duration }} Hrs
											@else
												N/A
											@endif
										</div>
									</div>
									@if ($data['decor']->kms_free || $data['decor']->kms_max)
										<div class="card-item">
											<div class="card-item__title blk valign-top">
												<i class="glyphicon glyphicon-retweet"></i> Transport
												<span>(from {{ $data['decor']->provider->area->name}}):</span>
											</div>
											<div class="card-item__body valign-top blk mar-t-3">
												@if (!$data['decor']->kms_free && $data['decor']->trans_min && $data['decor']->kms_max)
													@price($data['decor']->trans_min)
													@if ($data['decor']->trans_max && $data['decor']->trans_max > $data['decor']->trans_min)
														- @price($data['decor']->trans_max)
													@endif
													<span> up to {{ $data['decor']->kms_max }} KMs</span>
												@elseif ($data['decor']->kms_free)
													@if (!$data['decor']->trans_min)
														Free up to {{ max($data['decor']->kms_free, $data['decor']->kms_max) }} KMs
													@else
														@if ($data['decor']->kms_free == $data['decor']->kms_max)
															@price($data['decor']->trans_min)
															@if ($data['decor']->trans_max && $data['decor']->trans_max > $data['decor']->trans_min)
																- @price($data['decor']->trans_max)
															@endif
															for max. {{ $data['decor']->kms_max }} KMs
														@elseif ($data['decor']->kms_free < $data['decor']->kms_max)
															Free up to {{ $data['decor']->kms_free }} KMs
															<span>
															(add @price($data['decor']->trans_min)
																@if ($data['decor']->trans_max && $data['decor']->trans_max > $data['decor']->trans_min)
																	- @price($data['decor']->trans_max)
																@endif
																for max. {{ $data['decor']->kms_max }} KMs)
														</span>
														@else
														@endif
													@endif
												@else
												@endif
											</div>
										</div>
									@endif
								</div>
								<div class="card-item card-actions text-center hide__400">
									<a class="btn btn-default btn-view-more btn-more-details scroll-item " data-scroll="info-container">
										View Details
									</a>
								</div>
							</div>
							<div class="hs-section social-section text-center">
								@include('app.social-share-plugin')
							</div>
						</div>
						<!-- highlights end -->

						<!-- why Evibe? begin -->
						<div class="why-us-card hide">
							<div class="card-item">
								<div class="card-item__title">
									<h6 class="card-title-text mdl-typography--title title">Why Book With Evibe?</h6>
								</div>
								<div class="card-item__body">
									<ul class="no-mar no-pad ls-none">
										<li>
											<span class="text-e"><i class="glyphicon glyphicon-star"></i></span>
											<span><b>100% Delivery</b> Guarantee</span>
										</li>
										<li>
											<i class="glyphicon glyphicon-star"></i>
											<span>Hosted <b>1000+ Events</b></span>
										</li>
										<li>
											<i class="glyphicon glyphicon-star"></i>
											<span>Best <b>Price &amp; Quality</b></span>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<!-- why Evibe? end -->
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="col-md-12 menu-top-b bg-white sticky hidden-sm hidden-xs">
		<div class="col-lg-10 col-md-12 col-lg-offset-1 col-md-offset-0 tabs-scroll">
			<div class="head-scroll-fixed border-b-1 col-md-12">
				<div class="col-md-8">
					<h5 class="mar-t-10 mar-b-10 text-center__400 ov-text-no-wrap">
						[#{{ $data['decor']->code }}] {{ $data['decor']->name }}
						<span id="verified" class="text-success"><i
									class="material-icons">&#xE8E8;</i></span>
						<div class="mdl-tooltip mdl-tooltip--large" for="verified">Verified Provider</div>
					</h5>
				</div>
				<div class="col-md-4 mar-t-15">
					<div class="card-item item-profile price-item">
						@if ($data['decor']->worth && $data['decor']->worth > $data['decor']->min_price)
							<div class="price-worth in-blk">
								<strike>@price($data['decor']->worth)</strike>
							</div>
						@endif
						<div class="price-val in-blk mar-l-10">
							@price($data['decor']->min_price)
							@if ($data['decor']->max_price && $data['decor']->max_price > $data['decor']->min_price)
								- @price($data['decor']->max_price)
							@endif
						</div>
						@if ($data['decor']->range_info)
							<i class="glyphicon glyphicon-info-sign font-16" data-toggle="tooltip" data-placement="left" title="{{ $data['decor']->range_info }}"></i>
						@endif
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="col-md-12">
				<ul class="menu-scroll col-md-10" id="top-menu">
					@if ($data['decor']->info)
						<li><a href="#inclusions" data-scroll="inclusions">Inclusions</a></li>
					@endif
					@if ($data['decor']->more_info)
						<li><a href="#prerequisites">Prerequisites</a></li>
					@endif
					@if ($data['decor']->facts)
						<li><a href="#facts">Facts</a></li>
					@endif
					@if ($data['ratings']['total']['count'])
						<li><a href="#reviews">Reviews</a></li>
					@endif
					@if ($data['decor']->terms)
						<li><a href="#terms">Terms</a></li>
					@endif
				</ul>
				<div class="col-md-2 interested">
					<a class="btn btn-default btn-view-more btn-best-deal btn-book-now scroll-item" data-toggle="modal" data-target="#bookNowModal">
						<i class="material-icons valign-mid">&#xE8CC;</i>
						<span class="valign-mid">Book Now</span>
					</a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div id="info-container" class="col-md-12 bg-light-gray pad-t-20">
		<div class="col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0">
			<div id="decorDetails" class="info-more-panel">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-8">
							<div id="inclusions" class="main-card info-card"> <!-- inclusions begin -->
								<div class="card-title">
									<h5 class="card-title-text">Inclusions</h5>
								</div>
								<div class="card-info-text no-pad-t">
									{!! $data['decor']->info !!}
									@if (count($data['tags']))
										<ul class="tags-lists">
											@foreach ($data['tags'] as $key => $tag)
												<li><i class="glyphicon glyphicon-tag"></i> {{ $tag }}</li>
											@endforeach
										</ul>
									@endif
								</div>
							</div>
							@if ($data['decor']->more_info)
								<div id="prerequisites" class="main-card more-info-card"> <!-- prerequisites begin -->
									<div class="card-title">
										<h5 class="card-title-text">Prerequisites</h5>
									</div>
									<div class="card-info-text no-pad-t">
										{!! $data['decor']->more_info !!}
									</div>
								</div>
							@endif
							@if ($data['decor']->facts)
								<div id="facts" class="main-card more-info-card"> <!-- facts begin -->
									<div class="card-title">
										<h5 class="card-title-text">Facts</h5>
									</div>
									<div class="card-info-text no-pad-t">
										{!! $data['decor']->facts !!}
									</div>
								</div>
							@endif
							@include("app.review.partner", [
								"reviews"=> $data["ratings"],
								"showAllReviewsUrl" => route("provider-reviews:all", [
									getSlugFromText($data["decor"]->name),
									config("evibe.ticket.type.planner"),
									$data["decor"]->provider->id
								])
							])
							@if ($data['decor']->terms)
								<div id="terms" class="main-card terms-card">
									<div class="card-title">
										<h5 class="card-title-text">Terms of Booking</h5>
									</div>
									<div class="card-info-text no-pad-t">{!! $data['decor']->terms !!}</div>
								</div>
							@endif
						</div>
						<div class="col-md-4">
							<div id="provider-rating-card" class="main-card provider-rating-card"> <!-- provider rating card begin -->
								<div class="card-info-text">
									<div class="font-16">Provider Code:
										<b>{{ $data["decor"]->provider->code }}</b>
									</div>
									<div class="font-18 pad-t-10">
										<span>
											<input type="number" class="avg-rating hide" value="{{ $data['ratings']['total']['avg'] }}" title="{{ $data['ratings']['total']['avg'] }} average rating for provider of {{ $data['decor']->name }}"/>
										</span>
										@if($data['ratings']['total']['count'])
											<span id="reviews" class="card-item__title-hint scroll-item cur-point" data-scroll="reviews"> ({{ $data['ratings']['total']['count'] }} reviews) </span>
										@else
											<span class="card-item__title-hint cur-point"> (0 reviews) </span>
										@endif
									</div>
								</div>
							</div>
							<div class="main-card">
								@include('app.bidding.custom_ticket', [
									"url" => route('ticket.custom-ticket.decor', $data['occasionId'])
								])
							</div>
							@include("app.forms.profile_enquiry_form", [
								"enquiryUrl" => route('ticket.decor', [$data['occasionId'], $data['decor']->id])
							])
							@include('app.process.non-venue')
							@include('app.process.report-issue')
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12 bg-white similar-results">
		<div class="col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0">
			@if (count($data['similarStyles']))
				<div class="col-md-12">
					<div class="similar-items-wrap similar-decors-wrap">
						<h4 class="text-center">Similar Decor Styles</h4>
						<div class="similar-decors-list mdl-grid">
							@foreach ($data['similarStyles'] as $decor)
								<div class="col-xs-12 col-sm-6 col-md-4 no-pad-l">
									<div class="item-result decor-style">
										<div class="img-wrap">
											<a href="{{ route('city.occasion.birthdays.decors.profile', [$cityUrl, $decor->url]) }}">
												<img src="{{ $decor->getProfileImg() }}" alt="{{ $decor->name }} profile picture">
											</a>
											@include('app.shortlist_results', [
															"mapId" => $decor->id,
															"mapTypeId" => $data['mapTypeId'],
															"occasionId" => $data['occasionId'],
															"cityId" => $data['cityId']
														])
										</div>
										<div class="details">
											<div class="title">
												<a href="{{ route('city.occasion.birthdays.decors.profile', [$cityUrl, $decor->url]) }}">
													{{ $decor->name }}
												</a>
											</div>
											<div>
												<div class="col-md-6 col-sm-12 col-xs-12">
													<div class="rating-wrap">
														<input type="number" value="{{ $decor->getProviderAvgRating() }}" class="provider-avg-rating hide" title="{{ $decor->getProviderAvgRating() }} average rating for decor style {{ $decor->name }}"/>
													</div>
												</div>
												<div class="col-md-6 no-padding hidden-xs hidden-sm">
													<a href="{{ route('city.occasion.birthdays.decors.profile', [$cityUrl, $decor->url]) }}"
															class="btn btn-primary btn-view-more btn-venue-details pull-right">View Details</a>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="item-info pad-t-5">
												<div class="price-tag text-left mar-t-5 col-md-12 pad-r-0">
													<span class="price">
														@price($decor->min_price)
														@if ($decor->max_price && $decor->max_price > $decor->min_price)
															- @price($decor->max_price)
															@if ($decor->range_info)
																<span class="text-muted hide" data-toggle="tooltip" data-placement="left" title="{{ $decor->range_info }}">
																	<i class="glyphicon glyphicon-info-sign"></i>
																</span>
															@endif
														@endif
													</span>
													@if ($decor->worth && $decor->worth > $decor->min_price)
														<span class="price-worth">
															@price($decor->worth)
														</span>
													@endif
												</div>

												<div class="clearfix"></div>
											</div>
										</div>
									</div>
								</div>
							@endforeach
						</div>
					</div>
				</div>
			@endif
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
@endsection

@section("modals")
	@parent
	@include("auto-booking/checkout-modals/decor", [
		"data" => $data,
		"mapId" => $data["decor"]->id,
		"mapTypeId" => config("evibe.ticket.type.decor"),
		"occasionId" => $data["occasionId"]
	])
@endsection

@section('javascript')
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			var decorJs = "<?php echo elixir('js/decors/decor_profile.js'); ?>";
			$.getScript(decorJs);
		});
	</script>
@endsection
