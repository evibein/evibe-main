<header>
	<?php if (isset($refKeys) && is_array($refKeys) && array_key_exists('occasionHeaderRefKey', $refKeys))
	{
		$linkRef = $refKeys['occasionHeaderRefKey'];
	}
	else
	{
		$linkRef = "header";
	}; ?>

	@include('base.home.header.header-top-city')
	<div class="header-border full-width mar-b-10"></div>
</header>