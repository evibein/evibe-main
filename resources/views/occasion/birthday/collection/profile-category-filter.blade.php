@if(isset($data['collectionOptions']) && count($data['collectionOptions']))
	<li class="filter categories-filter collection-category-filter">
		<div class="super-cat">
			<div class="pull-left">
				<a class="all-styles" href="?category=all">All Categories</a>
			</div>
			<div class="clearfix"></div>
		</div>
		<ul class="no-mar no-pad ls-none item-sub-cats">
			@foreach($data['uniqueMapTypes'] as $mapType)
				<li>
					<div>
						<div class="pull-left">
							@if($data['selCategory'] == $mapType['name'])
								<span class="disabled">{{ $mapType['name'] }}</span>
							@else
								<a class="cat-link" href="?category={{ $mapType['name'] }}">{{ $mapType['name'] }}</a>
							@endif
						</div>
						<div class="pull-right font-12 text-muted count">
							<span @if($data['selCategory'] == $mapType['name']) class="disabled" @endif>
								({{ $mapType['count'] }})
							</span>
						</div>
						<div class="clearfix"></div>
					</div>
				</li>
			@endforeach
		</ul>
	</li>
@endif