@extends("layout.occasion.bachelor.base-bachelor-list")

@section("content")
	<div class="items-results bachelor-pkgs-results">
		<div class="col-sm-12">

			<!-- top section begin -->
			<div class="top-panel">
				<div class="col-sm-12">
					<h4 class="display-1 text-cap" title="{{ $data['seo']['pageHeader'] }}">
						Showing {{ $data['seo']['pageHeader'] }}</h4>
				</div>
				<div class="clearfix"></div>
			</div>
			<!-- top section end -->

			<!-- bottom section begin -->
			<div class="bottom-panel">

				<!-- left section begin -->
				<div class="col-xs-12 col-sm-5 col-md-3 col-lg-3 bottom-left-panel">
					<!-- filters begin -->
					<div class="text-center hide unhide__400">
						<div class="in-blk mar-r-20">
							<button class="btn btn-md btn-default btn-show-filter to-toggle__400" data-target=".item-filters">
								<i class="material-icons valign-mid">&#xE152;</i>
								<span class="valign-mid">Filters</span>
							</button>
						</div>
						<div class="in-blk">
							<button class="btn btn-md btn-default btn-show-filter to-toggle__400" data-target=".item-sort-options">
								<i class="material-icons valign-mid">&#xE164;</i>
								<span class="valign-mid">Sort</span>
							</button>
						</div>
					</div>
					<div class="item-filters no-pad-b__400 hide__400">
						<div class="title">Filter Results</div>
						<ul class="filters-list">
							<li class="filter categories-filter">
								<div class="super-cat">
									<div class="pull-left">
										<a class="all-styles" href="?category=all">All Categories</a>
									</div>
									<div class="clearfix"></div>
								</div>
								<ul class="no-mar no-pad ls-none item-sub-cats">
									@foreach ($data['filters']['cats'] as $key => $category)
										<li class="@if($data['filters']['active'] == $category['url']) active @endif">
											<div class="pull-left">
												<a class="cat-link" data-url="{{ $category['url'] }}" href="?category={{ $category['url'] }}">{{ $category['name'] }}</a>
											</div>
											<div class="clearfix"></div>
										</li>
									@endforeach
								</ul>
							</li>
							<li class="filter price-filter">
								<label>Price</label>

								<div class="price-slider price-slider__400">
									<div class="in-blk pos-rel mar-r-10">
										<span class="rupee-text"><span class='rupee-font'>&#8377;</span></span>
										<input id="priceMin" class="price-input" type="text" value="{{ $data['filters']['priceMin'] }}">

										<div class="text-muted text-center">From</div>
									</div>
									<div class="in-blk pos-rel mar-r-10">
										<span class="rupee-text"><span class='rupee-font'>&#8377;</span></span>
										<input id="priceMax" class="price-input" type="text" value="{{ $data['filters']['priceMax'] }}">

										<div class="text-muted text-center">To</div>
									</div>
									<div class="in-blk valign-top">
										<a id="filterPriceBtn" class="btn-price-go btn btn-xs btn-default">
											<i class="glyphicon glyphicon-chevron-right"></i>
										</a>
									</div>
								</div>
							</li>
							@if ($data['filters']['clearFilter'] || $data['filters']['active'])
								<li class="text-center reset-filters reset-filters__400">
									<a href="{{ route("city.occasion.bachelor.package.list", $cityUrl) }}?ref=reset-filters" class="font-16">
										<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
									</a>
								</li>
							@endif
						</ul>
					</div>
					<!-- filters end -->
					<div class="hide__400">
						@include('app.evibe_guarantee')
					</div>
				</div>
				<!-- left section end -->

				<!-- right section begin -->
				<div class="col-xs-12 col-sm-7 col-md-9 col-lg-9 bottom-right-panel">

					<!-- sort options begin -->
					<ul class="no-pad no-mar ls-none item-sort-options hide__400">
						<li class="text-muted font-13">Sort By:</li>
						<li class="@if($data['sort'] == 'popularity') active @endif">
							<a class="sort-option" data-value="popularity">Popularity</a>
						</li>
						<li class="@if($data['sort'] == 'plth' || $data['sort'] == 'phtl') active @endif">
							@if ($data['sort'] == 'phtl')
								<a class="sort-option" data-value="plth">Price <i
											class="glyphicon glyphicon-chevron-down"></i></a>
							@elseif ($data['sort'] == 'plth')
								<a class="sort-option" data-value="phtl">Price <i
											class="glyphicon glyphicon-chevron-up"></i></a>
							@else
								<a class="sort-option" data-value="plth">Price</a>
							@endif
						</li>
						<li class="@if($data['sort'] == 'new-arrivals') active @endif">
							<a class="sort-option" data-value="new-arrivals">New Arrivals</a>
						</li>
					</ul>
					<!-- sort options end -->

					<!-- results begin -->
					<div class="items-list venue-list">
						@if ($data['packages']->count())
							@foreach ($data['packages'] as $package)
								<div class="col-sm-12 col-md-6 col-lg-4 no-pad-l no-pad__400-600">
									<div class="item-result packages">
										<div class="img-wrap">
											<a href="{{ $data['profileBaseUrl'] . $package->url }}?ref=list-image">
												<img src="{{ $package->getProfileImg() }}" alt="{{ $package->name }} profile picture">
											</a>
											@include('app.shortlist_results', [
															"mapId" => $package->id,
															"mapTypeId" => $data['mapTypeId'],
															"occasionId" => $data['occasionId'],
															"cityId" => $data['cityId']
														])
										</div>
										<div class="details">
											<div class="title" title="{{ $package->name }}">
												<a href="{{ $data['profileBaseUrl'] . $package->url }}?ref=list-name">
													{{ $package->name }}
												</a>
											</div>
											<div class="action-btns">
												<div class="col-md-6 no-padding hidden-xs hidden-sm border-right">
													@include('app.shortlist_results_link', [
															"mapId" => $package->id,
															"mapTypeId" => $data['mapTypeId'],
															"occasionId" => $data['occasionId'],
															"cityId" => $data['cityId']
													])
												</div>
												<div class="col-md-6 no-padding hidden-xs hidden-sm">
													<a href="{{ $data['profileBaseUrl'] . $package->url . "?ref=book-now#bookNowModal" }}"
															class="item-book-now"
															target="_blank" rel="noopener">Book Now</a>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="content-details">
												<div class="col-md-8 col-sm-12 col-xs-12 no-pad">
													<div class="price-tag text-left">
														<span class="price">
															@price($package->price)
														</span>
														@if ($package->price_worth && $package->price_worth > $package->price)
															<span class="price-worth">
																@price($package->price_worth)
															</span>
														@endif
													</div>
												</div>
												<div class="col-md-4 hidden-xs hidden-sm no-pad">
													<div class="rating-wrap hide">
														<input type="number" value="{{ $package->provider->avgRating() }}" class="avg-rating provider-avg-rating hide" title="{{ $package->provider->avgRating() }} - average provider rating for package {{ $package['name'] }}"/>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="additional-info"></div>
											</div>
										</div>
									</div>
								</div>
							@endforeach
						@else
							<div class="no-results-wrap text-center">
								<h4 class="no-results-title no-mar">
									<i class="glyphicon glyphicon-warning-sign"></i>
									Oops, no Packages were found matching your filters.
								</h4>

								<div class="pad-t-20">
									<a href="{{ route("city.occasion.bachelor.package.list", $cityUrl) }}?ref=no-results" class="btn btn-danger btn-lg">
										<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
									</a>
								</div>
							</div>
						@endif
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection

@section('notification-popup')
@endsection