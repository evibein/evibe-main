@extends("layout.product.cake.list.m")

@section("header")
	@include('occasion.bachelor.header.m_list')
@endsection

@section('push-notifications')
	@include('plugins.onesignal', ['pushOccasion' => 'bachelor'])
@endsection

@section("content")
	<div class="bg-white mobile-list-page">

		<div class="list-content-section" id="listContentSection">

			@include("occasion.util.list.m_components.cake.top-section")

			@include("occasion.util.list.m_components.cake.content-wrap")

			@include("occasion.util.list.m_components.cake.hidden-data-section")

		</div>

		<div class="mobile-list-footer"></div>

	</div>

	@include("occasion.util.list.m_components.filters-section")

	@include("occasion.util.list.m_components.sort-section")

@endsection

@section("footer")
	@include('base.home.why-us')
	<div class="footer-wrap">
		@if (isset($data['views']['footer']) && view()->exists($data['views']['footer']))
			@include($data['views']['footer'])
		@endif
	</div>
@endsection