<header>
	@include('base.home.header.header-top-city')

	@if (isset($data['views']['header']) && view()->exists($data['views']['header']))
		@include($data['views']['header'])
	@endif
</header>