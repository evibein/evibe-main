@extends("layout.product.package.profile.m")

@section("header")
	@include('occasion.bachelor.header.m_profile')
@endsection

@section('push-notifications')
	@include('plugins.onesignal', ['pushOccasion' => 'bachelor'])
@endsection

@section("content")
	<div class="col-xs-12 col-sm-12 col-md-12 no-pad bg-white mobile-pro-page">
		@include("occasion.util.profile.m_components.package.gallery")

		@include("occasion.util.profile.m_components.package.title-section")

		@include("occasion.util.profile.m_components.package.product-description-section")

		@include("occasion.util.profile.m_components.loading-similar-products")
		<div id="showSimilarProducts"></div>

		@include("occasion.util.profile.m_components.package.hidden-data-section")

		@include("occasion.util.profile.m_components.package.call-to-action")
		<hr class="mobile-profile-hr">
		@include("base.home.why-us-vertical")
	</div>
	<div class="clearfix"></div>
@endsection

@section("modals")
	@parent
	@include('auto-booking/checkout-modals/generic-package', [
		"mapId" => $data['package']->id,
		"mapTypeId" => config('evibe.ticket.type.package'),
		"typeId" => config('evibe.ticket.type.generic-package'),
		"data" => $data['package']
	])
@endsection