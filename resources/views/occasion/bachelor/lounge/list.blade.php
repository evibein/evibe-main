@extends("layout.occasion.bachelor.base-bachelor-list")

@section("content")
	<div class="items-results bachelor-pkgs-results">
		<div class="col-sm-12">
			<div class="top-panel">
				<div class="col-sm-12">
					<h4 class="display-1 text-cap" title="{{ $data['seo']['pageHeader'] }}">
						Showing {{ $data['seo']['pageHeader'] }}</h4>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="bottom-panel">
				@if($data['packages']->count() > 0 || $data['filters']['clearFilter'])
					<div class="col-xs-12 col-sm-5 col-md-3 col-lg-3 bottom-left-panel">
						<div class="text-center hide unhide__400">
							<div class="in-blk mar-r-20">
								<button class="btn btn-md btn-default btn-show-filter to-toggle__400" data-target=".item-filters">
									<i class="material-icons valign-mid">&#xE152;</i>
									<span class="valign-mid">Filters</span>
								</button>
							</div>
							<div class="in-blk">
								<button class="btn btn-md btn-default btn-show-filter to-toggle__400" data-target=".item-sort-options">
									<i class="material-icons valign-mid">&#xE164;</i>
									<span class="valign-mid">Sort</span>
								</button>
							</div>
						</div>
						<div class="item-filters no-pad-b__400 hide__400">
							<div class="title">Filter Results</div>
							<ul class="filters-list">
								@include('occasion.util.list-filters.search-filter')
								@include('occasion.util.list-filters.category-filter')
								@include('occasion.util.list-filters.price-filter')
								@include('occasion.util.list-filters.reset-filter', ["routeName" => "city.occasion.bachelor.lounge.list"])
							</ul>
						</div>
						<div class="hide__400">
							@include('app.evibe_guarantee')
						</div>
					</div>
				@endif

				<div class="col-xs-12 @if($data['packages']->count() == 0 && !$data['filters']['clearFilter'] ) col-lg-12 col-md-12 col-sm-12 @else col-sm-7 col-md-9 col-lg-9 bottom-right-panel @endif">
					@if($data['packages']->count() > 0 || $data['filters']['clearFilter'])
						@include('occasion.util.list-filters.sort-filter')
						<div class="items-list bachelor-pkgs-list">
							@if (count($data['packages']))
								@foreach ($data['packages'] as $package)
									<div class="col-sm-12 col-md-6 col-lg-4 no-pad-l no-pad__400-600">
										<div class="item-result bachelor-pkg">
											<div class="img-wrap">
												<a href="{{ $data['profileBaseUrl'] . $package->url }}?ref=list-image">
													<img src="{{ $package->getProfileImg() }}" alt="{{ $package->name }} profile picture">
												</a>
												@include('app.shortlist_results', [
															"mapId" => $package->id,
															"mapTypeId" => $data['mapTypeId'],
															"occasionId" => $data['occasionId'],
															"cityId" => $data['cityId']
														])
											</div>
											<div class="details">
												<div class="title">
													<a href="{{ $data['profileBaseUrl'] . $package->url }}?ref=list-name">
														{{ $package->name }}
													</a>
												</div>
												<div class="action-btns">
													<div class="col-md-6 no-padding hidden-xs hidden-sm border-right">
														@include('app.shortlist_results_link', [
															"mapId" => $package->id,
															"mapTypeId" => $data['mapTypeId'],
															"occasionId" => $data['occasionId'],
															"cityId" => $data['cityId']
														])
													</div>
													<div class="col-md-6 no-padding hidden-xs hidden-sm">
														<a href="{{ $data['profileBaseUrl'] . $package->url . "?ref=book-now#bookNowModal" }}"
																class="item-book-now"
																target="_blank" rel="noopener">Book Now</a>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="content-details">
													<div>
														<div class="col-md-8 col-sm-12 col-xs-12 no-pad">
															<div class="price-tag text-left">
															<span class="price">
																@price($package->price)
															</span>
																@if ($package->price_worth && $package->price_worth > $package->price)
																	<span class="price-worth">
																	@price($package->price_worth)
																</span>
																@endif
															</div>
														</div>
														<div class="col-md-4 hidden-xs hidden-sm no-pad">
															<div class="rating-wrap">
																<a href="{{ $data['profileBaseUrl'] . $package->url . "?ref=book-now#bookNowModal" }}"
																		class="btn-view-more"
																		target="_blank" rel="noopener">Book Now</a>
															</div>
														</div>
														<div class="clearfix"></div>
													</div>
													<div class="additional-info"></div>
												</div>
											</div>
										</div>
									</div>
								@endforeach
							@else
								<div class="no-results-wrap text-center">
									<h4 class="no-results-title no-mar">
										<i class="glyphicon glyphicon-warning-sign"></i>
										Oops, no packages were found matching your filters.
									</h4>
									<div class="pad-t-20">
										<a href="{{ route("city.occasion.bachelor.lounge.list", $cityUrl) }}?ref=no-results" class="btn btn-danger btn-lg">
											<i class="glyphicon glyphicon-remove-circle"></i> Reset All Filters
										</a>
									</div>
								</div>
							@endif
							<div class="clearfix"></div>
						</div>
					@else
						<div class="no-results-wrap text-center">
							<h4 class="text-col-gr no-mar">We are adding amazing lounges soon, keep checking this page.</h4>
						</div>
					@endif
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection