@extends("layout.product.decor.list.d")

@section("header")
	@include('occasion.bachelor.header.header')
@endsection

@section('push-notifications')
	@include('plugins.onesignal', ['pushOccasion' => 'bachelor'])
@endsection

@section("content")

	<div class="bg-white desktop-list-page">
		<div class="col-md-12 col-lg-12">
			@include("occasion.util.list.d_components.decor.top-bar-section")
		</div>

		@include("occasion.util.list.d_components.decor.hidden-data-section")

		<div class="col-md-12 col-lg-12 pad-r-20">
			@include("occasion.util.list.d_components.decor.content-section")
		</div>
		<div class="clearfix"></div>
	</div>

	<div class="des-list-slide-container">
		@include("occasion.util.list.d_components.quick-view-slider-common")
	</div>

@endsection

@section("footer")
	@include('base.home.why-us')
	<div class="footer-wrap">
		@if (isset($data['views']['footer']) && view()->exists($data['views']['footer']))
			@include($data['views']['footer'])
		@endif
	</div>
@endsection