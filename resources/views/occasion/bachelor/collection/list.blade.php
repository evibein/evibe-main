@extends("layout.occasion.bachelor.base-bachelor-list")

@section("content")
	<div class="items-results">
		<div class="col-sm-12">
			<div class="top-panel">
				<div class="col-sm-10 col-sm-offset-1">
					<h4 title="{{ $data['seo']['pageHeader'] }}" class="text-cap">
						Showing {{ $data['seo']['pageHeader'] }}
					</h4>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="bottom-panel col-lg-10 col-lg-offset-1 col-sm-12 col-sm-offset-0">
				@if(count($data['collections']) > 0)
					@foreach($data['collections'] as $collection)
						<div class="col-sm-12 col-md-6 col-lg-4 no-pad-l no-pad__400-600">
							<div class="collection-card-list">
								<a href="{{ $data['collectionBaseUrl'].$collection['url'] }}?ref=home-collections">
									<img src="{{ $collection['coverImg'] }}" alt="{{ $collection['name'] }}">
									<div class="collection-card-list-title">
										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 collection-card-list-wrap">
											<img src="{{ $collection['profileImg'] }}" alt="{{ $collection['name'] }}">
										</div>
										<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 collection-card-list-title-wrap">
											<h3 class="mdl-color-text--white pull-left">{{ $collection['name'] }}</h3>
										</div>
										<h6 class="text-center mdl-color-text--white">{{ $collection['description'] }}</h6>
									</div>
								</a>
							</div>
						</div>

					@endforeach
				@else
					<div class="no-results-wrap text-center">
						<h4 class="text-col-gr no-mar">We are adding amazing youth party collections soon, keep checking this page.</h4>
					</div>
				@endif
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			if ($('.header-bottom').length === 0) {
				$('header:not([class])').addClass('couple-exp-header-wrap');
			}
		});
	</script>
@endsection
@section("footer")
	@include('base.home.why-us-list')
	@if (isset($data['views']['footer']) && view()->exists($data['views']['footer']))
		@include($data['views']['footer'])
	@endif
@endsection