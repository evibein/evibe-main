<div class="order-section">
	<!--Greetings card-->
	<div class="card">
		<div class="text-center pad-10">
			<div class="font-18 text-bold pad-10">
				Namaste {{ $data['customer']['name'] }}
			</div>
			<div>Greetings from Evibe. We are pleased to inform that your booking has been confirmed with Evibe. Please click below for more details.</div>
			<div class="text-center mar-t-30 mar-b-30">
				<a href="{{ $data['tmoLink'] }}" target="_blank" class="btn btn-tmo">Track My Order</a>
			</div>
			<div>
				Thanks for choosing evibe.in to book your party. We wish you a wonderful and memorable party.
			</div>
		</div>
	</div>
	<!--Providers card-->
	@if(isset($data['additional']['eventName']) && $data['additional']['eventName'])
		<div class="card">
			<div class="sub-heading">
				Party Details
			</div>
			<div class="mar-t-10">
				<span class="text-bold">Occasion :</span> {{ $data['additional']['eventName'] }}
			</div>
			<div class="mar-t-10">
				<span class="text-bold">Party Date Time :</span> {{ $data['partyDateTime'] }}
			</div>
		</div>
	@endif
	<div class="card">
		<div class="sub-heading">
			Your Order
		</div>
		<?php $counter = 0 ?>
		@foreach($data['bookings'] as $booking)
			<div class="mar-b-10">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-pad-l">
					<div class="booking-details-label">
						{{ EvibeUtil::getTruncateString($booking['typeBookingDetails']) }}
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-pad__400-600">
					<div class="text-right">
						<div class="text-right text-left-400-600 pad-t-10-400-600 text-primary">
							Balance Amount:
							<span class="highlight-price highlight-price-balance">@priceWc($booking['bookingAmount']-$booking['advanceAmount'])</span>
						</div>
						<div class="text-left-400-600 pad-t-10-400-600 text-muted">
							<div class="in-blk blk-400-600 mar-r-10 no-mar-400-600">Advance Paid:
								<span class="highlight-price"> @priceWc($booking['advanceAmount'])</span>
							</div>
							<div class="in-blk blk-400-600 pad-t-10-400-600">
								Booking Amount:
								<span class="highlight-price"> @priceWc($booking['bookingAmount'])</span>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
				<?php $counter++ ?>
				@if($counter !== count($data['bookings']))
					<hr>
				@endif
			</div>
		@endforeach
		<div class="sum-card">
			<div class="col-sm-4 pad-t-10">
				<div class="text-center">
					Total Booking Amount
					<div class="mar-t-3">
						<b class="font-18">@priceWc($data['totalBookingAmount'])</b>
					</div>
				</div>
			</div>
			<div class="col-sm-4 pad-t-10">
				<div class="text-center">
					Total Advance Paid
					<div class="mar-t-3">
						<b class="font-18">@priceWc($data['totalAdvanceAmount'])</b>
					</div>
				</div>
			</div>
			<div class="col-sm-4 pad-t-10">
				<div class="text-center">
					Total Balance Amount
					<div class="mar-t-3">
						<b class="font-18">@priceWc($data['totalBalanceAmount'])</b>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!--Venue details card-->
	<div class="card">
		<div class="sub-heading">Venue Details</div>
		@if(array_key_exists('altPhone',$data) && $data['altPhone'])
			<div>
				<b>Alt. Contact: </b> {{ $data['altPhone'] }}
			</div>
		@endif
		@if($data['hasVenue'] && $data['venueName'])
			<div>
				<b>Venue Name: </b> {{ $data['venueName'] }}
			</div>
		@endif
		<div class="mar-t-4">
			<b>Full Address:</b>
		</div>
		<div class="mar-t-4">
			<span>{{ $data['additional']['venueAddress'] }}</span>
		</div>
		<div class="mar-t-4">
			<u>Landmark</u>:
			<span> @if( $data['additional']['venueLandmark']) {{ $data['additional']['venueLandmark'] }} @else -- @endif </span>
		</div>
		<div class="mar-t-4">
			<u>Venue Type</u>:
			<span> @if($data['additional']['ticketVenueType']) {{ $data['additional']['ticketVenueType']->name }} @else -- @endif </span>
		</div>
		<div class="mar-t-4">
			<u>Google Maps</u>:
			<span class="mar-l-5">
				@if( $data['additional']['mapLink'])
					<a href="{{ "https://" . $data['additional']['mapLink'] }}" target="_blank" rel="noopener"><u>{{ $data['additional']['mapLink'] }}</u></a>
				@else
					--
				@endif
			</span>
		</div>
		<div class="google-map-warp mar-t-20">
			<div id="map">
				@include('order.venue_map')
			</div>
		</div>
	</div>
	<!--Order Details card -->
	@foreach($data['bookings'] as $booking)
		<div class="card">
			@if($data['additional']['paidAt'] < $booking['updatedAt'])
				<div class="highlight-text mar-t-5 pad-5 in-blk mdl-color-text--black">[UPDATED] {{ $booking['updatedMessage'] }}</div>
			@endif
			<div class="mar-t-15">
				<div class="col-sm-3 no-pad-l">
					<div class="text-bold ">Booking ID</div>
					<div class="mar-t-4">{{ $booking['bookingId'] }}</div>
				</div>
				<div class="col-sm-3 no-pad-l">
					<div class="text-bold">Booking Amount</div>
					<div class="mar-t-4">@priceWc($booking['bookingAmount'])</div>
				</div>
				<div class="col-sm-3 no-pad-l">
					<div class="text-bold">Advance Paid</div>
					<div class="mar-t-4">@priceWc($booking['advanceAmount'])</div>
				</div>
				<div class="col-sm-3 no-pad-l">
					<div class="text-bold">Balance Amount</div>
					<div class="mar-t-4">@priceWc($booking['bookingAmount']-$booking['advanceAmount'])</div>
				</div>
				<div class="clearfix"></div>
				<div class="mar-t-10">
					<span class="text-bold">Order Start Time :</span> {{ date("d/m/y h:i A", $booking['partyDateTime']) }}
				</div>
				<div class="mar-t-10">
					<b>Booking Details</b>
					<div class="mar-t-4">
						{!! $booking['bookingInfo'] !!}
					</div>
				</div>
				@if($data['additional']['specialNotes'])
					<div class="mar-t-10">
						<b>Special Notes* </b>(Needs approval if is not already discussed)
						<div>{!! $data['additional']['specialNotes'] !!}</div>
					</div>
				@endif
				@include('order.checkout_field',['checkoutFields'=> $data['additional']['checkoutFields'],'booking'=> $booking])
				@include('order.booking_gallery',['bookingGallery' => $booking['gallery']])
			</div>
			<div class="mar-t-15 hide">
				<div class="provider-details-head">
					Provider details
				</div>
				<div>
					<div class="col-xs-6 no-pad-l">
						<b>Name</b>
						<div>{{ $booking['provider']['person'] }}</div>
					</div>
					<div class="col-xs-6 no-pad-l">
						<b>Phone</b>
						<div>{{ $booking['provider']['phone'] }}</div>
					</div>
					<div class="clearfix"></div>
					<div style="margin-top:3px;font-size:12px;">
						<i>Provider will ensure your order is delivered on time and executed as per the booking details below.</i>
					</div>
				</div>
			</div>
		</div>
	@endforeach
	<div class="card">
		<div class="sub-heading">
			Your Information
		</div>
		@include('order.customer_info')
	</div>
	<div class="card">
		<div class="sub-heading">
			Cancellation & Refunds
		</div>
		@include('order.cancellation')
	</div>
	<div class="card">
		<div class="sub-heading">Terms of bookings</div>
		@include('order.booking_terms')
	</div>
</div>