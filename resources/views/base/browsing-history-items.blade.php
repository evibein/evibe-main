<!--Check whether current page is profile page or not -->
@if(session()->has('is_profile'))
	@php
		array_shift($data);
		session()->forget('is_profile');
	@endphp
@endif

<!--Display browsing history products-->
@php $i = 1; $count = count($data); @endphp

<!--  If History exists  -->
@if($count>0)
	<div class="clearfix"></div>
	@if (!($agent->isMobile()))    <!--  In Desktop -->
	<hr class="desktop-profile-hr no-mar-t" style="margin-left: -15px; margin-right: -15px">
	<div>
		<div class="col-sm-12 col-md-12 col-lg-12 row">
			<div class="col-sm-12 col-md-8 col-lg-8 browsing-history-products-title pad-b-15-imp text-left ">
				Recently Viewed
			</div>
			<div class="col-xs-4 col-md-4 col-lg-4  browsing-history-url pad-b-15-imp text-right">
				<a href="{{route('user.browsing.history.list.new')}}" target="_blank">View All</a>
			</div>
		</div>
		<div id="browsingHistoryCarousel" class="carousel slide" data-ride="carousel">
			<!-- Wrapper for slides -->
			<div class="carousel-inner pad-l-15-imp pad-r-15-imp" role="listbox">
				@foreach ($data as $product)
					@if($i % 6 == 1)
						<div class="item @if($i == 1) active @endif">
							@endif
							<div class="col-xs-2 col-sm-2 mar-b-15 no-pad-l item-wrap">
								<div class="browsing-history-product-img-wrap bg-light-gray">
									<a href="{{$product['link']}}?ref=browsing-history" target="_blank">
										<img class="browsing-history-product-img" src="{{ $product['img']}}">
									</a>
								</div>
								<div class="bh-product-title">
									<a href="{{$product['link']}}?ref=browsing-history" target="_blank">{{ $product['title'] }}</a>
								</div>
								<div class=" text-muted buy-now-btn text-center">
									<a href="{{$product['link']}}?ref=browsing-history#bookNowModal" class="buy-now-link" target="_blank">Buy Now</a>
								</div>

								<div class="bh-product-content">
									<div class="col-md-12 col-lg-12  col-md-12 col-lg-12 text-center">
										<div class="bh-product-price">
											<div class="bh-product-main-price">
												@if($product['priceworth'])
													<div class="bh-product-price-worth  in-blk">
														<span class="rupee-font">₹&nbsp;</span>
														<span class="displayWorth">{{$product['priceworth']}}</span>
													</div>
													&nbsp;&nbsp;&nbsp;
												@endif
												<span class="rupee-font">₹&nbsp;</span>{{$product['price']}}
												<span class="bh-product-price-kg"> {{$product['metric']}}</span>
											</div>

										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							@if($i % 6 == 0)
						</div>
					@endif
					@php $i++; @endphp
				@endforeach
				@if(count($data) % 6 != 0)
			</div>
			@endif
		</div>
		<a class="left carousel-control product-carousel-slider" href="#browsingHistoryCarousel" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left font-20-imp"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control product-carousel-slider" href="#browsingHistoryCarousel" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right font-20-imp"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
	@else <!--In Mobile-->
	<div>
		<div class="col-sm-12 col-md-12 col-lg-12 row pad-t-15 no-pad-l">
			<div class="col-xs-8 col-md-8 col-lg-8 browsing-history-products-title pad-b-15-imp text-left no-pad-l">
				Recently Viewed
			</div>

			<div class="col-xs-4 col-md-4 col-lg-4  browsing-history-url pad-b-15-imp text-right">
				<a href="{{route('user.browsing.history.list.new')}}">View All</a>
			</div>
		</div>

		<div id="browsingHistoryCarousel2" class="carousel slide" data-ride="carousel">
			<!-- Wrapper for slides -->
			<div class="carousel-inner pad-l-15" role="listbox">

				@foreach ($data as $product)
					@if($i % 2 == 1)
						<div class="item @if($i == 1) active @endif">
							@endif

							<div class="col-xs-6 col-sm-6 mar-b-15 no-pad-l">
								<div class="browsing-history-product-img-wrap bg-light-gray">
									<a href="{{$product['link']}}?ref=browsing-history">
										<img class="browsing-history-product-img" src="{{ $product['img']}}">
									</a>
								</div>
								<div class="bh-product-title">
									<a href="{{$product['link']}}?ref=browsing-history">{{ $product['title'] }}</a>
								</div>
								<div class="bh-product-content">
									<div class="  col-md-12 col-sm-12 col-xs-12 col-lg-12 text-center">
										<div class="bh-product-price">
											<div class="bh-product-main-price">
												<span class="rupee-font">₹</span>{{$product['price']}}
												<span class="bh-product-price-kg">  {{$product['metric']}}</span>
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							@if($i % 2 == 0)
						</div>
					@endif

					@php $i++; @endphp
				@endforeach
				@if(count($data) % 2 != 0)
			</div>
			@endif
		</div>
		<a class="left carousel-control product-carousel-slider" href="#browsingHistoryCarousel2" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left font-20-imp"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control product-carousel-slider" href="#browsingHistoryCarousel2" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right font-20-imp"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
	@endif
@endif