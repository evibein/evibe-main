@extends('layout.base')

@section('page-title')
	<title>{{ $data['seo']['pageTitle'] }} | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="{{ $data['seo']['pageDescription'] }}">
@endsection

@section('meta-keywords')
	<meta name="keywords" content="{{ $data['seo']['pageDescription'] }}">
@endsection

@section('og-title')
	<meta property="og:title" content="{{ $data['seo']['pageTitle'] }} | Evibe.in">
@endsection

@section('og-description')
	<meta property="og:description" content="{{ $data['seo']['pageDescription'] }}">
@endsection

@section('og-url')
	<meta property="og:url" content="https://evibe.in/{{ $cityUrl }}">
@endsection

@section('meta-canonical')
	<link rel="canonical" href="{{ $canonicalUrl }}/{{ $cityUrl }}">
@endsection

@section('seo:schema')
	@parent
	@if(isset($data['seo']['schema']) && count($data['seo']['schema']))
		@foreach($data['seo']['schema'] as $schema)
			{!! $schema !!}
		@endforeach
	@endif
@endsection

@section("header")
	@include('base.home.header.header-home-city')

	@if (isset($data['views']['header']) && view()->exists($data['views']['header']))
		@include($data['views']['header'])
	@endif
@endsection

@section('custom-css')
	<link rel="stylesheet" href="{{ elixir('css/af-home.css') }}">
	<link rel="stylesheet" href="{{ elixir('assets/css/home.css') }}">
	@if($agent->isMobile())
		<style>
			body {
				background-color: white !important;
			}
		</style>
	@endif
@endsection

@section("google-places-api")
@endsection

@section("content")
	<div class=" w-1366 base-home-bg-color">
		<div class="top-collections"></div>
		<div class=" section-why-us" style="background-color: #f8f8f8">
			<div class="cld-top-options-section" data-id="{{ config("evibe.occasion.surprises.id") }}">
				<div class="cld-top-options top-options"></div>
			</div>
			<div class="clearfix"></div>
			<div class="surprise-top-options-section " data-id="{{ config("evibe.occasion.surprises.id") }}">
				<div class="surprise-top-options top-options"></div>
				<div class="loading-div-outer mar-t-10 mar-b-20">
					<div class="inner-top-option-div">
						<h6 class="mar-t-20 no-mar-b in-blk pad-l-15 fw-normal font-18" style="text-transform: uppercase;">Trending Surprise Packages</h6>
						<hr style="margin-bottom: 0px;border:none;color:rgba(0,0,0,.1);background-color:rgba(0,0,0,.1);">
						<div class="text-center loading-item-wrap">
							@if(!($agent->isMobile()))
								@for($i=0;$i<6;$i++)
									<div class="mar-r-10 col-md-2 loading-item"></div>
								@endfor
							@else
								<div class="col-xs-8 mar-r-10 loading-item"></div>
								<div class="col-xs-4 mar-r-10 loading-item"></div>
							@endif
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class=" birthday-top-options-section" data-id="{{ config("evibe.occasion.kids_birthdays.id") }}">
				<div class="birthday-top-options top-options"></div>
				<div class="loading-div-outer mar-t-20 mar-b-10">
					<div class="inner-top-option-div">
						<h6 class="mar-t-20 no-mar-b in-blk pad-l-15 fw-normal font-18" style="text-transform: uppercase;">Trending Birthday Decors</h6>
						<hr style="margin-bottom: 0px;border:none;color:rgba(0,0,0,.1);background-color:rgba(0,0,0,.1);">
						<div class="text-center loading-item-wrap">
							@if(!($agent->isMobile()))
								@for($i=0;$i<6;$i++)
									<div class="mar-r-10 col-md-2 loading-item"></div>
								@endfor
							@else
								<div class="col-xs-8 mar-r-10 loading-item"></div>
								<div class="col-xs-4 mar-r-10 loading-item"></div>
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		@include("base.home.categories.birthday")
		@include("base.home.categories.cld")
		@include("base.home.categories.surprises")
		@include("base.home.categories.occasion")
		@include("base.home.categories.house-warming")
		@if($agent->isMobile() && !($agent->isTablet()))
			@include("base.home.home-usp")
		@endif
		@include("base.home.enquiry-form")
		<div class="clearfix"></div>
		<div class="g-reviews">
			@if(isset($data['reviews']))
				@include("base.home.g-reviews",['reviews' => $data['reviews']])
			@endif
		</div>
		<div class="blog-posts hide"></div>
	</div>
@endsection

@section("footer")
	<div class="footer-wrap">
		@if (isset($data['views']['footer']) && view()->exists($data['views']['footer']))
			@include($data['views']['footer'])
		@endif

		@include('base.home.footer.footer-common')
	</div>
@endsection

@section('js-framework')
	<script src="{{ elixir('js/af-home.js') }}"></script>
@endsection

@section("javascript")
	<script type="text/javascript">
		$(document).ready(function () {

			$(window).load(function () {
				var cityUrl = $("#sessionCityUrl").val();

				asyncAjaxRequest("/ajax/home/benefits", ".why-us-widget");
				asyncAjaxRequest("/ajax/home/" + cityUrl + "/top-collections", ".top-collections");

				asyncAjaxRequest("/ajax/home/" + cityUrl + "/evibe-media", ".evibe-media");
				asyncAjaxRequest("/ajax/home/" + $(".birthday-top-options-section").data("id") + "/top-options", ".birthday-top-options");
				asyncAjaxRequest("/ajax/home/" + $(".surprise-top-options-section").data("id") + "/top-options", ".surprise-top-options");
				asyncAjaxRequest("/ajax/home/" + $(".cld-top-options-section").data("id") + "/top-options?ref=cld", ".cld-top-options");
				/*asyncAjaxRequest("/ajax/home/" + $(".prepost-top-options-section").data("id") + "/top-options", ".prepost-top-options");*/
			});

			function asyncAjaxRequest(url, className) {
				$.ajax({
					url: url,
					type: "POST",
					async: true,
					tryCount: 0,
					retryLimit: 3,
					data: {},
					success: function (data) {
						if (typeof data !== undefined && data) {
							$(className).parent().find(".inner-top-option-div").parent().hide();
							$(className).append(data).removeClass("hide");
							$(className).parent().removeClass("hide");
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						this.tryCount++;
						if (this.tryCount <= this.retryLimit) {
							$.ajax(this);
						} else {
							window.notifyTeam({
								"url": url,
								"textStatus": textStatus,
								"errorThrown": errorThrown
							});
						}
					}
				});
			}

			function callPlayer(frame_id, func, args) {
				if (window.jQuery && frame_id instanceof jQuery) frame_id = frame_id.get(0).id;
				var iframe = document.getElementById(frame_id);
				if (iframe && iframe.tagName.toUpperCase() != 'IFRAME') {
					iframe = iframe.getElementsByTagName('iframe')[0];
				}

				/* When the player is not ready yet, add the event to a queue
				/* Each frame_id is associated with an own queue.
				/* Each queue has three possible states:
				/*  undefined = uninitialised / array = queue / 0 = ready */
				if (!callPlayer.queue) callPlayer.queue = {};
				var queue = callPlayer.queue[frame_id],
					domReady = document.readyState == 'complete';

				if (domReady && !iframe) {
					/* DOM is ready and iframe does not exist. Log a message */
					window.console && console.log('callPlayer: Frame not found; id=' + frame_id);
					if (queue) clearInterval(queue.poller);
				} else if (func === 'listening') {
					/* Sending the "listener" message to the frame, to request status updates */
					if (iframe && iframe.contentWindow) {
						func = '{"event":"listening","id":' + JSON.stringify('' + frame_id) + '}';
						iframe.contentWindow.postMessage(func, '*');
					}
				} else if (!domReady ||
					iframe && (!iframe.contentWindow || queue && !queue.ready) ||
					(!queue || !queue.ready) && typeof func === 'function') {
					if (!queue) queue = callPlayer.queue[frame_id] = [];
					queue.push([func, args]);
					if (!('poller' in queue)) {
						/* keep polling until the document and frame is ready */
						queue.poller = setInterval(function () {
							callPlayer(frame_id, 'listening');
						}, 250);
						/* Add a global "message" event listener, to catch status updates: */
						messageEvent(1, function runOnceReady(e) {
							if (!iframe) {
								iframe = document.getElementById(frame_id);
								if (!iframe) return;
								if (iframe.tagName.toUpperCase() != 'IFRAME') {
									iframe = iframe.getElementsByTagName('iframe')[0];
									if (!iframe) return;
								}
							}
							if (e.source === iframe.contentWindow) {
								clearInterval(queue.poller);
								queue.ready = true;
								messageEvent(0, runOnceReady);
								/* .. and release the queue: */
								while (tmp = queue.shift()) {
									callPlayer(frame_id, tmp[0], tmp[1]);
								}
							}
						}, false);
					}
				} else if (iframe && iframe.contentWindow) {
					if (func.call) return func();
					/* Frame exists, send message */
					iframe.contentWindow.postMessage(JSON.stringify({
						"event": "command",
						"func": func,
						"args": args || [],
						"id": frame_id
					}), "*");
				}

				/*! * IE8 does not support addEventListener... */
				function messageEvent(add, listener) {
					var w3 = add ? window.addEventListener : window.removeEventListener;
					w3 ?
						w3('message', listener, !1)
						:
						(add ? window.attachEvent : window.detachEvent)('onmessage', listener);
				}
			}

			$('.customer-test').on('slide.bs.carousel', function () {
				var iframeID = $(this).find('.active').find('iframe').attr("id");
				callPlayer(iframeID, "stopVideo");
			});

			$('#header-carousel-wrap').hover(function () {
				$(this).carousel('pause')
			}, function () {
				$(this).carousel('cycle')
			});

			(function loadInvitePreviewCarousel() {
				$.getScript("/js/util/bootstrapCarousalSwipe.js", function () {
					$('#header-carousel-wrap').carousel({
						interval: 5000,
						pause: true
					}).bcSwipe({threshold: 50});
				});
			})();
			(function getBlogPosts() {
				$.ajax({
					url: "/ajax/home/posts",
					type: "POST",
					tryCount: 0,
					retryLimit: 1,
					data: {},
					success: function (data) {
						$(".blog-posts").removeClass("hide").append(data);
						$("#blog-posts").addClass("pad-t-15");
					}
				});
			})();

		});
	</script>
@endsection