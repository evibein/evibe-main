<ul class="share-buttons">
	<li>
		<a href="{{ $data['shareUrl']['facebook'] }}" title="Share on Facebook" class="popup">
			<img src="{{ $galleryUrl }}/main/img/icons/Facebook.png"></a>
	</li>
	<li>
		<a href="{{ $data['shareUrl']['twitter'] }} " target="_blank" title="Tweet" class="popup" rel="noopener">
			<img src="{{ $galleryUrl }}/main/img/icons/Twitter.png"></a>
	</li>
	<li>
		<a href="{{ $data['shareUrl']['pinterest'] }}" title="Pin it" class="popup" rel="noopener">
			<img src="{{ $galleryUrl }}/main/img/icons/Pinterest.png"></a>
	</li>
</ul>