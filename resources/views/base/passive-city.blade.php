@extends('layout.base')

@section('page-title')
	<title>{{ $data['seo']['pageTitle'] }} | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="{{ $data['seo']['pageDescription'] }}">
@endsection

@section('meta-keywords')
	<meta name="keywords" content="{{ $data['seo']['pageDescription'] }}">
@endsection

@section('og-title')
	<meta property="og:title" content="{{ $data['seo']['pageTitle'] }} | Evibe.in">
@endsection

@section('og-description')
	<meta property="og:description" content="{{ $data['seo']['pageDescription'] }}">
@endsection

@section('og-url')
	<meta property="og:url" content="https://evibe.in/{{ $data['passiveCityUrl'] }}">
@endsection

@section('meta-canonical')
	<link rel="canonical" href="{{ $canonicalUrl }}/{{ $data['passiveCityUrl'] }}">
@endsection

@section('custom-css')
	<link rel="stylesheet" href="{{ elixir('css/af-home.css') }}">
	<link rel="stylesheet" href="{{ elixir('assets/css/home.css') }}">
	<link rel="stylesheet" href="{{ elixir('/css/page-about-us.css') }}"/>
@endsection

@section("google-places-api")
@endsection

@section('seo:schema')
	@parent
	@if(isset($data['seo']['schema']) && count($data['seo']['schema']))
		@foreach($data['seo']['schema'] as $schema)
			{!! $schema !!}
		@endforeach
	@endif
@endsection

@section("header")
	@include('base.home.header.header-home-city')
	@include("base.home.header.header-carousel", ["passiveCity" => 'true'])
@endsection

@section("content")
	<div class="w-1366 base-home-bg-color">
		<section class="about-sec abt-what-headline no-mar-t">
			<div class="abt-w-wrap text-center">
				<h2 class="abt-w-h1">Make Your Special Day More Memorable!</h2>
				<h4 class="abt-w-h2">10,000+ customers trust Evibe.in to make their event planning a breeze. Book party services ranging from venues to cakes, complete hassle-free at best prices and within three simple steps.</h4>
			</div>
		</section>
		<section class="about-sec abt-stats" style="background-color: #777">
			<div class="col-lg-10 col-md-10 col-lg-offset-1 col-md-offset-1 col-xs-offset-2 abt-stats-pad">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="abt-stat-item-wrap media">
						<div class="media-left media-middle">
							<img class="abt-stats-img" src="{{ $galleryUrl }}/main/pages/about/happy.png" alt="fun"/>
						</div>
						<div class="media-body abt-stats-text-l">
							<p class="media-heading">4,00,000+</p>
							<h6>HAPPY GUESTS</h6>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="abt-stat-item-wrap media">
						<div class="media-left media-middle">
							<img class="abt-stats-img" src="{{ $galleryUrl }}/main/pages/about/options.png" alt="fun"/>
						</div>
						<div class="media-body abt-stats-text-l">
							<p class="media-heading">5,000+</p>
							<h6>OPTIONS</h6>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="abt-stat-item-wrap media">
						<div class="media-left media-middle">
							<img class="abt-stats-img" src="{{ $galleryUrl }}/main/pages/about/partners.png" alt="fun"/>
						</div>
						<div class="media-body abt-stats-text-l">
							<p class="media-heading">500+</p>
							<h6>VERIFIED PARTNERS</h6>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="abt-stat-item-wrap media">
						<div class="media-left media-middle">
							<img class="abt-stats-img" src="{{ $galleryUrl }}/main/pages/about/followers.png" alt="fun"/>
						</div>
						<div class="media-body abt-stats-text-l">
							<p class="media-heading">1,50,000+</p>
							<h6>FOLLOWERS</h6>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</section>
		<div class="customer-reviews" style="padding-top: 55px"></div>
		<div class="blog-posts "></div>
		<section class="section-press">
			<div class="container padding-30">
				<h4 class="sec-title title-text text-center">Evibe<span class="text-lower">.in</span> in media</h4>
				<div class="row press text-center">
					<div class="col-md-4 col-sm-12 col-xs-12 press-focus">
						<img src="{{ $galleryUrl }}/main/img/icons/press/logo-toi2.png" alt="Evibe on Times of India">
						<p align="justify">
							"A platform for event services to organise a party"
						</p>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 press-focus">
						<img src="{{ $galleryUrl }}/main/img/icons/press/logo-indian-express.png" alt="Evibe on The New Indian Express">
						<p align="justify">
							"The Great Indian Party Planners"
						</p>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 press-focus">
						<img src="{{ $galleryUrl }}/main/img/icons/press/logo-bonnevie.png" alt="Evibe on Bonnevie News">
						<p align="justify">
							"Now Planning A Party Is Just An Evibe Away!"
						</p>
					</div>

					<ul class="press-logo top-30">
						<li>
							<img src="{{ $galleryUrl }}/main/img/icons/press/logo-yourstory.png" class="img-responsive img-centered" alt="Evibe on YourStory">
						</li>
						<li>
							<img src="{{ $galleryUrl }}/main/img/icons/press/logo-vccircle.png" class="img-responsive img-centered" style="opacity:0.8; height: 25px; margin-top: 10px;" alt="Evibe on VCCircle">
						</li>
						<li>
							<img src="{{ $galleryUrl }}/main/img/icons/press/logo-inc42.png" class="img-responsive img-centered" style="opacity:0.8; height: 30px; margin-top: 10px;" alt="Evibe on Inc42">
						</li>
						<li>
							<img src="{{ $galleryUrl }}/main/img/icons/press/logo-medianama.png" class="img-responsive img-centered" style="opacity:0.8; height: 25px; margin-top: 12px;" alt="Evibe on Medianama.com">
						</li>
						<li>
							<img src="{{ $galleryUrl }}/main/img/icons/press/logo-tia.png" class="img-responsive img-centered" style="opacity:0.8; height:30px; margin-top: 10px;" alt="Evibe on TechInAsia.com">
						</li>
					</ul>
				</div>
			</div>
		</section>
		@include("base.home.enquiry-form", ["passiveCity" => $data['passiveCityUrl']])
	</div>
@endsection

@section("footer")
	<div class="footer-wrap">
		@if (isset($data['views']['footer']) && view()->exists($data['views']['footer']))
			@include($data['views']['footer'])
		@endif
		@include('base.home.footer.footer-common')
	</div>
@endsection

@section('js-framework')
	<script src="{{ elixir('js/af-home.js') }}"></script>
@endsection

@section("javascript")
	<script type="text/javascript">
		$(document).ready(function () {

			$("a:not('.enable-link')").attr('disabled', 'disabled').css("cursor", "not-allowed").attr("href", "#");

			$('#header-carousel-wrap').hover(function () {
				$(this).carousel('pause')
			}, function () {
				$(this).carousel('cycle')
			});

			(function loadInvitePreviewCarousel() {
				$.getScript("/js/util/bootstrapCarousalSwipe.js", function () {
					$('#header-carousel-wrap').carousel({
						interval: 5000,
						pause: true
					}).bcSwipe({threshold: 50});
				});
			})();

			(function getBlogPosts() {
				$.ajax({
					url: "/ajax/home/posts",
					type: "POST",
					tryCount: 0,
					retryLimit: 1,
					data: {},
					success: function (data) {
						$(".blog-posts").removeClass("hide").append(data);
						$("#blog-posts").addClass("pad-t-15");
					}
				});
			})();

		});
	</script>
@endsection
