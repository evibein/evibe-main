<div class="bg-white desktop-list-page col-sm-10 col-md-8 col-lg-8 row col-md-offset-2 mar-b-30">
	<h1 class="fw-400 pad-l-15 page-heading no-mar-b">Your Recently Browsed Items</h1>
	<div class="list-content-wrap">
		<div class="list-options-wrap pad-t-15">
			<div class="col-xs-12 col-sm-12 no-pad-r">
				@php $count = 1; @endphp
				@foreach ($data as $card)
					<div class="col-xs-6 col-sm-6 no-pad-l pad-b-20">
						<div class="list-option-card">
							<div class="list-option-image-wrap text-left" style="height:90px">
								<a class="list-option-link option-profile-image option-profile-image-{{ $card['id'] }}" data-option-id="{{ $card['id'] }}" href="{{ $card['link'] }}?ref=bh-img">
									<img src="{{$card['img']}}" style="height: 100%;">
								</a>
							</div>
							<div class="list-option-content-wrap">
								<div class="list-option-title">
									<a class="list-option-link" href="{{ $card['link'] }}">
										{{ $card['title'] }}
									</a>
								</div>
								<div class="list-option-price">
									<div class="list-main-price">
										@if($card['priceworth'])
											<div class=" in-blk" style="font-size: 11px;text-decoration: line-through;">
												<span class="rupee-font">₹&nbsp;</span>
												<span class="displayWorth">{{$card['priceworth']}}</span>
											</div>
											&nbsp;&nbsp;&nbsp;
										@endif
										@price($card['price'])
										<span class="mar-l-4 list-price-kg">{{$card['metric']}}</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					@if(($count % 2) == 0)
						<div class="clearfix"></div> @endif

					@php $count++; @endphp
				@endforeach
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>