<div class="bg-white desktop-list-page container mar-b-20">
	<h1 class=" fw-400 page-heading pad-l-15">Your Recently Browsed Items</h1>
	@foreach($data as $card)
		<div class="col-md-3 col-lg-3 col-sm-3 col-xs-6 no-pad-r pad-b-20 ">
			<div class="des-list-card-wrap">
				<div class="list-option-card des-list-option-card">
					<div class="list-option-image-wrap text-left">
						<a class="list-option-link option-profile-image-{{ $card['id'] }}" data-option-id="{{ $card['id'] }}" href="{{ $card['link'] }}?ref=bh-img">
							<img src="{{$card['img']}}" class="list-option-image initial loaded">
						</a>
					</div>
					<div class="list-option-content-wrap text-center">
						<div class="list-option-title">
							<a class="list-option-link" href="{{$card['link']}}?ref=bh-title">
								{{ $card['title'] }}
							</a>
						</div>
						<div class="list-option-cta-wrap text-center">
							<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-pad">
								<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no-pad">
									<a class="item-shortlist" v-on:click="shortlistOptionFromListPage">
										<div class="hideShortlistResultsData list-cta-btn list-btn-shortlist" data-mapId="{{ $card['id'] }}" data-mapTypeId="{{ $card['ptid'] }}" data-cityId="{{getCityId()}}" data-urladd="{{ route('partybag.add') }}" data-urldelete="{{ route('partybag.delete') }}">
											Add To Cart
										</div>
									</a>
								</div>
								<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no-pad">
									<a href="{{$card['link']}}?ref=book-now#bookNowModal" class="item-book-now" target="_blank">
										<div class="list-cta-btn list-btn-book-now">
											Book Now
										</div>
									</a>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="list-option-price">
							<div class="list-main-price in-blk">
								@if($card['priceworth'])
									<div class=" in-blk" style="font-size: 11px;text-decoration: line-through;">
										<span class="rupee-font">₹&nbsp;</span>
										<span class="displayWorth">{{$card['priceworth']}}</span>
									</div>
									&nbsp;&nbsp;&nbsp;
								@endif
								@price($card['price'])
								<span class="mar-l-4 list-price-kg">{{$card['metric']}}</span>
							</div>
							<div class="list-price-tag hide in-blk">
								<i class="glyphicon glyphicon-thumbs-up"></i> Best Price
							</div>
							<div class="clearfix"></div>
						</div>

					</div>
				</div>
			</div>
		</div>
	@endforeach
</div>