@if ($agent->isMobile() && !($agent->isTablet()))
	<div class="text-center">
		<h4 class="sec-title title-text text-center text-col-gr top-0"> Best Service Promised</h4>
		<div class="in-blk" style="padding: 0 5%;">
			<img src="{{ $galleryUrl }}/img/icons/quality.png" style="border-radius: 50%; height: 60px;" alt="quality">
			<div class="mar-t-10">1,50,000+ Customers</div>
		</div>
		<div class="in-blk" style="padding: 0 5%;">
			<img src="{{ $galleryUrl }}/img/icons/deals.png" style="border-radius: 50%; height: 60px;" alt="best deals">
			<div class="mar-t-10">Best Prices & Packages</div>
		</div>
	</div>
@else
	<div class="text-center">
		<h6> Best Service Promised</h6>
		<div class="col-sm-6" >
			<img src="{{ $galleryUrl }}/img/icons/quality.png" style="border-radius: 50%; height: 60px; box-shadow: #4040401f 0 6px 12px;" alt="quality">
			<div class="mar-t-10">1,50,000+ Customers</div>
		</div>
		<div class="col-sm-6" >
			<img src="{{ $galleryUrl }}/img/icons/deals.png" style="border-radius: 50%; height: 60px; box-shadow: #4040401f 0 6px 12px;" alt="best deals">
			<div class="mar-t-10">Best Prices & Packages</div>
		</div>
	</div>
@endif