@php $cityUrl = (is_null(getCityUrl())) ? 'bangalore' : getCityUrl(); @endphp

@if ($agent->isMobile() || ($agent->isTablet()))
	<div class="mar-l-10 mar-r-10 bg-white-imp mar-t-15 text-center hide" style=" border-radius: 5px;">
		<h6 class="pad-t-15 no-mar-b no-mar-t fw-500">PREMIUM DIGITAL CARDS</h6>
		<p class="text-muted no-mar-b" style="letter-spacing: 0.8px;">Create the perfect card for your occasion</p>
		<div class="mar-t-15">
			<a class="in-blk mar-r-15 text-e" href="{{ route("e-cards.invites") }}?ref=home">Invites ></a>
			<a class="in-blk mar-r-15 text-e" href="{{ route("e-cards.wish-cards") }}?ref=home">Wish Cards ></a>
			<a class="in-blk text-e" href="{{ route("e-cards.thank-you-cards") }}?ref=home">Thank You Cards ></a>
		</div>
		<a href="{{ route("e-cards.invites") }}?ref=home-img">
			<img class="full-width" src="{{ $galleryUrl }}/main/img/home/invite-mobile-promo.png" alt="e-cards">
		</a>
	</div>
@else
	<div class="text-center w-1366  no-pad">
		<div style="background-color: #fff;margin-top: -30px" class="pad-b-20 pad-t-20">
			<div class="in-blk home-icon-wrap">
				<a href="{{ route('city.occasion.birthdays.home', $cityUrl) }}?ref=home" class="a-no-decoration-black update-opacity">
					<img class="home-cat-img" src="{{ $galleryUrl }}/main/img/home/v1/icons/birthday.png" alt="Birthday">
					<h6 class="home-icon-title mar-t-10  no-mar-b">Birthday</h6>
				</a>
			</div>
			<div class="in-blk home-icon-wrap">
				<a href="{{ route("city.cld.list", $cityUrl) }}?ref=home" class="a-no-decoration-black update-opacity">
					<img class="home-cat-img" src="{{ $galleryUrl }}/main/img/home/v1/icons/cld.png" alt="Candle light dinner">
					<h6 class="home-icon-title mar-t-10 no-mar-b">Candle Light Dinner</h6>
				</a>
			</div>
			<div class="in-blk home-icon-wrap">
				<a href="{{ config('evibe.host') . '/' .$cityUrl }}/h/couple-surprises?ref=home" class="a-no-decoration-black update-opacity">
					<img class="home-cat-img" src="{{ $galleryUrl }}/main/img/home/v1/icons/surprise.png" alt="Surprises">
					<h6 class="home-icon-title mar-t-10 no-mar-b">Surprises</h6>
				</a>
			</div>
			<div class="in-blk home-icon-wrap">
				<a href="{{ route('city.occasion.house-warming.home', $cityUrl) }}?ref=home" class="a-no-decoration-black update-opacity">
					<img class="home-cat-img" src="{{ $galleryUrl }}/main/img/home/v1/icons/house_warming.png" alt="House warming">
					<h6 class="home-icon-title mar-t-10 no-mar-b">House Warming</h6>
				</a>
			</div>
			<div class="in-blk home-icon-wrap">
				<a href="{{ route('city.occasion.ncdecors.list', $cityUrl) }}?ref=home" class="a-no-decoration-black update-opacity">
					<img class="home-cat-img" src="{{ $galleryUrl }}/main/img/home/v1/icons/naming-cermony.png" alt="Naming Ceremony">
					<h6 class="home-icon-title mar-t-10 no-mar-b">Naming Ceremony</h6>
				</a>
			</div>
			<div class="in-blk home-icon-wrap">
				<a href="{{ route('city.occasion.bsdecors.list', $cityUrl) }}?ref=home" class="a-no-decoration-black update-opacity">
					<img class="home-cat-img" src="{{ $galleryUrl }}/main/img/home/v1/icons/baby-shower.png" alt="Baby Shower">
					<h6 class="home-icon-title mar-t-10 no-mar-b">Baby Shower</h6>
				</a>
			</div>
			<div class="in-blk home-icon-wrap hide">
				<a href="{{ route('store-redirect', ['redirect' => config('evibe.store_host').'?utm_source=CityCategory&utm_medium=Website&utm_campaign=StorePromotion']) }}" class="a-no-decoration-black update-opacity">
					<img class="home-cat-img" src="{{ $galleryUrl }}/main/pages/occasion-home/shop-icon.png" alt="Party Props">
					<h6 class="home-icon-title mar-t-10 no-mar-b">Party Props</h6>
				</a>
			</div>
		</div>
	</div>
@endif