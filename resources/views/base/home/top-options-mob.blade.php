@php $carouselCount = 1; @endphp
@if(isset($data["categories"]))
	@foreach($data["categories"] as $key => $category)
		@if(count($category["options"]) > 2)
			@php $carouselCount++; $carouselOptionCount = 1; @endphp
			<div style="padding-top:15px">
				<div style="background-color: #fff;margin-left: 10px;margin-right: 10px;border-radius: 5px;">
					<div>
						<div class="col-xs-8">
							<h6 class="mar-t-20 no-mar-b in-blk fw-500" style="font-weight: 500">{{ $category["name"] }}</h6>
						</div>
						<div class="col-xs-4">
						<span class=" floating-evibe-btn-wrap pull-right mar-t-20">
						<a class="text-muted floating-evibe-btn  pad-t-5 pad-b-5 pad-l-10 pad-r-10" href="{{ $category["url"] }}?utm_source=masterHome&utm_campaign=top-options&utm_medium=website&utm_term={{ "seealloptions+" . str_replace(' ', '', $category["name"]) }}">View All</a>
					</span>
						</div>
						<div class="clearfix"></div>
						<hr style="margin-bottom: 0px; border:none; color:rgba(0,0,0,.1); background-color:rgba(0,0,0,.1)">
					</div>
					<div class="owl-carousel owl-theme">
						@if($key == config("evibe.ticket.type.decor"))
							@foreach($category["options"] as $option)
								<div class="item mar-r-20">
									@include("base.home.top-options.decors")
								</div>
								@php $carouselOptionCount++; @endphp
							@endforeach
						@elseif($key == config("evibe.ticket.type.cake"))
							@foreach($category["options"] as $cake)
								<div class="item mar-r-20">
									@include("base.home.top-options.cakes")
								</div>
								@php $carouselOptionCount++; @endphp
							@endforeach
						@elseif($key == config("evibe.ticket.type.package"))
							@foreach($category["options"] as $package)
								<div class="item mar-r-20">
									@include("base.home.top-options.package")
								</div>
								@php $carouselOptionCount++; @endphp
							@endforeach
						@elseif($key == config("evibe.ticket.type.service"))
							@foreach($category["options"] as $service)
								<div class="item mar-r-20">
									@include("base.home.top-options.services")
								</div>
								@php $carouselOptionCount++; @endphp
							@endforeach
						@elseif($key == config("evibe.ticket.type.venue-deals"))
							@foreach($category["options"] as $venueDeal)
								@include("base.home.top-options.venue-deals")
							@endforeach
						@elseif($key == config("evibe.ticket.type.trend"))
							@foreach($category["options"] as $trend)
								@include("base.home.top-options.trends")
							@endforeach
						@elseif($key == config("evibe.ticket.type.surprises") || $key == "cld")
							@foreach($category["options"] as $package)
								<div class="item mar-r-20">
									@include("base.home.top-options.surprises-packages")
								</div>
								@php $carouselOptionCount++; @endphp
							@endforeach
						@endif
					</div>
				</div>
			</div>
		@endif
	@endforeach
@endif

<script type="text/javascript">
	$('.owl-carousel').owlCarousel({
		stagePadding: 15,/*the little visible images at the end of the carousel*/
		dots: false,
		rtl: false,
		loop: true,
		margin: 0,
		nav: false,
		lazyload: true,
		responsive: {
			0: {
				items: 1.5
			},
		}
	});
</script>
