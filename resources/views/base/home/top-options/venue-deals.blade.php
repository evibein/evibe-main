<div class=" mar-b-15 trending">
	<a href="{{ route('city.occasion.birthdays.venue_deals.profile', [getCityUrl(), $venueDeal->url]) }}?ref=top-image">
		<div class="img-container" style="height: 150px; background-color: #fff;overflow: hidden">
			<img src="{{ $venueDeal->getProfileImg() }}" title="Birthday party venue -  {{ $venueDeal->name }}">
			@include('app.shortlist_results', [
									"mapId" => $venueDeal->id,
									"mapTypeId" => config('evibe.ticket.type.venue-deals'),
									"occasionId" => getEventIdFromSession(),
									"cityId" => getCityId()
								])
			@if($venueDeal->getDiscountPercentage() > 0)
				<div class="discount">
					<div class="square">{{$venueDeal->getDiscountPercentage()}} % off</div>
					<!---Don't remove these element, these are being used in css to generate discount label;-->
					<div class="traingle"></div>
				</div>
			@endif
		</div>
		<h6 class=" top-categories-title" > @truncateName($venueDeal->name)
			<br>
			<span>
				@price($venueDeal->price)
				<span class="star">*</span>
				@if ($venueDeal->price && $venueDeal->price_max > $venueDeal->price)
					- &nbsp; @price($venueDeal->price_max) <span class="star">*</span>
				@endif
			</span>
		</h6>
	</a>
</div>