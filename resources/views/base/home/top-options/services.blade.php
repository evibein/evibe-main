<div class=" mar-t-15 trending">
	<a href="{{ route('city.occasion.birthdays.ent.profile', [getCityUrl(), $service->url]) }}?utm_source=masterHome&utm_campaign=top-options&utm_medium=website&utm_term={{ $service->url }}">
		<div class="img-container" style="height: 150px;background-color: #fff;overflow: hidden; ">
			<img src="{{ $service->getProfilePic() }}" title="Birthday party service - {{ $service->name }}" style="display: inline-block;">
			@include('app.shortlist_results', [
									"mapId" => $service->id,
									"mapTypeId" => config('evibe.ticket.type.entertainment'),
									"occasionId" => getEventIdFromSession(),
									"cityId" => getCityId()
								])
		</div>
		<h6 class=" no-mar-b no-pad-b mar-t-5 top-categories-title"  >
			<a class="a-no-decoration-dark-black text-normal" href="{{ route('city.occasion.birthdays.ent.profile', [getCityUrl(), $service->url]) }}?utm_source=masterHome&utm_campaign=top-options&utm_medium=website&utm_term={{ $service->url }}">
				@truncateName($service->name)
			</a>
		</h6>
		<p class="text-center no-mar-b pad-b-15 pad-t-5" style="line-height: 15px">
				@price($service->min_price)*
		</p>
	</a>
</div>