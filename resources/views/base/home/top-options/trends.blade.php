<div class=" mar-b-15 trending">
	<a href="{{ route('city.occasion.birthdays.trends.profile', [getCityUrl(), $trend->url]) }}?ref=top-view">
		<div class="img-container" style="height: 150px;background-color: #fff;overflow: hidden ">
			<img src="{{ $trend->getProfileImg() }}" title="{{ $trend->name }}" alt="Birthday party trend - {{ $trend->name }}" style="display: inline-block;">
			@include('app.shortlist_results', [
									"mapId" => $trend->id,
									"mapTypeId" => config('evibe.ticket.type.trend'),
									"occasionId" => getEventIdFromSession(),
									"cityId" => getCityId()
								])
		</div>
		<h6 class=" no-mar-b no-pad-b mar-t-5 top-categories-title">
			<a class="a-no-decoration-dark-black text-normal" href="{{ route('city.occasion.surprises.package.profile', [getCityUrl(), $package->url]) }}?utm_source=masterHome&utm_campaign=top-options&utm_medium=website&utm_term={{ $package->url }}">
				{{ $trend->name }}</a>
		</h6>
		<p class="text-center no-mar-b pad-b-15" style="line-height: 15px">
			@price($trend->price)*
		</p>
	</a>
</div>