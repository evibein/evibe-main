<div class="mar-t-15 trending">
	<a href="{{ route('city.occasion.birthdays.cakes.profile', [getCityUrl(), $cake->url]) }}?utm_source=masterHome&utm_campaign=top-options&utm_medium=website&utm_term={{ $cake->url }}">
		<div class="img-container pos-rel" style="height: 150px; background-color: #fff;overflow: hidden;">
			<img src="{{ $cake->getProfileImg() }}" alt="Birthday party cake - {{ $cake->title }}" style="width:auto; z-index:3;position:absolute;left:50%;transform:translate(-50%)">
			<img src="{{ $cake->getProfileImg() }}" alt="Birthday party cake - {{ $cake->title }}" style="width:100%; z-index:2;position:absolute;filter:blur(5px);left:0;height:auto">
		</div>
		<h6 class=" no-mar-b no-pad-b mar-t-5 top-categories-title">
			<a class="a-no-decoration-dark-black text-normal" href="{{ route('city.occasion.birthdays.cakes.profile', [getCityUrl(), $cake->url]) }}?utm_source=masterHome&utm_campaign=top-options&utm_medium=website&utm_term={{ $cake->url }}">@truncateName($cake->title)</a>
		</h6>
		<p class="text-center no-mar-b pad-b-15 pad-t-5" style="line-height: 15px">
			@price($cake->price)*
		</p>
	</a>
</div>