<hr class="des-why-us-list-hr">
<div class="clearfix"></div>
<div class="section-why-us-list">
	<div class="container">
		<h4 class="sec-title text-center title-text">Evibe<span class="text-lower">.in</span> Brand Promise</h4>
		<div class="row">
			<div class="benefits-lists pad-b-30 pad	">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-center benefit">
					<img src="{{ config('evibe.gallery.host') }}/main/img/icons/guarantee.png" alt="Trusted by customers">
					<div class="text-center">
						<div class="ben-title title-text">
							Trusted by 1,50,000+ Customers
						</div>
						<div class="ben-desc">
							Your event is in safe hands. 1000s of customers book with us for our quality service & support.
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-center benefit">
					<img src="{{ config('evibe.gallery.host') }}/main/img/icons/easy-booking.png" alt="Easy Booking on Evibe">
					<div class="text-center">
						<div class="ben-title title-text">
							Stress-Free Party Planning
						</div>
						<div class="ben-desc">
							Get peace of mind. Book everything at one place with assurance of professional service.
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-center benefit">
					<img src="{{ config('evibe.gallery.host') }}/main/img/icons/real_photos_reviews.png" alt="Real photos & reviews">
					<div class="text-center">
						<div class="ben-title title-text">
							Real Pictures & Reviews
						</div>
						<div class="ben-desc">
							It’s now easy to select perfect options. See 5,500+ real pictures & customer reviews.
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-center benefit">
					<img src="{{ config('evibe.gallery.host')}}/main/img/icons/best_deals.png" alt="Best prices & packages">
					<div class="text-center">
						<div class="ben-title title-text">
							Best Prices & Packages
						</div>
						<div class="ben-desc">
							Get best value for money. Our prices are pre-negotiated and best in market!
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>