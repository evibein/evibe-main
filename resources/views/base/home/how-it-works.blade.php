<div class="section-how-it-works">
	<div class="container">
		<div class="col-md-12 col-xs-12 padding-75">
			<h4 class="sec-title title-text text-center top-0 mar-b-10">How it Works ?</h4>
			<p class="text-center font-16 text-e">Evibe.in, a better way to plan your party.</p>
			<div class="benefits-lists top-30 text-col-gr box-shadow">
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 benefit steps">
					<div class="numbers hidden-sm hidden-xs">1</div>
					<div class="col-md-2">
						<img src="{{ $galleryUrl }}/main/img/icons/search_b.png" alt="Total birthday party venues - party halls, play zones, restaurants and resorts">
					</div>
					<div class="col-md-10">
						<div class="ben-title">Browse</div>
						<div class="ben-desc">
							Checkout our 1,000+ wide range of offerings with best deals from places to
							cakes with accurate pricing, details, real pictures and reviews.
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 benefit steps" style="background:#383838;">
					<div class="numbers hidden-sm hidden-xs">2</div>
					<div class="arrow-right"></div>
					<div class="col-md-2">
						<img src="{{ $galleryUrl }}/main/img/icons/check.png" alt="Finalize">
					</div>
					<div class="col-md-10">
						<div class="ben-title">Finalize</div>
						<div class="ben-desc">Our expert party planning team will help you with any customisations /
							clarifications you may have to finalize your order.
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 benefit steps">
					<div class="numbers hidden-sm hidden-xs">3</div>
					<div class="arrow-right arrow-black"></div>
					<div class="col-md-2">
						<img src="{{ $galleryUrl }}/main/img/icons/book_b.png" alt="Total happy birthday party hosts">
					</div>
					<div class="col-md-10">
						<div class="ben-title">Book</div>
						<div class="ben-desc">Block your slot by securely paying an advance of {{config('evibe.ticket.advance.percentage')}}% of booking amount. Sit back
							relax and enjoy your party as rest is taken care of.
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>

</div>