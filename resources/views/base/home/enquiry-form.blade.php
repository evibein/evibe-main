@if($agent->isMobile()&&!$agent->isTablet())
	<section id="book" class="home-section booking-form-wrap base-home-bg-color no-pad-t no-pad-b mar-t-20 mar-b-20"> <!-- Booking form -->
		<div style="min-height: 650px">
			<div class="col-sm-12 col-md-12 text-center pad-t-20" style="background-image: url({{$galleryUrl}}/main/img/home/v1/form-bg.png);min-height:850px;">
				<h4 style="color:#fff">
					@if((!isset($formTitle)) && (!isset($passiveCity)))
						Tell us what you need.
					@elseif(isset($passiveCity))
						REQUEST EVIBE PRESENCE IN YOUR CITY
					@else
						{{ $formTitle }}
					@endif
				</h4>
				<h6 style="color:#fff">
					@if (!isset($formDescription))
						We will help you book the best party services based on your preferences with
						best deals.
					@else
						{{ $formDescription }}
					@endif
				</h6>
				<form class="homepage-booking-form form-card-shadow no-pad">
					<div>
						<div class="col-sm-12">
							<div class="text-muted low-font">*fields are mandatory.</div>
							<div class="errors-cnt alert-danger text-center hide"></div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-fields">
						<div class="pad-t-20">
							<div class="col-sm-12">
								<div class="form-group">
									<label for="enquiryFormName">Your Name*</label>
									<input id="enquiryFormName" name="enquiryFormName" type="text" class="form-control" placeholder="enter your full name" value="">
									@if(isset($passiveCity))
										<input id="passiveCity" name="passiveCity" type="hidden" class="form-control" value="{{ $data["passiveCityUrl"] }}">
									@else
										<input id="passiveCity" name="passiveCity" type="hidden" class="form-control" value="NA">
									@endif
								</div>
							</div>
							<div class="clearfix"></div>

						</div>
						<div class="pad-t-20">
							<div class="col-sm-12">
								<div class="form-group">
									<label for="enquiryFormPhone">Phone Number*</label>
									<div class="clearfix"></div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad">
										@if(isset($data['countries']) && count($data['countries']))
											<select id="enquiryFormCallingCode" name="enquiryFormCallingCode" class="form-control country-calling-code">
												@foreach($data['countries'] as $country)
													<option value="{{ $country['calling_code'] }}">{{ $country['calling_code'] }}</option>
												@endforeach
											</select>
										@else
											<input id="enquiryFormCallingCode" name="enquiryFormCallingCode" type="text" class="form-control" placeholder="code" value="+91">
										@endif
									</div>
									<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-pad-r">
										<input id="enquiryFormPhone" name="enquiryFormPhone" type="text" class="form-control" placeholder="10 digit phone number" value="">
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="pad-t-20">
							<div class="col-sm-12">
								<div class="form-group">
									<label for="enquiryFormEmail">Email Address*</label>
									<input id="enquiryFormEmail" name="enquiryFormEmail" type="text" class="form-control email-typo-error" placeholder="enter your valid email address" value="">
								</div>
							</div>
							<div class="col-sm-6 pad-t-20__400 pad-t-20-400-600 hide">
								<div class="form-group">
									<label for="enquiryFormDate">Party Date*</label>
									<input id="enquiryFormDate" name="enquiryFormDate" type="text" class="form-control" placeholder="when is your party?" value="NULL">
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="pad-t-20">
							<div class="col-sm-12">
								<div class="form-group">
									<label for="enquiryTimeSlot">When should we reach you?</label>
									<select class="form-control" id="enquiryTimeSlot" name="enquiryTimeSlot">
										<option value="-1">Select an option</option>
									</select>
								</div>
							</div>
							<div class="col-sm-6 pad-t-20__400 pad-t-20-400-600 hide">
								<div class="form-group">
									<label for="enquiryPlanningStatus">When are you planning to book?</label>
									<select class="form-control" id="enquiryPlanningStatus" name="enquiryPlanningStatus">
										<option value="-1">Select an option</option>
										<option value="1">Immediately</option>
										<option value="2">In a day</option>
										<option value="6">In 3 Days</option>
										<option value="3">In a week</option>
										<option value="4">In a month</option>
										<option value="5">I’m just browsing</option>
										<option value="NA" selected>NA</option>

									</select>
								</div>
							</div>
							<div class="hide" id="enquiryTimeSlotOptions">
								<option value="Immediate" data-min-time="36000" data-max-time="72000">In One Hour
								</option>
								<option value="Today Morning(10 AM - 1 PM)" data-min-time="0"
										data-max-time="36000">Today Morning (10 AM - 1 PM)
								</option>
								<option value="Today Afternoon(1 PM - 3 PM)" data-min-time="0"
										data-max-time="46800">Today Afternoon (1 PM - 3 PM)
								</option>
								<option value="Today Evening(3 PM - 6 PM)" data-min-time="0"
										data-max-time="54000">Today Evening (3 PM - 6 PM)
								</option>
								<option value="Today Night(6 PM - 8 PM)" data-min-time="0"
										data-max-time="64800">Today Night (6 PM - 8 PM)
								</option>
								<option value="Tomorrow Morning(10 AM - 1 PM)" data-min-time="46800"
										data-max-time="86399">Tomorrow Morning (10 AM - 1 PM)
								</option>
								<option value="Tomorrow Afternoon(1 PM - 3 PM)" data-min-time="54000"
										data-max-time="86399">Tomorrow Afternoon (1 PM - 3 PM)
								</option>
								<option value="Tomorrow Evening(3 PM - 6 PM)" data-min-time="64800"
										data-max-time="86399">Tomorrow Evening (3 PM - 6 PM)
								</option>
								<option value="Tomorrow Night(6 PM - 8 PM)" data-min-time="72000"
										data-max-time="86399">Tomorrow Night (6 PM - 8 PM)
								</option>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="pad-t-20">
							<div class="col-sm-12">
								<div class="form-group">
									<label for="enquiryFormComments">Your Requirements</label>
									<textarea id="enquiryFormComments" name="enquiryFormComments" class="form-control" placeholder="Your Requirements"></textarea>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="pad-t-20">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="fw-normal">
										{{ Form::checkbox('accepts', '1', false, ['id' => 'acceptsForm']) }}
										<span class="mar-l-4 ">
												I <span class="text-lower">have read and accept the <a href="{{ route('terms') }}" target="_blank" rel="noopener">terms of service</a>.</span>
											</span>
									</label>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="pad-t-20">
							<div class="col-sm-12">
								<div class="form-group">
									<input type="submit" value="SUBMIT" name="submit" id="btnSubmitFormHome" class="form-control btn btn-info">
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<input type="hidden" id="cityUrl" value="{{ getCityUrl() }}">
						<input type="hidden" id="hidTypeSourceFb" value="{{ config('evibe.ticket.type_source.facebook-ad') }}">
					</div>
				</form>
			</div>
		</div>
		<div class="clearfix"></div>
	</section>

@else
	<section id="book" class="home-section booking-form-wrap base-home-bg-color section-cat no-pad"> <!-- Booking form -->
		<div class="full-width wc-1366 ">
			<div class="col-sm-12 col-lg-8 col-md-8 col-lg-offset-2 col-md-offset-2 row form-card-shadow no-pad ">
				<div class="col-sm-12 col-md-5 col-lg-5 sec-title " style="background-image: url({{$galleryUrl}}/main/img/home/v1/form-bg.png);height:600px;padding-top: 125px;color:white;text-align: center;background-size: cover;padding-right:15px;padding-left: 15px">
					<h4 style="font-size:32px;line-height: 1.3">
						@if((!isset($formTitle)) && (!isset($passiveCity)))
							Tell us what you need.
						@elseif(isset($passiveCity))
							REQUEST OUR SERVICE IN YOUR CITY
						@else
							{{ $formTitle }}
						@endif
					</h4>
					<h6>
						@if (!isset($formDescription))
							We will help you book the best party services based on your preferences with
							<span class=" mar-l-4">best deals</span>.
						@else
							{{ $formDescription }}
						@endif
					</h6>

					@include("base.home.home-usp")

				</div>
				<div class="col-sm-12 col-md-7 col-lg-7 no-pad">
					<form class="homepage-booking-form" style="height: 600px">
						<div>
							<div class="col-sm-12">
								<div class="text-muted low-font">*fields are mandatory.</div>
								<div class="errors-cnt alert-danger text-center hide"></div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="form-fields">
							<div class="pad-t-20">
								<div class="col-sm-12">
									<div class="form-group">
										<label for="enquiryFormName">Your Name*</label>
										<input id="enquiryFormName" name="enquiryFormName" type="text" class="form-control" placeholder="enter your full name" value="">
										@if(isset($passiveCity))
											<input id="passiveCity" name="passiveCity" type="hidden" class="form-control" value="{{ $data["passiveCityUrl"] }}">
										@else
											<input id="passiveCity" name="passiveCity" type="hidden" class="form-control" value="NA">
										@endif
									</div>
								</div>
								<div class="clearfix"></div>

							</div>
							<div class="pad-t-20">
								<div class="col-sm-12">
									<div class="form-group">
										<label for="enquiryFormPhone">Phone Number*</label>
										<div class="clearfix"></div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad">
											@if(isset($data['countries']) && count($data['countries']))
												<select id="enquiryFormCallingCode" name="enquiryFormCallingCode" class="form-control country-calling-code">
													@foreach($data['countries'] as $country)
														<option value="{{ $country['calling_code'] }}">{{ $country['calling_code'] }}</option>
													@endforeach
												</select>
											@else
												<input id="enquiryFormCallingCode" name="enquiryFormCallingCode" type="text" class="form-control" placeholder="code" value="+91">
											@endif
										</div>
										<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-pad-r">
											<input id="enquiryFormPhone" name="enquiryFormPhone" type="text" class="form-control" placeholder="10 digit phone number" value="">
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="pad-t-20">
								<div class="col-sm-12">
									<div class="form-group">
										<label for="enquiryFormEmail">Email Address*</label>
										<input id="enquiryFormEmail" name="enquiryFormEmail" type="text" class="form-control email-typo-error" placeholder="enter your valid email address" value="">
									</div>
								</div>
								<div class="col-sm-6 pad-t-20__400 pad-t-20-400-600 hide">
									<div class="form-group">
										<label for="enquiryFormDate">Party Date*</label>
										<input id="enquiryFormDate" name="enquiryFormDate" type="text" class="form-control" placeholder="when is your party?" value="NULL">
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="pad-t-20">
								<div class="col-sm-12">
									<div class="form-group">
										<label for="enquiryTimeSlot">When should we reach you?</label>
										<select class="form-control" id="enquiryTimeSlot" name="enquiryTimeSlot">
											<option value="-1">Select an option</option>
										</select>
									</div>
								</div>
								<div class="col-sm-6 pad-t-20__400 pad-t-20-400-600 hide">
									<div class="form-group">
										<label for="enquiryPlanningStatus">When are you planning to book?</label>
										<select class="form-control" id="enquiryPlanningStatus" name="enquiryPlanningStatus">
											<option value="-1">Select an option</option>
											<option value="1">Immediately</option>
											<option value="2">In a day</option>
											<option value="6">In 3 Days</option>
											<option value="3">In a week</option>
											<option value="4">In a month</option>
											<option value="5">I’m just browsing</option>
											<option value="NA" selected>I’m just browsing</option>
										</select>
									</div>
								</div>
								<div class="hide" id="enquiryTimeSlotOptions">
									<option value="Immediate" data-min-time="36000" data-max-time="72000">In One Hour
									</option>
									<option value="Today Morning(10 AM - 1 PM)" data-min-time="0"
											data-max-time="36000">Today Morning (10 AM - 1 PM)
									</option>
									<option value="Today Afternoon(1 PM - 3 PM)" data-min-time="0"
											data-max-time="46800">Today Afternoon (1 PM - 3 PM)
									</option>
									<option value="Today Evening(3 PM - 6 PM)" data-min-time="0"
											data-max-time="54000">Today Evening (3 PM - 6 PM)
									</option>
									<option value="Today Night(6 PM - 8 PM)" data-min-time="0"
											data-max-time="64800">Today Night (6 PM - 8 PM)
									</option>
									<option value="Tomorrow Morning(10 AM - 1 PM)" data-min-time="46800"
											data-max-time="86399">Tomorrow Morning (10 AM - 1 PM)
									</option>
									<option value="Tomorrow Afternoon(1 PM - 3 PM)" data-min-time="54000"
											data-max-time="86399">Tomorrow Afternoon (1 PM - 3 PM)
									</option>
									<option value="Tomorrow Evening(3 PM - 6 PM)" data-min-time="64800"
											data-max-time="86399">Tomorrow Evening (3 PM - 6 PM)
									</option>
									<option value="Tomorrow Night(6 PM - 8 PM)" data-min-time="72000"
											data-max-time="86399">Tomorrow Night (6 PM - 8 PM)
									</option>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="pad-t-20">
								<div class="col-sm-12">
									<div class="form-group">
										<label for="enquiryFormComments">Your Requirements</label>
										<textarea id="enquiryFormComments" name="enquiryFormComments" class="form-control" placeholder="Your Requirements"></textarea>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="pad-t-20">
								<div class="col-sm-12">
									<div class="form-group">
										<label class="fw-normal">
											{{ Form::checkbox('accepts', '1', false, ['id' => 'acceptsForm']) }}
											<span class="mar-l-4">
												I <span class="text-lower" style="color:black">have read and accept the <a href="{{ route('terms') }}" target="_blank" rel="noopener">terms of service</a>.</span>
											</span>
										</label>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="pad-t-20">
								<div class="col-sm-12">
									<div class="form-group">
										<input type="submit" value="SUBMIT" name="submit" id="btnSubmitFormHome" class="form-control btn btn-info">
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<input type="hidden" id="cityUrl" value="{{ getCityUrl() }}">
							<input type="hidden" id="hidTypeSourceFb" value="{{ config('evibe.ticket.type_source.facebook-ad') }}">
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</section>
@endif
<div class="clearfix"></div>
