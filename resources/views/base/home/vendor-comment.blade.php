<div class="section-vendors padding-75">
	<div class="container">
		<h4 class="sec-title text-center no-mar-t mar-b-30">Are you a service provider?</h4>
		<div class="row no-margin vendor-box">
			<div class="col-md-7 no-margin no-padding vendor-feedback">
				<div id="vendor-slide" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<?php  $count = 0; ?>
						@foreach($data['vendorStories'] as $vendor)
							<div class="item @if($count == 0) active @endif">
								@if($vendor['type_id'] == 0)
									<img src="{{ $galleryUrl }}/main/img/{{$vendor['url']}}" alt="Evibe vendor story">
								@elseif($vendor['type_id'] == 1)
									<div class="partner-story-yt-img home-page-iframe-video-load" data-height="395" data-count="{{ $count }}" data-url="{{ $vendor['url'] }}">
										<img src="https://img.youtube.com/vi/{{ $vendor['url'] }}/hqdefault.jpg">
									</div>
								@else
								@endif
								<div class="ven-comment no-pad-t__400">
									<div class="ven-story">{{ $vendor['story'] }}"</div>
									<h6 class="pull-right pad-t-10 pad-r-10 no-mar">
										- {{ $vendor['provider']['person'] }}
										<span class="font-14 hide">(@vendorType($vendor['map_type_id']))</span> <!-- enable this with appropriate data in DB -->
									</h6>
									<div class="clearfix"></div>
								</div>
							</div>
							<?php $count++; ?>
						@endforeach
					</div>
					<a class="carousel-control vend-slide-prev hidden-sm hidden-xs" href="#vendor-slide" data-slide="prev">
						<img src="https://gallery.evibe.in/img/icons/icon_left_thin.png" alt="Prev">
					</a>
					<a class="carousel-control vend-slide-next hidden-sm hidden-xs" href="#vendor-slide" data-slide="next">
						<img src="https://gallery.evibe.in/img/icons/icon_right_thin.png" alt="Next">
					</a>
				</div>
			</div>
			<div class="col-md-5 col-xs-12 ven-content">
				<h4 class="sec-title text-col-pink text-center">Join Evibe.in and grow your Business now.</h4>
				<h6 class="text-pr-col">
					Sign-up now for free and get more business without any extra efforts.
					Pay only for what you earn.
				</h6>
				<div class="benefits-lists text-center text-col-wh">
					<a href="{{ route('partner.benefits') }}" class="btn btn-default btn-explore">Learn More</a>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function storyTrim() {
		var elements = document.getElementsByClassName('ven-story');
		Array.prototype.forEach.call(elements, function (element) {
			if (element.innerHTML.length >= 250) {
				var lastSpaceIndex = element.innerHTML.substr(0, 250).lastIndexOf(' ');
				element.innerHTML = element.innerHTML.substr(0, lastSpaceIndex) + ' ...';
			}
		});
	}

	storyTrim();
</script>
