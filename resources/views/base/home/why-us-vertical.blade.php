<h4 class="sec-title text-center font-20 pad-t-10 ">Evibe<span class="text-lower">.in</span> Brand Promise</h4>
<div class="section-why-us-profile benefit text-center ">
	<div class="mar-b-25">
		<div class=" img-profile-div">
			<img src="{{ config('evibe.gallery.host') }}/main/img/icons/guarantee.png" alt="Trusted by customers" class="img-profile">
		</div>
		<div class="ben-title-profile ">Trusted by 1,50,000+ Customers</div>
		<div class="ben-desc-profile">
			Your event is in safe hands. 1000s of customers book with us for our quality service & support.
		</div>
	</div>
	<div class="mar-b-25">
		<div class=" img-profile-div">
			<img src="{{ config('evibe.gallery.host') }}/main/img/icons/easy-booking.png" alt="Easy Booking on Evibe" class="img-profile">
		</div>
		<div class="ben-title-profile  pad-t-15">Stress-Free Party Planning</div>
		<div class="ben-desc-profile">
			Get peace of mind. Book everything at one place with assurance of professional service.
		</div>
	</div>
	<div class="mar-b-25">
		<div class=" img-profile-div">
			<img src="{{ config('evibe.gallery.host') }}/main/img/icons/real_photos_reviews.png" alt="Real photos & reviews" class="img-profile">
		</div>
		<div class="ben-title-profile  pad-t-15 ">Real Pictures & Reviews</div>
		<div class="ben-desc-profile">
			It’s now easy to select perfect options. See 5,500+ real pictures & customer reviews.
		</div>
	</div>
	<div class="mar-b-25">
		<div class=" img-profile-div">
			<img src="{{ config('evibe.gallery.host')}}/main/img/icons/best_deals.png" alt="Best prices & packages" class="img-profile">
		</div>
		<div class="ben-title-profile  pad-t-15 ">Best Prices & Packages</div>
		<div class="ben-desc-profile">
			Get best value for money. Our prices are pre-negotiated and best in market!
		</div>
	</div>
	<div class="clearfix"></div>
</div>