@php $sessionCityUrl = is_null(getCityUrl()) ? 'bangalore' : getCityUrl(); @endphp
@php $carouselItemCount=6 @endphp

<div class="section-cat mar-t-20">
	<h4 class="sec-title title-text text-center"><span>Our Customers </span><img src="https://gallery.evibe.in/main/img/home/heart.png" alt="Evibe customers"> Evibe<span class="text-lower">.in</span></h4>
	<div class="" style="margin-left: 50px;margin-right: 50px">
		<div id="g-reviews-carousel-wrap" class="carousel slide  header-wrap" data-ride="carousel">
			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				@if(isset($reviews) && count($reviews))
					@php $reviewCount = 0 @endphp
					@foreach($reviews as $review)
						@if(($reviewCount++)%3 == 0 || $agent->isMobile())
							<div class="item @if($loop->first) active @endif full-width clr-black">
								@endif
								<div class="col-sm-12 col-md-4 col-lg-4">
									<div class="google-reviews-card">
										@if(isset($review['profile_pic_url']) && $review['profile_pic_url'])
											<img src="{{$review['profile_pic_url']}}">
										@else
											<img src="https://lh4.googleusercontent.com/-gZl_LpT5sQU/AAAAAAAAAAI/AAAAAAAAAAA/X_Rq2u1nWiQ/s40-c-rp-mo-br100/photo.jpg">
										@endif
										<p class="fw-500 font-18">{{$review['name']}}<br>
											<span class="list-option-rating in-blk" style="color: #FFCE00;">
													<input type="number" class="google-avg-rating hide" value="{{ $review['rating'] }}" title="{{ $review['rating'] }} google rating "/>
												</span>
										</p>
										<p style="padding-left: 15px;padding-right: 15px;color: #657781">
											{{$review['review'] }}
										</p>
									</div>
								</div>
								@if($reviewCount%3 == 0 || $agent->isMobile())
							</div>
						@endif
					@endforeach
				@endif
			</div>
			<!-- Controls -->
			<a class="left carousel-control" href="#g-reviews-carousel-wrap" role="button" data-slide="prev">
				<img src="{{ $galleryUrl }}/main/img/icons/left-arrow-carousel.svg" class="custom-controls-style control-left" style="margin-left: -10px" alt="carousel control left">
			</a>
			<a class="right carousel-control" href="#g-reviews-carousel-wrap" role="button" data-slide="next">
				<img src="{{ $galleryUrl }}/main/img/icons/right-arrow-carousel.svg" class="custom-controls-style control-right" style="margin-left: 10px" alt="carousel control right">
			</a>
		</div>
		@if(!($agent->isMobile()))
			<div>
				<a href="https://www.google.com/search?client=ubuntu&channel=fs&q=evibe+bangalore&ie=utf-8&oe=utf-8#lrd=0x3bae1165dd2071c9:0x219bab4c2f1329f3,1,,," style="color:#55555"><img src="{{ $galleryUrl }}/main/img/home/greview.png" style="height: 115px" target='_blank' alt="google review card"> SEE ALL REVIEWS ON GOOGLE</a>
			</div>
		@else
			<div style="text-align: center;">
				<img src="{{ $galleryUrl }}/main/img/home/greview.png" alt="google review card" style="height: 115px">
				<p class="text-center">
					<a href="https://www.google.com/search?client=ubuntu&channel=fs&q=evibe+bangalore&ie=utf-8&oe=utf-8#lrd=0x3bae1165dd2071c9:0x219bab4c2f1329f3,1,,," style="color:#555555;text-decoration: underline;"> SEE ALL REVIEWS ON GOOGLE</a>
				</p>
			</div>
		@endif
	</div>
</div>
@section("javascript")
	@parent
	<script type="text/javascript">
		$.getScript("/js/util/star-rating.js", function () {
			function applyRatingStar(to) {
				$(to).rating({
					showClear: 0,
					readonly: 'true',
					showCaption: 0,
					size: 'xl',
					min: 0,
					max: 5,
					step: 0.1
				});
				$(to).removeClass('hide');
			}

			setInterval(applyRatingStar('.google-avg-rating'), 1000);
		});
	</script>
@endsection