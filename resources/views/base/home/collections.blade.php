<!-- @if(isset($data['collections']) && count($data['collections']) > 0)
	<div class="container">
		<div class="section-ocs mar-b-40">
			<h4 class="sec-title text-center font-weight-500 text-pr-col mar-b-40">
				Introducing Collections
			</h4>
			<h6 class="text-center top-10"></h6>
			<div class="row">
				@foreach($data['collections'] as $collection)
					<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 collection-card">
						<a href="{{ $data['collectionBaseUrl'].$collection['url'] }}?ref=list-image">
							<img src="{{ $collection['profileImg'] }}" alt="{{ $collection['name'] }}">
							<div class="collection-card-title text-center">
								<h3 class="text-center mdl-color-text--white">{{ $collection['name'] }}</h3>
								<h6 class="text-center mdl-color-text--white">{{ $collection['description'] }}</h6>
							</div>
						</a>
					</div>
				@endforeach
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 collection-card">
					<div class="collection-card-title see-all no-transistion">
						<a href="{{ route('city.collection.list', getCityUrl()) }}">
							<h4 class="text-center no-transistion">See All Collections</h4>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
@endif -->