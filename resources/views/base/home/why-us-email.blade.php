<div style="margin-top: 15px;">
	<div style=" margin-bottom: 10px; font-family: 'Montserrat', 'Roboto', 'Open Sans';font-size:20px; padding-top:15px">Evibe<span style="text-transform: lowercase">.in</span> Brand Promise</div>
	<div>
		<div>
			<div style="width: 150px; display: inline-block; margin-right: 20px; margin-top: 15px; vertical-align: top;text-align:center">
				<div style="margin-bottom: 4px;">
					<img src="{{ $galleryUrl }}/main/img/icons/guarantee.png" alt="clarify" style="margin-top: -4px;margin-right: 3px;vertical-align: middle;height:50px;width:50px">
					<div style="vertical-align: middle;font-size: 16px;color: #ED3E72;">Trusted by 1,50,000+ Customers</div>
				</div>
			</div>
			<div style="width: 150px; display: inline-block; margin-right: 20px; margin-top: 15px; vertical-align: top;text-align:center">
				<div style="margin-bottom: 4px;">
					<img src="{{ $galleryUrl }}/main/img/icons/easy-booking.png" alt="clarify" style="margin-top: -4px;margin-right: 3px;vertical-align: middle;height:50px;width:50px">
					<div style="vertical-align: middle;font-size: 16px;color: #ED3E72;">Stress-Free Party Planning</div>
				</div>
			</div>
			<div style="width: 150px; display: inline-block; margin-right: 20px; margin-top: 15px; vertical-align: top;text-align:center">
				<div style="margin-bottom: 4px;">
					<img src="{{$galleryUrl}}/main/img/icons/real_photos_reviews.png" alt="clarify" style="margin-top: -4px;margin-right: 3px;vertical-align: middle;height:50px;width:50px">
					<div style="vertical-align: middle;font-size: 16px;color: #ED3E72;">Real Pictures & Reviews</div>
				</div>
			</div>
			<div style="width: 150px; display: inline-block; margin-right: 20px; margin-top: 15px; vertical-align: top;text-align:center">
				<div style="margin-bottom: 4px;">
					<img src="{{ $galleryUrl }}/main/img/icons/best_deals.png" alt="clarify" style="margin-top: -4px;margin-right: 3px;vertical-align: middle;height:50px;width:50px">
					<div style="vertical-align: middle;font-size: 16px;color: #ED3E72;">Best Prices & Packages</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>