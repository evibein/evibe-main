<div class="header-top @if(request()->url() == route("city.search", getCityUrl())) couple-exp-header-wrap @endif">

	@include("campaign.top-bar")

	<nav class="navbar navbar-default navbar-home navbar-top" role="navigation">

		<div class="hide__400 hide-400-600"> <!-- desktop -->
			<nav class="master-home-top-navbar">
				@include("app.desktop.top-bar")
			</nav>
			<nav class="navbar-default navbar-home navbar-home-white no-mar-b" role="navigation">
				@include("app.desktop.search-bar")
			</nav>
			<div class="clearfix"></div>
		</div>

		<div class="hide show-400-600 unhide__400"> <!-- for media less than 600px -->

			<!-- city and phone number -->
			<div class="text-center pad-b-6">
				<div class="pad-t-5 pad-l-15 font-14 text-col-wh navbar-links top-strip">
					<div class="in-blk mar-r-15">
						<a href="#" class="select-city">
							<span class="glyphicon glyphicon-map-marker"></span>
							{{ getCityName() }}
						</a>
					</div>
					<div class="in-blk font-12 mar-r-15 clr-black nav-topbar-phone-mob">
						<span class="glyphicon glyphicon-earphone"></span>{{ config('evibe.contact.company.plain_phone') }}
					</div>
					<div class="in-blk">
						@include('app.partybag_header_top')
					</div>
				</div>
			</div>

			<!-- Logo and Search bar-->
			<div class="container no-pad">
				<div class="col-xs-8 col-sm-8">
					<a class="navbar-brand no-pad-t" href="/">
						<img src="{{ $galleryUrl }}/img/logo/logo_evibe.png" alt="Evibe.in logo">
					</a>
				</div>
				<div class="col-xs-2 col-sm-2 col-xs-offset-2 col-sm-offset-2">
					@include('app.login_user')
				</div>
				<div class="clearfix"></div>
			</div>

			<div class="mar-b-10">
				@include("app.mobile.search-bar")
			</div>
		</div>

	</nav>
</div>

@section("modals")
	@parent
	@include('app.modals.enquiry-form', [
		"url" => route("city.header.enquire", getCityUrl())
	])
	@if(request()->is('*/engagement-wedding-reception/*'))
		@include('app.modals.free-site-visit-modal')
	@endif
@endsection