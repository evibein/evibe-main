@php $sessionCityUrl = is_null(getCityUrl()) ? 'bangalore' : getCityUrl(); @endphp
@php $carouselItemCount=5 @endphp
@php $showLockdownPage = 0 @endphp
@if($sessionCityUrl == "bangalore" || $sessionCityUrl == "hyderabad")
    @php $showLockdownPage = 1 @endphp
    @php $carouselItemCount=7 @endphp

@endif

@if ($agent->isMobile() && !($agent->isTablet()))
    <div class=" mar-l-15 mar-r-15 mar-t-10 home-tiles-large" style="background-image: url('{{ $galleryUrl }}/main/img/home/tiles/mobile-tiles-full-screen-bdy.png');background-size:cover;">
        <div class="pad-l-20 pad-r-20">
            <h5 class="no-mar-b no-pad-b fw-400"> This Birthday</h5>
            <h4 class="mar-t-5"> Celebrate Like Never Before</h4>
            <div class="text-center full-width pad-t-15 font-12-imp">
                <a class="col-xs-3 text-center a-no-decoration no-pad-r" href="{{ route('city.occasion.birthdays.cakes.list',$sessionCityUrl) }}?ref=horizontal-navbar">
                    <img class="cat-img" src="{{ $galleryUrl }}/main/img/home/tiles/cake.png" alt="cakes icon">
                    <p class="cat-title">Cakes</p>
                </a>
                <a href="{{ route('city.occasion.birthdays.decors.list',['cityUrl'=>$sessionCityUrl]) }}?ref=horizontal-navbar" class="col-xs-3 text-center no-pad-r ">
                    <img class="cat-img" src="{{ $galleryUrl }}/main/img/home/tiles/decor.png" alt="decor icon">
                    <p class="cat-title ">Decors</p>
                </a>
                <a href="{{ route('city.occasion.birthdays.ent.list', $sessionCityUrl) }}?ref=horizontal-navbar" class="col-xs-3 text-center no-pad-r">
                    <img class="cat-img" src="{{ $galleryUrl }}/main/img/home/tiles/activity-wand.png" alt="ativities icon">
                    <p class="cat-title ">Activities</p>
                </a>
                <a href="{{ route('city.occasion.birthdays.home', getCityUrl()) }}?ref=horizontal-navbar" class="col-xs-3 text-center no-pad-r ">
                    <img class="cat-img" src="{{ $galleryUrl }}/main/img/home/tiles/add-button.png" alt="tiles icon">
                    <p class="cat-title ">More</p>
                </a>
            </div>
        </div>
    </div>
    <div class=" mar-l-15 mar-r-15 mar-t-15 home-tiles-medium" style="background-image: url('{{ $galleryUrl }}/main/img/home/homehero7.png');background-size:cover;">
        <div class="pad-l-20 pad-r-20 pad-b-15 text-center">
            <a href="{{route('inspirations.feed','baby-shower-decor-ideas')  }}?ref=mbl-home-cards" class="a-no-decoration">
                <div class="font-24 text-grey">Baby Shower Ideas</div>

                <div class="font-14 lh-24 mar-t-15 pad-l-15 pad-r-15 in-blk text-grey" style="border: 1px solid #000000;">Browse</div>
            </a>
        </div>
    </div>
    <div class=" mar-l-15 mar-r-15 mar-t-15 home-tiles-medium hide" style="background-image: url('{{ $galleryUrl }}/main/img/home/shop-hero-2.png');background-size:cover;">
        <div class="pad-l-20 pad-r-20 pad-b-15 text-center">
            <a href="{{ route('store-redirect', ['redirect' => config('evibe.store_host').'?utm_source=CityCard&utm_medium=Website&utm_campaign=StorePromotion']) }}" class="a-no-decoration">
                <div class="font-24 text-grey">Party Props</div>
                <div class="font-14 mar-t-10 text-grey">Delivered to your doorstep</div>
                <div class="font-14 lh-24 mar-t-15 pad-l-15 pad-r-15 in-blk text-grey" style="border: 1px solid #000000;">Shop Now</div>
            </a>
        </div>
    </div>
    <div class="full-width pad-r-15 pad-l-15 pad-t-15">
        <div class="col-xs-6 no-pad-l" style="padding-right: 8px">
            <div class="full-width text-center pad-t-10 home-tiles-large pad-10" style="background-image: url('{{ $galleryUrl }}/main/img/home/tiles/cld-home-tile.png')">
                <a href="{{ route('city.cld.list', getCityUrl()) }}?ref=horizontal-navbar" class="a-no-decoration">
                    <h4 class="fc-white no-mar-b mar-t-10">Celebrate Love.</h4>
                    <h6 class="fc-white mar-t-10 font-14"> Express With Our CandleLight Dinners</h6>
                    <p class="fc-white tiles-cta"> +Explore Now</p>
                </a>
            </div>

            <div class=" full-width  text-center mar-t-15 home-tiles-mini hide" style="background-image: url('{{ $galleryUrl }}/main/img/home/tiles/naming-ceremony-home-tile.png');">
                <a href="{{ route('city.occasion.ncdecors.list',$sessionCityUrl) }}?ref=horizontal-navbar" class="a-no-decoration">
                    <h5 class="text-grey mar-t-15 no-mar-b" style="line-height: 1.4"> Naming Ceremony Decorations</h5>
                    <p class="text-grey mar-t-5" style="border: 1px solid #000000;">+Explore Now</p>
                </a>
            </div>

            <div class="a-no-decoration full-width text-center mar-t-15 pad-10 home-tiles-large" style="background-image: url('{{ $galleryUrl }}/main/img/home/tiles/corporate-home-tiles.png');">
                <a href="{{route('city.occasion.corporate.home',$sessionCityUrl)}}?ref=horizontal-navbar" class="a-no-decoration">
                    <h4 class="fc-white no-mar-b mar-t-50">Corporate Parties</h4>
                    <h6 class="fc-white mar-t-10 font-14">Now With More Fun & More Energy</h6>
                    <p class="fc-white tiles-cta"> +Explore Now</p>
                </a>
            </div>
        </div>
        <div class="col-xs-6 no-pad-r" style="padding-left: 8px">
            <div class="full-width  text-center home-tiles-mini hide" style="background-image: url('{{ $galleryUrl }}/main/img/home/homeherom7.png');">
                <a href="{{ route('city.occasion.bsdecors.list',$sessionCityUrl) }}?ref=horizontal-navbar" class="a-no-decoration">
                    <h5 class="fc-white mar-t-30" style="line-height: 1.4"> Baby Shower Decorations</h5>
                    <p class="fc-white no-mar-t tiles-cta">+Browse Now</p>
                </a>


            </div>
            <div class="full-width text-center home-tiles-large pad-10" style="background-image: url('{{ $galleryUrl }}/main/img/home/tiles/surprise-home-tile.png');">
                <a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/h/couple-surprises?ref=horizontal-navbar" class="a-no-decoration">
                    <h4 class="fc-white no-mar-b mar-t-50 ">Shhh...</h4>
                    <h6 class=" fc-white mar-t-10">Lets Plan Surprises</h6>
                    <p class="fc-white tiles-cta"> +Explore Now</p>
                </a>
            </div>
            <div class="a-no-decoration full-width text-center mar-t-15 pad-10 home-tiles-large" style="background-image: url('{{ $galleryUrl }}/main/img/home/homeherom4.png');">
                <a href="{{ route('city.occasion.house-warming.home', getCityUrl()) }}?ref=horizontal-navbar" class="a-no-decoration">
                    <h4 class="fc-white no-mar-b mar-t-50">New House</h4>
                    <h6 class="fc-white mar-t-10 font-14">Make It Home</h6>
                    <p class="fc-white " style="letter-spacing: 1px;" > +Explore Decorations, Cakes and More</p>
                </a>
            </div>
        </div>
        @if(getCityUrl() == "hyderabad" || getCityUrl() == "bangalore")
            @php $campaignUrl = config('evibe.host')."/u/wedding-decors-in-".getCityUrl()  @endphp
            <div class="col-xs-12 mar-t-15 home-tiles-medium" style="background-image: url('{{ $galleryUrl }}/main/img/home/homeweddings-carousel-desk.png');background-size:cover;">
                <div class="pad-l-20 pad-r-20 pad-b-15 text-center">
                    <a href="{{ $campaignUrl }}?ref=mbl-home-cards" class="a-no-decoration">
                        <div class="font-24 fc-white">Home Weddings</div>
                        <p class="fc-white mar-t-10" style="letter-spacing: 1px;"> +Explore Decorations, Cakes and More</p>
                    </a>
                </div>
            </div>
        @endif
    </div>
    <div class="clearfix"></div>
@else
	<div class="intro-header">
		<div id="header-carousel-wrap" class="carousel slide  header-wrap" data-ride="carousel">
			<ol class="carousel-indicators">
				@for($i=0;$i<=$carouselItemCount;$i++)
					<li data-target="#header-carousel-wrap" data-slide-to="{{ $i }}"></li>
				@endfor
			</ol>
			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
                
                
                <div class="item active">
                    <a href="{{ route('virtual-birthday-party.home') }}?ref=carousel" class="a-no-decoration">
                        <img src="{{ $galleryUrl }}/main/img/home/homehero-vbd.png" class="carousel-hero-img" alt="Carousel banner ofVirtual Bdy">
                    </a>
                    <div class="carousel-caption carousel-caption-style">
                        <h1 class="carousel-header fc-white white-text-shadow no-mar">Introducing Virtual Celebrations</h1>
                        <p class=" fc-white font-26 ls-1-imp" style="text-shadow: -3px 8px 9px rgba(244, 238, 236, 0.4)">Gift a liftime memorable & fun virtual celebrations</p>
                      
                        <a href="{{ route('virtual-birthday-party.home') }}?ref=carousel" class="btn btn-home-carousel-cta btn-md">Book Now</a>
                    </div>
                </div>
                <div class="item">
                    <a href="{{ route('city.cld.list', $sessionCityUrl) }}?ref=carousel" class="a-no-decoration">
                        <img src="{{ $galleryUrl }}/main/img/home/homehero9.png" alt="Carousel banner of Candle light dinners" class="carousel-hero-img">
                    </a>
                    <div class="carousel-caption carousel-caption-style">
                        <h1 class="carousel-header fc-white white-text-shadow no-mar">Celebrating Love!</h1>
                        <p class=" fc-white font-26 ls-1-imp" style="text-shadow: -3px 8px 9px rgba(244, 238, 236, 0.4)">Make It Memorable With Candle Light Dinner.</p>
                        <p class="fc-white">Trending:&nbsp;
                            <a href="{{ route('city.cld.list', $sessionCityUrl) }}?category=best-rooftop-candle-light-dinner&ref=carousel" class="fc-orange-light">Rooftop,</a>
                            <a href="{{ route('city.cld.list', $sessionCityUrl) }}?category=best-moonlight-candle-light-dinner&ref=carousel" class="fc-orange-light"> Moonlight, </a>
                            <a href="{{ route('city.cld.list', $sessionCityUrl) }}?category=best-poolside-candle-light-dinner&ref=carousel" class="fc-orange-light">Poolside candle light dinners</a>
                        </p>
                        <a class="btn btn-home-carousel-cta btn-md" href="{{ route('city.cld.list', $sessionCityUrl) }}?ref=carousel">Book Now</a>
                    </div>
                </div>
                <div class="item">
                    <a href="{{ route('city.occasion.birthdays.home', getCityUrl()) }}" class="a-no-decoration">
                        <img src="{{ $galleryUrl }}/main/img/home/homehero1.jpg" alt="carousel banner of Birthday party" class="carousel-hero-img">
                    </a>
                    <div class="carousel-caption carousel-caption-style">
                        <h1 class="carousel-header black-text-shadow no-mar">BIRTHDAY!</h1>
                        <p class="font-26 ls-1-imp" style="text-shadow: -3px 8px 9px rgba(101,44,27,0.4)">Celebrate Like Never Before.</p>
                        <p>Browse all:&nbsp;
                            <a href="{{route('city.occasion.birthdays.cakes.list',$sessionCityUrl)}}?ref=carousel" class="fc-orange">Cakes,</a>
                            <a href="{{ route('city.occasion.birthdays.decors.list',['cityUrl'=>$sessionCityUrl]) }}?ref=carousel" class="fc-orange"> Decorations, </a>
                            <a href="{{route('city.occasion.birthdays.packages.list',$sessionCityUrl) }}?ref=carousel" class="fc-orange">Packages</a>
                        </p>
                        <a class="btn btn-home-carousel-cta btn-md btn-post">Enquire Now</a>
                    </div>
                </div>
			
                
				<div class="item">
					<a href="{{ route('inspirations.feed','baby-shower-decor-ideas') }}?ref=carousel" class="a-no-decoration">
						<img src="{{ $galleryUrl }}/main/img/home/homehero7.png" class="carousel-hero-img" alt="Carousel banner of Baby Shower">
					</a>
					<div class="carousel-caption carousel-caption-style">
						<h1 class="carousel-header black-text-shadow no-mar">BABY SHOWER</h1>
						<p class="ls-1-imp font-26" style="text-shadow: -3px 8px 9px rgba(101,44,27,0.4)">Embark A New Journey with Celebration</p>
						<p>Browse All:&nbsp;<a href="{{ route('inspirations.feed','baby-shower-decor-ideas') }}?ref=carousel" class="fc-orange">Decorations Ideas</a>
						</p>
						<a href="{{ route('inspirations.feed','baby-shower-decor-ideas') }}?ref=carousel" class="btn btn-home-carousel-cta btn-md">Explore Now</a>
					</div>
				</div>
				<div class="hide">
					<div class="item">
						<a href="{{ route('store-redirect', ['redirect' => config('evibe.store_host').'?utm_source=CityCarousel&utm_medium=Website&utm_campaign=StorePromotion']) }}" class="a-no-carousel-decoration">
							<img src="{{ $galleryUrl }}/main/img/home/shop-hero-2.png" class="carousel-hero-img" alt="Carousel banner of Evibe Store">
						</a>
						<div class="carousel-caption carousel-caption-style">
							<p class="carousel-header black-text-shadow">Evibe Store
								<span class="carousel-header-tag mar-l-10">Just Launched</span></p>
							<p class="carousel-heade font-26" style="text-shadow: -3px 8px 9px rgba(101,44,27,0.4)">Get latest party props delivered to your doorstep</p>
							<a href="{{ route('store-redirect', ['redirect' => config('evibe.store_host').'?utm_source=CityCarousel&utm_medium=Website&utm_campaign=StorePromotion']) }}" class="btn btn-home-carousel-cta btn-md">Shop Now</a>
						</div>
					</div>
				</div>
				
				
				<div class="item">
					<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/h/couple-surprises?ref=carousel">
						<img src="{{ $galleryUrl }}/main/img/home/homehero8.png" class="carousel-hero-img" alt="carousel banner of Surprises">
					</a>
					<div class="carousel-caption carousel-caption-style">
						<h1 class="carousel-header fc-white white-text-shadow no-mar">CRAZY IN LOVE!</h1>
						<p class=" fc-white font-26 ls-1-imp" style="text-shadow:0 2px 4px rgba(244, 238, 236, 0.4)">Express With Our Best Surprises.</p>
						<p class="fc-white">Popular Searches:&nbsp;
							<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=romantic-stay&ref=carousel" class="fc-orange-light">Romantic,</a>
							<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=home-decorations&ref=carousel" class="fc-orange-light"> Home Decoration, </a>
							<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&ref=carousel" class="fc-orange-light">Unique Surprises</a>
						</p>
						<a class="btn btn-home-carousel-cta btn-md" href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/h/couple-surprises?ref=carousel">Browse All</a>
					</div>
				</div>
				<div class="item">
					<a href="{{ route('city.occasion.house-warming.decors.list', $sessionCityUrl) }}?ref=carousel" class="a-no-decoration">
						<img src="{{ $galleryUrl }}/main/img/home/homehero10.png" class="carousel-hero-img" alt="Carousel banner of house-warming">
					</a>
					<div class="carousel-caption carousel-caption-style">
						<h1 class="carousel-header fc-white no-mar" style="text-shadow: 0 4px 3px rgb(55, 26, 26);">NEW HOUSE!</h1>
						<p class="ls-1-imp fc-white font-26" style="text-shadow:0 2px 4px rgb(55, 26, 26)">Let's Make It A Home</p>
						<p class="fc-white">Browse Top:&nbsp;
							<a href="{{ route('city.occasion.house-warming.decors.list', $sessionCityUrl) }}?ref=carousel" class="fc-white">House Warming Decorations,</a>
							<a href="{{ route('city.occasion.house-warming.cakes.list', $sessionCityUrl) }}?ref=carousel" class="fc-white"> Cakes, </a>
							<a href="{{route('city.occasion.house-warming.priest.list', $sessionCityUrl) }}?ref=carousel" class="fc-white">Priest</a>
						</p>
						<a class="btn btn-home-carousel-cta btn-md btn-post">Enquire Now</a>
					</div>
				</div>
                <div class="item">
                    <a href="{{ route('city.occasion.ncdecors.list',$sessionCityUrl) }}?ref=carousel" class="a-no-decoration">
                        <img src="{{ $galleryUrl }}/main/img/home/homehero6.jpg" class="carousel-hero-img" alt="Carousel banner of Naming Ceremony">
                    </a>
                    <div class="carousel-caption carousel-caption-style">
                        <p class="carousel-header black-text-shadow">IT'S ALL IN THE NAME!</p>
                        <p class="carousel-heade font-26" style="text-shadow: -3px 8px 9px rgba(101,44,27,0.4)">Let's Cherish Your Celebration</p>
                        <p>Browse All:&nbsp;<a href="{{ route('city.occasion.ncdecors.list',$sessionCityUrl) }}?ref=carousel" class="fc-orange">Decorations </a>
                        </p>
                        <a class="btn btn-home-carousel-cta btn-md btn-post">Enquire Now</a>
                    </div>
                </div>
            </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#header-carousel-wrap" role="button" data-slide="prev">
                <img src="{{ $galleryUrl }}/main/img/icons/left-arrow-carousel.svg" class="custom-controls-style control-left" alt="carousel control left">
            </a>
            <a class="right carousel-control" href="#header-carousel-wrap" role="button" data-slide="next">
                <img src="{{ $galleryUrl }}/main/img/icons/right-arrow-carousel.svg" class="custom-controls-style control-right" alt="carousel control right">
            </a>
        </div>
    </div>
@endif

