@if ($agent->isMobile() || ($agent->isTablet()))
	<header>
		@include("occasion.util.common.m_header-section")
		@include("occasion.util.navigation.navigation")
	</header>
@else
	<div class="top-nav">

		@include("campaign.top-bar")

		<nav class="navbar navbar-default navbar-home navbar-top no-mar-b" role="navigation">

			<div class="hide__400 hide-400-600"> <!-- desktop -->
				<nav class="master-home-top-navbar">
					@include("app.desktop.top-bar")
				</nav>
				<nav class="navbar-default navbar-home navbar-home-white no-mar-b" role="navigation">
					@include("app.desktop.search-bar")
				</nav>
				<div class="clearfix"></div>
			</div>

			<div class="hide show-400-600 unhide__400"> <!-- for media less than 600px -->
				@include("app.mobile.top-bar")
				@include("app.mobile.search-bar")
			</div>
		</nav>
	</div>
@endif

@section("modals")
	@parent
	@if(request()->is('*/engagement-wedding-reception*'))
		@include('app.modals.free-site-visit-modal')
	@endif
@endsection