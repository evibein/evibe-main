<div class="intro-header" style="height: auto;margin-bottom: 30px">
	<div id="header-carousel-wrap" class="carousel slide  header-wrap" data-ride="carousel">
		@php $cityUrl =(is_null(getCityUrl()))?'bangalore':getCityUrl(); $count=0; @endphp
		@if(count($data)>1)
			<ol class="carousel-indicators no-mar-b">
				@foreach($data as $carouselIndicator)
					<li data-target="#header-carousel-wrap" data-slide-to="@php echo $count; @endphp"></li>
					@php  $count++; @endphp
				@endforeach
			</ol>
		@endif
		<div class="carousel-inner" role="listbox">
			{{-- Hardcoded for surprises since it needs links in header --}}
			@if(request()->path() == "/h/couple-surprises")
				<div class="item " style="height: auto !important">
					<a href="#" class="a-no-decoration" target="_blank">
						<img src="{{ $galleryUrl }}/{{config('evibe.occasion-landingpages.surprises.gallery-path')}}/{{$slide['img_url_desk']}}" class="carousel-hero-img" alt="{{ $slide['alt_text'] }}">
					</a>

					<div class="carousel-caption carousel-caption-style" style="top:34% !important">
						<h1 class="carousel-header black-text-shadow no-mar" style="color:white">Surprise your special one</h1>
						<p class="ls-1-imp font-26" style="color:white;text-shadow: -3px 8px 9px rgba(101,44,27,0.4);margin-top: 10px">
							<a href='"{{ route('city.cld.list', $cityUrl) }}' class='a-no-decoration'>Candle light dinners</a>,<a href='{{ config('evibe.host') . '/' .$cityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=home-decorations' class="a-no-decoration"> surprise decorations</a>,
							<a href='{{ config('evibe.host') . '/' .$cityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=trending-decorations' class="a-no-decoration">trending dates</a></p>

					</div>
				</div>
			@else
				@foreach($data as $slide)
					<div class="item @if($loop->first) active @endif" style="height: auto !important">
						<a href="#" class="a-no-decoration" target="_blank">
							<img src="{{ $galleryUrl }}/{{config('evibe.occasion-landingpages.surprises.gallery-path')}}/{{$slide['img_url_desk']}}" class="carousel-hero-img" alt="{{ $slide['alt_text'] }}">
						</a>
					</div>
				@endforeach
			@endif
		</div>
		<!-- Controls -->
		@if(count($data)>1)
			<a class="left carousel-control" href="#header-carousel-wrap" role="button" data-slide="prev">
				<img src="{{ $galleryUrl }}/main/img/icons/left-arrow-carousel.svg" class="custom-controls-style control-left" alt="carousel control left">
			</a>
			<a class="right carousel-control" href="#header-carousel-wrap" role="button" data-slide="next">
				<img src="{{ $galleryUrl }}/main/img/icons/right-arrow-carousel.svg" class="custom-controls-style control-right" alt="carousel control right">
			</a>
		@endif
	</div>
</div>