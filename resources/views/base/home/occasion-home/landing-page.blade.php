@extends('layout.base')

@section('page-title')
	<title>{{$data['seo_title']}}</title>
@endsection

@section('meta-description')
	<meta name="description" content="{{$data['seo_desc']}}">
@endsection

@section('meta-keywords')
	<meta name="keywords" content="{{$data['seo_keywords']}}"/>
@endsection

@section('og-title')
	<meta property="og:title" content="{{$data['seo_title']}}">
@endsection

@section('og-description')
	<meta property="og:description" content="{{$data['seo_desc']}}">
@endsection

@section("header")
	@include('base.home.header.header-home-city')
	<div class="header-border"></div>
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-list.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/app/page-party-bag-list.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/owl-carousel.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/util/star-rating.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/app/occasion-home.css') }}"/>
@endsection

@section("content")
	<div class="w-1366 ">
		@php $cityUrl =(is_null(getCityUrl()))?'bangalore':getCityUrl();  @endphp
		@if($agent->isMobile())
			<div class="text-center">
				<h1 class="section-heading" style="margin-bottom: 10px;font-size: 25px;margin-top:30px !important">{{$data['pageTitle']}}</h1>
				<p class="mar-b-10 font-16 pad-l-10 pad-r-10">{{$data['pageDescription']}}</p>
			</div>
			<div class="full-width pad-l-15 pad-r-15 pad-t-5 mar-t-15 mar-b-30">
				@foreach($data['carouselCards'] as $card)
					<div class="col-xs-12 mar-t-10 no-pad-l no-pad-r" style="border-radius: 5px;">
						<a href="{{ config('evibe.host')}}/{{ $cityUrl }}/{{  $card['cta_route'] }}?ref=occasion-home" target="_blank">
							<img src="{{ $galleryUrl }}/{{config('evibe.valentines-day-landingpages.gallery-path')}}/{{$card['img_url_mbl']}}" style="background: linear-gradient(to right, #e1eec3, #f05053);border-radius: 5px;width:100%;">
						</a>
						<div class="clearfix"></div>
					</div>
				@endforeach
				<div class="clearfix"></div>
			</div>
			<div class="full-width offer-card-mbl mar-r-15 mar-l-15 mar-t-30" id="offer-card" style="height:300px;background-image: url('{{$galleryUrl}}/{{config('evibe.valentines-day-landingpages.gallery-path')}}/mbl-offer.png');">
				<h4 class="offer-card-heading-mbl">SPECIAL OFFER FOR YOU</h4>
				<p class="fc-white font-16 no-mar-b">Grab Your Coupon Before It's Too Late</p>
				<div class="clearfix"></div>
				<div class="text-center mar-b-15 mar-t-30">
					<span class="text-muted floating-evibe-btn pad-t-5 pad-b-5 pad-l-10 pad-r-10 font-20" style="background-color: white;color:red;border-radius: 3px;">Get My Coupon</span>
				</div>
			</div>
			<div class="products wrap pad-l-15 pad-r-15">
				@foreach($data['sections'] as $section)
					@if(count($section))
						@foreach($section['products'] as $subSection)
							<div class="text-center">
								<h1 class="section-heading no-mar-b no-pad-l no-pad-r"> {{$subSection['cardTitle']}}</h1>
								<div class="clearfix"></div>
							</div>
							<div class="owl-carousel owl-theme products-wrap" style="display: block;">
								@if(!($subSection['isCld']))
									@foreach($subSection['products'] as $package)
										<div class="item mar-t-15 trending add-border">
											<a href="@if(isset($package['fullUrl'])) $package['fullUrl'] @else {{ $section['profileBaseUrl'] . $package['url'] }}?ref=occasion-home @endif"  target="_blank">
												<div class="img-container blur-bg-wrap prod-img-container">
													<img src="{{ $package['profilePic'] }}" alt="{{ $package['name'] }} profile picture" style="width: auto; z-index: 3; position: absolute; left: 50%; transform: translate(-50%); height: 100%; top: 0;" class="thumb-img in-blk bg-black">
													<img src="{{ $package['profilePic'] }}" alt="{{ $package['name'] }} profile picture" class="blur-bg">
												</div>
												<h6 class=" no-mar-b no-pad-b mar-t-5 top-categories-title font-16-imp">
													<a class="a-no-decoration-dark-black text-normal" href="" target="_blank">{{ $package['name'] }}</a>
												</h6>
												<p class="text-center no-mar-b  pad-t-5 " style="line-height: 15px;font-size:15px">
													@if(isset($package['price_worth']) && ($package['price_worth'] > $package['price']))
														<span style="color: #a2a2a2;font-size: 14px;text-decoration:line-through;">@price($package['price_worth'])&nbsp;&nbsp;</span>
													@elseif(isset($package['worth']) && $package['worth'] > $package['min_price'])
														<span style="color: #a2a2a2;font-size: 14px;text-decoration:line-through;">@price($package['worth'])&nbsp;&nbsp;</span>
													@endif
													@if(isset($package['price']))
														@price($package['price'])
													@elseif(isset($package['worth']) && $package['min_price'])
														@price($package['min_price'])
													@endif
												</p>
											</a>
										</div>
									@endforeach
								@else
									@foreach($subSection['products'] as $package)
										<div class="item mar-t-15 trending add-border">
											<a href="{{ $section['profileBaseUrl'] . $package['url'] }}?ref=occasion-home" target="_blank">
												<div class="img-container blur-bg-wrap prod-img-container">
													<img src="{{ $package['profilePic'] }}" alt="{{ $package['name'] }} profile picture" style="width: auto; z-index: 3; position: absolute; left: 50%; transform: translate(-50%); height: 100%; top: 0;" class="thumb-img in-blk bg-black">
													<img src="{{ $package['profilePic'] }}" alt="{{ $package['name'] }} profile picture" class="blur-bg">
												</div>
												<h6 class=" no-mar-b no-pad-b mar-t-5 top-categories-title font-16-imp">
													<a class="a-no-decoration-dark-black text-normal" href="" target="_blank">{{ $package['name'] }}</a>
												</h6>
												<p class="text-center no-mar-b  pad-t-5 " style="line-height: 15px;font-size:15px">
													@if(isset($package['price_worth']) && ($package['price_worth'] > $package['price']))
														<span style="color: #a2a2a2;font-size: 14px;text-decoration:line-through;">@price($package['price_worth'])&nbsp;&nbsp;</span>
													@elseif(isset($package['worth']) && $package['worth'] > $package['min_price'])
														<span style="color: #a2a2a2;font-size: 14px;text-decoration:line-through;">@price($package['worth'])&nbsp;&nbsp;</span>
													@endif
													@if(isset($package['price']))
														@price($package['price'])
													@elseif(isset($package['worth']) && $package['min_price'])
														@price($package['min_price'])
													@endif
												</p>
											</a>
										</div>
									@endforeach
								@endif
								<div class="clearfix"></div>
							</div>
							<!-- Section CTA -->
							@if($subSection['ctaUrl'] )
								<div class="text-center " style="@if($loop->last)margin-bottom: 40px; @else margin-bottom:0;  @endif margin-top: 25px">
									<a class="text-muted floating-evibe-btn pad-t-5 pad-b-5 pad-l-10 pad-r-10" href="{{ config('evibe.host') }}/{{ $cityUrl }}/{{ $subSection['ctaUrl'] }}" target="_blank">View More Packages</a>
								</div>
							@endif
						@endforeach
					@endif
				@endforeach
			</div>
		@else
		<!-- Header Section -->
			@if(count($data['carouselCardsDesktop']))
				@include('base.home.occasion-home.carousel',['data' => $data['carouselCardsDesktop']])
			@endif
			<div class="clearfix"></div>
			<div class="page-content">
				<!-- Sections Start  -->
				<div class="cards-wrap text-center">
					@foreach($data['sections'] as $section)
						<div class="text-center">
							<h1 class="section-heading no-mar-b no-pad-l no-pad-r" style="margin-top: 10px !important">{{$section['sectionTitle']}}</h1>
							<p style="margin-bottom: 5px;font-size: 18px">{{$section['sectionDesc']}}</p>
							<div class="clearfix"></div>
						</div>
						@if(count($section['cards']))
							<div class=" " style="display: block;margin-left: -10px;margin-right: -10px">
								@foreach($section['cards'] as $card)
									<div class="in-blk  text-center" style="width:33.3%;margin-top:20px">
										<div class="pad-l-5 pad-r-5">
											<a href="{{ config('evibe.host')}}/{{ $cityUrl}}/{{  $card['cta'] }}" target="_blank">
												<img src="{{ $galleryUrl }}/{{config('evibe.valentines-day-landingpages.gallery-path')}}/{{$card['img_url_desk']}}" style="width:100%;height:auto" class="card-desktop">
											</a>

										</div>
									</div>
								@endforeach
							</div>
						@endif
						<div class="clearfix"></div>
					@endforeach
				</div>
				<!-- Sections End -->
				<!-- Offer Card -->
				<div class="full-width offer-card-mbl" id="offer-card" style="margin-left:-5px;margin-right:-5px;height:300px;margin-top:40px;background-image: url('{{$galleryUrl}}/{{config('evibe.valentines-day-landingpages.gallery-path')}}/offer-card-desk.png');">
					<h2 class="offer-card-heading-mbl">SPECIAL OFFER FOR YOU</h2>
					<h4 class="fc-white no-mar-b">Grab Your Coupon Before It's Too Late</h4>
					<div class="clearfix"></div>
					<div class="text-center mar-b-15 mar-t-30">
						<span class="text-muted floating-evibe-btn pad-t-5 pad-b-5 pad-l-10 pad-r-10 font-20" style="background-color: white;color:red;border-radius: 3px;">Get My Coupon</span>
					</div>
				</div>
				<!-- Products Start -->
				<div class="products wrap">
					@foreach($data['sections'] as $section)
						@if(count($section))
							@foreach($section['products'] as $subSection)

								<div class="text-center">
									<h1 class="section-heading no-mar-b no-pad-l no-pad-r"> {{$subSection['cardTitle']}}</h1>
									<div class="clearfix"></div>
								</div>
								<div class="owl-carousel owl-theme products-wrap" style="display: block;">
									@if(!($subSection['isCld']))
										@foreach($subSection['products'] as $package)
											
											<div class="item mar-t-15 trending add-border">
												<a href=" @if(isset($package['fullUrl'])) {{ $package['fullUrl'] }} @else {{ $section['profileBaseUrl'] . $package['url'] }}?ref=occasion-home @endif"  target="_blank">
													<div class="img-container blur-bg-wrap prod-img-container">
														<img src="{{ $package['profilePic'] }}" alt="{{ $package['name'] }} profile picture" style="width: auto; z-index: 3; position: absolute; left: 50%; transform: translate(-50%); height: 100%; top: 0;" class="thumb-img in-blk bg-black">
														<img src="{{ $package['profilePic'] }}" alt="{{ $package['name'] }} profile picture" class="blur-bg">
													</div>
													<h6 class=" no-mar-b no-pad-b mar-t-5 top-categories-title font-16-imp">
														<a class="a-no-decoration-dark-black text-normal" href="" target="_blank">{{ $package['name'] }}</a>
													</h6>
													<p class="text-center no-mar-b  pad-t-5 " style="line-height: 15px;font-size:15px">
														@if(isset($package['price_worth']) && ($package['price_worth'] > $package['price']))
															<span style="color: #a2a2a2;font-size: 14px;text-decoration:line-through;">@price($package['price_worth'])&nbsp;&nbsp;</span>
														@elseif(isset($package['worth']) && $package['worth'] > $package['min_price'])
															<span style="color: #a2a2a2;font-size: 14px;text-decoration:line-through;">@price($package['worth'])&nbsp;&nbsp;</span>
														@endif
														@if(isset($package['price']))
															@price($package['price'])
														@elseif(isset($package['worth']) && $package['min_price'])
															@price($package['min_price'])
														@endif
													</p>
												</a>
											</div>
										@endforeach

									@else

										@foreach($subSection['products'] as $package)
											<div class="item mar-t-15 trending add-border">
												<a href="{{ $section['profileBaseUrl'] . $package['url'] }}?ref=occasion-home" target="_blank">

													<div class="img-container blur-bg-wrap prod-img-container">
															<img src="{{ $package['profilePic'] }}" alt="{{ $package['name'] }} profile picture" style="width: auto; z-index: 3; position: absolute; left: 50%; transform: translate(-50%); height: 100%; top: 0;" class="thumb-img in-blk bg-black">
														<img src="{{ $package['profilePic'] }}" alt="{{ $package['name'] }} profile picture" class="blur-bg">
													</div>
													<h6 class=" no-mar-b no-pad-b mar-t-5 top-categories-title font-16-imp">
														<a class="a-no-decoration-dark-black text-normal" href="" target="_blank">{{ $package['name'] }}</a>
													</h6>
													<p class="text-center no-mar-b  pad-t-5 " style="line-height: 15px;font-size:15px">
														@if(isset($package['price_worth']) && ($package['price_worth'] > $package['price']))
															<span style="color: #a2a2a2;font-size: 14px;text-decoration:line-through;">@price($package['price_worth'])&nbsp;&nbsp;</span>
														@elseif(isset($package['worth']) && $package['worth'] > $package['min_price'])
															<span style="color: #a2a2a2;font-size: 14px;text-decoration:line-through;">@price($package['worth'])&nbsp;&nbsp;</span>
														@endif
														@if(isset($package['price']))
															@price($package['price'])
														@elseif(isset($package['worth']) && $package['min_price'])
															@price($package['min_price'])
														@endif
													</p>
												</a>
											</div>

										@endforeach

									@endif
									<div class="clearfix"></div>
								</div>

								<!-- Section CTA -->
								@if($subSection['ctaUrl'] )
									<div class="text-center " style="@if($loop->last)margin-bottom: 40px; @else margin-bottom:0;  @endif margin-top: 25px">
										<a class="text-muted floating-evibe-btn pad-t-5 pad-b-5 pad-l-10 pad-r-10" href="{{ config('evibe.host') }}/{{ $cityUrl }}/{{ $subSection['ctaUrl'] }}" target="_blank">View More Packages</a>
									</div>
								@endif
							@endforeach
						@endif
					@endforeach
				</div>
				<!-- Products End -->
			</div>
			<div class="g-reviews mar-t-50">
				@if(isset($data['reviews']))

					@include("base.home.g-reviews",['reviews' => $data['reviews']])
				@endif
			</div>

		@endif
	</div>
	<div class="full-width">
		@include("base.home.why-us")
		<div class="clearfix"></div>
	</div>
	@include("base.home.occasion-home.offer-modal")

	<input type="hidden" id="pageUrl" value="{{ Request::url()}}">
@endsection
@section("footer")
	<div class="footer-wrap">
		@if (isset($data['views']['footer']) && view()->exists($data['views']['footer']))
			@include($data['views']['footer'])
		@endif

		@include('base.home.footer.footer-common')
	</div>
@endsection
@section("javascript")
	@parent
	<script type="text/javascript">
		$.getScript("/js/owl-carousel.js", function () {

			$('.cards-li').owlCarousel({
				stagePadding: 0,/*the little visible images at the end of the carousel*/
				dots: false,
				rtl: false,
				loop: false,
				margin: 15,
				lazyload: true,
				responsive: {
					0: {
						items: 1.3
					},
					1000: {
						items: 4
					},

				},
				nav: true,
				navText: ["<span class='pad-10 bg-white-imp'><img src='https://image.flaticon.com/icons/svg/481/481130.svg' style='height:20px'></span>", "<span class='pad-10 bg-white-imp'><img src='https://image.flaticon.com/icons/svg/481/481127.svg' style='height:20px'></span>"]
			});

			$('.carousel-cards-mbl').owlCarousel({
				stagePadding: 0,/*the little visible images at the end of the carousel*/
				dots: false,
				rtl: false,
				loop: false,
				margin: 15,
				lazyload: true,
				responsive: {
					0: {
						items: 1.2
					},

				},
				nav: false,
			});

			$('.products-wrap').owlCarousel({
				stagePadding: 0,/*the little visible images at the end of the carousel*/
				dots: false,
				rtl: false,
				loop: false,
				margin: 20,
				lazyload: true,
				responsive: {
					0: {
						items: 1,
					},
					1000: {
						items: 4
					},

				},
				nav: true,
				navText: ["<span class='pad-10 bg-white-imp'><img src='https://image.flaticon.com/icons/svg/481/481130.svg' style='height:20px'></span>", "<span class='pad-10 bg-white-imp'><img src='https://image.flaticon.com/icons/svg/481/481127.svg' style='height:20px'></span>"]
			});
		});

		$(document).ready(function () {
			$("#offer-card").on("click", function () {
				var targetModal = $('#book-item-modal');
				targetModal.modal({
					'show': true,
					'keyboard': true
				});
			});
		});

		$('.enquiry-details-submit').click(function (e) {
			var cityUrl = '{{getCityUrl()}}';

			e.preventDefault();
			var url = '/' + cityUrl + '/book';
			var data = {
				'name': $('.lead-details-name').val(),
				'phone': $('.lead-details-phone').val(),
				'callingCode': '+91',
				'email': $('.lead-details-email').val(),
				'partyDate': $('.lead-details-party-date').val(),
				'comments': $('.lead-details-comments').val(),
				'accepts': 'yes',
				'passiveCity': 'NA',
				'isVDayLandingPage': '0',
				'isOccasionHomePage': '1',
				'pageUrl': $("#pageUrl").val(),
			};
			$('#btnSubmitFormHome').prop('disabled', true);
			$('.errors-cnt').addClass('hide');
			$("#form-content").addClass("hide");
			window.showLoading();

			$.ajax({
				url: url,
				type: 'POST',
				dataType: 'json',
				data: data,
				success: function (data) {
					window.hideLoading();
					$("#btnSubmitFormHome").removeAttr('disabled');
					if (data.success) {
						$("#form-content").addClass("hide");
						$(".offer-signup-thankyou").removeClass("hide");
					} else {
						var errors = data.errors;
						window.showNotyError(errors);
						$("#form-content").removeClass("hide");
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					$("#btnSubmitFormHome").removeAttr('disabled');
					$('.errors-cnt').removeClass('hide').text("Sorry, your request could not be saved. Please try again later.");
					window.hideLoading();
					window.notifyTeam({
						"url": url,
						"textStatus": textStatus,
						"errorThrown": errorThrown
					});
				}
			});
		});
	</script>
@endsection