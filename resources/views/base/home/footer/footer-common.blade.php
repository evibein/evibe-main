<?php
$ref = "footer";
if (isset($refKeys) && is_array($refKeys) && array_key_exists('companyFooterRefKey', $refKeys))
{
	$ref = $refKeys['companyFooterRefKey'];
}
$currentUrlPath = request()->path();
?>
@if($currentUrlPath && is_int(strpos($currentUrlPath, "virtual-party/")))
	<!-- do not show city footer -->
	{{-- @todo: Optimise, already have seperate file  to hide city footer --}}
@else
	@if (!($agent->isMobile()))
		<div class="pad-t-20 prod-directory">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 footer-links-list pad-l-30 pad-r-30" style="word-break: break-word">
				<div class="cat-title mar-n-l-15">
					<h5 class="mar-t-30 no-mar-b pad-b-5">Bangalore</h5>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">For Kids</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">
						<a href="/bangalore/birthday-party-planners/birthday-party-decorations/all?ref=footer" class="no-mar">All Decors</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/birthday-party-decorations/c/simple-balloon-decorations?ref=footer" class="no-mar ">Simple Balloon Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/birthday-party-decorations/c/simple-theme-decorations?ref=footer" class="no-mar ">Simple Theme Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/birthday-party-decorations/c/custom-theme-decorations?ref=footer" class="no-mar ">Custom Theme Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/birthday-party-decorations/c/custom-balloon-decorations?ref=footer" class="no-mar ">Custom Balloon Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/birthday-party-decorations/c/elegant-theme-decorations?ref=footer" class="no-mar ">Elegant Theme Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/birthday-party-decorations/c/crafts-decors?ref=footer" class="no-mar ">Craft Decors
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/cakes?ref=footer" class="no-mar ">All Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/cakes?category=standard&amp;ref=footer" class="no-mar ">Standard Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/cakes?category=fondant&amp;ref=footer" class="no-mar ">Fondant Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/cakes?category=photo&amp;ref=footer" class="no-mar ">Photo Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/cakes?category=cream&amp;ref=footer" class="no-mar ">Cream Cakes
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/entertainment-activities-stalls?ref=footer" class="no-mar ">Entertainment
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/entertainment-activities-stalls/c/eateries?ref=footer" class="no-mar ">Eateries
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/entertainment-activities-stalls/c/game-stalls?ref=footer" class="no-mar ">Game Stalls
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/entertainment-activities-stalls/c/entertainers?ref=footer" class="no-mar ">Entertainers
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/entertainment-activities-stalls/c/artists?ref=footer" class="no-mar ">Artists
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/entertainment-activities-stalls/c/rentals?ref=footer" class="no-mar ">Rentals
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/entertainment-activities-stalls/c/photo-video?ref=footer" class="no-mar ">Photo / Video
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/entertainment-activities-stalls/c/audio-light?ref=footer" class="no-mar ">Sound / Light
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/entertainment-activities-stalls/c/new-trends?ref=footer" class="no-mar "> New Trends
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/entertainment-activities-stalls/c/mascots?ref=footer" class="no-mar ">Mascots
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/packages?ref=footer" class="no-mar ">All Packages
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/packages?category=photography&amp;ref=footer" class="no-mar ">Photography
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/packages?category=entertainment-packages&amp;ref=footer" class="no-mar ">Just Entertainment
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/packages?category=handpicked-packages&amp;ref=footer" class="no-mar ">Handpicked Packages
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/food?ref=footer" class="no-mar ">Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/food?category=north-indian&amp;ref=footer" class="no-mar ">North Indian Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/food?category=south-indian&amp;ref=footer" class="no-mar ">South Indian Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/food?category=italian&amp;ref=footer" class="no-mar ">Italian Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/food?category=gujarati&amp;ref=footer" class="no-mar ">Gujarati Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/food?category=bengali&amp;ref=footer" class="no-mar ">Bengali Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/food?category=chinese&amp;ref=footer" class="no-mar ">Chinese Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/food?category=andhra&amp;ref=footer" class="no-mar ">Andhra Food
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/bangalore/house-warming/tents?ref=footer" class="no-mar ">Tents
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/bangalore/house-warming/add-ons?ref=footer" class="no-mar ">Trends
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/collections?ref=footer" class="no-mar ">All Collections
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/venues?ref=footer" class="no-mar ">Venues
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">For Couple</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">

						<a href="/bangalore/candle-light-dinner?ref=footer" class="no-mar">Candle Light Dinner</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/bangalore/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=cake-cutting-places&amp;ref=footer" class="no-mar">Cake Cutting Places</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/bangalore/h/couple-surprises?ref=footer" class="no-mar"> All Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&amp;ref=footer" class="no-mar ">Unique Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=home-decorations&amp;ref=footer" class="no-mar ">Home Decorations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=customised&amp;ref=footer" class="no-mar ">Customised Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=adventure&amp;ref=footer" class="no-mar ">Adventure Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=regular-surprise&amp;ref=footer" class="no-mar ">Regular Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=romantic&amp;ref=footer" class="no-mar ">Romantic Surprises</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">For Youth</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">
						<a href="/bangalore/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=adventure&amp;ref=footer" class="no-mar ">Adventure Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&amp;ref=footer" class="no-mar ">Unique Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=home-decorations&amp;ref=footer" class="no-mar ">Home Surprises</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">For Occasion</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">
						<a href="/u/wedding-decors-in-bangalore?ref=footer" class="no-mar">Home Weddings</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/house-warming/decorations/all?ref=footer" class="no-mar">House Warming Decorations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/house-warming/food?ref=footer" class="no-mar ">Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/house-warming/cakes?ref=footer" class="no-mar ">All Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/house-warming/priest?ref=footer" class="no-mar ">Priests
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/house-warming/tents?ref=footer" class="no-mar ">Tents
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/bangalore/naming-ceremony/decorations?ref=footer" class="no-mar ">Naming Ceremony Decorations
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/bangalore/baby-shower/decorations?ref=footer" class="no-mar ">Baby Shower Decorations
						</a>


					</div>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">Collections</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">
						<a href="/bangalore/birthday-party-planners/collections/1491563558-Home-balloon-decorations?ref=footer" class="no-mar no-pad-r-imp font-12 no-pad-t">Home Balloon Decorations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/collections/1491977136-Naming-Ceremony-Decorations?ref=footer" class="no-mar no-pad-r-imp font-12 no-pad-t"> Naming Ceremony Decorations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/collections/1498047173-Home-birthday-party-packages?ref=footer" class="no-mar "> Home Birthday Party</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/collections/1491569864-Kids-birthday-snack-boxes?ref=footer" class="no-mar "> Kids Birthday Snack Boxes</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/collections/1491648346-Baby-Shower-Decorations?ref=footer" class="no-mar "> Baby Shower Decorations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/collections/1492086680-Trending-Cakes-for-birthday?ref=footer" class="no-mar "> Trending Cakes For Birthday</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/collections/1492091586-First-birthday-Theme-cake-Inspirations?ref=footer" class="no-mar ">First Birthday Theme Cake Inspirations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/collections/1493129211-Simple-shop-opening-Decorations?ref=footer" class="no-mar "> Simple Shop Opening Decorations</a>

					</div>
				</div>
				<div class="cat-title mar-n-l-15">
					<h5 class="mar-t-30 no-mar-b pad-b-5">Hyderabad</h5>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">For Kids</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">
						<a href="/hyderabad/birthday-party-planners/birthday-party-decorations/all?ref=footer" class="no-mar">All Decors</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/birthday-party-decorations/c/simple-balloon-decorations?ref=footer" class="no-mar ">Simple Balloon Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/birthday-party-decorations/c/simple-theme-decorations?ref=footer" class="no-mar ">Simple Theme Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/birthday-party-decorations/c/custom-theme-decorations?ref=footer" class="no-mar ">Custom Theme Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/birthday-party-decorations/c/custom-balloon-decorations?ref=footer" class="no-mar ">Custom Balloon Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/birthday-party-decorations/c/elegant-theme-decorations?ref=footer" class="no-mar ">Elegant Theme Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/birthday-party-decorations/c/crafts-decors?ref=footer" class="no-mar ">Craft Decors
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/cakes?ref=footer" class="no-mar ">All Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/cakes?category=standard&amp;ref=footer" class="no-mar ">Standard Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/cakes?category=fondant&amp;ref=footer" class="no-mar ">Fondant Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/cakes?category=photo&amp;ref=footer" class="no-mar ">Photo Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/cakes?category=cream&amp;ref=footer" class="no-mar ">Cream Cakes
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/entertainment-activities-stalls?ref=footer" class="no-mar ">Entertainment
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/entertainment-activities-stalls/c/eateries?ref=footer" class="no-mar ">Eateries
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/entertainment-activities-stalls/c/game-stalls?ref=footer" class="no-mar ">Game Stalls
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/entertainment-activities-stalls/c/entertainers?ref=footer" class="no-mar ">Entertainers
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/entertainment-activities-stalls/c/artists?ref=footer" class="no-mar ">Artists
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/entertainment-activities-stalls/c/rentals?ref=footer" class="no-mar ">Rentals
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/entertainment-activities-stalls/c/photo-video?ref=footer" class="no-mar ">Photo / Video
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/entertainment-activities-stalls/c/audio-light?ref=footer" class="no-mar ">Sound / Light
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/entertainment-activities-stalls/c/new-trends?ref=footer" class="no-mar "> New Trends
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/entertainment-activities-stalls/c/mascots?ref=footer" class="no-mar ">Mascots
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/packages?ref=footer" class="no-mar ">All Packages
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/packages?category=photography&amp;ref=footer" class="no-mar ">Photography
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/packages?category=entertainment-packages&amp;ref=footer" class="no-mar ">Just Entertainment
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/packages?category=handpicked-packages&amp;ref=footer" class="no-mar ">Handpicked Packages
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/food?ref=footer" class="no-mar ">Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/food?category=north-indian&amp;ref=footer" class="no-mar ">North Indian Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/food?category=south-indian&amp;ref=footer" class="no-mar ">South Indian Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/food?category=italian&amp;ref=footer" class="no-mar ">Italian Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/food?category=gujarati&amp;ref=footer" class="no-mar ">Gujarati Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/food?category=bengali&amp;ref=footer" class="no-mar ">Bengali Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/food?category=chinese&amp;ref=footer" class="no-mar ">Chinese Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/food?category=andhra&amp;ref=footer" class="no-mar ">Andhra Food
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/tents?ref=footer" class="no-mar ">Tents
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/trends?ref=footer" class="no-mar ">Trends
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/collections?ref=footer" class="no-mar ">All Collections
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/venues?ref=footer" class="no-mar ">Venues
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">For Couple</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">

						<a href="/hyderabad/candle-light-dinner?ref=footer" class="no-mar  ">Candle Light Dinner</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/hyderabad/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=cake-cutting-places&amp;ref=footer" class="no-mar  ">Cake Cutting Places</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/hyderabad/h/couple-surprises?ref=footer" class="no-mar ">All Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&amp;ref=footer" class="no-mar ">Unique Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=home-decorations&amp;ref=footer" class="no-mar ">Home Decorations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=customised&amp;ref=footer" class="no-mar ">Customised Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=adventure&amp;ref=footer" class="no-mar ">Adventure Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=regular-surprise&amp;ref=footer" class="no-mar ">Regular Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=romantic&amp;ref=footer" class="no-mar ">Romantic Surprises</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">For Youth</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">
						<a href="/hyderabad/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=adventure&amp;ref=footer" class="no-mar ">Adventure Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&amp;ref=footer" class="no-mar ">Unique Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=home-decorations&amp;ref=footer" class="no-mar ">Home Surprises</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">For Occasion</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">
						<a href="/u/wedding-decors-in-hyderabad?ref=footer" class="no-mar">Home Weddings</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/house-warming/decorations/all?ref=footer" class="no-mar">House Warming Decorations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/house-warming/food?ref=footer" class="no-mar ">Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/house-warming/cakes?ref=footer" class="no-mar ">All Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/house-warming/priest?ref=footer" class="no-mar ">Priests
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/house-warming/tents?ref=footer" class="no-mar ">Tents
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/hyderabad/naming-ceremony/decorations?ref=footer" class="no-mar ">Naming Ceremony Decorations
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/hyderabad/baby-shower/decorations?ref=footer" class="no-mar ">Baby Shower Decorations
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">Collections</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">
						<a href="/hyderabad/birthday-party-planners/collections/1500873099-Home-balloon-decorations?ref=footer" class="no-mar no-pad-r-imp font-12 no-pad-t">Home Balloon Decorations</a>

						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/collections/1500899352-First-birthday-Theme-cake-Inspirations?ref=home-collections" class="no-mar "> Kids Birthday Snack Boxes</a>

						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/collections/1500899352-First-birthday-Theme-cake-Inspirations?ref=footer" class="no-mar ">First Birthday Theme Cake Inspirations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/collections/1500900808-Simple-shop-opening-Decorations?ref=footer" class="no-mar "> Simple Shop Opening Decorations</a>

					</div>
				</div>

				<div class="cat-title mar-n-l-15">
					<h5 class="mar-t-30 no-mar-b pad-b-5">Chennai</h5>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">For Kids</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">
						<a href="/chennai/birthday-party-planners/birthday-party-decorations/all?ref=footer" class="no-mar">All Decors</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/birthday-party-decorations/c/simple-balloon-decorations?ref=footer" class="no-mar ">Simple Balloon Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/birthday-party-decorations/c/simple-theme-decorations?ref=footer" class="no-mar ">Simple Theme Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/birthday-party-decorations/c/custom-theme-decorations?ref=footer" class="no-mar ">Custom Theme Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/birthday-party-decorations/c/custom-balloon-decorations?ref=footer" class="no-mar ">Custom Balloon Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/birthday-party-decorations/c/elegant-theme-decorations?ref=footer" class="no-mar ">Elegant Theme Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/birthday-party-decorations/c/crafts-decors?ref=footer" class="no-mar ">Craft Decors
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/chennai/birthday-party-planners/cakes?ref=footer" class="no-mar ">All Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/cakes?category=standard&amp;ref=footer" class="no-mar ">Standard Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/cakes?category=fondant&amp;ref=footer" class="no-mar ">Fondant Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/cakes?category=photo&amp;ref=footer" class="no-mar ">Photo Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/cakes?category=cream&amp;ref=footer" class="no-mar ">Cream Cakes
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/chennai/birthday-party-planners/entertainment-activities-stalls?ref=footer" class="no-mar ">Entertainment
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/entertainment-activities-stalls/c/eateries?ref=footer" class="no-mar ">Eateries
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/entertainment-activities-stalls/c/game-stalls?ref=footer" class="no-mar ">Game Stalls
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/entertainment-activities-stalls/c/entertainers?ref=footer" class="no-mar ">Entertainers
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/entertainment-activities-stalls/c/artists?ref=footer" class="no-mar ">Artists
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/entertainment-activities-stalls/c/rentals?ref=footer" class="no-mar ">Rentals
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/entertainment-activities-stalls/c/photo-video?ref=footer" class="no-mar ">Photo / Video
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/entertainment-activities-stalls/c/audio-light?ref=footer" class="no-mar ">Sound / Light
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/entertainment-activities-stalls/c/new-trends?ref=footer" class="no-mar "> New Trends
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/entertainment-activities-stalls/c/mascots?ref=footer" class="no-mar ">Mascots
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/chennai/birthday-party-planners/packages?ref=footer" class="no-mar ">All Packages
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/packages?category=photography&amp;ref=footer" class="no-mar ">Photography
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/packages?category=entertainment-packages&amp;ref=footer" class="no-mar ">Just Entertainment
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/packages?category=handpicked-packages&amp;ref=footer" class="no-mar ">Handpicked Packages
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/food?ref=footer" class="no-mar ">Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/food?category=north-indian&amp;ref=footer" class="no-mar ">North Indian Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/food?category=south-indian&amp;ref=footer" class="no-mar ">South Indian Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/food?category=italian&amp;ref=footer" class="no-mar ">Italian Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/food?category=gujarati&amp;ref=footer" class="no-mar ">Gujarati Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/food?category=bengali&amp;ref=footer" class="no-mar ">Bengali Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/food?category=chinese&amp;ref=footer" class="no-mar ">Chinese Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/food?category=andhra&amp;ref=footer" class="no-mar ">Andhra Food
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/chennai/birthday-party-planners/tents?ref=footer" class="no-mar ">Tents
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/chennai/birthday-party-planners/trends?ref=footer" class="no-mar ">Trends
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/chennai/birthday-party-planners/collections?ref=footer" class="no-mar ">All Collections
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/chennai/birthday-party-planners/venues?ref=footer" class="no-mar ">Venues
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">For Couple</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">

						<a href="/chennai/candle-light-dinner?ref=footer" class="no-mar  ">Candle Light Dinner</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/chennai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=cake-cutting-places&amp;ref=footer" class="no-mar  ">Cake Cutting Places</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/chennai/h/couple-surprises?ref=footer" class="no-mar">All Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&amp;ref=footer" class="no-mar ">Unique Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=home-decorations&amp;ref=footer" class="no-mar ">Home Decorations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=customised&amp;ref=footer" class="no-mar ">Customised Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=adventure&amp;ref=footer" class="no-mar ">Adventure Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=regular-surprise&amp;ref=footer" class="no-mar ">Regular Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=romantic&amp;ref=footer" class="no-mar ">Romantic Surprises</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">For Youth</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">

						<a href="/chennai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=adventure&amp;ref=footer" class="no-mar ">Adventure Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&amp;ref=footer" class="no-mar ">Unique Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=home-decorations&amp;ref=footer" class="no-mar ">Home Surprises</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">For Occasion</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">
						<a href="/chennai/house-warming/decorations/all?ref=footer" class="no-mar">House Warming Decorations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/house-warming/food?ref=footer" class="no-mar ">Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/house-warming/cakes?ref=footer" class="no-mar ">All Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/house-warming/priest?ref=footer" class="no-mar ">Priests
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/house-warming/tents?ref=footer" class="no-mar ">Tents
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/chennai/naming-ceremony/decorations?ref=footer" class="no-mar ">Naming Ceremony Decorations
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/chennai/baby-shower/decorations?ref=footer" class="no-mar ">Baby Shower Decorations
						</a>
					</div>
				</div>

				<div class="cat-title mar-n-l-15">
					<h5 class="mar-t-30 no-mar-b pad-b-5">Pune</h5>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">For Kids</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">
						<a href="/pune/birthday-party-planners/birthday-party-decorations/all?ref=footer" class="no-mar">All Decors</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/birthday-party-decorations/c/simple-balloon-decorations?ref=footer" class="no-mar ">Simple Balloon Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/birthday-party-decorations/c/simple-theme-decorations?ref=footer" class="no-mar ">Simple Theme Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/birthday-party-decorations/c/custom-theme-decorations?ref=footer" class="no-mar ">Custom Theme Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/birthday-party-decorations/c/custom-balloon-decorations?ref=footer" class="no-mar ">Custom Balloon Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/birthday-party-decorations/c/elegant-theme-decorations?ref=footer" class="no-mar ">Elegant Theme Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/birthday-party-decorations/c/crafts-decors?ref=footer" class="no-mar ">Craft Decors
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/pune/birthday-party-planners/cakes?ref=footer" class="no-mar ">All Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/cakes?category=standard&amp;ref=footer" class="no-mar ">Standard Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/cakes?category=fondant&amp;ref=footer" class="no-mar ">Fondant Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/cakes?category=photo&amp;ref=footer" class="no-mar ">Photo Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/cakes?category=cream&amp;ref=footer" class="no-mar ">Cream Cakes
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/pune/birthday-party-planners/entertainment-activities-stalls?ref=footer" class="no-mar ">Entertainment
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/entertainment-activities-stalls/c/eateries?ref=footer" class="no-mar ">Eateries
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/entertainment-activities-stalls/c/game-stalls?ref=footer" class="no-mar ">Game Stalls
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/entertainment-activities-stalls/c/entertainers?ref=footer" class="no-mar ">Entertainers
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/entertainment-activities-stalls/c/artists?ref=footer" class="no-mar ">Artists
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/entertainment-activities-stalls/c/rentals?ref=footer" class="no-mar ">Rentals
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/entertainment-activities-stalls/c/photo-video?ref=footer" class="no-mar ">Photo / Video
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/entertainment-activities-stalls/c/audio-light?ref=footer" class="no-mar ">Sound / Light
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/entertainment-activities-stalls/c/new-trends?ref=footer" class="no-mar "> New Trends
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/entertainment-activities-stalls/c/mascots?ref=footer" class="no-mar ">Mascots
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/pune/birthday-party-planners/packages?ref=footer" class="no-mar ">All Packages
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/packages?category=photography&amp;ref=footer" class="no-mar ">Photography
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/packages?category=entertainment-packages&amp;ref=footer" class="no-mar ">Just Entertainment
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/packages?category=handpicked-packages&amp;ref=footer" class="no-mar ">Handpicked Packages
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/food?ref=footer" class="no-mar ">Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/food?category=north-indian&amp;ref=footer" class="no-mar ">North Indian Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/food?category=south-indian&amp;ref=footer" class="no-mar ">South Indian Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/food?category=italian&amp;ref=footer" class="no-mar ">Italian Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/food?category=gujarati&amp;ref=footer" class="no-mar ">Gujarati Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/food?category=bengali&amp;ref=footer" class="no-mar ">Bengali Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/food?category=chinese&amp;ref=footer" class="no-mar ">Chinese Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/food?category=andhra&amp;ref=footer" class="no-mar ">Andhra Food
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/pune/birthday-party-planners/tents?ref=footer" class="no-mar ">Tents
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/pune/birthday-party-planners/trends?ref=footer" class="no-mar ">Trends
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/pune/birthday-party-planners/collections?ref=footer" class="no-mar ">All Collections
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/pune/birthday-party-planners/venues?ref=footer" class="no-mar ">Venues
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">For Couple</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">

						<a href="/pune/candle-light-dinner?ref=footer" class="no-mar ">Candle Light Dinner</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/pune/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=cake-cutting-places&amp;ref=footer" class="no-mar  ">Cake Cutting Places</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/pune/h/couple-surprises?ref=footer" class="no-mar  ">All Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&amp;ref=footer" class="no-mar ">Unique Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=home-decorations&amp;ref=footer" class="no-mar ">Home Decorations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=customised&amp;ref=footer" class="no-mar ">Customised Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=adventure&amp;ref=footer" class="no-mar ">Adventure Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=regular-surprise&amp;ref=footer" class="no-mar ">Regular Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=romantic&amp;ref=footer" class="no-mar ">Romantic Surprises</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">For Youth</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">
						<a href="/pune/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=adventure&amp;ref=footer" class="no-mar ">Adventure Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&amp;ref=footer" class="no-mar ">Unique Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=home-decorations&amp;ref=footer" class="no-mar ">Home Surprises</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">For Occasion</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">
						<a href="/pune/house-warming/decorations/all?ref=footer" class="no-mar">House Warming Decorations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/house-warming/food?ref=footer" class="no-mar ">Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/house-warming/cakes?ref=footer" class="no-mar ">All Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/house-warming/priest?ref=footer" class="no-mar ">Priests
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/house-warming/tents?ref=footer" class="no-mar ">Tents
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/pune/naming-ceremony/decorations?ref=footer" class="no-mar ">Naming Ceremony Decorations
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/pune/baby-shower/decorations?ref=footer" class="no-mar ">Baby Shower Decorations
						</a>
					</div>
				</div>

				<div class="cat-title mar-n-l-15">
					<h5 class="mar-t-30 no-mar-b pad-b-5">Delhi</h5>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">For Kids</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">
						<a href="/delhi/birthday-party-planners/birthday-party-decorations/all?ref=footer" class="no-mar">All Decors</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/birthday-party-decorations/c/simple-balloon-decorations?ref=footer" class="no-mar ">Simple Balloon Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/birthday-party-decorations/c/simple-theme-decorations?ref=footer" class="no-mar ">Simple Theme Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/birthday-party-decorations/c/custom-theme-decorations?ref=footer" class="no-mar ">Custom Theme Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/birthday-party-decorations/c/custom-balloon-decorations?ref=footer" class="no-mar ">Custom Balloon Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/birthday-party-decorations/c/elegant-theme-decorations?ref=footer" class="no-mar ">Elegant Theme Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/birthday-party-decorations/c/crafts-decors?ref=footer" class="no-mar ">Craft Decors
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/delhi/birthday-party-planners/cakes?ref=footer" class="no-mar ">All Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/cakes?category=standard&amp;ref=footer" class="no-mar ">Standard Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/cakes?category=fondant&amp;ref=footer" class="no-mar ">Fondant Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/cakes?category=photo&amp;ref=footer" class="no-mar ">Photo Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/cakes?category=cream&amp;ref=footer" class="no-mar ">Cream Cakes
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/delhi/birthday-party-planners/entertainment-activities-stalls?ref=footer" class="no-mar ">Entertainment
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/entertainment-activities-stalls/c/eateries?ref=footer" class="no-mar ">Eateries
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/entertainment-activities-stalls/c/game-stalls?ref=footer" class="no-mar ">Game Stalls
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/entertainment-activities-stalls/c/entertainers?ref=footer" class="no-mar ">Entertainers
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/entertainment-activities-stalls/c/artists?ref=footer" class="no-mar ">Artists
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/entertainment-activities-stalls/c/rentals?ref=footer" class="no-mar ">Rentals
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/entertainment-activities-stalls/c/photo-video?ref=footer" class="no-mar ">Photo / Video
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/entertainment-activities-stalls/c/audio-light?ref=footer" class="no-mar ">Sound / Light
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/entertainment-activities-stalls/c/new-trends?ref=footer" class="no-mar "> New Trends
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/entertainment-activities-stalls/c/mascots?ref=footer" class="no-mar ">Mascots
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/delhi/birthday-party-planners/packages?ref=footer" class="no-mar ">All Packages
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/packages?category=photography&amp;ref=footer" class="no-mar ">Photography
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/packages?category=entertainment-packages&amp;ref=footer" class="no-mar ">Just Entertainment
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/packages?category=handpicked-packages&amp;ref=footer" class="no-mar ">Handpicked Packages
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/delhi/birthday-party-planners/food?ref=footer" class="no-mar ">Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/food?category=north-indian&amp;ref=footer" class="no-mar ">North Indian Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/food?category=south-indian&amp;ref=footer" class="no-mar ">South Indian Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/food?category=italian&amp;ref=footer" class="no-mar ">Italian Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/food?category=gujarati&amp;ref=footer" class="no-mar ">Gujarati Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/food?category=bengali&amp;ref=footer" class="no-mar ">Bengali Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/food?category=chinese&amp;ref=footer" class="no-mar ">Chinese Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/food?category=andhra&amp;ref=footer" class="no-mar ">Andhra Food
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/delhi/birthday-party-planners/tents?ref=footer" class="no-mar ">Tents
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/delhi/birthday-party-planners/trends?ref=footer" class="no-mar ">Trends
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/collections?ref=footer" class="no-mar ">All Collections
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/delhi/birthday-party-planners/venues?ref=footer" class="no-mar ">Venues
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">For Couple</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">

						<a href="/delhi/candle-light-dinner?ref=footer" class="no-mar">Candle Light Dinner</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/delhi/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=cake-cutting-places&amp;ref=footer" class="no-mar  ">Cake Cutting Places</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/delhi/h/couple-surprises?ref=footer" class="no-mar"> All Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&amp;ref=footer" class="no-mar ">Unique Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=home-decorations&amp;ref=footer" class="no-mar ">Home Decorations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=customised&amp;ref=footer" class="no-mar ">Customised Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=adventure&amp;ref=footer" class="no-mar ">Adventure Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=regular-surprise&amp;ref=footer" class="no-mar ">Regular Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=romantic&amp;ref=footer" class="no-mar ">Romantic Surprises</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">For Youth</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">
						<a href="/delhi/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=adventure&amp;ref=footer" class="no-mar ">Adventure Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&amp;ref=footer" class="no-mar ">Unique Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=home-decorations&amp;ref=footer" class="no-mar ">Home Surprises</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">For Occasion</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">
						<a href="/delhi/house-warming/decorations/all?ref=footer" class="no-mar">House Warming Decorations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/house-warming/food?ref=footer" class="no-mar ">Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/house-warming/cakes?ref=footer" class="no-mar ">All Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/house-warming/priest?ref=footer" class="no-mar ">Priests
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/house-warming/tents?ref=footer" class="no-mar ">Tents
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/delhi/naming-ceremony/decorations?ref=footer" class="no-mar ">Naming Ceremony Decorations
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/delhi/baby-shower/decorations?ref=footer" class="no-mar ">Baby Shower Decorations
						</a>
					</div>
				</div>

				<div class="cat-title mar-n-l-15">
					<h5 class="mar-t-30 no-mar-b pad-b-5">Mumbai</h5>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">For Kids</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">
						<a href="/mumbai/birthday-party-planners/birthday-party-decorations/all?ref=footer" class="no-mar">All Decors</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/birthday-party-decorations/c/simple-balloon-decorations?ref=footer" class="no-mar ">Simple Balloon Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/birthday-party-decorations/c/simple-theme-decorations?ref=footer" class="no-mar ">Simple Theme Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/birthday-party-decorations/c/custom-theme-decorations?ref=footer" class="no-mar ">Custom Theme Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/birthday-party-decorations/c/custom-balloon-decorations?ref=footer" class="no-mar ">Custom Balloon Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/birthday-party-decorations/c/elegant-theme-decorations?ref=footer" class="no-mar ">Elegant Theme Decorations
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/birthday-party-decorations/c/crafts-decors?ref=footer" class="no-mar ">Craft Decors
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/cakes?ref=footer" class="no-mar ">All Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/cakes?category=standard&amp;ref=footer" class="no-mar ">Standard Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/cakes?category=fondant&amp;ref=footer" class="no-mar ">Fondant Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/cakes?category=photo&amp;ref=footer" class="no-mar ">Photo Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/cakes?category=cream&amp;ref=footer" class="no-mar ">Cream Cakes
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/entertainment-activities-stalls?ref=footer" class="no-mar ">Entertainment
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/entertainment-activities-stalls/c/eateries?ref=footer" class="no-mar ">Eateries
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/entertainment-activities-stalls/c/game-stalls?ref=footer" class="no-mar ">Game Stalls
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/entertainment-activities-stalls/c/entertainers?ref=footer" class="no-mar ">Entertainers
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/entertainment-activities-stalls/c/artists?ref=footer" class="no-mar ">Artists
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/entertainment-activities-stalls/c/rentals?ref=footer" class="no-mar ">Rentals
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/entertainment-activities-stalls/c/photo-video?ref=footer" class="no-mar ">Photo / Video
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/entertainment-activities-stalls/c/audio-light?ref=footer" class="no-mar ">Sound / Light
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/entertainment-activities-stalls/c/new-trends?ref=footer" class="no-mar "> New Trends
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/entertainment-activities-stalls/c/mascots?ref=footer" class="no-mar ">Mascots
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/packages?ref=footer" class="no-mar ">All Packages
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/packages?category=photography&amp;ref=footer" class="no-mar ">Photography
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/packages?category=entertainment-packages&amp;ref=footer" class="no-mar ">Just Entertainment
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/packages?category=handpicked-packages&amp;ref=footer" class="no-mar ">Handpicked Packages
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/food?ref=food" class="no-mar ">Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/food?category=north-indian&amp;ref=footer" class="no-mar ">North Indian Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/food?category=south-indian&amp;ref=footer" class="no-mar ">South Indian Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/food?category=italian&amp;ref=footer" class="no-mar ">Italian Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/food?category=gujarati&amp;ref=footer" class="no-mar ">Gujarati Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/food?category=bengali&amp;ref=footer" class="no-mar ">Bengali Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/food?category=chinese&amp;ref=footer" class="no-mar ">Chinese Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/food?category=andhra&amp;ref=footer" class="no-mar ">Andhra Food
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/tents?ref=footer" class="no-mar ">Tents
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/trends?ref=footer" class="no-mar ">Trends
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/collections?ref=footer" class="no-mar ">All Collections
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/venues?ref=footer" class="no-mar ">Venues
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">For Couple</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">

						<a href="/mumbai/candle-light-dinner?ref=footer" class="no-mar  ">Candle Light Dinner</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/mumbai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=cake-cutting-places&amp;ref=footer" class="no-mar  ">Cake Cutting Places</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/mumbai/h/couple-surprises?ref=footer" class="no-mar"> All Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&amp;ref=footer" class="no-mar ">Unique Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=home-decorations&amp;ref=footer" class="no-mar ">Home Decorations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=customised&amp;ref=footer" class="no-mar ">Customised Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=adventure&amp;ref=footer" class="no-mar ">Adventure Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=regular-surprise&amp;ref=footer" class="no-mar ">Regular Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=romantic&amp;ref=footer" class="no-mar ">Romantic Surprises</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">For Youth</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">
						<a href="/mumbai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=adventure&amp;ref=footer" class="no-mar ">Adventure Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&amp;ref=footer" class="no-mar ">Unique Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=home-decorations&amp;ref=footer" class="no-mar ">Home Surprises</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2 col-md-1 col-lg-1 no-pad-l no-pad-r-imp">
						<span class="font-12-imp text-white-imp no-pad-l ">For Occasion</span>
					</div>
					<div class="col-sm-10 col-md-11 col-lg-11">
						<a href="/mumbai/house-warming/decorations/all?ref=footer" class="no-mar">House Warming Decorations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/house-warming/food?ref=footer" class="no-mar ">Food
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/house-warming/cakes?ref=footer" class="no-mar ">All Cakes
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/house-warming/priest?ref=footer" class="no-mar ">Priests
						</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/house-warming/tents?ref=footer" class="no-mar ">Tents
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/mumbai/naming-ceremony/decorations?ref=footer" class="no-mar ">Naming Ceremony Decorations
						</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/mumbai/baby-shower/decorations?ref=footer" class="no-mar ">Baby Shower Decorations
						</a>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	@else
		<div class="pad-t-20 prod-directory prod-directory-mbl col-xs-12 col-sm-12 pad-l-15 pad-r-15 mar-b-20" style="word-break: break-word">
			<div class="text-white-imp dropdown-btn" data-toggle="collapse" href="bangalore-dropdown" aria-expanded="false" aria-controls="bangalore-dropdown">
				<h5 class="text-bold">Bangalore
					<span class="pull-right no-pad-r-imp">
					<i class="glyphicon glyphicon-menu-down"></i>
				</span>
				</h5>
			</div>
			<div class="collapse footer-dropdown-mobile" id="bangalore-dropdown">
				<div class=" row">
					<div class="col-xs-4 col-sm-4">
						<span class="text-white-imp no-pad-l">Birthdays</span>
					</div>
					<div class="col-xs-8 col-sm-8">
						<a href="/bangalore/birthday-party-planners/birthday-party-decorations/all?ref=footer" class="no-mar"> Decorations</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/cakes?ref=footer" class=" no-mar no-pad-l ">Cakes</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/entertainment-activities-stalls?ref=footer" class="no-mar "> Entertainment</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/packages?ref=footer" class="no-mar "> Packages</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/food?ref=footer" class="no-mar "> Food</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/tents?ref=footer" class="no-mar ">Tents</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/trends?ref=footer" class="no-mar ">Trends</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/venues?ref=footer" class="no-mar ">Venues</a>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-sm-4">
						<span class="text-white-imp no-pad-l">For Couple</span>
					</div>
					<div class="col-xs-8 col-sm-8">
						<a href="/bangalore/candle-light-dinner?ref=footer" class="no-mar  ">Candle Light Dinner</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/bangalore/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=cake-cutting-places&amp;ref=footer" class="no-mar   ">Cake Cutting Places</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/bangalore/h/couple-surprises?ref=footer" class="no-mar"> All Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&amp;ref=footer" class="no-mar ">Unique Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=home-decorations&amp;ref=footer" class="no-mar ">Home Decorations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=customised&amp;ref=footer" class="no-mar ">Customised Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=adventure&amp;ref=footer" class="no-mar  ">Adventure Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=regular-surprise&amp;ref=footer" class="no-mar ">Regular Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=romantic&amp;ref=footer" class="no-mar ">Romantic Surprises</a>

					</div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-sm-4">
						<span class="text-white-imp  no-pad-l">For Occasion</span>
					</div>
					<div class="col-xs-8 col-sm-8">
						<a href="/u/wedding-decors-in-bangalore?ref=footer" class="no-mar">Home Weddings</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/house-warming/decorations/all?ref=footer" class="no-mar  "> House Warming </a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/house-warming/decorations/all?ref=footer" class="no-mar  "> Decorations </a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/house-warming/food?ref=footer" class="no-mar ">Food</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/house-warming/priest?ref=footer" class="no-mar ">Priest</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/house-warming/tents?ref=footer" class="no-mar ">Tent</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/house-warming/add-ons?ref=footer" class="no-mar ">Add-Ons</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/house-warming/cakes?ref=footer" class="no-mar ">Cakes</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/bangalore/naming-ceremony/decorations?ref=footer" class="no-mar  "> Naming Ceremony Decorations </a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/bangalore/baby-shower/decorations?ref=footer" class="no-mar  "> Baby shower Decorations </a>

					</div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-sm-4">
						<span class="text-white-imp  no-pad-l">Collections</span>
					</div>
					<div class="col-xs-8 col-sm-8">
						<a href="/bangalore/birthday-party-planners/collections/1491563558-Home-balloon-decorations" class="no-mar ">Home Balloon Decorations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/collections/1491977136-Naming-Ceremony-Decorations" class="no-mar "> Naming Ceremony Decorations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/collections/1498047173-Home-birthday-party-packages" class="no-mar "> Home Birthday Party</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/collections/1491569864-Kids-birthday-snack-boxes" class="no-mar "> Kids Birthday Snack Boxes</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/collections/1491648346-Baby-Shower-Decorations" class="no-mar "> Baby Shower Decorations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/collections/1492086680-Trending-Cakes-for-birthday" class=""> Trending Cakes For Birthday</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/collections/1492091586-First-birthday-Theme-cake-Inspirations" class=" ">First Birthday Theme Cake Inspirations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/bangalore/birthday-party-planners/collections/1493129211-Simple-shop-opening-Decorations" class=" "> Shop Opening Decorations</a>
					</div>
				</div>
			</div>

			<div class="text-white-imp dropdown-btn" data-toggle="collapse" href="hyderabad-dropdown" aria-expanded="false" aria-controls="hyderabad-dropdown">
				<h5 class=" text-bold">Hyderabad
					<span class="pull-right no-pad-r-imp">
					<i class="glyphicon glyphicon-menu-down"></i>
				</span>
				</h5>
			</div>
			<div class="collapse" id="hyderabad-dropdown">
				<div class=" row">
					<div class="col-xs-4 col-sm-4">
						<span class="text-white-imp  no-pad-l">Birthdays</span>
					</div>
					<div class="col-xs-8 col-sm-8">
						<a href="/hyderabad/birthday-party-planners/birthday-party-decorations/all?ref=footer" class=" no-mar "> Decorations</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/cakes?ref=footer" class=" no-mar no-pad-l ">Cakes</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/entertainment-activities-stalls?ref=footer" class="no-mar "> Entertainment</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/packages?ref=footer" class="no-mar "> Packages</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/food?ref=footer" class="no-mar "> Food</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/tents?ref=footer" class="no-mar ">Tents</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/trends?ref=footer" class="no-mar ">Trends</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/venues?ref=footer" class="no-mar ">Venues</a>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-sm-4">
						<span class="text-white-imp  no-pad-l">For Couple</span>
					</div>
					<div class="col-xs-8 col-sm-8">
						<a href="/hyderabad/candle-light-dinner?ref=footer" class="no-mar  ">Candle Light Dinner</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/hyderabad/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=cake-cutting-places&amp;ref=footer" class="no-mar   ">Cake Cutting Places</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/hyderabad/h/couple-surprises?ref=footer" class="no-mar"> All Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&amp;ref=footer" class="no-mar ">Unique Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=home-decorations&amp;ref=footer" class="no-mar ">Home Decorations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=customised&amp;ref=footer" class="no-mar ">Customised Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=adventure&amp;ref=footer" class="no-mar  ">Adventure Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=regular-surprise&amp;ref=footer" class="no-mar ">Regular Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=romantic&amp;ref=footer" class="no-mar ">Romantic Surprises</a>

					</div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-sm-4">
						<span class="text-white-imp  no-pad-l">For Occasion</span>
					</div>
					<div class="col-xs-8 col-sm-8">
						<a href="/u/wedding-decors-in-hyderabad?ref=footer" class="no-mar">Home Weddings</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/house-warming/decorations/all?ref=footer" class="no-mar  "> House Warming </a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/house-warming/decorations/all?ref=footer" class="no-mar  "> Decorations </a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/house-warming/food?ref=footer" class="no-mar ">Food</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/house-warming/priest?ref=footer" class="no-mar ">Priest</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/house-warming/tents?ref=footer" class="no-mar ">Tent</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/house-warming/add-ons?ref=footer" class="no-mar ">Add-Ons</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/house-warming/cakes?ref=footer" class="no-mar ">Cakes</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/hyderabad/naming-ceremony/decorations?ref=footer" class="no-mar  "> Naming Ceremony Decorations </a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/hyderabad/baby-shower/decorations?ref=footer" class="no-mar  "> Baby shower Decorations </a>

					</div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-sm-4">
						<span class="text-white-imp  no-pad-l">Collections</span>
					</div>
					<div class="col-xs-8 col-sm-8 ">
						<a href="/hyderabad/birthday-party-planners/collections/1500873099-Home-balloon-decorations?ref=footer" class="no-mar ">Home Balloon Decorations</a>

						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/collections/1500899352-First-birthday-Theme-cake-Inspirations?ref=home-collections" class="no-mar "> Kids Birthday Snack Boxes</a>

						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/collections/1500899352-First-birthday-Theme-cake-Inspirations?ref=footer" class="no-mar ">First Birthday Theme Cake Inspirations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/hyderabad/birthday-party-planners/collections/1500900808-Simple-shop-opening-Decorations?ref=footer" class="no-mar "> Simple Shop Opening Decorations</a>

					</div>
				</div>
			</div>

			<div class="text-white-imp dropdown-btn" data-toggle="collapse" href="mumbai-dropdown-dropdown" aria-expanded="false" aria-controls="mumbai-dropdown">
				<h5 class="text-bold">Mumbai
					<span class="pull-right no-pad-r-imp">
					<i class="glyphicon glyphicon-menu-down"></i>
				</span>
				</h5>
			</div>
			<div class="collapse" id="mumbai-dropdown">
				<div class=" row">
					<div class="col-xs-4 col-sm-4">
						<span class="text-white-imp  no-pad-l">Birthdays</span>
					</div>
					<div class="col-xs-8 col-sm-8">
						<a href="/mumbai/birthday-party-planners/birthday-party-decorations/all?ref=footer" class=" no-mar no-pad-l "> Decorations</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/cakes?ref=footer" class=" no-mar no-pad-l ">Cakes</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/entertainment-activities-stalls?ref=footer" class="no-mar "> Entertainment</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/packages?ref=footer" class="no-mar "> Packages</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/food?ref=footer" class="no-mar "> Food</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/tents?ref=footer" class="no-mar ">Tents</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/trends?ref=footer" class="no-mar ">Trends</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/birthday-party-planners/venues?ref=footer" class="no-mar ">Venues</a>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-sm-4">
						<span class="text-white-imp  no-pad-l">For Couple</span>
					</div>
					<div class="col-xs-8 col-sm-8">
						<a href="/mumbai/candle-light-dinner?ref=footer" class="no-mar  ">Candle Light Dinner</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/mumbai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=cake-cutting-places&amp;ref=footer" class="no-mar   ">Cake Cutting Places</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/mumbai/h/couple-surprises?ref=footer" class="no-mar"> All Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&amp;ref=footer" class="no-mar ">Unique Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=home-decorations&amp;ref=footer" class="no-mar ">Home Decorations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=customised&amp;ref=footer" class="no-mar ">Customised Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=adventure&amp;ref=footer" class="no-mar  ">Adventure Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=regular-surprise&amp;ref=footer" class="no-mar ">Regular Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=romantic&amp;ref=footer" class="no-mar ">Romantic Surprises</a>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-sm-4">
						<span class="text-white-imp  no-pad-l">For Occasion</span>
					</div>
					<div class="col-xs-8 col-sm-8">
						<a href="/mumbai/house-warming/decorations/all?ref=footer" class="no-mar  "> House Warming </a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/house-warming/decorations/all?ref=footer" class="no-mar  "> Decorations </a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/house-warming/food?ref=footer" class="no-mar ">Food</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/house-warming/priest?ref=footer" class="no-mar ">Priest</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/house-warming/tents?ref=footer" class="no-mar ">Tent</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/house-warming/add-ons?ref=footer" class="no-mar ">Add-Ons</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/mumbai/house-warming/cakes?ref=footer" class="no-mar ">Cakes</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/mumbai/naming-ceremony/decorations?ref=footer" class="no-mar  "> Naming Ceremony Decorations </a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/mumbai/baby-shower/decorations?ref=footer" class="no-mar  "> Baby shower Decorations </a>
					</div>
				</div>
			</div>

			<div class="text-white-imp dropdown-btn" data-toggle="collapse" href="pune-dropdown" aria-expanded="false" aria-controls="pune-dropdown">
				<h5 class="text-bold">Pune
					<span class="pull-right no-pad-r-imp">
					<i class="glyphicon glyphicon-menu-down"></i>
				</span>
				</h5>
			</div>
			<div class="collapse" id="pune-dropdown">
				<div class=" row">
					<div class="col-xs-4 col-sm-4">
						<span class="text-white-imp  no-pad-l">Birthdays</span>
					</div>
					<div class="col-xs-8 col-sm-8">
						<a href="/pune/birthday-party-planners/birthday-party-decorations/all?ref=footer" class=" no-mar no-pad-l "> Decorations</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/pune/birthday-party-planners/cakes?ref=footer" class=" no-mar no-pad-l ">Cakes</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/entertainment-activities-stalls?ref=footer" class="no-mar "> Entertainment</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/packages?ref=footer" class="no-mar "> Packages</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/food?ref=footer" class="no-mar "> Food</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/tents?ref=footer" class="no-mar ">Tents</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/trends?ref=footer" class="no-mar ">Trends</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/birthday-party-planners/venues?ref=footer" class="no-mar ">Venues</a>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-sm-4">
						<span class="text-white-imp  no-pad-l">For Couple</span>
					</div>
					<div class="col-xs-8 col-sm-8">
						<a href="/pune/candle-light-dinner?ref=footer" class="no-mar  ">Candle Light Dinner</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/pune/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=cake-cutting-places&amp;ref=footer" class="no-mar   ">Cake Cutting Places</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/pune/h/couple-surprises?ref=footer" class="no-mar"> All Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&amp;ref=footer" class="no-mar ">Unique Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=home-decorations&amp;ref=footer" class="no-mar ">Home Decorations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=customised&amp;ref=footer" class="no-mar ">Customised Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=adventure&amp;ref=footer" class="no-mar  ">Adventure Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=regular-surprise&amp;ref=footer" class="no-mar ">Regular Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=romantic&amp;ref=footer" class="no-mar ">Romantic Surprises</a>

					</div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-sm-4">
						<span class="text-white-imp  no-pad-l">For Occasion</span>
					</div>
					<div class="col-xs-8 col-sm-8">
						<a href="/pune/house-warming/decorations/all?ref=footer" class="no-mar  "> House Warming </a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/house-warming/decorations/all?ref=footer" class="no-mar  "> Decorations </a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/house-warming/food?ref=footer" class="no-mar ">Food</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/house-warming/priest?ref=footer" class="no-mar ">Priest</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/house-warming/tents?ref=footer" class="no-mar ">Tent</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/house-warming/add-ons?ref=footer" class="no-mar ">Add-Ons</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/pune/house-warming/cakes?ref=footer" class="no-mar ">Cakes</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/pune/naming-ceremony/decorations?ref=footer" class="no-mar  "> Naming Ceremony Decorations </a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/pune/baby-shower/decorations?ref=footer" class="no-mar  "> Baby shower Decorations </a>
					</div>
				</div>

			</div>

			<div class="text-white-imp dropdown-btn" data-toggle="collapse" href="delhi-dropdown" aria-expanded="false" aria-controls="delhi-dropdown">
				<h5 class="text-bold">Delhi
					<span class="pull-right no-pad-r-imp">
					<i class="glyphicon glyphicon-menu-down"></i>
				</span>
				</h5>
			</div>
			<div class="collapse" id="delhi-dropdown">
				<div class=" row">
					<div class="col-xs-4 col-sm-4">
						<span class="text-white-imp  no-pad-l">Birthdays</span>
					</div>
					<div class="col-xs-8 col-sm-8">
						<a href="/delhi/birthday-party-planners/birthday-party-decorations/all?ref=footer" class=" no-mar no-pad-l "> Decorations</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/delhi/birthday-party-planners/cakes?ref=footer" class=" no-mar no-pad-l ">Cakes</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/entertainment-activities-stalls?ref=footer" class="no-mar "> Entertainment</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/packages?ref=footer" class="no-mar "> Packages</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/food?ref=footer" class="no-mar "> Food</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/tents?ref=footer" class="no-mar ">Tents</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/trends?ref=footer" class="no-mar ">Trends</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/birthday-party-planners/venues?ref=footer" class="no-mar ">Venues</a>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-sm-4">
						<span class="text-white-imp  no-pad-l">For Couple</span>
					</div>
					<div class="col-xs-8 col-sm-8">
						<a href="/delhi/candle-light-dinner?ref=footer" class="no-mar  ">Candle Light Dinner</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/delhi/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=cake-cutting-places&amp;ref=footer" class="no-mar   ">Cake Cutting Places</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/delhi/h/couple-surprises?ref=footer" class="no-mar"> All Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&amp;ref=footer" class="no-mar   ">Unique Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=home-decorations&amp;ref=footer" class="no-mar ">Home Decorations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=customised&amp;ref=footer" class="no-mar ">Customised Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=adventure&amp;ref=footer" class="no-mar  ">Adventure Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=regular-surprise&amp;ref=footer" class="no-mar ">Regular Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=romantic&amp;ref=footer" class="no-mar ">Romantic Surprises</a>

					</div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-sm-4">
						<span class="text-white-imp  no-pad-l">For Occasion</span>
					</div>
					<div class="col-xs-8 col-sm-8">
						<a href="/delhi/house-warming/decorations/all?ref=footer" class="no-mar  "> House Warming </a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/house-warming/decorations/all?ref=footer" class="no-mar  "> Decorations </a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/house-warming/food?ref=footer" class="no-mar ">Food</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/house-warming/priest?ref=footer" class="no-mar ">Priest</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/house-warming/tents?ref=footer" class="no-mar ">Tent</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/house-warming/add-ons?ref=footer" class="no-mar ">Add-Ons</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/delhi/house-warming/cakes?ref=footer" class="no-mar ">Cakes</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/delhi/naming-ceremony/decorations?ref=footer" class="no-mar  "> Naming Ceremony Decorations </a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/delhi/baby-shower/decorations?ref=footer" class="no-mar  "> Baby shower Decorations </a>
					</div>
				</div>

			</div>

			<div class="text-white-imp dropdown-btn hide" data-toggle="collapse" href="chennai-dropdown" aria-expanded="false" aria-controls="chennai-dropdown">
				<h5 class="text-bold">Chennai
					<span class="pull-right no-pad-r-imp">
					<i class="glyphicon glyphicon-menu-down"></i>
				</span>
				</h5>
			</div>
			<div class="collapse hide" id="chennai-dropdown">
				<div class=" row">
					<div class="col-xs-4 col-sm-4">
						<span class="text-white-imp  no-pad-l">Birthdays</span>
					</div>
					<div class="col-xs-8 col-sm-8">
						<a href="/chennai/birthday-party-planners/birthday-party-decorations/all?ref=footer" class=" no-mar no-pad-l "> Decorations</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/chennai/birthday-party-planners/cakes?ref=footer" class=" no-mar no-pad-l ">Cakes</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/entertainment-activities-stalls?ref=footer" class="no-mar "> Entertainment</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/packages?ref=footer" class="no-mar "> Packages</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/food?ref=footer" class="no-mar "> Food</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/tents?ref=footer" class="no-mar ">Tents</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/trends?ref=footer" class="no-mar ">Trends</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/birthday-party-planners/venues?ref=footer" class="no-mar ">Venues</a>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-sm-4">
						<span class="text-white-imp  no-pad-l">For Couple</span>
					</div>
					<div class="col-xs-8 col-sm-8">
						<a href="/chennai/candle-light-dinner?ref=footer" class="no-mar  ">Candle Light Dinner</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/chennai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=cake-cutting-places&amp;ref=footer" class="no-mar   ">Cake Cutting Places</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/chennai/h/couple-surprises?ref=footer" class="no-mar"> All Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&amp;ref=footer" class="no-mar ">Unique Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=home-decorations&amp;ref=footer" class="no-mar ">Home Decorations</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=customised&amp;ref=footer" class="no-mar ">Customised Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=adventure&amp;ref=footer" class="no-mar  ">Adventure Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=regular-surprise&amp;ref=footer" class="no-mar ">Regular Surprises</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=romantic&amp;ref=footer" class="no-mar ">Romantic Surprises</a>

					</div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-sm-4">
						<span class="text-white-imp  no-pad-l">For Occasion</span>
					</div>
					<div class="col-xs-8 col-sm-8">
						<a href="/chennai/house-warming/decorations/all?ref=footer" class="no-mar  "> House Warming </a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/house-warming/decorations/all?ref=footer" class="no-mar  "> Decorations </a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/house-warming/food?ref=footer" class="no-mar ">Food</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/house-warming/priest?ref=footer" class="no-mar ">Priest</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/house-warming/tents?ref=footer" class="no-mar ">Tent</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/house-warming/add-ons?ref=footer" class="no-mar ">Add-Ons</a>
						<span>&nbsp;/&nbsp;</span>
						<a href="/chennai/house-warming/cakes?ref=footer" class="no-mar ">Cakes</a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/chennai/naming-ceremony/decorations?ref=footer" class="no-mar  "> Naming Ceremony Decorations </a>
						<span>&nbsp;|&nbsp;</span>
						<a href="/chennai/baby-shower/decorations?ref=footer" class="no-mar  "> Baby shower Decorations </a>
					</div>
				</div>

			</div>
		</div>
	@endif
@endif
<div class="footer-wrap">
	<div class="footer-bottom">
		<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
			<div class="footer-bottom-inner">
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-pad__400-600">
					<div class="footer-logo mar-b-10">
						<div class="in-blk">
							<a href="/"><img src="{{ $galleryUrl }}/main/img/logo/logo_w.png" alt="Evibe logo"></a>
						</div>
					</div>
					<div class="copyrights-wrap">
						<div class="copytext">© 2014 - {{ date('Y', time()) }} Evibe Technologies Pvt. Ltd.</div>
						<div class="copytext">
							<span>All rights reserved</span>
							<div class="pad-t-10">
								<div class="in-blk">
									<span class="mar-r-2">•</span>
									<span class="mar-r-2"><a href="{{ route('terms') }}">Terms</a></span>
								</div>
								<div class="in-blk">
									<span class="mar-r-2">•</span>
									<span class="mar-r-2"><a href="{{ route('privacy') }}">Privacy</a></span>
								</div>
								<div class="in-blk">
									<span class="mar-r-2">•</span>
									<span class="mar-r-2"><a href="{{ route('refunds') }}">Refunds</a></span>
								</div>
							</div>
						</div>
					</div>
					<div class="socmed-body">
						<div class="socmed-body-inner">
							<div class="in-blk">
								<a href="https://www.facebook.com/evibe.in" target="_blank" rel="noopener">
									<div class="evibe-footer-fb-circle"></div>
								</a>
							</div>
							<div class="pad-l-10 in-blk">
								<a href="https://www.youtube.com/channel/UCLANgB4zAIj35TH1SsiuLIg?html5=1" target="_blank" rel="noopener">
									<div class="evibe-footer-youtube"></div>
								</a>
							</div>
							<div class="pad-l-10 in-blk">
								<a href="https://www.instagram.com/evibe.in/" target="_blank" rel="noopener">
									<div class="evibe-footer-instagram"></div>
								</a>
							</div>
							<div class="pad-l-10 in-blk">
								<a href="https://twitter.com/evibeIn" target="_blank" rel="noopener">
									<img src='{{ $galleryUrl }}/main/icons-social/twitter-512.png' style="width:32px;height: 32px;margin-top: -24px">
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 no-pad__400-600">
					<div class="footer-bottom-right">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 no-pad__400-600 mar-b-20">
							<h4 class="links-head">Company</h4>
							<ul class="links-list">
								<li class="link">
									<a href="/about?ref={{ $ref }}">About Us</a>
								</li>
								<li class="link">
									<a href="https://evibe.in/blog" target="_blank" rel="noopener">Evibe<span class="text-lower">.in</span> Blog</a>
								</li>
								<li class="link">
									@if(isset($cityUrl))
										<a href="{{ route('city.stories.customer', $cityUrl) }}?ref={{ $ref }}">Customer Reviews</a>
									@else
										<a href="{{ route('city.stories.customer','bangalore') }}?ref={{ $ref }}">Customer Reviews</a>
									@endif
								</li>
								<li class="link hide">
									<a href="/how-it-works?ref={{ $ref }}" class="hew-link">How It Works</a>
								</li>
								<li class="link">
									<a href="/faqs?ref={{ $ref }}">FAQs</a>
								</li>
								<li class="link">
									<a href="/about#join-us?ref={{ $ref }}">Join Us</a>
								</li>
							</ul>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 no-pad__400-600 mar-b-20">
							<h4 class="links-head">Partners</h4>
							<ul class="links-list">
								<li class="link">
									<a href="{{ route('partner.benefits') }}?ref={{ $ref }}">Why Sign Up?</a>
								</li>
								<li class="link">
									<a href="{{ route('partner.sign-up.show') }}?ref={{ $ref }}">Sign Up</a>
								</li>
								<li class="link">
									<a href="{{ route('partner.dash.new.login') }}?ref={{ $ref }}">Partner Login</a>
								</li>
							</ul>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 no-pad__400-600 mar-b-20">
							<h4 class="links-head">Contact</h4>
							<div>
								<div class="contact-item">
									<div class="in-blk contact-val">
										<span class="glyphicon glyphicon-earphone"></span>
										{{ config('evibe.contact.company.plain_phone') }}
									</div>
								</div>
								<div class="contact-item hide">
									<div class="in-blk">evibe.in</div>
								</div>
								<div class="contact-item neg-mar">
									<div class="in-blk contact-val">
										<a href="mailto:ping@evibe.in" target="_blank" rel="noopener">
											<span class="glyphicon glyphicon-envelope"></span>
											ping@evibe.in
										</a>
									</div>
								</div>
							</div>
							<div class="link">
								<a href="/feedback?ref={{ $ref }}">
									<span class="glyphicon glyphicon-comment"></span>
									Give Feedback</a>
							</div>
							<div class="link mar-t-8">
								<a href="{{ route("track.orders") }}?ref={{ $ref }}">
									<span class="glyphicon glyphicon-screenshot"></span>
									Track My Order
								</a>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>