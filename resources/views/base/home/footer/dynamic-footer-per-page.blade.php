@php
	$occasionId = isset($occasionId) ? $occasionId : 0;
	$mapTypeId = isset($data['mapTypeId']) ? $data['mapTypeId'] : 0;
	$validateConfigFooterValues = [
		config(('search.auto_hint.'.getCityId().'.'.$occasionId.'.'.$mapTypeId)),
		config(('search.auto_hint.'.getCityId().'.'.$occasionId.'.'.'0')),
		config(('search.auto_hint.'.getCityId().'.'.'0')),
		config(('search.auto_hint.0'))
		];
@endphp

@foreach($validateConfigFooterValues as $validateConfigFooterValue)
	@if(count($validateConfigFooterValue) > 0)
		<div class="pad-t-20 top-search">
			<div class="col-xs-12 col-sm-10 col-md-10 col-lg-12 footer-links-list ">
				<div class="col-xs-12 col-sm-12">
					<h5 class="mar-t-20 mar-b-10">Trending Searches</h5>
				</div>
				<div class="col-xs-12 col-sm-12 col-lg-12 mar-b-20">
					@foreach($validateConfigFooterValue as $link => $keyword)
						<a href="/{{ getCityUrl() }}/{{ $link }}&utm_source=footer&utm_campaign=top-searches&utm_medium=website&utm_content={{urlencode($keyword)}}" class="no-mar ls-none no-pad">{{ $keyword }}</a>
					@endforeach
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		@break
	@endif
@endforeach