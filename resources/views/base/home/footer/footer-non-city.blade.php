<?php
$ref = "footer";
if (isset($refKeys) && is_array($refKeys) && array_key_exists('companyFooterRefKey', $refKeys))
{
	$ref = $refKeys['companyFooterRefKey'];
}
?> 

<div class="footer-bottom">
	<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
		<div class="footer-bottom-inner">
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-pad__400-600">
				<div class="footer-logo mar-b-10">
					<div class="in-blk">
						<a href="/"><img src="{{ $galleryUrl }}/main/img/logo/logo_w.png" alt="Evibe logo"></a>
					</div>
				</div>
				<div class="copyrights-wrap">
					<div class="copytext">© 2014 - {{ date('Y', time()) }} Evibe Technologies Pvt. Ltd.</div>
					<div class="copytext">
						<span>All rights reserved</span>
						<div class="pad-t-10">
							<div class="in-blk">
								<span class="mar-r-2">•</span>
								<span class="mar-r-2"><a href="{{ route('terms') }}">Terms</a></span>
							</div>
							<div class="in-blk">
								<span class="mar-r-2">•</span>
								<span class="mar-r-2"><a href="{{ route('privacy') }}">Privacy</a></span>
							</div>
							<div class="in-blk">
								<span class="mar-r-2">•</span>
								<span class="mar-r-2"><a href="{{ route('refunds') }}">Refunds</a></span>
							</div>
						</div>
					</div>
				</div>
				<div class="socmed-body">
					<div class="socmed-body-inner">
						<div class="in-blk">
							<a href="https://www.facebook.com/evibe.in" target="_blank" rel="noopener">
								<img src="{{ $galleryUrl }}/main/img/logo/fb-circle.png" alt="Facebook page" title="Follow Evibe.in on Facebook">
							</a>
						</div>
						<div class="pad-l-10 in-blk">
							<a href="https://www.youtube.com/channel/UCLANgB4zAIj35TH1SsiuLIg?html5=1" rel="noopener" target="_blank">
								<img src="{{ $galleryUrl }}/main/img/logo/youtube.png" alt="Youtube page" title="Follow Evibe.in on Youtube">
							</a>
						</div>
						<div class="pad-l-10 in-blk">
							<a href="https://www.instagram.com/evibe.in/" rel="noopener" target="_blank">
								<img src="{{ $galleryUrl }}/main/img/logo/instagram.png" alt="Instagram page" title="Follow Evibe.in on Instagram">
							</a>
						</div>
						<div class="pad-l-10 in-blk">
							<a href="https://plus.google.com/113143091769031310564/" rel="noopener" target="_blank">
								<img src="{{ $galleryUrl }}/main/img/logo/google-plus.png" alt="Google+ page" title="Follow Evibe.in on Google+">
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 no-pad__400-600">
				<div class="footer-bottom-right">
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 no-pad__400-600 mar-b-20">
						<h4 class="links-head">Company</h4>
						<ul class="links-list">
							<li class="link">
								<a href="/about?ref={{ $ref }}">About Us</a>
							</li>
							<li class="link">
								<a href="https://evibe.in/blog" target="_blank" rel="noopener">Evibe<span class="text-lower">.in</span> Blog</a>
							</li>
							<li class="link hide">
								<a href="/how-it-works?ref={{ $ref }}" class="hew-link">How It Works</a>
							</li>
							<li class="link">
								<a href="/faqs?ref={{ $ref }}">FAQs</a>
							</li>
							<li class="link">
								<a href="/about#join-us?ref={{ $ref }}">Join Us</a>
							</li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 no-pad__400-600 mar-b-20">
						<h4 class="links-head">Partners</h4>
						<ul class="links-list">
							<li class="link">
								<a href="{{ route('partner.benefits') }}?ref={{ $ref }}">Why Sign Up?</a>
							</li>
							<li class="link">
								<a href="{{ route('partner.sign-up.show') }}?ref={{ $ref }}">Sign Up</a>
							</li>
							<li class="link">
								<a href="{{ route('partner.dash.new.login') }}?ref={{ $ref }}">Partner Login</a>
							</li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 no-pad__400-600 mar-b-20">
						<h4 class="links-head">Contact</h4>
						<div>
							<div class="contact-item">
								<div class="in-blk contact-val">
									<span class="glyphicon glyphicon-earphone"></span>
									{{ config('evibe.contact.company.plain_phone') }}
								</div>
							</div>
							<div class="contact-item hide">
								<div class="in-blk">evibe.in</div>
							</div>
							<div class="contact-item neg-mar">
								<div class="in-blk contact-val">
									<a href="mailto:ping@evibe.in" target="_blank" rel="noopener">
										<span class="glyphicon glyphicon-envelope"></span>
										ping@evibe.in
									</a>
								</div>
							</div>
						</div>
						<div class="link">
							<a href="/feedback?ref={{ $ref }}">
								<span class="glyphicon glyphicon-comment"></span>
								Give Feedback
							</a>
						</div>
						<div class="link mar-t-8">
							<a href="{{ route("track.orders") }}?ref={{ $ref }}">
								<span class="glyphicon glyphicon-screenshot"></span>
								Track My Order
							</a>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>