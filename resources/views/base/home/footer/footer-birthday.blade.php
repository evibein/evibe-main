<?php
$ref = "birthday-party";
$oFooterRef = "footer-master";
if (isset($refKeys) && is_array($refKeys) && array_key_exists('occasionImageRefKey', $refKeys))
{
	$ref = $refKeys['occasionImageRefKey'];
}
if (isset($refKeys) && is_array($refKeys) && array_key_exists('occasionFooterRefKey', $refKeys))
{
	$oFooterRef = $refKeys['occasionFooterRefKey'];
}
?>
<div class="footer-top pad-t-20 pad-b-20 bg-white">
	<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 col-sm-offset-1 col-md-offset-1 col-lg-offset-1 footer-links-list text-center">
		<h5 class="text-center text-col-gr"> Not Looking For Birthday Party ?</h5>
		<div class="col-md-4 hide">
			<a href="{{route('city.occasion.pre-post.home', $cityUrl)}}?ref={{ $ref }}" class="no-border">
				<div class="mar-t-10 ft-cat no-pad" style="background:url({{$galleryUrl}}/main/img/post-wedding.jpg);">
					<h5> Weddings</h5>
				</div>
			</a>
		</div>
		<div class="col-md-4 col-md-offset-2">
			<a href="{{route('city.occasion.bachelor.home', $cityUrl)}}?ref={{ $ref }}" class="no-border">
				<div class="mar-t-10 ft-cat no-pad" style="background:url({{$galleryUrl}}/main/img/bachelors-party.jpg);">
					<h5>Youth Party</h5>
				</div>
			</a>
		</div>
		<div class="col-md-4">
			<a href="{{route('city.occasion.surprises.home', $cityUrl)}}?ref={{ $ref }}" class="no-border">
				<div class="mar-t-10 ft-cat no-pad" style="background:url({{$galleryUrl}}/main/img/couple-exp.jpg);">
					<h5> Surprises</h5>
				</div>
			</a>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
</div>