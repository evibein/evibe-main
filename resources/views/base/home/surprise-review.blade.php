<div class="surprise-customer-review-bar">
	@if(isset($data['promotedReviews']))
		@foreach($data['promotedReviews'] as $promotedReview)
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mar-t-10 review-item">
				<div class="review-wrap">
					<div class="col-md-4 col-sm-4 col-xs-4 photo-wrap">
						<img src="{{ config("evibe.gallery.host") }}/main/customer/reviews/{{ $promotedReview['photo'] }}" class="img-responsive img-centered" alt="Evibe.in Surprises Review by {{ $promotedReview['name']  }}">
					</div>
					<div class="col-md-8 col-sm-8 col-xs-8 no-pad-l">
						<div class="mar-t-15"><b>{{ $promotedReview['name'] }}</b></div>
						<div class="mar-t-5 review-text-wrap font-13">
							<i>{{ $promotedReview['review'] }}</i>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		@endforeach
	@endif
	<div class="clearfix"></div>
</div>