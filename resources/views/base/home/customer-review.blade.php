<section class="section-cat"> <!-- adding check inside so that background color will be restored -->
	@if(isset($data['customerStories']) && count($data['customerStories']) > 0)
		<div class="container">
			<h4 class="sec-title title-text text-center">
				<span>Our Customers </span>
				<img src="{{$galleryUrl}}/main/img/home/heart.png" alt="Evibe customers"> Evibe<span class="text-lower">.in</span>
			</h4>
			<div id="cust-slide" class="carousel customer-test slide " data-ride="carousel" style="margin-top:15px">
				<div class="carousel-inner">
					<?php  $count = 0; ?>
					@foreach($data['customerStories'] as $customer)
						<div class="item row @if($count == 0) active @endif">
							<div class="col-md-6 col-sm-10 col-xs-12 sol-sm-offset-1 cust">
								@if ($customer['type_id'] == 1)
									<div class="cust-video home-page-iframe-video-load" data-height="300" data-count="{{ $count }}" data-url="{{ $customer['url'] }}">
										<img style="max-width: 100%;" src="https://img.youtube.com/vi/{{ $customer['url'] }}/hqdefault.jpg">
									</div>
								@else
									<div class="cust-image">
										<img src="{{ $galleryUrl }}/story/customer/{{ $customer['url'] }}" alt="Evibe.in review by {{ $customer['name'] }}">
									</div>
								@endif
							</div>
							<div class="col-md-6 col-sm-12 col-xs-12 cust cust-comments">
								<p class="cust-comments-text text-col-gr">
									<span style="padding-left:40px">
										{{ $customer['story'] }}
									</span>
								</p>
								<div class="col-md-8 col-md-offset-2">
									<div class="col-md-12 text-center no-padding no-margin">
										<h6 class="cust-name no-margin">{{ $customer['name'] }}</h6>
									</div>
								</div>
							</div>
						</div>
						<?php $count++; ?>
					@endforeach
				</div>
				<a class="carousel-control cust-slide-prev hidden-sm hidden-xs" href="#cust-slide" data-slide="prev">
					<img src="https://gallery.evibe.in/img/icons/icon_left_thin.png" alt="Prev">
				</a>
				<a class="carousel-control cust-slide-next hidden-sm hidden-xs" href="#cust-slide" data-slide="next">
					<img src="https://gallery.evibe.in/img/icons/icon_right_thin.png" alt="Next">
				</a>
			</div>
			@if(count($data['customerStories']) > 0)
				<div class="text-center mar-t-5">
					<a class="btn btn-link link-evibe-stories" href="@if(array_key_exists('occasionId',$data) && $data['occasionId']){{ route('city.stories.customer', getCityUrl()) }}?occasion=@occasionUrl($data['occasionId'])@else{{ route('city.stories.customer',getCityUrl()) }}  @endif">
						See All Customer Reviews
					</a>
				</div>
			@endif
		</div>
	@endif
</section>

<script type="text/javascript">
	$('.cust-comments-text').each(function (i, el) {
		var $el = $(el);
		var story = $el.text();
		var maxLength = 400;
		if (story.length >= maxLength) {
			var lastSpaceIndex = story.substr(0, maxLength).lastIndexOf(' ');
			$el.text(story.substr(0, lastSpaceIndex) + ' ...');
		}
	});
</script>