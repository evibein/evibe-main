<div class="section-social-stream padding-75">
	<div class="container">
		<h4 class="sec-title text-center no-mar-t mar-b-30">Social media buzz</h4>
		<div class="row no-margin social-stream-box">
			<div class="col-md-6 col-xs-8 col-lg-6 col-xs-offset-2 col-md-offset-0 col-lg-offset-0 pad-t-15">
				<div class="fb-stream-wrap no-mar">
					<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fevibe.in&tabs=timeline&width=480&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="500" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
				</div>
			</div>
			<div class="col-md-6 col-xs-8 col-lg-6 col-xs-offset-2 col-md-offset-0 col-lg-offset-0 pad-t-15">
				<div class="twitter-stream-wrap no-mar">
					<a class="twitter-timeline" data-width="500" data-height="500" data-link-color="#E81C4F" href="https://twitter.com/evibeIn">Tweets by evibeIn</a>
					<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<script type="text/javascript">
	(function (d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
