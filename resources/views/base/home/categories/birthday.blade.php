@php $sessionCityUrl = getCityUrl() @endphp
@if($agent->isMobile() || $agent->isTablet())
	<section class="section-cat mar-t-20 no-pad-b ">
		<h6 class="no-mar-b" style="padding-left: 15px;font-weight: 500;margin-top:12px;">GROWN ONE MORE YEAR</h6>
		<p class="section-sub-heading">Your birthday celebrations start here</p>
		<div class="col-xs-6 col-md-3 col-lg-3 text-center">
			<a href="{{ route('city.occasion.birthdays.decors.list', ['cityUrl'=>$sessionCityUrl]) }}?ref=home" class="a-no-decoration-black">
				<img src="{{$galleryUrl}}/main/img/home/v1/b13.png" class="category-card">
				<div class="category-card-title category-card-title-mobile">
					<p>Decorations</p>
				</div>
			</a>
		</div>
		<div class="col-xs-6 col-md-3 col-lg-3 text-center">
			<a href="{{ route('city.occasion.birthdays.cakes.list',$sessionCityUrl) }}?ref=home" class="a-no-decoration-black">
				<img src="{{$galleryUrl}}/main/img/home/v1/b12.png" class="category-card">
				<div class="category-card-title category-card-title-mobile">
					<p>Cakes</p>
				</div>
			</a>
		</div>
		<div class="col-xs-6 col-md-3 col-lg-3 text-center">
			<a href="{{ route('city.occasion.birthdays.ent.list',$sessionCityUrl) }}?ref=home" class="a-no-decoration-black">
				<img src="{{$galleryUrl}}/main/img/home/v1/b9.png " class="category-card">
				<div class="category-card-title category-card-title-mobile">
					<p>Entertainment</p>
				</div>
			</a>
		</div>
		<div class="col-xs-6 col-md-3 col-lg-3 text-center">
			<a href="{{ route('city.occasion.birthdays.food.list',$sessionCityUrl) }}?ref=home" class="a-no-decoration-black">
				<img src="{{$galleryUrl}}/main/img/home/v1/b11.png" class="category-card">
				<div class="category-card-title category-card-title-mobile">
					<p> Food</p>
				</div>
			</a>
		</div>
		<div class="clearfix"></div>
	</section>
@else
	<section class="section-cat mar-t-20 " >
		<h5 class="section-heading">GROWN ONE MORE YEAR</h5>
		<p class="section-sub-heading">Your birthday celebrations start here</p>
		<div class="col-sm-12 col-md-3 col-lg-3 text-center ">
			<div class="category-four-card-desk">
				<a href="{{ route('city.occasion.birthdays.decors.list',$sessionCityUrl) }}?ref=home" class="a-no-decoration-black">
					<img src="{{$galleryUrl}}/main/img/home/v1/b13.png" class="category-card-desk">
					<div class="category-four-card-title-desk">
						<p class="no-mar">Decorations</p>
					</div>
				</a>
			</div>
		</div>
		<div class="col-sm-12 col-md-3 col-lg-3 text-center">
			<div class="category-four-card-desk">
				<a href="{{ route('city.occasion.birthdays.cakes.list',$sessionCityUrl) }}?ref=home" class="a-no-decoration-black">
					<img src="{{$galleryUrl}}/main/img/home/v1/b12.png" class="category-card-desk">
					<div class="category-four-card-title-desk">
						<p class="no-mar">Cakes</p>
					</div>
				</a>
			</div>
		</div>
		<div class="col-sm-12 col-md-3 col-lg-3 text-center">
			<div class="category-four-card-desk">
				<a href="{{ route('city.occasion.birthdays.ent.list',$sessionCityUrl) }}?ref=home" class="a-no-decoration-black">
					<img src="{{$galleryUrl}}/main/img/home/v1/b9.png " class="category-card-desk">
					<div class="category-four-card-title-desk">
						<p class="no-mar">Entertainment</p>
					</div>
				</a>
			</div>
		</div>
		<div class="col-sm-12 col-md-3 col-lg-3 text-center ">
			<div class="category-four-card-desk">
				<a href="{{ route('city.occasion.birthdays.food.list',$sessionCityUrl) }}?ref=home" class="a-no-decoration-black">
					<img src="{{$galleryUrl}}/main/img/home/v1/b11.png" class="category-card-desk">
					<div class="category-four-card-title-desk">
						<p class="no-mar">Food</p>
					</div>
				</a>
			</div>
		</div>
		<div class="clearfix"></div>
	</section>
@endif