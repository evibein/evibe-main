@php $sessionCityUrl = getCityUrl() @endphp
	@if($agent->isMobile() || $agent->isTablet())
	<section class="section-cat  no-pad-b" >
		<h6 class="no-mar-b" style="padding-left: 15px;font-weight: 500;margin-top:12px">LITTLE THINGS MATTER</h6>
		<p class="section-sub-heading">Never leave single moment to surprise loved one</p>
		<div class="col-xs-6 col-md-3 col-lg-3 text-center">
			<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=romantic&ref=home" class="a-no-decoration-black">
				<img src="{{$galleryUrl}}/main/img/home/v1/s1.png " class="category-card">
				<div class="category-card-title category-card-title-mobile">
					<p>Romantic Surprises</p>
				</div>
			</a>
		</div>
		<div class="col-xs-6 col-md-3 col-lg-3 text-center">
			<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=adventure&ref=home" class="a-no-decoration-black">
				<img src="{{$galleryUrl}}/main/img/home/v1/s2.png" class="category-card">
				<div class="category-card-title category-card-title-mobile">
					<p>Adventerous Surprises</p>
				</div>
			</a>
		</div>
		<div class="col-xs-6 col-md-3 col-lg-3 text-center">
			<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&ref=home" class="a-no-decoration-black">
				<img src="{{$galleryUrl}}/main/img/home/v1/s3.png" class="category-card">
				<div class="category-card-title category-card-title-mobile">
					<p>Unique Surprises</p>
				</div>
			</a>
		</div>
		<div class="col-xs-6 col-md-3 col-lg-3 text-center">
			<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=customised&ref=home" class="a-no-decoration-black">
				<img src="{{$galleryUrl}}/main/img/home/v1/s4.png" class="category-card">
				<div class="category-card-title category-card-title-mobile">
					<p>Cutomised Surprises</p>
				</div>
			</a>
		</div>
		<div class="clearfix"></div>
	</section>
	@else
	<section class="section-cat mar-t-20"  >
		<h5 class="section-heading">LITTLE THINGS MATTER</h5>
		<p class="section-sub-heading">Never leave single moment to surprise loved one</p>
		<div class="col-sm-12 col-md-3 col-lg-3 text-center ">
			<div class="category-four-card-desk">
				<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=romantic&ref=home" class="a-no-decoration-black">
					<img src="{{$galleryUrl}}/main/img/home/v1/s1.png ">
					<div class="category-four-card-title-desk">
						<p class="no-mar">Romantic Surprises</p>
					</div>
				</a>
			</div>
		</div>
		<div class="col-sm-12 col-md-3 col-lg-3 text-center">
			<div class="category-four-card-desk">
				<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=adventure&ref=home" class="a-no-decoration-black">
					<img src="{{$galleryUrl}}/main/img/home/v1/s2.png">
					<div class="category-four-card-title-desk">
						<p class="no-mar">Adventerous Surprises</p>
					</div>
				</a>
			</div>
		</div>
		<div class="col-sm-12 col-md-3 col-lg-3 text-center">
			<div class="category-four-card-desk">
				<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&ref=home" class="a-no-decoration-black">
					<img src="{{$galleryUrl}}/main/img/home/v1/s3.png">
					<div class="category-four-card-title-desk">
						<p class="no-mar">Unique Surprises</p>
					</div>
				</a>
			</div>
		</div>
		<div class="col-sm-12 col-md-3 col-lg-3 text-center ">
			<div class="category-four-card-desk">
				<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=customised&ref=home" class="a-no-decoration-black">
					<img src="{{$galleryUrl}}/main/img/home/v1/s4.png">
					<div class="category-four-card-title-desk">
						<p class="no-mar">Cutomised Surprises</p>
					</div>
				</a>
			</div>
		</div>
		<div class="clearfix"></div>
	</section>
	@endif

