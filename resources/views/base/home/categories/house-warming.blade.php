@php $sessionCityUrl = getCityUrl() @endphp
	@if($agent->isMobile() && !$agent->isTablet())

	<section class="section-cat no-pad-b ">
		<h6 class="no-mar-b" style="padding-left: 15px;font-weight: 500;margin-top:12px">WELCOME HOME</h6>
		<p class="section-sub-heading">Leave hassle on us and welcome the new happiness</p>
		<div class="col-xs-6 col-md-3 col-lg-3 text-center">
			<a href="{{ route('city.occasion.house-warming.decors.list',$sessionCityUrl) }}?ref=home" class="a-no-decoration-black">
				<img src="{{$galleryUrl}}/main/img/home/v1/7.png" class="category-card">
				<div class="category-card-title category-card-title-mobile">
					<p>House Warming Decorations</p>
				</div>
			</a>
		</div>
		<div class="col-xs-6 col-md-3 col-lg-3 text-center">
			<a href="{{ route('city.occasion.house-warming.cakes.list',$sessionCityUrl) }}?ref=home" class="a-no-decoration-black">
				<img src="{{$galleryUrl}}/main/img/home/v1/6.png " class="category-card">
				<div class="category-card-title category-card-title-mobile">
					<p>House Warming Cakes</p>
				</div>
			</a>
		</div>
		<div class="col-xs-6 col-md-3 col-lg-3 text-center ">
			<a href="{{ route('city.occasion.house-warming.food.list',$sessionCityUrl) }}?ref=home" class="a-no-decoration-black">
				<img src="{{$galleryUrl}}/main/img/home/v1/5.png" class="category-card">
				<div class="category-card-title category-card-title-mobile">
					<p> Food Combos</p>
				</div>
			</a>
		</div>
		<div class="col-xs-6 col-md-3 col-lg-3 text-center">
			<a href="{{ route('city.occasion.house-warming.priest.list',$sessionCityUrl) }}?ref=home" class="a-no-decoration-black">
				<img src="{{$galleryUrl}}/main/img/home/v1/8.png" class="category-card">
				<div class="category-card-title category-card-title-mobile">
					<p> Priests</p>
				</div>
			</a>
		</div>
		<div class="clearfix"></div>
	</section>
@else
	<section class="section-cat mar-t-20 " >
		<h5 class="section-heading">WELCOME HOME</h5>
		<p class="section-sub-heading">Leave hassle on us and welcome the new happiness</p>
		<div class="col-sm-12 col-md-3 col-lg-3 text-center">
			<div class="category-four-card-desk">
				<a href="{{ route('city.occasion.house-warming.decors.list',$sessionCityUrl) }}?ref=home" class="a-no-decoration-black">
					<img src="{{$galleryUrl}}/main/img/home/v1/7.png" class="category-card-desk">
					<div class="category-four-card-title-desk">
						<p class="no-mar">House Warming Decorations</p>
					</div>
				</a>
			</div>
		</div>
		<div class="col-sm-12 col-md-3 col-lg-3 text-center">
			<div class="category-four-card-desk">
				<a href="{{ route('city.occasion.house-warming.cakes.list',$sessionCityUrl) }}?ref=home" class="a-no-decoration-black">
					<img src="{{$galleryUrl}}/main/img/home/v1/6.png " class="category-card-desk">
					<div class="category-four-card-title-desk">
						<p class="no-mar">House Warming Cakes</p>
					</div>
				</a>
			</div>
		</div>
		<div class="col-sm-12 col-md-3 col-lg-3 text-center">
			<div class="category-four-card-desk">
				<a href="{{ route('city.occasion.house-warming.food.list',$sessionCityUrl) }}?ref=home" class="a-no-decoration-black">
					<img src="{{$galleryUrl}}/main/img/home/v1/5.png" class="category-card-desk">
					<div class="category-four-card-title-desk">
						<p class="no-mar"> Food Combos</p>
					</div>
				</a>
			</div>
		</div>
		<div class="col-sm-12 col-md-3 col-lg-3 text-center">
			<div class="category-four-card-desk">
				<a href="{{ route('city.occasion.house-warming.priest.list',$sessionCityUrl) }}?ref=home" class="a-no-decoration-black">
					<img src="{{$galleryUrl}}/main/img/home/v1/8.png" class="category-card-desk">
					<div class="category-four-card-title-desk">
						<p class="no-mar"> Priests</p>
					</div>
				</a>
			</div>
		</div>
		<div class="clearfix"></div>

	</section>
@endif