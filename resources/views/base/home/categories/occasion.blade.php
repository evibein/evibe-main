@php $sessionCityUrl = getCityUrl() @endphp
@if($agent->isMobile())
	<section class="section-cat no-pad-b ">
		<h6 class="no-mar-b" style="padding-left: 15px;font-weight: 500;margin-top:12px">ENABLE MEMORIES</h6>
		<p class="section-sub-heading">Some occasions come once in life, Celebrate like that</p>
		<div class="col-sm-12 col-md-6 text-center">
			<a href="{{route('city.occasion.bsdecors.list', $sessionCityUrl)}}?ref=home" class="a-no-decoration-black">
				<img src="{{$galleryUrl}}/main/img/home/v1/bs.png" class="category-card" style="width: 100%;height: auto">
				<div class="three-card-cat">
					<h6>Baby Shower Decorations</h6>
				</div>
			</a>
		</div>
		<div class="col-sm-12 col-md-6 text-center">
			<a href="{{route('city.occasion.ncdecors.list', $sessionCityUrl)}}?ref=home" class="a-no-decoration-black">
				<img src="{{$galleryUrl}}/main/img/home/v1/ns.png" class="category-card" style="width: 100%;height: auto">
				<div class="three-card-cat">
					<h6>Naming Ceremony Decorations</h6>
				</div>
			</a>
		</div>
		<div class="clearfix"></div>
	</section>
@else
	<section class="section-cat mar-t-20">
		<h5 class="section-heading">ENABLE MEMORIES</h5>
		<p class="section-sub-heading">Some occasions come once in life, Celebrate like that</p>
		<div class="pad-l-15 pad-r-15">
			<div class="col-sm-12 category-single-card no-pad-l no-pad-r" style="float:none; background-image:url('{{$galleryUrl}}/main/img/home/v1/baby1.png');">
				<div class="text-left" style="padding-left:50%">
					<h4 class="category-single-card-heading"> Baby Shower Decorations</h4>
					<a href="{{route('city.occasion.bsdecors.list', $sessionCityUrl)}}?ref=home" class="btn btn-evibe btn-md"> Browse All</a>
				</div>
			</div>
			<div class="col-sm-12 mar-t-20 category-single-card no-pad-l no-pad-r" style="float:none; background-image:url('{{$galleryUrl}}/main/img/home/v1/baby3.png');margin-bottom: 20px">
				<div class="text-left" style="padding-left:10%">
					<h4 class="category-single-card-heading"> Naming Ceremony Decorations</h4>
					<a href="{{route('city.occasion.ncdecors.list', $sessionCityUrl)}}?ref=home" class="btn btn-evibe btn-md">Browse All</a>
				</div>
			</div>
		</div>
	</section>

@endif