@if($agent->isMobile() || $agent->isTablet())
	<section class="section-cat no-pad-b ">
		<h6 class="no-mar-b" style="padding-left: 15px;font-weight: 500;margin-top:12px">CELEBRATE LOVE</h6>
		<p class="section-sub-heading">Let the time stop & rekindle the moment </p>
		<div class="col-xs-6 col-md-3 col-lg-3 text-center">
			<a href="{{ route("city.cld.list", getCityUrl()) }}?ref=home" class="a-no-decoration-black">
				<img src="{{$galleryUrl}}/main/img/home/v1/b17.png" class="category-card">
				<div class="category-card-title category-card-title-mobile">
					<p>Handpicked Candle Light Dinner</p>
				</div>
			</a>
		</div>
		<div class="col-xs-6 col-md-3 col-lg-3 text-center">
			<a href="{{ route("city.cld.list", getCityUrl()) }}?category=best-moon-light-candle-light-dinner&ref=home" class="a-no-decoration-black">
				<img src="{{$galleryUrl}}/main/img/home/v1/b16.png" class="category-card">
				<div class="category-card-title category-card-title-mobile">
					<p>MoonLight Candle Light Dinner</p>
				</div>
			</a>
		</div>
		<div class="col-xs-6 col-md-3 col-lg-3 text-center">
			<a href="{{ route("city.cld.list", getCityUrl()) }}?category=best-rooftop-candle-light-dinner&ref=home" class="a-no-decoration-black">
				<img src="{{$galleryUrl}}/main/img/home/v1/b14.png " class="category-card">
				<div class="category-card-title category-card-title-mobile">
					<p>Rooftop Candle Light Dinner</p>
				</div>
			</a>
		</div>
		<div class="col-xs-6 col-md-3 col-lg-3 text-center">
			<a href="{{ route("city.cld.list", getCityUrl()) }}?category=best-pool-side-candle-light-dinner&ref=home" class="a-no-decoration-black">
				<img src="{{$galleryUrl}}/main/img/home/v1/b18.png" class="category-card">
				<div class="category-card-title category-card-title-mobile">
					<p> Poolside Candle Light Dinner</p>
				</div>
			</a>
		</div>
		<div class="clearfix"></div>
	</section>
@else
		<section class="section-cat no-pad-b" >
			<h5 class="section-heading">CELEBRATE LOVE</h5>
			<p class="section-sub-heading">Let the time stop & rekindle the moment </p>
			<div class="col-sm-12 pad-b-20 no-mar-b " style="float: none;" >
				<div class="col-sm-6  text-center no-pad-l" style="height:470px;overflow: hidden;">
					<a href="{{ route("city.cld.list", getCityUrl()) }}?category=best-rooftop-candle-light-dinner&ref=home" class="a-no-decoration-black">
						<img src="{{$galleryUrl}}/main/img/home/v1/cld1234.png" class="category-card category-three-card-hero-img">
						<div class="three-card-cat">
							<h6>Rooftop Candle Light Dinners</h6>
						</div>
					</a>
				</div>
				<div class="col-xs-6 row no-pad-r no-mar-r no-mar-l" style="height:470px;overflow: hidden;float: none">
					<div class="full-width text-left row pad-l-10 no-mar-l no-mar-r" >
						<a href="{{ route("city.cld.list", getCityUrl()) }}?category=best-pool-side-candle-light-dinner&ref=home" class="a-no-decoration-black flex-box">
							<div class="col-sm-7 no-pad-r" style="overflow: hidden; height: 207px">
								<img src="{{$galleryUrl}}/main/img/home/v1/cldhome2.png" style="height: 207px;width:auto;">
							</div>
							<div class="col-sm-5 no-pad-l no-pad-r flex-box category-three-card-sub" >
								<h6>Poolside Candle light Dinner<br><span>+Explore</span></h6>
							</div>
						</a>
					</div>
					<div class="full-width text-left pad-l-10 mar-t-15 no-mar-r no-mar-l">
						<a href="{{ route("city.cld.list", getCityUrl()) }}?category=best-moon-light-candle-light-dinner&ref=home" class="a-no-decoration-black flex-box" >
							<div class="col-sm-7 no-pad-r"  style="overflow: hidden; height: 207px">
								<img src="{{$galleryUrl}}/main/img/home/v1/cldhome3.png" class="full-width" style="height: 207px;width:auto;">
							</div>
							<div class="col-sm-5 no-pad-l no-pad-r flex-box category-three-card-sub">
								<h6>Moonlight Candle Light Dinner<br><span>+Explore</span></h6>
							</div>
						</a>
					</div>

				</div>
			</div>


		</section>
	@endif
	<div class="clearfix"></div>
