@if($agent->isMobile())
	@php $itemCount=1;@endphp
	<section class="section-cat mar-t-20 no-pad-b ">
		<h6 class="no-mar-b blog-title-mobile">OUR STORIES</h6>
		<p class="section-sub-heading">Recent posts from our blog</p>
		@foreach($data as $item)
			@if($itemCount==4)
				@break
			@endif
			<div class="container-fluid col-xs-12 blog-container-mobile">
				<div class="card item full-width" style="box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);">
					<a href="#">
						<div class="col-xs-5 blog-img-container-mobile" style="background: url('{{$item['image']}}') no-repeat center center;background-size:contain;"></div>
						<div class="col-xs-7 blog-text-wrap">
							<p class="no-mar">{{$item['title']}}</p>
						</div>
					</a>
					<div class="clearfix"></div>
				</div>
			</div>
			@php $itemCount++;@endphp
		@endforeach
		<div class="text-center mar-t-20 mar-b-10">
			<a class="btn btn-link link-evibe-stories no-mar-t" href="/blog?ref=home">
				Read All Stories
			</a>
		</div>
		<div class="clearfix"></div>
	</section>

@else
	<section class="section-cat mar-t-20">
		<h5 class="section-heading">OUR STORIES</h5>
		<p class="section-sub-heading">Recent posts from our blog</p>
		@foreach($data as $item)

			<div class="col-sm-12 col-md-3 col-lg-3 text-center mar-b-15">
				<div class="card item mar-r-20 blog-container-desk">
					<div class="blog-img-container-desk">
						<img class="card-img-top" src="{{$item['image']}}" alt="Cover Image of {{$item['title']}}">
					</div>
					<div class="card-body" style="height: 150px;">
						<h5 class="card-title blog-title-desk">{{$item['title']}}</h5>
						<p class="card-text text-left pad-l-15 pad-r-15 blog-content-desk">{{$item['content']}}</p>
						<a class="card-text text-center btn-link blog-cta" href="{{$item['link']}}">Read Complete Article</a>
					</div>
				</div>
			</div>
		@endforeach
		<div class="text-center mar-t-20">
			<a class="btn btn-link link-evibe-stories no-mar-t" href="/blog?ref=home">
				Read All Stories
			</a>
		</div>
		<div class="clearfix"></div>
	</section>
@endif