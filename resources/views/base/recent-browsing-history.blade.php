@extends('layout.base')

@section('page-title')
	<title>Recent browsing history | Evibe.in</title>
@endsection

@section('meta-description')
@endsection

@section('meta-keywords')
	<meta name="keywords" content="user browsing history"/>
@endsection

@section("header")
	@include('base.home.header.header-home-city')
	<div class="header-border"></div>
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-list.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/app/page-party-bag-list.css') }}"/>
@endsection

@section("content")
	@if(count($data) > 0)
		@if(!($agent->isMobile() && !($agent->isTablet())))
			@include("base.browsing-history.desktop.content-wrap")
		@else
			@include("base.browsing-history.mobile.content-wrap")
		@endif
	@else
		<div class="bg-white desktop-list-page col-sm-12 col-md-12 col-lg-12 row " style="margin-bottom:30px;padding-top: 20px">
			<div class="container text-center">
				<h1 class="page-heading">YOUR RECENTLY VIEWED ITEMS</h1>
				<div class="font-16 pad-b-15" style="color: #94989F;">Currently you have no recently viewed items.<br> After viewing product detail pages, look here to find an easy way to navigate back to pages you are interested in.</div>
				<div>
					<img height="150" src="{{ $galleryUrl }}/main/img/icons/wishlist-empty.png" alt="wishlist">
				</div>
				<div class="pad-t-20 mar-b-30">
					<a href="/" class="btn btn-partybag-options">
						Browse Now
					</a>
				</div>
			</div>
		</div>
	@endif
	<div class="clearfix"></div>
@endsection

@section("footer")
	<div class="footer-wrap">
		@include('app.footer_noncity')
	</div>
@endsection
