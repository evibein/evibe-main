@php $cityUrl = isset($data['cityUrl']) ? "in " . $data['cityUrl'] : ""; @endphp
<div class="col-md-4 col-lg-4 col-sm-4 col-xs-6 @if($agent->isMobile()) mar-t-20 @else mar-t-10 @endif">
	<div class="des-list-card-wrap">
		<div class="list-option-card @if(!($agent->isMobile())) des-list-option-card  h-300 @endif">
			<div class="list-option-image-wrap campaign-image-wrap  text-left" style="overflow: hidden;position: relative;">
				<a class="list-option-link blur-bg-wrap" href="{{ $card['link'] }}?ref=campaign_img">
					<img src="{{$card['img']}}" class="thumb-img list-option-image initial loaded" alt="{{$card['title']}} in {{ $cityUrl }} @ {{$card['price']}}">
					<img src="{{$card['img']}}" class="blur-bg list-option-image initial loaded" alt="{{$card['title']}} in {{ $cityUrl }} @ {{$card['price']}}">
				</a>
			</div>
			<div class="list-option-content-wrap no-pad-r" style="padding-left:5px; @if(isset($data['extraHeight']) && $data['extraHeight']) height: 106px; @else height: 86px; @endif">
				<div class="list-campaign-option-title list-option-title">
					<a class="list-option-link" href="{{$card['link']}}" style="text-overflow: ellipsis;">
						{{ $card['title'] }}
					</a>
				</div>
				@if(!($agent->isMobile() && !($agent->isTablet())))
					@if(isset($card['occasionId']) && !(($card['occasionId'] == config("evibe.occasion.surprises.id")) || ($card["ptid"] == config('evibe.ticket.type.service'))))
						<div class="list-option-cta-wrap text-center">
							<div class="no-pad">
								<div class="col-xs-6 no-pad">
									<a class="item-shortlist" v-on:click="shortlistOptionFromListPage">
										<div class="hideShortlistResultsData list-cta-btn list-btn-shortlist" data-mapId="{{ $card['id'] }}" data-mapTypeId="{{ $card['ptid'] }}" data-cityId="{{ $data['campaignData']['city'] }}" data-occasionId="{{ $card['occasionId'] }}" data-urlAdd="{{ route('partybag.add') }}" data-urlDelete="{{ route('partybag.delete') }}">
											Add To Cart
										</div>
									</a>
								</div>
								<div class="col-xs-6 no-pad">
									<a href="{{$card['link']}}#bookNowModal" class="item-book-now" target="_blank">
										<div class="list-cta-btn list-btn-book-now">
											Book Now
										</div>
									</a>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					@else
						<div class="list-option-cta-wrap text-center no-pad">
							<a href="{{$card['link']}}#bookNowModal" class="item-book-now" target="_blank">
								<div class="list-cta-btn list-btn-book-now no-border">
									Book Now
								</div>
							</a>
						</div>
					@endif
				@endif
				<div>
					@if($card['priceworth'])
						<div class="in-blk" style="text-decoration: line-through;">
							<span class="rupee-font">₹&nbsp;</span>
							<span class="price-worth">{{$card['priceworth']}}</span>
						</div>
					@endif
					@if(isset($card['price']))
						<div class="list-main-price in-blk pad-l-5 mar-r-10">
							@price($card['price'])
							<span class="mar-l-4 list-price-kg">{{$card['metric']}}</span>
						</div>
					@endif
					@if(isset($card['availableSlots']) && isset($data['extraHeight']) && $data['extraHeight'])
						@if($card['availableSlots'] > 15)
							<div class="in-blk text-g">new arrival</div>
						@elseif($card['availableSlots'] > 10)
							<div class="in-blk text-o">only {{ $card['availableSlots'] }} left</div>
						@elseif($card['availableSlots'] > 0)
							<div class="in-blk text-o">filling fast, only {{ $card['availableSlots'] }} left</div>
						@else
							<div class="in-blk text-r">sold out!</div>
						@endif
					@endif
				</div>
				@if(isset($card['avgRating']) && isset($card['reviewCount']) && $card['reviewCount'] > 0)
					<div class="full-width list-option-rating no-pad-l">
						<span class="mar-r-4">
							<input type="number" class="product-avg-rating hide" value="{{ $card['avgRating'] }}" title="{{ $card['avgRating'] }} average rating for provider of {{ $card['id'] }}"/>
						</span>
						<span class="list-rating-count in-blk">({{ $card['reviewCount'] }})</span>
					</div>
				@endif
				@if(isset($card['area']) && !(is_null($card['area'])))
					<div style="padding-top: 3px;font-size: 12px">
						<span class="glyphicon glyphicon-map-marker"></span>
						<span>{{$card['area']}}</span>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>