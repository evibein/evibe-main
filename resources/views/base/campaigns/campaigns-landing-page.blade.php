@extends('layout.base')

@section('page-title')
	<title>{{$data['campaignData']['page_title']}}</title>
@endsection

@section('meta-description')
	<meta name="description" content="{{$data['campaignData']['page_description']}}">
@endsection

@section('meta-keywords')
	<meta name="keywords" content="Top Indian Party Planner"/>
@endsection

@section("header")
	@include('base.home.header.header-home-city')
	<div class="header-border"></div>
@endsection

@section('seo:schema')
	@parent
	@if(isset($data['schema']) && count($data['schema']) > 0)
		@foreach($data['schema'] as $schema)
			{!! $schema !!}
		@endforeach
	@endif
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-list.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/app/page-party-bag-list.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/util/star-rating.css') }}"/>
@endsection

@section("content")
	@php $carouselCount=0; @endphp
	@if(count($data['productsData']) > 0)
		@if(count($data['carouselData'])>0)
			@include('base.campaigns.campaign-carousel', [
									"carouselData" => $data['carouselData'],
									"campaignData" => $data['campaignData']
								])
		@endif
		@php $isMobile = $agent->isMobile(); $isTablet = $agent->isTablet();@endphp

		<!--Products-->
		@if(isset($data['campaignData']['occasion-description']) && $data['campaignData']['occasion-description'])
			<div class="text-left">
				<p>{{$data['campaignData']['occasion-description']}}</p>
			</div>
		@endif
		<div class="desktop-list-page no-pad-t pad-b-30">
			@if(isset($data['campaignData']['occasion_description']) && ($data['campaignData']['occasion_description']))
				<p class="@if(!$isMobile) container @endif text-grey no-mar-b pad-t-20 no-mar-t">{{$data['campaignData']['occasion_description']}}</p>
			@endif
			@foreach($data['productsData'] as $section)
				@if(!($loop->first || $loop->last))
					<hr class="hr-style">
				@endif
				<div class="@if(!($agent->isMobile())) container @endif text-pink">
					<h5 class="mar-t-20 pad-l-15 pad-r-15">{{ $section['sectionName'] }}</h5>
					<p class="pad-l-15 pad-r-15">{{ $section['sectionDesc'] }}</p>
					@php $productCount = 1; @endphp
					@foreach($section['sectionProducts'] as $card)
						@if(isset($card['link']))
							@include("base.campaigns.campaign-products-flat-view")
							@php $productCount++; @endphp
						@endif
						@if(($agent->isMobile()) && ($productCount == 9))
							<div class="clearfix"></div>
						@endif
						@if($loop->last)
							<div class="clearfix"></div>

							{{-- @if($section['sectionUrl'])
								@if(isset($data['hideBrowseAll']) && $data['hideBrowseAll'])
								@else
									<div class="text-center mar-b-30 mar-t-30 text-pink">
										<a class="btn-evibe pad-15" href="{{$section['sectionUrl']}}?ref=campaign" style="padding:10px;letter-spacing: 1.2;text-decoration: none;"> Browse All</a>
									</div>
								@endif
							@endif --}}
						@endif
					@endforeach
				</div>
			@endforeach
			<div class="clearfix"></div>
		</div>
	@else
		<div class="bg-white desktop-list-page col-sm-12 col-md-12 col-lg-12 row" style="margin-bottom:30px; padding-top: 20px;">
			<div class="container text-center">
				<h1 class="page-heading">Its Not You, Its Us.</h1>
				<div class="font-16 pad-b-15" style="color: #94989F;">Currently We Are Facing Issues In Loading This Page. Please Visit After Sometime</div>
				<div>
					<img height="150" src="{{ $galleryUrl }}/main/img/icons/error-404.png" alt="ErrorInLoading">
				</div>
				<div class="pad-t-20 mar-b-30">
					<a href="/" class="btn btn-partybag-options">
						Visit Homepage
					</a>
				</div>
			</div>
		</div>
	@endif

	<!--USP-->
	@include("base.home.why-us")

	@include('app.modals.auto-popup', [
			"cityId" => $data['campaignData']['city'],
			"occasionId" => isset($data["occasionId"]) ? $data["occasionId"] : config("evibe.evibe.occasion.kids_birthdays.id"),
			"mapTypeId" => '1',
			"mapId" => "",
			"optionName" => "",
			"countries" => (isset($data['countries']) && count($data['countries'])) ? $data['countries'] : [],
			"isCampaignPageEnquiry" => "1"
	])
@endsection

@section("footer")
	<div class="footer-wrap">
		@include('app.footer_noncity')
	</div>
@endsection

@section("javascript")
	<script type="text/javascript">
		$.getScript("/js/util/star-rating.js", function () {
			function applyRatingStar(to) {
				$(to).rating({
					showClear: 0,
					readonly: 'true',
					showCaption: 0,
					size: 'xl',
					min: 0,
					max: 5,
					step: 0.1
				});
				$(to).removeClass('hide');
			}

			applyRatingStar('.product-avg-rating');

			$(".allProducts").slideUp();

			/*$(".showAll").on("click", function () {
			 $(".allProducts").slideDown();
			 $(".showAll").hide();
			 });*/
		});
	</script>

@endsection
