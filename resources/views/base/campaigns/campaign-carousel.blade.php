@if($agent->isMobile() && !($agent->isTablet()))
	<div class="intro-header" style="height: 250px">
		<div id="header-carousel-wrap" class="carousel slide " data-ride="carousel"  style="height: 250px">
			<ol class="carousel-indicators" style="bottom: -7px;">
				@php  $count = 0; @endphp
				@if(count($carouselData)>1)
					@foreach($carouselData as $carouselIndicator)
						<li data-target="#header-carousel-wrap" data-slide-to="@php echo $count; @endphp"></li>
						@php  $count++; @endphp
					@endforeach
				@endif
			</ol>
			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				@foreach($carouselData as $slide)
					<div class="item @if($loop->first) active @endif">
						<a href="{{$slide['destination_url']}}" class="a-no-decoration" target="_blank">
							<!--Must Change Below Path -->
							<img src="{{ $galleryUrl }}/main/img/home/{{$slide['mobile_img_url']}}" class="full-width" alt="{{$slide['alt_tag']}}">
						</a>
						<a href="{{$slide['destination_url']}}" class="carousel-caption carousel-caption-style-mobile a-no-decoration" style="font-size:30px;position: absolute;top:18% !important;" target="_blank">
							@if(isset($slide['mobile_img_h1']) && ($slide['mobile_img_h1']!=""))
								@if($campaignData['id']== 11 || $campaignData['id']==12)
									<h1 class="fc-blue-imp no-mar">{{$slide['mobile_img_h1']}}
										@if(isset($slide['mobile_img_h2']) && ($slide['mobile_img_h2']!=""))
											<br>
											<span class="fw-400 font-16 fc-blue-imp">{{$slide['mobile_img_h2']}}</span>
										@endif
									</h1>
								@else
									<h1 class="fc-white no-mar">{{$slide['mobile_img_h1']}}
										@if(isset($slide['mobile_img_h2']) && ($slide['mobile_img_h2']!=""))
											<br>
											<span class="fw-400 font-16">{{$slide['mobile_img_h2']}}</span>
										@endif
									</h1>
								@endif
							@endif
						</a>
						@if(($slide['cta_url']!="") &&($slide['cta_text']!=""))
							<div class="btn btn-evibe btn-sm"><a href="{{$slide['cta_url']}}" class="a-no-decoration" target="_blank">{{$slide['cta_text']}}</a></div>
						@endif
					</div>
				@endforeach
			</div>
		</div>
	</div>
@else
	<!-- Desktop -->
	<div style="height: 300px">
		<div id="header-carousel-wrap" class="carousel slide  header-wrap" data-ride="carousel" style="height: 300px;overflow: hidden">
			@php  $count=0; @endphp
			@if(count($carouselData)>1)
				<ol class="carousel-indicators no-mar-b">
					@foreach($carouselData as $carouselIndicator)
						<li data-target="#header-carousel-wrap" data-slide-to="@php echo $count; @endphp"></li>
						@php  $count++; @endphp
					@endforeach
				</ol>
			@endif
		<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				@foreach($carouselData as $slide)
					<div class="item @if($loop->first) active @endif">
						<a href="{{$slide['destination_url']}}" class="a-no-decoration" target="_blank">
							<img src="{{ $galleryUrl }}/main/img/home/{{$slide['desk_img_url']}}" alt="{{$slide['alt_tag']}}" class="carousel-hero-img">
						</a>
						<div class="carousel-caption hero-carousel-caption-style">
							@if($slide['desk_banner_h1'] != "")
								@if($campaignData['id']== 11 || $campaignData['id']==12)
									<p class="carousel-header fc-blue-imp black-text-shadow">{{$slide['desk_banner_h1']}}</p>
								@else
									<p class="carousel-header black-text-shadow">{{$slide['desk_banner_h1']}}</p>
								@endif
							@endif
							@if($slide['desk_banner_h2'] != "")
								@if($campaignData['id']== 11 || $campaignData['id']==12)
									<p class="carousel-header fc-blue-imp font-26" style="text-shadow: -3px 8px 9px rgba(101,44,27,0.4)">{{$slide['desk_banner_h2']}}</p>
								@else
									<p class="carousel-header font-26" style="text-shadow: -3px 8px 9px rgba(101,44,27,0.4)">{{$slide['desk_banner_h2']}}</p>
								@endif
							@endif
							@if($slide['cta_url']!="" && $slide['cta_text'] != "" )
								<a class="btn btn-evibe btn-md" href="{{$slide['cta_url']}}">{{$slide['cta_text']}}</a>
							@endif
						</div>
					</div>
				@endforeach
			</div>

			@if(count($carouselData) > 1)
			<!-- Controls -->
				<a class="left carousel-control" href="#header-carousel-wrap" role="button" data-slide="prev">
					<img src="{{ $galleryUrl }}/main/img/icons/left-arrow-carousel.svg" class="custom-controls-style control-left" alt="carousel control left">
				</a>
				<a class="right carousel-control" href="#header-carousel-wrap" role="button" data-slide="next">
					<img src="{{ $galleryUrl }}/main/img/icons/right-arrow-carousel.svg" class="custom-controls-style control-right" alt="carousel control right">
				</a>
			@endif
		</div>
	</div>
@endif