@if(!($agent->isMobile() && !($agent->isTablet())))
	@php $i = 1; $carouselCount++; @endphp
	<div id="productCarousel{{$carouselCount}}" class="carousel slide container" data-ride="carousel">
		<!-- Wrapper for slides -->
		<div class="carousel-inner pad-l-15-imp pad-r-15-imp" role="listbox">
			@foreach ($sectionProducts as $product)
				@if(isset($product['link']))
					@if($i % 6 == 1)
						<div class="item @if($i == 1) active @endif">
							@endif
							<div class="col-md-3 col-lg-3 col-sm-4 col-xs-1 no-pad-r pad-b-20 pad-l-20">
								<div class="des-list-card-wrap">
									<div class="list-option-card des-list-option-card">
										<div class="list-option-image-wrap text-left">
											<div class="list-option-image-wrap text-left">
												<a class="list-option-link" href=" {{ $product['link'] }}?ref=campaign_img">
													<img src="{{$product['img']}}" class="list-option-image initial loaded">
												</a>
											</div>

										</div>
										<div class="list-option-content-wrap">
											<div class="list-campaign-option-title">
												<a class="list-option-link" href="{{$product['link']}}">
													{{ $product['title'] }}
												</a>
											</div>
												@if(isset($item['occasionId']))
													@if(($item['occasionId'] == config("evibe.occasion.surprises.id")) || ($item["ticketTypeId"] == config('evibe.ticket.type.service')))
														<div class="list-option-cta-wrap text-center">
															<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-pad text-center">
																<a href="{{$product['link']}}#bookNowModal" class="item-book-now" target="_blank">
																	<div class="list-cta-btn list-btn-book-now">
																		Book Now
																	</div>
																</a>

																<div class="clearfix"></div>
															</div>
															<div class="clearfix"></div>
														</div>
													@else
														<div class="list-option-cta-wrap text-center">
															<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-pad">
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no-pad">
																	<a class="item-shortlist" v-on:click="shortlistOptionFromListPage">
																		<div class="hideShortlistResultsData list-cta-btn list-btn-shortlist" data-mapId="{{ $product['id'] }}" data-mapTypeId="{{ $product['ptid'] }}" data-cityId="{{ $data['campaignData']['city'] }}" data-occasionId="{{ $product['occasionId'] }}" data-urlAdd="{{ route('partybag.add') }}" data-urlDelete="{{ route('partybag.delete') }}">
																			Add To Cart&nbsp;
																		</div>
																	</a>
																</div>
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no-pad">
																	<a href="{{$product['link']}}#bookNowModal" class="item-book-now" target="_blank">
																		<div class="list-cta-btn list-btn-book-now">
																			Book Now
																		</div>
																	</a>
																</div>
																<div class="clearfix"></div>
															</div>
															<div class="clearfix"></div>
														</div>
													@endif

												@endif


										<!-- 	<div class="list-option-price row full-width">
												@if($agent->isMobile() && !($agent->isTablet()))
													<div class="col-sm-6 col-md-12 col-xs-6 no-pad ">
														@else
															<div class="col-sm-6 col-md-12 col-xs-6 text-center  ">
																@endif
																@if(isset($product['price']))
																	<div class="list-main-price in-blk ">
																		@price($product['price'])
																		<span class="mar-l-4 list-price-kg">{{$product['metric']}}</span>
																	</div>

																@endif
																@if($product['priceworth'])
																	<div class=" in-blk " style="font-size: 11px;text-decoration: line-through;margin-left:5px">
																		<span class="rupee-font">₹&nbsp;</span>
																		<span class="price-worth">{{$product['priceworth']}}</span>
																	</div>
																	&nbsp;&nbsp;&nbsp;
																@endif
															</div>

															@if($agent->isMobile() && !($agent->isTablet()))
																<div class=" pull-right col-sm-6 col-md-6 col-xs-6 no-pad">
																	<div class="partybag">
																		<a class="partybag-heart shortlistIcon no-mar-t no-mar-b" style="margin-top: -15px !important;font-size: 14px" v-on:click="shortlistOptionFromListPage" data-plainImg="{{ $galleryUrl }}/main/img/icons/party-bag_empty.png" data-filledImg="{{ $galleryUrl }}/main/img/icons/party-bag_filled.png" data-removeImg="{{ $galleryUrl }}/main/img/icons/party-bag_remove.png">
																			@if (($agent->isMobile() && !($agent->isTablet())))
																				<span class="icon-bagicon5 "></span>
																			@endif
																			<span class="hideShortlistResultsData" data-mapId="{{ $product['id'] }}" data-mapTypeId="{{ $product['ptid'] }}" data-cityId="{{ $data['campaignData']['city']}}" data-occasionId="{{ $product['occasionId'] }}" data-urlAdd="{{ route('partybag.add') }}" data-urlDelete="{{ route('partybag.delete') }}">
																												</span>
																		</a>
																	</div>
																</div>

															@endif

															<div class="clearfix"></div>
													</div>

											</div> -->
											@if(isset($product['avgRating']) && isset($product['reviewCount']))
												<div class="list-option-rating">
													@if($product['reviewCount'] > 0)
														<span class="mar-r-4">
																				<input type="number" class="product-avg-rating hide" value="{{ $product['avgRating'] }}" title="{{ $product['avgRating'] }} average rating for provider of {{ $product['id'] }}"/>
																			</span>
														<span class="list-rating-count in-blk">({{ $product['reviewCount'] }})</span>

													@endif
												</div>
											@endif
										</div>
									</div>
								</div>
								@if($i % 6 == 0)
							</div>
							@endif
							@php $i++; @endphp
							@endif
							@endforeach
							@if(count($section['sectionProducts']) % 6 != 0)
						</div>
					@endif
		</div>
		<a class="left carousel-control product-carousel-slider" href="#productCarousel{{$carouselCount}}" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left font-20-imp"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control product-carousel-slider" href="#productCarousel{{$carouselCount}}" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right font-20-imp"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
@endif