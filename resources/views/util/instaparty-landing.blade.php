@extends('layout.base')

@section('page-title')
	<title>{{ $data['pageTitle'] }} | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="{{ $data['meta']['description'] }}"/>
@endsection

@section('meta-keywords')
	<meta name="keywords" content="{{ $data['meta']['key-words'] }}"/>
@endsection

@section('og-title')
	<meta property="og:title" content="{{ $data['meta']['og-title'] }}"/>
@endsection

@section('og-description')
	<meta property="og:description" content="{{ $data['meta']['og-description'] }}"/>
@endsection

@section('og-url')
	<meta property="og:url" content="{{ $data['meta']['og-url'] }}"/>
@endsection

@section('meta-canonical')
	<link rel="canonical" href="{{ $canonicalUrl }}{{ $data['meta']['canonical'] }}"/>
@endsection

@section('press:title')
@endsection

@section("header")
@endsection

@section("footer")
@endsection

@section("content")
	<div class="landing-header">
		<div class="col-xs-6 no-pad">
			<div class="landing-header-left pull-left pad-l-10 text-center mar-t-5">
				<div class="landing-header-logo-wrap">
					<a href="/">
					<img class="landing-header-logo" src="{{ $galleryUrl }}/img/logo/logo_evibe.png" alt="Evibe.in logo">
					</a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="col-xs-6 no-pad">
			<div class="landing-header-right pull-right pad-r-10">
				<div class="landing-header-text-wrap">
					{{ $data['headerTag'] }}
				</div>
				<div class="landing-header-mobile pull-right">
					<a class="landing-mobile-link" href="tel:9640204000">
						<i class="glyphicon glyphicon-earphone landing-mobile"></i>
						9640204000</a>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="landing-body ip-landing-body">
		<div class="ip-hero-section-wrap">
			<div class="ip-page-title-wrap">
				<div class="ip-page-title">Decorate yourself under <span class="rupee-font">&#8377; </span> 1000</div>
			</div>
			<div class="ip-hero-img-collage-wrap">
				<img src="{{ $data['heroImg'] }}">
			</div>
		</div>
		<div class="ip-cta-wrap text-center pad-t-30 pad-b-15">
			<a href="#ipForm" class="btn btn-landing-submit btn-ip-cta">I'm Interested</a>
		</div>
		<div class="ip-how-it-works-wrap">
			<div class="ip-how-it-works-title">
				How It Works?
			</div>
			<ul class="ip-how-it-works-body">
				<li class="ip-how-it-works-component">
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
						<div class="ip-how-it-works-component-number">1</div>
					</div>
					<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 no-pad-l">
						<div class="ip-how-it-works-component-text">
							Fill the simple form below. We will share a perfect catalog for you.
						</div>
					</div>
					<div class="clearfix"></div>
				</li>
				<li class="ip-how-it-works-arrow text-center">
					@if($agent->isMobile() && !($agent->isTablet()))
						<i class="glyphicon glyphicon-arrow-down"></i>
					@else
						<i class="glyphicon glyphicon-arrow-right"></i>
					@endif
				</li>
				<li class="ip-how-it-works-component">
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
						<div class="ip-how-it-works-component-number">2</div>
					</div>
					<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 no-pad-l">
						<div class="ip-how-it-works-component-text">
							Choose best party supplies you like. We will deliver to your doorstep.
						</div>
					</div>
					<div class="clearfix"></div>
				</li>
				<li class="ip-how-it-works-arrow text-center">
					@if($agent->isMobile() && !($agent->isTablet()))
						<i class="glyphicon glyphicon-arrow-down"></i>
					@else
						<i class="glyphicon glyphicon-arrow-right"></i>
					@endif
				</li>
				<li class="ip-how-it-works-component">
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
						<div class="ip-how-it-works-component-number">3</div>
					</div>
					<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 no-pad-l">
						<div class="ip-how-it-works-component-text">
							Decorate it yourself according to your taste. Click tons of pics :)
						</div>
					</div>
					<div class="clearfix"></div>
				</li>
			</ul>
		</div>
		<div id="ipForm" class="ip-form-wrap mar-t-30 pad-t-30">
			<iframe src="https://docs.google.com/forms/d/e/1FAIpQLScO0RqAzjLnrryfnuziHmYxtYdTU4sAkYblfYPkqYDz3h_unw/viewform?embedded=true" width="640" height="1500" frameborder="0" marginheight="0" marginwidth="0">Loading�</iframe>
		</div>
		<div class="ip-usp-warp text-center">
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 mar-t-30">
				<div class="ip-usp-component">
					<div class="col-xs-4 col-sm-4 col-md-12 col-lg-12">
						<div class="ip-usp-component-img-wrap">
							<img src="{{ $galleryUrl }}/main/landing/party-supplies/price-tag.png">
						</div>
					</div>
					<div class="col-xs-8 col-sm-8 col-md-12 col-lg-12 no-pad-l__400-600 text-left-400-600">
						<div class="ip-usp-component-title">Unbeatable Prices</div>
						<div class="ip-usp-component-text">No more paying exorbitant prices</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 mar-t-30 hide">
				<div class="ip-usp-component">
					<div class="col-xs-4 col-sm-4 col-md-12 col-lg-12">
						<div class="ip-usp-component-img-wrap">
							<img src="{{ $galleryUrl }}/main/landing/party-supplies/fast-truck.png">
						</div>
					</div>
					<div class="col-xs-8 col-sm-8 col-md-12 col-lg-12 no-pad-l__400-600 text-left-400-600">
						<div class="ip-usp-component-title">Free 3 Days Delivery</div>
						<div class="ip-usp-component-text">For All Orders Above <span class="rupee-font">&#8377; </span>300, COD available</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 mar-t-30">
				<div class="ip-usp-component">
					<div class="col-xs-4 col-sm-4 col-md-12 col-lg-12">
						<div class="ip-usp-component-img-wrap">
							<img src="{{ $galleryUrl }}/main/landing/party-supplies/quality-badge.png">
						</div>
					</div>
					<div class="col-xs-8 col-sm-8 col-md-12 col-lg-12 no-pad-l__400-600 text-left-400-600">
						<div class="ip-usp-component-title">Hand-picked Products</div>
						<div class="ip-usp-component-text">Get ready to post HD photos</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 mar-t-30">
				<div class="ip-usp-component">
					<div class="col-xs-4 col-sm-4 col-md-12 col-lg-12">
						<div class="ip-usp-component-img-wrap">
							<img src="{{ $galleryUrl }}/main/landing/party-supplies/one-box.png">
						</div>
					</div>
					<div class="col-xs-8 col-sm-8 col-md-12 col-lg-12 no-pad-l__400-600 text-left-400-600">
						<div class="ip-usp-component-title">One-stop Party Store</div>
						<div class="ip-usp-component-text">Banners, props, foil balloons & more</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="landing-footer">
		<div class="col-xs-6 no-pad">
			<div class="landing-footer-left">
				<div class="landing-copy-text">(c) Evibe.in</div>
			</div>
		</div>
		<div class="col-xs-6 no-pad">
			<div class="landing-footer-right text-right pad-r-20">
				{{ $data['headerTag'] }}
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection

@section("modals")
	@parent
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">


	</script>
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-landing-pages.css') }}"/>

	<style type="text/css" rel="stylesheet" media="all">

		.ip-landing-body {
			border-top: 1px solid #EDEDED;
		}

		.ip-hero-section-wrap {
			position: relative;
		}

		.ip-page-title-wrap {
			position: absolute;
			left: 50%;
			top: 40%;
			margin-left: -376px;
			font-size: 48px;
			color: #FFFFFF;
			text-shadow: 0 0 20px #000000;
			font-weight: 600;
		}

		.ip-page-title {
			margin: 20px 0;
			padding: 0 15px;
			text-transform: capitalize;
			text-align: center;
		}

		.ip-hero-img-collage-wrap {
			max-width: 100%;
		}

		.ip-hero-img-collage-wrap img {
			height: 100%;
			width: 100%;
		}

		.ip-hero-img-wrap img {
			height: 100%;
			min-width: 100%;
		}

		.btn-ip-cta,
		.btn-ip-cta:active {
			color: #FFFFFF;
			font-size: 20px;
			padding: 10px 80px;
			border-radius: 3px;
			box-shadow: 0 0 5px 1px #888888;
			background-color: #DD4444;
			border-color: #DD4444;
			font-weight: 600;
		}

		.btn-ip-cta:hover {
			color: #FFFFFF;
			text-decoration: none;
		}

		.ip-usp-component-img-wrap {
			height: 100px;
			width: 100px;
			margin: 0 auto;
		}

		.ip-usp-component-img-wrap img {
			height: 100%;
			width: 100%;
		}

		.ip-usp-component-title {
			margin-top: 15px;
			font-size: 18px;
			text-transform: uppercase;
		}

		.ip-usp-component-text {
			margin-top: 5px;
			color: #DD4444;
		}

		.ip-how-it-works-wrap {
			border: 1px solid #DFDFDF;
			margin: 20px 20px 0 20px;
			border-radius: 10px;
		}

		.ip-how-it-works-title {
			font-size: 18px;
			font-weight: 600;
			text-align: center;
			padding: 10px 0;
			border-bottom: 1px solid #DFDFDF;
		}

		.ip-how-it-works-body {
			padding: 15px 0;
			margin: 0;
			line-height: 20px;
			font-size: 14px;
			list-style-type: none;
			display: table;
			width: 100%;
		}

		.ip-how-it-works-body li {
			display: table-cell;
			padding-top: 10px;
		}

		.ip-how-it-works-component {
			color: #6F6F6F;
			width: 30%;
		}

		.ip-how-it-works-component-number {
			font-size: 24px;
			border: 2px solid #6F6F6F;
			border-radius: 100%;
			display: inline-block;
			height: 40px;
			width: 40px;
			line-height: 40px;
			/*font-weight: 600;*/
		}

		.ip-how-it-works-component-text {
			font-size: 16px;
			line-height: 22px;
		}

		.ip-how-it-works-arrow {
			color: #6F6F6F;
			vertical-align: middle;
			padding: 5px 0;
			width: 5%;
		}

		.ip-usp-warp {
			padding-bottom: 50px;
		}

		.ip-form-wrap {
			border-bottom: 1px solid #DFDFDF;
			margin-bottom: 30px;
		}

		@media (max-width: 600px) {

			.ip-page-title-wrap {
				top: 30%;
				margin-left: -195px;
				font-size: 24px;
			}

			.btn-ip-cta,
			.btn-ip-cta:active {
				font-size: 18px;
				padding: 6px 50px;
			}

			.ip-usp-component-img-wrap {
				height: 70px;
				width: 70px;
			}

			.ip-usp-component-title {
				font-size: 17px;
			}

			.ip-how-it-works-body {
				display: block;
			}

			.ip-how-it-works-body li {
				display: block;
			}

			.ip-how-it-works-component {
				width: 100%;
			}

			.ip-how-it-works-arrow {
				width: 100%;
			}

			.ip-form-wrap {
				margin-bottom: 15px;
			}

		}

	</style>
@endsection