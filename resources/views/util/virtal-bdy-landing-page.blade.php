@extends('layout.base')

@section('page-title')
	<title>{{$data['seoTitle']}}</title>
@endsection

@section('meta-description')
	<meta name="description" content="{{$data['seoDesc']}}">
@endsection

@section('meta-keywords')
	<meta name="keywords" content="{{$data['keywords']}}"/>
@endsection

@section("header")
	@include('base.home.header.header-home-city')
	<div class="header-border"></div>
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-list.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/app/page-party-bag-list.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/app/vday-landingpage.css') }}"/>
	
@endsection

@section("content")
	<div class="w-1366 ">
		@if($agent->isMobile())
		{{-- Header Image --}}
			<div class="full-width" style="display:flex;align-items:center;flex-direction:column;justify-content:center;align-content:center;background-image:url('{{ $galleryUrl}}/main/img/hero-bg-vbp.png');background-size:cover;height:400px">
				<h1 style='color: white;    display:flex;    /* padding-top: 23%; */    text-align: center;    font-size: 38px;'>Virtual Birthday Party</h1>
				<p style='padding-left:15px;padding-right:15px;font-size: 14px;;text-align: center;color: #fff;    letter-spacing: 0.04rem;'>The better way of celebrating your child’s birthday party with your loved ones amidst this Covid-19 lockdown. You might be at home, but you can still have a memorable celebration.</p>
				<a href=" https://docs.google.com/forms/d/e/1FAIpQLSeuz42mOPWzTV3Zt-2L1foKPllxqz5J8KzbFU8JyFudFmdovw/viewform?usp=sf_link" class="btn btn-evibe btn-md " style="border-color:transparent;font-weight: 500;    letter-spacing: 1px;border-radius:4px">Enquire Now</a>
			</div>
				
		@else
			<div class="full-width" style="display:flex;align-items:center;flex-direction:column;justify-content:center;align-content:center;background-image:url('{{ $galleryUrl}}/main/img/hero-bg-vbp.png');background-size:cover;height:400px">
				<h1 style='color: white;    display:flex;    /* padding-top: 23%; */    text-align: center;    font-size: 40px;'>Virtual Birthday Party</h1>
				<p style='font-size: 18px;line-height:27px;text-align: center;color: #fff'>The better way of celebrating your child’s birthday party<br> with your loved ones amidst this Covid-19 lockdown. <br>You might be at home, but you can still have a memorable celebration.</p>
				<a target="_blank" href=" https://docs.google.com/forms/d/e/1FAIpQLSeuz42mOPWzTV3Zt-2L1foKPllxqz5J8KzbFU8JyFudFmdovw/viewform?usp=sf_link" class="btn btn-evibe btn-md " style="border-color:transparent;font-weight: 500;    letter-spacing: 1px;border-radius:4px">Enquire Now</a>
			</div>
		@endif

		<div class="full-width pad-l-15 pad-r-15 ">
			<h6 class="section-heading section-heading-v2 text-center">Awesome Birthday Party Packages</h6>
	
				<div class="col-sm-12 col-md-6" style="margin-top:35px">
					<div class="col-xs-12 text-center" style="min-height:380px;;box-shadow: 0 0rem 2rem rgba(0, 0, 0, 0.1); border-radius: 0.4rem;;  transition: 0.2s;border:1px solid rgb(242, 225, 225)">
						<h4 style="color:#333333cf!important">Gold Package </h4>
						<div><span style="color:#777;text-decoration:line-through;">₹1999</span>&nbsp;&nbsp;<span style="font-size:24px;font-weight:500;color:#ed3e72">₹999/-</span></div>
						<ul style="font-size:16px;list-style-type: square; list-style-position: outside;margin-top:15px;text-align: left">
							<li>Video conference for you and your guests to join the party (up to 15 kids)</li>
							<li>Birthday invite with the party link to share with your guests</li>
							<li>Party atmosphere for your child and their friends to celebrate</li>
							<li>Birthday party anchor to host your party</li>
							<li>The host will welcome and greet your guests, conduct a fun game with music.</li>
							<li>Sing together happy birthday</li>
							<li>Total party duration: 30 mins</li>

						</ul>
						<a target="_blank" href=" https://docs.google.com/forms/d/e/1FAIpQLSeuz42mOPWzTV3Zt-2L1foKPllxqz5J8KzbFU8JyFudFmdovw/viewform?usp=sf_link" class="btn btn-evibe btn-md  mar-b-20" style="border-color:transparent;font-weight: 500;    letter-spacing: 1px;border-radius:4px">Enquire Now</a>
					</div>
				</div>
				@if($agent->isMobile())	
					<div class="clearfix"></div>
				@endif
				<div class="col-sm-12 col-md-6" style="margin-top:35px">
					<div class="col-xs-12 text-center" style="min-height:380px;box-shadow: 0 0rem 2rem rgba(0, 0, 0, 0.1); border-radius: 0.4rem;;  transition: 0.2s;border:1px solid rgb(242, 225, 225)">
						<h4 style="color:#333333cf!important">Diamond Package </h4>
						<div><span style="text-decoration:line-through;">₹2999</span>&nbsp;&nbsp;<span style="font-size:24px;font-weight:500;color:#ed3e72">₹1499/-</span></div>
						<ul style="font-size:16px;list-style-type: square; list-style-position: outside;margin-top:15px;text-align: left">
							<li>Video conference for you and your guests to join the party (up to 25 kids)</li>
							<li>Birthday invite with the party link to share with your guests</li>
							<li>Party atmosphere for your child and their friends to celebrate</li>
							<li>Birthday party anchor to host your party</li>
							<li>The host will welcome and greet your guests, conduct two fun games with music & dance.</li>
							<li>Sing together happy birthday</li>
							<li>Total party duration: 60 minutes</li>

						</ul>
						<a target="_blank" href=" https://docs.google.com/forms/d/e/1FAIpQLSeuz42mOPWzTV3Zt-2L1foKPllxqz5J8KzbFU8JyFudFmdovw/viewform?usp=sf_link" class="btn btn-evibe btn-md  mar-b-20" style="border-color:transparent;font-weight: 500;    letter-spacing: 1px;border-radius:4px">Enquire Now</a>
					</div>

				</div>	
		</div>
		<div class="clearfix"></div>
		<div class="full-width text-center pad-l-15 pad-r-15" style="margin-top: 15px;overflow: hidden">
			<h6 class="section-heading section-heading-v2 text-center" style="padding-bottom:35px">A Peak Into The Experience</h6>
			{{-- <img src="https://aksharadaan.net/other-assets/desk-youtube-thumbnail.png" style="width:100%"> --}}
			@if($agent->isMobile())
				<iframe width="560" height="315" src="https://www.youtube.com/embed/4UeL_U4i7s0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			@else
				<iframe width="560" height="400" src="https://www.youtube.com/embed/4UeL_U4i7s0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

			@endif
		</div>

		<div class="clearfix"></div>
		<div class="full-width text-center" style="margin-top: 40px;">
			<h6 class="section-heading section-heading-v2 text-center" style="padding-bottom:35px">Add-ons</h6>
			<div class="col-xs-12">
				<div class="col-xs-12 col-md-8 col-md-offset-2" style="box-shadow: 0 3rem 6rem rgba(0, 0, 0, 0.1); border-radius: 0.4rem;;  transition: 0.2s;border:1px solid rgb(242, 225, 225)">
					<ul style="font-size:16px;list-style-type: circle; list-style-position: outside;margin-top:15px;text-align: left">
						<li>Party video recording video link (on YouTube) - INR 500</li>
						<li>Virtual birthday party theme backdrop for the birthday child - INR 500</li>
						<li>Magic show - INR 1500</li>
						<li>Other entertainment activities like Digital Caricature, Puppet Show, Clown Show, Craft Activity, etc., are available on demand.</li>


					</ul>
				</div>
				<div class="clearfix"></div>
				<a target="_blank" href=" https://docs.google.com/forms/d/e/1FAIpQLSeuz42mOPWzTV3Zt-2L1foKPllxqz5J8KzbFU8JyFudFmdovw/viewform?usp=sf_link" class="btn btn-evibe btn-md  mar-t-30" style="border-color:transparent;font-weight: 500;    letter-spacing: 1px;border-radius:4px">Enquire Now</a>

			</div>
		</div>
		<div class="clearfix"></div>
		<div class="full-width text-center pad-l-15 pad-r-15 " style="margin-top: 40px;padding-bottom:40px">
			<h6 class="section-heading section-heading-v2 text-center" style="padding-bottom:35px">How It Works</h6>
			<div class="text-center">
				<div class=" col-xs-12 col-md-4 text-center" >
					<div class="text-center"  style="font-size: 28px;height:85px;width:85px;border-radius:50%;margin:0 auto; box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);display:flex;align-items:center;align-content:center;justify-content:center;flex-direction:column;" >1</div>

					<div class="mar-t-15" style="font-weight: 600;font-size:16px">Package Selection
						<p style="font-size:16px;font-weight:normal;margin-top:10px">Our team will assist you in selecting the best virtual party package suitable for you</p></div>
				</div>
				@if($agent->isMobile())	
					<div class="clearfix"></div>
				@endif
				<div class="col-xs-12 col-md-4 text-center" > 
						<div class="text-center"  style="font-size: 28px;height:85px;width:85px;border-radius:50%;margin:0 auto; box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);display:flex;align-items:center;align-content:center;justify-content:center;flex-direction:column;" >2</div>

					<div class="mar-t-15" style="font-weight: 600;font-size:16px">Make Payment
						<p style="font-size:16px;font-weight:normal;margin-top:10px">Make payment through hassle free UPI payments</p></div>
				</div>
				@if($agent->isMobile())	
					<div class="clearfix"></div>
				@endif
				<div class="col-xs-12 col-md-4 text-center"> 
						<div class="text-center"  style="font-size: 28px;height:85px;width:85px;border-radius:50%;margin:0 auto; box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);display:flex;align-items:center;align-content:center;justify-content:center;flex-direction:column;" >3</div>

					<div class="mar-t-15"  style="font-weight: 600;font-size:16px">Party On!
						<p style="font-size:16px;font-weight:normal;margin-top:10px">An e-invite and a virtual party guide will be shared along with the party link to help you celebrate a memorable party.</p></div>
				</div>
				
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="full-width text-center">
			<a target="_blank" href=" https://docs.google.com/forms/d/e/1FAIpQLSeuz42mOPWzTV3Zt-2L1foKPllxqz5J8KzbFU8JyFudFmdovw/viewform?usp=sf_link" class="btn btn-evibe btn-md  mar-b-30 mar-t-30" style="border-color:transparent;font-weight: 500;    letter-spacing: 1px;border-radius:4px">Enquire Now</a>
		</div>



	</div>	

@endsection

@section("footer")
	<div class="footer-wrap">
		@include('app.footer_noncity')
	</div>
@endsection