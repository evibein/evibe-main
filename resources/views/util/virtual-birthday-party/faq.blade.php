<section class="faq">
	<h3 class="section-heading" style="  text-align:center;margin-top:0 !important">
		Frequently asked questions
	</h3>
	<div class="container">


		<ul class="faq-list">
			<li data-aos="fade-up" data-aos-delay="100" class="aos-init aos-animate">
				<a data-toggle="collapse" class="collapsed" href="#faq1" aria-expanded="true">What is Evibe.in?<span><i class="	glyphicon glyphicon-arrow-up"></i></span></a>
				<div id="faq1" class="collapse in" data-parent=".faq-list" style="">
					<p>
						Evibe.in is the one-stop platform to plan celebrations like birthdays, anniversaries & more completely stress-free. Evibe is a top rated and trusted platform by customers and has touched over half a million happy guests. With the new innovative Virtual Celebrations offering, Evibe is helping people across the globe to celebrate their special occasions.
					</p>
				</div>
			</li>

			<li data-aos="fade-up" data-aos-delay="200" class="aos-init aos-animate">
				<a data-toggle="collapse" href="#faq2" class="collapsed">What are Virtual Celebrations?<i class="	glyphicon glyphicon-arrow-up"></i></a>
				<div id="faq2" class="collapse" data-parent=".faq-list">
					<p>
						Virtual celebrations are just like your normal parties but happen over a video call and are a lot more fun. The entire experience is designed around the birthday child and is professionally organised with an experienced anchor to welcome, greet and entertain your guests.
					</p>
				</div>
			</li>

			<li data-aos="fade-up" data-aos-delay="300" class="aos-init aos-animate">
				<a data-toggle="collapse" href="#faq3" class="collapsed">Which video conference call software do you use?<span style="display:inline-block"><i class="	glyphicon glyphicon-arrow-up"></i></span></a>
				<div id="faq3" class="collapse" data-parent=".faq-list">
					<p>
						Currently, we partnered with Zoom to organise the video call with all the privacy settings ensured. We are also working on our own video conference app reimagined with celebrations as core
					</p>
				</div>
			</li>

			<li data-aos="fade-up" data-aos-delay="400" class="aos-init aos-animate">
				<a data-toggle="collapse" href="#faq4" class="collapsed">Can I see the video of any virtual birthday party?<i class="	glyphicon glyphicon-arrow-up"></i></a>
				<div id="faq4" class="collapse" data-parent=".faq-list">
					<p>
						Just like we want to protect your privacy, we want to do it for other customers too and hence we cannot provide you with the link to a complete virtual celebration. However, you can checkout this short video mashup: &nbsp;&nbsp;<a href="https://youtu.be/UJH7xeHbav4" target="_blank" class="a-no-decoration in-blk-imp" style="font-weight:500;text-decoration:underline">https://youtu.be/UJH7xeHbav4 </a>
					</p>
				</div>
			</li>

			<li data-aos="fade-up" data-aos-delay="500" class="aos-init aos-animate">
				<a data-toggle="collapse" href="#faq5" class="collapsed">I have more guests, what should I do?<i class="	glyphicon glyphicon-arrow-up"></i></a>
				<div id="faq5" class="collapse" data-parent=".faq-list">
					<p>
						The maximum of 20 unique logins is designed to have minimal technical glitches and to engage all the audience. With more guests joining beyond the limit it becomes hard to engage and we charge extra on pro-rata basis for the same.

					</p>
				</div>
			</li>

			<li data-aos="fade-up" data-aos-delay="600" class="aos-init aos-animate">
				<a data-toggle="collapse" href="#faq6" class="collapsed">What is the “wishes platform”?<i class="	glyphicon glyphicon-arrow-up"></i></a>
				<div id="faq6" class="collapse" data-parent=".faq-list">
					<p>
						We provide you with a unique link that you can use while inviting your guests. On this page, your guests can send wishes and dedications such as videos, messages, photos, sing poems, songs etc to wish your child.

					</p>
				</div>
			</li>
			<li data-aos="fade-up" data-aos-delay="600" class="aos-init aos-animate">
				<a data-toggle="collapse" href="#faq7" class="collapsed">Will the “wishes platform” link expire after my party?<i class="	glyphicon glyphicon-arrow-up"></i></a>
				<div id="faq7" class="collapse" data-parent=".faq-list">
					<p>
						The platform will be private and forever free for you to come back and reminiscence all the wonderful wishes from your family and friends.

					</p>
				</div>
			</li>
			<li data-aos="fade-up" data-aos-delay="600" class="aos-init aos-animate">
				<a data-toggle="collapse" href="#faq8" class="collapsed">Can I choose our own songs?<i class="	glyphicon glyphicon-arrow-up"></i></a>
				<div id="faq8" class="collapse" data-parent=".faq-list">
					<p>
						Absolutely, you can share the list in the party details form once your booking is done.

					</p>
				</div>
			</li>
			<li data-aos="fade-up" data-aos-delay="600" class="aos-init aos-animate">
				<a data-toggle="collapse" href="#faq9" class="collapsed">Can I have my child photo on the invite?<i class="	glyphicon glyphicon-arrow-up"></i></a>
				<div id="faq9" class="collapse" data-parent=".faq-list">
					<p>
						Yes, our invites are designed for personalisation. At the time of booking, you can upload your child’s picture.


					</p>
				</div>
			</li>
			<li data-aos="fade-up" data-aos-delay="600" class="aos-init aos-animate">
				<a data-toggle="collapse" href="#faq10" class="collapsed">Who is the anchor?<i class="	glyphicon glyphicon-arrow-up"></i></a>
				<div id="faq10" class="collapse" data-parent=".faq-list">
					<p>
						All our anchors are experienced, background checked and trained before. We assure you a professional celebration experience.


					</p>
				</div>
			</li>
			<li data-aos="fade-up" data-aos-delay="600" class="aos-init aos-animate">
				<a data-toggle="collapse" href="#faq11" class="collapsed">In which language does the anchor speak?<i class="	glyphicon glyphicon-arrow-up"></i></a>
				<div id="faq11" class="collapse" data-parent=".faq-list">
					<p>
						English is the primary language to host the party. If you are specific about having any other language, please choose the “Language Preference” add-on, we will try to match with the best anchor suiting your requirements.


					</p>
				</div>
			</li>
			<li data-aos="fade-up" data-aos-delay="600" class="aos-init aos-animate">
				<a data-toggle="collapse" href="#faq16" class="collapsed">Can I talk to the anchor before the party?<i class="  glyphicon glyphicon-arrow-up"></i></a>
				<div id="faq16" class="collapse" data-parent=".faq-list">
					<p>
						The entire party script is prepared by Evibe and the anchor will just execute during the party slot you have booked. So we don't have any option to talk to the anchor before your party.

					</p>
				</div>
			</li>
			<li data-aos="fade-up" data-aos-delay="600" class="aos-init aos-animate">
				<a data-toggle="collapse" href="#faq12" class="collapsed">Can I get the digital invite of my own design?<i class="  glyphicon glyphicon-arrow-up"></i></a>
				<div id="faq12" class="collapse" data-parent=".faq-list">
					<p>
						Our digital invitations are complimentary and are personalised with your party details. However, we have 6 templates for you to select at the time of booking . Please note any other customisations can be taken on request and for extra charges.

					</p>
				</div>
			</li>
			<li data-aos="fade-up" data-aos-delay="600" class="aos-init aos-animate">
				<a data-toggle="collapse" href="#faq13" class="collapsed">Can I select the games?<i class="  glyphicon glyphicon-arrow-up"></i></a>
				<div id="faq13" class="collapse" data-parent=".faq-list">
					<p>
						We have curated games list that are suited the best for virtual parties and it is provided to you for selection at the time of booking.

					</p>
				</div>
			</li>
			<li data-aos="fade-up" data-aos-delay="600" class="aos-init aos-animate">
				<a data-toggle="collapse" href="#faq14" class="collapsed">Can I increase the party duration?<i class="  glyphicon glyphicon-arrow-up"></i></a>
				<div id="faq14" class="collapse" data-parent=".faq-list">
					<p>
						We take multiple parties for a day and we usually keep a 5 minutes buffer just in case. So, if you need any extension beyond that you need to let us know while making a booking, it would be chargeable on a pro-rata basis.

					</p>
				</div>
			</li>
			<li data-aos="fade-up" data-aos-delay="600" class="aos-init aos-animate">
				<a data-toggle="collapse" href="#faq15" class="collapsed">What kind of games emcee will conduct any sample video?<i class="  glyphicon glyphicon-arrow-up"></i></a>
				<div id="faq15" class="collapse" data-parent=".faq-list">
					<p>
						We have tried over 100 games and shortlisted the best ones that can work well online without any extra arrangements or efforts from your side. We have hosted virtual parties for all ages and we have games mapped out accordingly. Post payment, you will receive a party form to put in your details along with the games list for your selection. 

					</p>
				</div>
			</li>
			<li data-aos="fade-up" data-aos-delay="600" class="aos-init aos-animate">
				<a data-toggle="collapse" href="#faq17" class="collapsed">How does virtual backdrop work?<i class="  glyphicon glyphicon-arrow-up"></i></a>
				<div id="faq17" class="collapse" data-parent=".faq-list">
					<p>
						Virtual backdrop is a feature on zoom so we suggest you test your zoom with a dummy theme image as a virtual backdrop on your device. If it works, you can opt in for this customised theme decorated backdrop image instead of doing elaborate decorations at your home.
					</p>
				</div>
			</li>
			<li data-aos="fade-up" data-aos-delay="600" class="aos-init aos-animate">
				<a data-toggle="collapse" href="#faq18" class="collapsed">Who is the point of contact for the event?<i class="  glyphicon glyphicon-arrow-up"></i></a>
				<div id="faq18" class="collapse" data-parent=".faq-list">
					<p>
						A team member from Evibe.in will be your point of contact as the whole script is prepared from our side and you will meet the artist directly at your party time on the video call. The team member details will be mentioned to you when you make a booking.					</p>
				</div>
			</li>
			<li data-aos="fade-up" data-aos-delay="600" class="aos-init aos-animate">
				<a data-toggle="collapse" href="#faq19" class="collapsed">How to differentiate kids event surprise packages? Do you have any surprise packages for adults or seniors?<i class="  glyphicon glyphicon-arrow-up"></i></a>
				<div id="faq19" class="collapse" data-parent=".faq-list">
					<p>
						If you are interested in hiring an anchor for your surprise, we can change the games format and it is a lot of fun for all ages. Otherwise you can opt in for a guitarist, stand up comedian etc as well and we can help you with the details and charges
					</p>
				</div>
			</li>
			<li data-aos="fade-up" data-aos-delay="600" class="aos-init aos-animate">
				<a data-toggle="collapse" href="#faq20" class="collapsed">What is the speed of the internet required for the event?<i class="  glyphicon glyphicon-arrow-up"></i></a>
				<div id="faq20" class="collapse" data-parent=".faq-list">
					<p>
						It is working well with most of the Indian Internet connection. To give you an example - it would work well with 1 MBPS connection. We however suggest you to have a test run from your side before you make a booking.
					</p>
				</div>
			</li>
			<li data-aos="fade-up" data-aos-delay="600" class="aos-init aos-animate">
				<a data-toggle="collapse" href="#faq21" class="collapsed">Why are you charging the amount whereas in zoom we can celebrate from our end?<i class="  glyphicon glyphicon-arrow-up"></i></a>
				<div id="faq21" class="collapse" data-parent=".faq-list">
					<p>
						Zoom is a software that is free for 45 minutes where the customer needs to manage everything. We are using zoom enterprise account which doesn’t end at 45 minutes and our services are bundled with artists to take care of the entire party
					</p>
				</div>
			</li>
			<li data-aos="fade-up" data-aos-delay="600" class="aos-init aos-animate">
				<a data-toggle="collapse" href="#faq22" class="collapsed">How does the duration work if we book add-on artists along with stellar vibes package?<i class="  glyphicon glyphicon-arrow-up"></i></a>
				<div id="faq22" class="collapse" data-parent=".faq-list">
					<p>
						If you opt for an add-on like magic show, puppet show etc. It will start after one hour session is concluded by the host
					</p>
				</div>
			</li>
		</ul>
	</div>
</section>