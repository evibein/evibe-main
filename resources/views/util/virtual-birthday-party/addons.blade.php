@php $currentUrlPath = request()->path(); @endphp	

<div class="col-xs-12  @if($agent->isMobile()) no-pad-l no-pad-r @endif">
	@foreach(config('evibe.virtual-party.birthday-pricings.addons') as $addon)
		@if(is_int(strpos($currentUrlPath, "us/")) && $addon['is-live-US']== "0")
				@continue
		@endif
		<div class="col-sm-6 col-md-4 addon-card">
			<div class="addon-card-wrap">
			<div class="addon-title in-blk">
				{{ $addon['title'] }}
			</div>
			<div class="addon-price in-blk">
				@if(is_int(strpos(request()->path(),"us/")))
					{{ $addon['us-price'] }}
				@else
					{{ $addon['ind-price'] }}
				@endif
			</div>
			
			<p class="addon-desc">{{ $addon['description'] }}</p>
			</div>
		</div>
	@endforeach
		

</div>