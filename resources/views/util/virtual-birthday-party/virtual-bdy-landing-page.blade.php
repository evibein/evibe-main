@extends('layout.base')

@section('page-title')
	<title>{{$data['seoTitle']}}</title>
@endsection

@section('meta-description')
	<meta name="description" content="{{$data['seoDesc']}}">
@endsection

@section('meta-keywords')
	<meta name="keywords" content="{{$data['keywords']}}"/>
@endsection

@section("header")
	@include('base.home.header.header-home-city')
	<div class="header-border"></div>
@endsection

@section('og-title')
	<meta property="og:title" content="{{ $data['seoTitle'] }}">
@endsection

@section('og-description')
	<meta property="og:description" content="{{ $data['seoDesc'] }}">
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-list.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/app/page-party-bag-list.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/app/vday-landingpage.css') }}"/>
	
@endsection

@section("content")
	<div class="w-1366 ">
		@if($agent->isMobile())
		{{-- Header Image --}}
			<div class="full-width" style="display:flex;align-items:center;flex-direction:column;justify-content:center;align-content:center;background-image:url('{{ $galleryUrl}}/main/img/hero-bg-vbp.png');background-size:cover;height:400px">
				<h1 style='color: white;    display:flex;    /* padding-top: 23%; */    text-align: center;    font-size: 38px;'>Evibe.in Virtual Celebrations</h1>
				<p style='padding-left:15px;padding-right:15px;font-size: 14px;;text-align: center;color: #fff;    letter-spacing: 0.04rem;'>Gift a lifetime memorable Virtual Birthday to your loved one amidst lockdown. Get digital theme invites, an anchor to welcome, greet & entertain all your guests on a video call. It feels more than a normal party.</p>
				<a href=" https://docs.google.com/forms/d/e/1FAIpQLSeuz42mOPWzTV3Zt-2L1foKPllxqz5J8KzbFU8JyFudFmdovw/viewform?usp=sf_link" class="btn btn-evibe btn-md " style="border-color:transparent;font-weight: 500;    letter-spacing: 1px;border-radius:4px">Enquire Now</a>
			</div>
				
		@else
			<div class="full-width" style="display:flex;align-items:center;flex-direction:column;justify-content:center;align-content:center;background-image:url('{{ $galleryUrl}}/main/img/hero-bg-vbp.png');background-size:cover;height:400px">
				<h1 style='color: white;    display:flex;    /* padding-top: 23%; */    text-align: center;    font-size: 40px;'>Evibe.in Virtual Celebrations</h1>
				<p style='font-size: 18px;line-height:27px;text-align: center;color: #fff'>Gift a lifetime memorable Virtual Birthday to your loved one amidst lockdown.<br> Get digital theme invites, an anchor to welcome, greet & entertain<br> all your guests on a video call. It feels more than a normal party.</p>
				<a target="_blank" href=" https://docs.google.com/forms/d/e/1FAIpQLSeuz42mOPWzTV3Zt-2L1foKPllxqz5J8KzbFU8JyFudFmdovw/viewform?usp=sf_link" class="btn btn-evibe btn-md " style="border-color:transparent;font-weight: 500;    letter-spacing: 1px;border-radius:4px">Enquire Now</a>
			</div>
		@endif

		<div class="full-width pad-l-15 pad-r-15 ">
			<h6 class="section-heading section-heading-v2 text-center">Awesome Birthday Party Packages</h6>
	
				<div class="col-sm-12 col-md-6" style="margin-top:35px">
					<div class="col-xs-12 text-center" style="min-height:395px;;box-shadow: 0 0rem 2rem rgba(0, 0, 0, 0.1); border-radius: 0.4rem;;  transition: 0.2s;border:1px solid rgb(242, 225, 225)">
						<h4 style="color:#333333cf!important">Happy Vibes Package</h4>
						<div><span style="color:#777;text-decoration:line-through;">₹1999</span>&nbsp;&nbsp;<span style="font-size:24px;font-weight:500;color:#ed3e72">₹999/-</span></div>
						<ul style="font-size:16px;list-style-type: square; list-style-position: outside;margin-top:15px;text-align: left">
							<li>Video conference for you and your guests to join the party (up to 15 kids)</li>
							<li>Digital birthday invite with the party link to share with your guests</li>
							<li>Digital wishes platform to save your guests dedications</li>
							<li>Total party duration: 30 mins</li>


						</ul>
						<a target="_blank" href=" https://docs.google.com/forms/d/e/1FAIpQLSeuz42mOPWzTV3Zt-2L1foKPllxqz5J8KzbFU8JyFudFmdovw/viewform?usp=sf_link" class="btn btn-evibe btn-md  mar-b-20 mar-t-20" style="border-color:transparent;font-weight: 500;    letter-spacing: 1px;border-radius:4px">Enquire Now</a>
					</div>
				</div>
				@if($agent->isMobile())	
					<div class="clearfix"></div>
				@endif
				<div class="col-sm-12 col-md-6" style="margin-top:35px">
					<div class="col-xs-12 text-center" style="min-height:395px;box-shadow: 0 0rem 2rem rgba(0, 0, 0, 0.1); border-radius: 0.4rem;;  transition: 0.2s;border:1px solid rgb(242, 225, 225)">
						<h4 style="color:#333333cf!important">Stellar Vibes Package </h4>
						<div><span style="text-decoration:line-through;">₹4999</span>&nbsp;&nbsp;<span style="font-size:24px;font-weight:500;color:#ed3e72">₹2499/-</span></div>
						<ul style="font-size:16px;list-style-type: square; list-style-position: outside;margin-top:15px;text-align: left">
							<li>Video conference for you and your guests to join the party (up to 25 kids)</li>
							<li>Birthday invite with the party link to share with your guests</li>
							<li>Digital wishes platform to save your guests dedications</li>
							<li>Party atmosphere for your child and their friends to celebrate</li>
							<li>Birthday party anchor to host your party</li>
							<li>The host will welcome and greet your guests, conduct two fun games with music & dance.</li>
							<li>Sing together happy birthday</li>
							<li>Total party duration: 60 minutes</li>


						</ul>
						<a target="_blank" href=" https://docs.google.com/forms/d/e/1FAIpQLSeuz42mOPWzTV3Zt-2L1foKPllxqz5J8KzbFU8JyFudFmdovw/viewform?usp=sf_link" class="btn btn-evibe btn-md  mar-b-20 mar-t-20" style="border-color:transparent;font-weight: 500;    letter-spacing: 1px;border-radius:4px">Enquire Now</a>
					</div>

				</div>	
		</div>
		<div class="clearfix"></div>
		<div class="full-width text-center pad-l-15 pad-r-15" style="margin-top: 15px;overflow: hidden">
			<h6 class="section-heading section-heading-v2 text-center" style="padding-bottom:35px">A Peak Into The Experience</h6>
			@if($agent->isMobile())
				<iframe width="560" height="315" src="https://www.youtube.com/embed/UJH7xeHbav4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			@else
				<iframe width="560" height="400" src="https://www.youtube.com/embed/UJH7xeHbav4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

			@endif
		</div>

		<div class="clearfix"></div>
		<div class="full-width text-center" style="margin-top: 40px;">
			<h6 class="section-heading section-heading-v2 text-center" style="padding-bottom:35px">Add-ons</h6>
			<div class="col-xs-12">
				<div class="col-xs-12 col-md-8 col-md-offset-2" style="box-shadow: 0 3rem 6rem rgba(0, 0, 0, 0.1); border-radius: 0.4rem;;  transition: 0.2s;border:1px solid rgb(242, 225, 225)">
					<ul style="font-size:16px;list-style-type: circle; list-style-position: outside;margin-top:15px;text-align: left">
						<li>Party takeover by the guest for an extra one hour - ₹1000
							<li style="margin-top:5px">Virtual parties happen once in a lifetime, store your memories with your party video recording link on YouTube [This video would be your party recording with instrumental music background and technical glitches removed from it]. Delivered within 3 - 5 business days from your party date. - ₹1000</li>
							<li style="margin-top:5px">Get your party decorations covered with virtual birthday theme backdrop and balloons - ₹1000</li>
							<li style="margin-top:5px">Magician to perform a special magic show for the kids. Performance Duration: 20 to 30 minutes - ₹2000</li>
							<li style="margin-top:5px">Female anchor would cost ₹1000 extra</li>
							<li style="margin-top:5px">Anchor with your language preference (Apart from English) would cost ₹1000 extra</li>
							<li style="margin-top:5px">Other entertainment activities like Digital Caricature, Puppet Show, Clown Show, Craft Activity, etc, are available on request.</li>




					</ul>
				</div>
				<div class="clearfix"></div>
				<a target="_blank" href=" https://docs.google.com/forms/d/e/1FAIpQLSeuz42mOPWzTV3Zt-2L1foKPllxqz5J8KzbFU8JyFudFmdovw/viewform?usp=sf_link" class="btn btn-evibe btn-md  mar-t-30" style="border-color:transparent;font-weight: 500;    letter-spacing: 1px;border-radius:4px">Enquire Now</a>

			</div>
		</div>
		<div class="clearfix"></div>
		<div class="full-width text-center pad-l-15 pad-r-15 " style="margin-top: 40px;padding-bottom:40px">
			<h6 class="section-heading section-heading-v2 text-center" style="padding-bottom:35px">How It Works</h6>
			<div class="text-center">
				<div class=" col-xs-12 col-md-4 text-center" >
					<div class="text-center"  style="font-size: 28px;height:85px;width:85px;border-radius:50%;margin:0 auto; box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);display:flex;align-items:center;align-content:center;justify-content:center;flex-direction:column;" >1</div>

					<div class="mar-t-15" style="font-weight: 600;font-size:16px">Choose
						<p style="font-size:16px;font-weight:normal;margin-top:10px"> Choose the best virtual celebrations package and add-ons suitable for your occasion.</p></div>
				</div>
				@if($agent->isMobile())	
					<div class="clearfix"></div>
				@endif
				<div class="col-xs-12 col-md-4 text-center" > 
						<div class="text-center"  style="font-size: 28px;height:85px;width:85px;border-radius:50%;margin:0 auto; box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);display:flex;align-items:center;align-content:center;justify-content:center;flex-direction:column;" >2</div>

					<div class="mar-t-15" style="font-weight: 600;font-size:16px">Book
						<p style="font-size:16px;font-weight:normal;margin-top:10px">Securely make payment using credit/debit, wallets, UPI & more.</p></div>
				</div>
				@if($agent->isMobile())	
					<div class="clearfix"></div>
				@endif
				<div class="col-xs-12 col-md-4 text-center"> 
						<div class="text-center"  style="font-size: 28px;height:85px;width:85px;border-radius:50%;margin:0 auto; box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);display:flex;align-items:center;align-content:center;justify-content:center;flex-direction:column;" >3</div>

					<div class="mar-t-15"  style="font-weight: 600;font-size:16px">Celebrate
						<p style="font-size:16px;font-weight:normal;margin-top:10px">Get ready to party as we take care of the rest.</p></div>
				</div>
				
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="full-width text-center">
			<a target="_blank" href=" https://docs.google.com/forms/d/e/1FAIpQLSeuz42mOPWzTV3Zt-2L1foKPllxqz5J8KzbFU8JyFudFmdovw/viewform?usp=sf_link" class="btn btn-evibe btn-md  mar-b-30 mar-t-30" style="border-color:transparent;font-weight: 500;    letter-spacing: 1px;border-radius:4px">Enquire Now</a>
		</div>



	</div>	

@endsection

@section("footer")
	<div class="footer-wrap">
		@include('app.footer_noncity')
	</div>
@endsection