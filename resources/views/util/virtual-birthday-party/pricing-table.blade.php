<div class="col-xs-12  text-center col-md-8 col-md-offset-2" id="pricing" style="color:#333;padding-top:20px;@if($agent->isMobile()) padding-left:0;padding-right:0; @endif">
  <div class="table-responsive-xs"  @if(!($agent->isMobile())) style="margin-left:20px;margin-right:20px" @else style="overflow-x:scroll" @endif>
    <table class="table">
      <thead>
        <tr>    
          <th>

          </th>
          <th style="font-size:17px;padding-top:5px;padding-bottom: 5px;text-align:center">
            Happy Vibes
            <br>
            @if(is_int(strpos(request()->path(),"us/")))
               <span style="text-decoration:line-through;">{{ config('evibe.virtual-party.birthday-pricings.packages.abroad.package-one-worth') }} </span>
                <span style="font-size:34px;font-weight:500;text-shadow:0px 1px #999">
                  {{ config('evibe.virtual-party.birthday-pricings.packages.abroad.package-one-price') }}
                </span>
            @else
              <span style="text-decoration:line-through;"> {{ config('evibe.virtual-party.birthday-pricings.packages.india.package-one-worth') }}</span>
              <span style="font-size:34px;font-weight:500;text-shadow:0px 1px #999">
               {{ config('evibe.virtual-party.birthday-pricings.packages.india.package-one-price') }}
              </span>
            @endif


          </th>
          <th style="background-color:#b1aaaabd;font-size:17px;padding-top:5px;padding-bottom: 5px;text-align:center">
            Stellar Vibes
               <br>
             @if(is_int(strpos(request()->path(),"us/")))
                <span style="text-decoration:line-through;"> {{ config('evibe.virtual-party.birthday-pricings.packages.abroad.package-two-worth') }}</span>

                <span style="font-size:34px;font-weight:500;text-shadow:0px 1px #999">
                  {{ config('evibe.virtual-party.birthday-pricings.packages.abroad.package-two-price') }}
                </span>

             @else
                <span style="text-decoration:line-through;"> {{ config('evibe.virtual-party.birthday-pricings.packages.india.package-three-worth') }}</span>

                <span style="font-size:34px;font-weight:500;text-shadow:0px 1px #999">
                  {{ config('evibe.virtual-party.birthday-pricings.packages.india.package-two-price') }}
                </span>

             @endif
            
          </th>
          <th style="font-size:17px;padding-top:5px;padding-bottom: 5px;text-align:center">
            Ultimate Vibes
               <br>
             @if(is_int(strpos(request()->path(),"us/")))
                <span style="text-decoration:line-through;"> {{ config('evibe.virtual-party.birthday-pricings.packages.abroad.package-two-worth') }}</span>

                <span style="font-size:34px;font-weight:500;text-shadow:0px 1px #999">
                  {{ config('evibe.virtual-party.birthday-pricings.packages.abroad.package-two-price') }}
                </span>

             @else
                <span style="text-decoration:line-through;"> {{ config('evibe.virtual-party.birthday-pricings.packages.india.package-three-worth') }}</span>

                <span style="font-size:34px;font-weight:500;text-shadow:0px 1px #999">
                  {{ config('evibe.virtual-party.birthday-pricings.packages.india.package-three-price') }}
                </span>

             @endif
            
          </th>

        </tr>
      </thead> 
      <tr>
        <td class="table-item">
          Private video conference

        </td>
        <td>
          <img src="{{$galleryUrl}}/main/virtual-party/ui2.png" style="height:20px">
        </td>
        <td>
          <img src="{{$galleryUrl}}/main/virtual-party/ui2.png"  style="height:20px">

        </td>
        <td>
          <img src="{{$galleryUrl}}/main/virtual-party/ui2.png"  style="height:20px">

        </td>
      </tr>
       <tr>
        <td class="table-item">
          No of unique guests logins
        </td>
        <td>10</td>
        <td>20</td>
         <td>25</td>
      </tr>
        <tr>
        <td class="table-item">
          Complimentary virtual theme invite
        </td>
        <td>
          <img src="{{$galleryUrl}}/main/virtual-party/ui2.png"  style="height:20px">
        </td>
        <td>
            <img src="{{$galleryUrl}}/main/virtual-party/na.svg" style="height:20px">

        </td>

         <td>
          <img src="{{$galleryUrl}}/main/virtual-party/ui2.png"  style="height:20px">

        </td>
      </tr>
        <tr>
        <td class="table-item">
          Complimentary Digital wishes platform &nbsp;<span  data-toggle="tooltip" data-placement="right" title="A dedicated page for your guests to send video/audio wishes and dedications" style="display:inline-block;width:10%"><img  src='{{$galleryUrl}}/main/virtual-party/info.svg' style="height:20px"></span>
        </td>
        <td>
            <img src="{{$galleryUrl}}/main/virtual-party/na.svg" style="height:20px">
        </td>
        <td>
           <img src="{{$galleryUrl}}/main/virtual-party/na.svg" style="height:20px">

        </td>
        <td>
          <img src="{{$galleryUrl}}/main/virtual-party/ui2.png"  style="height:20px">

        </td>
      </tr>
        <tr>
        <td class="table-item">

          Evibe Anchor to host your party
        </td>
        <td>
          <img src="{{$galleryUrl}}/main/virtual-party/na.svg" style="height:20px">
        </td>
        <td>
          <img src="{{$galleryUrl}}/main/virtual-party/ui2.png"  style="height:20px">

        </td>
         <td>
          <img src="{{$galleryUrl}}/main/virtual-party/ui2.png"  style="height:20px">

        </td>
      </tr>
        <tr>
        <td class="table-item">
          Fun games 

        </td>
        <td>
          <img src="{{$galleryUrl}}/main/virtual-party/na.svg" style="height:20px">
        </td>
        <td>
         3
        </td>
        <td>
         4
        </td>
      </tr>
        <tr>
        <td class="table-item">
          Total Party Duration
        </td>
        <td>
          60 minutes
        </td>
        <td>
          60 minutes
        </td>
        <td>
          90 minutes
        </td>
      </tr>
      <tr>
        <td>
        </td>
        @php $formUrl = "https://docs.google.com/forms/d/e/1FAIpQLSeuz42mOPWzTV3Zt-2L1foKPllxqz5J8KzbFU8JyFudFmdovw/viewform?usp=sf_link"; @endphp
         @if(is_int(strpos(request()->path(),"us/")))
            @php $formUrl = "https://docs.google.com/forms/d/e/1FAIpQLSfoGZZt_Pmw0K-fSY8ppT4A-gHiRRtqi_i6XLJBAPx3L9lWfA/viewform"; @endphp

         @endif
        <td>
          <a class="floating-evibe-btn header-cta-btn a-no-decoration " href="{{ $formUrl }}" target="_blank" style="display:inline-block; border-radius:5px;text-align:center"   > Choose

          </a>
        </td>
        <td>
          <a class="floating-evibe-btn header-cta-btn a-no-decoration" href="{{ $formUrl }}" target="_blank" style="display:inline-block;border-radius:5px;text-align:center" > Choose

          </a>
        </td>
         <td>
          <a class="floating-evibe-btn header-cta-btn a-no-decoration" href="{{ $formUrl }}" target="_blank" style="display:inline-block;border-radius:5px;text-align:center" > Choose

          </a>
        </td>

      </tr>

    </table>
  </div>

</div>