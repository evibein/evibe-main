@extends('layout.base')

@section('page-title')
	<title>{{$data['seoTitle']}}</title>
@endsection

@section('meta-description')
	<meta name="description" content="{{$data['seoDesc']}}">
@endsection

@section('meta-keywords')
	<meta name="keywords" content="{{$data['keywords']}}"/>
@endsection

@section("header")
	@include('base.home.header.header-home-city')
	<div class="header-border"></div>
@endsection

@section('og-title')
	<meta property="og:title" content="{{ $data['seoTitle'] }}">
@endsection

@section('og-description')
	<meta property="og:description" content="{{ $data['seoDesc'] }}">
@endsection

@section('custom-css')
	<link rel="stylesheet" href="{{ elixir('css/owl-carousel.css') }}">
	<link rel="stylesheet"  href="{{ elixir('css/virtual-party-updated.css') }}">
@endsection

@section('seo:schema')
	@parent
	@if(isset($data['seo']['schema']) && count($data['seo']['schema']))
		@foreach($data['seo']['schema'] as $schema)
			{!! $schema !!}
		@endforeach
	@endif
@endsection

@section("header")
	@include('base.home.header.header-home-city')
@endsection

@section("content")
	<div class="bg-white-imp " >
		{{-- Header Section --}}
			
			@if($agent->isMobile())
				<div class='add-bg-img' data-src="https://gallery.evibe.in/main/img/hero-bg-vbp.png" style="text-align:center;min-height:400px;;background-size:cover;padding-left:20px;padding-right:20px">
					<h2 class="carousel-heading" style="margin-top:0;padding-top:20px">
						Funnest Virtual Celebrations
					</h2>
					<p class="carousel-subheading">
						Celebrate your child's birthday like never before. Fun virtual party with friends & family across the globe, games, music, dance & more.
					</p>
					
					<div class="text-center youtube-video-wrap-mbl youtube-iframe">
					</div>
					<a href="#pricing" class="floating-evibe-btn header-cta-btn carousel-cta a-no-decoration"  style="margin-bottom:30px; text-align:center" > Enquire Now

					</a>

				</div>
			@else
			<div class="header-section-wrap add-bg-img" data-src="https://gallery.evibe.in/main/img/hero-bg-vbp.png">
				<div class="w-1366">
					<div class="col-xs-12 col-md-6 carousel-text-wrap">
						<h2 class="carousel-heading">
							Funnest Virtual Celebrations
						</h2>
						<p class="carousel-subheading">
							Celebrate your child's birthday like never before. Fun virtual party with friends & family across the globe, games, music, dance & more.
						</p>
						<a href="#pricing" class="floating-evibe-btn header-cta-btn carousel-cta a-no-decoration" style="text-align:center" > Enquire Now

						</a>
					</div>
					<div class="col-xs-12 col-md-6 video-img-wrap youtube-video-wrap-desk  youtube-iframe">
					</div>
				</div>
			</div>
			@endif
		
		<div class="clearfix"></div>
		<section class="section-wrap">
			<div class="w-1366">
				<h3 class="section-heading" style="margin-top:0 !important">
					 An innovation by Evibe.in: Masters of celebrations
				</h3>
				<div class="full-width">
						
							<div class="text-center" style="font-size:16px !important">
								<div class="col-md-3 col-xs-6 section-icon-wrap-mbl"  >
									<img data-src="{{$galleryUrl}}/main/virtual-party/1st2.webp" class="icon-style-1 vp-image lazy-loading" alt="India’s #1 Platform For Celebrations">
									<div class="mar-t-10">India’s #1 Platform For Celebrations</div>
								</div>
								<div class="col-md-3 col-xs-6 section-icon-wrap-mbl">
									<img data-src="{{$galleryUrl}}/main/virtual-party/half-million2.webp" class="icon-style-1 vp-image lazy-loading" alt="Half Million Happy Guests">
									<div class="mar-t-10">Half Million Happy Guests</div>
								</div>
								<div class="col-md-3 col-xs-6 section-icon-wrap-mbl" >
									<img data-src="{{$galleryUrl}}/main/virtual-party/hassle-free2.webp" class="icon-style-1 vp-image lazy-loading" alt="Stress-Free Experience">
									<div class="mar-t-10">Stress-Free Experience</div>
								</div>
								<div class="col-md-3 col-xs-6 section-icon-wrap-mbl">
									<img data-src="{{$galleryUrl}}/main/virtual-party/one-stop2.webp" class="icon-style-1 vp-image lazy-loading" alt="One Stop Solution">
									<div class="mar-t-10">One Stop Solution</div>
								</div>
							</div>
						
				</div>
			</div>
				<div class="clearfix"></div>
		</section>
		<section class="section-wrap-bg">
			<div class="w-1366">
				<h3 class="section-heading" style="margin-top:0 !important">
					 Magical virtual celebrations
				</h3>
				<div class="full-width">
						<div class="col-xs-12 col-md-4" style="height:100px">
								<div class="col-xs-4" style="	text-align:right">
									<img data-src="{{$galleryUrl}}/main/virtual-party/birthday-child2.webp" class="icon-style-2 vp-image lazy-loading" alt="birthday-child">
								</div>
								<div class="col-xs-8" style="display:flex;justify-content: 	center;flex-direction: 	column;height:90px"	>
									<span style="font-size:18px;font-weight:500">Birthday child focussed</span>
									

								</div>

						</div>

						<div class="col-xs-12 col-md-4" style="height:100px">
								<div class="col-xs-4" style="	text-align:right">
									<img data-src="{{$galleryUrl}}/main/virtual-party/digital-invite2.webp" class="icon-style-2 vp-image lazy-loading" alt="Digital theme invites">
								</div>
								<div class="col-xs-8" style="display:flex;justify-content: 	center;flex-direction: 	column;height:90px"	>
									<span style="font-size:18px;font-weight:500">Digital theme invites</span>
									

								</div>

						</div>
						<div class="col-xs-12 col-md-4" style="height:100px">
								<div class="col-xs-4" style="	text-align:right">
									<img data-src="{{$galleryUrl}}/main/virtual-party/evibe-ancor2.webp" class="icon-style-2 vp-image lazy-loading" alt="Evibe anchor ">
								</div>
								<div class="col-xs-8" style="display:flex;justify-content: 	center;flex-direction: 	column;height:90px"	>
									<span style="font-size:18px;font-weight:500">Evibe anchor to entertain guests</span>
									

								</div>

						</div>
						<div class="col-xs-12 col-md-4" style="height:100px">
								<div class="col-xs-4" style="	text-align:right">
									<img data-src="{{$galleryUrl}}/main/virtual-party/digital-wishes2.webp" class="icon-style-2 vp-image lazy-loading" alt="Digital wishes platform">
								</div>
								<div class="col-xs-8" style="display:flex;justify-content: 	center;flex-direction: 	column;height:90px"	>
									<span style="font-size:18px;font-weight:500">Digital wishes platform</span>
									
									<span class="test"  data-toggle="tooltip" data-placement="right" title="A dedicated page for your guests to send video/audio wishes and dedications" style="display:inline-block;width:10%"><img  src='https://image.flaticon.com/icons/svg/446/446101.svg' style="height:20px"></span>						

								</div>

						</div>
						<div class="col-xs-12 col-md-4" style="height:100px">
								<div class="col-xs-4" style="	text-align:right">
									<img data-src="{{$galleryUrl}}/main/virtual-party/secure-video2.webp" class="icon-style-2 vp-image lazy-loading" alt="Secure private video conference">
								</div>
								<div class="col-xs-8" style="display:flex;justify-content: 	center;flex-direction: 	column;height:90px"	>
									<span style="font-size:18px;font-weight:500">Secure private video conference</span>
									

								</div>

						</div>
						<div class="col-xs-12 col-md-4" style="height:100px">
								<div class="col-xs-4" style="	text-align:right">
									<img data-src="{{$galleryUrl}}/main/virtual-party/virtual-theme2.webp" class="icon-style-2 vp-image lazy-loading" alt="Fun virtual theme backgrounds">
								</div>
								<div class="col-xs-8" style="display:flex;justify-content: 	center;flex-direction: 	column;height:90px"	>
									<span style="font-size:18px;font-weight:500">Fun virtual theme backgrounds</span>
									

								</div>

						</div>

				</div>
			</div>
				<div class="clearfix"></div>
		</section>
		<section class="section-wrap" style="">
			<div class="w-1366">
				<h3  class="section-heading" style="margin-top:0 !important" >
					Customers <span><img data-src="https://image.flaticon.com/icons/svg/2107/2107845.svg" class="vp-image lazy-loading" alt="Love Icon" style="height:22px;margin-top:-14px"></span> virtual celebrations
				</h3>
				<div class="full-width">
						<div class="reviews-wrap full-width pad-l-15 pad-r-15 ">
							
							<div class="owl-carousel owl-theme" id="reviews-wrap" style="display: block;">
								@foreach(config('evibe.virtual-party.birthday-reviews') as $review)
									<div class="item mar-r-20 text-center box" style="   ">
										<div class="reviews-wrap-mbl" style="height: 230px; font-size: 18px;flex-direction:row;    line-height: 1.5;    border-radius: 15px; color: #7d5147;">
											<div class="col-xs-12 col-sm-12 col-md-3 img-item" style="text-align:right;float:right;">
													<img data-src="{{$galleryUrl}}/main/virtual-party/{{$review['feature-pic']}}" class="review-img vp-image lazy-loading" alt="evibe-customer- {{ $review['author']  }}" style="height:110px;width:110px !important	">
											</div>
											<div class="col-xs-12 col-sm-12 col-md-9 pad-t-20">
													<span class="review-text">{{$review['review']}}</span>
													
													<span style='display:block;margin-top:5px;color:#333;font-size:17px;font-style:italic'> {{ $review['author'] }} </span>
											</div>
											
											
											
										</div>
									</div>
								@endforeach
							</div>
							<div class="clearfix"></div>
						</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</section>
		<section class="section-wrap-bg text-center">
			<div class="w-1366">
				<h3  class="section-heading" style="margin-top:0 !important" >
					Tailor made packages for you
				</h3>
				@include('util.virtual-birthday-party.pricing-table')
				<div class="clearfix"></div>
				<h3 class="section-heading" style="margin-top:20px !important">
					Addons for more awesomeness
				</h3>
				@include('util.virtual-birthday-party.addons')
			</div>
				<div class="clearfix"></div>
		</section>

		<section class="section-wrap" >
			<div class="w-1366">
				<div class="col-xs-12">
					<h3 class="section-heading" style="margin-top:0 !important">
						Virtual celebrations:  All new possibilities
					</h3>
					<div class="col-xs-12 col-md-3">
							<div style="min-height:190px;text-align:center;margin-top:15px;border: 1px solid rgb(242, 225, 225); box-shadow: 0 0rem 2rem rgba(0, 0, 0, 0.1);    border-radius: 0.4rem;padding-left:10px;padding-right:10px">
						
										<img data-src="{{$galleryUrl}}/main/virtual-party/travel-free2.webp" class="icon-style-1 vp-image lazy-loading" alt="Travel Free">
										<div class="mar-t-5 font-18 pad-b-5" style="font-weight:600;">Travel Free</div>
										<div class="mar-t-5 pad-b-10">No more traffic, commute, hotel booking etc. Celebrate from the comfort of your home.</div>
							</div>
					</div>
					<div class="col-xs-12 col-md-3">
							<div style="min-height:190px;text-align:center;margin-top:15px;border: 1px solid rgb(242, 225, 225); box-shadow: 0 0rem 2rem rgba(0, 0, 0, 0.1);    border-radius: 0.4rem;padding-left:10px;padding-right:10px">
						
										<img data-src="{{$galleryUrl}}/main/virtual-party/np-geography2.webp" class="icon-style-1 vp-image lazy-loading" alt="No Geography Limit">
										<div class="mar-t-5 font-18 pad-b-5" style="font-weight:600">No Geography Limit</div>
										<div class="mar-t-5 pad-b-10">All the friends & family across the globe can be together.</div>
							</div>
								

					</div>
					<div class="col-xs-12 col-md-3">
							<div style="min-height:190px;text-align:center;margin-top:15px;border: 1px solid rgb(242, 225, 225); box-shadow: 0 0rem 2rem rgba(0, 0, 0, 0.1);    border-radius: 0.4rem;padding-left:10px;padding-right:10px">
						
										<img data-src="{{$galleryUrl}}/main/virtual-party/focussed2.webp" class="icon-style-1 vp-image lazy-loading" alt="Focussed">
										<div class="mar-t-5 font-18 pad-b-5" style="font-weight:600">Focussed</div>
										<div class="mar-t-5 pad-b-10">Everyone’s on the video to celebrate the special occasion and they’ll just do that.</div>
							</div>
								

					</div>
					<div class="col-xs-12 col-md-3">
							<div style="min-height:190px;text-align:center;margin-top:15px;border: 1px solid rgb(242, 225, 225); box-shadow: 0 0rem 2rem rgba(0, 0, 0, 0.1);    border-radius: 0.4rem;padding-left:10px;padding-right:10px">
						
										<img data-src="{{$galleryUrl}}/main/virtual-party/life-long2.webp" class="icon-style-1 vp-image lazy-loading" alt="Lifelong Memory">
										<div class="mar-t-5 font-18 pad-b-5" style="font-weight:600">Lifelong Memory</div>
										<div class="mar-t-5 pad-b-10">Unique experience and a lifetime memory for you and your guest.</div>
							</div>
								

					</div>
			</div>
			</div>
			<div class="clearfix"></div>
		</section>
		<section class="section-wrap-bg" style="margin-top:20px">
				<div class="w-1366">
					<div class="col-xs-12">
							@include('util.virtual-birthday-party.faq')
					</div>
				</div>
				<div class="clearfix"></div>
		</section>
		<section class="section-wrap" >

			<div class="w-1366 abt-media-wrap">
				<div class="col-xs-12">
				
					
					<h3 class="section-heading" style="margin-top:0 !important">Evibe in media</h3>
				
					<div class=" press text-center">
						<div class="col-md-4 col-sm-12 col-xs-12 press-focus">
							<img data-src="{{ $galleryUrl }}/main/img/icons/press/logo-toi2.png" class="vp-image lazy-loading" alt="Evibe on Times of India">
							<p align="justify">
								"A platform for event services to organise a party"
							</p>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 press-focus">
							<img data-src="{{ $galleryUrl }}/main/img/icons/press/logo-indian-express.png" class="vp-image lazy-loading" alt="Evibe on The New Indian Express">
							<p align="justify">
								"The Great Indian Party Planners"
							</p>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 press-focus">
							<img data-src="{{ $galleryUrl }}/main/img/icons/press/logo-bonnevie.png" class="vp-image lazy-loading" alt="Evibe on Bonnevie News">
							<p align="justify">
								"Now Planning A Party Is Just An Evibe Away!"
							</p>
						</div>

						<ul class="press-logo top-30">
							<li>
								<img data-src="{{ $galleryUrl }}/main/img/icons/press/logo-yourstory.png" class="img-responsive img-centered vp-image lazy-loading" alt="Evibe on YourStory">
							</li>
							<li>
								<img data-src="{{ $galleryUrl }}/main/img/icons/press/logo-vccircle.png" class="img-responsive img-centered vp-image lazy-loading" style="opacity:0.8; height: 25px; margin-top: 10px;" alt="Evibe on VCCircle">
							</li>
							<li>
								<img data-src="{{ $galleryUrl }}/main/img/icons/press/logo-inc42.png" class="img-responsive img-centered vp-image lazy-loading" style="opacity:0.8; height: 30px; margin-top: 10px;" alt="Evibe on Inc42">
							</li>
							<li>
								<img data-src="{{ $galleryUrl }}/main/img/icons/press/logo-medianama.png" class="img-responsive img-centered vp-image lazy-loading" style="opacity:0.8; height: 25px; margin-top: 12px;" alt="Evibe on Medianama.com">
							</li>
							<li>								
								<img data-src="{{ $galleryUrl }}/main/img/icons/press/logo-tia.png" class="img-responsive img-centered vp-image lazy-loading" style="opacity:0.8; height:30px; margin-top: 10px;" alt="Evibe on TechInAsia.com">
							</li>
						</ul>
					</div>
				</div>
				<div class="clearfix"></div>
			
			</div>
		</section>

	</div>

@endsection

@section("footer")
	@include('app.footer_noncity')
@endsection
@section("js-framework")
		<script src="{{ elixir('js/virtual-party.js') }}"></script>

@endsection

@section("google-places-api")
	
@endsection

@section("javascript")

		<script type="text/javascript">
			$(document).ready(function(){
			  
				 $('[data-toggle="tooltip"]').tooltip();
		
				$.getScript("/js/owl-carousel.js", function () {
						

						$('#reviews-wrap').owlCarousel({
							stagePadding: 15,/*the little visible images at the end of the carousel*/
							dots: true,
							rtl: false,
							loop: true,
							margin: 15,
							nav: false,
							lazyload: true,
							responsive: {
								0: {
									items: 1
								},
								1000: {
									items: 2.1
								},
							}
						});

				});


				$.getScript("/js/util/lazyLoadImages.js", function () {
					$(".vp-image").removeClass("lazy-loading");
					new LazyLoad();
				});





			});
		  
		</script>
@endsection