@extends('layout.base')

@section('page-title')
	<title>{{$data['seoTitle']}}</title>
@endsection

@section('meta-description')
	<meta name="description" content="{{$data['seoDesc']}}">
@endsection

@section('meta-keywords')
	<meta name="keywords" content="Top Indian Party Planner"/>
@endsection

@section("header")
	@include('base.home.header.header-home-city')
	<div class="header-border"></div>
@endsection

@section('seo:schema')
	@parent
	@if(isset($data['schema']) && count($data['schema']) > 0)
		@foreach($data['schema'] as $schema)
			{!! $schema !!}
		@endforeach
	@endif
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-list.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/app/page-party-bag-list.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/util/star-rating.css') }}"/>
@endsection

@section("content")
	<div class="w-1366" style="background-color:white" >
		<div class="full-width">
			@if(!($agent->isMobile()))
				<img src="{{ $galleryUrl }}/{{ config('evibe.lockdown-landing-pages.pageData.gallery-path') }}/{{ config('evibe.lockdown-landing-pages.pageData.carousel-img-desktop') }}" style="width:100%;height:auto">
			@else
				<img src="{{ $galleryUrl }}/{{ config('evibe.lockdown-landing-pages.pageData.gallery-path') }}/{{ config('evibe.lockdown-landing-pages.pageData.carousel-img-mobile') }}" style="width:100%;height:auto">
			@endif

		</div>
		<div class='clearfix'></div>
		{{-- Cards --}}
		<div class="cards-wrap col-xs-12 col-md-12 pad-t-30 pad-b-30 ">
			@if($data['cards'])
				@foreach($data['cards'] as $card)
					<div class="col-xs-12 col-md-4 mar-t-20">
						<div class="img-wrap" @if($agent->isMobile()) style="min-height: 355px" @else style="min-height: 420px " @endif >
							<a target="_blank" href="/u/{{ $card['campaignUrl'] }}"><img src="{{ $galleryUrl }}/{{ config('evibe.lockdown-landing-pages.pageData.gallery-path') }}/{{ $card['card-img-url'] }}" style="width:100%;height:auto">
							</a>
						</div>
					</div>
				@endforeach
			@endif
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>

		@if($agent->isMobile() && !($agent->isTablet()))
			@include("base.home.home-usp")
		@endif
		@include("base.home.enquiry-form")
		@include('base.home.press')
		<div class="clearfix"></div>
		<div class="g-reviews">
			@if(isset($data['reviews']))
				@include("base.home.g-reviews",['reviews' => $data['reviews']])
			@endif
		</div>

	</div>
	


@endsection
@section("footer")
	<div class="footer-wrap">
		@include('app.footer_noncity')
	</div>
@endsection


