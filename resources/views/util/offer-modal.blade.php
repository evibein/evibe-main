<div id="book-item-modal" class="modal fade no-pad-r-imp" role="dialog" tabindex="-1">
	<div class="modal-dialog modal-md">
		<div class="modal-content" style="background: #ED213A;  /* fallback for old browsers */background: -webkit-linear-gradient(to right, #93291E, #ED213A);  /* Chrome 10-25, Safari 5.1-6 */background: linear-gradient(to right, #93291E, #ED213A); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */">
			<div class="col-xs-12 no-pad text-center" id="form-content">
				@if ($agent->isMobile() && !($agent->isTablet()))
					<div>
						<div>
							<img style="height: 65px; margin-top: 20px; vertical-align: top;" src="{{ config("evibe.gallery.host") }}/img/icons/gift-box-white.png">
						</div>
						<div style="color: #ffffff; padding: 10px 0 20px 0">Submit your details & get your coupon.</div>
					</div>
					<div>
						<div class="col-xs-12">
							<input type="text" placeholder="Enter your full name" class="lead-details-name" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
							<input type="text" placeholder="Enter your email address" class="lead-details-email" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
							<input type="text" placeholder="Enter your phone number" class="lead-details-phone" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
							<input type="hidden" class="lead-details-comments" value="Enquiry is created from Vday Landing Page 2020" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
							<input id="enquiryFormDate" name="enquiryFormDate" type="hidden" class="lead-details-party-date" value="14-02-2020" placeholder="when is your party?" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
						</div>
						<div class="col-xs-6 col-xs-offset-3 mar-t-30 mar-b-30">
							<a class="btn btn-evibe full-width pad-t-10 pad-b-10 enquiry-details-submit" style="background-color: white; color: black;">
								GET COUPON
							</a>
							<a class="btn btn-evibe full-width pad-t-10 pad-b-10 enquiry-details-submitting hide" style="background-color: white; color: black;">SUBMITTING...</a>
						</div>
						<div class="clearfix"></div>
					</div>
				@else
					<div>
						<div>
							<img style="height: 65px; margin-top: 20px; vertical-align: top;" src="{{ config("evibe.gallery.host") }}/img/icons/gift-box-white.png">
						</div>
						<div style="color: #ffffff; padding: 10px 0 20px 0">Submit your details & get your coupon.</div>
					</div>
					<div>
						<div class="col-xs-6 col-xs-offset-3">
							<input type="text" placeholder="Enter your full name" class="lead-details-name" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
							<input type="text" placeholder="Enter your email address" class="lead-details-email" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
							<input type="text" placeholder="Enter your phone number" class="lead-details-phone" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
							<input type="hidden" placeholder="Enter your phone number" class="lead-details-comments" value="Enquiry is created from Vday Landing Page 2020" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
							<input id="enquiryFormDate" name="enquiryFormDate" type="hidden" class="lead-details-party-date" value="14-02-2020" placeholder="when is your party?" style="width: 100%; height: 42px; border: 0; border-radius: 3px; padding-left: 10px; margin-top: 10px;">
						</div>
						<div class="col-sm-4 col-xs-offset-4 mar-t-30 mar-b-30">
							<a class="btn btn-evibe full-width pad-t-10 pad-b-10 enquiry-details-submit" style="background-color: white; color: black;">
								GET COUPON
							</a>
							<a class="btn btn-evibe full-width pad-t-10 pad-b-10 enquiry-details-submitting hide" style="background-color: white; color: black;">SUBMITTING...</a>
						</div>
						<div class="clearfix"></div>
					</div>
				@endif
			</div>
			<div class="offer-signup-thankyou text-center hide">
				<div class="text-center">
					<img src="{{$galleryUrl}}/main/landingpages/cards/checkmark.png" style="    height: 100px;    margin-top: 29px;">
				</div>
				<div>
					<div style="font-size: 40px; margin-top: 30px; color: #ffffff; height: 35px; line-height: 40px">THANK YOU</div>
					<div style="color: #ffffff; margin: 25px 0; padding: 0 20px;">We have sent you the coupon to your mail & phone. Please use that to avail the offer.</div>
				</div>
				<div>
					<div style="color: #ffffff; margin-bottom: 25px; font-size: 16px;" class="text-underline cur-point in-blk" data-dismiss="modal">Continue Browsing</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>