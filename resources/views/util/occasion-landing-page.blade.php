@extends('layout.base')

@section('page-title')
	<title>{{$data['pageDetails']['seo_title']}}</title>
@endsection

@section('meta-description')
	<meta name="description" content="{{$data['pageDetails']['seo_desc']}}">
@endsection

@section('meta-keywords')
	<meta name="keywords" content="{{$data['pageDetails']['seo_keywords']}}"/>
@endsection

@section("header")
	@include('base.home.header.header-home-city')
	<div class="header-border"></div>
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-list.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/app/page-party-bag-list.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/owl-carousel.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/util/star-rating.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/app/vday-landingpage.css') }}"/>
@endsection

@section("content")
	<div class="w-1366 ">
		@if($agent->isMobile())
			<div class="text-center">
				<h1 class="section-heading" style="margin-bottom: 10px;font-size: 25px;margin-top:30px !important">{{$data['pageDetails']['title']}}</h1>
				<p class="mar-b-10 font-16 pad-l-10 pad-r-10">{{$data['pageDetails']['description']}}</p>
			</div>
			<div class="full-width pad-l-15 pad-r-15 pad-t-5">
				@foreach($data['cardsDetails'] as $card)
					<div class="full-width  text-center @if(!($loop->first)) mar-t-15 @endif" style="border-radius: 5px;">
						<a href="{{route('campaign.landingpage',$card['card_cta'])}}?ref=vdayLandingPage" target="_blank">
							<img src="{{$galleryUrl}}/{{config('evibe.valentines-day-landingpages.gallery-path')}}/{{$card['img_url_mobile']}}" style="background: linear-gradient(to right, #e1eec3, #f05053);border-radius: 5px;width:100%;">
						</a>
					</div>
				@endforeach
			</div>
			<div class="clearfix"></div>
			<!-- Offer Card -->
			<div class="full-width offer-card-mbl" id="offer-card" style="margin-right:15px;margin-left:15px;height:300px;background-image: url('{{$galleryUrl}}/{{config('evibe.valentines-day-landingpages.gallery-path')}}/mbl-offer.png');">
				<h4 class="offer-card-heading-mbl">SPECIAL OFFER FOR YOU</h4>
				<p class="fc-white font-16 no-mar-b">Grab Your Coupon Before It's Too Late</p>
				<p id="expire-time" class="count-down-wrap"></p>
				<div style="font-size: 13px;display: block;margin-top: -4px">
					<div style="width: 48px;margin-right:29px;display: inline-block;font-size: 15px;text-align: center;color:white">Days</div>
					<div style="width: 48px;margin-right:29px;display: inline-block;font-size: 15px;text-align: center;color:white">Hours</div>
					<div style="width: 48px;margin-right:29px;display: inline-block;font-size: 15px;text-align: center;color:white">Mins</div>
					<div style="width: 48px;display: inline-block;font-size: 15px;text-align: center;color:white">Secs</div>
				</div>
				<div class="text-center mar-t-25 mar-b-15 mar-t-30">
					<span class="text-muted floating-evibe-btn pad-t-5 pad-b-5 pad-l-10 pad-r-10 font-20" style="background-color: white;color:red;border-radius: 3px;">Get My Coupon</span>
				</div>
			</div>
			<div class="clearfix"></div>
			<!-- reviews -->
			<div class="reviews-wrap full-width pad-l-15 pad-r-15 mar-t-30">
				<h3 class="text-center section-heading"> Straight from Customers Heart</h3>
				<div class="text-center">
					<img src="{{$galleryUrl}}/{{config('evibe.valentines-day-landingpages.gallery-path')}}/underline1.png" class="mar-b-15" style="width: 200px;">
				</div>
				<div class="owl-carousel owl-theme" id="reviews-wrap" style="display: block;">
					@foreach(config('evibe.valentines-day-landingpages.reviews') as $review)
						<div class="item mar-r-20 text-center box" style="height: auto">
							<div class="reviews-wrap-mbl">
								<img src="{{$galleryUrl}}/{{config('evibe.valentines-day-landingpages.gallery-path')}}/quote.png" style="height: 20px;width: auto;">
								<span>
									{{$review['review']}}
								</span>
								<img src="{{$review['pic-url']}}" class="review-img">
							</div>
						</div>
					@endforeach
				</div>
			</div>
			<div class="clearfix"></div>
			<!-- Products -->
			<div class="product-wrap full-width pad-l-15 pad-r-15">
				@foreach($data['products'] as $section )
					@if(sizeof($section))
						@if(!($loop->first || $loop->last))
							<hr class="hr-style">
						@endif
						<h4 class="text-center section-heading" style='margin-top: 30px !important'>
							{{$section['heading']}}
						</h4>
						<div class="text-center">
							<img src="{{$galleryUrl}}/{{config('evibe.valentines-day-landingpages.gallery-path')}}/underline1.png" class="mar-b-15" style="width: 200px;">
						</div>
						<div class="clearfix"></div>
						<div class="owl-carousel owl-theme products-wrap" style="display: block;">
							@foreach($section['sectionProducts'] as $product)
								@if($product)
									<div class="item mar-t-15 trending add-border">
										<a href="{{$product['link']}}?ref=vdayLandingPage" target="_blank">
											<div class="img-container blur-bg-wrap prod-img-container">
												<img src="{{ $product['img'] }}" style="width: auto; z-index: 3; position: absolute; left: 50%; transform: translate(-50%); height: 100%; top: 0;" class="thumb-img in-blk bg-black">
												<img src="{{ $product['img'] }}" class="blur-bg">
											</div>
											<h6 class=" no-mar-b no-pad-b mar-t-5 top-categories-title font-16-imp">
												<a class="a-no-decoration-dark-black text-normal" href="" target="_blank">@truncateName($product['title'])</a>
											</h6>
											<p class="text-center no-mar-b  pad-t-5 " style="line-height: 15px;font-size:15px">
												@if(isset($product['priceworth']) && ($product['priceworth'] > $product['price']))
													<span style="color: #a2a2a2;font-size: 14px;text-decoration:line-through;">@price($product['priceworth'])&nbsp;&nbsp;</span>
												@endif
												@price($product['price'])
											</p>
										</a>
									</div>
								@endif
							@endforeach
						</div>
						<div class="clearfix"></div>
						<div class="text-center mar-t-20" style="@if($loop->last)margin-bottom: 30px; @else margin-bottom:10px @endif">
							<a class="text-muted floating-evibe-btn pad-t-5 pad-b-5 pad-l-10 pad-r-10" href="{{route('campaign.landingpage',$section['cUrl'])}}">View More Packages</a>
						</div>
					@endif
					<div class="clearfix"></div>
				@endforeach
			</div>
		@else
		<!-- Header Section -->
			<div class="full-width" style="max-height: 400px;overflow: hidden">
				<img src="{{$galleryUrl}}/{{config('evibe.valentines-day-landingpages.gallery-path')}}/header-banner.png" class="header-img">
			</div>
			<div class="clearfix"></div>
			<div>
				<div class="text-center">
					<h1 class="section-heading no-mar-b no-pad-l no-pad-r">{{$data['pageDetails']['title_desk']}}</h1>
					<img src="{{$galleryUrl}}/{{config('evibe.valentines-day-landingpages.gallery-path')}}/underline1.png" class="mar-b-15" style="width: 200px;">
					<p style="margin-bottom: 5px;font-size: 18px">{{$data['pageDetails']['description']}}</p>
					<div class="clearfix"></div>
				</div>
				<!-- Cards Section -->
				<div class="cards-wrap-desktop text-center">
					@foreach($data['cardsDetails'] as $card)
						<div class="in-blk  text-center" style="width:33%;overflow: hidden;padding-left: 7px;margin-top:35px">
							<div class="pad-l-10 pad-r-10">
								<a href="{{route('campaign.landingpage',$card['card_cta'])}}?ref=vdayLandingPage" target="_blank">
									<img src="{{$galleryUrl}}/{{config('evibe.valentines-day-landingpages.gallery-path')}}/{{$card['img_url_desktop']}}" style="width:100%;height:auto" class="card-desktop">
								</a>

							</div>
						</div>
					@endforeach

				</div>
				<div class="clearfix"></div>
				<!-- Offer Card -->
				<div class="full-width mar-l-10 mar-r-10 mar-t-40-imp offer-card-desktop" id="offer-card" style="height: 300px;background-image: url('{{$galleryUrl}}//{{config('evibe.valentines-day-landingpages.gallery-path')}}/offer-card-desk.png');">
					<h3 class="no-mar-b fc-white">SPECIAL OFFER FOR YOU</h3>
					<p class="fc-white font-16 no-mar-b">Grab Your Coupon Before It's Too Late</p>
					<p id="expire-time" class="count-down-wrap" style="padding:7px"></p>
					<div style="font-size: 13px;display: block;margin-top: -4px">
						<div style="width: 79px;display: inline-block;font-size: 15px;text-align: center;color:white">Days</div>
						<div style="width: 79px;display: inline-block;font-size: 15px;text-align: center;color:white">Hours</div>
						<div style="width: 79px;display: inline-block;font-size: 15px;text-align: center;color:white">Mins</div>
						<div style="width: 79px;display: inline-block;font-size: 15px;text-align: center;color:white">Secs</div>
					</div>
					<div class="text-center mar-b-15" style="margin-top: 30px;">
						<span class="text-muted floating-evibe-btn pad-t-5 pad-b-5 pad-l-10 pad-r-10 font-20" style="background-color: white;color:red;border-radius: 3px;">Get My Coupon</span>
					</div>
				</div>
				<!-- Reviews -->
				<div class="reviews-wrap full-width pad-l-15 pad-r-15 " style="margin-top: 25px">
					<h4 class="text-center section-heading"> Straight from Customers Heart</h4>
					<div class="text-center full-width">
						<img src="{{$galleryUrl}}/{{config('evibe.valentines-day-landingpages.gallery-path')}}/underline1.png" style="width: 200px;margin-bottom: 40px">
					</div>
					<div class="owl-carousel owl-theme" id="reviews-wrap" style="display: block;">
						@foreach(config('evibe.valentines-day-landingpages.reviews') as $review)
							<div class="item mar-r-20 text-center box" style="   ">
								<div class="reviews-wrap-mbl" style="height: 300px; font-size: 20px;    line-height: 1.5;    border-radius: 15px; color: #7d5147;">
									<img src="{{$galleryUrl}}/{{config('evibe.valentines-day-landingpages.gallery-path')}}/quote.png" style="height: 20px;width: auto;  ">
									<span>
										{{$review['review']}}
									</span>
									<img src="{{$review['pic-url']}}" class="review-img">
								</div>
							</div>
						@endforeach
					</div>
					<div class="clearfix"></div>
				</div>
				<!-- Products -->
				<div class="product-wrap full-width pad-l-15 pad-r-15 mar-t-30">
					@foreach($data['products'] as $section )
						@if(sizeof($section))
							<h4 class="text-center section-heading" style="@if(!($loop->first)) margin-top:50px !important;  @endif">
								{{$section['heading']}}

							</h4>
							<div style="text-align: center;width: 100%"><img src="{{$galleryUrl}}/{{config('evibe.valentines-day-landingpages.gallery-path')}}/underline1.png" style="width: 200px;margin-bottom: 5px;"></div>
							<div class="owl-carousel owl-theme products-wrap" style="display: block;">
								@foreach($section['sectionProducts'] as $product)
									@if($product)
										<div class="item mar-t-15 trending add-border">
											<a href="{{$product['link']}}?ref=vdayLandingPage">
												<div class="img-container blur-bg-wrap prod-img-container">
													<img src="{{ $product['img'] }}" class="thumb-img in-blk bg-black" style="height: 100%; width: auto;">
													<img src="{{ $product['img'] }}" class="blur-bg in-blk bg-black">
												</div>
												<h6 class=" no-mar-b no-pad-b mar-t-5 top-categories-title">
													<a class="a-no-decoration-dark-black text-normal" href="">@truncateName($product['title'])</a>
												</h6>
												<p class="text-center no-mar-b pad-t-5" style="line-height: 15px;font-size: 15px">
													@if(isset($product['priceworth']) && ($product['priceworth'] > $product['price']))
														<span style="color: #a2a2a2;font-size: 14px;text-decoration:line-through;">@price($product['priceworth'])</span>
													@endif
													<span>&nbsp;&nbsp;@price($product['price'])</span>
												</p>
											</a>
										</div>
									@endif
								@endforeach
							</div>
							<div class="clearfix"></div>
							<div class="text-center " style="@if($loop->last)margin-bottom: 40px; @else margin-bottom:0;  @endif margin-top: 25px">
								<a class="text-muted floating-evibe-btn pad-t-5 pad-b-5 pad-l-10 pad-r-10" href="{{route('campaign.landingpage',$section['cUrl'])}}" target="_blank">View More Packages</a>
							</div>
						@endif
						<div class="clearfix"></div>
					@endforeach
				</div>
			</div>
			<div class="clearfix"></div>
		@endif
	</div>
	@include("util.offer-modal")
	@include("base.home.why-us")
@endsection
@section("footer")
	<div class="footer-wrap">
		@if (isset($data['views']['footer']) && view()->exists($data['views']['footer']))
			@include($data['views']['footer'])
		@endif

		@include('base.home.footer.footer-common')
	</div>
@endsection
@section("javascript")
	@parent
	<script type="text/javascript">
		$.getScript("/js/owl-carousel.js", function () {
			$('#cardsLi').owlCarousel({
				stagePadding: 0,/*the little visible images at the end of the carousel*/
				dots: false,
				rtl: false,
				loop: false,
				margin: 0,
				nav: true,
				navText: ["<span class='pad-10 bg-white-imp'><img src='https://image.flaticon.com/icons/svg/481/481130.svg' style='height:20px'></span>", "<span class='pad-10 bg-white-imp'><img src='https://image.flaticon.com/icons/svg/481/481127.svg' style='height:20px'></span>"],
				lazyload: true,
				responsive: {
					0: {
						items: 1
					},
					1000: {
						items: 2
					},
					1500: {
						items: 2
					},
				}
			});

			$('#reviews-wrap').owlCarousel({
				stagePadding: 15,/*the little visible images at the end of the carousel*/
				dots: true,
				rtl: false,
				loop: true,
				margin: 15,
				nav: false,
				lazyload: true,
				responsive: {
					0: {
						items: 1
					},
					1000: {
						items: 3
					},
				}
			});

			$('.products-wrap').owlCarousel({
				stagePadding: 0,/*the little visible images at the end of the carousel*/
				dots: false,
				rtl: false,
				loop: false,
				margin: 15,
				lazyload: true,
				responsive: {
					0: {
						items: 1
					},
					1000: {
						items: 4
					},

				},
				nav: true,
				navText: ["<span class='pad-10 bg-white-imp'><img src='https://image.flaticon.com/icons/svg/481/481130.svg' style='height:20px'></span>", "<span class='pad-10 bg-white-imp'><img src='https://image.flaticon.com/icons/svg/481/481127.svg' style='height:20px'></span>"]
			});
		});

		(function showCountDown() {
			var countDownDate = new Date("{{config('evibe.valentines-day-landingpages.offers-expire-date')}}").getTime();

			var x = setInterval(function () {
				var now = new Date().getTime();
				var distance = countDownDate - now;
				var days = Math.floor(distance / (1000 * 60 * 60 * 24));
				var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				var seconds = Math.floor((distance % (1000 * 60)) / 1000);
				((days / 10) < 1) ? days = "0" + days : days;
				((hours / 10) < 1) ? hours = "0" + hours : hours;
				((minutes / 10) < 1) ? minutes = "0" + minutes : minutes;
				((seconds / 10) < 1) ? seconds = "0" + seconds : seconds;

				document.getElementById("expire-time").innerHTML = "<span id='expire-days' class='time-bg'>" + days + "</span><span style='font-size:35px'>&nbsp;:&nbsp;</span><span id='expire-hours' class='time-bg'>" + hours + "</span><span style='font-size:35px'>&nbsp;:&nbsp;</span><span id='expire-minutes' class='time-bg'>" + minutes + "</span><span style='font-size:35px'>&nbsp;:&nbsp;</span><span id='expire-secs' class='time-bg'>" + seconds + "</span>";
				if (distance < 0) {
					clearInterval(x);
					document.getElementById("offer-card").classList.add("hide");
				}
			}, 1000);

		})();

		$(document).ready(function () {
			$("#offer-card").on("click", function () {
				var targetModal = $('#book-item-modal');
				targetModal.modal({
					'show': true,
					'keyboard': true
				});
			});
		});

		$('.enquiry-details-submit').click(function (e) {
			var cityUrl = '{{getCityUrl()}}';
			e.preventDefault();
			var url = '/' + cityUrl + '/book';
			var data = {
				'name': $('.lead-details-name').val(),
				'phone': $('.lead-details-phone').val(),
				'callingCode': '+91',
				'email': $('.lead-details-email').val(),
				'partyDate': $('.lead-details-party-date').val(),
				'comments': $('.lead-details-comments').val(),
				'accepts': 'yes',
				'passiveCity': 'NA',
				'isVDayLandingPage': '1',
			};
			$('#btnSubmitFormHome').prop('disabled', true);
			$('.errors-cnt').addClass('hide');
			$("#form-content").addClass("hide");
			window.showLoading();

			$.ajax({
				url: url,
				type: 'POST',
				dataType: 'json',
				data: data,
				success: function (data) {
					window.hideLoading();
					$("#btnSubmitFormHome").removeAttr('disabled');
					if (data.success) {
						$("#form-content").addClass("hide");
						$(".offer-signup-thankyou").removeClass("hide");
					} else {
						var errors = data.errors;
						window.showNotyError(errors);
						$("#form-content").removeClass("hide");
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					$("#btnSubmitFormHome").removeAttr('disabled');
					$('.errors-cnt').removeClass('hide').text("Sorry, your request could not be saved. Please try again later.");
					window.hideLoading();
					window.notifyTeam({
						"url": url,
						"textStatus": textStatus,
						"errorThrown": errorThrown
					});
				}
			});
		});
	</script>
@endsection