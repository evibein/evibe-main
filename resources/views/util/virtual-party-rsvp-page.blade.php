@extends('layout.base')

@section('page-title')
	<title></title>
@endsection

@section('meta-description')
	<meta name="description" content="">
@endsection

@section('meta-keywords')
	<meta name="keywords" content="">
@endsection

@section('og-title')
	<meta property="og:title" content="">
@endsection

@section('og-description')
	<meta property="og:description" content="">
@endsection

@section('og-url')
	<meta property="og:url" content="">
@endsection


@section('seo:schema')
	@parent
	@if(isset($data['seo']['schema']) && count($data['seo']['schema']))
		@foreach($data['seo']['schema'] as $schema)
			{!! $schema !!}
		@endforeach
	@endif
@endsection

@section("header")
	@include('base.home.header.header-home-city')
@endsection
@section('custom-css')
	<link rel="stylesheet" href="{{ elixir('css/af-home.css') }}">
	<link rel="stylesheet" href="{{ elixir('assets/css/home.css') }}">
	<link rel="stylesheet" href="{{ elixir('css/app/inspiration-hub.css') }}">
	<!-- split css to new file -->
	<style>
		.desk-enquiry-inspirations {
			transition: all 0.4s;
			cursor: pointer;
		}

		.desk-enquiry-inspirations:hover {
			opacity: 0.8;
			box-shadow: 0px 8px 15px rgba(46, 229, 157, 0.4);
		}
		.count-down-wrap {
			color: grey;
			padding: 5px;
			font-size: 35px;
			margin-top: 30px;
		}

		input[type=checkbox]{
		height: 0;
		width: 0;
		visibility: hidden;
	}
	label {
		cursor: pointer;
		text-indent: -9999px;
		width: 100px;
		height: 50px;
		background: grey;
		display: block;
		border-radius: 100px;
		position: relative;
	}

	label:after {
		content: '';
		position: absolute;
		top: 0px;
		left: 2px;
		width: 50px;
		height: 50px;
		background: #fff;
		border-radius: 90px;
		transition: 0.3s;
	}

	input:checked + label {
		background: #bada55;

	}

	input:checked + label:after {
		left: calc(100% - 5px);
		transform: translateX(-100%);
	}

	label:active:after {
		width: 130px;
	}
	</style>
@endsection
@section("content")
	<div class=" w-1366 base-home-bg-color" style="padding-left:20px;padding-right:20px">
		<div class="full-width ">
			<h1 style="font-size: 2.75rem;   line-height: 4.5rem;    margin-bottom: 34px;">Cia's Birthday Party</h1>
		</div>
		<div class="col-xs-12 col-md-7 col-lg-7 blur-bg-wrap" style="min-height:400px">
				<img src="https://d1csarkz8obe9u.cloudfront.net/posterpreviews/wedding-card-flyer%2Cposter%2Cinvitation-card-design-template-993341329aaea1c700f5f3b7ddc38c90_screen.jpg?ts=1561472562" class="thumb-img" style="border-radius: 0 !important;">
				<img src="https://d1csarkz8obe9u.cloudfront.net/posterpreviews/wedding-card-flyer%2Cposter%2Cinvitation-card-design-template-993341329aaea1c700f5f3b7ddc38c90_screen.jpg?ts=1561472562" class="blur-bg" style="min-height:400px">
		</div>
		<div class="col-xs-12 col-md-5 col-lg-5">
				<h4> Party is going to start in </h4>
				<p id="expire-time" class="count-down-wrap"></p>
				<div style="font-size: 13px;display: block;margin-top: -4px">
					<div style="width: 48px;margin-right:29px;display: inline-block;font-size: 15px;text-align: center;color:grey">Days</div>
					<div style="width: 48px;margin-right:29px;display: inline-block;font-size: 15px;text-align: center;color:grey">Hours</div>
					<div style="width: 48px;margin-right:29px;display: inline-block;font-size: 15px;text-align: center;color:grey">Mins</div>
					<div style="width: 48px;display: inline-block;font-size: 15px;text-align: center;color:white">Secs</div>
				</div>
				<div class="mar-t-15" style="">
						<button class="	floating-evibe-btn" style="width:50%;	border-radius:20px;margin-top:10px">Join Party
							<img src="{{ $galleryUrl }}/right.png" style="display:inline-block;height:20px;padding-left:10px">
						</button>
						<button class="	floating-evibe-btn" style="width:50%;	border-radius:20px;margin-top:10px">Wish Cia
							<img src="{{ $galleryUrl }}/chat.png" style="display:inline-block;height:20px;padding-left:10px">

						</button>
				</div>
				<span class="" style="	border-radius:20px;display:block;margin-top:25px;font-size:18px;font-weight:500">Are You Going?
						
				</span>
				<input type="checkbox" id="switch" />
				<label for="switch">Toggle</label>

					<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
		<section style="padding-left:20px;padding-right:20px;margin-top:50px">
			<div class="container grid hide" id="inspirations-gallery" style="padding: 0; margin: 0 5px;">
					<div class="grid-sizer" style="width: 50% !important"></div>

						<div class="grid-item hero-grid__item text-center mar-b-20" style="min-height:150px;height:auto;width: 46% !important; margin: 0 2% 15px;">
							<a href="" class="a-no-decoration-black">
								<div class="inspiration-img-wrap">
									<div class="header " style="text-align:left">
											<span style="background-color:grey;height:70px;width:70px;border-radius:50%;padding-left:10px;padding-top:10px">AB</span>
									</div>
								</div>

							</a>
						</div>

					<div class="clearfix"></div>
				</div>


		</section>
	</div>

@endsection
@section("javascript")
		@parent
		<script>
				(function showCountDown() {
					var countDownDate = new Date("Apr 24, 2020 00:00:00").getTime();

					var x = setInterval(function () {
						var now = new Date().getTime();
						var distance = countDownDate - now;
						var days = Math.floor(distance / (1000 * 60 * 60 * 24));
						var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
						var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
						var seconds = Math.floor((distance % (1000 * 60)) / 1000);
						((days / 10) < 1) ? days = "0" + days : days;
						((hours / 10) < 1) ? hours = "0" + hours : hours;
						((minutes / 10) < 1) ? minutes = "0" + minutes : minutes;
						((seconds / 10) < 1) ? seconds = "0" + seconds : seconds;

						document.getElementById("expire-time").innerHTML = "<span id='expire-days' class='time-bg'>" + days + "</span><span style='font-size:35px'>&nbsp;:&nbsp;</span><span id='expire-hours' class='time-bg'>" + hours + "</span><span style='font-size:35px'>&nbsp;:&nbsp;</span><span id='expire-minutes' class='time-bg'>" + minutes + "</span><span style='font-size:35px'>&nbsp;:&nbsp;</span><span id='expire-secs' class='time-bg'>" + seconds + "</span>";
						if (distance < 0) {
							clearInterval(x);
							document.getElementById("offer-card").classList.add("hide");
						}
					}, 1000);

		})();
		</script>

@endsection