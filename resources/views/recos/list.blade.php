@extends('layout.base')

@section('page-title')
	<title>{{ $data['ticket']->name }}, best recommendations for your party based on your preferences | Evibe.in</title>
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/intro.css') }}"/>
@endsection

@section('meta-description')
	<meta name="description" content="best party services recommendation for {{ $data['ticket']->name }}"/>
@endsection

@section("header")
	@include('base.home.header.header-home-city')
	<div class="header-border"></div>
@endsection

@section("content")
	<input id="submitUrl" type="hidden" value="{{ route('customer.recommendation.confirm', $data['ticket']->id) }}">
	<input id="noSelectUrl" type="hidden" value="{{ route('customer.recommendation.confirm.dislike', $data['ticket']->id) }}">
	<input id="recoUrl" type="hidden" value="{{ $data['recoUrl'] }}">
	<input id="recoTicketId" type="hidden" value="{{ $data['ticket']->id }}">
	<input id="lastRated" type="hidden" value="{{ $data['ticket']->last_reco_rated_at }}">
	<input id="lastSkipped" type="hidden" value="{{ $data['ticket']->last_reco_skipped_at }}">
	<input id="lastDislikedAt" type="hidden" value="{{ $data['ticket']->last_reco_dislike_at }}">
	<div class="container">
		<div class="col-sm-12 no-pad text-center">
			<h5 class="no-pad-b"> Best Recommedations For You</h5>
			<div>
				<a class="btn checkout-to-btn btn-evibe text-upr" href="{{route("partybag.list")}}">
					Checkout to Cart
				</a>
			</div>
			<div class="clearfix"></div>
			<div class="recos-sec recos-list-wrap">
				@if(count($data['recommendations']) > 0)
					@foreach ($data['recommendations'] as $type => $values)
						<div class="recos-cat">
							<div class="recos-cat-title">
								<div class="pull-left">
									<h5 class="text-normal text-left">Best {{$type}}</h5>
								</div>
								<div class="pull-right">
									@if(array_key_exists($values[0]['ticketTypeId'], $data['recommendedType']))
										<h6 class="font-14">
											<a target="_blank" rel="noopener" class="btn btn-link no-pad-t" href="{{ $data['recommendedType'][$values[0]['ticketTypeId']]['url'] }}?utm_source=recommendations&utm_campaign=recommendations&utm_medium=recommendations&utm_content={{ $data['ticket']->id }}">
												<i class="glyphicon glyphicon-eye-open"></i> See All {{ $type }}
											</a>
										</h6>
									@endif
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="recos-cat-items">
								@foreach($values as $item)
									<div class="col-sm-12 col-md-4 col-lg-3 no-pad-l no-pad__400">
										<div class="item-result reco-item-result" @if($item['isSelected'] == 1) data-selected="true" @else data-selected="false" @endif data-id="{{ $item['id'] }}" data-type="{{ $type }}" data-url="{{ $item['url'] }}" data-title="{{ $item['name'] }}" data-code="{{ $item['code'] }}" data-type-id="{{$item['ticketTypeId']}}" data-price-min="{{$item['priceMin']}}" data-price-max="{{$item['priceMax']}}" style="padding-bottom: 15px">
											<div class="img-wrap" style="display: flex;align-items: center; justify-content: center;">
												<a href="{{ $item['url'] }}?utm_source=recommendations&utm_campaign=recommendations&utm_medium=recommendations&utm_content={{ $data['ticket']->id }}" target="_blank">
													<img src="{{ $item['imageUrl'] }}" alt="{{ $item['name'] }} profile picture">
												</a>
											</div>
											<div class="item-details">
												<div class="item-title">
													<a href="{{ $item['url'] }}?utm_source=recommendations&utm_campaign=recommendations&utm_medium=recommendations&utm_content={{ $data['ticket']->id }}" target="_blank" rel="noopener" style="color:#717171"> {{ $item['name'] }}</a>
												</div>
												<div class="mid-box pad-t-10">
													<div class="pull-left">
														@if($item['ticketTypeId'] == config('evibe.ticket.type.venue') && $item['venueHall'])
															<div class="price-tag">
																@if($item['venueHall']->rent_min)
																	<div>
																		<span class="valign-mid">
																			@price($item['venueHall']->rent_min)
																			@if ($item['venueHall']->rent_max)
																				- @price($item['venueHall']->rent_max)*
																			@else
																				* onwards
																			@endif
																			@if($item['venueHall']->rent_worth)
																				<span class="price-worth">@price($item['venueHall']->rent_worth)</span>
																			@endif
																		</span>
																		<div class="low-font mar-l-15">Rent ({{ $item['venueHall']->min_rent_duration }} Hrs)</div>
																	</div>
																@elseif ($item['venueHall']->price_min_veg)
																	<div>
																		<span class="valign-mid">
																			<span class="price-worth">
																				@price($item['venueHall']->price_worth_veg)
																			</span>
																			@price($item['venueHall']->price_min_veg)*
																		</span>
																		<div class="low-font mar-l-15">Price (per person)</div>
																	</div>
																@elseif ($item['venueHall']->venue->price_min_rent)
																	<div>
																		<span class="valign-mid">
																			<span class="price">@price($item['venueHall']->venue->price_min_rent)</span>
																			@if ($item['venueHall']->venue->price_max_rent)
																				- @price($item['venueHall']->venue->price_max_rent)*
																			@else
																				* onwards
																			@endif
																			@if($item['venueHall']->venue->worth_rent)
																				<span class="price-worth">@price($item['venueHall']->venue->worth_rent)</span>
																			@endif
																		</span>
																		<div class="low-font mar-l-15">Rent ({{ $item['venueHall']->venue->min_rent_duration }} Hrs)
																		</div>
																	</div>
																@elseif ($item['venueHall']->venue->price_min_veg)
																	<div>
																		<span class="valign-mid">
																			@price($item['venueHall']->venue->price_min_veg)*
																			<span class="price-worth">
																				@price($item['venueHall']->venue->min_veg_worth)
																			</span>
																		</span>
																		<div class="low-font mar-l-15">Price (per person)</div>
																	</div>
																@elseif ($item['venueHall']->venue->price_kid)
																	<div>
																		<span class="valign-mid">
																			@price($item['venueHall']->venue->price_kid)*
																			<span class="price-worth">@price($item['venueHall']->venue->worth_kid)</span>
																		</span>
																		<div class="low-font mar-l-15">Price (per kid)</div>
																	</div>
																@else
																@endif
															</div>
														@else
															<div class="price-tag">
																@price($item['priceMin'])
																@if ($item['priceMax'] && $item['priceMax'] > $item['priceMin'])
																	- @price($item['priceMax'])
																@endif
																@if ($item['priceWorth'] && $item['priceWorth'] > $item['priceMin'])
																	&nbsp;<span class="price-worth"> @price($item['priceWorth'])</span>
																@endif
																@if ($item['extras'])
																	<span class="mar-l-5">{{ $item['extras'] }}</span>
																@endif
															</div>
														@endif
													</div>
													<div class="pull-right " style="font-size: 12px">
														<div class="text-upr low-font text-grey" style="font-size: 12px">Code</div>
														{{ $item['code'] }}
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
											<!-- Surprise plans are not allowed to put in cart-->
											@if(isset($item['occasionId']))
												@if(($item['occasionId'] == config("evibe.occasion.surprises.id")) || ($item["ticketTypeId"] == config('evibe.ticket.type.service')))
													<div class="list-option-content-wrap" style="padding-top:10px">
														<div class="list-option-cta-wrap text-center">
															<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-pad">
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no-pad col-md-offset-3 col-xs-offset-3 col-sm-offset-3 col-lg-offset-3 btn-shortlist ">
																	<a href="{{$item['url']}}?ref=recos-page#bookNowModal" class="item-shortlist shortlistIcon selection-btn btn btn-lg btn-evibe text-upr" style="color:white;padding:15px">
																		Book Now
																	</a>
																</div>
																<div class="clearfix"></div>
															</div>
															<div class="clearfix"></div>
														</div>
													</div>
												@else
													<div class="list-option-content-wrap" style="padding-top:10px">
														<div class="list-option-cta-wrap text-center">
															<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-pad">
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no-pad col-md-offset-3 col-xs-offset-3 col-sm-offset-3 col-lg-offset-3 btn-shortlist ">
																	<a class="item-shortlist shortlistIcon selection-btn btn btn-lg btn-evibe text-upr" style="color:white;padding:15px" v-on:click="shortlistOptionFromListPage">
																		<div class="hideShortlistResultsData list-cta-btn list-btn-shortlist " data-mapId="{{ $item['pId'] }}" data-mapTypeId="{{ $item['ticketTypeId'] }}" data-cityId="{{getCityId()}}" data-occasionid="{{$item['occasionId']}}" data-urlAdd="{{ route('partybag.add') }}" data-urlDelete="{{ route('partybag.delete') }}">
																			Add To Cart
																		</div>
																	</a>
																</div>
																<div class="clearfix"></div>
															</div>
															<div class="clearfix"></div>
														</div>
													</div>
												@endif
											@endif

										</div>
									</div>
								@endforeach
								<div class="clearfix"></div>
							</div>
						</div>
						<hr class="category-end">
					@endforeach
					@if($data['ticket']->event_id =! config("evibe.occasion.surprises.id"))
						<div class="text-center mar-t-30">
							<a class="btn-no-choices hide">
								<i class="glyphicon glyphicon-thumbs-down"></i> I did not like any option
							</a>
							<a class="btn btn-lg btn-evibe btn-submit-choices text-upr">
								<i class="glyphicon glyphicon-send"></i> I Need Modifications
							</a>
						</div>
					@endif
				@else
					<h4 class="text-danger alert alert-danger mar-t-30 mar-b-30">
						<i class="glyphicon glyphicon-info-sign"></i>Sorry! We could not find any recommendations.
					</h4>
				@endif
			</div>
			<div class="pad-b-30 text-center">
				<div class="font-18 mar-t-10">
					Not satisfied with our recommendations?
				</div>
				<div class="mar-t-10">
					<a class="btn btn-default btn-no-choices">
						Browse More
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="hide">
		<div class="col-xs-12 no-pad btn-evibe btn-mobile-submit btn-recos-mobile">
			<a class="btn-no-choices hide">
				<div class="reco-dislike-btn col-xs-6 no-pad">
					I did not like<br>any option
				</div>
			</a>
			<a class="btn-submit-choices">
				<div class="reco-like-btn col-xs-12 no-pad">
					Submit My<br>Choices
				</div>
			</a>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	@include('base.home.why-us')
@endsection

@section("modals")
	@parent
	<div>
		@include('app.modals.auto-popup',
		[
		"cityId" => getCityId(),
		"occasionId" => "",
		"mapTypeId" => "",
		"mapId" => "",
		"optionName" => "",
		"countries" => (isset($data['countries']) && count($data['countries'])) ? $data['countries'] : []
		])
	</div>
	@include('app.modals.recos.submit-success-modal', ["customerName" => $data['ticket']->name])
	@include('app.modals.recos.next-steps-modal', [
		"customerName" => $data['ticket']->name,
		"ticketId" => $data['ticket']->id
	])
	@include('layout.base-vue-modals')
@endsection

@section("footer")
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			$.getScript("/js/util/intro.js", function () {
				if (parseInt(Cookies.get("recommendationIntro"), 10) !== 2) {
					$(".reco-alert-info").addClass("hide");
					introJs().addSteps([
						{
							element: document.querySelectorAll('.btn-shortlist')[0],
							intro: "Step 1: Click on \"Add To Cart\" for all the options you like.",
							position: "bottom",
							scrollTo: "element",
						},
						{
							element: ".btn-submit-choices",
							intro: "Step 2: Click on this button to share your modifications with us(If any). We will get in touch with you for the next steps.",
							position: "top",
							scrollTo: "element"
						}

					]).start()
						.oncomplete(function () {
							$(".reco-alert-info").removeClass("hide");
							Cookies.set("recommendationIntro", "2", {expires: 2});
						})

						.onexit(function () {
							$(".reco-alert-info").removeClass("hide");
							Cookies.set("recommendationIntro", "2", {expires: 2});
						});
				}
			});

			var lastSkipped = $('#lastSkipped').val();
			var submitted = false;
			var today = new Date();
			var t = lastSkipped.split(/[- :]/);
			var budget = {};
			createBudgetObject();

			/* Apply each element to the Date function */
			today.setHours(0, 0, 0, 0);

			$('.partybag-wrap-mobile').addClass('hide');
			$('.tawkchat-minified-wrapper').addClass('hide');

			(function initModals() {
				$('#recoNextStepsModal').modal({
					keyboard: true,
					show: false
				});

				$("#recoNextStepsModal").find(".mdl-textfield").removeClass("is-upgraded").attr("data-upgraded", "");
				if (!(typeof (componentHandler) === 'undefined')) {
					componentHandler.upgradeDom();
				}
			})();

			function createBudgetObject() {
				$('.item-result').each(function (i, item) {
					if ($(this).find('.shortlistedIcon').length) {
						if (typeof budget[$(this).data('type-id')] === "undefined") {
							budget[$(this).data('type-id')] = {};
							if ($(this).data('price-max') > 0) {
								budget[$(this).data('type-id')][$(this).data('id')] = {
									minPrice: $(this).data('price-min'),
									maxPrice: $(this).data('price-max'),
									id: $(this).data('id'),
									name: $(this).data('title'),
									url: $(this).data('url'),
									code: $(this).data('code'),
									type: $(this).data('type')
								};
							} else {
								budget[$(this).data('type-id')][$(this).data('id')] = {
									minPrice: $(this).data('price-min'),
									maxPrice: $(this).data('price-min'),
									id: $(this).data('id'),
									name: $(this).data('title'),
									url: $(this).data('url'),
									code: $(this).data('code'),
									type: $(this).data('type')
								};
							}
						} else {
							if ($(this).data('price-max') > 0) {
								budget[$(this).data('type-id')][$(this).data('id')] = {
									minPrice: $(this).data('price-min'),
									maxPrice: $(this).data('price-max'),
									id: $(this).data('id'),
									name: $(this).data('title'),
									url: $(this).data('url'),
									code: $(this).data('code'),
									type: $(this).data('type')

								};
							} else {
								budget[$(this).data('type-id')][$(this).data('id')] = {
									minPrice: $(this).data('price-min'),
									maxPrice: $(this).data('price-min'),
									id: $(this).data('id'),
									name: $(this).data('title'),
									url: $(this).data('url'),
									code: $(this).data('code'),
									type: $(this).data('type')

								};
							}
						}

					}
				});

				calculateBudget();
			}

			function pushInItemValue() {
				var itemValue = [];
				for (var mapTypeId in budget) {
					if ((budget.hasOwnProperty(mapTypeId)) && (typeof budget[mapTypeId] !== "undefined")) {
						for (var mapId in budget[mapTypeId]) {
							if ((budget[mapTypeId].hasOwnProperty(mapId)) && (typeof budget[mapTypeId][mapId] !== "undefined")) {

								itemValue.push({
									id: budget[mapTypeId][mapId]['id'],
									name: budget[mapTypeId][mapId]['name'],
									url: budget[mapTypeId][mapId]['url'],
									code: budget[mapTypeId][mapId]['code'],
									type: budget[mapTypeId][mapId]['type']
								});
							}
						}
					}
				}
				return itemValue;
			}

			function calculateBudget() {
				var minPrice = 0;
				var maxPrice = 0;
				var sortMin = [];
				var sortMax = [];

				for (var mapTypeId in budget) {
					if ((budget.hasOwnProperty(mapTypeId)) && (typeof budget[mapTypeId] !== "undefined")) {
						for (var mapId in budget[mapTypeId]) {
							if ((budget[mapTypeId].hasOwnProperty(mapId)) && (typeof budget[mapTypeId][mapId] !== "undefined")) {
								sortMin.push([budget[mapTypeId][mapId]['minPrice']]);
								sortMax.push([budget[mapTypeId][mapId]['maxPrice']]);
							}
						}

						sortMin.sort(function (a, b) {
							return a - b
						});
						sortMax.sort(function (a, b) {
							return a - b
						});

						minPrice = +minPrice + +sortMin[0];
						maxPrice = +maxPrice + +sortMax[sortMax.length - 1];
						sortMin = [];
						sortMax = [];
					}
				}

				if (!minPrice) {
					$('.reco-budget-wrap').addClass('hide');
				} else {
					$('.reco-budget-wrap').removeClass('hide');
				}

				if (maxPrice === minPrice) {
					$('#budgetMin').text(minPrice).val(minPrice);
					$('.max-type').addClass('hide');
				} else {
					$('#budgetMin').text(minPrice).val(minPrice);
					$('#budgetMax').text(maxPrice).val(maxPrice);
					$('.max-type').removeClass('hide');
				}
			}

			function showSuccessContent() {
				$('.reco-success-content').removeClass('hide');
				$('.reco-fail-content').addClass('hide');
			}

			function showFailContent() {
				$('.reco-fail-content').removeClass('hide');
				$('.reco-success-content').addClass('hide');
			}

			$('.btn-next-steps-submit').click(function (event) {
				event.preventDefault();
				createBudgetObject();
				var nonSelectedIds = [];
				submitted = true;
				$('#recoNextStepsModal').modal('hide');
				window.showLoading();
				$.ajax({
					type: 'POST',
					url: $(this).data('url'),
					dataType: 'JSON',
					data: {
						recoOption: $("input[name=recoSuccessOption]:checked").val(),
						recoText: $('#recoSuccessText').val(),
						selectedRecos: pushInItemValue(),
						nonSelectedMappings: nonSelectedIds
					},
					success: function (data) {
						window.hideLoading();
						if (data.success) {
							$('#modalRecoSuccess').modal('show');
						} else {
							var error = "Some error occurred while submitting next steps. Please try again";
							if (data.error) {
								error = data.error;
							}
							$('#recoNextStepsModal').modal('show');
							window.showNotyError(error);
						}
					},
					fail: function () {
						window.hideLoading();
						window.showNotyError();
					}
				});
			});

			$('.btn-new-reco-submit').click(function (event) {
				event.preventDefault();

				var nonSelectedIds = [];
				var recoUrl = $('#recoUrl').val();
				submitted = true;
				$('#recoNextStepsModal').modal('hide');
				window.showLoading();

				$.ajax({
					type: 'POST',
					url: $(this).data('url'),
					dataType: 'JSON',
					data: {
						recoOption: $("input[name=recoFailOption]:checked").val(),
						recoText: $('#recoFailText').val(),
						selectedRecos: pushInItemValue(),
						nonSelectedMappings: nonSelectedIds,
						recoUrl: recoUrl
					},
					success: function (data) {
						window.hideLoading();
						if (data.success) {
							window.location.replace("/");
						} else {
							var error = "Some error occurred while requesting new recommendations. Please try again";
							if (data.error) {
								error = data.error;
							}
							$('#recoNextStepsModal').modal('show');
							window.showNotyError(error);
						}
					},
					fail: function () {
						window.hideLoading();
						window.showNotyError();
					}
				});
			});

			/* hide modal */
			$('#modalRecoSuccess').find('.close, .btn-danger').click(function () {
				$('#modalRecoSuccess').modal('hide');
			});

			/* handle submit */
			$('.btn-submit-choices').click(function (event) {
				event.preventDefault();
				var selectedItems = $('.shortlistedIcon > div');
				if (selectedItems.length == 0) {
					alert('Please add an option to cart by clicking on "ADD TO CART" to request for modifications');
					return false;
				}

				showSuccessContent();
				$('#recoNextStepsModal').modal('show');
			});

			/* handle "i did not like option" */
			$('.btn-no-choices').click(function (event) {
				event.preventDefault();

				showFailContent();
				$('#recoNextStepsModal').modal('show');
			});
		});
	</script>
@endsection