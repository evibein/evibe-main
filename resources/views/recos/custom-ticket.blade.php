<div class="col-sm-12 col-md-4 col-lg-3 no-pad-l no-pad__400">
	@if($item['ticketTypeId'] == config('evibe.ticket.type.decor'))
		<div class="reverse-bidding-wrap reco-rev-bidding-wrap">
			<div class="title text-e">
				Have your own themes?
			</div>
			<div class="content">
				You can now upload your customised designs. Based on the availability, we will get the best quote from our verified partners.
			</div>
			<button class="btn btn-default btn-bidding btn-decor">
				<i class="material-icons valign-mid">&#xE2C6;</i>
				<span class="valign-mid">Submit My Designs</span>
			</button>
		</div>

	@elseif($item['ticketTypeId'] == config('evibe.ticket.type.cake'))
		<div class="reverse-bidding-wrap reco-rev-bidding-wrap">
			<div class="title text-e">
				Have your own inspirations?
			</div>
			<div class="content">
				You can now upload your customised designs. Based on the availability, we will get the best quote from our verified partners.
			</div>
			<button class="btn btn-default btn-bidding btn-cake">
				<i class="material-icons valign-mid">&#xE2C6;</i>
				<span class="valign-mid">Submit My Designs</span>
			</button>
		</div>

	@elseif($item['ticketTypeId'] == config('evibe.ticket.type.food'))
		<div class="reverse-bidding-wrap reco-rev-bidding-wrap">
			<div class="title text-e">
				Have your own menu?
			</div>
			<div class="content">
				You can now upload your customised menu. Based on the availability, we will get the best quote from our verified partners.
			</div>
			<button class="btn btn-default btn-bidding btn-food">
				<i class="material-icons valign-mid">&#xE2C6;</i>
				<span class="valign-mid">Submit My Menu</span>
			</button>
		</div>
	@endif
</div>