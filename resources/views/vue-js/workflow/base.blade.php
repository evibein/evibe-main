<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en"> <!--<![endif]-->

<head>
	<!-- GTM DataLayer -->
	@yield('gtm-data-layer')

	<!-- trackers -->
	@include('track.gtm')

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width">
	<meta name="google-site-verification" content="aAggmaMI_knVUeswYULXQSKD41sWlr2z3G21bdkPZoI"/>
	<meta name="sitelock-site-verification" content="7865"/>

	@section('meta-description')
		<meta name="description" content="One stop shop to find and book the best birthday party planners, party organisers, party halls, restaurants, magicians, artists, home theme decorators, balloon decorators, theme cakes, standard cakes">
	@show

	@section('meta-keywords')
		<meta name="keywords" content="kids birthday, party halls, party organisers, magicians">
	@show

	@section('meta-canonical')
	@show

	<meta name="msvalidate.01" content="31489A18F1AD59FC089CB9B9A7E5A85D"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<!-- Bing webmaster -->

	<!-- Open graph tags -->
	@section('og-title')
		<meta property="og:title" content="Best Kids Birthday Planners, Party Organisers, Venues | Evibe.in">
	@show

	@section('og-url')
		<meta property="og:url" content="https://evibe.in">
	@show

	@section('og-image')
		<meta property="og:image" content="{{ $galleryUrl }}/img/logo/logo_evibe.png">
	@show

	@section('og-description')
		<meta property="og:description" content="One stop shop to find and book the best birthday planners, party organisers, party halls, restaurants, magicians, artists, home theme decorators, balloon decorators, balloon decorators, theme cakes, standard cakes.">
	@show

	<meta property="og:site_name" content="Evibe.in">
	<meta property="fb:admins" content="">

	@section('seo:schema')
		<script type="application/ld+json">
			{
				"@context": "http://schema.org",
				"@type": "Organization",
				"name": "Evibe.in",
				"legalName": "Evibe Technologies Private Limited",
				"url": "https://www.evibe.in",
				"email": "ping@evibe.in",
				"logo": "https://gallery.evibe.in/img/logo/logo_evibe.png",
				"foundingDate": "2014-02-10",
				"contactPoint": [{
					"@type": "ContactPoint",
					"telephone": "+91-9640204000",
					"contactType": "customer service",
					"areaServed": "IN",
					"availableLanguage": ["English", "Telugu", "Hindi", "Kannada"]
				}],
				"founders": [{
					"@type": "Person",
					"name": "B Anjaneyulu Reddy",
					"jobTitle": "CEO"
				},{
					"@type": "Person",
					"name": "Swathi Bavanaka",
					"jobTitle": "COO"
				}],
				"sameAs": [
					"https://www.facebook.com/evibe.in",
					"https://www.instagram.com/evibe.in",
					"https://twitter.com/evibein",
					"https://plus.google.com/+EvibeIn",
					"https://www.linkedin.com/company-beta/3152628",
					"https://www.youtube.com/channel/UCLANgB4zAIj35TH1SsiuLIg"
				]
			}
		</script>
	@show

	@section('page-title')
		<title>Best Kids Birthday Planners, Youth Party Organisers, Wedding Reception & Engagement Services | Evibe.in</title>
	@show

	@section('custom-css')
		<link rel="stylesheet" href="{{ elixir('css/workflow.css') }}"> <!-- css -->
	@show

	@section('material-icons')
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	@show

	<link rel="icon" href="{{ $galleryUrl }}/img/logo/favicon_evibe_v2.png" type="image/png" sizes="16x16"/>
	<link rel="apple-touch-icon" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="57x57" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-57x57.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="72x72" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-72x72.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="76x76" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-76x76.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="114x114" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-114x114.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="120x120" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-120x120.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="144x144" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-144x144.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="152x152" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-152x152.png" type="image/png"/>

	@yield('push-notifications')
	@yield('custom-head')
</head>

<body>
@section('seo-search')
	@if (getCityUrl())
		<div itemscope itemtype="http://schema.org/WebSite" class="hide">
			<meta itemprop="url" content="{{ route('city.home', getCityUrl()) }}"/>
			<form itemprop="potentialAction" itemscope itemtype="http://schema.org/SearchAction">
				<meta itemprop="target" content="{{ route('city.search', getCityUrl()) }}?q={search_term_string}"/>
				<input itemprop="query-input" type="text" name="search_term_string" required/>
				<input type="submit"/>
			</form>
		</div>
	@endif
@show

<!-- Application modals -->
@section("modals")
	<section id="modalLoading" class="modal modal-loading" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-body text-center">
					<h4>
						<img src="{{ $galleryUrl }}/img/icons/loading.gif" alt="Loading icon"> Loading, please wait...
					</h4>
					<div class="text-danger">(Please do not press refresh / back button)</div>
				</div>
			</div>
		</div>
	</section>
@show

<!-- Application content -->
<div class="application" id="workflow">
	<input type="hidden" id="vueJsAppDebug" value="{{ config("app.debug") }}">

	@yield('header')

	<div class="workflow-container-wrap">
		@yield('content')
	</div>

	<footer>
		@yield('footer')
	</footer>
</div>

@section('js-framework')
	<script src="{{ elixir('js/workflow.js') }}"></script>
@show

<!-- Load Google Fonts -->
<!-- @author: Anji; @since 2 Nov 2016 -->
<!-- @see: https://www.lockedowndesign.com/load-google-fonts-asynchronously-for-page-speed/ -->
<!-- <script type="text/javascript">
	WebFontConfig = {
		google: {families: ['Roboto:300,400,500,700|Material+Icons']}
	};
	(function () {
		var wf = document.createElement('script');
		wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
			'://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js';
		wf.type = 'text/javascript';
		wf.async = 'true';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(wf, s);
	})();
</script> -->

@yield('javascript')

</body>
</html>