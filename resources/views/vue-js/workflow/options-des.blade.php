@extends("vue-js.workflow.base")

@section("content")
	<div class="workflow-loading-screen">
		<div>
			<img src="{{ config("evibe.gallery.host")."/img/gifs/evibe_services.gif" }}" width="100" height="100">
		</div>
	</div>
	<div class="kids-events-workflow-wrap hide page-content">
		<workflow-top-nav>
			<div class="pull-left in-blk pad-10">
				<img src="http://gallery.evibe.in/img/logo/logo_evibe.png">
			</div>
			<div class="pull-right mar-t-15 mar-r-15 font-24 question-mark hide" @click="helpIconIntro">
				<i class="glyphicon glyphicon-question-sign"></i>
			</div>
			<div class="clearfix"></div>
			<div class="workflow-screens-hr"></div>
		</workflow-top-nav>
		<workflow-body-nav>
			<div class="col-sm-8">
				<div class="workflow-change-requirement-wrap">
					Showing best options matching your <b>@{{ occasion }}</b>
					<a href="{{ route("city.workflow.home", [getCityUrl(), $data["searchId"]]) }}" class="workflow-change-requirement-button pad-l-5">
						<span class="text-normal">(change)</span>
					</a>
				</div>
				<div class="mar-l-10 mar-b-10" id="workflowIntrojsStep1">
					<div class="workflow-screen5-service-wrap workflow-screen5-service-wrap-des" v-for="(service, index) in services" :class="selectedService(index)" @click="changeService(index)">
						<i class="material-icons" v-text="getImageFromIndex(index)"></i>@{{ service.name }}
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="bottom-navigation-shortlist-button bottom-navigation-shortlist-button-des text-center pull-right" @click="showShortListedOptions">
					<i class="material-icons">shopping_basket</i>
					<span class="partybag-badge">@{{ shortlistCount }}</span>
				</div>
				<button class="bottom-navigation-enquire-button bottom-navigation-enquire-button-des mar-t-20 pull-right text-center" @click="enquireNow">
					<span>ENQUIRE NOW</span>
				</button>
			</div>
			<div class="clearfix"></div>
			<div class="workflow-screen5-top-shadow"></div>
			<div class="workflow-screen5-options-wrap-des">
				<div class="col-md-10 col-xs-offset-1">
					<div v-for="(services, index) in serviceDetails" v-if="currentScreen == index" class="mar-b-10">
						<div v-if="services.length === 0" class="text-center text-warning font-16 mar-t-30">
							No options found based on your selection for @{{ occasion }}, Please
							<a href="{{ route("city.workflow.home", [getCityUrl(), $data["searchId"]]) }}" class="workflow-change-requirement-button pad-l-5 pad-r-5">
								<span class="text-normal">modify your requirements.</span>
							</a>
						</div>
						<div v-else>
							<div class="col-lg-3 col-md-3 pad-l-10 pad-r-10" v-for="option in services">
								<div class="list-option-card mar-t-20 text-center">
									<div class="list-option-image-wrap text-left">
										<a class="list-option-link" :href="option.url" target="_blank">
											<img class="list-option-image" :src="option.profilePic" :alt="option.name" onError="this.src='{{ $galleryUrl }}/img/icons/gift.png'; this.style='padding: 10px 10px'">
										</a>
									</div>
									<div class="list-option-content-wrap text-center mar-t-3 mar-l-5 mar-r-5">
										<a class="list-option-title" :href="option.url" target="_blank">
											@{{ option.name }}
										</a>
										<div class="list-option-price">
											<div class="list-price-worth in-blk mar-r-5 text-muted" v-if="option.priceWorth > option.minPrice">
												<strike><span class='rupee-font'>&#8377; </span> @{{ formatPrice(option.priceWorth) }}</strike>
											</div>
											<div class="list-main-price in-blk">
												<span class='rupee-font'>&#8377; </span> @{{ formatPrice(option.minPrice) }}
												<span v-if="option.maxPrice > option.minPrice"> - <span class='rupee-font'>&#8377; </span>@{{ formatPrice(option.maxPrice) }}</span>
											</div>
										</div>
									</div>
									<div class="text-center" :class="{'workflow-screen5-shortlist-btn': option.isSelected == 0, 'workflow-screen5-remove-btn': option.isSelected == 1, 'shortlist-initialize-introjs-button': shortlistInitializeButton == 1}">
								<span v-if="option.isSelected === 0" @click="shortlistOption(index, option.id)">
									<i class="glyphicon glyphicon-thumbs-up"></i> Shortlist This
								</span>
										<span v-else @click="removeShortlistedOption(index, option.id)">
									<i class="glyphicon glyphicon-remove"></i> Remove
								</span>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</workflow-body-nav>
		<workflow-bottom-nav class="bottom-navigation full-width">
			<div class="workflow-screens-hr"></div>
		</workflow-bottom-nav>
	</div>
	<div class="workflow-enquiry-screen-des">
		<div id="modalWorkflowEnquiryForm" class="modal" role="dialog" tabindex="-1">
			<div class="modal-dialog modal-md">.
				<div class="modal-content mar-r-20">
					<div class="modal-body">
						<div class="modal-inner-header mar-b-10">
							<button type="button" class="close font-24" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title text-center">Enquire Now</h4>
						</div>
						<div class="col-xs-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
								<input class="mdl-textfield__input text-normal" type="text" name="enquiryName" v-model="screen6name" id="enquiryName" value=""/>
								<label class="mdl-textfield__label text-normal" for="enquiryName">Name</label>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
								<input class="mdl-textfield__input text-normal" type="text" name="enquiryPhone" v-model="screen6phone" id="enquiryPhone" value=""/>
								<label class="mdl-textfield__label text-normal" for="enquiryPhone">Phone Number</label>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
								<input class="mdl-textfield__input text-normal" type="text" name="enquiryEmail" v-model="screen6email" id="enquiryEmail" value=""/>
								<label class="mdl-textfield__label text-normal" for="enquiryEmail">Email</label>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
								<textarea class="mdl-textfield__input text-normal" name="comment" id="comment" v-model="screen6comment"></textarea>
								<label class="mdl-textfield__label text-normal mar-b-10" for="comment">Your Requirements
									<small> (Share more details to help you better)</small>
								</label>
							</div>
						</div>
						<div class="col-xs-12">
							<label for="workflowEnquiryPlanStatus">When are you planning to book?</label>
							<select id="workflowEnquiryPlanStatus" class="form-control" v-model="screen6bookingLikeliness">
								<option value="-1">Select an option</option>
								<option value="1">Immediately</option>
								<option value="2">In a week</option>
								<option value="3">In a month</option>
								<option value="4">Later</option>
								<option value="5">Just Browsing</option>
							</select>
						</div>
						<div class="col-xs-12 mar-t-20">
							<label for="workflowEnquiryTimeSlot">When should we reach you?</label>
							<select id="workflowEnquiryTimeSlot" class="form-control" v-model="screen6timeSlot">
								<option value="-1">Select an option</option>
							</select>
							<div class="hide" id="workflowEnquiryTimeSlotOptions">
								<option value="Immediate" data-min-time="36000" data-max-time="72000">In One Hour
								</option>
								<option value="Today Morning(10 AM - 1 PM)" data-min-time="0"
										data-max-time="36000">Today Morning (10 AM - 1 PM)
								</option>
								<option value="Today Afternoon(1 PM - 3 PM)" data-min-time="0"
										data-max-time="46800">Today Afternoon (1 PM - 3 PM)
								</option>
								<option value="Today Evening(3 PM - 6 PM)" data-min-time="0"
										data-max-time="54000">Today Evening (3 PM - 6 PM)
								</option>
								<option value="Today Night(6 PM - 8 PM)" data-min-time="0"
										data-max-time="64800">Today Night (6 PM - 8 PM)
								</option>
								<option value="Tomorrow Morning(10 AM - 1 PM)" data-min-time="46800"
										data-max-time="86399">Tomorrow Morning (10 AM - 1 PM)
								</option>
								<option value="Tomorrow Afternoon(1 PM - 3 PM)" data-min-time="54000"
										data-max-time="86399">Tomorrow Afternoon (1 PM - 3 PM)
								</option>
								<option value="Tomorrow Evening(3 PM - 6 PM)" data-min-time="64800"
										data-max-time="86399">Tomorrow Evening (3 PM - 6 PM)
								</option>
								<option value="Tomorrow Night(6 PM - 8 PM)" data-min-time="72000"
										data-max-time="86399">Tomorrow Night (6 PM - 8 PM)
								</option>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="text-center mar-t-30 mar-b-10">
							<a class="btn-evibe pad-10 font-16 cur-point" @click="submitEnquiry">
								SUBMIT
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="workflow-shortlist-screen hide">
		<workflow-top-nav>
			<div class="workflow-screen6-top-nav text-center">
				<span class="pull-left mar-l-10" @click="closeShortlistNow">
					<i class="material-icons">keyboard_backspace</i>
				</span>
				<div class="in-blk">
					<h4>Shortlisted Options</h4>
				</div>
				<div class="in-blk pull-right mar-r-10 top-enquiry-form-party-bag">
					<i class="material-icons">shopping_basket</i>
					<span class="partybag-badge">@{{ shortlistCount }}</span>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="workflow-screens-hr"></div>
		</workflow-top-nav>
		<workflow-body-nav>
			<div class="workflow-screen7-options-wrap workflow-screen7-options-wrap-des">
				<div class="workflow-screen7-loading mar-t-30 text-center">Getting your shortlisted options...</div>
				<div v-for="(shortListServices, index) in screen7ShortlistedOptions" class="mar-b-10 workflow-screen7-options">
					<div class="col-xs-6 col-md-3 col-lg-2 pad-l-10 pad-r-10" v-for="shortlistOption in shortListServices.options" v-if="shortlistOption.isSelected === 1">
						<div class="list-option-card mar-t-15 text-center">
							<div class="list-option-image-wrap text-left">
								<a class="list-option-link" :href="shortlistOption.url" target="_blank">
									<img class="list-option-image" :src="shortlistOption.profilePic" :alt="shortlistOption.name" onError="this.src='{{ $galleryUrl }}/img/icons/gift.png'; this.style='padding: 10px 10px'">
								</a>
							</div>
							<div class="list-option-content-wrap text-center">
								<a class="list-option-title" :href="shortlistOption.url" target="_blank">
									@{{ shortlistOption.name }}
								</a>
								<div class="list-option-price">
									<div class="list-price-worth in-blk mar-r-5 text-muted" v-if="shortlistOption.priceWorth > shortlistOption.minPrice">
										<strike><span class='rupee-font'>&#8377; </span> @{{ formatPrice(shortlistOption.priceWorth) }}</strike>
									</div>
									<div class="list-main-price in-blk">
										<span class='rupee-font'>&#8377; </span> @{{ formatPrice(shortlistOption.minPrice) }}
										<span v-if="shortlistOption.maxPrice > shortlistOption.minPrice"> - <span class='rupee-font'>&#8377; </span>@{{ formatPrice(shortlistOption.maxPrice) }}</span>
									</div>
								</div>
							</div>
							<div class="text-center" :class="{'workflow-screen5-shortlist-btn': shortlistOption.isSelected == 0, 'workflow-screen5-remove-btn': shortlistOption.isSelected == 1, 'shortlist-initialize-introjs-button': shortlistInitializeButton == 1}">
								<span v-if="shortlistOption.isSelected === 1" @click="removeScreen7ShortlistedOption(index, shortlistOption.id)">
									<i class="glyphicon glyphicon-remove"></i> Remove
								</span>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</workflow-body-nav>
		<workflow-bottom-nav class="bottom-navigation full-width text-center">
			<div class="workflow-screens-hr"></div>
			<button class="bottom-navigation-enquire-button bottom-navigation-enquire-button-des mar-t-20 mar-b-20 text-center" @click="enquireNow">
				<span>ENQUIRE NOW</span>
			</button>
		</workflow-bottom-nav>
	</div>
	<input type="hidden" id="workflowCurrentScreen" value="options">
	<input type="hidden" id="userCityId" value="{{ getCityId() }}">
	<input type="hidden" id="workflowShortlistUrl" value="{{ route("city.workflow.options.shortlist", [getCityUrl(), $data["searchId"]]) }}">
	<input type="hidden" id="workflowShortlistedOptionsUrl" value="{{ route("city.workflow.options.shortlist.list", [getCityUrl(), $data["searchId"]]) }}">
	<input type="hidden" id="workflowEnquireUrl" value="{{ route("city.workflow.options.enquire", [getCityUrl(), $data["searchId"]]) }}">
	<input type="hidden" id="workflowRemoveShortlistUrl" value="{{ route("city.workflow.options.delete", [getCityUrl(), $data["searchId"]]) }}">
	<input type="hidden" id="workflowDataUrl" value="{{ route("city.workflow.options.data", [getCityUrl(), $data["searchId"]]) }}">
@endsection