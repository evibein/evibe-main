@extends("vue-js.workflow.base")

@section("content")
	<div class="workflow-loading-screen">
		<div>
			<img src="{{ config("evibe.gallery.host")."/img/gifs/evibe_services.gif" }}" width="100" height="100">
		</div>
	</div>
	<div class="kids-events-workflow-wrap kids-events-workflow-wrap-mob hide page-content">
		<workflow-top-nav>
			<div class="pull-left in-blk pad-10">
				<img src="http://gallery.evibe.in/img/logo/logo_evibe.png">
			</div>
			<div class="pull-right mar-t-15 mar-r-15 font-24 question-mark hide" @click="helpIconIntro">
				<i class="glyphicon glyphicon-question-sign"></i>
			</div>
			<div class="bottom-navigation-shortlist-button text-center pull-right" @click="showShortListedOptions">
				<i class="material-icons">shopping_basket</i>
				<span class="partybag-badge">@{{ shortlistCount }}</span>
			</div>
			<div class="clearfix"></div>
			<div class="workflow-screens-hr"></div>
		</workflow-top-nav>
		<workflow-body-nav>
			<div class="workflow-screen5-options-mob">
				<div class="workflow-change-requirement-wrap">
					Showing best options matching your <b>@{{ occasion }}</b>
					<a href="{{ route("city.workflow.home", [getCityUrl(), $data["searchId"]]) }}" class="workflow-change-requirement-button pad-l-5">
						<span class="text-normal">(change)</span>
					</a>
				</div>
				<div class="clearfix"></div>
				<div class="mar-t-10 mar-l-10 mar-b-10" id="workflowIntrojsStep1">
					<div class="workflow-screen5-service-wrap" v-for="(service, index) in services" :class="selectedService(index)" @click="changeService(index)">
						<i class="material-icons" v-text="getImageFromIndex(index)"></i>@{{ service.name }}
					</div>
				</div>
				<div class="workflow-screen5-top-shadow"></div>
				<div class="workflow-screen5-options-wrap-mob">
					<div v-for="(services, index) in serviceDetails" v-if="currentScreen == index" class="mar-b-10">
						<div v-if="services.length === 0" class="text-center text-warning font-16 mar-t-30 pad-l-10 pad-r-10 pad-t-10">
							No options found based on your selection for @{{ occasion }}, Please
							<a href="{{ route("city.workflow.home", [getCityUrl(), $data["searchId"]]) }}" class="workflow-change-requirement-button pad-l-5 pad-r-5">
								<span class="text-normal">modify your requirements.</span>
							</a>
						</div>
						<div v-else>
							<div class="col-xs-6 pad-l-10 pad-r-10" v-for="option in services">
								<div class="list-option-card mar-t-20 text-center">
									<div class="list-option-image-wrap text-left">
										<a class="list-option-link" :href="option.url" target="_blank">
											<img class="list-option-image" :src="option.profilePic" :alt="option.name" onError="this.src='{{ $galleryUrl }}/img/icons/gift.png'; this.style='padding: 10px 10px'">
										</a>
									</div>
									<div class="list-option-content-wrap text-center">
										<a class="list-option-title" :href="option.url" target="_blank">
											@{{ option.name }}
										</a>
										<div class="list-option-price">
											<div class="list-price-worth in-blk mar-r-5 text-muted" v-if="option.priceWorth > option.minPrice">
												<strike><span class='rupee-font'>&#8377; </span> @{{ formatPrice(option.priceWorth) }}</strike>
											</div>
											<div class="list-main-price in-blk">
												<span class='rupee-font'>&#8377; </span> @{{ formatPrice(option.minPrice) }}
												<span v-if="option.maxPrice > option.minPrice"> - <span class='rupee-font'>&#8377; </span>@{{ formatPrice(option.maxPrice) }}</span>
											</div>
										</div>
									</div>
									<div class="text-center" :class="{'workflow-screen5-shortlist-btn': option.isSelected == 0, 'workflow-screen5-remove-btn': option.isSelected == 1, 'shortlist-initialize-introjs-button': shortlistInitializeButton == 1}">
								<span v-if="option.isSelected === 0" @click="shortlistOption(index, option.id)">
									<i class="glyphicon glyphicon-thumbs-up"></i> Shortlist This
								</span>
										<span v-else @click="removeShortlistedOption(index, option.id)">
									<i class="glyphicon glyphicon-remove"></i> Remove
								</span>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</workflow-body-nav>
		<workflow-bottom-nav class="bottom-navigation full-width">
			<div class="workflow-screens-hr"></div>
			<button class="bottom-navigation-enquire-button text-center" @click="enquireNow">
				<span>ENQUIRE NOW</span>
			</button>
		</workflow-bottom-nav>
	</div>
	<div class="workflow-enquiry-screen workflow-enquiry-screen-mob hide">
		<workflow-top-nav>
			<div class="workflow-screen6-top-nav text-center">
				<span class="pull-left mar-l-10" @click="closeEnquireNow">
					<i class="material-icons">keyboard_backspace</i>
				</span>
				<div class="in-blk">
					<h4>Enquire Now</h4>
				</div>
				<div class="in-blk pull-right mar-r-10 top-enquiry-form-party-bag" @click="showShortListedOptions">
					<i class="material-icons">shopping_basket</i>
					<span class="partybag-badge">@{{ shortlistCount }}</span>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="workflow-screens-hr"></div>
		</workflow-top-nav>
		<workflow-body-nav>
			<form id="enquiryForm" action="{{ "" }}">
				<div class="sticky-error-wrap sticky-error-wrap-enquiry hide pad-t-20">
					<span class="error-message">Please fill all the required details.</span>
				</div>
				<div class="form-group pad-t-20 pad-l-15 pad-r-15">
					<div class="col-xs-12">
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
							<input class="mdl-textfield__input text-normal" type="text" name="enquiryName" v-model="screen6name" id="enquiryName" value=""/>
							<label class="mdl-textfield__label text-normal" for="enquiryName">Name</label>
						</div>
					</div>
					<div class="col-xs-12">
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
							<input class="mdl-textfield__input text-normal" type="text" name="enquiryPhone" v-model="screen6phone" id="enquiryPhone" value=""/>
							<label class="mdl-textfield__label text-normal" for="enquiryPhone">Phone Number</label>
						</div>
					</div>
					<div class="col-xs-12">
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
							<input class="mdl-textfield__input text-normal" type="text" name="enquiryEmail" v-model="screen6email" id="enquiryEmail" value=""/>
							<label class="mdl-textfield__label text-normal" for="enquiryEmail">Email</label>
						</div>
					</div>
					<div class="col-xs-12">
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
							<textarea class="mdl-textfield__input text-normal" name="comment" id="comment" v-model="screen6comment"></textarea>
							<label class="mdl-textfield__label text-normal mar-b-10" for="comment">Your Requirements
								<small> (Share more details to help you better)</small>
							</label>
						</div>
					</div>
					<div class="col-xs-12">
						<label for="workflowEnquiryPlanStatus">When are you planning to book?</label>
						<select id="workflowEnquiryPlanStatus" class="form-control" v-model="screen6bookingLikeliness">
							<option value="-1">Select an option</option>
							<option value="1">Immediately</option>
							<option value="2">In a week</option>
							<option value="3">In a month</option>
							<option value="4">Later</option>
							<option value="5">Just Browsing</option>
						</select>
					</div>
					<div class="col-xs-12 mar-t-20">
						<label for="workflowEnquiryTimeSlot">When should we reach you?</label>
						<select id="workflowEnquiryTimeSlot" class="form-control" v-model="screen6timeSlot">
							<option value="-1">Select an option</option>
						</select>
						<div class="hide" id="workflowEnquiryTimeSlotOptions">
							<option value="Immediate" data-min-time="36000" data-max-time="72000">In One Hour
							</option>
							<option value="Today Morning(10 AM - 1 PM)" data-min-time="0"
									data-max-time="36000">Today Morning (10 AM - 1 PM)
							</option>
							<option value="Today Afternoon(1 PM - 3 PM)" data-min-time="0"
									data-max-time="46800">Today Afternoon (1 PM - 3 PM)
							</option>
							<option value="Today Evening(3 PM - 6 PM)" data-min-time="0"
									data-max-time="54000">Today Evening (3 PM - 6 PM)
							</option>
							<option value="Today Night(6 PM - 8 PM)" data-min-time="0"
									data-max-time="64800">Today Night (6 PM - 8 PM)
							</option>
							<option value="Tomorrow Morning(10 AM - 1 PM)" data-min-time="46800"
									data-max-time="86399">Tomorrow Morning (10 AM - 1 PM)
							</option>
							<option value="Tomorrow Afternoon(1 PM - 3 PM)" data-min-time="54000"
									data-max-time="86399">Tomorrow Afternoon (1 PM - 3 PM)
							</option>
							<option value="Tomorrow Evening(3 PM - 6 PM)" data-min-time="64800"
									data-max-time="86399">Tomorrow Evening (3 PM - 6 PM)
							</option>
							<option value="Tomorrow Night(6 PM - 8 PM)" data-min-time="72000"
									data-max-time="86399">Tomorrow Night (6 PM - 8 PM)
							</option>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</form>
		</workflow-body-nav>
		<workflow-bottom-nav class="bottom-navigation full-width">
			<div class="workflow-screens-hr"></div>
			<div class="bottom-navigation-submit-button text-center" @click="submitEnquiry">
				SUBMIT
			</div>
		</workflow-bottom-nav>
	</div>
	<div class="workflow-shortlist-screen workflow-shortlist-screen-mob hide">
		<workflow-top-nav>
			<div class="workflow-screen6-top-nav text-center">
				<span class="pull-left mar-l-10" @click="closeShortlistNow">
					<i class="material-icons">keyboard_backspace</i>
				</span>
				<div class="in-blk">
					<h4>Shortlisted Options</h4>
				</div>
				<div class="in-blk pull-right mar-r-10 top-enquiry-form-party-bag">
					<i class="material-icons">shopping_basket</i>
					<span class="partybag-badge">@{{ shortlistCount }}</span>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="workflow-screens-hr"></div>
		</workflow-top-nav>
		<workflow-body-nav>
			<div class="workflow-screen7-options-wrap">
				<div class="workflow-screen7-loading">Getting your shortlisted options</div>
				<div v-for="(services, index) in screen7ShortlistedOptions" class="mar-b-10 workflow-screen7-options hide">
					<div class="col-xs-6 pad-l-10 pad-r-10" v-for="option in services.options" v-if="option.isSelected === 1">
						<div class="list-option-card mar-t-15 text-center">
							<div class="list-option-image-wrap text-left">
								<a class="list-option-link" :href="option.url" target="_blank">
									<img class="list-option-image" :src="option.profilePic" :alt="option.name" onError="this.src='{{ $galleryUrl }}/img/icons/gift.png'; this.style='padding: 10px 10px'">
								</a>
							</div>
							<div class="list-option-content-wrap text-center">
								<a class="list-option-title" :href="option.url" target="_blank">
									@{{ option.name }}
								</a>
								<div class="list-option-price">
									<div class="list-price-worth in-blk mar-r-5 text-muted" v-if="option.priceWorth > option.minPrice">
										<strike><span class='rupee-font'>&#8377; </span> @{{ formatPrice(option.priceWorth) }}</strike>
									</div>
									<div class="list-main-price in-blk">
										<span class='rupee-font'>&#8377; </span> @{{ formatPrice(option.minPrice) }}
										<span v-if="option.maxPrice > option.minPrice"> - <span class='rupee-font'>&#8377; </span>@{{ formatPrice(option.maxPrice) }}</span>
									</div>
								</div>
							</div>
							<div class="text-center" :class="{'workflow-screen5-shortlist-btn': option.isSelected == 0, 'workflow-screen5-remove-btn': option.isSelected == 1, 'shortlist-initialize-introjs-button': shortlistInitializeButton == 1}">
								<span v-if="option.isSelected === 1" @click="removeScreen7ShortlistedOption(index, option.id)">
									<i class="glyphicon glyphicon-remove"></i> Remove
								</span>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</workflow-body-nav>
		<workflow-bottom-nav class="bottom-navigation full-width">
			<div class="workflow-screens-hr"></div>
			<button class="bottom-navigation-enquire-button text-center" @click="enquireNow">
				<span>ENQUIRE NOW</span>
			</button>
		</workflow-bottom-nav>
	</div>
	<input type="hidden" id="workflowCurrentScreen" value="options">
	<input type="hidden" id="userCityId" value="{{ getCityId() }}">
	<input type="hidden" id="workflowShortlistUrl" value="{{ route("city.workflow.options.shortlist", [getCityUrl(), $data["searchId"]]) }}">
	<input type="hidden" id="workflowShortlistedOptionsUrl" value="{{ route("city.workflow.options.shortlist.list", [getCityUrl(), $data["searchId"]]) }}">
	<input type="hidden" id="workflowEnquireUrl" value="{{ route("city.workflow.options.enquire", [getCityUrl(), $data["searchId"]]) }}">
	<input type="hidden" id="workflowRemoveShortlistUrl" value="{{ route("city.workflow.options.delete", [getCityUrl(), $data["searchId"]]) }}">
	<input type="hidden" id="workflowDataUrl" value="{{ route("city.workflow.options.data", [getCityUrl(), $data["searchId"]]) }}">
@endsection