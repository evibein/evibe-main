@extends("vue-js.workflow.base")

@section("content")
	<div class="workflow-loading-screen">
		<div>
			<img src="{{ config("evibe.gallery.host")."/img/gifs/evibe_services.gif" }}" width="100" height="100">
		</div>
	</div>
	<div class="kids-events-workflow-wrap hide">
		<workflow-top-nav>
			<div class="col-sm-6">
				<div class="plan-progress-wrap">
					<div class="inliner"></div>
					<div class="inlined">
						<div class="progress-meter">
							<div class="track">
								<span class="progress"></span>
							</div>
							<ol class="progress-points">
								<li class="progress-point active" @click="progressPoint(1)">
									<div>Event</div>
									<div>Type</div>
								</li>
								<li class="progress-point" @click="progressPoint(2)">
									<div>Event</div>
									<div>Details</div>
								</li>
								<li class="progress-point" @click="progressPoint(3)">
									<div>Services</div>
									<div>Required</div>
								</li>
								<li class="progress-point" @click="progressPoint(4)">
									<div>Service</div>
									<div>Details</div>
								</li>
							</ol>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 mar-t-10">
				<div class="in-blk font-24 pull-right workflow-home-button">
					<i class="material-icons">home</i>
				</div>
			</div>
			<div class="clearfix"></div>
		</workflow-top-nav>
		<workflow-body-nav class="mar-l-20">
			<div class="workflow-screen1" v-if="currentScreen === 1">
				<div class="pad-l-10">
					<h5 class="font-16 text-bold" v-text="screen1.question"></h5>
				</div>
				<div class="pad-l-10" v-for="(option, index) in screen1.options">
					<input type="radio" :id="labelScreen1(option.eventId)" class="custom-radio-animation" v-model="occasion" name="occasion" :value="option.eventId">
					<label :for="labelScreen1(option.eventId)" class="custom-radio-animation-label" v-text="option.eventName"></label>
					<br>
				</div>
			</div>
			<div class="workflow-screen2" v-if="currentScreen === 2">
				<div v-for="(question, index) in screen2">
					<div class="pad-l-10">
						<h5 class="font-16 mar-t-20 text-bold" v-text="question.question"></h5>
					</div>
					<div v-if="question.optionTypeId === 2">
						<div v-for="(option, optionIndex) in question.options" class="in-blk">
							<input type="radio" :id="labelScreen2(index, optionIndex)" class="custom-radio-animation" v-model="form.parent_id[index]" :name="index" :value="optionIndex">
							<label :for="labelScreen2(index, optionIndex)" class="custom-radio-animation-label mar-l-10" v-text="option"></label>
						</div>
					</div>
					<div class="pad-b-10" v-else-if="question.optionTypeId === 3">
						<div class="pad-l-10 pad-r-10">
							<select :name="index" class="form-control" v-model="form.parent_id[index]">
								<option disabled value="" selected="selected">Please select one option</option>
								<option v-for="(option, optionIndex) in question.options" :value="optionIndex" v-text="option"></option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="workflow-screen3" v-if="currentScreen === 3">
				<div class="pad-l-10">
					<h5 class="font-16 text-bold" v-text="screen3.question"></h5>
				</div>
				<div class="pad-l-10" v-for="(option, index) in screen3.options">
					<input type="checkbox" :id="labelScreen3(option.serviceId)" class="custom-checkbox-animation" v-model="services" name="services" :value="option.serviceId">
					<label :for="labelScreen3(option.serviceId)" class="custom-checkbox-animation-label" v-text="option.serviceName"></label>
					<br>
				</div>
			</div>
			<div class="workflow-screen4" v-if="currentScreen === 4">
				<div class="workflow-screen4-service-details workflow-screen4-service-details-des">
					<div class="pad-l-10" v-for="(option, index) in screen4.options" v-if="inScreen3(option.id)">
						<div class="service-header-name text-bold">What kind of @{{ option.name }} do you need?</div>
						<div class="pad-l-10 pad-b-10" v-for="(subOption, subOptionIndex) in option.subOptions">
							<input type="checkbox" :id="labelScreen4(subOptionIndex)" class="custom-checkbox-animation" v-model="serviceDetails" :name="getServiceDetailsName(index)" :value="subOptionIndex">
							<label :for="labelScreen4(subOptionIndex)" class="custom-checkbox-animation-label">
								<span>@{{ subOption.name }} <br></span>
							</label>
							<div class="text-muted category-description">
								<span v-if="subOption.min == 0">Starting from
									<span class='rupee-font'>&#8377;</span> @{{ subOption.min }}
									<span v-if="subOption.max > 0"> to
										<span class='rupee-font'>&#8377;</span>@{{ subOption.max }}
									</span>
									<span v-else></span>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</workflow-body-nav>
		<workflow-bottom-nav class="bottom-navigation-des full-width">
			<div class="workflow-screens-hr"></div>
			<div class="bottom-navigation-back-button-des text-center" @click="back" v-if="currentScreen > 1">
				<i class="material-icons">keyboard_backspace</i>
				<span> BACK</span>
			</div>
			<button class="bottom-navigation-next-button-des text-center green-bg" v-if="currentScreen === 4" @click="submitPlan">
				<span>SHOW OPTIONS</span>
			</button>
			<button class="bottom-navigation-next-button-des text-center" v-else @click="next">
				<span>NEXT</span>
				<i class="material-icons">arrow_right_alt</i>
			</button>
		</workflow-bottom-nav>
		<div class="clearfix"></div>
	</div>
	<input type="hidden" id="workflowCurrentScreen" value="requirements">
	<input type="hidden" id="userCityId" value="{{ getCityId() }}">
	<input type="hidden" id="workflowDataUrl" value="{{ route("city.workflow.home.data", [getCityUrl(), $data["planToken"]]) }}">
	<input type="hidden" id="workflowDataSubmitUrl" value="{{ route("city.workflow.save", [getCityUrl()]) }}">
	<input type="hidden" id="dataPlanToken" value="{{ $data["planToken"] }}">
@endsection