@extends("refer-earn.base")

@section('page-title')
	<title>Your Refer & Earn Dashboard - Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="Refer and earn program registration"/>
@endsection

@section("content")
	<div class="referral-customer-statistics-wrap">
		<div class="text-center pad-10">
			<a href="/">
				<img src="{{ $galleryUrl }}/main/pages/refer_earn/logo_evibe.png" alt="Evibe logo" height="80px">
			</a>
		</div>
		<div class="text-center col-xs-10 col-xs-offset-1 referral-customer-statistics-share-wrap">
			<div class="col-sm-6 col-xs-12 referral-customer-statistics-share mar-t-30">
				<img src="{{ $galleryUrl }}/main/pages/refer_earn/gift-box.png" width="200" height="130">
				<div class="text-center">
					<div>
						<h3 class="text-muted">Refer Friends. Get Rewards.</h3>
						<h4 class="text-muted mar-b-30">Give your friends &#8377;200* OFF on sign up. You get &#8377;200* OFF when your they book with Evibe.in.</h4>
						<div>
							<div>
								<div class="referral-copy-btn-text">
									{{ $data["referralUrl"] }}
								</div>
								<button type="button" class="referral-copy-btn">
									Copy Link
								</button>
							</div>
							<div class="copy-notification"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-xs-12 mar-t-30">
				<div class="col-md-6 col-sm-12 mar-t-30">
					<div class="referral-customer-statistics-metric-wrap">
						<div class="pad-b-10">Friends Signed Up</div>
						<span>{{ $data["statistics"]["signUpCount"] }}</span>
					</div>
				</div>
				<div class="col-md-6 col-sm-12 mar-t-30">
					<div class="referral-customer-statistics-metric-wrap">
						<div class="pad-b-10">Friends Booked</div>
						<span>{{ $data["statistics"]["bookedCount"] }}</span>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="referral-customer-statistics-coupon-wrap">
					<div class="pad-b-20">Coupon Code Available</div>
					@if(count($data["coupons"]) > 0)
						@foreach($data["coupons"] as $coupon)
							<div class="coupon">{{ $coupon["coupon_code"] }}
								<div class="text-e mar-t-10 font-16">
									&#8377;200* OFF
								</div>
								<small class="text-muted text-normal font-13">*valid upto {{ date('d M Y', strtotime($coupon["offer_end_time"])) }}, Minimum order Rs. 2,000, all T&C apply</small>
							</div>
						@endforeach
					@else
						<span>No Coupon Codes!</span>
					@endif
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
		<div class="text-center col-xs-10 col-xs-offset-1 referral-customer-statistics-how-it-works-wrap mar-b-15 pad-b-15">
			@include("refer-earn.how-it-works")
		</div>
		<div class="clearfix"></div>
		<div style="height: 200px;"></div>
	</div>
	<div style="bottom: 0; box-shadow: 0 -3px 20px 0 #757575; background-color: white; width: 100%; position: fixed">
		<div class="text-center">
			<h5 class="text-muted">Share on social media</h5>
			<div class="mar-l-5 mar-r-5 mar-b-5 in-blk">
				@if ($agent->isMobile() && !($agent->isTablet()))
					<a id="referralMobWhatsappShare" href="whatsapp://send?text=I found Evibe.in to be the best party planners. Make your next event planning effortless and stress-free with Evibe. Register now with Evibe.in to get Rs 200* OFF on your next party booking. Click {{ $data["referralUrl"] }}?src=whatsapp" data-source-id="{{ config("evibe.referral.source.whatsapp") }}" class="referral-share-btns" data-value="whatsapp" target="_blank"><img src="{{ $galleryUrl }}/main/pages/refer_earn/whatsapp.png" width="70" height="70"></a>
					<a id="referralMobMessengerShare" href="fb-messenger://share?app_id=521736568283788&link={{ $data["referralUrl"] }}?src=messenger" class="mar-l-10 referral-share-btns" data-source-id="{{ config("evibe.referral.source.messenger") }}" data-value="messenger" target="_blank"><img src="{{ $galleryUrl }}/main/pages/refer_earn/messenger.png" width="70" height="70"></a>
				@else
					<a id="referralDesWhatsappShare" href="https://api.whatsapp.com/send?text=I found Evibe.in to be the best party planners. Make your next event planning effortless and stress-free with Evibe. Register now with Evibe.in to get Rs 200* OFF on your next party booking. Click  {{ $data["referralUrl"] }}?src=whatsapp" data-source-id="{{ config("evibe.referral.source.whatsapp") }}" class="referral-share-btns" data-value="whatsapp" target="_blank"><img src="{{ $galleryUrl }}/main/pages/refer_earn/whatsapp.png" width="70" height="70"></a>
					<a id="referralDesMessengerShare" href="https://www.facebook.com/v2.9/dialog/send?app_id=521736568283788&link={{ $data["referralUrl"] }}&src=messenger&redirect_uri=https://evibe.in" data-source-id="{{ config("evibe.referral.source.messenger") }}" class="mar-l-10 referral-share-btns" data-value="messenger" target="_blank"><img src="{{ $galleryUrl }}/main/pages/refer_earn/messenger.png" width="70" height="70"></a>
				@endif
			</div>
			<div class="mar-l-5 mar-r-5 mar-b-5 in-blk">
				@if ($agent->isMobile() && !($agent->isTablet()))
					<a id="referralMobSMSShare" href="sms:?&body=I+found+Evibe.in+to+be+the+best+party+planners.+Make+your+next+event+planning+effortless+and+stress-free+with+Evibe.+Register+now+with+Evibe.in+to+get+Rs+200*+OFF+on+your+next+party+booking.+Click+{{ $data["referralUrl"] }}?src=sms" data-source-id="{{ config("evibe.referral.source.sms") }}" class="referral-share-btns" data-value="sms" target="_blank"><img src="{{ $galleryUrl }}/main/pages/refer_earn/sms.png" width="70" height="70"></a>
				@else
					<a id="referralDesSMSShare" href="sms:?&body=I found Evibe.in to be the best party planners. Make your next event planning effortless and stress-free with Evibe. Register now with Evibe.in to get Rs 200* OFF on your next party booking. Click {{ $data["referralUrl"] }}?src=sms" data-source-id="{{ config("evibe.referral.source.sms") }}" class="referral-share-btns" data-value="sms" target="_blank"><img src="{{ $galleryUrl }}/main/pages/refer_earn/sms.png" width="70" height="70"></a>
				@endif
				<a id="referralEmailShare" href="mailto:?subject=Get Rs 200* OFF at Evibe.in&body=I found Evibe.in to be the best party planners. Register now with Evibe.in to get Rs 200* OFF on your next party booking. Click {{ $data["referralUrl"] }}?src=email" data-source-id="{{ config("evibe.referral.source.email") }}" class="mar-l-10 referral-share-btns" data-value="email" target="_blank"><img src="{{ $galleryUrl }}/main/pages/refer_earn/email.png" width="70" height="70"></a>
			</div>
			<h6 class="text-muted">You'll see a preview before you post</h6>
		</div>
	</div>
	<input type="hidden" id="friendReferralSource" value="{{ request("id") }}">
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			(function customerShareButton() {
				$(".referral-share-btns").on("click", function () {
					let userId = $("#friendReferralSource").val();
					if (userId > 0) {
						$.ajax({
							url: "/re/update/share/" + userId + "/" + $(this).data("value"),
							type: 'post',
							dataType: 'json'
						}).done(function (data) {
							if (!data.success) {
								showNotyError("Some error occurred while loading your data, Please reload and try again.")
							}
						}).fail(function (jqXHR, textStatus, errorThrown) {
							window.notifyTeam({
								"url": "/re/update/share/" + userId + "/" + $(this).data("value"),
								"textStatus": textStatus + ", errorThrown: " + errorThrown,
								"errorThrown": $.parseJSON(jqXHR.responseText)
							});
						});
					}
				});
			})();

			(function customerCopyButton() {
				$(".referral-copy-btn").on("click", function (event) {
					event.preventDefault();
					let $temp = $("<input>");
					$("body").append($temp);
					$temp.val($(".referral-copy-btn-text").text()).select();
					document.execCommand("copy");
					$temp.remove();

					let copyNotification = $("div.copy-notification");
					copyNotification.text("Copied!").fadeIn("slow", function () {
						setTimeout(function () {
							copyNotification.fadeOut("slow", function () {
								copyNotification.empty();
							});
						}, 2000);
					});
				});
			})();

			(function emailTriggers() {
				var pageUrl = window.location.href;
				if (pageUrl.indexOf('#EmailWhatsapp') !== -1) {
					$('#referralDesWhatsappShare').click();
					$('#referralMobWhatsappShare').click();
				} else if (pageUrl.indexOf('#EmailMessenger') !== -1) {
					$('#referralDesMessengerShare').click();
					$('#referralMobMessengerShare').click();
				} else if (pageUrl.indexOf('#EmailSMS') !== -1) {
					$('#referralDesSMSShare').click();
					$('#referralMobSMSShare').click();
				}
			})();
		})
	</script>
@endsection