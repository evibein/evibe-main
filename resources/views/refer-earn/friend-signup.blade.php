@extends("refer-earn.base")

@section('page-title')
	<title>Register Now | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="Refer and earn program registration"/>
@endsection

@section("content")
	<div class="referral-friend-signup-wrap">
		<div class="text-center pad-10">
			<a href="javascript:void(0)">
				<img src="{{ $galleryUrl }}/main/pages/refer_earn/logo_evibe.png" alt="Evibe logo" height="80px">
			</a>
		</div>
		<div class="referral-friend-signup-hero-slider">
			<div class="pos-rel">
				<ul class="rslides">
					<li>
						<div class="top-slide top-slide-home" style="background-image:url({{ $galleryUrl }}/main/img/home/bg-1.jpg)"></div>
					</li>
					<li>
						<div class="top-slide top-slide-home" style="background-image:url({{ $galleryUrl }}/main/img/home/bg-2.jpg)"></div>
					</li>
					<li>
						<div class="top-slide top-slide-home" style="background-image:url({{ $galleryUrl }}/main/img/home/bg-3.jpg)"></div>
					</li>
					<li>
						<div class="top-slide top-slide-home" style="background-image:url({{ $galleryUrl }}/main/img/home/bg-4.jpg)"></div>
					</li>
					<li>
						<div class="top-slide top-slide-home" style="background-image:url({{ $galleryUrl }}/main/img/home/bg-7.jpg)"></div>
					</li>
				</ul>
			</div>
			<div class="bg-overlay"></div>
			<div class="intro-message">
				<h1 class="text-bg">Plan Your Next Event Effortlessly!</h1>
				<div class="lines-2">
					<h4>Birthdays | Surprises | House Warming | Weddings | Youth Parties | Corporate</h4>
				</div>
				<div class="text-center mar-t-20">
					<button class="btn btn-evibe font-18 pad-10 mar-t-20 referral-signup-btn">SIGN UP NOW & GET &#8377;200* OFF</button>
				</div>
			</div>
		</div>
		@include('base.home.why-us', ['containerPadding' => 'pad-t-30 pad-b-30'])
		<div class="referral-friend-signup-form">
			<div class="col-sm-8 col-xs-12 col-sm-offset-2 no-pad-l no-pad-r">
				<div class="text-center pad-l-20 pad-r-20">
					<h2 class="text-muted re-register-form-title">Register & get &#8377;200* OFF!</h2>
				</div>
				<form id="friendReferralSignUpForm" action="{{ route("re.friend.signup.submit") }}">
					<div class="sticky-error-wrap sticky-error-wrap-enquiry hide pad-t-20">
						<span class="error-message">Please fill all the required details.</span>
					</div>
					<div class="col-xs-12 col-sm-8 col-sm-offset-2">
						<div class="form-group pad-t-20 text-center">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="referralEnquireFirstName" id="referralEnquireFirstName" value=""/>
								<label class="mdl-textfield__label" for="referralEnquireFirstName">Name</label>
							</div>
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="referralEnquirePhone" id="referralEnquirePhone" value=""/>
								<label class="mdl-textfield__label" for="referralEnquirePhone">Phone Number</label>
							</div>
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="referralEnquireEmail" id="referralEnquireEmail" value=""/>
								<label class="mdl-textfield__label" for="referralEnquireEmail">Email</label>
							</div>
						</div>
						<div class="text-center">
							<label class="referral-accepts-surprise">
								<input id="acceptsForm" type="checkbox" value="1">
								<span class="mar-l-5">I would like to recieve surprise on my special days</span>
							</label>
						</div>
						<div class="form-group pos-rel referral-special-occasion-section-wrap hide">
							<div class="col-xs-12">
								<div class="mar-b-10 mar-t-20">
									<div class="mar-b-5">
										<img class="special-date-image in-blk valign-mid mar-r-5" src="{{ $galleryUrl }}/main/pages/refer_earn/special_date.png" alt="Evibe logo" height="50px">
										<h4 class="no-mar-b no-mar-t in-blk valign-mid spl-date-title">Add your special dates</h4>
									</div>
									<h6 class="no-mar-t text-muted">We will send a special surprise for you 😍🎉</h6>
								</div>
								<div class="special-occasions-section">
									<div class="special-occasion-wrap">
										<div class="col-md-4 col-sm-12 no-pad-r no-pad-l">
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<select class="mdl-textfield__input mar-t-8" id="referralOccasion" name="referralOccasion">
													@foreach($data["specialDates"] as $key => $specialDate)
														<option value="{{ $key }}">{{ $specialDate }}</option>
													@endforeach
												</select>
												<label class="mdl-textfield__label" for="referralOccasion">Occasion Type</label>
											</div>
										</div>
										<div class="col-md-4 col-sm-12 no-pad-r">
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<select class="mdl-textfield__input mar-t-8" id="referralRelation" name="referralRelation">
													@foreach($data["relations"] as $key => $relation)
														<option value="{{ $key }}">{{ $relation }}</option>
													@endforeach
												</select>
												<label class="mdl-textfield__label" for="referralRelation">Occasion Of</label>
											</div>
										</div>
										<div class="col-md-4 col-sm-12 no-pad-r">
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<input class="mdl-textfield__input friend-signUp-date" type="text" name="referralDate" id="referralDate" value=""/>
												<label class="mdl-textfield__label" for="referralDate">Date</label>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
								<div class="text-center">
									<a class="btn btn-primary add-more-special-dates">
										<i class="glyphicon glyphicon-plus"></i> Add More
									</a>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="form-group pad-b-20">
							<div class="col-sm-12 mar-t-30 text-center">
								<div id="friendReferralSignUpSubmit" class="btn btn-evibe font-20 full-width">
									Register
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="clearfix"></div>
				</form>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="landing-body-section-customer-story pad-t-30">
			<div class="landing-customer-page-head">
				Why <span class="text-e">Customers Choose Evibe.in</span>?
			</div>
			<div class="pos-rel">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6 text-center">
					<div class="pad-t-15 pad-b-15">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/6lwEXs51qJw?html5=1"></iframe>
						</div>
					</div>
					<div class="pad-b-15">
						<div class="fb-like" data-href="https://www.facebook.com/evibe.in/" data-layout="button_count" data-action="like" data-size="large" data-show-faces="false" data-share="false">
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
					<ul class="landing-customer-benefits-list">
						<li class="landing-customer-list-option">Wide range of options</li>
						<li class="landing-customer-list-option">Best prices with quality service</li>
						<li class="landing-customer-list-option">Secure &amp; hassle free booking</li>
						<li class="landing-customer-list-option">Friendly customer support</li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="landing-body-section-stats">
			<div class="col-xs-3 text-center no-pad">
				<div class="landing-stats-text-1 text-e">{{ config("evibe.stats.parties_done") }}</div>
				<div class="landing-stats-text-2">Parties Booked</div>
			</div>
			<div class="col-xs-3 text-center no-pad">
				<div class="landing-stats-text-1 text-e">5K+</div>
				<div class="landing-stats-text-2">Real Pictures</div>
			</div>
			<div class="col-xs-3 text-center no-pad">
				<div class="landing-stats-text-1 text-e">800K+</div>
				<div class="landing-stats-text-2">Balloons Blown</div>
			</div>
			<div class="col-xs-3 text-center no-pad">
				<div class="landing-stats-text-1 text-e">128K+</div>
				<div class="landing-stats-text-2">Social Media Followers</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="text-center">
			<button class="btn btn-evibe font-18 pad-10 mar-b-30 referral-signup-btn">SIGN UP NOW & GET &#8377;200* OFF</button>
		</div>
		<div class="landing-footer">
			<div class="col-xs-6 no-pad">
				<div class="landing-footer-left">
					<div class="landing-copy-text">© 2014 - {{ date('Y', time()) }} Evibe Technologies Pvt. Ltd.</div>
				</div>
			</div>
			<div class="col-xs-6 no-pad">
				<div class="landing-footer-right text-right pad-r-20">
					Your Favourite Party Planner
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="special-occasion-wrap-hide hide">
		<hr class="special-occasion-wrap-hr">
		<div class="col-md-4 col-sm-12 no-pad-r no-pad-l">
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
				<select class="mdl-textfield__input" id="referralOccasion" name="referralOccasion">
					@foreach($data["specialDates"] as $key => $specialDate)
						<option value="{{ $key }}">{{ $specialDate }}</option>
					@endforeach
				</select>
				<label class="mdl-textfield__label" for="referralOccasion">Occasion Type</label>
			</div>
		</div>
		<div class="col-md-4 col-sm-12 no-pad-r">
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
				<select class="mdl-textfield__input" id="referralRelation" name="referralRelation">
					@foreach($data["relations"] as $key => $relation)
						<option value="{{ $key }}">{{ $relation }}</option>
					@endforeach
				</select>
				<label class="mdl-textfield__label" for="referralRelation">Occasion Of</label>
			</div>
		</div>
		<div class="col-md-4 col-sm-12 no-pad-r">
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
				<input class="mdl-textfield__input friend-signUp-date" type="text" name="referralDate" id="referralDate" value=""/>
				<label class="mdl-textfield__label" for="referralDate">Date</label>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<input type="hidden" id="friendReferralCode" value="{{ $data["referralCode"] }}">
@endsection

@section("javascript")
	<script type="text/javascript">
		$(document).ready(function () {
			(function initializations() {
				$(".rslides").responsiveSlides({
					speed: 500,
					timeout: 5000
				});

				$(".referral-signup-btn").on("click", function () {
					$('html, body').animate({
						scrollTop: $(".referral-friend-signup-form").offset().top
					}, 700);
				});

				$(".referral-accepts-surprise input, .referral-accepts-surprise span").on("click", function () {
					if ($("#acceptsForm:checked").val() == 1) {
						$(".referral-special-occasion-section-wrap").removeClass("hide");
					} else {
						$(".referral-special-occasion-section-wrap").addClass("hide");
					}
				});

				loadBdayInput();
			})();

			/* facebook like button */
			(function (d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) return;
				js = d.createElement(s);
				js.id = id;
				js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));

			(function addMoreSpecialDates() {
				$(".add-more-special-dates").on("click", function () {
					let clone = $(".special-occasion-wrap-hide").clone(true, true).addClass("special-occasion-wrap").removeClass("special-occasion-wrap-hide").removeClass("hide");
					clone.find(".mdl-textfield").removeClass("is-upgraded").attr("data-upgraded", "");
					$(".special-occasions-section").append(clone);
					componentHandler.upgradeDom();
					loadBdayInput();
				})
			})();

			(function submitFriendSignUpData() {
				$("#friendReferralSignUpSubmit").on("click", function (event) {
					event.preventDefault();
					window.showLoading();

					let specialDates = [];

					$(".special-occasion-wrap").each(function () {
						if ($(this).find("#referralDate").val() !== '') {
							specialDates.push({
								occasion: $(this).find("#referralOccasion").val(),
								relation: $(this).find("#referralRelation").val(),
								date: $(this).find("#referralDate").val()
							})
						}
					});

					$.ajax({
						url: $("#friendReferralSignUpForm").attr("action"),
						type: 'post',
						dataType: 'json',
						data: {
							"firstName": $("#referralEnquireFirstName").val(),
							"lastName": $("#referralEnquireLastName").val(),
							"phone": $("#referralEnquirePhone").val(),
							"email": $("#referralEnquireEmail").val(),
							"referralCode": $("#friendReferralCode").val(),
							"specialDates": specialDates
						}
					}).done(function (data) {
						hideLoading();
						if (data.success) {
							window.location.replace(data.res["redirectUrl"]);
						} else {
							showNotyError(data.res["error"])
						}
					}).fail(function (jqXHR, textStatus, errorThrown) {
						hideLoading();
						window.notifyTeam({
							"url": $("#referralGetUniqueLink").val(),
							"textStatus": textStatus + ", errorThrown: " + errorThrown,
							"errorThrown": $.parseJSON(jqXHR.responseText)
						});
					});
				});
			})();

			/* First destroy all the previous loaded date time pickers, then reapply all */
			function loadBdayInput() {
				$(".friend-signUp-date")
					.datetimepicker('destroy')
					.datetimepicker({
						timepicker: false,
						format: 'd/m',
						scrollInput: false,
						scrollMonth: false,
						scrollTime: false,
						closeOnDateSelect: true,
						onSelectDate: function (ct, $i) {
							$i.parent().addClass('is-dirty');
						}
					});

				/*Hiding the year manually, there is no option  provided by the creator*/
				$(".xdsoft_year").addClass("hide");
			}
		});
	</script>
@endsection