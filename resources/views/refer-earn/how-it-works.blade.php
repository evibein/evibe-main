<div class="pad-t-30">
	<h3 class="no-mar-t">How It Works</h3>
</div>
<div class="in-blk mar-30">
	<div style="min-height: 50px;">
		<img src="https://gallery.evibe.in/main/pages/refer_earn/share_link_icon.png" width="50">
	</div>
	<div class="mar-t-5">Share your link</div>
</div>
<div class="in-blk mar-30">
	<div style="min-height: 50px;">
		<img src="https://gallery.evibe.in/main/pages/refer_earn/shopping-icon.png" width="50">
	</div>
	<div class="mar-t-5">Your friend buys</div>
</div>
<div class="in-blk mar-30">
	<div style="min-height: 50px;">
		<img src="https://gallery.evibe.in/main/pages/refer_earn/reward-gift-icon.png" width="40">
	</div>
	<div class="mar-t-5">You get rewarded</div>
</div>
<div class="clearfix"></div>