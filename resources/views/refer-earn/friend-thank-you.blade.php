@extends("refer-earn.base")

@section('page-title')
	<title>Thank you for registering | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="Refer and earn program registration"/>
@endsection

@section("content")
	<div class="referral-friend-signup-wrap">
		<div class="text-center pad-10">
			<a href="/">
				<img src="{{ $galleryUrl }}/main/pages/refer_earn/logo_evibe.png" alt="Evibe logo" height="80px">
			</a>
		</div>
		<div class="referral-thank-you-wrap col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 text-center">
			<div class="pos-rel">
				<img class="mar-t-10" src="{{ $galleryUrl . "/main/pages/refer_earn/gift-box.png" }}" style="max-width:180px;">
			</div>
			<div class="text-muted mar-t-15 mar-b-15">
				<h2 class="no-mar-t no-mar-b mar-b-10 font-24">Thanks for signing up!</h2>
				<h4 class="no-mar-t no-mar-b font-18 text-normal">Use the coupon code below to get &#8377;200* OFF on your next party booking</h4>
			</div>
			<div class="mar-b-30">
				<div class="referral-thank-you-coupon">
					<h4 class="no-mar-t no-mar-b"><b>{{ $data["couponCode"] }}</b></h4>
					<div class="text-e mar-t-10 mar-b-10">
						&#8377;200* OFF
					</div>
					<small class="text-muted">*valid upto {{ date('d M Y', strtotime($data["couponExpiryDate"])) }}, Minimum order Rs. 2,000, all T&C apply</small>
				</div>
			</div>
			<button class="referral-thank-you-redirect-btn">
				<a href="https://evibe.in?utm_source=re-thank-you&utm_campaign=referearn&utm_medium=website" target="_blank" class="a-no-decoration">PLAN NOW</a>
			</button>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection