@extends('layout.base')

@section('page-title')
	<title>Thank You Card | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="Download your thank you card"/>
@endsection

@section('meta-keywords')
	<meta name="keywords" content="Evibe.in"/>
@endsection

@section('og-title')
	<meta property="og:title" content="Thank You Card | Evibe.in"/>
@endsection

@section('og-description')
	<meta property="og:description" content="Download your thank you card"/>
@endsection

@section('og-url')
	<meta property="og:url" content="https://evibe.in"/>
@endsection

@section('press:title')
@endsection

@section("header")
	@include("app.header_logo")
@endsection

@section("content")
	<div class="thank-you-card-container">
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-lg-offset-3 col-md-offset-3">
			<div class="border-right pad-10 mar-b-30">
				@if(isset($data['invalidAttempt']) && $data['invalidAttempt'])
					<div class="mar-t-30">
						<i class="glyphicon glyphicon-remove-sign text-r"></i> Sorry, you are not authorised to use this page.
						<span class="mar-l-2">However, if you think this is a mistake, reach us at {{ config('evibe.contact.customer.group') }}</span>
					</div>
				@elseif(isset($data['error']) && $data['error'])
					<div class="mar-t-30">
						<i class="glyphicon glyphicon-exclamation-sign text-r"></i> Sorry, something went wrong while fetching your thank you card.
						<span class="mar-l-2">Kindly refresh the page and try again. Still if the problem persists, please reach us at {{ config('evibe.contact.customer.group') }}</span>
					</div>
				@else
					<input type="hidden" id="tyTicketId" value="{{ $data['ticketId'] }}">
					<div class="tyc-header text-center mar-t-20">
						{{ $data['customerName'] }}, share the personalised "Thank You" card with your guests. Download the image and share it on WhatsApp / Facebook Messenger / Email.
					</div>
					<div class="tyc-body mar-t-20">
						<img id="tycImage" class="full-width" src="{{ $data['tycUrl'] }}">
					</div>
					<div class="tyc-cta mar-t-30 text-center">
						<div class="tyc-download-wrap">
							<form id="tycDownloadForm" action="{{ route('customer.tyc-image') }}?imagePath={{ $data['tycPath'] }}" method="POST">
								<button type="submit" id="tycDownloadBtn" class="btn btn-primary btn-tyc-download">DOWNLOAD THANK YOU CARD</button>
							</form>
							<div class="tyc-download-help-text mar-t-10">
								<span class="hide__400 hide-400-600">If the download doesn't work, kindly right click on the the image and save it.</span>
								<span class="hide unhide__400 unhide__400-600">If the download doesn't work, kindly long press on the image and download it.</span>
							</div>
						</div>
					</div>
				@endif
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			var tyTicketId = $("#tyTicketId").val();

			/* hide party bag */
			$('.partybag-wrap-mobile').addClass('hide');

			$('#tycDownloadBtn').click(function (event) {
				event.preventDefault();
				$('#tycDownloadForm').submit();
			});

		});
	</script>
@endsection