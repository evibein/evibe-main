@extends('layout.base')

@section('page-title')
	<title>Party Invitation | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="RSVP for the party invitation"/>
@endsection

@section('meta-keywords')
	<meta name="keywords" content="Evibe.in"/>
@endsection

@section('og-title')
	<meta property="og:title" content="Party Invitation | Evibe.in"/>
@endsection

@section('og-description')
	<meta property="og:description" content="RSVP for the party invitation"/>
@endsection

@section('og-url')
	<meta property="og:url" content="https://evibe.in"/>
@endsection

@section('press:title')
@endsection

@section("header")
	@include("app.header_cards")
@endsection

@section("content")
	<div class="invitation-container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
			<div class="pad-10 mar-b-30">
				@if(isset($data['invalidAttempt']) && $data['invalidAttempt'])
					<div class="mar-t-30">
						<i class="glyphicon glyphicon-remove-sign text-r"></i> Sorry, you are not authorised to use this page.
						<span class="mar-l-2">However, if you think this is a mistake, reach us at {{ config('evibe.contact.customer.group') }}</span>
					</div>
				@elseif(isset($data['error']) && $data['error'])
					<div class="mar-t-30">
						<i class="glyphicon glyphicon-exclamation-sign text-r"></i> Sorry, something went wrong while fetching your party invitation.
						<span class="mar-l-2">Kindly refresh the page and try again. Still if the problem persists, please reach us at {{ config('evibe.contact.customer.group') }}</span>
					</div>
				@else
					<input type="hidden" id="hidTicketId" value="{{ $data['ticketId'] }}">
					<div class="col-sm-12 col-xs-12 @if(isset($data['postPartyRSVP']) && $data['postPartyRSVP']) col-lg-6 col-md-6 col-lg-offset-3 col-md-offset-3 @else col-md-7 col-lg-7 @endif">
						<div class="invite-img-wrap mar-t-20">
							<img id="inviteImage" class="full-width" src="{{ $data['inviteImageUrl'] }}">
						</div>
						<div class="invite-download-wrap mar-t-20 text-center">
							<form id="inviteDownloadForm" action="{{ route('iv.image') }}?imagePath={{ $data['inviteImagePath'] }}" method="POST">
								<button type="submit" id="inviteDownloadBtn" class="btn btn-primary">Download Invitation</button>
							</form>
							<div class="tyc-download-help-text mar-t-10">
								<span class="hide__400 hide-400-600">If the download doesn't work, kindly right click on the the image and save it.</span>
								<span class="hide unhide__400 unhide__400-600">If the download doesn't work, kindly long press on the image and download it.</span>
							</div>
						</div>
					</div>
					@if(isset($data['postPartyRSVP']) && $data['postPartyRSVP'])
					@else
						<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
							<div class="invite-form-container-wrap">
								@if(isset($data['ticketRSVPs']) && count($data['ticketRSVPs']))
								@else
									<div class="text-left">
										Be the first to RSVP to this party invitation...
									</div>
								@endif
								<div class="invite-form-wrap">
									<div class="form-group">
										<div class="invite-form-head text-center">
											<b>RSVP Your Availability</b>
										</div>
										<div class="invite-form-body mar-t-15">
											@if(isset($data['rsvpOptions']) && count($data['rsvpOptions']))
												<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 col-md-offset-3 col-lg-offset-3 no-pad-l mar-t-20-400-600 mar-t-20__400 mar-b-10">
													<label for="rsvpStatus" class="custom-mdl-label">Will you attend?</label>
													<select class="form-control" id="rsvpStatus" name="rsvpStatus">
														<option value="-1">Select an option</option>
														@foreach($data['rsvpOptions'] as $rsvpOption)
															<option value="{{ $rsvpOption->id }}">{{ $rsvpOption->name }}</option>
														@endforeach
													</select>
												</div>
											@endif
											<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 col-md-offset-3 col-lg-offset-3 no-pad-l">
												<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
													<input class="mdl-textfield__input mar-b-5" type="text" name="name" id="name" value=""/>
													<label class="mdl-textfield__label" for="name">Name</label>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 col-md-offset-3 col-lg-offset-3 no-pad-l">
												<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
													<input class="mdl-textfield__input mar-b-5" type="text" name="phone" id="phone" value=""/>
													<label class="mdl-textfield__label" for="phone">Phone Number</label>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 col-md-offset-3 col-lg-offset-3 no-pad-l hide">
												<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
													<input class="mdl-textfield__input mar-b-5" type="text" name="email" id="email" value=""/>
													<label class="mdl-textfield__label" for="email">Email</label>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 col-md-offset-3 col-lg-offset-3 no-pad-l mar-t-20">
												<label for="comments" class="custom-mdl-label">Blessings
													<small> (optional)</small>
												</label>
												<textarea id="comments" name="comments" class="form-control" placeholder="Write Your blessings/wishes"></textarea>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 col-md-offset-3 col-lg-offset-3 no-pad-l mar-t-20 text-center">
												<div class="btn btn-evibe font-16 mar-t-20" id="btnInviteSubmit" data-url="{{ route("iv.rsvp") }}">
													<span>SUBMIT</span>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					@endif
					<div class="clearfix"></div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-lg-offset-3 col-md-offset-3">

						<div class="invite-rsvp-status-wrap">
							<div class="invite-rsvp-head">
								@if(isset($data['ticketRSVPs']) && count($data['ticketRSVPs']))
									<b>{{ count($data['ticketRSVPs']) }} RSVP responses so far...</b>
								@else
									<div class="text-center">
										@if(isset($data['postPartyRSVP']) && $data['postPartyRSVP'])
											<div class="invite-thank-you-wrap">
												Thank you for your blessings and making this party a grand success
											</div>
										@else
										@endif
									</div>
								@endif
							</div>
							@if(isset($data['ticketRSVPs']) && count($data['ticketRSVPs']))
								<div class="invite-rsvp-body mar-t-15">
									<div class="scrollmenu invite-rsvp-tabs">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 no-pad">
											<a class="scrollmenu-element full-width" href="#yes" data-toggle="tab">Yes ({{ $data['rsvpYesCount'] }})</a>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 no-pad">
											<a class="scrollmenu-element full-width" href="#mayBe" data-toggle="tab">May Be ({{ $data['rsvpMayBeCount'] }})</a>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 no-pad">
											<a class="scrollmenu-element full-width" href="#no" data-toggle="tab">No ({{ $data['rsvpNoCount'] }})</a>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="invite-rsvp-tabs-content">
										<div class="tab-content" id="tabs">
											<div class="tab-pane mar-10" id="yes">
												@foreach($data['ticketRSVPs'] as $ticketRSVP)
													@if($ticketRSVP->type_rsvp_id == config('evibe.type-rsvp.yes'))
														<div class="invite-rsvp-response">
															<div class="text-bold">{{ $ticketRSVP->name }}</div>
															@if($ticketRSVP->comments)
																<div class="mar-l-20">
																	<span class="glyphicon glyphicon-comment valign-mid"></span><span class="valign-mid">{{ $ticketRSVP->comments }}</span>
																</div>
															@endif
														</div>
													@endif
												@endforeach
											</div>
											<div class="tab-pane mar-10" id="mayBe">
												@foreach($data['ticketRSVPs'] as $ticketRSVP)
													@if($ticketRSVP->type_rsvp_id == config('evibe.type-rsvp.may-be'))
														<div class="invite-rsvp-response">
															<div class="text-bold">{{ $ticketRSVP->name }}</div>
															@if($ticketRSVP->comments)
																<div class="mar-l-20">
																	<span class="glyphicon glyphicon-comment valign-mid"></span><span class="valign-mid">{{ $ticketRSVP->comments }}</span>
																</div>
															@endif
														</div>
													@endif
												@endforeach
											</div>
											<div class="tab-pane mar-10" id="no">
												@foreach($data['ticketRSVPs'] as $ticketRSVP)
													@if($ticketRSVP->type_rsvp_id == config('evibe.type-rsvp.no'))
														<div class="invite-rsvp-response">
															<div class="text-bold">{{ $ticketRSVP->name }}</div>
															@if($ticketRSVP->comments)
																<div class="mar-l-20">
																	<span class="glyphicon glyphicon-comment valign-mid"></span><span class="valign-mid">{{ $ticketRSVP->comments }}</span>
																</div>
															@endif
														</div>
													@endif
												@endforeach
											</div>
										</div>
									</div>
								</div>
							@endif
						</div>
					</div>
					<div class="clearfix"></div>
				@endif
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			function activateTab(tab) {
				var $activatedTab = $('.invite-rsvp-tabs a[href="#' + tab + '"]');
				$activatedTab.tab('show');
				$('.invite-rsvp-tabs a').removeClass('scrollmenu-element-active');
				$activatedTab.addClass('scrollmenu-element-active');
			}

			(function eventsHandler() {
				$('.invite-rsvp-tabs a').click(function (event) {
					event.preventDefault();
					$('.invite-rsvp-tabs a').removeClass('scrollmenu-element-active');
					$(this).addClass('scrollmenu-element-active');
				});
			})();

			$('#btnInviteSubmit').on('click', function (event) {
				event.preventDefault();

				var data = {
					'rsvpOption': $('#rsvpStatus :selected').val(),
					'name': $('#name').val(),
					'phone': $('#phone').val(),
					/* 'email': $('#email').val(), */
					'comments': $('#comments').val(),
					'ticketId': $('#hidTicketId').val()
				};

				$.ajax({
					url: $(this).data('url'),
					type: "POST",
					dataType: "json",
					data: data,
					success: function (data) {
						if (data.success) {
							var successMsg = 'Your response has been successfully submitted';
							if (data.successMsg) {
								successMsg = data.successMsg;
							}
							window.showNotySuccess(successMsg);
							setTimeout(function () {
								location.reload();
							}, 1000);
						} else {
							var error = 'Some error occured while submitting your response. Kindly refresh the page and try again.';
							if (data.error) {
								error = data.error;
							}
							window.showNotyError(error);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						window.showNotyError();
						window.notifyTeam({
							"url": url,
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			});

			$('.invite-download-btn').click(function (event) {
				event.preventDefault();
				$('#inviteDownloadForm').submit();
			});

			activateTab('yes');
		});
	</script>
@endsection