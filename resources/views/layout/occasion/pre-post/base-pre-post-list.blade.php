@extends("layout.base-pre-post")

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-list.css') }}"/>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			var listJs = "<?php echo elixir('js/app/results_util.js'); ?>";
			$.getScript(listJs);
		});
	</script>
@endsection