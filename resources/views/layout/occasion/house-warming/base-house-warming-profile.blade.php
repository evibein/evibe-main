@extends("layout.base-house-warming")

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-profile.css') }}"/>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			var profileJs = "<?php echo elixir('js/app/profile_util.js'); ?>";
			$.getScript(profileJs);
		});
	</script>
@endsection