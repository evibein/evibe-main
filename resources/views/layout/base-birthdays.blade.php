@extends('layout.base-occasion')

@section('page-title')
	<title>{{ $data['seo']['pageTitle'] }} | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="{{ $data['seo']['pageDescription'] }}">
@endsection

@section('meta-keywords')
	<meta name="keywords" content="{{ isset($data['seo']['keyWords']) ? $data['seo']['keyWords'] : ""}}">
@endsection

@section('og-title')
	<meta property="og:title" content="{{ $data['seo']['pageTitle'] }} | Evibe.in">
@endsection

@section('og-description')
	<meta property="og:description" content="{{ $data['seo']['pageDescription'] }}">
@endsection

@section('og-url')
	<meta property="og:url" content="{{ isset($data['seo']['pageUrl']) ? $data['seo']['pageUrl'] : "" }}"/>
@endsection

@section('og-image')
	<meta property="og:image" content="{{ isset($data['seo']['metaImage']) ? $data['seo']['metaImage'] : "" }}"/>
@endsection

@section('seo:schema')
	@parent
	@if(isset($data['seo']['schema']) && count($data['seo']['schema']))
		@foreach($data['seo']['schema'] as $schema)
			{!! $schema !!}
		@endforeach
	@endif
@endsection

@section("header")
	@include('occasion.birthday.header.header')
@endsection

@section('push-notifications')
	@include('plugins.onesignal', ['pushOccasion' => 'birthday'])
@endsection

@section("footer")
	<div class="footer-wrap">
		@include('base.home.footer.footer-common')
	</div>
@endsection