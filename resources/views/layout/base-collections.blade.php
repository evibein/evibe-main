@extends('layout.base')

@section('page-title')
	<title>Best Resorts, Villas, Entertainment For Bachelors Party In {{ $cityName }} | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="Best deals for resorts, villas, cakes for bachelors party in {{ $cityName }}. Host best Bachelors party in {{ $cityName }}">
@endsection

@section('meta-keywords')
	<meta name="keywords" content="bachelors party, {{ $cityName }}, resorts, villas, lounges, food, cakes, best deals">
@endsection

@section('og-title')
	<meta property="og:title" content="Best Resorts, Villas, Entertainment For Bachelors Party In {{ $cityName }} | Evibe.in"/>
@endsection

@section('og-description')
	<meta property="og:description" content="Best deals for resorts, villas, cakes for bachelors party in {{ $cityName }}. Host best Bachelors party in {{ $cityName }}."/>
@endsection

@section('notification-popup')
@endsection

@section("header")
	@include('occasion.bachelor.header.header')
@endsection

@section("footer")
	@if (isset($data['views']['footer']) && view()->exists($data['views']['footer']))
		@include($data['views']['footer'])
	@endif
@endsection