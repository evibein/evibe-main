<!-- @see: Directly going to d_base as the js file sequence for venue is different from other products. Reason: yet to debug! Not even sure if this is fixed now or not  -->

@extends('layout.d_base')

@include("layout.base-meta-seo")

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-profile.css') }}"/>
@endsection

@section("footer")
	<div class="footer-google-reviews"></div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			/* see: do not change the order in gulp file */
			var profileDesktopJs = "<?php echo elixir('js/venue-profile-desktop.js'); ?>";

			$.getScript(profileDesktopJs);

		});
	</script>
@endsection

@include("layout.product.venue.profile.c")

<!-- Add any venue specific desktop code -->