@extends('layout.d_base')

@include("layout.base-meta-seo")

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-profile.css') }}"/>
@endsection

@section("footer")
	<div class="footer-google-reviews"></div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			/* see: do not change the order in gulp file */
			var profileDesktopJs = "<?php echo elixir('js/profile-desktop.js'); ?>";

			$.getScript(profileDesktopJs);

		});
	</script>
@endsection