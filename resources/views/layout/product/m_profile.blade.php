@extends('layout.m_base')

@include("layout.base-meta-seo")

@section("modals")
	@parent
	@include('app.modals.enquiry-form', [
		"url" => route("city.header.enquire", getCityUrl())
	])
@endsection

@section("footer")
	<div class="footer-google-reviews"></div>
	<div class="footer-wrap">
		@include('base.home.footer.footer-common')
	</div>
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-profile.css') }}"/>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			var profileMobileJs = "<?php echo elixir('js/profile-mobile.js'); ?>";

			$.getScript(profileMobileJs);
		});
	</script>
@endsection