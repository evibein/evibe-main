@extends('layout.d_base')

@include("layout.base-meta-seo")

@section("modals")
	@parent
	<!-- Inclusion of enquiry modal has been removed as it is being included in headers for desktop (included in multiple cases) -->
	<!--
	@include('app.modals.enquiry-form', [
		"url" => route("city.header.enquire", getCityUrl())
	])
	-->
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-list.css') }}"/>
@endsection

@section("footer")
	<div class="footer-google-reviews"></div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			var listDesktopJs = "<?php echo elixir('js/list-desktop.js'); ?>";

			$.getScript(listDesktopJs);
		});
	</script>
@endsection