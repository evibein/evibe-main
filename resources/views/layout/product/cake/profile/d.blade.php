@extends('layout.product.d_profile')

<!-- Add any cake profile page specific desktop code -->

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			var cakeJs = "<?php echo elixir('js/cakes/cake_profile.js'); ?>";
			$.getScript(cakeJs);
		});
	</script>
@endsection