@extends('layout.product.m_profile')

<!-- Add any cake profile page specific mobile code -->

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			var cakeJs = "<?php echo elixir('js/cakes/cake_profile.js'); ?>";
			$.getScript(cakeJs);
		});
	</script>
@endsection