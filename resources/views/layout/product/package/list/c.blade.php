@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			/*		$('.footer-top').addClass('hide')*/
			var $surOccasionOption = $('#surOccasionOption').val();
			var $surAgeGroupOption = $('#surAgeGroupOption').val();

			/* On change relations it will change both the occasion tags & age tags */
			$('#surRelations').on('change', function () {
				sortOccasionList(this);
				sortAgeGroupList(this);
			});

			function sortOccasionList(surRelId) {
				var oid = $(surRelId).val();

				/* getting all the negative id's for that relation */
				var negativeTagIds = [];
				$('#OccasionMappingOptions option').each(function () {
					if ($(this).data('sr-id') == oid) {
						negativeTagIds.push($(this).val());
					}
				});

				/* hiding all the options which id's are there in the negativeTagIds */
				$('#surOccasions option').each(function () {
					$(this).removeClass('hide');
					if (($.inArray($(this).val(), negativeTagIds)) >= 0) {
						$(this).addClass('hide');
					}
				});

				/* Resetting the "ALL" after relation change */
				$('#surOccasions').val(0);
				$(surRelId).data('options', '');
			}

			function sortAgeGroupList(surRelId) {
				var aid = $(surRelId).val();

				/* getting all the negative id's for that relation */
				var negativeTagIds = [];
				$('#AgeMappingOptions option').each(function () {
					if ($(this).data('sr-id') == aid) {
						negativeTagIds.push($(this).val());
					}
				});

				/* hiding all the options which id's are there in the negativeTagIds */
				$('#surAgeGroups option').each(function () {
					$(this).removeClass('hide');
					if (($.inArray($(this).val(), negativeTagIds)) >= 0) {
						$(this).addClass('hide');
					}
				});

				/* Resetting the "ALL" after relation change */
				$('#surAgeGroups').val(0);
				$(surRelId).data('options', '');
			}

			/* First time loading the fields & assign the selected value */
			sortOccasionList('#surRelations');
			if ($surOccasionOption !== undefined && $surOccasionOption !== "") {
				$('#surOccasions').val($surOccasionOption);
			}

			/* First time loading the fields & assign the selected value */
			sortAgeGroupList('#surRelations');
			if ($surAgeGroupOption !== undefined && $surAgeGroupOption !== "") {
				$('#surAgeGroups').val($surAgeGroupOption);
			}

			$('#surMainFilterBtn').on('click', function (event) {
				event.preventDefault();

				var surListUrl = $('#surResultsUrl').val();
				var surReference = $('#surResultsUrl').data('ref');

				var surRelId = $('#surRelations').val();
				var surRelText = $("#surRelations option:selected").text();

				var surOccId = $('#surOccasions').val();
				var surOccText = $("#surOccasions option:selected").text();

				var surAgeId = $('#surAgeGroups').val();
				var surAgeText = $("#surAgeGroups option:selected").text();

				surListUrl += "/sr/" + surRelId + "-" + surRelText + "/so/" + surOccId + "-" + surOccText + "/sa/" + surAgeId + "-" + surAgeText + "?ref=" + surReference;
				window.location.href = surListUrl;
			});

		});
	</script>
@endsection