@extends("layout.product.d_list")

@include("layout.product.package.list.c")

<!-- Add any package list page specific desktop code -->

@if(isset($data['mapTypeId']) && $data['mapTypeId'] == config('evibe.ticket.type.surprises'))
@section("javascript")
	@parent
	<script type="text/javascript">

		$(document).ready(function () {
			function moveScroller() {
				var $anchor = $(".scroll-anchor");
				var $scroller = $('.des-list-sur-filter-bar');
				var $emptyDiv = $('.scroll-empty-div');
				var $scrollDivHeight = $scroller.outerHeight();

				var move = function () {
					var st = $(window).scrollTop();
					if ($anchor.length > 0) {
						var ot = $anchor.offset().top;
						if (st > ot) {
							$scroller.css({
								position: "fixed",
								top: "0px"
							});
							$emptyDiv.css({
								height: $scrollDivHeight
							});
						} else {
							$scroller.css({
								position: "relative",
								top: ""
							});
							$emptyDiv.css({
								height: "0"
							});
						}
					}
				};
				$(window).scroll(move);
				move();
			}

			moveScroller();
		});

	</script>
@endsection
@endif