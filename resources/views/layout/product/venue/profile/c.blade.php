@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			/* gallery */
			$('.loading-img-wrap').remove();
			$('.venue-gallery-wrap').removeClass('hide');

			$('.avg-rating').rating({
				showClear: 0,
				readonly: 'true',
				showCaption: 0,
				size: 'xl',
				min: 0,
				max: 5,
				step: 0.1
			});

			$('.avg-rating').removeClass('hide');

			/* show Google map */
			function initialize() {

				var myLatlng = new google.maps.LatLng(12.9944049, 77.6787036);
				var mapOptions = {
					zoom: 16,
					center: myLatlng,
					clickable: false,
					scrollwheel: false,
					mapTypeControl: false,
					mapMaker: false
				};

				var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

				var marker = new google.maps.Marker({
					position: myLatlng,
					map: map
				});
			}

			google.maps.event.addDomListener(window, 'load', initialize);
		});
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
@endsection