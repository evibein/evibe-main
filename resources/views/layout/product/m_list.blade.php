@extends('layout.m_base')

@include("layout.base-meta-seo")

@section("modals")
	@parent
	@include('app.modals.enquiry-form', [
		"url" => route("city.header.enquire", getCityUrl())
	])
@endsection

@section("footer")
	<div class="footer-google-reviews"></div>
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-list.css') }}"/>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			var listMobileJs = "<?php echo elixir('js/list-mobile.js'); ?>";

			$.getScript(listMobileJs);
		});
	</script>
@endsection