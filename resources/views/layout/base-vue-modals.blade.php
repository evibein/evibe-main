@section("vue-modals")
	@parent
	<section class="modal fade pad-10" id="shortlistModal" tabindex="-1" role="dialog" aria-labelledby="shortlistModal" aria-hidden="true">
		<div class="modal-dialog no-mar-400-600 modal-sm">
			<div class="modal-content no-border">
				<div class="modal-body">
					<div class="cross-button ga-btn-close-book-now-modal text-center" data-dismiss="modal">x</div>
					<div id="shortlistModalContainer"></div>
					<div class="shortlist-cta-container text-center">
						<div class="shortlist-add-to-bag-container">
							<div class="text-center mar-t-20 mar-b-15">
								<div v-on:click="shortlistModalAddToCartBtn" class="btn btn-primary add-to-cart-btn">
									<i class="icon-bagicon5"></i>
									<span class="mar-l-4 text-uppercase">Add To Cart</span>
								</div>
							</div>
						</div>
						<div class="shortlist-clear-bag-container hide">
							<div class="shortlist-clear-bag-msg"></div>
							<div class="mar-t-5 mar-b-5">
								<div v-on:click="shortlistModalCloseClearBag" class="btn btn-default shortlist-clear-bag-btn">
									<span class="mar-l-4 text-uppercase">No</span>
								</div>
								<div v-on:click="shortlistModalClearBagBtn" class="btn btn-default shortlist-clear-bag-btn">
									<span class="mar-l-4 text-uppercase">Yes</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection