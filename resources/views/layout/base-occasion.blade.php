@extends('layout.base')

@include('layout.base-vue-modals')

@section('javascript')
	@parent
	<script type="text/javascript">
		/* Ajax request to render customer reviews */
		/* This is similar to city home page reviews implementation but not Async */
		/* After business API is done, this can be combined with google reviews to load only when there is no response/error from business API */
		var $ajaxUrl = "/ajax/home/customer-review";
		$.ajax({
			url: $ajaxUrl,
			type: "POST",
			async: true,
			tryCount: 0,
			retryLimit: 3,
			data: {},
			success: function (data) {
				if (typeof data !== undefined && data) {
					$('.occasion-customer-reviews').append(data);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				this.tryCount++;
				if (this.tryCount <= this.retryLimit) {
					$.ajax(this);
				}
				else {
					window.notifyTeam({
						"url": $ajaxUrl,
						"textStatus": textStatus,
						"errorThrown": errorThrown
					});
				}
			}
		});
	</script>
@endsection