@section('page-title')
	<title>{{ isset($data['seo']['pageTitle']) ?  $data['seo']['pageTitle'] : "Your private party planner" }} | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="{{ isset($data['seo']['pageDescription']) ?  $data['seo']['pageDescription'] : 'Evibe.in - Your private party planner' }}">
@endsection

@section('meta-keywords')
	<meta name="keywords" content="{{ isset($data['seo']['keyWords']) ? $data['seo']['keyWords'] : ""}}">
@endsection

@section('og-title')
	<meta property="og:title" content="{{ isset($data['seo']['pageTitle']) ?  $data['seo']['pageTitle'] : 'Your private party planner' }} | Evibe.in">
@endsection

@section('og-description')
	<meta property="og:description" content="{{ isset($data['seo']['pageDescription']) ?  $data['seo']['pageDescription'] : 'Evibe.in - Your private party planner' }}">
@endsection

@section('og-url')
	<meta property="og:url" content="{{ isset($data['seo']['pageUrl']) ? $data['seo']['pageUrl'] : "" }}"/>
@endsection

@section('og-image')
	<meta property="og:image" content="{{ isset($data['seo']['metaImage']) ? $data['seo']['metaImage'] : "" }}"/>
@endsection

@section('seo:schema')
	@parent
	@if(isset($data['seo']['schema']) && count($data['seo']['schema']))
		@foreach($data['seo']['schema'] as $schema)
			{!! $schema !!}
		@endforeach
	@endif
@endsection