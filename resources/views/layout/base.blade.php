<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en"> <!--<![endif]-->

<head>
	<!-- GTM DataLayer -->
@yield('gtm-data-layer')

<!-- trackers -->
	@include('track.gtm')

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width">
	<meta name="google-site-verification" content="aAggmaMI_knVUeswYULXQSKD41sWlr2z3G21bdkPZoI"/>
	<meta name="sitelock-site-verification" content="7865"/>
	<meta name="p:domain_verify" content="fc38283c1f85e5668a5087f580081a34"/>

	@section('meta-description')
		<meta name="description" content="One stop shop to find and book the best birthday party planners, party organisers, party halls, restaurants, magicians, artists, home theme decorators, balloon decorators, theme cakes, standard cakes">
	@show

	@section('meta-keywords')
		<meta name="keywords" content="kids birthday, party halls, party organisers, magicians">
	@show

	@section('meta-canonical')
	@show

	<meta name="msvalidate.01" content="31489A18F1AD59FC089CB9B9A7E5A85D"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<!-- Bing webmaster -->

	<!-- Open graph tags -->
	@section('og-title')
		<meta property="og:title" content="Best Kids Birthday Planners, Party Organisers, Venues | Evibe.in">
	@show

	@section('og-url')
		<meta property="og:url" content="https://evibe.in">
	@show

	@section('og-image')
		<meta property="og:image" content="{{ $galleryUrl }}/img/icons/brand_image_new.png">
	@show

	@section('og-description')
		<meta property="og:description" content="One stop shop to find and book the best birthday planners, party organisers, party halls, restaurants, magicians, artists, home theme decorators, balloon decorators, balloon decorators, theme cakes, standard cakes.">
	@show

	<meta property="og:site_name" content="Evibe.in">
	<meta property="fb:admins" content="">

	@section('seo:schema')
		<script type="application/ld+json">
			{
				"@context": "http://schema.org",
				"@type": "Organization",
				"name": "Evibe.in",
				"legalName": "Evibe Technologies Private Limited",
				"url": "https://www.evibe.in",
				"email": "ping@evibe.in",
				"foundingDate": "2014-02-10",
				"contactPoint": [{
					"@type": "ContactPoint",
					"telephone": "+91-9640204000",
					"contactType": "customer service",
					"areaServed": "IN",
					"availableLanguage": ["English", "Telugu", "Hindi", "Kannada"]
				}],
				"founders": [{
					"@type": "Person",
					"name": "B Anjaneyulu Reddy",
					"jobTitle": "CEO"
				},{
					"@type": "Person",
					"name": "Swathi Bavanaka",
					"jobTitle": "COO"
				}],
				"sameAs": [
					"https://www.facebook.com/evibe.in",
					"https://www.instagram.com/evibe.in",
					"https://twitter.com/evibein",
					"https://plus.google.com/+EvibeIn",
					"https://www.linkedin.com/company-beta/3152628",
					"https://www.youtube.com/channel/UCLANgB4zAIj35TH1SsiuLIg"
				],
				"logo": [{
					"@type": "ImageObject",
					"url": "https://gallery.evibe.in/img/logo/logo_evibe.png",
					"width": "125",
					"height": "33",
					"caption": "evibe.in"
				}]
			}

		</script>
	@show

	@section('page-title')
		<title>Best Kids Birthday Planners, Youth Party Organisers, Wedding Reception & Engagement Services | Evibe.in</title>
	@show

	@section('custom-css')
		<link rel="stylesheet" href="{{ elixir('css/all.css') }}">
	@show

	<link rel="icon" href="{{ $galleryUrl }}/img/logo/favicon_evibe_v2.png" type="image/png" sizes="16x16"/>
	<link rel="apple-touch-icon" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="57x57" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-57x57.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="72x72" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-72x72.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="76x76" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-76x76.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="114x114" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-114x114.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="120x120" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-120x120.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="144x144" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-144x144.png" type="image/png"/>
	<link rel="apple-touch-icon" sizes="152x152" href="{{ $galleryUrl }}/img/favicons/apple-touch-icon-152x152.png" type="image/png"/>
	<link rel="manifest" href="{{ asset('manifest.json') }}"/>
	<meta name="theme-color" content="#ed3e72">

	@yield('push-notifications')
	@yield('custom-head')
</head>

<body @section('body-attrs') @show class="home-bg-color">

@include('track.gtm-ns')

@section('seo-search')
	@if(getCityUrl())
		<div itemscope itemtype="http://schema.org/WebSite" class="hide">
			<meta itemprop="url" content="{{ route('city.home', getCityUrl()) }}"/>
			<form itemprop="potentialAction" itemscope itemtype="http://schema.org/SearchAction">
				<meta itemprop="target" content="{{ route('city.search', getCityUrl()) }}?q={search_term_string}"/>
				<input itemprop="query-input" type="text" name="search_term_string" required/>
				<input type="submit"/>
			</form>
		</div>
	@elseif (isset($data["campaignData"]) && (count($data["campaignData"]) > 0))
		<div itemscope itemtype="http://schema.org/WebSite" class="hide">
			<meta itemprop="url" content="{{ route('campaign.landingpage', $data["campaignData"]["url_slug"]) }}"/>
		</div>
	@elseif (isset($data["isLandingPage"]) && $data["isLandingPage"] == "2")
		<div itemscope itemtype="http://schema.org/WebSite" class="hide">
			<meta itemprop="url" content="{{ route('landindpage.landingpage', $data["pageDetails"]["url"]) }}"/>
		</div>
	@endif
@show

<!-- Application modals -->
@section("modals")
	<section id="modalLoading" class="modal modal-loading" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-body">
					<h4>
						<img src="{{ $galleryUrl }}/img/icons/loading.gif" alt="Loading icon"> Loading, please wait...
					</h4>
					<div class="text-danger">(Please do not press refresh / back button)</div>
				</div>
			</div>
		</div>
	</section>
@show

@section("city-change-modal")
	@include('app.modals.city_change_modal')
@show

@section("login-modal")
	@include('app.modals.login_modal')
	@include('app.modals.thank_you_sign_up_modal')
@show

<div class="notfication"></div>

<!-- Application content -->
@section('app')
	<div class="application" id="evibeApp">
		<input type="hidden" id="vueJsAppDebug" value="{{ config("app.debug") }}">
		<input type="hidden" id="sessionCityUrl" value="{{ getCityUrl() }}">
		<input type="hidden" id="cityOccasionUrl" value="{{ isset($cityOccasionUrl) ? $cityOccasionUrl : "" }}">
		<input type="hidden" id="globalSearchOccasionId" value="{{ isset($occasionId) ? $occasionId : 0 }}">
		<input type="hidden" id="globalSearchCity" value="{{ getCityId() }}">
		<input type="hidden" id="globalSearchRemoteHost" value="{{ config('evibe.host') }}">
		<input type="hidden" id="globalSearchAction" value="{{ route('city.search', getCityUrl()) }}">
		<input type="hidden" id="hidAdvPercentage" value="{{config('evibe.ticket.advance.percentage')}}">
		<input type="hidden" id="googleApiIntegration" value="yes">
		<input type="hidden" id="cssURLKey" value="{{ config("evibe.cssURLKey") }}">
		<input type="hidden" id="formEnquiryOpen" value="0">

		@if(isset($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'], "wv") > 0)
			<div class="loading-screen-gif" style="position:fixed; background-image: linear-gradient(#ffffff00, #ffffffe0, #ffffff00); width: 100%; height: 100%; display: table; z-index: 999999999;">
				<div style="display: table-cell; height: 100%; width: 100%; vertical-align: middle; text-align: center;">
					<img src="{{ config("evibe.gallery.host")."/img/gifs/party.gif" }}" alt="loading..." width="200" height="200">
				</div>
			</div>
		@endif

		@yield('header')

		<div class="container-wrap">
			@yield('content')
		</div>

		<footer>
			@yield('footer')
		</footer>

		@section('vue-modals')
			<section class="modal fade pad-10 party-bag-enquiry-modal" id="partyBagEnquiryModal" tabindex="-1" role="dialog" aria-labelledby="partyBagEnquiryModal" aria-hidden="true">
				<div class="modal-dialog no-mar-400-600 modal-sm">
					<div class="modal-content no-border">
						<div class="cross-button ga-btn-close-book-now-modal text-center" data-dismiss="modal">x</div>
						<div class="modal-header text-center">
							<div class="text-bold font-20">
								Cart Enquiry
							</div>
						</div>
						<div class="modal-body no-pad party-bag-enquiry-container">
							<div class="success-cnt partybag-alert-success font-18 text-center hide"></div>
							<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1 no-pad mar-t-15 mar-b-15">
								<form id="partyBagEnquiryForm" action="{{ route('partybag.enquire') }}" method="POST">
									<div class="pad-b-10 text-muted">
										Please fill the form and click on <b>"Submit"</b> to enquire.
									</div>
									<div class="errors-cnt partybag-alert-danger no-mar-b hide text-center"></div>
									<div class="text-center">
										<div class="col-md-12">
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<input type="text" name="pbEnqEventDate" id="pbEnqEventDate" value="{{ old('pbEnqEventDate') }}" class="mdl-textfield__input">
												<label for="pbEnqEventDate" class="mdl-textfield__label text-normal">Party Date</label>
											</div>
										</div>
										<div class="col-md-12">
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<input type="text" name="pbEnqPhone" id="pbEnqPhone" class="mdl-textfield__input" value="{{ old('pbEnqPhone') }}">
												<label for="pbEnqPhone" class="mdl-textfield__label text-normal">Phone Number</label>
											</div>
										</div>
										<div class="col-md-12">
											<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
												<input type="text" name="pbEnqPartyLocation" id="pbEnqPartyLocation" class="mdl-textfield__input google-auto-complete" placeholder="" value="{{ old('pbEnqPartyLocation') }}">
												<label for="pbEnqPartyLocation" class="mdl-textfield__label text-normal">Party Location</label>
												<input type="hidden" class="google-location-details" name="locationDetails" value="">
											</div>
										</div>
										<div class="col-md-12">
											<div class="mdl-card__actions">
												<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" data-upgraded=",MaterialButton,MaterialRipple" v-on:click="submitEnquiry">
													<i class="material-icons valign-mid"></i>
													<span class="valign-mid mar-l-4">Submit Details</span>
													<span class="mdl-button__ripple-container">
											<span class="mdl-ripple"></span>
										</span>
												</button>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								</form>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</section>
		@show

		@yield('notification-popup')
	</div>
@show

@if (isset($data) && isset($data['cityId']) && isset($data['occasionId']) && isset($data['mapTypeId']))
	@if ($agent->isDesktop() && (!request()->is('*/engagement-wedding-reception*') && !request()->is('*/bachelor-party*')))
		<div class="text-center reverse-bidding-wrap mar-t-20 pad-10 contact-details-card hide" style="color: #696969; border: 1px solid #D9D9D9; background-color: #F8F8F8;">
			<div class="title text-e">Need Help In Choosing?</div>
			<div class="content">Let us help you in finding the best one. Leave us an enquiry, Our Expert will you call in 2-4 business hours</div>
			<button class="btn btn-default btn-bidding contact-details-card-enquire-now">
				<i class="material-icons valign-mid">email</i>
				<span class="valign-mid">Enquire Now</span>
			</button>
			<div class="font-15 pad-t-8">
				<i class="glyphicon glyphicon-phone no-pad no-mar mar-r-2"></i>
				{{ config('evibe.contact.company.plain_phone') }}
			</div>
			<div class="font-15 pad-t-8 hide">
				<img src="{{ $galleryUrl }}/main/img/icons/whatsapp.png" height="18px" width="18px">
				<a href="https://web.whatsapp.com/send?phone=91{{ config("evibe.contact.customer.whatsapp.phone") }}&text=Hello Evibe.in, I am interested in your services for my party." target="_blank" class="whatsapp-wrap mar-t-8 pad-l-5 valign-mid a-grey font-15">
					{{ config("evibe.contact.customer.whatsapp.phone") }}
				</a>
			</div>
		</div>
	@endif
@endif

<!-- Load Google Fonts -->
<!-- @author: Anji; @since 2 Nov 2016 -->
<!-- @see: https://www.lockedowndesign.com/load-google-fonts-asynchronously-for-page-speed/ -->
<script type="text/javascript">
	WebFontConfig = {
		google: {families: ['Roboto:300,400,500,700|Material+Icons&display=swap']}
	};
	(function () {
		let wf = document.createElement('script');
		wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
			'://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js';
		wf.type = 'text/javascript';
		wf.async = 'true';
		let s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(wf, s);
	})();
</script>

@section('js-framework')
	<script src="{{ elixir('js/all.js') }}"></script>
@show

<script id="js400" data-url="{{ elixir('js/app/400.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function () {
		if (document.documentElement.clientWidth <= 600) {
			var url = $("#js400").data("url");
			$.getScript(url);
		}

		/* Report error with payment pages */
		(function checkCSSLoad() {
			var ss = document.styleSheets;
			var count = 0;
			var currentUrl = window.location.href;

			/* do not check for intermediate page */
			if (currentUrl.match("intermediate")) {
				return false;
			}

			for (var i = 0, max = ss.length; i < max; i++) {
				if (ss[i].href != null &&
					(ss[i].href.indexOf($("#cssURLKey").val()) !== -1 || ss[i].href.indexOf("af-") !== -1 || ss[i].href.indexOf("page-home") !== -1 || ss[i].href.indexOf("page-list") !== -1 || ss[i].href.indexOf("page-profile") !== -1)) {
					count = 1;

					if (ss[i].cssRules == null || ss[i].cssRules.length === 0) {
						var cookieCount = parseInt(Cookies.get("css_error_" + window.location.pathname), 10);

						if (!cookieCount) {
							Cookies.set("css_error_" + window.location.pathname, '1', {expires: 15});
							cookieCount = 1;
						} else {
							Cookies.set("css_error_" + window.location.pathname, cookieCount + 1, {expires: 15});
							cookieCount = cookieCount + 1;
						}

						showErrorAndSendErrorMail("CSS content not loaded.", ss[i].href, "css_error_" + window.location.pathname + ": " + cookieCount);

						$('body').prepend('<div style="background-color: #FF0000;text-align: center;padding: 10px;margin-bottom: 10px;color: #FFFFFF;">' +
							'<span style="font-size: 20px;">Sorry, There is an issue with loading this page</span>' +
							'<div style="margin-top: 15px;margin-bottom: 5px;"><a id="cssLoadErrorReloadCTA" href="' + window.location.href +
							'" style="padding: 5px;border: 1px solid #FFFFFF;background-color: #FFFFFF;cursor: pointer;color: #000000;text-decoration: none;">Click Here To Refresh This Page</a></div>' +
							'</div>');
					}
				}

				if (i === (max - 1)) {
					if (count === 0) {
						/*showErrorAndSendErrorMail("CSS file not found.", "", "");*/
					}
				}
			}
		})();

		function showErrorAndSendErrorMail(message, link, cookieInfo) {
			$.ajax({
				"url": "/report/notify/error/css",
				"type": "POST",
				"dataType": "JSON",
				"data": {
					"url": link,
					"textStatus": cookieInfo,
					"errorThrown": message
				}
			});
		}
	});
</script>

@section("google-places-api")
	<script src="https://maps.googleapis.com/maps/api/js?key={{ config('evibe.google.places_key') }}&libraries=places"></script>
@show

@yield('javascript')

</body>
</html>