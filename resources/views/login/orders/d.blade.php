@extends('layout.base')

@section('page-title')
	<title>Evibe.in | My orders</title>
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/util/track-my-order.css') }}"/>
@endsection

@section("header")
	@include('base.home.header.header-top-city')
	<hr class="no-mar">
@endsection

@section("content")
	@if($data['bookings'])
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 cust-order pad-t-10" style="min-height: calc(100vh - 430px);">
			<h1 class="text-dark order-heading  page-heading">
				Your Upcoming Orders
			</h1>
			@foreach($data['bookings'] as $booking)
				<div class="col-md-4 col-sm-6 col-lg-4 col-xs-12 no-pad  mar-b-10">
					<div class=" order-data order-card">
						<div class="pad-10 header">
							<div class="pull-left">
								<span class="text-dark">#</span><span class="text-dark text-bold">{{$booking['enquiryId']}}</span>
							</div>
							<div class="clearfix"></div>
						</div>
						<hr class="no-mar no-pad">
						<div class=" mar-t-5 col-md-12 col-sm-12 col-lg-12 col-xs-12 no-pad pad-b-5">
							<div class="mar-t-5 mar-l5">
								<span class="col-md-6 col-sm-6 col-lg-5 col-xs-6  pad-l-10 no-pad"><i class="glyphicon glyphicon-calendar pad-r-3"></i>Party Date:</span>
								<span class="col-md-6 col-sm-6 col-lg-7 col-xs-6 no-pad text-light-grey">{{$booking['partyDate']}}</span>
								<div class="clearfix"></div>
							</div>
							<div class="mar-t-5 ">
								<span class="col-md-6 col-sm-6 col-lg-5 col-xs-6  pad-l-10 no-pad"><i class="glyphicon glyphicon-gift pad-r-3"></i>Details:</span>
								<span class="text-light-grey col-md-6 col-sm-6 col-lg-7 col-xs-6 no-pad  text-overflow">{{$booking['details']}}</span>
								<div class="clearfix"></div>
							</div>
							<div class="mar-t-5">
								<span class="col-md-6 col-sm-6 col-lg-5 col-xs-6   pad-l-10 no-pad"><i class="glyphicon glyphicon-map-marker pad-r-3"></i>Location:</span>
								<span class="text-light-grey col-md-6  col-sm-6  col-lg-7 col-xs-6   no-pad  ">{{$booking['partyLocation']}}</span>
								<div class="clearfix"></div>
							</div>
							<div class="mar-t-5">
								<span class="col-md-6 col-sm-6 col-lg-5 col-xs-6   pad-l-10 no-pad"><span class="rupee-font pad-l-5 pad-r-5">₹</span>Order Amount:</span>
								<span class="text-light-grey col-md-6  col-sm-6  col-lg-7  col-xs-6   no-pad  ">{{$booking['bookingAmount']}} ₹</span>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="text-center mar-b-10">
							<a href="/{{$booking['customerOrderDetailsLink']}}" target="_blank">
								<button class="btn btn-success">Order Details</button>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			@endforeach
		</div>
	@else
		<div class="mar-t-20" style="min-height:500px">
			<div class="text-center"><img src="https://gallery.evibe.in/main/img/icons/caution-sign.png"></div>
			<div class="text-center mar-t-10 text-danger"> Sorry, you do not have any upcoming bookings yet</div>
		</div>
	@endif
	<div class="clearfix"></div>
@endsection

@section("footer")
	<div class="footer-wrap">
		@include('base.home.footer.footer-non-city')
	</div>
@endsection