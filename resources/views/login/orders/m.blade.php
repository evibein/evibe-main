@extends("layout.product.ent.profile.m")

@section('page-title')
	<title>Orders</title>
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/util/track-my-order.css') }}"/>
@endsection

@section("header")
	<header>
		<!-- @todo: make into single header by getting occasion using config, if headers are not used for opening up categories -->
		<div class="mobile-header">
			<div class="mobile-header-wrap">
				<div class="mar-l-10 mobile-header-back in-blk">
					<a href="{{$data['previousUrl']}}">
						<i class="material-icons mobile-back-icon">&#xE317;</i>
					</a>
				</div>
				<div class="mobile-header-logo-wrap in-blk">
					<a href="/">
						<img class="mobile-header-logo" src="{{ $galleryUrl }}/img/logo/logo_evibe.png" alt="Evibe.in logo">
					</a>
				</div>
				<div class="in-blk pull-right">
				<span class="mar-r-20">
					<a href="/user/logout">
						Sign Out
					</a>
				</span>
				</div>
			</div>
		</div>
	</header>
@endsection

@section("content")
	@php
		$cityUrl = getCityUrl();
	@endphp
	<div class="pad-l-10 pad-t-5 pad-b-5" style="background: #F7F7F7">Welcome
		<span class="text-danger text-bold"> {{getAuthUserName()}}</span></div>
	<hr class="hr no-pad no-mar">
	@if($data['bookings'])
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 cust-order pad-t-5 no-pad pad-l-10 pad-r-10">
			<h1 class="text-dark order-heading " style="padding-top: 10px;margin-bottom:10px:padding-bottom:5px">
				Your Upcoming Orders
			</h1>
			@foreach($data['bookings'] as $booking)
				<div class="col-md-3 col-sm-6 col-lg-6 col-xs-12 no-pad order-card mar-b-15 ">
					<div class=" order-data">
						<div class="pad-10 header">
							<div class="pull-left">
								<span class="text-dark">#</span><span class="text-dark text-bold">{{$booking['enquiryId']}}</span>
							</div>
							<div class="clearfix"></div>
						</div>
						<hr class="no-mar">
						<div class=" mar-t-5 col-md-12 col-sm-12 col-lg-12 col-xs-12 no-pad pad-b-5">
							<div class="mar-t-5">
								<span class="col-md-4 col-sm-4 col-lg-4 col-xs-5 pad-l-10 no-pad "><i class="glyphicon glyphicon-calendar pad-r-3"></i>Party Date:</span>
								<span class="col-md-8 col-sm-8 col-lg-8  col-xs-7 no-pad pad-l-5 text-light-grey">{{$booking['partyDate']}}</span>
							</div>
							<div class="mar-t-5 ">
								<span class="col-md-4 col-sm-4 col-lg-4 col-xs-5 pad-l-10 no-pad "><i class="glyphicon glyphicon-gift pad-r-3"></i>Details:</span>
								<span class=" col-md-8 col-sm-8 col-lg-8  col-xs-7 no-pad pad-l-5 text-overflow text-light-grey">{{$booking['details']}}</span>
							</div>
							<div class="mar-t-5">
								<span class="col-md-4 col-sm-4 col-lg-4 col-xs-5 pad-l-10 no-pad "><i class="glyphicon glyphicon-map-marker pad-r-3"></i>Location:</span>
								<span class=" col-md-8 col-sm-8 col-lg-8 col-xs-7 no-pad pad-l-5 text-light-grey ">{{$booking['partyLocation']}}</span>
							</div>
							<div class="">
								<span class="col-md-4 col-sm-4 col-lg-4 col-xs-5 pad-l-10 no-pad "><span class="rupee-font pad-l-3 pad-r-4">₹</span>Order Amount:</span>
								<span class=" col-md-8 col-sm-8 col-lg-8  col-xs-7 no-pad pad-l-5 text-light-grey">{{$booking['bookingAmount']}} ₹	</span>
							</div>
						</div>
						<div class="text-center mar-b-10">
							<a href="/{{$booking['customerOrderDetailsLink']}}" target="_blank">
								<button class="btn btn-success xs-low-font">Order Details</button>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			@endforeach
		</div>
	@else
		<div class="mar-t-20">
			<div class="text-center"><img src="https://gallery.evibe.in/main/img/icons/caution-sign.png"></div>
			<div class="text-center mar-t-10 text-danger"> Sorry, you do not have any upcoming bookings yet</div>
		</div>
	@endif
	<div class="clearfix"></div>
@endsection

@section("footer")
	<div class="footer-wrap">
		@include('base.home.footer.footer-non-city')
	</div>
@endsection