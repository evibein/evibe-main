@extends('layout.base')

@section('page-title')
	<title>Track My Order | Evibe.in</title>
@endsection

@section('body-attrs')
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/util/track-my-order.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/util/slick.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/util/slick-theme.css') }}"/>
@endsection

@section("header")
	@include('base.home.header.header-home-city')
	<div class="header-border full-width mar-b-10"></div>
@endsection

@section("content")
	@if($data['mergeTickets'])
		<div class="pad-l-10 pad-r-10 col-lg-10 col-lg-offset-1" style="min-height: calc(100vh - 430px);">
			<h1 class="text-center mar-t-10 page-heading">My Upcoming Orders</h1>
			@foreach($data['mergeTickets'] as $mergeTicketKey => $mergeTicket)
				@php
					$count = 0;
					$totalBookingAmount = 0;
					$totalAdvanceAmount = 0;
					$ticketBookings = $mergeTicket["bookings"];
					$firstBooking = $ticketBookings->first();
					$isAutoPay = in_array(config("evibe.ticket.status.auto_paid"), $mergeTicket["statusIds"]) || in_array(config("evibe.ticket.status.service_auto_pay"), $mergeTicket["statusIds"]) ? "YES" : "NO";
					$isVenueBooking = $firstBooking->is_venue_booking;
					$minBookTime = $maxBookTime = "";
					if($ticketBookings && (count($ticketBookings) > 0))
					{
						$minBookTime = $firstBooking->party_date_time;
						$maxBookTime = $firstBooking->party_date_time;
						foreach($ticketBookings as $booking)
							if($booking->party_date_time > $maxBookTime)
								$maxBookTime = $booking->party_date_time;
							if($booking->party_date_time < $minBookTime)
								$minBookTime = $booking->party_date_time;
					}
				@endphp
				@if ($agent->isMobile() && !($agent->isTablet()))
					<div class="mar-b-20 tmo-ticket-wrap">
						<div class="tmo-ticket">
							<div class="pad-10 no-pad-t header">
								<div class="col-xs-8 no-pad-l pad-t-5">
									<span class="font-10" style="line-height: 10px;">PARTY DATE :</span>
									<br>
									<span class="font-16 text-bold" style="line-height: 16px;">
									<span class="glyphicon glyphicon-calendar"> </span>
										@if($minBookTime == $maxBookTime)
											{{ date('d M Y, h:i A', $minBookTime) }}
										@else
											{{ date('d M Y, h:i A', $minBookTime) . " - " . date('d M Y, h:i A', $maxBookTime) }}
										@endif
								</span>
								</div>
								<div class="col-xs-4 no-pad pad-t-5">
									<span class="font-10" style="line-height: 10px;">ORDER ID :</span>
									<br>
									<span class="font-16 text-bold" style="line-height: 16px;">
									@if($mergeTicket["enquiryId"] == "")
											{{ "EVB-BK-" . $firstBooking->ticket_id }}
										@else
											{{ $mergeTicket["enquiryId"] }}
										@endif
								</span>
								</div>
								<div class="clearfix"></div>
							</div>
							<hr class="no-mar no-pad">
							@if($isAutoPay == "YES")
								<div class="mar-l-5 mar-r-5 mar-t-10">
									<div class="alert alert-warning">
										<i class="glyphicon glyphicon-alert"></i> We are checking the availability and will confirm your booking within 4 business hours.
									</div>
								</div>
							@endif
							<div class="tmo-ticket-booking">
								<div class="col-lg-9 col-md-9">
									@foreach($ticketBookings as $ticketBooking)
										@php
											$ticket = isset($mergeTicket[$ticketBooking->ticket_id]) ? $mergeTicket[$ticketBooking->ticket_id] : "";
											$isAutoBooked = $ticket != "" ? $ticket->is_auto_booked : "";
											$mapLink = $ticket != "" ? $ticket->map_link : "";
											$venueAddress = $ticket != "" ? $ticket->venue_address : "";
											$balanceAmount = $ticketBooking->advance_amount ? ($ticketBooking->booking_amount - $ticketBooking->advance_amount) : ($ticketBooking->booking_amount - $ticketBooking->token_amount);
											$partner = $ticketBooking->provider;
										@endphp
										<h6 class="no-mar-b text-bold">
											Order:
										</h6>
										<div>
											<div class="font-16 mar-b-3 tmo-order-info">
												@if($ticketBooking->mapping->map_type_id == config("evibe.ticket.type.add-on"))
													@if($ticketBooking->gallery->count() > 0)
														@foreach($ticketBooking->gallery as $gallery)
															<div class="in-blk">
																<a href="{{ $gallery->getOriginalImagePath() }}" target="_blank">
																	<img src="{{ $gallery->getResultsImagePath() }}" alt="{{ $gallery->title }}" style="height: 100px;">
																</a>
															</div>
														@endforeach
														<div class="clearfix"></div>
													@endif
													<div class="text-warning text-bold in-blk" style="border: 1px dashed #8a6d3b; padding: 3px 5px; margin: 3px 5px 3px 0;">ADD ON</div>
												@endif
												@if($isAutoBooked == 1)
													@if($ticketBooking->mapping->map_type_id == config("evibe.ticket.type.add-on"))
														<div class="auto-booking-profile-image" data-id="{{ $ticketBooking->id }}"></div>
														{!! $ticketBooking->booking_info !!}
													@else
														<div class="auto-booking-profile-image col-xs-5 text-center no-pad-l no-pad-r" data-id="{{ $ticketBooking->id }}"></div>
														<div class="col-xs-7 no-pad-r">
															{!! $ticketBooking->booking_info !!}
														</div>
														<div class="clearfix"></div>
													@endif
												@else
													{{ ucwords($ticketBooking->booking_type_details) }}
													<div style="color: #9e9e9e; font-size: 14px;">{{ substr(strip_tags($ticketBooking->booking_info), 0, 43) . "..." }}</div>
												@endif
												<div class="font-13 normal-booking-order-details" style="color: #448aff;" data-ticket-booking-id="{{ $ticketBooking->id }}">
													<span class="text-underline">Show Order Details</span></div>
												<span class="hide order-details-{{ $ticketBooking->id }}">{!! $ticketBooking->booking_info !!}</span>
											</div>
											@if(!is_null($ticketBooking->coupon_id))
												<div class="text-bold font-20" style="color: #656565;">@priceWc($ticketBooking->booking_amount - $ticketBooking->discount_amount)</div>
												<span class="text-bold text-success font-12">(&#8377; {{ $ticketBooking->discount_amount }} Coupon Discount)</span>
											@else
												<div class="text-bold font-20" style="color: #656565;">@priceWc($ticketBooking->booking_amount)</div>
											@endif
										</div>
										<h6 class="no-mar-b text-bold">
											Party Venue:
										</h6>
										<div>
											@if($isVenueBooking == 1)
												<a href="{{ $mapLink }}" target="_blank">Google Maps Link</a><br>
											@endif
											@if($ticketBooking->is_venue_booking && $ticketBooking->provider && $ticketBooking->provider->name)
												<span style="color: #656565;" class="text-bold font-16 lh-24">{{ $ticketBooking->provider->name }}</span>
											@endif
											<div style="width: 280px;">{{ $venueAddress }}</div>
										</div>
										<h6 class="no-mar-b text-bold text-e">
											Party Coordinator:
										</h6>
										<div style="background-color: #F5F5F5;">
											@if($partner && $ticket->status_id == config("evibe.ticket.status.booked"))
												<div style="padding: 10px;">
													<i class="glyphicon glyphicon-user"></i>&#160;<span style="color: #ED3E72">{{ $partner->person }}</span><br>
													<div class="mar-t-5">
														<i class="glyphicon glyphicon-phone"></i>&#160;<span style="color: #ED3E72">{{ $partner->phone }}</span>
													</div>
												</div>
											@else
												<div class="text-center">
													<img src="{{ config("evibe.gallery.host") . "/img/icons/to_be_confirmed.png" }}" alt="To be confirmed" style="height: 100px;">
												</div>
											@endif
										</div>
										<h6 class="no-mar-b text-bold">
											Payment Info:
										</h6>
										<div>
											@if($ticketBooking->booking_amount - $ticketBooking->advance_amount == 0)
												<div class="mar-t-5">
													<span class="text-success text-bold pad-5" style="border: 1px dashed #3c763d"><span class="glyphicon glyphicon-thumbs-up"></span>PAID</span>
												</div>
											@else
												Amount Paid:
												@if(!is_null($ticketBooking->coupon_id))
													<div class="text-success text-bold font-20 in-blk">&#160;@priceWc($ticketBooking->advance_amount - $ticketBooking->discount_amount)</div>
												@else
													<div class="text-success text-bold font-20 in-blk">&#160;@priceWc($ticketBooking->advance_amount)</div>
												@endif
												<br>
												Balance Amount:
												<div class="text-bold font-20 in-blk" style="color: #656565;">&#160;@priceWc($ticketBooking->booking_amount - $ticketBooking->advance_amount)</div>
												<div class="text-info font-13">
													<i class="glyphicon glyphicon-info-sign"></i> Please carry cash
												</div>
											@endif
											@php $totalBookingAmount = $totalBookingAmount + $ticketBooking->booking_amount;
											$totalAdvanceAmount = $totalAdvanceAmount + $ticketBooking->advance_amount - $ticketBooking->discount_amount; @endphp
										</div>
										<hr class="hr-text no-pad">
									@endforeach
								</div>
								<div class="col-lg-3 col-md-3 text-center mar-b-15">
									<div class="mar-t-10">
										<a data-ticket-id="{{ $firstBooking->ticket_id }}" style="border-radius: 5px;" class="btn btn-warning full-width text-bold need-more-service">Add Other Services</a>
									</div>
									<div class="mar-t-10">
										<a data-ticket-id="{{ $firstBooking->ticket_id }}" style="border-radius: 5px;" class="btn btn-warning full-width text-bold update-party-venue">Update My </a>
									</div>
									<div class="mar-t-10">
										<a data-ticket-id="{{ $firstBooking->ticket_id }}" style="border-radius: 5px;"
												data-total-amount="{{ $totalBookingAmount }}"
												data-time-diff="{{ $minBookTime - time() }}"
												data-advance-amount="{{ $totalAdvanceAmount }}" class="btn btn-warning full-width text-bold request-cancellation">Request Cancellation</a>
									</div>
									<div class="mar-t-10">
										<a data-ticket-id="{{ $firstBooking->ticket_id }}" style="border-radius: 5px;" class="btn btn-warning full-width text-bold raise-complaint">Raise Complaint</a>
									</div>
									@if(isset($mergeTicket["needProof"]) && $mergeTicket["needProof"])
										<div class="mar-t-10">
											<a data-ticket-id="{{ $firstBooking->ticket_id }}" style="border-radius: 5px;" class="btn btn-warning full-width text-bold upload-id-proofs">Upload Id Proofs</a>
										</div>
									@endif
								</div>
								<div class="clearfix"></div>
								<div class="ref-images pad-l-15 pad-r-15">
									@if($mergeTicket["referenceImages"]->count() > 0)
										<h6 class="no-mar-b text-bold">
											Reference Images:
										</h6>
										@foreach($mergeTicket["referenceImages"] as $bookingRefImage)
											<a href="{{ $bookingRefImage->getOriginalImagePath() }}" target="_blank">
												<img src="{{ $bookingRefImage->getResultsImagePath() }}" style="max-width: 100px; margin-right: 10px; margin-bottom: 10px;">
											</a>
										@endforeach
									@endif
								</div>
								<div class="ref-images pad-l-15 pad-r-15">
									@if(count($mergeTicket["customerProofs"]))
										@php
											$invalidProofCount = 0;
											$validProofCount = 0;
										@endphp
										@foreach($mergeTicket["customerProofs"] as $customerProof)
											@if($customerProof->rejected_at)
												@php $invalidProofCount++; @endphp
											@endif
											@if($customerProof->approved_at)
												@php $validProofCount++; @endphp
											@endif
										@endforeach
										<h6 class="no-mar-b text-bold">
											<span>Uploaded Proofs: </span>
											@if($validProofCount && ($validProofCount == count($mergeTicket["customerProofs"])))
												<span class="tmo-proofs-success-msg">Validation successful</span>
											@endif
										</h6>
										@if($invalidProofCount)
											<div class="mar-b-5">Your uploaded proofs have failed to get validated. Kindly upload valid proofs</div>
											<a data-ticket-id="{{ $firstBooking->ticket_id }}" style="border-radius: 5px;" class="btn btn-danger text-bold upload-id-proofs mar-b-10">Upload Id Proofs</a>
										@else
											<div class="mar-t-5">
												@foreach($mergeTicket["customerProofs"] as $customerProof)
													<a href="{{ $customerProof->getOriginalImagePath() }}" target="_blank">
														<img src="{{ $customerProof->getResultsImagePath() }}" style="max-width: 100px; margin-right: 10px; margin-bottom: 10px;">
													</a>
												@endforeach
											</div>
										@endif
									@elseif(isset($mergeTicket["needProof"]) && $mergeTicket["needProof"])
										<div class="mar-b-5">Valid ID proof is yet to be uploaded</div>
										<a data-ticket-id="{{ $firstBooking->ticket_id }}" style="border-radius: 5px;" class="btn btn-danger text-bold upload-id-proofs mar-b-10">Upload Id Proofs</a>
									@endif
								</div>
							</div>
							@if(isset($mergeTicket['addOns']) && count($mergeTicket['addOns']) && (time() < ($minBookTime - (3*3600))))
								<hr class="no-mar no-pad">
								<div class="tmo-ticket-add-ons pad-l-15 pad-r-15" data-merge-ticket-id="{{ $mergeTicketKey }}">
									<div>
										<div class="in-blk">
											<h6 class="no-mar-b text-bold mar-r-10">
												Add-ons for your party:
											</h6>
										</div>
										<div class="in-blk">
											<span id="addOnsMergeTicketPrice_{{ $mergeTicketKey }}" class="hide tmo-add-ons-summary">
											<b><span class="tmo-add-ons-count"></span></b> add-ons selected - <b><span><span class="rupee-font">&#8377; </span><span class="tmo-add-ons-price">0</span></span></b>
											<a href="#" class="tmo-add-ons-remove-btn mar-l-10" data-merge-ticket-id="{{ $mergeTicketKey }}">(remove all)</a>
										</span>
										</div>
									</div>
									<div id="addOnsMergeTicket_{{ $mergeTicketKey }}" class="tmo-add-ons-carousel mar-t-20 mar-b-10" data-slides-to-show="1">
										@foreach($mergeTicket['addOns'] as $addOn)
											<div class="tmo-add-on pad-l-5 pad-r-5">
												@include('occasion.util.add-ons.card')
											</div>
										@endforeach
									</div>
									<div class="text-center">
										<div data-merge-ticket-id="{{ $mergeTicketKey }}" data-primary-ticket-id="{{ $mergeTicket['ticketIds'][0] }}" class="btn btn-primary btn-br-4 include-selected-add-ons-btn mar-t-10 mar-b-10">Include Selected Add-ons</div>
									</div>
								</div>
							@endif
							<div class="clearfix"></div>
						</div>
					</div>
				@else
					<div class="mar-b-20 tmo-ticket-wrap">
						<div class="tmo-ticket">
							<div class="pad-10 no-pad-t header">
								<div class="pull-left pad-t-5">
									<span class="font-10" style="line-height: 10px;">PARTY DATE :</span>
									<br>
									<span class="font-16 text-bold" style="line-height: 16px;">
									<span class="glyphicon glyphicon-calendar"> </span>
										@if($minBookTime == $maxBookTime)
											{{ date('d M Y, h:i A', $minBookTime) }}
										@else
											{{ date('d M Y, h:i A', $minBookTime) . " - " . date('d M Y, h:i A', $maxBookTime) }}
										@endif
								</span>
								</div>
								<div class="pull-right pad-t-5">
									<span class="font-10" style="line-height: 10px;">ORDER ID :</span>
									<br>
									<span class="font-16 text-bold" style="line-height: 16px;">
										@if($mergeTicket["enquiryId"] == "")
											{{ "EVB-BK-" . $firstBooking->ticket_id }}
										@else
											{{ $mergeTicket["enquiryId"] }}
										@endif
									</span>
								</div>
								<div class="clearfix"></div>
							</div>
							<hr class="no-mar no-pad">
							@if($isAutoPay == "YES")
								<div class="mar-l-10 mar-r-10 mar-t-10">
									<div class="alert alert-warning">
										<i class="glyphicon glyphicon-alert"></i> We are checking the availability and will confirm your booking within 4 business hours.
									</div>
								</div>
							@endif
							<div class="tmo-ticket-booking">
								<div class="col-lg-9 col-md-9">
									<table class="table mar-t-10 mar-b-10">
										<thead>
										<tr>
											<th></th>
											<th>Order</th>
											<th>Party Venue</th>
											<th style="color: #ED3E72; background-color: #F5F5F5;">Party Coordinator</th>
											<th>Payment Info</th>
										</tr>
										</thead>
										<tbody>
										@foreach($ticketBookings as $ticketBooking)
											@php
												$count++;
												$ticket = isset($mergeTicket[$ticketBooking->ticket_id]) ? $mergeTicket[$ticketBooking->ticket_id] : "";
												$isAutoBooked = $ticket != "" ? $ticket->is_auto_booked : "";
												$mapLink = $ticket != "" ? $ticket->map_link : "";
												$venueAddress = $ticket != "" ? $ticket->venue_address : "";
												$balanceAmount = $ticketBooking->advance_amount ? ($ticketBooking->booking_amount - $ticketBooking->advance_amount) : ($ticketBooking->booking_amount - $ticketBooking->token_amount);
												$partner = $ticketBooking->provider;
											@endphp
											<tr>
												<td>
													({{ $count }})
												</td>
												<td>
													<div class="no-mar tmo-order-info" style="font-weight: 400; font-size: 16px; color: #333333; width: 200px;">
														@if($ticketBooking->mapping->map_type_id == config("evibe.ticket.type.add-on"))
															@foreach($ticketBooking->gallery as $gallery)
																<div class="in-blk mar-b-10">
																	<a href="{{ $gallery->getResultsImagePath() }}" target="_blank">
																		<img src="{{ $gallery->getResultsImagePath() }}" alt="{{ $gallery->title }}" style="height: 100px;">
																	</a>
																</div>
															@endforeach
															<div class="clearfix"></div>
															<span class="text-warning text-bold" style="border: 1px dashed #8a6d3b; padding: 3px 5px; margin-right: 5px;">ADD ON</span>
														@endif
														@if($isAutoBooked == 1)
															<div class="auto-booking-profile-image" data-id="{{ $ticketBooking->id }}"></div>
															{!! $ticketBooking->booking_info !!}
														@else
															{{ ucwords($ticketBooking->booking_type_details) }}
															<div style="color: #9e9e9e; font-size: 14px;">{{ substr(strip_tags($ticketBooking->booking_info), 0, 43) . "..." }}</div>
														@endif
														<div class="font-13 cur-point normal-booking-order-details" data-ticket-booking-id="{{ $ticketBooking->id }}">
															<span class="text-underline" style="color: #448aff;">Show Order Details</span>
														</div>
														<span class="hide order-details-{{ $ticketBooking->id }}">{!! $ticketBooking->booking_info !!}</span>
													</div>
													@if(!is_null($ticketBooking->coupon_id))
														<div class="text-bold font-20" style="color: #656565;">@priceWc($ticketBooking->booking_amount - $ticketBooking->discount_amount)</div>
														<span class="text-bold text-success font-12">(&#8377; {{ $ticketBooking->discount_amount }} Coupon Discount)</span>
													@else
														<div class="text-bold font-20" style="color: #656565;">@priceWc($ticketBooking->booking_amount)</div>
													@endif
												</td>
												<td>
													@if($isVenueBooking == 1)
														<a href="{{ $mapLink }}" target="_blank">Google Maps Link</a>
														<br>
													@endif
													@if($ticketBooking->is_venue_booking && $ticketBooking->provider && $ticketBooking->provider->name)
														<span style="color: #656565;" class="text-bold font-16 lh-24">{{ $ticketBooking->provider->name }}</span>
													@endif
													<div style="width: 200px;">{{ $venueAddress }}</div>
												</td>
												<td style="background-color: #F5F5F5;">
													<div class="font-15">
														@if($partner && $ticket->status_id == config("evibe.ticket.status.booked"))
															<i class="glyphicon glyphicon-user"></i>&#160;
															<span style="color: #ED3E72">{{ $partner->person }}</span>
															<div class="mar-t-5">
																<i class="glyphicon glyphicon-phone"></i>&#160;<span style="color: #ED3E72">{{ $partner->phone }}</span>
															</div>
														@else
															<img src="{{ config("evibe.gallery.host") . "/img/icons/to_be_confirmed.png" }}" alt="To be confimed" style="height: 100px;">
														@endif
													</div>
												</td>
												<td>
													@if($ticketBooking->booking_amount - $ticketBooking->advance_amount == 0)
														<div class="mar-t-5">
															<div class="text-success text-bold pad-5 in-blk" style="border: 1px dashed #3c763d">
																<i class="glyphicon glyphicon-thumbs-up"></i>
																<div class="in-blk pad-l-3">PAID</div>
															</div>
														</div>
													@else
														Amount Paid:
														@if(!is_null($ticketBooking->coupon_id))
															<div class="text-success text-bold font-20 in-blk">&#160;@priceWc($ticketBooking->advance_amount - $ticketBooking->discount_amount)</div>
														@else
															<div class="text-success text-bold font-20 in-blk">&#160;@priceWc($ticketBooking->advance_amount)</div>
														@endif
														<br>
														Balance Amount:
														<div class="text-bold font-20 in-blk" style="color: #656565;">&#160;@priceWc($ticketBooking->booking_amount - $ticketBooking->advance_amount)</div>
														<div class="text-info font-13">
															<i class="glyphicon glyphicon-info-sign"></i> Please carry cash
														</div>
													@endif
													@php $totalBookingAmount = $totalBookingAmount + $ticketBooking->booking_amount;
													$totalAdvanceAmount = $totalAdvanceAmount + $ticketBooking->advance_amount - $ticketBooking->discount_amount; @endphp
												</td>
											</tr>
										@endforeach
										</tbody>
									</table>
								</div>
								<div class="col-lg-3 col-md-3 text-center mar-b-15">
									<div class="mar-t-10">
										<a data-ticket-id="{{ $firstBooking->ticket_id }}" style="border-radius: 5px;" class="btn btn-warning full-width text-bold need-more-service">Add Other Services</a>
									</div>
									<div class="mar-t-10">
										<a data-ticket-id="{{ $firstBooking->ticket_id }}" style="border-radius: 5px;" class="btn btn-warning full-width text-bold update-party-venue">Update My Order</a>
									</div>
									<div class="mar-t-10">
										<a data-ticket-id="{{ $firstBooking->ticket_id }}" style="border-radius: 5px;"
												data-total-amount="{{ $totalBookingAmount }}"
												data-time-diff="{{ $minBookTime - time() }}"
												data-advance-amount="{{ $totalAdvanceAmount }}" class="btn btn-warning full-width text-bold request-cancellation">Request Cancellation</a>
									</div>
									<div class="mar-t-10">
										<a data-ticket-id="{{ $firstBooking->ticket_id }}" style="border-radius: 5px;" class="btn btn-warning full-width text-bold raise-complaint">Raise Complaint</a>
									</div>
									@if(isset($mergeTicket["needProof"]) && $mergeTicket["needProof"])
										<div class="mar-t-10">
											<a data-ticket-id="{{ $firstBooking->ticket_id }}" style="border-radius: 5px;" class="btn btn-warning full-width text-bold upload-id-proofs">Upload Id Proofs</a>
										</div>
									@endif
								</div>
								<div class="clearfix"></div>
								<div class="ref-images pad-l-15 pad-r-15">
									@if($mergeTicket["referenceImages"]->count() > 0)
										<h6 class="no-mar-b text-bold">
											Reference Images:
										</h6>
										@foreach($mergeTicket["referenceImages"] as $bookingRefImage)
											<a href="{{ $bookingRefImage->getOriginalImagePath() }}" target="_blank">
												<img src="{{ $bookingRefImage->getResultsImagePath() }}" style="max-height: 100px; margin-right: 10px; margin-bottom: 10px;">
											</a>
										@endforeach
									@endif
								</div>
								<div class="ref-images pad-l-15 pad-r-15">
									@if(count($mergeTicket["customerProofs"]))
										@php
											$invalidProofCount = 0;
											$validProofCount = 0;
										@endphp
										@foreach($mergeTicket["customerProofs"] as $customerProof)
											@if($customerProof->rejected_at)
												@php $invalidProofCount++; @endphp
											@endif
											@if($customerProof->approved_at)
												@php $validProofCount++; @endphp
											@endif
										@endforeach
										<h6 class="no-mar-b text-bold">
											<span>Uploaded Proofs: </span>
											@if($validProofCount && ($validProofCount == count($mergeTicket["customerProofs"])))
												<span class="tmo-proofs-success-msg">Validation successful</span>
											@elseif(!$invalidProofCount)
												<span class="tmo-proofs-progress-msg">Validation In-progress</span>
											@endif
										</h6>
										@if($invalidProofCount)
											<div class="mar-b-5">Your uploaded proofs have failed to get validated. Kindly upload valid proofs</div>
											<a data-ticket-id="{{ $firstBooking->ticket_id }}" style="border-radius: 5px;" class="btn btn-danger text-bold upload-id-proofs mar-b-10">Upload Id Proofs</a>
										@else
											<div class="mar-t-5">
												@foreach($mergeTicket["customerProofs"] as $customerProof)
													<a href="{{ $customerProof->getOriginalImagePath() }}" target="_blank">
														<img src="{{ $customerProof->getResultsImagePath() }}" style="max-width: 100px; margin-right: 10px; margin-bottom: 10px;">
													</a>
												@endforeach
											</div>
										@endif
									@elseif(isset($mergeTicket["needProof"]) && $mergeTicket["needProof"])
										<div class="mar-b-5">Valid ID proof is yet to be uploaded</div>
										<a data-ticket-id="{{ $firstBooking->ticket_id }}" style="border-radius: 5px;" class="btn btn-danger text-bold upload-id-proofs mar-b-10">Upload Id Proofs</a>
									@endif
								</div>
							</div>
							@if(isset($mergeTicket['addOns']) && count($mergeTicket['addOns']) && (time() < ($minBookTime - (3*3600))))
								<hr class="no-mar no-pad">
								<div class="tmo-ticket-add-ons pad-l-15 pad-r-15" data-merge-ticket-id="{{ $mergeTicketKey }}">
									<div>
										<div class="in-blk">
											<h6 class="no-mar-b text-bold mar-r-10">
												Add-ons for your party:
											</h6>
										</div>
										<div class="in-blk">
											<span id="addOnsMergeTicketPrice_{{ $mergeTicketKey }}" class="hide tmo-add-ons-summary">
											<b><span class="tmo-add-ons-count"></span></b> add-ons selected - <b><span><span class="rupee-font">&#8377; </span><span class="tmo-add-ons-price">0</span></span></b>
											<a href="#" class="tmo-add-ons-remove-btn mar-l-10" data-merge-ticket-id="{{ $mergeTicketKey }}">(remove all)</a>
										</span>
										</div>
									</div>
									<div id="addOnsMergeTicket_{{ $mergeTicketKey }}" class="tmo-add-ons-carousel mar-t-20 mar-b-10" data-slides-to-show="3">
										@foreach($mergeTicket['addOns'] as $addOn)
											<div class="tmo-add-on pad-l-5 pad-r-5">
												@include('occasion.util.add-ons.card')
											</div>
										@endforeach
									</div>
									<div class="text-center">
										<div data-merge-ticket-id="{{ $mergeTicketKey }}" data-primary-ticket-id="{{ $mergeTicket['ticketIds'][0] }}" class="btn btn-primary btn-br-4 include-selected-add-ons-btn mar-t-10 mar-b-10">Include Selected Add-ons</div>
									</div>
								</div>
							@endif
							<div class="clearfix"></div>
						</div>
					</div>
				@endif
			@endforeach

			<div class="in-blk mar-b-20">
				<h6 class="mar-b-10">Cancellation Policy:</h6>
				<table class="table table-bordered no-mar-b text-muted">
					<thead>
					<tr>
						<th>Party Cancellation Time</th>
						<th>Refund Percentage*</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td>0 - 24 hours before party</td>
						<td>0%</td>
					</tr>
					<tr>
						<td>1 day- 3 days before party</td>
						<td>25%</td>
					</tr>
					<tr>
						<td>4 days - 10 days before party</td>
						<td>50%</td>
					</tr>
					<tr>
						<td>11 days or above before party</td>
						<td>70%</td>
					</tr>
					</tbody>
				</table>
				<div class="mar-t-5 text-muted">
					* Refund Percentage will be calculated on <b>Total Advance Amount</b>
				</div>
			</div>
		</div>
	@endif
	@if($data['productBookings'])
		@php
			$productCount = 0;
		@endphp
		<div class="pad-l-10 pad-r-10 col-lg-10 col-lg-offset-1" style="min-height: calc(100vh - 430px);">
			<h4 class="text-center mar-t-10">My Orders</h4>
			<div class="mar-b-20 tmo-ticket-wrap">
				<div class="tmo-ticket">
					<div class="tmo-ticket-booking">
						<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
							<table class="table mar-t-10 mar-b-10">
								<thead>
								<tr>
									<th></th>
									<th>Order</th>
									<th>Shipping Address</th>
									<th>Order Status</th>
								</tr>
								</thead>
								<tbody>
								@foreach($data['productBookings'] as $productBooking)
									@php
										$productCount++;
									@endphp
									<tr>
										<td>
											{{ $productCount }}
										</td>
										<td>
											<div class="no-mar tmo-order-info" style="font-weight: 400; font-size: 16px; color: #333333; width: 200px;">
												{!! $productBooking->booking_info !!}
											</div>
											<div class="text-bold font-20" style="color: #656565;">@priceWc($productBooking->booking_amount)</div>
										</td>
										<td>
											<div>{{ $productBooking->address }}</div>
											<div>{{ $productBooking->city }}</div>
											<div>{{ $productBooking->state }}</div>
											<div>{{ $productBooking->zip_code }}</div>
										</td>
										<td>
											{{ $productBooking->status->name }}
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	@endif
	@if(!$data['mergeTickets'] && !$data['productBookings'])
		<div class="mar-t-30" style="min-height: calc(100vh - 530px);">
			<div class="text-center"><img src="https://gallery.evibe.in/main/img/icons/caution-sign.png"></div>
			<h5 class="text-center mar-t-10 text-danger"> Sorry, you do not have any upcoming bookings yet.</h5>
		</div>
	@endif
	<div class="clearfix"></div>
@endsection

@section("modals")
	@parent
	@include('login.track-my-order.modals')
@endsection

@section("footer")
	<div class="footer-wrap">
		@include('base.home.footer.footer-non-city')
	</div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			$("#tmoNewPartyDate").datetimepicker({
				"timepicker": true,
				"scrollInput": false,
				"scrollMonth": false,
				"closeOnDateSelect": false,
				"minDate": 0,
				"format": 'd-m-Y H:i',
				"disabledDates": window.disableDates(),
				onSelectDate: function (ct, $i) {
					$i.parent().addClass("is-dirty");
				}
			});
			var $ticketId = "";
			var urlParams = window.getUrlParameters();
			var $ref = typeof urlParams["ref"] !== "undefined" ? urlParams["ref"] : "";

			$('.lazy-loading').each(function () {
				var img = $(this);
				img.attr('src', img.data('src'));
			});

			$(".update-party-date-time").on("click", function () {
				closeAllModals();

				$("#modalUpdatePartyDate").modal("show");
				$ticketId = $(this).data("ticket-id");
			});

			$(".need-more-service").on("click", function () {
				closeAllModals();

				$("#modalNeedMoreServices").modal("show");
				$ticketId = $(this).data("ticket-id");
			});

			$(".request-cancellation").on("click", function () {
				closeAllModals();
				showLoading();

				$.ajax({
					url: '/track/request/cancel/charges/' + $(this).data("ticket-id"),
					type: 'POST',
					dataType: 'json',
					data: {}
				}).done(function (data, textStatus, jqXHR) {
					if (data.success) {
						hideLoading();

						var bookingAmount = data.bookingAmount;
						var advanceAmount = data.advanceAmount;
						var cancellationCharges = data.cancellationCharges;
						var refundAmount = data.refundAmount;

						$(".tmo-cancel-total").empty().append(bookingAmount);
						$(".tmo-cancel-advance").empty().append(advanceAmount);
						$(".tmo-cancel-charges").empty().append(cancellationCharges.toFixed(1));
						$(".tmo-cancel-refund").empty().append(refundAmount.toFixed(1));

						$("#modalRequestCancellation").modal("show");
					} else {
						var error = "Some error occurred while calculating cancellation charges.";
						if (data.error) {
							error = data.error;
						}
						window.showNotyError(error);
					}
				}).fail(function (jqXHR, textStatus, errorThrown) {
					window.showNotyError("Some error occurred while fetching cancellation changes");
				});

				$ticketId = $(this).data("ticket-id");
			});

			$(".update-party-venue").on("click", function () {
				closeAllModals();

				$("#modalUpdatePartyVenue").modal("show");
				$ticketId = $(this).data("ticket-id");
			});

			$(".upload-id-proofs").on("click", function () {
				closeAllModals();

				$("#modalUploadOrderProofs").modal("show");
				$ticketId = $(this).data("ticket-id");
			});

			$(".raise-complaint").on("click", function () {
				closeAllModals();

				$("#modalRaiseComplaint").modal("show");
				$ticketId = $(this).data("ticket-id");
			});

			$("#proofTMOUploadBtn").on('click', function (event) {
				event.preventDefault();
				uploadCustomerTMOOrderProofs();
			});

			var slickJs = "<?php echo elixir('js/util/slick.js'); ?>";
			$.getScript(slickJs, function () {

				$('.tmo-add-ons-carousel').each(function (i, el) {
					$(el).slick({
						dots: false,
						infinite: false,
						speed: 300,
						slidesToShow: $(el).data('slides-to-show'),
						slidesToScroll: $(el).data('slides-to-show'),
						arrows: true
					});
				});
			});

			$('.add-on-add-unit').on('click', function (event) {
				event.preventDefault();

				var parentElement = $(this).parent();

				var addOnId = parentElement.data('id');
				var addOnPrice = parentElement.data('price');
				var addOnPriceWorth = parentElement.data('price-worth');
				var addOnMaxCount = parentElement.data('max-units');
				var addOnCount = parentElement.data('count-units');
				var addOnName = parentElement.data('name');

				if (addOnCount < addOnMaxCount) {
					addOnCount++;

					/* update all instances of this add-on */
					$.each($('.add-on-cta-list'), function (key, value) {
						if ($(this).data('id') == addOnId) {
							$(this).data('count-units', addOnCount);

							$(this).parent().parent().parent().parent().parent().css({"border-color": "#30AC15"});
							$(this).parent().parent().parent().parent().children('.shortlisted-label').removeClass('hide');
							$(this).children('.add-on-add-unit').addClass('hide');
							$(this).children('.add-on-remove-unit').removeClass('hide');
						}
					});

				} else {
					window.showNotyError("You have added maximum number of units of this add-on applicable");
				}

				updateTMOAddOnsData();

			});

			$('.add-on-remove-unit').on('click', function (event) {
				event.preventDefault();

				var parentElement = $(this).parent();

				var addOnId = parentElement.data('id');
				var addOnPrice = parentElement.data('price');
				var addOnPriceWorth = parentElement.data('price-worth');
				var addOnMaxCount = parentElement.data('max-units');
				var addOnCount = parentElement.data('count-units');

				if (addOnCount > 0) {
					addOnCount--;

					$.each($('.add-on-cta-list'), function (key, value) {
						if ($(this).data('id') == addOnId) {
							$(this).data('count-units', addOnCount);

							if (addOnCount == 0) {
								$(this).parent().parent().parent().parent().parent().css({"border-color": "#EFEFEF"});
								$(this).parent().parent().parent().parent().children('.shortlisted-label').addClass('hide');
								$(this).children('.add-on-add-unit').removeClass('hide');
								$(this).children('.add-on-remove-unit').addClass('hide');
							}
						}
					});

					/*
					 parentElement.data('count-units', addOnCount);
					 listParent.siblings('.add-on-unit-count').text(addOnCount);
					 */

				} else {
					/* window.showNotyError("You have already removed this add-on"); */
				}

				updateTMOAddOnsData();

			});

			var $selectedAddOnsPrice = 0;
			var $selectedAddOnsPriceWorth = 0;

			$('.include-selected-add-ons-btn').on('click', function (event) {
				var $mergeTicketId = $(this).data('merge-ticket-id');
				var $primaryTicketId = $(this).data('primary-ticket-id');
				var $id = '#addOnsMergeTicket_' + $mergeTicketId;
				var $addOn = $($id + ' .add-on-cta-list');

				$selectedAddOnsPrice = 0;
				$selectedAddOnsPriceWorth = 0;
				$('.tmo-modal-selected-add-ons-list').empty();

				if ($addOn.length > 0) {

					var addOns = [];
					$.each($addOn, function (key, value) {
						if ($(this).data('count-units') > 0) {
							addOns.push({
								'id': $(this).data('id'),
								'name': $(this).data('name'),
								'price': $(this).data('price'),
								'priceWorth': $(this).data('price-worth'),
								'countUnits': $(this).data('count-units')
							});
						}
					});

					console.log(addOns.length);
					console.log(addOns);

					if (addOns.length) {
						$element = "<div>";
						for (var i = 0; i < addOns.length; i++) {
							/* compute price */
							$selectedAddOnsPrice = $selectedAddOnsPrice + addOns[i]['price'];
							$selectedAddOnsPriceWorth = $selectedAddOnsPrice + addOns[i]['priceWorth'];

							/* form element and append */
							$element += "<div class='text-left tmo-modal-add-on' data-add-on-name='" + addOns[i]['name'] + "' data-add-on-price='" + addOns[i]['price'] + "'>" +
								"<div class='col-xs-5 col-sm-8 col-md-8 col-lg-8'>" + addOns[i]['name'] + "</div>" +
								"<div class='col-xs-3 col-sm-2 col-md-2 col-lg-2 no-pad-r	'>Rs. " + addOns[i]['price'] + "</div>" +
								"<div class='col-xs-4 col-sm-2 col-md-2 col-lg-2'><a href='#' class='tmo-modal-add-on-remove' data-add-on-id='" + addOns[i]['id'] + "' data-merge-ticket-id='" + $mergeTicketId + "'>Remove</a></div>" +
								"<div class='clearfix'></div>" +
								"<hr>" +
								"</div>";
						}
						$element += "<div class='text-left'>" +
							"<div class='col-xs-5 col-sm-8 col-md-8 col-lg-8'><b>Total Add-ons Amount</b></div>" +
							"<div class='col-xs-7 col-sm-4 col-md-4 col-lg-4'><b>Rs. <span class='tmo-modal-add-ons-total-price'>" + $selectedAddOnsPrice + "</span></b></div>" +
							"</div>";
						$element += "<div class='clearfix'></div>";
						$element += "</div>";

						$('.tmo-modal-selected-add-ons-list').append($element);
						$('.tmo-modal-submit-add-ons-btn').data('merge-ticket-id', $mergeTicketId);
						$('.tmo-modal-submit-add-ons-btn').data('primary-ticket-id', $primaryTicketId);

						$('#previewSelectedAddOns').modal('show');

						/* functionality to remove from modal */
					} else {
						window.showNotyError("Kindly select any available add-ons for your party and then include them");
					}
				}
			});

			$('.tmo-modal-selected-add-ons-list').on('click', '.tmo-modal-add-on-remove', function (event) {
				event.preventDefault();

				var $addOnRemoveBtn = $(this);
				var $addOnId = $(this).data('add-on-id');
				var $mergeTicketId = $(this).data('merge-ticket-id');

				var $id = '#addOnsMergeTicket_' + $mergeTicketId;
				var $mergeTicketAddOns = $($id + ' .add-on-cta-list');

				if ($mergeTicketAddOns.length > 0) {
					$.each($mergeTicketAddOns, function (key, value) {
						if ($(this).data('id') == $addOnId) {
							var addOnCount = $(this).data('count-units');
							if (addOnCount > 0) {
								addOnCount--;

								$(this).data('count-units', addOnCount);

								if (addOnCount == 0) {
									$(this).parent().parent().parent().parent().parent().css({"border-color": "#EFEFEF"});
									$(this).parent().parent().parent().parent().children('.shortlisted-label').addClass('hide');
									$(this).children('.add-on-add-unit').removeClass('hide');
									$(this).children('.add-on-remove-unit').addClass('hide');
								}

								/* Remove from modal */
								$addOnRemoveBtn.parent().parent().remove();

								/* Update overall price */
								$selectedAddOnsPrice = $selectedAddOnsPrice - $(this).data('price');
								$selectedAddOnsPriceWorth = $selectedAddOnsPriceWorth - $(this).data('price-worth');
								$('.tmo-modal-add-ons-total-price').text($selectedAddOnsPrice);

								/*
								 parentElement.data('count-units', addOnCount);
								 listParent.siblings('.add-on-unit-count').text(addOnCount);
								 */

							} else {
								/* window.showNotyError("You have already removed this add-on"); */
							}
						}
					});
				}

				updateTMOAddOnsData();
			});

			$('.tmo-add-ons-remove-btn').on('click', function (event) {
				event.preventDefault();

				var $mergeTicketId = $(this).data('merge-ticket-id');

				var $id = '#addOnsMergeTicket_' + $mergeTicketId;
				var $mergeTicketAddOns = $($id + ' .add-on-cta-list');

				if ($mergeTicketAddOns.length > 0) {
					$.each($mergeTicketAddOns, function (key, value) {

						$(this).data('count-units', 0);
						$(this).parent().parent().parent().parent().parent().css({"border-color": "#EFEFEF"});
						$(this).parent().parent().parent().parent().children('.shortlisted-label').addClass('hide');
						$(this).children('.add-on-add-unit').removeClass('hide');
						$(this).children('.add-on-remove-unit').addClass('hide');

					});
				}

				updateTMOAddOnsData();
			});

			function updateTMOAddOnsData() {

				$('.tmo-ticket-add-ons').each(function (i, el) {
					var $mergeTicketId = $(el).data('merge-ticket-id');

					var $id = '#addOnsMergeTicket_' + $mergeTicketId;
					var $addOn = $($id + ' .add-on-cta-list');

					$selectedAddOnsPrice = 0;
					$selectedAddOnsPriceWorth = 0;

					if ($addOn.length > 0) {

						var addOns = [];
						$.each($addOn, function (key, value) {
							if ($(this).data('count-units') > 0) {
								addOns.push({
									'id': $(this).data('id'),
									'name': $(this).data('name'),
									'price': $(this).data('price'),
									'priceWorth': $(this).data('price-worth'),
									'countUnits': $(this).data('count-units')
								});

								$selectedAddOnsPrice = $selectedAddOnsPrice + $(this).data('price');
								$selectedAddOnsPriceWorth = $selectedAddOnsPriceWorth + $(this).data('price-worth');
							}
						});

						var $addOnsSummaryId = '#addOnsMergeTicketPrice_' + $mergeTicketId;
						if (addOns.length) {
							$($addOnsSummaryId).removeClass('hide');
							$($addOnsSummaryId + ' .tmo-add-ons-price').text($selectedAddOnsPrice);
							$($addOnsSummaryId + ' .tmo-add-ons-count').text(addOns.length);
						} else {
							/* hide just in case */
							$($addOnsSummaryId).addClass('hide');
						}
					}
				});
			}

			$('.tmo-modal-submit-add-ons-btn').on('click', function () {
				var comments = "Customer has requested the following add-ons: ";
				$('.tmo-modal-add-on').each(function (i, el) {
					comments += $(el).data('add-on-name') + ": Rs. " + $(el).data('add-on-price') + "; ";
				});
				$requestType = "Need Add On Services";
				$requestReason = comments;

				$("#previewSelectedAddOns").modal("hide");
				$ticketId = $(this).data('primary-ticket-id');
				AjaxRequestTMO($requestType, $requestReason);
			});

			function uploadCustomerTMOOrderProofs() {
				var formData = new FormData();
				var customerProofFront = $("#customerTMOProofFront")[0].files;
				var customerProofBack = $("#customerTMOProofBack")[0].files;

				formData.append('proofType', $('#inpCustomerTMOProofType').val());
				formData.append('customerProofFront', customerProofFront[0]);
				formData.append('customerProofBack', customerProofBack[0]);

				var $ajaxUrl = "/track/upload/customer-proofs";

				$.ajax({
					url: $ajaxUrl,
					dataType: 'JSON',
					type: 'POST',
					processData: false,
					cache: false,
					contentType: false,
					data: formData,
					success: function (data) {
						if (data.success) {
							window.showNotySuccess("Your identification proofs have been uploaded successful. Our team will get in touch with you");
							setTimeout(function () {
								location.reload();
							}, 1500);
						} else {
							window.showNotyError(data.errorMsg);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						window.notifyTeam({
							"url": $ajaxUrl,
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			}

			$(".tmo-party-date-submit").on("click", function () {
				var partyDate = $("#tmoNewPartyDate").val();
				if (partyDate == "") {
					showNotyError("Please select the new party Date/Time.")
				} else {
					$requestType = "Party Date/Time Update";
					$requestReason = "Date: <b>" + partyDate + "</b><br>" + "Comments: <b>" + $("#tmoNewPartyDateComments").val() + "</b>";

					$("#modalUpdatePartyDate").modal("hide");
					AjaxRequestTMO($requestType, $requestReason);
				}
			});

			$(".tmo-need-more-services-submit").on("click", function () {
				var comments = $("#tmoNeedMorePartyServicesComments").val();
				if (comments == "") {
					showNotyError("Please enter your requirements.")
				} else {
					$requestType = "Add Other Services";
					$requestReason = "Comments: <b>" + comments + "</b>";

					$("#modalNeedMoreServices").modal("hide");
					AjaxRequestTMO($requestType, $requestReason);
				}
			});

			$(".tmo-request-cancellation-submit").on("click", function () {
				var comments = $("#tmoRequestCancellationComments").val();
				$requestType = "Request Cancellation";
				$requestReason = "Comments: <b>" + comments + "</b>";

				$("#modalRequestCancellation").modal("hide");
				AjaxRequestTMO($requestType, $requestReason);
			});

			$(".tmo-update-party-venue-submit").on("click", function () {
				var comments = $("#tmoUpdatePartyVenueComments").val();
				if (comments == "") {
					showNotyError("Please enter your requirements.")
				} else {
					$requestType = "Update My Order";
					$requestReason = "Comments: <b>" + comments + "</b>";

					$("#modalUpdatePartyVenue").modal("hide");
					AjaxRequestTMO($requestType, $requestReason);
				}
			});

			$(".tmo-raise-complaint-submit").on("click", function () {
				var comments = $("#tmoRaiseComplaintComments").val();
				if (comments == "") {
					showNotyError("Please enter your issue.")
				} else {
					$requestType = "Raise Complaint";
					$requestReason = comments;

					$("#modalRaiseComplaint").modal("hide");
					AjaxRequestTMO($requestType, $requestReason);
				}
			});

			$(".normal-booking-order-details").on("click", function () {
				$ticketBookingId = $(this).data("ticket-booking-id");
				$orderDetails = $(".order-details-" + $ticketBookingId).html();
				$("#showOrderDetails").find(".modal-body").empty().append($orderDetails);
				$("#showOrderDetails").modal("show");
			});

			(function getProfileImagesForAutoBooking() {
				let i = 0;
				var bookingIds = [];

				$(".auto-booking-profile-image").each(function () {
					bookingIds[i] = $(this).data("id");
					i++;
				});

				$.ajax({
					url: "/track/request/ab/profile-images",
					type: "POST",
					data: {"bookingIds": bookingIds},
					success: function (data) {
						if (data.success) {
							var images = data.profileImages;

							$(".auto-booking-profile-image").each(function () {
								var image = images[$(this).data("id")];
								if (image) {
									$(this).append("<a href=\"" + image + "\" target=\"_blank\">" +
										"<img src=\"" + image + "\" style=\"max-height: 100px;\"></a>");
								}
							});
						} else if (data.error) {
							showNotyError(data.error);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						window.showNotyError("An error occurred while submitting your request. Please try again later.");
						window.notifyTeam({
							"url": "track/request/ab/profile-images",
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			})();

			function AjaxRequestTMO($requestType, $requestReason) {
				showLoading();
				$.ajax({
					url: "/track/request/save/" + $ticketId,
					type: "POST",
					data: {"requestType": $requestType, "requestReason": $requestReason},
					success: function (data) {
						if (data.success) {
							hideLoading();
							if ($requestType === "Raise Complaint") {
								$("#modalTMOComplaintThankYou").modal("show");
							} else {
								$("#modalTMOThankYou").modal("show");
							}
						} else if (data.error) {
							hideLoading();
							showNotyError(data.error);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						hideLoading();
						window.showNotyError("An error occurred while submitting your request. Please try again later.");
						window.notifyTeam({
							"url": "track/request/save",
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			}

			function closeAllModals() {
				$("#modalUpdatePartyDate").modal("hide");
				$("#modalNeedMoreServices").modal("hide");
				$("#modalRequestCancellation").modal("hide");
				$("#modalUpdatePartyVenue").modal("hide");
				$("#modalRaiseComplaint").modal("hide");
			}
		});
	</script>
@endsection