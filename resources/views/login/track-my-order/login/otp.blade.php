@extends("layout.base")

@section("header")
	@include('base.home.header.header-top-city')
	<div class="header-border full-width mar-b-10"></div>
@endsection

@section("content")
	<div class="partner-login-wrap" role="main" style="margin:0; min-height: calc(100vh - 392px)">
		<div class="col-md-4 col-sm-8 col-md-offset-4 col-sm-offset-2 mar-t-30 mar-b-30 text-center">
			<h1 class="no-mar-b page-heading">
				Track My Order
				<hr class="hr-text no-mar no-pad">
			</h1>
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="margin-bottom: -15px; margin-top: 10px;">
				<input class="mdl-textfield__input mar-b-5 cur-disabled" type="text" name="tmoEnquiryPhone" id="tmoEnquiryPhone" value="{{ $data["phone"] }}" disabled="disabled"/>
				<label class="mdl-textfield__label cur-disabled" for="tmoEnquiryPhone">Phone Number</label>
			</div>
			<div class="clearfix"></div>
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="margin-bottom: -15px; margin-top: 10px;">
				<input class="mdl-textfield__input mar-b-5" type="text" name="tmoOTP" id="tmoOTP" value=""/>
				<label class="mdl-textfield__label" for="tmoOTP">Enter OTP</label>
			</div>
			<div class="clearfix"></div>
			<div class="mdl-textfield">
				<a class="full-width btn btn-primary tmo-sign-up-submit">LOGIN</a>
				<a class="full-width btn btn-primary disabled cur-loading tmo-login-get-otp-loading hide">LOGIN</a>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			var token = "";
			(function submitOTP() {
				$(".tmo-sign-up-submit").on("click", function () {
					$phoneNumber = $("#tmoEnquiryPhone").val();
					$otp = $("#tmoOTP").val();

					if (!$.isNumeric($phoneNumber) || $phoneNumber.length !== 10 && $phoneNumber < 1111111111) {
						showNotyError("Please enter a valid phone number.")
					} else if (!$.isNumeric($otp) && $otp.length !== 6 && $otp < 111110) {
						showNotyError("Please enter a valid OTP.")
					} else {
						showLoading();

						$.ajax({
							url: '/track/login/submit-otp?token=' + token,
							type: "POST",
							tryCount: 0,
							retryLimit: 3,
							data: {"phone": $phoneNumber, "otp": $otp},
							success: function (data) {
								if (data.success) {
									if (data.error) {
										hideLoading();
										showNotyError(data.error);
									} else {
										window.location.href = data.redirectUrl;
									}
								}
							},
							error: function (jqXHR, textStatus, errorThrown) {
								this.tryCount++;
								if (this.tryCount <= this.retryLimit) {
									$.ajax(this);
								} else {
									hideLoading();

									window.showNotyError("An error occurred while getting data. Please reload the page.");
									window.notifyTeam({
										"url": '/track/login/submit-otp?token=' + token,
										"textStatus": textStatus,
										"errorThrown": errorThrown
									});
								}
							}
						})
					}
				});
			})();
		});
	</script>
@endsection

@section("footer")
	<div class="footer-wrap">
		@include('base.home.footer.footer-non-city')
	</div>
@endsection