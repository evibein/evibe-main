@extends("layout.base")

@section('page-title')
	<title>Track My Order | Evibe.in</title>
@endsection

@section("header")
	@include('base.home.header.header-home-city')
	<div class="header-border full-width mar-b-10"></div>
@endsection

@section("content")
	<div class="partner-login-wrap" role="main" style="margin:0; min-height: calc(100vh - 392px)">
		<div class="col-md-4 col-sm-8 col-md-offset-4 col-sm-offset-2 mar-t-30 mar-b-30 text-center">
			<h1 class="no-mar-b in-blk page-heading">
				Track My Order
				<hr class="hr-text no-mar no-pad">
			</h1>
			<div class="clearfix"></div>
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="margin-bottom: -15px; margin-top: 10px;">
				<input class="mdl-textfield__input mar-b-5" type="text" name="tmoEnquiryPhone" id="tmoEnquiryPhone" value=""/>
				<label class="mdl-textfield__label" for="tmoEnquiryPhone">Phone Number</label>
			</div>
			<br>
			<span class="text-info font-13">(Please do not append <b>"0"</b> or <b>"+91"</b>. Use a valid 10 digit number Ex: <b>{{ config("evibe.contact.company.phone") }}</b>)</span>
			<div class="clearfix"></div>
			<div class="mdl-textfield">
				<a class="full-width btn btn-primary tmo-login-get-otp">REQUEST OTP</a>
				<a class="full-width btn btn-primary disabled cur-loading tmo-login-get-otp-loading hide">REQUEST OTP</a>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
        $(document).ready(function () {
            var token = "";

            (function getOTP() {
                $(".tmo-login-get-otp").on("click", function () {
                    $(".tmo-login-get-otp").addClass("hide");
                    $(".tmo-login-get-otp-loading").removeClass("hide");
                    showLoading();

                    $phoneNumber = $("#tmoEnquiryPhone").val();

                    if ($.isNumeric($phoneNumber) && $phoneNumber.length === 10 && $phoneNumber > 1111111111) {
                        $.ajax({
                            url: '/track/login/getOTP',
                            type: "POST",
                            tryCount: 0,
                            retryLimit: 3,
                            data: {"phone": $phoneNumber},
                            success: function (data) {
                                if (data.success) {
                                    hideLoading();
                                    showNotySuccess("OTP has been sent to your phone number");
                                    token = data.hashCode;
                                }

                                window.location.replace("/track/login/" + $phoneNumber + "?token=" + token);
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                this.tryCount++;
                                if (this.tryCount <= this.retryLimit) {
                                    $.ajax(this);
                                }
                                else {
                                    $(".tmo-login-get-otp").removeClass("hide");
                                    $(".tmo-login-get-otp-loading").addClass("hide");
                                    hideLoading();

                                    window.showNotyError("An error occurred while getting data. Please reload the page.");
                                    window.notifyTeam({
                                        "url": '/track/login/getOTP',
                                        "textStatus": textStatus,
                                        "errorThrown": errorThrown
                                    });
                                }
                            }
                        })
                    } else {
                        $(".tmo-login-get-otp").removeClass("hide");
                        $(".tmo-login-get-otp-loading").addClass("hide");
                        hideLoading();
                        showNotyError("Please enter a valid phone number.");
                    }
                });
            })();
        });
	</script>
@endsection

@section("footer")
	<div class="footer-wrap">
		@include('base.home.footer.footer-non-city')
	</div>
@endsection