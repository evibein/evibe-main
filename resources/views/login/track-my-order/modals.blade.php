@php $isMobile = $agent->isMobile() && !$agent->isTablet() ? "2" : "1" @endphp
<div id="modalUpdatePartyDate" class="modal fade" role="dialog" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			@if($isMobile != 2)
				<button type="button" data-dismiss="modal" class="close tmo-modal-close-btn">×</button>
			@endif
			<h5 class="text-center">Update Party Date/Time</h5>
			<div class="modal-body">
				<div class="form-group">
					<div class="col-sm-12">
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
							<input class="mdl-textfield__input mar-b-5" type="text" name="tmoNewPartyDate" id="tmoNewPartyDate" value=""/>
							<label class="mdl-textfield__label" for="tmoNewPartyDate">Party Date</label>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<label for="tmoNewPartyDateComments" class="custom-mdl-label">Comments
								<small> (optional)</small>
							</label>
							<textarea id="tmoNewPartyDateComments" name="tmoNewPartyDateComments" class="form-control"></textarea>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="text-center">
					@if($isMobile == 2)
						<div class="btn btn-link font-16 mar-t-5 tmo-submit-option-btn" data-dismiss="modal">
							<span>Cancel</span>
						</div>
					@endif
					<div class="btn btn-evibe font-16 mar-t-5 tmo-party-date-submit">
						<span>Request Update</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="modalNeedMoreServices" class="modal fade" role="dialog" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			@if($isMobile != 2)
				<button type="button" data-dismiss="modal" class="close tmo-modal-close-btn">×</button>
			@endif
			<h5 class="text-center">Add Other Services</h5>
			<div class="modal-body">
				<div class="form-group">
					<div class="col-sm-12">
						<div class="form-group">
							<label for="tmoNeedMorePartyServicesComments" class="custom-mdl-label">Please Share Your Additional Requirements</label>
							<textarea id="tmoNeedMorePartyServicesComments" name="tmoNeedMorePartyServicesComments" class="form-control"></textarea>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="text-center">
					@if($isMobile == 2)
						<div class="btn btn-link font-16 mar-t-5 tmo-submit-option-btn" data-dismiss="modal">
							<span>Cancel</span>
						</div>
					@endif
					<div class="btn btn-evibe font-16 mar-t-5 tmo-need-more-services-submit">
						<span>Request Other Services</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="modalRequestCancellation" class="modal fade" role="dialog" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			@if($isMobile != 2)
				<button type="button" data-dismiss="modal" class="close tmo-modal-close-btn">×</button>
			@endif
			<h5 class="text-center">Are you sure to cancel this order?</h5>
			<div class="col-md-8 col-md-offset-2 font-13 mar-t-10 mar-b-5">
				<div>
					<div class="pull-left">Total Booking Amount</div>
					<div class="pull-right" style="color: #7e808c;">&#8377; <span class="tmo-cancel-total">0</span>
					</div>
				</div>
				<div class="clearfix"></div>
				<div>
					<div class="pull-left">Total Advance Paid</div>
					<div class="pull-right" style="color: #7e808c;">&#8377; <span class="tmo-cancel-advance"></span>
					</div>
				</div>
				<div class="clearfix"></div>
				<div>
					<div class="pull-left" style="color: #ff6767;">Cancellation Charges</div>
					<div class="pull-right" style="color: #ff6767;">&#8377; <span class="tmo-cancel-charges"></span>
					</div>
				</div>
				<div class="clearfix"></div>
				<div style="border-bottom: 1px dotted black; margin-top: 5px;">
					<div class="pull-left mar-t-5">Refund Amount</div>
					<div class="pull-right mar-t-5" style="color: #7e808c;">&#8377;
						<span class="tmo-cancel-refund"></span></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
			<div class="modal-body">
				<div class="form-group">
					<div class="col-sm-12">
						<div class="form-group">
							<label for="tmoRequestCancellationComments" class="custom-mdl-label">Comments
								<small> (optional)</small>
							</label>
							<textarea id="tmoRequestCancellationComments" name="tmoRequestCancellationComments" class="form-control"></textarea>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="text-center">
					<div class="btn btn-link font-16 mar-t-5 tmo-submit-option-btn" data-dismiss="modal">
						<span>Cancel</span>
					</div>
					<div class="btn btn-evibe font-16 mar-t-5 tmo-request-cancellation-submit">
						<span>Yes</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="modalUpdatePartyVenue" class="modal fade" role="dialog" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			@if($isMobile != 2)
				<button type="button" data-dismiss="modal" class="close tmo-modal-close-btn">×</button>
			@endif
			<h5 class="text-center">Update My Order</h5>
			<div class="modal-body">
				<div class="form-group">
					<div class="col-sm-12">
						<div class="form-group">
							<label for="tmoUpdatePartyVenueComments" class="custom-mdl-label">Please enter your requirements</label>
							<textarea id="tmoUpdatePartyVenueComments" name="tmoUpdatePartyVenueComments" class="form-control"></textarea>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="text-center">
					@if($isMobile == 2)
						<div class="btn btn-link font-16 mar-t-5 tmo-submit-option-btn" data-dismiss="modal">
							<span>Cancel</span>
						</div>
					@endif
					<div class="btn btn-evibe font-16 mar-t-5 tmo-update-party-venue-submit">
						<span>Request Update</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="modalTMOThankYou" class="modal fade" role="dialog" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			@if($isMobile != 2)
				<button type="button" data-dismiss="modal" class="close tmo-modal-close-btn">×</button>
			@endif
			<div class="modal-body">
				<div style="color: #ff8c00; font-size: 20px" class="mar-t-20 pad-l-10">
					<i class="glyphicon glyphicon-ok"></i> Request Submitted
				</div>
				<h6 class="pad-10 no-mar-b">
					We've received your request.
				</h6>
				<h6 class="pad-10 no-mar-t">
					A representative from our team will get back to you within 4 business hours. If you need anything urgent, we recommend to reach us at
					&nbsp;<a href="mailto:{{ config('evibe.contact.customer.group') }}">{{ config('evibe.contact.customer.group') }}</a>.
				</h6>
			</div>
		</div>
	</div>
</div>
<div id="showOrderDetails" class="modal fade" role="dialog" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			@if($isMobile != 2)
				<button type="button" data-dismiss="modal" class="close tmo-modal-close-btn">×</button>
			@endif
			<h5 class="text-center text-underline no-mar-b">Order Details</h5>
			<div class="modal-body">
			</div>
			@if($isMobile == 2)
				<div class="text-center mar-b-15">
					<div class="btn btn-primary font-16" data-dismiss="modal">Close</div>
				</div>
			@endif
		</div>
	</div>
</div>
<div id="modalRaiseComplaint" class="modal fade" role="dialog" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			@if($isMobile != 2)
				<button type="button" data-dismiss="modal" class="close tmo-modal-close-btn">×</button>
			@endif
			<h5 class="text-center">Raise Complaint</h5>
			<div class="font-12 mar-l-30 mar-r-30">
				<u>Note:</u> This input is only applicable for refund or party quality related issues.
			</div>
			<div class="modal-body">
				<div class="form-group">
					<div class="col-sm-12">
						<div class="form-group">
							<label for="tmoRaiseComplaintComments" class="custom-mdl-label">Please enter your issue</label>
							<textarea id="tmoRaiseComplaintComments" name="tmoRaiseComplaintComments" class="form-control"></textarea>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="text-center">
					@if($isMobile == 2)
						<div class="btn btn-link font-16 mar-t-5 tmo-submit-option-btn" data-dismiss="modal">
							<span>Cancel</span>
						</div>
					@endif
					<div class="btn btn-evibe font-16 mar-t-5 tmo-raise-complaint-submit">
						<span>Submit</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="modalTMOComplaintThankYou" class="modal fade" role="dialog" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			@if($isMobile != 2)
				<button type="button" data-dismiss="modal" class="close tmo-modal-close-btn">×</button>
			@endif
			<div class="modal-body">
				<div style="color: #ff8c00; font-size: 20px" class="mar-t-20 pad-l-10">
					<i class="glyphicon glyphicon-ok"></i> Complaint Received
				</div>
				<h6 class="pad-10 no-mar-b">
					We've received your issue.
				</h6>
				<h6 class="pad-10 no-mar-t">
					A representative from our team will get back to you with a resolution within 24 - 48 hrs. We've sent you an email regarding the same. If you need anything urgent, we recommend you to reply to that email.
				</h6>
			</div>
		</div>
	</div>
</div>
<div id="modalUploadOrderProofs" class="modal fade" role="dialog" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			@if($isMobile != 2)
				<button type="button" data-dismiss="modal" class="close tmo-modal-close-btn">×</button>
			@endif
			<h5 class="text-center">Upload Proofs</h5>
			<div class="modal-body text-center">
				<form id="uploadOrderProofImagesTMOForm">
					<div class="ps-order-proofs-text mar-b-5">
						Kindly upload any one of the following identification and address proofs
					</div>
					<div class="ps-order-proofs-selection">
						<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 no-pad">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								@if(isset($data['typeProofs']))
									<select class="mdl-textfield__input mar-b-5" id="inpCustomerTMOProofType" name="inpCustomerTMOProofType" style="height: 29px;">
										@foreach($data['typeProofs'] as $proof)
											<option value="{{ $proof->id }}">{{ $proof->name }}</option>
										@endforeach
									</select>
								@endif
								<label class="mdl-textfield__label" for="inpCustomerTMOSource">Id Type</label>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="image-upload-rules font-12 text-info">
						( Valid Formats: JPG, jpeg, jpg, png, pdf. Maximum file size:<b><u>4 MB</u></b> )
					</div>
					<div class="form-group mar-t-10">
						<input type="hidden" id="uploadProofsUrl">
						<div class="in-blk">
							<div class="mar-t-5">
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
									<div>1. Front side</div>
								</div>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<div><input type="file" name="customerTMOProofFront" id="customerTMOProofFront"/>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="mar-t-15">
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
									<div>2. Back side</div>
								</div>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<div><input type="file" name="customerTMOProofBack" id="customerTMOProofBack"/>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="text-center mar-t-15">
							@if($isMobile == 2)
								<button class="btn btn-primary font-16 mar-r-20" data-dismiss="modal">Close</button>
							@endif
							<button type="submit" id="proofTMOUploadBtn" class="btn btn-warning font-16">
								<i class="glyphicon glyphicon-upload"></i> Upload
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div id="previewSelectedAddOns" class="modal fade" role="dialog" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			@if($isMobile != 2)
				<button type="button" data-dismiss="modal" class="close tmo-modal-close-btn">×</button>
			@endif
			<h5 class="text-center">Confirm Selected Add-Ons</h5>
			<div class="modal-body text-center">
				<div class="tmo-modal-selected-add-ons-list">

				</div>
				<div class="mar-t-20">
					@if($isMobile == 2)
						<button class="btn btn-link font-16 mar-r-20" data-dismiss="modal">Close</button>
					@endif
					<div class="btn btn-primary tmo-modal-submit-add-ons-btn" data-merge-ticket-id="" data-primary-ticket-id="">Submit</div>
				</div>
			</div>
		</div>
	</div>
</div>