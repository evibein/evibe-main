@extends('layout.base')

@section('page-title')
	<title>Wishlist | Evibe.in</title>
@endsection

@section('meta-description')
@endsection

@section('meta-keywords')
	<meta name="keywords" content="party services options wishlist, Evibe.in wishlist"/>
@endsection

@section("header")
	@include('base.home.header.header-home-city')
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-list.css') }}"/>
@endsection

@section("content")
	@if(auth()->check())
		<div style="background-color: #ffffff;margin-bottom: 50px; border-top: 1px solid #f3f3f3;">
			@if(count($data['results']) > 0)
				@if ($agent->isMobile() && !($agent->isTablet()))
					<div class="mar-b-30">
						<h1 class="fw-400 pad-l-15 no-mar-b page-heading">Your Wishlist</h1>
						<div class="list-content-wrap">
							<div class="list-options-wrap pad-t-15">
								<div class="col-xs-12 col-sm-12 no-pad-r">
									@php $count = 1; @endphp
									@foreach($data['results'] as $card)
										<div class="col-xs-6 col-sm-6 no-pad-l pad-b-20">
											<div class="list-option-card">
												<div class="list-option-image-wrap text-left" style="height:90px">
													<a class="list-option-link option-profile-image option-profile-image-{{ $card['id'] }}" data-option-id="{{ $card['id'] }}" href="{{ $card['fullUrl'] }}?ref=wishlist-img">
														<img src="{{$card['profileImg']}}" style="height: 100%;max-width: 100%">
													</a>
												</div>
												<div class="list-option-content-wrap">
													<div class="list-option-title">
														<a class="list-option-link" href="{{ $card['fullUrl'] }}">
															{{ $card['title'] }}
														</a>
													</div>
													<div class="list-option-price">
														<div class="list-main-price">
															@if($card['worth'])
																<div class=" in-blk" style="font-size: 11px;text-decoration: line-through;">
																	<span class="rupee-font">₹&nbsp;</span>
																	<span class="price-worth">{{$card['worth']}}</span>
																</div>
																&nbsp;&nbsp;&nbsp;
															@endif
															@if ($card['price'] != 0)
																<span class="price card-price">@price($card['price'])
																	@if($card['priceMax'] && $card['priceMax'] > $card['price']) - {{ $card['priceMax'] }}@endif
																</span>
																@if ($card['priceMax'] && $card['priceMax'] > $card['price'])
																	*
																@endif
															@endif
														</div>
													</div>
												</div>
											</div>
										</div>
										@if(($count % 2) == 0)
											<div class="clearfix"></div> @endif
										@php $count++; @endphp
									@endforeach
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				@else
					<div class="desktop-list-page col-sm-10 col-md-8 col-lg-8 row col-md-offset-2" style="margin-bottom:20px;padding-top: 20px">
						<h1 class="fw-400 page-heading pad-l-15">Your Wishlist</h1>
						@foreach($data['results'] as $card)
							@if(isset($card['fullUrl']))
								<div class="col-md-3 col-lg-3 col-sm-3 col-xs-6 no-pad-r pad-b-20 ">
									<div class="des-list-card-wrap">
										<div class="list-option-card des-list-option-card">
											<div class="list-option-image-wrap text-left">
												<a class="list-option-link  href=" {{ $card['fullUrl'] }}?ref=pb_img">
													<img src="{{$card['profileImg']}}" class="list-option-image initial loaded">
												</a>
											</div>
											<div class="list-option-content-wrap text-center">
												<div class="list-option-title">
													<a class="list-option-link" href="{{ $card['fullUrl'] }}?ref=pb_title">
														{{ $card['title'] }}
													</a>
												</div>
												<!-- @Mustdo: Please check  add to cart (on hover)-->
												<div class="list-option-cta-wrap text-center">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 no-pad">
														<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no-pad">
															<a class="item-shortlist" v-on:click="shortlistOptionFromListPage">
																<div class="hideShortlistResultsData list-cta-btn list-btn-shortlist" data-mapId="{{ $card['id'] }}" data-mapTypeId="{{ $card['mapTypeId'] }}" data-cityId="{{$card['cityId']}}" data-occasionId="{{ $card['occasionId'] }}" data-urladd="{{ route('partybag.add') }}" data-urldelete="{{ route('partybag.delete') }}">
																	Add To Cart
																</div>
															</a>
														</div>
														<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 no-pad">
															<a href="{{$card['fullUrl']}}?ref=book-now#bookNowModal" class="item-book-now" target="_blank">
																<div class="list-cta-btn list-btn-book-now">
																	Book Now
																</div>
															</a>
														</div>
														<div class="clearfix"></div>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="list-option-price pad-t-5">
													<div class="list-main-price in-blk">
														@if ($card['price'] != 0)
															<span class="price card-price">@price($card['price'])
																@if($card['priceMax'] && $card['priceMax'] > $card['price']) - {{ $card['priceMax'] }}@endif
															</span>
															@if ($card['priceMax'] && $card['priceMax'] > $card['price'])
																*
															@endif
															@if ($card['worth'])
																<span class="price-worth mar-l-5" style="font-size: 11px;text-decoration: line-through;">@price($card['worth'])</span>
															@endif
															@if( $card['additionalPriceLine'])
																<span class="hide">{{ $card['additionalPriceLine'] }}</span>
															@endif
														@else
															<span>----</span>
														@endif
													</div>
													<div class="list-price-tag hide in-blk">
														<i class="glyphicon glyphicon-thumbs-up"></i> Best Price
													</div>
													<div class="clearfix"></div>
												</div>

											</div>
										</div>
									</div>
								</div>
							@endif
						@endforeach
					</div>
				@endif
			@else
				<div class="text-center">
					<h4>YOUR WISHLIST IS EMPTY</h4>
					<div class="font-16 pad-b-15" style="color: #94989F;">Add items that you like to your wishlist.</div>
					<div>
						<img height="150" src="{{ $galleryUrl }}/main/img/icons/wishlist-empty.png" alt="wishlist">
					</div>
					<div class="pad-t-20">
						<a href="/" class="btn btn-partybag-options">
							See Options
						</a>
					</div>
				</div>
			@endif
		</div>
		<div class="clearfix"></div>
	@else
		<div style="background-color: #ffffff; padding-top: 30px; margin-bottom: 50px; text-align: center; border-top: 1px solid #f3f3f3;">
			<h4>PLEASE LOG IN</h4>
			<div class="font-16 pad-b-15" style="color: #94989F;">Login to view items in your wishlist.</div>
			<div>
				<img height="150" src="{{ $galleryUrl }}/main/img/icons/wishlist-empty.png" alt="wishlist">
			</div>
			<div class="mar-t-15">
				<a class="btn btn-default btn-large btn-fb-login text-left" href="{{ route('auth.login', 'facebook') }}">
					<img src="{{ $galleryUrl }}/main/img/logo/facebook-signup.png" class="" alt="Signup with facebook">
					<span class="btn-fb-login-txt">Continue with Facebook</span>
				</a>
			</div>
			<div class="pad-t-10">
				<a class="btn btn-default btn-large btn-google-login text-left" href="{{ route('auth.login', 'google') }}">
					<img src="{{ $galleryUrl }}/main/img/logo/google-signup.png" class="" alt="Signup with google">
					<span class="btn-google-login-txt">Continue with Google</span>
				</a>
			</div>
		</div>
	@endif
@endsection

@section("footer")
	<div class="footer-wrap">
		@include('app.footer_noncity')
	</div>
@endsection

@section("javascript")
	<script type="text/javascript">
		$(document).ready(function () {
			var listJs = "<?php echo elixir('js/app/results_util.js'); ?>";
			$.getScript(listJs);
		});
	</script>
@endsection