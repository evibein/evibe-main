@extends('layout.base')

@section('page-title')
	<title>{{ $data['seo']['pageTitle'] }} | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="{{ $data['seo']['pageDescription'] }}"/>
@endsection

@section('meta-keywords')
	<meta name="keywords" content="party services options shortlist, Evibe.in party bag, estimated quote for shortlisted option"/>
@endsection

@section('og-title')
	<meta property="og:title" content="{{ $data['seo']['pageTitle'] }} | Evibe.in"/>
@endsection

@section('og-description')
	<meta property="og:description" content="{{ $data['seo']['pageDescription'] }} "/>
@endsection

@section("header")
	@include('base.home.header.header-home-city')
	<div class="header-border"></div>
@endsection

@section('og-url')
	<meta property="og:url" content="{{ route("partybag.list") }}"/>
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-party-bag-list.css') }}"/>
@endsection

@section("content")
	@if(auth()->check())
		<div id="cart-container">
			@if (!($agent->isMobile() && !($agent->isTablet())))
				<div class="row col-md-10 col-lg-10 col-sm-12 col-md-offset-1 col-lg-offset-1 cart-wrap">
					@php $totalPrice=0; $totalAdvanceToPay=0; $transport=0 @endphp
					@if(count($data['results']) > 0)
						<h1 class="page-heading">My Cart</h1>
						<div class="col-md-9 col-xs-12 col-lg-9 col-sm-12 " id="product-wrap" style="margin-bottom: 30px">
							@foreach($data['results']['products'] as $card)
								@if(isset($card['fullUrl']))
									@if(isset($card['worth']) && ($card['worth'] || $card['productPrice']) && $card['advanceAmount'])
										<div class="row item-wrap-border">
											<div class="col-sm-12 col-md-4 col-lg-4 col-xs-12 item-img-wrap">
												<a href="{{$card['fullUrl']}}" class="a-no-decoration ">
													<img src="{{ $card['profileImg'] }}" alt="{{ $card['title'] }}" title="{{ $card['title'] }}" style="height:180px;max-width: 100%">
												</a>
											</div>
											<div class="col-sm-12 col-md-8 col-lg-8 col-xs-12">
												<h6><a href="{{$card['fullUrl']}}" class="a-no-decoration-black fw-400">{{$card['title']}}</a></h6>
												<div class="col-md-6 col-sm-12 col-lg-6">
													<div>
														<span class="pull-left">Product Price</span>
														@if($card['worth'])
															<span class="pull-right">@price($card['worth'])</span>
														@else
															<span class="pull-right">@price($card['productPrice'])</span>
														@endif
													</div>
													<div class="clearfix"></div>
													<div>
														@if(($card['worth'])&&$card['productPrice'])
															<span class="pull-left">Evibe Offer</span>
															<span class="pull-right text-g"> - @price($card['worth']-$card['productPrice'])</span>
														@endif
													</div>
													<div class="clearfix"></div>
													<div>
														<span class="pull-left">Transport Amount</span>
														@if($card['mapTypeId']===config("evibe.ticket.type.cake"))
															@if($card['transportAmount'])
																<span class="pull-right">@price($card['transportAmount'])</span>
															@else
																<span class="pull-right text-g">FREE</span>
															@endif
														@else
															@if($card['mapTypeId']===config("evibe.ticket.type.decor"))
																@if($data['others']['partyPinCode']!="")
																	@if($card['transportAmount'])
																		<span class="pull-right">@price($card['transportAmount'])</span>
																	@else
																		<span class="pull-right text-g">FREE</span>
																	@endif
																@else
																	<span class="pull-right">To be calculated</span>
																@endif
															@else
																@if($card['transportAmount'])
																	<span class="pull-right">@price($card['transportAmount'])</span>
																	@php $totalPrice += $card['transportAmount']; @endphp
																@else
																	<span class="pull-right text-g">FREE</span>
																@endif
															@endif
														@endif
													</div>
													<div class="clearfix"></div>
													@if(!($card['mapTypeId'] == config("evibe.ticket.type.cake")))
														<div>
															<span class="pull-left">Total Price</span>
															<span class="pull-right">@price($card['price'])</span>
															@php $totalPrice+= $card['price']; @endphp
														</div>
													@endif
													<div class="clearfix"></div>
													<hr>
													<div>
														@if($card['mapTypeId'] == config("evibe.ticket.type.cake"))
															<span class="pull-left">Total Price</span>
															<span class="pull-right">@price($card['price'])</span>
															@php $totalPrice+= $card['price']; @endphp
														@elseif(($card['mapTypeId']!=config("evibe.ticket.type.cake"))&&($card['advanceAmount']))
															<span class="pull-left">Advance To Pay</span>
															<span class="pull-right">@price($card['advanceAmount'])</span>
															@php $totalAdvanceToPay += $card['advanceAmount']; @endphp
														@endif

													</div>
												</div>
												<div class="clearfix"></div>
												<p style="margin-top:10px ">
												<span class="removeItemFromCart" style="cursor: pointer;border:1px solid #dee4ed;padding:5px;color:#546a5e"><i class="glyphicon glyphicon-remove-circle"></i> Remove from cart
													<span id="remove-cart-loading-wrap" class="hide">
														<img src="/images/loading.gif" alt="Please Wait..We are working your request" style="height: 20px;">
													</span>
												</span>
													<input type="hidden" value="{{$card['mapId']}}" class="mapId">
													<input type="hidden" value="{{$card['mapTypeId']}}" class="mapTypeId">
													<input type="hidden" value="{{$card['occasionId']}}" class="occasionId">
													<input type="hidden" value="{{$card['cityId']}}" class="cityId">
												</p>
											</div>
										</div>
										@php  $transport += $card['transportAmount']; @endphp
									@else
										<div class="row item-wrap-border">
											<div class="col-sm-12 col-md-4 col-lg-4 col-xs-12 item-img-wrap">
												<a href="{{$card['fullUrl']}}" class="a-no-decoration ">
													<img src="{{ $card['profileImg'] }}" alt="{{ $card['title'] }}" title="{{ $card['title'] }}" style="height:180px;max-width: 100%">
												</a>
											</div>
											<div class="col-sm-12 col-md-8 col-lg-8 col-xs-12">
												<h6><a href="{{$card['fullUrl']}}" class="a-no-decoration-black fw-400">{{$card['title']}}</a></h6>
												<p class="text-r font-italic center"> Something Went Wrong With This Product. Kindly Remove From Cart And Add</p>
												<p style="margin-top:10px ">
														<span class="removeItemFromCart" style="cursor: pointer;border:1px solid #dee4ed;padding:5px;color:#546a5e"><i class="glyphicon glyphicon-remove-circle"></i> Remove from cart
															<span id="remove-cart-loading-wrap" class="hide">
																<img src="/images/loading.gif" alt="Please Wait..We are working your request" style="height: 20px;">
															</span>
														</span>
													<input type="hidden" value="{{$card['mapId']}}" class="mapId">
													<input type="hidden" value="{{$card['mapTypeId']}}" class="mapTypeId">
													<input type="hidden" value="{{$card['occasionId']}}" class="occasionId">
													<input type="hidden" value="{{$card['cityId']}}" class="cityId">
												</p>
											</div>
										</div>
									@endif
								@endif
							@endforeach
							<div>
								<input type="hidden" value="{{$transport}}" id="transpportAmount1">
							</div>
							<div class="bottom-cta-wrap hide">
								<span id="clear-cart" class="clear-cart-wrap"><span class="glyphicon glyphicon-trash"></span>Clear Cart</span>
								<a href="{{ route("wishlist.list") }}" class="a-no-decoration pad-l-5 pad-r-5 mar-l-5 clear-cart-wrap" style="color:inherit"><span class="glyphicon glyphicon-plus"></span> Add More From Wishlist</a>
							</div>
						</div>
						<div class="col-md-3 col-xs-12 col-lg-3 col-sm-12 ">
							<div class="secure-text-wrap">
								<img src="https://gallery.evibe.in/img/app/evibe_shield.png"><span> 100% Secure</span>
							</div>
							<div class="col-sm-12 col-md-12 col-lg-12 order-details">
								<span>Order Details</span>
								<div class="sidebar-checkout-input-wrap">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="padding: 24px 0">
										@if($data['others']['partyDate']!="")
											<input type="text" name="pbCheckoutEventDate" id="pbCheckoutEventDate" class="font-14 mdl-textfield__input" value="{{$data['others']['partyDate']}}">

										@else
											<input type="text" name="pbCheckoutEventDate" id="pbCheckoutEventDate" class="font-14 mdl-textfield__input">
										@endif
										<label for="pbCheckoutEventDate" class="font-14  mdl-textfield__label text-normal">Party Date</label>
									</div>

									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="margin-top: -16px; padding:24px 0">
										@if($data['others']['partyPinCode']!="")
											<input name="pbCheckoutPartyPinCode" id="pbCheckoutPartyPinCode" class="font-14 mdl-textfield__input" value="{{$data['others']['partyPinCode']}}" maxlength="6">
										@else
											<input name="pbCheckoutPartyPinCode" id="pbCheckoutPartyPinCode" class="font-14  mdl-textfield__input" maxlength="6">
										@endif
										<label for="pbCheckoutPartyPinCode" class="font-14 mdl-textfield__label text-normal">Party Pin Code</label>
									</div>
								</div>
								<div class="error-msg hide no-pad-t pad-b-10"></div>
								<div class="clearfix"></div>
								<!-- Price-->
								<div class="col-sm-6 col-md-7 col-lg-7 no-pad-l no-pad-r">
									<span class="text-bold pull-left">Total Price</span>
								</div>
								<div class="col-sm-6 col-md-5 col-lg-5 no-pad-l no-pad-r">
									<span class="pull-right text-bold">@price($totalPrice) </span>
								</div>
								<div class="col-sm-6 col-md-7 col-lg-7 no-pad-l no-pad-r">
									<span class="pull-left text-bold">Advance To Pay</span>
								</div>
								<div class="col-sm-6 col-md-7 col-lg-5 no-pad-l no-pad-r">
									<span class="pull-right text-bold">@price($totalAdvanceToPay)</span>
								</div>
								<div class="clearfix"></div>
								<div class="btn-evibe cur-point col-sm-12 col-lg-10 col-md-10 col-md-offset-1 col-lg-offset-1 checkout-btn mar-t-15 mar-b-5 font-14 text-center pad-t-10 pad-b-10">
									Checkout Cart
								</div>
								<div class="sidebar-checkout-loader-wrap text-center hide">
									<div class="sidebar-checkout-loader-image">
										<img src="/images/loading.gif" alt="">
									</div>
									<div class="sidebar-checkout-loader-text mar-t-5">Please wait..</div>
								</div>

							</div>
						</div>
					@else
						<div class="party-bag-top-sec" style="margin-top: 50px;border:none">
							<div class="text-center font-18">
								<img src="{{ $galleryUrl . "/main/img/empty-cart.jpg" }}" alt="empty party bag" style="height:250px">
							</div>
							<div class="text-bold">
								Your cart is currently empty
							</div>
							<div class="pad-t-20">
								<a href="/" class="btn btn-partybag-options font-18">
									Browse More
								</a>
								<a href="{{ route("wishlist.list") }}" class="btn btn-partybag-options font-18" style="margin-left: 10px">
									Add From Wishlist
								</a>
							</div>
						</div>
					@endif
				</div>
				<div class="clearfix"></div>
			@else
				<div class="full-width cart-wrap">
					@php $totalPrice=0; $totalAdvanceToPay=0; $transport=0; @endphp
					@if(count($data['results']) > 0)
						<h5 class="pad-l-15">My Cart</h5>

						<!-- Product details-->
						<div class="full-width mar-b-10 pad-l-10 pad-r-10">
							@foreach($data['results']['products'] as $card)
								@if(isset($card['fullUrl']))
									@if(isset($card['worth']) && ($card['worth'] || $card['productPrice']) && $card['advanceAmount'])
										<div class="row item-wrap-border pad-b-15">
											<div class="col-sm-6 col-lg-6 col-xs-6 pad-t-15">
												<a href="{{$card['fullUrl']}}" class="a-no-decoration">
													<img src="{{ $card['profileImg'] }}" alt="{{ $card['title'] }}" title="{{ $card['title'] }}" style="max-height: 150px;width: 100%">
												</a>
												<p class="no-mar-b pad-t-15 text-center removeItemFromCart">
													<span class="glyphicon glyphicon-remove-circle"></span> Remove Item
													<span id="remove-cart-loading-wrap" class="hide">
													<img src="/images/loading.gif" alt="Please Wait..We are working your request" style="height: 20px;">
												</span>
												</p>
												<input type="hidden" value="{{$card['mapId']}}" class="mapId">
												<input type="hidden" value="{{$card['mapTypeId']}}" class="mapTypeId">
												<input type="hidden" value="{{$card['occasionId']}}" class="occasionId">
												<input type="hidden" value="{{$card['cityId']}}" class="cityId">
											</div>
											<div class="col-sm-6 col-lg-6 col-xs-6 no-pad-l ">
												<h6 class="mar-t-15"><a href="{{$card['fullUrl']}}" class="a-no-decoration-black">{{$card['title']}}</a></h6>
												<div>
													<span class="pull-left">Product Price</span>
													@if(($card['worth']))
														<span class="pull-right">@price($card['worth'])</span>
													@else
														<span class="pull-right">@price($card['productPrice'])</span>
													@endif
												</div>
												<div class="clearfix"></div>
												<div>
													@if(($card['worth']))
														<span class="pull-left">Evibe Offer</span>
														<span class="pull-right text-g"> - @price($card['worth']-$card['productPrice'])</span>
													@endif
												</div>
												<div class="clearfix"></div>
												<div>
													<span class="pull-left">Transport Amount</span>
													@if($card['mapTypeId']===config("evibe.ticket.type.cake"))
														@if($card['transportAmount'])
															<span class="pull-right">@price($card['transportAmount'])</span>
														@else
															<span class="pull-right text-g">FREE</span>
														@endif
													@else
														@if($card['mapTypeId']===config("evibe.ticket.type.decor"))
															@if($data['others']['partyPinCode']!="")
																@if($card['transportAmount'])
																	<span class="pull-right">@price($card['transportAmount'])</span>
																@else
																	<span class="pull-right text-g">FREE</span>
																@endif
															@else
																<span class="pull-right">To be calculated</span>
															@endif
														@else
															@if($card['transportAmount'])
																<span class="pull-right">@price($card['transportAmount'])</span>
																@php $totalPrice+= $card['transportAmount']; @endphp
															@else
																<span class="pull-right text-g">FREE</span>
															@endif
														@endif
													@endif
												</div>
												<div class="clearfix"></div>
												@if(!($card['mapTypeId']==config("evibe.ticket.type.cake")))
													<div>
														<span class="pull-left">Total Price</span>
														<span class="pull-right">@price($card['price'])</span>
														@php $totalPrice+= $card['price']; @endphp
													</div>
													<div class="clearfix"></div>
													<hr>
													<div>
														<span class="pull-left">Advance To Pay</span>
														<span class="pull-right">@price($card['advanceAmount'])</span>
														@php $totalAdvanceToPay += $card['advanceAmount']; @endphp
													</div>
												@else
													<hr>
													<div>
														<span class="pull-left">Advance To Pay</span>
														<span class="pull-right">@price($card['advanceAmount'])</span>
														@php $totalAdvanceToPay += $card['advanceAmount']; @endphp
													</div>
												@endif
											</div>
										</div>
										<div class="clearfix"></div>
									@else
										<div class="row item-wrap-border pad-b-15">
											<div class="col-sm-6 col-lg-6 col-xs-6 pad-t-15">
												<a href="{{$card['fullUrl']}}" class="a-no-decoration">
													<img src="{{ $card['profileImg'] }}" alt="{{ $card['title'] }}" title="{{ $card['title'] }}" style="max-height: 150px;width: 100%">
												</a>
												<p class="no-mar-b pad-t-15 text-center removeItemFromCart">
													<span class="glyphicon glyphicon-remove-circle"></span> Remove Item
													<span id="remove-cart-loading-wrap" class="hide">
													<img src="/images/loading.gif" alt="Please Wait..We are working your request" style="height: 20px;">
												</span>
												</p>
												<input type="hidden" value="{{$card['mapId']}}" class="mapId">
												<input type="hidden" value="{{$card['mapTypeId']}}" class="mapTypeId">
												<input type="hidden" value="{{$card['occasionId']}}" class="occasionId">
												<input type="hidden" value="{{$card['cityId']}}" class="cityId">
											</div>
											<div class="col-sm-6 col-lg-6 col-xs-6 no-pad-l ">
												<h6 class="mar-t-15"><a href="{{$card['fullUrl']}}" class="a-no-decoration-black">{{$card['title']}}</a></h6>
												<p class="text-r font-italic center"> Something Went Wrong With This Product. Kindly Remove From Cart And Add</p>

											</div>
										</div>
									@endif
								@endif
							@endforeach
							@php  $transport += $card['transportAmount']; @endphp
						</div>
						<div>
							<input type="hidden" value="{{$transport}}" id="transpportAmount1">
						</div>
						<div class="clearfix"></div>

						<!-- Product details end-->
						<!--Total Pricing-->

						<div class="full-width pad-l-10 pad-r-10">
							<div class="col-sm-12 col-md-12 col-lg-12 order-details ">
								<span style="font-size:16px">Order Details</span>
								<div class="sidebar-checkout-input-wrap">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width: 100%;padding:24px 0;">
										@if($data['others']['partyDate']!="")
											<input type="text" name="pbCheckoutEventDate" id="pbCheckoutEventDate" class="mdl-textfield__input font-14" value="{{$data['others']['partyDate']}}">

										@else
											<input type="text" name="pbCheckoutEventDate" id="pbCheckoutEventDate" class="mdl-textfield__input font-14">
										@endif
										<label for="pbCheckoutEventDate" class="mdl-textfield__label text-normal font-14">Party Date</label>
									</div>

									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width: 100%;padding:24px 0">
										@if($data['others']['partyPinCode']!="")
											<input name="pbCheckoutPartyPinCode" id="pbCheckoutPartyPinCode" class="mdl-textfield__input font-14" value="{{$data['others']['partyPinCode']}}" maxlength="6">
										@else
											<input name="pbCheckoutPartyPinCode" id="pbCheckoutPartyPinCode" class="mdl-textfield__input" style="font-size:14px" maxlength="6">
										@endif
										<label for="pbCheckoutPartyPinCode" class="mdl-textfield__label text-normal font-14">Party Pin Code</label>
									</div>
									<div class="error-msg hide no-pad-t pad-b-10">

									</div>
								</div>
								<div class="clearfix"></div>
								<!-- Price-->
								<div class="col-sm-7 col-xs-7 col-md-7 col-lg-7 no-pad-l no-pad-r">
									<span class="text-bold">Total Price</span>
								</div>
								<div class="col-sm-5 col-md-5 col-xs-5 col-lg-5 no-pad-l no-pad-r">
									<span class="pull-right text-bold">@price($totalPrice) </span>
								</div>
								<div class="clearfix"></div>

								<div class="col-sm-7 col-md-7 col-xs-7 col-lg-7 no-pad-l no-pad-r">
									<span class="pull-left text-bold">Advance To Pay</span>
								</div>
								<div class="col-sm-5 col-md-7 col-xs-5 col-lg-5 no-pad-l no-pad-r">
									<span class="pull-right text-bold">@price($totalAdvanceToPay)</span>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="secure-text-wrap">
								<img src="https://gallery.evibe.in/img/app/evibe_shield.png"><span> 100% Secure</span>
							</div>
						</div>
						<!-- Botttom strip -->
						<div class="text-center checkout-btn bottom-strip ">
							Checkout Cart
						</div>
						<div class="sidebar-checkout-loader-image sidebar-checkout-loader-wrap text-center hide bottom-strip no-mar-t" style="height: inherit;">
							<img src="/images/loading.gif" alt="">
						</div>

						<!-- Botttom strip end -->
					@else
						<div class="party-bag-top-sec" style="margin-top: 50px;border:none">
							<div class="text-center font-18">
								<img src="{{ $galleryUrl . "/main/img/empty-cart.jpg" }}" alt="empty party bag" style="height:250px">
							</div>
							<div class="text-center">
								<p class="text-bold">Your cart is currently empty</p>

							</div>

							<div class="pad-t-20">
								<a href="/" class="btn btn-partybag-options font-18 no-pad-l no-pad-r-imp text-center " style="width: 220px">
									Browse More
								</a>
								<a href="{{ route("wishlist.list") }}" class="btn btn-partybag-options font-18 mar-t-10 no-pad-l no-pad-r-imp text-center" style="width: 220px">
									Add From Wishlist
								</a>
							</div>
						</div>
						<div class="clearfix"></div>
					@endif
				</div>
			@endif
		</div>
	@else
		<div class="text-center" style="background-color: #ffffff; padding-top: 30px; margin-bottom: 50px; text-align: center; border-top: 1px solid #f3f3f3;">
			<h4>PLEASE LOG IN</h4>
			<div class="font-16 pad-b-15" style="color: #94989F;">Login to view items in your cart.</div>
			<div>
				<img height="150" src="{{ $galleryUrl }}/main/img/icons/wishlist-empty.png" alt="wishlist">
			</div>
			<div class="mar-t-15">
				<a class="btn btn-default btn-large btn-fb-login text-left" href="{{ route('auth.login', 'facebook') }}">
					<img src="{{ $galleryUrl }}/main/img/logo/facebook-signup.png" class="" alt="Signup with facebook">
					<span class="btn-fb-login-txt">Continue with Facebook</span>
				</a>
			</div>
			<div class="pad-t-10">
				<a class="btn btn-default btn-large btn-google-login text-left" href="{{ route('auth.login', 'google') }}">
					<img src="{{ $galleryUrl }}/main/img/logo/google-signup.png" class="" alt="Signup with google">
					<span class="btn-google-login-txt">Continue with Google</span>
				</a>
			</div>
		</div>
	@endif
@endsection

@section("footer")
	<div class="footer-wrap">
		@include('base.home.footer.footer-common')
	</div>
@endsection

@section("javascript")
	<script type="text/javascript">
		$(document).ready(function () {
			var cartContainer = $("#cart-container");

			(function inputActions() {
				cartContainer.on("click", "#clear-cart", function () {
					if (confirm("Are You sure? You want to clear the cart?")) {
						$.ajax({
							url: '/my/party-bag/clear',
							method: 'POST',
							data: {},
							success: function (data) {
								$(".cart-wrap").load(" .cart-wrap > *");
							},
							error: function (jqXHR, textStatus, errorThrown) {
								self.alertDialog("Sorry, an error has oasaccurred, please try again later.");
								window.notifyTeam({
									"url": "/my/party-bag/clear",
									"textStatus": textStatus,
									"errorThrown": errorThrown
								});
							}
						});
					}

				});

				cartContainer.on("click", ".removeItemFromCart", function () {
					$(this).find("#remove-cart-loading-wrap").removeClass("hide");
					$mapTypeId = $(this).siblings('.mapTypeId').val();
					$mapId = $(this).siblings('.mapId').val();
					$cityId = $(this).siblings('.cityId').val();
					$occasionId = $(this).siblings('.occasionId').val();
					var data = {
						"city_id": $cityId,
						"occasion_id": $occasionId,
						"map_id": $mapId,
						"map_type_id": $mapTypeId,
					};

					$.ajax({
						url: '/my/party-bag/delete',
						method: 'POST',
						data: data,
						success: function (data) {
							$(".cart-wrap").load(" .cart-wrap > *");
							$("#remove-cart-loading-wrap").addClass("hide");
						},
						error: function (qXHR, textStatus, errorThrown) {
							$("#remove-cart-loading-wrap").removeClass("hide");
							window.showNotyError("Sorry, An error has occurred, Please try again later");
							window.notifyTeam({
								"url": '/my/party-bag/delete',
								"textStatus": textStatus,
								"errorThrown": errorThrown
							});
						},
					});
				});

				cartContainer.on("click", ".checkout-btn", function () {
					$('.error-msg').addClass("hide");
					$('.checkout-btn').addClass("hide");
					$('.sidebar-checkout-loader-wrap').removeClass('hide');

					var inputData = {
						'partyDate': $('#pbCheckoutEventDate').val(),
						'partyPinCode': $('#pbCheckoutPartyPinCode').val()
					};
					var ajaxUrl = '/my/party-bag/validate-checkout';
					if (ajaxUrl) {
						$.ajax({
							url: ajaxUrl,
							type: 'POST',
							dataType: 'json',
							data: inputData,
							success: function (data, textStatus, jqXHR) {
								var feasibilityMsg = "";
								if (data.success) {
									if (data.checkoutFeasibility) {
										if (data.checkoutTransportAmount) {
											var transport1 = $('#transpportAmount1').val();
											if (transport1 == data.checkoutTransportAmount) {
												redirectToCheckout();
											} else {
												feasibilityMsg = 'Transportation charges have been updated for some services for the party on ' + this.partyDate + ' at location ' + this.partyPinCode + '. Kindly proceed to checkout to book the services';
												$('.error-msg').text(feasibilityMsg).addClass('text-g').removeClass('hide');
												$('.sidebar-checkout-loader-wrap').addClass('hide');
												$('#product-wrap').load(' #product-wrap >*');
												$('.checkout-btn').removeClass("hide checkout-btn").addClass("proceed-to-checkout").text('Proceed To Checkout');
												$('.sidebar-checkout-input-wrap').addClass("hide");
											}
										} else {
											redirectToCheckout();
										}
									} else {
										feasibilityMsg = 'Delivery of some services is not possible for the entered pin code. Please try again with different pin code';

										if (data.checkoutFeasibilityMsg) {
											feasibilityMsg = data.checkoutFeasibilityMsg;
										}
										$('.error-msg').text(feasibilityMsg).addClass('text-r').removeClass('hide');
										$('.checkout-btn').removeClass("hide");
										$('.sidebar-checkout-loader-wrap').addClass('hide');
									}
								} else {
									$('.checkout-btn').removeClass("hide");
									$('.sidebar-checkout-loader-wrap').addClass('hide');
									var errorMsg = 'Sorry, an error has occurred, please try again later';
									if (data.errorMsg) {
										errorMsg = data.errorMsg;
									}
									window.showNotyError(errorMsg);
								}
							},
							error: function (jqXHR, textStatus, errorThrown) {
								$('.checkout-btn').removeClass("hide");
								$('.sidebar-checkout-loader-wrap').addClass('hide');
								window.showNotyError("Sorry, An error has occurred, Please try again later");

								window.notifyTeam({
									"url": ajaxUrl,
									"textStatus": textStatus,
									"errorThrown": errorThrown
								});
							},
						});
					}
				});
			})();

			cartContainer.on("click", ".proceed-to-checkout", function () {
				redirectToCheckout();
			});

			function redirectToCheckout() {
				window.location.href = '/my/party-bag/checkout';
			}
		});
	</script>
@endsection