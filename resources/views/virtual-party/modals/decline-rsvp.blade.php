<div id="modalDeclineRSVP" class="modal fade modal-md mar-auto no-pad-r" role="dialog" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="col-sm-12 col-md-12 col-lg-12 no-pad">
				<div class="modal-body">
					<div class="">
						<div class="title font-20 lh-24 text-center mar-10">
							Aww... we will miss you. Do you want to send your wishes to @if(isset($data['pageDetails']['celebrity_text']) && $data['pageDetails']['celebrity_text']) {{ $data['pageDetails']['celebrity_text'] }} @else the celebrity @endif
						</div>
						<div class="text-center mar-t-15">
							<div id="declineRSVPSendWishBtn" class="in-blk btn btn-evibe mar-r-10">Yes</div>
							<div class="in-blk btn btn-hollow-blue" data-dismiss="modal">Not Now</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>