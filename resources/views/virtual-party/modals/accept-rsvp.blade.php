<div id="modalAcceptRSVP" class="modal fade modal-md mar-auto no-pad-r" role="dialog" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content mar-r-20">
			<div class="col-sm-12 col-md-12 col-lg-12 no-pad">
				<button type="button" data-dismiss="modal" class="close accept-rsvp-modal-close-btn vp-modal-close-btn">x</button>
				<div class="modal-body">
					<div class="contact-details-wrap">
						<div class="title font-20 lh-24 mar-10">
							Super!! Provide your details to let @if(isset($data['pageDetails']['celebrity_text']) && $data['pageDetails']['celebrity_text']) {{ $data['pageDetails']['celebrity_text'] }} @else the celebrity @endif know
						</div>
						<div class="form-group mar-t-5">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
									<input class="mdl-textfield__input mar-b-5" type="text" name="acceptRSVPName" id="acceptRSVPName" value=""/>
									<label class="mdl-textfield__label" for="acceptRSVPName">Name</label>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
									<input class="mdl-textfield__input mar-b-5" type="text" name="acceptRSVPEmail" id="acceptRSVPEmail" value=""/>
									<label class="mdl-textfield__label" for="acceptRSVPEmail">Email</label>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 hide">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
									<input class="mdl-textfield__input mar-b-5" type="text" name="acceptRSVPRelationship" id="acceptRSVPRelationship" value=""/>
									<label class="mdl-textfield__label" for="acceptRSVPRelationship">Relationship</label>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
								<div class="btn btn-evibe full-width" id="submitAcceptRSVPBtn" data-url="{{ route('virtual-party.accept-rsvp') }}">Submit</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="calender-event-wrap hide">
						<div>
							<div class="text-center">
								<div class="in-blk">
									<img src="{{ $galleryUrl }}/main/img/icons/hi_five.png" height="80px" width="80px" alt="success icon">
								</div>
								<div class="in-blk">
									<h6 class="mar-l-10 font-16">Yayy!! @if(isset($data['pageDetails']['celebrity_text']) && $data['pageDetails']['celebrity_text']) {{ $data['pageDetails']['celebrity_text'] }} @else the celebrity @endif will be delighted to know this</h6>
								</div>
							</div>
						</div>
						<div class="text-center mar-b-15 mar-t-10">
							<!-- Button code -->
							<div title="Add to Calendar" class="addeventatc">
								Add to Calendar
								<span class="start">{{ date('Y-m-d H:i', strtotime($data['pageDetails']['party_date_time'])) }}</span>
								<span class="timezone">Asia/Kolkata</span>
								<span class="title">@if(isset($data['pageDetails']['celebrity_text']) && $data['pageDetails']['celebrity_text']) {{ $data['pageDetails']['celebrity_text'] }}'s @else The celebrity's @endif @if(isset($data['pageDetails']['event']) && $data['pageDetails']['event']) {{ $data['pageDetails']['event'] }} @else Party @endif</span>
								<span class="description">Virtual Party @ {{ route('virtual-party.home',[$data['pageDetails']['url']]) }} at {{ date('d M Y, h:i A', strtotime($data['pageDetails']['party_date_time'])) }}</span>
								<span class="location">{{ route('virtual-party.home',[$data['pageDetails']['url']]) }}</span>
							</div>
							<div id="" class="btn btn-evibe in-blk hide">
								<span>Add To Calendar</span>
							</div>
							<div id="" class="btn btn-hollow-blue in-blk mar-l-20 hide">
								<span>Not Now</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>