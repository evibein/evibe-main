<div id="modalSendWishes" class="modal fade modal-md mar-auto no-pad-r" role="dialog" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content mar-r-20">
			<div class="col-sm-12 col-md-12 col-lg-12 no-pad">
				<button type="button" data-dismiss="modal" class="close send-wishes-modal-close-btn vp-modal-close-btn">x</button>
				<div class="modal-body">
					<div class="hide hidden-input-wrap">
						<input type="hidden" id="sendWishesRSVP" value="">
					</div>
					<div class="wish-details-wrap pad-b-15">
						<div class="form-title text-left">
							Send your wishes to @if(isset($data['pageDetails']['celebrity_text']) && $data['pageDetails']['celebrity_text']) {{ $data['pageDetails']['celebrity_text'] }} @else the celebrity @endif ...
						</div>
						<div class="form-group">
							<div class="mar-t-10">
								<textarea id="sendWishesComments" name="sendWishesComments" rows="6" class="form-control font-16" placeholder="Enter Your Message" required></textarea>
							</div>
							<div class="mar-t-15">
								<div class="font-16">
									Add a birthday card or record poem or song to surprise  @if(isset($data['pageDetails']['celebrity_text']) && $data['pageDetails']['celebrity_text']) {{ $data['pageDetails']['celebrity_text'] }} @else the celebrity @endif
								</div>
								<div class="mar-t-5">
									<div class="in-blk mar-r-20">
										<label class="">
											<input type="radio" name="sendWishesUploadType" class="wishRadioBtn" value="1"> <img src="{{ $galleryUrl }}/img/svg/camera.svg" style="width: 30px;    padding-left: 5px;">&nbsp;&nbsp;Upload Image
										</label>
									</div>
									<div class="in-blk">
										<label class="">
											<input type="radio" name="sendWishesUploadType"  class="wishRadioBtn" value="2"> <img src="{{ $galleryUrl }}/img/svg/video.svg" style="width: 30px;padding-left:5px">&nbsp;&nbsp;Upload Video
										</label>
									</div>
								</div>
							</div>
							<div class="upload-file-wrap">
								<div class="image-upload-wrap hide mar-t-5">
									<div class="in-blk mar-r-15">Upload Card (max: 5 MB)</div>
									<div class="file in-blk mar-t-5__400-600">
										<input type="file" name="wishCard" id="wishCard"/>
									</div>
								</div>
								<div class="video-upload-wrap hide mar-t-5">
									<div class="in-blk mar-r-15">Upload Video (max: 100 MB)</div>
									<div class="file in-blk mar-t-5__400-600">
										<input type="file" name="wishVideo" id="wishVideo" accept="video/*"/>
									</div>
								</div>
							</div>
						</div>
						<div class="form-cta text-center">
							<button type="submit" id="validateWishesBtn" class="btn btn-evibe full-width" data-url="{{ route('virtual-party.validate-wishes') }}">
								<span class="">POST</span>
							</button>
						</div>
					</div>
					<div class="contact-details-wrap hide">
						<div class="form-title-2 text-left">
							Please enter your details to let @if(isset($data['pageDetails']['celebrity_text']) && $data['pageDetails']['celebrity_text']) {{ $data['pageDetails']['celebrity_text'] }} @else the celebrity @endif know that you have wished
						</div>
						<div class="form-group mar-t-5">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
									<input class="mdl-textfield__input mar-b-5" type="text" name="sendWishesName" id="sendWishesName" value=""/>
									<label class="mdl-textfield__label" for="sendWishesName">Name</label>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
									<input class="mdl-textfield__input mar-b-5" type="text" name="sendWishesEmail" id="sendWishesEmail" value=""/>
									<label class="mdl-textfield__label" for="sendWishesEmail">Email</label>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 hide">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
									<input class="mdl-textfield__input mar-b-5" type="text" name="sendWishesRelationship" id="sendWishesRelationship" value=""/>
									<label class="mdl-textfield__label" for="sendWishesRelationship">Relationship</label>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
								<div class="btn btn-evibe full-width" id="sendWishesBtn" data-url="{{ route('virtual-party.send-wishes') }}">Send Wishes</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="form-cta text-center"></div>
					</div>
					<div class="thank-you-wrap hide">
						<div>
							<div class="text-center">
								<div class="in-blk">
									<img src="{{ $galleryUrl }}/main/img/icons/hi_five.png" height="80px" width="80px" alt="success icon">
								</div>
								<div class="in-blk">
									<h6 class="mar-l-10 font-16">Yayy!! we've posted your wishes for @if(isset($data['pageDetails']['celebrity_text']) && $data['pageDetails']['celebrity_text']) {{ $data['pageDetails']['celebrity_text'] }} @else the celebrity @endif</h6>
								</div>
							</div>
						</div>
						<div class="text-center mar-b-15">
							<div id="refreshSendWishesModalBtn" class="btn btn-evibe font-16 mar-t-10">
								<span>View Feed</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>