<div class="modal" tabindex="-1" role="dialog" id="intermediateModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				
				<div class="container" style="width:100%">
					<p style="font-size:17px;color:#000000b0;font-weight:600;" class="text-center">How To Attend</p>
					<div>
						<div class="">
							<p style=" font-weight: 600;    font-size: 16px;    color: #000000ad;">1. Basics<br><span style="font-weight:500;font-size:15px;">Ensure you have a basic internet connection and a device with mic and webcam.</span></p>
						</div>
						<div class="">
							<p  style=" font-weight: 600;    font-size: 16px;    color: #000000ad;">2. Zoom<br><span style="font-weight:500;font-size:15px;">Download app from <a href="https://www.zoom.us" target="_blank">here</a>. We will ensure your privacy is protected with all the recommended settings.</span></p>
						</div>
						<div class="">
							<p  style=" font-weight: 600;    font-size: 16px;    color: #000000ad;">3. Virtual Present<br><span style="font-weight:500;font-size:15px;">Use your creativity to make a virtual card as birthday present for birthday child. You can also prepare a poem or song for a special dedication.</span></p>
						</div>
						<div class="">
							<p  style=" font-weight: 600;    font-size: 16px;    color: #000000ad;">4. Get Ready<br><span style="font-weight:500;font-size:15px;">Get your costume ready & dress up for the party. We may take virtual group photo. Oops a screenshot. :)</span></p>
						</div>
						<div class="">
							<p  style=" font-weight: 600;    font-size: 16px;    color: #000000ad;">5. Join Party<br><span style="font-weight:500;font-size:15px;">Click the Join Party button below.  </span></p>
						</div>
					</div>
				</div>

			</div>
			<div class="modal-footer mar-t-10 mar-b-10">
				<a href="" target="_blank"  class="floating-evibe-btn a-no-decoration" style="border-radius:20px;padding-left:20px;padding-right:20px;font-size:14px !important;" id='zoomLinkBtn'>Join Party Now</a>
			</div>
		</div>

	</div>
</div>