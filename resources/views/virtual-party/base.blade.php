@extends('layout.base')

@section('page-title')
	<title>{{ $data['seoTitle'] }}</title>
@endsection

@section('meta-description')
	<meta name="description" content="{{ $data['seoDesc'] }}">
@endsection

@section('meta-keywords')
	<meta name="keywords" content="VirtualParty, Evibe, Online">
@endsection

@section('og-title')
	<meta property="og:title" content="{{ $data['seoTitle'] }}">
@endsection

@section('og-description')
	<meta property="og:description" content="{{ $data['seoDesc'] }}">
@endsection

@section('og-url')
	<meta property="og:url" content="{{ request()->fullUrl() }}">
@endsection

@section('og-image')
	<meta property="og:image" content="{{ $galleryUrl.'/virtual-party/'.$data['pageDetails']['id'].'/'.$data['pageDetails']['invite_url'] }}"/>
@endsection


@section('seo:schema')
	@parent
	@if(isset($data['seo']['schema']) && count($data['seo']['schema']))
		@foreach($data['seo']['schema'] as $schema)
			{!! $schema !!}
		@endforeach
	@endif
@endsection

@section("header")
	@include('base.home.header.header-home-city')
@endsection
@section('custom-css')
	<link rel="stylesheet" href="{{ elixir('css/af-home.css') }}">
	<link rel="stylesheet" href="{{ elixir('assets/css/home.css') }}">
	<link rel="stylesheet" href="{{ elixir('css/app/inspiration-hub.css') }}">
	<link rel="stylesheet" href="{{ elixir('css/app/virtual-party.css') }}">

	<link href="https://fonts.googleapis.com/css?family=Baloo+Paaji+2|Lobster&display=swap" rel="stylesheet">
@endsection

@section("content")
	@include('virtual-party.upload-progression-loader')

	<div class="hide hidden-details-section">
		@if(isset($data['pageDetails']) && $data['pageDetails'])
			@if(isset($data['pageDetails']['id']) && $data['pageDetails']['id'])
				<input type="hidden" id="vpId" name="vpId" value="{{ $data['pageDetails']['id'] }}">
			@endif
			<input type="hidden" id="celebName" value="{{ $data['pageDetails']['celebrity_text'] }}">
			<input type="hidden" id="partyDate" value="{{ $data['pageDetails']['party_date_time'] }}">
			<input type="hidden" id="partyZoomLink" value="{{ $data['pageDetails']['zoom_link'] }}">
			<input type="hidden" id="isPartyCompleted" value="{{ $data['pageDetails']['is_party_completed'] }}">

		@endif
	</div>

	<div class="bg-white-imp pad-l-15" style="padding-left:20px;padding-right:20px;padding-bottom:20px;font-family: 'Baloo Paaji 2', cursive !important;background-image:url('https://gallery.evibe.in/main/img/icons/virtual-party.png');background-repeat:repeat;">
		<div class="w-1366">
			<div>
				@php $d = DateTime::createFromFormat('Y-m-d H:i:s', $data['pageDetails']["party_date_time"]); @endphp
				<div class="col-xs-12 col-md-7 no-pad-l col-lg-7 pad-t-20  no-pad-r__400-600">
					<div class="virtual-party-left-section">
						<h1 class="heading-bdy-text pad-t-15">{{ $data['pageDetails']['celebrity_text'] }}'s {{ $data['pageDetails']['event'] }}</h1>
						<p class="virtual-party-date">{{ date('d M Y, h:i A', $d->getTimestamp()) }}</p>
					</div>
				</div>
				<div class="col-xs-12 col-md-5 no-pad-l no-pad-r col-lg-5 pad-t-20">
					<div class="header-wrap">
						<h4 style="margin:0;font-size:16px;color:grey;margin-top:20px" class="party-notice hide"> Party starts in</h4>
						<div id="countdown-wrap" class="hide">
							<p id="expire-time" class="count-down-wrap no-mar-t"></p>
						</div>
						<div class="mar-t-15" style="">
							<button id="join-party-btn" class="floating-evibe-btn header-cta-btn hide">
								<img src="{{ $galleryUrl }}/main/img/icons/join.png">
								<span style="line-height: 22px;">Join Party</span>
							</button>
							<button  class="openSendWishesModalBtn floating-evibe-btn header-cta-btn">
								<img src="{{ $galleryUrl }}/main/img/icons/wish.png">
								<span style="line-height: 22px;">Wish {{ $data['pageDetails']['celebrity_text'] }}</span>
							</button>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<hr style="background: linear-gradient(to right, transparent, #adadad, transparent); width: 86%; margin: 40px 7% 30px 7%;">
			<div id="addFeed" >
					
			</div>
		</div>
	</div>

	@include('virtual-party.modals.waiting')
	@include('virtual-party.modals.wishes')
	@include('virtual-party.modals.decline-rsvp')
	@include('virtual-party.modals.accept-rsvp')
	@include('virtual-party.modals.intermediate-join-party-modal')
	<div class="clearfix"></div>
@endsection
@section("footer")
	<div class="footer-wrap">
		@include('base.home.footer.footer-common')
	</div>
@endsection
@section("javascript")
	@parent
	<!-- AddEvent script -->
	<script type="text/javascript" src="https://addevent.com/libs/atc/1.6.1/atc.min.js" async defer></script>

	<script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js'></script>

	<script>
		$(document).ready(function () {
			function waitForvidLoad(vids, callback) {
		        if(vids.length === 0){
		            callback();
		        }
			    var vidsLoaded = 0;
			    vids.on('loadeddata', function() {
			      vidsLoaded++;
			      if (vids.length === vidsLoaded) {
			        callback();
			      }
			    });
			  }
			(function showFeed(){
				var feedFetchUrl = '/virtual-party/fetch-feed';
				$.ajax({
					url: feedFetchUrl,
					type: "POST",
					data: {
						'pageId' : $("#vpId").val(),
						'celebName' : $("#celebName").val()
					},
					success: function (data) {

							$("#addFeed").append(data);
							
							var vids = $('#addFeed').find('video');

							$.getScript("/js/app/pt.js", function () {
							var $grid = $('#inspirations-gallery').masonry({
								itemSelector: '.hero-grid__item',
								percentPosition: true,
								horizontalOrder: true


							});
							$grid.imagesLoaded(function () {
									$grid.masonry();


							});

							waitForvidLoad(vids, function() {
							 		
									$grid.masonry();
									
							});
							$("#inspirations-gallery").removeClass("hide");
								
						});

						

					},
					error: function (jqXHR, textStatus, errorThrown) {
						window.showNotyError("Some error occurred  while fetching the feed");
						
					}
				});


			})();

			(function unselectRadioButton(){
				$("input:radio").on("click",function (e) {
				    var inp=$(this); 
				    if (inp.is(".theone")) { 
				        inp.prop("checked",false).removeClass("theone");
				        $(".upload-file-wrap").addClass('hide');
				        return 0;
				    }
				    $("input:radio[name='"+inp.prop("name")+"'].theone").removeClass("theone");
				    inp.addClass("theone");
				    $(".upload-file-wrap").removeClass('hide');
				});
			})();
			(function closeNoticeCard(){
				$(".close-notice-btn").on('click',function(){
					$(".notice-card").hide();
				});
			})();
			(function showCountDown() {
				var partyDate = $('#partyDate').val();
				partyDate = partyDate.replace(" ","T");
				var countDownDate = new Date(partyDate).getTime();

				var x = setInterval(function () {
					var now = new Date().getTime();
					var distance = countDownDate - now;
					var days = Math.floor(distance / (1000 * 60 * 60 * 24));
					var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
					var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
					var seconds = Math.floor((distance % (1000 * 60)) / 1000);
					if(hours <= -5)
					{
						$(".party-notice").hide();
						$("#join-party-btn").hide();	
						$("#rsvpSection").hide();
						clearInterval(x);
					}
					else
					{
						((days / 10) < 1) ? days = "0" + days : days;
						((hours / 10) < 1) ? hours = "0" + hours : hours;
						((minutes / 10) < 1) ? minutes = "0" + minutes : minutes;
						((seconds / 10) < 1) ? seconds = "0" + seconds : seconds;
						
						@if($agent->isMobile())
							if(days>0)
							{
								document.getElementById("expire-time").innerHTML = "<div class='time-wrap'><span id='expire-days' class='time-bg'>" + days + "</span><span class='time-in'>Days</span></div><span style='font-size:35px;margin-top:-10px'>&nbsp;&nbsp;</span><div class='time-wrap'><span id='expire-hours' class='time-bg'>" + hours + "</span><span class='time-in'>Hours</span></div><span style='font-size:35px'>&nbsp;&nbsp;</span><div class='time-wrap'><span id='expire-minutes' class='time-bg'>" + minutes + "</span><span class='time-in'>Mins</span></div>";

							}
							else
							{
								document.getElementById("expire-time").innerHTML = "<div class='time-wrap'><span id='expire-hours' class='time-bg'>" + hours + "</span><span class='time-in'>Hours</span></div><span style='font-size:35px'>&nbsp;&nbsp;</span><div class='time-wrap'><span id='expire-minutes' class='time-bg'>" + minutes + "</span><span class='time-in'>Mins</span></div><span style='font-size:35px'>&nbsp;&nbsp;</span><div class='time-wrap'><span id='expire-secs' class='time-bg'>" + seconds + "</span><span class='time-in'>Secs</span></div>";

							}
						@else

							document.getElementById("expire-time").innerHTML = "<div class='time-wrap'><span id='expire-days' class='time-bg'>" + days + "</span><span class='time-in'>Days</span></div><span style='font-size:35px;margin-top:-10px'>&nbsp;&nbsp;</span><div class='time-wrap'><span id='expire-hours' class='time-bg'>" + hours + "</span><span class='time-in'>Hours</span></div><span style='font-size:35px'>&nbsp;&nbsp;</span><div class='time-wrap'><span id='expire-minutes' class='time-bg'>" + minutes + "</span><span class='time-in'>Mins</span></div><span style='font-size:35px'>&nbsp;&nbsp;</span><div class='time-wrap'><span id='expire-secs' class='time-bg'>" + seconds + "</span><span class='time-in'>Secs</span></div>";

						@endif
					
						if (distance < 0) {
							

							document.getElementById("countdown-wrap").classList.add("hide");
							$(".party-notice").text('Join party  by clicking below button');
							clearInterval(x);

						}	
					}
					
				}, 1000);

			})();

			function declineRSVP() {
				$("#modalDeclineRSVP").modal('show');
			}

			function acceptRSVP() {
				$("#modalAcceptRSVP").modal('show');
			}

			function uploadProgression() {
				$("#modalUploadProgression").modal('show');
			}

			var urlParams = window.getUrlParameters();
			if(urlParams && urlParams['type'] && (urlParams['type'] === 'wish')) {
				/* hide party timer and party join CTA */
				$("#join-party-btn").addClass('hide');
				$(".party-notice").addClass('hide');
				$("#countdown-wrap").addClass('hide');
				$("#rsvpSection").addClass('hide');
			} else {
				/* show party timer and party join CTA */
				$("#join-party-btn").removeClass('hide');
				$(".party-notice").removeClass('hide');
				$("#countdown-wrap").removeClass('hide');
				$("#rsvpSection").removeClass('hide');
			}

			$("#join-party-btn").on("click", function () {
				var partyDate = $('#partyDate').val();
				var countDownDate = new Date(partyDate).getTime();
				var now = new Date().getTime();
				var distance = countDownDate - now;
				var days = Math.floor(distance / (1000 * 60 * 60 * 24));
				var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				var seconds = Math.floor((distance % (1000 * 60)) / 1000);
				
					var eventUrl = $("#partyZoomLink").val();
					var eventTab = window.open(eventUrl, '_blank');
  					eventTab.focus();
					
				
			});

			$(".audio-thumb").on("click", function () {
				if ($(this).hasClass("active")) {
					$(this).next().get(0).pause();
					$(this).removeClass("active");
				} else {
					$(this).next().get(0).play();
					$(this).addClass("active");
				}

			});

			$('#modalSendWishes, #modalDeclineRSVP, #modalAcceptRSVP, #modalUploadProgression').modal({
				"backdrop": 'static',
				"keyboard": false,
				"show": false
			});

			$(document).on('click','.openSendWishesModalBtn', function (event) {
				event.preventDefault();
				$("#modalSendWishes").modal('show');
			});
		
			
			$("input[type=radio][name=sendWishesUploadType]").on('change', function () {
				if (this.value == 1) {
					/* image */
					$('.image-upload-wrap').removeClass('hide');
					$('.video-upload-wrap').addClass('hide');
				} else if (this.value == 2) {
					/* video */
					$('.video-upload-wrap').removeClass('hide');
					$('.image-upload-wrap').addClass('hide');
				}
			});

			$("#validateWishesBtn").on('click', function (event) {
				event.preventDefault();
				$("#validateWishesBtn").text("Please Wait...");
				$("#validateWishesBtn").prop('disabled',true);
				var formData = new FormData();

				var wishCard = $("#wishCard")[0].files;
				var wishVideo = $("#wishVideo")[0].files;
				var $uploadType = $('input[name="sendWishesUploadType"]:checked').val();

				formData.append('wishComments', $('#sendWishesComments').val());
				formData.append('uploadType', $uploadType);
				formData.append('wishCard', wishCard[0]);
				formData.append('wishVideo', wishVideo[0]);

				if(!(($('#sendWishesComments').val()).length))
				{
					window.showNotyError("Please enter message.");
					$("#validateWishesBtn").text("Post");
					$("#validateWishesBtn").prop('disabled',false);
					return false;
				}
				if ($uploadType === '2') {
					if (wishVideo[0].size > 100000000) {
						/* limit the file upload size */
						window.showNotyError("Uploaded video size is too big. Please upload a video of size less than 100 MB");
						$("#validateWishesBtn").text("Post");
						$("#validateWishesBtn").prop('disabled',false);
						return false;
					}
					$("#uploadProgression").removeClass('hide');
				}

				$.ajax({
					url: $(this).data('url'),
					type: "POST",
					data: formData,
					cache: false,
					processData: false,
					contentType: false,
					success: function (data) {
						$("#uploadProgression").addClass('hide');
						if (data.success) {
							$('#modalSendWishes .wish-details-wrap').addClass('hide');
							$('#modalSendWishes .contact-details-wrap').removeClass('hide');
						} else {
							var $errorMsg = "Some error occurred while sending your wishes. Please try again.";
							$("#validateWishesBtn").text("Post");
							$("#validateWishesBtn").prop('disabled',false);
							if (data.errorMsg) {
								$errorMsg = data.errorMsg;
							}
							window.showNotyError($errorMsg);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						$("#uploadProgression").addClass('hide');
						$("#validateWishesBtn").text("Post");
						$("#validateWishesBtn").prop('disabled',false);
						window.showNotyError("Some error occurred while submitting your request");
						window.notifyTeam({
							"url": url,
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			});

			$("#sendWishesBtn").on('click', function (event) {
				event.preventDefault();
				$("#sendWishesBtn").text("Sending...");
				$(this).prop("disabled",true);
				var formData = new FormData();
				var $uploadType = $('input[name="sendWishesUploadType"]:checked').val();
				var wishCard = $("#wishCard")[0].files;
				var wishVideo = $("#wishVideo")[0].files;

				formData.append('vpId', $('#vpId').val());
				formData.append('sendWishesName', $('#sendWishesName').val());
				formData.append('sendWishesEmail', $('#sendWishesEmail').val());
				/* formData.append('sendWishesRelationship', $('#sendWishesRelationship').val()); */
				formData.append('wishComments', $('#sendWishesComments').val());
				formData.append('sendWishesRSVP', $('#sendWishesRSVP').val());
				formData.append('uploadType', $uploadType);
				formData.append('wishCard', wishCard[0]);
				formData.append('wishVideo', wishVideo[0]);

				$.ajax({
					url: $(this).data('url'),
					type: "POST",
					data: formData,
					cache: false,
					processData: false,
					contentType: false,
					success: function (data) {
						if (data.success) {
							$("#uploadProgression").addClass('hide');
							$('#modalSendWishes .contact-details-wrap').addClass('hide');
							$('#modalSendWishes .thank-you-wrap').removeClass('hide');
							/* hiding modal close button so that user will click on "View Feed", which will refresh the page */
							$('.send-wishes-modal-close-btn').addClass('hide');
							$('#modalSendWishes .modal-content').removeClass('mar-r-20');
						} else {
							$("#sendWishesBtn").prop('disabled',false);
							$("#sendWishesBtn").text("Send");

							$("#uploadProgression").addClass('hide');
							var $errorMsg = "Some error occurred while sending your wishes. Please try again.";
							if (data.errorMsg) {
								$errorMsg = data.errorMsg;
							}
							window.showNotyError($errorMsg);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						$("#sendWishesBtn").prop('disabled',false);
						$("#sendWishesBtn").text("Send");
						window.showNotyError("Some error occurred while submitting your request");
						window.notifyTeam({
							"url": url,
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			});

			$("#refreshSendWishesModalBtn").on('click', function (event) {
				event.preventDefault();
				window.location.reload();
			});

			$("#vpAcceptRSVPBtn").on('click', function (event) {
				event.preventDefault();
				$("#modalAcceptRSVP").modal('show');
			});

			$("#vpDeclineRSVPBtn").on('click', function (event) {
				event.preventDefault();
				$("#modalDeclineRSVP").modal('show');
			});

			$("#declineRSVPSendWishBtn").on('click', function (event) {
				event.preventDefault();
				$("#modalDeclineRSVP").modal('hide');
				$("#modalSendWishes").modal('show');

				$("#sendWishesRSVP").val('No');
			});

			$("#submitAcceptRSVPBtn").on('click', function (event) {
				event.preventDefault();

				$.ajax({
					url: $(this).data('url'),
					type: "POST",
					data: {
						'vpId': $("#vpId").val(),
						'acceptRSVPName': $("#acceptRSVPName").val(),
						'acceptRSVPEmail': $("#acceptRSVPEmail").val(),
						/*'acceptRSVPRelationship': $("#acceptRSVPRelationship").val()*/
					},
					dataType: "JSON",
					success: function (data) {
						if (data.success) {
							$(".contact-details-wrap").addClass('hide');
							$(".calender-event-wrap").removeClass('hide');
						} else {
							var $errorMsg = "Some error occurred while sending your wishes. Please try again.";
							if (data.errorMsg) {
								$errorMsg = data.errorMsg;
							}
							window.showNotyError($errorMsg);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						window.showNotyError("Some error occurred while submitting your request");
						window.notifyTeam({
							"url": url,
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			});

		});
	</script>

@endsection