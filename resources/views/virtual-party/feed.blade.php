<section>
				
					@php $colors = array('red','green','blue','brown','pink','grey','black'); @endphp
					@if($data['feed'] && count($data['feed']))
						<div class="grid hide" id="inspirations-gallery" style="margin: auto auto 30px;">
						@if($agent->isMobile())

							<div class="grid-sizer" style="width: 50% !important"></div>
							@foreach($data['feed'] as $wish)
								<div class="col-xs-12 text-center " style="margin-bottom:30px !important;text-align:left;padding-left:40px;padding-right:40px;border-radius:15px;background: #a8ff78;   box-shadow: 0 0 6px 2px rgba(0,0,0,.1);background-color:white;overflow:hidden;min-height:100px;margin-right: 5px;margin-left: 5px;margin-top: 0;margin-bottom: 0; ">
									<div style=" min-height:150px; padding-top:10px;  border-radius: 2px;  display: inline-block;">
										<div style="margin-left:-30px;;display:inline-block;width:35px;height:35px;color:white;border-radius:50%;background-color:#F05E23;text-align:center;padding-top:6px;">{{ substr( $wish->partyGuest['name'] ,0,2) }}</div>
										<div style="display:inline-block;padding-left:10px;color:#3b3838bd;font-weight:600;margin-bottom:15px;">{{ $wish->partyGuest['name'] }} Sent Message</div>
										<div class="clearfix"></div>
										@if($wish['upload_type'] == 1)
											<div class="msg-wrapper">
												@if(!(is_null($wish['wish_text'])) || $wish['wish_text']!="")
													<div class='wish-text'>
														{{ utf8_decode($wish['wish_text']) }}
													</div>
												@endif
												<img src="{{ $galleryUrl.'/virtual-party/'.$data['pageId'].'/wishes/'.$wish['vp_guest_id'].'/'.$wish['upload_url'] }}" style="width: 100%;padding-left:10px;padding-right:10px;box-shadow:none">
											</div>
										@elseif($wish['upload_type'] == 2)
											<div class="msg-wrapper text-center">
												@if(!(is_null($wish['wish_text'])) || $wish['wish_text']!="")
													<div class='wish-text'>
														{{ utf8_decode($wish['wish_text']) }}
													</div>
												@endif
												<video width="100%" height="auto" controls>
													@php $ext = pathinfo($wish['upload_url'], PATHINFO_EXTENSION); @endphp
													@if($ext=="mp4")
														<source src="{{ $galleryUrl.'/virtual-party/'.$data['pageId'].'/wishes/'.$wish['vp_guest_id'].'/'.$wish['upload_url'] }}#t=0.2" type="video/mp4">
													@elseif($ext=="webm")
														<source src="{{ $galleryUrl.'/virtual-party/'.$data['pageId'].'/wishes/'.$wish['vp_guest_id'].'/'.$wish['upload_url'] }}#t=0.2" type="video/webm">
													@elseif($ext=="ogg")
														<source src="{{ $galleryUrl.'/virtual-party/'.$data['pageId'].'/wishes/'.$wish['vp_guest_id'].'/'.$wish['upload_url'] }}#t=0.2" type="video/ogg">
													@elseif($ext=="mov" || $ext == "MOV")
														<source src="{{ $galleryUrl.'/virtual-party/'.$data['pageId'].'/wishes/'.$wish['vp_guest_id'].'/'.$wish['upload_url'] }}#t=0.2" type="video/mp4">
													@endif
													Your browser does not support the video.
												</video>
												<div class="clearfix"></div>
											</div>
										@else
											@if(!(is_null($wish['wish_text'])) || $wish['wish_text']!="")
													<div class='wish-text'>
														{{ utf8_decode($wish['wish_text']) }}
													</div>
											@endif
										@endif
										<div style="position:absolute;bottom:5px;right:30px">
											<span>{{ date_format($wish['created_at'],"d M y, h:i A") }}</span>
										</div>
									</div>
								</div>
							@endforeach
						@else
							<div class="grid-sizer" style="width: 30%"></div>
							@foreach($data['feed'] as $wish)
								<div class="grid-item hero-grid__item text-center" style="width:33% !important;">
									<div class="feed-item-wrap">
										<div class="contact-wrap" style="background-color:{{ $colors[mt_rand(0,count($colors)-1)] }};">{{ substr( $wish->partyGuest['name'] ,0,2) }}</div>
										<div class="contact-msg">{{ $wish->partyGuest['name'] }} sent a message </div>
										<div class="clearfix"></div>
										@if($wish['upload_type'] == 1)
											<div class="msg-wrapper">
												@if(!(is_null($wish['wish_text'])) || $wish['wish_text']!="")
													<div class='wish-text'>
														{{ utf8_decode($wish['wish_text']) }}
													</div>
												@endif
												<img src="{{ $galleryUrl.'/virtual-party/'.$data['pageId'].'/wishes/'.$wish['vp_guest_id'].'/'.$wish['upload_url'] }}" style="width: 100%;padding-left:10px;padding-right:10px;box-shadow:none">
											</div>
										@elseif($wish['upload_type'] == 2)
											<div class="msg-wrapper">
												@if(!(is_null($wish['wish_text'])) || $wish['wish_text']!="")
													<div class='wish-text'>
														{{ utf8_decode($wish['wish_text']) }}
													</div>
												@endif
												<video width="100%" height="auto" controls>
													@php $ext = pathinfo($wish['upload_url'], PATHINFO_EXTENSION); @endphp
													@if($ext=="mp4")
														<source src="{{ $galleryUrl.'/virtual-party/'.$data['pageId'].'/wishes/'.$wish['vp_guest_id'].'/'.$wish['upload_url'] }}#t=0.2" type="video/mp4">
													@elseif($ext=="webm")
														<source src="{{ $galleryUrl.'/virtual-party/'.$data['pageId'].'/wishes/'.$wish['vp_guest_id'].'/'.$wish['upload_url'] }}#t=0.2" type="video/webm">
													@elseif($ext=="ogg")
														<source src="{{ $galleryUrl.'/virtual-party/'.$data['pageId'].'/wishes/'.$wish['vp_guest_id'].'/'.$wish['upload_url'] }}#t=0.2" type="video/ogg">
													@elseif($ext=="mov" || $ext == "MOV")
														<source src="{{ $galleryUrl.'/virtual-party/'.$data['pageId'].'/wishes/'.$wish['vp_guest_id'].'/'.$wish['upload_url'] }}#t=0.2" type="video/mp4">
													@endif
													Your browser does not support the video.
												</video>
												<div class="clearfix"></div>
											</div>
										@else
											@if(!(is_null($wish['wish_text'])) || $wish['wish_text']!="")
													<div class='wish-text'>
														{{ utf8_decode($wish['wish_text']) }}
													</div>
											@endif
										@endif
										<div class="clearfix"></div>
										<div style="float:right;;bottom:5px;right:30px;padding-bottom:10px">
											<span>
												
											{{ date_format($wish['created_at'],"d M y, h:i A") }}</span>
										</div>
									</div>
								</div>
							@endforeach
						@endif
						</div>
					@else

						<div class="notice-card" style="height:auto;background-color:white;box-shadow:0 1px 4px 0 rgba(0,0,0,0.1), 0 2px 4px 0 rgba(0,0,0,0.1);border-top:4px solid #ED3E72;border-radius:10px;padding-bottom: 5px">
							<span class='close-notice-btn' style="float:right;padding-right:15px;padding-top:15px;font-size:26px">x</span>
							<div class="text-center    " >
								<h5>Be the first to wish {{ $data['celebName'] }} 
									<br>
								<button class="btn header-cta-btn openSendWishesModalBtn" style="display:inline-block;width:auto;border:1px solid #777;background-color:white">Wish Now</button> 
								
								</h5>

							</div>
						</div>
						<div class="clearfix"></div>

			
					@endif
					<div class="clearfix"></div>
			
			</section>

