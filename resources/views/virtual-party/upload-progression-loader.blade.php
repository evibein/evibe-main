<div id="uploadProgression" class="upload-progression-wrap hide">
	<div class="upload-progression text-center">
		<div class="">
			<img src="{{ $galleryUrl }}/main/img/icons/file-uploading.gif" height="150px" alt="success icon">
		</div>
		<div class="text-center mar-t-15 text-e font-20 hide">
			Uploading your video
		</div>
	</div>
</div>