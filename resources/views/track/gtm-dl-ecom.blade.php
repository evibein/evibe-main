@php
	function getTransactionJs(&$trans) {
	return <<<HTML
{
	'id': '{$trans['id']}',
	'affiliation': '{$trans['affiliation']}',
	'revenue': '{$trans['revenue']}',
	'shipping': '{$trans['shipping']}',
	'tax': '{$trans['tax']}',
	'currency': '{$trans['currency']}'
}
HTML;
}

function getItemJs(&$transId, &$item) {
  return <<<HTML
{
  'id': '$transId',
  'name': '{$item['name']}',
  'sku': '{$item['sku']}',
  'category': '{$item['category']}',
  'price': '{$item['price']}',
  'quantity': '{$item['quantity']}',
  'currency': '{$item['currency']}'
},
HTML;
}
@endphp

<input type="hidden" id="transactionTotal" value="{{ $trans["revenue"] }}">
<input type="hidden" id="transactionTax" value="{{ $trans["tax"] }}">
<input type="hidden" id="transactionShipping" value="{{ $trans["shipping"] }}">

<script>
	window.dataLayer = window.dataLayer || [];
	dataLayer.push({
		'transactionId': '@php echo $transId; @endphp',
		'transactionAffiliation': '@php echo $trans["affiliation"]; @endphp',
		'transactionTotal': JSON.parse(document.getElementById('transactionTotal').value),
		'transactionTax': JSON.parse(document.getElementById('transactionTax').value),
		'transactionShipping': JSON.parse(document.getElementById('transactionShipping').value),
		'transactionProducts': [
			@php
				if (isset($transId) && isset($item)) {
					echo getItemJs($transId, $item);
				}
				if (isset($transId) && isset($items)) {
					foreach ($items as &$item) {
						echo getItemJs($transId, $item);
					}
				}
			@endphp
		]
	});
</script>