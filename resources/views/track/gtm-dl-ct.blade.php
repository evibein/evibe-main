@if (isset($transactionId) && $transactionId)
	<script>
		window.dataLayer = window.dataLayer || [];
		dataLayer.push({
			'transactionId': '@php echo $transactionId; @endphp'
		});
	</script>
@endif