<!-- OneSignal web notifications -->
<link rel="manifest" href="/manifest.json">
<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
<script type="text/javascript">
	var appId = "<?php echo config("onesignal.main.appId"); ?>";
	var cityName = "<?php echo strtolower(getCityName()); ?>";
	var occasionName = "<?php echo $pushOccasion; ?>";
	var OneSignal = window.OneSignal || [];
	var pushMessage = {
		"birthday": "Get best deals & latest themes, trends on birthday parties, house parties.",
		"youth": "Get best deals & latest trends on get together, youth parties, outings.",
		"surprises": "Get best deals & latest packages related to surprises.",
		"house-warming": "Get best deals and latest trends on house warming ceremonies.",
		"pre-post": "Get best deals & latest trends on pre/post wedding ceremonies."
	};

	OneSignal.push(["init", {
		"appId": appId,
		"autoRegister": false,
		"notifyButton": {
			"enable": false
		},
		"promptOptions": {
			/* actionMessage limited to 90 characters */
			"actionMessage": pushMessage[occasionName],

			/* acceptButtonText limited to 15 characters */
			"acceptButtonText": "ALLOW NOW",

			/* cancelButtonText limited to 15 characters */
			"cancelButtonText": "NO THANKS"
		},
		"welcomeNotification": {
			"disable": true,
			"title": "Evibe.in",
			"message": "Thanks for subscribing"
		},
		"safari_web_id": 'web.onesignal.auto.365cbfbd-b203-4342-b6f2-394fa1a1712a',
		"persistNotification": false,
		"allowLocalhostAsSecureOrigin": true
	}]);

	/* trigger permission message after 12 seconds */
	setTimeout(function () {
		OneSignal.push(function () {

			/* register */
			OneSignal.isPushNotificationsEnabled(function (isEnabled) {
				if (!isEnabled) {
					/**
					 * see: disabled on 8 Jul 2017
					 * needs re-work based on cookies / sessions
					 */
					/* OneSignal.showHttpPrompt(); */
				}
			});

			/* send tags (create/update) */
			OneSignal.sendTags({
				"city": cityName,
				"occasion": occasionName
			});
		});
	}, 12000);

</script>