@extends("layout.base")

@section("page-title")
	<title>{{ $productName }} - All Product Reviews | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="Showing all customer reviews for {{ $productName }}"/>
@endsection

@section('meta-keywords')
	<meta name="keywords" content="customer reviews, {{ $productName }} customer reviews"/>
@endsection

@section('og-title')
	<meta property="og:title" content="{{ $productName }} - All Product Reviews | Evibe.in"/>
@endsection

@section('og-description')
	<meta property="og:description" content="Showing all customer reviews for {{ $productName }}"/>
@endsection

@section("header")
	@include('base.home.header.header-home-city')
	<div class="header-border full-width mar-b-10"></div>
@endsection

@section("footer")
	@include('base.home.footer.footer-common')
@endsection

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-all-reviews.css') }}">
@endsection

@section("content")
	<div class="reviews-card page-review-all-wrapper">
		<div class="container">
			<h4>Showing All Product Reviews For <a href="{{ $productUrl }}?ref=all-reviews">{{ $productName }}</a></h4>
			@if($highlights["showHighlights"])
				<div class="review-highlights-wrap">
					<div class="col-sm-6 no-pad">
						<div class="review-highlight-data-wrap top-positive">
							<h5 class="review-highlight-title">Top Positive Review</h5>
							<div class="review-highlight-data">
								@include("app.review.review-item", [
										"review" => $highlights["topPositiveReview"],
										"isShowLocation" => isset($isShowLocation) ? $isShowLocation : true,
										"isHighlightReview" => true
									])
							</div>
						</div>
					</div>
					<div class="col-sm-6 no-pad-r mar-t-10__400 mar-t-10-400-600 no-pad-l__400">
						<div class="review-highlight-data-wrap top-critical">
							<h5 class="review-highlight-title">Top Critical Review</h5>
							<div class="review-highlight-data">
								@include("app.review.review-item", [
										"review" => $highlights["topCriticalReview"],
										"isShowLocation" => isset($isShowLocation) ? $isShowLocation : true,
										"isHighlightReview" => true
									])
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			@endif
			<div class="review-filters-wrap">
				<div class="in-blk">
					<label for="rSortOptions">Sort by</label>
				</div>
				<div class="in-blk mar-l-15">
					<div class="form-group">
						<select name="rSortOptions" id="rSortOptions" class="form-control">
							<option value="topRated" @if($sortBy == "topRated") selected="selected" @endif>Top Rated
							</option>
							<option value="mostRecent" @if($sortBy == "mostRecent") selected="selected" @endif>Most Recent
							</option>
							<option value="leastRated" @if($sortBy == "leastRated") selected="selected" @endif>Least Rated
							</option>
						</select>
					</div>
				</div>
			</div>
			@if (isset($reviews) && $reviews->count())
				<ul class="reviews-list ls-none no-pad no-mar">
					@foreach($reviews as $review)
						<li class="review">
							@include("app.review.review-item", [
									"review" => $review,
									"isShowLocation" => isset($isShowLocation) ? $isShowLocation : true
								])
						</li>
					@endforeach
				</ul>
				<div class="mar-t-20 text-center">
					<div>{{ $reviews->appends(["sort" => $sortBy])->links() }}</div>
				</div>
			@else
				<div class="alert alert-danger">
					Sorry, no reviews were found for this provider.
					<a href="{{ request()->url() }}">Try Reset Filters.</a>
				</div>
			@endif
		</div>
	</div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			var reviewsUtilJs = "<?php echo elixir('js/app/reviews_util.js'); ?>";
			$.getScript(reviewsUtilJs)
				.done(function (script, textStatus) {
					$("#rSortOptions").unbind("change") /* remove previous listener from reviews_util. */
						.change(function () {
							var sortBy = $(this).val();
							var params = "?sort=" + sortBy + "&page=1";

							window.location = (window.location.origin + window.location.pathname + params);
						});
				});
		});
	</script>
@endsection