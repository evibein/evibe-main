@php
	$breakPos = strlen($content);
	if ($breakPos > $charsCount) {
		$breakPos = strpos(wordwrap($content, $charsCount), "\n");
	}
@endphp
<span class="r-txt-wrap">
	<span>{{ substr($content, 0, $breakPos) }}</span>
	@if (strlen($content) > $charsCount)
		<span class="text-extras truncated-text-view">
			<span class="review-ellipsis">..</span>
			<a class="link-read link-read-more">read more</a>
		</span>
		<span class="text-extras extra-text-view hide">
			<span class="more-review-text ">{{ substr($content, $breakPos) }}</span>
			<a class="link-read link-read-less">(read less)</a>
		</span>
	@endif
</span>