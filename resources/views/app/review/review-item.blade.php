@php
	$reviewCharsCount = 250; $replyCharsCount = 150;
	if (isset($isHighlightReview)) {$reviewCharsCount = 120; $replyCharsCount = 75;}
@endphp
<div class="a-review-wrap">
	<div class="review-data-wrap">
		<div class="in-blk">
			<input type="text" class="review-rating hide" value="{{ $review['rating'] }}">
		</div>
		<div class="in-blk reviewer text-col-gr">{{ $review['name'] }}
			@if ($isShowLocation && isset($review['location']) && $review['location'])
				<span style="color:#333; font-size:13px; font-weight: 400"> ({{ $review['location'] }})</span>
			@endif
		</div>
	</div>
	@foreach($review['individuals'] as $individual)
		<div class="in-blk mar-r-15">
			<div class="in-blk font-12 text-muted font-italic">
				{{ $individual['question'] }}
			</div>
			<div class="in-blk font-10 mar-l-5 individual-review">
				<input type="text" class="review-rating hide" value="{{ $individual['rating'] }}">
			</div>
		</div>
	@endforeach
	<div class="mar-t-5">
		<div class="review-txt">
			@include("app.review.review-item-text", [
				"content" => $review["review"],
				"charsCount" => $reviewCharsCount
			])
		</div>
		@if(count($review['extraTicketReviews']) > 0)
			@foreach($review['extraTicketReviews'] as $extraReview)
				<div class="font-12 text-muted font-italic">
					{{ $extraReview['question'] }} {{ $extraReview['answers'] }}
				</div>
			@endforeach
		@endif
		@if ($review['reply'])
			<div class="review-reply @if(isset($isHighlightReview)) no-pad no-mar-r @endif">
				<span><i class="glyphicon glyphicon-share-alt"></i><b> Provider Response: </b></span>
				<span>
					@include("app.review.review-item-text", [
						"content" => $review['reply'],
						"charsCount" => $replyCharsCount
					])
				</span>
			</div>
		@endif
	</div>
</div>