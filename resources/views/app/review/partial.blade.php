<div class="review-filters-wrap">
	<div class="col-sm-6 text-left no-pad valign-mid">
		<div class="in-blk">
			<label for="rSortOptions" class="review-sort-lbl">Sort by</label>
		</div>
		<div class="in-blk mar-l-15">
			<div class="form-group">
				<select name="rSortOptions" id="rSortOptions" class="form-control">
					<option value="r-srt-top-rated" selected="selected">Top Rated</option>
					<option value="r-srt-most-recent">Most Recent</option>
					<option value="r-srt-least-rated">Least Rated</option>
				</select>
			</div>
		</div>
	</div>
	@if($reviews["total"]["count"] > 10 && isset($showAllReviewsUrl))
		<div class="col-sm-6 text-right valign-mid no-pad pad-t-10__400 text-left__400 hide__400">
			<a class="btn btn-link no-pad-l__400 no-pad-b__400" href="{{ $showAllReviewsUrl }}" target="_blank">
				Show All {{ $reviews["total"]["count"] }} Reviews
			</a>
		</div>
	@endif
	<div class="clearfix"></div>
</div>
<ul class="reviews-list ls-none no-pad no-mar">
	@foreach($reviews["reviews"]["topRated"] as $review)
		<li class="review r-review r-srt-top-rated">
			@include("app.review.review-item", [
					"review" => $review,
					"isShowLocation" => isset($isShowLocation) ? $isShowLocation : true
				])
		</li>
	@endforeach

	@foreach($reviews["reviews"]["mostRecent"] as $mostRecentReview)
		<li class="review r-review r-srt-most-recent hide">
			@include("app.review.review-item", [
					"review" => $mostRecentReview,
					"isShowLocation" => isset($isShowLocation) ? $isShowLocation : true
				])
		</li>
	@endforeach

	@foreach($reviews["reviews"]["leastRated"] as $leastRatedReview)
		<li class="review r-review r-srt-least-rated hide">
			@include("app.review.review-item", [
					"review" => $leastRatedReview,
					"isShowLocation" => isset($isShowLocation) ? $isShowLocation : true
				])
		</li>
	@endforeach
</ul>
@if($reviews["total"]["count"] > 10 && isset($showAllReviewsUrl))
	<div class="text-center product-reviews-link">
		<a class="btn btn-link " href="{{ $showAllReviewsUrl }}" target="_blank">
			Show All {{ $reviews["total"]["count"] }} Reviews
		</a>
	</div>
@endif


@section("javascript")
	@parent
	<script src="{{ elixir('js/app/reviews_util.js') }}"></script>
@endsection
