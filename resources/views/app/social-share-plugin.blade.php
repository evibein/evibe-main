<!-- AddToAny BEGIN -->
<div class="text-center pad-t-5 in-blk">
	<div class="a2a_kit a2a_kit_size_38 a2a_default_style social-share-buttons-wrap">
		<a class="a2a_button_whatsapp a2a_custom_social_share_button" data-name="WhatsApp"></a>
		<a class="a2a_button_facebook_messenger a2a_custom_social_share_button" data-name="Messenger"></a>
		<a class="a2a_button_sms a2a_custom_social_share_button" data-name="SMS"></a>
		<a class="a2a_button_email a2a_custom_social_share_button" data-name="Email"></a>
	</div>
</div>

@section('javascript')
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			/* @see: Do not load JS through script to avoid UI issue */
			setTimeout(function showSocialSharePlugin() {
				$.getScript("/js/util/addtoany.js", function () {
					$(".social-share-buttons-wrap").removeClass("hide");
				});
			}, 5000);

			$('.a2a_custom_social_share_button').on('click', function () {
				var shareMedium = $(this).data("name") ? $(this).data("name") : "Other";
			});
		});
	</script>
@endsection