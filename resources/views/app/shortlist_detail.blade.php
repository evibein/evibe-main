<a id="shortlistBtn" class="btn btn-default btn-view-more btn-best-deal btn-short-list-add mar-r-10" v-on:click="shortlistOptionFromProfilePage">
	<span class="hideShortlistData" data-cityId="{{ $data['shortlistData']['cityId'] }}" data-occasionId="{{ $data['shortlistData']['occasionId'] }}" data-mapId="{{ $data['shortlistData']['mapId'] }}" data-mapTypeId="{{ $data['shortlistData']['mapTypeId'] }}" data-urlAdd="{{ route('partybag.add') }}" data-urlDelete="{{ route('partybag.delete') }}" data-plainImgWhite="{{ $galleryUrl }}/main/img/icons/party-bag_empty_w.png" data-removeImgGrey="{{ $galleryUrl }}/main/img/icons/party-bag_empty_g.png">
	</span>
	<i class="icon-bagicon5 valign-mid"></i>
	<span class="valign-mid">Shortlist</span>
</a>