@extends('layout.base')

@section("modals")
	<!-- User confirm email modal -->
	@include('app.modals.confirm_email_modal')
@show

@section("header")
	@include('app.header_splash')
@endsection

@section("footer")
	@include('app.footer_noncity')
@endsection

@section("javascript")
	<script type="text/javascript">
		$(document).ready(function () {
			$('#confirmEmailModal').modal({backdrop: 'static', keyboard: false});
		});
	</script>
@endsection