<div class="hide__400 hide-400-600">
	<div class="partybag-wrap">
		<a href="{{ route('partybag.list') }}" class="navbar-mobile">
			<img src="{{ $galleryUrl }}/main/img/icons/party-bag.png" alt="party bag" class="partybag-header">
			<div class="partybag-count" v-cloak>
				@{{ pbCount }}
			</div>
		</a>
	</div>
</div>

<div class="hide show-400-600 unhide__400">
	<div class="partybag-wrap partybag-wrap-mobile hide show-400-600 unhide__400">
		<a href="{{ route('partybag.list') }}" class="navbar-mobile">
			<img src="{{ $galleryUrl }}/main/img/icons/party-bag_w.png" alt="party bag" class="partybag-header">
			<div class="partybag-count" v-cloak>
				@{{ pbCount }}
			</div>
		</a>
	</div>
</div>