@extends('layout.base')

@section("header")
	@include('app.header_splash')
@endsection

@section("footer")
	@include('app.footer_noncity')
@endsection

@section("javascript")
	<script type="text/javascript">
		$(document).ready(function () {
			$('#cityChangeModal').modal({backdrop: 'static', keyboard: false});
		});
	</script>
@endsection