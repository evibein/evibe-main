<div class="media-section">
	<div class="bg-cover"></div>
	<div id="mediaItems" class="carousel slide" data-ride="carousel" data-interval="25000">
		<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<div class="m-text"><q>The Great Indian Party Planners</q></div>
			</div>
			<div class="item">
				<div class="m-text">
					<q>Our wedding arrangement problems gave us the idea for the startup</q></div>
			</div>
			<div class="item">
				<div class="m-text"><q>Now Planning A Party Is Just An Evibe Away!</q></div>
			</div>
		</div>
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#mediaItems" data-slide-to="0" class="active">
				<img src="{{ $galleryUrl }}/img/logo/logo_ie_200x100.png" alt="Evibe on The New Indian Express">
			</li>
			<li data-target="#mediaItems" data-slide-to="1">
				<img src="{{ $galleryUrl }}/img/logo/logo_toi_200x100.png" alt="Evibe on Times of India">
			</li>
			<li data-target="#mediaItems" data-slide-to="2">
				<img src="{{ $galleryUrl }}/img/logo/logo_bv_200x100.png" alt="Evibe on BONNE VIE News">
			</li>
		</ol>
	</div>
	<ul class="set2">
		<li>
			<img src="{{ $galleryUrl }}/img/logo/logo_ys.png" alt="Evibe on YourStory.com">
		</li>
		<li>
			<img src="{{ $galleryUrl }}/img/logo/logo_vcc.png" alt="Evibe on VCCircle">
		</li>
		<li>
			<img src="{{ $galleryUrl }}/img/logo/logo_tia.png" alt="Evibe on TechInAsia">
		</li>
		<li>
			<img src="{{ $galleryUrl }}/img/logo/logo_inc42.png" alt="Evibe on Inc 42 Magazine">
		</li>
		<li>
			<img src="{{ $galleryUrl }}/img/logo/logo_mn.png" alt="Evibe on MediaNama.com">
		</li>
	</ul>
</div>