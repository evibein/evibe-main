<form id="uploadOrderProofImagesForm" data-url="{{ route('ajax.checkout.upload-order-proofs', $data['ticketId']) }}" @if(isset($data['customerOrderProofs']) && $data['customerOrderProofs']) class="hide" @endif>
	<div class="ps-order-proofs-selection">
		<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 no-pad">
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
				<select class="mdl-textfield__input" id="inpCustomerProofType" name="inpCustomerProofType" style="height: 29px;">
					@foreach($data['proofs'] as $proof)
						<option value="{{ $proof->id }}">{{ $proof->name }}</option>
					@endforeach
				</select>
				<label class="mdl-textfield__label" for="inpCustomerSource">ID Proof Type</label>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="image-upload-rules font-12 text-info">
		(Valid Formats: JPG, jpeg, jpg, png. Max: <b><u>4 MB</u></b>)
	</div>
	<div class="form-group mar-t-10">
		<input type="hidden" id="uploadProofsUrl" value="{{ route('ajax.checkout.upload-order-proofs', $data['ticketId']) }}">
		<div class="in-blk">
			<div class="mar-t-5">
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<div>1. Front side</div>
				</div>
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<div><input type="file" name="customerProofFront" id="customerProofFront"/></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="mar-t-15">
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<div>2. Back side</div>
				</div>
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<div><input type="file" name="customerProofBack" id="customerProofBack"/></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="text-center mar-t-15">
			<button type="submit" id="proofUploadBtn" class="btn btn-warning">
				<i class="glyphicon glyphicon-upload"></i> Upload
			</button>
		</div>
	</div>
</form>