<div class="hide" id="username">{{ getAuthUserName() }}</div>
<li v-if="user == ''" class="user-login-li-wrap">
	<a href="#" class="user-login user-login-btn" v-on:click.stop.prevent="userLogin">
		<img class="hide show-400-600 unhide__400 userBlack" src="{{ $galleryUrl }}/img/icons/user-black.png">
		<img class="hide show-400-600 unhide__400 userWhite" src="{{ $galleryUrl }}/img/icons/user-white.png">
		<span class="hide-400-600">Login</span>
	</a>
</li>
<li v-else class="user-login-li-wrap">
	<a href="/my/order" class="user-login user-login-btn">
		<img class="hide show-400-600 unhide__400 userBlack" src="{{ $galleryUrl }}/img/icons/user-black.png">
		<img class="hide show-400-600 unhide__400 userWhite" src="{{ $galleryUrl }}/img/icons/user-white.png">
	</a>
	@php
		$name = getAuthUserName();
		if(strpos($name,' '))
		{
			$names = explode(" ", $name);
			$name= $names[0];
				if(isset($names[1][0]))
				{
					$name = $name . " " . $names[1][0];
				}
		}
		else
		{
			$name = strlen($name) <= 20 ? $name : substr($name, 0, 15) . "...";
		}
	@endphp
	<div class="in-blk top-header-dropdown hide-400-600">
		<a href={{route('track.orders')}}>
			<span class="hide-400-600">{{$name}}</span>
		</a>
		<div class="top-header-dropdown-content user-header-dropdown-content hide-400-600">
			<a href="{{route('track.orders')}}" class="no-mar-t no-mar-b">Track My Order</a>
			<a href="{{ route('partybag.list') }}">
				<div>My Party Bag</div>
			</a>
			<hr style="padding: 0 !important; margin: 0 !important;">
			<a href="/user/logout" class="user-login user-login-btn">LogOut</a>
		</div>
	</div>
</li>
