<a class="item-shortlist item-view-details in-blk" v-on:click="shortlistOptionFromListPage">
	@if(isset($mapId) && isset($mapTypeId) && isset($occasionId) && isset($cityId))
		<div class="hideShortlistResultsData list-cta-btn list-btn-shortlist" data-mapId="{{ $mapId }}" data-mapTypeId="{{ $mapTypeId }}" data-cityId="{{ $cityId }}" data-occasionId="{{ $occasionId }}" data-urlAdd="{{ route('partybag.add') }}" data-urlDelete="{{ route('partybag.delete') }}">
			Add To Cart
		</div>
	@endif
</a>