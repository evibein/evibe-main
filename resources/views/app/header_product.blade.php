<header>
	<div class="header-top" style="height: 74px; border-bottom: 1px solid #E0E0E0 !important;">
		<div class="in-blk mar-l-30 mar-t-15">
			<a class="" href="#">
				<img src="{{ $galleryUrl }}/img/logo/logo_evibe.png" alt="Evibe logo">
			</a>
		</div>
		<div class="in-blk pull-right mar-r-30 mar-t-15">
			<div class="header-secure-img-wrap in-blk">
				<img class="header-secure-img secure-image" src="{{ config('evibe.gallery.host') }}/img/app/evibe_shield.png" alt="Evibe guarantee">
			</div>
			<div class="in-blk mar-l-5 text-bold">100% Secure</div>
		</div>
		<div class="clearfix"></div>
	</div>
</header>

@section("modals")
	@parent
	@include('app.modals.enquiry-form', [
		"url" => route("city.header.enquire", "no-city")
	])
@endsection