@extends('layout.base')

@section('page-title')
	<title>Privacy Policy | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="privacy policy of Evibe.in"/>
@endsection

@section('meta-keywords')
	<meta name="keywords" content="Evibe.in, privacy policy"/>
@endsection

@section('og-title')
	<meta property="og:title" content="Privacy Policy | Evibe.in"/>
@endsection

@section('og-description')
	<meta property="og:description" content="privacy policy of Evibe.in."/>
@endsection

@section('og-url')
	<meta property="og:url" content="https://evibe.in/privacy"/>
@endsection

@section('meta-canonical')
	<link rel="canonical" href="{{ $canonicalUrl }}/privacy"/>
@endsection

@section("header")
	@include('base.home.header.header-top-city')
@endsection

@section("footer")
	@include('app.footer_noncity')
@endsection

@section("content")
	<article class="bkgnd-grey">
		<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
			<section class="page-sec privacy-sec page-sec-card">
				<div class="sec-head">
					<div class="sec-title-wrap">
						<i class="glyphicon glyphicon-th-list"></i>
						<h1 class="sec-title">Privacy Policy</h1>
					</div>
				</div>
				<div class="sec-body">
					<p>
						We thank you for placing trust in us. We ensure highest standards in maintaining your information privacy, please read the following statements to learn more.
					</p>
					<p>
						NOTE: This Privacy Policy is effective from
						<strong>January 12, 2015</strong>. Please be aware that our privacy policy can be changed any time without notice. So, please make sure you check this policy regularly.
					</p>
					<p>
						<strong>By visiting this Website, you agree to all the terms and conditions of this Privacy Policy.</strong>
						<span>Please do not use our Website if you do not agree. You agree to our use and disclosure of your information in accordance with this Privacy Policy just with use of the Website. This Policy is subject to the </span>
						<a href="{{ route('terms') }}" target="_blank">Terms of Use</a>.
					</p>
					<section class="privacy-items">
						<ul class="ls-none no-pad">
							<li class="privacy-item">
								<strong>1. Information Collection and Use</strong>
								<p>
									<span>Evibe Technologies Private Limited (Evibe) is the only owner of the information collected on this Website. We shall not share, sell or rent this information in any other way that is not mentioned in this Policy. Evibe collects this information from its users through </span>
									<a href="https://evibe.in">https://evibe.in</a>
									<span> (Website) or office phone number.</span>
								</p>
							</li>
							<li class="privacy-item">
								<strong>2. Personal Information</strong>
								<p>You do not need to provide any of your personal information until and unless you want to reserve / enquire about a service. You are required to give your contact information to make an enquiry and to make a reservation you should provide us with necessary payment details. In case, if you have to update or remove your personal data, we will provide a way to update, or remove your personal information.</p>
							</li>
							<li class="privacy-item">
								<strong>3. Suppliers</strong>
								<p>When you place an enquiry or make a reservation / booking using Evibe, your contact information and other special messages is provided to the Supplier or person that manages the reservations for that Supplier. Evibe will not be responsible if they fail to comply with respect to your information. Any complaints regarding use of your information by a Supplier listed on the Website should be addressed directly by the Supplier.</p>
							</li>
							<li class="privacy-item">
								<strong>4. Personal Notifications</strong>
								<p>As an Evibe user, you will receive email / sms updates from us about our new services, special offers, and other noteworthy items. Also, you can choose to receive our newsletter. However, you can unsubscribe by editing your preferences by following the "unsubscribe" link in the e-mail / sms.</p>
							</li>
							<li class="privacy-item">
								<strong>5. Statistical Analysis</strong>
								<p>In order to build quality and more useful service we perform statistical analysis of collective behaviour of all the customers and visitor, measure interests and demographics regarding preferences, ratings, satisfaction – We use non-personally identifiable information. In this regards, we release anonymous demographic and statistical information on our users to advertisers, Suppliers, affiliates and other business partners. We do this to help third party providers and our Suppliers improve their service offerings to our users.</p>
							</li>
							<li class="privacy-item">
								<strong>6. Cookies</strong>
								<p>"Cookies" are small files placed on your hard drive that are no way linked to your personally identifiable information. We use these cookies on our Website to track and target the interests of our users to enhance the experience. We offer certain features that are only available through the use of a "cookie".</p>
							</li>
							<li class="privacy-item">
								<strong>7. Log Files</strong>
								<p>We automatically log "Session Data" from your computer's connection to the Internet. Session Data constitutes information like IP Address, Operating System, and web browser, We use this data to analyze trends, track user moments and get collective demographic information for aggregate use.</p>
							</li>
							<li class="privacy-item">
								<strong>8. Links to External Websites</strong>
								<p>Our Website contains links to other sites and we are not responsible for the privacy practices of such sites. Once you leave our Website, we strongly encourage you to read the privacy statements of each and every Website that collects personally identifiable information.</p>
							</li>
							<li class="privacy-item">
								<strong>9. Legal Obligations</strong>
								<p>We put all efforts to preserve your privacy. But, we might need to disclose personal information when required by law wherein we have a great belief that such action is mandatory to comply with a court order, current judicial proceeding, or legal process served on our Website.</p>
							</li>
							<li class="privacy-item">
								<strong>10. Questions And Contact Information</strong>
								<p>If you would like to: access, correct, amend or delete any personal information we have about you, register a complaint, or simply want more information contact our Privacy Compliance Officer at {{ config('evibe.contact.company.phone') }} or by mail at {{ config('evibe.contact.company.email') }}.</p>
							</li>
						</ul>
					</section>
				</div>
			</section>
		</div>
		<div class="clearfix"></div>
	</article>
@endsection