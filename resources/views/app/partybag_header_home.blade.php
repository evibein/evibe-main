<div class="mar-t-10">
	<div class="partybag-wrap">
		<a href="{{ route('partybag.list') }}" class="navbar-mobile">
			<img src="{{ $galleryUrl }}/main/img/icons/party-bag_w.png" alt="party bag" class="partybag-header">
			<div class="partybag-count" v-cloak>
				@{{ pbCount }}
			</div>
		</a>
	</div>
</div>