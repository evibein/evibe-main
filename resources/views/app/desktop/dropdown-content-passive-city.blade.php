@php $sessionCityUrl = is_null($sessionCityUrl) ? 'bangalore' : $sessionCityUrl; @endphp
<ul class="nav navbar-nav master-home-top-navbar-left">
	<li class="dropdown dropdown-large">
		<a href="#" class="dropdown-toggle text-grey font-13" data-toggle="dropdown">FOR KIDS</a>
		<ul class="dropdown-menu dropdown-menu-large row">
			<li class="col-sm-6 ">
				<ul>
					<li class="dropdown-header nav-hover-style ">
						<a href="" class="dropdown-header nav-hover-style-link-color no-pad-l">Decorations</a>
					</li>
					<li class="nav-hover-style"><a >All Decorations</a></li>
					<li class="nav-hover-style"><a>Simple Balloon Decorations</a></li>
					<li class="nav-hover-style"><a >Customised Balloon Decorations</a></li>
					<li class="nav-hover-style"><a>Basic Theme Decorations</a></li>
					<li class="nav-hover-style"><a>Medium Theme Decorations</a></li>
					<li class="nav-hover-style"><a>High-End Theme Decorations</a></li>
					<li class="nav-hover-style"><a>Flower Decorations</a></li>
					<li class="nav-hover-style"><a>Drapes Decorations</a></li>
					<li class="dropdown-header nav-hover-style pad-t-15">
						<a class="dropdown-header nav-hover-style-link-color no-pad-l">Food</a>
					</li>
					<li class="nav-hover-style"><a >All </a></li>
					<li class="nav-hover-style"><a>Food Combo</a></li>
					<li class="nav-hover-style"><a>Snack Combo</a></li>
				</ul>
			</li>
			<li class="col-sm-3 ">
				<ul>
					<li class="dropdown-header nav-hover-style">
						<a  class="dropdown-header nav-hover-style-link-color no-pad-l">Entertainment</a>
					</li>
					<li class="nav-hover-style"><a>All Entertainment</a>
					</li>
					<li class="nav-hover-style"><a>Eateries</a></li>
					<li class="nav-hover-style"><a >Game Stalls</a></li>
					<li class="nav-hover-style"><a >Entertainers</a></li>
					<li class="nav-hover-style"><a >Rentals</a></li>
					<li class="nav-hover-style"><a >Artists</a></li>
					<li class="nav-hover-style"><a>Photo / Video</a></li>
					<li class="nav-hover-style"><a>Sound / Lights</a></li>
					<li class="nav-hover-style"><a>New Trends</a></li>
					<li class="nav-hover-style"><a>Mascots</a></li>
				</ul>
			</li>
			<li class="col-sm-3 ">
				<ul>
					<li class="dropdown-header nav-hover-style ">
						<a  class="dropdown-header nav-hover-style-link-color no-pad-l">Cakes</a>
					</li>
					<li class="nav-hover-style"><a >All Cakes</a>
					</li>
					<li class="nav-hover-style"><a>Girl Themes</a></li>
					<li class="nav-hover-style"><a>Boy Themes</a></li>
					<li class="nav-hover-style"><a>Neutral Themes</a></li>

					<li class="dropdown-header nav-hover-style pad-t-15">
						<a  class="dropdown-header nav-hover-style-link-color no-pad-l">Packages</a>
					</li>
					<li class="nav-hover-style"><a >All Packages</a>
					</li>
					<li class="nav-hover-style"><a >Basic</a></li>
					<li class="nav-hover-style"><a>Moderate</a></li>
					<li class="nav-hover-style"><a>Premium</a></li>
				</ul>
			</li>
		</ul>
	</li>
	<li class="dropdown dropdown-large">
		<a href="#" class="dropdown-toggle text-grey font-13" data-toggle="dropdown">FOR COUPLE</a>
		<ul class="dropdown-menu dropdown-menu-large row">
			<li class="col-sm-6 ">
				<ul>
					<li class="dropdown-header nav-hover-style no-pad-l"><a  class="dropdown-header nav-hover-style-link-color">Candle Light Dinner</a></li>
					<li class="dropdown-header nav-hover-style ">
						<a class="dropdown-header nav-hover-style-link-color no-pad-l">Cake Cutting Places</a>
					</li>
					<li class="dropdown-header nav-hover-style ">
						<a  class="dropdown-header nav-hover-style-link-color no-pad-l">Surprises</a>
					</li>
					<li class="nav-hover-style"><a>All Surprises</a>
					</li>
					<li class="nav-hover-style"><a >Romantic Stay</a></li>
					<li class="nav-hover-style"><a>Home decorations</a></li>
					<li class="nav-hover-style"><a>Regular surprises</a></li>
					<li class="nav-hover-style"><a >Customised surprises</a></li>
					<li class="nav-hover-style"><a>Unique </a></li>
					<li class="nav-hover-style"><a>Romantic</a></li>
					<li class="nav-hover-style"><a>Adventure</a></li>
				</ul>
			</li>
			<li class="col-sm-6 ">
				<ul>
					<li class="dropdown-header nav-hover-style">Popular Searches</li>
					<li class="nav-hover-style"><a>Unique birthday surprise</a></li>
					<li class="nav-hover-style"><a>Poolside dinner</a></li>
					<li class="nav-hover-style"><a>Romantic surprise</a></li>
				</ul>
			</li>
		</ul>
	</li>
	<li class="dropdown dropdown-large">
		<a href="#" class="dropdown-toggle text-grey font-13" data-toggle="dropdown">FOR OCCASION</a>
		<ul class="dropdown-menu dropdown-menu-large row">
			<li class="col-sm-6">
				<ul>
					<li class="dropdown-header nav-hover-style">
						<a  class="dropdown-header nav-hover-style-link-color no-pad-l">House Warming</a>
					</li>
					<li class="nav-hover-style"><a>Decorations</a></li>
					<li class="nav-hover-style"><a>Food</a></li>
					<li class="nav-hover-style"><a>Cakes</a></li>
					<li class="nav-hover-style"><a >Priests</a></li>
					<li class="nav-hover-style"><a >Tents</a></li>
				</ul>
			</li>
			<li class="col-sm-6 ">
				<ul>
					<li class="dropdown-header nav-hover-style">
						<a  class="dropdown-header nav-hover-style-link-color no-pad-l">Naming Ceremony
						</a>
					</li>
					<li class="nav-hover-style"><a>All Decorations</a></li>
					<li class="dropdown-header nav-hover-style pad-t-15">
						<a  class="dropdown-header nav-hover-style-link-color no-pad-l">Baby Shower
						</a>
					</li>
					<li class="nav-hover-style"><a>All Decorations</a></li>

				</ul>
			</li>
		</ul>
	</li>
	<li class="dropdown dropdown-large" >
		<a href="{{route('city.occasion.corporate.home',$sessionCityUrl)}}?ref=navbar" class="text-grey font-13" style="padding-bottom: 16px">FOR COMPANIES</a>
	</li>
	<li class="dropdown dropdown-large">
		<a href="#" class="dropdown-toggle text-grey font-13" data-toggle="dropdown">INVITES<span class="header-highlight-text-des">FREE</span></a>
		<ul class="dropdown-menu dropdown-menu-large row">
			<li class="col-sm-6 ">
				<ul>
					<li class=" dropdown-header  nav-hover-style"><a href="{{ route('e-cards.invites') }}?ref=navbar" class="dropdown-header nav-hover-style-link-color no-pad-l enable-link">Invites</a></li>
					<li class=" dropdown-header  nav-hover-style"><a href="{{ route('e-cards.wish-cards') }}?ref=navbar" class="dropdown-header nav-hover-style-link-color no-pad-l enable-link">Wish Cards</a></li>
					<li class=" dropdown-header  nav-hover-style"><a href="{{ route('e-cards.thank-you-cards') }}?ref=navbar" class="dropdown-header nav-hover-style-link-color no-pad-l enable-link">Thank You Cards</a></li>
				</ul>
			</li>
			<li class="col-sm-6 ">
				<ul>
					<li class=" dropdown-header nav-hover-style"><a href="{{ route('e-cards.invites') }}?ref=navbar" class="dropdown-header nav-hover-style-link-color no-pad-l enable-link">Popular Cards</a></li>
					<li class="nav-hover-style"><a href="{{ route('iv.create.party-details',"baby-boss-theme-invitation") }}?ref=navbar" class="enable-link">Baby Boss Theme Invite</a></li>
					<li class="nav-hover-style"><a href="{{ route('iv.create.party-details',"cinderella-theme-invitation") }}?ref=navbar" class="enable-link">Cinderella Theme Invite</a></li>
					<li class="nav-hover-style"><a href="{{ route('iv.create.party-details',"birthday-wish-for-dreamgirl") }}?ref=navbar" class="enable-link">Thank You Card For Baby Shower</a></li>
				</ul>
			</li>
		</ul>
	</li>
</ul>