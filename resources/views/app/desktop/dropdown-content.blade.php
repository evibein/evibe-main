@php $sessionCityUrl = is_null($sessionCityUrl) ? 'bangalore' : $sessionCityUrl; @endphp
<ul class="nav navbar-nav master-home-top-navbar-left">
	<li class="dropdown dropdown-large">
		<a href="#" class="dropdown-toggle text-grey font-13" data-toggle="dropdown">KIDS</a>
		<ul class="dropdown-menu dropdown-menu-large row">
			<li class="col-sm-5">
				<ul>
					<li class="dropdown-header nav-hover-style ">
						<a href="{{ route('city.occasion.birthdays.decors.list',['cityUrl'=>$sessionCityUrl]) }}?ref=navbar" class="dropdown-header nav-hover-style-link-color no-pad-l">Decorations</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.decors.list',['cityUrl'=>$sessionCityUrl]) }}?ref=navbar">All Decorations</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.decors.category',['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'simple-balloon-decorations']) }}?ref=navbar">Simple Balloon Decorations</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.decors.category',['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'customised-balloon-decorations']) }}?ref=navbar">Customised Balloon Decorations</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.decors.category',['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'basic-theme-decorations']) }}?ref=navbar">Basic Theme Decorations</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.decors.category',['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'medium-theme-decorations']) }}?ref=navbar">Medium Theme Decorations</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.decors.category',['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'high-end-theme-decorations']) }}?ref=navbar">High-End Theme Decorations</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.decors.category',['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'flower-decorations']) }}?ref=navbar">Flower Decorations</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.decors.category',['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'drapes-decorations']) }}?ref=navbar">Drapes Decorations</a>
					</li>
					<li class="dropdown-header nav-hover-style pad-t-15">
						<a href="{{ route('city.occasion.birthdays.food.list',$sessionCityUrl) }}?ref=navbar" class="dropdown-header nav-hover-style-link-color no-pad-l">Food</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.food.list',$sessionCityUrl) }}?ref=navbar">All </a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.food.list', $sessionCityUrl) }}?category=food-combo&ref=navbar">Food Combo</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.food.list', $sessionCityUrl) }}?category=snacks-combo&ref=navbar">Snack Combo</a>
					</li>
				</ul>
			</li>
			<li class="col-sm-4">
				<ul>
					<li class="dropdown-header nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.ent.list',$sessionCityUrl) }}?ref=navbar" class="dropdown-header nav-hover-style-link-color no-pad-l">Entertainment</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.ent.list',$sessionCityUrl) }}?ref=navbar">All Entertainment</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.ent.list.category', ['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'eateries']) }}?ref=navbar">Eateries</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.ent.list.category', ['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'game-stalls']) }}?ref=navbar">Game Stalls</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.ent.list.category', ['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'entertainers']) }}?ref=navbar">Entertainers</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.ent.list.category', ['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'artists']) }}?ref=navbar">Artists</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.ent.list.category', ['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'rentals']) }}?ref=navbar">Rentals</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.ent.list.category', ['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'photo-video']) }}?ref=navbar">Photo / Video</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.ent.list.category', ['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'sound-lights']) }}?ref=navbar">Sound / Lights</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.ent.list.category', ['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'new-trends']) }}?ref=navbar">New Trends</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.ent.list.category', ['cityUrl'=>$sessionCityUrl,'categoryUrl'=>'mascots']) }}?ref=navbar">Mascots</a>
					</li>
					<li class="dropdown-header nav-hover-style pad-t-15 hide">
						<a href="{{ route('store-redirect', ['redirect' => config('evibe.store_host').'/collections/kids-birthday-party-supplies?utm_source=CategoryHeader_Kids&utm_medium=Website&utm_campaign=StorePromotion']) }}" class="dropdown-header nav-hover-style-link-color no-pad-l">Party Props</a>
					</li>
				</ul>
			</li>
			<li class="col-sm-3 ">
				<ul>
					<li class="dropdown-header nav-hover-style ">
						<a href="{{ route('city.occasion.birthdays.cakes.list',$sessionCityUrl) }}?ref=navbar" class="dropdown-header nav-hover-style-link-color no-pad-l">Cakes</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.cakes.list',$sessionCityUrl) }}?ref=navbar">All Cakes</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.cakes.list', $sessionCityUrl) }}?category=girl-themes&ref=navbar">Girl Themes</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.cakes.list', $sessionCityUrl) }}?category=boy-themes&ref=navbar">Boy Themes</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.cakes.list', $sessionCityUrl) }}?category=neutral-themes&ref=navbar">Neutral Themes</a>
					</li>

					<li class="dropdown-header nav-hover-style pad-t-15">
						<a href="{{ route('city.occasion.birthdays.packages.list',$sessionCityUrl) }}?ref=navbar" class="dropdown-header nav-hover-style-link-color no-pad-l">Packages</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.packages.list',$sessionCityUrl) }}?ref=navbar">All Packages</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.packages.list', $sessionCityUrl) }}?category=basic-combos&ref=navbar">Basic</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.packages.list', $sessionCityUrl) }}?category=moderate-combos&ref=navbar">Moderate</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.birthdays.packages.list', $sessionCityUrl) }}?category=premium-combos&ref=navbar">Premium</a>
					</li>
					<li class="dropdown-header nav-hover-style pad-t-15">
						<a href="{{ route('city.occasion.birthdays.party-halls.list',$sessionCityUrl) }}?ref=navbar" class="dropdown-header nav-hover-style-link-color no-pad-l">Party Halls</a>
					</li>
				</ul>
			</li>
		</ul>
	</li>
	<li class="dropdown dropdown-large">
		<a href="#" class="dropdown-toggle text-grey font-13" data-toggle="dropdown">COUPLES</a>
		<ul class="dropdown-menu dropdown-menu-large row">
			<li class="col-sm-6 ">
				<ul>
					<li class="dropdown-header nav-hover-style no-pad-l">
						<a href="{{ route("city.cld.list", $sessionCityUrl) }}?ref=navbar" class="dropdown-header nav-hover-style-link-color">Candle Light Dinner</a>
					</li>
					<li class="dropdown-header nav-hover-style ">
						<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=cake-cutting-places&ref=navbar" class="dropdown-header nav-hover-style-link-color no-pad-l">Cake Cutting Places</a>
					</li>
					<li class="dropdown-header nav-hover-style ">
						<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/h/couple-surprises?ref=navbar" class="dropdown-header nav-hover-style-link-color no-pad-l">Surprises</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/h/couple-surprises?ref=navbar">All Surprises</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=romantic-stay&ref=navbar">Romantic Stay</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=home-decorations&ref=navbar">Home decorations</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=regular-surprise&ref=navbar">Regular surprises</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=customised&ref=navbar">Customised surprises</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&ref=navbar">Unique </a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=romantic&ref=navbar">Romantic</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=adventure&ref=navbar">Adventure</a>
					</li>
				</ul>
			</li>
			<li class="col-sm-6 ">
				<ul>
					<li class="dropdown-header nav-hover-style">Popular Searches</li>
					<li class="nav-hover-style">
						<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&ref=navbar">Unique birthday surprise</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=unique&ref=navbar">Poolside dinner</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ config('evibe.host') . '/' .$sessionCityUrl }}/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=romantic-stay&ref=navbar">Romantic surprise</a>
					</li>
					<li class="dropdown-header nav-hover-style hide">
						<a href="{{ route('store-redirect', ['redirect' => config('evibe.store_host').'/collections/youth-birthday-party-supplies?utm_source=CategoryHeader_Couples&utm_medium=Website&utm_campaign=StorePromotion']) }}" class="dropdown-header nav-hover-style-link-color no-pad-l">Party Props</a>
					</li>
				</ul>
			</li>
		</ul>
	</li>
	<li class="dropdown dropdown-large">
		<a href="#" class="dropdown-toggle text-grey font-13" data-toggle="dropdown">OCCASIONS</a>
		<ul class="dropdown-menu dropdown-menu-large row">
			<li class="col-sm-6">
				<ul>
					<li class="dropdown-header nav-hover-style">
						<a href="{{ route('city.occasion.pre-post.home', $sessionCityUrl) }}?ref=navbar" class="dropdown-header nav-hover-style-link-color no-pad-l">Weddings</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.pre-post.decors.list', $sessionCityUrl) }}?ref=navbar">Decorations</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.pre-post.cakes.list', $sessionCityUrl) }}?ref=navbar">Cakes</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{route('city.occasion.pre-post.ent.list', $sessionCityUrl) }}?ref=navbar">Entertainment</a>
					</li>
					<li class="dropdown-header nav-hover-style pad-t-15">
						<a href="{{ route('city.occasion.house-warming.decors.list',$sessionCityUrl) }}?ref=navbar" class="dropdown-header nav-hover-style-link-color no-pad-l">House Warming</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.house-warming.decors.list', $sessionCityUrl) }}?ref=navbar">Decorations</a>
					</li>

					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.house-warming.food.list',$sessionCityUrl) }}?ref=navbar">Food</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.house-warming.cakes.list', $sessionCityUrl) }}?ref=navbar">Cakes</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.house-warming.priest.list', $sessionCityUrl) }}?ref=navbar">Priests</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.house-warming.tent.list', $sessionCityUrl) }}?ref=navbar">Tents</a>
					</li>
				</ul>
			</li>
			<li class="col-sm-6 ">
				<ul>
					<li class="dropdown-header nav-hover-style">
						<a href="{{ route('city.occasion.sodecors.list',$sessionCityUrl) }}?ref=navbar" class="dropdown-header nav-hover-style-link-color no-pad-l">Store Openings
						</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.sodecors.list',$sessionCityUrl) }}?ref=navbar">All Decorations</a>
					</li>
					<li class="dropdown-header nav-hover-style pad-t-15">
						<a href="{{ route('city.occasion.ncdecors.list',$sessionCityUrl) }}?ref=navbar" class="dropdown-header nav-hover-style-link-color no-pad-l">Naming Ceremony
						</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.ncdecors.list',$sessionCityUrl) }}?ref=navbar">All Decorations</a>
					</li>
					<li class="dropdown-header nav-hover-style pad-t-15">
						<a href="{{route('inspirations.feed','baby-shower-decor-ideas')}}?ref=navbar" class="dropdown-header nav-hover-style-link-color no-pad-l">Baby Shower
						</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('city.occasion.bsdecors.list',$sessionCityUrl) }}?ref=navbar">All Decorations</a>
					</li>

				</ul>
			</li>
		</ul>
	</li>
	<li class="dropdown dropdown-large">
		<a href="{{route('city.occasion.corporate.home',$sessionCityUrl)}}?ref=navbar" class="text-grey font-13" style="padding-bottom: 16px;">COMPANIES</a>
	</li>
	<li class="dropdown dropdown-large">
		<a href="#" class="dropdown-toggle text-grey font-13" data-toggle="dropdown">INVITES<span class="header-highlight-text-des hide">FREE</span></a>
		<ul class="dropdown-menu dropdown-menu-large row">
			<li class="col-sm-6 ">
				<ul>
					<li class=" dropdown-header  nav-hover-style">
						<a href="{{ route('e-cards.invites') }}?ref=navbar" class="dropdown-header nav-hover-style-link-color no-pad-l">Invites</a>
					</li>
					<li class=" dropdown-header  nav-hover-style">
						<a href="{{ route('e-cards.wish-cards') }}?ref=navbar" class="dropdown-header nav-hover-style-link-color no-pad-l">Wish Cards</a>
					</li>
					<li class=" dropdown-header  nav-hover-style">
						<a href="{{ route('e-cards.thank-you-cards') }}?ref=navbar" class="dropdown-header nav-hover-style-link-color no-pad-l">Thank You Cards</a>
					</li>
				</ul>
			</li>
			<li class="col-sm-6 ">
				<ul>
					<li class=" dropdown-header nav-hover-style">
						<a href="{{ route('e-cards.invites') }}?ref=navbar" class="dropdown-header nav-hover-style-link-color no-pad-l">Popular Cards</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('iv.create.party-details',"baby-boss-theme-invitation") }}?ref=navbar">Baby Boss Theme Invite</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('iv.create.party-details',"cinderella-theme-invitation") }}?ref=navbar">Cinderella Theme Invite</a>
					</li>
					<li class="nav-hover-style">
						<a href="{{ route('iv.create.party-details',"birthday-wish-for-dreamgirl") }}?ref=navbar">Thank You Card For Baby Shower</a>
					</li>
				</ul>
			</li>
		</ul>
	</li>
	<li class="dropdown dropdown-large hide">
		<a href="{{ route('store-redirect', ['redirect' => config('evibe.store_host').'?utm_source=CategoryHeader&utm_medium=Website&utm_campaign=StorePromotion']) }}" class="text-grey font-13" style="padding-bottom: 16px;">PARTY PROPS
			<span class="header-highlight-text-des">New</span></a>
	</li>
</ul>