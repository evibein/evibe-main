@php $sessionCityUrl = getCityUrl() @endphp
<div class="pad-r-20 nav-shadow">
	<div class="col-xs-12 col-sm-1 col-md-1 col-lg-1 no-pad">
		<a class="navbar-brand topnav" href="/" style="padding-top:7px">
			<img src="{{ $galleryUrl }}/img/logo/logo_evibe.png" alt="Evibe logo">
		</a>
	</div>
	@if(!(isset($data["passiveCity"]) && $data["passiveCity"]))
		<div class="col-xs-5 col-sm-7 col-md-8 col-lg-7 nav-links-wrap" style="padding-top: 1px;">
			@include('app.desktop.dropdown-content')
		</div>
		<div class="col-xs-4 col-sm-4 col-md-3 col-lg-4 no-pad-r">
			<div style="display: flex; align-items: stretch;">
				<div style="flex-grow: 4;">
					@if(session()->has('cityId'))
						<div class="pos-rel desk mar-t-13 hide"> <!-- see: desktop search bar -->
							<div class="desktop-header-search-bar has-feedback full-width search-box" data-referral="desktop">
								<input id="desktopGlobalSearchInput" type="text" name="q" data-limit="8" class="header-search-bar-input full-width typeahead" placeholder="Search decorations, cakes & more" value="@if((isset($data) && isset($data['query']) && !empty($data['query']))){{ $data['query'] }}@elseif(request("query") && !empty(request("query"))){{ request("query") }}@endif">
								<a href="#" class="header-search-icon-container">
									<i class="glyphicon glyphicon-search header-search-icon"></i>
								</a>
								<img class="typeahead-spinner" src="{{ $galleryUrl }}/main/img/search/loading-search.gif">
							</div>
						</div>
					@endif
				</div>
				<div class="no-pad-l no-pad-r" style="flex-shrink: 0; flex-grow: 1">
					<div class="top-header-list in-blk pull-right">
						<div class="mar-r-20 in-blk text-center">
							<a href="{{route("partybag.list")}}" class="a-no-decoration-black">
								<img class="nav-icon" src="{{ $galleryUrl }}/img/icons/shopping-bag.png">
								<p class="font-12 no-mar-b">Cart</p>
							</a>
						</div>
						@php
							$name = getAuthUserName();
							if(strpos($name,' '))
							{
								$names = explode(" ", $name);
								$name= $names[0];
									if(isset($names[1][0]))
									{
										$name = $name . " " . $names[1][0];
									}
							}
							else
							{
								$name = strlen($name) <= 20 ? $name : substr($name, 0, 20) . "...";
							}
						@endphp
						@if($name=='')
							<div class="user-login-li-wrap in-blk text-center">
								<a href="#" class="a-no-decoration-black" v-on:click.stop.prevent="userLogin">
									<img class="nav-icon" src="{{ $galleryUrl }}/img/icons/user-black.png">
									<p class="font-12 no-mar-b">User</p>
								</a>
							</div>
						@else
							<div class="user-login-li-wrap dropdown in-blk text-center">
								<a href="#" class="a-no-decoration-black" data-toggle="dropdown">
									<img class="nav-icon" src="{{ $galleryUrl }}/img/icons/user-black.png"><br>
									<span class="font-12">{{$name}}</span>
								</a>
								<ul class="dropdown-menu dropdown-right-mini text-center" style="position: absolute;right: 0;left: inherit;">
									<li><a href="{{route('track.orders')}}" class="no-mar-t no-mar-b bold-on-hover">Track My Order</a></li>
									<li>
										<a href="{{route('user.browsing.history.list.new')}}" class="no-mar-t no-mar-b bold-on-hover">
											My Browsing History
										</a>
									</li>
									<li class="full-width"><a href="{{ route("wallet.home") }}?ref=des-nav" class="no-mar-t no-mar-b bold-on-hover">My Wallet</a></li>
									<li class="full-width"><a href="{{ route("wishlist.list") }}" class="no-mar-t no-mar-b bold-on-hover">Wishlist</a></li>
									<li><a href="/user/logout" class="user-login user-login-btn no-mar-t no-mar-b bold-on-hover a-no-decoration-black">Sign Out</a></li>
								</ul>
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="hide" id="username">{{ getAuthUserName() }}</div>
	@else
		<div class="col-xs-5 col-sm-7 nav-links-wrap" style="padding-top: 1px;">
			@include('app.desktop.dropdown-content-passive-city')
		</div>
	@endif
</div>