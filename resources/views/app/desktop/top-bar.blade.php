@php $sessionCityUrl = getCityUrl() @endphp
@php $currentUrlPath = request()->path(); @endphp	
@php $showHeading = 1 @endphp
@if(is_int(strpos($currentUrlPath, "virtual-birthday-party")))
	@php $showHeading = 0 @endphp
@endif

<div class="navbar-collapse">
	<!-- Commented campaign text in header -->
	@if(isset($data["passiveCity"]) && $data["passiveCity"])
		<ul class="master-home-top-navbar-left" style="padding-left: 30% !important">
			<li>
				<a class="campaign-text text-center enable-link" style="color:red !important">
					CURRENTLY WE ARE NOT SERVING IN THIS CITY. <span class="btn-post" style="text-decoration: underline;"> CONTACT US</span> FOR MORE INFO
				</a>
			</li>
		</ul>
	@else
		<div class="mar-t-3 pad-l-5 in-blk pull-left">
			@if(isset($showHeading) && $showHeading == 1)
				<a href="{{ route('virtual-birthday-party.home') }}?ref=top-bar" target="_blank" class="a-no-decoration-black" >Introducing Virtual Celebrations
					{{-- <span style="padding-left:5px"><img src="https://image.flaticon.com/icons/svg/2917/2917106.svg" style="height:12px"></span> --}}
					<span style="background:red;color:white;padding:5px;border-radius: 10px;padding-right: 7px;padding-left: 7px;padding-top: 1px;padding-bottom: 1px;margin-left: 5px;font-size:14px">New</span></a>
			@else
					<a href="https://angel.co/company/evibe/jobs" target="_blank" class="a-no-decoration-black">We are Hiring!</a>
			@endif
		
			
		</div>
	@endif
	<ul class="master-home-top-navbar-right">
		<li>
			<a href="javascript:void(0)" class="select-city enable-link">
				<i class="glyphicon glyphicon-map-marker no-mar-r"></i>
				@if(isset($data["passiveCity"]) && $data["passiveCity"])
					{{ $data["passiveCityUrl"] }}
				@else
					{{ ucfirst(getCityName()) }}
				@endif
			</a>
		</li>
		@if (!request()->is('*/engagement-wedding-reception*') && !request()->is('*/bachelor-party*') && !request()->is('*/corporate-events*'))
			<li>
				<a href="javascript:void(0)" class="no-click evibe-cheppandi nav-topbar-phone enable-link">
					<i class="glyphicon glyphicon-phone no-pad no-mar"></i>
					{{ config('evibe.contact.company.plain_phone') }}
				</a>
			</li>
		@endif
		<li>
			<a href="https://evibe.in/blog" target="_blank" rel="noopener" class="enable-link">
				<span> Blog</span>
			</a>
		</li>
		@if(!(isset($data["passiveCity"]) && $data["passiveCity"]))
			<li>
				<a href="{{ route("track.orders") }}" rel="noopener">
					<i class="glyphicon glyphicon-screenshot no-mar-r"></i> Track My Order
				</a>
			</li>
		@endif
	</ul>
</div>