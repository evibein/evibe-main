<header>
	<div class="header-top">
		<nav class="navbar navbar-default navbar-home navbar-top" role="navigation">

			<!-- desktop -->
			<div class="hide__400 hide-400-600">
				<nav class="master-home-top-navbar">
					<div class="navbar-collapse">
						<ul class="master-home-top-navbar-left">
							<li>
								<a href="{{ route('city.occasion.birthdays.home', [getCityUrl(), "ref" => "header"]) }}" class="@if(request()->is('*'.config('evibe.occasion.kids_birthdays.url').'*')){{ "active" }}@endif">Birthdays</a>
							</li>
							<li>
								<a href="{{ route('city.cld.list', getCityUrl()) }}">Candle Light Dinners</a>
							</li>
							<li>
								<a href="{{ route('city.occasion.pre-post.home', [getCityUrl(), "ref" => "header"]) }}" class="@if(request()->is('*'.config('evibe.occasion.pre-post.url').'*')){{ "active" }}@endif">Weddings</a>
							</li>
							<li>
								<a href="{{ route('city.occasion.bachelor.home', [getCityUrl(), "ref" => "header"]) }}" class="@if(request()->is('*'.config('evibe.occasion.bachelor.url').'*')){{ "active" }}@endif">Youth Party</a>
							</li>
							<li>
								<a href="{{ route('city.occasion.surprises.home', [getCityUrl(), "ref" => "header"]) }}" class="@if(request()->is('*'.config('evibe.occasion.surprises.url').'*')){{ "active" }}@endif">Surprises</a>
							</li>
							<li>
								<a href="{{ route('city.occasion.house-warming.home', [getCityUrl(), "ref" => "header"]) }}" class="@if(request()->is('*'.config('evibe.occasion.house-warming.url').'*')){{ "active" }}@endif">House Warming</a>
							</li>
						</ul>
						<ul class="master-home-top-navbar-right">
							<li>
								<a href="javascript:void(0)" class="select-city">
									<i class="glyphicon glyphicon-map-marker no-mar-r"></i>
									{{ ucfirst(getCityName()) }}
								</a>
							</li>
							<li>
								<a href="javascript:void(0)" class="no-click evibe-cheppandi nav-topbar-phone">
									<i class="glyphicon glyphicon-phone no-pad no-mar"></i>
									{{ config('evibe.contact.company.plain_phone') }}
								</a>
							</li>
							<li>
								<a href="https://evibe.in/blog" target="_blank" rel="noopener">
									<span> Blog</span>
								</a>
							</li>
							@include('app.login_user')
						</ul>
					</div>
				</nav>
				<nav class="navbar-default navbar-home navbar-home-white no-mar-b" role="navigation">
					<div class="in-blk">
						<a class="navbar-brand topnav" href="/">
							<img src="{{ $galleryUrl }}/img/logo/logo_evibe.png" alt="Evibe logo">
						</a>
					</div>
				</nav>
				<div class="clearfix"></div>
			</div>
			<!-- for media less than 600px -->
			<div class="hide show-400-600 unhide__400">
				<!-- city and phone number -->
				<div class="text-center">
					<div class="pad-t-5 font-16 text-col-grey navbar-links top-strip">
						<div class="in-blk mar-r-15">
							<span class="glyphicon glyphicon-earphone"></span>{{ config('evibe.contact.company.plain_phone') }}
						</div>
						<div class="in-blk">
							@if(Request::url() != route('partybag.list'))
								@include('app.partybag_header_top')
							@endif
						</div>
					</div>
				</div>

				<!-- Logo and Search bar-->
				<div class="container">
					<div class="col-xs-6 col-sm-6">
						<a class="navbar-brand no-pad-t" href="/">
							<img src="https://gallery.evibe.in/img/logo/logo_evibe.png" alt="Evibe.in logo">
						</a>
					</div>
					<div class="col-xs-3 col-sm-3 col-sm-offset-3 col-xs-offset-3">
						@include('app.login_user')
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</nav>
	</div>
</header>