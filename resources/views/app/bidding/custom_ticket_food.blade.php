@if (!request()->is('*/engagement-wedding-reception*') && !request()->is('*/bachelor-party*'))
	<div class="reverse-bidding-wrap">
		<div class="title text-e">
			@if(isset($title)) {{ $title }} @else Have your own menu?@endif
		</div>
		<div class="content">
			@if(isset($content))
				{{ $content }}
			@else
				You can now upload your customised menu. Based on the availability, we will get the best quote from our verified partners.
			@endif
		</div>
		<button class="btn btn-default btn-bidding">
			<i class="material-icons valign-mid">&#xE2C6;</i>
			<span class="valign-mid">Submit My Menu</span>
		</button>
	</div>

	@include("app.bidding.modals.food")

	@include("app.bidding.bidding_common")
@endif