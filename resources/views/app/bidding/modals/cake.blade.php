<div class="modal fade modal-custom-form modal-reverse-bidding modal-custom-cake" tabindex="-1" role="dialog" id="modalBiddingDetails">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="modal-inner-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title text-center">Share your customised designs</h4>
				</div>
				<form id="formReverseBidding" action="{{ $url }}" class="form-reverse-bidding form-custom-cake">
					<div class="sticky-error-wrap mar-b-20 hide">
						<span class="error-message">Please fill all the required details.</span>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-lg-6 col-sm-6">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="name" id="name"/>
								<label class="mdl-textfield__label" for="name">Name</label>
							</div>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-6">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="email" id="email"/>
								<label class="mdl-textfield__label" for="email">Email</label>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-lg-6 col-sm-6">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="phone" id="phone"/>
								<label class="mdl-textfield__label" for="phone">Phone Number</label>
							</div>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-6">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="partyDate" id="partyDate" autocomplete="off"/>
								<label class="mdl-textfield__label" for="partyDate">Party Date</label>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-lg-6 col-sm-6">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input google-auto-complete" type="text" name="location" id="location" placeholder=""/>
								<label class="mdl-textfield__label" for="location">Party Location</label>
								<input type="hidden" class="google-location-details" name="locationDetails" value="">
							</div>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-6">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="budget" id="budget"/>
								<label class="mdl-textfield__label" for="budget">Your Budget</label>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-lg-6 col-sm-6">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<textarea class="mdl-textfield__input" name="comment" id="comment"></textarea>
								<label class="mdl-textfield__label" for="comment">Your Requirement</label>
							</div>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-6">
							<div class="image-upload-wrap pad-10">
								<div>Upload Designs (max: 5 MB)</div>
								<div class="file">
									<input type="file" name="images[]" multiple="multiple"/>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<input type="hidden" id="ticketId" name="ticketId" value="@if(isset($data['ticket']->id)) {{ $data['ticket']->id }} @else {{ 0 }} @endif">
					<!-- for recommendation purposes -->
					<div class="form-group">
						<div class="col-sm-12 text-center">
							<button id="btnCustomTicketSubmit" type="submit" class="btn btn-primary btn-submit btn-custom-ticket-submit">
								<div class="default">
									<i class="material-icons valign-mid">&#xE2C6;</i>
									<span class="valign-mid">Submit</span>
								</div>
								<div class="waiting hide">
									<i class="material-icons valign-mid">&#xE2C6;</i>
									<span class="valign-mid">Submitting..</span>
								</div>
							</button>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="sticky-error-wrap mar-b-20 hide">
						<span class="error-message">Please fill all the required details.</span>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>