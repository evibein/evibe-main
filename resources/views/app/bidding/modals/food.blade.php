<div class="modal fade modal-custom-form modal-reverse-bidding" tabindex="-1" role="dialog" id="modalBiddingDetails">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="modal-inner-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title text-center">Share your customised menu</h4>
				</div>
				<div class="modal-suggest-text mar-t-10 text-center">
					<div class="alert alert-info">
						<i class="glyphicon glyphicon-info-sign"></i>You can either type your menu or upload your menu.
					</div>
				</div>
				<form id="formReverseBidding" action="{{ $url }}" class="form-reverse-bidding">
					<div class="sticky-error-wrap mar-b-20 hide">
						<span class="error-message">Please fill all the required details.</span>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-lg-6 col-sm-6">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="name" id="name"/>
								<label class="mdl-textfield__label" for="name">Name</label>
							</div>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-6">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="email" id="email"/>
								<label class="mdl-textfield__label" for="email">Email</label>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-lg-6 col-sm-6">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="phone" id="phone"/>
								<label class="mdl-textfield__label" for="phone">Phone Number</label>
							</div>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-6">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="partyDate" id="partyDate" autocomplete="off"/>
								<label class="mdl-textfield__label" for="partyDate">Party Date</label>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-lg-6 col-sm-6">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input google-auto-complete" type="text" name="location" id="location" placeholder=""/>
								<label class="mdl-textfield__label" for="location">Party Location</label>
								<input type="hidden" class="google-location-details" name="locationDetails" value="">
							</div>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-6">
							<label for="" class="custom-mdl-label">Menu Type</label>
							<div>
								<label class="no-mar mar-r-10">
									<input type="radio" name="foodType" id="foodType" value="1"/>
									<span class="radio-btn-text">Veg</span>
								</label>
								<label class="no-mar">
									<input type="radio" name="foodType" id="foodType" value="2"/>
									<span class="radio-btn-text">Non-Veg</span>
								</label>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-lg-6 col-sm-6">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="budgetPerPerson" id="budgetPerPerson"/>
								<label class="mdl-textfield__label" for="budgetPerPerson">Budget per person</label>
							</div>
						</div>

						<div class="col-md-6 col-lg-6 col-sm-6">
							<label for="" class="custom-mdl-label">Food Service Type</label>
							<div>
								<label class="no-mar mar-r-10">
									<input type="radio" name="foodServiceType" id="foodServiceType" value="Packaged"/>
									<span class="radio-btn-text">Packaged</span>
								</label>
								<label class="no-mar">
									<input type="radio" name="foodServiceType" id="foodServiceType" value="Service"/>
									<span class="radio-btn-text">Service</span>
								</label>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-lg-6 col-sm-6">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<textarea class="mdl-textfield__input" name="menuText" id="menuText"></textarea>
								<label class="mdl-textfield__label" for="menuText">Your Menu</label>
							</div>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-6">
							<div class="image-upload-wrap pad-10">
								<div>Upload Menu (max: 2 MB)</div>
								<div class="file">
									<input type="file" name="menu"/>
								</div>
								<div>(Formats: txt, doc, pdf, jpg, png)</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-lg-6 col-sm-6">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="venueMinGuests" id="venueMinGuests"/>
								<label class="mdl-textfield__label" for="venueMinGuests">Guest Count</label>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<div class="col-sm-12 text-center">
							<button id="btnCustomTicketSubmit" type="submit" class="btn btn-primary btn-submit">
								<div class="default">
									<i class="material-icons valign-mid">&#xE2C6;</i>
									<span class="valign-mid">Submit</span>
								</div>
								<div class="waiting hide">
									<i class="material-icons valign-mid">&#xE2C6;</i>
									<span class="valign-mid">Submitting..</span>
								</div>
							</button>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="sticky-error-wrap mar-b-20 hide">
						<span class="error-message">Please fill all the required details.</span>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>