@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			$('.btn-bidding').click(function (event) {
				event.preventDefault();

				/* clear the form data */
				document.getElementById("formReverseBidding").reset();

				/* open the modal */
				var biddingModal = $('#modalBiddingDetails');
				biddingModal.find('.sticky-error-wrap').addClass('hide');
				biddingModal.modal('show');
			});

			var btnSubmit = $('#btnCustomTicketSubmit');

			/* submit the form */
			$('#formReverseBidding').submit(function (e) {
				e.preventDefault();

				function showSubmitButton() {
					btnSubmit.attr('disabled', false);
					btnSubmit.find('.default').removeClass('hide');
					btnSubmit.find('.waiting').addClass('hide');
				}

				function hideSubmitButton() {
					btnSubmit.attr('disabled', true);
					btnSubmit.find('.default').addClass('hide');
					btnSubmit.find('.waiting').removeClass('hide');
				}

				var formUrl = $(this).attr('action');
				hideSubmitButton();

				$.ajax({
					url: formUrl,
					type: "POST",
					data: new FormData(this),
					cache: false,
					processData: false,
					contentType: false,
					success: function (data) {
						showSubmitButton();
						if (data.success) {
							/* redirect to thank you page */
							if (data.redirectTo) {
								window.location = data.redirectTo;
							} else {
								window.showNotySuccess("Thank you! We've received your request. We will get in touch with you shortly.");
							}
						}
						else if (data.error) {
							var errorObj = $('#modalBiddingDetails').find('.sticky-error-wrap').removeClass('hide').find('.error-message');
							errorObj.text(data.error);
						}
						else {
							$('#modalBiddingDetails').modal("hide");
							window.showNotyError("Some error occurred while submitting your request");
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						showSubmitButton();
						window.showNotyError("Some error occurred while submitting your request");
						window.notifyTeam({
							"url": url,
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			});
		});
	</script>
@endsection