<div class="generic-page-loader-wrap hide">
	<div class="loader-animation-wrap">
		<img src="/images/loading.gif" alt="">
	</div>
</div>

@section('custom-css')
	@parent
	<style type="text/css" rel="stylesheet" media="all">
		/* ab-pre-checkout-loader */
		.generic-page-loader-wrap {
			position: fixed;
			z-index: 1050;
			height: 100%;
			width: 100%
		}

		.loader-animation-wrap {
			position: absolute;
			display: inline-block;
			background-color: #FBFBFB;
			padding: 6px;
			border-radius: 100%;
			height: 45px;
			width: 45px;
			top: 0;
			bottom: 0;
			left: 0;
			right: 0;
			margin: auto;
		}
	</style>
@endsection