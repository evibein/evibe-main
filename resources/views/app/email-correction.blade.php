@extends('layout.base')

@section('page-title')
	<title>My details | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="Submit your email id"/>
@endsection

@section('meta-keywords')
	<meta name="keywords" content="Evibe.in"/>
@endsection

@section('og-title')
	<meta property="og:title" content="My details | Evibe.in"/>
@endsection

@section('og-description')
	<meta property="og:description" content="Submit your email id"/>
@endsection

@section('og-url')
	<meta property="og:url" content="https://evibe.in"/>
@endsection

@section('press:title')
@endsection

@section("header")
	<header>
		<div class="header-top">
			<nav class="navbar navbar-default navbar-home navbar-top" role="navigation">
				<!-- desktop -->
				<div class="hide__400 hide-400-600">
					<nav class="master-home-top-navbar">
						<div class="navbar-collapse">
							<ul class="master-home-top-navbar-left">
								<li>
									<a href="{{ route('city.occasion.birthdays.home', [getCityUrl(), "ref" => "header"]) }}" class="@if(request()->is('*'.config('evibe.occasion.kids_birthdays.url').'*')){{ "active" }}@endif">Birthdays</a>
								</li>
								<li>
									<a href="{{ route('city.cld.list', getCityUrl()) }}">Candle Light Dinners</a>
								</li>
								<li class="hide">
									<a href="{{ route('city.occasion.pre-post.home', [getCityUrl(), "ref" => "header"]) }}" class="@if(request()->is('*'.config('evibe.occasion.pre-post.url').'*')){{ "active" }}@endif">Weddings</a>
								</li>
								<li>
									<a href="{{ route('city.occasion.bachelor.home', [getCityUrl(), "ref" => "header"]) }}" class="@if(request()->is('*'.config('evibe.occasion.bachelor.url').'*')){{ "active" }}@endif">Youth Party</a>
								</li>
								<li>
									<a href="{{ route('city.occasion.surprises.home', [getCityUrl(), "ref" => "header"]) }}" class="@if(request()->is('*'.config('evibe.occasion.surprises.url').'*')){{ "active" }}@endif">Surprises</a>
								</li>
								<li>
									<a href="{{ route('city.occasion.house-warming.home', [getCityUrl(), "ref" => "header"]) }}" class="@if(request()->is('*'.config('evibe.occasion.house-warming.url').'*')){{ "active" }}@endif">House Warming</a>
								</li>
								<li class="hide">
									<a href="{{ route('city.occasion.corporate.home', [getCityUrl(), "ref" => "header"]) }}" class="@if(request()->is('*corporate*')){{ "active" }}@endif">Corporate Events</a>
								</li>
							</ul>
							<ul class="master-home-top-navbar-right">
								<li>
									<a href="javascript:void(0)" class="select-city">
										<i class="glyphicon glyphicon-map-marker no-mar-r"></i>
										{{ ucfirst(getCityName()) }}
									</a>
								</li>
								<li>
									<a href="javascript:void(0)" class="no-click evibe-cheppandi nav-topbar-phone">
										<i class="glyphicon glyphicon-phone no-pad no-mar"></i>
										{{ config('evibe.contact.company.plain_phone') }}
									</a>
								</li>
								<li>
									<a href="https://evibe.in/blog" target="_blank" rel="noopener">
										<span> Blog</span>
									</a>
								</li>
								@include('app.login_user')
							</ul>
						</div>
					</nav>
					<nav class="navbar-default navbar-home navbar-home-white no-mar-b" role="navigation">
						<div class="in-blk">
							<a class="navbar-brand topnav" href="/">
								<img src="{{ $galleryUrl }}/img/logo/logo_evibe.png" alt="Evibe logo">
							</a>
						</div>
						<div class="in-blk pull-right" id="navbarNav">
							<ul class="nav navbar-nav top-header-list" v-cloak>
								@if(request()->is('*corporate-events*'))
									<li class="pad-l-10">
										<a href="#" class="corporate-enq-btn">
											<span class="glyphicon glyphicon-envelope"></span>
											<span>Enquire Now</span>
										</a>
									</li>
								@else
									<li class="pad-l-10">
										<a href="#" class="btn-post">
											<span class="glyphicon glyphicon-envelope"></span>
											<span>Enquire Now</span>
										</a>
									</li>
									<li>
										<a href="{{ route('partybag.list') }}" class="party-bag-header-navbar">
											<i class="icon-bagicon5"></i>
											Party Bag
											<span class="partybag-badge master-home-pbCount">@{{ pbCount }}</span>
										</a>
									</li>
								@endif
							</ul>
						</div>
					</nav>
					<div class="clearfix"></div>
				</div>

				<!-- for media less than 600px -->
				<div class="hide show-400-600 unhide__400">
					<!-- city and phone number -->
					<div class="text-center pad-b-6 hide">
						<div class="pad-t-5 pad-l-15 font-12 text-col-wh navbar-links top-strip">
							<div class="in-blk font-12 mar-r-15 top-header-static-page-evibe-cheppandi">
								<span class="glyphicon glyphicon-earphone"></span>{{ config('evibe.contact.company.plain_phone') }}
							</div>
							<div class="in-blk">
								@if(request()->url() != route('partybag.list'))
									@include('app.partybag_header_top')
								@endif
							</div>
						</div>
					</div>

					<!-- Logo and Search bar-->
					<div class="container no-pad mar-t-20">
						<div class="col-xs-12 col-sm-12 text-center">
							<a class="no-pad-t" href="/">
								<img src="{{ $galleryUrl }}/img/logo/logo_evibe.png" alt="Evibe.in logo">
							</a>
						</div>
						<div class="col-xs-2 col-sm-2 hide">
							@include('app.login_user')
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</nav>
		</div>
	</header>
@endsection

@section("content")
	<div class="container mar-t-30">
		<div class="col-md-6 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-2">
			<div class="email-correction-title mar-t-30 mar-l-2">
				@if(isset($data['invalidAttempt']) && $data['invalidAttempt'])
				@elseif(isset($data['showThankYou']) && $data['showThankYou'])
				@else
					Update your Email Id
				@endif
			</div>
			<div class="email-correction-wrap mar-t-10 mar-b-30">
				<div class="email-correction-content">
					@if(isset($data['invalidAttempt']) && $data['invalidAttempt'])
						<i class="glyphicon glyphicon-remove-sign text-r"></i> Sorry, you are not authorised to use this page.
						<span class="mar-l-2">However, if you think this is a mistake, reach us at {{ config('evibe.contact.customer.group') }}</span>
					@elseif(isset($data['showThankYou']) && $data['showThankYou'])
						<span class="font-18">
							<i class="glyphicon glyphicon-ok-sign text-g font-24 valign-bottom"></i> Your email id has been updated
						</span>
					@else
						Hi {{ $data['customerName'] }}, the email id given (
						<span class="text-r">{{ $data['invalidEmail'] }}</span>) is invalid.
						<span class="mar-l-2">Kindly submit the correct email address to receive emails.</span>
						<div class="mar-t-15">
							<div class="col-md-8 col-sm-8 col-xs-8 input-field no-pad-r">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input class="mdl-textfield__input" type="text" name="correctedEmail" id="correctedEmail"/>
									<label class="mdl-textfield__label" for="correctedEmail">
										Enter correct email</label>
								</div>
							</div>
							<input type="hidden" id="hidSuppressionId" value="{{ $data['suppressionId'] }}">
							<input type="hidden" id="hidInvalidEmail" value="{{ $data['invalidEmail'] }}">
							<div class="col-md-4 col-sm-4 col-xs-4 pad-t-15 no-pad-r pull-right">
								<button type="submit" class="btn btn-primary" id="btnCorrectionEmail" data-url="{{ route('validate-email.save') }}">Submit
								</button>
							</div>
							<div class="clearfix"></div>
						</div>
					@endif
				</div>
				<div class="email-correction-loader"></div>
			</div>
		</div>
	</div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			$('.partybag-wrap-mobile').addClass('hide');

			$('#btnCorrectionEmail').click(function (event) {
				event.preventDefault();

				$.ajax({
					url: $(this).data('url'),
					type: "POST",
					data: {
						correctedEmail: $('#correctedEmail').val(),
						suppressionId: $('#hidSuppressionId').val()
					},
					success: function (data) {
						if (data.success) {
							window.location.reload();
						}
						else {
							var $error = 'Some error occurred while submitting data';

							if (data.error) {
								$error = data.error;
							}
							window.showNotyError($error);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						showSubmitButton();
						window.showNotyError("Some error occurred while submitting your request.");
						window.notifyTeam({
							"url": url,
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			});

		});
	</script>
@endsection