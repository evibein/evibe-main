@extends('app.google-reviews.base')

<style type="text/css" rel="stylesheet" media="all">

	.google-review-carousel .carousel-inner > .item {
		-webkit-transition: 0.6s ease-in-out left;
		-moz-transition: 0.6s ease-in-out left;
		-o-transition: 0.6s ease-in-out left;
		transition: 0.6s ease-in-out left;
	}

</style>

<script type="text/javascript">

	$('.google-review-carousel .item').each(function () {
		var next = $(this).next();
		if (!next.length) {
			next = $(this).siblings(':first');
		}
		next.children(':first-child').clone().appendTo($(this));

		if (next.next().length > 0) {
			next.next().children(':first-child').clone().appendTo($(this));
		}
		else {
			$(this).siblings(':first').children(':first-child').clone().appendTo($(this));
		}
	});

</script>