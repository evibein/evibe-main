@if(count($reviews))
	<div class="google-reviews-wrap">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="google-reviews-title text-center">
				<h4>
					Customer Stories
				</h4>
			</div>
			<div class="google-reviews-cards">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="carousel slide google-review-carousel" id="googleReviewsCarousel">
						<div class="carousel-inner">
							@php $i = 1; @endphp
							@foreach($reviews as $review)
								<div class="item @if($i == 1) active @endif">
									<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
										<div class="google-review-wrap text-center">
											<div class="google-review-img-wrap">
												@if(isset($review['profile_photo_url']) && $review['profile_photo_url'])
													<img class="google-review-img" src="{{ $review['profile_photo_url'] }}">
												@else
													<img class="google-review-img" src="{{ $galleryUrl }}/main/img/icons/google_user.png">
												@endif
											</div>
											<div class="google-review-name mar-t-5">
												@if(isset($review['author_name']) && $review['author_name'])
													{{ $review['author_name'] }}
												@else
													A Google Customer
												@endif
											</div>
											<div class="google-review-rating mar-t-5">
												@php $gRating = (isset($review['rating']) && $review['rating']) ? $review['rating'] : 5; @endphp
												<input type="number" class="google-avg-rating hide" value="{{ $gRating }}" title="@if(isset($review['author_name']) && $review['author_name']) {{ $review['author_name'] }} @else A Google Customer @endif has given rating of {{ $gRating }}"/>
											</div>
											<div class="google-review-text mar-t-10">
												@if(isset($review['text']) && $review['text'])
													{{ $review['text'] }}
												@else
												@endif
											</div>
										</div>
									</div>
								</div>
								@php $i++; @endphp
							@endforeach
						</div>
						<a class="left carousel-control google-carousel-control" href="#googleReviewsCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
						<a class="right carousel-control google-carousel-control" href="#googleReviewsCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="google-reviews-cta text-left mar-t-20">
				<a id="googleReviews" href="{{ config('evibe.google.reviews.link') }}" target="_blank">
					<div class="google-card mar-l-30">
						<div class="google-reviews-content">
							<div class="google-logo-img-wrap in-blk">
								<img class="google-logo-img" src="{{ $galleryUrl }}/main/img/icons/g_rate.png">
							</div>
							<div class="in-blk valign-mid mar-r-5">
								<div class="">
							<span class="google-star-rating in-blk">
								<input type="number" class="google-avg-rating hide" value="{{ config('evibe.google.reviews.rating') }}" title="Evibe.in has {{ config('evibe.google.reviews.rating') }} rating from {{ config('evibe.google.reviews.count') }} reviews"/>
							</span>
									<span class="google-rating valign-mid mar-l-5 in-blk">({{ config('evibe.google.reviews.rating') }})</span>
								</div>
								<div class="google-reviews-count">
									{{ config('evibe.google.reviews.count') }} reviews
								</div>
							</div>
						</div>
						<div class="google-reviews-text mar-t-5">
							View All Customer Reviews
						</div>
					</div>
				</a>
				<div class="customer-video-card">

				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
@endif

<link rel="stylesheet" href="{{ elixir('css/util/star-rating.css') }}"/>

<style type="text/css" rel="stylesheet" media="all">

	.google-reviews-wrap {
		background-color: #FFF;
		padding: 30px 0;
	}

	.google-review-wrap {
		border: 1px solid #efefef;
		padding: 30px;
		border-radius: 10px;
	}

	.google-review-img-wrap {
		height: 50px;
		width: 50px;
		-webkit-border-radius: 100%;
		-moz-border-radius: 100%;
		border-radius: 100%;
		margin: auto;
	}

	.google-review-img {
		height: 100%;
		width: 100%;
	}

	.google-review-name {
		font-size: 14px;
		font-weight: bold;
		text-transform: capitalize;
	}

	.google-carousel-control {
		width: 0% !important;
		color: #EFEFEF;
	}

	.carousel-control.left, .carousel-control.right {
		background-image: none;
	}

	.google-card {
		border: 1px solid #efefef;
		display: inline-block;
		padding: 5px 10px 5px 5px;
		border-radius: 10px;
		color: #696969;
	}

	.google-card:hover,
	.google-card:active,
	.google-card:focus {
		color: #696969;
		text-decoration: none;
	}

	.google-logo-img-wrap {
		height: 50px;
	}

	.google-logo-img {
		height: 100%;
	}

	.google-rating {
		font-size: 12px;
	}

	.google-star-rating .rating-container .rating-stars {
		color: #FFCE00;
	}

	.google-reviews-count {
		font-size: 12px;
	}

	.google-card .google-reviews-content {
		border-bottom: 1px solid #EFEFEF;
	}

	.google-card .google-reviews-text {
		font-size: 12px;
		text-align: center;
		color: #448aff;
	}

</style>

<script type="text/javascript">


	$.getScript("/js/util/star-rating.js", function () {
		function applyRatingStar(to) {
			$(to).rating({
				showClear: 0,
				readonly: 'true',
				showCaption: 0,
				size: 'xl',
				min: 0,
				max: 5,
				step: 0.1
			});
			$(to).removeClass('hide');
		}

		applyRatingStar('.google-avg-rating');
	});

	var maxHeight = 0;
	$('.google-review-text').each(function (i, el) {
		var $el = $(el);
		var story = $el.text();
		var maxLength = 400;
		if (story.length >= maxLength) {
			var lastSpaceIndex = story.substr(0, maxLength).lastIndexOf(' ');
			$el.text(story.substr(0, lastSpaceIndex) + ' ...');
		}

		if ($(this).height() > maxHeight) {
			maxHeight = $(this).height();
		}
	});
	$('.google-review-text').css('height', maxHeight);

</script>
