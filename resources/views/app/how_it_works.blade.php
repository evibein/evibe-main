@extends('layout.base')

@section('page-title')
	<title>How Does Evibe Works? | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="Simple 3 steps to find and book the best party organisers, planners, party halls, decorators and many more."/>
@endsection

@section('meta-keywords')
	<meta name="keywords" content="How does Evibe.in work"/>
@endsection

@section('og-title')
	<meta property="og:title" content="How Does Evibe Works? | Evibe.in"/>
@endsection

@section('og-description')
	<meta property="og:description" content="Simple 3 steps to find and book the best party organisers, planners, party halls, decorators and many more."/>
@endsection

@section('og-url')
	<meta property="og:url" content="https://evibe.in/how-it-works"/>
@endsection

@section('meta-canonical')
	<link rel="canonical" href="{{ $canonicalUrl }}/how-it-works"/>
@endsection

@section("header")
	@include('app.header_noncity')
@endsection

@section("footer")
	@include('app.footer_noncity')
@endsection

@section("content")
	<article class="bkgnd-grey">
		<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
			<section class="page-sec how-it-works-sec page-sec-card">
				<section class="sub-sec">
					<div class="sec-head">
						<div class="sec-title-wrap">
							<h1 class="sec-title">Here's how Evibe can help you.</h1>
						</div>
					</div>
					<div class="sec-body">
						<p class="history-msg">
						<p>Evibe is a one stop platform to find and book the best party halls, birthday planners, artists and many more. Basically, everything at one place.</p>
						<p>In three simple steps, hire the best vendors completely customized based on your preference.</p>
						</p>
						<div class="hiw-items">
							<div class="hiw-item-wrap">
								<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
									<div class="hiw-item hiw-item-first">
										<div class="col-lg-8 col-md-8 col-sm-4">
											<div class="pull-left num-cnt">
												<h3 class="number">1</h3>
											</div>
											<div class="hiw-text">
												<h2 class="hiw-title">Discover</h2>
												<div class="hiw-subtitle">
													<ul>
														<li>Search for the vendors that match your preferences based on their previous work, offerings, gallery, packages and authentic reviews.</li>
														<li>Submit an enquiry to the vendors that you like.</li>
													</ul>
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4">
											<div class="hiw-item-img">
												<img src="{{ $galleryUrl }}/img/hew/discover.png" alt="Step 1: Discover">
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="hiw-items-sep mar-t-15">
								<hr>
							</div>
							<div class="hiw-item-wrap hiw-enquire">
								<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
									<div class="hiw-item">
										<div class="col-lg-4 col-md-4 col-sm-4">
											<div class="hiw-item-img">
												<img src="{{ $galleryUrl }}/img/hew/enquire.png" alt="Step 2: Enquire">
											</div>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-4">
											<div class="pull-left num-cnt">
												<h3 class="number">2</h3>
											</div>
											<div class="hiw-text">
												<h2 class="hiw-title">Shortlist</h2>
												<div class="hiw-subtitle">
													<ul>
														<li>Answer a few specific questions about what you need.</li>
														<li>Get access to the contact details of the most qualified vendor who can complete your request.</li>
													</ul>
												</div>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="hiw-items-sep">
								<hr>
							</div>
							<div class="hiw-item-wrap hiw-book">
								<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
									<div class="hiw-item">
										<div class="col-lg-8 col-md-8 col-sm-4">
											<div class="pull-left num-cnt">
												<h3 class="number">3</h3>
											</div>
											<div class="hiw-text">
												<h2 class="hiw-title">Hire</h2>
												<div class="hiw-subtitle">
													<ul>
														<li>Call or meet the recommended vendor if you need more customization.</li>
														<li>Book when you are convinced. Say goodbye to roaming!</li>
													</ul>
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4">
											<div class="hiw-item-img">
												<img src="{{ $galleryUrl }}/img/hew/book.png" alt="Step 3: Book">
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</section>
			</section>
		</div>
		<div class="clearfix"></div>
	</article>
@endsection