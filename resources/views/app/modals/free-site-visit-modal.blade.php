<div id="modalBookFreeSiteVisit" class="modal fade modal-book-free-site-visit" role="dialog" tabindex="-1" data-type="Normal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content mar-r-20">
			<button type="button" class="close site-visit-enquiry-close-btn">&times;
			</button>
			<div class="modal-header text-center show-400-600 unhide__400 hide">
				<h4 class="bs-title text-bold mar-t-5 no-mar-b">
					BOOK <span class="text-e">FREE</span> SITE VISIT
				</h4>
			</div>
			<div class="col-md-5 col-lg-5 no-pad hidden-sm hidden-xs site-visit-left-side-div">
				<div class="pc-left-side pc-left-side-spl-exps no-pad">
					<div class="pos-abs">
						<img src="{{ $galleryUrl }}/main/img/marriage-modal.jpg">
					</div>
					<div class="ls-none no-pad no-mar pc-booking-steps pc-spl-exps-booking-steps">
						<h4 class="bs-title text-bold">
							BOOK <span class="text-e">FREE</span> SITE VISIT
						</h4>
						<div class="font-18">
							<div class="pad-t-30">
								<i class="material-icons mar-r-5 font-22">thumb_up</i>
								<div class="in-blk valign-top mar-t-3">Guaranteed Lowest Price.</div>
							</div>
							<div class="pad-t-30">
								<i class="material-icons mar-r-5 font-22">verified_user</i>
								<div class="in-blk valign-top mar-t-3">Trusted by 1000s of Customers.</div>
							</div>
							<div class="pad-t-30">
								<i class="material-icons mar-r-5 font-22">update</i>
								<div class="in-blk valign-top mar-t-3">Latest Designs.</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-7 col-lg-7 no-pad">
				<form id="siteVisitEnquiryForm">
					<div class="modal-body sv-first-question">
						<div class="pad-l-10">
							<h4 class="text-cap">Who is getting married ?</h4>
						</div>
						<div class="pad-10">
							<input type="radio" id="featured1" class="custom-radio-animation" data-name="your" name="marriagePerson" value="Myself">
							<label for="featured1" class="custom-radio-animation-label">Myself</label>
							<br>
							<input type="radio" id="featured2" class="custom-radio-animation" data-name="your Cousin's" name="marriagePerson" value="My Cousin">
							<label for="featured2" class="custom-radio-animation-label">My Cousin</label>
							<br>
							<input type="radio" id="featured3" class="custom-radio-animation" data-name="your Daughter's" name="marriagePerson" value="My Daughter">
							<label for="featured3" class="custom-radio-animation-label">My Daughter</label>
							<br>
							<input type="radio" id="featured4" class="custom-radio-animation" data-name="your Son's" name="marriagePerson" value="My Son">
							<label for="featured4" class="custom-radio-animation-label">My Son</label>
							<br>
							<input type="radio" id="featured5" class="custom-radio-animation" data-name="your Friend's" name="marriagePerson" value="My Friend">
							<label for="featured5" class="custom-radio-animation-label">My Friend</label>
							<br>
							<input type="radio" id="featured6" class="custom-radio-animation" data-name="the" name="marriagePerson" value="Others">
							<label for="featured6" class="custom-radio-animation-label">Others</label>
						</div>
						<ul class="pager">
							<li><a class="sv-first-next">Next ></a></li>
						</ul>
					</div>
					<div class="modal-body sv-second-question hide">
						<div class="pad-l-10">
							<h4 class="text-cap">Awesome, so what is the event ?</h4>
						</div>
						<div class="pad-10">
							<input type="radio" id="featured7" class="custom-radio-animation" data-name="engagement" value="Engagement" name="personEvent">
							<label for="featured7" class="custom-radio-animation-label">Engagement</label>
							<br>
							<input type="radio" id="featured8" class="custom-radio-animation" data-name="sangeet" value="Sangeet" name="personEvent">
							<label for="featured8" class="custom-radio-animation-label">Sangeet</label>
							<br>
							<input type="radio" id="featured9" class="custom-radio-animation" data-name="wedding" value="Wedding" name="personEvent">
							<label for="featured9" class="custom-radio-animation-label">Wedding</label>
							<br>
							<input type="radio" id="featured10" class="custom-radio-animation" data-name="reception" value="Reception" name="personEvent">
							<label for="featured10" class="custom-radio-animation-label">Reception</label>
							<br>
							<input type="radio" id="featured11" class="custom-radio-animation" data-name="event" value="Other" name="personEvent">
							<label for="featured11" class="custom-radio-animation-label">Other</label>
							<br>
						</div>
						<ul class="pager">
							<li><a class="sv-second-back mar-r-15">< Back</a></li>
							<li><a class="sv-second-next">Next ></a></li>
						</ul>
					</div>
					<div class="modal-body sv-third-question hide">
						<div class="pad-l-10">
							<h4 class="text-cap">Are you looking for any other events ?</h4>
						</div>
						<div class="pad-10">
							<input type="checkbox" id="featured12" class="custom-checkbox-animation" value="Engagement" name="personOtherEvent">
							<label for="featured12" class="custom-checkbox-animation-label full-width">Engagement</label>
							<input type="checkbox" id="featured13" class="custom-checkbox-animation" value="Sangeet" name="personOtherEvent">
							<label for="featured13" class="custom-checkbox-animation-label full-width">Sangeet</label>
							<input type="checkbox" id="featured14" class="custom-checkbox-animation" value="Wedding" name="personOtherEvent">
							<label for="featured14" class="custom-checkbox-animation-label full-width">Wedding</label>
							<input type="checkbox" id="featured15" class="custom-checkbox-animation" value="Reception" name="personOtherEvent">
							<label for="featured15" class="custom-checkbox-animation-label full-width">Reception</label>
							<input type="checkbox" id="featured16" class="custom-checkbox-animation" value="Other" name="personOtherEvent">
							<label for="featured16" class="custom-checkbox-animation-label full-width">Other</label>
						</div>
						<ul class="pager">
							<li><a class="sv-third-back mar-r-15">< Back</a></li>
							<li><a class="sv-third-next">Next ></a></li>
						</ul>
					</div>
					<div class="modal-body sv-fourth-question hide">
						<div class="pad-l-10">
							<h4 class="text-cap sv-fourth-question-title">When is the event ?</h4>
						</div>
						<div>
							<div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input class="mdl-textfield__input mar-b-5" type="text" name="personPartyDate" id="personPartyDate" value=""/>
									<label class="mdl-textfield__label" for="personPartyDate">Event Date</label>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<ul class="pager">
							<li><a class="sv-fourth-back mar-r-15">< Back</a></li>
							<li><a class="sv-fourth-next">Next ></a></li>
						</ul>
					</div>
					<div class="modal-body sv-fifth-question hide">
						<div class="pad-l-10">
							<h4 class="text-cap">Where is the event location ?</h4>
						</div>
						<div>
							<div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
									<input class="mdl-textfield__input mar-b-5 google-auto-complete" type="text" name="svEventLocation" placeholder="" id="svEventLocation" value=""/>
									<label class="mdl-textfield__label" for="svEventLocation">Location</label>
									<input type="hidden" class="google-location-details" name="locationDetails" value="">
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<ul class="pager">
							<li><a class="sv-fifth-back mar-r-15">< Back</a></li>
							<li><a class="sv-fifth-next">Next ></a></li>
						</ul>
					</div>
					<div class="modal-body sv-sixth-question hide">
						<div class="pad-l-10">
							<h4 class="text-cap no-mar-b">What services are you looking for ?</h4>
						</div>
						<div class="pad-10 sv-selected-services-wrap">
							<h6 class="no-mar-t font-13"><i>Decors</i></h6>
							<div>
								<input type="checkbox" id="featured17" class="custom-checkbox-animation" value="Budgeted stage decoration (< Rs. 15,000)" name="svSelectedServices">
								<label for="featured17" class="custom-checkbox-animation-label full-width">Budgeted stage decoration (< &#8377;15,000)</label>
								<input type="checkbox" id="featured18" class="custom-checkbox-animation" value="Moderate stage decoration (> &#8377;25,000)" name="svSelectedServices">
								<label for="featured18" class="custom-checkbox-animation-label full-width">Moderate stage decoration (> &#8377;25,000)</label>
								<input type="checkbox" id="featured19" class="custom-checkbox-animation" value="Latest stage decoration (> &#8377;30,000)" name="svSelectedServices">
								<label for="featured19" class="custom-checkbox-animation-label full-width">Latest stage decoration (> &#8377;30,000)</label>
								<input type="checkbox" id="featured20" class="custom-checkbox-animation" value="Wedding Mandap decoration (> &#8377;40,000)" name="svSelectedServices">
								<label for="featured20" class="custom-checkbox-animation-label full-width">Wedding Mandap decoration (> &#8377;40,000)</label>
							</div>
							<hr>
							<h6 class="font-13"><i>Entertainment & Add-ons</i></h6>
							<div>
								<input type="checkbox" id="featured21" class="custom-checkbox-animation" value="Entertainment Options (&#8377;2000 onwards)" name="svSelectedServices">
								<label for="featured21" class="custom-checkbox-animation-label full-width">Entertainment Options (&#8377;2000 onwards)</label>
								<input type="checkbox" id="featured26" class="custom-checkbox-animation" value="Makeup Artists (&#8377;5000 onwards)" name="svSelectedServices">
								<label for="featured26" class="custom-checkbox-animation-label full-width">Makeup Artists (&#8377;5000 onwards)</label>
								<input type="checkbox" id="featured22" class="custom-checkbox-animation" value="Standard Photography (Rs 5,000 - &#8377;50,000)" name="svSelectedServices">
								<label for="featured22" class="custom-checkbox-animation-label full-width">Standard Photography (&#8377;5,000 - &#8377;50,000)</label>
								<input type="checkbox" id="featured23" class="custom-checkbox-animation" value="Candid Photography (&#8377;10,000 - &#8377;1,00,000)" name="svSelectedServices">
								<label for="featured23" class="custom-checkbox-animation-label full-width">Candid Photography (&#8377;10,000 - &#8377;1,00,000)</label>
								<input type="checkbox" id="featured24" class="custom-checkbox-animation" value="Shamiana/Chairs/Tables (&#8377;4000 onwards)" name="svSelectedServices">
								<label for="featured24" class="custom-checkbox-animation-label full-width">Shamiana/Chairs/Tables (&#8377;4000 onwards)</label>
								<input type="checkbox" id="featured25" class="custom-checkbox-animation" value="Theme Cake (&#8377;3000 onwards)" name="svSelectedServices">
								<label for="featured25" class="custom-checkbox-animation-label full-width">Theme Cake (&#8377;3000 onwards)</label>
								<input type="checkbox" id="featured27" class="custom-checkbox-animation" value="Others" name="svSelectedServices">
								<label for="featured27" class="custom-checkbox-animation-label full-width">Others</label>
							</div>
						</div>
						<ul class="pager">
							<li><a class="sv-sixth-back mar-r-15">< Back</a></li>
							<li><a class="sv-sixth-next">Next ></a></li>
						</ul>
					</div>
					<div class="modal-body sv-seventh-question hide">
						<div class="pad-l-10">
							<h4 class="text-cap">Please share your site visit details</h4>
						</div>
						<div>
							<div class="col-md-6 col-lg-6 col-sm-12">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input class="mdl-textfield__input mar-b-5" type="text" name="svAddressLine1" id="svAddressLine1" value=""/>
									<label class="mdl-textfield__label" for="svAddressLine1">Address line 1</label>
								</div>
							</div>
							<div class="col-md-6 col-lg-6 col-sm-12">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input class="mdl-textfield__input mar-b-5" type="text" name="svAddressLine2" id="svAddressLine2" value=""/>
									<label class="mdl-textfield__label" for="svAddressLine2">Address line 2</label>
								</div>
							</div>
							<div>
								<div class="col-md-6 col-lg-6 col-sm-12">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<input class="mdl-textfield__input mar-b-5" type="text" name="svLocation" id="svLocation" value=""/>
										<label class="mdl-textfield__label" for="svLocation">Location</label>
										<input type="hidden" class="google-location-details" name="locationDetails" value="">
									</div>
								</div>
								<div class="col-md-6 col-lg-6 col-sm-12">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<input class="mdl-textfield__input mar-b-5" type="text" name="svZipCode" id="svZipCode" value=""/>
										<label class="mdl-textfield__label" for="svZipCode">Zip code</label>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div>
								<div class="col-md-6 col-lg-6 col-sm-12">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<input class="mdl-textfield__input mar-b-5" type="text" name="svVisitDate" id="svVisitDate" value=""/>
										<label class="mdl-textfield__label" for="svVisitDate">Date</label>
									</div>
								</div>
								<div class="col-md-6 col-lg-6 col-sm-12">
									<label for="svVisitSlot" class="custom-mdl-label">Time Slot*</label>
									<select class="form-control" id="svVisitSlot" name="svVisitSlot">
										<option value="10:00 AM - 12:00 AM">Morning (10:00 AM - 1:00 PM)</option>
										<option value="12:00 AM - 02:00 PM">Afternoon (1:00 PM - 3:00 PM)</option>
										<option value="02:00 AM - 04:00 PM">Evening (3:00 PM - 6:00 PM)</option>
										<option value="04:00 AM - 06:00 PM">Night (6:00 PM - 8:00 PM)</option>
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
						</div>
						<ul class="pager">
							<li><a class="sv-seventh-back mar-r-15">< Back</a></li>
							<li><a class="sv-seventh-next">Next ></a></li>
						</ul>
					</div>
					<div class="modal-body sv-eighth-question hide">
						<div class="pad-l-10">
							<h4 class="text-cap">How soon are you planning to book ?</h4>
						</div>
						<div class="pad-10">
							<input type="radio" id="featured28" class="custom-radio-animation" data-name="engagement" value="Immediately" name="svPlanningStatus">
							<label for="featured28" class="custom-radio-animation-label">Immediately</label>
							<br>
							<input type="radio" id="featured29" class="custom-radio-animation" data-name="sangeet" value="In a week" name="svPlanningStatus">
							<label for="featured29" class="custom-radio-animation-label">In a week</label>
							<br>
							<input type="radio" id="featured30" class="custom-radio-animation" data-name="wedding" value="In a month" name="svPlanningStatus">
							<label for="featured30" class="custom-radio-animation-label">In a month</label>
							<br>
							<input type="radio" id="featured31" class="custom-radio-animation" data-name="reception" value="Later" name="svPlanningStatus">
							<label for="featured31" class="custom-radio-animation-label">Later</label>
							<br>
							<input type="radio" id="featured32" class="custom-radio-animation" data-name="reception" value="Just Browsing" name="svPlanningStatus">
							<label for="featured32" class="custom-radio-animation-label">Just Browsing</label>
						</div>
						<ul class="pager">
							<li><a class="sv-eighth-back mar-r-15">< Back</a></li>
							<li><a class="sv-eighth-next">Next ></a></li>
						</ul>
					</div>
					<div class="modal-body sv-ninth-question hide">
						<div class="pad-l-10">
							<h4 class="text-cap">How Should We Contact You ?</h4>
						</div>
						<div class="form-group">
							<div class="col-sm-10 col-xs-10 col-xs-offset-1 col-sm-offset-1 no-pad mar-t-15">
								<div class="col-md-6 col-lg-6 col-sm-12">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
										<input class="mdl-textfield__input mar-b-5" type="text" name="svContactName" id="svContactName" value="@if(auth()->check()){{ auth()->user()->name }}@endif"/>
										<label class="mdl-textfield__label" for="svContactName">Name*</label>
									</div>
								</div>
								<div class="col-md-6 col-lg-6 col-sm-12">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<input class="mdl-textfield__input mar-b-5" type="text" name="svContactPhone" id="svContactPhone" value=""/>
										<label class="mdl-textfield__label" for="svContactPhone">Phone Number*</label>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-10 col-xs-10 col-xs-offset-1 col-sm-offset-1 no-pad">
								<div class="col-md-6 col-lg-6 col-sm-12">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<input class="mdl-textfield__input mar-b-5 email-typo-error" type="text" name="scContactEmail" id="svContactEmail" value="@if(auth()->check()){{ auth()->user()->username }}@endif"/>
										<label class="mdl-textfield__label" for="scContactEmail">Email*</label>
									</div>
								</div>
								<div class="col-md-6 col-lg-6 col-sm-12">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<input class="mdl-textfield__input mar-b-5" type="text" name="svContactAltNumber" id="svContactAltNumber" value=""/>
										<label class="mdl-textfield__label" for="svContactAltNumber">Alternate Phone Number</label>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="text-center">
							<div class="btn btn-link in-blk mar-t-20 sv-ninth-back mar-r-15">< Back</div>
							<div class="btn btn-evibe font-16 mar-t-20 submit-pre-post-site-visit" data-url="{{ route("city.occasion.pre-post.save.site.visit", getCityUrl()) }}">
								<span>Book Site Visit</span>
							</div>
							<div class="btn btn-evibe font-16 mar-t-20 submitting-pre-post-site-visit hide">
								<span>Submitting....</span>
							</div>
						</div>
					</div>
				</form>
				<div class="modal-body sv-thank-you pad-l-20 pad-r-20 valign-mid hide">
					<h5 class="text-center success pad-t-20 pad-b-20">
						<img src="{{ $galleryUrl }}/main/img/icons/hi_five.png" height="80px" width="80px" alt="success icon">
						<br>We received your requirements. We will get in touch with you shortly.
					</h5>
					<b><i>Next Steps?</i></b>
					<div class="pad-l-20 pad-t-10">
						<div>1. You will receive the site visit appointment confirmation status within 1 business day.</div>
						<div>2. Get all your questions clarified from Evibe wedding expert.</div>
						<div>3. Get a pre-negotiated price quote within 1 business day after site visit. No obligation to book.</div>
						<div>4. If you are satisfied, book the services with 50% advance payment and get a detailed booking receipt.</div>
					</div>
					<div class="text-center">
						<div class="btn btn-evibe font-16 mar-t-20 sv-continue-browsing">
							<span>CONTINUE BROWSING</span>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			window.questionNumber = 1;

			$("#personPartyDate, #svVisitDate").datetimepicker({
				timepicker: false,
				format: 'Y/m/d',
				minDate: '-1969/12/29',
				closeOnDateSelect: true,
				disabledDates: window.disableDates(),
				onSelectDate: function (ct, $i) {
					$i.parent().addClass('is-dirty');
				}
			});

			(function formActionButtons() {

				$('.sv-first-next').on('click', function () {
					window.questionNumber = 2;
					if ($("input:radio[name='marriagePerson']").is(":checked")) {
						$('.sv-first-question').addClass('hide');
						$('.sv-second-question').removeClass('hide');
					}
					else {
						showNotyError("Please select an option to continue")
					}
				});

				$('.sv-second-back').on('click', function () {
					window.questionNumber = 1;

					$('.sv-first-question').removeClass('hide');
					$('.sv-second-question').addClass('hide');
				});

				$('.sv-second-next').on('click', function () {
					window.questionNumber = 3;

					if ($("input:radio[name='personEvent']").is(":checked")) {
						$('.sv-second-question').addClass('hide');

						var selectedEvent = $("input:radio[name='personEvent']:checked").val();

						$("input:checkbox[name='personOtherEventSkip']").prop('checked', true);

						$("input:checkbox[name='personOtherEvent']").each(function () {

							$("label[for='" + $(this).attr('id') + "']").removeClass('hide');
							$(this).removeClass('hide').prop('checked', false);

							if ($(this).val() == selectedEvent) {
								$("label[for='" + $(this).attr('id') + "']").addClass('hide');
								$(this).addClass('hide');
							}
						});

						$('.sv-third-question').removeClass('hide');
					}
					else {
						showNotyError("Please select an option to continue")
					}
				});

				$('.sv-third-back').on('click', function () {
					window.questionNumber = 2;

					$('.sv-third-question').addClass('hide');
					$('.sv-second-question').removeClass('hide');
				});

				$('.sv-third-next').on('click', function () {
					window.questionNumber = 4;

					var name = $("input:radio[name='marriagePerson']:checked").data("name");
					var event = $("input:radio[name='personEvent']:checked").data("name");
					$('.sv-fourth-question-title').empty().text("When is " + name + " " + event + " ?");

					$('.sv-third-question').addClass('hide');
					$('.sv-fourth-question').removeClass('hide');
				});

				$('.sv-fourth-back').on('click', function () {
					window.questionNumber = 3;

					$('.sv-fourth-question').addClass('hide');
					$('.sv-third-question').removeClass('hide');
				});

				$('.sv-fourth-next').on('click', function () {
					window.questionNumber = 5;

					if ($('#personPartyDate').val()) {
						$('.sv-fourth-question').addClass('hide');
						$('.sv-fifth-question').removeClass('hide');
					}
					else {
						showNotyError("Please select date to continue")
					}
				});

				$('.sv-fifth-back').on('click', function () {
					window.questionNumber = 4;

					$('.sv-fifth-question').addClass('hide');
					$('.sv-fourth-question').removeClass('hide');
				});

				$('.sv-fifth-next').on('click', function () {
					window.questionNumber = 6;

					if ($('#svEventLocation').val()) {
						$('.sv-fifth-question').addClass('hide');
						$('.sv-sixth-question').removeClass('hide');
					}
					else {
						showNotyError("Please enter the location to continue")
					}
				});

				$('.sv-sixth-back').on('click', function () {
					window.questionNumber = 5;

					$('.sv-sixth-question').addClass('hide');
					$('.sv-fifth-question').removeClass('hide');
				});

				$('.sv-sixth-next').on('click', function () {
					window.questionNumber = 7;

					if ($("input:checkbox[name='svSelectedServices']").is(":checked")) {
						$('.sv-sixth-question').addClass('hide');
						$('.sv-seventh-question').removeClass('hide');
					}
					else {
						showNotyError("Please select an option to continue")
					}
				});

				$('.sv-seventh-back').on('click', function () {
					window.questionNumber = 6;

					$('.sv-seventh-question').addClass('hide');
					$('.sv-sixth-question').removeClass('hide');
				});

				$('.sv-seventh-next').on('click', function () {
					window.questionNumber = 8;

					if ($('#svAddressLine1').val() && $('#svAddressLine2').val() && $('#svLocation').val() && $('#svZipCode').val() && $('#svVisitDate').val()) {
						if ($.isNumeric($('#svZipCode').val()) && ($('#svZipCode').val().toString().length == 6)) {
							$('.sv-seventh-question').addClass('hide');
							$('.sv-eighth-question').removeClass('hide');
						}
						else {
							showNotyError("Please enter a valid pin code.")
						}
					}
					else {
						showNotyError("Please select an option to continue")
					}
				});

				$('.sv-eighth-back').on('click', function () {
					window.questionNumber = 7;

					$('.sv-eighth-question').addClass('hide');
					$('.sv-seventh-question').removeClass('hide');
				});

				$('.sv-eighth-next').on('click', function () {
					window.questionNumber = 9;

					if ($("input:radio[name='svPlanningStatus']").is(":checked")) {
						$('.sv-eighth-question').addClass('hide');
						$('.sv-ninth-question').removeClass('hide');
					}
					else {
						showNotyError("Please fill all the fields to continue")
					}
				});

				$('.btn-svq-back').on('click', function () {

					var preQn = $(this).data('pre-q');
					$('.sv-question').addClass('hide');
					window.questionNumber = $(this).data('cur-q');
					$('.sv-questions-list').find('.sv-question[data-qid=' + preQn).removeClass('hide');
					$('.sv-question').removeClass('hide');
				});

				$('.sv-continue-browsing').on('click', function () {
					/* Cookie expires in 15 days - to be used to show auto popup */
					Cookies.set('SiteVisitSubmit', 'Yes', {expires: 15});
					location.reload();
				})
			})();

			(function needHelpButtonCLick() {
				$('.btn-header-book-free-site-visit, .btn-profile-book-free-site-visit').click(function (event) {
					event.preventDefault();

					/* open the modal */
					var svModal = $('#modalBookFreeSiteVisit');
					svModal.data("type", "Normal");
					svModal.modal({
						'show': true,
						'backdrop': 'static',
						'keyboard': false
					});
				});
			})();

			(function saveSiteVisitDetails() {
				$('.submit-pre-post-site-visit').on('click', function (e) {
					e.preventDefault();

					var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

					if (!$('#svContactName').val() || !$('#svContactPhone').val() || !$('#svContactEmail').val()) {
						showNotyError("Please fill the details");
					}
					else if (!$.isNumeric($('#svContactPhone').val()) || ($('#svContactPhone').val().toString().length != 10)) {
						showNotyError("Please enter a valid phone number")
					}
					else if ($('#svContactAltNumber').val() && (!$.isNumeric($('#svContactAltNumber').val()) || ($('#svContactAltNumber').val().toString().length != 10))) {
						showNotyError("Please enter a valid alternate phone number")
					}
					else if (!regex.test($('#svContactEmail').val())) {
						showNotyError("Please enter a valid Email Address");
					}
					else {
						var otherEvents = [];
						var servicesSelected = [];
						var i = j = 0;

						$("input:checkbox[name='personOtherEvent']:checked").each(function () {
							otherEvents[i] = $(this).val();
							i++;
						});

						$("input:checkbox[name='svSelectedServices']:checked").each(function () {
							servicesSelected[j] = $(this).val();
							j++;
						});

						var formUrl = $(this).data('url');
						disableSubmitButton();

						var data = {
							'marriagePerson': $("input:radio[name='marriagePerson']:checked").val(),
							'personEvent': $("input:radio[name='personEvent']:checked").val(),
							'personOtherEvent': otherEvents,
							'personPartyDate': $('#personPartyDate').val(),
							'svEventLocation': $("#svEventLocation").val(),
							'svSelectedServices': servicesSelected,
							'svPlanningStatus': $("input:radio[name='svPlanningStatus']:checked").val(),
							'svAddressLine1': $('#svAddressLine1').val(),
							'svAddressLine2': $('#svAddressLine2').val(),
							'svLocation': $('#svLocation').val(),
							'svZipCode': $('#svZipCode').val(),
							'svVisitDate': $('#svVisitDate').val(),
							'svVisitSlot': $('#svVisitSlot').val(),
							'svContactName': $('#svContactName').val(),
							'svContactPhone': $('#svContactPhone').val(),
							'svContactEmail': $('#svContactEmail').val(),
							'svContactAltNumber': $('#svContactAltNumber').val()
						};

						$.ajax({
							url: formUrl,
							type: "POST",
							data: data,
							success: function (data) {
								if (data.success) {
									window.questionNumber = "submit";

									$('.sv-ninth-question').addClass('hide');
									$('.sv-thank-you').removeClass('hide');
								}
								else if (data.error) {
									showSubmitButton();
									showNotyError(data.error);
								}
							},
							error: function (jqXHR, textStatus, errorThrown) {
								showSubmitButton();
								window.showNotyError("An error occurred while submitting your request. Please try again later.");
								window.notifyTeam({
									"url": url,
									"textStatus": textStatus,
									"errorThrown": errorThrown
								});
							}
						});
					}
				});
			})();

			(function closeSiteVisitModal() {
				$('.site-visit-enquiry-close-btn').on('click', function () {
					var svModal = $('#modalBookFreeSiteVisit');
					svModal.modal("hide");

					/* Set cookie for showing auto popup check - expires in 30 mins */
					var isAutoPopupClose = svModal.data('type');
					if (isAutoPopupClose !== undefined && isAutoPopupClose === "Auto") {
						Cookies.set('AutoSiteVisitClose', 'Yes', {expires: (1 / 48)});
					}
				})
			})();

			function showSubmitButton() {
				$('.submit-pre-post-site-visit').removeClass('hide');
				$('.submitting-pre-post-site-visit').addClass('hide');
			}

			function disableSubmitButton() {
				$('.submit-pre-post-site-visit').addClass('hide');
				$('.submitting-pre-post-site-visit').removeClass('hide');
			}
		});
	</script>
@endsection