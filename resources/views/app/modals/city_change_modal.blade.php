<section class="modal fade city-modal" style="margin:auto" id="cityChangeModal" tabindex="-1" role="dialog" aria-labelledby="cityChangeModalLabel" aria-hidden="true">
	<div class="modal-dialog" @if(!(($agent->isMobile())&&(!($agent->isTablet())))) style="max-width:1024px; width: 80%;" @endif>
		<div class="modal-content">
			<!-- <div class="pad-l-15 pad-r-15 no-pad-b full-width">
				<div class="modal-header ui-widget ui-autocomplete-input text-center city-search-wrap no-pad full-width" style="height: 45px; border: 1px solid #CCCCCC; border-radius: 5px; display: inline-flex;">
					<span class="glyphicon glyphicon-search pad-l-15" style="margin-top: 14px; color: #999999;"></span>
					<input type="text" id="avail-cities" class="city-search-bar full-width pad-l-10" placeholder="Search for your city" style="height: 43px; border-radius: 5px; color: #999999; flex: 1; border: none;">
				</div>
			</div> -->
			<div class="modal-body no-pad-t">
				<div class="Title text-center">
					<h5> Popular Cities</h5>
				</div>
				@if($agent->isMobile() || $agent->isTablet())
					<div class="active-cities text-center">
						<div class="col-xs-6 text-center">
							<a value="1" data-url="bangalore" class="city-link enable-link" data-colorimg="{{ $galleryUrl }}/main/img/icons/cities/bangalore-color.png" data-bwimg="{{ $galleryUrl }}/main/img/icons/cities/bangalore-bw.png">
								<img class="city-modal-image" src="{{ $galleryUrl }}/main/img/icons/cities/bangalore-color.png" alt="Bangalore">
								<div class="city-name">Bangalore</div>
							</a>
						</div>
						<div class="col-xs-6 text-center">
							<a value="2" data-url="hyderabad" class="city-link enable-link" data-colorimg="{{ $galleryUrl }}/main/img/icons/cities/hyderabad-color.png" data-bwimg="{{ $galleryUrl }}/main/img/icons/cities/hyderabad-bw.png">
								<img class="city-modal-image" src="{{ $galleryUrl }}/main/img/icons/cities/hyderabad-color.png" alt="Hyderabad">
								<div class="city-name">Hyderabad</div>
							</a>
						</div>
						<div class="col-xs-6 text-center">
							<a value="4" data-url="mumbai" class="city-link enable-link" data-colorimg="{{ $galleryUrl }}/main/img/icons/cities/mumbai-color.png" data-bwimg="{{ $galleryUrl }}/main/img/icons/cities/mumbai-bw.png">
								<img class="city-modal-image" src="{{ $galleryUrl }}/main/img/icons/cities/mumbai-color.png" alt="Mumbai">
								<div class="city-name">Mumbai</div>
							</a>
						</div>
						<div class="col-xs-6 text-center">
							<a value="3" data-url="delhi" class="city-link enable-link" data-colorimg="{{ $galleryUrl }}/main/img/icons/cities/delhi-color.png" data-bwimg="{{ $galleryUrl }}/main/img/icons/cities/delhi-bw.png">
								<img class="city-modal-image" src="{{ $galleryUrl }}/main/img/icons/cities/delhi-color.png" alt="Delhi">
								<div class="city-name">Delhi</div>
							</a>
						</div>
						<div class="col-xs-6 text-center">
							<a value="5" data-url="pune" class="city-link enable-link" data-colorimg="{{ $galleryUrl }}/main/img/icons/cities/pune-color.png" data-bwimg="{{ $galleryUrl }}/main/img/icons/cities/pune-bw.png">
								<img class="city-modal-image" src="{{ $galleryUrl }}/main/img/icons/cities/pune-color.png" alt="Pune">
								<div class="city-name">Pune</div>
							</a>
						</div>
						<div class="col-xs-6 text-center">
							<a value="8" data-url="chennai" class="city-link enable-link" data-colorimg="{{ $galleryUrl }}/main/img/icons/cities/chennai_color.png" data-bwimg="{{ $galleryUrl }}/main/img/icons/cities/chennai-bw.png">
								<img class="city-modal-image" src="{{ $galleryUrl }}/main/img/icons/cities/chennai-color.png" alt="Chennai">
								<div class="city-name">Chennai</div>
							</a>
						</div>
						<div class="col-xs-12 text-center">
							<a value="8" data-url="jaipur" class="city-link enable-link" data-colorimg="{{ $galleryUrl }}/main/img/icons/cities/jaipur_color.png" data-bwimg="{{ $galleryUrl }}/main/img/icons/cities/jaipur-bw.png">
								<img class="city-modal-image" src="{{ $galleryUrl }}/main/img/icons/cities/jaipur-color.png" alt="Jaipur">
								<div class="city-name">Jaipur</div>
							</a>
						</div>
						<div class="clearfix"></div>
						<div class="btn-view-all-cities mar-t-20" style="color:#333;cursor: pointer;"> View Other Cities
							<span class="glyphicon glyphicon-chevron-down" style="margin-left: 6px;margin-top: 0;top: 2px;"></span></div>
						<div class="all-cities-wrap text-left mar-t-20">
							<div class="mar-t-30 mar-b-20 font-16 text-bold text-center hide">Other Cities</div>
						</div>
					</div>
				@else
					<div class="active-cities text-center">
						<div class="" style="display: inline-block;margin: 0 2%;text-align: center;">
							<a value="1" data-url="bangalore" class="city-link enable-link" data-colorimg="{{ $galleryUrl }}/main/img/icons/cities/bangalore-color.png" data-bwimg="{{ $galleryUrl }}/main/img/icons/cities/bangalore-bw.png">
								<img class="city-modal-image" src="{{ $galleryUrl }}/main/img/icons/cities/bangalore-bw.png" alt="Bangalore">
								<div class="city-name">Bangalore</div>
							</a>
						</div>
						<div class="" style="display: inline-block;margin: 0 2%;text-align: center;">
							<a value="2" data-url="hyderabad" class="city-link enable-link" data-colorimg="{{ $galleryUrl }}/main/img/icons/cities/hyderabad-color.png" data-bwimg="{{ $galleryUrl }}/main/img/icons/cities/hyderabad-bw.png">
								<img class="city-modal-image" src="{{ $galleryUrl }}/main/img/icons/cities/hyderabad-bw.png" alt="Hyderabad">
								<div class="city-name">Hyderabad</div>
							</a>
						</div>
						<div class="" style="display: inline-block;margin: 0 2%;text-align: center;">
							<a value="4" data-url="mumbai" class="city-link enable-link" data-colorimg="{{ $galleryUrl }}/main/img/icons/cities/mumbai-color.png" data-bwimg="{{ $galleryUrl }}/main/img/icons/cities/mumbai-bw.png">
								<img class="city-modal-image" src="{{ $galleryUrl }}/main/img/icons/cities/mumbai-bw.png" alt="Mumbai">
								<div class="city-name">Mumbai</div>
							</a>
						</div>
						<div class="" style="display: inline-block;margin: 0 2%;text-align: center;">
							<a value="3" data-url="delhi" class="city-link enable-link" data-colorimg="{{ $galleryUrl }}/main/img/icons/cities/delhi-color.png" data-bwimg="{{ $galleryUrl }}/main/img/icons/cities/delhi-bw.png">
								<img class="city-modal-image" src="{{ $galleryUrl }}/main/img/icons/cities/delhi-bw.png" alt="Delhi">
								<div class="city-name">Delhi</div>
							</a>
						</div>
						<div class="" style="display: inline-block;margin: 0 2%;text-align: center;">
							<a value="5" data-url="pune" class="city-link enable-link" data-colorimg="{{ $galleryUrl }}/main/img/icons/cities/pune-color.png" data-bwimg="{{ $galleryUrl }}/main/img/icons/cities/pune-bw.png">
								<img class="city-modal-image" src="{{ $galleryUrl }}/main/img/icons/cities/pune-bw.png" alt="Pune">
								<div class="city-name">Pune</div>
							</a>
						</div>
						<div class="" style="display: inline-block;margin: 0 2%;text-align: center;">
							<a value="8" data-url="chennai" class="city-link enable-link" data-colorimg="{{ $galleryUrl }}/main/img/icons/cities/chennai_color.png" data-bwimg="{{ $galleryUrl }}/main/img/icons/cities/chennai-bw.png">
								<img class="city-modal-image" src="{{ $galleryUrl }}/main/img/icons/cities/chennai-bw.png" alt="Chennai">
								<div class="city-name">Chennai</div>
							</a>
						</div>
						<div class="" style="display: inline-block;margin: 0 2%;text-align: center;">
							<a value="8" data-url="jaipur" class="city-link enable-link" data-colorimg="{{ $galleryUrl }}/main/img/icons/cities/jaipur_color.png" data-bwimg="{{ $galleryUrl }}/main/img/icons/cities/jaipur-bw.png">
								<img class="city-modal-image" src="{{ $galleryUrl }}/main/img/icons/cities/jaipur-bw.png" alt="Jaipur">
								<div class="city-name">Jaipur</div>
							</a>
						</div>
						<div class="clearfix"></div>
						<div class="btn-view-all-cities mar-t-20 cur-point" style="color:#333"> View Other Cities<span class="glyphicon glyphicon-chevron-down" style="margin-left: 6px;margin-top: 0;top: 2px;"></span></div>
						<div class="all-cities-wrap all-cities-wrap-desktop all-cities-wrap text-left mar-t-20" style="padding-left: 10%; padding-right: 10%;">
							<div class="mar-t-30 mar-b-20 font-16 text-bold text-center hide">Other Cities</div>
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</section>
