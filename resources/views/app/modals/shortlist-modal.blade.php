@if(isset($data) && $data)
	<div class="shortlist-modal-container">
		<div class="mar-15">
			<div class="shortlist-modal-product-title">{{ $data['name'] }}</div>
			<div class="hide" id="hidShortlistModalProductInfo" data-product-id="{{ $data['productId'] }}" data-product-type-id="{{ $data['productTypeId'] }}"></div>
			<div class="hide" id="hidShortlistModalProductPrice" data-product-price="{{ $data['price'] }}" data-product-price-worth="{{ $data['priceWorth'] }}" data-advance-percent="{{ $data['advancePercent'] }}"></div>
			<div class="mar-t-20">
				<div class="shortlist-modal-price-container">
					<div class="split-price-wrap">
						<span class="pull-left">Product Price</span>
						<span class="pull-right">
							<span class='rupee-font mar-r-2'>&#8377;</span>
							<span class="price-worth" data-price="">{{ $data['priceWorth'] }}</span>
						</span>
						<div class="clearfix"></div>
					</div>
					<div class="split-price-wrap hide">
						<span class="pull-left">Product Actual Price</span>
						<span class="pull-right">
							<span class='rupee-font mar-r-2'>&#8377;</span>
							<span class="price" data-price="">{{ $data['price'] }}</span>
						</span>
						<div class="clearfix"></div>
					</div>
					<div class="split-price-wrap">
						<span class="pull-left">Evibe discount</span>
						<span class="pull-right text-g">
							<span class="mar-r-2">-</span>
							<span class='rupee-font mar-r-2'>&#8377;</span>
							<span class="price-off" data-price="">{{ $data['priceWorth'] - $data['price'] }}</span>
						</span>
						<div class="clearfix"></div>
					</div>
					<div class="product-split-transport-price">
						<span class="pull-left">Transportation Charges</span>
						<span id="transportFree" class="transport-free pull-right text-free">Free</span>
						<span id="transportPriceContent" class="transport-price-content pull-right hide">
							<span class='rupee-font mar-r-2'>&#8377;</span>
							<span class="transport-price" data-price=""></span>
						</span>
						<div class="clearfix"></div>
					</div>
					<hr class="shortlist-modal-hr">
					<div class="split-price-wrap">
						<span class="pull-left">Total Price</span>
						<span class="pull-right">
							<span class='rupee-font mar-r-2'>&#8377;</span>
							<span class="total-price" data-rice="">{{ $data['price'] }}</span>
						</span>
						<div class="clearfix"></div>
					</div>
					<hr class="shortlist-modal-hr hide">
					<div class="split-price-wrap text-bold hide">
						<span class="pull-left">Advance To Pay</span>
						<span class="pull-right text-e">
							<span class='rupee-font mar-r-2'>&#8377;</span>
							<span class="advance-amount" data-price=""></span>
						</span>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="shortlist-modal-delivery-text mar-t-10 font-14 lh-18"></div>
		</div>
		<div>
			<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1 text-center no-pad">
				@if(isset($data['productTypeId']) && ($data['productTypeId']) == config('evibe.ticket.type.decor'))
				@elseif(isset($data['productTypeId']) && ($data['productTypeId']) == config('evibe.ticket.type.cake'))
					@if(isset($data['deliverySlots']) && count($data['deliverySlots']))
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="input-field shortlist-modal-field-wrap">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<label class="mdl-textfield__label" for="shortlistModalDeliverySlot">
										<span class="label-text">Delivery Slot</span>
									</label>
									<select name="shortlistModalDeliverySlot" id="shortlistModalDeliverySlot" class="mdl-textfield__input">
										@foreach($data['deliverySlots'] as $slot)
											<option value="{{ $slot->id }}" data-price="{{ $slot->price }}">{{ $slot->formattedSlot() }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
					@endif
					@if(isset($data['checkoutFields']) && count($data['checkoutFields']) > 0)
						@foreach($data['checkoutFields'] as $catId => $addOn)
							@if($addOn && isset($addOn['values']) && count($addOn['values']))
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="input-field shortlist-modal-field-wrap" data-catid="{{ $catId }}" data-name="{{ $addOn['name'] }}">
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
											<label class="mdl-textfield__label">
												<span class="label-text">{{ $addOn['name'] }}</span>
											</label>
											<select name="shortlistModalFieldCat" class="mdl-textfield__input shortlist-modal-field-cat">
												@foreach($addOn['values'] as $categoryId => $addOnValue)
													<option value="{{ $addOnValue['id'] }}" data-price="{{ $addOnValue['price'] }}" data-worth="{{ $addOnValue['priceWorth'] }}" data-isfixed="{{ $addOnValue['isFixed'] }}" data-weight="{{ $addOnValue['weight'] }}" data-name="{{ $addOnValue['name'] }}" data-displayoption="@if(isset($addOnValue['identifier'])) {{ $addOnValue['identifier'] }}@endif">
														@if(isset($addOnValue['identifier'])){{ $addOnValue['identifier'] }}@else{{ $addOnValue['name'] }}@endif
													</option>
												@endforeach
											</select>
										</div>
									</div>
								</div>
							@endif
						@endforeach
					@endif
				@elseif(isset($data['productTypeId']) &&
				((($data['productTypeId']) == config('evibe.ticket.type.package')) ||
				(($data['productTypeId']) == config('evibe.ticket.type.food')) ||
				(($data['productTypeId']) == config('evibe.ticket.type.priests')) ||
				(($data['productTypeId']) == config('evibe.ticket.type.tents')) ||
				(($data['productTypeId']) == config('evibe.ticket.type.surprises')) ||
				(($data['productTypeId']) == config('evibe.ticket.type.generic-package'))))
				@endif
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
@endif

<script type="text/javascript">
	$(document).ready(function () {

		var productPrice = $('#hidShortlistModalProductPrice').data('product-price');
		var productPriceWorth = $('#hidShortlistModalProductPrice').data('product-price-worth');
		var advancePercent = $('#hidShortlistModalProductPrice').data('advance-percent');
		var updatedProductPrice = productPrice;
		var updatedProductPriceWorth = productPriceWorth;
		var transportCharges = 0;
		var deliverySlotPrice = 0;

		/* To apply css to text fields after loading */
		$(".shortlist-modal-container").find(".mdl-textfield").removeClass("is-upgraded").attr("data-upgraded", "");
		if (!(typeof(componentHandler) === 'undefined')) {
			componentHandler.upgradeDom();
		}

		/* add delivery charges on change (if applicable) */
		$('#shortlistModalDeliverySlot').change(function () {
			var slotId = $(this).val();
			var slotSelector = $('.shortlist-modal-container').find('#shortlistModalDeliverySlot');
			calculateAndUpdateDeliverySlotPrice(slotId, slotSelector)
		});

		/* change price on changing of option */
		$('.shortlist-modal-container .shortlist-modal-field-cat').change(function (event) {
			event.preventDefault();
			var selected = getCurrentValue();
			calculateCatPrice(selected);
			updateCalculatedPrices();
		});

		/* get all selected values */
		function getCurrentValue() {
			var selectedData = [];
			$('.shortlist-modal-container .shortlist-modal-field-cat option:selected').each(function () {
				selectedData.push({
					'catId': $(this).parents('.shortlist-modal-field-wrap').data('catid'),
					'catName': $(this).parents('.shortlist-modal-field-wrap').data('name'),
					'id': $(this).val(),
					'price': $(this).data('price'),
					'worth': $(this).data('worth'),
					'isFixed': $(this).data('isfixed'),
					'weight': $(this).data('weight'),
					'name': $(this).data('name')
				})
			});
			$('.shortlist-modal-container .shortlist-modal-field-cat option').each(function () {
				if ($(this).is(":selected")) {
					$(this).text($(this).data('name'));
				}
				else if ($(this).data('displayoption').length) {
					$(this).text($(this).data('displayoption'));
				}
			});
			return selectedData;
		}

		/* calculate delivery slot price */
		function calculateDeliverySlotPrice(slotId, slotSelector) {
			deliverySlotPrice = slotSelector.find('option[value=' + slotId + ']').data('price');
		}

		function calculateAndUpdateDeliverySlotPrice(slotId, slotSelector) {
			calculateDeliverySlotPrice(slotId, slotSelector);
			updateCalculatedPrices();
		}

		/* show price */
		function calculateCatPrice(selected) {
			var allPrice = calculatePrice(selected);

			if (!isNaN(allPrice.price) && (allPrice.price !== '') && (allPrice.price !== undefined)) {
				updatedProductPrice = allPrice.price;
			}
			if (!isNaN(allPrice.worth) && (allPrice.worth !== '') && allPrice.worth !== undefined) {
				updatedProductPriceWorth = allPrice.worth;
			}
		}

		/* calculate price based on selected options */
		function calculatePrice(selected) {

			var defaultSelector = $('.shortlist-modal-container .shortlist-modal-field-cat option[value="-1"]');
			var defaultPrice = defaultSelector.data('price');
			var defaultWorth = defaultSelector.data('worth');
			var price = defaultPrice, worth = defaultWorth;
			var weight = 0;
			var thisWorth = 0;

			$.each(selected, function (key, data) {
				/* weight (fixed part) */
				/* @see: DO NOT CHANGE THIS ORDER */

				if (data.catId === 1) {
					price = data.price;
					worth = data.worth ? data.worth : data.price;
					weight = data.weight;
				}
				/* flavour or eggless */
				else {
					if (data.isFixed === 1) {
						thisWorth = data.worth ? data.worth : data.price;
						price = price + data.price;
						worth = worth + thisWorth;
					}
					else {
						/* calculate based on per kg */
						thisWorth = data.worth ? data.worth : data.price;
						price = price + weight * (data.price);
						worth = worth + weight * thisWorth;
					}
				}
			});

			return {'price': parseInt(price, 10), 'worth': parseInt(worth, 10)};
		}

		/* update calculated prices to the modal */
		function updateCalculatedPrices() {
			var totalPrice = parseInt(updatedProductPrice + transportCharges + deliverySlotPrice, 10);
			var updatedProductPriceOff = parseInt(updatedProductPriceWorth - updatedProductPrice, 10);
			var advanceAmount = parseInt(totalPrice * advancePercent / 100, 10);

			$('.shortlist-modal-price-container .price-worth').text(priceFormat(updatedProductPriceWorth));
			$('.shortlist-modal-price-container .price-off').text(priceFormat(updatedProductPriceOff));
			$('.shortlist-modal-price-container .total-price').text(priceFormat(totalPrice));
			$('.shortlist-modal-price-container .advance-amount').text(priceFormat(advanceAmount));

			if ((transportCharges + deliverySlotPrice) > 0) {
				$('.shortlist-modal-price-container #transportFree').addClass('hide');
				$('.shortlist-modal-price-container #transportPriceContent .transport-price').text(priceFormat(parseInt((transportCharges + deliverySlotPrice), 10)));
				$('.shortlist-modal-price-container #transportPriceContent').removeClass('hide');
			}
			else {
				$('.shortlist-modal-price-container #transportPriceContent').addClass('hide');
				$('.shortlist-modal-price-container #transportFree').removeClass('hide');
			}

			/* update data-price attr of price split elements */
			$('.shortlist-modal-price-container .price-worth').data('price', parseInt(updatedProductPriceWorth, 10));
			$('.shortlist-modal-price-container .price').data('price', parseInt(updatedProductPrice, 10));
			$('.shortlist-modal-price-container .price-off').data('price', parseInt(updatedProductPriceOff, 10));
			$('.shortlist-modal-price-container .transport-price').data('price', parseInt((transportCharges + deliverySlotPrice), 10));
			$('.shortlist-modal-price-container .total-price').data('price', parseInt(totalPrice, 10));
			$('.shortlist-modal-price-container .advance-amount').data('price', parseInt(advanceAmount, 10));
		}

		/* convert numbers to Indian price format */
		function priceFormat($num) {
			$num = $num.toString();
			var lastThree = $num.substring($num.length - 3);
			var otherNumbers = $num.substring(0, $num.length - 3);
			if (otherNumbers != '')
				lastThree = ',' + lastThree;
			var $res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

			return $res;
		}

		/* calculate price at the time of page loading */
		/* @see: do not change the following order */
		calculateCatPrice(getCurrentValue());

		/* check if slot has any delivery charge */
		var slotSelector = $('.shortlist-modal-container').find('#shortlistModalDeliverySlot');
		var slotId = slotSelector.val();
		calculateDeliverySlotPrice(slotId, slotSelector);

		updateCalculatedPrices();

	});
</script>