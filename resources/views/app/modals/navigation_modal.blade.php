<div id="navigationModal" class="modal fade" role="dialog" tabindex="-1">
	<div class="modal-dialog" @if (!$agent->isMobile()) style="width: 400px;" @endif>
		<div class="modal-content" style="position: relative; overflow: hidden;">
			<img src="{{ $galleryUrl }}/main/img/home/confetti-bg.png" alt="confetti background" class="pos-abs" style="height: auto; opacity: 0.3;">
			<h4 class="mar-l-30 mar-r-30 mar-b-20 pos-rel">
				How can we help you?
				<span class="pull-right nav-modal-close-btn">
					<button type="button" class="close font-24" style="border: 2px solid #2D2D2D; border-radius: 50%; width: 28px; opacity: 1; text-shadow: none;">×</button>
				</span>
			</h4>
			<div class="mar-l-15 mar-r-15 mar-t-8 pos-rel">
				<div class="col-xs-12 mar-b-20">
					<div class="cur-point nav-modal-first-section" style="background-color: white; border-radius: 20px; border: 1px solid #FFFFFF; padding: 15px; box-shadow: 3px 3px 5px #595959;">
						<div style="display: flex; align-items: stretch;">
							<div>
								<h6 class="mar-t-10 mar-b-10 text-bold" style="color: #F0677B">I Will Select Myself</h6>
								<p class="font-13"> I am planning a small party at my place, and I have time to go through all the options.</p>
							</div>
							<div style="align-self: center;">
								<span class="pull-right font-40" style="color: #F0677B"><i class="glyphicon glyphicon-chevron-right"></i></span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 mar-b-20">
					<div class="cur-point nav-modal-second-section" style="background-color: white; border-radius: 20px; border: 1px solid #FFFFFF; padding: 15px; box-shadow: 3px 3px 5px #595959;">
						<div style="display: flex; align-items: stretch;">
							<div>
								<h6 class="mar-t-10 mar-b-10 text-bold" style="color: #F0677B">I Need Immediate Help</h6>
								<p class="font-13">I am a little busy and need help to choose the right options. (we will reach you in 2-4 business hours)</p>
							</div>
							<div style="align-self: center;">
								<span class="pull-right font-40" style="color: #F0677B"><i class="glyphicon glyphicon-chevron-right"></i></span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 mar-b-20">
					<div class="cur-point nav-modal-third-section" style="background-color: white; border-radius: 20px; border: 1px solid #FFFFFF; padding: 15px; box-shadow: 3px 3px 5px #595959;">
						<div style="display: flex; align-items: stretch;">
							<div>
								<h6 class="mar-t-10 mar-b-10 text-bold" style="color: #F0677B">I Need Concierge Service</h6>
								<p class="font-13">I need a dedicated Evibe certified expert to plan a high budget, customized party.</p>
							</div>
							<div style="align-self: center;">
								<span class="pull-right font-40" style="color: #F0677B"><i class="glyphicon glyphicon-chevron-right"></i></span>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="mar-20" style="text-align: center;">
				<img src="{{ $galleryUrl }}/main/img/home/google-reviews-240120.png" class="pos-rel" alt="google review card" style="height: 75px; box-shadow: 0 0 1px 0 #a6a6a6;">
				<p class="text-center in-blk mar-l-10 mar-r-10 no-mar-b" style="color: #6F6F6F; display: block;">India's most trusted party planners</p>
			</div>
		</div>
	</div>
</div>