<div>
	<input type="hidden" id="popUpCityId" value="{{ $cityId }}">
	<input type="hidden" id="popUpEventId" value="{{ $occasionId }}">
	<input type="hidden" id="popUpPageId" value="{{ $mapTypeId }}">
	<input type="hidden" id="popUpOptionId" value="{{ $mapId }}">
	<input type="hidden" id="popUpOptionName" value="{{ $optionName }}">
	<input type="hidden" id="popUpUrl" value="{{ url()->current() }}">
	@if(isset($isInspirationsPageEnquiry) && $isInspirationsPageEnquiry)
		<input type="hidden" id="isInspirationsPageEnquiry" value="1">
	@else
		<input type="hidden" id="isInspirationsPageEnquiry" value="0">
	@endif 
	@if(isset($isCampaignPageEnquiry))
		@if($agent->isMobile())
			<input type="hidden" id="isCampaignPageEnquiry" value="23">
		@else
			<input type="hidden" id="isCampaignPageEnquiry" value="24">
		@endif
	@else
		<input type="hidden" id="isCampaignPageEnquiry" value="0">
	@endif
</div>

@if ($agent->isMobile() && !($agent->isTablet()))
	<div class="popup-profile-enquire-button hide mar-t-10 text-center">
		<span>Have Any Questions? </span>
		<a class="btn btn-link product-btn-availability product-btn-enquire-now popup-need-help-btn" data-loaded="1">
			Enquire Now
		</a>
	</div>
@else
	<div class="popup-profile-enquire-button hide enquiry-card-wrap no-pad-b text-center">
		<div class="font-14">
			<span>Have Any Questions?</span>
			<a class="btn btn-link product-btn-enquire-now popup-need-help-btn no-pad mar-l-10" data-loaded="1">
				Enquire Now
			</a>
		</div>
		<div class="font-12 pad-t-5 text-center hide">
			<img src="{{ $galleryUrl }}/main/img/icons/whatsapp.png" height="17px" width="17px">
			@if ($agent->isMobile() && !($agent->isTablet()))
				<a href="whatsapp://send?phone=91{{ config("evibe.contact.customer.whatsapp.phone") }}&text=Hello Evibe.in, I am interested in your services for my party." class="whatsapp-wrap mar-t-8 pad-l-5 valign-mid a-grey font-15">
					{{ config("evibe.contact.customer.whatsapp.phone") }}
				</a>
			@else
				<a href="https://web.whatsapp.com/send?phone=91{{ config("evibe.contact.customer.whatsapp.phone") }}&text=Hello Evibe.in, I am interested in your services for my party." target="_blank" class="whatsapp-wrap mar-t-8 pad-l-5 valign-mid a-grey font-15">
					{{ config("evibe.contact.customer.whatsapp.phone") }}
				</a>
			@endif
		</div>
	</div>
@endif

@php
	$profileUrl = $galleryUrl . "/main/img/customer_support_giffy.png";
	$categoryName = !is_null(config("evibe.ticket.type.id." . $mapTypeId)) ? config("evibe.ticket.type.id." . $mapTypeId) : "Option";
	$backGroundImage = $mapId > 0 ? $profileUrl : $galleryUrl . "/main/img/customer_support_giffy.png";
@endphp

@if ($agent->isMobile() && !($agent->isTablet()))
	@if($mapTypeId > 0 && !($mapId > 0))
		<div class="fab-customer-support ga-mobile-fab-customer-support">
			<img src="{{ $galleryUrl }}/main/img/icons/support_solid_white.png" style="height:100%; width: 100%;">
		</div>
	@endif
@else
	<div class="fab-auto-popup">
		<div class="auto-popup-fab-callout">Need Any Help?</div>
		<img src="{{ $galleryUrl }}/main/img/icons/giffy.png" width="45" height="63">
		<i class="glyphicon glyphicon-envelope"></i>
	</div>
@endif

<div id="autoPopupActionButtonWrap" class="text-center hide">
	<img src="{{ $galleryUrl }}/main/img/icons/help_customer_icon_giffy.png" width="85" height="85">
	<div class="popup-header-text-wrap">
		<button type="button" class="close need-help-popup-no-mobile">×</button>
		@if($mapId > 0)
			<h5 class="mar-t-10 lh-12e">Do you need any help with this {{ $categoryName }}?</h5>
		@else
			<h5 class="mar-t-10 lh-12e">Are you unable to find the right {{ $categoryName }}?</h5>
		@endif
	</div>
	<div class="clearfix"></div>
	<div>
		<div class="btn-success text-bold font-16 pad-10 need-help-popup-yes">Yes, I need help</div>
		<div class="btn-link in-blk font-14 pad-t-5 need-help-popup-no">@if($mapId > 0){{ "No thanks, I will check" }}@else{{ "No, I will choose myself" }}@endif</div>
	</div>
</div>

{{-- @php $navigationModalClose = \Illuminate\Support\Facades\Cookie::get('navigationModalClose'); @endphp
@if($occasionId == config("evibe.occasion.kids_birthdays.id") && $navigationModalClose != 2 && (!strpos(request()->url(), "/u/")))
	@include("app.modals.navigation_modal")
@endif --}}

<div id="modalAutoPopup" class="modal fade" role="dialog" tabindex="-1">
	<div class="modal-dialog modal-lg">
		<div class="modal-content mar-r-20">
			<div class="col-md-5 col-lg-5 no-pad hidden-sm hidden-xs">
				<div class="pc-left-side pc-left-side-spl-exps no-pad">
					<h4 class="show-popup-image-header hide">Schedule Call With Our Expert
						@if(isset($occasionId) && ($occasionId == config('evibe.occasion.kids_birthdays.id'))) Birthday
						@elseif(isset($occasionId) && ($occasionId == config('evibe.occasion.house-warming.id'))) HouseWarming
						@endif planner
					</h4>
					<img class="full-width" src="{{ $backGroundImage }}" style="border-radius: 5px; height: 556px;">
					<div class="auto-popup-bg-overlay"></div>
				</div>
			</div>
			<div class="col-sm-12 col-md-7 col-lg-7 no-pad">
				<div class="modal-header no-border text-center">
					<button type="button" data-dismiss="modal" class="close popup-modal-close-btn">×</button>
					<div class="mar-t-10">
						<div class="font-24 fw-500">Submit Your Enquiry</div>
						<div class="font-14 lh-18 mar-t-15">Our party planning expert will reach you in 2 - 4 business hours.</div>
					</div>
				</div>
				<img class="full-width hidden-md hidden-lg popup-header-image-mobile hide" src="{{ $galleryUrl }}/main/img/customer_support_mobile_giffy.png" style="border-radius: 5px">
				<h4 class="hidden-md hidden-lg pos-abs popup-header-mobile hide">Schedule Call With Our Expert Planner</h4>
				<div class="modal-body" style="border-bottom-right-radius: 5px; border-bottom-left-radius: 5px;">
					<div class="show-popup-submit-details">
						<div class="show-popup-header text-center">
							<div class="hide">
								@if($mapId)
									<h4 class="no-mar-t">How can we help you?</h4>
								@else
									<h5>Let us help you choose the perfect option for you.</h5>
								@endif
							</div>

						</div>
						<div class="sticky-error-wrap sticky-error-wrap-popup hide">
							<span class="error-message">Please fill all the required details.</span>
						</div>
						<div class="form-group">
							<div class="col-md-8 col-md-offset-2 col-sm-12">
								<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no-pad">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<select class="mdl-textfield__input mar-b-5" id="popUpEnquiryTitle" name="popUpEnquiryTitle" style="height: 29px;">
											<option></option>
											<option value="1">Mr</option>
											<option value="2">Ms</option>
										</select>
										<label class="mdl-textfield__label" for="popUpEnquiryTitle">Title</label>
									</div>
								</div>
								<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 no-pad-r">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
										<input class="mdl-textfield__input mar-b-5" type="text" name="popUpEnquiryName" id="popUpEnquiryName" value=""/>
										<label class="mdl-textfield__label" for="popUpEnquiryName">Name</label>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="col-md-8 col-md-offset-2 col-sm-12">
								<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 no-pad">
									@if(isset($countries) && count($countries))
										<select id="popUpEnquiryCallingCode" name="popUpEnquiryCallingCode" class="form-control country-calling-code-popup-modal">
											@foreach($countries as $country)
												<option value="{{ $country->calling_code }}">{{ $country->calling_code }}</option>
											@endforeach
										</select>
									@else
										<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
											<input class="mdl-textfield__input mar-b-5" type="text" name="popUpEnquiryCallingCode" id="popUpEnquiryCallingCode" value="+91"/>
											<label class="mdl-textfield__label" for="popUpEnquiryCallingCode">Code</label>
										</div>
									@endif
								</div>
								<div class="col-md-8 col-lg-8 col-sm-8 col-xs-8 no-pad-r">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
										<input class="mdl-textfield__input mar-b-5" type="text" name="popUpEnquiryPhone" id="popUpEnquiryPhone" value=""/>
										<label class="mdl-textfield__label" for="popUpEnquiryPhone">Phone Number</label>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="col-md-8 col-md-offset-2 col-sm-12 @if(!(isset($isInspirationsPageEnquiry) && $isInspirationsPageEnquiry)) hide @endif">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
									<input class="mdl-textfield__input mar-b-5 email-typo-error" type="text" name="popUpEnquiryEmail" id="popUpEnquiryEmail" value=""/>
									<label class="mdl-textfield__label" for="popUpEnquiryEmail">Email</label>
								</div>
							</div>
							<div class="col-md-8 col-md-offset-2 col-sm-12 @if(isset($isInspirationsPageEnquiry) && $isInspirationsPageEnquiry) hide @endif">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
									<input class="mdl-textfield__input mar-b-5" type="text" name="popUpEnquiryDate" id="popUpEnquiryDate" value=""/>
									<label class="mdl-textfield__label" for="popUpEnquiryDate">Party Date</label>
								</div>
							</div>
							<div class="col-md-8 col-md-offset-2 col-sm-12 text-left">
								<label class="fw-400" for="popUpPlanningStatus">Your Requirements
									<small> (optional)</small>
								</label>
								<textarea id="autoPopupEnquiryFormComments" name="autoPopupEnquiryFormComments" class="form-control"></textarea>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="text-center">
							<div class="btn btn-evibe font-16 mar-t-5 popup-submit-option-btn" data-url="{{ route("auto.popup.save.data") }}">
								<span>SUBMIT</span>
							</div>
							<div class="btn btn-evibe font-16 mar-t-5 submitting-auto-popup hide">
								<span>Submitting....</span>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-body show-popup-thank-you pad-5 text-center hide">
					<img src="{{ $galleryUrl }}/main/img/icons/hi_five.png" height="80px" width="80px" alt="success icon">
					<h6 class="main-content text-center"></h6>
					<div class="text-center">
						<div class="btn btn-evibe auto-popup-continue-browsing font-16 mar-t-20" data-dismiss="modal">
							<span>CONTINUE BROWSING</span>
						</div>
					</div>
				</div>
				<div class="popup-keypoints hidden-sm hidden-xs">
					<div class="text-center">
						<i class="material-icons">verified_user</i>
						<span class="in-blk valign-top mar-t-3 pad-l-5">
							100% service delivery guarentee
						</span>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {
			var mapId = $('#popUpOptionId').val();
			var mapTypeId = parseInt($('#popUpPageId').val(), 10);
			var openedPopup = 1;
			var biddingModal = $('#modalAutoPopup');
			var label = "";
			var price = 0;
			var isNavModal = 0;
			if ($("#hidProductCode").length) {
				label = $("#hidProductCode").val();
			}
			if ($("#hidProductPrice").length) {
				price = $("#hidProductPrice").val();
			}
			var startTime = new Date();
			var seconds = 0;

			(function initFunctions() {
				setInterval(function () {
					$(".fab-auto-popup").css("animation", "");
					setTimeout(function () {
						$(".fab-auto-popup").css("animation", "shake 0.82s cubic-bezier(.36, .07, .19, .97) both");
					}, 1000);
				}, 14000);

				setTimeout(function () {
					$(".auto-popup-fab-callout").css({"opacity": "0"});
				}, 3000);

				let navigationModalCookie = parseInt(Cookies.get('navigationModalClose'), 10);
				if (($('.modal.in').length === 0) && (navigationModalCookie !== 2)) {
					$("#navigationModal").modal({
						'show': true,
						'backdrop': 'static',
						'keyboard': false
					});
				}

				$(".need-help-popup-yes").css("cursor", "pointer");
				$(".need-help-popup-no").css("cursor", "pointer");
				$(".need-help-popup-no-mobile").css("cursor", "pointer");
				$("#popUpEnquiryDate").datetimepicker({
					"timepicker": false,
					"minDate": 0,
					"format": 'Y/m/d',
					"scrollInput": false,
					"scrollMonth": false,
					"scrollTime": false,
					"closeOnDateSelect": true,
					"disabledDates": window.disableDates(),
					onSelectDate: function (ct, $i) {
						$i.parent().addClass("is-dirty");
					}
				});
			})();

			(function navigationModalTriggers() {
				$(".nav-modal-first-section").on("click", function () {
					closeNavigationModal();
				});

				$(".nav-modal-close-btn").on("click", function () {
					closeNavigationModal();
				});

				$(".nav-modal-second-section").on("click", function () {
					closeNavigationModal();
					isNavModal = 2;
					biddingModal.modal({
						'show': true,
						'backdrop': 'static',
						'keyboard': false
					});
				});

				$(".nav-modal-third-section").on("click", function () {
					closeNavigationModal();
					isNavModal = 3;
					biddingModal.modal({
						'show': true,
						'backdrop': 'static',
						'keyboard': false
					});
				});
			})();

			(function fabButtonTriggers() {
				$(".fab-auto-popup").on("click", function () {
					if ($(".fab-auto-popup").length > 0) {
						$(".fab-auto-popup").addClass("hide");
					} else {
						$(".fab-auto-popup-call").addClass("hide");
					}

					isNavModal = 0;
					biddingModal.modal({
						'show': true,
						'backdrop': 'static',
						'keyboard': false
					});
				});

				$(".fab-auto-popup-call").on("click", function () {
					$(this).addClass("hide");
					$("#modalAutoPopupCall").modal({
						'show': true,
						'backdrop': 'static',
						'keyboard': false
					});
				});

				$(".btn-request-call").on("click", function () {
					$(".fab-auto-popup-call").addClass("hide");

					$("#modalAutoPopupCall").modal("hide");

					isNavModal = 0;
					biddingModal.modal({
						'show': true,
						'backdrop': 'static',
						'keyboard': false
					});
				});

				$(".fab-customer-support").on("click", function () {
					isNavModal = 0;
					biddingModal.modal({
						'show': true,
						'backdrop': 'static',
						'keyboard': false
					});
				});
			})();

			(function formActionButtons() {
				$('.popup-modal-close-btn').on('click', function () {
					if ($(".fab-auto-popup").length > 0) {
						$(".fab-auto-popup").removeClass("hide");
					} else {
						$(".fab-auto-popup-call").removeClass("hide");
					}
					biddingModal.modal({
						'show': false
					});
				});

				$('.popup-call-modal-close-btn').on('click', function () {
					$(".fab-auto-popup-call").removeClass("hide");
					biddingModal.modal({
						'show': false
					});
				});

				$('.auto-popup-continue-browsing').on('click', function () {
					location.reload();
				})
			})();

			(function needHelpButtonCLick() {
				$('.popup-need-help-btn').on("click", function (event) {
					event.preventDefault();
					openedPopup = 2;
					if ($(".fab-auto-popup").length > 0) {
						$(".fab-auto-popup").addClass("hide");
					} else {
						$(".fab-auto-popup-call").addClass("hide");
					}
					$("#autoPopupActionButtonWrap").css("transform", "translateY(150%)");

					isNavModal = 0;
					biddingModal.modal({
						'show': true,
						'backdrop': 'static',
						'keyboard': false
					});
				});
			})();

			(function showAutoPopUp() {
				let submitCookie = parseInt(Cookies.get('autoPopUpSubmit'), 10);
				let pageCookie = parseInt(Cookies.get('autoPopUpClose'), 10);
				let popUpTriggerTime = {"1": "20000", "2": "100000"};

				if (mapId) {
					popUpTriggerTime = {"1": "25000", "2": "100000"};
				}

				$.each(popUpTriggerTime, function (key, value) {
					setTimeout(function () {
						let currentUrl = window.location.href;
						if (currentUrl.search("/bachelor-party") !== -1) {
							return false;
						}

						if (($('.modal.in').length === 0) && (parseInt(openedPopup, 10) === 1) && (submitCookie !== 2)) {
							if (pageCookie !== 2) {
								if ((seconds + 20000) < 90000) {
									if ($(".fab-auto-popup").length > 0) {
										$(".fab-auto-popup").addClass("hide");
									} else {
										$(".fab-auto-popup-call").addClass("hide");
									}
									$("#autoPopupActionButtonWrap").css("transform", "translateY(0%)");
								}
							}
						}

						$(".need-help-popup-yes").on("click", function () {
							$("#autoPopupActionButtonWrap").css("transform", "translateY(150%)");
							if ($(".fab-auto-popup").length > 0) {
								$(".fab-auto-popup").addClass("hide");
							} else {
								$(".fab-auto-popup-call").addClass("hide");
							}

							isNavModal = 0;
							biddingModal.modal({
								'show': true,
								'backdrop': 'static',
								'keyboard': false
							});
						});

						$(".need-help-popup-no, .need-help-popup-no-mobile").on("click", function () {
							$("#autoPopupActionButtonWrap").css("transform", "translateY(150%)");
							if ($(".fab-auto-popup").length > 0) {
								$(".fab-auto-popup").removeClass("hide");
							} else {
								$(".fab-auto-popup-call").removeClass("hide");
							}
							var endTime = new Date();
							var timeDiff = endTime - startTime;
							seconds = Math.round(timeDiff);

							let endOfTheDay = new Date(new Date().setHours(23, 59, 59, 999));
							Cookies.set("autoPopUpClose", '2', {expires: endOfTheDay});
						});
					}, value);
				});
			})();

			(function saveAutoPopUpFormDetails() {
				$('.popup-submit-option-btn').on('click', function (e) {
					e.preventDefault();
					disableSubmitButton();

					let formUrl = $(this).data('url');
					let urlParams = window.getUrlParameters();
					let data = {
						'name': $('#popUpEnquiryName').val(),
						'title': $('#popUpEnquiryTitle').val(),
						'phone': $('#popUpEnquiryPhone').val(),
						'callingCode': $('#popUpEnquiryCallingCode').val(),
						'email': $('#popUpEnquiryEmail').val(),
						'date': $('#popUpEnquiryDate').val(),
						'timeSlot': $('#popUpEnquiryTimeSlot :selected').val(),
						'planningStatus': $('#popUpPlanningStatus :selected').val(),
						'mapTypeId': $('#popUpPageId').val(),
						'mapId': $('#popUpOptionId').val(),
						'eventId': $('#popUpEventId').val(),
						'cityId': $('#popUpCityId').val(),
						'isEnquiry': openedPopup,
						'requirements': $('#autoPopupEnquiryFormComments').val(),
						'userPageUrl': window.location.href,
						'surprisesTheatreCity': $(".landing-surprises-City").find(":selected").text(),
						'surprisesTheatreName': $(".landing-surprises-Theatre").find(":selected").text(),
						'sourceId': urlParams["refsrc"],
						'isCampaignPageEnquiry': $("#isCampaignPageEnquiry").val(),
						'isInspirationsPageEnquiry': $("#isInspirationsPageEnquiry").val(),
						'isNavModal': isNavModal
					};

					/* add-ons */
					var $addOn = $('.add-on-cta-list');

					if ($addOn.length > 0) {

						var addOns = [];
						$.each($addOn, function (key, value) {
							if ($(this).data('count-units') > 0) {
								addOns[$(this).data('id')] = $(this).data('count-units');
							}
						});

						data['addOns'] = addOns;
					}

					$.ajax({
						url: formUrl,
						type: "POST",
						data: data,
						success: function (data) {
							if (data.success) {
								if (data.redirectTo) {
								    window.location = data.redirectTo;
								} else {
									$('.show-popup-header').addClass('hide');
									$('.show-popup-submit-details').addClass('hide');
									$('.popup-header-mobile').addClass('hide');
									$('.popup-header-image-mobile').addClass('hide');
									$('.show-popup-thank-you').removeClass('hide').find('.main-content').text("Awesome, " + $('#popUpEnquiryName').val() + ". Our expert will reach you as per your request. We can’t wait to talk to you :\)");
								}
							} else if (data.error) {
								showSubmitButton();
								showNotyError(data.error);
							}
						},
						error: function (jqXHR, textStatus, errorThrown) {
							showSubmitButton();
							window.showNotyError("An error occurred while submitting your request. Please try again later.");
							window.notifyTeam({
								"url": formUrl,
								"textStatus": textStatus,
								"errorThrown": errorThrown
							});
						}
					});
				});
			})();

			function closeNavigationModal() {
				var today = new Date();
				var resultDate = new Date(today);
				resultDate.setDate(today.getDate() + 60);

				$("#navigationModal").modal("hide");
				Cookies.set("navigationModalClose", '2', {expires: resultDate});
			}

			function showSubmitButton() {
				$('.popup-submit-option-btn').removeClass('hide');
				$('.submitting-auto-popup').addClass('hide');
			}

			function disableSubmitButton() {
				$('.popup-submit-option-btn').addClass('hide');
				$('.submitting-auto-popup').removeClass('hide');
			}
		});
	</script>
@endsection