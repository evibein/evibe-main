<div class="modal fade modal-custom-form modal-job-application" tabindex="-1" role="dialog" id="modalJobApplication">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="modal-inner-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">&times;</span></button>
					<h4 class="modal-title text-center">Be a part of Evibe.in</h4>
				</div>
				<form id="fromJobApplication" action="{{ $url }}" class="form-job-application">
					<div class="sticky-error-wrap hide">
						<span class="error-message">Please fill all the required details.</span>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-lg-6 col-sm-12">
							<label class="" for="name">Name</label>
							<input class="form-control" type="text" name="name" id="name" value="{{ old('name') }}"/>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-12">
							<label class="" for="email">Email</label>
							<input class="form-control" type="text" name="email" id="email" value="{{ old('email') }}"/>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<div class="col-md-12 col-lg-12 col-sm-12">
							<label for="options" class="custom-mdl-label">Your Interest</label>
							<select class="form-control abt-job-interest-options" name="options">
								<option value="Technology">Technology</option>
								<option value="Customer Relationship">Customer Relationship</option>
								<option value="Business Development">Business Development</option>
								<option value="Operations">Operations</option>
								<option value="Offline Marketing">Offline Marketing</option>
								<option value="Digital Marketing">Digital Marketing</option>
								<option value="Accounts">Accounts</option>
								<option value="Other">Other</option>
							</select>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<div class="col-md-12 col-lg-12 col-sm-12">
							<label class="" for="comment">Why join us?</label>
							<textarea class="form-control" name="comment" id="comment">{{ old('comment') }}</textarea>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<div class="col-md-12 col-lg-12 col-sm-12">
							<div class="image-upload-wrap pad-10">
								<div>Upload resume (max: 2 MB)</div>
								<div class="file">
									<input type="file" name="resume"/>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<div class="col-sm-12 text-center">
							<button id="btnJobApplicationSubmit" type="submit" class="btn btn-primary btn-submit">
								<div class="default">
									<i class="material-icons valign-mid">&#xE2C6;</i>
									<span class="valign-mid">Submit</span>
								</div>
								<div class="waiting hide">
									<i class="material-icons valign-mid">&#xE2C6;</i>
									<span class="valign-mid">Submitting..</span>
								</div>
							</button>
						</div>
						<div class="clearfix"></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<section class="modal fade" id="resumeFormSuccessModal" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 class="modal-title">Thank you :-)</h3>
			</div>
			<div class="modal-body">
					<span class="msg msg-gen">
						<p>Your application has been submitted successfully. We will get
							back to you shortly.
						</p>
					</span>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary pull-right" data-dismiss="modal" aria-hidden="true">Okay
				</button>
			</div>
		</div>
	</div>
</section>

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			$('.abt-hire-btn').click(function (event) {
				event.preventDefault();
				$("#fromJobApplication")[0].reset(); /* clear the form data */

				/* open the modal */
				var biddingModal = $('#modalJobApplication');
				biddingModal.find('.sticky-error-wrap').addClass('hide');
				biddingModal.modal('show');
			});

			/* submit the form */
			$('#fromJobApplication').submit(function (e) {
				e.preventDefault();

				var btnSubmit = $('#btnJobApplicationSubmit');

				function hideSubmitButton() {
					btnSubmit.attr('disabled', true);
					btnSubmit.find('.default').addClass('hide');
					btnSubmit.find('.waiting').removeClass('hide');
				}

				function showSubmitButton() {
					btnSubmit.attr('disabled', false);
					btnSubmit.find('.default').removeClass('hide');
					btnSubmit.find('.waiting').addClass('hide');
				}

				var formUrl = $(this).attr('action');
				hideSubmitButton();

				$.ajax({
					url: formUrl,
					type: "POST",
					data: new FormData(this),
					cache: false,
					processData: false,
					contentType: false,
					success: function (data) {
						showSubmitButton();
						if (data.success) {
							$('#modalJobApplication').modal('hide');
							$('#resumeFormSuccessModal').modal('show');
						} else if (data.error) {
							$('#modalJobApplication').modal('show');
							window.showNotyError(data.error);
						} else {
							$('#modalJobApplication').modal('show');
							window.showNotyError("Some error occurred while submitting your request")
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						showSubmitButton();
						window.showNotyError("Some error occurred while submitting your request");
						window.notifyTeam({
							"url": url,
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});

			});
		});
	</script>
@endsection