<section class="modal fade reco-rating-feedback-modal no-pad" id="recoRatingFeedbackModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body text-center">
				<div class="pad-b-15 reco-rate-head">How do you rate the recommendations?</div>
				<div class="reco-rating">
					<input type="text" id="recoRating" value="" class="form-control reco-rating-field"/>
				</div>
				<div class="reco-rate-info">
					<i class="glyphicon glyphicon-info-sign"></i> Your feedback will help us improve the recommendations.
				</div>
				<div class="reco-rat-submit-btns">
					<div class="reco-rat-submit pull-right">
						<div class="btn rat-submit-btn">
							Submit
						</div>
					</div>
					<div class="reco-rat-skip-wrap pull-right">
						<button type="button" class="btn btn-link reco-rat-skip no-pad">I don't need better recommendations</button>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
</section>