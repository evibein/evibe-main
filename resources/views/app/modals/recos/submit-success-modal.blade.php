<div class="modal fade modal-reco-success" id="modalRecoSuccess" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title no-pab-b">Thank You, {{ $customerName }}</h4>
			</div>
			<div class="modal-body font-16">
				<div class="pad-t-15">
					We've received your selected choices. Thanks for choosing the next step. We will proceed accordingly. You may close this window now.
				</div>
				<div class="pad-t-15 pad-b-15">
					<span>In case of any queries, please reach us at</span>
					<b> {{ config('evibe.contact.company.phone') }}</b> ({{ config('evibe.contact.company.working.start_time') }} - {{ config('evibe.contact.company.working.end_time') }}) or email us at
					<b> {{ config('evibe.contact.company.email') }}</b>.
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>