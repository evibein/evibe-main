<section class="modal reco-next-steps-modal no-pad" id="recoNextStepsModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="text-center no-mar-t mar-b-20">Next steps</h4>
				<div class="reco-success-content">
					<div class="font-20">
						<h5>Kindly explain the modifications you required. Our representative will reach you </h5>
					</div>
					<div class="reco-options mar-t-10 font-14 hide">
						<div>
							<input type="radio" id="1" class="custom-radio-animation" name="recoSuccessOption" value="{{ config('evibe.reco-next-steps.1') }}">
							<label for="1" class="custom-radio-animation-label">{{ config('evibe.reco-next-steps.1') }}</label>
						</div>
						<div>
							<input type="radio" id="2" class="custom-radio-animation" name="recoSuccessOption" value="{{ config('evibe.reco-next-steps.2') }}">
							<label for="2" class="custom-radio-animation-label">{{ config('evibe.reco-next-steps.2') }}</label>
						</div>
						<div>
							<input type="radio" id="3" class="custom-radio-animation" name="recoSuccessOption" value="{{ config('evibe.reco-next-steps.3') }}" checked>
							<label for="3" class="custom-radio-animation-label">{{ config('evibe.reco-next-steps.3') }}</label>
						</div>
					</div>
					<div class="reco-answer mar-t-10">
						<div class="form-group">
							<label for="recoSuccessText" class="custom-mdl-label font-16">Your message
								<small> (optional)</small>
							</label>
							<textarea id="recoSuccessText" name="recoSuccessText" class="form-control" rows="3"></textarea>
						</div>
					</div>
				</div>
				<div class="reco-fail-content">
					<div class="font-20">
						<h6><b>What did you expect but not there on recommendations?*(It will help us to improve)</b></h6>
					</div>
					<div class="reco-options mar-t-10 font-14 hide">
						<div>
							<input type="radio" id="4" class="custom-radio-animation" name="recoFailOption" value="{{ config('evibe.reco-next-steps.4') }}">
							<label for="4" class="custom-radio-animation-label">{{ config('evibe.reco-next-steps.4') }}</label>
						</div>
						<div>
							<input type="radio" id="5" class="custom-radio-animation" name="recoFailOption" value="{{ config('evibe.reco-next-steps.5') }}">
							<label for="5" class="custom-radio-animation-label">{{ config('evibe.reco-next-steps.5') }}</label>
						</div>
						<div>
							<input type="radio" id="6" class="custom-radio-animation" name="recoFailOption" value="{{ config('evibe.reco-next-steps.6') }}" checked>
							<label for="6" class="custom-radio-animation-label">{{ config('evibe.reco-next-steps.6') }}</label>
						</div>
					</div>
					<div class="reco-answer mar-t-20">
						<div class="form-group">
							<label for="recoFailText" class="custom-mdl-label font-16">Your message
								<small> (optional)</small>
							</label>
							<textarea id="recoFailText" name="recoFailText" class="form-control" rows="3"></textarea>
						</div>
					</div>
				</div>
				<div class="reco-success-content text-center mar-t-20">
					<button class="btn btn-evibe btn-next-steps-submit" data-url="{{ route('customer.recommendation.next-steps', [$ticketId]) }}">Submit</button>
				</div>
				<div class="reco-fail-content text-center mar-t-20">
					<button class="btn btn-evibe btn-new-reco-submit" data-url="{{ route('customer.recommendation.new-recos', [$ticketId]) }}">Submit</button>
				</div>
			</div>
		</div>
	</div>
</section>