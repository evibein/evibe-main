<section class="modal modal-login fade mar-10" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModal" aria-hidden="true">
	<div class="modal-dialog no-mar-400-600 modal-lg">
		<div class="modal-content no-border">
			<div class="modal-body no-pad">
				<div class="col-md-5 no-pad no-mar hidden-sm hidden-xs " style="height:530px;">
					<img src="{{$galleryUrl}}/main/img/1.jpg" style="position: absolute;width:100%;height: 100%;object-fit: cover">
					<div class="pc-left-side" style="vertical-align: middle">
						<div class="login-modal-pc-bg-overlay"></div>
						<ul class="ls-none no-mar login-modal-content">
							<li>
								<div class="bs-title">
									<span class="mar-t-3"><i class="material-icons">&#xE161;</i></span>
									<span> Save your shortlists</span>
								</div>
							</li>
							<li>
								<div class="bs-title">
									<i class="material-icons">&#xE913;</i> Enquire at once
								</div>
							</li>
							<li>
								<div class="bs-title">
									<i class="material-icons">&#xE8B3;</i> Track your orders
								</div>
							</li>
							<li>
								<div class="bs-title">
									<i class="material-icons">&#xE7EF;</i> Share your shortlists*
								</div>
							</li>
							<li>
								<div class="cs-title">
									* coming soon
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-md-7 col-sm-12 col-xs-12 no-pad sign-in-model">
					<div class="login-right-side ">
						<div class="login-title">
							Sign In to Evibe.in
						</div>
						<hr class="hr-text no-mar no-pad">
						<div class=" login_form form-group row pad-t-20">
							<div class="col-sm-12 col-lg-2 col-md-2"></div>
							<div class="col-sm-12 col-lg-8 col-md-8 ">
								<div class="text-center">
									<div class="error-message-sign-in errors-cnt alert alert-danger in-blk hide"></div>
								</div>
								<div class="no-pad-l text-center pad-t-10">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
										<input class="email mdl-textfield__input email-typo-error" type="email" name="email" id="loginPopUpEmail" value=""/>
										<label class="mdl-textfield__label fw-normal no-pad-b no-mar-b" for="loginPopUpEmail" >Your email</label>
									</div>
								</div>
								<div class="no-pad-l text-center mar-n-t-15 ">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
										<input class="mdl-textfield__input password no-pad" type="password" name="password" id="loginPopUpPassword" value=""/ style="padding-bottom: 8px">
										<label class="mdl-textfield__label fw-normal no-pad-b no-mar-b" for="loginPopUpPassword">Password</label>
									</div>
								</div>
								<div class="text-center mar-b-15">
									<button class="btn btn-evibe form-sign-in-button mar-t-10">Sign In</button>
									<div class="pad-t-5 font-12"><span>Don't have account?</span><a class="button-sign-up"> Sign Up</a></div>
								</div>
							</div>
						</div>
						<hr class="hr-text no-mar no-pad" data-content="OR">
						<div class="mar-t-10 text-center">
							<a class="btn btn-default btn-large btn-fb-login text-left" href="{{ route('auth.login', 'facebook') }}">
								<span>
									<img src="{{ $galleryUrl }}/main/img/logo/facebook-signup.png" class="" alt="Signup with facebook"></span>
								<span class="btn-fb-login-txt">Login with Facebook</span>
							</a>
						</div>
						<div class="text-center pad-t-10">
							<a class="btn btn-default btn-large btn-google-login text-left" href="{{ route('auth.login', 'google') }}">
								<span>
									<img src="{{ $galleryUrl }}/main/img/logo/google-signup.png" class="" alt="Signup with google"></span>
								<span class="btn-google-login-txt">Login with Google</span>
							</a>
						</div>
						<div class="login-modal-terms">
							By signing in, I agree to all Evibe.in's
							<a href="{{ route('terms') }}" target="_blank" rel="noopener" class="mar-l-4">terms of service</a>.
						</div>
					</div>
				</div>

				<div class="col-md-7 col-sm-12 col-xs-12 no-pad sign-up-model hide">
					<div class="login-right-side ">
						<div class="login-title">
							Create your Evibe Account
						</div>
						<hr class="hr-text no-pad no-mar">
						<div class=" signUp_form form-group row pad-t-20">
							<div class="col-sm-12 col-lg-8 col-md-8 col-md-offset-2 col-lg-offset-2">
								<div class="text-center">
									<div class="error-message-sign-in errors-cnt alert alert-danger in-blk hide"></div>
								</div>
								<div class="no-pad-l text-center pad-t-10">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
										<input class="sign-up-fullName mdl-textfield__input mar-b-5  " type="text" name="fullName" id="popUpSignupName" value=""/>
										<label class="mdl-textfield__label fw-normal" for="popUpSignupName">Full Name</label>
									</div>
								</div>
								<div class="no-pad-l text-center mar-n-t-15">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width ">
										<input class="sign-up-email mdl-textfield__input mar-b-5  " type="email" name="email" id="popUpSingupEmail" value=""/ >
										<label class="mdl-textfield__label fw-normal" for="popUpSingupEmail">Email</label>
									</div>
								</div>
								<div class="no-pad-l text-center mar-n-t-15">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width ">
										<input class=" sign-up-password mdl-textfield__input mar-b-5 " type="password" name="password" id="popUpSignupPassword" value=""/ >
										<label class="mdl-textfield__label fw-normal" for="popUpSignupPassword">Password</label>
									</div>
								</div>
								<div class="no-pad-l text-center mar-n-t-15">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width ">
										<input class="mdl-textfield__input mar-b-5 sign-up-rePassword" type="password" name="password" id="sign-up-rePassword" value=""/ >
										<label class="mdl-textfield__label fw-normal" for="sign-up-rePassword">Re-Type Password</label>
									</div>
								</div>
								<div class="text-center mar-b-15">
									<button class="btn btn-evibe form-sign-up-button mar-t-10">Sign Up</button>

									<div class="pad-t-5 font-12"><span>Already have an account?<a class="button-sign-in"> LogIn</a></div>
								</div>
								<div class="login-modal-terms">
									By signing up, I agree to all Evibe.in's
									<a href="{{ route('terms') }}" target="_blank" rel="noopener" class="mar-l-4">terms of service</a>.
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</section>
