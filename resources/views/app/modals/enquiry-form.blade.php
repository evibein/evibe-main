<div class="modal fade modal-custom-form modal-enquiry-form" tabindex="-1" role="dialog" id="modalEnquiryForm">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="modal-inner-header">
					<button type="button" class="close close-reset-form ga-btn-close-enquire-now-modal" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title text-center">Post Your Requirements</h4>
				</div>
				<form id="enquiryForm" action="{{ $url }}">
					<input type="hidden" id="hidHeaderEnqUrl" value="{{ $url }}">
					<input type="hidden" id="hidTypeSourceFb" value="{{ config('evibe.ticket.type_source.facebook-ad') }}">
					<div class="sticky-error-wrap sticky-error-wrap-enquiry hide pad-t-20">
						<span class="error-message">Please fill all the required details.</span>
					</div>
					<div class="form-group pad-t-20">
						<div class="col-md-12 col-lg-12 col-sm-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
								<input class="mdl-textfield__input ticket_modal_elements" type="text" name="enquiryName" id="enquiryName" value=""/>
								<label class="mdl-textfield__label" for="enquiryName">Name</label>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-lg-6 col-sm-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ticket_modal_elements">
								<input class="mdl-textfield__input" type="text" name="enquiryPhone" id="enquiryPhone" value=""/>
								<label class="mdl-textfield__label" for="enquiryPhone">Phone Number</label>
							</div>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ticket_modal_elements">
								<input class="mdl-textfield__input email-typo-error" type="text" name="enquiryEmail" id="enquiryEmail" value=""/>
								<label class="mdl-textfield__label" for="enquiryEmail">Email</label>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-lg-6 col-sm-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ticket_modal_elements">
								<input class="mdl-textfield__input" type="text" name="enquiryDate" id="enquiryDate" value=""/>
								<label class="mdl-textfield__label" for="enquiryDate">Party Date</label>
							</div>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ticket_modal_elements">
								<input class="mdl-textfield__input" type="text" name="enquiryComments" id="enquiryComments" value=""/>
								<label class="mdl-textfield__label" for="enquiryComments">Your Requirements</label>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<div class="col-lg-12 col-md-12 col-sm-12">
							<label class="fw-normal">
								{{ Form::checkbox('accepts', '1', false, ['id' => 'accepts']) }}
								<span class="mar-l-2">
									I <span class="text-lower">have read and accept the <a href="{{ route('terms') }}" target="_blank" rel="noopener">terms of service</a>.</span>
								</span>
							</label>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<div class="col-sm-12 text-center">
							<button class="btn btn-link mar-r-5 close-reset-form ga-btn-close-enquire-now-modal" data-dismiss="modal">
								<i class="glyphicon glyphicon-remove"></i> <em>Cancel</em>
							</button>
							<button id="btnSubmitEnq" type="submit" class="btn btn-primary btn-submit mar-l-5">
								<div class="default">
									<span class="glyphicon glyphicon-ok"></span>
									<span class="valign-mid">SUBMIT</span>
								</div>
								<div class="waiting hide">
									<span class="glyphicon glyphicon-ok"></span>
									<span class="valign-mid">SUBMITTING..</span>
								</div>
							</button>
						</div>
						<div class="clearfix"></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>