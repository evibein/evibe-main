<div id="thankYouModal" class="modal thank-you-modal" role="dialog" tabindex="-1">
	<div class="modal-dialog modal-md">
		<div class="modal-content pad-t-30 pad-b-30 text-center">
			<img src="{{$galleryUrl}}/img/icons/tick-icon.png" class="image">
			<h2 class="no-mar hide__400 hide-400-600">Success!</h2>
			<h3 class="no-mar hide unhide__400 unhide__400-600">Success!</h3>
			<h4 class="text-muted hide__400 hide-400-600">Thank You for Signing Up</h4>
			<h5 class="text-muted hide unhide__400 unhide__400-600">Thank You for Signing Up</h5>
			<div class="text-success">Redirecting to website in 3 seconds.</div>
		</div>
	</div>
</div>