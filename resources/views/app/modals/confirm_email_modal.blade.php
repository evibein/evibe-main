<section class="modal confirm-email-modal fade" id="confirmEmailModal" tabindex="-1" role="dialog" aria-labelledby="confirmEmailModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body text-center">
				<div class="pad-b-15 font-18 "> Confirm your email </div>
				<!-- form with one user input and hidden fields -->
				<form action="{{ route('auth.create') }}" method="POST" class="no-mar-b pad-l-10 pad-r-10">
					@if ($errors->count() > 0)
						<div class="errors-cnt alert-danger">{{ $errors->first() }}</div>
					@endif
					<input type="text" class="hide" value="{{ $data['provider']['providerName'] }}" name="providerName" id="providerName">
					<input type="text" class="hide" value="{{ $data['provider']['userName'] }}" name="userName" id="userName">
					<input type="text" class="hide" value="{{ $data['provider']['avatar'] }}" name="userAvatar" id="userAvatar">
					<input type="text" class="hide" value="{{ $data['provider']['userId'] }}" name="userId" id="userId">
					<input type="text" class="hide" value="{{ $data['redirectUrl'] }}" name="redirectUrl" id="redirectUrl">
					<input type="text" class="form-control" placeholder="Enter email" name="userEmail" id="userEmail" value="{{ old('userEmail') }}">
					<div class="pad-t-20 text-right">
						<a href="@if(isset($data['redirectUrl']) && $data['redirectUrl']){{ $data['redirectUrl'] }}@else / @endif" class="skip-now">Skip Now</a>
						<button class="btn btn-default btn-save-email">Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
