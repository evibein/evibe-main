<div class="modal fade modal-custom-form modal-job-application" tabindex="-1" role="dialog" id="modalCorporateEnquiry">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="modal-inner-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">&times;</span></button>
					<h4 class="modal-title text-center">Enquire with us</h4>
				</div>
				<form id="fromCorporateEnquiry" action="{{ $url }}" class="form-job-application">
					<div class="sticky-error-wrap hide">
						<span class="error-message">Please fill all the required details.</span>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-lg-6 col-sm-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
								<input class="mdl-textfield__input" type="text" name="name" id="name" value="{{ old('name') }}"/>
								<label class="mdl-textfield__label" for="name">Name</label>
							</div>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="phone" id="phone" value="{{ old('phone') }}"/>
								<label class="mdl-textfield__label" for="name">Phone</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-lg-6 col-sm-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input email-typo-error" type="text" name="email" id="email" value="{{ old('email') }}"/>
								<label class="mdl-textfield__label" for="email">Email</label>
							</div>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="companyName" id="companyName" value="{{ old('companyName') }}"/>
								<label class="mdl-textfield__label" for="email">Company Name</label>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-lg-6 col-sm-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="date" id="date" value="{{ old('date') }}"/>
								<label class="mdl-textfield__label" for="name">Date</label>
							</div>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<select class="mdl-textfield__input" id="source" name="source">
									@foreach($data["ticketSources"] as $ticketSource)
										<option value="{{ $ticketSource->id }}">{{ $ticketSource->name }}</option>
									@endforeach
								</select>
								<label class="mdl-textfield__label" for="source">Where did you hear about Evibe.in?</label>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<div class="col-md-12 col-lg-12 col-sm-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
									<textarea class="mdl-textfield__input" type="text" name="requirements" id="requirements">{{ old('requirements') }}</textarea>
								<label class="mdl-textfield__label" for="requirements">Requirements</label>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<div class="col-sm-12 text-center">
							<button id="btnCorporateEnquirySubmit" type="submit" class="btn btn-primary btn-submit">
								<div class="default">
									<i class="material-icons valign-mid">&#xE2C6;</i>
									<span class="valign-mid">SUBMIT</span>
								</div>
								<div class="waiting hide">
									<i class="material-icons valign-mid">&#xE2C6;</i>
									<span class="valign-mid">Submitting..</span>
								</div>
							</button>
						</div>
						<div class="clearfix"></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<section class="modal fade" id="resumeFormSuccessModal" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 class="modal-title">Thank you :-)</h3>
			</div>
			<div class="modal-body">
					<span class="msg msg-gen">
						<p>Your application has been submitted successfully. We will get
							back to you shortly.
						</p>
					</span>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary pull-right" data-dismiss="modal" aria-hidden="true">Okay
				</button>
			</div>
		</div>
	</div>
</section>

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			$("#date").datetimepicker({
				timepicker: false,
				minDate: 0,
				format: 'Y/m/d',
				scrollInput: false,
				scrollMonth: false,
				scrollTime: false,
				closeOnDateSelect: true,
				disabledDates: window.disableDates(),
				onSelectDate: function (ct, $i) {
					$i.parent().addClass('is-dirty');
				}
			});

			$('.corporate-enq-btn').on('click', function () {
				$("#fromCorporateEnquiry")[0].reset(); /* clear the form data */

				/* open the modal */
				var biddingModal = $('#modalCorporateEnquiry');
				biddingModal.find('.sticky-error-wrap').addClass('hide');
				biddingModal.modal('show');
			});

			/* submit the form */
			$('#fromCorporateEnquiry').submit(function (e) {
				e.preventDefault();

				var btnSubmit = $('#btnCorporateEnquirySubmit');

				function hideSubmitButton() {
					btnSubmit.attr('disabled', true);
					btnSubmit.find('.default').addClass('hide');
					btnSubmit.find('.waiting').removeClass('hide');
				}

				function showSubmitButton() {
					btnSubmit.attr('disabled', false);
					btnSubmit.find('.default').removeClass('hide');
					btnSubmit.find('.waiting').addClass('hide');
				}

				var formUrl = $(this).attr('action');
				hideSubmitButton();

				$.ajax({
					url: formUrl,
					type: "POST",
					data: new FormData(this),
					cache: false,
					processData: false,
					contentType: false,
					success: function (data) {
						showSubmitButton();
						if (data.success) {
							$('#modalCorporateEnquiry').modal('hide');
							$('#resumeFormSuccessModal')
								.modal('show')
								.find(".msg-gen").text("We have received your enquiry. We will get in touch with your shortly.");
						}
						else if (data.error) {
							var errorObj = $('#modalCorporateEnquiry').find('.sticky-error-wrap').removeClass('hide').find('.error-message');
							errorObj.text(data.error);
						}
						else {
							$('#modalJobApplication').modal('show');
							window.showNotyError("Some error occurred while submitting your request")
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						showSubmitButton();
						window.showNotyError("Some error occurred while submitting your request");
						window.notifyTeam({
							"url": url,
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});

			});
		});
	</script>
@endsection