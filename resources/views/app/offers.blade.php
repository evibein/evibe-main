@extends('layout.base')

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/app/page-about-us.css') }}"/>
@endsection

@section('page-title')
	<title>Offers | Evibe.in</title>
@endsection

@section("header")
	@include('app.header_noncity')
@endsection

@section("footer")
	@include('app.footer_noncity')
@endsection

@section("content")
	<div class="about-wrapper">
		<section>
			<div class="col-xs-12 col-sm-8 col-sm-offset-2 mar-t-30">
				<img src="{{ $galleryUrl }}/img/icons/fcoffer.png" alt="Evibe.in vision" class="abt-vision-img">
			</div>
			<div class="clearfix"></div>
		</section>
		<section>
			<div class="col-xs-12 col-sm-8 col-sm-offset-2 mar-t-30 mar-b-30">
				<div class="fc-terms-conditions-wrap">
					<h5 class="text-center pad-b-20">Terms & Conditions</h5>
					<ul>
						<li>
							Use FreeCharge wallet for payment and get cashback in your FreeCharge wallet
						</li>
						<li>
							Maximum cashback for &#8377;100 for orders above Rs. 4,000.
						</li>
						<li>
							Offer limited to once per user
						</li>
						<li>
							Cashback will be auto-credited to customer's FreeCharge wallet within 24 hours
						</li>
						<li>
							Cashback will be cancelled in case of full refunds or cancellation of orders
						</li>
						<li>
							In case of partial refunds/cancellations, refund amount will be adjusted with the cashback received on initial payment
						</li>
						<li>
							After the refund is initiated, amount will be credited back to wallet within 7-10 days
						</li>
						<li>
							Both Evibe.in & FreeCharge reserve the right to discontinue the offer without any prior notice
						</li>
					</ul>
				</div>
			</div>
			<div class="clearfix"></div>
		</section>
	</div>
@endsection