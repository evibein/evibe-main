@extends('layout.base')

@section('page-title')
	<title>Give Feedback | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="Write to us your feedback (or) suggestions (or) anything you feel about our services."/>
@endsection

@section('meta-keywords')
	<meta name="keywords" content="Evibe.in, feedback evibe.in, give feedback"/>
@endsection

@section('og-title')
	<meta property="og:title" content="Give Feedback | evibe.in"/>
@endsection

@section('og-description')
	<meta property="og:description" content="Write to us your feedback (or) suggestions (or) anything you feel about our services."/>
@endsection

@section('og-url')
	<meta property="og:url" content="https://evibe.in/feedback"/>
@endsection

@section('meta-canonical')
	<link rel="canonical" href="{{ $canonicalUrl }}/feedback"/>
@endsection

@section("header")
	@include('base.home.header.header-home-city')
	<div class="header-border full-width"></div>
@endsection

@section("footer")
	@include('base.home.footer.footer-common')
@endsection

@section("content")
	<article class="bkgnd-grey">
		<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
			<section class="page-sec feedback-sec page-sec-card">
				<div class="sec-head">
					<div class="sec-title-wrap">
						<i class="glyphicon glyphicon-pencil"></i>

						<h1 class="sec-title">Give Us feedback</h1>
					</div>
				</div>
				<div class="sec-body">
					<div class="sub-head @if($success) hide @endif">
						<p>
							We thank you very much for showing interest in giving us feedback.
							We are always on our toes to provide the best service to our customers,
							and this is certainly not possible without your support.
						</p>
					</div>
					@if ($success)
						<p class="success-msg">
							Thanks a ton for your feedback, we really appreciate it :-)
						</p>
						<a href="/" class="btn btn-default">Go To Home page</a>
					@else
						<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12 feedback-form-cnt">
							@if ($errors->count() > 0)
								<div class="errors-cnt alert-danger">
									<ul class="errors-list ls-none">
										<li>{{ $errors->first() }}</li>
									</ul>
								</div>
							@endif
							{{ Form::open([
								'url' => 'feedback',
								'method' => 'POST',
								'class' => 'form-horizontal feedback-form',
								'role' => 'form',
								'id' => 'feedbackForm'
							]) }}

							<div class="form-group">
								<div class="col-lg-2 col-md-2 col-sm-2 no-pad">
									{{ Form::label('feedbackFormName', 'Name', ['class' => 'control-label']) }}
								</div>
								<div class="col-lg-9 col-lg-offset-1 col-md-9 col-md-offset-1 col-sm-9 col-sm-offset-1">
									{{ Form::text('feedbackFormName', old('feedbackFormName') , [
										'placeholder' => 'Enter your name',
										'class' => 'form-control'
									]) }}
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2 col-md-2 col-sm-2 no-pad">
									{{ Form::label('feedbackFormEmail', 'Email', ['class' => 'control-label']) }}
								</div>
								<div class="col-lg-9 col-lg-offset-1 col-md-9 col-md-offset-1 col-sm-9 col-sm-offset-1">
									{{ Form::text('feedbackFormEmail', old('feedbackFormEmail') , [
										'placeholder' => 'myname@example.com',
										'class' => 'form-control'
									]) }}
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2 col-md-2 col-sm-2 no-pad">
									{{ Form::label('feedbackFormPhone', 'Phone', ['class' => 'control-label']) }}
								</div>
								<div class="col-lg-9 col-lg-offset-1 col-md-9 col-md-offset-1 col-sm-9 col-sm-offset-1">
									{{ Form::text('feedbackFormPhone', old('feedbackFormPhone') , [
										'placeholder' => '10 digit phone number',
										'class' => 'form-control'
									]) }}
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-2 col-md-2 col-sm-2 no-pad">
									{{ Form::label('feedbackFormComments', 'Feedback', ['class' => 'control-label']) }}
								</div>
								<div class="col-lg-9 col-lg-offset-1 col-md-9 col-md-offset-1 col-sm-9 col-sm-offset-1">
									{{ Form::textarea('feedbackFormComments', old('feedbackFormComments') , [
										'placeholder' => 'Please write your feedback',
										'class' => 'form-control vertical-resize',
										'rows' => '4',
										'spellcheck' => 'true'
									]) }}
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="form-group">
								<div class="col-lg-9 col-lg-offset-3 col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3">
									{{ Form::input('submit', 'feedbackFormsumbit', 'Submit Feedback', [
										'class' => 'btn btn-evibe',
										'id' => 'feedbackFormsumbit'
									]) }}
								</div>
								<div class="clearfix"></div>
							</div>
							{{ Form::close() }}
						</div>
						<div class="clearfix"></div>
					@endif
				</div>
			</section>
		</div>
		<div class="clearfix"></div>
	</article>
@endsection