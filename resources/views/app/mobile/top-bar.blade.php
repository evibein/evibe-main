<div class="text-center pad-b-6">
	<div class="pad-t-5 pad-l-15 font-12 text-col-wh navbar-links top-strip">
		<div class="in-blk mar-r-15">
			<a href="#" class="select-city">
				<span class="glyphicon glyphicon-map-marker"></span>
				{{ getCityName() }}
			</a>
		</div>
		<div class="in-blk font-12 mar-r-15">
			<span class="glyphicon glyphicon-earphone"></span>{{ config('evibe.contact.company.plain_phone') }}
		</div>
		<div class="in-blk font-12">
			<a href="{{ route("track.orders") }}" class="a-no-decoration">
				<span class="glyphicon glyphicon-screenshot"></span> Track My Order
			</a>
		</div>
		<div class="in-blk">
			@include('app.partybag_header_home')
		</div>
	</div>
</div>
<div class="container no-pad">
	<div class="col-xs-8 col-sm-8">
		<a class="navbar-brand no-pad-t" href="/">
			<img src="{{ $galleryUrl }}/img/logo/logo_evibe_home.png" alt="Evibe.in logo">
		</a>
	</div>
	<div class="col-xs-1 col-sm-1 col-xs-offset-2 col-sm-offset-2">
		@include('app.login_user')
	</div>
	<div class="clearfix"></div>
</div>