<div>
	@if(session()->has('cityId'))
		<div class="mobile-search-btn mar-t-10 hide">
			<div class="mobile-header-search-bar has-feedback full-width" style="z-index: 1;" data-referral="mobile">
				<input id="mobileGlobalSearchInput" type="text" name="q" data-limit="8" class="header-search-bar-input full-width typeahead" placeholder="Search" value="@if((isset($data) && isset($data['query']) && !empty($data['query']))){{ $data['query'] }} @elseif(request("query") && !empty(request("query"))){{ request("query") }}@endif">
				<a href="#" class="header-search-icon-container">
					<i class="glyphicon glyphicon-search header-search-icon mobile-header-search-icon"></i>
				</a>
				<img class="typeahead-spinner" src="{{ $galleryUrl }}/main/img/search/loading-search.gif">
			</div>
			<div id="mobileGlobalSearchOverlay" style="background-color: rgba(0, 0, 0, 0.5); position: fixed; left: 0; top: 0; width: 100%; height: 100%;"></div>
		</div>
	@endif
</div>