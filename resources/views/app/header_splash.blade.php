<header>
	<div class="campaign-header hide"></div>
	<div class="header-top pad-t-10 pad-b-10">
		<div class="container">
			<div class="text-center">
				<div class="logo-cnt valign-mid">
					<a href="/">
						<img src="{{ $galleryUrl }}/img/logo/logo_evibe_splash_header.png" alt="Evibe.in logo" title="Evibe.in" class="splash-img">
					</a>
					<div class="mar-t-10">
						<h1 class="splash-title">LET'S PLAN YOUR PARTY ONLINE</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>