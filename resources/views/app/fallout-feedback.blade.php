@extends('layout.base')

@section('page-title')
	<title>Give Feedback | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="Write to us your feedback (or) suggestions (or) anything you feel for leaving us."/>
@endsection

@section('meta-keywords')
	<meta name="keywords" content="Evibe.in, feedback evibe.in, give feedback"/>
@endsection

@section('og-title')
	<meta property="og:title" content="Give Feedback | evibe.in"/>
@endsection

@section('og-description')
	<meta property="og:description" content="Write to us your feedback (or) suggestions (or) anything you feel for leaving us."/>
@endsection

@section('og-url')
	<meta property="og:url" content="https://evibe.in/fallout-feedback"/>
@endsection

@section('meta-canonical')
	<link rel="canonical" href="{{ $canonicalUrl }}/fallout-feedback"/>
@endsection

@section("header")
	@include('app.header_noncity')
@endsection

@section("footer")
	@include('app.footer_noncity')
@endsection

@section("content")

	@if(isset($invalidThankYou) && $invalidThankYou)
		<div class="col-sm-10 col-md-10 col-lg-10 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
			<div class="fallout-thankyou-wrap">
				<div class="fallout-thankyou-msg text-center">
					<i class="glyphicon glyphicon-ok-circle"></i> Thanks for submitting your valuable feedback.
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	@elseif(isset($retentionUnSubscribe) && $retentionUnSubscribe)
		<div class="col-sm-10 col-md-10 col-lg-10 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
			<div class="fallout-thankyou-wrap mar-t-30">
				<div class="text-center retention-message">
					Thanks for letting us know. We are a bit sad, but nevertheless, we whole-heartedly want you to have a great celebration.
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	@else
		<div class="col-sm-10 col-md-10 col-lg-10 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
			<div class="fallout-section-wrap">
				<div class="fallout-init-message text-center">
					Thanks for letting us know. We are a bit sad about you leaving us, but nevertheless, we whole-heartedly want your party to be a grand success.
				</div>
				<div class="fallout-cta-wrap mar-t-15 text-center">
					<div class="col-sm-10 col-md-10 col-lg-10 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
						<div class="fallout-cta-msg">
							<i class="glyphicon glyphicon-info-sign"></i> Before you go, kindly do this small favour of letting us know the reason for leaving us. Your feedback will help us serve better.
						</div>
						<div role="button" data-toggle="collapse" href="#falloutFeedback" aria-expanded="false" class="btn btn-primary btn-evibe fallout-cta-btn mar-t-10 hide">
							I would like to help
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="fallout-feedback-wrap fallout-section-wrap mar-b-30" id="falloutFeedback">
				<div class="fallout-feedback-text text-center">
					Kindly, select reasons for the drop off and submit your response. If any of your reasons is not mentioned, please use the comments box to state your reason.
				</div>
				<div class="col-sm-10 col-md-10 col-lg-10 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
					<div class="fallout-feedback-options-wrap mar-t-15">
						@foreach($data['options'] as $option)
							<div class="fallout-feedback-option">
								<label>
									<input type="checkbox" name="{{ $option['option_text'] }}" id="{{ $option['id'] }}" class="fallout-option" value="{{ $option['id'] }}" @if(isset($option['submitted']) && $option['submitted']) checked @endif>
									{{ ucfirst($option['option_text']) }}
								</label>
							</div>
						@endforeach
						<div class="fallout-feedback-option">
							<textarea class="fallout-comments" id="falloutComment" placeholder="Your comments please...">@if(isset($data['comments']) && $data['comments']) {{ $data['comments'] }} @endif</textarea>
						</div>
						<input type="hidden" id="falloutPostUrl" value="{{ $data['postUrl'] }}">
						<input type="hidden" id="ticketId" value="{{ $data['ticketId'] }}">
					</div>
					<div class="fallout-feedback-cta text-center mar-t-10">
						<div id="falloutFeedbackSubmit" class="btn btn-warning btn-fallout-feedback">Submit</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="fallout-thankyou-wrap hide">
				<div class="fallout-thankyou-msg text-center">
					<i class="glyphicon glyphicon-ok-circle"></i> Thanks for submitting your valuable feedback.
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	@endif

@endsection

@section("javascript")
	<script type="text/javascript">
		$(document).ready(function () {

			var $falloutSectionWrap = $('.fallout-section-wrap');
			var $falloutThankyouWrap = $('.fallout-thankyou-wrap');

			$('#falloutFeedbackSubmit').click(function (event) {
				event.preventDefault();

				var idSelector = function () {
					return this.id;
				};
				var selectedOptions = $(":checkbox:checked").map(idSelector).get();

				if ((selectedOptions.length === 0) || ($('#falloutComment').lenght === 0)) {
					window.showNotyError("Kindly select any option or write your comments");
					return false;
				}
				/* do ajax */
				$.ajax({
					url: $('#falloutPostUrl').val(),
					type: "POST",
					dataType: "json",
					data: {
						'selectedOptions': selectedOptions,
						'comment': $('#falloutComment').val(),
						'ticketId': $('#ticketId').val()
					},
					success: function (data) {
						if (data.success) {
							$falloutSectionWrap.addClass('hide');
							$falloutThankyouWrap.removeClass('hide');
						}
						else {
							$falloutSectionWrap.addClass('hide');
							$falloutThankyouWrap.removeClass('hide');
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						$falloutSectionWrap.addClass('hide');
						$falloutThankyouWrap.removeClass('hide');
						window.notifyTeam({
							"url": url,
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			});
		});
	</script>
@endsection