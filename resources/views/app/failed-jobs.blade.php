<div style="padding:15px; border:5px solid #f5e8e3">
	<p>
		Hi Team,
	</p>
	<p>
		There is a failed Job, Please go through below details and fix it immediately.
	</p>

	<table border="1" style="border-collapse: collapse; margin-top:10px">
		<tr>
			<td style="padding:8px">
				Project
			</td>
			<td style="padding:8px">
				@if(isset($data['project']))
					{{ $data['project'] }}
				@endif
			</td>
		</tr>
		<tr>
			<td style="padding:8px">
				Exception Code
			</td>
			<td style="padding:8px">
				@if(isset($data['code']))
					{{ $data['code'] }}
				@endif
			</td>
		</tr>
		<tr>
			<td style="padding:8px">
				Message
			</td>
			<td style="padding:8px">
				@if(isset($data['errorTitle']))
					{{ $data['errorTitle'] }}
				@endif
			</td>
		</tr>
		<tr>
			<td style="padding:8px">
				File
			</td>
			<td style="padding:8px">
				@if(isset($data['file']))
					{{ $data['file'] }}
				@endif
			</td>
		</tr>
		<tr>
			<td style="padding:8px">
				Line
			</td>
			<td style="padding:8px">
				@if(isset($data['line']))
					{{ $data['line'] }}
				@endif
			</td>
		</tr>
		<tr>
			<td style="padding:8px">
				Details
			</td>
			<td style="padding:8px">
				@if(isset($data['details']))
					{{ $data['details'] }}
				@endif
			</td>
		</tr>
	</table>
	<div style="margin-top:10px">
		<div>Thanks in Advance</div>
		<div>Team Evibe.in</div>
	</div>
</div>