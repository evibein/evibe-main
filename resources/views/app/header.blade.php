<header>
	<div class="campaign-header hide"></div>
	<div class="header-top pad-t-10 pad-b-10">
		<div class="container">
			<div class="pull-left no-pull__400 text-center__400">
				<div class="logo-cnt valign-mid">
					<a href="/">
						<img src="{{ $galleryUrl }}/img/logo/logo_evibe.png" alt="Evibe.in logo" title="Evibe.in">
					</a>
					<div class="hide hide-400-600 unhide__400 font-18 mar-t-10">
						<i class="glyphicon glyphicon-earphone"></i>
						<span>{{ config('evibe.contact.company.phone') }}</span>
					</div>
				</div>
				<div class="in-blk vr valign-mid hide__400 hide-400-600"></div>
				<div class="in-blk city-holder valign-mid hide__400 hide-400-600">
					<span>{{ getCityName() }}</span>
				</div>
			</div>
			<div class="pull-right no-pull__400 hide__400">
				<ul class="header-top-items">
					<li class="phone-box">
						<span class="hide__400 font-14-400-600">
							<i class="glyphicon glyphicon-earphone"></i>
							<span>{{ config('evibe.contact.company.phone') }}</span>
						</span>
					</li>
					<li>
						<button class="btn btn-default btn-sm btn-post text-upr hide__400 hide-400-600" data-formtype="{{ config('evibe.ticket.type.header') }}">Enquire Now
						</button>
					</li>
					<li>
						@if(hasPartnerUser())
							<a href="{{ route('partner.profile',auth()->user()->short_url) }}?ref=header" class="no-pad-r">
								<i class="glyphicon glyphicon-user"></i> My Profile
							</a>
						@else
							<a href="{{ route('partner.benefits') }}?ref=header" class="btn btn-info btn-sm text-upr hide__400 hide-400-600">Partner
								Sign up</a>
						@endif
					</li>
				</ul>
			</div>
			<div class="clearfix hide__400"></div>
		</div>
	</div>
	<div class="header-bottom">
		<div class="container">
			<div id='cssmenu' class="menu-links">
				<ul>
					<li class="active">
						<a href="{{ route('city.occasion.birthdays.venue_deals.list', getCityUrl()) }}?ref=header">Venues</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.birthdays.decors.list', getCityUrl()) }}?ref=header">Decor Styles</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.birthdays.ent.list', getCityUrl()) }}?ref=header">Entertainment</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.birthdays.packages.list', getCityUrl()) }}?ref=header">Birthday Packages</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.birthdays.trends.list', getCityUrl()) }}?ref=header">Party Trends</a>
					</li>
					<li>
						<a href="{{ route('city.occasion.birthdays.cakes.list', getCityUrl()) }}?ref=header">Cakes</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</header>