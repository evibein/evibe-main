@extends('layout.base')

@section("header")
	@include('app.header_noncity')
@endsection

@section("footer")
	@include('app.footer_noncity')
@endsection

@section("content")
	<section class="fqs-sec">
		<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
			<div class="sec-title">
				<h1>Careers</h1>
			</div>
			<div class="sec-body">

			</div>
		</div>
		<div class="clearfix"></div>
	</section>
@endsection
