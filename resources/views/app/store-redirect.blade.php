@extends('layout.base')

@section('custom-css')
	@parent
	<link rel="stylesheet" href="{{ elixir('css/util/slick.css') }}"/>
	<link rel="stylesheet" href="{{ elixir('css/util/slick-theme.css') }}"/>
	<style type="text/css" rel="stylesheet" media="all">

		.store-redirect-container-wrap {
			border: 1px solid #A8A8A8;
			border-radius: 10px;
			margin: 100px 20px 20px 20px;
			/*display: inline-block;*/
			text-align: center;
			/*padding: 0 130px;*/
		}

		.sr-redirect-wrap {
			margin-top: 70px;
			margin-bottom: 70px;
		}

		.sr-redirect-msg {
			font-size: 20px;
			color: darkorange;
		}

		.sr-logo-wrap {
			height: 100px;
			margin-top: 15px;
		}

		.sr-logo-wrap img {
			height: 100%;
		}

		.sr-redirect-site-text {
			margin-top: 5px;
			font-size: 16px;
			font-weight: 600;
			color: #555555;
		}

		.sr-usp-wrap {
			margin-bottom: 70px;
		}

		.sr-usp-item-wrap {
			text-align: center;
		}

		.sr-usp-img-wrap {
			height: 110px;
		}

		.sr-usp-img-wrap img {
			height: 100%;
		}

		.sr-usp-title {
			margin-top: 15px;
		}

		@media only screen and (max-width: 600px) {

			.store-redirect-container-wrap {
				margin: 15px 0 15px 0;
			}

			.sr-redirect-wrap {
				margin-bottom: 50px;
			}

			.sr-redirect-msg {
				font-size: 18px;
			}

			.sr-redirect-site-text {
				line-height: 22px;
				font-size: 15px;
			}

			.sr-logo-wrap {
				height: 60px;
			}

			.sr-usp-img-wrap {
				height: 70px;
			}

			.sr-usp-carousel div {
				margin: auto;
				left: 0;
				right: 0;
			}

			.sr-usp-carousel .sr-usp-img-wrap img {
				margin: auto;
			}

			.sr-usp-carousel .sr-usp-title {
				margin-top: 15px;
			}

		}

	</style>
@endsection

@section('content')
	<div class="w-1366 text-center">
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2">
			<div class="store-redirect-container-wrap">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
					<div class="sr-redirect-wrap">
						<div class="sr-redirect-msg">You are now being redirected to ...</div>
						<div class="sr-logo-wrap">
							<img src="{{ $galleryUrl }}/main/img/icons/evibe-store-black.png" alt="EvibeStore">
						</div>
						<div class="sr-redirect-site-text">Get Latest Party Supplies Delivered To Your Doorstep</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
					<div class="sr-usp-wrap">
						@if($agent->isMobile() || $agent->isTablet())
							<div class="sr-usp-carousel">
								<div class="text-center">
									<div class="sr-usp-item-wrap hide">
										<div class="sr-usp-img-wrap">
											<img src="{{ $galleryUrl }}/main/img/icons/evibe-store-tag.png">
										</div>
										<div class="sr-usp-title">
											BEST PRICES
										</div>
									</div>
								</div>
								<div class="text-center">
									<div class="sr-usp-item-wrap hide">
										<div class="sr-usp-img-wrap">
											<img src="{{ $galleryUrl }}/main/img/icons/evibe-store-box.png">
										</div>
										<div class="sr-usp-title">
											ALL IN ONE PARTY STORE
										</div>
									</div>
								</div>
								<div class="text-center">
									<div class="sr-usp-item-wrap hide">
										<div class="sr-usp-img-wrap">
											<img src="{{ $galleryUrl }}/main/img/icons/evibe-store-delivery-van.png">
										</div>
										<div class="sr-usp-title">
											FREE DELIVERY
										</div>
									</div>
								</div>
							</div>
						@else
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ">
								<div class="sr-usp-item-wrap">
									<div class="sr-usp-img-wrap">
										<img src="{{ $galleryUrl }}/main/img/icons/evibe-store-tag.png">
									</div>
									<div class="sr-usp-title">
										BEST PRICES
									</div>
								</div>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ">
								<div class="sr-usp-item-wrap">
									<div class="sr-usp-img-wrap">
										<img src="{{ $galleryUrl }}/main/img/icons/evibe-store-box.png">
									</div>
									<div class="sr-usp-title">
										ALL IN ONE PARTY STORE
									</div>
								</div>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ">
								<div class="sr-usp-item-wrap">
									<div class="sr-usp-img-wrap">
										<img src="{{ $galleryUrl }}/main/img/icons/evibe-store-delivery-van.png">
									</div>
									<div class="sr-usp-title">
										FREE DELIVERY
									</div>
								</div>
							</div>
						@endif
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="hide">
		<input type="hidden" id="redirectUrl" value="{{ $redirect }}">
	</div>
@endsection

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			var slickJs = "<?php echo elixir('js/util/slick.js'); ?>";
			$.getScript(slickJs, function () {

				$('.sr-usp-carousel').slick({
					dots: false,
					infinite: true,
					speed: 300,
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
					autoplay: true,
					autoplaySpeed: 1000
				});

				$('.sr-usp-item-wrap').removeClass('hide');

			});

			setTimeout(function () {
				var $redirectUrl = $('#redirectUrl').val();
				if (!$redirectUrl) {
					$redirectUrl = "store.evibe.in"
				}
				window.location.href = $redirectUrl;
			}, 4500);

		});
	</script>
@endsection