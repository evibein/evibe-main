<header>
	<div class="header-top">
		<nav class="navbar navbar-default navbar-home navbar-top trim-navbar-top" role="navigation">
			<!-- desktop -->
			<div class="hide__400 hide-400-600">
				<nav class="navbar-default navbar-home navbar-home-white no-mar-b" role="navigation">
					<div class="in-blk">
						<a class="navbar-brand topnav" href="/">
							<img src="{{ $galleryUrl }}/img/logo/logo_evibe.png" alt="Evibe logo">
						</a>
					</div>
					<div class="in-blk pull-right pad-r-20" id="navbarNav">
						<ul class="nav navbar-nav top-header-list" v-cloak>
							<li class="pad-l-10">
								<a>
									<span class="glyphicon glyphicon-phone"></span>
									<span>{{ config('evibe.contact.invitations.phone') }}</span>
								</a>
							</li>
							<li class="pad-l-10">
								<a>
									<span class="glyphicon glyphicon-envelope"></span>
									<span>{{ config('evibe.contact.invitations.group') }}</span>
								</a>
							</li>
						</ul>
					</div>
				</nav>
				<div class="clearfix"></div>
			</div>

			<!-- for media less than 600px -->
			<div class="hide show-400-600 unhide__400">
				<!-- city and phone number -->

				<!-- Logo and Search bar-->
				<div class="container no-pad text-center">
					<div class="col-xs-12 col-sm-12 mar-t-10 mar-b-10">
						<a href="/">
							<img src="{{ $galleryUrl }}/img/logo/logo_evibe.png" alt="Evibe.in logo">
						</a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</nav>
	</div>
</header>

@section("modals")
	@parent
	@include('app.modals.enquiry-form', [
		"url" => route("city.header.enquire", "no-city")
	])
@endsection