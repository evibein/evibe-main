@extends('layout.base')

@section('custom-css')
	<link rel="stylesheet" href="{{ elixir('/css/page-about-us.css') }}"/>
@endsection

@section('page-title')
	<title>About Us | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="What is Evibe.in? Who are the team members? How to join Evibe.in?"/>
@endsection

@section('meta-keywords')
	<meta name="keywords" content="about Evibe.in, Evibe.in team, join Evibe.in"/>
@endsection

@section('og-title')
	<meta property="og:title" content="About Us | Evibe.in"/>
@endsection

@section('og-description')
	<meta property="og:description" content="What is Evibe.in? Who are the team members? How to join Evibe.in?"/>
@endsection

@section('og-url')
	<meta property="og:url" content="https://evibe.in/about"/>
@endsection

@section('meta-canonical')
	<link rel="canonical" href="{{ $canonicalUrl }}/about"/>
@endsection

@section('press:title')
@endsection

@section("header")
	@include('base.home.header.header-top-city')
@endsection

@section("footer")
	@include('app.footer_noncity')
@endsection

@section("content")
	<div class="about-wrapper">
		<section class="about-sec abt-vision-sec">
			<div class="abt-vision-wrapper">
				<div class="abt-v-img-wrap">
					<img src="{{ $galleryUrl }}/main/pages/about/vision-bg-min.jpg" alt="Evibe.in vision" class="abt-vision-img">
					<div class="bg-overlay abt-v-bg-overlay"></div>
				</div>
				<p class="abt-v-txt-s">
					We are here to
				</p>
				<p class="abt-v-txt-l">
					Enable Billions of Memories
				</p>
			</div>
		</section>
		<section class="about-sec abt-what-headline">
			<div class="abt-w-wrap text-center">
				<h2 class="abt-w-h1">Make Your Special Day More Memorable!</h2>
				<h4 class="abt-w-h2">10,000+ customers trust Evibe.in to make their event planning a breeze. Book party services ranging from venues to cakes, complete hassle-free at best prices and within three simple steps.</h4>
			</div>
		</section>
		<section class="about-sec abt-stats">
			<div class="col-lg-10 col-md-10 col-lg-offset-1 col-md-offset-1 col-xs-offset-2 abt-stats-pad">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="abt-stat-item-wrap media">
						<div class="media-left media-middle">
							<img class="abt-stats-img" src="{{ $galleryUrl }}/main/pages/about/happy.png" alt="fun"/>
						</div>
						<div class="media-body abt-stats-text-l">
							<p class="media-heading">4,00,000+</p>
							<h6>HAPPY GUESTS</h6>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="abt-stat-item-wrap media">
						<div class="media-left media-middle">
							<img class="abt-stats-img" src="{{ $galleryUrl }}/main/pages/about/options.png" alt="fun"/>
						</div>
						<div class="media-body abt-stats-text-l">
							<p class="media-heading">5,000+</p>
							<h6>OPTIONS</h6>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="abt-stat-item-wrap media">
						<div class="media-left media-middle">
							<img class="abt-stats-img" src="{{ $galleryUrl }}/main/pages/about/partners.png" alt="fun"/>
						</div>
						<div class="media-body abt-stats-text-l">
							<p class="media-heading">500+</p>
							<h6>VERIFIED PARTNERS</h6>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="abt-stat-item-wrap media">
						<div class="media-left media-middle">
							<img class="abt-stats-img" src="{{ $galleryUrl }}/main/pages/about/followers.png" alt="fun"/>
						</div>
						<div class="media-body abt-stats-text-l">
							<p class="media-heading">1,50,000+</p>
							<h6>FOLLOWERS</h6>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</section>
		<section class="about-sec abt-story">
			<div class="abt-story-wrap text-left">
				<div class="abt-story-head">
					<h1 class="abt-story-h no-mar-t no-mar-b " style="letter-spacing: normal;">
						Our story
					</h1>
				</div>
				<div class="abt-story-body">
					<p class="abt-story-p">
						It all started during the chill winters in Bangalore when a to-be-weds could not find service providers as per their requirements for their wedding slated to happen in Hyderabad - a problem that should not have existed in the first place with the advancement of technology. The problem resonated with many of their friends & acquaintances, especially the parents who wanted to make it extra special on their child birthday.
					</p>
					<p class="abt-story-p">
						The duo saw the opportunity, took the responsibility to solve the problem and decided to build the largest platform for celebrations in India - one step at a time. We believe we can create a day when anyone who wants to celebrate any occasion - irrespective of size/preferences - it will be just a few taps and done. Everything is taken care of with best of the quality, 100% assurance and of course at the best deals.
					</p>
				</div>
				<blockquote>
					<h5 class="pad-t-10">The more you celebrate your life, the more there is in life to celebrate.</h5>
					<footer>
						Oprah Winfrey
					</footer>
				</blockquote>
			</div>
		</section>
		<section class="about-sec abt-people-story text-center">
			<div class="abt-people-wrap">
				<div class="abt-people-head">
					<h2 class="abt-people-h">
						Our Partners & Customers <img class="img-abt-people-love"
								src="{{ $galleryUrl }}/img/icons/heart.png" alt="love"/> us
					</h2>
				</div>
				<div class="abt-people-body">
					<div class="abt-people-video abt-partner-video col-lg-6 col-md-6 no-pad no-mar">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/2ayJozUZIjg?html5=1"></iframe>
						</div>
						<div class="abt-people-h2">
							<p class="abt-people-text">
								<a class="abt-people-txt-link" href="https://evibe.in/partner/signup" target="_blank" rel="noopener">Sign up with Evibe.in</a>
							</p>
						</div>
					</div>
					<div class="abt-people-video abt-customer-video col-lg-6 col-md-6 no-pad no-mar">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/6lwEXs51qJw?html5=1"></iframe>
						</div>
						<div class="abt-people-h2">
							<p class="abt-people-text">
								<a class="abt-people-txt-link" href="https://evibe.in" target="_blank" rel="noopener">Start planning your party</a>
							</p>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="abt-media-wrap pad-t-10">
				<div class="abt-media-body">
					@include('base.home.press')
				</div>
			</div>
		</section>

		<section class="about-sec abt-team">
			<div class="abt-group-wrap">
				<div class="abt-group-img-wrap">
					<img class="abt-group-img" src="{{ $galleryUrl }}/main/pages/about/team.jpg" alt="Evibe.in Team">
					<div class="bg-overlay abt-group-bg-overlay"></div>
				</div>
				<p class="abt-group-text-l">
					Team Evibers
				</p>
				<p class="abt-group-text-s">
					We are a family of dreamers & doers
				</p>
			</div>
			<div class="abt-basic-set abt-culture-warp text-center">
				<div class="row-2">
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 culture-item">
						<div class="txt-block text-center">
							<img class="" src="{{ $galleryUrl }}/main/pages/about/passion-1.png" width=80px height=80px alt="passionate">
							<h3>Passionate</h3>
							<p>We are just mad at everything about celebrations.</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 culture-item">
						<div class="txt-block text-center">
							<img class="" src="{{ $galleryUrl }}/main/pages/about/responsible-1.png" width=80px height=80px alt="passionate">
							<h3>Responsible</h3>
							<p>We are bigger than our defined roles.</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 culture-item">
						<div class="txt-block text-center">
							<img class="" src="{{ $galleryUrl }}/main/pages/about/impact-1.png" width=80px height=80px alt="passionate">
							<h3>Impact</h3>
							<p>We are born to make an impact.</p>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="row-2">
					<div class="col-lg-8 col-md-8 col-sm-8 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 culture-item">
							<div class="txt-block text-center">
								<img class="" src="{{ $galleryUrl }}/main/pages/about/things-1.png" width=80px height=80px alt="passionate">
								<h3>Get things done</h3>
								<p>Well, we believe actions speaks beyond words, And we live it.</p>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 culture-item">
							<div class="txt-block text-center">
								<img class="" src="{{ $galleryUrl }}/main/pages/about/fun-1.png" width=80px height=80px alt="passionate">
								<h3>Fun</h3>
								<p>Our tiniest success to biggest failure, we celebrate alike.</p>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="abt-team-wrap">
				<div class="abt-team-body text-center">
					@foreach($data['team'] as $member)
						<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 abt-team-img-wrap">
							<img class="img-responsive abt-team-image" src="{{ $galleryUrl }}/img/team/{{ $member->image_url }}" alt="{{ $member->first_name }}">
							<div class="abt-team-text text-center">
								<h4 class="abt-team-imgDescription ">{{ $member->first_name }}</h4>
							</div>
						</div>
					@endforeach
					<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 abt-team-img-wrap">
						<img class="img-responsive abt-team-image" src="{{ $galleryUrl }}/img/team/you.png" alt="V.I.P.">
						<div class="abt-team-vip text-center">
							<h4 class="abt-team-imgDescription">YOU</h4>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</section>
		<section class="about-sec abt-jobs">
			<div class="abt-jobs-wrap">
				<div class="abt-jobs-head">
					<h3>Join us</h3>
				</div>
				<blockquote>
					<h5>Change starts with you, but it does not start until you do</h5>
					<footer>
						Tom Ziglar
					</footer>
				</blockquote>
				<div class="abt-jobs-body">
					<p>We are always looking for people who believe they can be part of something big, have the courage to face failures, thirst to learn new stuff and above all a "doer". If you think you are this person, we are eager for you to join our family.</p>
					<button class="btn btn-default abt-hire-btn">
						<i class="material-icons valign-mid">&#xE2C6;</i>
						<span class="valign-mid">Submit Your Resume</span>
					</button>
				</div>
			</div>
		</section>
	</div>
@endsection

@section("modals")
	@parent
	@include('app.modals.job_application_modal', ["url" => route('application.form')])
@endsection

@section('js-framework')
	<script src="{{ elixir('js/af-about-us.js') }}"></script>
	<script>
		if ($(".about-us-images").length > 0) {
			$.getScript("/js/util/lazyLoadImages.js", function () {
				$(".about-us-images").removeClass("lazy-loading");
				new LazyLoad();
			});
		}
	</script>
@endsection