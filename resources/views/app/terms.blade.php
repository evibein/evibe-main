@extends('layout.base')

@section('page-title')
	<title>Terms and Conditions | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="terms and conditions to use Evibe.in"/>
@endsection

@section('meta-keywords')
	<meta name="keywords" content="Evibe.in, terms and conditions"/>
@endsection

@section('og-title')
	<meta property="og:title" content="Terms and Conditions | Evibe.in"/>
@endsection

@section('og-description')
	<meta property="og:description" content="terms and conditions to use Evibe.in."/>
@endsection

@section('og-url')
	<meta property="og:url" content="https://evibe.in/terms"/>
@endsection

@section('meta-canonical')
	<link rel="canonical" href="{{ $canonicalUrl }}/terms"/>
@endsection

@section("header")
	@include('base.home.header.header-top-city')
@endsection

@section("footer")
	@include('app.footer_noncity')
@endsection

@section("content")
	<article class="bkgnd-grey">
		<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
			<section class="page-sec terms-sec page-sec-card">
				<div class="sec-head">
					<div class="sec-title-wrap">
						<i class="glyphicon glyphicon-tasks"></i>
						<h1 class="sec-title">Terms Of Service</h1>
					</div>
				</div>
				<div class="sec-body">
					<p>
						This is the agreement (<strong>"Agreement"</strong> or
						<span><strong>"User Agreement"</strong>) for Evibe Technologies Private Limited (<strong>"Evibe"</strong>). This User Agreement explains the terms of uses applicable on your use of our services listed under the website </span>
						<a href="https://evibe.in">https://evibe.in</a>
						<span> (the "Website"). Do not access or use our services, if you do not agree with the terms of service mentioned in this Agreement.</span>
					</p>
					<p>
						<span>You should read and agree to all the conditions mentioned in this Agreement and </span>
						<a href="{{ route('privacy') }}" target="_blank">Privacy Policy</a>.
						<span> Your use of the website constitutes your acceptance to these conditions. Please do not use the Website if you do not agree with any part of this User Agreement.</span>
					<p>
						<strong>Evibe can modify this User Agreement anytime by posting the changed terms of service on the Website.</strong>
					</p>
					<section class="tos-items">
						<ul class="ls-none no-pad">
							<li class="tos-item">
								<strong>1. Service Description</strong>
								<p>Evibe provides services (the
									<strong>"Service"</strong>) via the Website or Phone to assist individuals planning events (<strong>"User"</strong> or
									<strong>"You"</strong>) in searching for, identifying, reviewing and securing orders and reservations with participating third party event-related suppliers and vendors (<strong>"Vendors"</strong>), including, without limitation, planners, decorators, magicians, emcees, game organizers, food counter specialists, venue providers, caterers and entertainment providers that are listed on the Website (<strong>"Suppliers"</strong>). The Website acts as a venue to allow the Suppliers to sell their services at any posted price. Evibe is not a part of the actual transaction between User and Supplier. Hence, Evibe does not hold any control over the accuracy, quality and the ability of the Vendors to sell and Users to pay for the services respectively. Evibe shall not be responsible for the completion or failure of a transaction.
								</p>
							</li>
							<li class="tos-item">
								<strong>2. Eligibility to Accept the User Agreement</strong>
								<p>You must be more than 18 years of age to agree with these conditions. Please do not use our Website if you do not qualify.</p>
							</li>
							<li class="tos-item">
								<strong>3. Website Usage</strong>
								<p>You agree to stick to these terms irrespective of your registration with the services, by clicking on the
									<strong>"I have read and accept the terms of service"</strong> button. You agree that you will only make legitimate reservations and orders on the Website and shall not use the Website for any other purposes. If you agree to these terms on behalf of any legal entity or other person, you must warrant that you possess the authority to that entity to these terms. If you do not agree to the policy and terms, You have no right to use the services.
								</p>
								<span>
								<p>By using the Website you agree that:
									<ol>
										<li>You will not use any crawler or automated program, device, software or anything that accesses the content of the Website without Evibe’s written permission.</li>
										<li>You will not Distribute, sell, copy or modify any part of the Website without Evibe's written agreement.</li>
										<li>You will not hack or impose huge load on our Website.</li>
										<li>You will not attempt to interfere or directly interfere with the smooth working of the Website or any other illegal activities conducted on the Website;</li>
										<li>You will only make a booking with only one Vendor for a single request.</li>
										<li>You will not make fake enquiries or false orders to test our Service or the Website or for reselling the reservation.</li>
										<li>You will not misrepresent or false state your affiliation with the entity or a person.</li>
										<li>You will not disguise the origin of any content available on the Website.</li>
										<li>You will not transmit or post any information that is used to illegally collude against another entity or person in restraint of trade or  contain a virus or other dangerous item or trademark, copyright, or other proprietary right protected, unless with the written permission / agreement of the owner for such rights, or violates their publicity rights or privacy.</li>
										<li>You will not post, display, upload or make availability of anything threatening, unlawful, obscene, inflammatory, pornographic, indecent, or any material that could lead to something that is considered to be a criminal offense or violate any law.</li>
										<li>You agree to receive promotional, transactional and commercial communications from Evibe through email or SMS.</li>
										<li>In some cases, your order/party images/videos may be used on our website for demonstration/reference to new customers. In the case where you are not interested in participating in this program, you may opt-out by sending an email to consent@evibe.in. Please be noted that it may take up to 48 business hours to remove your picture/videos.</li>
										</ol>
									</p>
									<p>Your reservation will be cancelled, deposit will be deducted and You can be subjected to terms of the Vendor if you fail to comply with these terms.</p>
							</span>
							</li>
							<li class="tos-item">
								<strong>4. Booking and Cancellations Policy</strong>
								<p>The Vendor and not Evibe, is solely responsible for fulfilling the service requests submitted by the User through the Website. It is Vendor, not Evibe, that decides the booking fee payable by the User to the Evibe for booking a service provided on the Website. Payment is done by Cheque, Cash, Demand Draft or Bank Transfers.</p>
								<span>
								<p>User cannot enquire more than three different Vendors for the same request. In twenty four business hours after you submit an enquiry, an Evibe Manager (<strong>"eM"</strong>) will contact you. eM checks the availability of the Vendor. If available, eM manages and coordinates the appointment and meetings between you and the Vendor.</p>
								<p>Please review the Vendor's terms of service and privacy policy before booking their services. If you chose to make a booking of a service provided by the Vendor, eM handles your advance payment process. You will receive a confirmation email or SMS with the booking and vendor contact information upon completion of the payment. </p>
								<p><strong>You agree that your deposit will not be refunded in any case once you book any service through Evibe</strong>. Additionally, you agree that you are solely responsible for all taxes, fees and other charges applicable for any booking with any Supplier through this Website.</p>
							</span>
							</li>
							<li class="tos-item">
								<strong>5. No Endorsement</strong>
								<p>Evibe does not endorse any Supplier listed on the Website. You are completely responsible for determining the Supplier identity. You agree not to impose any liability on, or seek any legal remedy from Evibe in this regard.</p>
							</li>
							<li class="tos-item">
								<strong>6. Vendor Agreement Terms</strong>
								<p>After you book a Vendor’s service with us, you agree that you might be required to enter into an agreement with the Vendor and if that is the case, you will accept any terms imposed by the Vendor in oral or written. You agree that only you are responsible for performing the obligations of the terms of service specified by the Vendor and Evibe will not hold any responsibility in this regard.</p>
							</li>
							<li class="tos-item">
								<strong>7. Insurance and Taxes</strong>
								<p>You agree that it is only your responsibility to obtain any insurance required and handle tax obligation related to your booking. Under any circumstance, Evibe does not carry any insurance for the bookings you made with us.</p>
							</li>
							<li class="tos-item">
								<strong>8. Disclaimer</strong>
								<p>YOU ARE COMPLETELY RESPONSIBLE FOR USING THE SERVICES AND AGREE THAT EVIBE DOES NOT RUN ANY IN DEPTH BACKGROUND VERIFICATION ON ANY SUPPLIER. EVIBE DOES NOT WARRANTY THE QUALITY, OR THE ACCURACY OF ANY SERVICES OR SITE CONTENT PRESENT ON THE WEBSITE. YOU AGREE ON THE SOLE RESPONSIBILITY FOR ALL OF YOUR INTERACTIONS AND COMMUNICATIONS WITH OTHER PERSONS AS A RESULT OF YOUR USE OF THE SERVICES, INCLUDING, BUT NOT LIMITED TO, AND ANY SUPPLIERS. YOU ALSO AGREE THAT EVIBE DOES NOT VERIFY OR REVIEW THE STATEMENTS OF SUPPLIERS AND HENCE TAKE NECESSARY PRECAUTIONS IN ANY OFFLINE / ONLINE INTERACTION.</p>
							</li>
							<li class="tos-item">
								<strong>9. Breach</strong>
								<p>If you breach this User Agreement or if Evibe is not able to verify any of your information, Evibe may terminate your association, listing, and refuse to provide service to you.</p>
							</li>
							<li class="tos-item">
								<strong>10. Privacy</strong>
								<p>
									<span>You agree that the terms mentioned in the </span>
									<a href="{{ route('privacy') }}" class="mar-l-4" target="_blank" rel="noopener">Privacy Policy</a>
									<span> are reasonable. Personal information you submit to Evibe either online or offline is governed by Evibe's Privacy Policy. You also agree to grant permission to use of your personal information by Suppliers, Evibe or its affiliates aligned with the purposes mentioned in the policy.</span>
								</p>
							</li>
							<li class="tos-item">
								<strong>11. Limitation of Liability</strong>
								<p>EVIBE ASSUMES NO RESPONSIBILITY, AND SHALL NOT BE LIABLE FOR, ANY DAMAGES TO, OR VIRUSES THAT MAY INFECT YOUR COMPUTER EQUIPMENT OR OTHER PROPERTY ON ACCOUNT OF YOUR ACCESS TO, USE OF, OR BROWSING THE WEBSITE OR YOUR DOWNLOADING OF ANY MATERIALS, DATA, TEXT, IMAGES, VIDEO OR AUDIO FROM THE WEBSITE.
									IN NO EVENT SHALL EVIBE OR ANY THIRD PARTY PROVIDERS OR DISTRIBUTORS BE LIABLE FOR ANY INJURY, LOSS, CLAIM, DAMAGE, OR ANY SPECIAL, EXEMPLARY, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND (INCLUDING, BUT NOT LIMITED TO LOST PROFITS OR LOST SAVINGS),
									WHETHER BASED IN CONTRACT, TORT, STRICT LIABILITY, OR OTHERWISE, WHICH ARISES OUT OF OR IS IN ANY WAY CONNECTED WITH (I) ANY USE OF THE WEBSITE OR CONTENT FOUND HEREIN, (II) ANY FAILURE OR DELAY (INCLUDING, BUT NOT LIMITED TO THE USE OF OR INABILITY TO USE ANY COMPONENT OF THIS SITE FOR RESERVATIONS OR ORDERS),
									OR (III) THE PERFORMANCE OR NON PERFORMANCE BY EVIBE OR ANY THIRD PARTY PROVIDERS OR DISTRIBUTORS, INCLUDING, BUT NOT LIMITED TO, NON PERFORMANCE RESULTING FROM BANKRUPTCY, REORGANIZATION, INSOLVENCY, DISSOLUTION OR LIQUIDATION EVEN IF SUCH PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF DAMAGES TO SUCH PARTIES OR ANY OTHER PARTY.
									IN ADDITION, ALL USERS SPECIFICALLY UNDERSTAND AND AGREE THAT ANY THIRD PARTY DIRECTING USERS TO THE WEBSITE BY REFERRAL, LINK OR ANY OTHER MEANS IS NOT LIABLE TO USER FOR ANY REASON WHATSOEVER, INCLUDING BUT NOT LIMITED TO DAMAGES OR LOSS ASSOCIATED WITH THE USE OF THE SERVICE, THE WEBSITE OR WEBSITE CONTENT.
									EVIBE IS NEITHER AN AGENT NOR IS AFFILIATED WITH ANY SUPPLIER WITH WHICH A USER MAKES A RESERVATION OR PLACES AN ORDER. ASIDE FROM THE SERVICE PROVIDED ON THE WEBSITE, EVIBE IS NOT LIABLE FOR ANY INJURY, LOSS, CLAIM, DAMAGE, OR ANY SPECIAL, EXEMPLARY, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND (INCLUDING,
									BUT NOT LIMITED TO LOST PROFITS OR LOST SAVINGS), WHETHER BASED IN CONTRACT, TORT, STRICT LIABILITY, OR OTHERWISE, WHICH ARISES OUT OF OR IS IN ANY WAY CONNECTED WITH A USER'S USE OF A SUPPLIER.
									If, notwithstanding the foregoing, Evibe or any third party provider or distributor should be found liable for any loss or damage which arises out of or is in any way connected with any of the above described functions or uses of this Website or its content,
									the liability of Evibe and the third party providers and distributors shall in no event exceed, in the aggregate, the greater of (a) the Supplier Reservation Fee for reserving a Supplier, or (b) INR 5000.00 whichever is lower. In its sole discretion, in addition to any other rights or remedies available to Evibe and without any liability whatsoever, Evibe at any time and without notice may terminate or restrict your access to any component of the Website. Some states do not allow limitation of liability, so the foregoing limitation may not apply to you.
								</p>
							</li>
							<li class="tos-item">
								<strong>12. Indemnity</strong>
								<p>You agree to indemnify and hold us and (as applicable) our parent, subsidiaries, affiliates, officers, directors, agents, and employees, harmless from any claim or demand, including reasonable attorneys" fees, made by any third party due to or arising out of your breach of this Agreement or the documents it incorporates by reference, or your violation of any law or the rights of a third party.</p>
							</li>
							<li class="tos-item">
								<strong>13. Feedback</strong>
								<p>
									<span>We appreciate comments and feedback for the Website. You can submit Feedback </span>
									<a href="/feedback" target="_blank" rel="noopener">here</a>
									<span> or you can email us at </span>
									<a href="mailto:ping@evibe.in" class="mar-l-4" target="_blank" rel="noopener">ping@evibe.in</a>
									<span>. By submitting, you grant Evibe a worldwide, perpetual, non-exclusive, royalty-free and fully sub-licensable license to reproduce, modify, adapt, publish, and otherwise use, with or without attribution such content on services of Evibe, Suppliers and Evibe affiliates.</span>
								</p>
							</li>
							<li class="tos-item">
								<strong>14. Disputes</strong>
								<p>In case of any disagreement arising out of these terms, the Parties shall first attempt for a resolution within 60 days. If the Parties fail to resolve the issue, either of them may choose to continue the arbitration. These proceedings shall take place in Bangalore and shall be governed by the Arbitration and Conciliation Act, 1996. The award passed by the Arbitrators shall be final and the courts of Bangalore shall have exclusive jurisdiction.</p>
							</li>
							<li class="tos-item">
								<strong>15. Contact</strong>
								<p>
									<span>Please contact us at </span>
									<a href="mailto:ping@evibe.in" class="mar-l-4" target="_blank" rel="noopener">ping@evibe.in</a>
									<span> if you have any questions related to the terms of service mentioned.</span>
								</p>
							</li>
						</ul>
					</section>
				</div>
			</section>
		</div>
		<div class="clearfix"></div>
	</article>
@endsection