<div class="main-card process-card"> <!-- process card begin -->
	<h5 class="headline">How It Works?</h5>
	<ul class="no-mar no-pad process-items">
		<li>
			<div class="item-title-cnt">
				<img class="item-icon" src="{{ $galleryUrl}}/img/icons/finalize.png" alt="block">
				<span class="item-title">Pay Token</span>
			</div>
			<div class="item-desc">Securely pay token amount to block this deal for your party slot.</div>
		</li>
		<li>
			<div class="item-title-cnt">
				<img class="item-icon" src="{{ $galleryUrl}}/img/icons/24-hours.png" alt="availability">
				<span class="item-title">Availability</span>
			</div>
			<div class="item-desc">We will check the venue availability and confirm within 4 business hours.</div>
		</li>
		<li>
			<div class="item-title-cnt">
				<img class="item-icon" src="{{ $galleryUrl}}/img/icons/city-hall.png" alt="site visit">
				<span class="item-title">Site Visit</span>
			</div>
			<div class="item-desc">Checkout the place to your satisfaction & discuss all details with venue manager within 48 hrs after availability confirmation.</div>
		</li>
		<li>
			<div class="item-title-cnt">
				<img class="item-icon" src="{{ $galleryUrl}}/img/icons/credit-card.png" alt="book">
				<span class="item-title">Pay Advance</span>
			</div>
			<div class="item-desc">Pay balance booking advance amount at the venue to complete your booking.</div>
		</li>
		<li>
			<div class="item-title-cnt">
				<img class="item-icon" src="{{ $galleryUrl}}/img/icons/wine-glasses.png" alt="party">
				<span class="item-title">Relax & Enjoy</span>
			</div>
			<div class="item-desc">Venue manager will ensure everything is done as per your requirement.</div>
		</li>
	</ul>
</div>