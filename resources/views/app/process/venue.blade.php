<div class="main-card process-card"> <!-- process card begin -->
	<h5 class="headline">How It Works?</h5>
	<ul class="no-mar no-pad process-items">
		<li>
			<div class="item-title-cnt">
				<img class="item-icon" src="{{ $galleryUrl}}/img/icons/write.png" alt="enquire">
				<span class="item-title">Enquire</span>
			</div>
			<div class="item-desc">Fill your contact details above. Our venue expert will speak to you.</div>
		</li>
		<li class="hide">
			<div class="item-title-cnt">
				<img class="item-icon" src="{{ $galleryUrl}}/img/icons/city-hall.png" alt="site visit">
				<span class="item-title">Site Visit</span>
			</div>
			<div class="item-desc">If you want to visit the place, we will help you schedule your site visit.</div>
		</li>
		<li>
			<div class="item-title-cnt">
				<img class="item-icon" src="{{ $galleryUrl}}/img/icons/finalize.png" alt="finalize">
				<span class="item-title">Finalise</span>
			</div>
			<div class="item-desc">Get your queries answered & finalise your venue upon satisfaction.</div>
		</li>
		<li>
			<div class="item-title-cnt">
				<img class="item-icon" src="{{ $galleryUrl}}/img/icons/credit-card.png" alt="book">
				<span class="item-title">Book</span>
			</div>
			<div class="item-desc">Make {{config('evibe.ticket.advance.percentage')}}% of total order amount to block your slot (based on availability).</div>
		</li>
		<li>
			<div class="item-title-cnt">
				<img class="item-icon" src="{{ $galleryUrl}}/img/icons/wine-glasses.png" alt="party">
				<span class="item-title">Relax & Enjoy</span>
			</div>
			<div class="item-desc">Your venue manager will take care of all arrangements.</div>
		</li>
	</ul>
</div>