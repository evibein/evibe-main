<div>
	<input type="hidden" class="reportCityId" id="reportCityId" value="{{ $data['shortlistData']['cityId'] }}">
	<input type="hidden" class="reportEventId" id="reportEventId" value="{{ $data['shortlistData']['occasionId'] }}">
	<input type="hidden" class="reportPageId" id="reportPageId" value="{{ $data['shortlistData']['mapTypeId'] }}">
	<input type="hidden" class="reportOptionId" id="reportOptionId" value="{{ $data['shortlistData']['mapId'] }}">
	<input type="hidden" class="reportUrl" id="reportUrl" value="{{ url()->current() }}">
</div>

<div id="reportIssueWrap" class="reportIssueWrap text-center pad-b-10">
	<a href="#" id="reportIssue" class="report-issue">
		<i class="glyphicon glyphicon-flag"></i> Report This Listing
	</a>
</div>

<div class="modal fade modal-report-issue" tabindex="-1" role="dialog" id="modalReportIssue">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="modal-inner-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title text-center">Report Issue With Listing</h4>
				</div>
				<div class="ri-thank-you-message text-center hide">
					<i class="glyphicon glyphicon-ok-circle text-success"></i> Thanks for taking the time to report this listing. It doesn't go unnoticed!
				</div>
				<form id="fromReportIssue" class="report-issue-form" data-url="{{ route('report.issue') }}">
					<div class="sticky-error-wrap hide">
						<span class="error-message">Please fill the issue.</span>
					</div>
					<div class="form-group">
						<div class="col-md-12 col-lg-12 col-sm-12">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
									<textarea class="mdl-textfield__input" type="text" name="reportComment" id="reportComment" minlength="10"></textarea>
								<label class="mdl-textfield__label" for="reportComment">What is the issue?</label>
							</div>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-6">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="reporterName" id="reporterName" value="@if(auth()->check()){{ auth()->user()->name }}@endif"/>
								<label class="mdl-textfield__label no-mar-b" for="reporterName">Name*</label>
							</div>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-6">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="reporterEmail" id="reporterEmail" value="@if(auth()->check()){{ auth()->user()->username }}@endif"/>
								<label class="mdl-textfield__label no-mar-b" for="reporterEmail">Email*</label>
							</div>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-6">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" name="reporterPhone" id="reporterPhone" maxlength="10"/>
								<label class="mdl-textfield__label no-mar-b" for="reporterPhone">Phone Number</label>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<div class="col-sm-12 text-center">
							<button class="btn btn-link mar-r-5 close-reset-form" data-dismiss="modal">
								<i class="glyphicon glyphicon-remove"></i> <em>Cancel</em>
							</button>
							<button id="formReportBtnSubmit" type="submit" class="btn btn-primary btn-submit mar-l-5">
								<span class="default">
									<span class="glyphicon glyphicon-ok"></span>
									<span class="valign-mid">SUBMIT</span>
								</span>
								<span class="waiting hide">
									<span class="glyphicon glyphicon-ok"></span>
									<span class="valign-mid">SUBMITTING..</span>
								</span>
							</button>
						</div>
						<div class="clearfix"></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@section("javascript")
	@parent
	<script type="text/javascript">
		$(document).ready(function () {

			var btnSubmit = $('#formReportBtnSubmit');

			function showSubmitButton() {
				btnSubmit.attr('disabled', false);
				btnSubmit.find('.default').removeClass('hide');
				btnSubmit.find('.waiting').addClass('hide');
			}

			function disableSubmitButton() {
				btnSubmit.attr('disabled', true);
				btnSubmit.find('.default').addClass('hide');
				btnSubmit.find('.waiting').removeClass('hide');
			}

			$('#reportIssue').click(function (event) {
				event.preventDefault();

				/* open the modal */
				var biddingModal = $('#modalReportIssue');
				biddingModal.modal({
					'show': true,
					'backdrop': 'static'
				});
			});

			/* submit the form */
			$('#fromReportIssue').submit(function (e) {
				e.preventDefault();

				var formUrl = $(this).data('url');
				disableSubmitButton();

				var data = {
					'reportComment': $('#reportComment').val(),
					'reporterName': $('#reporterName').val(),
					'reporterEmail': $('#reporterEmail').val(),
					'reporterPhone': $('#reporterPhone').val(),
					'reportCityId': $('#reportCityId').val(),
					'reportEventId': $('#reportEventId').val(),
					'reportPageId': $('#reportPageId').val(),
					'reportOptionId': $('#reportOptionId').val(),
					'reportUrl': $('#reportUrl').val()
				};

				$.ajax({
					url: formUrl,
					type: "POST",
					data: data,
					success: function (data) {
						if (data.success) {
							$('#fromReportIssue').addClass('hide');
							$('.ri-thank-you-message').removeClass('hide');
						}
						else if (data.error) {
							showSubmitButton();
							var errorObj = $('#fromReportIssue').find('.sticky-error-wrap').removeClass('hide').find('.error-message');
							errorObj.text(data.error);
						}
						else {
							showSubmitButton();
							$('#fromReportIssue').modal('show');
							window.showNotyError("An error occurred while submitting your request. Please try again later.");
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						showSubmitButton();
						window.showNotyError("An error occurred while submitting your request. Please try again later.");
						window.notifyTeam({
							"url": url,
							"textStatus": textStatus,
							"errorThrown": errorThrown
						});
					}
				});
			});
		});
	</script>
@endsection