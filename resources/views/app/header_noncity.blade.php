<header>
	<div class="header-top">
		<nav class="navbar navbar-default navbar-home navbar-top" role="navigation">
			<!-- desktop -->
			<div class="hide__400 hide-400-600">
				<nav class="master-home-top-navbar">
					<div class="navbar-collapse">
						<ul class="master-home-top-navbar-left">
							<li>
								<a href="{{ route('city.occasion.birthdays.home', [getCityUrl(), "ref" => "header"]) }}" class="@if(request()->is('*'.config('evibe.occasion.kids_birthdays.url').'*')){{ "active" }}@endif">Birthdays</a>
							</li>
							<li>
								<a href="{{ route('city.cld.list', getCityUrl()) }}">Candle Light Dinners</a>
							</li>
							<li class="hide">
								<a href="{{ route('city.occasion.pre-post.home', [getCityUrl(), "ref" => "header"]) }}" class="@if(request()->is('*'.config('evibe.occasion.pre-post.url').'*')){{ "active" }}@endif">Weddings</a>
							</li>
							<li>
								<a href="{{ route('city.occasion.bachelor.home', [getCityUrl(), "ref" => "header"]) }}" class="@if(request()->is('*'.config('evibe.occasion.bachelor.url').'*')){{ "active" }}@endif">Youth Party</a>
							</li>
							<li>
								<a href="{{ route('city.occasion.surprises.home', [getCityUrl(), "ref" => "header"]) }}" class="@if(request()->is('*'.config('evibe.occasion.surprises.url').'*')){{ "active" }}@endif">Surprises</a>
							</li>
							<li>
								<a href="{{ route('city.occasion.house-warming.home', [getCityUrl(), "ref" => "header"]) }}" class="@if(request()->is('*'.config('evibe.occasion.house-warming.url').'*')){{ "active" }}@endif">House Warming</a>
							</li>
							<li class="hide">
								<a href="{{ route('city.occasion.corporate.home', [getCityUrl(), "ref" => "header"]) }}" class="@if(request()->is('*corporate*')){{ "active" }}@endif">Corporate Events</a>
							</li>
						</ul>
						<ul class="master-home-top-navbar-right">
							<li>
								<a href="javascript:void(0)" class="select-city">
									<i class="glyphicon glyphicon-map-marker no-mar-r"></i>
									{{ ucfirst(getCityName()) }}
								</a>
							</li>
							<li>
								<a href="javascript:void(0)" class="no-click evibe-cheppandi nav-topbar-phone">
									<i class="glyphicon glyphicon-phone no-pad no-mar"></i>
									{{ config('evibe.contact.company.plain_phone') }}
								</a>
							</li>
							<li>
								<a href="https://evibe.in/blog" target="_blank" rel="noopener">
									<span> Blog</span>
								</a>
							</li>
							@include('app.login_user')
						</ul>
					</div>
				</nav>
				<nav class="navbar-default navbar-home navbar-home-white no-mar-b" role="navigation">
					<div class="in-blk">
						<a class="navbar-brand topnav" href="/">
							<img src="{{ $galleryUrl }}/img/logo/logo_evibe.png" alt="Evibe logo">
						</a>
					</div>
					<div class="in-blk pull-right" id="navbarNav">
						<ul class="nav navbar-nav top-header-list" v-cloak>
							@if(request()->is('*corporate-events*'))
								<li class="pad-l-10">
									<a href="#" class="corporate-enq-btn">
										<span class="glyphicon glyphicon-envelope"></span>
										<span>Enquire Now</span>
									</a>
								</li>
							@else
								<li class="pad-l-10">
									<a href="#" class="btn-post">
										<span class="glyphicon glyphicon-envelope"></span>
										<span>Enquire Now</span>
									</a>
								</li>
								<li>
									<a href="{{ route('partybag.list') }}" class="party-bag-header-navbar">
										<i class="icon-bagicon5"></i>
										Cart
										<span class="partybag-badge master-home-pbCount">@{{ pbCount }}</span>
									</a>
								</li>
							@endif
						</ul>
					</div>
				</nav>
				<div class="clearfix"></div>
			</div>

			<!-- for media less than 600px -->
			<div class="hide show-400-600 unhide__400">
				<!-- city and phone number -->
				<div class="text-center pad-b-6">
					<div class="pad-t-5 pad-l-15 font-12 text-col-wh navbar-links top-strip">
						<div class="in-blk font-12 mar-r-15 top-header-static-page-evibe-cheppandi">
							<span class="glyphicon glyphicon-earphone"></span>{{ config('evibe.contact.company.plain_phone') }}
						</div>
						<div class="in-blk">
							@if(request()->url() != route('partybag.list'))
								@include('app.partybag_header_top')
							@endif
						</div>
					</div>
				</div>

				<!-- Logo and Search bar-->
				<div class="container no-pad">
					<div class="col-xs-9 col-sm-9">
						<a class="navbar-brand no-pad-t" href="/">
							<img src="{{ $galleryUrl }}/img/logo/logo_evibe.png" alt="Evibe.in logo">
						</a>
					</div>
					<div class="col-xs-2 col-sm-2">
						@include('app.login_user')
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</nav>
	</div>
</header>

@section("modals")
	@parent
	@include('app.modals.enquiry-form', [
		"url" => route("city.header.enquire", "no-city")
	])
@endsection