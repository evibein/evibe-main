<header>
	<div class="header-top">
		<nav class="navbar navbar-default navbar-home navbar-top trim-navbar-top" role="navigation">
			<nav class="navbar-default navbar-home navbar-home-white no-mar-b" role="navigation">
				<div class="in-blk mar-l-15 no-mar-400-600">
					<a class="navbar-brand topnav" href="/">
						<img src="{{ $galleryUrl }}/img/logo/logo_evibe.png" alt="Evibe logo">
					</a>
				</div>
				<div class="in-blk pull-right mar-r-15 no-mar-400-600" id="navbarNav">
					<ul class="nav navbar-nav top-header-list pad-r-30" v-cloak>
						<li class="pad-l-10">
							<a class="header-email-text">
								<span class="glyphicon glyphicon-envelope"></span>
								<span>support@evibe.in</span>
							</a>
						</li>
					</ul>
				</div>
			</nav>
			<div class="clearfix"></div>
		</nav>
	</div>
</header>

@section("modals")
	@parent
	@include('app.modals.enquiry-form', [
		"url" => route("city.header.enquire", "no-city")
	])
@endsection