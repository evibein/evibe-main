@extends('layout.base')

@section('page-title')
	<title>Refund Policy | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="refund policy of Evibe.in"/>
@endsection

@section('meta-keywords')
	<meta name="keywords" content="Evibe.in, refund policy"/>
@endsection

@section('og-title')
	<meta property="og:title" content="refund Policy | Evibe.in"/>
@endsection

@section('og-description')
	<meta property="og:description" content="refund policy of Evibe.in."/>
@endsection

@section('og-url')
	<meta property="og:url" content="https://evibe.in/refunds"/>
@endsection

@section('meta-canonical')
	<link rel="canonical" href="{{ $canonicalUrl }}/refunds"/>
@endsection

@section("header")
	@include('base.home.header.header-top-city')
@endsection

@section("footer")
	@include('app.footer_noncity')
@endsection

@section("content")
	<article class="bkgnd-grey">
		<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
			<section class="page-sec privacy-sec page-sec-card">
				<div class="sec-head">
					<div class="sec-title-wrap">
						<i class="glyphicon glyphicon-th-list pad-r-5"></i>
						<h1 class="sec-title">Refund Policy</h1>
					</div>
				</div>
				<div class="sec-body">
					<section class="privacy-items">
						<div>In case of cancellation, refundable amount is calculated based on the following policies:</div>
						<table class="table table-condensed table-bordered mar-t-10">
							<tr>
								<th>Party Cancellation Time</th>
								<th>Refund Percentage*</th>
							</tr>
							<tr>
								<td>0 - 24 hours before party</td>
								<td>0%</td>
							</tr>
							<tr>
								<td>1 day- 3 days before party</td>
								<td>25%</td>
							</tr>
							<tr>
								<td>4 days - 10 days before party</td>
								<td>50%</td>
							</tr>
							<tr>
								<td>11 days or above before party</td>
								<td>70%</td>
							</tr>
						</table>
						<div class="mar-t-5">* Refund Percentage will be calculated on <b>Total Advance Amount</b></div>
					</section>
				</div>
			</section>
		</div>
		<div class="clearfix"></div>
	</article>
@endsection