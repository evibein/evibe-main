@extends('layout.base')

@section('page-title')
	<title>Frequently Asked Questions | Evibe.in</title>
@endsection

@section('meta-description')
	<meta name="description" content="What is evibe.in? Who can use evibe.in? Is evibe.in a event management company? What kind of vendors can I find for my party? What is evibe.in cancellation policy?"/>
@endsection

@section('meta-keywords')
	<meta name="keywords" content="eVibe.in faqs, frequently asked questions">
@endsection

@section('og-title')
	<meta property="og:title" content="Frequently Asked Questions | Evibe.in"/>
@endsection

@section('og-description')
	<meta property="og:description" content="What is evibe.in? Who can use evibe.in? Is evibe.in an event management company? What kind of vendors can I find for my party? What is evibe.in cancellation policy?"/>
@endsection

@section('og-url')
	<meta property="og:url" content="https://evibe.in/faqs"/>
@endsection

@section('meta-canonical')
	<link rel="canonical" href="{{ $canonicalUrl }}/faqs"/>
@endsection

@section("header")
	@include('base.home.header.header-top-city')
@endsection

@section("footer")
	@include('app.footer_noncity')
@endsection

@section("content")
	<article class="bkgnd-grey">
		<div class="col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
			<section class="page-sec faqs-sec pages-sec-card">
				<div class="sec-head">
					<div class="sec-title-wrap">
						<i class="glyphicon glyphicon-question-sign"></i>
						<h1 class="sec-title">Frequently Asked Questions</h1>
					</div>
				</div>
				<div class="sec-body">
					@if (count($data['faqs']) != 0)
						<ul class="faqs-list ls-none no-pad">
							@foreach ($data['faqs'] as $faq)
								<li class="faq">
									<div class="question">{{ $faq->question }}</div>
									<div class="answer">{!! $faq->answer !!}</div>
								</li>
							@endforeach
						</ul>
					@else
						<div class="no-data-msg">Sorry, there are no FAQs.</div>
					@endif
				</div>
			</section>
		</div>
		<div class="clearfix"></div>
	</article>
@endsection