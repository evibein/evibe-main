@if (!request()->is('*/engagement-wedding-reception*') && !request()->is('*/bachelor-party*'))
	@if($currentPage % 2 == 1 && $loopIteration == 8 || $currentPage % 2 == 0 && $loopIteration == 12)
		<div class="@if(isset($noOfCards) && $noOfCards == 4) col-md-3 col-lg-3 col-sm-4 col-xs-6 no-pad-r @else col-sm-12 col-md-6 col-lg-4 no-pad-l no-pad__400-600 @endif">
			<div class="custom-card">
				<div class="custom-card-header">
					<div class="custom-card-title">
						Have your own designs?
					</div>
					<div class="custom-card-content">
						You can now upload your customized designs.
						Based on the availability, we will get the best quote from our verified partners.
					</div>
				</div>
				<div class="custom-card-footer">
					<button class="btn btn-default btn-bidding">
						<i class="material-icons valign-mid">&#xE2C6;</i>
						<span class="valign-mid">Submit My Designs</span>
					</button>
				</div>
			</div>
		</div>
	@endif
@endif