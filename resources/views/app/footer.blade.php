<?php
$occasionUrl = config('evibe.occasion.kids_birthdays.url');

$venueBase = "/$cityUrl/$occasionUrl/" . config('evibe.results_url.venues');
$decorUrl = "/$cityUrl/$occasionUrl/" . config("evibe.results_url.decors");
$entUrl = "/$cityUrl/$occasionUrl/" . config("evibe.results_url.entertainment") . "?ref=footer";
$packagesUrl = "/$cityUrl/$occasionUrl/" . config("evibe.results_url.packages") . "?ref=footer";
$addOnUrl = "/$cityUrl/$occasionUrl/" . config("evibe.results_url.addons") . "?ref=footer";
$cakesUrl = "/$cityUrl/$occasionUrl/" . config("evibe.results_url.cakes") . "?ref=footer";
?>

<div class="footer-wrap">
	<div class="footer-top">
		<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 col-sm-offset-1 col-md-offset-1 col-lg-offset-1 footer-links-list">
			<div class="col-xs-12 col-sm-3 mar-t-40">
				<div class="cat-title">Decor Styles</div>
				<ul class="cat-list">
					<li class="footer-link">
						<a href="{{ $decorUrl }}?style=simple-balloons-decorations">Simple Balloons Styles</a>
					</li>
					<li class="footer-link">
						<a href="{{ $decorUrl }}?style=custom-balloons-decorations">Custom Balloons Styles</a>
					</li>
					<li class="footer-link">
						<a href="{{ $decorUrl }}?style=drapes-decorations">Drape Styles</a>
					</li>
					<li class="footer-link">
						<a href="{{ $decorUrl }}?style=basic-theme-decorations">Basic Theme Styles</a>
					</li>
					<li class="footer-link">
						<a href="{{ $decorUrl }}?style=moderate-theme-decorations">Moderate Theme Styles</a>
					</li>
					<li class="footer-link">
						<a href="{{ $decorUrl }}?style=highend-theme-decorations">High End Theme Styles</a>
					</li>
					<li class="footer-link">
						<a href="{{ $decorUrl }}?style=3d-theme-decorations">3D Theme Styles</a>
					</li>
				</ul>
			</div>
			<div class="col-xs-12 col-sm-3 mar-t-40">
				<div class="cat-title">Entertainment</div>
				<ul class="cat-list">
					<li class="footer-link"><a href="{{ $entUrl }}">Stage Magic Show</a></li>
					<li class="footer-link"><a href="{{ $entUrl }}">Close-Up Magic Show</a></li>
					<li class="footer-link"><a href="{{ $entUrl }}">MC Games</a></li>
					<li class="footer-link"><a href="{{ $entUrl }}">Clown Show</a></li>
					<li class="footer-link"><a href="{{ $entUrl }}">Face Painting</a></li>
					<li class="footer-link"><a href="{{ $entUrl }}">Live Cartoon Character</a></li>
					<li class="footer-link"><a href="{{ $entUrl }}">Puppet Show</a></li>
					<li class="footer-link more-link">
						<a href="{{ $entUrl }}">
							<i class="glyphicon glyphicon-plus"></i> Show More Options
						</a>
					</li>
				</ul>
			</div>
			<div class="col-xs-12 col-sm-3 mar-t-40">
				<div class="cat-title">Birthday Packages</div>
				<ul class="cat-list">
					<li class="footer-link">
						<a href="{{ $packagesUrl }}&category=decor">Having Decors</a>
					</li>
					<li class="footer-link">
						<a href="{{ $packagesUrl }}&category=decor-entertainment-combo">Decor & Entertainment Combo</a>
					</li>
					<li class="footer-link">
						<a href="{{ $packagesUrl }}&category=food">Food Combos</a>
					</li>
					<li class="footer-link">
						<a href="{{ $packagesUrl }}&category=handpicked-packages">Handpicked Packages</a>
					</li>
					<li class="footer-link">
						<a href="{{ $packagesUrl }}&category=special-experiences">Special Experiences</a>
					</li>
				</ul>
			</div>
			<div class="col-xs-12 col-sm-3 mar-t-40">
				<div class="cat-title">Addons & Cakes</div>
				<ul class="cat-list">
					<li class="footer-link"><a href="{{ $cakesUrl }}">Cakes</a></li>
					<li class="footer-link"><a href="{{ $addOnUrl }}">Cotton Candy</a></li>
					<li class="footer-link"><a href="{{ $addOnUrl }}">Instant Photo Booth</a></li>
					<li class="footer-link"><a href="{{ $addOnUrl }}">Photography</a></li>
					<li class="footer-link"><a href="{{ $addOnUrl }}">Sound System</a></li>
					<li class="footer-link"><a href="{{ $addOnUrl }}">Chocolate Fountain</a></li>
					<li class="footer-link more-link">
						<a href="{{ $addOnUrl }}">
							<i class="glyphicon glyphicon-plus"></i> Show More Options
						</a>
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="footer-mid">
		<div class="col-xs-12 col-sm-12">
			<div class="seo-venues-title">Best Venues for Birthday Parties</div>
			<div>
				<div class="col-xs-12 col-sm-3 mar-t-40">
					<div class="cat-title">By Venue Type</div>
					<ul class="cat-list">
						<li class="footer-link">
							<a href="{{ $venueBase }}?type=5-star-hotels&ref=footer">5 Star Hotels in {{ $cityName }}</a>
						</li>
						<li class="footer-link">
							<a href="{{ $venueBase }}?type=4-star-hotels&ref=footer">4 Star Hotels in {{ $cityName }}</a>
						</li>
						<li class="footer-link">
							<a href="{{ $venueBase }}?type=3-star-hotels&ref=footer">3 Star Hotels in {{ $cityName }}</a>
						</li>
						<li class="footer-link">
							<a href="{{ $venueBase }}?type=party-halls&ref=footer">Party Halls in {{ $cityName }}</a>
						</li>
						<li class="footer-link">
							<a href="{{ $venueBase }}?type=resorts&ref=footer">Resorts in {{ $cityName }}</a>
						</li>
						<li class="footer-link">
							<a href="{{ $venueBase }}?type=restaurants&ref=footer">Restaurants in {{ $cityName }}</a>
						</li>
						<li class="footer-link more-link">
							<a href="{{ route("city.occasion.birthdays.venues.links", $cityUrl) }}">
								<i class="glyphicon glyphicon-plus"></i> Show More Venue Types
							</a>
						</li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-3 mar-t-40">
					<div class="cat-title">By Hall Type</div>
					<ul class="cat-list">
						<li class="footer-link">
							<a href="{{ $venueBase }}?category=banquet-halls&ref=footer">Banquet Halls in {{ $cityName }}</a>
						</li>
						<li class="footer-link">
							<a href="{{ $venueBase }}?category=open-air&ref=footer">Open Air Spaces in {{ $cityName }}</a>
						</li>
						<li class="footer-link">
							<a href="{{ $venueBase }}?category=open-air-pool-side&ref=footer">Pool Side Spaces in {{ $cityName }}</a>
						</li>
						<li class="footer-link">
							<a href="{{ $venueBase }}?category=terrace-halls&&ref=footer">Terrace Halls in {{ $cityName }}</a>
						</li>
						<li class="footer-link">
							<a href="{{ $venueBase }}?category=restaurants&ref=footer">Restaurant Spaces in {{ $cityName }}</a>
						</li>
						<li class="footer-link more-link">
							<a href="{{ route("city.occasion.birthdays.venues.links", $cityUrl) }}">
								<i class="glyphicon glyphicon-plus"></i> Show More Hall Types
							</a>
						</li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-3 mar-t-40">
					<div class="cat-title">By Location</div>
					<ul class="cat-list">
						<li class="footer-link">
							<a href="{{ $venueBase }}?location=Indiranagar&ref=footer">Best Venues in Indira Nagar</a>
						</li>
						<li class="footer-link">
							<a href="{{ $venueBase }}?location=Koramangala&ref=footer">Best Venues in Koramangala</a>
						</li>
						<li class="footer-link">
							<a href="{{ $venueBase }}?location=Whitefield&ref=footer">Best Venues in Whitefield</a>
						</li>
						<li class="footer-link">
							<a href="{{ $venueBase }}?location=Jayanagar&ref=footer">Best Venues in Jayanagar</a>
						</li>
						<li class="footer-link">
							<a href="{{ $venueBase }}?location=J.P%20Nagar&ref=footer">Best Venues in J. P. Nagar</a>
						</li>
						<li class="footer-link">
							<a href="{{ $venueBase }}?location=Marathahalli%20Outer%20Ring%20Road&ref=footer">Best Venues in Marathahalli</a>
						</li>
						<li class="footer-link">
							<a href="{{ $venueBase }}?location=Hebbal&ref=footer">Best Venues in Hebbal</a>
						</li>
						<li class="footer-link more-link">
							<a href="{{ route("city.occasion.birthdays.venues.links", $cityUrl) }}">
								<i class="glyphicon glyphicon-plus"></i> Show More Locations
							</a>
						</li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-3 mar-t-40">
					<div class="cat-title">By Pricing</div>
					<ul class="cat-list">
						<li class="footer-link">
							<a href="{{ $venueBase }}?price_min=0&price_max=300&ref=footer">
								Upto <span class='rupee-font'>&#8377; </span> 300 (per person)
							</a>
						</li>
						<li class="footer-link">
							<a href="{{ $venueBase }}?price_min=300&price_max=500&ref=footer">
								From <span class='rupee-font'>&#8377; </span> 300 to
								<span class='rupee-font'>&#8377; </span> 500
							</a>
						</li>
						<li class="footer-link">
							<a href="{{ $venueBase }}?price_min=500&price_max=750&ref=footer">
								From <span class='rupee-font'>&#8377; </span> 500 to
								<span class='rupee-font'>&#8377; </span> 750
							</a>
						</li>
						<li class="footer-link">
							<a href="{{ $venueBase }}?price_min=750&price_max=1000&ref=footer">
								From <span class='rupee-font'>&#8377; </span> 750 to
								<span class='rupee-font'>&#8377; </span> 1,000
							</a>
						</li>
						<li class="footer-link">
							<a href="{{ $venueBase }}?price_min=1000&price_max=5000&ref=footer">
								Above <span class='rupee-font'>&#8377; </span> 1,000
							</a>
						</li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="footer-bottom">
		<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
			<div class="footer-bottom-inner">
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-pad__400-600">
					<div class="footer-logo">
						<div class="in-blk">
							<a href="/">evibe.in</a>
						</div>
					</div>
					<div class="copyrights-wrap">
						<div class="copytext">© 2013 - 2016 Evibe Technologies Pvt. Ltd.</div>
						<div class="copytext">
							<span>All rights reserved</span>
							<div class="pad-t-10">
								<div class="in-blk">
									<span>•</span>
									<span><a href="{{ route('terms') }}" target="_blank">Terms</a></span>
								</div>
								<div class="in-blk">
									<span>•</span>
									<span><a href="" target="_blank">Privacy</a></span>
								</div>
								<div class="in-blk">
									<span>•</span>
									<span><a href="{{ route('refunds') }}" target="_blank">Refunds</a></span>
								</div>
							</div>
						</div>
					</div>
					<div class="socmed-body">
						<div class="socmed-body-inner">
							<div class="socmed-gplus in-blk">
								<a href="https://plus.google.com/113143091769031310564/" target="_blank" rel="noopener">
									<img src="{{ $galleryUrl }}/img/logo/google_plus_logo.png" alt="Google+ page" title="Follow Evibe on Google+">
								</a>
							</div>
							<div class="socmed-fb in-blk">
								<a href="https://www.facebook.com/evibe.in" target="_blank" rel="noopener">
									<img src="{{ $galleryUrl }}/img/logo/facebook_logo.png" alt="Facebook page" title="Follow Evibe on Facebook">
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 no-pad__400-600">
					<div class="footer-bottom-right">
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad__400-600">
							<h4 class="links-head">Company</h4>
							<ul class="links-list">
								<li class="link">
									<a href="/about?ref=footer">About Us</a>
								</li>
								<li class="link hide">
									<a href="/how-it-works?ref=footer" class="hew-link">How It Works</a>
								</li>
								<li class="link">
									<a href="/faqs?ref=footer">FAQs</a>
								</li>
								<li class="link">
									<a href="/about#join-us?ref=footer">Join Us</a>
								</li>
							</ul>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad__400-600">
							<h4 class="links-head">Partners</h4>
							<ul class="links-list">
								<li class="link">
									<a href="{{ route('partner.benefits') }}?ref=footer">Why Sign Up?</a>
								</li>
								<li class="link">
									<a href="{{ route('partner.sign-up.show') }}?ref=footer">Sign Up</a>
								</li>
								<li class="link">
									<a href="{{ route('partner.dash.new.login') }}?ref={{ $ref }}">Partner Login</a>
								</li>
							</ul>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-pad__400-600">
							<h4 class="links-head">Contact</h4>
							<div>
								<div class="contact-item">
									<div class="in-blk hide-400-600">
										<img src="{{ $galleryUrl }}/img/icons/icon_phone.png" alt="Phone Number" class="phone-icon-ft" title="Evibe phone number">
									</div>
									<div class="in-blk contact-val">{{ config('evibe.contact.company.phone') }}</div>
								</div>
								<div class="contact-item hide">
									<div class="in-blk">
										<img src="{{ $galleryUrl }}/img/icons/skype_icon.png" alt="Skype" class="icons" title="Evibe Skype Id">
									</div>
									<div class="in-blk">evibe.in</div>
								</div>
								<div class="contact-item neg-mar">
									<div class="in-blk hide-400-600">
										<img src="{{ $galleryUrl }}/img/icons/email_icon.png" alt="email" class="email-icon-ft" title="Evibe email address">
									</div>
									<div class="in-blk contact-val">
										<a href="mailto:ping@evibe.in" target="_blank" rel="noopener">ping@evibe.in</a>
									</div>
								</div>
							</div>
							<div class="link">
								<a href="/feedback?ref=footer">Give Feedback</a>
							</div>
							<div class="link mar-t-8">
								<a href="{{ route("track.orders") }}">Track My Order</a>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
@section("footer")
	<div class="footer-wrap">
		@include('base.home.footer.footer-top')
		@include('base.home.footer.footer-common')
	</div>
@endsection