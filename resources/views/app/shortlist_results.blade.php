<div class="partybag">
	<a class="partybag-heart shortlistIcon" v-on:click="shortlistOptionFromListPage" data-plainImg="{{ $galleryUrl }}/main/img/icons/party-bag_empty.png" data-filledImg="{{ $galleryUrl }}/main/img/icons/party-bag_filled.png" data-removeImg="{{ $galleryUrl }}/main/img/icons/party-bag_remove.png">
		@if (($agent->isMobile() && !($agent->isTablet())))
			<span class="icon-bagicon5"></span>
		@endif
		<span class="hideShortlistResultsData" data-mapId="{{ $mapId }}" data-mapTypeId="{{ $mapTypeId }}" data-cityId="{{ $cityId }}" data-occasionId="{{ $occasionId }}" data-urlAdd="{{ route('partybag.add') }}" data-urlDelete="{{ route('partybag.delete') }}">
		</span>
	</a>
</div>