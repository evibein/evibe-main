<?php

/**
 * SMS configuration
 *
 * @author Anji <anji@evibe.in>
 * @since  31 Dec 2014
 */

return [

	/*
	|--------------------------------------------------------------------------
	| Login Credentials
	|--------------------------------------------------------------------------
	*/
	'username'         => 'anji@evibe.in',
	'password'         => '123qwe',

	/*
	|--------------------------------------------------------------------------
	| Android App Login Credentials
	|--------------------------------------------------------------------------
	*/
	'gateway_username' => 'anji.t6@gmail.com',
	'gateway_password' => 'smsgatewayapi',
	'gateway_deviceid' => 8713,

	/*
	|--------------------------------------------------------------------------
	| Sender Id
	|--------------------------------------------------------------------------
	|
	| New Sender Id needs approval. If requested for route "T", default
	| template message with palceholders must be provided.
	|
	| Defaults available:
	| (1) SMSIND - route "T" with a default template
	| (2) PROMOTIONAL - route "P"
	| (3) EVIBES - route "T" (old: EEVIBE)
	|
	*/
	'sender_id'        => 'EVIBES',

	/*
	|--------------------------------------------------------------------------
	| SMS Type
	|--------------------------------------------------------------------------
	| 
	| T = Transactional (need approval)
	| P/PS = Promotional
	|
	*/
	'route'            => [
		"transactional" => 'T',
		"promotional"   => 'PS'
	]
];