<?php

return [
	'hotjar' => [
		"id" => env("HOTJAR_ID", '234393'),
		"sv" => env("HOTJAR_SV", '5')
	],

	'tawkTo' => [
		"url" => env("TAWKTO_URL", "https://embed.tawk.to/5826ada94172980c5e7e2bf1/default")
	]
];