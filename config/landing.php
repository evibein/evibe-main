<?php

return [

	'media' => [
		'toi'      => [
			'img'   => 'toi.png',
			'title' => 'Times of India'
		],
		'dc'       => [
			'img'   => 'dc.png',
			'title' => 'Deccan Chronicle'
		],
		'sakshi'   => [
			'img'   => 'sakshi.png',
			'title' => 'Sakshi'
		],
		'ie'       => [
			'img'   => 'indian-express.png',
			'title' => 'Indian Express'
		],
		'nt'       => [
			'img'   => 'namaste-telangana.png',
			'title' => 'Namaste Telangana'
		],
		'bonnevie' => [
			'img'   => 'bonnevie.png',
			'title' => 'Bonnevie'
		],
		'tia'      => [
			'img'   => 'techinasia.png',
			'title' => 'Tech In Asia'
		],
		'vcc'      => [
			'img'   => 'vccircle.png',
			'title' => 'VCCircle'
		],
		'ys'       => [
			'img'   => 'your-story.png',
			'title' => 'Your Story'
		],
		'nbw'      => [
			'img'   => 'next-big-what.png',
			'title' => 'Next Big What'
		],
		'inc42'    => [
			'img'   => 'inc42.png',
			'title' => 'Inc42'
		]
	],

	'birthday-party-decorations-hyderabad' => [
		'cityId'       => 2,
		'event-id'     => 1,
		'redirect-url' => 'https://evibe.in/hyderabad/birthday-party-planners/birthday-party-decorations/all',
		'page-title'   => 'Best Birthday Party Balloons, Theme Decorations From Top Birthday Party Planners In Hyderabad',
		'heroImage'    => '/main/landing/birthday-party-decorations/landingCollage.png',
		'header-tag'   => 'Your Birthday Party Planner',
		'headline-1'   => 'EXPLORE 100s OF AMAZING BIRTHDAY PARTY DECORATIONS. BEST DEALS GUARANTEED.',
		'sub-heading'  => 'Simple balloon decorations for home to elegant birthday party theme decorations, book at best prices with on-time & quality service from Top Birthday Party Planners in Hyderabad.',
		'form-title'   => 'Fill simple form below. See real decoration pictures, complete inclusions and customer reviews.',
		'cta-button'   => 'EXPLORE DECORS NOW',
		'sneak-peaks'  => [
			[
				'img'   => '/main/landing/birthday-party-decorations/sneak-peaks/1.png',
				'title' => 'Simple Balloon Decorations'
			],
			[
				'img'   => '/main/landing/birthday-party-decorations/sneak-peaks/2.png',
				'title' => 'Drapes Decorations'
			],
			[
				'img'   => '/main/landing/birthday-party-decorations/sneak-peaks/3.png',
				'title' => 'Elegant Theme Decorations'
			],
			[
				'img'   => '/main/landing/birthday-party-decorations/sneak-peaks/4.png',
				'title' => 'Craft Decorations'
			]
		],
		'meta'         => [
			'description'    => 'Top birthday party planners in Hyderabad',
			'key-words'      => 'birthday-party-planners, birthday-party-decorations', 'birthday-decoration-planners',
			'og-title'       => 'Your birthday party planner',
			'op-description' => 'How can you make the most of your birthday party through Evibe.in?',
			'og-url'         => 'https://evibe.in/ga/landing/birthday-party-decorations',
			'canonical'      => '/ga/landing/birthday-party-decorations'
		],
		'benefits'     => [
			'1' => 'Wide range of options',
			'2' => 'Best prices with quality service',
			'3' => 'Secure & hassle free booking',
			'4' => 'Friendly customer support'
		],
		'stats'        => [
			'1' => [
				'number' => '6K+',
				'text'   => 'Birthday Parties Booked'
			],
			'2' => [
				'number' => '5K+',
				'text'   => 'Real Decoration Pictures'
			],
			'3' => [
				'number' => '800K+',
				'text'   => 'Balloons Blown'
			],
			'4' => [
				'number' => '128K+',
				'text'   => 'Social Media Followers'
			]
		]
	],

	'birthday-party-decorations-bangalore' => [
		'cityId'       => 1,
		'event-id'     => 1,
		'redirect-url' => 'https://evibe.in/bangalore/birthday-party-planners/birthday-party-decorations/all',
		'page-title'   => 'Best Birthday Party Balloon, Theme Decorations From Top Birthday Party Planners In Bangalore',
		'heroImage'    => '/main/landing/birthday-party-decorations/landingCollage.png',
		'header-tag'   => 'Your Birthday Party Planner',
		'headline-1'   => 'EXPLORE 100s OF AMAZING BIRTHDAY PARTY DECORATIONS. BEST DEALS GUARANTEED.',
		'sub-heading'  => 'Simple balloon decorations for home to elegant birthday party theme decorations, book at best prices with on-time & quality service from Top Birthday Party Planners in Bangalore.',
		'form-title'   => 'Fill simple form below. See real decoration pictures, complete inclusions and customer reviews.',
		'cta-button'   => 'EXPLORE DECORS NOW',
		'sneak-peaks'  => [
			[
				'img'   => '/main/landing/birthday-party-decorations/sneak-peaks/1.png',
				'title' => 'Simple Balloon Decorations'
			],
			[
				'img'   => '/main/landing/birthday-party-decorations/sneak-peaks/2.png',
				'title' => 'Drapes Decorations'
			],
			[
				'img'   => '/main/landing/birthday-party-decorations/sneak-peaks/3.png',
				'title' => 'Elegant Theme Decorations'
			],
			[
				'img'   => '/main/landing/birthday-party-decorations/sneak-peaks/4.png',
				'title' => 'Craft Decorations'
			]
		],
		'meta'         => [
			'description'    => 'Top birthday party planners in Bangalore',
			'key-words'      => 'birthday-party-planners, birthday-party-decorations', 'birthday-decoration-planners',
			'og-title'       => 'Your birthday party planner',
			'op-description' => 'How can you make the most of your birthday party through Evibe.in?',
			'og-url'         => 'https://evibe.in/ga/landing/birthday-party-decorations',
			'canonical'      => '/ga/landing/birthday-party-decorations'
		],
		'benefits'     => [
			'1' => 'Wide range of options',
			'2' => 'Best prices with quality service',
			'3' => 'Secure & hassle free booking',
			'4' => 'Friendly customer support'
		],
		'stats'        => [
			'1' => [
				'number' => '6K+',
				'text'   => 'Birthday Parties Booked'
			],
			'2' => [
				'number' => '5K+',
				'text'   => 'Real Decoration Pictures'
			],
			'3' => [
				'number' => '800K+',
				'text'   => 'Balloons Blown'
			],
			'4' => [
				'number' => '128K+',
				'text'   => 'Social Media Followers'
			]
		]
	],

	'ads' => [
		"base_price" => 4500,
		"options"    => [
			"City"    => [
				'Bengaluru' => "blr",
				'Hyderabad' => "hyd",
				'New Delhi' => "ndl",
				'Mumbai'    => "mum",
				'Pune'      => "pun",
				'Chennai'   => "chn"
			],
			"Theatre" => [
				"Single Screen" => "single_screen",
				"Inox"          => "inox",
				"PVR"           => "pvr"
			]
		],
		"prices"     => [
			"blr" => [
				"inox"          => 7000,
				"pvr"           => 7800,
				"single_screen" => 4500
			],
			"hyd" => [
				"inox"          => 7000,
				"pvr"           => 7800,
				"single_screen" => 4500
			],
			"ndl" => [
				"inox"          => 7000,
				"pvr"           => 7800,
				"single_screen" => 4500
			],
			"mum" => [
				"inox"          => 7000,
				"pvr"           => 7800,
				"single_screen" => 4500
			],
			"pun" => [
				"inox"          => 7000,
				"pvr"           => 7800,
				"single_screen" => 4500
			],
			"chn" => [
				"inox"          => 7000,
				"pvr"           => 7800,
				"single_screen" => 4500
			]
		]
	]
];