<?php

return [
	'options' => [
		env('CITY_BANGALORE_ID', 1) => [

			env('TYPE_EVENT_BIRTHDAY', 1) => [],

			env('TYPE_EVENT_BACHELOR', 11) => [],

			env('TYPE_EVENT_PRE_POST', 13) => [],

			env('TYPE_EVENT_HOUSE_WARMING', 14) => [],

			env('TYPE_EVENT_SURPRISES', 16) => [
				1 => [
					781,
					607,
					755,
					373,
					1388,
					870,
					2225,
					199,
					2256,
					2253,
					836,
					2255,
					2258,
					743,
					2267,
					2270,
					812,
					2170
				]
			],

			env('TYPE_EVENT_CORPORATES', 5) => []

		],

		env('CITY_HYDERABAD_ID', 2) => [

			env('TYPE_EVENT_BIRTHDAY', 1) => [],

			env('TYPE_EVENT_BACHELOR', 11) => [],

			env('TYPE_EVENT_PRE_POST', 13) => [],

			env('TYPE_EVENT_HOUSE_WARMING', 14) => [],

			env('TYPE_EVENT_SURPRISES', 16) => [
				1 => [
					2272,
					2238,
					1849,
					2263,
					1676,
					1768,
					1851,
					872,
					2177,
					2171,
					573,
					2264,
					827
				]
			],

			env('TYPE_EVENT_CORPORATES', 5) => []

		],

		env('CITY_DELHI_ID', 3) => [],

		env('CITY_MUMBAI_ID', 4) => [],

		env('CITY_PUNE_ID', 5) => []
	]
];