<?php

return [

	// TYPE_CHANGE_INCREASE = 1
	// TYPE_CHANGE_DECREASE = 2

	"defaults" => [],

	env('CITY_BANGALORE_ID', 1) => [
		"defaults"                     => [],
		"event" => [
			env('TYPE_EVENT_BIRTHDAY', 1)  => [],
			env('TYPE_EVENT_BACHELOR', 11) => [
				"defaults"                 => [],
				"category" => [
					env('TYPE_PAGE_VILLA', 18) => [
						"defaults" => [
							"defaults" => [
								//"percent" => 10,
								//"type"    => 1
							],
							"day"      => [
								env('TYPE_DAY_MONDAY', 1)    => [
									//"percent" => 5,
									//"type" => 2
								],
								env('TYPE_DAY_TUESDAY', 2)   => [
									//"percent" => 5,
									//"type" => 2
								],
								env('TYPE_DAY_WEDNESDAY', 3) => [
									//"percent" => 5,
									//"type" => 2
								],
								env('TYPE_DAY_THURSDAY', 4)  => [
									//"percent" => 5,
									//"type" => 1
								],
								env('TYPE_DAY_FRIDAY', 5)    => [
									//"percent" => 10,
									//"type" => 1
								],
								env('TYPE_DAY_SATURDAY', 6)  => [
									//"percent" => 10,
									//"type" => 1
								],
								env('TYPE_DAY_SUNDAY', 7)    => [
									//"percent" => 5,
									//"type" => 1
								],
							],
							"date"     => [
								// format - m/d/y
								"03/05/2019" => [
									//"percent" => 10,
									//"type" => 1
								]
							],
						],
						"id" => [
							865 => [
								"defaults" => [

								],
								"day"      => [

								],
								"date"     => [
									"05/03/2019" => [
										//"percent" => 10,
										//"type" => 1
									]
								],
							]
						]
					]
				],
			]
		],
	],

	env('CITY_HYDERABAD_ID', 2) => [],

];