<?php

/*
 * Format to be followed:
 *
 * If the array given is [a, b, c, d]
 * Price category ranges will be shown as follows:
 *
 *    [
 *      below a
 *      a - b
 *      b - c
 *      c - d
 *      above d
 *    ]
 *
 * Note: Kindly ensure that "a > MIN(option_price)" && "d < MAX(option_price)"
 */

return [

	"defaults"                         => [5000, 10000, 20000, 40000],

	// @todo: example of how this works

	/* birthday */
	env('TYPE_EVENT_BIRTHDAY', 1)      => [

		"defaults"                 => [5000, 10000, 20000, 40000],

		/* decors */
		env('TYPE_PAGE_DECOR', 13) => [5000, 10000, 20000, 40000],

		/* food */
		env('TYPE_PAGE_FOOD', 20)  => [4000, 8000, 16000, 24000],

		/* ent */
		env('TYPE_PAGE_ENT', 14)   => [2000, 2500, 3500, 5000],

		/* package */
		env('TYPE_PAGE_PACKAGE', 1)   => [5000, 10000, 20000, 40000],
	],

	/* Youth party */
	env('TYPE_EVENT_BACHELOR', 11)     => [

		"defaults"                  => [16000, 19000, 22000, 26000],

		/* decors */
		env('TYPE_PAGE_DECOR', 13)  => [5000, 10000, 20000, 40000],

		/* villas */
		env('TYPE_PAGE_VILLA', 18)  => [8000, 12000, 16000, 24000],

		/* resorts */
		env('TYPE_PAGE_RESORT', 17) => [8000, 12000, 16000, 24000],

		/* lounges */
		env('TYPE_PAGE_LOUNGE', 19) => [16000, 19000, 22000, 26000],

		/* food */
		env('TYPE_PAGE_FOOD', 20)   => [8000, 12000, 16000, 20000],

		env('TYPE_PAGE_ENT', 14) => [2000, 2500, 3500, 5000],

	],

	/* pre post */
	env('TYPE_EVENT_PREPOST', 13)      => [

		"defaults"                 => [5000, 10000, 20000, 40000],

		/* decors */
		env('TYPE_PAGE_DECOR', 13) => [5000, 10000, 20000, 40000],

	],

	/* house warming */
	env('TYPE_EVENT_HOUSEWARMING', 14) => [

		"defaults"                 => [5000, 10000, 20000, 40000],

		/* decors */
		env('TYPE_PAGE_DECOR', 13) => [5000, 10000, 20000, 40000],

		/* food */
		env('TYPE_PAGE_FOOD', 20)  => [4000, 8000, 16000, 24000],

	],

	/* surprises */
	env('TYPE_EVENT_SURPRISES', 16)    => [

		"defaults"                            => [8000, 12000, 16000, 24000],

		/* surprise-packages */
		env('TYPE_PAGE_SURPRISE_PACKAGE', 21) => [8000, 12000, 15000, 18000],

	],

];