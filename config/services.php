<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, SparkPost and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => env('MAILGUN_DOMAIN'),
		'secret' => env('MAILGUN_SECRET'),
	],

	'mandrill' => [
		'secret' => env('MANDRILL_SECRET'),
	],

	'ses' => [
		'key'    => env('AWS_ACCESS_KEY_ID'),
		'secret' => env('AWS_SECRET_ACCESS_KEY'),
		'region' => env('AWS_REGION', 'us-east-1'),
	],

	'stripe' => [
		'model'  => App\User::class,
		'key'    => env('STRIPE_KEY'),
		'secret' => env('STRIPE_SECRET'),
	],

	'facebook' => [
		'client_id'     => env('FACEBOOK_LOGIN_CLIENT_ID', '317100052022250'),
		'client_secret' => env('FACEBOOK_LOGIN_CLIENT_SECRET', '2c2dcc9d0e6a20a492b11b41a7022c3a'),
		'redirect'      => env('FACEBOOK_LOGIN_REDIRECT', 'https://evibe.in/callback/facebook'),
	],

	'google' => [
		'client_id'     => env('GOOGLE_LOGIN_CLIENT_ID', '418270435702-bt46judcmialnnldi3v1b4uiviusc29p.apps.googleusercontent.com'),
		'client_secret' => env('GOOGLE_LOGIN_CLIENT_SECRET', 'rcz7btU8lCePNTUvNxJO2HgN'),
		'redirect'      => env('GOOGLE_LOGIN_REDIRECT', 'https://evibe.in/callback/google')
	]
];