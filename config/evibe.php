<?php

return [

	'host'         => env('LIVE_HOST', 'https://evibe.in'),
	'dash_host'    => env('DASH_HOST', 'http://dash.evibe.in'),
	'store_host'   => env('STORE_HOST', 'https://store.evibe.in'),
	'youtube_host' => env('YOUTUBE_BASE_URL', 'https://www.youtube.com/watch?html5=1&v='),

	'banner-quotes' => [
		'ganesh_sub_heading' => "Celebrate With Joy And Devotion",
	],

	/*
	|-------------------------------------------------------------------------
	| APIs
	|--------------------------------------------------------------------------
	*/
	'google'        => [
		're_cap_site'     => env('RE_CAP_SITE'),
		're_cap_secret'   => env('RE_CAP_SECRET'),
		'map_key'         => env('GOOGLE_MAP_KEY'),
		'dist_matrix_key' => env('GOOGLE_DIST_MATRIX_KEY'),
		'places_key'      => env('GOOGLE_PLACES_KEY'),
		'reviews-key'     => env('GOOGLE_REVIEWS_KEY', 'AIzaSyAAQz3oD9D4BUOMSEa5JzrIIHlR9PlHC5k'),
		'place-id'        => [
			'bangalore-office' => env('GOOGLE_PLACE_ID_BAN_OFFICE', 'ChIJyXEg3WURrjsR8ykTL0yrmyE')
		],
		'reviews'         => [
			'min-rating'     => env('GOOGLE_REVIEW_MIN_RATING', 3),
			'min-avg-rating' => env('GOOGLE_REVIEW_MIN_AVG_RATING', 4),
			'rating'         => env('GOOGLE_REVIEW_RATING'),
			'count'          => env('GOOGLE_REVIEW_COUNT'),
			'link'           => env('GOOGLE_REVIEW_LINK', ' https://bit.ly/2Q888Om'),
		],
		'shortener_key'   => env('GOOGLE_SHORTENER_KEY')
	],

	'inspiration-hub' => [
		'image-path' => '/main/img/inspiration-hub/ideas/',
	],

	'custom-tags-list-page'       => [
		1 => 'Luxury',
		2 => 'Limited Edition',
		3 => 'Best Selling',
		4 => 'Trending'
	],
	'lockdown-landing-pages' => [
		'pageData'	 => [
			'carousel-img-desktop' => 'header-lockdown.png',
			'carousel-img-mobile'  => 'header-lockdown-mbl.png',
			'gallery-path'	       => 'main/lockdown-landingpage',

		],
		'bangalore'	 => [
			'surprises'	  => [
				'card-img-url'	 => 'surprises.png',
				'campaignUrl'	 => 'quarantine-surprises-in-bangalore'
			],
			'birthday-deocations'	  => [
				'card-img-url'	 => 'decorations-bdy.png',
				'campaignUrl'	 => 'lockdown-decors-in-bangalore'
			],
			'birthday-cakes'	  => [
				'card-img-url'	 => 'cakes-bdy.png',
				'campaignUrl'	 => 'lockdown-cakes-in-bangalore'
			],

			'housewarming-decorations'	  => [
				'card-img-url'	 => 'housewarming.png',
				'campaignUrl'	 => 'lockdown-housewarming-decorations-in-bangalore'
			],
			'housewarming-priests'	  => [
				'card-img-url'	 => 'priest.png',
				'campaignUrl'	 => 'lockdown-priests'
			],

		],
		'hyderabad'	 => [
			'surprises'	  => [
				'card-img-url'	 => 'surprises.png',
				'campaignUrl'	 => 'quarantine-surprises-in-hyderabad'
			],
			'birthday-deocations'	  => [
				'card-img-url'	 => 'decorations-bdy.png',
				'campaignUrl'	 => 'lockdown-decors-in-hyderabad'
			],

		],
	],

	'virtual-party' => [
		'upload-types' => [
			'image'  => '0',
			'text'	 => '1',
			'audio'	 => '2',
			'video'	 => '3',
		],
		'birthday-reviews' => [
			'1'	=> [
				'author' => 'Ganesh, Entrepreneur, Hyderabad',
				'review' => 'All our guests from different places enjoyed my son’s 7th birthday. I’m thinking of hosting virtual birthday parties even after lockdown, they are so much fun.',
				'feature-pic' => 'Ganesh2.webp'
			],
			'2'	=> [
				'author' => 'Chetan, Engineer, USA',
				'review' => 'It’s the first time I\'ve joined a virtual party. Never imagined an anchor will make so much difference. Totally loved the entire experience and I felt it was worth $300 - $500.',
				'feature-pic' => 'Chetan2.webp'
			],
			'3'	=> [
				'author' => 'Varsha, Business, Mumbai',
				'review' => 'We arranged a virtual birthday party for my son . He enjoyed it a lot and was very happy to have celebrated his birthday with his friends even in these times of lockdown . As it was a surprise for Nihal he was stumped. Thank you evibe so much....🥰',
				'feature-pic' => 'varsha2.webp'
			],
			'4'	=> [
				'author' => 'Yuti, Student, Mumbai',
				'review' => 'Just wow! It was a surprise birthday party for me. Evibe and all my friends made it super special. Thank you very much guys 😍😍 ',
				'feature-pic' => 'Yuti2.webp'
			],
			'5'	=> [
				'author' => 'Misha, 4th Grade, Bangalore',
				'review' => 'I attended didi’s virtual birthday and liked it very much. I asked mumma for the same party for my birthday too 🙂',
				'feature-pic' => 'misha2.webp'
			],
		],
		'birthday-pricings' => [
			'packages' => [
				'india'	 => [
					'package-one-worth' => '₹1999',
					'package-one-price' => '₹999',
					'package-two-worth' => '₹4999',
					'package-two-price' => '₹2499',
					'package-three-price' => '₹3499',
					'package-three-worth' => '₹5999',
				],
				'abroad'	 => [
					'package-one-worth' => '$79',
					'package-one-price' => '$39',
					'package-two-worth' => '$199',
					'package-two-price' => '$99',
				],


			],
			'addons'	=> [
				'1'	 => [
					'title'	 => 'Virtual  Santa',
					'ind-price'	 => '₹1999',
					'us-price'	 => '$79',
					'is-live-US'	=> '1',
					'description'	=> "For this christmas, Santa wants to meet you on screen & tell you short story. He will sing and dance with you for your favorite songs for 20 min.   ",

				],
				'2'	 => [
					'title'	 => 'Magic Show',
					'ind-price'	 => '₹2000',
					'us-price'	 => '$79',
					'is-live-US'	=> '1',
					'description'	=> 'Mesmerise your guests with fun magic performances for 20-30 mins to entertain kids & adults at like',

				],
				'3'	 => [
					'title'	 => 'Video Recording',
					'ind-price'	 => '₹500',
					'us-price'	 => '$39',
					'is-live-US'	=> '1',
					'description'	=> 'Get the entire party raw recording delivered in 5 business days post party. If you want to manage this yourself, we can make a co-host to get this done.',

				],
				'4'	 => [
					'title'	 => 'Dance Party ',
					'ind-price'	 => '₹2000',
					'us-price'	 => '$79',
					'is-live-US'	=> '1',
					'description'	=> 'Get a dance pro to get your guests moving and also teach everyone to do fun and easy party dances for 20 minutes.',

				],
				'5'	 => [
					'title'	 => 'Unique Birthday Greetings',
					'ind-price'	 => '₹1000',
					'us-price'	 => '$39',
					'is-live-US'	=> '1',
					'description'	=> "Ventriloquist Puppeteer Josef wishing the child with his Muppet and singing a song, joke and live chat according to the child's age. Duration: 2 - 5 min and suitable for ages 3 and up",

				],
				'6'	 => [
					'title'	 => 'Live Puppet Show & Magic',
					'ind-price'	 => '₹3000',
					'us-price'	 => '$79',
					'is-live-US'	=> '1',
					'description'	=> 'Live Puppet show and magic by Ventriloquist Josef - includes magic birthday song. Duration 20 min. Suitable for ages 3 - 7',

				],
				'7'	 => [
					'title'	 => 'Virtual Backdrop',
					'ind-price'	 => '₹1000',
					'us-price'	 => '$39',
					'is-live-US'	=> '1',
					'description'	=> 'Get your party decorations covered with virtual birthday theme backdrop and balloons',

				],
				'8'	 => [
					'title'	 => 'Take Over',
					'ind-price'	 => '₹1000',
					'us-price'	 => '$39',
					'is-live-US'	=> '1',
					'description'	=> 'Take over for an additional one hour after the party to have some family conversations or play games yourselves.',

				],
				'9'	 => [
					'title'	 => 'Female Anchor',
					'ind-price'	 => '₹500',
					'us-price'	 => '$39',
					'is-live-US'	=> '1',
					'description'	=> 'Get a female anchor to welcome, greet and entertain your guests and the birthday child. These are extra charges',

				],
				'10'	 => [
					'title'	 => 'Language Preference',
					'ind-price'	 => '₹1000',
					'us-price'	 => '$39',
					'is-live-US'	=> '1',
					'description'	=> 'Get an anchor who speaks your mother tongue to have local masti.',

				],
				'11'	 => [
					'title'	 => 'Harry potter show',
					'ind-price'	 => '₹2000',
					'us-price'	 => '$79',
					'is-live-US'	=> '1',
					'description'	=> 'A special artist whose place is decked up with harry potter theme will showcase the tour, read short character highlights and do a quiz for about 20 minutes.',

				],
				'12'	 => [
					'title'	 => 'More Fun',
					'ind-price'	 => '',
					'us-price'	 => '',
					'is-live-US'	=> '1',
					'description'	=> 'Other entertainment activities like Digital Caricature, Puppet Show, Clown Show, Craft Activity, etc, are available on request',

				],
			]
		]
	],
	/*
	|-------------------------------------------------------------------------
	| Landingpages URLs should be in match with DB urls
	|--------------------------------------------------------------------------
	*/
	'occasion-landingpages'       => [
		'surprises'       => [
			'gallery-path'       => 'main/occasion-home/surprises',
			'filters-tags'       => [
				'home-decorations' => '185',
				'trending-dates'   => '265',
			],
			'trending-dates-ids' => [
				'ice-cream-date' => '264',
				'coffee-date'    => '263',
			],
			'collections_ids'    => [
				'trending-surprises' => "1",
			],
		],
		'birthday-decors' => [
			'balloon-decoration' => [
				"id"  => '2',
				"url" => 'balloon-decorations',
				"ids" => [
					'21' => 'simple-decorations',
					'51' => 'customised-decorations',
				],
			],
			'theme-decorations'  => [
				"id"  => '3',
				"url" => 'theme-decorations',
				"ids" => [
					'70'  => 'basic-theme-decorations',
					'238' => 'medium-theme-decorations',
					'239' => 'high-end-theme-decorations'
				],
			],
			'craft-decorations'  => [
				"id"  => '4',
				"url" => 'craft-decorations',
				"ids" => [
					'95'  => 'exclusive-drapes-decorations',
					'122' => 'craft-decors',
				],
			],
		],
		'birthday-cakes'  => [
			'standard-cakes' => [
				"id"  => '5',
				"url" => 'standard-cakes',
				"ids" => [
					'105' => 'standard-cakes',

				],
			],
			'fondant-cakes'  => [
				"id"  => '6',
				"url" => 'fondant-cakes',
				"ids" => [
					'106' => 'fondant-cakes',

				],
			],
			'photo-cakes'    => [
				"id"  => '7',
				"url" => 'photo-cakes',
				"ids" => [
					'107' => 'photo-cakes',

				],
			],
		],
		'birthday-food'   => [
			'snacks'  => [
				"id"  => '8',
				"url" => 'snacks-combo',
				"ids" => [
					'39' => 'snacks',

				],
			],
			'veg'     => [
				"id"  => '9',
				"url" => 'veg-combo',
				"ids" => [
					'76' => 'veg',

				],
			],
			'non-veg' => [
				"id"  => '10',
				"url" => 'non-veg-combo',
				"ids" => [
					'77' => 'non-veg',

				],
			],

		],
		'birthday-ent'    => [
			'eateries'     => [
				"id"  => '11',
				"url" => 'eateries',
				"ids" => [
					'190' => 'eateries',

				],
			],
			'game-stalls'  => [
				"id"  => '12',
				"url" => 'game-stalls',
				"ids" => [
					'191' => 'game-stalls',

				],
			],
			'entertainers' => [
				"id"  => '13',
				"url" => 'entertainers',
				"ids" => [
					'192' => 'entertainers',

				],
			],
			'artists'      => [
				"id"  => '14',
				"url" => 'artists',
				"ids" => [
					'193' => 'artists',

				],
			],
			'rentals'      => [
				"id"  => '15',
				"url" => 'rentals',
				"ids" => [
					'194' => 'rentals',

				],
			],
			'photo-video'  => [
				"id"  => '16',
				"url" => 'photo-video',
				"ids" => [
					'195' => 'photo-video',

				],
			],
			'sound-lights' => [
				"id"  => '17',
				"url" => 'sound-lights',
				"ids" => [
					'196' => 'sound-lights',

				],
			],
			'new-trends'   => [
				"id"  => '18',
				"url" => 'new-trends',
				"ids" => [
					'197' => 'new-trends',

				],
			],

		],
	],

	/*
	|-------------------------------------------------------------------------
	| Landingpages URLs should be in match with DB urls
	|--------------------------------------------------------------------------
	*/
	'valentines-day-landingpages' => [
		'gallery-path'       => 'main/landingpages/cards',
		'header-image-desk'  => 'https://gallery.evibe.in/main/img/home/vday-2020-cld-carousel.png',
		'offers-expire-date' => env('VALENTINE_DAY_OFFER_EXPIRE'),
		'coupon-code'        => env('VALENTINE_DAY_COUPON_CODE'),
		'max-limit'          => env('VALENTINE_DAY_COUPON_MAX_LIMIT'),
		'reviews'            => [
			1 => [
				"author"  => "Nizar Pasha",
				"review"  => "One of the finest surprise party organisers. Very cooperative team with commitment to deliver on time.",
				"pic-url" => "https://lh5.googleusercontent.com/-f1QUHtpUICg/AAAAAAAAAAI/AAAAAAAAAAA/DDFpaoaQpc8/s40-c-rp-mo-br100/photo.jpg",
			],
			2 => [
				"author"  => "Vaishnavi",
				"review"  => "The decoration was awesome.food was really very tasty. Had a great time.",
				"pic-url" => "https://lh5.googleusercontent.com/-XYfWii9G5gg/AAAAAAAAAAI/AAAAAAAAAAA/L8MCO93Q98M/s40-c-rp-mo-br100/photo.jpg",
			],
			3 => [
				"author"  => "Kiran Patel",
				"review"  => "It was effortless and smooth experience. Exactly shown as images",
				"pic-url" => "https://lh6.googleusercontent.com/-rKKXepO56Bw/AAAAAAAAAAI/AAAAAAAAAAA/UiMbGV4eP0o/s40-c-rp-mo-ba2-br100/photo.jpg",
			],
			4 => [
				"author"  => "Kiran Patel",
				"review"  => "It was effortless and smooth experience. Exactly shown as images",
				"pic-url" => "https://lh3.googleusercontent.com/-P1WoDBycrKc/AAAAAAAAAAI/AAAAAAAAAAA/1d1VXuWUQGo/s40-c-rp-mo-br100/photo.jpg",
			],
			4 => [
				"author"  => "Shubhank",
				"review"  => "The best solution and ultimate customer satisfaction and fair on price point.",
				"pic-url" => "https://lh6.googleusercontent.com/-NshWqGOY2uk/AAAAAAAAAAI/AAAAAAAAAAA/Tp2UnKJ73eg/s40-c-rp-mo-ba2-br100/photo.jpg",
			],

		],
		'for-hyderabad'      => [
			"url"          => "valentine-day-specials-in-hyderabad",
			"campaignUrls" => [
				"1" => [
					'url'     => 'valentines-day-2020-romantic-candlelight-dinner-hyderabad',
					'heading' => 'Top CandleLight Dinners'
				],
				"2" => [
					'url'     => 'valentines-day-2020-special-surprise-home-decorations-hyderabad',
					'heading' => 'Top Surprise Decorations'
				],
				"3" => [
					'url'     => 'valentines-day-2020-trending-dates-hyderabad',
					'heading' => 'Trending Dates',
				],
				"4" => [
					'url'     => 'valentines-day-2020-unique-surprises-hyderabad',
					'heading' => 'Unique Surprises',
				],
				"5" => [
					'url'     => 'valentines-day-2020-staycations-hyderabad',
					'heading' => 'Staycations',
				],
			],
		],
		'for-bangalore'      => [
			"url"          => "valentine-day-specials-in-bangalore",
			"campaignUrls" => [
				"1" => [
					'url'     => 'valentines-day-2020-romantic-candlelight-dinner-bangalore',
					'heading' => 'Top CandleLight Dinners'
				],
				"2" => [
					'url'     => 'valentines-day-2020-special-surprise-home-decorations-bangalore',
					'heading' => 'Top Surprise Decorations'
				],
				"3" => [
					'url'     => 'valentines-day-2020-trending-dates-bangalore',
					'heading' => 'Trending Dates',
				],
				"4" => [
					'url'     => 'valentines-day-2020-unique-surprises-bangalore',
					'heading' => 'Unique Surprises',
				],
				"5" => [
					'url'     => 'valentines-day-2020-staycations-bangalore',
					'heading' => 'Staycations',
				],
			],
		],
		'for-pune'           => [
			"url"          => "valentine-day-specials-in-pune",
			"campaignUrls" => [
				"1" => [
					'url'     => 'valentines-day-2020-surprise-ideas-pune',
					'heading' => 'Top Surprise Ideas'
				],
			],
		],
		'for-delhi'          => [
			"url"          => "valentine-day-specials-in-delhi",
			"campaignUrls" => [
				"1" => [
					'url'     => 'valentines-day-2020-surprise-ideas-delhi',
					'heading' => 'Top Surprise Ideas'
				],
			],
		],
		'for-mumbai'         => [
			"url"          => "valentine-day-specials-in-mumbai",
			"campaignUrls" => [
				"1" => [
					'url'     => 'valentines-day-2020-surprise-ideas-mumbai',
					'heading' => 'Top Surprise Ideas'
				],
			],
		],
		'for-chennai'        => [
			"url"          => "valentine-day-specials-in-chennai",
			"campaignUrls" => [
				"1" => [
					'url'     => 'valentines-day-2020-surprise-ideas-chennai',
					'heading' => 'Top Surprise Ideas'
				],
			],
		]
	],

	'bitly' => [
		'access_token'     => env('BITLY_ACCESS_TOKEN'),
		'alt_access_token' => env('BITLY_ALT_ACCESS_TOKEN'),
	],

	'evibes' => [
		'access_token' => env('EVIBES_SHORTEN_ACCESS_TOKEN', "11feae8ebff7ca417beab3586e0f8a")
	],

	'delhivery' => [
		'pincode-serviceability' => "https://track.delhivery.com/c/api/pin-codes/json/",
	],

	'api'                  => [
		'base_url'         => env('API_BASE_URL', 'http://apis.evibe.in/'),
		'client_id'        => env('MAIN_CLIENT_ID'),
		'partner'          => [
			'prefix' => 'partner/v1/',
		],
		'workflow'         => [
			'prefix' => 'common/plan/v1'
		],
		'delivery_images'  => [
			'prefix' => 'common/partner/delivery-images'
		],
		'referandearn'     => [
			'prefix' => 're/v1'
		],
		'pay_notification' => 'payment/notification/',
		'product'          => [
			'availability-check' => 'product/availability-check'
		],
		'auto-followups'   => [
			'prefix' => 'common/auto-followups/v1/'
		],
		'finance'          => [
			'prefix'        => 'common/finance/v1/',
			'cancellations' => [
				'prefix' => 'common/finance/v1/cancellations/bookings' // @see: bookings is included
			],
			'settlements'   => [
				'prefix'       => 'common/finance/v1/settlements',
				'settled-list' => 'settled'
			],
		],
		'aws'              => [
			'prefix' => 'aws/v1/',
			'sns'    => 'aws/v1/sns/',
		],
		'avail-check'      => [
			'prefix' => 'common/partner/avail-check'
		]
	],
	'zoomcar_referrer'     => env('ZOOMCAR_REFERRER'),
	'christmas_santa_gift' => 'https://www.townscript.com/e/evibein-santa-wishes-412104',
	'partyOneBearerCode'   => env('PARTY_ONE_BEARER_CODE'),

	/*
	|-------------------------------------------------------------------------
	| URLs for results
	|--------------------------------------------------------------------------
	*/
	'results_url'          => [
		'decors'          => 'decorations',
		'packages'        => 'packages',
		'trends'          => 'trends',
		'venues'          => 'venues',
		'party_halls'     => 'party-halls',
		'planners'        => 'planners',
		'experiences'     => 'unique-celebration-experiences',
		'cakes'           => 'cakes',
		'entertainment'   => 'entertainment-activities-stalls',
		'addons'          => 'photography-videos-fun-eats',
		'venue_deals'     => 'venue-deals',
		'campaign'        => [],
		'food'            => 'food',
		'add_ons'         => 'add-ons',
		'kids_play_areas' => 'kids-play-areas'
	],
	'profile_url'          => [
		'decors'        => 'decorations',
		'packages'      => 'packages',
		'trends'        => 'trends',
		'planner'       => 'birthday-planner',
		'venues'        => 'venues',
		'party_halls'   => 'party-halls',
		'experiences'   => 'unique-celebration-experience',
		'cakes'         => 'cakes',
		'entertainment' => 'entertainment',
		'venue_deals'   => 'venue-deals',
		'campaigns'     => [
			'vday16' => 'c/valentines-day-2016/packages'
		]
	],
	'mail_errors'          => env('MAIL_ERRORS', false),

	/*
	|--------------------------------------------------------------------------
	| Contact
	|--------------------------------------------------------------------------
	*/
	'contact'              => [
		'company'     => [
			'phone'              => env('COMPANY_PHONE', '+91 9640204000'),
			'plain_phone'        => env('COMPANY_PLAIN_PHONE', '9640204000'),
			'vday_cld'           => '9640887000',
			'mobile'             => env('COMPANY_MOBILE', '9640204000'),
			'email'              => env('PING_EMAIL', 'ping@evibe.in'),
			'system_alert_email' => env('SYSTEM_ALERT_EMAIL', 'system.alert@evibe.in'),
			'working'            => [
				'start_day'     => 'Monday',
				'end_day'       => 'Saturday',
				'start_time'    => '10 AM',
				'end_time'      => '8 PM',
				'deadline_time' => '8 PM'
			],
		],
		'operations'  => [
			'name'                  => env('OPERATION_HEAD_NAME', 'Jyothi'),
			'phone'                 => env('OPERATION_HEAD_PHONE', '9640204000'),
			'email'                 => env('OPERATION_HEAD_EMAIL', 'jyothi.m@evibe.in'),
			//'group'               => env('OPERATION_GROUP_EMAIL', 'operations@evibe.in'),
			'group'                 => env('OPERATION_GROUP_EMAIL', 'ops@evibe.in'),
			'group1'                => env('OPERATION_GROUP_EMAIL1', 'ops@evibe.in'),
			'alert_no_action_email' => env('OPERATIONS_ALERT_NO_ACTION_EMAIL', 'ops.alert.noaction@evibe.in')
		],
		'admin'       => [
			'name'  => env('ADMIN_NAME', 'Anji'),
			'phone' => env('ADMIN_PHONE', '7259509827'),
			'email' => env('ADMIN_EMAIL', 'anji@evibe.in')
		],
		'customer'    => [
			'whatsapp' => [
				'phone' => env('CUSTOMER_WHATSAPP_NUMBER', '9640204000')
			],
			'email'    => env('CUSTOMER_HEAD_EMAIL', 'swathi@evibe.in'),
			'group'    => env('CUSTOMER_GROUP_EMAIL', 'enquiry@evibe.in')
		],
		'business'    => [
			'email' => env('BIZ_HEAD_EMAIL', 'business@evibe.in'),
			'group' => env('BIZ_GROUP_EMAIL', 'business@evibe.in'),
			'phone' => env('BIZ_HEAD_PHONE', '9640204000'),
		],
		'tech'        => [
			'group' => env('TECH_GROUP_EMAIL', 'tech@evibe.in'),
			'email' => env('TECH_ADMIN_EMAIL', 'sysad@evibe.in'),
		],
		'hire'        => [
			'email' => env('HIRE_GROUP_EMAIL', 'hire@evibe.in')
		],
		'invitations' => [
			'group' => env('INVITE_GROUP_EMAIL', 'invites@evibe.in'),
			'phone' => env('INVITE_GROUP_PHONE', '9133704000')
		],
		'support'     => [
			'group' => env('SUPPORT_GROUP_EMAIL', 'support@evibe.in')
		]
	],

	/*
	|--------------------------------------------------------------------------
	| Services
	|--------------------------------------------------------------------------
	|
	| All the IDs must always be in `sync` with database table `services`
	*/
	'services'             => [
		'venues'           => [
			'party_halls' => 2,
			'play_zones'  => 39,
			'restaurants' => 40
		],
		'planners'         => 1,
		'packages'         => 36,
		'trends'           => 41,
		'experiences'      => 42,
		'cakes'            => 43,
		'entertainment'    => 55,
		'addons'           => 56,
		'parentCategoryId' => env('SERVICE_PARENT_CATEGORY_ID', 189)
	],

	/*
	|--------------------------------------------------------------------------
	| Gallery
	|--------------------------------------------------------------------------
	*/
	'gallery'              => [
		'type'       => [
			'image' => 0,
			'video' => 1
		],
		'host'       => env('GALLERY_HOST', 'https://gallery.evibe.in'),
		'root'       => env('GALLERY_ROOT', '../../gallery'),
		'logo_path'  => env('EVIBE_LOGO_PATH', '/img/logo/logo_evibe.png'),
		'image_code' => 'PIC'
	],

	/*
	|--------------------------------------------------------------------------
	| Cakes
	|--------------------------------------------------------------------------
	*/
	'cake'                 => [
		'egg'        => 1,
		'eggless'    => 2,
		'filter_id'  => 1,
		'no_results' => 'We are adding amazing cakes soon, keep checking this page.'
	],

	/*
	|--------------------------------------------------------------------------
	| Cake price type
	|--------------------------------------------------------------------------
	*/
	'cake_price_type'      => [
		'fixed_price'  => 1,
		'per_kg_price' => 2,
	],

	'cake_cat_value' => [
		'weight'  => env('CAKE_CAT_VALUE_WEIGHT_ID', 1),
		'flavour' => env('CAKE_CAT_VALUE_FLAVOUR_ID', 2),
		'eggless' => env('CAKE_CAT_VALUE_EGGLESS_ID', 3),
	],

	/*
	|--------------------------------------------------------------------------
	| Tickets
	|--------------------------------------------------------------------------
	|
	| All the type IDs must always be in `sync` with database table `type_ticket_id`
	*/
	'ticket'         => [
		'type' => [
			'package'         => env('TYPE_PAGE_PACKAGE', 1),
			'planner'         => 2,
			'venue'           => 3,
			'service'         => 4,
			'trend'           => env('TYPE_PAGE_TREND', 5),
			'cake'            => env('TYPE_PAGE_CAKE', 6),
			'home_page'       => 7,
			'no_results'      => 8,
			'dash'            => 9,
			'header'          => 10,
			'error'           => 11,
			'experience'      => 12,
			'decor'           => env('TYPE_PAGE_DECOR', 13),
			'entertainment'   => env('TYPE_PAGE_ENT', 14),
			'halls'           => 15,
			'artist'          => 16,
			'resorts'         => env('TYPE_PAGE_RESORT', 17),
			'villas'          => env('TYPE_PAGE_VILLA', 18),
			'lounges'         => env('TYPE_PAGE_LOUNGE', 19),
			'food'            => env('TYPE_PAGE_FOOD', 20),
			'surprises'       => env('TYPE_PAGE_SURPRISE_PACKAGE', 21),
			'venue-deals'     => env('TYPE_PAGE_VENUE_DEALS', 22),
			'priests'         => env('TYPE_PAGE_PRIEST', 23),
			'tents'           => env('TYPE_PAGE_TENT', 24),
			'party_bag'       => 25,
			'campaign'        => 26,
			'generic-package' => env('TYPE_PAGE_GENERIC_PACKAGE', 27),
			'add-on'          => env('TYPE_PAGE_ADD_ON', 28),
			'product'         => env('TYPE_PAGE_PRODUCT', 29),
			'id'              => [
				env('TYPE_PAGE_PACKAGE', 1) => "Package",
				env('TYPE_PAGE_TREND', 5)   => "Trend",
				env('TYPE_PAGE_CAKE', 6)    => "Cake",
				env('TYPE_PAGE_DECOR', 13)  => "Decoration",
				env('TYPE_PAGE_ENT', 14)    => "Service",
				env('TYPE_PAGE_FOOD', 20)   => "Food",
				env('TYPE_PAGE_PRIEST', 23) => "Priest",
				env('TYPE_PAGE_TENT', 24)   => "Tent"
			]
		],

		'enq' => [
			'pre' => [
				'home_page'          => 'ENQ-HP-',
				'no_result'          => 'ENQ-NR-',
				'header'             => 'ENQ-HR-',
				'planner'            => 'ENQ-PL-',
				'venue'              => 'ENQ-VE-',
				'service'            => 'ENQ-SE-',
				'package'            => 'ENQ-PA-',
				'experience'         => 'ENQ-EX-',
				'trend'              => 'ENQ-TR-',
				'cake'               => 'ENQ-CK-',
				'decor'              => 'ENQ-DS-',
				'ent_addon'          => 'ENQ-SR-',
				"custom_ticket"      => "EVB-CT-",
				'auto_booked'        => 'EVB-AU-',
				'site_visit'         => 'EVB-SM-',
				'auto_popup'         => 'EVB-AP-',
				'new_recommendation' => 'EVB-RE-',
				'google_ads'         => 'EVB-GA-',
				'facebook_ads'       => 'EVB-FB-',
				'product_delivery'   => 'EVB-PD-'
			]
		],

		'status' => [
			'initiated'        => 1,
			'progress'         => 2,
			'confirmed'        => 3,
			'booked'           => 4,
			'cancelled'        => 5,
			'enquiry'          => 6,
			'followup'         => 7,
			'auto_paid'        => 8,
			'auto_cancel'      => 9,
			'service_auto_pay' => env('TYPE_STATUS_SERVICE_AP', 12),
			'not_interested'   => env('TYPE_STATUS_NOT_INTERESTED', 13),
			'duplicate'        => env('TYPE_STATUS_NOT_DUPLICATE', 14),
			'related'          => env('TYPE_STATUS_NOT_RELATED', 15),
			'irrelevant'       => env('TYPE_STATUS_IRRELEVANT', 16),
			'no_response'      => env('TYPE_STATUS_NO_RESPONSE', 17),
			'invalid_email'    => env('TYPE_STATUS_INVALID_EMAIL', 18)
		],

		'messages' => [
			'customer_booked_party_done_less_30'     => 'Repeat customer - Already booked from us in past 30 days',
			'customer_booked_party_not_done_less_30' => 'Upsell customer - Already booked from us in past 30 days',
			'customer_booked_party_done_more_30'     => 'Repeat customer - Already booked from us in past',
			'customer_booked_party_not_done_more_30' => 'Upsell customer - Already booked from us in past',
			'customer_confirmed_status_less_30'      => 'Repeat customer - Order processed but not paid',
			'customer_auto_cancelled'                => 'Repeat customer - Auto-cancelled from us in past',
			'customer_cancelled'                     => 'Repeat customer - Cancelled from us in past',
			'default_more_30'                        => 'Repeat Customer - from past',
			'default_less_30'                        => 'Repeat customer in past 30 days'
		],

		'type_lead_status' => [
			'hot'    => 1,
			'medium' => 2,
			'cold'   => 3,
		],

		'success_type' => [
			'confirmed' => 1,
			'booked'    => 2
		],

		'type_update' => [
			'phone'  => 'Phone',
			'email'  => 'Email',
			'auto'   => 'Auto',
			'manual' => 'Manual',
			'other'  => 'Other'
		],

		'status_message' => [
			'booking_edit'            => 'Ticket booking edited',
			'order_process'           => 'Order process email sent',
			'more_info_edit'          => 'Booking more info edited',
			'payment_received'        => 'Payment received',
			'cancelled'               => 'Ticket got cancelled',
			'booking_deleted'         => 'Ticket booking deleted',
			'custom_quote'            => 'Custom quote email sent',
			'recommendation'          => 'Recommendation email sent',
			'exchange_contact'        => 'Contacts exchanged',
			'booking_finalize'        => 'Ticket booking finalized',
			'custom_email'            => 'Custom email sent',
			'no_response'             => 'No response notification sent to customer',
			'cancel_email'            => 'Cancel email sent to customer & vendor manually',
			'cancel_toggle_on'        => 'Auto cancel email toggle switched on',
			'cancel_toggle_off'       => 'Auto cancel toggle switched off',
			'reopen'                  => 'Ticket reopened',
			'edit_ticket'             => 'Ticket information has been edited',
			'receipt_automatic'       => 'Booked receipt sent automatically after payment',
			'receipt_manual'          => 'Booked receipt sent manually via Dash',
			'availability_asked'      => 'Venue availability asked',
			'reco_select'             => 'Customer selected few options from recommendations',
			'reco_select_dislike'     => 'Customer did not like our recommendations, but selected a few options',
			'reco_dislike'            => 'Customer did not like our recommendations',
			'reco_custom_ticket'      => 'Customer has raised custom ticket from recommendations page',
			'customer_not_interested' => 'Customer is not interested in booking with us',
			'retention_un_subscribe'  => 'Customer un-subscribed for our retention notifications'
		],

		'advance' => [
			'unpaid'     => 0,
			'paid'       => 1,
			'percentage' => env('PAYMENT_ADVANCE', 50)
		],

		'type_source' => [
			'google-ad'   => env('TYPE_SOURCE_GOOGLE_AD', 9),
			'facebook-ad' => env('TYPE_SOURCE_FB_AD', 36),
			'email-ad'    => env('TYPE_SOURCE_EMAIl_AD', 4)
		],

		'enquiry_source' => [
			'phone'                         => 1,
			'header_enquiry'                => 2,
			'chat'                          => 3,
			'feasibility_check'             => 4,
			'autobook'                      => 5,
			'cd_decor'                      => 6,
			'cd_food'                       => 7,
			'cd_cake'                       => 8,
			'product_enquiry'               => 9,
			'direct'                        => 10,
			'direct_copy'                   => 11,
			'pb_enquiry'                    => 12,
			'home_enquiry'                  => 13,
			'ga'                            => 14,
			'corporate'                     => 15,
			'site_visit'                    => 16,
			'auto_popup'                    => 17,
			'workflow'                      => 18,
			'delivery_image'                => 19,
			'pb_checkout'                   => 20,
			'inspiration_hub_enquiry'       => 21,
			'v-day-landingPage'             => 22,
			'campaign-page-enquiry-mobile'  => 23,
			'campaign-page-enquiry-desktop' => 24,
			'navigation_modal'              => 25,
			'occasion-home'                 => 26,
		],

		'bookingLikeliness' => [
			"1" => "In an hour",
			"2" => "In a day",
			"6" => "In 3 days",
			"3" => "In a week",
			"4" => "In a month",
			"5" => "Just started planning"
		]

	],

	/*
	|--------------------------------------------------------------------------
	| Ticket Field on checkout page & booking type
	|--------------------------------------------------------------------------
	*/
	'checkout_field' => [
		'cake'           => [
			'message'         => env('CHECKOUT_CAKE_MESSAGE', 1),
			'delivery_slot'   => env('CHECKOUT_CAKE_DELIVERY_SLOT', 155),
			'delivery_charge' => env('CHECKOUT_CAKE_DELIVERY_CHARGE', 156),
			'type'            => env('CHECKOUT_CAKE_TYPE', 21),
			'weight'          => env('CHECKOUT_CAKE_WEIGHT', 22),
			'flavour'         => env('CHECKOUT_CAKE_FLAVOUR', 23)
		],
		'bachelor-venue' => [
			'guest-count' => env('CHECKOUT_BACHELOR_VENUE_GUEST_COUNT', 478)
		],
		'decor'          => [
			'delivery' => [
				env('TYPE_BOOKING_ABD_BALLOONS', 20)        => env('CHECKOUT_ABD_BALLOONS_DFT', 489),
				env('TYPE_BOOKING_ABD_CUSTOM_BALLOONS', 21) => env('CHECKOUT_ABD_CUSTOM_BALLOONS_DFT', 490),
				env('TYPE_BOOKING_ABD_THEMES', 22)          => env('CHECKOUT_ABD_THEMES_DFT', 491),
				env('TYPE_BOOKING_ABD_FLOWERS', 23)         => env('CHECKOUT_ABD_FLOWERS_DFT', 492),
				env('TYPE_BOOKING_ABD_DECOR', 24)           => env('CHECKOUT_ABD_DECOR_DFT', 493),
			]
		]
	],

	'booking_type'     => [
		'venue'          => env('TYPE_BOOKING_VENUE', 3),
		'cake'           => env('TYPE_BOOKING_CAKE', 9),
		'ent'            => env('TYPE_BOOKING_ENTERTAINMENT', 6),
		'decor'          => env('TYPE_BOOKING_DECOR'),
		'raksha-bandhan' => env('TYPE_BOOKING_RAKSHA_BANDHAN'),
	],

	/*
	|--------------------------------------------------------------------------
	| SMS Templates
	|--------------------------------------------------------------------------
	*/
	'sms_tpl'          => [
		'book' => [
			'customer'       => 'Hi #field1#, Congrats! your event is booked with Evibe.in. We received payment of Rs. #field2# for your order #field3#. Track and manage your order here: #field4#',
			'customer_proof' => 'Hi #customer#, Congrats! your event (#orderId#) is booked with Evibe.in. Kindly upload your ID proof here: #tmoLink# immediately to avoid cancellation.',
			'vendor'         => 'Hi #field1#, order booked for #field2#. Received Rs. #field3# from #field4#. Pls check your email for full details & call #field5# within 8 business hours to assure from your side. Evibe.in',
			'update'         => [
				'partner'  => 'Hi #field1#, party order for #field2# by #field3# has been updated. Please check email sent to #field4# or click #field5# for updated order details. Team Evibe.in',
				'customer' => 'Hi #field1#, based on your request, we updated your event order #field2#. Track and manage your order here: #field3# Evibe.in'
			]
		],

		'book_type2' => [
			'customer' => 'Hi #field1#, Congrats! your event is booked with Evibe.in. We received payment of Rs. #field2# for your order #field3#. Kindly carry Rs. #field4# in cash for balance settlement to coordinators. Track and manage your order here: #field5#',
			'vendor'   => 'Hi #field1#, order booked for #field2# from #field3# by #field4# with advance of Rs. #field5#. Pls. check your email for full details & call #field6# ASAP. Also, remind them to pay the balance amount of Rs. #field7# in cash. Evibe.in'
		],

		'auto_book' => [
			'pay_success' => [
				'customer'       => 'Hi #field1#, your payment of Rs. #field2# for Evibe.in (#field3#) is successful. We will confirm your booking within #field4# business hrs (based on availability). Track and manage your order here: #field5#',
				'customer_proof' => 'Hi #customer#, your payment of Rs. #advanceAmount# for party (#orderID#) is successful. Kindly upload your ID proof here: #tmoLink#. We will confirm your order within #hours# business hrs (based on availability). Team Evibe.in',
				'vendor'         => 'Hi #field1#, availability check for #field2#,  #field3# guests on #field4#. A customer is ready to book, pls confirm on #field5# at the earliest to process booking. Team Evibe'
			],
			'cancel'      => [
				'customer' => 'Hi #field1#, sorry! your order for #field2# for #field3# in not available. Full refund of Rs. #field4# will be initiated in 2 business days. For any queries, pls reply to the email sent to #field5#. Team Evibe'
			],
			'venue_deals' => [
				'pay_success' => [
					'customer' => 'Hi #field1#, your payment of Rs. #field2# for Evibe.in (#field3#) is successful. We will confirm your booking within #field4# business hrs (based on availability). Track and manage your order here: #field5#',
					'vendor'   => 'Hi #field1#, availability check for #field2#,  #field3# guests on #field4#, slot: #field5#. Pls confirm your availability on #field6# at the earliest. Team Evibe.in'

				],
			],
			'cake'        => [
				'pay_success' => [
					'customer' => 'Hi #field1#, your payment of Rs. #field2# for Evibe.in (#field3#) is successful. We will confirm your booking within #field4# business hrs (based on availability). Track and manage your order here: #field5#',
					'partner'  => 'Hi #field1#, availability check for #field2#, #field3# for #field4# slot on #field5#. Please check complete details & confirm availability on #field6#. Team Evibe.in'
				]

			],
			'decor'       => [
				'pay_success' => [
					'customer' => 'Hi #field1#, your payment of Rs. #field2# for Evibe.in (#field3#) is successful. We will confirm your booking within #field4# business hrs (based on availability). Track and manage your order here: #field5#',
					'partner'  => 'Hi #field1#, availability check for party on #field2# at #field3# for #field4#. Customer has already paid advance of Rs. #field5#. Please check all details & confirm your availability at #field6# to accept booking. Evibe.in'
				]

			]

		],

		'product-book' => [
			'pay-success' => [
				'customer' => "Hi #customer#, thank you for placing order for #product# (#bookingOrderId#). It will be delivered #deliveryText#. Track your order here: #tmoLink#. Evibe.in",
			],
		],

		'refer_earn' => [
			"customer" => [
				"reminder" => "Hi #field1#, thanks for your interest in referral program. Please share your unique link #field3# with your friends on WhatsApp/Facebook etc. Your friend gets #field2#* OFF on sign up and you will get #field2#* OFF when they book. Evibe.in"
			]
		],

		'product_non_live' => "Dear #field1#, we are sorry that your package/service in unavailable. Our party planning expert will contact you shortly to help you out. However, if you need anything urgent, please call #field2#. Evibe.in",

		'invite' => [
			'notify-form' => 'Hi #customerName#, please fill the form: #formLink# before tomorrow noon to receive your specialised e-invitation card. Team Evibe.in'
		],

		'landing' => [
			'default'   => 'Hi, Congrats! Use code #field2# to get Rs. #field3#* OFF at #field4# - trusted party planners. Enquiry? Call #field5#',
			'kids'      => 'Hi, I am #field1# from Evibe.in - Trusted Birthday Planners. Congrats! Use code #field2# to get Rs. #field3#* OFF on best decors, cakes, artists, packages and more for your event at best prices only at #field4#. Have questions? pls. call me on #field5#.',
			'surprises' => 'Hi, I am #field1# from Evibe.in - Trusted Surprise Planners. Congrats! Use code #field2# to get Rs. #field3#* OFF on top candlelight dinners, flash mobs, balloons, rose decors and more surprises at best prices only at #field4#. Need help? pls. call me on #field5#.',
			'piab'      => 'Use code #couponCode# to get #discount#* OFF on any of our party kits. Need help? Write to #piabSupportEmail# Evibe.in'
		],

		'tmo' => [
			'login' => [
				'otp' => 'Your One Time Password (OTP) is #field1#. Pls do not share with anyone. Evibe.in'
			]
		],

		'customer_proof' => [
			'upload' => [
				'customer' => 'Hi #customer#, thank you for uploading your ID proof. You can track the validation progress here: #tmoLink# Evibe.in',
			],
		],
	],

	/*
	|--------------------------------------------------------------------------
	| Event Types
	|--------------------------------------------------------------------------
	|
	| Should be in sync with `type_event` table
	*/
	'occasion'         => [
		'corporates'    => [
			'id'   => env('TYPE_EVENT_CORPORATES', 5),
			'name' => 'Corporates Event Management',
			'url'  => 'corporate-events'
		],
		'house-warming' => [
			'id'               => env('TYPE_EVENT_HOUSEWARMING', 14),
			'name'             => 'House Warming',
			'hashtag'          => 'HouseWarming',
			'url'              => 'house-warming',
			'default_og_image' => "https://gallery.evibe.in/img/logo/logo_evibe.png",
			'decors'           => [
				'name'        => 'Housewarming Decorations',
				'results_url' => 'decorations',
				'profile_url' => 'decorations'
			],
			'food'             => [
				'name'        => 'Catering',
				'profile_url' => 'food',
				'results_url' => 'food'
			],
			'tents'            => [
				'name'        => 'Tents',
				'results_url' => 'tents',
				'profile_url' => 'tents'
			],
			'priest'           => [
				'name'        => 'Priests',
				'results_url' => 'priest',
				'profile_url' => 'priest'
			],
			'cakes'            => [
				'name'        => 'Cakes',
				'results_url' => 'cakes',
				'profile_url' => 'cakes'
			],
			'ent'              => [
				'name'        => 'Add-ons',
				'results_url' => 'add-ons',
				'profile_url' => 'add-ons'
			]
		],
		'surprises'     => [
			'id'      => env('TYPE_EVENT_SURPRISES', 16),
			'name'    => 'Surprise Planners',
			'hashtag' => 'Surprises',
			'url'     => 'surprise-planners',
			'package' => [
				'name'        => 'Surprises',
				'results_url' => 'packages',
				'profile_url' => 'packages'
			],
			'cld'     => [
				'name'        => 'Candlelight Dinners',
				'url'         => 'candle-light-dinner',
				'results_url' => 'candle-light-dinner',
				'profile_url' => 'candle-light-dinner'
			],
			'ent'     => [
				'name'        => 'Special Couple Experiences Entertainment',
				'results_url' => 'entertainment',
				'profile_url' => 'entertainment'
			],
			'cakes'   => [
				'name'        => 'Special Couple Experiences Cakes',
				'results_url' => 'cakes',
				'profile_url' => 'cakes'
			],
			'decors'  => [
				'name'        => 'Special Couple Experiences Decorations',
				'results_url' => 'decorations',
				'profile_url' => 'decorations'
			]
		],
		'pre-post'      => [
			'id'      => env('TYPE_EVENT_PREPOST', 13),
			'name'    => 'Engagement and Wedding Reception',
			'hashtag' => 'EngagementReception',
			'url'     => 'engagement-wedding-reception',
			'package' => [
				'profile_url' => 'packages',
				'result_url'  => ''
			],
			'decors'  => [
				'results' => 'decorations',
				'name'    => 'Flower Decorations',
				'profile' => ''
			],
			'cakes'   => [
				'name'        => 'Theme Cakes',
				'results_url' => 'cakes',
				'profile_url' => 'cakes'
			],
			'ent'     => [
				'name'        => 'Wedding Reception Entertainment',
				'results_url' => 'entertainment',
				'profile_url' => 'entertainment'
			]
		],
		'bachelor'      => [
			'id'               => env('TYPE_EVENT_BACHELOR', 11),
			'name'             => 'Youth Party',
			'hashtag'          => 'BachelorsParty',
			'url'              => 'bachelor-party',
			'default_og_image' => "https://gallery.evibe.in/img/logo/logo_evibe.png",
			'cakes'            => [
				'name'        => 'Fun Cakes',
				'results_url' => 'cakes',
				'profile_url' => 'cakes'
			],
			'resort'           => [
				'name'        => 'Resorts',
				'results_url' => 'resorts',
				'profile_url' => 'resorts'
			],
			'villa'            => [
				'name'        => 'Villas',
				'results_url' => 'villas',
				'profile_url' => 'villas'
			],
			'lounge'           => [
				'name'        => 'Lounges',
				'results_url' => 'lounges',
				'profile_url' => 'lounges'
			],
			'food'             => [
				'name'        => 'Home Caterers',
				'profile_url' => 'food',
				'results_url' => 'food'
			],
			'package'          => [
				'profile_url' => 'packages',
				'result_url'  => ''
			],
			'ent'              => [
				'name'        => 'Bachelor Entertainment',
				'results_url' => 'entertainment',
				'profile_url' => 'entertainment'
			],
			'decors'           => [
				'name'        => 'Decorations',
				'results_url' => 'decorations',
				'profile_url' => 'decorations'
			]
		],

		'naming_ceremony' => [
			'id'      => env('TYPE_EVENT_NAMING_CEREMONY', 8),
			'name'    => 'Naming Ceremony Decorations',
			'hashtag' => 'Naming Ceremony',
			'url'     => 'naming-ceremony/',
			'decors'  => [
				'results'         => 'naming-ceremony',
				'name'            => 'Naming Ceremony Decorations',
				'profile'         => '',
				'category_cat_id' => env('CATEGORY_KIDS_DECORS_ID', 15),
				'gender_cat_id'   => env('GENDER_KIDS_DECORS_ID', 55),
				'place_cat_id'    => env('PLACE_KIDS_DECORS_ID', 204),
				'theme_cat_id'    => env('THEME_KIDS_DECORS_ID', 48),
			]
		],

		'store-opening' => [
			'id'      => env('TYPE_EVENT_STORE_OPENING', 20),
			'name'    => 'Store Opening Decorations',
			'hashtag' => 'storeOpenings',
			'url'     => 'store-openings/',
			'decors'  => [
				'results'         => 'store-openings',
				'name'            => 'Store Opening Decorations',
				'profile'         => '',
				'category_cat_id' => env('CATEGORY_KIDS_DECORS_ID', 15),
				'gender_cat_id'   => env('GENDER_KIDS_DECORS_ID', 55),
				'place_cat_id'    => env('PLACE_KIDS_DECORS_ID', 204),
				'theme_cat_id'    => env('THEME_KIDS_DECORS_ID', 48),
			]
		],

		'baby-shower' => [
			'id'      => env('TYPE_EVENT_BABY_SHOWER', 19),
			'name'    => 'Baby Shower Decorations',
			'hashtag' => 'Baby Shower',
			'url'     => 'baby-shower/',
			'decors'  => [
				'results'         => 'baby-shower',
				'name'            => 'Baby Shower Decorations',
				'profile'         => '',
				'category_cat_id' => env('CATEGORY_KIDS_DECORS_ID', 15),
				'gender_cat_id'   => env('GENDER_KIDS_DECORS_ID', 55),
				'place_cat_id'    => env('PLACE_KIDS_DECORS_ID', 204),
				'theme_cat_id'    => env('THEME_KIDS_DECORS_ID', 48),
			]
		],

		'kids_birthdays'   => [
			'id'         => env('TYPE_EVENT_BIRTHDAY', 1),
			'name'       => 'Birthday Party Planners',
			'hashtag'    => 'BirthdayParty',
			'url'        => 'birthday-party-planners',
			'decors'     => [
				'results'         => 'birthday-party-decorations',
				'name'            => 'Birthday Decorations',
				'profile'         => '',
				'category_cat_id' => env('CATEGORY_KIDS_DECORS_ID', 15),
				'gender_cat_id'   => env('GENDER_KIDS_DECORS_ID', 55),
				'place_cat_id'    => env('PLACE_KIDS_DECORS_ID', 204),
				'theme_cat_id'    => env('THEME_KIDS_DECORS_ID', 48),
			],
			'food'       => [
				'name'        => 'Birthday Catering',
				'results_url' => 'food',
				'profile_url' => 'food'
			],
			'venue-deal' => [
				'name'        => 'Venue Deals',
				'profile_url' => 'venue-deals',
				'results_url' => 'venue-deals/all',
				'url'         => 'venue-deal'
			],
			'cakes'      => [
				'name'          => 'Birthday Cakes',
				'results_url'   => 'cakes',
				'profile_url'   => 'cakes',
				'gender_cat_id' => env('GENDER_KIDS_CAKES_ID', 109),
				'type_cat_id'   => env('TYPE_KIDS_CAKES_ID', 104)
			],
			'ent'        => [
				'name'        => 'Birthday Entertainment',
				'results_url' => 'entertainment-activities-stalls',
				'profile_url' => 'entertainment-activities-stalls'
			],
			'packages'   => [
				'name'        => 'Birthday Packages',
				'results_url' => 'packages',
				'profile_url' => 'packages'
			],
			'tents'      => [
				'name'        => 'Birthday Tents',
				'results_url' => 'tents',
				'profile_url' => 'tents'
			]
		],
		'christmas'        => [
			'id' => env('CHRISTMAS_EVENT_ID')
		],
		'valentine-day'    => [
			'id' => env('VALENTINE_DAY_EVENT_ID', 18)
		],
		'collections'      => [
			'results_url' => 'collections',
			'profile_url' => 'collections'
		],
		'raksha-bandhan'   => [
			'name'            => 'Raksha Bandhan',
			'id'              => env('TYPE_EVENT_RAKSHA_BANDHAN'),
			'url'             => 'raksha-bandhan',
			'event-date'      => '1502150401', // todo: change
			'is_image_tag_id' => env('TYPE_TAG_IMAGE_FOR_CAMPAIGN_RAKSHA_BANDHAN', '')
		],
		'independence-day' => [
			'id'  => env('TYPE_EVENT_INDEPENDENCE_DAY', ''),
			'url' => 'independence-day'
		],
		'new-year-2018'    => [
			'id' => env('TYPE_EVENT_NEW_YEAR_2018', 29)
		],
		'vd_2018'          => [
			'id' => env('TYPE_EVENT_VD_2018', 31)
		],
		'first_birthday'   => [
			'id' => env('TYPE_EVENT_FIRST_BIRTHDAY', 36)
		],
		'birthday_2-5'     => [
			'id' => env('TYPE_EVENT_BIRTHDAY_2-5', 37)
		],
		'birthday_6-12'    => [
			'id' => env('TYPE_EVENT_BIRTHDAY_6-12', 38)
		],

	],

	/*
	|--------------------------------------------------------------------------
	| Package Categories
	|--------------------------------------------------------------------------
	|
	| Should be in sync with `type_package_field_cat` table
	*/
	'pkg_fields'       => [
		'cat'  => [
			'highlights' => [
				'id' => 1
			],
			'inclusions' => [
				'id' => 2
			],
			'individual' => [
				'id' => 3
			]
		],
		'keys' => [
			'menu_veg'      => env('CUSTOM_PKG_FIELD_MENU_VEG', 'veg_menu'),
			'menu_nonveg'   => env('CUSTOM_PKG_FIELD_MENU_NON_VEG', 'non-veg_menu'),
			'price_nonveg'  => env('CUSTOM_PKG_FIELD_PRICE_NON_VEG', 'price_non-veg'),
			'worth_nonveg'  => env('CUSTOM_PKG_FIELD_WORTH_NON_VEG', 'worth_non-veg'),
			'hasAc'         => env('CUSTOM_PKG_FIELD_HAS_AC', 'has_ac'),
			'rent_duration' => env('CUSTOM_PKG_FIELD_RENT', 'rent_duration_in_hrs'),
			'venuePolicies' => env('CUSTOM_PKG_FIELD_VENUE_POLICIES', 'venue_policies')
		]
	],

	/*
	|--------------------------------------------------------------------------
	| Type input
	|--------------------------------------------------------------------------
	*/
	'input'            => [
		'text'     => 1,
		'number'   => 2,
		'textarea' => 3,
		'option'   => 4,
		'radio'    => 5,
		'checkbox' => 6,
		'date'     => 7
	],

	/*
	|--------------------------------------------------------------------------
	| User Actions
	|--------------------------------------------------------------------------
	|
	| Should be in sync with `type_user_action` table
	*/
	'user_action'      => [
		'login'  => 1,
		'logout' => 2
	],

	/*
	|--------------------------------------------------------------------------
	| Shortlist Actions
	|--------------------------------------------------------------------------
	|
	| Should be in sync with `type_shortlist_action` table
	*/
	'shortlist_action' => [
		'add'    => 1,
		'delete' => 2,
		'update' => 3
	],

	/*
	|----------------------------------------------------------------------------
	| Partner app key, being used for app notification for partner application
	|----------------------------------------------------------------------------
	*/
	'partner_app_key'  => [
		'enquiry'  => [
			'list'    => 'enquiry_list',
			'details' => 'enquiry_details'
		],
		'order'    => [
			'list'    => 'order_list',
			'details' => 'order_details'
		],
		'delivery' => [
			'list'    => 'delivery_list',
			'details' => 'delivery_details'
		]
	],

	/*
	|----------------------------------------------------------------------------
	| Roles
	|----------------------------------------------------------------------------
	|
	| Should be in sync with database `role` table
	*/
	'roles'            => [
		'customer'         => env('CUSTOMER_ROLE_ID', 13),
		'super_admin'      => env('ROLE_SUPER_ADMIN', 1),
		'admin'            => env('ROLE_ADMIN', 2),
		'planner'          => env('ROLE_PLANNER', 3),
		'venue'            => env('ROLE_VENUE', 4),
		'bd'               => env('ROLE_BD', 5),
		'customer_delight' => env('ROLE_CUSTOMER_DELIGHT', 6),
		'artist'           => env('ROLE_ARTIST', 7),
		'operations'       => env('ROLE_OPERATIONS', 8),
		'sr_crm'           => env('ROLE_SENIOR_CRM', 9),
		'invite_usr'       => env('ROLE_INVITE_USER', 10),
		'marketing'        => env('ROLE_MARKETING', 11),
		'tech'             => env('ROLE_TECH', 12),
	],

	/*
	|----------------------------------------------------------------------------
	| Budget Calculation
	|----------------------------------------------------------------------------
	|
	| Values used for budget calculation in Party Bag
	*/
	'budget'           => [
		'cake'              => [
			'min' => 1,
			'max' => 1
		],
		'decor'             => [
			'min' => 1,
			'max' => 1
		],
		'trend'             => [
			'min' => 1,
			'max' => 1
		],
		'entertainment'     => [
			'min' => 1,
			'max' => 1
		],
		'venue-deals'       => [
			'min' => 1,
			'max' => 1
		],
		'birthday-packages' => [
			'min' => 1,
			'max' => 1
		],
		'resorts'           => [
			'min' => 1,
			'max' => 1
		],
		'villas'            => [
			'min' => 1,
			'max' => 1
		],
		'lounges'           => [
			'min' => 1,
			'max' => 1
		],
		'food'              => [
			'min' => 1,
			'max' => 1
		],
		'surprises'         => [
			'min' => 1,
			'max' => 1
		],
		'venue-halls'       => [
			'min' => 1,
			'max' => 1
		],
		'tents'             => [
			'min' => 1,
			'max' => 1
		],
		'priests'           => [
			'min' => 1,
			'max' => 1
		],
	],

	/*
	|--------------------------------------------------------------------------
	| Notification type
	|--------------------------------------------------------------------------
	*/
	'enquiry'          => [
		'type' => [
			'avlCheck'    => env('ENQ_AVL_CHECK_ID', 2),
			'customQuote' => env('ENQ_CUSTOM_QUOTE_ID', 4),
		]
	],

	'project_id' => env('PROJECT_ID_MAIN', 1),

	'spl_valentine_package_id_2017' => env('SPECIAL_VALENTINE_PACKAGE_ID_2017', 739),

	/*
	|--------------------------------------------------------------------------
	| Auto Book ticket types (should maintain same view files)
	|--------------------------------------------------------------------------
	*/
	'auto-book'                     => [
		env('TYPE_PAGE_CAKE', 6)              => 'cake',
		env('TYPE_PAGE_DECOR', 13)            => 'decor',
		env('TYPE_PAGE_ENT', 14)              => 'ent',
		env('TYPE_PAGE_RESORT', 17)           => 'resorts',
		env('TYPE_PAGE_VILLA', 18)            => 'villas',
		env('TYPE_PAGE_LOUNGE', 19)           => 'lounges',
		env('TYPE_PAGE_SURPRISE_PACKAGE', 21) => 'surprises-packages',
		env('TYPE_PAGE_VENUE_DEALS', 22)      => 'venue-deals',
		env('TYPE_PAGE_GENERIC_PACKAGE', 27)  => 'generic-package',
		env('TYPE_PAGE_ADD_ON', 28)           => 'add-on',
		'campaign'                            => [
			env('TYPE_EVENT_RAKSHA_BANDHAN', '')   => 'raksha-bandhan',
			env('TYPE_EVENT_INDEPENDENCE_DAY', '') => 'independence-day'
		]
	],

	/*
	|--------------------------------------------------------------------------
	| Cities (Must be in sync with 'city' table)
	|--------------------------------------------------------------------------
	|
	| Used for Google places API.
	*/
	'city'                          => [
		'Bengaluru'    => env('CITY_BANGALORE_ID', 1),
		'Hyderabad'    => env('CITY_HYDERABAD_ID', 2),
		'Secunderabad' => env('CITY_HYDERABAD_ID', 2),
		'New Delhi'    => env('CITY_DELHI_ID', 3),
		'Mumbai'       => env('CITY_MUMBAI_ID', 4),
		'Pune'         => env('CITY_PUNE_ID', 5),
		'Chennai'      => env('CITY_CHENNAI_ID', 8),

	],

	/* Campaign Token */
	'campaign-token'                => '9640204000',

	/* type_venue_hall table */
	/* @todo: remove this after revamp of venue-deals AB */
	'type-venue-hall'               => [
		'banquet-hall' => env('TYPE_HALL_BANQUET_HALL', 2)
	],

	/* ganesh-chaturthi specific collection profile urls */
	'ganesh-chaturthi-profile-url'  => [
		'bangalore' => '1502193483-Ganesh-Chaturthi-Decors-Entertainment',
		'hyderabad' => '1503149003-Vinayaka-chaviti-Decors-Entertainment',
	],

	/* christmas specific collection profile urls */
	'christmas-profile-url'         => [
		'bangalore' => '1545139461-Christmas-special-candle-light-dinners',
		'hyderabad' => '1545132059-Christmas-special-candle-light-dinners',
	],

	/* new year specific collection profile urls */
	'new-year-profile-url'          => [
		//'bangalore' => '1544873981-New-Year-Collection',
		//'hyderabad' => '1513168011-Refreshing-New-Year-Getaways',
	],

	/* valentines day specific surprise collection profile urls */
	'valentines-day-surprises-url'  => [
		'bangalore' => '1549114446-Valentines-day-special-packages',
		'hyderabad' => '1548928891-Valentines-day-special-packages',
		'delhi'     => '1549016342-Valentines-day-special-packages',
		//'mumbai'    => '1518509697-Valentines-day-2018-Surprises',
		'pune'      => '1549016683-Valentines-day-special-packages',
	],

	/* valentines day CLD collection profile urls */
	'valentines-day-cld-url'        => [
		'bangalore' => 'valentines-day-2022-romantic-candlelight-dinner-bangalore',
		'hyderabad' => 'valentines-day-2022-romantic-candlelight-dinner-hyderabad',
		//'delhi'     => '1549007831-Valentines-day-2019-special-candle-light-dinners',
		//'mumbai'    => '1518445981-Candle-Light-Dinners-for-Valentines-Day-2018',
		//'pune'      => '1549004766-Valentines-day-2019-special-candle-light-dinners',
	],

	/* ipl collection profile urls */
	'ipl-profile-url'               => [
		'bangalore' => '1523177354-IPL-2018-Farmhouses',
		'hyderabad' => '1523086324-IPL-2018-Farmhouses',
	],

	/* campaign cities */
	'campaign-city'                 => [
		'bangalore' => env('CITY_BANGALORE_ID', 1),
		'hyderabad' => env('CITY_HYDERABAD_ID', 2),
		'delhi'     => env('CITY_DELHI_ID', 3),
		'mumbai'    => env('CITY_MUMBAI_ID', 4),
		'pune'      => env('CITY_PUNE_ID', 5),
	],

	/* campaign specific url */
	'campaign-specific-url'         => [
		'ganesh-chaturthi'         => '-Ganesh-Chaturthi-Decors-Entertainment',
		'vinayaka-chavithi'        => '-Vinayaka-chaviti-Decors-Entertainment',
		'christmas'                => '-Christmas-special-candle-light-dinners',
		'new-year'                 => '-New-Year-Collection',
		'valentines-day-surprises' => '-Valentines-day-special-packages',
		'valentines-day-cld'       => '-Valentines-day-2019-special-candle-light-dinners',
		'ipl'                      => '-IPL-2018-Farmhouses',
	],

	/* time-specific campaigns */
	'campaign-limit'                => [
		'ganesh-chaturthi' => '1503685799',
		'christmas'        => '1545762599', // 25th DEC 2019 23:59:59
		'new-year'         => '1546280999', // 31th DEC 2018 23:59:59
		'valentines-day'   => '1644845399', // 14th FEB 2022 18:59:59
		'ipl'              => '1527445799', // 27th MAY 2018 23:59:59
	],

	/* campaign package ids */
	'campaign-packages'             => [
		'christmas' => env('CHRISTMAS_PACKAGE_IDS', ''),
		'new-year'  => env('NEW_YEAR_PACKAGE_IDS', '')
	],

	/* campaign dates (Y-m-d) */
	'campaign-dates'                => [
		'christmas'      => [
			'2018-12-25'
		],
		'new-year'       => [
			'2018-12-31'
		],
		'valentines-day' => [
			'2020-02-08',
			'2020-02-09',
			'2020-02-10',
			'2020-02-11',
			'2020-02-12',
			'2020-02-13',
			'2020-02-14',
			'2021-02-14',
			'2022-02-14',
		]
	],

	/* star icon image paths for rating in email */
	'star_icon'                     => [
		'full'    => "/img/icons/full_star.png",
		'34th'    => "/img/icons/34th_star.png",
		'half'    => "/img/icons/half_star.png",
		'quarter' => "/img/icons/quarter_star.png"
	],

	/*
	|--------------------------------------------------------------------------
	| TypeTicketId - name (based on route names)
	|--------------------------------------------------------------------------
	*/
	'type-product-route-name'       => [
		env('TYPE_PAGE_PACKAGE', 1)           => 'packages',
		env('TYPE_PAGE_VENUE', 3)             => 'venues',
		env('TYPE_PAGE_CAKE', 6)              => 'cakes',
		env('TYPE_PAGE_DECOR', 13)            => 'decors',
		env('TYPE_PAGE_ENT', 14)              => 'ent',
		env('TYPE_PAGE_RESORT', 17)           => 'resort',
		env('TYPE_PAGE_VILLA', 18)            => 'villa',
		env('TYPE_PAGE_LOUNGE', 19)           => 'lounge',
		env('TYPE_PAGE_FOOD', 20)             => 'food',
		env('TYPE_PAGE_SURPRISE_PACKAGE', 21) => 'package',
		env('TYPE_PAGE_VENUE_DEALS', 22)      => 'venue_deals',
		env('TYPE_PAGE_PRIEST', 23)           => 'priest',
		env('TYPE_PAGE_TENT', 24)             => 'tent',
	],

	'type-product-title'    => [
		env('TYPE_PAGE_PACKAGE', 1)           => 'Package',
		env('TYPE_PAGE_VENUE', 3)             => 'Venue',
		env('TYPE_PAGE_CAKE', 6)              => 'Cake',
		env('TYPE_PAGE_DECOR', 13)            => 'Decor Style',
		env('TYPE_PAGE_ENT', 14)              => 'Entertainment',
		env('TYPE_PAGE_RESORT', 17)           => 'Resort',
		env('TYPE_PAGE_VILLA', 18)            => 'Villa',
		env('TYPE_PAGE_LOUNGE', 19)           => 'Lounge',
		env('TYPE_PAGE_SURPRISE_PACKAGE', 21) => 'Surprise Package',
		env('TYPE_PAGE_VENUE_DEALS', 22)      => 'Venue Deal',
		env('TYPE_PAGE_PRIEST', 23)           => 'Priest',
		env('TYPE_PAGE_TENT', 24)             => 'Tent',
		env('TYPE_PAGE_ADD_ON', 28)           => 'Add On',
	],

	/*
	|--------------------------------------------------------------------------
	| EventId - name (based on route names)
	|--------------------------------------------------------------------------
	*/
	'type-event-route-name' => [
		env('TYPE_EVENT_BIRTHDAY', 1)        => 'birthdays',
		env('TYPE_EVENT_CORPORATES', 5)      => 'corporate-events',
		env('TYPE_EVENT_BACHELOR', 11)       => 'bachelor',
		env('TYPE_EVENT_PRE_POST', 13)       => 'pre-post',
		env('TYPE_EVENT_SURPRISES', 16)      => 'surprises',
		env('TYPE_EVENT_HOUSE_WARMING', 14)  => 'house-warming',
		env('TYPE_EVENT_NAMING_CEREMONY', 8) => 'naming-ceremony',
		env('TYPE_EVENT_STORE_OPENING', 20)  => 'store-opening',
		env('TYPE_EVENT_BABY_SHOWER', 19)    => 'baby-shower'
	],

	'type-event-title' => [
		env('TYPE_EVENT_BIRTHDAY', 1)        => 'Birthday Party',
		env('TYPE_EVENT_CORPORATES', 5)      => 'Corporate Events',
		env('TYPE_EVENT_BACHELOR', 11)       => 'Youth Party',
		env('TYPE_EVENT_PRE_POST', 13)       => 'Weddings',
		env('TYPE_EVENT_SURPRISES', 16)      => 'Surprises',
		env('TYPE_EVENT_HOUSE_WARMING', 14)  => 'House Warming',
		env('TYPE_EVENT_NAMING_CEREMONY', 8) => 'Naming Ceremony',
		env('TYPE_EVENT_BABY_SHOWER', 19)    => 'Baby Shower',
		env('TYPE_EVENT_STORE_OPENING', 20)  => 'Store Opening',
	],

	'type-event-breadcrumb' => [
		env('TYPE_EVENT_BIRTHDAY', 1)        => 'Birthday Party',
		env('TYPE_EVENT_BACHELOR', 11)       => 'Youth Party',
		env('TYPE_EVENT_PRE_POST', 13)       => 'Wedding',
		env('TYPE_EVENT_SURPRISES', 16)      => 'Surprises',
		env('TYPE_EVENT_HOUSE_WARMING', 14)  => 'House Warming',
		env('TYPE_EVENT_NAMING_CEREMONY', 8) => 'Naming Ceremony',
		env('TYPE_EVENT_BABY_SHOWER', 19)    => 'Baby Shower',
		env('TYPE_EVENT_BABY_SHOWER', 20)    => 'Store Opening',
	],

	/*
	|--------------------------------------------------------------------------
	| Price Category related todo: remove after getting from dash or logic written
	|--------------------------------------------------------------------------
	*/
	'price-category'        => [
		'bounds'    => [
			'low'    => 1,
			'high'   => 2,
			'normal' => 3
		],
		'decor'     => env('PRICE_CATEGORY_DECOR', 5000),
		'resorts'   => env('PRICE_CATEGORY_RESORTS', 8000),
		'villas'    => env('PRICE_CATEGORY_VILLAS', 8000),
		'lounges'   => env('PRICE_CATEGORY_LOUNGES', 8000),
		'surprises' => env('PRICE_CATEGORY_SURPRISES', 5000),
	],

	/*
	|--------------------------------------------------------------------------
	| User Device related
	|--------------------------------------------------------------------------
	*/
	'user-device'           => [
		'type' => [
			'1' => 'Mobile',
			'2' => 'Desktop',
		],
	],

	'partner'                => [
		'package_status' => [
			'draft'     => 1,
			'submitted' => 2,
			'rejected'  => 3
		],

		'discussion' => [
			1 => "Gallery images quality is not good",
			2 => "Gallery images are not matching with description provided",
			3 => "More Gallery images needed to understand the package",
			4 => "Price is too high",
			5 => "Similar package already exists",
			6 => "Inclusions are not upto the mark"
		],

		'help_info' => [
			"profile"                    => "In this screen, you can see all your profile information. If you want to make any update, kindly use “Contact Us” on top right corner.",
			"messages"                   => "In this screen, you can see all the Messages",
			"live"                       => "In this screen, you can see list of all your packages which are already live on Evibe.in. Click on a package to see complete package details.",
			"submitted"                  => "In this screen, you can see the list of all your packages which are being reviewed by our team. Click on a package to see complete package details.",
			"draft"                      => "In this screen, you can see the list of all your packages which are saved but not submitted yet. Please click on the package and submit it.",
			"all"                        => "In this screen, you can see the list of all your packages on Evibe.in. Please use “Add Package” and submit your packages.",
			"dashboard"                  => "In this screen, you can see all the bookings & metrics for the current month. You can change the date interval by clicking on the date filter on the right-hand side & press apply.",
			"enquiries"                  => "In this screen, you can see all your availability checks and quotations.",
			"orders"                     => "In this screen, you can see all your upcoming orders.",
			"orders-profile"             => "In this screen, you can see all the info related to the particular order including gallery(If images uploaded) & location.",
			"pending-deliveries"         => "In this screen, you can see all the pending deliveries from past 10 days.",
			"pending-deliveries-profile" => "In this screen, you can upload delivery images and click on \"I am done\" to complete the delivery.",
			"settlements"                => "In this screen, you can see the list of all the settlements for your services"
		]
	],

	/*
	|--------------------------------------------------------------------------
	| Paginate Options
	|--------------------------------------------------------------------------
	*/
	'paginate-options'       => [
		'generic'         => '52',
		'generic-desktop' => '51', // @see: verify no.of results being shown before changing the count
		'generic-mobile'  => '50',
	],

	/*
	|--------------------------------------------------------------------------
	| Surprise Package Tags
	|--------------------------------------------------------------------------
	*/
	'surprise-parent-tags'   => [
		'relationship' => env('SURPRISE_RELATIONSHIP_TAG'),
		'occasion'     => env('SURPRISE_OCCASION_TAG'),
		'age-group'    => env('SURPRISE_AGE_GROUP_TAG'),
	],

	/*
	|--------------------------------------------------------------------------
	| Surprise Relation Tags
	|--------------------------------------------------------------------------
	*/
	'surprise-relation-tags' => [
		'gf'                  => env('RELATION_GF_TAG_ID', 154),
		'bf'                  => env('RELATION_BF_TAG_ID', 155),
		'mother'              => env('RELATION_MOTHER_TAG_ID', 156),
		'father'              => env('RELATION_FATHER_TAG_ID', 157),
		'husband'             => env('RELATION_HUSBAND_TAG_ID', 158),
		'wife'                => env('RELATION_WIFE_TAG_ID', 159),
		'friends'             => env('RELATION_FRIENDS_TAG_ID', 160),
		'parents'             => env('RELATION_PARENTS_TAG_ID', 161),
		'couple'              => env('RELATION_COUPLE_TAG_ID', 162),
		'candle-light-dinner' => env('RELATION_CANDLE_LIGHT_DINNER_TAG_ID', 186)
	],

	/*
	|--------------------------------------------------------------------------
	| Product Tags
	|--------------------------------------------------------------------------
	*/
	'product-tags'           => [
		'candle-light-dinner' => env('CANDLE_LIGHT_DINNER_TAG_ID', 186),
		'cake-cutting-place'  => env('CAKE_CUTTING_PLACE_TAG_ID', 260)
	],

	/*
	|--------------------------------------------------------------------------
	| Party slots (@see: should not exceed 12:00 AM)
	|--------------------------------------------------------------------------
	*/
	'party-slot'             => [
		env('PARTY_SLOT_ANY', 1)       => '8:00 AM - 11:59 PM',
		env('PARTY_SLOT_MORNING', 2)   => '8:00 AM - 11:59 AM',
		env('PARTY_SLOT_AFTERNOON', 3) => '12:00 PM - 5:00 PM',
		env('PARTY_SLOT_EVENING', 4)   => '5:00 PM - 8:00 PM',
		env('PARTY_SLOT_NIGHT', 5)     => '8:00 PM - 10:00 PM',
		env('PARTY_SLOT_MIDNIGHT', 6)  => '10:00 PM - 11:59 PM',
		env('PARTY_SLOT_BREAKFAST', 7) => '7:00 AM - 10:00 AM',
		env('PARTY_SLOT_LUNCH', 8)     => '12:00 PM - 3:00 PM',
		env('PARTY_SLOT_HI-TEA', 9)    => '4:00 PM - 7:00 PM',
		env('PARTY_SLOT_DINNER', 10)   => '7:00 PM - 10:00 PM',
	],

	'transport'            => [
		'free-kms' => 20
	],

	/*
	|--------------------------------------------------------------------------
	| Payment Gateways (should be in sync with 'type_payment_gateway'
	|--------------------------------------------------------------------------
	*/
	'type-payment-gateway' => [
		'razorpay'   => 1,
		'payumoney'  => 2,
		'freecharge' => 3,
		'paytm'      => 4
	],

	/*
	 * Default id for accepting partner review greater than 4
	 */
	'default'              => [
		'review_handler'       => env('DEFAULT_REVIEW_HANDLER', 2),
		'access_token_handler' => env('DEFAULT_ACCESS_TOKEN_HANDLER', 2),
		'evibe_service_fee'    => 0.15,
		'gst'                  => 0.18,
		'cgst'                 => 0.09,
		'sgst'                 => 0.09,
		'igst'                 => 0.09
	],

	/*
	 * Default error codes for error email and logging error
	 */
	'error_code'           => [
		'update_settlement_booking' => 12,
		'delete_settlement_booking' => 13,
		'create_settlement_booking' => 14,
		'create_function'           => 21,
		'fetch_function'            => 22,
		'update_function'           => 23,
		'delete_function'           => 24,
		'hack'                      => 100
	],

	/*
	 * Default error codes for error email and logging error
	 */
	'type_question'        => [
		'text'     => env('TYPE_QUESTION_TEXT', 1),
		'radio'    => env('TYPE_QUESTION_RADIO', 2),
		'checkbox' => env('TYPE_QUESTION_CHECKBOX', 3),
		'rating'   => env('TYPE_QUESTION_RATING', 4),
		'select'   => env('TYPE_QUESTION_SELECT', 5),
	],

	/*
	 * Recommendation next steps
	 */
	'reco-next-steps'      => [
		'1' => 'I want to book',
		'2' => 'I need more details',
		'3' => 'I want to customise',
		'4' => 'Give options in higher price range',
		'5' => 'Give options in lower price range',
		'6' => 'Other, mentioned in message',
	],

	'type_reminder' => [
		'followup'       => env('TYPE_REMINDER_FOLLOWUP', 1),
		'thank-you-card' => env('TYPE_REMINDER_THANK_YOU_CARD', 2),
		'retention'      => env('TYPE_REMINDER_RETENTION', 3)
	],

	'referral' => [
		'source' => [
			"whatsapp"  => env('TYPE_SOURCE_WHATSAPP', 33),
			"facebook"  => env('TYPE_SOURCE_FACEBOOK', 7),
			"messenger" => env('TYPE_SOURCE_MESSENGER', 34),
			"email"     => env('TYPE_SOURCE_EMAIL', 4),
			"twitter"   => env('TYPE_SOURCE_TWITTER', 30),
			"sms"       => env('TYPE_SOURCE_SMS', 35)
		]
	],

	'products' => [
		'percent-ranges' => [
			'defaults' => [
				'negPercent' => 10,
				'posPercent' => 30
			]
		]
	],

	"feedback" => [
		"question" => [
			"re" => [
				"id"     => env("FEEDBACK_QUESTION_RE_ID", 2),
				"option" => [
					"yes" => env("FEEDBACK_QUESTION_RE_YES", 8),
					"no"  => env("FEEDBACK_QUESTION_RE_NO", 9)
				]
			]
		]
	],

	'cache' => [
		'refresh-time' => env('CACHE_REFRESH_TIME', 360),
		'expiry-time'  => env('CACHE_EXPIRY_TIME', '23:30:00')
	],

	/*
	|-------------------------------------------------------------------------
	| Social Stats
	|--------------------------------------------------------------------------
	*/
	'stats' => [
		"parties_done" => env("TOTAL_PARTIES_DONE", "10K+")
	],

	'occasionId' => [
		env('TYPE_EVENT_BIRTHDAY', 1)      => 'Birthday Party Planners',
		env('TYPE_EVENT_COPORATES', 5)     => 'Corporates Event Management',
		env('TYPE_EVENT_BACHELOR', 11)     => 'Youth Party',
		env('TYPE_EVENT_PREPOST', 13)      => 'Engagement and Wedding Reception',
		env('TYPE_EVENT_HOUSEWARMING', 14) => 'House Warming',
		env('TYPE_EVENT_SURPRISES', 16)    => 'Surprise Planners'
	],

	'e-invite' => [
		'g-form-url' => env('E_INVITE_G_FORM_URL', 'https://goo.gl/forms/xez6ACwq36OgfAwi2')
	],

	'type-rsvp' => [
		'yes'    => env('TYPE_RSVP_YES', 1),
		'may-be' => env('TYPE_RSVP_MAY_BE', 2),
		'no'     => env('TYPE_RSVP_NO', 3),
	],

	'review'              => [
		'question' => [
			'ReferEarn' => env("REFERANDEARN_REVIEW_QUESTION_ID", 2)
		]
	],

	/* should be in sync with 'type_ticket_booking' table */
	'type-ticket-booking' => [
		'venue'                     => env('TYPE_TICKET_BOOKING_VENUE', 3),
		'theme-decoration'          => env('TYPE_TICKET_BOOKING_THEME_DECORATION', 5),
		'entertainment'             => env('TYPE_TICKET_BOOKING_ENTERTAINMENT', 6),
		'catering'                  => env('TYPE_TICKET_BOOKING_CATERING', 8),
		'cake'                      => env('TYPE_TICKET_BOOKING_CAKE', 9),
		'snacks'                    => env('TYPE_TICKET_BOOKING_SNACKS', 10),
		'organiser'                 => env('TYPE_TICKET_BOOKING_ORGANISER', 11),
		'photographer'              => env('TYPE_TICKET_BOOKING_PHOTOGRAPHER', 12),
		'videographer'              => env('TYPE_TICKET_BOOKING_VIDEOGRAPHER', 13),
		'special-artist'            => env('TYPE_TICKET_BOOKING_SPECIAL_ARTIST', 14),
		'add-on-services'           => env('TYPE_TICKET_BOOKING_ADD_ON_SERVICES', 15),
		'balloon-decoration'        => env('TYPE_TICKET_BOOKING_BALLOON_DECORATION', 16),
		'decoration-entertainment'  => env('TYPE_TICKET_BOOKING_DECORATION_ENTERTAINMENT', 17),
		'custom-balloon-decoration' => env('TYPE_TICKET_BOOKING_CUSTOM_BALLOON_DECORATION', 18),
		'trends'                    => env('TYPE_TICKET_BOOKING_TRENDS', 19)
	],

	'type-tag'  => [
		'craft-theme-decoration'      => env('TYPE_TAG_CRAFT_THEME_DECORATION', 122),
		'exclusive-drapes-decoration' => env('TYPE_TAG_EXCLUSIVE_DRAPES_DECORATION', 95),
		'elegant-flower-decoration'   => env('TYPE_TAG_ELEGANT_FLOWER_DECORATION', 92),
		'basic-flower-decoration'     => env('TYPE_TAG_BASIC_FLOWER_DECORATION', 90),
		'elegant-theme-decoration'    => env('TYPE_TAG_ELEGANT_THEME_DECORATION', 53),
		'drapes-decoration'           => env('TYPE_TAG_DRAPES_DECORATION', 52),
		'custom-balloon-decoration'   => env('TYPE_TAG_CUSTOM_BALLOON_DECORATION', 51),
		'custom-theme-decoration'     => env('TYPE_TAG_CUSTOM_THEME_DECORATION', 23),
		'simple-theme-decoration'     => env('TYPE_TAG_SIMPLE_THEME_DECORATION', 22),
		'simple-balloon-decoration'   => env('TYPE_TAG_SIMPLE_BALLOON_DECORATION', 21),
		'decor-category'              => env('TYPE_TAG_DECOR_CATEGORY', 15),
		'service'                     => env('TYPE_TAG_SERVICE', 187),
		'service-category'            => env('TYPE_TAG_SERVICE_CATEGORY', 189),
		'eateries'                    => env('TYPE_TAG_EATERIES', 190),
		'food'                        => env('TYPE_TAG_FOOD', 67),
		'decoration-entertainment'    => env('TYPE_TAG_DECORATION_ENTERTAINMENT', 66),
		'cake'                        => env('TYPE_TAG_CAKE', 233),
		'valentines-day-2020'         => env('TYPE_TAG_VALENTINES_DAY_2020', 262),
		'valentines-day-2021'         => env('TYPE_TAG_VALENTINES_DAY_2020', 266),
		'candle-light-dinners'        => env('TYPE_TAG_CANDLE_LIGHT_DINNERS', 186)
	],

	/* E-card types */
	'type-card' => [
		'invite'         => env('TYPE_CARD_INVITE', 1),
		'wish-card'      => env('TYPE_CARD_WISH', 2),
		'thank-you-card' => env('TYPE_CARD_THANK_YOU', 3),
		'gif'            => env('TYPE_CARD_GIF', 4),
	],

	'type-card-category' => [
		'free' => env('TYPE_CARD_CATEGORY_FREE', 1),
		'pro'  => env('TYPE_CARD_CATEGORY_PRO', 2),
	],

	/* Default partner */
	/* Include venue partners if needed */
	'default-partner'    => [
		'id'   => env('DEFAULT_PLANNER_ID', 518),
		'city' => [
			'1' => [
				'id' => env('DEFAULT_BANGALORE_PARTNER_ID', 220),
			],
			'2' => [
				'id' => env('DEFAULT_HYDERABAD_PARTNER_ID', 518),
			],
			'3' => [
				'id' => env('DEFAULT_DELHI_PARTNER_ID', 724),
			],
			'4' => [
				'id' => env('DEFAULT_MUMBAI_PARTNER_ID', 633),
			],
			'5' => [
				'id' => env('DEFAULT_PUNE_PARTNER_ID', 725),
			],
		]
	],

	'popup' => [
		'categories' => [
			"Cakes"         => 6,
			"Decoration"    => 13,
			"Entertainment" => 14,
			"Food"          => 18,
			"Photography"   => 14
		]
	],

	'check-in-slots' => [
		env('CANDLE_LIGHT_DINNER_TAG_ID', 186) => [
			0 => [
				"checkInTime"  => "12:30 PM",
				"checkOutTime" => "2:30 PM",
			],
			1 => [
				"checkInTime"  => "1:00 PM",
				"checkOutTime" => "3:00 PM",
			],
			2 => [
				"checkInTime"  => "7:00 PM",
				"checkOutTime" => "9:00 PM",
			],
			3 => [
				"checkInTime"  => "7:30 PM",
				"checkOutTime" => "9:30 PM",
			],
			4 => [
				"checkInTime"  => "8:00 PM",
				"checkOutTime" => "10:00 PM",
			],
			5 => [
				"checkInTime"  => "8:30 PM",
				"checkOutTime" => "10:30 PM",
			],
			6 => [
				"checkInTime"  => "9:00 PM",
				"checkOutTime" => "11:00 PM",
			]
		],
		env('CAKE_CUTTING_PLACE_TAG_ID', 260)  => [
			0 => [
				"checkInTime"  => "4:00 PM",
				"checkOutTime" => "6:00 PM",
			],
			1 => [
				"checkInTime"  => "10:00 PM",
				"checkOutTime" => "12:00 AM",
			],
		]
	],

	'cssURLKey' => env('CSS_URL_KEY', "all-"),

	'offer' => [
		'default' => [
			'key'         => env('DEFAULT_OFFER_KEY', 1),
			'key_piab'    => env('DEFAULT_OFFER_KEY_PIAB', 1),
			'coupon_piab' => env('DEFAULT_OFFER_COUPON_PIAB', 73547),
			'evibe_star'  => env('DEFAULT_OFFER_EVIBE_STAR', 74070),
			'vday2020'    => env('DEFAULT_OFFER_VDAY_2020', 77946)
		]
	],

	'token' => [
		'track-my-order' => 'EVBTMO',
		'delhivery'      => '2b575f6fd5a4c7291bce8be186aea87ca110312d'
	],

	'product-booking-status' => [
		'initiated' => env('PRODUCT_BOOKING_STATUS_INITIATED', 1),
		'paid'      => env('PRODUCT_BOOKING_STATUS_PAID', 2),
		'shipped'   => env('PRODUCT_BOOKING_STATUS_SHIPPED', 3),
		'delivered' => env('PRODUCT_BOOKING_STATUS_DELIVERED', 4),
		'cancelled' => env('PRODUCT_BOOKING_STATUS_CANCELLED', 5),
	],

	'review-question' => [
		'why-choose-evibe' => env('REVIEW_QUESTION_WHY_CHOOSE_EVIBE', 1),
	],

	'piab-customer-reviews' => [
		1 => [
			'name'   => 'Mr. Chaitanya',
			'photo'  => 'chaitanya.jpg',
			'review' => 'We wanted to throw a welcome party for my aunt who was in recovery and ordered a party-kit from evibe which was delivered within the next day. After a long time it felt like my cousins and I had some fun time decorating the room. My aunt broke into tears after seeing the gesture. Thank you evibe for enabling us with such great memories.'
		],
		2 => [
			'name'   => 'Mr. Jeevan',
			'photo'  => 'jeevan.jpg',
			'review' => 'Cousin\'s birthday is coming up & it\'s the first time we were celebrating away from our hometown, so we didn\'t know any local shops here to buy materials & decorate the house. We were browsing online and found evibe party kits. I received the delivery on time. My cousin was really surprised & amazed that we did that decoration by our self. Thanks evibe for the hassle free kit.'
		],
		3 => [
			'name'   => 'Mr. Vikas Malpani',
			'photo'  => 'vikas-malpani.jpg',
			'review' => 'Ordered party kit from Evibe to celebrate my kid\'s birthday party and I must say, I was surprised at the premium quality that I got for such a reasonable price. And the delivery too is lightning fast. Thank you Evibe for such a great product.'
		]
	],

	'store' => [
		'promo-header' => [
			'msg' => env('STORE_PROMO_HEADER_MSG', 'Latest Party Props Delivered To Your Doorstep'),
			'url' => env('STORE_PROMO_HEADER_URL', 'https://store.evibe.in')
		]
	],

	'decommission-time' => '1644863400' // 00:00 Feb 15, 2022
];
