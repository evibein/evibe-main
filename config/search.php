<?php

return [
	'category'  => [
		'filters' => [
			env('TYPE_PAGE_PACKAGE', 1)           => [
				"id"    => 1,
				"title" => "Packages",
				"icon"  => "packages2_g.png"
			],
			env('TYPE_PAGE_SERVICE', 4)           => [
				"id"    => 4,
				"title" => "Service",
				"icon"  => "entertainment2_g.png"
			],
			env('TYPE_PAGE_TREND', 5)             => [
				"id"    => 5,
				"title" => "Trend",
				"icon"  => "trends_g.png"
			],
			env('TYPE_PAGE_CAKE', 6)              => [
				"id"    => 6,
				"title" => "Cake",
				"icon"  => "cake2_g.png"
			],
			env('TYPE_PAGE_DECOR', 13)            => [
				"id"    => 13,
				"title" => "Decor",
				"icon"  => "decoration2_g.png"
			],
			env('TYPE_PAGE_ENT', 14)              => [
				"id"    => 14,
				"title" => "Entertainment",
				"icon"  => "entertainment2_g.png"
			],
			env('TYPE_PAGE_RESORT', 17)           => [
				"id"    => 17,
				"title" => "Resorts",
				"icon"  => "resorts_g.png"
			],
			env('TYPE_PAGE_VILLA', 18)            => [
				"id"    => 18,
				"title" => "Villas",
				"icon"  => "villa_g.png"
			],
			env('TYPE_PAGE_LOUNGE', 19)           => [
				"id"    => 19,
				"title" => "Lounges",
				"icon"  => "lounge_g.png"
			],
			env('TYPE_PAGE_FOOD', 20)             => [
				"id"    => 20,
				"title" => "Food",
				"icon"  => "food2_g.png"
			],
			env('TYPE_PAGE_SURPRISE_PACKAGE', 21) => [
				"id"    => 21,
				"title" => "Surprises",
				"icon"  => "packages2_g.png"
			],
			env('TYPE_PAGE_VENUE_DEALS', 22)      => [
				"id"    => 22,
				"title" => "Venue Deals",
				"icon"  => "venue2_g.png"
			],
			env('TYPE_PAGE_PRIEST', 23)           => [
				"id"    => 23,
				"title" => "Priest",
				"icon"  => "priest_g.png"
			],
			env('TYPE_PAGE_TENT', 24)             => [
				"id"    => 24,
				"title" => "Tent",
				"icon"  => "tent2_g.png"
			],
		]
	],

	// default keywords mentioning should have id_str & should mention the same in header_js
	'auto_hint' => [

		0 => [
			"search?q=Birthday%20theme%20decorations"      => 'Birthday theme decorations',
			"search?q=Housewarming%20flower%20decorations" => 'Housewarming flower decorations',
			"search?q=Theme%20cakes"                       => 'Theme cakes',
			"search?q=Candle%20light%20dinner"             => 'Candle light dinner',
			"search?q=Home%20decorations"                  => 'Home decorations',
			"search?q=Unique%20Surprises"                  => 'Unique Surprises'
		],

		env('CITY_BANGALORE_ID', 1) => [

			0 => [
			],

			env('TYPE_EVENT_BIRTHDAY', 1) => [

				0 => [
					'search?q=Simple%20theme%20decorations'      => 'Simple theme decorations',
					'search?q=Naming%20ceremony%20decorations'   => 'Naming ceremony decorations',
					'search?q=Baby%20shower%20decorations'       => 'Baby shower decorations',
					'search?q=3D%20theme%20decorations'          => '3D theme decorations',
					'search?q=Theme%20cakes'                     => 'Theme cakes',
					'search?q=Mascots'                           => 'Mascots',
					'search?q=Emcee%20Magic%20show%20Caricature' => 'Emcee, Magic show, Caricature',
					'search?q=Candid%20photography'              => 'Candid photography'
				],

				env('TYPE_PAGE_DECOR', 13) => [
					'search?q=Princess%20theme%20decoration'     => 'Princess theme decoration',
					'search?q=Frozen%20theme%20decoration'       => 'Frozen theme decoration',
					'search?q=Chota%20Bheem%20decoration'        => 'Chota Bheem decorations',
					'search?q=Little%20man%20theme%20decoration' => 'Little man theme decoration',
					'search?q=Butterfly%20theme%20decoration'    => 'Butterfly theme decoration',
					'search?q=Jungle%20theme%20decoration'       => 'Jungle theme decoration',
					'search?q=Minion%20theme%20decoration'       => 'Minion theme decoration',
					'search?q=Naming%20ceremony%20decorations'   => 'Naming ceremony decorations',
					'search?q=Baby%20shower%20decorations'       => 'Baby shower decorations',
					'search?q=3D%20theme%20decorations'          => '3D theme decorations',
					'search?q=Simple%20theme%20decorations'      => 'Simple theme decorations',
					'search?q=Simple%20balloon%20decorations'    => 'Simple balloon decorations',
					'search?q=Craft%20decorations'               => 'Craft decorations'
				]

			],

			env('TYPE_EVENT_BACHELOR', 11) => [

				0 => []

			],

			env('TYPE_EVENT_PRE_POST', 13) => [

				0 => []

			],

			env('TYPE_EVENT_HOUSE_WARMING', 14) => [

				0 => [
					'search?q=1BHK%20flower%20decor'                => '1BHK flower decor',
					'search?q=2BHK%20flower%20decor'                => '2BHK flower decor',
					'search?q=3BHK%20flower%20decor'                => '3BHK flower decor',
					'search?q=Housewarming%20entrance%20decoration' => 'Housewarming entrance decoration',
					'search?q=Priest%20for%20housewarming'          => 'Priest for housewarming',
					'search?q=Shamiyana%20for%20housewarming'       => 'Shamiyana for housewarming'
				]

			],

			env('TYPE_EVENT_SURPRISES', 16) => [

				0 => [
					'search?q=Candle%20light%20dinner'      => 'Candle light dinner',
					'search?q=Unique%20birthday%20surprise' => 'Unique birthday surprise',
					'search?q=Romantic%20surprise'          => 'Romantic surprise',
					'search?q=Poolside%20dinner'            => 'Poolside dinner'
				]

			],

			env('TYPE_EVENT_CORPORATES', 5) => [

			]

		],

		env('CITY_HYDERABAD_ID', 2) => [

			0 => [
			],

			env('TYPE_EVENT_BIRTHDAY', 1) => [

				0 => [
					'search?q=Simple%20theme%20decorations'      => 'Simple theme decorations',
					'search?q=Naming%20ceremony%20decorations'   => 'Naming ceremony decorations',
					'search?q=Baby%20shower%20decorations'       => 'Baby shower decorations',
					'search?q=3D%20theme%20decorations'          => '3D theme decorations',
					'search?q=Theme%20cakes'                     => 'Theme cakes',
					'search?q=Mascots'                           => 'Mascots',
					'search?q=Emcee%20Magic%20show%20Caricature' => 'Emcee, Magic show, Caricature',
					'search?q=Candid%20photography'              => 'Candid photography'
				],

				env('TYPE_PAGE_DECOR', 13) => [
					'search?q=Princess%20theme%20decoration'     => 'Princess theme decoration',
					'search?q=Frozen%20theme%20decoration'       => 'Frozen theme decoration',
					'search?q=Chota%20Bheem%20decoration'        => 'Chota Bheem decorations',
					'search?q=Little%20man%20theme%20decoration' => 'Little man theme decoration',
					'search?q=Butterfly%20theme%20decoration'    => 'Butterfly theme decoration',
					'search?q=Jungle%20theme%20decoration'       => 'Jungle theme decoration',
					'search?q=Minion%20theme%20decoration'       => 'Minion theme decoration',
					'search?q=Naming%20ceremony%20decorations'   => 'Naming ceremony decorations',
					'search?q=Baby%20shower%20decorations'       => 'Baby shower decorations',
					'search?q=3D%20theme%20decorations'          => '3D theme decorations',
					'search?q=Simple%20theme%20decorations'      => 'Simple theme decorations',
					'search?q=Simple%20balloon%20decorations'    => 'Simple balloon decorations',
					'search?q=Craft%20decorations'               => 'Craft decorations'
				]

			],

			env('TYPE_EVENT_BACHELOR', 11) => [

				0 => []

			],

			env('TYPE_EVENT_PRE_POST', 13) => [

				0 => []

			],

			env('TYPE_EVENT_HOUSE_WARMING', 14) => [

				0 => [
					'search?q=1BHK%20flower%20decor'                => '1BHK flower decor',
					'search?q=2BHK%20flower%20decor'                => '2BHK flower decor',
					'search?q=3BHK%20flower%20decor'                => '3BHK flower decor',
					'search?q=Housewarming%20entrance%20decoration' => 'Housewarming entrance decoration',
					'search?q=Priest%20for%20housewarming'          => 'Priest for housewarming',
					'search?q=Shamiyana%20for%20housewarming'       => 'Shamiyana for housewarming'
				]

			],

			env('TYPE_EVENT_SURPRISES', 16) => [

				0 => [
					'search?q=Candle%20light%20dinner'      => 'Candle light dinner',
					'search?q=Unique%20birthday%20surprise' => 'Unique birthday surprise',
					'search?q=Romantic%20surprise'          => 'Romantic surprise',
					'search?q=Poolside%20dinner'            => 'Poolside dinner'
				]

			],

			env('TYPE_EVENT_CORPORATES', 5) => [

			]

		],

		env('CITY_DELHI_ID', 3) => [],

		env('CITY_MUMBAI_ID', 4) => [],

		env('CITY_PUNE_ID', 5) => []
	]
];