<?php

return [

	"recommendation" => [

		"rating" => [

			"skipped" => "Customer has skipped rating the recommendations",

			"rated" => "Customer has rated the recommendations"
		],

		"dislike" => [

			"followup" => "Customer did not like the recommendations, need to followup."

		],

		"shortlist" => [

			"complete" => "Recommendation followup marked as complete as customer responded to the recommendations."

		]

	]
];