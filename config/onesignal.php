<?php

return [

	"path" => env("ONESIGNAL_API_PATH", "https://onesignal.com/api/v1"),

	"dash" => [
		"appId"     => env("ONESIGNAL_APP_ID_DASH", "2fddf801-5723-4d68-bec4-8db284bd7c1c"),
		"apiKey"    => env("ONESIGNAL_API_KEY_DASH", "MzAyYTc2NWUtNGY4My00YWQ3LTkzOWYtMjk1Y2FlNWI5YzJi"),
		"subdomain" => env("ONESIGNAL_SUBDOMAIN_DASH", "evibe-dash"),
		"segments"  => [
			"crm"    => [env("ONESIGNAL_SEGMENT_CRM", "CRM Team"), env("ONESIGNAL_SEGMENT_SR_CRM", "Sr CRM")],
			"admins" => [env("ONESIGNAL_SEGMENT_ADMIN", "Admins")],
			"bd"     => [env("ONESIGNAL_SEGMENT_BD", "BD Team")]
		]
	],

	"main" => [
		"appId"  => env("ONESIGNAL_APP_ID_MAIN", "da6d0d8d-1ddc-4213-9661-38085e1d41de"),
		"apiKey" => env("ONESIGNAL_API_KEY_MAIN", "NDM0ZWE2NjctNmM3Ni00ODU1LTg1ODctOGMyODgwYzgwN2Vk")
	]

];