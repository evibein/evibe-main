<?php

/**
 * All SEO text for various pages
 *
 * Grammar:
 * ------------------
 * pt: page title
 * pd: page description
 * ph: page header
 * kw: keywords
 * {replace}: replaceable text
 * ------------------
 *
 * @author  Anji <anji@evibe.in>
 * @since   29 Dec 2016
 *
 * @version 04-Feb-2019
 */

return [

	/* all static pages */
	"app"       => [

	],

	/* campaigns */
	"campaigns" => [

	],

	/* landing */
	"landing"   => [

		"corporates" => [
			"pt" => "Top event management for corporate events, office parties, festival celebrations in {city}",
			"pd" => "{city}'s best platform to book event services for corporate events like anniversaries, product launches, family day, kids day, office parties etc. Book decorations, artists, entertainers, sound/lights & many other services at best price.",
			"kw" => "{city} corporate events, corporate event management {city}"
		]

	],

	/* defaults */
	"defaults"  => [

		"base" => [
			"pt" => "Best birthday party planners, surprise planners, housewarming",
			"pd" => "Book best party services at best prices & quality. Top theme decorations, candle light dinners, cakes & more. Real pictures & customer reviews.",
			"ph" => "Trusted Party Planners",
			"kw" => "birthday planners, party organisers, bachelor party organiser, wedding reception planners, engagement planners, surprise planners"
		],

		"city" => [

			/* city home page */
			"home"                               => [
				"pt" => "Best birthday party planners, surprise planners, housewarming in {city}",
				"pd" => "Book top party services in {city} at best prices & quality. Top themes decorations, candle light dinners, surprises & more. See real pictures & customer reviews.",
				"ph" => "Let's plan your party online",
				"kw" => "birthday planners in {city}, party organisers in {city}, bachelor party organiser in {city}, wedding reception planners in {city}, engagement planners in {city}, surprise planner in {city}"
			],

			/* city collections */
			"collections"                        => [

				"list" => [
					"pt" => "Best party collections in {city}",
					"pd" => "Top party collections with decorations, cakes, entertainers, catering, venue deals & more. Best prices & 100% delivery guarantee.",
					"ph" => "best party collections",
					"kw" => "collections, {city} collections, collections in {city}"
				],

				"profile" => [
					"pt" => "Top {item} - party collections in {city}",
					"pd" => "Best {item} collections for party in {city}. Best deals & 100% delivery guarantee",
					"ph" => "{item}",
					"kw" => "{item} collection in {city}, {city}, {item}"
				]

			],

			/* Naming ceremony */
			env('TYPE_EVENT_NAMING_CEREMONY', 8) => [

				env('TYPE_PAGE_DECOR', 13) => [

					"list" => [
						"pt" => "Best Naming Ceremony Decorations, Balloon decorations in {city}",
						"pd" => "Book best naming ceremony decorations, balloon decorations for birthday party in {city}. Best prices, real pictures with home delivery.",
						"ph" => "Best naming cereomony decorations in {city}",
						"kw" => "birthday decorations in {city}, balloon decorations in {city}, {city} birthday decorations, birthday theme decorations in {city}"
					],

					"filter" => [
						"pt" => "Best {tagTitle}, birthday decorations in {city}",
						"pd" => "{city}'s top {tagDesc}. Free home delivery.",
						"ph" => "Best birthday {tagName} in {city}",
						"kw" => "birthday decorations in {city}, balloon decorations in {city}, {city} birthday decorations, birthday {tagName} in {city}"
					],

					"profile" => [
						"pt" => "{item} - birthday {category} in {city}",
						"pd" => "Book best {item} birthday {category} from top birthday planners in {city}. Real images, customer reviews, best prices.",
						"ph" => "{item}",
						"kw" => "{item} in {city}, {category} in {city}, {city} {category}, {item}, {category}"
					]
				],
			],

			/* Store Openings */
			env('TYPE_EVENT_STORE_OPENING', 20) => [

				env('TYPE_PAGE_DECOR', 13) => [

					"list" => [
						"pt" => "Best Store Opening Decorations, Balloon decorations in {city}",
						"pd" => "Book best store opening decorations, balloon decorations  in {city}. Best prices, real pictures with home delivery.",
						"ph" => "Best Store Opening Decorations in {city}",
						"kw" => "store opening decorations in {city}, balloon decorations in {city}, {city} shop decorations,  theme decorations in {city}"
					],

					"filter" => [
						"pt" => "Best {tagTitle}, birthday decorations in {city}",
						"pd" => "{city}'s top {tagDesc}. Free home delivery.",
						"ph" => "Best birthday {tagName} in {city}",
						"kw" => "store opening decorations in {city}, balloon decorations in {city}, {city} shop decorations,  theme decorations in {city}"
					],

					"profile" => [
						"pt" => "{item} -  {category} in {city}",
						"pd" => "Book best {item}  {category} from top birthday planners in {city}. Real images, customer reviews, best prices.",
						"ph" => "{item}",
						"kw" => "{item} in {city}, {category} in {city}, {city} {category}, {item}, {category}"
					]
				],
			],

			/* Baby Shower */
			env('TYPE_EVENT_BABY_SHOWER', 19)    => [

				env('TYPE_PAGE_DECOR', 13) => [

					"list" => [
						"pt" => "Best Baby Shower Decorations, Balloon decorations in {city}",
						"pd" => "Book the best baby shower decorations, balloon decorations  in {city}. Best prices, real pictures with home delivery.",
						"ph" => "Best naming cereomony decorations in {city}",
						"kw" => "Baby shower decorations,birthday decorations in {city}, balloon decorations in {city}, {city} birthday decorations, birthday theme decorations in {city}"
					],

					"filter" => [
						"pt" => "Best {tagTitle}, birthday decorations in {city}",
						"pd" => "{city}'s top {tagDesc}. Free home delivery.",
						"ph" => "Best birthday {tagName} in {city}",
						"kw" => "birthday decorations in {city}, balloon decorations in {city}, {city} birthday decorations, birthday {tagName} in {city}"
					],

					"profile" => [
						"pt" => "{item} - birthday {category} in {city}",
						"pd" => "Book best {item} birthday {category} from top birthday planners in {city}. Real images, customer reviews, best prices.",
						"ph" => "{item}",
						"kw" => "{item} in {city}, {category} in {city}, {city} {category}, {item}, {category}"
					]
				],
			],

			/* birthday parties */
			env('TYPE_EVENT_BIRTHDAY', 1)        => [

				/* landing */
				"landing"                        => [
					"pt" => "Best birthday party planners, party organisers in {city}",
					"pd" => "{city}'s top birthday party organisers, balloon decorations, theme decorations, themes, cakes, function halls, artists for birthday party in {city}. Best prices & 100% delivery guarantee.",
					"ph" => "India\'s Most Trusted Birthday Party Planners",
					"kw" => "birthday party planners in {city}, {city} birthday planners, birthday party organisers in {city}, {city} birthday organisers"
				],

				/* collections */
				"collections"                    => [

					"list" => [
						"pt" => "Best birthday party collections in {city}",
						"pd" => "Top birthday party collections with decorations, theme cakes, entertainers, catering & more. Best prices & 100% delivery guarantee.",
						"ph" => "best birthday party collections",
						"kw" => "birthday collections, {city} birthday collections, birthday collections in {city}"
					],

					"profile" => [
						"pt" => "Top {item} - birthday party collections in {city}",
						"pd" => "Best {item} collections for birthday party in {city}. Best deals & 100% delivery guarantee",
						"ph" => "{item}",
						"kw" => "{item} collection in {city}, {city}, {item}"
					]

				],

				/* packages */
				env('TYPE_PAGE_PACKAGE', 1)      => [

					"list" => [
						"pt" => "Best birthday party packages in {city}",
						"pd" => "Top {city}'s birthday party packages with balloon decorations, kids entertainment, kids snacks & more. See real picture & customer reviews. Best prices & quality service.",
						"ph" => "All Birthday Party Packages",
						"kw" => "birthday packages in {city}, birthday party packages in {city}, {city} birthday packages"
					],

					"profile" => [
						"pt" => "Book {item} birthday party package in {city}",
						"pd" => "Book {city}'s best birthday party package: {item} for Rs. {price}. Best price & quality service",
						"ph" => "{item}",
						"kw" => "{item} in {city}, {item} for birthday party packages"
					]
				],

				/* tent */
				env('TYPE_PAGE_TENT', 24)        => [

					"list" => [
						"pt" => "Best birthday party tent services in {city}",
						"pd" => "Book {city}'s best tent, chair services for birthday party in {city}. Best prices & quality service.",
						"ph" => "Best birthday party tent packages In {city}",
						"kw" => "birthday party tent in {city}, {city} tent, birthday tent, chairs, birthday tent services in {city}"
					],

					"profile" => [
						"pt" => "Book {item} - birthday party tent service in {city}",
						"pd" => "Book {city}'s best birthday party tent service: {item} for Rs. {price}. Best deals & quality service.",
						"ph" => "{item}",
						"kw" => "{item} in {city}, {item} for birthday party tent"
					]
				],

				/* decor styles */
				env('TYPE_PAGE_DECOR', 13)       => [

					"list" => [
						"pt" => "Best birthday theme decorations, balloon decorations in {city}",
						"pd" => "Top first birthday theme decorations, balloon decorations for birthday party in {city}. Best prices, real pictures with home delivery.",
						"ph" => "Best birthday party decorations in {city}",
						"kw" => "birthday decorations in {city}, balloon decorations in {city}, {city} birthday decorations, birthday theme decorations in {city}"
					],

					"filter" => [
						"pt" => "Best {tagTitle}, birthday decorations in {city}",
						"pd" => "{city}'s top {tagDesc}. Free home delivery.",
						"ph" => "Best birthday {tagName} in {city}",
						"kw" => "birthday decorations in {city}, balloon decorations in {city}, {city} birthday decorations, birthday {tagName} in {city}"
					],

					"profile" => [
						"pt" => "{item} - birthday {category} in {city}",
						"pd" => "Book best {item} birthday {category} from top birthday planners in {city}. Real images, customer reviews, best prices.",
						"ph" => "{item}",
						"kw" => "{item} in {city}, {category} in {city}, {city} {category}, {item}, {category}"
					]

				],

				/* venue deals */
				env('TYPE_PAGE_VENUE_DEALS', 22) => [

					"list" => [
						"pt" => "Best banquet halls, party halls for birthday party{list-cat} in {city}",
						"pd" => "Top banquet halls, function halls for kids birthday party in {city} at star hotels, resorts at top locations. Best prices, real pictures, menu, facilities & more.",
						"ph" => "Best birthday function halls in {city}",
						"kw" => "birthday party halls, banquet halls, function halls, {city} banquet halls, {city} function halls"
					],

					"filter" => [
						"pt" => "Best banquet halls, party halls for birthday in {location}, {city}",
						"pd" => "Top banquet halls, function halls for birthday party in {location}, {city} at star hotels, top places. Best prices, real pictures, menu, facilities & more.",
						"ph" => "Best birthday function halls in {location}, {city}",
						"kw" => "birthday party halls in {location}, {location} function halls, banquet halls in {location}, {location} {city} party halls"
					],

					"filter-cat" => [
						"pt" => "Best {category} banquet halls for birthday in {location}, {city}",
						"pd" => "Top banquet halls, function halls for birthday party in {location}, {city} at {category}. Best prices, real pictures, menu, facilities & more.",
						"ph" => "Best birthday function halls in {location}, {city}",
						"kw" => "birthday party halls in {location}, {location} function halls, banquet halls in {location}, {location} {city} party halls"
					],

					"profile" => [
						"pt" => "Book {venue} banquet hall in {location}, {city} for birthday party",
						"pd" => "Book {venue} top banquet hall, party hall for birthday parties in {location}, {city}. Check availability, best price, menu, real pictures & customer reviews.",
						"ph" => "{item}",
						"kw" => "{venue} {location} {city}, banquet hall in {location}, party hall in {location}, {venue} banquet hall in {city}"
					]
				],

				/* cakes */
				env('TYPE_PAGE_CAKE', 6)         => [

					"list" => [
						"pt" => "Order online birthday theme cakes, standard cakes in {city}, free home delivery",
						"pd" => "Order {city} best birthday theme cakes, standard cakes. Frozen theme, jungle theme, photo cakes & more. Egg & egg-less options. Free home delivery.",
						"ph" => "Best birthday cakes in {city}",
						"kw" => "birthday theme cakes in {city}, {city} theme cakes, {city} birthday cakes, {city} order cakes online"
					],

					"filter" => [
						"pt" => "Order online birthday {tagTitle} cakes, standard cakes in {city}, free home delivery",
						"pd" => "Order {city}'s best birthday {tagDesc}. Egg & egg-less options. Free home delivery.",
						"ph" => "Best birthday {tagName} cakes in {city}",
						"kw" => "birthday theme cakes in {city}, {city} theme cakes, {city} birthday cakes, {city} order cakes online, birthday {tagName} in {city}"
					],

					"profile" => [
						"pt" => "Order {item} online - birthday cake in {city}, free home delivery",
						"pd" => "Order online best birthday cake - {item} in {city}. Fresh birthday cakes, theme cakes. Free home delivery available.",
						"ph" => "{item}",
						"kw" => "birthday cake {item}, {item} cake in {city}, {city}, {item} birthday cake"
					]
				],

				/* food */
				env('TYPE_PAGE_FOOD', 20)        => [

					"list" => [
						"pt" => "Best birthday party caterers, snack box, veg/non-veg combos in {city}",
						"pd" => "Book {city}'s small party caterers, home party caterers, snack box, mini pizzas, cup cakes, veg/non-veg combos for birthday party in {city}. Best price & quality.",
						"ph" => "Best Birthday Party Catering Combos In {city}",
						"kw" => "Birthday caterers {city}, {city} small birthday caterers, home party caterers in {city}"
					],

					"profile" => [
						"pt" => "Book {item} - birthday party catering in {city}",
						"pd" => "Book best birthday party catering {item} for Rs. {price} in {city}. Best price & quality.",
						"ph" => "{item}",
						"kw" => "birthday catering in {city}, {item} in {city}"
					]

				],

				/* ent */
				env('TYPE_PAGE_ENT', 14)         => [

					"list" => [
						"pt" => "Best birthday party entertainment, activities, face painting, clowns & more services in {city}",
						"pd" => "{city}'s best birthday magic shows, puppet show, clowns, face painting, tattoo artists, bouncing castles & more birthday party entertainment. Best prices & quality service.",
						"ph" => "Best birthday party entertainment services in {city}",
						"kw" => "birthday party entertainment in {city}, {city} entertainment for birthday party"
					],

					"filter" => [
						"pt" => "Top {category} - birthday party entertainment in {city}",
						"pd" => "{city}'s best {category} - entertainment for birthday party. See real photos, pricing. Best price & 100% service delivery guarantee with quality service.",
						"ph" => "Best birthday party entertainment {category} in {city}",
						"kw" => "entertainments in {city}, entertainments {category} in {city}, {occasion} entertainment in {city}, {occasion} {category} entertainment in {city}"
					],

					"profile" => [
						"pt" => "Book {item} for birthday party entertainment in {city}",
						"pd" => "Book {city}'s birthday party kids entertainment {item}, activity with details, real pictures. Best prices & quality service.",
						"ph" => "{item}",
						"kw" => "birthday party entertainment in {city}, {city} entertainment for birthday party, {item} for birthday entertainment"
					]

				]

			],

			/* pre post */
			env('TYPE_EVENT_PREPOST', 13)        => [

				/* collections */
				"collections"              => [

					"list" => [
						"pt" => "Best wedding reception, engagement party collections in {city}",
						"pd" => "Top wedding reception, engagement party collections with stage decorations, flower decorations, theme cakes, entertainers, catering & more. Best prices & 100% delivery guarantee.",
						"ph" => "best wedding reception, engagement party collections",
						"kw" => "wedding reception, engagement collections, {city} wedding reception, engagement collections, wedding reception, engagement collections in {city}"
					],

					"profile" => [
						"pt" => "Top {item} - wedding reception, engagement party collection in {city}",
						"pd" => "Best {item} collections for wedding reception, engagement party in {city}. Best deals & 100% delivery guarantee",
						"ph" => "{item}",
						"kw" => "{item} collection in {city}, {city}, {item}"
					]

				],

				/* decor styles */
				env('TYPE_PAGE_DECOR', 13) => [

					"list" => [
						"pt" => "Best wedding reception, engagement stage, flower decorators, simple decorations in {city}",
						"pd" => "Best {city} stage decorations, flower, drapes decorations with photos, prices for wedding reception, engagement in {city}. Best price & quality service.",
						"ph" => "Stage, flower decoration for engagements, receptions in {city}",
						"kw" => "{city} stage decorations, flower decorations {city}, engagement in {city}, wedding receptions in {city}"
					],

					"filter" => [
						"pt" => "Best {tagTitle} - wedding reception decorations in {city}",
						"pd" => "{city}'s top {tagDesc}. Best prices & quality service.",
						"ph" => "Best engagement, wedding reception {tagName} in {city}",
						"kw" => "engagement/reception decorations in {city}, stage decorations in {city}, {city} flower decorations, engagement/reception {tagName} in {city}"
					],

					"profile" => [
						"pt" => "{item} for wedding reception, engagement in {city}",
						"pd" => "Book {city} best wedding reception, engagement decoration: {item} by best stage, flower decorators in {city} for Rs. {price}",
						"ph" => "{item}",
						"kw" => "{item} in {city}, {category} in {city}, {city} {category}, {item}, {category}"
					],

				],

				/* cakes */
				env('TYPE_PAGE_CAKE', 6)   => [

					"list" => [
						"pt" => "Order online wedding reception, engagement cakes in {city}. Free home delivery",
						"pd" => "Order {city} special cakes for wedding receptions, engagements. Photo cakes, 3D cakes, layered cakes, standard cakes with best deals and free home delivery.",
						"ph" => "Best Cakes For Wedding Receptions in {city}",
						"kw" => "wedding reception cakes {city}, {city} wedding reception cakes, {city} engagement cakes, {city} cakes engagement wedding receptions"
					],

					"profile" => [
						"pt" => "Order {item} - wedding reception, engagement cake in {city}, free home delivery",
						"pd" => "Order online best wedding reception, engagement cake - {item} in {city}. Fresh fun cakes, theme cakes for weddings receptions, engagements. Free home delivery available.",
						"ph" => "{item}",
						"kw" => "wedding reception, engagement cake {item}, {item} cake in {city}, {city}, {item} wedding reception cake"
					]

				],

				/* ent */
				env('TYPE_PAGE_ENT', 14)   => [

					"list" => [
						"pt" => "Best wedding reception/engagement entertainment, candid photography, mehandi artists in {city}",
						"pd" => "Book {city}'s best mehandi artists, candid photography, props, creative artists & more entertainment services for wedding reception, engagement in {city}. Best prices & quality service.",
						"ph" => "Entertainment Services For Engagement | Reception in {city}",
						"kw" => "wedding reception, engagement entertainment in {city}, {city} wedding reception, engagement, wedding reception, engagement party in {city}"
					],

					"filter" => [
						"pt" => "Top {category} wedding reception/engagement entertainment in {city}",
						"pd" => "{city}'s best wedding reception/engagement entertainment - {category}. See real photos, pricing. Best price & 100% service delivery guarantee with quality service.",
						"ph" => "Best Engagement | Reception entertainment {category} in {city}",
						"kw" => "entertainments in {city}, entertainments {category} in {city}, {occasion} entertainment in {city}, {occasion} {category} entertainment in {city}"
					],

					"profile" => [
						"pt" => "Book {item} - wedding reception, engagement entertainment service in {city}",
						"pd" => "Book {city}'s best engagement, wedding reception entertainment service: {item} for Rs. {price}. Best prices & quality service.",
						"ph" => "{item}",
						"kw" => "wedding reception, engagement entertainment in {city}, {city} entertainment for wedding reception, engagement, {item} for wedding reception, engagement, wedding reception, engagement in {city}"
					]

				]

			],

			/* house warming */
			env('TYPE_EVENT_HOUSEWARMING', 14)   => [

				/* collections */
				"collections"               => [

					"list" => [
						"pt" => "Best housewarming, gruhapravesam collections in {city}",
						"pd" => "Top housewarming, gruhapravesam collections with flower decorations, priests, catering & more. Best prices & 100% delivery guarantee.",
						"ph" => "best housewarming, gruhapravesam collections",
						"kw" => "housewarming, gruhapravesam collections, {city} housewarming, gruhapravesam collections, housewarming, gruhapravesam collections in {city}"
					],

					"profile" => [
						"pt" => "Top {item} - housewarming, gruhapravesam collection in {city}",
						"pd" => "Top {item} collections for housewarming, gruhapravesam in {city}. Best deals & 100% delivery guarantee",
						"ph" => "{item}",
						"kw" => "{item} collection in {city}, {city}, {item}"
					]

				],

				/* decor styles */
				env('TYPE_PAGE_DECOR', 13)  => [

					"list" => [
						"pt" => "Best housewarming, gruhapravesam flower decorations in {city}",
						"pd" => "Top simple flower decorations for housewarming, gruhapravesam by best flower decorators in {city}. Best prices, real pictures & quality service.",
						"ph" => "Best housewarming decorations in {city}",
						"kw" => "{city} housewarming decorations, gruhapravesam flower decorations {city}, gruhapravesam in {city}, housewarming in {city}"
					],

					"filter" => [
						"pt" => "Best {tagTitle} - house warming, gruhapravesam decorations in {city}",
						"pd" => "{city}'s top {tagDesc}. Best prices & quality service.",
						"ph" => "Best house warming, gruhapravesam {tagName} in {city}",
						"kw" => "house warming, gruhapravesam decorations in {city}, house warming, gruhapravesam in {city}, {city} flower decorations, house warming, gruhapravesam {tagName} in {city}"
					],

					"profile" => [
						"pt" => "{item} for housewarming, gruhapravesam in {city}",
						"pd" => "Book {city} best housewarming, gruhapravesam decoration: {item} by best stage, flower decorators in {city} for Rs. {price}",
						"ph" => "{item}",
						"kw" => "{item} in {city}, {category} in {city}, {city} {category}, {item}, {category}"
					],

				],

				/* food */
				env('TYPE_PAGE_FOOD', 20)   => [

					"list" => [
						"pt" => "Best housewarming, gruhapravesam catering in {city}",
						"pd" => "Book {city}'s best housewarming catering, home party caterers, small party caterers, pure veg combos for housewarming/gruhapravesam in {city}. Best deals with free home delivery.",
						"ph" => "Best Housewarming Catering Combos In {city}",
						"kw" => "Housewarming catering {city}, {city} home party caterers, pure veg combos catering in {city}, gruhapravesam caterers in {city}"
					],

					"profile" => [
						"pt" => "Book {item} - housewarming, gruhapravesam catering in {city}",
						"pd" => "Book best housewarming, gruhapravesam catering {item} for Rs. {price} in {city}. Best price & quality.",
						"ph" => "{item}",
						"kw" => "housewarming, gruhapravesam catering in {city}, {item} in {city}, housewarming catering"
					]

				],

				/* priests */
				env('TYPE_PAGE_PRIEST', 23) => [

					"list" => [
						"pt" => "Best housewarming, gruhapravesam priest services in {city}",
						"pd" => "Book {city}'s best priest services for your Housewarming, gruhapravesam. Best prices & quality service.",
						"ph" => "Best House Warming Priest Packages In {city}",
						"kw" => "housewarming, gruhapravesam priest in {city}, {city} priests, housewarming priests, gruhapravesam priests"
					],

					"profile" => [
						"pt" => "Book {item} - housewarming priest service in {city}",
						"pd" => "Book {city}'s best housewarming, gruhapravesam priest service: {item} for Rs. {price}. Best deals & quality service.",
						"ph" => "{item}",
						"kw" => "{item} in {city}, {item} for housewarming, gruhapravesam"
					]
				],

				/* tent */
				env('TYPE_PAGE_TENT', 24)   => [

					"list" => [
						"pt" => "Best housewarming, gruhapravesam tent services in {city}",
						"pd" => "Book {city}'s best tent, chair services for your housewarming, gruhapravesam. Best prices & quality service.",
						"ph" => "Best House Warming Tent Packages In {city}",
						"kw" => "housewarming, gruhapravesam tent in {city}, {city} tent, housewarming tent, gruhapravesam tent, chairs"
					],

					"profile" => [
						"pt" => "Book {item} - housewarming tent service in {city}",
						"pd" => "Book {city}'s best housewarming, gruhapravesam tent service: {item} for Rs. {price}. Best deals & quality service.",
						"ph" => "{item}",
						"kw" => "{item} in {city}, {item} for housewarming, gruhapravesam tent"
					]
				],

				/* ent */
				env('TYPE_PAGE_ENT', 14)    => [

					"list" => [
						"pt" => "Best housewarming, gruhapravesam services in {city}",
						"pd" => "{city}'s top photography, video coverage, services for housewarming, gruhapravesam in {city}. Best deals & quality service.",
						"ph" => "Best Housewarming Entertainment Services In {city}",
						"kw" => "housewarming, gruhapravesam add-ons in {city}, {city} add-ons, housewarming add-ons, gruhapravesam add-ons"
					],

					"filter" => [
						"pt" => "Top {category} - housewarming, gruhapravesam services in {city}",
						"pd" => "{city}'s best {category} - entertainment services for housewarming, gruhapravesam. See real photos, pricing. Best price & 100% service delivery guarantee with quality service.",
						"ph" => "Best Housewarming Entertainment {category} in {city}",
						"kw" => "entertainments in {city}, entertainments {category} in {city}, housewarming, gruhapravesam entertainment in {city}, housewarming {category} entertainment in {city}"
					],

					"profile" => [
						"pt" => "Book {item} - housewarming service in {city}",
						"pd" => "Book {city}'s top housewarming, gruhapravesam service: {item} for Rs. {price}. Best deals & quality service.",
						"ph" => "{item}",
						"kw" => "{item} in {city}, {item} for housewarming, gruhapravesam service, housewarming service"
					]
				],

				/* cakes */
				env('TYPE_PAGE_CAKE', 6)    => [

					"list" => [
						"pt" => "Best housewarming cakes, standard cakes in {city}, free home delivery",
						"pd" => "Order {city} best housewarming cakes, standard cakes. Free home delivery.",
						"ph" => "Best housewarming cakes in {city}",
						"kw" => "housewarming cakes in {city}, {city} theme cakes, {city} housewarming cakes, {city} order cakes online"
					],

					"profile" => [
						"pt" => "Order {item} online - housewarming cake in {city}, free home delivery",
						"pd" => "Order online best housewarming cake - {item} in {city}. Fresh housewarming cakes, theme cakes. Free home delivery available.",
						"ph" => "{item}",
						"kw" => "housewarming cake {item}, {item} cake in {city}, {city}, {item} housewarming cake"
					]
				],

			],

			/* Youth party */
			env('TYPE_EVENT_BACHELOR', 11)       => [

				/* collections */
				"collections"               => [

					"list" => [
						"pt" => "Best youth, bachelor party collections in {city}",
						"pd" => "Top youth, bachelor party collections with resorts, villas, farm houses, barbeque & more. Best prices & 100% delivery guarantee.",
						"ph" => "best youth party collections",
						"kw" => "bachelor party collections, {city} bachelor party collections, bachelor party collections in {city}"
					],

					"profile" => [
						"pt" => "Top {item} - youth, bachelor party collection in {city}",
						"pd" => "Top {item} collections for bachelor party in {city}. Best deals & 100% delivery guarantee",
						"ph" => "{item}",
						"kw" => "{item} collection in {city}, {city}, {item}"
					]

				],

				/* cakes */
				env('TYPE_PAGE_CAKE', 6)    => [

					"list" => [
						"pt" => "Order online youth party cakes in {city}. Free home delivery",
						"pd" => "Order {city}'s best fun cakes for bachelor party, bachelorette party. Photo cakes, 3D cakes, theme cakes, standard cakes with best prices & free home delivery in {city}.",
						"ph" => "Best Youth Party Cakes In {city}",
						"kw" => "bachelor party cakes in {city}, {city} bachelor party cakes, fun cakes in {city}"
					],

					"profile" => [
						"pt" => "Order {item} - bachelor cake in {city}, free home delivery",
						"pd" => "Order online best bachelor cake - {item} in {city}. Fresh fun cakes, theme cakes for bachelor party, bachelorette party. Free home delivery available.",
						"ph" => "{item}",
						"kw" => "bachelor cake {item}, {item} cake in {city}, {city}, {item} bachelorette cake"
					]

				],

				/* food */
				env('TYPE_PAGE_FOOD', 20)   => [

					"list" => [
						"pt" => "Best home party caterers, bbq, veg/non-veg combos for youth party in {city}",
						"pd" => "Book {city}'s home party caterers, small party caterers, bbq, barbecue live counter, veg/non-veg combos for bachelor party in {city}. Best price & quality.",
						"ph" => "Best Youth Party Catering Combos In {city}",
						"kw" => "bachelor party catering {city}, {city} home party caterers, live bbq in {city}, bachelor party food {city}"
					],

					"profile" => [
						"pt" => "Book {item} - home party catering in {city}",
						"pd" => "Book best bachelor party catering {item} for Rs. {price} in {city}. Best price & quality.",
						"ph" => "{item}",
						"kw" => "bachelor catering in {city}, {item} in {city}"
					]

				],

				/* ent */
				env('TYPE_PAGE_ENT', 14)    => [

					"list" => [
						"pt" => "Best youth party games, props, entertainment in {city}",
						"pd" => "Book {city}'s best bachelor party, bachelorette party games, entertainment, activities, props, hookah setup, caricature artists & more. Best deals & quality service.",
						"ph" => "Bachelor Party Entertainment Services in {city}",
						"kw" => "bachelor party entertainment in {city}, {city} entertainment for bachelor, bachelorette party"
					],

					"filter" => [
						"pt" => "Top {category} entertainment services for youth party in {city}",
						"pd" => "{city}'s best bachelor party - {category} - entertainment services. See real photos, pricing. Best price & 100% service delivery guarantee with quality service.",
						"ph" => "Best Youth Party Entertainment {category} in {city}",
						"kw" => "entertainments in {city}, entertainments {category} in {city}, bachelor party entertainment in {city}, bachelor party {category} entertainment in {city}"
					],

					"profile" => [
						"pt" => "Book {item} youth party games, entertainment, activity in {city}",
						"pd" => "Book {city}'s best bachelor party games, entertainment, props, activity: {item} from Rs. {price}. Best deals & quality service.",
						"ph" => "{item}",
						"kw" => "bachelor party entertainment in {city}, {city} entertainment for bachelor party, {item} for bachelor entertainment"
					]

				],

				/* resort */
				env('TYPE_PAGE_RESORT', 17) => [

					"list" => [
						"pt" => "Best resorts for youth party, destinations, places in {city}",
						"pd" => "Book {city}'s top resorts, places, destinations for bachelor party, bachelorette party. Best private space at resorts with swimming pool, dinner, cottage rooms & more at best price.",
						"ph" => "Best Bachelor Party Resorts in {city}",
						"kw" => "bachelor party resorts {city}, {city} resorts, resorts, places in {city}, resorts for bachelor party {city}"
					],

					"profile" => [
						"pt" => "Book {item} - best youth party resort, destination in {city}",
						"pd" => "Book {city}'s top resort for bachelor party: {item}. Price starting from Rs. {price}. Best price & quality service.",
						"ph" => "{item}",
						"kw" => "bachelor resort in {city}, {item} in {city}"
					]

				],

				/* villa */
				env('TYPE_PAGE_VILLA', 18)  => [

					"list" => [
						"pt" => "Best villas, farm houses, destinations for youth party in {city}",
						"pd" => "Book {city}'s top private villas, exclusive farm houses for bachelor party, bahcelorette party. List of budgeted villas, farm houses around {city} with late night parties.",
						"ph" => "Best Bachelor Party Villas + Farm Houses In {city}",
						"kw" => "bachelor party villas {city}, {city} villas, farm houses, places in {city}, villas for bachelor party {city}"
					],

					"profile" => [
						"pt" => "Book {item} - best bachelor party villa, farm house in {city}",
						"pd" => "Book {city}'s top villa, farm houses for bachelor party: {item}. Price starting from Rs. {price}. Best price & quality service.",
						"ph" => "{item}",
						"kw" => "bachelor villa, farm house in {city}, {item} in {city}"
					]
				],

				/* lounge */
				env('TYPE_PAGE_LOUNGE', 19) => [

					"list" => [
						"pt" => "Best restaurants, pubs, lounges for youth party in {city}",
						"pd" => "Book {city}'s best youth party restaurants, pubs, lounges. Deals at roof-top places, top destinations with dinner, barbeque's, swimming pool & many more. Value of money & best deals.",
						"ph" => "Best Bachelor Party Lounges In {city}",
						"kw" => "bachelor party lounges {city}, {city} lounges, lounges, places in {city}, lounges for bachelor party {city}"
					],

					"profile" => [
						"pt" => "Book {item} - best youth party restaurant, lounge in {city}",
						"pd" => "Book {city}'s lounge, restaurant, pub for bachelor party: {item}. Price starting from Rs. {price}. Best price & quality service.",
						"ph" => "{item}",
						"kw" => "bachelor villa, farm house in {city}, {item} in {city}"
					]
				],

				/* decor styles */
				env('TYPE_PAGE_DECOR', 13)  => [

					"list" => [
						"pt" => "Best youth party decorations in {city}",
						"pd" => "Top youth party decorations, helium balloons, theme decorations for bachelor party in {city}. Best prices, real pictures with home delivery.",
						"ph" => "Best youth party decorations in {city}",
						"kw" => "bachelor party decorations in {city}, {city} bachelor party decorations, bachelor theme decorations in {city}"
					],

					"filter" => [
						"pt" => "Best {tagTitle}, youth party decorations in {city}",
						"pd" => "{city}'s top {tagDesc}. Free home delivery.",
						"ph" => "Best youth {tagName} in {city}",
						"kw" => "bachelor party decorations in {city}, {city} bachelor party decorations, bachelor party {tagName} in {city}"
					],

					"profile" => [
						"pt" => "{item} - youth party {category} in {city}",
						"pd" => "Book best {item} youth party {category} from top bachelor party planners in {city}. Real images, customer reviews, best prices.",
						"ph" => "{item}",
						"kw" => "{item} in {city}, {category} in {city}, {city} {category}, {item}, {category}"
					]

				],

			],

			/* surprises */
			env('TYPE_EVENT_SURPRISES', 16)      => [

				/* home page */
				'home'                                => [
					'pt' => 'Top surprises, gifts for birthday, anniversary in {city} for husband, wife, partner',
					'pd' => '{city}\'s best surprises for husband, wife - birthday, anniversary. Home decorations, romantic, adventure experiences & more',
					'ph' => 'India\'s Most Trusted Surprise Planners',
					'kw' => 'Surprises in {city}, {city} birthday gift for husband, {city} anniversary gift for husband'
				],

				/* collections */
				"collections"                         => [

					"list" => [
						"pt" => "Best surprises, experiences collections in {city}",
						"pd" => "Top surprises collections for birthday, anniversary from best surprise planners in {city}. Best prices & 100% delivery guarantee.",
						"ph" => "best couple experiences collections",
						"kw" => "couple experiences collections, surprise collection in {city}, {city} bachelor party collections, bachelor party collections in {city}"
					],

					"profile" => [
						"pt" => "Best {item} in {city}",
						"pd" => "Top {item} collections for surprises, couple experiences in {city}. Best deals & 100% delivery guarantee",
						"ph" => "{item}",
						"kw" => "{item} collection in {city}, {city}, {item}"
					]

				],

				/* packages */
				env('TYPE_PAGE_SURPRISE_PACKAGE', 21) => [

					'list' => [
						'pt' => 'Top surprises for birthday, anniversary in {city} - balloon surprises, adventure & more',
						'pd' => '{city}\'s best balloon surprises, home surprises, advenutre & more for husband, wife, parents. Celebrate birthday, anniversary & more. Trusted surprise planners.',
						'ph' => 'best surprises in {city}',
						'kw' => '{city} surprises, {city} surprises for wife, surprises for husband in {city}, {city} top surprises'
					],

					'filter' => [
						'pt' => 'Top surprises, gifts {relation} {occasion} in {city}',
						'pd' => '{city}\'s best surprises {relation} {occasion}. See real photos & customer reviews. Best price guarantee.',
						'ph' => 'best surprises {relation} {occasion} in {city}',
						'kw' => '{city} surprises, {city} surprises for {relation}, {relation} {occasion} {city} surprises'
					],

					'category' => [
						'pt' => 'Top {category_title} surprises {relation} {occasion} in {city}',
						'pd' => '{city}\'s best {category_desc} surprises {relation} {occasion}. Best price guaranteed.',
						'ph' => 'best {category_name} {relation} {occasion} in {city}',
						'kw' => '{city} {category_name} surprises, {city} surprises {relation}, surprises {relation} {occasion} {city}, {city} top surprises'
					],

					'profile' => [
						'pt' => '{item} - {city}',
						'pd' => 'Book best {item} surprise party packages from top surprise planners in {city}. Unique, romantic options, home decors, candlelight dinner & more. Best price, 100% service delivery guarantee',
						'ph' => '{item}',
						'kw' => 'surprises, surprises in {city}, {item}, {city} surprise planners, {item} in {city}'
					]

				],

				/* Candle Light Dinner */
				'candle-light-dinner'                 => [
					'list' => [
						'pt' => 'Top {category_title} In {area} {city} - Private, Romantic, Poolside',
						'pd' => 'Book {area} {city}’s best {category_desc} at top star hotels & restaurants. Private, poolside, rooftop, romantic & more options. Best deals guaranteed.',
						'ph' => 'Best {category_name} In {area} {city}',
						'kw' => '{city} surprises,{city} {category_name}, {city} surprises for wife, surprises for husband in {city}, {city} top surprises'
					],

					'profile' => [
						'pt' => 'Book {item} in {area} {city}',
						'pd' => 'Book best {item} in {area} {city} for Rs. {price} only at Evibe.in - trusted by 15K+ customers. Top candlelight dinner option. Best price & service guarantee',
						'ph' => '{item}',
						'kw' => 'surprises, surprises in {city}, {item}, {city} surprise planners, {item} in {city}'
					]
				]
			],
		],
	],

	/* hyderabad */
	2           => [
		"home"                        => [
			"pt" => "Best birthday party planners, surprise planners, wedding reception organisers in Hyderabad",
			"pd" => "Hyderabad's top birthday party organisers, youth party planners, surprise planners, experiences & more from best party organisers in Hyderabad. Best prices, real pictures & customer reviews.",
			"ph" => "India\'s Most Trusted Party Planners"
		],

		/* birthday parties */
		env('TYPE_EVENT_BIRTHDAY', 1) => [

			"landing" => [
				"pt" => "Best birthday party planners, birthday party organisers in Hyderabad",
				"pd" => "Hyderabad's top birthday planners, balloon decorations, theme decorations, cakes, function halls for birthday party in Hyderabad. Best prices & 100% delivery guarantee.",
				"ph" => "India\'s Most Trusted Birthday Party Planners",
				"kw" => "birthday party planners in Hyderabad, Hyderabad birthday planners, birthday party organisers in Hyderabad, Hyderabad birthday organisers"
			]

		]

	]

];