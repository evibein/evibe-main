<?php

/*
 * Wallet related configurations
 * */

return [
	'default' => [
		'amount'  => env('WALLET_DEFAULT_AMOUNT', '300'),
		'user_id' => env('WALLET_ADMIN_ID', '213')
	]
];