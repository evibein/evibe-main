<?php
/**
 * PayUMoney options
 *
 * @author Anji <anji@evibe.in>
 * @since  28 Apr 2015
 */

return [

	'end_point' => env('PAYUMONEY_ENDPOINT', 'https://secure.payu.in/_payment'),

	'salt' => env('PAYUMONEY_SALT', 'kdiruBMd'),

	//  'key' => env('PAYUMONEY_KEY', '2LEHxq'),
	 'key' => env('PAYUMONEY_KEY', '2LW'), // @see: broken key
	// 'key' => env('PAYUMONEY_KEY', 'test'),

	'merchant_id' => env('PAYUMONEY_MERCHANTID', '5121550'),

	/**
	 * @author: Vikash
	 * @since 28 Dec 2015
	 *
	 * Commenting surl, furl and using direct routes in PayUController
	 * Issue with using 'route' method in Config file since Laravel 5.2
	 */
	// 'surl' => returnRoute('payu.success'),

	//'furl' => route('payu.fail')

];