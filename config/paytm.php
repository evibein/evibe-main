<?php

/**
 * Paytm options
 *
 * @since  08 Feb 2018
 */

return [
	'end_point' => env('PAYTM_ENDPOINT', 'https://securegw.paytm.in/theia/processTransaction'),

	'mid' => env('PAYTM_MID', 'EvibeT04495456477313'),

	// 'key' => env('PAYTM_KEY', 'GrD&pwi&wq4xhkID'),
	'key' => env('PAYTM_KEY', 'GrD'), // @see: broken key

	'type' => env('PAYTM_TYPE', 'WEBPROD')
];