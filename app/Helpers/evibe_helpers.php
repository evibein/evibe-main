<?php

/**
 * @important
 *
 * If there any changed made in this file, please use `composer dump`
 * otherwise your changes will not be reflected
 */

use \App\Models\Util\City;
use \App\Models\Types\TypeEvent;
use \Carbon\Carbon;
use \App\Models\ViewCount\ProfileViewCounter;
use \App\Jobs\HandlePageViewCount;
use Illuminate\Support\Facades\Cookie;

// get city id from session
if (!function_exists('getCityId'))
{
	/**
	 * Returns city id from session if exists else returns null.
	 *
	 * @param null
	 *
	 * @return integer
	 */
	function getCityId()
	{
		return (int)session('cityId', -1);
	}
}

// get city name from session
if (!function_exists('getCityName'))
{
	/**
	 * Returns city name from session if exists else returns null.
	 *
	 * @param null
	 *
	 * @return string
	 */
	function getCityName()
	{
		return session('cityName', null);
	}
}

// get city url from session
if (!function_exists('getCityUrl'))
{
	/**
	 * Returns city url from session if exists else returns null.
	 *
	 * @param null
	 *
	 * @return string
	 */
	function getCityUrl()
	{
		return session('cityUrl', null);
	}
}

// set and check city sessions
if (!function_exists('checkAndSetCitySessions'))
{
	/**
	 * Sets cityId, cityName, cityUrl into sessions and shares with all the views.
	 *
	 * @param city object
	 *
	 * @return boolean
	 */
	function checkAndSetCitySessions($city = null)
	{
		// cannot optimised further by checking $city->id == getCityId()
		// as city values will not be shared with "views" by default
		if (!is_null($city))
		{
			$sessionVars = [
				['key' => 'cityId', 'value' => $city->id],
				['key' => 'cityUrl', 'value' => $city->url],
				['key' => 'cityName', 'value' => $city->name]
			];

			foreach ($sessionVars as $sessionVar)
			{
				$key = $sessionVar['key'];
				$value = $sessionVar['value'];
				$currentValue = session($key, null);

				if (is_null($currentValue) || $currentValue != $value)
				{
					session([$key => $value]);
				}
			}

			view()->share('cityId', $city->id);
			view()->share('cityUrl', $city->url);
			view()->share('cityName', $city->name);

			return true;
		}

		return false;
	}
}

if (!function_exists('getAllCities'))
{
	/**
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	function getAllCities()
	{
		return City::select("id", "name", "is_default", "is_active", "code", "url")
		           ->get();
	}
}

if (!function_exists('getAllOccasions'))
{
	/**
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	function getAllOccasions()
	{
		return TypeEvent::select("id", "name", "meta_name", "url", "show_customer")
		                ->get();
	}
}

if (!function_exists('getAuthUserName'))
{
	/**
	 * @return string
	 */
	function getAuthUserName()
	{
		if (auth()->check())
		{
			$user = auth()->user();

			return $user->name;
		}

		return '';
	}
}

if (!function_exists('getAuthUserID'))
{
	function getAuthUserID()
	{
		if (auth()->check())
		{
			$user = auth()->user();

			return $user->id;
		}

		return '';
	}
}

if (!function_exists('hasPartnerUser'))
{
	/**
	 * @return boolean
	 */

	function hasPartnerUser()
	{
		$partnerRoles = [
			config('evibe.roles.planner'),
			config('evibe.roles.venue')
		];

		$roleId = auth()->user() ? auth()->user()->role_id : null;
		if ($roleId && in_array($roleId, $partnerRoles))
		{
			return true;
		}

		return false;
	}
}

if (!function_exists('getAreaOfCity'))
{
	/**
	 * @return array
	 */
	function getAreaOfCity()
	{
		$cityId = getCityId();

		if ($cityId)
		{
			return \App\Models\Util\Area::where('city_id', $cityId)->get();
		}
	}
}

if (!function_exists('getTextFromSlug'))
{
	/**
	 * @param $url
	 *
	 * @return string
	 */
	function getTextFromSlug($url)
	{
		return ucwords(str_replace("-", " ", $url));
	}
}

if (!function_exists('getSlugFromText'))
{
	/**
	 * @param $url
	 *
	 * @return string
	 */
	function getSlugFromText($text)
	{
		$url = preg_replace("/[^\w ]+/", "", trim($text));
		$url = preg_replace('/\s\s+/', " ", $url);
		$url = str_replace(' ', '-', $url);

		return $url;
	}
}

if (!function_exists('updateProfileViewCounter'))
{
	function updateProfileViewCounter($productTypeId, $productId)
	{
		// if the users reloads, we do not consider for page view counter
		// if the user goes to a new product page and returns, then it is counted
		// @todo: re-enable it after optimization

		//if ($lastProductPageViewed = session("evb_usr_pro_url", "default_default"))
		//{
		//	$lastViewParts = explode("_", $lastProductPageViewed);
		//
		//	if (count($lastViewParts) == 2 &&
		//		!($lastViewParts[0] == $productTypeId && $lastViewParts[1] == $productId))
		//	{
		//		$startTime = Carbon::now()->startOfDay()->timestamp;
		//		$endTime = Carbon::now()->endOfDay()->timestamp;
		//		$userId = getAuthUserID();
		//
		//		$newCookieId = rand(10000, 999999999);
		//		if (!Cookie::has('evbbrowsingid'))
		//		{
		//			Cookie::queue(Cookie::make('evbbrowsingid', $newCookieId, 129600));
		//		}
		//
		//		session()->put('is_profile', '1');
		//
		//		dispatch(new HandlePageViewCount([
		//			                                 "startTime"     => $startTime,
		//			                                 "endTime"       => $endTime,
		//			                                 "productTypeId" => $productTypeId,
		//			                                 "productId"     => $productId,
		//			                                 "userId"        => $userId,
		//			                                 "cookieId"      => Cookie::has('evbbrowsingid') ? Cookie::get('evbbrowsingid') : $newCookieId
		//		                                 ]));
		//	}
		//}

		session()->put('evb_usr_pro_url', $productTypeId . '_' . $productId);
	}
}

if (!function_exists('getPartyDateTimeByBookingsPartyTime'))
{
	function getPartyDateTimeByBookingsPartyTime($minDateTime, $maxDateTime)
	{
		// if ticket has multiple ticket bookings
		// - on same date, use just the date
		// - on different dates, use range min - max
		// - only one booking, use date time format
		$partyDateTime = date("d M Y", $minDateTime);
		if ($minDateTime == $maxDateTime)
		{
			$partyDateTime = date("d M Y h:i A", $minDateTime);
		}
		elseif (date("d m y", $minDateTime) != date("d m y", $maxDateTime))
		{
			$partyDateTime = date("d/m/y", $minDateTime) . ' - ' . date("d/m/y", $maxDateTime);
		}

		return $partyDateTime;
	}
}

if (!function_exists('getEventIdFromSession'))
{
	function getEventIdFromSession()
	{
		// if eventId is there in sessions, return it else return null
		$eventId = (session()->has('eventId') && session('eventId') >= 1) ? session('eventId') : null;

		return $eventId;
	}
}