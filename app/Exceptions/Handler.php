<?php

namespace App\Exceptions;

use App\Jobs\Emails\Errors\MailAppErrorToAdminJob;
use App\Models\Util\SiteErrorLog;
use Exception;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		\Illuminate\Auth\AuthenticationException::class,
		\Illuminate\Auth\Access\AuthorizationException::class,
		\Symfony\Component\HttpKernel\Exception\HttpException::class,
		\Illuminate\Database\Eloquent\ModelNotFoundException::class,
		\Illuminate\Session\TokenMismatchException::class,
		\Illuminate\Validation\ValidationException::class,
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param \Exception $exception
	 *
	 * @return void
	 */
	public function report(Exception $e)
	{
		parent::report($e);

		if (!config('app.debug'))
		{
			$code = $e->getCode() ?: -1;
			$errorUrl = request()->fullUrl();
			$errorMethod = request()->method() ?: '--';
			$errorMessage = $e->getMessage() ?: '';

			// ignore build files (css) issues
			if ($errorUrl &&
				(strpos($errorUrl, "evibe.in/build/css") || strpos($errorUrl, "evibe.in/build/js")))
			{
				return;
			}

			if ($e instanceof ModelNotFoundException)
			{
				$exceptionType = 'ModelNotFoundException';
			}
			elseif ($e instanceof NotFoundHttpException)
			{
				$exceptionType = 'NotFoundHttpException';
			}
			elseif ($e instanceof HttpException)
			{
				$exceptionType = 'HttpException';
				$code = $e->getStatusCode();
			}
			elseif ($e instanceof ModelNotFoundException)
			{
				$exceptionType = 'ModelNotFoundException';
			}
			elseif ($e instanceof AuthorizationException)
			{
				$exceptionType = 'AuthorizationException';
			}
			else
			{
				$exceptionType = 'Error Exception';
			}

			$errorData = [
				'url'           => $errorUrl,
				'previous_url'  => request()->session()->previousUrl(),
				'ip_address'    => request()->ip(),
				'method'        => $errorMethod,
				'error_message' => $errorMessage,
				'exception'     => $exceptionType,
				'project_id'    => config('evibe.project_id'),
				'code'          => $code,
				'details'       => $e->getTraceAsString(),
				'created_at'    => date('Y-m-d H:i:s'),
				'updated_at'    => date('Y-m-d H:i:s')
			];

			SiteErrorLog::create($errorData);

			if ($this->isReportAdmin($code, $exceptionType, $errorMessage))
			{
				$emailData = [
					'code'     => $code,
					'url'      => $errorUrl,
					'method'   => $errorMethod,
					'messages' => $errorMessage,
					'details'  => $e->getTraceAsString()
				];

				dispatch(new MailAppErrorToAdminJob($emailData));
			}
		}
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Exception               $exception
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $exception)
	{
		return parent::render($request, $exception);
	}

	/**
	 * Convert an authentication exception into an unauthenticated response.
	 *
	 * @param \Illuminate\Http\Request                 $request
	 * @param \Illuminate\Auth\AuthenticationException $exception
	 *
	 * @return \Illuminate\Http\Response
	 */
	protected function unauthenticated($request, AuthenticationException $exception)
	{
		if ($request->expectsJson())
		{
			return response()->json(['error' => 'Unauthenticated.'], 401);
		}

		return redirect()->guest('login');
	}

	// email errors only in some cases
	private function isReportAdmin($code, $exceptionType, $errorMessage)
	{
		if (!config('evibe.mail_errors') ||
			in_array($code, [1, 404, 401, 405]) ||
			$exceptionType === "NotFoundHttpException")
		{
			return false;
		}

		$ignoreMessages = [
			'nodes found',
			//'Redis is configured',
			//'loading the dataset in memory',
			//'tcp://localhost:6379'
		];

		$reportError = true;
		foreach ($ignoreMessages as $ignoreMessage)
		{
			$pos = strpos($errorMessage, $ignoreMessage);
			if ($pos || $pos === 0)
			{
				$reportError = false;
				break;
			}
		}

		return $reportError;
	}
}
