<?php

namespace App\Listeners;

use App\Http\Controllers\Auth\UserAuthController;
use App\Events\SocialUserSigninSuccessEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SocialUserSigninSuccessEventListener
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  SocialUserSigninSuccessEvent $event
	 *
	 * @return void
	 */
	public function handle(SocialUserSigninSuccessEvent $event)
	{
		$provider = $event->provider;
		$providerUser = $event->providerUser;
		$socialAccount = $event->socialAccount;

		(new UserAuthController)->handleSocialUserSignInSuccess($provider, $providerUser, $socialAccount);
	}
}
