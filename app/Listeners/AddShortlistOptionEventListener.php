<?php

namespace App\Listeners;

use App\Events\AddShortlistOptionEvent;
use App\Http\Controllers\Loggers\ShortlistActionLogController;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddShortlistOptionEventListener
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  AddShortlistOptionEvent $event
	 *
	 * @return void
	 */
	public function handle(AddShortlistOptionEvent $event)
	{
		$shortlistOptionId = $event->shortlistOptionId;

		(new ShortlistActionLogController)->logShortlistAddAction($shortlistOptionId);
	}
}
