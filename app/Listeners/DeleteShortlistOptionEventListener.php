<?php

namespace App\Listeners;

use App\Events\DeleteShortlistOptionEvent;
use App\Events\UpdateShortlistOptionEvent;
use App\Http\Controllers\Loggers\ShortlistActionLogController;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeleteShortlistOptionEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateShortlistOptionEvent  $event
     * @return void
     */
    public function handle(DeleteShortlistOptionEvent $event)
    {
	    $shortlistOptionId = $event->shortlistOptionId;
	    (new ShortlistActionLogController)->logShortlistDeleteAction($shortlistOptionId);
    }
}
