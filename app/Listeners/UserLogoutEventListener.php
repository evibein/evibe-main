<?php

namespace App\Listeners;

use App\Events\UserLogoutEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Http\Controllers\Loggers\UserActionLogController;

class UserLogoutEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserLogoutEvent  $event
     * @return void
     */
    public function handle(UserLogoutEvent $event)
    {
        $userId = $event->userId;
	    (new UserActionLogController)->logUserLogoutAction($userId);
    }
}
