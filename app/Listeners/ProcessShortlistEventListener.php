<?php

namespace App\Listeners;

use App\Events\ProcessShortlistEvent;
use App\Http\Controllers\PartyBag\PartyBagController;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProcessShortlistEventListener
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  ProcessShortlistEvent $event
	 *
	 * @return void
	 */
	public function handle(ProcessShortlistEvent $event)
	{
		(new PartyBagController)->shortlistOptionToPartyBag($event->shortlistData);
	}
}
