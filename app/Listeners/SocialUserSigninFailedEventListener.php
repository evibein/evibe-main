<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Log;
use App\Events\SocialUserSigninFailedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SocialUserSigninFailedEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SocialUserSigninFailedEvent  $event
     * @return void
     */
    public function handle(SocialUserSigninFailedEvent $event)
    {
        Log::error('User denied permissions in provider for evibe.in for the social login ' . $event->providerName);
    }
}
