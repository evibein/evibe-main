<?php

namespace App\Listeners;

use App\Events\UserLoginEvent;
use App\Events\ProcessShortlistEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Http\Controllers\Loggers\UserActionLogController;
use Illuminate\Support\Facades\Log;

class UserLoginEventListener
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  UserLoginEvent $event
	 *
	 * @return void
	 */
	public function handle(UserLoginEvent $event)
	{
		$userId = $event->userId;

		(new UserActionLogController)->logUserLoginAction($userId);

		//if (session('in.evibe.temp_shortlist_data'))
		//{
		//	$tempShortlistData = session('in.evibe.temp_shortlist_data');
		//
		//	event(new ProcessShortlistEvent($tempShortlistData));
		//
		//	session('in.evibe.temp_shortlist_data');
		//}
	}
}
