<?php

/**
 * @author Anji <anji@evibe.in>
 * @since  21 May 2016
 * @see    Extending Laravel package Chencha (social-share) to add Hashtag and
 *         to fix issue with showing images in Pinterest share.
 */
namespace App\Services;

use Chencha\Share\Share;

class MyShare extends Share
{
	protected $hashtag;

	// @override
	public function load($url, $title = '', $hashtag = '', $media = '')
	{
		parent::load($url, $title, $media)->hashtag = $hashtag;

		return $this;
	}
}