<?php

namespace App\Providers;

use App\Evibe\Validator\EvibeValidator;
use App\Http\Controllers\Util\CouponController;
use App\Models\Vendor\Vendor;
use App\Models\Venue\Venue;

use Evibe\Utilities\EvibeUtilDataStore;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		Validator::resolver(function ($translator, $data, $rules, $messages) {
			return new EvibeValidator($translator, $data, $rules, $messages);
		});

		// disable query log
		if (env('APP_ENV', 'local') !== 'local')
		{
			DB::connection()->disableQueryLog();
		}

		//DB::listen(function($query) {
		//	File::append(
		//		storage_path('/logs/query.log'),
		//		microtime() . " - " . $query->sql . ' [' . implode(', ', $query->bindings) . ']' . PHP_EOL
		//	);
		//  });

		$dataStore = app()->make("EvibeUtilDataStore");
		$agent = app()->make("agent");

		view()->share('cities', $dataStore->getAllCities());
		view()->share('galleryUrl', config('evibe.gallery.host'));
		view()->share('canonicalUrl', "https://evibe.in");
		view()->share('agent', $agent);

		// Blade directives
		Blade::directive('price', function ($expression) {
			return "<span class='rupee-font'>&#8377;</span> <?php echo EvibeUtil::formatPrice(e($expression)); ?>";
		});

		Blade::directive('priceWc', function ($expression) {
			return "<span class='rupee-font'>&#8377;</span> <?php echo EvibeUtil::formatPriceWithoutChar(e($expression)); ?>";
		});

		Blade::directive('amount', function ($expression) {
			return "<?php echo EvibeUtil::formatPrice(e($expression)); ?>";
		});

		Blade::directive('offPrice', function ($expression) {
			list($price, $priceWorth) = explode(', ', $expression);

			if ($price && $priceWorth && ($price < $priceWorth))
			{
				return "<span class='rupee-font'>&#8377;</span> <?php echo EvibeUtil::formatPrice(e($priceWorth - $price)); ?>";
			}
			else
			{
				return null;
			}
		});

		Blade::directive('offPriceAmount', function ($expression) {
			list($price, $priceWorth) = explode(', ', $expression);

			if ($price && $priceWorth && ($price < $priceWorth))
			{
				return "<?php echo EvibeUtil::formatPrice(e($priceWorth - $price)); ?>";
			}
			else
			{
				return null;
			}
		});

		Blade::directive('advAmount', function ($expression) {
			return "<span class='rupee-font'>&#8377;</span> <?php echo EvibeUtil::getFormattedRoundedTokenAmount(e($expression)); ?>";
		});

		Blade::directive('number', function ($expression) {
			return "<?php echo EvibeUtil::formatNumber(e($expression)); ?>";
		});

		Blade::directive('capitalize', function ($text) {
			return "<?php echo ucwords(strtolower($text)); ?>";
		});

		Blade::directive('truncateName', function ($name) {
			$name = strlen($name) <= 45 ? $name : substr($name, 0, 36) . "...";

			return "<?php echo $name; ?>";
		});

		Blade::directive('formatReviewDate', function ($date) {
			$date = date("D, M j \'y", strtotime($date));

			return "<?php echo $date; ?>";
		});

		Blade::directive('vendorType', function ($vendorType) {
			$typeName = "Event Organiser";
			if ($vendorType == config('evibe.ticket.type.venue') || $vendorType == config('evibe.ticket.type.halls'))
			{
				$typeName = "Venue Manager";
			}
			if ($vendorType == config('evibe.ticket.type.resorts'))
			{
				$typeName = "Resort Manager";
			}
			if ($vendorType == config('evibe.ticket.type.villas'))
			{
				$typeName = "Villa Manager";
			}
			if ($vendorType == config('evibe.ticket.type.artist'))
			{
				$typeName = "Artist";
			}

			return "$typeName";
		});

		// get event url
		Blade::directive('occasionUrl', function ($eventId) {
			return "<?php echo EvibeUtil::getEventUrl(e($eventId)); ?>";
		});

		// Model morphs
		$morphs = [
			config('evibe.ticket.type.planner') => Vendor::class,
			config('evibe.ticket.type.artist')  => Vendor::class,
			config('evibe.ticket.type.venue')   => Venue::class,
			config('evibe.ticket.type.trend')   => Vendor::class,
			config('evibe.ticket.type.decor')   => Vendor::class,
			config('evibe.ticket.type.cake')    => Vendor::class,
			config('evibe.ticket.type.package') => Vendor::class,
		];

		Relation::morphMap($morphs, false);
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		// register debugbar only for local environment
		if ($this->app->environment() != "production")
		{
			$this->app->register("Barryvdh\Debugbar\ServiceProvider");
		}

		// register custom classes
		$this->app->singleton("EvibeUtilDataStore", function () {
			return new EvibeUtilDataStore();
		});

		$this->app->singleton("CouponController", function () {
			return new CouponController();
		});
	}
}