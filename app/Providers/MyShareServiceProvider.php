<?php

namespace App\Providers;

use App\Services\MyShare;
use Chencha\Share\ShareServiceProvider;

class MyShareServiceProvider extends ShareServiceProvider
{
	public function register()
	{
		parent::register();

		$this->app->singleton('share', function ($app)
		{
			return new MyShare($app);
		});
	}
}