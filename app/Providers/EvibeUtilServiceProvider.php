<?php

namespace App\Providers;

use Evibe\Utilities\EvibeUtil;

use Illuminate\Support\ServiceProvider;

class EvibeUtilServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{

	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton('evibe_util', function ($app) {
			return new EvibeUtil();
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return ['evibe_util'];
	}
}
