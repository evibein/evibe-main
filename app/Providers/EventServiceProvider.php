<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
	/**
	 * The event listener mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		'App\Events\SocialUserSigninFailedEvent'  => [
			'App\Listeners\SocialUserSigninFailedEventListener',
		],
		'App\Events\SocialUserSigninSuccessEvent' => [
			'App\Listeners\SocialUserSigninSuccessEventListener',
		],
		'App\Events\UserLogoutEvent'              => [
			'App\Listeners\UserLogoutEventListener',
		],
		'App\Events\UserLoginEvent'               => [
			'App\Listeners\UserLoginEventListener'
		],
		'App\Events\AddShortlistOptionEvent'      => [
			'App\Listeners\AddShortlistOptionEventListener'
		],
		'App\Events\DeleteShortlistOptionEvent'   => [
			'App\Listeners\DeleteShortlistOptionEventListener'
		],
		'App\Events\ProcessShortlistEvent'        => [
			'App\Listeners\ProcessShortlistEventListener'
		]
	];

	/**
	 * The subscriber classes to register.
	 *
	 * @var array
	 */
	protected $subscribe = [
	];

	/**
	 * Register any events for your application.
	 *
	 * @return void
	 */
	public function boot()
	{
		parent::boot();

		//
	}
}
