<?php

namespace App\Models\Campaigns;

use App\Models\BaseModel;

class CampaignProducts extends BaseModel
{
	protected $table = 'campaign_products';
}