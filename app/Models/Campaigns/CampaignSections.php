<?php

namespace App\Models\Campaigns;

use App\Models\BaseModel;

class CampaignSections extends BaseModel
{
	protected $table = 'campaign_sections';
}