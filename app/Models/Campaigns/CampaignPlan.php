<?php

namespace App\Models\Campaigns;

use App\Models\BaseModel;

class CampaignPlan extends BaseModel
{
	protected $table = 'campaign_plan';
}