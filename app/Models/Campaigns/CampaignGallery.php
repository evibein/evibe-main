<?php

namespace App\Models\Campaigns;

use App\Models\BaseModel;

class CampaignGallery extends BaseModel
{
	protected $table = 'campaign_carousel_images';
}