<?php

namespace App\Models\Log;

use App\Models\BaseModel;

class LogInvalidSearchOption extends BaseModel
{
	protected $table = 'log_invalid_search_options';
}