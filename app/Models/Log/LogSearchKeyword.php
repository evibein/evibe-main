<?php

namespace App\Models\Log;

use App\Models\BaseModel;

class LogSearchKeyword extends BaseModel
{
	protected $table = 'log_search_keywords';
}
