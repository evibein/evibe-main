<?php

namespace App\Models\Log;

use App\Models\BaseModel;

class LogCouponUsage extends BaseModel
{
	protected $table = 'log_coupon_usage';
}