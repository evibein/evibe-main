<?php

namespace App\Models\Cancellations;

use App\Models\BaseModel;

class CustomerCancellationPolicy extends BaseModel
{
	protected $table = 'customer_cancellation_policy';
}