<?php

namespace App\Models\BrowsingActivity;

use App\Models\BaseModel;

class BrowsingActivity extends BaseModel
{
	protected $table = 'user_browsing_activity_profile';
}