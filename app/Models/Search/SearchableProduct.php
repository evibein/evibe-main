<?php

namespace App\Models\Search;

use App\Models\BaseModel;
use App\Models\Util\City;
use Laravel\Scout\Searchable;

class SearchableProduct extends BaseModel
{
	use Searchable;

	protected $table = "searchable_product";

	public function city()
	{
		return $this->belongsTo(City::class, "city_id");
	}
}
