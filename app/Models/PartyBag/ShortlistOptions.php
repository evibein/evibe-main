<?php

namespace App\Models\PartyBag;

use App\Models\BaseModel;

class ShortlistOptions extends BaseModel
{
	protected $table = 'shortlist_option';
}