<?php

namespace App\Models\PartyBag;

use App\Models\BaseModel;

class ShortlistOptionFieldValue extends BaseModel
{
	protected $table = 'shortlist_option_field_value';
}