<?php

namespace App\Models\VirtualParty;

use App\Models\BaseModel;
use App\Models\VirtualParty\VirtualPartyGuest;

class VirtualPartyGuestWish extends BaseModel
{
	protected $table = 'virtual_party_guest_wish';

	public function partyGuest()
	{
		return $this->belongsTo(VirtualPartyGuest::class,'vp_guest_id');
	}

	
}