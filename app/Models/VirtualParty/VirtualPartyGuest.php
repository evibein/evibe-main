<?php

namespace App\Models\VirtualParty;

use App\Models\BaseModel;

class VirtualPartyGuest extends BaseModel
{
	protected $table = 'virtual_party_guest';
}