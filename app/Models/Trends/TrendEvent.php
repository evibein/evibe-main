<?php

namespace App\Models\Trend;

use App\Models\BaseModel;
use App\Models\Types\TypeEvent;

class TrendEvent extends BaseModel
{
    protected $table = 'trend_event';

    public function event()
    {
        return $this->belongsTo(TypeEvent::class, 'type_event_id');
    }
}