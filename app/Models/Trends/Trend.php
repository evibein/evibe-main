<?php

namespace App\Models\Trend;

use App\Evibe\EvibeTraits\EvibeSearchable;
use App\Evibe\EvibeTraits\EvibeShortListable;
use App\Models\BaseModel;
use App\Models\Util\City;
use App\Models\Vendor\Vendor;

class Trend extends BaseModel
{
	use EvibeSearchable, EvibeShortListable;

	protected $table = 'trending';

	public function city()
	{
		return $this->belongsTo(City::class, 'city_id');
	}

	public function provider()
	{
		return $this->belongsTo(Vendor::class, 'planner_id');
	}

	public function gallery()
	{
		return $this->hasMany(TrendGallery::class, 'trending_id');
	}

	public function getLink($occasionUrl)
	{
		return '/' . getCityUrl() . '/' . $occasionUrl . '/' . config('evibe.profile_url.trends') . '/' . $this->url;
	}

	public function getProfileImg()
	{
		$profilePicUrl = $this->getGalleryBaseUrl() . '/img/app/default_trending.png';

		$galleryItem = TrendGallery::where('trending_id', $this->id)
		                           ->where('type', config('evibe.gallery.type.image'))
		                           ->orderBy('updated_at', 'DESC')
		                           ->first();

		if ($galleryItem)
		{
			$profilePicUrl = $galleryItem->getPath("results");
		}

		return $profilePicUrl;
	}

	public function getPriceWorth()
	{
		return 0;
	}

	public function scopeForEvent($query, $eventId)
	{
		return $query->join('trend_event', 'trend_event.trend_id', '=', 'trending.id')
		             ->whereNull('trend_event.deleted_at')
		             ->where('trend_event.type_event_id', $eventId);
	}

	public function scopeIsLive($query)
	{
		return $query->where('trending.is_live', 1);
	}

	// fetch the decors for the particular city
	// trend->city_id = City.url.id
	// can be improved much with the city sql query
	public function scopeForCity($query)
	{
		$cityUrl = getCityUrl();
		$city = City::where('city.url', $cityUrl)->first();
		$cityId = $city->id ? $city->id : 1;  // set the default to bangalore

		return $query->join('planner', 'planner.id', '=', 'trending.planner_id')
		             ->where('planner.city_id', $cityId);

	}

	public function scopeForSearchSelectCols($query)
	{
		return $query->select("id", "name", "info", "url", "city_id", "price",
		                      "planner_id", "price_worth", "is_live", "price_max",
		                      "range_info");

	}

	public function event()
	{
		return $this->hasMany(TrendEvent::class, 'trend_id');
	}

	public function getPartyBagPriceMin()
	{
		return $this->price ? $this->price : 0;
	}

	public function getPartyBagPriceMax()
	{
		return $this->price ? $this->price : 0;
	}

	public function getPartyBagPriceWorthMin()
	{
		return $this->price ? $this->price : 0;
	}

	public function getPartyBagPriceWorthMax()
	{
		return $this->price ? $this->price : 0;
	}

	public function getCardInfo($fullUrl, $price, $mapTypeId, $occasionId)
	{
		$cardInfo = [
			'id'                  => $this->id,
			'title'               => $this->name ? $this->name : "Trend",
			'price'               => $price,
			'profileImg'          => $this->getProfileImg(),
			'fullUrl'             => $fullUrl,
			'priceMax'            => '',
			'additionalLine'      => '',
			'worth'               => '',
			'rangeInfo'           => '',
			'pbPriceMin'          => $this->getPartyBagPriceMin(),
			'pbPriceMax'          => $this->getPartyBagPriceMax(),
			'pbPriceWorthMin'     => $this->getPartyBagPriceWorthMin(),
			'pbPriceWorthMax'     => $this->getPartyBagPriceWorthMax(),
			'rating'              => '',
			'additionalPriceLine' => ''
		];

		if ($mapTypeId)
		{
			$cardInfo['mapTypeId'] = $mapTypeId;
		}

		if ($occasionId)
		{
			$cardInfo['occasionId'] = $occasionId;
		}

		return $cardInfo;
	}

	public function getSearchableCardInfo($cityUrl, $occasionUrl, $params)
	{
		$searchableCardInfo = [];
		$fullUrl = $this->getFullLinkForSearch($cityUrl, $occasionUrl);
		$occasionId = $this->getOccasionIdForSearch($occasionUrl);
		$price = $this->price;
		$mapTypeId = isset($params['map_type_id']) ? $params['map_type_id'] : null;

		if ($fullUrl && $price)
		{
			$searchableCardInfo = $this->getCardInfo($fullUrl, $price, $mapTypeId, $occasionId);
		}

		return $searchableCardInfo;
	}

	public function getOccasionIdForSearch($occasionUrl)
	{
		if (!$occasionUrl)
		{
			$firstEvent = $this->event->first();
			if ($firstEvent)
			{
				return $firstEvent->event->id;
			}
		}

		return null;
	}

	public function getFullLinkForSearch($cityUrl, $occasionUrl)
	{
		// check if occasion url is empty, get first occasion url
		if (!$occasionUrl || is_null($occasionUrl))
		{
			$firstEvent = $this->event ? $this->event->first() : null;
			$occasionUrl = ($firstEvent && $firstEvent->event) ? $firstEvent->event->url : config('evibe.occasion.kids_birthdays.url');
		}

		$link = "";
		if (isset($this->evibeUtil->occasionUrlRouteMapping[$occasionUrl]))
		{
			$occasionRouteName = $this->evibeUtil->occasionUrlRouteMapping[$occasionUrl];
			$link = $this->getLinkByRouteName($occasionRouteName . "trends.profile", [$cityUrl, $this->url]);
		}

		return $link;
	}

	public function forSearchIsLive()
	{
		return ($this->is_live && $this->provider && $this->provider->is_live && $this->event && $this->event->count());
	}

	public function forSearchPrice()
	{
		return ($this->price ? $this->price : 0);
	}

	public function forSearchCity($cityId)
	{
		return ($this->provider->city_id == $cityId);
	}

	// for party bag
	public function getShortListableCardInfo($cityUrl, $occasionUrl, $params, $shortlistParams)
	{
		$cardInfo = [];
		$fullUrl = $this->getFullLinkForSearch($cityUrl, $occasionUrl);
		$price = $this->price;

		if ($fullUrl && $price)
		{
			$cardInfo = $this->getCardInfo($fullUrl, $price, null, null);
		}

		$cardInfo = array_merge($cardInfo, $shortlistParams);

		return $cardInfo;
	}

	// for collections
	public function getCollectionCardInfo($cityUrl, $occasionUrl, $params, $collectionParams)
	{
		$collectionCardInfo = [];
		$fullUrl = $this->getFullLinkForSearch($cityUrl, $occasionUrl);
		$price = $this->price;
		$occasionId = null;
		if (!$occasionUrl)
		{
			$firstEvent = $this->event->first();
			if ($firstEvent)
			{
				$occasionId = $firstEvent->event->id;
			}
		}

		if ($fullUrl && $price)
		{
			$collectionCardInfo = $this->getCardInfo($fullUrl, $price, null, $occasionId);
		}

		if (!$collectionParams['occasionId'])
		{
			$collectionParams['occasionId'] = $occasionId;
		}

		$collectionCardInfo = array_merge($collectionCardInfo, $collectionParams);

		return $collectionCardInfo;
	}
}