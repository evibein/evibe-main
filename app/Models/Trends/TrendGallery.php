<?php

namespace App\Models\Trend;

use App\Models\BaseModel;

class TrendGallery extends BaseModel
{
	protected $table = 'trending_gallery';

	public function trend()
	{
		return $this->belongsTo(Trend::class, 'trending_id');
	}

	public function getPath($type = null)
	{
		$trend = $this->trend;
		$path = config('evibe.gallery.host');
		$path .= '/trends/' . $trend->id . '/images/';
		if ($type)
		{
			$path .= $type . '/'; // results / profile / thumbs
		}
		$path .= $this->url;

		return $path;
	}
}
