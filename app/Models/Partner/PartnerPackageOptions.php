<?php

namespace App\Models\Partner;

use App\Models\BaseModel;

class PartnerPackageOptions extends BaseModel
{
	protected $table = 'partner_package_options';
}