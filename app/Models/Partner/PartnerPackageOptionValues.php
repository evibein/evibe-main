<?php

namespace App\Models\Partner;

use App\Models\BaseModel;

class PartnerPackageOptionValues extends BaseModel
{
	protected $table = 'partner_package_options_values';
}