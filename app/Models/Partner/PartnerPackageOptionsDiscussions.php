<?php

namespace App\Models\Partner;

use App\Models\BaseModel;

class PartnerPackageOptionsDiscussions extends BaseModel
{
	protected $table = 'partner_package_options_messages';
	protected $guarded = ['id'];
}