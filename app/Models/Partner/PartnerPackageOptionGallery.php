<?php

namespace App\Models\Partner;

use App\Models\BaseModel;

class PartnerPackageOptionGallery extends BaseModel
{
	protected $table = 'partner_package_option_gallery';
}