<?php

namespace App\Models\Partner;

use App\Models\BaseModel;

class PartnerAverageRating extends BaseModel
{
	protected $table = 'partner_average_rating';
}