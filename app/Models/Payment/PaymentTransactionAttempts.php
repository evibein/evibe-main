<?php

namespace App\Models\Payment;

use App\Models\BaseModel;

class PaymentTransactionAttempts extends BaseModel
{
	protected $table = 'payment_transaction_attempts';
}