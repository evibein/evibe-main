<?php

namespace App\Models\Payment;

use App\Models\BaseModel;

class PaymentInitiations extends BaseModel
{
	protected $table = 'payment_initiations';
}