<?php

namespace App\Models\Vendor;

use App\Models\BaseModel;
use App\Models\Service\Service;

class VendorService extends BaseModel
{
	protected $table = 'planner_service';

	public function service()
	{
		return $this->belongsTo(Service::class, 'service_id');
	}
}
