<?php

namespace App\Models\Vendor;

use App\Models\BaseModel;
use App\Models\Package\Package;
use App\Models\Partner\PartnerAverageRating;
use App\Models\Ticket\TicketBooking;
use App\Models\Types\TypeComPref;
use App\Models\Types\TypeTag;
use App\Models\Types\TypeVendor;
use App\Models\Util\Area;
use App\Models\Util\City;
use App\Models\Util\ExperienceTag;
use App\Models\Util\PackageProviderInterface;
use App\Models\Util\PartnerReviewInterface;
use App\Models\Util\User;
use App\Models\Util\VendorStory;

class Vendor extends BaseModel implements PackageProviderInterface, PartnerReviewInterface
{
	protected $table = 'planner';

	public function city()
	{
		return $this->belongsTo(City::class, 'city_id');
	}

	public function area()
	{
		return $this->belongsTo(Area::class, 'area_id');
	}

	public function gallery()
	{
		return $this->hasMany(VendorGallery::class, 'planner_id');
	}

	public function type()
	{
		return $this->belongsTo(TypeVendor::class, 'type_id');
	}

	public function rating()
	{
		return $this->hasOne(PartnerAverageRating::class, 'partner_id')
		            ->where("partner_type_id", config("evibe.ticket.type.planner"));
	}

	public function avgRating()
	{
		$avgRating = VendorReview::where('map_id', $this->id)
		                         ->where('map_type_id', config('evibe.ticket.type.planner'))
		                         ->where('is_accepted', 1);

		return round($avgRating->avg('rating'), 2);
	}

	public function packages()
	{
		return $this->morphMany(Package::class, null, 'map_type_id', 'map_id');
	}

	public function stories()
	{
		return $this->morphMany(VendorStory::class, null, 'map_type_id', 'map_id');
	}

	public function faqs()
	{
		return $this->hasMany(VendorFaq::class, 'planner_id');
	}

	// @todo fix this method
	public function getDirectReviews($count = -1, $skip = -1)
	{
		$directReviews = [];
		$count = ($count != -1) ? $count : PHP_INT_MAX;

		$countObj = $reviewsObj = VendorReview::with('reply')
		                                      ->where('map_id', $this->id)
		                                      ->where('is_accepted', 1)
		                                      ->where('map_type_id', config('evibe.ticket.type.planner'))
		                                      ->orderBy('created_at', 'DESC');

		$directReviews['totalCount'] = $countObj->count();

		if ($skip != -1)
		{
			$reviewsObj->skip($skip);
		}
		$reviewsObj->take($count); // offset needs limit

		$directReviews['reviews'] = $reviewsObj->get();

		return $directReviews;
	}

	public function expTag()
	{
		return $this->belongsTo(ExperienceTag::class, 'exp_tag_id');
	}

	public function tags()
	{
		return $this->belongsToMany(TypeTag::class, 'planner_tags', 'planner_id', 'tile_tag_id')
		            ->wherePivot('deleted_at', null);
	}

	public function comPref()
	{
		return $this->belongsTo(TypeComPref::class, 'com_pref_id');
	}

	// @todo: occasion should be taken into consideration
	public function getLink()
	{
		$cityUrl = $this->city->url;
		$profileUrl = config('evibe.profile_url.planner');
		if (!$cityUrl)
		{
			$cityUrl = getCityUrl();
		}

		return '/' . $cityUrl . '/' . $profileUrl . '/' . $this->url;
	}

	public function bookings()
	{
		return $this->morphMany(TicketBooking::class, null, 'map_type_id', 'map_id');
	}

	public function getPriceWorth()
	{
		return 0;
	}

	public function getPersonAttribute($value)
	{
		return $this->getFormattedName($value);
	}

	public function getPartnerReview($skip = 0, $take = 10, $sortBy = false)
	{
		$mapTypeId = config('evibe.ticket.type.planner');

		return $this->partnerReviewData($mapTypeId, $this->id, $skip, $take, $sortBy);
	}

	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}
}
