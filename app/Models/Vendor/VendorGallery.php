<?php

namespace App\Models\Vendor;

use App\Models\BaseModel;

class VendorGallery extends BaseModel
{
	protected $table = 'planner_gallery';
}