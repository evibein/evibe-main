<?php

namespace App\Models\Vendor;

use App\Models\BaseModel;
use App\Models\Ticket\TicketBooking;
use App\Models\Types\TypeTicket;
use App\Models\Util\PartnerReviewAnswer;
use App\Models\Util\ReviewReply;

class VendorReview extends BaseModel
{
	protected $table = 'planner_review';

	public function reply()
	{
		return $this->hasOne(ReviewReply::class, 'review_id');
	}

	public function individualAnswers()
	{
		return $this->hasMany(PartnerReviewAnswer::class, 'review_id');
	}

	public function typeTicket()
	{
		return $this->belongsTo(TypeTicket::class, 'type_ticket_id');
	}

	public function booking()
	{
		return $this->belongsTo(TicketBooking::class, 'ticket_booking_id');
	}
}
