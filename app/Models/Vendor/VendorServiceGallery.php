<?php

namespace App\Models\Vendor;

use App\Models\BaseModel;

class PlannerServiceGallery extends BaseModel
{
	protected $table = 'planner_service_gallery';
}