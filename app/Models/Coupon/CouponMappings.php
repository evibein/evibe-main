<?php

namespace App\Models\Coupon;

use App\Models\BaseModel;

class CouponMappings extends BaseModel
{
	protected $table = 'coupon_mappings';
}