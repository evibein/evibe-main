<?php

namespace App\Models\Coupon;

use App\Models\BaseModel;

class CouponRedeemed extends BaseModel
{
	protected $table = 'coupon_redeemed';
}