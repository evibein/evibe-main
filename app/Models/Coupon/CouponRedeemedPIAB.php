<?php

namespace App\Models\Coupon;

use App\Models\BaseModel;

class CouponRedeemedPIAB extends BaseModel
{
	protected $table = 'coupon_redeemed_piab';
}