<?php

namespace App\Models\Product;

use App\Models\BaseModel;

class ProductBooking extends BaseModel
{
	protected $table = 'product_booking';

	public function status()
	{
		return $this->belongsTo(ProductBookingStatus::class, 'product_booking_status_id');
	}

	public function style()
	{
		return $this->belongsTo(ProductStyle::class, 'product_style_id');
	}
}