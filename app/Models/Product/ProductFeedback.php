<?php

namespace App\Models\Product;

use App\Models\BaseModel;

class ProductFeedback extends BaseModel
{
	protected $table = 'product_feedback';
}