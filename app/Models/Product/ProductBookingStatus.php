<?php

namespace App\Models\Product;

use App\Models\BaseModel;

class ProductBookingStatus extends BaseModel
{
	protected $table = 'product_booking_status';
}