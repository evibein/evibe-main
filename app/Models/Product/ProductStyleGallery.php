<?php

namespace App\Models\Product;

use App\Models\BaseModel;

class ProductStyleGallery extends BaseModel
{
	protected $table = 'product_style_gallery';

	public function productStyle()
	{
		return $this->belongsTo(ProductStyle::class, 'product_style_id');
	}

	public function getPath($type = null)
	{
		$productStyle = $this->productStyle;

		if ($this->type_id == config('evibe.gallery.type.video'))
		{
			$path = '';
			$videoUrl = $this->validateYouTubeLink($this->url);

			if ($videoUrl)
			{
				if ($type == 'thumbs')
				{
					$path = $videoUrl->thumbnail_url;
				}
				else
				{
					$path = $this->getVideoWithCustomOption($this->url);
				}
			}

		}
		else
		{
			$path = config('evibe.gallery.host');
			$path .= '/product-styles/' . $productStyle->id . '/images/';
			if ($type)
			{
				$path .= $type . '/'; // results / profile / thumbs
			}
			$path .= $this->url;
		}

		return $path;
	}
}