<?php

namespace App\Models\Product;

use App\Models\BaseModel;

class ProductCategory extends BaseModel
{
	protected $table = 'product_category';
}