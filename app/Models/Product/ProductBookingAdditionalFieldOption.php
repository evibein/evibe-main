<?php

namespace App\Models\Product;

use App\Models\BaseModel;

class ProductBookingAdditionalFieldOption extends BaseModel
{
	protected $table = 'product_booking_additional_field_option';
}