<?php

namespace App\Models\Product;

use App\Models\BaseModel;

class ProductStyle extends BaseModel
{
	protected $table = 'product_style';

	public function gallery()
	{
		return $this->hasMany(ProductStyleGallery::class, 'product_style_id');
	}

	public function product()
	{
		return $this->belongsTo(Product::class, 'product_id');
	}
}