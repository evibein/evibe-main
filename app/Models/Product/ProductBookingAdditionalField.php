<?php

namespace App\Models\Product;

use App\Models\BaseModel;

class ProductBookingAdditionalField extends BaseModel
{
	protected $table = 'product_booking_additional_field';

	public function fieldOptions()
	{
		return $this->hasMany(ProductBookingAdditionalFieldOption::class, 'additional_field_id');
	}

	public function getFieldValue($productBookingId)
	{
		$productBookingAdditionalFieldValue = ProductBookingAdditionalFieldValue::where([
			                                                                                'product_booking_id'  => $productBookingId,
			                                                                                'additional_field_id' => $this->id,
		                                                                                ])->first();
		if ($productBookingAdditionalFieldValue)
		{
			return $productBookingAdditionalFieldValue->value;
		}
	}
}