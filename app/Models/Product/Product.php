<?php

namespace App\Models\Product;

use App\Models\BaseModel;

class Product extends BaseModel
{
	protected $table = 'product';

	public function styles()
	{
		return $this->hasMany(ProductStyle::class, 'product_id');
	}
}