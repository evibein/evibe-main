<?php

namespace App\Models\Product;

use App\Models\BaseModel;

class ProductBookingAdditionalFieldValue extends BaseModel
{
	protected $table = 'product_booking_additional_field_value';
}