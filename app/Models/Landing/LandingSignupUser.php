<?php

namespace App\Models\Landing;

use App\Models\BaseModel;

class LandingSignupUser extends BaseModel
{
	protected $table = 'landing_signup_users';
}