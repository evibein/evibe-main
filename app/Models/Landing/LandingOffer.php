<?php

namespace App\Models\Landing;

use App\Models\BaseModel;

class LandingOffer extends BaseModel
{
	protected $table = 'offer_landing';
}