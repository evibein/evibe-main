<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;
use App\Models\Package\Package;
use App\Models\Types\TypeBookingConcept;
use App\Models\Types\TypeTicketBooking;
use App\Models\Util\Area;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class TicketBooking extends BaseModel
{
	protected $table = 'ticket_bookings';

	protected static function boot()
	{
		parent::boot();

		static::addGlobalScope('cancelled_at', function (Builder $builder) {
			$builder->whereNull('cancelled_at');
		});
	}

	public function mapping()
	{
		return $this->belongsTo(TicketMapping::class, 'ticket_mapping_id');
	}

	public function package()
	{
		return $this->belongsTo(Package::class, 'planner_package_id');
	}

	public function area()
	{
		return $this->belongsTo(Area::class, 'area_id');
	}

	public function provider()
	{
		return $this->morphTo(null, 'map_type_id', 'map_id');
	}

	public function gallery()
	{
		return $this->hasMany(TicketBookingGallery::class, 'ticket_booking_id');
	}

	public function ticket()
	{
		return $this->belongsTo(Ticket::class, 'ticket_id');
	}

	public function typeBooking()
	{
		return $this->belongsTo(TypeTicketBooking::class, 'type_ticket_booking_id');
	}

	public function bookingConcept()
	{
		return $this->belongsTo(TypeBookingConcept::class, 'type_booking_concept_id');
	}

	public function scopeBookedTicket($query)
	{
		return $query->join('ticket', 'ticket_bookings.ticket_id', '=', 'ticket.id')
		             ->where('ticket.status_id', config('evibe.ticket.status.booked'))
		             ->where('ticket_bookings.is_advance_paid', 1)
		             ->whereNull('ticket.deleted_at')
		             ->whereNull('ticket_bookings.deleted_at');
	}

	public function scopeForDeliverable($query, $diffMinutes = 120)
	{
		$end = Carbon::now()->timestamp;
		$end += $diffMinutes * 60; // we show 60 minutes before party stat time as live delivery
		$tenDaysStart = Carbon::today()->addDay('-10')->startOfDay()->timestamp;
		$dateRange = [$tenDaysStart, $end];

		return $query->whereBetween('ticket_bookings.party_date_time', $dateRange)
		             ->where('ticket_bookings.is_delivered', 0)
		             ->orderBy('ticket_bookings.party_date_time', 'DESC');
	}
}