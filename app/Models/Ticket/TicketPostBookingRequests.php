<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;

class TicketPostBookingRequests extends BaseModel
{
	protected $table = 'post_booking_requests';
}