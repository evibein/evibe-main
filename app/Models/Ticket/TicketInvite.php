<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;

class TicketInvite extends BaseModel
{
	protected $table = 'ticket_invites';
}