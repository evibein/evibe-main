<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;

class TicketFalloutFeedback extends BaseModel
{
	protected $table = 'ticket_fallout_feedback';
}