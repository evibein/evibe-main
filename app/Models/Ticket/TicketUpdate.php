<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;

class TicketUpdate extends BaseModel
{
	protected $table = 'ticket_update';
}