<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;

class TypeTicketSource extends BaseModel
{
	protected $table = 'type_ticket_source';
}