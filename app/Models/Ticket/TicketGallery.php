<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;

class TicketGallery extends BaseModel
{
	protected $table = 'ticket_gallery';
}
