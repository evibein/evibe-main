<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;
use App\Models\Cake\Cake;
use App\Models\Decor\Decor;
use App\Models\Package\Package;
use App\Models\Trend\Trend;
use App\Models\Vendor\Vendor;
use App\Models\Venue\Venue;
use App\Models\Venue\VenueHall;

class TicketMapping extends BaseModel
{
	protected $table = 'ticket_mapping';

	public function ticket()
	{
		return $this->belongsTo(Ticket::class, 'ticket_id');
	}

	public function getValues()
	{
		$type = 'party organiser';
		$mapId = $this->map_id;
		$uniqueId = null;
		$id = null;
		$name = null;
		$providerMapTypeId = $this->map_type_id;

		switch ($this->map_type_id)
		{
			case 1:
			case 17:
			case 18:
			case 19:
			case 20:
			case 21:
			case 22:
			case 23:
			case 24:
				$package = Package::find($mapId);
				if ($package && $package->provider)
				{
					$id = $package->provider->id;
					$name = $package->provider->person;
					$providerMapTypeId = $package->map_type_id;
				}
				break;

			case 2:
				$vendor = Vendor::find($mapId);
				if ($vendor)
				{
					$id = $vendor->id;
					$name = $vendor->person;
				}
				break;

			case 3:
				$venueHall = VenueHall::find($mapId);
				if ($venueHall && $venueHall->venue)
				{
					$id = $venueHall->venue->id;
					$name = $venueHall->venue->name; // venue
					$type = 'venue';
				}
				break;

			case 5:
				$trend = Trend::find($mapId);
				if ($trend)
				{
					$id = $trend->provider->id;
					$name = $trend->provider->person;
				}
				break;

			case 6:
				$cake = Cake::find($mapId);
				if ($cake)
				{
					$id = $cake->provider->id;
					$name = $cake->provider->person;
					$type = 'cake provider';
				}
				break;

			case 13:
				$decor = Decor::find($mapId);
				if ($decor)
				{
					$id = $decor->provider->id;
					$name = $decor->provider->person;
					$type = 'decor provider';
				}
				break;
		}

		$mappingValues = [
			'type'      => $type,
			'id'        => $id,
			'name'      => $name,
			'mapTypeId' => $providerMapTypeId,
			'uniqueKey' => $providerMapTypeId . '-' . $id
		];

		return $mappingValues;
	}

}