<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;

class TicketBookingGallery extends BaseModel
{
	protected $table = 'ticket_booking_gallery';

	public function ticketBooking()
	{
		return $this->belongsTo(TicketBooking::class, 'ticket_booking_id');
	}

	public function getImageBasePath()
	{
		$ticketBookingId = $this->ticket_booking_id;
		$ticketId = $this->ticketBooking->ticket->id;

		$imagePath = config('evibe.gallery.host') . '/ticket/' . $ticketId . '/' . $ticketBookingId . '/';

		return $imagePath;
	}

	public function getOriginalImagePath()
	{
		if ($this->type == 1)
		{
			return $this->url;
		}

		return $this->getImageBasePath() . $this->url;
	}

	public function getResultsImagePath()
	{
		if ($this->type == 1)
		{
			return $this->url;
		}

		return $this->getImageBasePath() . 'results/' . $this->url;
	}
}
