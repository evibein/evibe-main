<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;

class TicketFollowup extends BaseModel
{
	protected $table = 'ticket_followups';
}
