<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;

class TicketGoogleAnalyticsData extends BaseModel
{
	protected $table = 'ticket_google_analytics_data';
}