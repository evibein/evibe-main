<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;

class TicketThankYouCards extends BaseModel
{
	protected $table = 'ticket_thankyou_cards';
}