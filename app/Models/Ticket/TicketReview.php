<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;
use App\Models\Util\TicketReviewQuestionAnswer;

class TicketReview extends BaseModel
{
	protected $table = 'ticket_review';

	public function extraAnswers()
	{
		return $this->hasMany(TicketReviewQuestionAnswer::class, 'review_id');
	}
}
