<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;

class TicketReminderStack extends BaseModel
{
	protected $table = 'ticket_reminders_stack';
}