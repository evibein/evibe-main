<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;
use App\Models\Types\TypeSlot;
use App\Models\Types\TypeTicket;
use App\Models\Types\TypeVenue;
use App\Models\Util\Area;
use App\Models\Util\City;
use App\Models\Util\User;

class Ticket extends BaseModel
{
	protected $table = 'ticket';

	public function area()
	{
		return $this->belongsTo(Area::class, 'area_id');
	}

	public function city()
	{
		return $this->belongsTo(City::class, 'city_id');
	}

	public function bookings()
	{
		return $this->hasMany(TicketBooking::class, 'ticket_id');
	}

	public function handler()
	{
		return $this->belongsTo(User::class, 'handler_id');
	}

	public function followups()
	{
		return $this->hasMany(TicketFollowup::class, 'ticket_id');
	}

	public function mappings()
	{
		return $this->hasMany(TicketMapping::class, 'ticket_id');
	}

	public function typeCheckoutVenue()
	{
		return $this->belongsTo(TypeVenue::class, 'type_venue_id');
	}

	public function slot()
	{
		return $this->belongsTo(TypeSlot::class, 'type_slot_id');
	}

	public function isBooked()
	{
		return ($this->status_id == config('evibe.ticket.status.booked'));
	}

	public function isAutoPaid()
	{
		return ($this->status_id == config('evibe.ticket.status.auto_paid'));
	}

	public function ticketReview()
	{
		return $this->hasOne(TicketReview::class, 'ticket_id');
	}

	public function raisedForType()
	{
		return $this->belongsTo(TypeTicket::class, 'type_ticket_id');
	}

	// Mutators
	public function setNameAttribute($value)
	{
		$this->attributes['name'] = $this->getFormattedName($value);
	}

	public function getNameAttribute($value)
	{
		return $this->getFormattedName($value);
	}
}