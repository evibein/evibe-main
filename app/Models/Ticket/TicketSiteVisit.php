<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;

class TicketSiteVisit extends BaseModel
{
	protected $table = 'ticket_site_visit';
}