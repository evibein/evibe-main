<?php

namespace App\Models\AddOn;

use App\Models\BaseModel;

class AddOnTags extends BaseModel
{
	protected $table = 'add_on_tags';
}