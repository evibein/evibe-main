<?php

namespace App\Models\AddOn;

use App\Models\BaseModel;

class AddOnEvent extends BaseModel
{
	protected $table = 'add_on_event';
}