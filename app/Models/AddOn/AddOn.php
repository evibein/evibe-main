<?php

namespace App\Models\AddOn;

use App\Models\BaseModel;

class AddOn extends BaseModel
{
	protected $table = 'add_on';

	public function scopeForEvent($query, $eventId)
	{
		return $query->select('add_on.*', 'add_on_event.event_id')
		             ->join('add_on_event', 'add_on_event.add_on_id', '=', 'add_on.id')
		             ->whereNull('add_on_event.deleted_at')
		             ->where('add_on_event.event_id', $eventId);
	}

	public function scopeIsLive($query)
	{
		return $query->where('add_on.is_live', 1);
	}

	public function scopeForCity($query, $cityId = null)
	{
		if (!$cityId)
		{
			$cityUrl = getCityUrl();
			$city = City::where('url', $cityUrl)->first();
			$cityId = $city ? $city->id : 1;  // set the default to bangalore
		}

		return $query->where('add_on.city_id', $cityId);
	}
}