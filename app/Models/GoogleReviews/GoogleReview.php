<?php

namespace App\Models\GoogleReviews;

use App\Models\BaseModel;

class GoogleReview extends BaseModel
{
	protected $table = 'google_reviews';
}