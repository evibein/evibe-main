<?php

namespace App\Models\Invite;

use App\Models\BaseModel;

class InviteCardCategory extends BaseModel
{
	protected $table = 'type_card_categories';
}