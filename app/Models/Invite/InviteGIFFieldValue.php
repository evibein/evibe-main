<?php

namespace App\Models\Invite;

use App\Models\BaseModel;

class InviteGIFFieldValue extends BaseModel
{
	protected $table = 'type_invite_gif_values';
}