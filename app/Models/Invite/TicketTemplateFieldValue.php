<?php

namespace App\Models\Invite;

use App\Models\BaseModel;

class TicketTemplateFieldValue extends BaseModel
{
	protected $table = 'ticket_template_field_value';
}