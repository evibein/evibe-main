<?php

namespace App\Models\Invite;

use App\Models\BaseModel;

class InviteTemplateUsers extends BaseModel
{
	protected $table = 'invite_template_users';
}