<?php

namespace App\Models\Invite;

use App\Models\BaseModel;

class InviteTemplate extends BaseModel
{
	protected $table = 'invite_template';

	public function cardCategory()
	{
		return $this->belongsTo(InviteCardCategory::class, 'type_card_category_id');
	}
}