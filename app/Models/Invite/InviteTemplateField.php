<?php

namespace App\Models\Invite;

use App\Models\BaseModel;

class InviteTemplateField extends BaseModel
{
	protected $table = 'invite_template_field';
}