<?php

namespace App\Models\Package;

use App\Models\BaseModel;

class PackageItineraryValue extends BaseModel
{
	protected $table = 'planner_package_itinerary';
}