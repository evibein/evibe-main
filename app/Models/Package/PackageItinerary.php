<?php

namespace App\Models\Package;

use App\Models\BaseModel;

class PackageItinerary extends BaseModel
{
	protected $table = 'package_itinerary_name';

	public function values()
	{
		return $this->hasMany(PackageItineraryValue::class, 'itinerary_id');
	}
}