<?php

namespace App\Models\Package;

use App\Models\BaseModel;
use App\Models\Vendor\VendorMoreService;
use App\Models\Vendor\VendorService;

class PackageService extends BaseModel
{
	protected $table = 'planner_package_service';

	public function plannerService()
	{
		return $this->belongsTo(VendorService::class, 'planner_service_id');
	}

	public function plannerMoreService()
	{
		return $this->belongsTo(VendorMoreService::class, 'planner_service_id');
	}
}
