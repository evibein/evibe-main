<?php

namespace App\Models\Package;

use App\Models\BaseModel;

class PackageReview extends BaseModel
{
	protected $table = 'planner_package_reviews';
}