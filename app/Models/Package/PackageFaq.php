<?php

namespace App\Models\Package;

use App\Models\BaseModel;

class PackageFaq extends BaseModel
{
	protected $table = 'planner_package_faqs';
}