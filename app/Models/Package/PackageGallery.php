<?php

namespace App\Models\Package;

use App\Models\BaseModel;

class PackageGallery extends BaseModel
{
	protected $table = 'planner_package_gallery';

	public function package()
	{
		return $this->belongsTo(Package::class, 'planner_package_id');
	}

	public function getPath($type = null)
	{
		$package = $this->package;
		$providerType = $package->map_type_id == config('evibe.ticket.type.venue') ? "venues" : "planner"; //
		$path = config('evibe.gallery.host');
		$path .= "/$providerType/" . $package->map_id . "/images";
		$path .= "/packages/" . $package->id . '/';
		$path .= $type ? "$type/" : ""; // results / profile / thumbs
		$path .= $this->url;

		return $path;
	}

	public function getVideoInfo()
	{
		$data = [
			'url'   => '',
			'thumb' => ''
		];
		$videoUrl = $this->validateYouTubeLink($this->url);
		if ($videoUrl)
		{
			$data['url'] = $this->getVideoWithCustomOption($this->url);
			$data['thumb'] = $videoUrl->thumbnail_url;
		}

		return $data;
	}
}
