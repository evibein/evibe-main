<?php

namespace App\Models\Package;

use App\Evibe\EvibeTraits\EvibeSearchable;
use App\Evibe\EvibeTraits\EvibeShortListable;
use App\Http\Controllers\Base\BaseRecommendationController;
use App\Models\BaseModel;
use App\Models\Types\TypePackageField;
use App\Models\Types\TypeTag;
use App\Models\Types\TypeVenueHall;
use App\Models\Util\Area;
use App\Models\Util\City;
use App\Models\Vendor\VendorReview;
use App\Models\Types\TypeEvent;

use Evibe\Implementers\CountsViews;
use Illuminate\Support\Facades\DB;

class Package extends BaseModel implements CountsViews
{
	use EvibeSearchable, EvibeShortListable;

	protected $table = 'planner_package';

	/* @implements */
	public function incrementViews()
	{
		$this->views_count += 1;
		$this->save();
	}

	public function gallery()
	{
		return $this->hasMany(PackageGallery::class, 'planner_package_id');
	}

	public function provider()
	{
		return $this->morphTo(null, 'map_type_id', 'map_id');
	}

	public function faqs()
	{
		return $this->hasMany(PackageFaq::class, 'planner_package_id');
	}

	public function reviews()
	{
		return $this->hasMany(PackageReview::class, 'planner_package_id');
	}

	public function itinerary()
	{
		return $this->hasMany(PackageItinerary::class, 'package_id');
	}

	public function services()
	{
		return $this->hasMany(PackageService::class, 'planner_package_id');
	}

	public function tags()
	{
		return $this->belongsToMany(TypeTag::class, 'planner_package_tags', 'planner_package_id', 'tile_tag_id')
		            ->wherePivot('deleted_at', null);
	}

	public function city()
	{
		return $this->belongsTo(City::class, 'city_id');
	}

	public function area()
	{
		return $this->belongsTo(Area::class, 'area_id');
	}

	public function propertyType()
	{
		return $this->belongsTo(TypeVenueHall::class, 'hall_type_id');
	}

	public function getTags()
	{
		$tags = [];
		$packageTagsList = $this->tags->unique('id');
		$availableTags = [];
		$allTags = TypeTag::all();
		foreach ($allTags as $tag)
		{
			$availableTags[$tag->id] = [
				'name'      => $tag->identifier,
				'parent_id' => $tag->parent_id
			];
		}

		foreach ($packageTagsList as $packageTag)
		{
			$currentTag = $availableTags[$packageTag->id];
			$tagString = $currentTag['name'];
			if ($tagString)
			{
				$tags[$packageTag->id] = $tagString;
			}
		}

		return $tags;
	}

	public function getProfileImg($type = "results")
	{
		$profileImgUrl = config('evibe.gallery.host');
		$profileImgObj = PackageGallery::where('planner_package_id', $this->id)
		                               ->where('type', config('evibe.gallery.type.image'))
		                               ->orderBy('is_profile', 'DESC')
		                               ->orderBy('created_at', 'DESC')
		                               ->first();

		if ($profileImgObj)
		{
			$profileImgUrl = $profileImgObj->getPath($type);
		}

		return $profileImgUrl;
	}

	public function getProviderAvgRating()
	{
		return $this->provider->avgRating();
	}

	public function getAvgRating()
	{
		$packageRatings = PackageReview::select(DB::raw('count(*) as count, avg(rating) as avg'))->first();

		return [
			'count' => $packageRatings->count ? $packageRatings->count : 0,
			'value' => $packageRatings->avg ? $packageRatings->avg : 0
		];
	}

	public function scopeNonArtist($query)
	{
		return $query->where('map_type_id', '!=', config('evibe.ticket.type.artist'));
	}

	public function getSimilarPackages($eventId = null)
	{
		// get packages having similar tags as this package
		$tagIds = array_column($this->tags->toArray(), 'id');

		$eventIds = [config('evibe.occasion.kids_birthdays.id'), config('evibe.occasion.bachelors.id')];
		if ($eventId)
		{
			$eventIds = [$eventId];
		}

		$similarPackages = Package::join('planner_package_tags', 'planner_package_tags.planner_package_id', '=', 'planner_package.id')
		                          ->whereIn('planner_package_tags.tile_tag_id', $tagIds)
		                          ->select('planner_package.*')
		                          ->distinct()
		                          ->where('planner_package.id', '!=', $this->id)
		                          ->where('planner_package.is_live', 1)
		                          ->whereIn('planner_package.event_id', $eventIds)
		                          ->orderBy('planner_package.priority', 'DESC')
		                          ->forCity()
		                          ->take(3)
		                          ->get();

		return $similarPackages;
	}

	public function getReviews($count = -1, $skip = -1)
	{
		$count = ($count != -1) ? $count : PHP_INT_MAX;
		$totalRating = 0;
		$reviews = ['count' => 0, 'reviews' => [], 'avgRating' => 0];
		$countObj = $reviewsObj = VendorReview::where('map_type_id', $this->type_ticket_id)
		                                      ->where('map_id', $this->id)
		                                      ->orderBy('created_at', 'DESC');

		if ($skip != -1)
		{
			$reviewsObj->skip($skip);
		}
		$reviewsObj->take($count)->get();

		foreach ($reviewsObj as $packageReview)
		{
			$reviewReply = '';
			if ($packageReview->reply)
			{
				$reviewReply = $packageReview->reply->reply;
			}

			array_push($reviews['reviews'], [
				'name'   => $packageReview->name,
				'review' => $packageReview->review,
				'email'  => $packageReview->email,
				'phone'  => $packageReview->phone,
				'rating' => $packageReview->rating,
				'reply'  => $reviewReply
			]);

			$totalRating += $packageReview->rating;
		}

		$reviews['totalCount'] = $countObj->count();
		if ($count)
		{
			$reviews['avgRating'] = round($totalRating / $count, 2);
		}

		return $reviews;
	}

	public function getLink($occasionUrl, $cityUrl = null)
	{
		$host = config('evibe.host');
		$profileUrl = config('evibe.profile_url.packages');

		if (is_null($cityUrl))
		{
			$cityUrl = $this->provider->city->url;
		}

		return $host . '/' . $cityUrl . '/' . $occasionUrl . '/' . $profileUrl . '/' . $this->url;
	}

	public function getPriceWorth()
	{
		$worth = $this->price_worth;
		if ($this->price_worth <= $this->price)
		{
			$worth = 0;
		}

		return $worth;
	}

	public function scopeForEvent($query, $eventId)
	{
		return $query->where('planner_package.event_id', $eventId);
	}

	public function scopeJoinTags($query)
	{
		return $query->join('planner_package_tags', 'planner_package_tags.planner_package_id', '=', 'planner_package.id')
		             ->whereNull('planner_package_tags.deleted_at');
	}

	public function scopeJoinVenue($query)
	{
		return $query->join('venue', 'venue.id', '=', 'planner_package.map_id')
		             ->whereNull('venue.deleted_at');
	}

	// @todo: also check is_live for provider
	public function scopeIsLive($query)
	{
		return $query->where('planner_package.is_live', 1);
	}

	public function scopeForSearchSelectCols($query)
	{
		return $query->select("id", "name", "info", "price", "url",
		                      "map_id", "map_type_id", "is_live", "price_worth", "price_max",
		                      "type_ticket_id", "event_id", "city_id");
	}

	// @see: assumes already joined with tags
	public function scopeForPage($query, $pageId)
	{
		$pageIds = is_array($pageId) ? $pageId : [$pageId];

		return $query->whereIn('planner_package.type_ticket_id', $pageIds);
	}

	public function scopeForNotPage($query, $pageIds)
	{
		$pageIds = is_array($pageIds) ? $pageIds : [$pageIds];

		return $query->whereNotIn('planner_package.type_ticket_id', $pageIds);
	}

	public function scopeForCity($query)
	{
		$cityId = getCityId();

		return $query->where('planner_package.city_id', '=', $cityId);
	}

	// written for maptypeid in hyderabad bachelors list
	public function getMapTypeId()
	{
		$data = (new BaseRecommendationController)->getPackageCategoryBasedOnEvent($this, $this->event_id);

		if ($data && isset($data['ticketTypeId']))
		{
			return $data['ticketTypeId'];
		}
	}

	public function getDiscountPercentage()
	{
		$discountPercentage = 0;
		$worthPrice = $this->getPriceWorth();
		if ($worthPrice > 0)
		{
			$minPrice = $this->price;

			$discountPercentage = (($worthPrice - $minPrice) / $worthPrice) * 100;
		}

		return round($discountPercentage);
	}

	public function event()
	{
		return $this->belongsTo(TypeEvent::class, "event_id");
	}

	public function getAdditionalLine($typeName)
	{
		$rent_duration = false;
		if ($typeName == 'venue-deals')
		{
			$rentDurationFieldId = TypePackageField::where('name', config('evibe.pkg_fields.keys.rent_duration'))
			                                       ->value('id');

			if ($rentDurationFieldId)
			{
				$rent_duration = PackageFieldValue::where(['package_id' => $this->id, 'type_package_field_id' => $rentDurationFieldId])
				                                  ->value('value');
			}

			if ($rent_duration)
			{
				return '( ' . $rent_duration . ' hours rent)';
			}
			else
			{
				return '(per person)';
			}
		}
		else
		{
			return '';
		}
	}

	public function getRangeInfo($typeName)
	{
		if ($typeName == 'venue-deals' && $this->guestMin)
		{
			if ($this->guestMax && $this->guestMax > 0)
			{
				return $this->guestMin . ' - ' . $this->guestMax;
			}

			return $this->guestMin;
		}

		return ($this->range_info ? $this->range_info : "");
	}

	public function getPartyBagPriceMin($typeName)
	{
		$partyBagPriceMin = 0;

		switch ($typeName)
		{
			case "venue-deals":
				$rent_duration = 0;
				$partyBagPriceMin = $this->price;
				$rentDurationFieldId = TypePackageField::where('name', config('evibe.pkg_fields.keys.rent_duration'))
				                                       ->value('id');

				if ($rentDurationFieldId)
				{
					$rent_duration = PackageFieldValue::where(['package_id' => $this->id, 'type_package_field_id' => $rentDurationFieldId])
					                                  ->value('value');
				}
				if (!$rent_duration)
				{
					if ($this->guestMin)
					{
						$partyBagPriceMin = $this->price * $this->guestMin;
					}
				}
				break;
			case "packages":
				$partyBagPriceMin = $this->price;
				break;
			case "birthday-packages":
				$partyBagPriceMin = $this->price;
				break;
			case "resorts":
				$partyBagPriceMin = $this->price;
				break;
			case "villas":
				$partyBagPriceMin = $this->price;
				break;
			case "lounges":
				$partyBagPriceMin = $this->price;
				break;
			case "food":
				$partyBagPriceMin = $this->price;
				break;
			case "surprises":
				$partyBagPriceMin = $this->price;
				break;
		}

		return $partyBagPriceMin;
	}

	public function getPartyBagPriceMax($typeName)
	{
		$partyBagPriceMax = 0;
		switch ($typeName)
		{
			case "venue-deals":
				$rent_duration = 0;
				$partyBagPriceMax = $this->price;
				if ($this->price_max && $this->price_max > $this->price)
				{
					$partyBagPriceMax = $this->price_max;
				}
				$rentDurationFieldId = TypePackageField::where('name', config('evibe.pkg_fields.keys.rent_duration'))
				                                       ->value('id');

				if ($rentDurationFieldId)
				{
					$rent_duration = PackageFieldValue::where(['package_id' => $this->id, 'type_package_field_id' => $rentDurationFieldId])
					                                  ->value('value');
				}
				if (!$rent_duration)
				{
					if ($this->guestMin)
					{
						if ($this->guestMax && $this->guestMax > $this->guestMin)
						{
							$partyBagPriceMax = $partyBagPriceMax * $this->guestMax;
						}
						else
						{
							$partyBagPriceMax = $partyBagPriceMax * $this->guestMin;
						}
					}
				}
				break;
			case "packages":
				$partyBagPriceMax = $this->price;
				if ($this->price_max && $this->price_max > $this->price)
				{
					$partyBagPriceMax = $this->price_max;
				}
				break;
			case "birthday-packages":
				$partyBagPriceMax = $this->price;
				if ($this->price_max && $this->price_max > $this->price)
				{
					$partyBagPriceMax = $this->price_max;
				}
				break;
			case "resorts":
				$partyBagPriceMax = $this->price;
				if ($this->price_max && $this->price_max > $this->price)
				{
					$partyBagPriceMax = $this->price_max;
				}
				break;
			case "villas":
				$partyBagPriceMax = $this->price;
				if ($this->price_max && $this->price_max > $this->price)
				{
					$partyBagPriceMax = $this->price_max;
				}
				break;
			case "lounges":
				$partyBagPriceMax = $this->price;
				if ($this->price_max && $this->price_max > $this->price)
				{
					$partyBagPriceMax = $this->price_max;
				}
				break;
			case "food":
				$partyBagPriceMax = $this->price;
				if ($this->price_max && $this->price_max > $this->price)
				{
					$partyBagPriceMax = $this->price_max;
				}
				break;
			case "surprises":
				$partyBagPriceMax = $this->price;
				if ($this->price_max && $this->price_max > $this->price)
				{
					$partyBagPriceMax = $this->price_max;
				}
				break;
		}

		return $partyBagPriceMax;
	}

	public function getPartyBagPriceWorthMin($typeName)
	{
		$partyBagPriceWorthMin = 0;
		switch ($typeName)
		{
			case "venue-deals":
				$rent_duration = 0;
				$partyBagPriceWorthMin = $this->price;
				if ($this->price_worth && $this->price_worth > $this->price)
				{
					$partyBagPriceWorthMin = $this->price_worth;
				}
				$rentDurationFieldId = TypePackageField::where('name', config('evibe.pkg_fields.keys.rent_duration'))
				                                       ->value('id');

				if ($rentDurationFieldId)
				{
					$rent_duration = PackageFieldValue::where(['package_id' => $this->id, 'type_package_field_id' => $rentDurationFieldId])
					                                  ->value('value');
				}
				if (!$rent_duration)
				{
					if ($this->guestMin)
					{
						$partyBagPriceWorthMin = $partyBagPriceWorthMin * $this->guestMin;
					}
				}
				break;
			case "packages":
				$partyBagPriceWorthMin = $this->price;
				if ($this->price_worth && $this->price_worth > $this->price)
				{
					$partyBagPriceWorthMin = $this->price_worth;
				}
				break;
			case "birthday-packages":
				$partyBagPriceWorthMin = $this->price;
				if ($this->price_worth && $this->price_worth > $this->price)
				{
					$partyBagPriceWorthMin = $this->price_worth;
				}
				break;
			case "resorts":
				$partyBagPriceWorthMin = $this->price;
				if ($this->price_worth && $this->price_worth > $this->price)
				{
					$partyBagPriceWorthMin = $this->price_worth;
				}
				break;
			case "villas":
				$partyBagPriceWorthMin = $this->price;
				if ($this->price_worth && $this->price_worth > $this->price)
				{
					$partyBagPriceWorthMin = $this->price_worth;
				}
				break;
			case "lounges":
				$partyBagPriceWorthMin = $this->price;
				if ($this->price_worth && $this->price_worth > $this->price)
				{
					$partyBagPriceWorthMin = $this->price_worth;
				}
				break;
			case "food":
				$partyBagPriceWorthMin = $this->price;
				if ($this->price_worth && $this->price_worth > $this->price)
				{
					$partyBagPriceWorthMin = $this->price_worth;
				}
				break;
			case "surprises":
				$partyBagPriceWorthMin = $this->price;
				if ($this->price_worth && $this->price_worth > $this->price)
				{
					$partyBagPriceWorthMin = $this->price_worth;
				}
				break;
		}

		return $partyBagPriceWorthMin;
	}

	public function getPartyBagPriceWorthMax($typeName)
	{
		$partyBagPriceWorthMax = 0;
		switch ($typeName)
		{
			case "venue-deals":
				$rent_duration = 0;
				$partyBagPriceWorthMax = $this->getPartyBagPriceMax($typeName);
				if ($this->price_worth && $this->price_worth > $this->getPartyBagPriceMax($typeName))
				{
					$partyBagPriceWorthMax = $this->getPartyBagPriceMax($typeName);
				}
				$rentDurationFieldId = TypePackageField::where('name', config('evibe.pkg_fields.keys.rent_duration'))
				                                       ->value('id');

				if ($rentDurationFieldId)
				{
					$rent_duration = PackageFieldValue::where(['package_id' => $this->id, 'type_package_field_id' => $rentDurationFieldId])
					                                  ->value('value');
				}
				if (!$rent_duration)
				{
					if ($this->guestMin)
					{
						if ($this->guestMax && $this->guestMax > $this->guestMin)
						{
							$partyBagPriceWorthMax = $partyBagPriceWorthMax * $this->guestMax;
						}
						else
						{
							$partyBagPriceWorthMax = $partyBagPriceWorthMax * $this->guestMin;
						}
					}
				}
				break;
			case "packages":
				$partyBagPriceWorthMax = $this->price;
				if ($this->price_worth && $this->price_worth > $this->getPartyBagPriceMax($typeName))
				{
					$partyBagPriceWorthMax = $this->price_worth;
				}
				break;
			case "birthday-packages":
				$partyBagPriceWorthMax = $this->price;
				if ($this->price_worth && $this->price_worth > $this->getPartyBagPriceMax($typeName))
				{
					$partyBagPriceWorthMax = $this->price_worth;
				}
				break;
			case "resorts":
				$partyBagPriceWorthMax = $this->price;
				if ($this->price_worth && $this->price_worth > $this->getPartyBagPriceMax($typeName))
				{
					$partyBagPriceWorthMax = $this->price_worth;
				}
				break;
			case "villas":
				$partyBagPriceWorthMax = $this->price;
				if ($this->price_worth && $this->price_worth > $this->getPartyBagPriceMax($typeName))
				{
					$partyBagPriceWorthMax = $this->price_worth;
				}
				break;
			case "lounges":
				$partyBagPriceWorthMax = $this->price;
				if ($this->price_worth && $this->price_worth > $this->getPartyBagPriceMax($typeName))
				{
					$partyBagPriceWorthMax = $this->price_worth;
				}
				break;
			case "food":
				$partyBagPriceWorthMax = $this->price;
				if ($this->price_worth && $this->price_worth > $this->getPartyBagPriceMax($typeName))
				{
					$partyBagPriceWorthMax = $this->price_worth;
				}
				break;
			case "surprises":
				$partyBagPriceWorthMax = $this->price;
				if ($this->price_worth && $this->price_worth > $this->getPartyBagPriceMax($typeName))
				{
					$partyBagPriceWorthMax = $this->price_worth;
				}
				break;
		}

		return $partyBagPriceWorthMax;
	}

	public function getCardInfo($fullUrl, $price, $typeName, $mapTypeId, $occasionId)
	{
		$cardInfo = [
			'id'                  => $this->id,
			'title'               => $this->name ? $this->name : "Package",
			'price'               => $price,
			'profileImg'          => $this->getProfileImg(),
			'fullUrl'             => $fullUrl,
			'priceMax'            => $this->price_max,
			'worth'               => $this->price_worth,
			'additionalLine'      => $this->getAdditionalLine($typeName),
			'rangeInfo'           => $this->getRangeInfo($typeName),
			'pbPriceMin'          => $this->getPartyBagPriceMin($typeName),
			'pbPriceMax'          => $this->getPartyBagPriceMax($typeName),
			'pbPriceWorthMin'     => $this->getPartyBagPriceWorthMin($typeName),
			'pbPriceWorthMax'     => $this->getPartyBagPriceWorthMax($typeName),
			'rating'              => $this->getProviderAvgRating(),
			'additionalPriceLine' => ''
		];

		if ($mapTypeId)
		{
			$cardInfo['mapTypeId'] = $mapTypeId;
		}

		if ($occasionId)
		{
			$cardInfo['occasionId'] = $occasionId;
		}

		return $cardInfo;
	}

	public function getSearchableCardInfo($cityUrl, $occasionUrl, $params)
	{
		$cityUrl = $cityUrl ? $cityUrl : getCityUrl();
		$typeName = array_key_exists("name", $params) ? $params['name'] : "";
		$occasionUrl = $occasionUrl ?: $this->getParentEventUrl();
		$mapTypeId = isset($params['map_type_id']) ? $params['map_type_id'] : null;
		$occasionId = $this->event->id;

		$searchableCardInfo = [];
		$fullUrl = $this->getFullLinkForSearch($cityUrl, $occasionUrl, $typeName);
		$price = $this->price;

		if ($fullUrl && $price)
		{
			$searchableCardInfo = $this->getCardInfo($fullUrl, $price, $typeName, $mapTypeId, $occasionId);
		}

		return $searchableCardInfo;
	}

	public function getParentEventUrl()
	{
		$event = $this->event;

		if (!$event)
		{
			return "";
		}

		if ($event->parent_id)
		{
			$parentEvent = TypeEvent::select('parent_id', 'url')
			                        ->where('id', $event->parent_id)
			                        ->first();

			if ($parentEvent)
			{
				return $parentEvent->url;
			}
		}

		return $event->url;
	}

	public function getFullLinkForSearch($cityUrl, $occasionUrl, $typeName)
	{
		$route = "";
		if (!$this->provider || !$this->provider->city)
		{
			return $route;
		}

		$cityUrl = is_null($cityUrl) ? $this->provider->city->url : $cityUrl;
		$occasionUrl = $occasionUrl ?: $this->event->url;

		switch ($typeName)
		{
			case "venue-deals":
				$occasionRouteName = $this->evibeUtil->occasionUrlRouteMapping[$occasionUrl];
				$route = $this->getLinkByRouteName($occasionRouteName . "venue_deals.profile", [$cityUrl, $this->url]);
				break;
			case "packages":
				$occasionRouteName = $this->evibeUtil->occasionUrlRouteMapping[$occasionUrl];
				$route = $this->getLinkByRouteName($occasionRouteName . "packages.profile", [$cityUrl, $this->url]);
				break;
			case "birthday-packages":
				$occasionRouteName = $this->evibeUtil->occasionUrlRouteMapping[$occasionUrl];
				$route = $this->getLinkByRouteName($occasionRouteName . "packages.profile", [$cityUrl, $this->url]);
				break;
			case "resorts":
				$occasionRouteName = $this->evibeUtil->occasionUrlRouteMapping[$occasionUrl];
				$route = $this->getLinkByRouteName($occasionRouteName . "resort.profile", [$cityUrl, $this->url]);
				break;
			case "villas":
				$occasionRouteName = $this->evibeUtil->occasionUrlRouteMapping[$occasionUrl];
				$route = $this->getLinkByRouteName($occasionRouteName . "villa.profile", [$cityUrl, $this->url]);
				break;
			case "lounges":
				$occasionRouteName = $this->evibeUtil->occasionUrlRouteMapping[$occasionUrl];
				$route = $this->getLinkByRouteName($occasionRouteName . "lounge.profile", [$cityUrl, $this->url]);
				break;
			case "food":
				$occasionRouteName = $this->evibeUtil->occasionUrlRouteMapping[$occasionUrl];
				$route = $this->getLinkByRouteName($occasionRouteName . "food.profile", [$cityUrl, $this->url]);
				break;
			case "surprises":
				if ($this->isCandleLightDinner($this->id))
				{
					$route = route("city.cld.profile", [$cityUrl, $this->url]);
				}
				else
				{
					$occasionRouteName = $this->evibeUtil->occasionUrlRouteMapping[$occasionUrl];
					$route = $this->getLinkByRouteName($occasionRouteName . "package.profile", [$cityUrl, $this->url]);
				}
				break;
			case "tent":
				$occasionRouteName = $this->evibeUtil->occasionUrlRouteMapping[$occasionUrl];
				$route = $this->getLinkByRouteName($occasionRouteName . "tent.profile", [$cityUrl, $this->url]);
				break;
			case "priest":
				$occasionRouteName = $this->evibeUtil->occasionUrlRouteMapping[$occasionUrl];
				$route = $this->getLinkByRouteName($occasionRouteName . "priest.profile", [$cityUrl, $this->url]);
				break;
		}
		return $route;
	}

	public function isCandleLightDinner($packageId)
	{
		$isCLDPackage = PackageTag::where("tile_tag_id", config("evibe.type-tag.candle-light-dinners"))
		                          ->where("planner_package_id", $packageId)
		                          ->first();

		return $isCLDPackage ? true : false;
	}

	public function forSearchIsLive()
	{
		return ($this->is_live == 1 && $this->provider && $this->provider->is_live && $this->event);
	}

	public function forSearchPrice()
	{
		return ($this->price ? $this->price : 0);
	}

	public function forSearchCity($cityId)
	{
		return ($this->provider->city_id == $cityId);
	}

	// for party bag
	public function getShortListableCardInfo($cityUrl, $occasionUrl, $params, $shortlistParams)
	{
		$cityUrl = $cityUrl ? $cityUrl : getCityUrl();
		$typeName = array_key_exists("name", $params) ? $params['name'] : "";
		$occasionUrl = $occasionUrl ? $occasionUrl : $this->event->url;

		$cardInfo = [];
		$fullUrl = $this->getFullLinkForSearch($cityUrl, $occasionUrl, $typeName);
		$price = $this->price;

		if ($fullUrl && $price)
		{
			$cardInfo = $this->getCardInfo($fullUrl, $price, $typeName, null, null);
		}

		$cardInfo = array_merge($cardInfo, $shortlistParams);

		return $cardInfo;
	}

	// for party bag
	public function getCollectionCardInfo($cityUrl, $occasionUrl, $params, $collectionParams)
	{
		$cityUrl = $cityUrl ? $cityUrl : getCityUrl();
		$typeName = array_key_exists("name", $params) ? $params['name'] : "";
		$occasionUrl = $occasionUrl ? $occasionUrl : $this->event->url;
		$occasionId = $this->event->id;

		$collectionCardInfo = [];
		$fullUrl = $this->getFullLinkForSearch($cityUrl, $occasionUrl, $typeName);
		$price = $this->price;

		// hack to redirect options to surprise packages profile pages
		if ($fullUrl && $occasionId == config('evibe.occasion.vd_2018.id'))
		{
			$fullUrl = str_replace("birthday-party-planners", "surprise-planners", $fullUrl);
		}

		if ($fullUrl && $price)
		{
			$collectionCardInfo = $this->getCardInfo($fullUrl, $price, $typeName, null, $occasionId);
		}

		// hack to overwrite occasionId if not existing for the option
		if (!$collectionParams['occasionId'])
		{
			$collectionParams['occasionId'] = $occasionId;
		}

		$collectionCardInfo = array_merge($collectionCardInfo, $collectionParams);

		return $collectionCardInfo;
	}

	public function isRequiresBookingImage($campaignUrl)
	{
		$packageTagIds = $this->tags()->select("type_tags.id")->pluck("id")->toArray();
		$uploadTagId = config("evibe.occasion.$campaignUrl.is_image_tag_id");

		return in_array($uploadTagId, $packageTagIds) ? 1 : 0;
	}
}