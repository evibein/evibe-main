<?php

namespace App\Models\Package;

use App\Models\BaseModel;

class PackageFieldValue extends BaseModel
{
	protected $table = 'package_field_value';
}