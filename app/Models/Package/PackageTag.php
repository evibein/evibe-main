<?php

namespace App\Models\Package;

use App\Models\BaseModel;

class PackageTag extends BaseModel
{
	protected $table = 'planner_package_tags';
}