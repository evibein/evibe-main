<?php

/**
 * @author Anji <anji@evibe.in>
 * @since  11 Sep 2015
 */

namespace App\Models\Decor;

use App\Models\BaseModel;
use App\Models\Types\TypeTag;

class DecorTag extends BaseModel
{
	protected $table = 'decor_tags';

	public function tag()
	{
		return $this->belongsTo(TypeTag::class, 'tag_id');
	}
}