<?php

/**
 * @author Anji <anji@evibe.in>
 * @since  11 Sep 2015
 */

namespace App\Models\Decor;

use App\Models\BaseModel;

class DecorReview extends BaseModel
{
	protected $table = 'decor_review';
}