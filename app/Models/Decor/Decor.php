<?php

/**
 * @author Anji <anji@evibe.in>
 * @since  11 Sep 2015
 */

namespace App\Models\Decor;

use App\Evibe\EvibeTraits\EvibeSearchable;
use App\Evibe\EvibeTraits\EvibeShortListable;
use App\Models\BaseModel;
use App\Models\Types\TypeTag;
use App\Models\Vendor\Vendor;
use App\Models\Vendor\VendorReview;

use App\Models\Util\City;

class Decor extends BaseModel
{
	use EvibeSearchable, EvibeShortListable;

	protected $table = 'decor';

	public function provider()
	{
		return $this->belongsTo(Vendor::class, 'provider_id');
	}

	public function tags()
	{
		return $this->belongsToMany(TypeTag::class, 'decor_tags', 'decor_id', 'tag_id')
		            ->wherePivot('deleted_at', null);
	}

	public function gallery()
	{
		return $this->hasMany(DecorGallery::class, 'decor_id');
	}

	public function reviews()
	{
		return $this->hasMany(DecorReview::class, 'decor_id');
	}

	public function scopeForEvent($query, $eventId)
	{
		return $query->join('decor_event', 'decor_event.decor_id', '=', 'decor.id')
		             ->whereNull('decor_event.deleted_at')
		             ->where('decor_event.event_id', $eventId);
	}

	public function scopeJoinTags($query)
	{
		return $query->join('decor_tags', 'decor_tags.decor_id', '=', 'decor.id')
		             ->whereNull('decor_tags.deleted_at');
	}

	public function scopeHasTags($query, $tagIds)
	{
		return $query->joinTags()
		             ->whereIn('decor_tags.tag_id', $tagIds);
	}

	public function scopeIsLive($query)
	{
		return $query->where('decor.is_live', 1);
	}

	// fetch the decors for the particular city
	// decor->provider->city_id = City.url.id
	// @todo: can be improved much with the city sql query
	public function scopeForCity($query, $cityId = null)
	{
		if (!$cityId)
		{
			$cityUrl = getCityUrl();
			$city = City::where('url', $cityUrl)->first();
			$cityId = $city ? $city->id : 1;  // set the default to bangalore
		}

		return $query->join('planner', 'planner.id', '=', 'decor.provider_id')
		             ->where('planner.city_id', $cityId);
	}

	public function scopeForSearchSelectCols($query)
	{
		return $query->select("id", "name", "info", "is_live", "url", "provider_id",
		                      "min_price", "max_price", "worth", "range_info");

	}

	public function getProfileImg()
	{
		$profileImgUrl = config('evibe.gallery.host');
		$profileImgObj = DecorGallery::where('decor_id', $this->id)
		                             ->where('type_id', config('evibe.gallery.type.image'))
		                             ->orderBy('is_profile', 'DESC')
		                             ->orderBy('created_at', 'DESC')
		                             ->first();

		if ($profileImgObj)
		{
			$profileImgUrl .= '/decors/' . $this->id . '/images/results/' . $profileImgObj->url;
		}

		return $profileImgUrl;
	}

	public function getProviderAvgRating()
	{
		return $this->provider->avgRating();
	}

	public function getLink($occasionUrl, $cityUrl = null)
	{
		$host = config('evibe.host');
		$profileUrl = config('evibe.profile_url.decors');
		if (is_null($cityUrl))
		{
			$cityUrl = $this->provider->city->url;
		}

		return $host . '/' . $cityUrl . '/' . $occasionUrl . '/' . $profileUrl . '/' . $this->url;
	}

	public function getReviews($count = -1, $skip = -1)
	{
		$count = ($count != -1) ? $count : PHP_INT_MAX;
		$totalRating = 0;
		$reviews = ['count' => 0, 'reviews' => [], 'avgRating' => 0];
		$countObj = $reviewsObj = VendorReview::where('map_type_id', config('evibe.ticket.type.decor'))
		                                      ->where('map_id', $this->id)
		                                      ->orderBy('created_at', 'DESC');

		if ($skip != -1)
		{
			$reviewsObj->skip($skip);
		}
		$reviewsObj->take($count)->get();

		foreach ($reviewsObj as $decorReview)
		{
			$reviewReply = '';
			if ($decorReview->reply)
			{
				$reviewReply = $decorReview->reply->reply;
			}

			array_push($reviews['reviews'], [
				'name'   => $decorReview->name,
				'review' => $decorReview->review,
				'email'  => $decorReview->email,
				'phone'  => $decorReview->phone,
				'rating' => $decorReview->rating,
				'reply'  => $reviewReply
			]);

			$totalRating += $decorReview->rating;
		}

		$reviews['totalCount'] = $countObj->count();
		if ($count)
		{
			$reviews['avgRating'] = round($totalRating / $count, 2);
		}

		return $reviews;
	}

	public function getTags()
	{
		$tags = [];
		$decorTags = $this->tags;

		foreach ($decorTags as $decorTag)
		{
			if ($decorTag->name)
			{
				$tags[$decorTag->id] = $decorTag->name;
			}
		}

		return $tags;
	}

	public function getSimilarStyles($eventId = null)
	{
		$currentDecorTags = DecorTag::where('decor_id', '=', $this->id)->pluck('tag_id');
		$areaId = $this->provider->area_id;
		$eventIds = [config('evibe.occasion.kids_birthdays.id'), config('evibe.occasion.pre-post.id')];
		if ($eventId)
		{
			$eventIds = [$eventId];
		}

		$similarDecors = Decor::join('decor_tags', 'decor_tags.decor_id', '=', 'decor.id')
		                      ->join('planner', 'planner.id', '=', 'decor.provider_id')
		                      ->join('decor_event', 'decor_event.decor_id', '=', 'decor.id')
		                      ->whereNull('decor_tags.deleted_at')
		                      ->whereNull('planner.deleted_at')
		                      ->whereNull('decor_event.deleted_at')
		                      ->where('decor.id', '!=', $this->id)
		                      ->whereIn('decor_event.event_id', $eventIds)
		                      ->where(function ($tagQuery) use ($currentDecorTags, $areaId) {
			                      $tagQuery->whereIn('decor_tags.tag_id', $currentDecorTags)
			                               ->orWhere('planner.area_id', '=', $areaId);
		                      })
		                      ->orderBy('decor.name', 'ASC')
		                      ->select('decor.*')->distinct()->take(3)->get();

		return $similarDecors;
	}

	public function getPriceWorth()
	{
		return $this->worth;
	}

	public function event()
	{
		return $this->hasMany(DecorEvent::class, 'decor_id');
	}

	public function getPartyBagPriceMin()
	{
		return $this->min_price;
	}

	public function getPartyBagPriceMax()
	{
		if ($this->max_price && $this->max_price > $this->min_price)
		{
			return $this->max_price;
		}

		return $this->min_price;
	}

	public function getPartyBagPriceWorthMin()
	{
		if ($this->worth && $this->worth > $this->min_price)
		{
			return $this->worth;
		}

		return $this->min_price;
	}

	public function getPartyBagPriceWorthMax()
	{
		if ($this->worth && $this->worth > $this->getPartyBagPriceMax())
		{
			return $this->worth;
		}

		return $this->getPartyBagPriceMax();
	}

	public function getCardInfo($fullUrl, $price, $mapTypeId, $occasionId)
	{
		$cardInfo = [
			'id'                  => $this->id,
			'title'               => $this->name ? $this->name : "Decor",
			'price'               => $price,
			'profileImg'          => $this->getProfileImg(),
			'fullUrl'             => $fullUrl,
			'additionalLine'      => '',
			'priceMax'            => $this->max_price,
			'worth'               => $this->worth,
			'rangeInfo'           => $this->range_info,
			'pbPriceMin'          => $this->getPartyBagPriceMin(),
			'pbPriceMax'          => $this->getPartyBagPriceMax(),
			'pbPriceWorthMin'     => $this->getPartyBagPriceWorthMin(),
			'pbPriceWorthMax'     => $this->getPartyBagPriceWorthMax(),
			'rating'              => $this->getProviderAvgRating(),
			'additionalPriceLine' => ''
		];

		if ($mapTypeId)
		{
			$cardInfo['mapTypeId'] = $mapTypeId;
		}

		if ($occasionId)
		{
			$cardInfo['occasionId'] = $occasionId;
		}

		return $cardInfo;
	}

	public function getOccasionIdForSearch($occasionUrl)
	{
		if (!$occasionUrl)
		{
			$firstEvent = $this->event->first();
			if ($firstEvent)
			{
				return $firstEvent->event->id;
			}
		}

		return null;
	}

	public function getSearchableCardInfo($cityUrl, $occasionUrl, $params)
	{
		$searchableCardInfo = [];
		$fullUrl = $this->getFullLinkForSearch($cityUrl, $occasionUrl);
		$occasionId = $this->getOccasionIdForSearch($occasionUrl);
		$price = $this->min_price;
		$mapTypeId = isset($params['map_type_id']) ? $params['map_type_id'] : null;

		if ($fullUrl && $price)
		{
			$searchableCardInfo = $this->getCardInfo($fullUrl, $price, $mapTypeId, $occasionId);
		}

		return $searchableCardInfo;
	}

	public function getFullLinkForSearch($cityUrl, $occasionUrl)
	{
		// check if occasion url is empty, get first occasion url
		if (!$occasionUrl || is_null($occasionUrl))
		{
			$firstEvent = $this->event ? $this->event->first() : null;
			$occasionUrl = ($firstEvent && $firstEvent->event) ? $firstEvent->event->url : config('evibe.occasion.kids_birthdays.url');
		}

		$link = "";
		if (isset($this->evibeUtil->occasionUrlRouteMapping[$occasionUrl]))
		{
			$occasionRouteName = $this->evibeUtil->occasionUrlRouteMapping[$occasionUrl];
			$link = $this->getLinkByRouteName($occasionRouteName . "decors.profile", [$cityUrl, $this->url]);
		}

		return $link;
	}

	public function forSearchIsLive()
	{
		return ($this->is_live && $this->provider && $this->provider->is_live && $this->event && $this->event->count());
	}

	public function forSearchPrice()
	{
		return ($this->min_price ? $this->min_price : 0);
	}

	public function forSearchCity($cityId)
	{
		return ($this->provider->city_id == $cityId);
	}

	// for party bag
	public function getShortListableCardInfo($cityUrl, $occasionUrl, $params, $shortlistParams)
	{
		$cardInfo = [];
		$fullUrl = $this->getFullLinkForSearch($cityUrl, $occasionUrl);
		$price = $this->min_price;

		if ($fullUrl && $price)
		{
			$cardInfo = $this->getCardInfo($fullUrl, $price, null, null);
		}

		$cardInfo = array_merge($cardInfo, $shortlistParams);

		return $cardInfo;
	}

	// for collections
	public function getCollectionCardInfo($cityUrl, $occasionUrl, $params, $collectionParams)
	{
		$collectionCardInfo = [];
		$fullUrl = $this->getFullLinkForSearch($cityUrl, $occasionUrl);
		$price = $this->min_price;
		$occasionId = null;
		if (!$occasionUrl)
		{
			$firstEvent = $this->event->first();
			if ($firstEvent)
			{
				$occasionId = $firstEvent->event->id;
			}
		}

		if ($fullUrl && $price)
		{
			$collectionCardInfo = $this->getCardInfo($fullUrl, $price, null, $occasionId);
		}

		if (!$collectionParams['occasionId'])
		{
			$collectionParams['occasionId'] = $occasionId;
		}

		$collectionCardInfo = array_merge($collectionCardInfo, $collectionParams);

		return $collectionCardInfo;
	}
}
