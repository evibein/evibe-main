<?php

namespace App\Models\Decor;

use App\Models\BaseModel;
use App\Models\Types\TypeEvent;

class DecorEvent extends BaseModel
{
    protected $table = 'decor_event';

    public function event()
    {
        return $this->belongsTo(TypeEvent::class, 'event_id');
    }
}
