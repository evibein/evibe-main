<?php

/**
 * @author Anji <anji@evibe.in>
 * @since  11 Sep 2015
 */

namespace App\Models\Decor;

use App\Models\BaseModel;
use Illuminate\Support\Facades\Log;

class DecorGallery extends BaseModel
{
	protected $table = 'decor_gallery';

	public function decor()
	{
		return $this->belongsTo(Decor::class, 'decor_id');
	}

	public function getPath($type = null)
	{
		$decor = $this->decor;

		if ($this->type_id == config('evibe.gallery.type.video'))
		{
			$path = '';
			$videoUrl = $this->validateYouTubeLink($this->url);

			if ($videoUrl)
			{
				if ($type == 'thumbs')
				{
					$path = $videoUrl->thumbnail_url;
				}
				else
				{
					$path = $this->getVideoWithCustomOption($this->url);
				}
			}

		}
		else
		{
			$path = config('evibe.gallery.host');
			$path .= '/decors/' . $decor->id . '/images/';
			if ($type)
			{
				$path .= $type . '/'; // results / profile / thumbs
			}
			$path .= $this->url;
		}

		return $path;
	}
}