<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class VendorSignUp extends BaseModel
{
	protected $table = 'vendor_signup';

	// Mutators
	public function setPersonAttribute($value)
	{
		$this->attributes['person'] = $this->getFormattedName($value);
	}

	public function getPersonAttribute($value)
	{
		return $this->getFormattedName($value);
	}

	public function setNameAttribute($value)
	{
		$this->attributes['name'] = $this->getFormattedName($value);
	}

	public function getNameAttribute($value)
	{
		return $this->getFormattedName($value);
	}
}