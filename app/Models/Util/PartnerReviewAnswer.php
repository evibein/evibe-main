<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class PartnerReviewAnswer extends BaseModel
{
	protected $table = 'partner_review_answer';

	public function question()
	{
		return $this->belongsTo(PartnerReviewQuestion::class, 'question_id');
	}
}