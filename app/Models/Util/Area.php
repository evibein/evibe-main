<?php

namespace App\Models\Util;

use App\Models\BaseModel;
use App\Models\Vendor\Vendor;

class Area extends BaseModel
{
	protected $table = 'area';

	public function planners()
	{
		return $this->hasMany(Vendor::class, 'area_id');
	}
}
