<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class CampaignUser extends BaseModel
{
	protected $table = 'campaign_user';

	public function getPosterUrl()
	{
		return config('evibe.gallery.host') . $this->getDirectory() . $this->poster_url;
	}

	public function getPosterPath()
	{
		return config('evibe.gallery.root') . $this->getDirectory() . $this->poster_url;
	}

	private function getDirectory()
	{
		return '/santa-posters/posters/dec' . (int)date("d", strtotime($this->taken_at)) . '/';
	}
}