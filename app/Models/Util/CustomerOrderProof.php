<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class CustomerOrderProof extends BaseModel
{
	protected $table = 'customer_order_proofs';

	public function getImageBasePath()
	{
		$ticketId = $this->ticket_id;

		$imagePath = config('evibe.gallery.host') . '/ticket/' . $ticketId . '/customer-proofs/';

		return $imagePath;
	}

	public function getOriginalImagePath()
	{
		if ($this->type == 1)
		{
			return $this->url;
		}

		return $this->getImageBasePath() . $this->url;
	}

	public function getResultsImagePath()
	{
		if ($this->type == 1)
		{
			return $this->url;
		}

		return $this->getImageBasePath() . 'results/' . $this->url;
	}
}