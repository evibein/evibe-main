<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;

class AccessToken extends Model
{
	protected $table = 'access_tokens';
	protected $guarded = [];
	public static $rules = [];
}
