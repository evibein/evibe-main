<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class PostalPinCode extends BaseModel
{
	protected $table = 'postal_pin_code';
}