<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class AutoBooking extends BaseModel
{
	protected $table = 'auto_booking';

	public function isAutoBookable()
	{
		return $this->morphTo(null, 'map_type', 'map_id');
	}
}