<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class DeliverySlot extends BaseModel
{
	protected $table = 'type_delivery_slot';

	public function formattedSlot()
	{
		$slot = $this->start_time . ' - ' . $this->end_time;

		return strtoupper($slot);
	}
}
