<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class VendorStory extends BaseModel
{
	protected $table = 'stories_vendor';

	public function provider()
	{
		return $this->morphTo(null, 'map_type_id', 'map_id');
	}
}
