<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class TicketReviewQuestionOption extends BaseModel
{
	protected $table = 'evibe_review_question_option';

	public function question()
	{
		return $this->belongsTo(TicketReviewQuestion::class, 'question_id');
	}
}
