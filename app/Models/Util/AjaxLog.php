<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class AjaxLog extends BaseModel
{
	protected $table = 'log_ajax';
}