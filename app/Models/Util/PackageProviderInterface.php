<?php

namespace App\Models\Util;

interface PackageProviderInterface
{
	public function city();

	public function area();

	public function gallery();

	public function type();

	public function avgRating();

	public function packages();

	public function getLink();

	public function getDirectReviews($count = -1, $skip = -1);
}