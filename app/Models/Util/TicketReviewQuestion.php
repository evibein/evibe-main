<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class TicketReviewQuestion extends BaseModel
{
	protected $table = 'evibe_review_question';

	public function options()
	{
		return $this->hasMany(TicketReviewQuestionOption::class, 'question_id');
	}

	public function stringifyAnswer($reviewId)
	{
		$options = $this->options;
		$string = "";
		foreach ($options as $option)
		{
			$answer = TicketReviewQuestionAnswer::where('review_id', $reviewId)->where('option_id', $option->id)->count();
			if ($answer)
			{
				$string .= $option->text . ', ';
			}
		}

		return rtrim($string, ', ');
	}
}
