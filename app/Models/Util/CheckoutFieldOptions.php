<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class CheckoutFieldOptions extends BaseModel
{
	protected $table = 'checkout_field_options';
}