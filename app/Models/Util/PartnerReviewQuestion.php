<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class PartnerReviewQuestion extends BaseModel
{
	protected $table = 'type_partner_review_question';
}
