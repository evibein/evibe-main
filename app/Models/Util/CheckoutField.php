<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class CheckoutField extends BaseModel
{
	protected $table = 'checkout_field';

	public function getCheckoutValue($ticketId, $ticketBookingId = null)
	{
		$checkoutValue = CheckoutFieldValue::where([
			                                           'ticket_id'         => $ticketId,
			                                           'checkout_field_id' => $this->id,
		                                           ]);
		if ($ticketBookingId)
		{
			$checkoutValue = $checkoutValue->where('ticket_booking_id', $ticketBookingId);
		}

		$checkoutValue = $checkoutValue->first();

		if ($checkoutValue)
		{
			return $checkoutValue->value;
		}
		else
		{
			// @todo: remove on 01 Nov 2019 (Implemented on 31 Aug 2019)
			$checkoutValue = CheckoutFieldValue::where([
				                                           'ticket_id'         => $ticketId,
				                                           'checkout_field_id' => $this->id,
			                                           ])->first();
			if ($checkoutValue)
			{
				return $checkoutValue->value;
			}
		}
	}

	public function checkoutOptions()
	{
		return $this->hasMany(CheckoutFieldOptions::class, 'checkout_field_id');
	}
}
