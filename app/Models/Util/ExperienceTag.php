<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class ExperienceTag extends BaseModel
{
	protected $table = 'experience_tags';
}
