<?php

namespace App\Models\Util;

use App\Models\BaseModel;
use App\Models\Types\TypeEvent;

class CustomerStory extends BaseModel
{
	protected $table = 'stories_customer';

	public function scopeForCity($query, $cityIds)
	{
		if (is_array($cityIds))
		{
			return $query->whereIn('stories_customer.city_id', $cityIds);
		}

		return $query->where('stories_customer.city_id', '=', $cityIds);
	}

	public function city()
	{
		return $this->belongsTo(City::class, 'city_id');
	}

	public function event()
	{
		return $this->belongsTo(TypeEvent::class, 'event_id');
	}
}
