<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class OptionAvailability extends BaseModel
{
	protected $table = 'option_availability';
}