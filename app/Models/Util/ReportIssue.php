<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class ReportIssue extends BaseModel
{
	protected $table = 'report_issue';
}