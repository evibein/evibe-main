<?php

namespace App\Models\Util;

use App\Models\BaseModel;
use App\Models\Vendor\Vendor;

class City extends BaseModel
{
	protected $table = 'city';

	public function planners()
	{
		return $this->hasMany(Vendor::class, 'city_id');
	}
}
