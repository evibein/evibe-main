<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;

class AuthClient extends Model
{
	protected $table = 'auth_clients';
	protected $guarded = [];
	public static $rules = [];
}
