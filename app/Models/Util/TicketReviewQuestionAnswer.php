<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class TicketReviewQuestionAnswer extends BaseModel
{
	protected $table = 'evibe_review_question_answer';
}
