<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class DynamicProductParams extends BaseModel
{
	protected $table = 'dynamic_product_params';
}