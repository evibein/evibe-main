<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class SiteErrorLog extends BaseModel
{
	protected $table = 'log_site_errors';
}