<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class ProductVariation extends BaseModel
{
	protected $table = 'product_variation';
}