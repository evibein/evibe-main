<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class CheckoutFieldValue extends BaseModel
{
	protected $table = 'ticket_checkout_field_value';
}
