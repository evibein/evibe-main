<?php

namespace App\Models\Util;

use Illuminate\Database\Eloquent\Model;

class UserAction extends Model
{
	protected $table = 'user_action';
}
