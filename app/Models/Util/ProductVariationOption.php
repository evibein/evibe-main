<?php

namespace App\Models\Util;

use App\Models\BaseModel;

class ProductVariationOption extends BaseModel
{
	protected $table = 'product_variation_option';
}