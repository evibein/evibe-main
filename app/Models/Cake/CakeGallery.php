<?php

/**
 * @author Anji <anji@evibe.in>
 * @since  14 May 2015
 */

namespace App\Models\Cake;

use App\Models\BaseModel;

class CakeGallery extends BaseModel
{
	protected $table = 'cake_gallery';

	public function cake()
	{
		return $this->belongsTo(Cake::class, 'cake_id');
	}

	public function getLink($type = null)
	{
		$cake = $this->cake;
		$provider = $cake ? $cake->provider : null;
		$imgLink = $this->getGalleryBaseUrl();

		if ($cake && $provider)
		{
			$imgLink .= '/cakes/' . $cake->id . '/';
			$imgLink .= $type ? "$type/" : ""; // results / profile / thumbs
			$imgLink .= $this->url;
		}

		return $imgLink;
	}
}