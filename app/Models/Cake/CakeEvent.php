<?php

namespace App\Models\Cake;

use App\Models\BaseModel;
use App\Models\Types\TypeEvent;

class CakeEvent extends BaseModel
{
	protected $table = "cake_event";

	public function event()
	{
		return $this->belongsTo(TypeEvent::class, 'event_id');
	}
}
