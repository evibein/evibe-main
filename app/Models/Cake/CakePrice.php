<?php

/**
 * @author Anji <anji@evibe.in>
 * @since  15 May 2015
 */

namespace App\Models\Cake;

use App\Models\BaseModel;

class CakePrice extends BaseModel
{
	protected $table = 'cake_price';

	public function cake()
	{
		return $this->belongsTo(Cake::class, 'cake_id');
	}
}