<?php

/**
 * @author Anji <anji@evibe.in>
 * @since  26 May 2015
 */

namespace App\Models\Cake;

use App\Models\BaseModel;

class CakeReview extends BaseModel
{
	protected $table = 'cake_reviews';

	public function cake()
	{
		return $this->belongsTo(Cake::class, 'cake_id');
	}
}