<?php

/**
 * @author Anji <anji@evibe.in>
 * @since  14 May 2015
 */

namespace App\Models\Cake;

use App\Models\BaseModel;
use App\Models\Types\TypeTag;

class CakeTags extends BaseModel
{
	protected $table = 'cake_tags';

	public function cake()
	{
		return $this->belongsTo(Cake::class, 'cake_id');
	}

	public function tag()
	{
		return $this->belongsTo(TypeTag::class, 'tag_id');
	}
}