<?php

namespace App\Models\Cake;

use App\Models\BaseModel;
use App\Models\Types\TypeCakeCategory;
use App\Models\Types\TypeCakeCategoryValue;

class CakeFieldValues extends BaseModel
{
	protected $table = 'cake_field_values';

	public function cakeCategory()
	{
		return $this->belongsTo(TypeCakeCategory::class, 'cake_cat_id');
	}

	public function catValue()
	{
		return $this->belongsTo(TypeCakeCategoryValue::class, 'cake_cat_value_id');
	}
}
