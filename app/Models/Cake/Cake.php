<?php

/**
 * @author Anji <anji@evibe.in>
 * @since  14 May 2015
 */

namespace App\Models\Cake;

use App\Evibe\EvibeTraits\EvibeSearchable;
use App\Evibe\EvibeTraits\EvibeShortListable;
use App\Models\BaseModel;
use App\Models\Types\TypeCakeFlavour;
use App\Models\Types\TypeTag;
use App\Models\Vendor\Vendor;
use App\Models\Util\City;

class Cake extends BaseModel
{
	use EvibeSearchable, EvibeShortListable;

	protected $table = 'cake';

	public function tags()
	{
		return $this->belongsToMany(TypeTag::class, 'cake_tags', 'cake_id', 'tag_id')
		            ->wherePivot('deleted_at', null);
	}

	public function gallery()
	{
		return $this->hasMany(CakeGallery::class, 'cake_id');
	}

	public function prices()
	{
		return $this->hasMany(CakePrice::class, 'cake_id');
	}

	public function provider()
	{
		return $this->belongsTo(Vendor::class, 'provider_id');
	}

	public function recommendedFlavour()
	{
		return $this->belongsTo(TypeCakeFlavour::class, 'flavour_id');
	}

	public function scopeForEvent($query, $eventId)
	{
		return $query->join('cake_event', 'cake_event.cake_id', '=', 'cake.id')
		             ->whereNull('cake_event.deleted_at')
		             ->where('cake_event.event_id', $eventId);
	}

	public function scopeForTags($query)
	{
		return $query->join('cake_tags', 'cake_tags.cake_id', '=', 'cake.id')
		             ->whereNull('cake_tags.deleted_at');
	}

	public function scopeIsLive($query)
	{
		return $query->where('cake.is_live', 1);
	}

	// fetch the cakes for the particular city
	// cake->provider->city_id = City.url.id
	// can be improved much with the city sql query
	public function scopeForCity($query)
	{
		$cityUrl = getCityUrl();
		$city = City::where('city.url', $cityUrl)->first();
		// @todo: alert the incorrect cityUrl
		$cityId = ($city && $city->id) ? $city->id : 1;  // set the default to bangalore

		return $query->join('planner', 'planner.id', '=', 'cake.provider_id')
		             ->where('planner.city_id', $cityId);
	}

	public function scopeForSearchSelectCols($query)
	{
		return $query->select("id", "title", "info", "price_per_kg", "min_order",
		                      "price", "url", "is_live", "price_worth", "provider_id",
		                      "min_order", "flavour_id");
	}

	public function getLink($occasionUrl, $cityUrl = null)
	{
		$host = config('evibe.host');
		$profileUrl = config('evibe.profile_url.cakes');

		if (is_null($cityUrl))
		{
			$cityUrl = $this->provider->city->url;
		}

		if (is_null($occasionUrl))
		{
			return $host . '/' . $cityUrl . '/' . $profileUrl . '/' . $this->url;
		}

		return $host . '/' . $cityUrl . '/' . $occasionUrl . '/' . $profileUrl . '/' . $this->url;
	}

	public function getPriceWorth()
	{
		$worth = $this->price;
		$cakePrices = CakePrice::where('cake_id', $this->id)
		                       ->orderBy('price', 'ASC')
		                       ->first();

		if ($cakePrices)
		{
			$worth = $cakePrices->worth;
		}

		return $worth;
	}

	public function getProfileImg()
	{
		$path = $this->getGalleryBaseUrl() . '/img/app/default_cake.png';
		$profileImgObj = CakeGallery::where('cake_id', $this->id)
		                            ->where('type', config('evibe.gallery.type.image'))
		                            ->orderBy('is_profile', 'DESC')
		                            ->orderBy('updated_at', 'DESC')
		                            ->first();

		if ($profileImgObj)
		{
			$path = $profileImgObj->getLink("results");
		}

		return $path;
	}

	public function event()
	{
		return $this->hasMany(CakeEvent::class, 'cake_id');
	}

	public function getPartyBagPriceMin()
	{
		if ($this->base_price)
		{
			return $this->base_price;
		}
		else
		{
			if ($this->price_per_kg && $this->min_order)
			{
				return $this->price_per_kg * $this->min_order;
			}
			else
			{
				return $this->price;
			}
		}
	}

	public function getPartyBagPriceMax()
	{
		if ($this->base_price)
		{
			return $this->base_price;
		}
		else
		{
			if ($this->price_per_kg && $this->min_order)
			{
				return $this->price_per_kg * $this->min_order;
			}
			else
			{
				return $this->price;
			}
		}
	}

	public function getPartyBagPriceWorthMin()
	{
		if ($this->price_worth && $this->price_worth > $this->getPartyBagPriceMin())
		{
			return $this->price_worth;
		}

		return $this->getPartyBagPriceMin();
	}

	public function getPartyBagPriceWorthMax()
	{
		if ($this->price_worth && $this->price_worth > $this->getPartyBagPriceMax())
		{
			return $this->price_worth;
		}

		return $this->getPartyBagPriceMax();
	}

	public function getCardInfo($fullUrl, $pricePerKg, $mapTypeId, $occasionId)
	{
		$cardInfo = [
			'id'                  => $this->id,
			'title'               => $this->title ? $this->title : "Cake",
			'price'               => $pricePerKg,
			'profileImg'          => $this->getProfileImg(),
			'fullUrl'             => $fullUrl,
			'additionalLine'      => $this->getAdditionalLine(),
			'priceMax'            => '',
			'worth'               => '',
			'rangeInfo'           => '',
			'pbPriceMin'          => $this->getPartyBagPriceMin(),
			'pbPriceMax'          => $this->getPartyBagPriceMax(),
			'pbPriceWorthMin'     => $this->getPartyBagPriceWorthMin(),
			'pbPriceWorthMax'     => $this->getPartyBagPriceWorthMax(),
			'rating'              => '',
			'additionalPriceLine' => $this->getAdditionaPriceLine()
		];

		if ($mapTypeId)
		{
			$cardInfo['mapTypeId'] = $mapTypeId;
		}

		if ($occasionId)
		{
			$cardInfo['occasionId'] = $occasionId;
		}

		return $cardInfo;
	}

	public function getOccasionIdForSearch($occasionUrl)
	{
		if (!$occasionUrl)
		{
			$firstEvent = $this->event->first();

			if ($firstEvent)
			{
				return $firstEvent->event->id;
			}
		}

		return null;
	}

	public function getSearchableCardInfo($cityUrl, $occasionUrl, $params)
	{
		$searchableCardInfo = [];
		$fullUrl = $this->getFullLinkForSearch($cityUrl, $occasionUrl);
		$occasionId = $this->getOccasionIdForSearch($occasionUrl);
		$pricePerKg = $this->price_per_kg;
		$mapTypeId = isset($params['map_type_id']) ? $params['map_type_id'] : null;

		if ($fullUrl && $pricePerKg)
		{
			$searchableCardInfo = $this->getCardInfo($fullUrl, $pricePerKg, $mapTypeId, $occasionId);
		}

		return $searchableCardInfo;
	}

	public function getAdditionalLine()
	{
		return 'Minimum Order: ' . $this->min_order . ' KG';
	}

	public function getAdditionaPriceLine()
	{
		return '/ KG';
	}

	public function getFullLinkForSearch($cityUrl, $occasionUrl)
	{
		// check if occasion url is empty, get first occasion url
		if (!$occasionUrl || is_null($occasionUrl))
		{
			$firstEvent = $this->event ? $this->event->first() : null;
			$occasionUrl = ($firstEvent && $firstEvent->event) ? $firstEvent->event->url : config('evibe.occasion.kids_birthdays.url');
		}

		/**
		 * @todo  : this is quick fix, need to remove it
		 *
		 * since couple experiences does not have cakes category
		 * hard coding it to birthdays
		 *
		 * @since : 13 May 2017
		 * @author: Anji <anji@evibe.in>
		 */
		if ($occasionUrl == config('evibe.occasion.surprises.url'))
		{
			$occasionUrl = config('evibe.occasion.kids_birthdays.url');
		}

		$link = "";
		if (isset($this->evibeUtil->occasionUrlRouteMapping[$occasionUrl]))
		{
			$occasionRouteName = $this->evibeUtil->occasionUrlRouteMapping[$occasionUrl];
			$link = $this->getLinkByRouteName($occasionRouteName . "cakes.profile", [$cityUrl, $this->url]);
		}

		return $link;
	}

	public function forSearchIsLive()
	{
		// check if cake is live, provider is live, cake has at least one event mapped
		return ($this->is_live && $this->provider && $this->provider->is_live && $this->event && $this->event->count());
	}

	public function forSearchPrice()
	{
		return ($this->price ? $this->price : 0);
	}

	public function forSearchCity($cityId)
	{
		return ($this->provider->city_id == $cityId);
	}

	// for party bag
	public function getShortListableCardInfo($cityUrl, $occasionUrl, $params, $shortlistParams)
	{
		$cardInfo = [];
		$fullUrl = $this->getFullLinkForSearch($cityUrl, $occasionUrl);
		$pricePerKg = $this->price_per_kg;

		if ($fullUrl && $pricePerKg)
		{
			$cardInfo = $this->getCardInfo($fullUrl, $pricePerKg, null, null);
		}

		$cardInfo = array_merge($cardInfo, $shortlistParams);

		return $cardInfo;
	}

	// for collections
	public function getCollectionCardInfo($cityUrl, $occasionUrl, $params, $collectionParams)
	{
		$collectionCardInfo = [];
		$fullUrl = $this->getFullLinkForSearch($cityUrl, $occasionUrl);
		$pricePerKg = $this->price_per_kg;
		$occasionId = null;
		if (!$occasionUrl)
		{
			$firstEvent = $this->event->first();
			if ($firstEvent)
			{
				$occasionId = $firstEvent->event->id;
			}
		}

		if ($fullUrl && $pricePerKg)
		{
			$collectionCardInfo = $this->getCardInfo($fullUrl, $pricePerKg, null, $occasionId);
		}

		if (!$collectionParams['occasionId'])
		{
			$collectionParams['occasionId'] = $occasionId;
		}

		$collectionCardInfo = array_merge($collectionCardInfo, $collectionParams);

		return $collectionCardInfo;
	}
}