<?php

namespace App\Models\OptionReview;

use App\Models\BaseModel;

class OptionReviewGallery extends BaseModel
{
	protected $table = 'option_review_gallery';
}