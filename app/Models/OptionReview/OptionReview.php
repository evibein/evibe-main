<?php

namespace App\Models\OptionReview;

use App\Models\BaseModel;

class OptionReview extends BaseModel
{
	protected $table = 'option_review';
}