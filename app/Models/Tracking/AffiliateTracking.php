<?php

namespace App\Models\Tracking;

use App\Models\BaseModel;

class AffiliateTracking extends BaseModel
{
	protected $table = 'affiliate_tracking';
}