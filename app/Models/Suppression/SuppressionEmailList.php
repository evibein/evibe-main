<?php

namespace App\Models\Suppression;

use App\Models\BaseModel;

class SuppressionEmailList extends BaseModel
{
	protected $table = 'suppression_email_list';
}