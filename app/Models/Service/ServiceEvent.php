<?php

namespace App\Models;

use App\Models\Types\TypeEvent;
use App\Models\Types\TypeServices;

class ServiceEvent extends BaseModel
{
	protected $table = 'service_event';

	public function event()
	{
		return $this->belongsTo(TypeEvent::class, 'type_event_id');
	}

	public function services()
	{
		return $this->hasMany(TypeServices::class, 'id');
	}

}
