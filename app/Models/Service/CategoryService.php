<?php

namespace App\Models\Service;

use App\Models\BaseModel;

class CategoryService extends BaseModel
{
	protected $table = 'category_planner_services';
}
