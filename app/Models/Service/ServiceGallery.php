<?php

namespace App\Models\Service;

use App\Models\BaseModel;
use App\Models\Types\TypeServices;
use Illuminate\Support\Facades\Log;

class ServiceGallery extends BaseModel
{
	protected $table = 'service_gallery';

	public function service()
	{
		return $this->belongsTo(TypeServices::class, 'type_service_id');
	}

	public function getLink($type = null)
	{
		$service = $this->service;
		if ($service)
		{
			// video link
			if ($this->type_id == 1)
			{
				$galLink = '';
				$urlResponse = $this->validateYouTubeLink($this->url);

				if ($urlResponse)
				{
					if ($type == 'thumb')
					{
						$galLink = $urlResponse->thumbnail_url;
					}
					else
					{
						$galLink = $this->getVideoWithCustomOption($this->url);
					}
				}
			}
			else
			{
				$imgLink = $this->getGalleryBaseUrl();
				$imgLink .= '/services/' . $service->id . '/images/';

				if ($type)
				{
					$imgLink .= $type . '/';
				}

				$galLink = $imgLink . $this->url;
			}
		}
		else
		{
			$galLink = ''; // @todo Default id
		}

		return $galLink;
	}

}
