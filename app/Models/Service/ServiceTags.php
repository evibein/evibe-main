<?php

namespace App\Models\Service;

use App\Models\BaseModel;
use App\Models\Types\TypeTag;

class ServiceTags extends BaseModel
{
	protected $table = 'service_tags';

	public function tag()
	{
		return $this->belongsTo(TypeTag::class, 'tag_id');
	}
}