<?php

namespace App\Models\Referral;

use App\Models\BaseModel;

class LogReferralFriendActions extends BaseModel
{
	protected $table = 'log_referral_friend_actions';
}