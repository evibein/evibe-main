<?php

namespace App\Models\Referral;

use App\Models\BaseModel;

class LogReferralPreActions extends BaseModel
{
	protected $table = 'log_referral_pre_actions';
}