<?php

namespace App\Models;

use App\Models\Types\TypeTicket;
use App\Models\Util\PartnerReviewAnswer;
use App\Models\Util\PartnerReviewQuestion;
use App\Models\Util\ReviewReply;
use App\Models\Util\TicketReviewQuestion;
use App\Models\Vendor\VendorReview;
use Carbon\Carbon;
use Evibe\Facades\EvibeUtilFacade as EvibeUtil;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BaseModel extends Model
{
	use SoftDeletes;

	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function getGalleryBaseUrl()
	{
		return config('evibe.gallery.host');
	}

	public function formatPrice($price)
	{
		return EvibeUtil::formatPrice($price);
	}

	public function validateYouTubeLink($url)
	{
		$fullUrl = "http://www.youtube.com/oembed?url=http://www.youtube.com/watch?v=$url&format=json&html5=1";

		try
		{
			$result = file_get_contents($fullUrl);
			$result = json_decode($result);
		} catch (\Exception $e)
		{
			$result = false;
		}

		return $result;

	}

	public function getVideoWithCustomOption($url, $rel = 0)
	{
		$baseUrl = config('evibe.youtube_host') . $url . '&cc_load_policy=1&showinfo=0&rel=' . $rel;

		return $baseUrl;
	}

	public function getFormattedName($name)
	{
		return ucwords(strtolower(trim($name)));
	}

	// review for venue and non-venue partner
	protected function partnerReviewData($mapTypeId, $mapId, $skip, $take, $sortBy)
	{
		$data = [
			"reviews"    => [
				"topRated"   => [],
				"mostRecent" => [],
				"leastRated" => []
			],
			"highlights" => [
				"showHighlights"    => false,
				"topPositiveReview" => false,
				"topCriticalReview" => false
			],
			"total"      => [
				"count" => 0,
				"avg"   => 0
			]
		];
		$topCriticalReview = false;
		$topPositiveReview = false;
		$showHighlights = false;
		$eventId = getEventIdFromSession();

		// get all reviews
		$reviews = VendorReview::select("id", "created_at", "rating", "location", "name", "review", "type_ticket_id")
		                       ->where('map_type_id', $mapTypeId)
		                       ->where('map_id', $mapId)
		                       ->where('is_accepted', 1)
		                       ->orderBy("rating", "DESC")
		                       ->orderBy('created_at', 'DESC')
		                       ->get();

		/* @see: not fetching reviews by event
		 * if ($eventId)
		 * {
		 * $reviews = $reviews->where('event_id', '=', $eventId);
		 * }
		 */

		$totalCount = $reviews->count();
		$averageRating = $reviews->average("rating");
		$allReviews = $reviews;

		/**
		 * Not fetching this content until it is optimized, effects in product review page
		 * Commented by @JR on 12 Jan 2019
		 */
		if (($totalCount > $take) && (2 == 1))
		{
			// top positive review = review with review text & high rating
			// may not be always highest rated
			$allReviewsWithText = $allReviews->filter(function ($item) {
				return (!is_null($item->review) && $item->review);
			});

			$topPositiveReview = $this->getReviewDetails($allReviewsWithText->first());
			$topRatingWithText = is_array($topPositiveReview) ? $topPositiveReview["rating"] : 0;

			$leastRatingWithText = $allReviewsWithText->min("rating");
			$topCriticalReview = $allReviewsWithText->filter(function ($item) use ($leastRatingWithText) {
				return ($item->rating == $leastRatingWithText);
			});
			$topCriticalReview = $topCriticalReview->sortByDesc("created_at");
			$topCriticalReview = $this->getReviewDetails($topCriticalReview->first());

			// show highlights only if top ratings is above threshold
			if ($topRatingWithText > 3.5 && ($topRatingWithText != $leastRatingWithText))
			{
				$showHighlights = true;
			}
		}

		$topRatedReviews = $reviews->slice($skip, $take)->toArray();

		// get sort data based on the sort type
		// false === for profile page
		$mostRecentReviews = [];
		$allReviewsBackup = $allReviews;
		if ($sortBy === false || $sortBy === "mostRecent")
		{
			$allReviews = $allReviews->sortByDesc("created_at");
			$mostRecentReviews = $allReviews->slice($skip, $take)->toArray();
		}

		$leastRatedReviews = [];
		if ($sortBy === false || $sortBy === "leastRated")
		{
			$allReviewsBackup = $allReviewsBackup->sortByDesc(function ($item) {
				// least rated & latest first
				return (5 - $item->rating) . "_" . Carbon::createFromFormat("Y-m-d H:i:s", $item->created_at)->timestamp;
			});
			$leastRatedReviews = $allReviewsBackup->slice($skip, $take)->toArray();
		}

		$allReviewIds = array_merge(array_column($topRatedReviews, "id"), array_column($mostRecentReviews, "id"), array_column($leastRatedReviews, "id"));
		$typeTickets = TypeTicket::all()
		                         ->pluck("name", "id")
		                         ->toArray();

		$reviewReplies = ReviewReply::whereIn("review_id", $allReviewIds)
		                            ->pluck("reply", "review_id")
		                            ->toArray();

		$partnerReviewAnswers = PartnerReviewAnswer::select("question_id", "rating", "review_id")
		                                           ->whereIn("review_id", $allReviewIds)
		                                           ->get();

		$partnerReviewQuestions = PartnerReviewQuestion::whereIn("id", $partnerReviewAnswers->pluck("question_id")->toArray())
		                                               ->pluck("question", "id")
		                                               ->toArray();

		$loop = [
			"topRated"   => $topRatedReviews,
			"mostRecent" => $mostRecentReviews,
			"leastRated" => $leastRatedReviews
		];

		foreach ($loop as $key => $value)
		{
			foreach ($value as $review)
			{
				$individualReview = [];
				$individualAnswers = $partnerReviewAnswers->where("review_id", $review["id"]);
				if ($individualAnswers->count() > 0)
				{
					foreach ($individualAnswers as $answer)
					{
						if (isset($partnerReviewQuestions[$answer["question_id"]]))
						{
							$individualReview[] = [
								'question' => ucfirst($partnerReviewQuestions[$answer["question_id"]]),
								'rating'   => $answer->rating
							];
						}
					}
				}

				$data["reviews"][$key][] = [
					"name"               => $review["name"],
					"review"             => $review["review"],
					"rating"             => trim($review["rating"]),
					"location"           => $review["location"] ? $review["location"] : false,
					"reply"              => isset($reviewReplies[$review["id"]]) ? $reviewReplies[$review["id"]] : "",
					"individuals"        => $individualReview,
					"reviewFor"          => isset($typeTickets[$review["type_ticket_id"]]) ? ucfirst($typeTickets[$review["type_ticket_id"]]) : "",
					"extraTicketReviews" => [],
					"created_at"         => $review["created_at"]
					// "extraTicketReviews" => $this->getTicketExtraReview($review)
				];
			}
		}

		$data["total"]["count"] = $totalCount;
		$data["total"]["avg"] = $averageRating;

		$data["highlights"]["showHighlights"] = $showHighlights;
		$data["highlights"]["topPositiveReview"] = $topPositiveReview;
		$data["highlights"]["topCriticalReview"] = $topCriticalReview;

		return $data;
	}

	private function getReviewDetails($review)
	{
		if (!$review)
		{
			return false;
		}

		$individualReview = [];
		$location = $review->location ? $review->location : false;
		$answers = $review->individualAnswers;

		// fetch sub reviews
		if ($answers->count())
		{
			foreach ($answers as $answer)
			{
				if ($answer->question)
				{
					$individualReview[] = [
						'question' => $answer->question ? ucfirst($answer->question->question) : "",
						'rating'   => $answer->rating
					];
				}
			}
		}

		// fetch location
		if (!$review->location && $review->booking && $review->booking->ticket)
		{
			if ($review->booking->ticket->area)
			{
				$location = $review->booking->ticket->area->name;
			}
			elseif ($review->booking->ticket->city)
			{
				$location = $review->booking->ticket->city->name;
			}
		}

		$reviewDetails = [
			"name"               => $review->name,
			"review"             => $review->review,
			"rating"             => trim($review->rating),
			"location"           => $location,
			"reply"              => $review->reply ? $review->reply->reply : "",
			"individuals"        => $individualReview,
			"reviewFor"          => $review->typeTicket ? ucfirst($review->typeTicket->name) : "",
			"extraTicketReviews" => $this->getTicketExtraReview($review)
		];

		return $reviewDetails;
	}

	private function getTicketExtraReview($review)
	{
		$extraTicketReviews = [];
		$ticketReview = $review->booking && $review->booking->ticket ? $review->booking->ticket->ticketReview : null;
		// @see: include only those questions which needs to be shown to customer
		$validQuestions = [
			config('evibe.review-question.why-choose-evibe')
		];
		$ticketReviewQuestions = TicketReviewQuestion::whereIn('id', $validQuestions)->get();

		if (!empty($ticketReview))
		{
			foreach ($ticketReviewQuestions as $ticketReviewQuestion)
			{
				$nonCustomerShowQuestionIds = [config("evibe.review.question.ReferEarn")];

				if ($ticketReviewQuestion->stringifyAnswer($ticketReview->id) && $ticketReviewQuestion->question && !in_array($ticketReviewQuestion->id, $nonCustomerShowQuestionIds))
				{
					$extraTicketReviews[] = [
						'question' => $ticketReviewQuestion->question,
						'answers'  => $ticketReviewQuestion->stringifyAnswer($ticketReview->id)
					];
				}
			}
		}

		return $extraTicketReviews;
	}
}