<?php

namespace App\Models\Venue;

use App\Models\BaseModel;
use App\Models\Types\TypeVenueGallery;

class VenueHallGallery extends BaseModel
{
	protected $table = 'venue_hall_gallery';

	public function hall()
	{
		return $this->belongsTo(VenueHall::class, 'venue_hall_id');
	}

	public function type()
	{
		return $this->belongsTo(TypeVenueGallery::class, 'type_id');
	}

	public function getPath($type = null)
	{
		$hall = $this->hall;
		$path = config('evibe.gallery.host');

		$path .= '/venues/' . $hall->venue_id;
		$path .= '/halls/' . $hall->id . '/images/';
		$path .= $type ? $type . '/' : '';
		$path .= $this->url;

		return $path;
	}
}