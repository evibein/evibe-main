<?php

namespace App\Models\Venue;

use App\Models\BaseModel;
use App\Models\Types\TypeEvent;

class VenueHallEvent extends BaseModel
{
    protected $table = "venue_hall_event";

    public function event()
    {
        return $this->belongsTo(TypeEvent::class, 'event_id');
    }
}
