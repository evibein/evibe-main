<?php

namespace App\Models\Venue;

use App\Models\BaseModel;
use App\Models\Types\TypeVenueCuisine;

class VenueCuisine extends BaseModel
{
	protected $table = 'venue_cuisine';

	public function type()
	{
		return $this->belongsTo(TypeVenueCuisine::class, 'type_id');
	}
}