<?php

namespace App\Models\Venue;

use App\Models\BaseModel;

class VenueReview extends BaseModel
{
	protected $table = 'venue_review';
}