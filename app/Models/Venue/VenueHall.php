<?php

namespace App\Models\Venue;

use App\Evibe\EvibeTraits\EvibeSearchable;
use App\Evibe\EvibeTraits\EvibeShortListable;
use App\Models\BaseModel;
use App\Models\Types\TypeVenueHall;
use App\Models\Util\AreaNearby;
use App\Models\Util\City;

class VenueHall extends BaseModel
{
	use EvibeSearchable, EvibeShortListable;

	protected $table = 'venue_hall';

	public function venue()
	{
		return $this->belongsTo(Venue::class, 'venue_id');
	}

	public function gallery()
	{
		return $this->hasMany(VenueHallGallery::class, 'venue_hall_id');
	}

	public function type()
	{
		return $this->belongsTo(TypeVenueHall::class, 'type_id');
	}

	public function scopeOfCity($query, $cityId)
	{
		return $query->join('venue', 'venue.id', '=', 'venue_hall.venue_id')
		             ->where('venue.is_live', 1)
		             ->where('venue.city_id', $cityId);
	}

	public function scopeForEvent($query, $eventId)
	{
		return $query->join('venue_hall_event', 'venue_hall_event.venue_hall_id', '=', 'venue_hall.id')
		             ->whereNull('venue_hall_event.deleted_at')
		             ->where('venue_hall_event.event_id', $eventId);
	}

	// fetch the cakes for the particular city
	// venuehall->venue->city_id = City.url.id
	// can be improved much with the city sql query
	public function scopeForCity($query)
	{
		$cityUrl = getCityUrl();
		$city = City::where('city.url', $cityUrl)->first();
		$cityId = $city->id ? $city->id : 1;  // set the default to bangalore

		return $query->where('venue.city_id', $cityId);
	}

	public function prepareData()
	{
		$type = $this->type->name;

		if ($this->type_id == 1)
		{
			$type = 'Party space';
		}

		$data = [
			'id'              => $this->id,
			'code'            => $this->code,
			'url'             => $this->url,
			'type'            => $type,
			'name'            => $this->name,
			'floor'           => $this->floor,
			'hasAc'           => $this->has_ac,
			'capacityMin'     => $this->cap_min,
			'capacityMax'     => $this->cap_max,
			'capacityFloat'   => $this->cap_float,
			'minRentDuration' => $this->min_rent_duration,
			'worthRent'       => $this->rent_worth,
			'priceMinRent'    => $this->rent_min,
			'priceMaxRent'    => $this->rent_max,
			'worthVeg'        => $this->min_veg_worth,
			'worthNonVeg'     => $this->min_nonveg_worth,
			'priceMinVeg'     => $this->price_min_veg,
			'priceMinNonveg'  => $this->price_min_nonveg,
		];

		return $data;
	}

	public function prepareGallery()
	{
		$data = [];
		$list = VenueHallGallery::with('type')
		                        ->where('venue_hall_id', $this->id)
		                        ->orderBy('type_id')
		                        ->get();

		foreach ($list as $item)
		{
			if (!$item->type->is_public)
			{
				continue;
			}

			array_push($data, [
				'id'    => $item->id,
				'type'  => $item->type->name,
				'url'   => $item->getPath("profile"),
				'thumb' => $item->getPath("thumbs"),
				'title' => $item->type->name
			]);
		}

		return $data;
	}

	// @todo: get correct profile image
	public function getProfileImg()
	{
		$profileImgObj = VenueHallGallery::where('venue_hall_id', $this->id)
		                                 ->orderBy('is_profile', 'DESC')
		                                 ->orderBy('created_at', 'DESC')
		                                 ->first();

		// check images in hall
		if ($profileImgObj)
		{
			$profileImg = $profileImgObj->getPath("results");
		}

		// if not available, get from venue
		else
		{
			$venue = $this->venue;
			$profileImg = $venue->getProfileImg();
		}

		return $profileImg;
	}

	public function getLink($occasionUrl)
	{
		$cityUrl = $this->venue->city->url;

		return '/' . $cityUrl . '/' . $occasionUrl . '/' . config('evibe.profile_url.party_halls') . '/' . $this->url;
	}

	public function getSimilarHalls()
	{
		// get similar halls based on Type, location
		$typeId = $this->type->id;
		$testCounts = VenueHall::with('venue')
		                       ->join('venue', 'venue.id', '=', 'venue_hall.venue_id')
		                       ->where('venue.is_live', 1)
		                       ->where('venue_hall.id', '!=', $this->id)
		                       ->select('venue_hall.id', 'venue_hall.venue_id', 'venue_hall.created_at', 'venue_hall.deleted_at', 'venue_hall.updated_at', 'venue_hall.type_id', 'venue_hall.name', 'venue_hall.floor', 'venue_hall.has_ac', 'venue_hall.cap_min', 'venue_hall.cap_max', 'venue_hall.cap_float', 'venue_hall.code', 'venue_hall.url')
		                       ->where('venue_hall.type_id', $typeId)
		                       ->take(3)
		                       ->get();

		// similar halls are less, include nearby areas
		// @todo: see to how to improve without copying entire query
		if ($testCounts->count() < 3)
		{
			$areaId = $this->venue->area->id;
			$nearByAreas = $this->getNearbyAreas($areaId);

			$similarHalls = VenueHall::with('venue')
			                         ->join('venue', 'venue.id', '=', 'venue_hall.venue_id')
			                         ->where('venue.is_live', 1)
			                         ->where('venue_hall.id', '!=', $this->id)
			                         ->select('venue_hall.id', 'venue_hall.venue_id', 'venue_hall.created_at', 'venue_hall.deleted_at', 'venue_hall.updated_at', 'venue_hall.type_id', 'venue_hall.name', 'venue_hall.floor', 'venue_hall.has_ac', 'venue_hall.cap_min', 'venue_hall.cap_max', 'venue_hall.cap_float', 'venue_hall.code', 'venue_hall.url')
			                         ->where(function ($query) use ($nearByAreas, $typeId) {
				                         $query->where('venue_hall.type_id', $typeId)
				                               ->orWhereIn('venue.area_id', $nearByAreas);
			                         })
			                         ->take(1)
			                         ->get();
		}
		else
		{
			$similarHalls = $testCounts;
		}

		return $similarHalls;
	}

	private function getNearbyAreas($areaId)
	{
		$nearbyAreas = [$areaId];
		$allAreaMappings = AreaNearby::where('area1', $areaId)
		                             ->orWhere('area2', $areaId)
		                             ->get();

		foreach ($allAreaMappings as $areaMapping)
		{
			if ($areaMapping->area1 == $areaId)
			{
				array_push($nearbyAreas, $areaMapping->area2);
			}
			if ($areaMapping->area2 == $areaId)
			{
				array_push($nearbyAreas, $areaMapping->area1);
			}
		}

		return array_unique($nearbyAreas); // get unique
	}

	public function getPriceWorth()
	{
		return 0;
	}

	public function event()
	{
		return $this->hasMany(VenueHallEvent::class, 'venue_hall_id');
	}

	public function getTitle()
	{
		if ($this->type_id == 1)
		{
			return 'Party Space at ' . $this->venue->area->name;
		}
		else
		{
			return $this->type->name . ' at ' . $this->venue->area->name;
		}
	}

	public function getPrice()
	{
		if ($this->min_rent)
		{
			return $this->rent_min;
		}
		elseif ($this->price_min_veg)
		{
			return $this->price_min_veg;
		}
		elseif ($this->venue->price_min_rent)
		{
			return $this->venue->price_min_rent;
		}
		elseif ($this->venue->price_min_veg)
		{
			return $this->venue->price_min_veg;
		}
		elseif ($this->venue->price_kid)
		{
			return $this->venue->price_kid;
		}
	}

	public function getWorth()
	{
		if ($this->min_rent)
		{
			return $this->rent_worth;
		}
		elseif ($this->price_min_veg)
		{
			return $this->price_worth_veg;
		}
		elseif ($this->venue->price_min_rent)
		{
			return $this->venue->worth_rent;
		}
		elseif ($this->venue->price_min_veg)
		{
			return $this->venue->min_veg_worth;
		}
		elseif ($this->venue->price_kid)
		{
			return $this->venue->worth_kid;
		}
	}

	public function getAdditionalLine()
	{
		if ($this->min_rent)
		{
			return 'Rent (' . $this->min_rent_duration . 'Hrs)';
		}
		elseif ($this->price_min_veg)
		{
			return '(per person)';
		}
		elseif ($this->venue->price_min_rent)
		{
			return 'Rent (' . $this->venue->min_rent_duration . ' Hrs)';
		}
		elseif ($this->venue->price_min_veg)
		{
			return '(per person)';
		}
		elseif ($this->venue->price_kid)
		{
			return '(per kid)';
		}
	}

	public function getPriceMax()
	{
		if ($this->min_rent)
		{
			if ($this->rent_max)
			{
				return $this->rent_max . '*';
			}
			else
			{
				return '* onwards';
			}
		}
		elseif ($this->price_min_veg)
		{
			return '';
		}
		elseif ($this->venue->price_min_rent)
		{
			if ($this->venue->price_max_rent)
			{
				return $this->venue->price_max_rent . '*';
			}
			else
			{
				return '* onwards';
			}
		}
		elseif ($this->venue->price_min_veg)
		{
			return '';
		}
		elseif ($this->venue->price_kid)
		{
			return '';
		}
	}

	public function getPartyBagPriceMin()
	{
		if ($this->min_rent)
		{
			return $this->rent_min;
		}
		elseif ($this->price_min_veg)
		{
			return $this->price_min_veg * $this->cap_min;
		}
		elseif ($this->venue->price_min_rent)
		{
			return $this->venue->price_min_rent;
		}
		elseif ($this->venue->price_min_veg)
		{
			return $this->venue->price_min_veg * $this->venue->cap_min;
		}
		elseif ($this->venue->price_kid)
		{
			return $this->venue->price_kid * $this->venue->cap_min;
		}
	}

	public function getPartyBagPriceMax()
	{
		if ($this->min_rent)
		{
			if ($this->rent_max && $this->rent_max > $this->rent_min)
			{
				return $this->rent_max;
			}

			return $this->rent_min;
		}
		elseif ($this->price_min_veg)
		{
			if ($this->cap_max && $this->cap_max > $this->cap_min)
			{
				return $this->price_min_veg * $this->cap_max;
			}

			return $this->price_min_veg * $this->cap_min;
		}
		elseif ($this->venue->price_min_rent)
		{
			if ($this->venue->price_max_rent && $this->venue->price_max_rent > $this->venue->price_min_rent)
			{
				return $this->venue->price_max_rent;
			}

			return $this->venue->price_min_rent;
		}
		elseif ($this->venue->price_min_veg)
		{
			if ($this->venue->cap_max && $this->venue->cap_max > $this->venue->cap_min)
			{
				return $this->venue->price_min_veg * $this->venue->cap_max;
			}

			return $this->venue->price_min_veg * $this->venue->cap_min;
		}
		elseif ($this->venue->price_kid)
		{
			if ($this->venue->cap_max && $this->venue->cap_max > $this->venue->cap_min)
			{
				return $this->venue->price_kid * $this->venue->cap_max;
			}

			return $this->venue->price_kid * $this->venue->cap_min;
		}
	}

	public function getPartyBagPriceWorthMin()
	{
		if ($this->min_rent)
		{
			if ($this->rent_worth && $this->rent_worth > $this->rent_min)
			{
				return $this->rent_worth;
			}

			return $this->rent_min;
		}
		elseif ($this->price_min_veg)
		{
			if ($this->price_worth_veg && $this->price_worth_veg > $this->price_min_veg)
			{
				return $this->price_worth_veg * $this->cap_min;
			}

			return $this->price_min_veg * $this->cap_min;
		}
		elseif ($this->venue->price_min_rent)
		{
			if ($this->venue->worth_rent && $this->venue->worth_rent > $this->venue->price_min_rent)
			{
				return $this->venue->worth_rent;
			}

			return $this->venue->price_min_rent;
		}
		elseif ($this->venue->price_min_veg)
		{
			if ($this->venue->min_veg_worth && $this->venue->min_veg_worth > $this->venue->price_min_veg)
			{
				return $this->venue->min_veg_worth * $this->venue->cap_min;
			}

			return $this->venue->price_min_veg * $this->venue->cap_min;
		}
		elseif ($this->venue->price_kid)
		{
			if ($this->venue->worth_kid && $this->venue->worth_kid > $this->venue->price_kid)
			{
				return $this->venue->worth_kid * $this->venue->cap_min;
			}

			return $this->venue->price_kid * $this->venue->cap_min;
		}
	}

	public function getPartyBagPriceWorthMax()
	{
		if ($this->min_rent)
		{
			if ($this->rent_worth && $this->rent_worth > $this->getPartyBagPriceMax())
			{
				return $this->rent_worth;
			}

			return $this->getPartyBagPriceMax();
		}
		elseif ($this->price_min_veg)
		{
			$priceWorthVeg = $this->price_min_veg;
			if ($this->price_worth_veg && $this->price_worth_veg > $this->price_min_veg)
			{
				$priceWorthVeg = $this->price_worth_veg;
			}
			if ($this->cap_max && $this->cap_max > $this->cap_min)
			{
				return $priceWorthVeg * $this->cap_max;
			}

			return $priceWorthVeg * $this->cap_min;
		}
		elseif ($this->venue->price_min_rent)
		{
			if ($this->venue->worth_rent && $this->venue->worth_rent > $this->getPartyBagPriceMax())
			{
				return $this->venue->worth_rent;
			}

			return $this->getPartyBagPriceMax();
		}
		elseif ($this->venue->price_min_veg)
		{
			$venuePriceWorthVeg = $this->venue->price_min_veg;
			$venueCapacity = $this->venue->cap_min;
			if ($this->venue->min_veg_worth && $this->venue->min_veg_worth > $this->getPartyBagPriceMax())
			{
				$venuePriceWorthVeg = $this->venue->min_veg_worth;
			}
			if ($this->venue->cap_max && $this->venue->cap_max > $this->venue->cap_min)
			{
				$venueCapacity = $this->venue->cap_max;
			}

			return $venuePriceWorthVeg * $venueCapacity;
		}
		elseif ($this->venue->price_kid)
		{
			$venuePriceWorthKid = $this->venue->price_min_veg;
			$venueCapacity = $this->venue->cap_min;
			if ($this->venue->worth_kid && $this->venue->worth_kid > $this->venue->price_kid)
			{
				$venuePriceWorthKid = $this->venue->worth_kid;
			}
			if ($this->venue->cap_max && $this->venue->cap_max > $this->venue->cap_min)
			{
				$venueCapacity = $this->venue->cap_max;
			}

			return $venuePriceWorthKid * $venueCapacity;
		}
	}

	public function getCardInfo($fullUrl, $price, $mapTypeId, $occasionId)
	{
		$cardInfo = [
			'id'                  => $this->id,
			'title'               => $this->getTitle(),
			'price'               => $price,
			'profileImg'          => $this->getProfileImg(),
			'fullUrl'             => $fullUrl,
			'additionalLine'      => $this->getAdditionalLine(),
			'priceMax'            => $this->getPriceMax(),
			'worth'               => $this->getWorth(),
			'rangeInfo'           => '',
			'pbPriceMin'          => $this->getPartyBagPriceMin(),
			'pbPriceMax'          => $this->getPartyBagPriceMax(),
			'pbPriceWorthMin'     => $this->getPartyBagPriceWorthMin(),
			'pbPriceWorthMax'     => $this->getPartyBagPriceWorthMax(),
			'rating'              => '',
			'additionalPriceLine' => ''
		];

		if ($mapTypeId)
		{
			$cardInfo['mapTypeId'] = $mapTypeId;
		}

		if ($occasionId)
		{
			$cardInfo['occasionId'] = $occasionId;
		}

		return $cardInfo;
	}

	public function getSearchableCardInfo($cityUrl, $occasionUrl, $params)
	{
		$searchableCardInfo = [];
		$fullUrl = $this->getFullLinkForSearch($cityUrl, $occasionUrl);
		$occasionId = $this->getOccasionIdForSearch($occasionUrl);
		$price = $this->getPrice();
		$mapTypeId = isset($params['map_type_id']) ? $params['map_type_id'] : null;

		if ($fullUrl && $price)
		{
			$searchableCardInfo = $this->getCardInfo($fullUrl, $price, $mapTypeId, $occasionId);
		}

		return $searchableCardInfo;
	}

	public function getFullLinkForSearch($cityUrl, $occasionUrl)
	{
		// check if occasion url is empty, get first occasion url
		if (!$occasionUrl || is_null($occasionUrl))
		{
			$firstEvent = $this->event ? $this->event->first() : null;
			$occasionUrl = ($firstEvent && $firstEvent->event) ? $firstEvent->event->url : config('evibe.occasion.kids_birthdays.url');
		}

		$link = "";
		if (isset($this->evibeUtil->occasionUrlRouteMapping[$occasionUrl]))
		{
			$occasionRouteName = $this->evibeUtil->occasionUrlRouteMapping[$occasionUrl];
			$link = $this->getLinkByRouteName($occasionRouteName . "venues.profile", [$cityUrl, $this->url]);
		}

		return $link;
	}

	public function getOccasionIdForSearch($occasionUrl)
	{
		if (!$occasionUrl)
		{
			$firstEvent = $this->event->first();
			if ($firstEvent)
			{
				return $firstEvent->event->id;
			}
		}

		return null;
	}

	public function forSearchIsLive()
	{
		return ($this->venue && $this->venue->is_live && $this->event && $this->event->count());
	}

	public function forSearchPrice()
	{
		return ($this->getPrice() ? $this->getPrice() : 0);
	}

	public function forSearchCity($cityId)
	{
		return ($this->venue->city_id == $cityId);
	}

	// for party bag
	public function getShortListableCardInfo($cityUrl, $occasionUrl, $params, $shortlistParams)
	{
		$cardInfo = [];
		$fullUrl = $this->getFullLinkForSearch($cityUrl, $occasionUrl);
		$price = $this->getPrice();

		if ($fullUrl && $price)
		{
			$cardInfo = $this->getCardInfo($fullUrl, $price, null, null);
		}

		$cardInfo = array_merge($cardInfo, $shortlistParams);

		return $cardInfo;
	}

	// for collections
	public function getCollectionCardInfo($cityUrl, $occasionUrl, $params, $collectionParams)
	{
		$collectionCardInfo = [];
		$fullUrl = $this->getFullLinkForSearch($cityUrl, $occasionUrl);
		$price = $this->getPrice();
		$occasionId = null;
		if (!$occasionUrl)
		{
			$firstEvent = $this->event->first();
			if ($firstEvent)
			{
				$occasionId = $firstEvent->event->id;
			}
		}

		if ($fullUrl && $price)
		{
			$collectionCardInfo = $this->getCardInfo($fullUrl, $price, null, $occasionId);
		}

		if (!$collectionParams['occasionId'])
		{
			$collectionParams['occasionId'] = $occasionId;
		}

		$collectionCardInfo = array_merge($collectionCardInfo, $collectionParams);

		return $collectionCardInfo;
	}
}