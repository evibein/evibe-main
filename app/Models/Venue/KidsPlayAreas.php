<?php

namespace App\Models\Venue;

use App\Models\BaseModel;

class KidsPlayAreas extends BaseModel
{
	protected $table = 'kids_venues_data';

	public function gallery()
	{
		return $this->hasMany('App\Models\Venue\KidsVenuesGallery', 'place_id', 'place_id');
	}

	public function getThumbnail()
	{
		$placeId = $this->place_id;
		$image = $this->gallery()->first();
		if ($image)
		{
			return ($image->image_url);
		}
		else
		{
			// Error Image
			return "https://gallery.evibe.in/main/pages/play-area/default-thumb.jpeg";
		}
	}

	public function getReviews()
	{
		return $this->hasMany('App\Models\Venue\KidsVenuesReviews', 'place_id', 'place_id');
	}
}