<?php

namespace App\Models\Venue;

use App\Models\BaseModel;

class KidsVenuesGallery extends BaseModel
{
	protected $table =  'kids_venues_gallery';
}