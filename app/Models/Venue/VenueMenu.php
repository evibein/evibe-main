<?php

namespace App\Models\Venue;

use App\Models\BaseModel;
use App\Models\Types\TypeVenueMenu;

class VenueMenu extends BaseModel
{
	protected $table = 'venue_menu';

	public function venue()
	{
		return $this->belongsTo(Venue::class, 'venue_id');
	}

	public function type()
	{
		return $this->belongsTo(TypeVenueMenu::class, 'type_id');
	}

	public function getPath()
	{
		$path = config('evibe.gallery.host');

		$path .= '/venues/' . $this->venue_id;
		$path .= '/menus/' . $this->url;

		return $path;
	}
}