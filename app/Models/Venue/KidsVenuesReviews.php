<?php

namespace App\Models\Venue;

use App\Models\BaseModel;

class KidsVenuesReviews extends BaseModel
{
	protected $table =  'kids_venues_reviews';
}