<?php

namespace App\Models\Venue;

use App\Models\BaseModel;
use App\Models\Types\TypeVenueGallery;

class VenueGallery extends BaseModel
{
	protected $table = 'venue_gallery';

	public function venue()
	{
		return $this->belongsTo(Venue::class, 'venue_id');
	}

	public function getPath($type = null)
	{
		$path = config('evibe.gallery.host');

		$path .= '/venues/' . $this->venue_id . '/images/';
		$path .= $type ? $type . '/' : ''; // results / profile / thumbs
		$path .= $this->url;

		return $path;
	}

	public function type()
	{
		return $this->belongsTo(TypeVenueGallery::class, 'type_id');
	}
}