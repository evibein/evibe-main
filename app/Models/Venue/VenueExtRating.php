<?php

namespace App\Models\Venue;

use App\Models\BaseModel;
use App\Models\Types\TypeExtRating;

class VenueExtRating extends BaseModel
{
	protected $table = 'venue_ext_ratings';

	public function type()
	{
		return $this->belongsTo(TypeExtRating::class, 'type_ratings_website_id');
	}

	public function venue()
	{
		return $this->belongsTo(Venue::class, 'venue_id');
	}
}
