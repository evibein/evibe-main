<?php

namespace App\Models\Venue;

use App\Evibe\EvibeTraits\EvibeSearchable;
use App\Evibe\EvibeTraits\EvibeShortListable;
use App\Models\BaseModel;
use App\Models\Package\Package;
use App\Models\Partner\PartnerAverageRating;
use App\Models\Ticket\TicketBooking;
use App\Models\Types\TypeVenue;
use App\Models\Util\Area;
use App\Models\Util\City;
use App\Models\Util\PackageProviderInterface;
use App\Models\Util\PartnerReviewInterface;
use App\Models\Util\User;
use App\Models\Util\VendorStory;
use App\Models\Vendor\VendorReview;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Venue extends BaseModel implements PackageProviderInterface, PartnerReviewInterface
{
	use EvibeSearchable, EvibeShortListable;

	protected $table = 'venue';

	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}

	public function city()
	{
		return $this->belongsTo(City::class, 'city_id');
	}

	public function area()
	{
		return $this->belongsTo(Area::class, 'area_id');
	}

	public function type()
	{
		return $this->belongsTo(TypeVenue::class, 'type_id');
	}

	public function gallery()
	{
		return $this->hasMany('App\Models\VenueGallery', 'venue_id');
	}

	public function rating()
	{
		return $this->hasOne(PartnerAverageRating::class, 'partner_id')
		            ->where("partner_type_id", config("evibe.ticket.type.venue"));
	}

	public function avgRating()
	{
		return $this->prepareExtRatings()['avgRating'];
	}

	public function packages()
	{
		return $this->morphMany(Package::class, null, 'map_type_id', 'map_id');
	}

	public function stories()
	{
		return $this->morphMany(VendorStory::class, null, 'map_type_id', 'map_id');
	}

	// @todo: Being used in Package fetching code, returning empty for now
	public function getLink()
	{
		return '';
	}

	public function getDirectReviews($count = -1, $skip = -1)
	{
		$directReviews = [];
		$count = ($count != -1) ? $count : PHP_INT_MAX;

		$countObj = $reviewsObj = VendorReview::with('reply')
		                                      ->where('map_id', $this->id)
		                                      ->where('is_accepted', 1)
		                                      ->where('map_type_id', config('evibe.ticket.type.venue'))
		                                      ->orderBy('created_at', 'DESC');

		$directReviews['totalCount'] = $countObj->count();

		if ($skip != -1)
		{
			$reviewsObj->skip($skip);
		}
		$reviewsObj->take($count); // offset needs limit

		$directReviews['reviews'] = $reviewsObj->get();

		return $directReviews;
	}

	public function cuisines()
	{
		return $this->hasMany('App\Models\VenueCuisine', 'venue_id');
	}

	public function menus()
	{
		return $this->hasMany('App\Models\VenueMenu', 'venue_id');
	}

	public function reviews()
	{
		return $this->hasMany('App\Models\VenueReview', 'venue_id');
	}

	public function halls()
	{
		return $this->hasMany('App\Models\VenueHall', 'venue_id');
	}

	public function getCuisines()
	{
		$cuisines = '';
		$vc = VenueCuisine::where('venue_id', $this->id)->get();

		foreach ($vc as $c)
		{
			$cuisines .= $c->type->name . ", ";
		}

		return $cuisines ? substr($cuisines, 0, strlen($cuisines) - 2) : "--"; // remove trailing ", "
	}

	public function prepareData()
	{
		$info = preg_replace_callback('/([.!?])\s*(\w)/', function ($matches) {
			return strtoupper($matches[1] . ' ' . $matches[2]);
		}, ucfirst(strtolower($this->info)));

		$data = [
			'id'                          => $this->id,
			'code'                        => $this->code,
			'name'                        => $this->name,
			'url'                         => $this->url,
			'info'                        => $info,
			'terms'                       => $this->terms,
			'city'                        => $this->city->name,
			'area'                        => $this->area->name,
			'fullAddress'                 => $this->full_address,
			'landmark'                    => $this->landmark,
			'zip'                         => $this->zip,
			'mapLat'                      => $this->map_lat,
			'mapLong'                     => $this->map_long,
			'mapLink'                     => ($this->map_lat && $this->map_long) ? "https://www.google.com/maps/place/" . $this->map_lat . "+" . $this->map_long . "/" . $this->map_lat . "," . $this->map_long . ",17z" : null,
			'isPureVeg'                   => $this->is_pure_veg,
			'capacityMin'                 => $this->cap_min,
			'capacityMax'                 => $this->cap_max,
			'capacityFloat'               => $this->cap_float,
			'carParkCount'                => $this->car_park_count,
			'hasValetParking'             => $this->has_valet_parking,
			'taxPercent'                  => $this->tax_percent,
			'priceMinRent'                => $this->price_min_rent,
			'priceMaxRent'                => $this->price_max_rent,
			'minRentDuration'             => $this->min_rent_duration,
			'worthRent'                   => $this->worth_rent,
			'priceKid'                    => $this->price_kid,
			'priceAdult'                  => $this->price_adult,
			'worthKid'                    => $this->worth_kid,
			'worthAdult'                  => $this->worth_adult,
			'worthVeg'                    => $this->min_veg_worth,
			'worthNonVeg'                 => $this->min_nonveg_worth,
			'priceMinVeg'                 => $this->price_min_veg,
			'priceMinNonveg'              => $this->price_min_nonveg,
			'isAlcoholServed'             => $this->is_alcohol_served,
			'hasLift'                     => $this->has_lift,
			'hasSoundSystem'              => $this->has_sound_system,
			'priceSoundSystem'            => $this->price_sound_system,
			'hasMike'                     => $this->has_mike,
			'priceMike'                   => $this->price_mike,
			'hasProjector'                => $this->has_projector,
			'priceProjector'              => $this->price_projector,
			'isOutsideDecorsAllowed'      => $this->is_outside_decors_allowed,
			'priceOutsideDecorsAllowance' => $this->price_outside_decors_allowance,
			'isLive'                      => $this->is_live,
			'isPartner'                   => $this->is_partner,
			'isVerified'                  => $this->is_verified,
			'aboutVenue'                  => $this->about_venue,
			'cuisines'                    => $this->getCuisines()
		];

		return $data;
	}

	public function prepareExtRatings()
	{
		$ratings = [];
		$lastUpdatedOn = '';
		$avgRating = 0;

		$ratingsObj = VenueExtRating::with('type')->where('venue_id', $this->id);
		$ratingsList = $ratingsObj->get();

		// get last updated on
		if (count($ratingsList) > 0)
		{
			$ratings = $ratingsList->toArray();
			$avgRating = $ratingsObj->avg('rating_value');
			$lastUpdatedOn = DB::table('venue_ext_ratings')
			                   ->where('venue_id', $this->id)
			                   ->whereNull('deleted_at')
			                   ->min('updated_at');

			$lastUpdatedOn = Carbon::createFromTimeStamp(strtotime($lastUpdatedOn))->diffForHumans();
		}

		$extRatings = [
			'ratings'       => $ratings,
			'lastUpdatedOn' => $lastUpdatedOn,
			'avgRating'     => $avgRating,
			'reviewCount'   => count($ratings)
		];

		return $extRatings;
	}

	public function prepareGallery()
	{
		$data = [];
		$list = VenueGallery::with('type')
		                    ->where('venue_id', $this->id)
		                    ->orderBy('type_id')
		                    ->get();

		foreach ($list as $item)
		{
			if (!$item->type->is_public)
			{
				continue;
			}

			array_push($data, [
				'id'    => $item->id,
				'type'  => $item->type->name,
				'url'   => $item->getPath("profile"),
				'thumb' => $item->getPath("thumbs"),
				'title' => $item->type->name
			]);
		}

		return $data;
	}

	public function prepareMenus()
	{
		$data = [];
		$list = VenueMenu::where('venue_id', $this->id)->get();

		foreach ($list as $item)
		{
			array_push($data, [
				'id'        => $item->id,
				'type'      => $item->type->name,
				'name'      => $item->name,
				'price'     => $item->price,
				'worth'     => $item->worth,
				'minGuests' => $item->min_guests,
				'url'       => $item->getPath()
			]);
		}

		return $data;
	}

	public function getProfileImg()
	{
		$profileImg = $this->getGalleryBaseUrl() . '/img/app/default_venue.png';
		$profileImgObj = VenueGallery::select('venue_gallery.id', 'venue_gallery.venue_id', 'venue_gallery.title', 'venue_gallery.url')
		                             ->where('venue_id', $this->id)
		                             ->join('type_venue_gallery', 'type_venue_gallery.id', '=', 'venue_gallery.type_id')
		                             ->where('type_venue_gallery.is_public', 1)
		                             ->orderBy('venue_gallery.type_id', 'DESC')
		                             ->orderBY('venue_gallery.created_at', 'DESC')
		                             ->first();

		if ($profileImgObj)
		{
			$profileImg = $profileImgObj->getPath("results");
		}
		else
		{
			// if no valid image available for venue, get from hall
			$profileImgObj = VenueHallGallery::select('venue_hall_gallery.id', 'venue_hall_gallery.venue_hall_id', 'venue_hall_gallery.title', 'venue_hall_gallery.url')
			                                 ->join('venue_hall', 'venue_hall.id', '=', 'venue_hall_gallery.venue_hall_id')
			                                 ->join('type_venue_gallery', 'type_venue_gallery.id', '=', 'venue_hall_gallery.type_id')
			                                 ->whereNull('venue_hall.deleted_at')
			                                 ->where('venue_hall.venue_id', $this->id)
			                                 ->where('type_venue_gallery.is_public', 1)
			                                 ->orderBy('venue_hall_gallery.type_id', 'DESC')
			                                 ->orderBY('venue_hall_gallery.created_at', 'DESC')
			                                 ->first();

			if ($profileImgObj)
			{
				$profileImg = $profileImgObj->getPath("results");
			}
		}

		return $profileImg;
	}

	public function bookings()
	{
		return $this->morphMany(TicketBooking::class, null, 'map_type_id', 'map_id');
	}

	public function getCardInfo()
	{
		$cardInfo = [
			'title'       => $this->name,
			'desc'        => '',
			'price'       => $this->price_max_rent,
			'price_worth' => $this->price_min_rent,
			'profile'     => $this->getProfileImg(),
			'fullUrl'     => '',
			'isLive'      => $this->is_live
		];

		return $cardInfo;
	}

	public function getSearchableCardInfo()
	{
		$searchableCardInfo = $this->getCardInfo();

		return $searchableCardInfo;
	}

	public function getPersonAttribute($value)
	{
		return $this->getFormattedName($value);
	}

	public function getShortListableCardInfo()
	{
		return $this->getCardInfo();
	}

	public function getPartnerReview($skip = 0, $take = 10, $sortBy = false)
	{
		$mapTypeId = config('evibe.ticket.type.venue');

		return $this->partnerReviewData($mapTypeId, $this->id, $skip, $take, $sortBy);

	}
}