<?php

namespace App\Models\Inspirations;

use App\Models\BaseModel;

class InspirationPages extends BaseModel
{
	protected $table = 'inspirations_pages';
}