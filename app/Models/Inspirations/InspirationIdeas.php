<?php

namespace App\Models\Inspirations;

use App\Models\BaseModel;

class InspirationIdeas extends BaseModel
{
	protected $table = 'inspiration_ideas';
}