<?php

namespace App\Models\Collection;

use App\Models\BaseModel;

class Collection extends BaseModel
{
	protected $table = 'collection';

	public function getHomePageProfileImg()
	{
		$profileImgUrl = config('evibe.gallery.host');
		$profileImgUrl .= '/collection/' . $this->id . '/images/home/' . $this->profile_image;

		return $profileImgUrl;
	}

	public function getListPageProfileImg()
	{
		$profileImgUrl = config('evibe.gallery.host');
		$profileImgUrl .= '/collection/' . $this->id . '/images/list/' . $this->profile_image;

		return $profileImgUrl;
	}

	public function getListPageCoverImg()
	{
		$coverImgUrl = config('evibe.gallery.host');
		$coverImgUrl .= '/collection/' . $this->id . '/images/list/' . $this->cover_image;

		return $coverImgUrl;
	}

	public function getProfilePageProfileImg()
	{
		$profileImgUrl = config('evibe.gallery.host');
		$profileImgUrl .= '/collection/' . $this->id . '/images/profile/' . $this->profile_image;

		return $profileImgUrl;
	}

	public function getProfilePageCoverImg()
	{
		$coverImgUrl = config('evibe.gallery.host');
		$coverImgUrl .= '/collection/' . $this->id . '/images/profile/' . $this->cover_image;

		return $coverImgUrl;
	}
}