<?php

namespace App\Models\Collection;

use App\Models\BaseModel;

class CollectionOptions extends BaseModel
{
	protected $table = 'collection_option';
}