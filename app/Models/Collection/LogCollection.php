<?php

namespace App\Models\Collection;

use App\Models\BaseModel;

class LogCollection extends BaseModel
{
	protected $table = 'log_collection';
}