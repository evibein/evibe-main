<?php

namespace App\Models\Types;

use App\Models\BaseModel;

class TypePackageFieldCategory extends BaseModel
{
	protected $table = 'type_package_field_cat';
}