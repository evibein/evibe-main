<?php

namespace App\Models\Types;

use App\Evibe\EvibeTraits\EvibeSearchable;
use App\Evibe\EvibeTraits\EvibeShortListable;
use App\Models\BaseModel;
use App\Models\Service\ServiceGallery;
use App\Models\ServiceEvent;
use App\Models\Util\City;

class TypeServices extends BaseModel
{
	use EvibeSearchable, EvibeShortListable;

	protected $table = 'type_service';

	public function gallery()
	{
		return $this->hasMany(ServiceGallery::class, 'type_service_id');
	}

	public function event()
	{
		return $this->hasMany(ServiceEvent::class, 'type_service_id');
	}

	public function getProfilePic($type = "results")
	{
		$host = config('evibe.gallery.host');
		$profilePic = $host;

		$serviceId = $this->type_service_id > 0 ? $this->type_service_id : $this->id;

		// if none of them has is_profile = 1, get last uploaded
		$profileImgObj = ServiceGallery::where('type_service_id', $serviceId)
		                               ->where('type_id', config('evibe.gallery.type.image'))
		                               ->orderBy('is_profile', 'DESC')
		                               ->orderBy('updated_at', 'DESC')
		                               ->first();

		if ($profileImgObj)
		{
			$profilePic .= '/services/' . $serviceId . '/images/results/' . $profileImgObj->url;
		}

		return $profilePic;
	}

	public function getEventString()
	{
		$serviceEvents = $this->event;
		$eventsList = '';
		foreach ($serviceEvents as $serviceEvent)
		{
			$eventsList .= $serviceEvent->event->name . ', ';
		}
		$allEvents = substr($eventsList, 0, -2);

		return $allEvents;
	}

	public function scopeForSearchSelectCols($query)
	{
		return $query->select("id", "name", "info", "url", "range_info",
		                      "min_price", "max_price", "worth_price",
		                      "city_id", "is_live");
	}

	public function scopeForEvent($query, $eventId)
	{
		return $query->join('service_event', 'service_event.type_service_id', '=', 'type_service.id')
		             ->whereNull('service_event.deleted_at')
		             ->where('service_event.type_event_id', $eventId);
	}

	public function city()
	{
		return $this->belongsTo(City::class, "city_id");
	}

	// fetch the decors for the particular city
	// type_service->city_id = City.url.id
	// can be improved much with the city sql query
	public function scopeForCity($query, $cityId = null)
	{
		if (!$cityId)
		{
			$cityUrl = getCityUrl();
			$city = City::where('city.url', $cityUrl)->first();
			$cityId = ($city && $city->id) ? $city->id : 1;  // set the default to bangalore
		}

		return $query->where('type_service.city_id', '=', $cityId);
	}

	public function scopeIsLive($query)
	{
		return $query->where('type_service.is_live', 1);
	}

	public function getPartyBagPriceMin()
	{
		return $this->min_price;
	}

	public function getPartyBagPriceMax()
	{
		if ($this->max_price && $this->max_price > $this->min_price)
		{
			return $this->max_price;
		}

		return $this->min_price;
	}

	public function getPartyBagPriceWorthMin()
	{
		if ($this->worth_price && $this->worth_price > $this->min_price)
		{
			return $this->worth_price;
		}

		return $this->min_price;
	}

	public function getPartyBagPriceWorthMax()
	{
		if ($this->worth_price && $this->worth_price > $this->getPartyBagPriceMax())
		{
			return $this->worth_price;
		}

		return $this->getPartyBagPriceMax();
	}

	public function getCardInfo($fullUrl, $price, $mapTypeId, $occasionId)
	{
		$cardInfo = [
			'id'                  => $this->id,
			'title'               => $this->name ? $this->name : "Entertainment",
			'price'               => $price,
			'profileImg'          => $this->getProfilePic(),
			'fullUrl'             => $fullUrl,
			'worth'               => $this->worth_price,
			'priceMax'            => $this->max_price,
			'additionalLine'      => '',
			'rangeInfo'           => $this->rangeInfo,
			'pbPriceMin'          => $this->getPartyBagPriceMin(),
			'pbPriceMax'          => $this->getPartyBagPriceMax(),
			'pbPriceWorthMin'     => $this->getPartyBagPriceWorthMin(),
			'pbPriceWorthMax'     => $this->getPartyBagPriceWorthMax(),
			'rating'              => '',
			'additionalPriceLine' => ''
		];

		if ($mapTypeId)
		{
			$cardInfo['mapTypeId'] = $mapTypeId;
		}

		if ($occasionId)
		{
			$cardInfo['occasionId'] = $occasionId;
		}

		return $cardInfo;
	}

	public function getSearchableCardInfo($cityUrl, $occasionUrl, $params)
	{
		$searchableCardInfo = [];
		$fullUrl = $this->getFullLinkForSearch($cityUrl, $occasionUrl);
		$occasionId = $this->getOccasionIdForSearch($occasionUrl);
		$price = $this->min_price;
		$mapTypeId = isset($params['map_type_id']) ? $params['map_type_id'] : null;

		if ($fullUrl && $price)
		{
			$searchableCardInfo = $this->getCardInfo($fullUrl, $price, $mapTypeId, $occasionId);
		}

		return $searchableCardInfo;
	}

	public function getOccasionIdForSearch($occasionUrl)
	{
		if (!$occasionUrl)
		{
			$firstEvent = $this->event->first();

			if ($firstEvent)
			{
				return $firstEvent->event->id;
			}
		}

		return null;
	}

	public function getFullLinkForSearch($cityUrl, $occasionUrl)
	{
		// check if occasion url is empty, get first occasion url
		if (!$occasionUrl || is_null($occasionUrl))
		{
			$firstEvent = $this->event ? $this->event->first() : null;
			$occasionUrl = ($firstEvent && $firstEvent->event) ? $firstEvent->event->url : config('evibe.occasion.kids_birthdays.url');
		}

		$link = "";
		if (isset($this->evibeUtil->occasionUrlRouteMapping[$occasionUrl]))
		{
			$occasionRouteName = $this->evibeUtil->occasionUrlRouteMapping[$occasionUrl];
			$link = $this->getLinkByRouteName($occasionRouteName . "ent.profile", [$cityUrl, $this->url]);
		}

		return $link;
	}

	public function forSearchIsLive()
	{
		return ($this->event && $this->event->count() > 0);
	}

	public function forSearchPrice()
	{
		return ($this->min_price ? $this->min_price : 0);
	}

	public function forSearchCity($cityId)
	{
		return (getCityId() == $cityId);
	}

	// for party bag
	public function getShortListableCardInfo($cityUrl, $occasionUrl, $params, $shortlistParams)
	{
		$cardInfo = [];
		$fullUrl = $this->getFullLinkForSearch($cityUrl, $occasionUrl);
		$price = $this->min_price;

		if ($fullUrl && $price)
		{
			$cardInfo = $this->getCardInfo($fullUrl, $price, null, null);
		}

		$cardInfo = array_merge($cardInfo, $shortlistParams);

		return $cardInfo;
	}

	// for collections
	public function getCollectionCardInfo($cityUrl, $occasionUrl, $params, $collectionParams)
	{
		$collectionCardInfo = [];
		$fullUrl = $this->getFullLinkForSearch($cityUrl, $occasionUrl);
		$price = $this->min_price;
		$occasionId = null;
		if (!$occasionUrl)
		{
			$firstEvent = $this->event->first();
			if ($firstEvent)
			{
				$occasionId = $firstEvent->event->id;
			}
		}

		if ($fullUrl && $price)
		{
			$collectionCardInfo = $this->getCardInfo($fullUrl, $price, null, $occasionId);
		}

		if (!$collectionParams['occasionId'])
		{
			$collectionParams['occasionId'] = $occasionId;
		}

		$collectionCardInfo = array_merge($collectionCardInfo, $collectionParams);

		return $collectionCardInfo;
	}

	public function tags()
	{
		return $this->belongsToMany(TypeTag::class, 'service_tags', 'service_id', 'tag_id')
		            ->wherePivot('deleted_at', null);
	}
}