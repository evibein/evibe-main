<?php

namespace App\Models\Types;

use App\Models\BaseModel;

class TypeBookingConcept extends BaseModel
{
	protected $table = 'type_booking_concept';
}