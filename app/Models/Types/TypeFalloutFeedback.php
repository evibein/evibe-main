<?php

namespace App\Models\Types;

use App\Models\BaseModel;

class TypeFalloutFeedback extends BaseModel
{
	protected $table = 'type_fallout_feedback';
}