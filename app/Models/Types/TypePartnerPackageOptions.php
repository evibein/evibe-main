<?php

namespace App\Models\Types;

use App\Models\BaseModel;

class TypePartnerPackageOptions extends BaseModel
{
	protected $table = 'type_partner_options';
}