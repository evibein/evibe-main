<?php

namespace App\Models\Types;

use App\Models\BaseModel;

class TypeCakeFlavour extends BaseModel
{
	protected $table = 'type_cake_flavour';
}
