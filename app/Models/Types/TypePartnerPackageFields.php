<?php

namespace App\Models\Types;

use App\Models\BaseModel;

class TypePartnerPackageFields extends BaseModel
{
	protected $table = 'type_partner_options_fields';
}