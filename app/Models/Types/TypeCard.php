<?php

namespace App\Models\Types;

use App\Models\BaseModel;

class TypeCard extends BaseModel
{
	protected $table = 'type_card';
}