<?php

namespace App\Models\Types;

use App\Models\BaseModel;

class TypePackageField extends BaseModel
{
	protected $table = 'type_package_field';
}