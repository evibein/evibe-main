<?php

namespace App\Models\Types;

use App\Models\BaseModel;

class TypeTag extends BaseModel
{
	protected $table = 'type_tags';

	public function scopeForEvent($query, $eventId)
	{
		return $query->where(function ($innerQuery) use ($eventId)
		{
			$innerQuery->whereNull('type_event')
			           ->orWhere('type_event', $eventId);
		});
	}

	public function scopeForPage($query, $pageId)
	{
		if (is_array($pageId))
		{
			return $query->whereIn('map_type_id', $pageId);
		}

		return $query->where('map_type_id', $pageId);
	}

	public function scopeFilterable($query)
	{
		return $query->where('is_filter', 1);
	}
}