<?php

namespace App\Models\Types;

use App\Models\BaseModel;

class TypeVenue extends BaseModel
{
	protected $table = 'type_venue';
}
