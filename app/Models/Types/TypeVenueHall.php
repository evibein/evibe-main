<?php

namespace App\Models\Types;

use App\Models\BaseModel;

class TypeVenueHall extends BaseModel
{
	protected $table = 'type_venue_hall';
}