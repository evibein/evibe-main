<?php

/**
 * @author Harish Annavajjala <harish.annavajjala@evibe.in>
 * @since  5-Mar-2015
 */
namespace App\Models\Types;

use App\Models\BaseModel;

class TypePackageGallery extends BaseModel
{
	protected $table = 'type_package_gallery';
}
