<?php

namespace App\Models\Types;

use App\Models\BaseModel;

class TypeRSVP extends BaseModel
{
	protected $table = 'type_rsvp';
}