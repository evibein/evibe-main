<?php

namespace App\Models\Types;

use App\Models\BaseModel;

class TypeVenueMenu extends BaseModel
{
	protected $table = 'type_venue_menu';
}