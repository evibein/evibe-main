<?php

namespace App\Models\Types;

use App\Models\BaseModel;

class TypeCakeCategory extends BaseModel
{
	protected $table = 'type_cake_category';
}
