<?php

namespace App\Models\Types;

use App\Models\BaseModel;

class TypeVendor extends BaseModel
{
	protected $table = 'type_vendors';
}