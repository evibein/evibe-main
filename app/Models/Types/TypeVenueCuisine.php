<?php

namespace App\Models\Types;

use App\Models\BaseModel;

class TypeVenueCuisine extends BaseModel
{
	protected $table = 'type_venue_cuisine';
}