<?php

namespace App\Models\Types;

use App\Models\BaseModel;

class TypeCakeCategoryValue extends BaseModel
{
	protected $table = 'type_cake_category_value';
}
