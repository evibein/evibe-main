<?php

namespace App\Models\Types;

use App\Models\BaseModel;

class TypeVenueGallery extends BaseModel
{
	protected $table = 'type_venue_gallery';
}