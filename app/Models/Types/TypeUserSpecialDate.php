<?php

namespace App\Models\Types;

use App\Models\BaseModel;

class TypeUserSpecialDate extends BaseModel
{
	protected $table = 'type_user_special_date';
}