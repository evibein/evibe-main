<?php

namespace App\Models\Types;

use App\Models\BaseModel;

class TypeEvent extends BaseModel
{
	protected $table = 'type_event';
}