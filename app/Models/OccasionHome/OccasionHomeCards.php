<?php

namespace App\Models\OccasionHome;

use App\Models\BaseModel;

class OccasionHomeCards extends BaseModel
{
	protected $table = 'occasion_home_cards';
}