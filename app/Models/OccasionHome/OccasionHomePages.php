<?php

namespace App\Models\OccasionHome;

use App\Models\BaseModel;

class OccasionHomePages extends BaseModel
{
	protected $table = 'occasion_home_pages';
}