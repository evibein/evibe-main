<?php

namespace App\Models\OccasionHome;

use App\Models\BaseModel;

class OccasionHomeSections extends BaseModel
{
	protected $table = 'occasion_home_sections';
}