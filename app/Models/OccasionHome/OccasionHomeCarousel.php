<?php

namespace App\Models\OccasionHome;

use App\Models\BaseModel;

class OccasionHomeCarousel extends BaseModel
{
	protected $table = 'occasion_home_carousel';
}