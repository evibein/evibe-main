<?php

namespace App\Models\Wallet;

use App\Models\BaseModel;

class Wallet extends BaseModel
{
	/**
	 * @var array
	 */
	protected $fillable = [
		'user_id',
		'user_type',
		'slug',
		'description',
		'balance',
		'expires_on'
	];

	protected $table = 'wallets';
}