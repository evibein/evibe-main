<?php

namespace App\Models\Wallet;

use App\Models\BaseModel;

class Transaction extends BaseModel
{
	/**
	 * @var array
	 */
	protected $fillable = [
		'wallet_id',
		'user_id',
		'transaction_name',
		'type',
		'amount',
		'uuid',
		'confirmed',
		'expires_on',
		'deleted_at',
		'claimed_on'
	];

	/**
	 * @var array
	 */
	protected $table = "wallet_transactions";
}
