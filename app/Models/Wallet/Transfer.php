<?php

namespace App\Models\Wallet;

use App\Models\BaseModel;

class Transfer extends BaseModel
{
	/**
	 * @var array
	 */
	protected $fillable = [
		'status',
		'discount',
		'deposit_id',
		'withdraw_id',
		'from_type',
		'from_id',
		'to_type',
		'to_id',
		'uuid',
		'fee',
	];

	protected $table = "wallet_transfers";
}
