<?php

namespace App\Models\Wallet;

use App\Models\BaseModel;

class WalletRedeemed extends BaseModel
{
	protected $table = 'wallet_redeemed';
}