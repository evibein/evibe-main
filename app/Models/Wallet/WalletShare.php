<?php

namespace App\Models\Wallet;

use App\Models\BaseModel;

class WalletShare extends BaseModel
{
	protected $table = 'wallet_share_urls';
}