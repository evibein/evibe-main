<?php

namespace App\Models\Surprise;

use App\Models\BaseModel;
use App\Models\Types\TypeTag;

class SurpriseTagMappings extends BaseModel
{
	protected $table = 'surprise_tag_mappings';

	public function occasionTag()
	{
		return $this->belongsTo(TypeTag::class, 'so_id');
	}

	public function ageGroupTag()
	{
		return $this->belongsTo(TypeTag::class, 'sa_id');
	}
}