<?php

namespace App\Jobs\SMS\TMO;

use App\Jobs\SMS\UtilSendSMS;

class SendOTPToCustomer extends UtilSendSMS
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		if ($data["phone"] && ((int)$data["phone"] > 0))
		{
			$tplCustomer = config('evibe.sms_tpl.tmo.login.otp');
			$replaces = [
				'#field1#' => $data["otp"]
			];

			$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);
			$smsData = [
				'to'   => $data["phone"],
				'text' => $smsText
			];

			$this->sms($smsData);
		}
	}
}