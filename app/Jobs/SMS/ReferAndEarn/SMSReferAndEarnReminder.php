<?php

namespace App\Jobs\SMS\ReferAndEarn;

use App\Jobs\SMS\UtilSendSMS;
use App\Models\Util\User;

class SMSReferAndEarnReminder extends UtilSendSMS
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$ticket = $this->data;
		$customer = User::where("username", $ticket->email)->first();

		if ($customer && $customer->referral_code && $customer->phone)
		{
			$signUpUrl = config("evibe.host") . "/re/" . $customer->referral_code . "&ref=sms";

			$tplCustomer = config('evibe.sms_tpl.refer_earn.customer.reminder');
			$replaces = [
				'#field1#' => $customer->name,
				'#field2#' => "Rs 200",
				'#field3#' => $signUpUrl
			];

			$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);
			$smsData = [
				'to'   => $customer->phone,
				'text' => $smsText
			];

			$this->sms($smsData);
		}
	}
}
