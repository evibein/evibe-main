<?php

namespace App\Jobs\SMS\Landing;

use App\Jobs\SMS\UtilSendSMS;

class SMSSurpriseLandingUser extends UtilSendSMS
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		if ($data["phone"] && ((int)$data["phone"] > 0))
		{
			//if ($data["eventId"] == config("evibe.occasion.kids_birthdays.id"))
			//{
			//	$tplCustomer = config('evibe.sms_tpl.landing.kids');
			//}
			//else
			//{
			//	$tplCustomer = config('evibe.sms_tpl.landing.surprises');
			//}
			$tplCustomer = config('evibe.sms_tpl.landing.default');

			$replaces = [
				'#field1#' => "Kushi",
				'#field2#' => $data["referralCode"],
				'#field3#' => (isset($data["discountAmount"]) && $data["discountAmount"] > 0) ? $data["discountAmount"] : "50",
				'#field4#' => "www.evibe.in",
				'#field5#' => config("evibe.contact.company.phone")
			];

			$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);
			$smsData = [
				'to'   => $data["phone"],
				'text' => $smsText
			];

			$this->sms($smsData, "PROMOTIONAL");
		}
	}
}