<?php

namespace App\Jobs\SMS\Landing;

use App\Jobs\SMS\UtilSendSMS;

class SMSPiabLandingUser extends UtilSendSMS
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		if ($data["phone"] && ((int)$data["phone"] > 0))
		{
			$tplCustomer = config('evibe.sms_tpl.landing.piab');

			$replaces = [
				'#couponCode#'       => $data["referralCode"],
				'#discount#'         => "INR " . $data["discountAmount"],
				'#piabSupportEmail#' => config("evibe.contact.support.group")
			];

			$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);
			$smsData = [
				'to'   => $data["phone"],
				'text' => $smsText
			];

			$this->sms($smsData, "PROMOTIONAL");
		}
	}
}