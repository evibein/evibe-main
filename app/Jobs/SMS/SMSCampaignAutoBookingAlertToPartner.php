<?php

namespace App\Jobs\SMS;

class SMSCampaignAutoBookingAlertToPartner extends UtilSendSMS
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$this->sms($this->data);
	}
}
