<?php

namespace App\Jobs\SMS\Payment\Vendor;

use App\Jobs\SMS\UtilSendSMS;

class SMSAutoBookingPaymentAlertToPartnerJob extends UtilSendSMS
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$this->sms($this->data);
	}
}