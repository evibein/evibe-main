<?php

namespace App\Jobs;

use App\Models\BrowsingActivity\BrowsingActivity;
use App\Models\ViewCount\ProfileViewCounter;
use Carbon\Carbon;

class HandlePageViewCount extends BaseJob
{

	private $data;

	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 3;

	/**
	 * The number of seconds the job can run before timing out.
	 *
	 * @var int
	 */
	public $timeout = 120;

	public function __construct($data)
	{
		$this->data = $data;
		$this->onConnection("sqs_loggers")
		     ->onQueue(config('queue.connections.sqs_loggers.queue'));
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		// @see: Hard coded id(2851428) for now, for optimization
		$viewCounter = ProfileViewCounter::where("id", ">", "2851428")
		                                 ->where('created_at', '>=', Carbon::now()->startOfDay()->timestamp)
		                                 ->where('product_type_id', $data['productTypeId'])
		                                 ->where('product_id', $data['productId'])
		                                 ->where('start_time', $data["startTime"])
		                                 ->where('end_time', $data["endTime"])
		                                 ->first();

		if ($viewCounter)
		{
			$viewCounter->update(["total_views" => $viewCounter->total_views + 1]);
		}
		else
		{
			$viewCounter = ProfileViewCounter::create([
				                                          "product_type_id" => $data['productTypeId'],
				                                          "product_id"      => $data['productId'],
				                                          "start_time"      => $data["startTime"],
				                                          "end_time"        => $data["endTime"],
				                                          "total_views"     => 1
			                                          ]);
		}

		$viewCounterId = $viewCounter->id;
		$openedAt = Carbon::now()->timestamp;
		if ($data["userId"] > 0)
		{
			BrowsingActivity::create([
				                         'view_counter_profile_id' => $viewCounterId,
				                         'user_id'                 => $data["userId"],
				                         'start_time'              => $openedAt
			                         ]);
		}
		else
		{
			BrowsingActivity::create([
				                         'cookie_id'               => $data["cookieId"],
				                         'view_counter_profile_id' => $viewCounterId,
				                         'start_time'              => $openedAt
			                         ]);
		}
	}
}