<?php

namespace App\Jobs\Util;

use App\Jobs\BaseJob;
use Illuminate\Filesystem\Filesystem;
//use Pelago\Emogrifier as OriginalEmogrifier;

class AppendInlineCss extends BaseJob
{
	private $file;
	private $path;

	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 3;

	/**
	 * The number of seconds the job can run before timing out.
	 *
	 * @var int
	 */
	public $timeout = 120;

	public function __construct($file, $path)
	{
		$this->file = $file;
		$this->path = $path;
		//$this->onConnection("sqs_loggers")
		//     ->onQueue(config('queue.connections.sqs_loggers.queue'));
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$path = $this->path;
		$file = $this->file;

		$html = file_get_contents($path . "/raw_" . $file);
		$css = file_get_contents(public_path() . elixir('/build/css/page-about-us.css'));

		//$emogrifier = (new OriginalEmogrifier());
		//$emogrifier->setHtml($html);
		//$emogrifier->setCss($css);
		//$emogrifier->disableInvisibleNodeRemoval();

		//$output = $emogrifier->emogrify();
		//
		//$files = (new Filesystem());
		//
		//$files->makeDirectory($path, 0777, true, true);
		//$files->put($path . "/" . $file, $output, true);
	}
}