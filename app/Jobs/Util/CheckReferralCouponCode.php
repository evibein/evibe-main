<?php

namespace App\Jobs\Util;

use App\Jobs\BaseJob;
use App\Jobs\Emails\ReferAndEarn\SendEmailToCustomerOnReferralBookingSuccess;
use App\Models\Referral\LogReferralFriendActions;
use App\Models\Ticket\Ticket;
use App\Models\Util\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class CheckReferralCouponCode extends BaseJob
{

	private $email;

	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 3;

	/**
	 * The number of seconds the job can run before timing out.
	 *
	 * @var int
	 */
	public $timeout = 120;

	public function __construct($email)
	{
		$this->email = $email;
		$this->onConnection("sqs_loggers")
		     ->onQueue(config('queue.connections.sqs_loggers.queue'));
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$email = $this->email;
		$user = User::where("username", $email)->first();

		if ($user && $user->referred_by && !is_null($user->referred_by))
		{
			$validPaymentStatus = [config("evibe.ticket.status.booked"), config("evibe.ticket.status.auto_paid"), config("evibe.ticket.status.service_auto_pay")];
			$bookedTickets = Ticket::where("email", $email)
			                       ->whereIn("status_id", $validPaymentStatus)
			                       ->whereNotNull("paid_at")
			                       ->get()
			                       ->count();

			if ($bookedTickets == 1)
			{
				$logReferralBooking = LogReferralFriendActions::create([
					                                                       "customer_id"         => $user->referred_by,
					                                                       "friend_id"           => $user->id,
					                                                       "type_referral_event" => "book"
				                                                       ]);

				if (!$logReferralBooking)
				{
					$this->sendNonExceptionErrorReportToTeam([
						                                         'message' => 'Error occurred while logging data into log_referral_friend_actions',
						                                         'details' => 'Error occurred while logging success booking data into log_referral_friend_actions for the customer with email Id' . $email
					                                         ]);
				}

				dispatch(new SendEmailToCustomerOnReferralBookingSuccess([
					                                                         "user" => $user,
				                                                         ]));
			}
		}
	}
}