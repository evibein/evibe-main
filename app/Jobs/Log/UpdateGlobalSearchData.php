<?php

namespace App\Jobs\Log;

use App\Jobs\BaseJob;
use App\Models\Search\SearchableProduct;
use Illuminate\Support\Facades\Log;

class UpdateGlobalSearchData extends BaseJob
{
	private $data;
	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 3;

	public function __construct($options)
	{
		$this->data = $options;
		$this->onConnection("sqs_loggers")
		     ->onQueue(config('queue.connections.sqs_loggers.queue'));
	}

	/**
	 * The number of seconds the job can run before timing out.
	 *
	 * @var int
	 */
	public $timeout = 120;

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$searchResults = $this->data;

		try
		{
			foreach ($searchResults as $key => $searchResult)
			{
				$searchProduct = SearchableProduct::find($key);
				if ($searchProduct)
				{
					$searchProduct->update([
						                       "profile_url"   => $searchResult["fullUrl"],
						                       "profile_image" => $searchResult["profileImg"]
					                       ]);
				}
			}
		} catch (\Exception $e)
		{
			$this->sendFailedJobsToTeam($e);
		}
	}

	public function failed(\Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}
