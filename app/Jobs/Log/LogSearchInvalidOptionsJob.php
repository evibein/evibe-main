<?php

namespace App\Jobs\Log;

use App\Jobs\BaseJob;
use App\Models\Log\LogInvalidSearchOption;
use Illuminate\Support\Facades\Log;

class LogSearchInvalidOptionsJob extends BaseJob
{
	private $data;
	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 3;

	/**
	 *
	 * @param array $options
	 *
	 */
	public function __construct($options)
	{
		$this->data = $options;
		$this->onConnection("sqs_loggers")
		     ->onQueue(config('queue.connections.sqs_loggers.queue'));
	}

	/**
	 * The number of seconds the job can run before timing out.
	 *
	 * @var int
	 */
	public $timeout = 120;

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$logInsertData = [
			"product_type_id"    => $data["productTypeId"],
			"product_subtype_id" => $data["productSubtypeId"],
			"product_id"         => $data["productId"],
			"error_message"      => $data["errorMessage"]
		];

		try
		{
			LogInvalidSearchOption::create($logInsertData);
		} catch (\Exception $e)
		{
			Log::error('*** - ERROR BEGIN - *****');
			Log::error("failed to save invalid option in. Insert data show below");
			Log::error($logInsertData);
			Log::error('*** - ERROR END - *****');
		}
	}

	public function failed()
	{
		Log::error('*** - ERROR BEGIN - *****');
		Log::error("LogSearchInvalidOptions Job failed to save log search keyword.");
		Log::error('*** - ERROR END - *****');

		// @todo: notify email to sysadmin
	}
}
