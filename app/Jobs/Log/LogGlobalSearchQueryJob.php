<?php

namespace App\Jobs\Log;

use App\Jobs\BaseJob;
use App\Models\Log\LogSearchKeyword;
use Illuminate\Support\Facades\Log;

class LogGlobalSearchQueryJob extends BaseJob
{
	private $keyword;
	private $cityId;
	private $eventId;
	private $pageId;
	private $pageUrl;
	private $isGlobalSearch;

	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 3;

	/**
	 * LogGlobalSearchQueryJob constructor.
	 *
	 * Create a new job instance.
	 *
	 * @param array $options
	 *
	 */
	public function __construct($options = [])
	{
		if (!count($options))
		{
			return;
		}

		if (isset($options['keyword']))
		{
			$this->keyword = $options['keyword'];
		}

		if (isset($options['cityId']))
		{
			$this->cityId = $options['cityId'];
		}

		if (isset($options['eventId']))
		{
			$this->eventId = $options['eventId'];
		}

		if (isset($options['pageId']))
		{
			$this->pageId = $options['pageId'];
		}

		if (isset($options['pageUrl']))
		{
			$this->pageUrl = $options['pageUrl'];
		}

		if (isset($options['isGlobalSearch']))
		{
			$this->isGlobalSearch = $options['isGlobalSearch'];
		}

		$this->onConnection("sqs_loggers")
		     ->onQueue(config('queue.connections.sqs_loggers.queue'));
	}

	/**
	 * The number of seconds the job can run before timing out.
	 *
	 * @var int
	 */
	public $timeout = 120;

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$logInsertData = [
			'keyword'          => $this->keyword,
			'city_id'          => $this->cityId ? $this->cityId : null,
			'event_id'         => $this->eventId ? $this->eventId : null,
			'page_id'          => $this->pageId ? $this->pageId : null,
			'page_url'         => $this->pageUrl ? $this->pageUrl : " -- ",
			'is_global_search' => $this->isGlobalSearch ? $this->isGlobalSearch : 0
		];

		try
		{
			LogSearchKeyword::create($logInsertData);
		} catch (\Exception $e)
		{
			Log::error('*** - ERROR BEGIN - *****');
			Log::error("failed to save log search keyword. Insert data show below");
			Log::error($logInsertData);
			Log::error('*** - ERROR END - *****');
		}
	}

	public function failed()
	{
		Log::error('*** - ERROR BEGIN - *****');
		Log::error("LogGlobalSearchQueryJob Job failed to save log search keyword.");
		Log::error('*** - ERROR END - *****');
	}
}
