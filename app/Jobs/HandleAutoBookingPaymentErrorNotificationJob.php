<?php

namespace App\Jobs;

use App\Jobs\Emails\Payment\BaseMailerJob;
use Illuminate\Support\Facades\Mail;

class HandleAutoBookingPaymentErrorNotificationJob extends BaseMailerJob
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = [
			'code'     => $this->data['code'],
			'title'    => '<p>' . $this->data['title'] . '</p>',
			'details'  => '<p>' . $this->data['details'] . '</p>',
			'ticketId' => $this->data['ticketId'],
			'type'     => $this->data['type'],
			'sub'      => $this->data['sub']
		];

		Mail::send('emails.report.auto_booking_action', ['data' => $data], function ($m) use ($data)
		{
			$m->from(config('evibe.contact.company.email'), 'Error')
			  ->to(config('evibe.contact.tech.group'))
			  ->cc(config('evibe.contact.customer.group'))
			  ->subject($data['sub']);
		});
	}
}
