<?php

namespace App\Jobs;

use App\Jobs\Emails\Payment\BaseMailerJob;
use App\Models\Util\CheckoutField;
use App\Models\Util\CheckoutFieldValue;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class HandleAutoBookingNotificationToApp extends BaseMailerJob
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $accessToken;
	private $booking;

	public function __construct($booking, $accessToken)
	{
		$this->booking = $booking;
		$this->accessToken = $accessToken;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$booking = $this->booking;
		$mapping = $booking->mapping;
		$typeId = $booking->type_ticket_booking_id;
		$ticket = $booking->ticket;
		$eventId = $ticket->event_id;

		$apiData = [
			'enquiry-type'            => config('evibe.enquiry.type.avlCheck'),
			'avl-party-date'          => date("Y/m/d, H:i:s", $booking->party_date_time),
			'avl-booking-cat'         => $booking->type_ticket_booking_id,
			'avl-budget-price'        => $booking->booking_amount,
			'avl-booking-cat-details' => $booking->typeBooking ? $booking->typeBooking->name : "Auto Booking",
			'avl-booking-info'        => $booking->booking_info,
			'isAutoBooking'           => 1,
			'createdBy'               => $booking->provider ? $booking->provider->user_id : null
		];

		$crmFields = CheckoutField::where('type_ticket_booking_id', $typeId)
		                          ->where(function ($query) use ($eventId)
		                          {
			                          $query->where('event_id', $eventId)
			                                ->orWhereNull('event_id');
		                          })
		                          ->where('is_crm', 1)
		                          ->get();

		foreach ($crmFields as $crmField)
		{
			$apiData[$crmField->name] = $crmField->getCheckoutValue($ticket->id, $booking->id) ? $crmField->getCheckoutValue($ticket->id, $booking->id) : 'n/a';
		}

		$url = config('evibe.api.base_url') . config('evibe.api.partner.prefix') . 'enquiries/create/ask/' . $mapping->id;

		try
		{
			$client = new Client();
			$response = $client->request('POST', $url, [
				'headers' => [
					'access-token' => $this->accessToken
				],
				'json'    => $apiData
			]);
			$response = $response->getBody();
			$response = \GuzzleHttp\json_decode($response, true);

			if (empty($response['success']) || !$response['success'])
			{
				$this->reportAutoBookingErrorToTeam($response['error']);
			}
		} catch (ClientException $e)
		{
			$this->reportAutoBookingErrorToTeam($e);
		}
	}

	private function reportAutoBookingErrorToTeam($e)
	{
		if (is_object($e))
		{
			$data = [
				'code'    => '<p>' . $e->getCode() . '</p>',
				'title'   => '<p>' . $e->getMessage() . '</p>',
				'details' => '<p>' . $e->getTraceAsString() . '</p>'
			];
		}
		else
		{
			$data = [
				'custError' => $e
			];
		}
		$data['sub'] = '[Error] [URGENT] Auto booking error while creating new enquiry.';

		Mail::send('emails.report.auto_booking_enquiry', ['data' => $data], function ($m) use ($data)
		{
			$m->from(config('evibe.contact.company.email'), 'Error')
			  ->to(config('evibe.contact.tech.group'))
			  ->subject($data['sub']);
		});
	}
}
