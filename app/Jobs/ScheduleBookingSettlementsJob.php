<?php

namespace App\Jobs;

use App\Http\Controllers\Partner\BasePartnerController;
use App\Jobs\Emails\Payment\BaseMailerJob;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketBooking;
use App\Models\Util\User;
use Carbon\Carbon;
use GuzzleHttp\Client;

class ScheduleBookingSettlementsJob extends BaseMailerJob
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		if (isset($data['ticketId']) && $data['ticketId'])
		{
			$ticketId = $data['ticketId'];
			$ticket = Ticket::find($ticketId);
			if ($ticket)
			{
				$ticketBookings = TicketBooking::where('ticket_id', $ticketId)
				                               ->where('is_advance_paid', 1)
				                               ->whereNull('deleted_at')
				                               ->get();

				if (count($ticketBookings))
				{
					// create or fetch user.
					$userId = null;
					$accessUser = null;
					$existingUser = User::where('username', $ticket->email)
					                    ->orWhere('phone', $ticket->phone)
					                    ->first();

					if ($existingUser)
					{
						$accessUser = $existingUser;
						$userId = $existingUser->id;
					}
					else
					{
						$newUser = User::create([
							                        'username'   => $ticket->email,
							                        'password'   => bcrypt($ticket->phone),
							                        'phone'      => $ticket->phone,
							                        'name'       => $ticket->name,
							                        'role_id'    => config('evibe.roles.customer'),
							                        'created_at' => Carbon::now(),
							                        'updated_at' => Carbon::now()
						                        ]);

						if ($newUser)
						{
							$userId = $newUser->id;
							$accessUser = $newUser;
						}
					}

					if ($userId)
					{
						$ticket->user_id = $userId;
						$ticket->save();
					}
					else
					{
						$this->sendNonExceptionErrorReportToTeam([
							                                         'code'      => config('evibe.error_code.create_settlement_booking'),
							                                         'url'       => request()->fullUrl(),
							                                         'method'    => request()->method(),
							                                         'message'   => '[ScheduleBookingSettlementsJob.php - ' . __LINE__ . '] Unable to find or create userId',
							                                         'exception' => '[ScheduleBookingSettlementsJob.php - ' . __LINE__ . '] Unable to find or create userId',
							                                         'trace'     => '[Ticket Id: ' . $ticketId . ']',
							                                         'details'   => '[Ticket Id: ' . $ticketId . ']'
						                                         ]);

						// default - Hack to provide user id for access-token purpose
						$userId = config('evibe.default.access_token_handler');
					}

					// @see: commented trigger as another update trigger is in place while sending receipts
					/*
					foreach ($ticketBookings as $ticketBooking)
					{
						// make api trigger
						$url = config('evibe.api.base_url') . config('evibe.api.finance.prefix') . 'bookings/' . $ticketBooking->id . '/create';
						$method = new BasePartnerController();
						$accessUser = $accessUser ?: User::find($userId);
						$accessToken = $method->getAccessTokenFromUserId($accessUser);

						// api call to create schedule settlement booking
						try
						{
							$client = new Client();
							$res = $client->request('POST', $url, [
								'headers' => [
									'access-token' => $accessToken
								],
								'json'    => ''
							]);

							$res = $res->getBody();
							$res = \GuzzleHttp\json_decode($res, true);

							if (isset($res['success']) && !$res['success'])
							{
								$error = isset($res['error']) ? $res['error'] : 'Some error occurred while creating schedule settlement booking';
								$this->sendNonExceptionErrorReportToTeam([
									                                         'code'      => config('evibe.error_code.create_settlement_booking'),
									                                         'url'       => request()->fullUrl(),
									                                         'method'    => request()->method(),
									                                         'message'   => '[ScheduleBookingSettlementsJob.php - ' . __LINE__ . '] ' . $error,
									                                         'exception' => '[ScheduleBookingSettlementsJob.php - ' . __LINE__ . '] ' . $error,
									                                         'trace'     => '[Ticket Id: ' . $ticketId . '] [Ticket Booking Id: ' . $ticketBooking->id . ']',
									                                         'details'   => '[Ticket Id: ' . $ticketId . '] [Ticket Booking Id: ' . $ticketBooking->id . ']'
								                                         ]);
							}
						} catch (\Exception $e)
						{
							$this->sendErrorReportToTeam($e);
						}
					}
					*/
				}
				else
				{
					$this->sendNonExceptionErrorReportToTeam([
						                                         'code'      => config('evibe.error_code.create_settlement_booking'),
						                                         'url'       => request()->fullUrl(),
						                                         'method'    => request()->method(),
						                                         'message'   => '[ScheduleBookingSettlementsJob.php - ' . __LINE__ . '] There are no bookings for this ticket',
						                                         'exception' => '[ScheduleBookingSettlementsJob.php - ' . __LINE__ . '] There are no bookings for this ticket',
						                                         'trace'     => '[Ticket Id: ' . $ticketId . ']',
						                                         'details'   => '[Ticket Id: ' . $ticketId . ']'
					                                         ]);
				}
			}
			else
			{
				$this->sendNonExceptionErrorReportToTeam([
					                                         'code'      => config('evibe.error_code.create_settlement_booking'),
					                                         'url'       => request()->fullUrl(),
					                                         'method'    => request()->method(),
					                                         'message'   => '[ScheduleBookingSettlementsJob.php - ' . __LINE__ . '] Unable to find ticket to create schedule settlement booking',
					                                         'exception' => '[ScheduleBookingSettlementsJob.php - ' . __LINE__ . '] Unable to find ticket to create schedule settlement booking',
					                                         'trace'     => '[Ticket Id: ' . $ticketId . ']',
					                                         'details'   => '[Ticket Id: ' . $ticketId . ']'
				                                         ]);
			}
		}
		else
		{
			$this->sendNonExceptionErrorReportToTeam([
				                                         'code'      => config('evibe.error_code.create_settlement_booking'),
				                                         'url'       => request()->fullUrl(),
				                                         'method'    => request()->method(),
				                                         'message'   => '[ScheduleBookingSettlementsJob.php - ' . __LINE__ . '] Unable to fetch ticket id to create schedule settlement booking',
				                                         'exception' => '[ScheduleBookingSettlementsJob.php - ' . __LINE__ . '] Unable to fetch ticket id to create schedule settlement booking',
				                                         'trace'     => 'Cannot get ticket Id',
				                                         'details'   => 'Cannot get ticket Id'
			                                         ]);
		}
	}

	public function failed(\Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}
