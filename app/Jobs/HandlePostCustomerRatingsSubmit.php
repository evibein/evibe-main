<?php

namespace App\Jobs;

class HandlePostCustomerRatingsSubmit extends BaseJob
{
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	public function handle()
	{

	}

	public function failed(\Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
} 