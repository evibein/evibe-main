<?php

namespace App\Jobs;

use App\Http\Controllers\Partner\BasePartnerController;
use App\Jobs\Emails\Payment\BaseMailerJob;
use App\Models\Ticket\TicketReminderStack;
use App\Models\Util\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class HandleUpdatingTicketAutoFollowup extends BaseMailerJob
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		if (isset($data['ticketId']) && $data['ticketId'])
		{
			$ticketId = $data['ticketId'];
			$ticketReminder = TicketReminderStack::join('type_reminder_group', 'type_reminder_group.id', '=', 'ticket_reminders_stack.type_reminder_group_id')
			                                     ->whereNull('type_reminder_group.deleted_at')
			                                     ->whereNull('ticket_reminders_stack.deleted_at')
			                                     ->whereNull('ticket_reminders_stack.invalid_at')
			                                     ->whereNull('ticket_reminders_stack.terminated_at')
			                                     ->where('ticket_id', $data['ticketId'])
			                                     ->select('ticket_reminders_stack.*', 'type_reminder_group.type_reminder_id')
			                                     ->first();

			if ($ticketReminder)
			{
				$url = config('evibe.api.base_url') . config('evibe.api.auto-followups.prefix') . $ticketId . '/recalculate/' . $ticketReminder->type_reminder_id;
				$method = new BasePartnerController();

				// taking default handler for system updates
				$accessToken = $method->getAccessTokenFromUserId(User::find(config('evibe.default.access_token_handler')));

				try
				{
					$client = new Client();
					$res = $client->request('PUT', $url, [
						'headers' => [
							'access-token' => $accessToken
						],
						'json'    => ''
					]);

					$res = $res->getBody();
					$res = \GuzzleHttp\json_decode($res, true);
					if (isset($res['success']) && !$res['success'])
					{
						Log::error("failed to update auto followup for the ticket: " . $ticketId);
					}

				} catch (\Exception $exception)
				{
					Log::error($exception->getMessage());
				}
			}
		}
	}
}
