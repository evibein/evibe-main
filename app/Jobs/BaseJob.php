<?php

namespace App\Jobs;

use App\Models\Ticket\Ticket;
use App\Models\Util\SiteErrorLog;
use Evibe\Facades\EvibeUtilFacade as AppUtil;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpKernel\Exception\HttpException;

class BaseJob extends Job implements ShouldQueue
{
	use InteractsWithQueue, SerializesModels;

	public function formatPrice($value)
	{
		return AppUtil::formatPrice($value);
	}

	public function formatPriceWithoutChar($price)
	{
		return AppUtil::formatPriceWithoutChar($price);
	}

	public function getShortUrl($longUrl)
	{
		return AppUtil::getShortUrl($longUrl);
	}

	public function getTruncateString($string)
	{
		return AppUtil::getTruncateString($string);
	}

	public function getVenueMapLink($venue)
	{
		$baseMapLink = "https://www.google.com/maps/place/";
		if (!$venue)
		{
			return "";
		}

		$venueMapLink = $baseMapLink . str_replace(" ", "+", $venue->name);
		if ($venue->map_lat && $venue->map_long)
		{
			$venueMapLink = $baseMapLink . $venue->map_lat . "+" . $venue->map_long;
			$venueMapLink .= "/" . $venue->map_lat . "," . $venue->map_long . ",17z";
		}

		return $venueMapLink;
	}

	public function sendFailedJobsToTeam($exception)
	{
		$data = [
			"from"       => config('evibe.contact.company.email'),
			"to"         => config('evibe.contact.tech.group'),
			"errorTitle" => "Default Error Title",
			"code"       => "Default Code",
			"details"    => "Default Details",
			"file"       => "Default File",
			"line"       => "Default Line"
		];

		if (!is_null($exception) && $exception instanceof \Exception)
		{
			$data['errorTitle'] = $exception->getMessage();
			$data['code'] = $exception->getCode();
			$data['details'] = $exception->getTraceAsString();
			$data['file'] = $exception->getFile();
			$data['line'] = $exception->getLine();
		}

		$data['project'] = 'MAIN';
		$data['sub'] = '[Failed Job] - ' . date('F j, Y, g:i a');

		Mail::send('app.failed-jobs', ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.contact.company.email'), 'Evibe Errors')
			  ->to($data['to'])
			  ->subject($data['sub']);
		});

		return true;
	}

	public function sendErrorReportToTeam($e, $sendEmail = true)
	{
		return AppUtil::sendErrorReportToTeam($e, $sendEmail);
	}

	public function sendNonExceptionErrorReportToTeam($data, $sendEmail = true)
	{
		return AppUtil::sendNonExceptionErrorReportToTeam($data, $sendEmail);
	}

	public function getEnquiryIdForEmailSubject($ticketId)
	{
		$ticket = Ticket::find($ticketId);

		if (!$ticket)
		{
			return "";
		}

		// Get the first booked ticket, if there are multiple tickets with same phone & party date
		$partyDate = date("y-m-d", $ticket->event_date);
		$similarTicket = Ticket::where("phone", $ticket->phone)
		                       ->where("status_id", config("evibe.ticket.status.booked"))
		                       ->whereBetween("event_date", [strtotime($partyDate) - 1, strtotime($partyDate) + 86401])
		                       ->orderBy("paid_at")
		                       ->get()
		                       ->first();

		if ($similarTicket)
		{
			$ticket = $similarTicket;
		}

		$enquiryId = $ticket->enquiry_id;
		if (is_null($enquiryId) || $enquiryId == "")
		{
			$enquiryId = "EVB-BK-" . $ticket->id;

			$ticket->update(["enquiry_id" => $enquiryId]);
		}

		return " - #" . $enquiryId;
	}
}