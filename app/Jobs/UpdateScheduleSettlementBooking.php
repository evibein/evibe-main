<?php

namespace App\Jobs;

use App\Http\Controllers\Partner\BasePartnerController;
use App\Jobs\Emails\Payment\BaseMailerJob;
use App\Models\Util\User;
use GuzzleHttp\Client;

class UpdateScheduleSettlementBooking extends BaseMailerJob
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$ticketBookingId = isset($data['ticketBookingId']) && $data['ticketBookingId'] ? $data['ticketBookingId'] : null;
		if($ticketBookingId)
		{
			$handler = isset($data['handler']) && $data['handler'] ? $data['handler'] : null;
			if(!$handler)
			{
				// report error
				$this->sendNonExceptionErrorReportToTeam([
					                                         'code'      => config('evibe.error_code.update_settlement_booking'),
					                                         'url'       => request()->fullUrl(),
					                                         'method'    => request()->method(),
					                                         'message'   => '[UpdateScheduleSettlementBooking.php - ' . __LINE__ . '] ' . 'Unable to find handler for the ticket booking. So, updating with default handler.',
					                                         'exception' => '[UpdateScheduleSettlementBooking.php - ' . __LINE__ . '] ' . 'Unable to find handler for the ticket booking. So, updating with default handler.',
					                                         'trace'     => '[Ticket Booking Id: ' . $ticketBookingId . ']',
					                                         'details'   => '[Ticket Booking Id: ' . $ticketBookingId . ']'
				                                         ]);

				// default handler
				$handler = User::find(config('evibe.default.access_token_handler'));
			}

			// make api trigger
			$url = config('evibe.api.base_url') . config('evibe.api.finance.prefix') . 'bookings/' . $ticketBookingId . '/update';
			$method = new BasePartnerController();
			$accessToken = $method->getAccessTokenFromUserId($handler);

			// api call to update schedule settlement booking
			try
			{
				$client = new Client();
				$res = $client->request('PUT', $url, [
					'headers' => [
						'access-token' => $accessToken
					],
					'json'    => ''
				]);

				$res = $res->getBody();
				$res = \GuzzleHttp\json_decode($res, true);

				if (isset($res['success']) && !$res['success'])
				{
					$error = isset($res['error']) ? $res['error'] : 'Some error occurred while updating schedule settlement booking';
					$this->sendNonExceptionErrorReportToTeam([
						                                         'code'      => config('evibe.error_code.update_settlement_booking'),
						                                         'url'       => request()->fullUrl(),
						                                         'method'    => request()->method(),
						                                         'message'   => '[UpdateScheduleSettlementBooking.php - ' . __LINE__ . '] ' . $error,
						                                         'exception' => '[UpdateScheduleSettlementBooking.php - ' . __LINE__ . '] ' . $error,
						                                         'trace'     => '[Ticket Booking Id: ' . $ticketBookingId . ']',
						                                         'details'   => '[Ticket Booking Id: ' . $ticketBookingId . ']'
					                                         ]);
				}
			} catch (\Exception $e)
			{
				$this->sendErrorReportToTeam($e);
			}

		}
		else
		{
			$this->sendNonExceptionErrorReportToTeam([
				                                         'code'      => config('evibe.error_code.update_settlement_booking'),
				                                         'url'       => request()->fullUrl(),
				                                         'method'    => request()->method(),
				                                         'message'   => '[UpdateScheduleSettlementBooking.php - ' . __LINE__ . '] ' . 'Unable to find ticket booking from ticket booking id.',
				                                         'exception' => '[UpdateScheduleSettlementBooking.php - ' . __LINE__ . '] ' . 'Unable to find ticket booking from ticket booking id.',
				                                         'trace'     => '[Ticket Booking Id: ' . $ticketBookingId . ']',
				                                         'details'   => '[Ticket Booking Id: ' . $ticketBookingId . ']'
			                                         ]);
		}
	}

	public function failed(\Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}
