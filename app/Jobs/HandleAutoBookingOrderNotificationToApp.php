<?php

namespace App\Jobs;

use App\Jobs\Emails\Payment\BaseMailerJob;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketBooking;
use App\Models\Util\CheckoutField;
use App\Models\Util\CheckoutFieldValue;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class HandleAutoBookingOrderNotificationToApp extends BaseMailerJob
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $accessToken;
	private $bookings;
	private $ticket;

	public function __construct($ticket, $bookings, $accessToken)
	{
		$this->ticket = $ticket;
		$this->bookings = $bookings;
		$this->accessToken = $accessToken;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$bookings = $this->bookings;
		$ticket = $this->ticket;
		$eventId = $ticket->event_id;

		// todo: change
		$firstBooking = $bookings->first();
		$firstMapping = $firstBooking->mapping;

		$bookingCategory = '';
		$bookingAmount = 0;
		$bookingInfo = '';
		$partnerUserId = null;
		$typeTicketBookingIds = [];
		foreach ($bookings as $booking)
		{
			$bookingCategory .= $booking->type_ticket_booking_id . '-';
			$bookingAmount += $booking->booking_amount;
			$bookingInfo .= $booking->booking_info . ', ';
			// todo: assuming that notification is for bookings of single partner
			$partnerUserId = $partnerUserId ?: ($booking->provider ? $booking->provider->user_id : null);

			array_push($typeTicketBookingIds, $booking->type_ticket_booking_id);
		}
		$bookingCategory = rtrim($bookingCategory, '-');
		$bookingInfo = rtrim($bookingInfo, ', ');

		$apiData = [
			'enquiry-type'            => config('evibe.enquiry.type.avlCheck'),
			'avl-party-date'          => date("Y/m/d, H:i:s", $ticket->event_date),
			'avl-booking-cat'         => $bookingCategory,
			'avl-budget-price'        => $bookingAmount,
			'avl-booking-cat-details' => "Auto Booking",
			'avl-booking-info'        => $bookingInfo,
			'isAutoBooking'           => 1,
			'createdBy'               => $partnerUserId
		];

		/* As the data is being sent to API, do not change this for now. */
		/*
		$checkoutFields = null;
		$bookingCheckoutFields = null;
		foreach ($bookings as $booking)
		{
			$bookingCheckoutFields = CheckoutField::select('checkout_field.*', DB::raw($booking->id . ' as ticket_booking_id'))
			                                      ->where('type_ticket_booking_id', $booking->type_ticket_booking_id)
			                                      ->where(function ($query) use ($eventId) {
				                                      $query->where('event_id', $eventId)
				                                            ->orWhereNull('event_id');
			                                      })
			                                      ->where('is_crm', 1)
			                                      ->get();
			Log::info("book checkout fields: " . $bookingCheckoutFields);

			if (!$checkoutFields)
			{
				$checkoutFields = $bookingCheckoutFields;
			}
			elseif ($checkoutFields && $bookingCheckoutFields)
			{
				$checkoutFields = $checkoutFields->toBase()->merge($bookingCheckoutFields);
			}
		}

		foreach ($checkoutFields as $key => $checkoutField)
		{
			$checkoutFields[$key]->booking_checkout_unique_id = $checkoutField->ticket_booking_id . '_' . $checkoutField->id;
		}

		foreach ($checkoutFields as $crmField)
		{
			$apiData[$crmField->name] = $crmField->getCheckoutValue($ticket->id) ? $crmField->getCheckoutValue($ticket->id) : 'n/a';
		}
		*/

		$crmFields = CheckoutField::whereIn('type_ticket_booking_id', $typeTicketBookingIds)
		                          ->where(function ($query) use ($eventId) {
			                          $query->where('event_id', $eventId)
			                                ->orWhereNull('event_id');
		                          })
		                          ->where('is_crm', 1)
		                          ->get();

		foreach ($crmFields as $crmField)
		{
			$apiData[$crmField->name] = $crmField->getCheckoutValue($ticket->id) ? $crmField->getCheckoutValue($ticket->id) : 'n/a';
		}

		$url = config('evibe.api.base_url') . config('evibe.api.partner.prefix') . 'enquiries/create/ask/' . $firstMapping->id;

		try
		{
			$client = new Client();
			$response = $client->request('POST', $url, [
				'headers' => [
					'access-token' => $this->accessToken
				],
				'json'    => $apiData
			]);
			$response = $response->getBody();
			$response = \GuzzleHttp\json_decode($response, true);

			if (empty($response['success']) || !$response['success'])
			{
				$this->reportAutoBookingErrorToTeam($response['error']);
			}
		} catch (ClientException $e)
		{
			$this->reportAutoBookingErrorToTeam($e);
		}
	}

	private function reportAutoBookingErrorToTeam($e)
	{
		if (is_object($e))
		{
			$data = [
				'code'    => '<p>' . $e->getCode() . '</p>',
				'title'   => '<p>' . $e->getMessage() . '</p>',
				'details' => '<p>' . $e->getTraceAsString() . '</p>'
			];
		}
		else
		{
			$data = [
				'custError' => $e
			];
		}
		$data['sub'] = '[Error] [URGENT] Auto booking error while creating new enquiry.';

		Mail::send('emails.report.auto_booking_enquiry', ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.contact.company.email'), 'Error')
			  ->to(config('evibe.contact.tech.group'))
			  ->subject($data['sub']);
		});
	}
}