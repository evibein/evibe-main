<?php

namespace App\Jobs;

use App\Http\Controllers\Payments\TicketBookingPaymentController;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketBooking;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use App\Jobs\Emails\Payment\Team\MailNewBookingConceptAlertToTeamJob;
use App\Models\Ticket\TicketBookingGallery;
use App\Models\Types\TypeBookingConcept;
use Illuminate\Support\Facades\Mail;

class HandlePostPaymentProcessJob extends BaseJob
{
	private $ticket;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($ticketId)
	{
		$ticket = Ticket::with('bookings')->find($ticketId);
		if ($ticket)
		{
			$this->ticket = $ticket;
		}
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$ticket = $this->ticket;
		if (!$ticket)
		{
			return;
		}

		// delete followups
		$ticket->followups()->update(['is_complete' => 1, 'updated_at' => date('Y-m-d H:i:s')]);

		/**
		 * send alert to operational team regarding new booking concept if its alert-able
		 *
		 * @author: Jeevan Reddy <jeevan.reddy@evibe.in>
		 * @since : 12 Apr 2017
		 */
		$ticketBookings = $ticket->bookings;
		$alertTypeBookingIds = TypeBookingConcept::where('is_operational_alert', 1)
		                                         ->pluck('id')
		                                         ->toArray();

		foreach ($ticketBookings as $booking)
		{
			$provider = $booking->provider;

			if (!$provider)
			{
				continue;
			}

			if (in_array($booking->type_booking_concept_id, $alertTypeBookingIds))
			{
				$bookingImages = TicketBookingGallery::where('ticket_booking_id', $booking->id)->get();
				$conceptData = [
					'person'    => $provider->name,
					'ticket'    => $ticket,
					'booking'   => $booking,
					'partyDate' => date('d-m-y,H:i:s', $booking->party_date_time),
					'images'    => $bookingImages,
				];

				dispatch(new MailNewBookingConceptAlertToTeamJob($conceptData));
			}

			// for auto bookings, pay success is not booking
			if (!$ticket->is_auto_booked)
			{
				// check if first booking, alert partner
				$existingOrders = TicketBooking::where('map_id', $booking->map_id)
				                               ->where('map_type_id', $booking->map_type_id)
				                               ->where('ticket_id', '!=', $ticket->id)
				                               ->where('is_advance_paid', config('evibe.ticket.advance.paid'))
				                               ->get();

				if ($existingOrders->count() == 0)
				{
					$url = config('evibe.api.base_url') . config('evibe.api.pay_notification') . 'first-booking/' . $booking->id;
					$token = Hash::make($booking->id . $booking->created_at);

					try
					{
						$client = new Client();
						$client->request('POST', $url, [
							'json' => [
								'token' => $token
							]
						]);

					} catch (ClientException $e)
					{
						// report admin in case of error
						$this->reportTeam($e, $ticket, $provider);
					}
				}

				// send app notification to partner
				try
				{
					$ticketPaymentObj = new TicketBookingPaymentController();
					$ticketPaymentObj->sendAppNotificationToPartnerOnNewOrder($booking);
				} catch (\Exception $e)
				{
					Log::error("Error while sending the new order receipt for booking id :" . $booking->id);
				}
			}
		}
	}

	private function reportTeam($e, $ticket, $provider)
	{
		$sub = "[URGENT] [ERROR] :: Some error occurred while sending the first booking email to partner $provider->person ($provider->name)";

		$data = [
			'title'    => $e->getMessage(),
			'code'     => $e->getCode(),
			'details'  => $e->getTraceAsString(),
			'ticketId' => $ticket->id,
			'sub'      => $sub
		];

		Mail::send('emails.report.first_booking_error', ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.contact.company.email'), 'Error')
			  ->to(config('evibe.contact.tech.group'))
			  ->cc(config('evibe.contact.customer.group'))
			  ->subject($data['sub']);
		});
	}
}
