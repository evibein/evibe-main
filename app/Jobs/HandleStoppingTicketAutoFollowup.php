<?php

namespace App\Jobs;

use App\Http\Controllers\Partner\BasePartnerController;
use App\Jobs\Emails\Payment\BaseMailerJob;
use App\Models\Ticket\TicketReminderStack;
use App\Models\Util\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class HandleStoppingTicketAutoFollowup extends BaseMailerJob
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$stopMessagePrefix = '[MAIN] - Triggers - ';
		if (isset($data['ticketId']) && $data['ticketId'])
		{
			$ticketId = $data['ticketId'];
			$invalidAt = (isset($data['invalidAt']) && $data['invalidAt']) ? $data['invalidAt'] : null;
			$terminatedAt = (isset($data['terminatedAt']) && $data['terminatedAt']) ? $data['terminatedAt'] : null;
			$stopMessage = (isset($data['stopMessage']) && $data['stopMessage']) ? $stopMessagePrefix . $data['stopMessage'] : 'Termination happened from MAIN trigger';
			// @see: for now, default 'is followup' in case if we have missed any where
			// todo: remove default as 'followup' when using auto-followups for another category
			// @see: one ticket might have multiple reminders going on at same time
			$typeReminderId = (isset($data['typeReminderId']) && $data['typeReminderId']) ? $data['typeReminderId'] : config('evibe.type_reminder.followup');
			$ticketReminder = TicketReminderStack::join('type_reminder_group', 'type_reminder_group.id', '=', 'ticket_reminders_stack.type_reminder_group_id')
			                                     ->whereNull('type_reminder_group.deleted_at')
			                                     ->whereNull('ticket_reminders_stack.deleted_at')
			                                     ->whereNull('ticket_reminders_stack.invalid_at')
			                                     ->whereNull('ticket_reminders_stack.terminated_at')
			                                     ->where('ticket_reminders_stack.ticket_id', $ticketId)
			                                     ->where('type_reminder_group.type_reminder_id', $typeReminderId)
			                                     ->select('ticket_reminders_stack.*', 'type_reminder_group.type_reminder_id')
			                                     ->first();

			$jsonData = [
				'invalidAt'    => $invalidAt,
				'terminatedAt' => $terminatedAt,
				'stopMessage'  => $stopMessage
			];

			if ($ticketReminder)
			{
				$url = config('evibe.api.base_url') . config('evibe.api.auto-followups.prefix') . $ticketId . '/stop/' . $ticketReminder->type_reminder_id;
				$method = new BasePartnerController();

				// taking default handler for system updates
				$accessToken = $method->getAccessTokenFromUserId(User::find(config('evibe.default.access_token_handler')));

				try
				{
					$client = new Client();
					$res = $client->request('PUT', $url, [
						'headers' => [
							'access-token' => $accessToken
						],
						'json'    => $jsonData
					]);

					$res = $res->getBody();
					$res = \GuzzleHttp\json_decode($res, true);
					if (isset($res['success']) && !$res['success'])
					{
						Log::error("failed to stop auto followup for the ticket: " . $ticketId);
					}

				} catch (\Exception $exception)
				{
					Log::error($exception->getMessage());
				}
			}
		}
	}
}
