<?php

namespace App\Jobs\Emails\Landing;

use App\Models\Coupon\Coupon;
use Illuminate\Support\Facades\Mail;
use App\Jobs\Emails\Payment\BaseMailerJob;
use App\Models\Util\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Exception;
use Illuminate\Support\Facades\Log;

class MailSurpriseSignupSuccessToCustomer extends BaseMailerJob
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */

	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		try
		{
			$client = new Client();
			$user = User::find(config("evibe.default.access_token_handler"));

			$response = $client->request('POST', config("evibe.api.base_url") . "coupon/generate", [
				'headers' => [
					'access-token' => $this->getAccessTokenFromUser($user)
				],
				'json'    => [
					"couponPrefix"   => "LA",
					"stringLength"   => 8,
					"discountAmount" => "200",
					"minOrder"       => "3000",
					"description"    => "Surprise landing page enquiry success.",
					"offerEndTime"   => Carbon::now()->addMonths(6)->toDateTimeString()
				]
			]);

			$response = $response->getBody();
			$response = \GuzzleHttp\json_decode($response, true);

			if ($response['success'])
			{
				$coupon = Coupon::where("coupon_code", $response["couponCode"])->first();

				$data["validUpto"] = $coupon ? date('d M Y', strtotime($coupon->offer_end_time)) : "";
				$data['sub'] = "Here is your ₹200 coupon code";
				$data["referralCode"] = $response["couponCode"];

				Mail::send('emails.landing.surprise-landing-signup-success', ['data' => $data], function ($m) use ($data) {
					$m->from(config('evibe.contact.company.email'), 'Evibe.in')
					  ->to($data["email"])
					  ->subject($data['sub']);
				});
			}
		} catch (ClientException $e)
		{
			$apiResponse = $e->getResponse()->getBody(true);
			$apiResponse = \GuzzleHttp\json_decode($apiResponse);
			$res['error'] = $apiResponse->errorMessage;
		}
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}
