<?php

namespace App\Jobs\Emails\ReportIssue;

use App\Jobs\Emails\UtilEmail;
use Illuminate\Support\Facades\Mail;
use Exception;

class BookingPartyDateTimeAlertToTeam extends UtilEmail
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */

	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$data['sub'] = '[#' . $data['ticketId'] . '] Party date time error while making payment';

		Mail::send('emails.report-issue.booking-date-time-issue', ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.contact.company.email'), 'Booking Payment Error')
			  ->to(config('evibe.contact.customer.group'), 'Customer Team')
			  ->subject($data['sub']);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}
