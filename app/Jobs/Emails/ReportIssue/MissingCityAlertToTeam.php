<?php

namespace App\Jobs\Emails\ReportIssue;

use App\Jobs\Emails\UtilEmail;
use Illuminate\Support\Facades\Mail;
use Exception;

class MissingCityAlertToTeam extends UtilEmail
{
	private $data;

	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 3;

	/**
	 * The number of seconds the job can run before timing out.
	 *
	 * @var int
	 */
	public $timeout = 120;

	/**
	 * Create a new job instance.
	 *
	 * @param $data
	 */
	public function __construct($data)
	{
		$this->data = $data;

		// run on a separate queue connection
		$this->onConnection("sqs_reporters")
		     ->onQueue(config('queue.connections.sqs_reporters.queue'));
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$data['sub'] = 'Missing city for an option - ' . $data['name'];

		Mail::send('emails.report-issue.missing-city-alert-team', ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.contact.company.email'), 'Missing City Alert')
			  ->to(config('evibe.contact.business.group'), 'Business Team')
			  ->subject($data['sub']);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}
