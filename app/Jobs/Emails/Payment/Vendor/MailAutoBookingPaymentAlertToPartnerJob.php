<?php

namespace App\Jobs\Emails\Payment\Vendor;

use App\Jobs\Emails\Payment\BaseAutoBookingMailerJob;
use Exception;
use Illuminate\Support\Facades\Mail;

class MailAutoBookingPaymentAlertToPartnerJob extends BaseAutoBookingMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job - Send receipt email
	 * Sent to customer with vendor, handler and admin in cc
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		// @see: comes here only if booking data exists
		//$bookingName = $this->getShortName($data['booking']['name'], 20);
		$partyDateTime = $data['partyDateTime'];
		$subAutoBooking = '[#' . $data['ticketId'] . '] Availability check for party on ' . $partyDateTime;

		$mailData = $this->getABMailData();
		$data = array_merge($data, $mailData);
		$data['sub'] = $subAutoBooking;

		if ($data['email'])
		{
			Mail::send('emails.auto-book.pay-success.partner-avail-check', ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.customer.group'), 'Team Evibe')
				  ->to($data['email'])
				  ->cc($data['vendorCcAddresses'])
				  ->bcc(config('evibe.contact.operations.alert_no_action_email'))
				  ->replyTo(config('evibe.contact.customer.group'))
				  ->subject($data['sub']);
			});
		}
		else
		{
			Mail::send('emails.auto-book.pay-success.partner-avail-check', ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.customer.group'), 'Team Evibe')
				  ->to(config('evibe.contact.customer.group'))
				  ->cc(config('evibe.contact.operations.alert_no_action_email'))
				  ->bcc(config('evibe.contact.tech.group'))
				  ->replyTo(config('evibe.contact.customer.group'))
				  ->subject('[Invalid Email Address]. Sub: ' . $data['sub']);
			});
		}

	}

	public function failed(Exception $exception)
	{
		$data['exception'] = $exception;
		$this->sendFailedJobsToTeam($data);
	}
}