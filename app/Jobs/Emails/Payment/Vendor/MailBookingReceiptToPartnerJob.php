<?php

namespace App\Jobs\Emails\Payment\Vendor;

use App\Jobs\Emails\Payment\BaseMailerJob;
use Exception;

class MailBookingReceiptToPartnerJob extends BaseMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	public function handle()
	{
		$data = $this->data;
		$from = 'Team Evibe <ping@evibe.in>';
		$sub = '[#' . $data['ticketId'] . '] ' . ucfirst($data['booking']['provider']['person']) . ', ';
		$sub .= 'you have a confirmed booking for ' . $data['customer']['name'];
		$sub .= ' on ' . $data['booking']['partyDate'];

		if ($data['booking']['isVenue'] != 1)
		{
			$sub .= ' at ' . $data['additional']['area'];
		}

		$mailData = [
			'to'      => [$data['booking']['provider']['email']],
			'cc'      => $data['vendorCcAddresses'],
			'replyTo' => $data['vendorCcAddresses'],
			'from'    => $from,
			'sub'     => $sub,
			'body'    => $this->getBookedEmailMarkup($data)
		];

		if (!empty($data['is_auto_booked']))
		{
			$bookingName = $data['booking']['venueName'];
			$subAutoBooking = '[#' . $data['ticketId'] . '] Availability check for ' . $bookingName . ', ' . $data['additional']['guestsCount'] . ' guests for ' . $data['booking']['partyDate'] . " ?";
			$mailData['sub'] = $subAutoBooking;

			$mailData['replyTo'] = [config('evibe.contact.customer.group')];
			$mailData['body'] = $this->getAutoBookingBookedEmailMarkup($data);
		}

		$this->sendMail($mailData);
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}

	private function getAutoBookingBookedEmailMarkup($data)
	{
		// greetings
		$greetingsMarkup = file_get_contents(base_path('resources/views/emails/orders/vendor/ab_greetings.html'));
		$greetingsReplaces = [
			'@date@'            => date('d/m/y h:i A'),
			'@evibePhone@'      => config('evibe.contact.company.phone'),
			'@proName@'         => $data['booking']['provider']['person'],
			'@packageInfo@'     => $data['booking']['bookingInfo'],
			'@checkInDate@'     => $data['booking']['partyDate'],
			'@checkInTime@'     => $data['booking']['checkInTime'],
			'@checkOutTime@'    => $data['booking']['checkOutTime'],
			'@guestsCount@'     => $data['additional']['guestsCount'],
			'@biz_head_number@' => config('evibe.contact.business.phone'),
		];
		$greetingsMarkup = str_replace(array_keys($greetingsReplaces), array_values($greetingsReplaces), $greetingsMarkup);

		$markup = "<div style='background-color:#f5f5f5;padding:10px 20px;width:700px;font-size:14px;line-height:22px;'>";
		$markup .= $greetingsMarkup;
		$markup .= "</div>"; // close
		$markup .= "<div style='padding-top:10px;font-size:12px;color:#999'>If you are receiving the message in Spam or Junk folder, please mark it as 'not spam' and add senders id to contact list or safe list.</div>";

		return $markup;
	}

	private function getBookedEmailMarkup($data)
	{
		// greetings
		$greetingsMarkup = file_get_contents(base_path('resources/views/emails/orders/vendor/brv_greetings.html'));
		$partnerDashEmailLinkMarkup = '<div style="text-align: center; margin-top: 20px; margin-bottom: 25px;">
											<a href="' . $data['partnerDashEmailLink'] . '" target="_blank" style="padding: 8px 20px; border-radius: 4px; display: inline-block; background-color: #ED2E72; color: #FFFFFF; font-size: 20px; font-weight: bold; text-decoration: none;">View My Orders</a>
									   </div>';
		$greetingsReplaces = [
			'@date@'                       => date('d/m/y h:i A'),
			'@evibePhone@'                 => config('evibe.contact.company.phone'),
			'@proName@'                    => $data['booking']['provider']['person'],
			'@salutation@'                 => $this->getPartnerEmailSalutation(),
			'@partnerDashEmailLinkMarkup@' => $partnerDashEmailLinkMarkup
		];
		$greetingsMarkup = str_replace(array_keys($greetingsReplaces), array_values($greetingsReplaces), $greetingsMarkup);

		// order details
		$orderDetailsMarkup = file_get_contents(base_path('resources/views/emails/orders/vendor/brv_order_details.html'));
		$splNotes = isset($data['booking']['specialNotes']) ? $data['booking']['specialNotes'] : "";
		$splNotes = $splNotes ? $splNotes : " -- ";

		$orderDetailsReplaces = [
			'@bookId@'             => $data['booking']['bookingId'],
			'@partyDate@'          => date("d/m/y H:i", $data['booking']['partyDateTime']),
			'@eventName@'          => $data['additional']['eventName'],
			'@bookAmt@'            => $this->formatPrice($data['booking']['bookingAmount']),
			'@advPd@'              => $this->formatPrice($data['booking']['advanceAmount']),
			'@bookingInfo@'        => $data['booking']['bookingInfo'],
			'@splNts@'             => $splNotes,
			'@balAmt@'             => $this->formatPrice($data['booking']['bookingAmount'] - $data['booking']['advanceAmount']),
			'@updatedMarkup@'      => "",
			'@bookingConceptName@' => $data['bookingConceptName'] ? $data['bookingConceptName'] : " -- "
		];
		$orderDetailsMarkup = str_replace(array_keys($orderDetailsReplaces), array_values($orderDetailsReplaces), $orderDetailsMarkup);

		// adding checkout page fields
		if (count($data['additional']['checkoutFields']) > 0 || count($data['booking']['gallery']) > 0)
		{
			$orderDetailsMarkup .= '<div style="background-color:#FFFFFF; padding: 0 20px 20px 20px">';

			if (count($data['additional']['checkoutFields']) > 0)
			{

				foreach ($data['additional']['checkoutFields'] as $checkoutField)
				{
					if (!empty($data['additional'][$checkoutField['name']]) && ($checkoutField['type_ticket_booking_id'] == $data['booking']['typeTicketBookingId']))
					{
						$orderDetailsMarkup .= '<div style="padding-bottom:15px;">
											<div><b>' . $checkoutField['identifier'] . '</b></div>
											<div>' . $data['additional'][$checkoutField['name']] . '</div>
									   </div>';
					}
				}
			}

			if (count($data['booking']['gallery']) > 0)
			{
				$orderDetailsMarkup .= '<div><b>Reference Image(s)</b></div>';
				foreach ($data['booking']['gallery'] as $gallery)
				{
					$orderDetailsMarkup .= '<a href="' . $gallery->getOriginalImagePath() . '" target="_blank" style="text-decoration:none">
									<img src="' . $gallery->getResultsImagePath() . '" alt="' . $gallery->title . '" style="border:1px solid #d9d9d9;margin-right: 5px;margin-top:10px;max-width: 140px;max-height: 90px;">
								   </a>';
				}

			}
			$orderDetailsMarkup .= '</div>';
		}

		// Venue details: include only for non-venue bookings
		$venueDetailsMarkup = "";
		if ($data['booking']['isVenue'] != 1)
		{
			$venueDetailsMarkup = file_get_contents(base_path('resources/views/emails/orders/vendor/brv_venue_info.html'));
			$landMark = $data['additional']['venueLandmark'];
			$landMark = $landMark ? $landMark : " -- ";
			$venueDetailsReplaces = [
				'@area@'      => $data['additional']['area'],
				'@venue@'     => $data['additional']['venueAddress'],
				'@landMark@'  => $landMark,
				'@mapLink@'   => $data['additional']['mapLink'],
				'@venueType@' => $data['additional']['ticketVenueType'] ? $data['additional']['ticketVenueType']->name : '--',
			];
			$venueDetailsMarkup = str_replace(array_keys($venueDetailsReplaces), array_values($venueDetailsReplaces), $venueDetailsMarkup);
		}

		// Customer information
		if (isset($data['booking']['venuePartnerName']) && $data['booking']['venuePartnerName'] &&
			isset($data['booking']['venueDelivery']) && $data['booking']['venueDelivery'] &&
			isset($data['booking']['itemMapTypeId']) && ($data['booking']['itemMapTypeId'] == config('evibe.ticket.type.add-on')) &&
			isset($data['booking']['provider']['providerTypeId']) && ($data['booking']['provider']['providerTypeId'] != config('evibe.ticket.type.venue')))
		{
			$customerInfoMarkup = file_get_contents(base_path('resources/views/emails/orders/vendor/brv_av_customer_info.html'));
			$altPhone = $data['additional']['altPhone'];
			$altPhone = $altPhone ? $altPhone : " -- ";
			$customerInfoReplaces = [
				'@venuePartnerName@'      => $data['booking']['venuePartnerName'] ? $data['booking']['venuePartnerName'] : " -- ",
				'@venuePartnerPhone@'     => $data['booking']['venuePartnerPhone'] ? $data['booking']['venuePartnerPhone'] : " -- ",
				'@$venuePartnerAltPhone@' => $data['booking']['venuePartnerAltPhone'] ? $data['booking']['venuePartnerAltPhone'] : " -- ",
				'@custName@'              => $data['customer']['name'],
				'@custPhone@'             => $data['customer']['phone'],
				'@custAltPh@'             => $altPhone,
			];
		}
		else
		{
			$customerInfoMarkup = file_get_contents(base_path('resources/views/emails/orders/vendor/brv_customer_info.html'));
			$altPhone = $data['additional']['altPhone'];
			$altPhone = $altPhone ? $altPhone : " -- ";
			$customerInfoReplaces = [
				'@custName@'  => $data['customer']['name'],
				'@custPhone@' => $data['customer']['phone'],
				'@custAltPh@' => $altPhone,
			];
		}
		$customerContactInfoMarkup = '<div style="text-align: center;">
											<a href="' . $data['partnerDashEmailLink'] . '" target="_blank" style="padding: 4px 12px; border-radius: 4px; display: inline-block; background-color: #ED2E72; color: #FFFFFF; font-size: 16px; font-weight: bold; text-decoration: none;">Customer Contact Info</a>
									   </div>';
		$customerInfoReplaces['@customerContactInfoMarkup@'] = $customerContactInfoMarkup;
		$customerInfoMarkup = str_replace(array_keys($customerInfoReplaces), array_values($customerInfoReplaces), $customerInfoMarkup);

		// Evibe process: pre event / on event / post event
		$evibeProcessMarkup = file_get_contents(base_path('resources/views/emails/orders/vendor/brv_evibe_process.html'));
		$evibeCancellationPolicyPartner = file_get_contents(base_path('resources/views/emails/orders/vendor/brv_evibe_process.html'));

		$markup = "<div style='background-color:#d9d9d9;padding:30px 20px;width:700px;font-size:14px;line-height:22px;'>";
		$markup .= $greetingsMarkup . $orderDetailsMarkup . $venueDetailsMarkup . $customerInfoMarkup . $evibeProcessMarkup . $evibeCancellationPolicyPartner;
		$markup .= "</div>"; // close
		$markup .= "<div style='padding-top:10px;font-size:12px;color:#999'>If you are receiving the message in Spam or Junk folder, please mark it as 'not spam' and add senders id to contact list or safe list.</div>";

		return $markup;
	}
}