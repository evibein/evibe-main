<?php

namespace App\Jobs\Emails\Payment\Vendor;

use App\Jobs\Emails\Payment\BaseMailerJob;
use Exception;

class MailAutoBookingAlertToPartnerJob extends BaseMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	public function handle()
	{
		$data = $this->data;
		$from = 'Team Evibe <ping@evibe.in>';
		$bookingName = $data['booking']['venueName'];
		$subAutoBooking = '[#' . $data['ticketId'] . '] Availability check for ' . $bookingName . ', ';
		if ($data['additional']['guestsCount'])
		{
			$subAutoBooking .= $data['additional']['guestsCount'] . ' guests';
		}
		$subAutoBooking .= ' for ' . $data['booking']['checkInDate'];

		$mailData = [
			'to'      => [$data['booking']['provider']['email']],
			'cc'      => $data['vendorCcAddresses'],
			'replyTo' => [config('evibe.contact.customer.group')],
			'from'    => $from,
			'sub'     => $subAutoBooking,
			'body'    => $this->getAutoBookingAlertEmailMarkup($data),
		];

		$this->sendMail($mailData);
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}

	private function getAutoBookingAlertEmailMarkup($data)
	{
		if ($data['firstBooking']['is_venue_booking'] == 1)
		{
			$greetingMarkup = $this->getEmailMarkup('auto-book/pay-success/abv_ps_ask.html');
		}
		else
		{
			$greetingMarkup = $this->getEmailMarkup('auto-book/pay-success/abv_ps_p_ask.html');
		}

		$cardData = $this->getVendorEmailData($data);
		$cardContent = $this->replaceEmailVariables($greetingMarkup, $cardData);
		$emailContent = $this->getMergedEmailContent($cardContent);

		return $emailContent;
	}

	private function getVendorEmailData($data)
	{
		$cardReplaces = [
			'@date@'             => date('d/m/y h:i A'),
			'@evibePhone@'       => config('evibe.contact.company.phone'),
			'@custName@'         => $data['customer']['name'],
			'@proName@'          => $data['booking']['provider']['person'],
			'@packageInfo@'      => $data['booking']['bookingInfo'],
			'@checkInDate@'      => $data['booking']['checkInDate'],
			'@checkInTime@'      => $data['booking']['checkInTime'] ? $data['booking']['checkInTime'] : "N/A",
			'@checkOutTime@'     => $data['booking']['checkOutTime'] ? $data['booking']['checkOutTime'] : "N/A",
			'@guestsCount@'      => $data['additional']['guestsCount'] ? $data['additional']['guestsCount'] : "N/A",
			'@biz_head_number@'  => config('evibe.contact.business.phone'),
			'@confirmationLink@' => $data['vendorAskUrl'],
			'@salutation@'       => $this->getPartnerEmailSalutation(),
			'@deliveryAddress@'  => $data['additional']['venueAddress'],
			'@splNotes@'         => $data['additional']['specialNotes'] ? $data['additional']['specialNotes'] : "--"
		];

		return $cardReplaces;
	}
}