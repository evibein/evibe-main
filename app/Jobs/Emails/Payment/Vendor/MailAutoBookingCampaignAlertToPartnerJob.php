<?php

namespace App\Jobs\Emails\Payment\Vendor;

use App\Jobs\Emails\Payment\BaseCampaignMailerJob;
use Exception;
use Illuminate\Support\Facades\Mail;

class MailAutoBookingCampaignAlertToPartnerJob extends BaseCampaignMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job - Send receipt email
	 * Sent to customer with vendor, handler and admin in cc
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$bookingName = $this->getShortName($data['booking']['name'], 20);
		$subAutoBooking = '[Evibe.in] Booking confirmed for ' . $bookingName . ', ';

		$mailData = $this->getCampaignMailData($data);

		//$ticket = Ticket::find($data['ticketId']);
		//$occasionId = $ticket->event_id;

		$mailData['sub'] = $subAutoBooking;

		if ($mailData['booking']['provider']['email'])
		{
			Mail::send('emails.auto-book.pay-success.campaign.alert-partner', ['data' => $mailData], function ($m) use ($mailData) {
				$m->from(config('evibe.contact.company.email'), 'Team Evibe')
				  ->to($mailData['booking']['provider']['email'])
				  ->cc($mailData['vendorCcAddresses'])
				  ->replyTo(config('evibe.contact.customer.group'))
				  ->subject($mailData['sub']);

				foreach ($mailData['imageLinks'] as $imageLink)
				{
					$m->attach(config('evibe.gallery.host') . "/ticket-bookings/" . $mailData['booking']['id'] . "/" . $imageLink['url']);
				}
			});
		}
		else
		{
			Mail::send('emails.auto-book.pay-success.campaign.alert-partner', ['data' => $mailData], function ($m) use ($mailData) {
				$m->from(config('evibe.contact.company.email'), 'Team Evibe')
				  ->to(config('evibe.contact.customer.group'))
				  //->cc(config('evibe.contact.operations.group'))
				  ->bcc(config('evibe.contact.tech.group'))
				  ->replyTo(config('evibe.contact.customer.group'))
				  ->subject('[Invalid Email Address]. Sub: ' . $mailData['sub']);

				foreach ($mailData['imageLinks'] as $imageLink)
				{
					$m->attach(config('evibe.gallery.host') . "/ticket-bookings/" . $mailData['booking']['id'] . "/" . $imageLink['url']);
				}
			});
		}

	}

	public function failed(Exception $exception)
	{
		$data['exception'] = $exception;
		$this->sendFailedJobsToTeam($data);
	}
}