<?php

namespace App\Jobs\Emails\Payment\Vendor;

use App\Jobs\Emails\Payment\BaseDecorMailerJob;
use Exception;
use Illuminate\Support\Facades\Mail;

class MailAutoBookingDecorAlertToPartnerJob extends BaseDecorMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job - Send receipt email
	 * Sent to customer with vendor, handler and admin in cc
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$bookingName = $this->getShortName($data['booking']['name'], 20);
		$subAutoBooking = '[Evibe.in] Availability check for ' . $bookingName . ', ';

		$mailData = $this->getDecorMailData($data);
		$mailData['sub'] = $subAutoBooking;

		if ($mailData['booking']['provider']['email'])
		{
			Mail::send('emails.auto-book.pay-success.decor.alert-partner', ['data' => $mailData], function ($m) use ($mailData) {
				$m->from(config('evibe.contact.company.email'), 'Team Evibe')
				  ->to($mailData['booking']['provider']['email'])
				  ->cc($mailData['vendorCcAddresses'])
				  ->replyTo(config('evibe.contact.operations.group'))
				  ->subject($mailData['sub']);
			});
		}
		else
		{
			Mail::send('emails.auto-book.pay-success.decor.alert-partner', ['data' => $mailData], function ($m) use ($mailData) {
				$m->from(config('evibe.contact.company.email'), 'Team Evibe')
				  ->to(config('evibe.contact.customer.group'))
				  ->cc(config('evibe.contact.operations.group'))
				  ->bcc(config('evibe.contact.tech.group'))
				  ->replyTo(config('evibe.contact.operations.group'))
				  ->subject('[Invalid Email Address]. Sub: ' . $mailData['sub']);
			});
		}

	}

	public function failed(Exception $exception)
	{
		$data['exception'] = $exception;
		$this->sendFailedJobsToTeam($data);
	}
}