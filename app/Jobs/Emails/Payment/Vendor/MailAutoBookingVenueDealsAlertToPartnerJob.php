<?php

namespace App\Jobs\Emails\Payment\Vendor;

use App\Jobs\Emails\Payment\BaseMailerJob;
use Exception;

class MailAutoBookingVenueDealsAlertToPartnerJob extends BaseMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{

		$data = $this->data;
		$from = 'Team Evibe <ping@evibe.in>';
		$venueName = $data['booking']['venueName'];
		$subAutoBooking = '[Evibe.in] Availability check for ' . $venueName . ', ';
		$subAutoBooking .= $data['additional']['guestsCount'] . ' guests for ' . $data['booking']['checkInDate'];

		$mailData = [
			'to'      => [$data['booking']['provider']['email']],
			'cc'      => $data['vendorCcAddresses'],
			'replyTo' => [config('evibe.contact.operations.group')],
			'from'    => $from,
			'sub'     => $subAutoBooking,
			'body'    => $this->getAutoBookingAlertEmailMarkup($data),
		];

		$this->sendMail($mailData);
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}

	private function getAutoBookingAlertEmailMarkup($data)
	{
		$greetingMarkup = $this->getEmailMarkup('auto-book/pay-success/venue-deals/vd_abv_ps_ask.html');
		$cardData = $this->getVendorEmailData($data);
		$cardContent = $this->replaceEmailVariables($greetingMarkup, $cardData);
		$emailContent = $this->getMergedEmailContent($cardContent);

		return $emailContent;
	}

	private function getVendorEmailData($data)
	{
		$foodType = $data['additional']['foodType'];
		$foodTypeMarkup = "";

		if ($foodType)
		{
			$foodTypeMarkup = '<div><b> Food Type: </b>';

			if ($foodType == 1)
			{
				$foodTypeMarkup .= '<span> <img src=' . $this->getVegLogoPath() . ' alt="veg"> Veg</span>';
			}
			elseif ($foodType == 2)
			{
				$foodTypeMarkup .= '<span> <img src=' . $this->getNonVegLogoPath() . ' alt="non-veg"> Non Veg</span>';
			}

			$foodTypeMarkup .= '</div>';
		}

		$cardReplaces = [
			'@date@'             => date('d/m/y h:i A'),
			'@evibePhone@'       => config('evibe.contact.company.phone'),
			'@custName@'         => $data['customer']['name'],
			'@proName@'          => $data['booking']['provider']['person'],
			'@packageInfo@'      => $data['booking']['bookingInfo'],
			'@tokenAmount@'      => $this->formatPrice($data['booking']['tokenAmount']),
			'@checkInDate@'      => $data['booking']['checkInDate'],
			'@checkInTime@'      => $data['booking']['checkInTime'],
			'@checkOutTime@'     => $data['booking']['checkOutTime'],
			'@guestsCount@'      => $data['additional']['guestsCount'],
			'@biz_head_number@'  => config('evibe.contact.business.phone'),
			'@confirmationLink@' => $data['vendorAskUrl'],
			'@slot@'             => $data['additional']['slot'] ? $data['additional']['slot']->name : "--",
			'@foodType@'         => $foodTypeMarkup,
			'@salutation@'       => $this->getPartnerEmailSalutation(),
			'@splNotes@'         => $data['additional']['specialNotes'] ? $data['additional']['specialNotes'] : "--"
		];

		return $cardReplaces;
	}
}
