<?php

namespace App\Jobs\Emails\Payment\Vendor;

use App\Jobs\Emails\Payment\BaseCakeMailerJob;
use Exception;

class MailAutoBookingCakeAlertToPartnerJob extends BaseCakeMailerJob
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */

	public function __construct($data)
	{
		parent::__construct($data);
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$from = 'Team Evibe <ping@evibe.in>';
		$subAutoBooking = '[Evibe.in] Availability check for ' . $data['booking']['name'] . ' (' . $this->weight . 's) for ' . $data['booking']['checkInDate'] . ' at ' . $data['additional']['venueLocation'] . '?';

		$mailData = [
			'to'      => [$data['booking']['provider']['email']],
			'cc'      => $data['vendorCcAddresses'],
			'replyTo' => [config('evibe.contact.operations.group')],
			'from'    => $from,
			'sub'     => $subAutoBooking,
			'body'    => $this->getAutoBookingAlertEmailMarkup($data),
		];

		$this->sendMail($mailData);
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}

	private function getAutoBookingAlertEmailMarkup($data)
	{
		$greetingMarkup = $this->getEmailMarkup('auto-book/pay-success/cake/abv_ps_ask.html');
		$cardData = $this->getVendorEmailData($data);
		$cardContent = $this->replaceEmailVariables($greetingMarkup, $cardData);
		$emailContent = $this->getMergedEmailContent($cardContent);

		return $emailContent;
	}

	private function getVendorEmailData($data)
	{
		$priceMarkup = $this->getCakePriceWithMarkup();

		$cardReplaces = [
			'@date@'             => date('d/m/y h:i A'),
			'@evibePhone@'       => config('evibe.contact.company.phone'),
			'@custName@'         => $data['customer']['name'],
			'@proName@'          => $data['booking']['provider']['person'],
			'@cakeInfo@'         => $data['booking']['bookingInfo'],
			'@checkInDate@'      => $data['booking']['checkInDate'],
			'@biz_head_number@'  => config('evibe.contact.business.phone'),
			'@confirmationLink@' => $data['vendorAskUrl'],
			'@cakeSlot@'         => $this->slot,
			'@cakeWeight@'       => $this->weight,
			'@cakeName@'         => $data['booking']['name'],
			'@advanceAmount@'    => $data['booking']['advanceAmount'],
			'@location@'         => $data['additional']['venueLocation'],
			'@fullAddress@'      => $data['additional']['venueAddress'],
			'@extraCakeInfo@'    => $this->extraCakeInfo(),
			'@cakePrice@'        => $priceMarkup['price'],
			'@delivery@'         => $priceMarkup['deliveryMarkup'],
			'@totalPrice@'       => $priceMarkup['orderAmount'],
			'@balanceAmtMarkup@' => $priceMarkup['balanceAmountMarkup'],
			'@salutation@'       => $this->getPartnerEmailSalutation(),
			'@splNotes@'         => $data['additional']['specialNotes'] ? $data['additional']['specialNotes'] : "--"
		];

		return $cardReplaces;
	}
}
