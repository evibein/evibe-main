<?php

namespace App\Jobs\Emails\Payment\Team;

use App\Jobs\Emails\Payment\BaseDecorMailerJob;
use Exception;
use Illuminate\Support\Facades\Mail;

class MailAutoBookingDecorAlertToTeamJob extends BaseDecorMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job - Send receipt email
	 * Sent to customer with vendor, handler and admin in cc
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$subAutoBooking = 'Auto Booking for ' . $data['booking']['name'] . ' for ' . $data['booking']['checkInDate'] . ' by ' . $data['customer']['name'];

		$mailData = $this->getDecorMailData($data);
		$mailData['sub'] = $subAutoBooking;

		Mail::send('emails.auto-book.pay-success.decor.alert-team', ['data' => $mailData], function ($m) use ($mailData)
		{
			$m->from(config('evibe.contact.company.email'), 'Auto Book Alert')
			  ->to(config('evibe.contact.customer.group'))
			  ->subject($mailData['sub']);
		});
	}

	public function failed(Exception $exception)
	{
		$data['exception'] = $exception;
		$this->sendFailedJobsToTeam($data);
	}
}