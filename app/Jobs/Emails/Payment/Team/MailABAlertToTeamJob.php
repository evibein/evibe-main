<?php

namespace App\Jobs\Emails\Payment\Team;

use App\Jobs\Emails\Payment\BaseAutoBookingMailerJob;
use Exception;
use Illuminate\Support\Facades\Mail;

class MailABAlertToTeamJob extends BaseAutoBookingMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job - Send receipt email
	 * Sent to customer with vendor, handler and admin in cc
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		//$bookingName = $this->getShortName($data['booking']['name'], 20);
		$subAutoBooking = '[#' . $data['ticketId'] . '] Auto Booking for party on ' . $data['partyDateTime'] . ' by ' . $data['customer']['name'];

		$mailData = $this->getABMailData();
		$data = array_merge($data, $mailData);
		$data['sub'] = $subAutoBooking;
		$data['pbCheckoutSourceId'] = config('evibe.ticket.enquiry_source.pb_checkout');

		Mail::send('emails.auto-book.pay-success.ab-alert-team', ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.contact.company.system_alert_email'), 'Auto Book Alert')
			  ->to(config('evibe.contact.operations.alert_no_action_email'))
			  ->subject($data['sub']);
		});
	}

	public function failed(Exception $exception)
	{
		$data['exception'] = $exception;
		$this->sendFailedJobsToTeam($data);
	}

}