<?php

namespace App\Jobs\Emails\Payment\Team;

use App\Jobs\Emails\Payment\BaseEntMailerJob;
use Exception;
use Illuminate\Support\Facades\Mail;

class MailAutoBookingServiceAlertToTeamJob extends BaseEntMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job - Send receipt email
	 * Sent to CRM team
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$subAutoBooking = 'Auto Booking for ' . $data['booking']['name'] . ' for ' . $data['booking']['checkInDate'] . ' by ' . $data['customer']['name'];

		$mailData = $this->getEntMailData($data);

		Mail::send('emails.auto-book.pay-success.ent.alert-team', ['data' => $mailData], function ($m) use ($mailData, $subAutoBooking)
		{
			$m->from(config('evibe.contact.company.email'), 'Auto Book Alert')
			  ->to(config('evibe.contact.customer.group'))
			  ->subject($subAutoBooking);
		});
	}

	public function failed(Exception $exception)
	{
		$data['exception'] = $exception;
		$this->sendFailedJobsToTeam($data);
	}
}