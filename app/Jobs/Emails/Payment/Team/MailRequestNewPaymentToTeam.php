<?php

namespace App\Jobs\Emails\Payment\Team;

use App\Jobs\Emails\Payment\BaseMailerJob;
use Exception;
use Illuminate\Support\Facades\Mail;

class MailRequestNewPaymentToTeam extends BaseMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	public function handle()
	{
		$data = $this->data;
		$sub = '[#' . $data['id'] . '] [URGENT] ' . $data["name"] . ' requesting for new payment link';

		$mailData = [
			'ticketId' => $data["id"]
		];

		Mail::send('emails.tickets.request-new-payment', ['data' => $mailData], function ($m) use ($sub) {
			$m->from(config('evibe.contact.company.email'), 'Order Process')
			  ->to(config('evibe.contact.customer.group'))
			  ->subject($sub);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}