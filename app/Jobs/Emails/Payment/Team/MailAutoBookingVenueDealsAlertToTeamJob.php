<?php

namespace App\Jobs\Emails\Payment\Team;

use App\Jobs\Emails\Payment\BaseMailerJob;
use Exception;

class MailAutoBookingVenueDealsAlertToTeamJob extends BaseMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$from = 'Auto Book Alert <ping@evibe.in>';
		$venueName = $data['booking']['venueName'];
		$sub = 'Auto Booking for ' . $venueName . ', ' . $data['additional']['venueLocation'] . ' on ' . $data['booking']['checkInDate'] . ' (' . $data['additional']['slot']->name . ')' . ' by ' . $data['customer']['name'];

		$mailData = [
			'to'   => [config('evibe.contact.operations.group')],
			'from' => $from,
			'sub'  => $sub,
			'body' => $this->getAutoBookingBookedEmailMarkup($data)
		];

		$this->sendMail($mailData);
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}

	private function getAutoBookingBookedEmailMarkup($data)
	{
		$greetingMarkup = $this->getEmailMarkup('auto-book/pay-success/venue-deals/vd_abt_ps_details.html');
		$cardData = $this->getTeamEmailData($data);
		$cardContent = $this->replaceEmailVariables($greetingMarkup, $cardData);
		$emailContent = $this->getMergedEmailContent($cardContent);

		return $emailContent;
	}

	private function getTeamEmailData($data)
	{
		$foodType = $data['additional']['foodType'];
		$foodTypeMarkup = "--";

		if (!empty($foodType))
		{
			if ($foodType == 1)
			{
				$foodTypeMarkup = "Veg";
			}
			elseif ($foodType == 2)
			{
				$foodTypeMarkup = "Non Veg";
			}
		}

		$cardReplaces = [
			'@date@'             => date('d/m/y h:i A'),
			'@evibePhone@'       => config('evibe.contact.company.phone'),
			'@custName@'         => $data['customer']['name'],
			'@custEmail@'        => $data['customer']['email'],
			'@custPhone@'        => $data['customer']['phone'],
			'@checkInDate@'      => $data['booking']['checkInDate'],
			'@checkInTime@'      => $data['booking']['checkInTime'],
			'@checkOutTime@'     => $data['booking']['checkOutTime'],
			'@bookingInfo@'      => $data['booking']['bookingInfo'],
			'@advancePaid@'      => $data['booking']['advanceAmount'],
			'@bookId@'           => $data['booking']['bookingId'],
			'@guestsCount@'      => $data['additional']['guestsCount'],
			'@advPd@'            => $this->formatPrice($data['booking']['tokenAmount']),
			'@providerName@'     => $data['booking']['provider']['person'],
			'@providerPhone@'    => $data['booking']['provider']['phone'],
			'@providerAltPhone@' => $data['booking']['provider']['altPhone'] ? $data['booking']['provider']['altPhone'] : '---',
			'@providerEmail@'    => $data['booking']['provider']['email'],
			'@providerEmails@'   => $this->getAllVendorEmail($data['booking']['provider']),
			'@slot@'             => $data['additional']['slot'] ? $data['additional']['slot']->name : "--",
			'@foodType@'         => $foodTypeMarkup,
			'@splNotes@'         => $data['additional']['specialNotes'] ? $data['additional']['specialNotes'] : "--"
		];

		return $cardReplaces;
	}
}
