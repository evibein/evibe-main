<?php

namespace App\Jobs\Emails\Payment\Team;

use App\Jobs\Emails\Payment\BaseCakeMailerJob;
use Exception;

class MailAutoBookingCakeAlertToTeamJob extends BaseCakeMailerJob
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */

	public function __construct($data)
	{
		parent::__construct($data);
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$from = 'Auto Book Alert <ping@evibe.in>';
		$sub = 'Auto Booking for ' . $data['booking']['name'] . ' for ' . $data['booking']['checkInDate'] . ' by ' . $data['customer']['name'];

		$mailData = [
			'to'   => [config('evibe.contact.operations.group')],
			'from' => $from,
			'sub'  => $sub,
			'body' => $this->getAutoBookingBookedEmailMarkup($data)
		];

		$this->sendMail($mailData);
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}

	private function getAutoBookingBookedEmailMarkup($data)
	{
		$greetingMarkup = $this->getEmailMarkup('auto-book/pay-success/cake/ab_team_ps_details.html');
		$cardData = $this->getTeamEmailData($data);
		$cardContent = $this->replaceEmailVariables($greetingMarkup, $cardData);
		$emailContent = $this->getMergedEmailContent($cardContent);

		return $emailContent;
	}

	private function getTeamEmailData($data)
	{
		$cakePrice = $this->calculateCakePrice();

		$cardReplaces = [
			'@date@'             => date('d/m/y h:i A'),
			'@evibePhone@'       => config('evibe.contact.company.phone'),
			'@custName@'         => $data['customer']['name'],
			'@custEmail@'        => $data['customer']['email'],
			'@custPhone@'        => $data['customer']['phone'],
			'@bookingInfo@'      => $data['booking']['bookingInfo'],
			'@bookId@'           => $data['booking']['bookingId'],
			'@guestsCount@'      => $data['additional']['guestsCount'],
			'@advPd@'            => $this->formatPrice($data['booking']['advanceAmount']),
			'@bookingAmt@'       => $this->formatPrice($data['booking']['bookingAmount']),
			'@providerName@'     => $data['booking']['provider']['person'],
			'@providerPhone@'    => $data['booking']['provider']['phone'],
			'@providerAltPhone@' => $data['booking']['provider']['altPhone'] ? $data['booking']['provider']['altPhone'] : '---',
			'@providerEmail@'    => $data['booking']['provider']['email'],
			'@providerEmails@'   => $this->getAllVendorEmail($data['booking']['provider']),
			'@cakeWeight@'       => $this->weight,
			'@cakeExtraInfo@'    => $this->getCakeMarkup(),
			'@checkInDate@'      => $data['booking']['checkInDate'],
			'@cakeSlot@'         => $this->slot,
			'@location@'         => $data['additional']['venueLocation'],
			'@fullAddress@'      => $data['additional']['venueAddress'],
			'@cakePrice@'        => $cakePrice['price'],
			'@deliveryCharges@'  => $cakePrice['deliveryMarkup'],
			'@splNotes@'         => $data['additional']['specialNotes'] ? $data['additional']['specialNotes'] : "--"
		];

		return $cardReplaces;
	}

	private function getCakeMarkup()
	{
		$markup = "";
		$cakeFields = $this->checkCakeField();

		if (!empty($cakeFields['type']))
		{
			$markup = '<tr>
					    <td style="padding:8px"><b>Cake Type</b></td>
					    <td style="padding:8px;">' . $cakeFields['type'] . '</td>
				      </tr>';
		}

		if (!empty($cakeFields['flavour']))
		{
			$markup .= '<tr>
					    <td style="padding:8px"><b>Cake Flavour</b></td>
					    <td style="padding:8px;">' . $cakeFields['flavour'] . '</td>
				      </tr>';
		}

		if (!empty($cakeFields['message']))
		{
			$markup .= '<tr>
					    <td style="padding:8px"><b>Message on Cake</b></td>
					    <td style="padding:8px;">' . $cakeFields['message'] . '</td>
				      </tr>';
		}

		return $markup;
	}

	private function calculateCakePrice()
	{
		$data = $this->data;
		$price = $data['booking']['bookingAmount'];
		$cakeExtraVal = $this->checkCakeField();
		$deliveryCharge = !empty($cakeExtraVal['deliveryCharge']) ? $cakeExtraVal['deliveryCharge'] : 0;

		if ($deliveryCharge)
		{
			$price = (int)($price - $deliveryCharge);
			$deliveryMarkup = "Rs. " . $this->formatPriceWithoutChar($deliveryCharge);
		}
		else
		{
			$deliveryMarkup = "<span style='color:rgba(0, 169, 12, 0.83)'> Free </span>";
		}

		return [
			'price'          => $this->formatPriceWithoutChar($price),
			'deliveryMarkup' => $deliveryMarkup,
		];
	}
}
