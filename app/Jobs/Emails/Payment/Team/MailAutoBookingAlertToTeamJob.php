<?php

namespace App\Jobs\Emails\Payment\Team;

use App\Jobs\Emails\Payment\BaseMailerJob;
use Exception;
use Illuminate\Support\Facades\Mail;

class MailAutoBookingAlertToTeamJob extends BaseMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job - Send receipt email
	 * Sent to customer with vendor, handler and admin in cc
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$from = 'Auto Book Alert <ping@evibe.in>';
		$bookingName = $data['booking']['venueName'];
		$sub = '[#' . $data['ticketId'] . '] Auto Booking for ' . $bookingName . ' on ' . $data['booking']['checkInDate'] . ' by ' . $data['customer']['name'];

		$emailAddressData = [
			'to'   => config('evibe.contact.customer.group'),
			'from' => $from,
			'sub'  => $sub,
		];
		$emailData = $this->getAutoBookingEmailData($data);
		$emailData = array_merge($emailData, $emailAddressData);

		Mail::send('emails.auto-book.pay-success.auto-book-alert-team', ['data' => $emailData], function ($m) use ($emailData) {
			$m->from(config('evibe.contact.company.email'), 'Auto Book Alert')
			  ->to(config('evibe.contact.customer.group'))
			  ->subject($emailData['sub']);
		});

	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}

	private function getAutoBookingEmailData($data)
	{
		$emailData = [
			'date'            => date('d/m/y h:i A'),
			'evibePhone'      => config('evibe.contact.company.phone'),
			'custName'        => $data['customer']['name'],
			'custEmail'       => $data['customer']['email'],
			'custPhone'       => $data['customer']['phone'],
			'checkInDate'     => $data['booking']['checkInDate'],
			'checkInTime'     => $data['booking']['checkInTime'],
			'checkOutTime'    => $data['booking']['checkOutTime'],
			'bookingInfo'     => $data['booking']['bookingInfo'],
			'advancePaid'     => $data['booking']['advanceAmount'],
			'bookId'          => $data['booking']['bookingId'],
			'guestsCount'     => $data['additional']['guestsCount'] ? $data['additional']['guestsCount'] : 'N/A',
			'bookAmt'         => $this->formatPrice($data['booking']['bookingAmount']),
			'advPd'           => $this->formatPrice($data['booking']['advanceAmount']),
			'balAmt'          => $this->formatPrice($data['booking']['bookingAmount'] - $data['booking']['advanceAmount']),
			'providerName'    => $data['booking']['provider']['person'],
			'providerPhone'   => $data['booking']['provider']['phone'],
			'providerEmail'   => $data['booking']['provider']['email'],
			'deliveryAddress' => $data['additional']['venueAddress'],
			'isVenue'         => $data['booking']['isVenue'],
			'splNotes'        => $data['additional']['specialNotes'] ? $data['additional']['specialNotes'] : "--"
		];

		return $emailData;
	}

}