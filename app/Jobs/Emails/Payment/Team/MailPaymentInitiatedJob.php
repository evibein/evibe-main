<?php

namespace App\Jobs\Emails\Payment\Team;

use App\Jobs\Emails\UtilEmail;
use Exception;

class MailPaymentInitiatedJob extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Send payment initiated email to admin / handler.
	 *
	 * This alert is sent to track if customer submitted data successfully but did
	 * not make the payment successfully (no receipt sent)
	 *
	 * @author  Anji <anji@evibe.in>
	 * @since   21 Apr 2015
	 * @updated 20 Apr 2016
	 */
	public function handle()
	{
		$data = $this->data;
		$from = 'Pay Init<ping@evibe.in>';
		$sub = '[#' . $data['ticketId'] . '] Payment initiated by ' . $data['name'];
		$body = 'Hey, <br/><br/>' .
			'This is to inform that ' . $data['name'] . ' has initiated payment of Rs. ' . $data['advanceAmount'] .
			' at ' . $data['now'] . ' using ' . $data['gateway'] . '.<br/><br/>' .
			'If you do not receive receipt email in a while, it means customer could not make the payment. ' .
			'Reach him/her at ' . $data['phone'] . ' and assist to make the payment either through payment gateway / NEFT. <br/><br/>' .
			'Bank Name: IDBI Bank<br/>' .
			'Account Name: Evibe Technologies Private Limited<br/>' .
			'Account Number: 0202102000015817<br/>' .
			'IFSC Code: IBKL0000202<br/>' .
			'Branch: Indiranagar<br/><br/>' .
			'User Device Info: ' . $data['browserInfo'] . '<br>' .
			'Payment Gateway: ' . $data['gateway'] . '<br>' .
			'Payment Link: ' . $data['previousUrl'] . '<br><br>' .
			'Best, <br/>' .
			'Admin';

		if ($data['is_auto_booked'])
		{
			$from = 'Auto Pay Init<ping@evibe.in>';
			$sub = '[#' . $data['ticketId'] . '] Auto Payment initiated by ' . $data['name'];
		}

		$mailData = [
			'to'      => $data['to'],
			'cc'      => [],
			'from'    => $from,
			'replyTo' => ['ping@evibe.in'],
			'sub'     => $sub,
			'body'    => $body
		];

		$this->sendMail($mailData);
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}