<?php

namespace App\Jobs\Emails\Payment\Team;

use App\Jobs\Emails\UtilEmail;
use Exception;

class MailProviderFirstBookingAlertToTeamJob extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$from = 'Evibe Alert <ping@evibe.in>';
		$sub = 'First Booking For ' . $data['person'];

		$body = "Hello Team, <br/><br/>" .
			"This is to notify that <b>" . $data['person'] . "</b> has got first booking through us from " . $data['customerName'] .
			" (phone: " . $data['customerPhone'] . "). Pls call " . $data['person'] . " on " . $data['phone'] . " and brief him/her again on our process." .
			"<br/><br/>" .
			"Best, <br/>" .
			"Admin";

		$mailData = [
			'to'   => [config('evibe.contact.customer.group')],
			'from' => $from,
			'sub'  => $sub,
			'body' => $body
		];

		$this->sendMail($mailData);
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}