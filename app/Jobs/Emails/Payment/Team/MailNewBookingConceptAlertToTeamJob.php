<?php

namespace App\Jobs\Emails\Payment\Team;

use App\Jobs\Emails\UtilEmail;
use Illuminate\Support\Facades\Mail;
use Exception;

class MailNewBookingConceptAlertToTeamJob extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$conceptName = $data['booking']->bookingConcept ? $data['booking']->bookingConcept->name : "";
		$data['sub'] = '[#' . $data['ticket']['id'] . '] [' . $conceptName . '] Party on ' . $data['partyDate'] . ', partner: ' . $data['person'];
		$data['url'] = config('evibe.dash_host') . '/tickets/' . $data['ticket']['id'] . '/bookings/';

		Mail::send('emails.orders.customer.ticket_booking_concept', ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.contact.company.email'), 'Concept Alert')
			  ->to(config('evibe.contact.customer.group'))
			  ->subject($data['sub']);

			foreach ($data['images'] as $image)
			{
				if ($image->type && ($image->type == 1))
				{
					$m->attach($image->url);
				}
				elseif ($image->type && ($image->type == 2))
				{
					$m->attach(config('evibe.gallery.host') . '/ticket/' . $data['ticket']['id'] . '/' . $data['booking']['id'] . '/' . $image->url);
				}
			}
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}