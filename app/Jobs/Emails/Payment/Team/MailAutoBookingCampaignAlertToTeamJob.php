<?php

namespace App\Jobs\Emails\Payment\Team;

use App\Jobs\Emails\Payment\BaseCampaignMailerJob;
use Exception;
use Illuminate\Support\Facades\Mail;

class MailAutoBookingCampaignAlertToTeamJob extends BaseCampaignMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job - Send receipt email
	 * Sent to customer with vendor, handler and admin in cc
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$bookingName = $this->getShortName($data['booking']['name'], 20);
		$subAutoBooking = 'Auto Booking for ' . $data['booking']['name'] . ' for ' . $data['booking']['checkInDate'] . ' by ' . $data['customer']['name'];

		$mailData = $this->getCampaignMailData($data);

		//$ticket = Ticket::find($data['ticketId']);
		//$occasionId = $ticket->event_id;

		$mailData['sub'] = $subAutoBooking;

		Mail::send('emails.auto-book.pay-success.campaign.alert-team', ['data' => $mailData], function ($m) use ($mailData)
		{
			$m->from(config('evibe.contact.company.email'), 'Campaign AB Alert')
			  ->to(config('evibe.contact.customer.group'))
			  ->subject($mailData['sub']);

			foreach ($mailData['imageLinks'] as $imageLink)
			{
				$m->attach(config('evibe.gallery.host') . "/ticket-bookings/" . $mailData['booking']['id'] . "/" . $imageLink['url']);
			}
		});

	}

	public function failed(Exception $exception)
	{
		$data['exception'] = $exception;
		$this->sendFailedJobsToTeam($data);
	}
}