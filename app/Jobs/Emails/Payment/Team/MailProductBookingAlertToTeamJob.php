<?php

namespace App\Jobs\Emails\Payment\Team;

use App\Jobs\Emails\Payment\BaseAutoBookingMailerJob;
use Exception;
use Illuminate\Support\Facades\Mail;

class MailProductBookingAlertToTeamJob extends BaseAutoBookingMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job - Send receipt email
	 * Sent to customer with vendor, handler and admin in cc
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$mailData = $this->getABMailData();
		$data = array_merge($data, $mailData);

		Mail::send($data['mailView'], ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.contact.company.email'), 'Party In A Box Alert')
			  ->to(config('evibe.contact.support.group'))
			  ->subject($data['mailSub']);
		});
	}

	public function failed(Exception $exception)
	{
		$data['exception'] = $exception;
		$this->sendFailedJobsToTeam($data);
	}

}