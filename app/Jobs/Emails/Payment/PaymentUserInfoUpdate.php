<?php

namespace App\Jobs\Emails\Payment;

use App\Jobs\Emails\UtilEmail;
use App\Models\Payment\PaymentTransactionAttempts;
use Exception;

class PaymentUserInfoUpdate extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * saving payment initiated to DB.
	 */
	public function handle()
	{
		$data = $this->data;

		try
		{
			PaymentTransactionAttempts::create([
				                                   "ticket_id"              => isset($data["ticketId"]) ? $data["ticketId"] : null,
				                                   "piab_id"                => isset($data["piabId"]) ? $data["piabId"] : null,
				                                   "url"                    => $data["url"],
				                                   "browser_info"           => $data["browserInfo"],
				                                   "ip_address"             => $data["ipAddress"],
				                                   "payment_start_time"     => $data["paymentStartTime"],
				                                   "payment_gateway_id"     => $data["paymentGatewayId"],
				                                   "payment_transaction_id" => $data["transactionId"]
			                                   ]);
		} catch (Exception $exception)
		{
			$this->sendFailedJobsToTeam($exception);
		}
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}