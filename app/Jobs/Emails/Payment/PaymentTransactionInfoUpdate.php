<?php

namespace App\Jobs\Emails\Payment;

use App\Jobs\Emails\UtilEmail;
use App\Models\Payment\PaymentTransactionAttempts;
use Exception;

class PaymentTransactionInfoUpdate extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * saving payment updated to DB.
	 */
	public function handle()
	{
		$data = $this->data;

		try
		{
			if (isset($data["ticketId"]) && isset($data["isSuccess"]))
			{
				$transaction = PaymentTransactionAttempts::where("ticket_id", $data["ticketId"])
				                                         ->orderBy("created_at", "DESC")
				                                         ->first();

				if ($transaction && $data["isSuccess"])
				{
					$transaction->update([
						                     "payment_bank_success" => 2,
						                     "pay_reference_id"     => isset($data["paymentRefId"]) ? $data["paymentRefId"] : null
					                     ]);
				}
				elseif ($transaction)
				{
					$transaction->update([
						                     "payment_evibe_success" => 2,
						                     "payment_end_time"      => time()
					                     ]);
				}
			}
			elseif (isset($data["piabId"]) && isset($data["isSuccess"]))
			{
				$transaction = PaymentTransactionAttempts::where("piab_id", $data["piabId"])
				                                         ->orderBy("created_at", "DESC")
				                                         ->first();

				if ($transaction && $data["isSuccess"])
				{
					$transaction->update([
						                     "payment_bank_success" => 2,
						                     "pay_reference_id"     => isset($data["paymentRefId"]) ? $data["paymentRefId"] : null
					                     ]);
				}
				elseif ($transaction)
				{
					$transaction->update([
						                     "payment_evibe_success" => 2,
						                     "payment_end_time"      => time()
					                     ]);
				}
			}
		} catch (Exception $exception)
		{
			$this->sendFailedJobsToTeam($exception);
		}
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}