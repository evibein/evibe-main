<?php

namespace App\Jobs\Emails\Payment\Customer;

use App\Jobs\Emails\Payment\BaseEntMailerJob;
use Exception;
use Illuminate\Support\Facades\Mail;

class MailAutoBookingServiceAlertToCustomerJob extends BaseEntMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job - Send receipt email
	 * Sent to customer
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$subAutoBooking = 'Your payment is successful - #' . $data['ticket']['enquiry_id'] . '. Order to be Confirmed';

		$mailData = $this->getEntMailData($data);
		$mailData['sub'] = $subAutoBooking;

		if ($mailData['customer']['email'])
		{
			Mail::send('emails.auto-book.pay-success.ent.alert-customer', ['data' => $mailData], function ($m) use ($mailData) {
				$m->from(config('evibe.contact.customer.group'), 'Evibe Payments')
				  ->to($mailData['customer']['email'])
				  ->replyTo(config('evibe.contact.customer.group'))
				  ->subject($mailData['sub']);
			});

			$this->EmailReceiptSuccessToTeam($data["ticket"]["id"], "Auto Booking Email was sent successfully to the customer", "Auto Booking Email Delivered Successfully");
		}
		else
		{
			Mail::send('emails.auto-book.pay-success.ent.alert-customer', ['data' => $mailData], function ($m) use ($mailData) {
				$m->from(config('evibe.contact.company.email'), 'Team Evibe')
				  ->to(config('evibe.contact.customer.group'))
				  ->cc(config('evibe.contact.tech.group'))
				  ->replyTo(config('evibe.contact.customer.group'))
				  ->subject('[Invalid Email Address]. Sub: ' . $mailData['sub']);
			});
		}
	}

	public function failed(Exception $exception)
	{
		$data['exception'] = $exception;
		$this->sendFailedJobsToTeam($data);
	}
}