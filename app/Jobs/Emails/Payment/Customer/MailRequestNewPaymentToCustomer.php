<?php

namespace App\Jobs\Emails\Payment\Customer;

use App\Jobs\Emails\Payment\BaseMailerJob;
use Exception;
use Illuminate\Support\Facades\Mail;

class MailRequestNewPaymentToCustomer extends BaseMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	public function handle()
	{
		$data = $this->data;
		$sub = '[#' . $data["ticketId"] . '] Your request for new payment link';

		Mail::send('emails.tickets.request-new-payment-customer', ['data' => $data], function ($m) use ($data, $sub) {
			$m->from(config('evibe.contact.customer.group'), 'Team Evibe.in')
			  ->to($data['mail'])
			  ->replyTo(config('evibe.contact.customer.group'))
			  ->cc(config('evibe.contact.customer.group'))
			  ->bcc(config('evibe.contact.operations.alert_no_action_email'))
			  ->subject($sub);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}