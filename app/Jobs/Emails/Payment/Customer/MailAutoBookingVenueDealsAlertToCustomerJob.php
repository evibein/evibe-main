<?php

namespace App\Jobs\Emails\Payment\Customer;

use App\Jobs\Emails\Payment\BaseMailerJob;
use Exception;

class MailAutoBookingVenueDealsAlertToCustomerJob extends BaseMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$from = 'Evibe Payments <enquiry@evibe.in>';
		$subBookingAlert = 'Your payment is successful - #' . $data['ticket']['enquiry_id'] . '. Order to be Confirmed';

		$mailData = [
			'to'      => [$data['customer']['email']],
			'replyTo' => [config('evibe.contact.customer.group')],
			'from'    => $from,
			'sub'     => $subBookingAlert,
			'body'    => $this->getAutoBookingAlertEmailMarkup($data)
		];

		$this->sendMail($mailData);
		$this->EmailReceiptSuccessToTeam($data["ticket"]["id"], "Auto Booking Email was sent successfully to the customer", "Auto Booking Email Delivered Successfully");
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}

	private function getAutoBookingAlertEmailMarkup($data)
	{
		$cardMarkup = $this->getEmailMarkup('auto-book/pay-success/venue-deals/vd_ab_ps_card1.html');
		$cardMarkup .= $this->getEmailMarkup('auto-book/pay-success/venue-deals/vd_ab_ps_card2.html');

		$cardData = $this->getCustomerEmailData($data);
		$cardContent = $this->replaceEmailVariables($cardMarkup, $cardData);

		$cardContent .= $this->getEmailMarkup('customer/venue-deals_faqs.html');
		$cardContent .= $this->getEmailMarkup('customer/cancellation.html');
		$cardContent .= $this->getEmailMarkup('auto-book/terms.html');

		$emailContent = $this->getMergedEmailContent($cardContent);

		return $emailContent;
	}

	private function getCustomerEmailData($data)
	{
		$foodTypeMarkup = "";
		if (!empty($data['additional']['foodType']))
		{
			$foodTypeMarkup = '<div style="margin-top:15px;">
									<b>Food Type : </b>';
			if ($data['additional']['foodType'] == 1)
			{
				$foodTypeMarkup .= '<span> <img src=' . $this->getVegLogoPath() . ' alt="veg"> Veg</span>';
			}
			elseif ($data['additional']['foodType'] == 2)
			{
				$foodTypeMarkup .= '<span> <img src=' . $this->getNonVegLogoPath() . ' alt="non-veg"> Non Veg</span>';
			}
			$foodTypeMarkup .= '</div>';
		}

		$cardReplaces = [
			'@date@'          => date('d/m/y h:i A'),
			'@evibePhone@'    => config('evibe.contact.company.phone'),
			'@bookingId@'     => $data['booking']['bookingId'],
			'@custName@'      => $data['customer']['name'],
			'@checkInDate@'   => $data['booking']['checkInDate'],
			'@venueName@'     => $data['booking']['venueName'],
			'@venueLocation@' => $data['booking']['venueLocation'],
			'@bookingInfo@'   => $data['booking']['bookingInfo'],
			'@advancePaid@'   => $this->formatPrice($data['booking']['tokenAmount']),
			'@guestsCount@'   => $data['additional']['guestsCount'],
			'@bookAmt@'       => $this->formatPrice($data['booking']['bookingAmount']),
			'@advPd@'         => $this->formatPrice($data['booking']['tokenAmount']),
			'@vdName@'        => $data['booking']['name'],
			'@slot@'          => $data['additional']['slot'] ? $data['additional']['slot']->name : "--",
			'@foodType@'      => $foodTypeMarkup,
			'@splNotes@'      => $data['additional']['specialNotes'] ? $data['additional']['specialNotes'] : "--",
			'@trackMyOrder@'  => $this->getTrackMyOrderUrlTemplate($data["ticketId"])
		];

		return $cardReplaces;
	}
}
