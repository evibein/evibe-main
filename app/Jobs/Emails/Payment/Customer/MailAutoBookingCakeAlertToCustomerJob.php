<?php

namespace App\Jobs\Emails\Payment\Customer;

use App\Jobs\Emails\Payment\BaseCakeMailerJob;
use Exception;

class MailAutoBookingCakeAlertToCustomerJob extends BaseCakeMailerJob
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		parent::__construct($data);
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{

		$data = $this->data;
		$from = 'Evibe Payments <enquiry@evibe.in>';
		$subBookingAlert = 'Your payment is successful - #' . $data['ticket']['enquiry_id'] . '. Order to be Confirmed';

		$mailData = [
			'to'      => [$data['customer']['email']],
			'replyTo' => [config('evibe.contact.customer.group')],
			'from'    => $from,
			'sub'     => $subBookingAlert,
			'body'    => $this->getAutoBookingAlertEmailMarkup($data)
		];

		$this->sendMail($mailData);
		$this->EmailReceiptSuccessToTeam($data["ticket"]["id"], "Auto Booking Email was sent successfully to the customer", "Auto Booking Email Delivered Successfully");
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}

	private function getAutoBookingAlertEmailMarkup($data)
	{
		$cardMarkup = $this->getEmailMarkup('auto-book/pay-success/cake/ab_ps_card1.html');
		$cardMarkup .= $this->getEmailMarkup('auto-book/pay-success/cake/ab_ps_card2.html');
		$cardMarkup .= $this->getEmailMarkup('auto-book/pay-success/cake/ab_ps_card3.html');

		$cardData = $this->getCustomerEmailData($data);
		$cardContent = $this->replaceEmailVariables($cardMarkup, $cardData);

		$cardContent .= $this->getEmailMarkup('customer/cake_faqs.html');
		$cardContent .= $this->getCustomerTermsAndCancellation();

		$emailContent = $this->getMergedEmailContent($cardContent);

		return $emailContent;
	}

	private function getCustomerEmailData($data)
	{

		$cardReplaces = [
			'@date@'            => date('d/m/y h:i A'),
			'@evibePhone@'      => config('evibe.contact.company.phone'),
			'@cakeWeight@'      => $this->weight,
			'@bookingId@'       => $data['booking']['bookingId'],
			'@custName@'        => $data['customer']['name'],
			'@checkInDate@'     => $data['booking']['partyDate'],
			'@advancePaid@'     => $this->formatPrice($data['booking']['advanceAmount']),
			'@bookingInfo@'     => $data['booking']['bookingInfo'],
			'@bookingName@'     => $data['booking']['name'],
			'@cakeExtraField@'  => $this->getCakeExtraField(),
			'@deliverySlot@'    => $this->slot,
			'@deliveryAddress@' => $data['additional']['venueAddress'],
			'@splNotes@'        => $data['additional']['specialNotes'] ? $data['additional']['specialNotes'] : "--",
			'@trackMyOrder@'    => $this->getTrackMyOrderUrlTemplate($data["ticketId"])
		];

		return $cardReplaces;
	}

	private function getCakeExtraField()
	{
		$extraField = "";
		$data = $this->checkCakeField();

		if (!empty($data['flavour']))
		{
			$extraField = '<div style="margin-top:15px;">
                             <b>Flavour: </b>
                             <span>' . $data['flavour'] . '</span>
                            </div>';
		}
		if (!empty($data['type']))
		{
			$extraField .= '<div style="margin-top:15px;">
                             <b>Type: </b>
                             <span>' . $data['type'] . '</span>
                            </div>';
		}
		if (!empty($data['message']))
		{
			$extraField .= '<div style="margin-top:15px;">
                             <b>Message on Cake: </b>
                             <span>' . $data['message'] . '</span>
                            </div>';
		}

		return $extraField;
	}
}
