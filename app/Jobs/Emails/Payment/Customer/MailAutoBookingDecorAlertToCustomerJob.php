<?php

namespace App\Jobs\Emails\Payment\Customer;

use App\Jobs\Emails\Payment\BaseDecorMailerJob;
use Exception;
use Illuminate\Support\Facades\Mail;

class MailAutoBookingDecorAlertToCustomerJob extends BaseDecorMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job - Send receipt email
	 * Sent to customer with vendor, handler and admin in cc
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$subAutoBooking = 'Your payment is successful - #' . $data['ticket']['enquiry_id'] . '. Order to be Confirmed';

		$mailData = $this->getDecorMailData($data);
		$mailData['sub'] = $subAutoBooking;

		Mail::send('emails.auto-book.pay-success.decor.alert-customer', ['data' => $mailData], function ($m) use ($mailData) {
			$m->from(config('evibe.contact.customer.group'), 'Evibe Payments')
			  ->to($mailData['customer']['email'])
			  ->replyTo(config('evibe.contact.customer.group'))
			  ->subject($mailData['sub']);
		});

		$this->EmailReceiptSuccessToTeam($data["ticket"]["id"], "Auto Booking Email was sent successfully to the customer", "Auto Booking Email Delivered Successfully");
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}