<?php

namespace App\Jobs\Emails\Payment\Customer;

use App\Jobs\Emails\Payment\BaseAutoBookingMailerJob;
use Exception;
use Illuminate\Support\Facades\Mail;

class MailAutoBookingPaymentReceiptToCustomerJob extends BaseAutoBookingMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job - Send receipt email
	 * Sent to customer with vendor, handler and admin in cc
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		//$bookingName = $this->getShortName($data['booking']['name'], 20);
		$mailData = $this->getABMailData();
		$data = array_merge($data, $mailData);

		if ($data['customer']['email'])
		{
			Mail::send($data['mailView'], ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.customer.group'), 'Evibe Payments')
				  ->to($data['customer']['email'])
				  ->replyTo(config('evibe.contact.customer.group'))
				  ->subject($data['mailSub']);
			});

			$this->EmailReceiptSuccessToTeam($data["ticket"]["id"], "Auto Booking Email was sent successfully to the customer", "Auto Booking Email Delivered Successfully");
		}
		else
		{
			Mail::send($data['mailView'], ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.company.email'), 'Team Evibe')
				  ->to(config('evibe.contact.customer.group'))
				  ->subject('[Invalid Email Address]. Sub: ' . $data['mailSub']);
			});
		}
	}

	public function failed(Exception $exception)
	{
		$data['exception'] = $exception;
		$this->sendFailedJobsToTeam($data);
	}
}