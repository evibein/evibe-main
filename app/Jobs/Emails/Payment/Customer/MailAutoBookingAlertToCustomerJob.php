<?php

namespace App\Jobs\Emails\Payment\Customer;

use App\Jobs\Emails\Payment\BaseMailerJob;
use Exception;

class MailAutoBookingAlertToCustomerJob extends BaseMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job - Send receipt email
	 * Sent to customer with vendor, handler and admin in cc
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$from = 'Evibe Payments  <enquiry@evibe.in>';
		$subBookingAlert = '[#' . $data["ticket"]["id"] . '] Your payment is successful. Order to be Confirmed';

		$mailData = [
			'to'      => [$data['customer']['email']],
			'replyTo' => [config('evibe.contact.customer.group')],
			'from'    => $from,
			'sub'     => $subBookingAlert,
			'body'    => $this->getAutoBookingAlertEmailMarkup($data)
		];

		$this->sendMail($mailData);
		$this->EmailReceiptSuccessToTeam($data["ticket"]["id"], "Auto Booking Email was sent successfully to the customer", "Auto Booking Email Delivered Successfully");
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}

	private function getAutoBookingAlertEmailMarkup($data)
	{
		$cardMarkup = "";
		if ($data['firstBooking']['is_venue_booking'] == 1)
		{
			$cardMarkup .= $this->getEmailMarkup('auto-book/pay-success/ab_ps_card1.html');
			$cardMarkup .= $this->getEmailMarkup('auto-book/pay-success/ab_ps_card2.html');
		}
		else
		{
			$cardMarkup .= $this->getEmailMarkup('auto-book/pay-success/ab_ps_card4.html');
			$cardMarkup .= $this->getEmailMarkup('auto-book/pay-success/ab_ps_card3.html');
		}

		$cardData = $this->getCustomerEmailData($data);
		$cardContent = $this->replaceEmailVariables($cardMarkup, $cardData);

		$cardContent .= $this->getEmailMarkup('customer/faqs.html');
		$cardContent .= $this->getCustomerTermsAndCancellation();

		$emailContent = $this->getMergedEmailContent($cardContent);

		return $emailContent;
	}

	private function getCustomerEmailData($data)
	{
		$cardReplaces = [
			'@date@'            => date('d/m/y h:i A'),
			'@evibePhone@'      => config('evibe.contact.company.phone'),
			'@bookingId@'       => $data['booking']['bookingId'],
			'@custName@'        => $data['customer']['name'],
			'@checkInDate@'     => $data['booking']['checkInDate'],
			'@checkInTime@'     => $data['booking']['checkInTime'] ? $data['booking']['checkInTime'] : "N/A",
			'@checkOutTime@'    => $data['booking']['checkOutTime'] ? $data['booking']['checkOutTime'] : "N/A",
			'@bookingInfo@'     => $data['booking']['bookingInfo'],
			'@advancePaid@'     => $this->formatPrice($data['booking']['advanceAmount']),
			'@guestsCount@'     => $data['additional']['guestsCount'] ? $data['additional']['guestsCount'] : "N/A",
			'@bookAmt@'         => $this->formatPrice($data['booking']['bookingAmount']),
			'@advPd@'           => $this->formatPrice($data['booking']['advanceAmount']),
			'@balAmt@'          => $this->formatPrice($data['booking']['bookingAmount'] - $data['booking']['advanceAmount']),
			'@deliveryAddress@' => $data['additional']['venueAddress'],
			'@splNotes@'        => $data['additional']['specialNotes'] ? $data['additional']['specialNotes'] : "--",
			'@trackMyOrder@'    => $this->getTrackMyOrderUrlTemplate($data["ticketId"])
		];

		return $cardReplaces;
	}

}