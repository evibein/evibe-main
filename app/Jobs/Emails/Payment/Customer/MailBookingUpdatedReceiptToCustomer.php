<?php

namespace App\Jobs\Emails\Payment\Customer;

use App\Jobs\Emails\Payment\BaseMailerJob;
use Exception;

class MailBookingUpdatedReceiptToCustomer extends BaseMailerJob
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$from = 'Evibe Receipts <enquiry@evibe.in>';
		$sub = '[#' . $data["ticketId"] . '] Your Event Is Booked';
		$sub .= isset($data['ticketId']) ? $this->getEnquiryIdForEmailSubject($data["ticketId"]) : "";

		$mailData = [
			'to'      => [$data['customer']['email']],
			'replyTo' => $data['ccAddresses'],
			'from'    => $from,
			'sub'     => $sub,
			'body'    => $this->getBookedEmailMarkup($data)
		];

		$this->sendMail($mailData);
		if (isset($data['ticketId']))
		{
			$this->EmailReceiptSuccessToTeam($data["ticketId"], "Booking Email was sent successfully to the customer", "Booking Email Delivered Successfully");
		}
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}

	private function getBookedEmailMarkup($data)
	{
		$termsMarkup = file_get_contents(base_path('resources/views/emails/orders/customer/terms.html'));
		$cancellationMarkup = file_get_contents(base_path('resources/views/emails/orders/customer/cancellation.html'));

		// greetings
		$card1Markup = file_get_contents(base_path('resources/views/emails/orders/customer/br_updated_greetings.html'));
		$card1Replaces = [
			'@date@'         => date('d/m/y h:i A'),
			'@evibePhone@'   => config('evibe.contact.company.phone'),
			'@custName@'     => $data['customer']['name'],
			'@trackMyOrder@' => $this->getTrackMyOrderUrlTemplate($data["ticketId"])
		];
		$card1Markup = str_replace(array_keys($card1Replaces), array_values($card1Replaces), $card1Markup);

		// add-on details
		$addOnDetailsMarkup = '';
		if (isset($data['addOns']) && count($data['addOns']))
		{
			$addOnDetailsMarkup .= "<div style='background-color:#FFFFFF;padding:20px;margin-top:30px;'>
											<div style='color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;'>ADD-ONS AVAILABLE</div>
											<a href='" . $data['tmoLink'] . "'>
											<table style='margin-top: 10px;'>
											<tr>";

			foreach ($data['addOns'] as $key => $addOn)
			{
				$addOnDetailsMarkup .= "<td style='width:28%; display: inline-block; vertical-align: top; padding-right: 3%;'>
											<div style='border: 1px solid #EFEFEF; padding: 10px; overflow: hidden;'>
													<div style=''>
														<div style='background-color: #F9F9F9; height: 120px; width: 140px;'>
															<img data-src='" . config('evibe.gallery.host') . "/addons/" . $addOn->id . "/" . $addOn->image_url . "' alt='" . $addOn->name . " profile picture' style='height: 100%; width: 100%;'>
														</div>
													</div>
													<div>
														<div>
															<div style='font-size: 16px; line-height: 20px; font-weight: 500; margin-top: 5px;'>
																" . ucwords($addOn->name) . "
															</div>";
				if ($addOn->price_per_unit)
				{
					$addOnDetailsMarkup .= "<div style='margin-top:5px;'>";
					if ($addOn->price_worth_per_unit)
					{
						$addOnDetailsMarkup .= "<span style='font-size: 14px; color: #878787; text-decoration: line-through;'>Rs. " . $addOn->price_worth_per_unit . "</span>";
					}
					$addOnDetailsMarkup .= "<span style='color: #ED3E72; font-size: 16px; font-weight: 500; margin-left: 4px;'>Rs. " . $addOn->price_per_unit . "</span>";
					if ($addOn->price_worth_per_unit && ($addOn->price_per_unit < $addOn->price_worth_per_unit))
					{
						$addOnDetailsMarkup .= "<span style='font-size: 12px; font-weight: 500; margin-left: 4px;'>(RS. " . $addOn->price_worth_per_unit - $addOn->price_per_unit . " OFF)</span>";
					}
					$addOnDetailsMarkup .= "</div>";
				}

				$addOnDetailsMarkup .= "<div style='margin-top: 10px; margin-bottom: 5px;'>
																<div style='padding: 0; text-align: left; width: 100%;' class='add-on-cta-list add-on-id-" . $addOn->id . "' data-id='" . $addOn->id . "' data-price='" . $addOn->price_per_unit . "' data-price-worth='" . $addOn->price_worth_per_unit . "' data-max-units='" . $addOn->max_units . "' data-count-units='0' data-name='" . $addOn->name . "'>
																	<div style='font-size: 14px; line-height: 20px; padding: 2px 12px 2px 10px; border-radius: 10px; display: inline-block; color: #30AC15; border: 1px solid #30AC15;'>+ Add</div>
																</div>
															</div>
														</div>
													</div>";

				if ($addOn->info)
				{
					$addOnDetailsMarkup .= "<div style='font-size: 13px; line-height: 18px; margin-top: 10px;'>
															" . $addOn->info . "
														</div>";
				}

				$addOnDetailsMarkup .= "</div></td>";

				if ($key == 2)
				{
					break;
				}
			}

			$addOnDetailsMarkup .= "</tr></table></a></div>";
		}

		// party details
		$partyDetailsCardMarkup = file_get_contents(base_path('resources/views/emails/orders/customer/br_party_details.html'));
		$partyDetailsCardReplaces = [
			'@eventName@'     => $data['additional']['eventName'],
			'@partyDateTime@' => $data['partyDateTime'],
		];
		$partyDetailsCardMarkup = str_replace(array_keys($partyDetailsCardReplaces), array_values($partyDetailsCardReplaces), $partyDetailsCardMarkup);

		// add venue details card
		$landMark = $data['additional']['venueLandmark'];
		$landMark = $landMark ? $landMark : " -- ";
		$venueMapLink = $data['additional']['mapLink'];
		$venueDetailsMarkup = file_get_contents(base_path('resources/views/emails/orders/customer/br_venue_details.html'));
		$venueDetailsReplaces = [
			'@venueName@'     => "", // initialize to empty
			'@venueAltPh@'    => "",
			'@venueType@'     => $data['additional']['ticketVenueType'] ? $data['additional']['ticketVenueType']->name : '--',
			'@venueAddress@'  => $data['additional']['venueAddress'],
			'@venueLandMark@' => $landMark,
			'@venueMaps@'     => $this->getShortUrl($venueMapLink)
		];

		// if ticket has at least one venue booking, show venue name
		if ($data['ticketHasVenueBooking'] && $data['ticketVenueName'])
		{
			$venueDetailsReplaces['@venueName@'] = "<div style='margin-top:15px;'>
														<div><b>Venue Name:</b> " . $data['ticketVenueName'] . " </div>
													</div>";
		}

		$bookingCounter = 0;
		$providerMarkup = '';

		foreach ($data['allBookings'] as $booking)
		{
			$bookingCounter++;
			$bookingAmount = $booking['bookingAmount'];
			$advancePaid = $booking['advanceAmount'];
			$balanceAmount = $booking['bookingAmount'] - $booking['advanceAmount'];

			//your provider card
			$providerMarkup .= '<div style="padding-bottom:15px;">
									<div style="float:left">
										<div style="font-size:15px; margin-left:5px"><span style="padding:3px 10px;background: #efefef;border-radius:2px;color:#333">' . $this->getTruncateString($booking['typeBookingDetails']) . '</span></div>
										<div style="padding:5px; display: none"><b>' . $booking['provider']['person'] . '</b> (' . $booking['provider']['phone'] . ')</div>
									</div>
									<div style="float: right;">
										<div style=" text-align:right; font-size:15px;">Balance Amount: <span style="font-size:18px">&#8377;' . $this->formatPriceWithoutChar($balanceAmount) . '</span></div>
										<div style=" font-size:12px;">Advance Paid:   <span style="font-size:18px"> &#8377;' . $this->formatPriceWithoutChar($advancePaid) . '</span> Booking Amount: <span style="font-size:18px"> &#8377;' . $this->formatPriceWithoutChar($bookingAmount) . '</span></div>
									</div>
									<div style="clear: both;"></div>';

			if ($bookingCounter !== count($data['allBookings']))
			{
				$providerMarkup .= '<hr style="width: 90%;margin:10px auto 0 auto">';
			}

			$providerMarkup .= '</div>';
			$additionalChargesMarkup = '';

			if ((isset($data['additional']['internetHandlingFee']) && ($data['additional']['internetHandlingFee'] > 0)) || (isset($data['couponDiscount']) && ($data['couponDiscount'] > 0)))
			{
				$additionalChargesMarkup = '<div>';
				if ($data['additional']['internetHandlingFee'] > 0)
				{
					$additionalChargesMarkup .= '<ul style="display:table;table-layout:fixed;padding: 0;margin: 5px 0 0 0;list-style:none;width:100%;/* border-bottom: 1px solid black; */">
								<li style="display:table-cell;text-align: left;">
									<div>Internet Handling Charges</div>
								</li>
								<li style="display:table-cell;text-align: right;">
									<div>&#8377; ' . $data['additional']['internetHandlingFee'] . '</div>
								</li>
							</ul>';
				}

				if ($data['couponDiscount'] > 0)
				{
					$additionalChargesMarkup .= '<ul style="display:table;table-layout:fixed;padding: 0;margin: 5px 0 0 0;list-style:none;width:100%;/* border-bottom: 1px solid black; */">
								<li style="display:table-cell;text-align: left;">
									<div>Coupon Discount</div>
								</li>
								<li style="display:table-cell;text-align: right;">
									<div>&#8377; ' . $data['couponDiscount'] . '</div>
								</li>
							</ul>';
				}

				$additionalChargesMarkup .= '<hr></div>';
			}

			$venueDetailsMarkup = str_replace(array_keys($venueDetailsReplaces), array_values($venueDetailsReplaces), $venueDetailsMarkup);
		}

		$cardYourProviderMarkup = file_get_contents(base_path('resources/views/emails/orders/customer/br_your_provider.html'));
		$cardYourProviderReplaces = [
			'@totalBookingAmount@'      => $this->formatPriceWithoutChar($data['totalBookingAmount']),
			'@totalAdvancePaid@'        => $this->formatPriceWithoutChar($data['totalAdvancePaid']),
			'@totalBalanceAmount@'      => $this->formatPriceWithoutChar($data['totalBalanceAmount']),
			'@providerMarkup@'          => $providerMarkup,
			'@additionalChargesMarkup@' => $additionalChargesMarkup,
		];

		$cardYourProviderMarkup = str_replace(array_keys($cardYourProviderReplaces), array_values($cardYourProviderReplaces), $cardYourProviderMarkup);

		// customer contact
		$card4Markup = file_get_contents(base_path('resources/views/emails/orders/customer/br_customer_contact.html'));
		$altPhone = $data['additional']['altPhone'];
		$altPhone = $altPhone ? $altPhone : " -- ";
		$card4Replaces = [
			'@custName@'  => $data['customer']['name'],
			'@custPhone@' => $data['customer']['phone'],
			'@custAltPh@' => $altPhone,
		];
		$card4Markup = str_replace(array_keys($card4Replaces), array_values($card4Replaces), $card4Markup);

		$markup = "<div style='background-color:#d9d9d9;padding:30px 20px;width:700px;font-size:14px;line-height:22px;'>";
		$markup .= $card1Markup . $addOnDetailsMarkup . $partyDetailsCardMarkup . $cardYourProviderMarkup . $venueDetailsMarkup . $card4Markup;
		$markup .= $cancellationMarkup . $termsMarkup;
		$markup .= "</div>"; // close
		$markup .= "<div style='padding-top:10px;font-size:12px;color:#999'>If you are receiving the message in Spam or Junk folder, please mark it as 'not spam' and add senders id to contact list or safe list.</div>";

		return $markup;
	}
}
