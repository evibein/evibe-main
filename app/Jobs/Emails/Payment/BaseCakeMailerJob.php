<?php

namespace App\Jobs\Emails\Payment;

use App\Models\Util\CheckoutField;

class BaseCakeMailerJob extends BaseMailerJob
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	protected $data;
	protected $weight;
	protected $slot;
	protected $deliveryCharge;

	public function __construct($data)
	{
		$this->data = $data;
		$this->weight = $this->formatWeight($this->getCheckoutFieldValueByName(config('evibe.checkout_field.cake.weight')));
		$this->slot = $this->getCheckoutFieldValueByName(config('evibe.checkout_field.cake.delivery_slot'));
		$this->deliveryCharge = $this->getCheckoutFieldValueByName(config('evibe.checkout_field.cake.delivery_charge'));
	}

	public function checkCakeField()
	{
		$extraFields = [];
		$flavour = $this->getCheckoutFieldValueByName(config('evibe.checkout_field.cake.flavour'));
		$type = $this->getCheckoutFieldValueByName(config('evibe.checkout_field.cake.type'));
		$message = $this->getCheckoutFieldValueByName(config('evibe.checkout_field.cake.message'));
		$deliveryCharge = $this->deliveryCharge;

		if ($flavour)
		{
			$extraFields['flavour'] = $flavour;
		}

		if ($type)
		{
			$extraFields['type'] = $type;
		}

		if ($message)
		{
			$extraFields['message'] = $message;
		}

		if ($deliveryCharge)
		{
			$extraFields['deliveryCharge'] = $deliveryCharge;
		}

		return $extraFields;
	}

	public function getCheckoutFieldValueByName($id)
	{
		$data = $this->data;
		$checkoutField = CheckoutField::find($id);
		$value = null;

		if ($checkoutField && isset($data['additional'][trim($checkoutField->name)]))
		{
			$value = $data['additional'][trim($checkoutField->name)];
		}

		return $value;
	}

	protected function formatWeight($weight)
	{
		return strtolower(ucfirst($weight));
	}

	// cake tabular information for order info
	protected function extraCakeInfo()
	{
		$markup = "";
		$type = "Egg";
		$data = $this->checkCakeField();

		if (!empty($data['type']))
		{
			$type = $data['type'];
		}
		$markup = "<div><strong>Type:</strong> " . $type . "</div>";

		if (!empty($data['flavour']))
		{
			$markup .= "<div><strong>Flavour:</strong> " . $data['flavour'] . "</div>";
		}

		if (!empty($data['message']))
		{
			$markup .= "<div><strong>Message on Cake:</strong> " . $data['message'] . "</div>";
		}

		return $markup;

	}

	protected function getCakePriceWithMarkup()
	{
		$data = $this->data;
		$price = $data['booking']['bookingAmount'];
		$balanceAmount = $data['booking']['bookingAmount'] - $data['booking']['advanceAmount'];
		$deliveryCharge = 0;
		$balanceAmountMarkup = "";

		if (!empty($this->deliveryCharge))
		{
			$deliveryCharge = (int)$this->deliveryCharge;
		}

		if ($deliveryCharge)
		{
			$price = (int)($price - $deliveryCharge);
			$deliveryMarkup = "Rs. " . $this->formatPriceWithoutChar($deliveryCharge);
		}
		else
		{
			$deliveryMarkup = "<span style='color:rgba(0, 169, 12, 0.83)'> Free </span>";
		}

		if ($balanceAmount)
		{
			$balanceAmountMarkup = "<div><strong>Balance Amount:</strong> " . $this->formatPriceWithoutChar($balanceAmount) . "</div>";
		}

		return [
			'price'               => $this->formatPriceWithoutChar($price),
			'deliveryMarkup'      => $deliveryMarkup,
			'orderAmount'         => $data['booking']['bookingAmount'],
			'balanceAmountMarkup' => $balanceAmountMarkup
		];
	}
}
