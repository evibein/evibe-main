<?php

namespace App\Jobs\Emails\Payment;

use App\Jobs\Emails\UtilEmail;
use App\Models\Payment\PaymentInitiations;
use Exception;

class PaymentInitiationUpdate extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * saving payment initiated to DB.
	 */
	public function handle()
	{
		$data = $this->data;

		try
		{
			PaymentInitiations::create([
				                           "ticket_id"    => isset($data["ticketId"]) ? $data["ticketId"] : null,
				                           "piab_id"      => isset($data["piabId"]) ? $data["piabId"] : null,
				                           "browser_info" => $data["browserInfo"],
				                           "previous_url" => $data["previousUrl"],
				                           "ip_address"   => $data["ipAddress"]
			                           ]);
		} catch (Exception $exception)
		{
			$this->sendFailedJobsToTeam($exception);
		}
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}