<?php

namespace App\Jobs\Emails\Payment;

use App\Models\Package\Package;
use App\Models\Ticket\TicketBookingGallery;

class BaseCampaignMailerJob extends BaseMailerJob
{
	public function getCampaignMailData($data)
	{
		$package = Package::find($data['booking']['itemMapId']);
		$baseBookingAmount = $package->price;

		$emailData = [
			'date'        => date('d/m/y h:i A'),
			'evibePhone'  => config('evibe.contact.company.phone'),
			'delivery'    => $data['booking']['bookingAmount'] - $baseBookingAmount,
			'baseAmt'     => $this->formatPrice($baseBookingAmount),
			'deliveryAmt' => $this->formatPrice($data['booking']['bookingAmount'] - $baseBookingAmount),
			'bookAmt'     => $this->formatPrice($data['booking']['bookingAmount']),
			'advPd'       => $this->formatPrice($data['booking']['advanceAmount']),
			'balAmt'      => $this->formatPrice($data['booking']['bookingAmount'] - $data['booking']['advanceAmount']),
			'bizHead'     => config('evibe.contact.business.phone'),
			'evibeLink'   => config('evibe.host'),
			'evibeEmail'  => config('evibe.contact.company.email'),
			'startTime'   => config('evibe.contact.company.working.start_time'),
			'endTime'     => config('evibe.contact.company.working.end_time'),
		];

		$data['imageLinks'] = TicketBookingGallery::where('ticket_booking_id', $data['booking']['id'])->get()->toArray();

		return array_merge($data, $emailData);
	}
}