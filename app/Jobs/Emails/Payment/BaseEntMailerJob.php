<?php

namespace App\Jobs\Emails\Payment;

use App\Models\Ticket\Ticket;
use App\Models\Types\TypeServices;
use App\Models\Util\CheckoutField;
use App\Models\Util\CheckoutFieldValue;

class BaseEntMailerJob extends BaseMailerJob
{
	public function getEntMailData($data)
	{
		$ent = TypeServices::find($data['booking']['itemMapId']);
		$baseBookingAmount = $ent->min_price;

		$ticket = Ticket::find($data['ticketId']);
		$occasionId = $ticket->event_id;

		// get ent checkout fields with values
		$entFields = $this->getEntCheckoutFields($occasionId, $data['booking']['typeTicketBookingId']);
		$entCheckoutFieldsWithValues = $this->getEntCheckoutFieldsWithValuesInArray($entFields, $data['ticketId']);

		$emailData = [
			'date'           => date('d/m/y h:i A'),
			'evibePhone'     => config('evibe.contact.company.phone'),
			'delivery'       => $data['booking']['bookingAmount'] - $baseBookingAmount,
			'baseAmt'        => $this->formatPrice($baseBookingAmount),
			'deliveryAmt'    => $this->formatPrice($data['booking']['bookingAmount'] - $baseBookingAmount),
			'bookAmt'        => $this->formatPrice($data['booking']['bookingAmount']),
			'advPd'          => $this->formatPrice($data['booking']['advanceAmount']),
			'balAmt'         => $this->formatPrice($data['booking']['bookingAmount'] - $data['booking']['advanceAmount']),
			'checkoutFields' => $entCheckoutFieldsWithValues,
			'bizHead'        => config('evibe.contact.business.phone'),
			'evibeLink'      => config('evibe.host'),
			'evibeEmail'     => config('evibe.contact.company.email'),
			'startTime'      => config('evibe.contact.company.working.start_time'),
			'endTime'        => config('evibe.contact.company.working.end_time'),
		];

		return array_merge($data, $emailData);
	}

	public function getEntCheckoutFields($eventId, $typeTicketBookingId)
	{
		$checkoutFields = CheckoutField::where('type_ticket_booking_id', $typeTicketBookingId)
		                               ->where(function ($query) use ($eventId)
		                               {
			                               $query->where('event_id', $eventId)
			                                     ->orWhereNull('event_id');
		                               })
		                               ->where('is_crm', 0)
		                               ->where('is_auto_booking', 1)
		                               ->get();

		return $checkoutFields;
	}

	public function getEntCheckoutFieldsWithValuesInArray($decorFields, $ticketId)
	{
		$decorCheckoutFields = [];

		foreach ($decorFields as $decorField)
		{
			$decorCheckoutField = [
				$decorField->identifier => $this->getEntCheckoutValue($ticketId, $decorField->id)
			];
			$decorCheckoutFields = array_merge($decorCheckoutFields, $decorCheckoutField);
		}

		return $decorCheckoutFields;
	}

	private function getEntCheckoutValue($ticketId, $id)
	{
		$checkoutValue = CheckoutFieldValue::where([
			                                           'ticket_id'         => $ticketId,
			                                           'checkout_field_id' => $id,
		                                           ])->first();

		return $checkoutValue->value;
	}
}