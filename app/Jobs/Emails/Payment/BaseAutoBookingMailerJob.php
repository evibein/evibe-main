<?php

namespace App\Jobs\Emails\Payment;

use App\Models\Decor\Decor;
use App\Models\Ticket\Ticket;
use App\Models\Util\CheckoutField;
use App\Models\Util\CheckoutFieldValue;

class BaseAutoBookingMailerJob extends BaseMailerJob
{
	public function getABMailData()
	{
		$emailData = [
			'date'           => date('d/m/y h:i A'),
			'evibePhone'     => config('evibe.contact.company.phone'),
			'bizHead'        => config('evibe.contact.business.phone'),
			'evibeLink'      => config('evibe.host'),
			'evibeEmail'     => config('evibe.contact.company.email'),
			'startTime'      => config('evibe.contact.company.working.start_time'),
			'endTime'        => config('evibe.contact.company.working.end_time'),
		];

		return $emailData;
	}

	public function getDecorMailData($data)
	{
		$decor = Decor::find($data['booking']['itemMapId']);
		$baseBookingAmount = $decor->min_price;

		$ticket = Ticket::find($data['ticketId']);
		$occasionId = $ticket->event_id;

		// get decor checkout fields with values
		$decorFields = $this->getDecorCheckoutFields($occasionId, $data['booking']['typeTicketBookingId']);
		$decorCheckoutFieldsWithValues = $this->getDecorCheckoutFieldsWithValuesInArray($decorFields, $data['ticketId']);

		$emailData = [
			'date'           => date('d/m/y h:i A'),
			'evibePhone'     => config('evibe.contact.company.phone'),
			'delivery'       => $data['booking']['bookingAmount'] - $baseBookingAmount,
			'baseAmt'        => $this->formatPrice($baseBookingAmount),
			'deliveryAmt'    => $this->formatPrice($data['booking']['bookingAmount'] - $baseBookingAmount),
			'bookAmt'        => $this->formatPrice($data['booking']['bookingAmount']),
			'advPd'          => $this->formatPrice($data['booking']['advanceAmount']),
			'balAmt'         => $this->formatPrice($data['booking']['bookingAmount'] - $data['booking']['advanceAmount']),
			'checkoutFields' => $decorCheckoutFieldsWithValues,
			'bizHead'        => config('evibe.contact.business.phone'),
			'evibeLink'      => config('evibe.host'),
			'evibeEmail'     => config('evibe.contact.company.email'),
			'startTime'      => config('evibe.contact.company.working.start_time'),
			'endTime'        => config('evibe.contact.company.working.end_time'),
		];

		return array_merge($data, $emailData);
	}

	public function getDecorCheckoutFields($eventId, $typeTicketBookingId)
	{
		$checkoutFields = CheckoutField::where('type_ticket_booking_id', $typeTicketBookingId)
		                               ->where(function ($query) use ($eventId)
		                               {
			                               $query->where('event_id', $eventId)
			                                     ->orWhereNull('event_id');
		                               })
		                               ->where('is_crm', 0)
		                               ->where('is_auto_booking', 1)
		                               ->get();

		return $checkoutFields;
	}

	public function getDecorCheckoutFieldsWithValuesInArray($decorFields, $ticketId)
	{
		$decorCheckoutFields = [];

		foreach ($decorFields as $decorField)
		{
			$decorCheckoutField = [
				$decorField->identifier => $this->getDecorCheckoutValue($ticketId, $decorField->id)
			];
			$decorCheckoutFields = array_merge($decorCheckoutFields, $decorCheckoutField);
		}

		return $decorCheckoutFields;
	}

	private function getDecorCheckoutValue($ticketId, $id)
	{
		$checkoutValue = CheckoutFieldValue::where([
			                                           'ticket_id'         => $ticketId,
			                                           'checkout_field_id' => $id,
		                                           ])->first();

		return $checkoutValue->value;
	}
}