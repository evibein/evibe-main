<?php

namespace App\Jobs\Emails\Payment;

use App\Jobs\Emails\UtilEmail;
use App\Models\Util\AccessToken;
use App\Models\Util\AuthClient;
use Illuminate\Support\Facades\Log;

class BaseMailerJob extends UtilEmail
{
	/**
	 * Get the email markup from the file
	 */
	public function getEmailMarkup($template)
	{
		$emailMarkup = file_get_contents(base_path('resources/views/emails/orders/' . $template));

		return $emailMarkup;
	}

	/**
	 * Replaces the email markup variables with user data
	 */
	public function replaceEmailVariables($emailMarkup, $cardReplaces)
	{
		$emailContent = str_replace(array_keys($cardReplaces), array_values($cardReplaces), $emailMarkup);

		return $emailContent;
	}

	public function getMergedEmailContent($data)
	{
		$markup = "<div style='background-color:#F5F5f5;padding:10px 20px;width:700px;font-size:14px;line-height:22px;'>";
		$markup .= $data;
		$markup .= "</div>";
		$markup .= "<div style='padding-top:10px;font-size:12px;color:#999'>If you are receiving the message in Spam or Junk folder, please mark it as 'not spam' and add senders id to contact list or safe list.</div>";

		return $markup;
	}

	public function getCustomerTermsAndCancellation()
	{
		$cancellationMarkup = file_get_contents(base_path('resources/views/emails/orders/customer/cancellation.html'));
		$termsMarkup = file_get_contents(base_path('resources/views/emails/orders/customer/terms.html'));

		return $cancellationMarkup . $termsMarkup;
	}

	// Get the Package Names only upto 10 chars
	public function getShortName($name, $len = 10)
	{
		$shortName = (strlen($name) > $len) ? substr($name, 0, $len) . '...' : $name;

		return $shortName;
	}

	public function getAllVendorEmail($vendor)
	{
		$emails = $vendor['email'];
		$allEmailFields = ['altEmail', 'altEmail1', 'altEmail2', 'altEmail3'];

		foreach ($allEmailFields as $altEmail)
		{
			if ($vendor[$altEmail])
			{
				$emails .= ", $vendor[$altEmail]";
			}
		}

		return $emails;
	}

	public function getVegLogoPath()
	{
		return config('evibe.gallery.host') . '/img/icons/venue-facilities/green_veg.png';
	}

	public function getNonVegLogoPath()
	{
		return config('evibe.gallery.host') . '/img/icons/venue-facilities/red_nonveg.png';
	}

	public function getPartnerEmailSalutation()
	{
		$markup = "	<div>Best Regards,</div>
				    <div>Your wellwishers at <a href='" . config('evibe.host') . "' style='color:#333'>Evibe.in</div>";

		return $markup;
	}

	public function getAccessTokenFromUser($user)
	{
		$clientId = config('evibe.api.client_id');
		$accessToken = false;

		$token = AccessToken::where('user_id', $user->id)
		                    ->where('auth_client_id', $clientId)
		                    ->first();

		if (!$token)
		{
			$client = AuthClient::where('auth_client_id', $clientId)->first();
			if ($client)
			{
				$accessToken = $this->issueAccessToken($user, $client);
			}
			else
			{
				Log::error("Invalid client is trying to make the api request");
			}
		}
		else
		{
			$accessToken = $token->access_token;
		}

		return $accessToken;
	}

	public function issueAccessToken($user, $client)
	{
		$accessToken = new AccessToken;
		$accessToken->access_token = $this->getNewAccessToken();
		$accessToken->auth_client_id = $client->auth_client_id;
		$accessToken->user_id = $user->id;

		$accessToken->save();

		return $accessToken->access_token;
	}

	private function getNewAccessToken($len = 40)
	{
		$stripped = '';
		do
		{
			$bytes = openssl_random_pseudo_bytes($len, $strong);

			// We want to stop execution if the key fails because, well, that is bad.
			if ($bytes === false || $strong === false)
			{
				// @codeCoverageIgnoreStart
				throw new \Exception('Error Generating Key');
				// @codeCoverageIgnoreEnd
			}
			$stripped .= str_replace(['/', '+', '='], '', base64_encode($bytes));
		} while (strlen($stripped) < $len);

		return substr($stripped, 0, $len);
	}
}