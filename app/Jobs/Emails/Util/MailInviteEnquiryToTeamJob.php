<?php

namespace App\Jobs\Emails\Util;

use App\Jobs\Emails\Payment\BaseMailerJob;
use Illuminate\Support\Facades\Mail;

class MailInviteEnquiryToTeamJob extends BaseMailerJob
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */

	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$data["sub"] = "[P1] Invite user details - Send payment link";
		$data["mailView"] = 'emails.e-cards.invite-user-details';
		$data["fromText"] = 'Invite Team';
		$data["toEmail"] = config('evibe.contact.tech.group');

		Mail::send($data['mailView'], ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.contact.company.email'), $data['fromText'])
			  ->to($data['toEmail'])
			  ->subject($data['sub']);
		});
	}
}