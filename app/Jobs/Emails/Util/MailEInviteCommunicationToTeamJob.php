<?php

namespace App\Jobs\Emails\Util;

use App\Jobs\Emails\UtilEmail;
use App\Models\Types\TypeEvent;
use App\Models\Util\City;
use Exception;
use Illuminate\Support\Facades\Mail;

class MailEInviteCommunicationToTeamJob extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job - Send receipt email
	 * Sent to customer with vendor, handler and admin in cc
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$data['occasion'] = isset($data['eventId']) && $data['eventId'] ? TypeEvent::find($data['eventId'])->name : null;
		$data['city'] = isset($data['cityId']) && $data['cityId'] ? City::find($data['cityId'])->name : null;

		$data['sub'] = "[#" . $data["ticketId"] . "] A customer wants E-Invite for party on " . $data['partyDate'];

		Mail::send('emails.util.e-invite-team', ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.contact.company.email'), 'Team Evibe')
			  ->to(config('evibe.contact.invitations.group'))
			  ->replyTo(config('evibe.contact.invitations.group'))
			  ->subject($data['sub']);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}