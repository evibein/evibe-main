<?php

namespace App\Jobs\Emails\Util;

use App\Jobs\Emails\Payment\BaseMailerJob;
use Illuminate\Support\Facades\Mail;
use Exception;

class MailPartnerQueryToTeamJob extends BaseMailerJob
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		Mail::send('emails.enquiry.partner_query', ['data' => $data], function ($m) use ($data)
		{
			$m->from(config('evibe.contact.company.email'), 'Team Evibe')
			  ->to($data['to'], 'Business')
			  ->replyTo($data['replyTo'])
			  ->subject($data['sub']);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}
