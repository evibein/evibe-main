<?php

namespace App\Jobs\Emails\Util;

use App\Jobs\Emails\UtilEmail;
use Illuminate\Support\Facades\Mail;
use Exception;

class UpdatePartyLocationAlertToTeamJob extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$data['to'] = config('evibe.contact.customer.group');
		$data['from'] = config('evibe.contact.company.email');
		$data['sub'] = '[#' . $data['ticketId'] . '] Update party location of ticket';

		Mail::send('emails.util.update-party-location', ['data' => $data], function ($m) use ($data)
		{
			$m->from($data['from'], 'Update party location')
			  ->to($data['to'])
			  ->subject($data['sub']);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}
