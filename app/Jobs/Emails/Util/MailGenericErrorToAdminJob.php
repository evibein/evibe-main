<?php

namespace App\Jobs\Emails\Util;

use App\Jobs\Emails\Payment\BaseMailerJob;
use Illuminate\Support\Facades\Mail;
use Exception;

class MailGenericErrorToAdminJob extends BaseMailerJob
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$data['sub'] = '[Main Error] Code: ' . $data['code'] . ' - ' . date('F j, Y, g:i a');

		Mail::send('emails.errors.generic-error', ['data' => $data], function ($m) use ($data)
		{
			$m->from(config('evibe.contact.company.email'), 'Evibe Errors')
			  ->to(config('evibe.contact.tech.group'))
			  ->replyTo(config('evibe.contact.tech.group'))
			  ->subject($data['sub']);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}
