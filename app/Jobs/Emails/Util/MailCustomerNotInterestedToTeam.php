<?php

namespace App\Jobs\Emails\Util;

use App\Jobs\Emails\UtilEmail;
use App\Models\Types\TypeEvent;
use App\Models\Util\City;
use Exception;
use Illuminate\Support\Facades\Mail;

class MailCustomerNotInterestedToTeam extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job - Send receipt email
	 * Sent to customer with vendor, handler and admin in cc
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$data['sub'] = "[#".$data["ticketId"]."] A customer is not interested in booking with us";

		$data['occasion'] = isset($data['eventId']) && $data['eventId'] ? TypeEvent::find($data['eventId'])->name : null;
		$data['city'] = isset($data['cityId']) && $data['cityId'] ? City::find($data['cityId'])->name : null;
		$data['dashLink'] = config('evibe.dash_host') . '/tickets/' . $data['ticketId'] . '/information';

		Mail::send('emails.util.customer-not-interested', ['data' => $data], function ($m) use ($data)
		{
			$m->from(config('evibe.contact.company.email'), 'Not Interested Alert')
			  ->to(config('evibe.contact.customer.group'))
			  ->subject($data['sub']);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}