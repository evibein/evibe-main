<?php

namespace App\Jobs\Emails\Util;

use App\Jobs\Emails\UtilEmail;
use App\Models\Types\TypeEvent;
use App\Models\Util\City;
use Exception;
use Illuminate\Support\Facades\Mail;

class MailCustomerProofUploadAlertToCustomer extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job - Send receipt email
	 * Sent to customer with vendor, handler and admin in cc
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$data['sub'] = '[#' . $data['ticketId'] . '] Your upload is successful';

		if (isset($data['customerEmail']) && $data['customerEmail'])
		{
			Mail::send('emails.util.proof-upload-customer', ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.company.email'), 'Team Evibe')
				  ->to($data['customerEmail'])
				  ->replyTo(config('evibe.contact.company.email'))
				  ->subject($data['sub']);
			});
		}
		else
		{
			Mail::send('emails.util.proof-upload-customer', ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.company.email'), 'Team Evibe')
				  ->to(config('evibe.contact.tech.group'))
				  ->subject('[Invalid Email Address]. Sub: ' . $data['sub']);
			});
		}

	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}