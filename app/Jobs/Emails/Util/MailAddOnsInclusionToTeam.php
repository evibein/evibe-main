<?php

namespace App\Jobs\Emails\Util;

use App\Jobs\Emails\UtilEmail;
use App\Models\Types\TypeEvent;
use App\Models\Util\City;
use Exception;
use Illuminate\Support\Facades\Mail;

class MailAddOnsInclusionToTeam extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job - Send receipt email
	 * Sent to customer with vendor, handler and admin in cc
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$data['occasion'] = isset($data['additional']['eventId']) && $data['additional']['eventId'] ? TypeEvent::find($data['additional']['eventId'])->name : null;
		$data['city'] = isset($data['additional']['cityId']) && $data['additional']['cityId'] ? City::find($data['additional']['cityId'])->name : null;

		$data['sub'] = "[#" . $data['ticketId'] . "] A customer has included add-ons for party on " . $data['additional']['partyDate'];

		Mail::send('emails.util.add-on-team', ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.contact.company.email'), 'Team Evibe')
			  ->to(config('evibe.contact.customer.group'))
			  ->subject($data['sub']);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}