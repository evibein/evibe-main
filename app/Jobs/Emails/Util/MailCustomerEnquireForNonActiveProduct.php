<?php

namespace App\Jobs\Emails\Util;

use App\Jobs\Emails\UtilEmail;
use Illuminate\Support\Facades\Mail;
use Exception;

class MailCustomerEnquireForNonActiveProduct extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$data['to'] = $data['customerEmail'];
		$data['from'] = config('evibe.contact.company.email');
		$data['sub'] = $data['customerName'] . " thanks for placing an enquiry";

		Mail::send('emails.auto-book.package-not-live-alert-customer', ['data' => $data], function ($m) use ($data) {
			$m->from($data['from'], 'Team Evibe')
			  ->to($data['to'])
			  ->subject($data['sub']);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}