<?php

namespace App\Jobs\Emails\Util;

use App\Jobs\Emails\UtilEmail;
use Exception;
use Illuminate\Support\Facades\Mail;

class MailCustomerFailToProceedABToTeamJob extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job - Send receipt email
	 * Sent to customer with vendor, handler and admin in cc
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$data['sub'] = "[#" . $data["ticketId"] . "] A customer did feasibility check - " . $data['phone'] . " - " . $data['typeDevice'];

		Mail::send('emails.auto-book.fail-to-proceed-alert-team', ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.contact.company.system_alert_email'), 'AB Pre-Init')
			  ->to(config('evibe.contact.operations.alert_no_action_email'))
			  ->subject($data['sub']);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}