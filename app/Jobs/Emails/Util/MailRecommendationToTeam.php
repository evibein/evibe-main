<?php

namespace App\Jobs\Emails\Util;

use App\Jobs\Emails\UtilEmail;
use Illuminate\Support\Facades\Mail;
use Exception;

class MailRecommendationToTeam extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$ticket = $data['ticket'];
		$data['ticketLink'] = config('evibe.dash_host') . '/tickets/' . $ticket->id;

		$data['to'] = config('evibe.contact.customer.group');
		$data['from'] = config('evibe.contact.company.email');

		Mail::send('emails.recos.reco_like', ['data' => $data], function ($m) use ($data)
		{
			$m->from($data['from'], 'Team Evibe')
			  ->to($data['to'])
			  ->subject($data['sub']);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}