<?php

namespace App\Jobs\Emails\Util;

use App\Jobs\Emails\UtilEmail;
use Illuminate\Support\Facades\Mail;
use Exception;

class MailTeamCustomerEnquireForNonActiveProduct extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$data['to'] = config('evibe.contact.customer.group');
		$data['from'] = config('evibe.contact.company.email');
		$data['sub'] = "Customer Enquired for Non-Active Product";

		Mail::send('emails.auto-book.package-not-live-alert-team', ['data' => $data], function ($m) use ($data) {
			$m->from($data['from'], 'Team Evibe')
			  ->to($data['to'])
			  ->subject($data['sub']);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}