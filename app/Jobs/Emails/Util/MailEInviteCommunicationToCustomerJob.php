<?php

namespace App\Jobs\Emails\Util;

use App\Jobs\Emails\UtilEmail;
use App\Models\Types\TypeEvent;
use App\Models\Util\City;
use Exception;
use Illuminate\Support\Facades\Mail;

class MailEInviteCommunicationToCustomerJob extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job - Send receipt email
	 * Sent to customer with vendor, handler and admin in cc
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$data['occasion'] = isset($data['eventId']) && $data['eventId'] ? TypeEvent::find($data['eventId'])->name : null;

		$data['sub'] = '[ACTION NEEDED] Please submit details to get e-invitation card';
		$data['cc'] = [config('evibe.contact.invitations.group')];
		if (isset($data['customerAltEmail']) && $data['customerAltEmail'])
		{
			array_push($data['cc'], $data['customerAltEmail']);
		}

		if (isset($data['customerEmail']) && $data['customerEmail'])
		{
			Mail::send('emails.util.e-invite-customer', ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.invitations.group'), 'Evibe Invites')
				  ->to($data['customerEmail'])
				  ->cc($data['cc'])
				  ->replyTo(config('evibe.contact.invitations.group'))
				  ->subject($data['sub']);
			});
		}
		else
		{
			Mail::send('emails.util.e-invite-customer', ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.invitations.group'), 'Evibe Invites')
				  ->to(config('evibe.contact.tech.group'))
				  ->subject('[Invalid Email Address]. Sub: ' . $data['sub']);
			});
		}

	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}