<?php

namespace App\Jobs\Emails\Review;

use App\Jobs\Emails\UtilEmail;
use App\Models\Util\SiteErrorLog;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class MailPartnerAutoReviewJob extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		try
		{
			$data = $this->data;
			$client = new Client();
			$res = $client->request('POST', $data['url']);
			$res = $res->getBody();
			$res = \GuzzleHttp\json_decode($res, true);

			if (!$res['success'])
			{
				array_push($data, ["error" => "API automate feedback job dispatch failure"]);
				$this->sendErrorMessage($data);
			}
		} catch (ClientException $e)
		{
			$apiResponse = $e->getResponse()->getBody(true);
			$apiResponse = \GuzzleHttp\json_decode($apiResponse);
			$res['error'] = $apiResponse->errorMessage;

			array_push($data, ["error" => $res["error"]]);
			$this->sendErrorMessage($data);
		}
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}

	public function sendErrorMessage($data)
	{
		$inputData = [
			"url"        => $data['url'],
			"code"       => "MAIN_JOB_ERROR",
			"project_id" => 1,
			"details"    => $data['error'],
			"created_at" => Carbon::now(),
			"updated_at" => Carbon::now()
		];

		SiteErrorLog::create($inputData);
	}
}