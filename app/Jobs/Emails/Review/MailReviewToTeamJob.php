<?php

namespace App\Jobs\Emails\Review;

use App\Jobs\Emails\UtilEmail;
use Exception;

class MailReviewToTeamJob extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$mailData = [
			'to'   => [
				config('evibe.contact.customer.group'),
				config('evibe.contact.business.group'),
				config('evibe.contact.admin.email')
			],
			'from' => 'Review Alert<ping@evibe.in>',
			'sub'  => '[#' . $data['ticketId'] . '] Received review from ' . $data['customer']['name'] . ' - ' . $data['customer']['email'] . ' - ' . $data['customer']['phone'],
			'body' => $this->getEmailMarkup($data)
		];

		$this->sendMail($mailData);
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}

	private function getEmailMarkup($data)
	{
		$isRecommend = $data["isRecommend"] ? 'Yes' : 'No';
		$vendorReviewMarkup = '<div style="font-size:15px;font-weight:600; margin-bottom:15px; width: 80%;">Partner Rating</div>';
		$evibeFeedback = $data["moreFeedback"] ? $data["moreFeedback"] : ' --- ';
		$sixStarReview = $data["sixStar"] ? $data["sixStar"] : ' --- ';
		$handlerName = array_key_exists("handlerName", $data) ? $data['handlerName'] : "Team";

		if (isset($data['partnerRatingsData']))
		{
			foreach ($data['partnerRatingsData'] as $vendor)
			{
				$comment = $vendor['comment'] ? $vendor['comment'] : ' --- ';
				$vendorReviewMarkup .= '<table border="1" style="border-collapse: collapse;margin-bottom: 15px;">
										<tr>
											<td  colspan="2" style="padding: 8px;"><b>Ratings for ' . $vendor['type'] . ' ' . $vendor['name'] . ' (scale of 5) </b></td>
										</tr>';
				if (array_key_exists('partnerReview', $vendor))
				{
					foreach ($vendor['partnerReview'] as $partnerReview)
					{
						$vendorReviewMarkup .= '<tr>
											<td style="padding:8px"><b>' . $partnerReview['question'] . '</b></td>
											<td style="padding:8px">' . $partnerReview['ratings'] . '</td>
										</tr>';
					}
				}

				$vendorReviewMarkup .= '<tr>
											<td style="padding:8px"><b>Comment</b></td>
											<td style="padding:8px">' . $comment . '</td>
										</tr>';
				$vendorReviewMarkup .= '</table>';
			}
		}

		$body = '
			<div style="background-color:#F5F5F5;padding:10px;max-width:700px;">
				<div style="padding: 20px 30px 10px 30px;background-color: #FFFFFF;">
					<div>
						<div style="float: left;">
							<div>
								<a href="https://evibe.in">
									<img src="https://gallery.evibe.in/img/logo/logo_evibe.png" alt="">
								</a>
							</div>
						</div>
						<div style="float:right;">
							<div>
								<span>Website: </span>
								<span><a target="_blank" href="https://evibe.in">www.evibe.in</a></span>
							</div>
							<div style="padding-top:10px;">Phone: ' . config('evibe.contact.company.phone') . '</div>
						</div>
						<div style="clear:both;"></div>
					</div>
					<hr>
					<div style="padding-top: 10px;">
						<p>Hello Team,</p>
						<p>We have received a new customer feedback. Check below for details: </p>
						<p>
							' . $vendorReviewMarkup . '
							<div style="font-size:15px;font-weight:600; margin-bottom:15px; width: 80%;">Evibe Ratings</div>
							<table border="1" style="width:80%;border-collapse:collapse;">
									<tr>
										<td style="padding:8px">
											<b>Customer care (' . $handlerName . ')</b></td>
										<td style="padding:8px">' . $data['customerCare'] . '</td>
									</tr>
									<tr>
										<td style="padding:8px"><b>Ease of booking</b></td>
										<td style="padding:8px">' . $data['easeOfBooking'] . '</td>
									</tr>
									<tr>
										<td style="padding:8px;"><b>Do you recommend Evibe to friends?</b></td>
										<td style="padding:8px;">' . $isRecommend . '</td>
									</tr>';
		if (isset($data['extraQData']) && count($data['extraQData']))
		{
			foreach ($data['extraQData'] as $extraQus)
			{
				$body .= '<tr>
                       	<td style="padding:8px;"><b>' . $extraQus['question'] . '</b></td>
										<td style="padding:8px;">' . $extraQus['answer'] . '</td>
                    </tr>';
			}
		}

		$body .= '<tr>
										<td style="padding:8px;"><b>How was your experience with Evibe.in?</b></td>
										<td style="padding:8px;">' . $evibeFeedback . '</td>
									</tr>
									<tr>
										<td style="padding:8px;"><b>What convinced you to book from us?</b></td>
										<td style="padding:8px;">' . $sixStarReview . '</td>
									</tr>
									<tr>
										<td style="padding:8px;"><b>Ticket Id</b></td>
										<td style="padding:8px;">' . $data["ticketId"] . '</td>
									</tr>
									<tr>
										<td style="padding:8px;"><b>Customer Name</b></td>
										<td style="padding:8px;">' . $data["customer"]["name"] . '</td>
									</tr>
									<tr>
										<td style="padding:8px;"><b>Email</b></td>
										<td style="padding:8px;">' . $data["customer"]["email"] . '</td>
									</tr>
									<tr>
										<td style="padding:8px;"><b>Phone</b></td>
										<td style="padding:8px;">' . $data["customer"]["phone"] . '</td>
									</tr>
									<tr>
										<td style="padding:8px;"><b>Occasion</b></td>
										<td style="padding:8px;">' . $data["occasion"] . '</td>
									</tr>
									<tr>
										<td style="padding:8px;"><b>City</b></td>
										<td style="padding:8px;">' . $data["city"] . '</td>
									</tr>
							</table>
						</p>
						<p>
							<div>Regards,</div>
							<div>Admin</div>
						</p>
					</div>
				</div>
			</div>
		';

		return $body;
	}
}