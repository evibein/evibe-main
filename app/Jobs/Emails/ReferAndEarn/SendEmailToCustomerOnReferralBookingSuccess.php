<?php

namespace App\Jobs\Emails\ReferAndEarn;

use App\Jobs\Emails\Payment\BaseMailerJob;
use App\Models\Coupon\Coupon;
use App\Models\Util\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendEmailToCustomerOnReferralBookingSuccess extends BaseMailerJob
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 3;

	/**
	 * The number of seconds the job can run before timing out.
	 *
	 * @var int
	 */
	public $timeout = 120;

	public function __construct($data)
	{
		$this->data = $data;
		$this->onConnection("sqs_loggers")
		     ->onQueue(config('queue.connections.sqs_loggers.queue'));
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$referredCustomer = User::find($data["user"]["referred_by"]);

		if ($referredCustomer)
		{
			try
			{
				$client = new Client();

				$response = $client->request('POST', config("evibe.api.base_url") . "coupon/generate", [
					'headers' => [
						'access-token' => $this->getAccessTokenFromUser($data["user"])
					],
					'json'    => [
						"couponPrefix"   => "RE",
						"stringLength"   => 8,
						"userId"         => $data["user"]["referred_by"],
						"discountAmount" => "200",
						"minOrder"       => "2000",
						"description"    => "Refer & Earn customer coupon for friend success signup.",
						"offerEndTime"   => Carbon::now()->addYear()->toDateTimeString()
					]
				]);

				$response = $response->getBody();
				$response = \GuzzleHttp\json_decode($response, true);

				if ($response['success'])
				{
					$hashContent = str_replace(' ', '', $referredCustomer->id . $referredCustomer->name);
					$userToken = Hash::make((string)$hashContent);
					$dashBoardUrl = config("evibe.host") . "/my/re/share?id=" . $referredCustomer->id . "&token=" . $userToken . "&ref=email";
					$coupon = Coupon::where("coupon_code", $response["couponCode"])->first();

					$data['sub'] = "Congrats " . $referredCustomer->name . ", here’s your Rs 200* OFF for a successful referral";
					$data["referralCode"] = $response["couponCode"];
					$data["friendName"] = $data["user"]->name;
					$data["dashboardUrl"] = $dashBoardUrl;
					$data["whatsappShareLink"] = $dashBoardUrl . "#EmailWhatsapp";
					$data["messengerShareLink"] = $dashBoardUrl . "#EmailMessenger";
					$data["smsShareLink"] = $dashBoardUrl . "#EmailSMS";
					$data["validUpto"] = $coupon ? date('d M Y', strtotime($coupon->offer_end_time)) : "";

					Mail::send('emails.refer-earn.customer-reward-booking-success', ['data' => $data], function ($m) use ($data, $referredCustomer) {
						$m->from(config('evibe.contact.company.email'), 'Evibe.in')
						  ->to($referredCustomer->username)
						  ->subject($data['sub']);
					});
				}
			} catch (ClientException $e)
			{
				$apiResponse = $e->getResponse()->getBody(true);
				$apiResponse = \GuzzleHttp\json_decode($apiResponse);
				$res['error'] = $apiResponse->errorMessage;
			}
		}
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}