<?php

namespace App\Jobs\Emails\ReferAndEarn;

use App\Jobs\Emails\Payment\BaseMailerJob;
use App\Models\Util\User;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class EmailToCustomerAfterFeedback extends BaseMailerJob
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 3;

	/**
	 * The number of seconds the job can run before timing out.
	 *
	 * @var int
	 */
	public $timeout = 120;

	public function __construct($data)
	{
		$this->data = $data;
		$this->onConnection("sqs_loggers")
		     ->onQueue(config('queue.connections.sqs_loggers.queue'));
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$ticket = $this->data;
		$customer = User::where("username", $ticket->email)->first();

		if ($customer && $customer->referral_code)
		{
			$data = [
				"email"       => $customer->username,
				"subject"     => 'Exclusive: Get Rs 200* OFF for every friend you refer',
				"referralUrl" => config("evibe.main_url") . "re/" . $customer->referral_code
			];

			$hashContent = str_replace(' ', '', $customer->id . $customer->name);
			$userToken = Hash::make((string)$hashContent);
			$dashBoardUrl = config("evibe.host") . "/my/re/share?id=" . $customer->id . "&token=" . $userToken . "&ref=email";

			$data["dashboardUrl"] = $dashBoardUrl;
			$data["whatsappShareLink"] = $dashBoardUrl . "#EmailWhatsapp";
			$data["messengerShareLink"] = $dashBoardUrl . "#EmailMessenger";
			$data["smsShareLink"] = $dashBoardUrl . "#EmailSMS";

			Mail::send('emails.refer-earn.referral-link-to-customer', ["data" => $data], function ($mail) use ($data) {
				$mail->from(config('evibe.contact.company.email'), 'Evibe.in');
				$mail->to($data["email"]);
				$mail->subject($data["subject"]);
			});
		}
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}