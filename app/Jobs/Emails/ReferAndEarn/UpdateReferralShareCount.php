<?php

namespace App\Jobs\Emails\ReferAndEarn;

use App\Jobs\BaseJob;
use App\Models\Referral\LogReferralPreActions;
use Exception;

class UpdateReferralShareCount extends BaseJob
{

	private $data;

	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 3;

	/**
	 * The number of seconds the job can run before timing out.
	 *
	 * @var int
	 */
	public $timeout = 120;

	public function __construct($data)
	{
		$this->data = $data;
		$this->onConnection("sqs_loggers")
		     ->onQueue(config('queue.connections.sqs_loggers.queue'));
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		LogReferralPreActions::create([
			                              "customer_id" => $data["customerId"],
			                              "type_action" => $data["typeAction"],
			                              "source_id"   => $data["sourceId"]
		                              ]);
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}