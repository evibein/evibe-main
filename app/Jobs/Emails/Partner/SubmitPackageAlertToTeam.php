<?php

namespace App\Jobs\Emails\Partner;

use App\Jobs\Emails\UtilEmail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Exception;

class SubmitPackageAlertToTeam extends UtilEmail
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */

	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		if ($data['is_update'] == 1)
		{
			$data['sub'] = '[Partner Portal] Partner has updated a package from partner portal';
		}
		else
		{
			$data['sub'] = '[Partner Portal] Partner has added a new package from partner portal';
		}

		Mail::send('emails.partner.submit_package_team', ['data' => $data], function ($m) use ($data)
		{
			$m->from(config('evibe.contact.company.email'), 'Evibe.in')
			  ->to(config('evibe.contact.business.group'), 'Business Team')
			  ->subject($data['sub']);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}
