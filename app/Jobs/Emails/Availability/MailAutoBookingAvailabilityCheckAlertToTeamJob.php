<?php

namespace App\Jobs\Emails\Availability;

use App\Jobs\Emails\Payment\BaseMailerJob;
use Illuminate\Support\Facades\Mail;

class MailAutoBookingAvailabilityCheckAlertToTeamJob extends BaseMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job - Send receipt email
	 * Sent to customer with vendor, handler and admin in cc
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$data['sub'] = "A customer initiated auto booking for a booked option";

		Mail::send('emails.auto-book.option-unavailability', ['data' => $data], function ($m) use ($data)
		{
			$m->from(config('evibe.contact.company.system_alert_email'), 'AB Option Unavailable')
			  ->to(config('evibe.contact.operations.alert_no_action_email'))
			  //->cc(config('evibe.contact.operations.group'))
			  ->subject($data['sub']);
		});

	}
}