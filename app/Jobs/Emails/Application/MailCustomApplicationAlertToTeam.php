<?php

namespace App\Jobs\Emails\Application;

use App\Jobs\Emails\UtilEmail;
use Illuminate\Support\Facades\Mail;
use Exception;

class MailCustomApplicationAlertToTeam extends UtilEmail
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */

	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$data['sub'] = 'Application form by ' . $data['name'];

		Mail::send('emails.application.custom_application_team', ['data' => $data], function ($m) use ($data)
		{
			$m->from(config('evibe.contact.company.email'), 'Job Alert')
			  ->to(config('evibe.contact.hire.email'))
			  ->subject($data['sub']);

			$m->attach($data['resume']);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}
