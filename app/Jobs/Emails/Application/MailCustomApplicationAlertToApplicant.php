<?php

namespace App\Jobs\Emails\Application;

use App\Jobs\Emails\UtilEmail;
use Illuminate\Support\Facades\Mail;
use Exception;

class MailCustomApplicationAlertToApplicant extends UtilEmail
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */

	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$data['sub'] = ucfirst($data['name']) . ', we have received your application';

		if ($data['email'])
		{
			Mail::send('emails.application.custom_application_applicant', ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.company.email'), 'Team Evibe.in')
				  ->to($data['email'])
				  ->subject($data['sub']);

				$m->attach($data['resume']);
			});
		}
		else
		{
			Mail::send('emails.application.custom_application_applicant', ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.company.email'), 'Team Evibe.in')
				  ->to(config('evibe.contact.customer.group'))
				  ->cc(config('evibe.contact.tech.group'))
				  ->subject('[Invalid Email Address]. Sub: ' . $data['sub']);

				$m->attach($data['resume']);
			});
		}
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}
