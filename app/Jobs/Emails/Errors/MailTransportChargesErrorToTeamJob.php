<?php

namespace App\Jobs\Emails\Errors;

use App\Jobs\Emails\UtilEmail;
use Exception;
use Illuminate\Support\Facades\Mail;

class MailTransportChargesErrorToTeamJob extends UtilEmail
{
	private $data;

	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 3;

	/**
	 * The number of seconds the job can run before timing out.
	 *
	 * @var int
	 */
	public $timeout = 120;

	/**
	 * Create a new job instance.
	 *
	 * @param $data
	 */
	public function __construct($data)
	{
		$this->data = $data;

		// run on a separate queue connection
		$this->onConnection("sqs_reporters")
		     ->onQueue(config('queue.connections.sqs_reporters.queue'));
	}

	/**
	 * Execute the job - Send receipt email
	 * Sent to customer with vendor, handler and admin in cc
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$data['sub'] = '[#' . $data['ticketId'] . '] Error in Transportation Charges';

		Mail::send('emails.errors.transport-charges-alert-team', ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.contact.company.email'), 'Trans Alert')
			  ->to(config('evibe.contact.customer.group'))
			  ->cc(config("evibe.contact.tech.group"))
			  ->subject($data['sub']);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}