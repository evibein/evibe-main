<?php

namespace App\Jobs\Emails\Errors;

use App\Jobs\Emails\UtilEmail;
use Exception;

class MailFeedbackSubmitErrorToAdminJob extends UtilEmail
{
	private $data;

	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 3;

	/**
	 * The number of seconds the job can run before timing out.
	 *
	 * @var int
	 */
	public $timeout = 120;

	/**
	 * Create a new job instance.
	 *
	 * @param $data
	 */
	public function __construct($data)
	{
		$this->data = $data;

		// run on a separate queue connection
		$this->onConnection("sqs_reporters")
		     ->onQueue(config('queue.connections.sqs_reporters.queue'));
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$body = 'Hello Admin, <br><br>' .
			'An error occurred while saving feedback from a customer. Check details below and fix it ASAP: ' .
			'<br><br>';

		$body .= '<em>Ticket Id: </em><strong>' . $data['ticketId'] . '</strong><br>' .
			'<em>Recommend Evibe?: </em><strong>' . $data['isRecommend'] . '</strong><br>' .
			'<br><br>' .
			'Best Regards,<br>' .
			'Admin<br>' .
			'<br>';

		$mailData = [
			'to'   => [config('evibe.contact.admin.email')],
			'from' => 'Fail Alert<enquiry@evibe.in>',
			'sub'  => '[#' . $data['ticketId'] . '] Failed to save feedback',
			'body' => $body
		];

		$this->sendMail($mailData);
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}