<?php

namespace App\Jobs\Emails\Errors;

use App\Jobs\Emails\UtilEmail;
use Exception;

class MailSMSErrorToAdminJob extends UtilEmail
{
	private $data;

	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 3;

	/**
	 * The number of seconds the job can run before timing out.
	 *
	 * @var int
	 */
	public $timeout = 120;

	/**
	 * Create a new job instance.
	 *
	 * @param $data
	 */
	public function __construct($data)
	{
		$this->data = $data;

		// run on a separate queue connection
		$this->onConnection("sqs_reporters")
		     ->onQueue(config('queue.connections.sqs_reporters.queue'));
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$mailData = [
			'to'      => [$data['to']],
			'from'    => "SMS Error Alert <ping@evibe.in>",
			'replyTo' => ['ping@evibe.in'],
			'sub'     => $data['subject'],
			'body'    => isset($data['text']) ? $data['text'] : 'SMS Sending Failed, check and fix at the earliest. Data unavailable. Number will be in the subject'
		];

		$this->sendMail($mailData);
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}