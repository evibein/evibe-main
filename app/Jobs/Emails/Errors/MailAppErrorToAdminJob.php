<?php

namespace App\Jobs\Emails\Errors;

use App\Jobs\Emails\UtilEmail;
use Exception;

class MailAppErrorToAdminJob extends UtilEmail
{
	private $data;

	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 1;

	/**
	 * The number of seconds the job can run before timing out.
	 *
	 * @var int
	 */
	public $timeout = 120;

	/**
	 * Create a new job instance.
	 *
	 * @param $data
	 */
	public function __construct($data)
	{
		$this->data = $data;

		// run on a separate queue connection
		$this->onConnection("sqs_reporters")
		     ->onQueue(config('queue.connections.sqs_reporters.queue'));
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$to = [config('evibe.contact.tech.group')];
		$from = 'High Alert <ping@evibe.in>';
		$errorSub = '[Main] ' . $data['code'] . ' occurred on website - ' . date('F j, Y, g:i a');
		$markup = "<div style='padding: 10px;'>
					<p>Hi Team,</p>
					<p>An error occurred in the application, please rectify it ASAP.</p>
					<table border='1' style='margin-top:10px; border-collapse: collapse'>
						 <tr>
							   <th style='padding:8px'>Url</th>
							   <td style='padding:8px'>" . $data['url'] . "</td>
	                     </tr>
	                      <tr>
							   <th style='padding:8px'>Code</th>
							   <td style='padding:8px'>" . $data['code'] . "</td>
	                     </tr>
	                      <tr>
							   <th style='padding:8px'>Method</th>
							   <td style='padding:8px'>" . $data['method'] . "</td>
	                     </tr>
	                     <tr>
							   <th style='padding:8px'>Exception</th>
							   <td style='padding:8px'>" . $data['messages'] . "</td>
	                     </tr>
	                      <tr>
							   <th style='padding:8px'>Trace</th>
							   <td style='padding:8px'>" . $data['details'] . "</td>
	                     </tr>
                    </table>
                  </div>";
		$mailData = [
			'to'   => $to,
			'from' => $from,
			'sub'  => $errorSub,
			'body' => $markup
		];

		$this->sendMail($mailData);
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}