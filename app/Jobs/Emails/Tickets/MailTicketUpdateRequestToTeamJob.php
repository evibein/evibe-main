<?php

namespace App\Jobs\Emails\Tickets;

use Illuminate\Support\Facades\Mail;
use Exception;

class MailTicketUpdateRequestToTeamJob extends UtilMailTicket
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		if (isset($data['requestType']) && ($data['requestType'] == "Raise Complaint") && isset($data['customerEmail']) && $data['customerEmail'])
		{
			$subject = "[#" . $data["ticketId"] . "] [POST BOOKING] Complaint raised for Order";

			Mail::send('emails.tmo.complaint-customer', ['data' => $data], function ($m) use ($subject, $data) {
				$m->from(config('evibe.contact.company.email'), "Team Evibe")
				  ->to($data['customerEmail'])
				  ->cc(config('evibe.contact.customer.group'))
				  ->subject($subject);
			});
		}
		else
		{
			$subject = "[#" . $data["ticketId"] . "] [POST BOOKING] Order Update Request from Customer";

			Mail::send('emails.enquiry.post_booking_customer_alert_team', ['data' => $data], function ($m) use ($subject) {
				$m->from(config('evibe.contact.company.email'), "Team Evibe")
				  ->to(config('evibe.contact.customer.group'))
				  ->subject($subject);
			});
		}
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}