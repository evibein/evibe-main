<?php

namespace App\Jobs\Emails\Tickets;

use Exception;

class MailTicketSuccessToPartyBagCustomerJob extends UtilMailTicket
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$to = [$data['email']];
		$from = 'Team Evibe <enquiry@evibe.in>';
		$ticketSub = "[#" . $data["ticketId"] . "] Regarding your event on " . date("d M Y", $data["event_date"]);
		$body = 'Namaste ' . $data['name'] . ',<br><br>';

		switch ($data['type_ticket_id'])
		{
			case 1:
				$body .= 'We have received an enquiry for the package <b>' . $data['vendor_usp'] . '</b>.';
				break;

			case 2:
				$body .= 'We have received an enquiry for the vendor <b>' . $data['vendor_usp'] . '</b>.';
				break;

			case 3:
				$body .= 'We have received an enquiry for the venue <b>' . $data['vendor_usp'] . '</b>.';
				break;

			case 4:
				$body .= 'We have received an enquiry for the service <b>' . $data['vendor_usp'] . '</b>.';
				break;

			case 5:
				$body .= 'We have received an enquiry for the trending experience <b>' . $data['vendor_usp'] . '</b>.';
				break;

			case 12:
				$body .= 'We have received an enquiry for the experience <b>' . $data['vendor_usp'] . '</b>.';
				break;

			case 13:
				$body .= 'We have received an enquiry for the decor style <b>' . $data['vendor_usp'] . '</b>.';
				break;

			default:
				$body .= 'We have received an enquiry related to party services.';
				break;

		}

		$checkMyBagButton = file_get_contents(base_path('resources/views/emails/enquiry/partybagbutton.html'));

		$body .= '<br><br>' .
			'Please note your enquiry number: ' . '<strong>#' . $data['enquiryId'] . '</strong>.<br><br>' .
			$checkMyBagButton .
			'Our party planning expert will contact you shortly. However, if you need anything urgent, ' .
			'we recommend you reply to this email. We will respond to you ASAP' .
			'<br><br>' .
			'We look forward to assisting you.<br>' .
			'Best Regards,<br>' .
			'Team Evibe<br>' .
			'<a href="http://youtu.be/8HdTF43Djko" target="_blank">Every birthday is special</a>' .
			'<br><br>';

		// include next steps
		$nextStepsMarkup = file_get_contents(base_path('resources/views/emails/enquiry/process.html'));
		$body .= $nextStepsMarkup;

		$body .= '<br><br>';
		$body .= '--------------------------------------------<br>' .
			'Your Ticket Details<br>' .
			'--------------------------------------------<br><br>' .
			'<em>Name: </em><strong>' . $data['name'] . '</strong><br>' .
			'<em>Email: </em><strong>' . $data['email'] . '</strong><br>' .
			'<em>Phone Number: </em><strong>' . $data['phone'] . '</strong><br>' .
			'<em>Party Date: </em><strong>' . date("d/m/y", $data['event_date']) . '</strong><br>';

		if ($data['budget'])
		{
			$body .= '<em>Budget: </em><strong> Rs. ' . $data['budget'] . '</strong><br>';
		}

		if (isset($data['comments']) && $data['comments'])
		{
			$body .= '<em>Requirements: </em><strong>' . $data['comments'] . '</strong><br>';
		}

		if (array_key_exists('party_location', $data) && $data['party_location'])
		{
			$body .= '<em>Party Location: </em><strong>' . $data['party_location'] . '</strong><br>';
		}

		if (array_key_exists('guests_count', $data) && $data['guests_count'])
		{
			$body .= '<em>Guests Count: </em><strong>' . $data['guests_count'] . '</strong><br>';
		}

		switch ($data['type_ticket_id'])
		{
			case 1:
				$body .= '<em>Package Requested: </em><strong>' . $data['vendor_usp'] . '</strong><br>';
				break;

			case 2:
				$body .= '<em>Vendor Code: </em><strong>' . $data['vendor_code'] . '</strong><br>';
				break;

			case 3:
				$body .= '<em>Venue Code: </em><strong>' . $data['vendor_code'] . '</strong><br>';
				break;

			case 4:
				$body .= '<em>Service Requested: </em><strong>' . $data['vendor_usp'] . '</strong><br>';
				break;

			case 5:
				$body .= '<em>Trend: </em><strong>' . $data['vendor_usp'] . '</strong><br>';
				break;

			case 12:
				$body .= '<em>Experience: </em><strong>' . $data['vendor_usp'] . '</strong><br>';
				break;
		}

		$body .= '<br><br>';

		$mailData = [
			'to'   => $to,
			'from' => $from,
			'sub'  => $ticketSub,
			'body' => $body
		];

		$this->sendMail($mailData);
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}