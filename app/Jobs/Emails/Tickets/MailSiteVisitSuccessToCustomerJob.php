<?php

namespace App\Jobs\Emails\Tickets;

use Illuminate\Support\Facades\Mail;
use Exception;

class MailSiteVisitSuccessToCustomerJob extends UtilMailTicket
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$data['sub'] = '[#' . $data['ticketId'] . '] ' . ucfirst($data['name']) . ', thanks for placing your enquiry for site visit';

		if ($data['email'])
		{
			Mail::send('emails.enquiry.enquiry_site_visit_customer', ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.customer.group'), 'Team Evibe.in')
				  ->to($data['email'])
				  ->subject($data['sub']);
			});
		}
		else
		{
			Mail::send('emails.enquiry.enquiry_site_visit_customer', ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.customer.group'), 'Team Evibe.in')
				  ->to(config('evibe.contact.customer.group'))
				  ->cc(config('evibe.contact.tech.group'))
				  ->subject('[Invalid Email Address]. Sub: ' . $data['sub']);
			});
		}
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}