<?php

namespace App\Jobs\Emails\Tickets;

use Illuminate\Support\Facades\Mail;
use Exception;

class MailAutoPopupTicketSuccessToCustomerJob extends UtilMailTicket
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		if (is_null($data["date"]) || !($data['date']))
		{
			$data['sub'] = "[#" . $data['ticketId'] . "] Regarding your event";
		}
		else
		{
			$data['sub'] = "[#" . $data['ticketId'] . "] Regarding your event on " . date("d M Y", $data["date"]);
		}
		Mail::send('emails.enquiry.enquiry_auto_popup_ticket_alert_customer', ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.contact.customer.group'), 'Team Evibe')
			  ->to($data['email'])
			  ->subject($data['sub']);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}