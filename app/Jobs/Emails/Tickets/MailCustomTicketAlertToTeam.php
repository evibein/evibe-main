<?php

namespace App\Jobs\Emails\Tickets;

use Illuminate\Support\Facades\Mail;
use Exception;

class MailCustomTicketAlertToTeam extends UtilMailTicket
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */

	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$data['sub'] = isset($data['teamSub']) ? $data['teamSub'] : '[#' . $data['ticketId'] . '] [' . $data['occasion'] . '] Enquiry for custom ' . strtolower($data['raisedFor']) . ' by ' . $data['name'];

		switch ($data['typeId'])
		{
			case config('evibe.ticket.type.cake'):
			case config('evibe.ticket.type.decor'):
				$data['view'] = 'emails.enquiry.custom_ticket_team';

				break;

			case config('evibe.ticket.type.food'):
				$data['view'] = 'emails.enquiry.custom_ticket_food_team';

				break;

		}

		Mail::send($data['view'], ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.contact.company.system_alert_email'), 'Custom Ticket Alert')
			  ->to(config('evibe.contact.operations.alert_no_action_email'))
			  ->replyTo(config('evibe.contact.customer.group'))
			  ->subject($data['sub']);

			if (isset($data['image']) && $data['image'])
			{
				$m->attach($data['image']);
			}

			if (isset($data['images']) && (count($data['images']) > 0))
			{
				foreach ($data['images'] as $image)
				{
					$m->attach($image);
				}
			}
		});

	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}
