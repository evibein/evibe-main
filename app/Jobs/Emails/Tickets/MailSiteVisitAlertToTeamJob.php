<?php

namespace App\Jobs\Emails\Tickets;

use Illuminate\Support\Facades\Mail;
use Exception;

class MailSiteVisitAlertToTeamJob extends UtilMailTicket
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$subject = "[#" . $data["ticketId"] . "] New site visit enquiry from " . $data["name"] . " in " . $data["cityName"];

		Mail::send('emails.enquiry.enquiry_site_visit_alert_team', ['data' => $data], function ($m) use ($subject) {
			$m->from(config('evibe.contact.company.email'), "Site Visit Alert")
			  ->to(config('evibe.contact.customer.group'))
			  ->subject($subject);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}