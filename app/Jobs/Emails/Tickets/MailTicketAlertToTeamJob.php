<?php

namespace App\Jobs\Emails\Tickets;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Exception;

class MailTicketAlertToTeamJob extends UtilMailTicket
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$subject = isset($data["city"]) ? "[" . $data["city"] . "] " : "";
		$subject .= isset($data["occasion"]) ? "[" . $data["occasion"] . "] " : "";
		$subject .= "Party on " . date("d/m/y", $data['event_date']) . " from " . $data["name"];
		//$data['customerPreviousStatus'] = $this->getMsgBasedOnPreviousStatus($data['email'], $data['phone']);
		// @see: default message because eof unknown alert. Need to test in staging
		$data['customerPreviousStatus'] = "Default message, needs to be tested in staging";

		Mail::send('emails.enquiry.enquiry_ticket_alert_team', ['data' => $data], function ($m) use ($subject) {
			$m->from(config('evibe.contact.company.email'), "New Ticket")
			  ->to(config('evibe.contact.customer.group'))
			  ->subject($subject);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}