<?php

namespace App\Jobs\Emails\Tickets;

use Illuminate\Support\Facades\Mail;
use Exception;

class MailCustomTicketAlertToCustomer extends UtilMailTicket
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */

	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		switch ($data['typeId'])
		{
			case config('evibe.ticket.type.cake'):
			case config('evibe.ticket.type.decor'):
				$data['sub'] = '[#' . $data['ticketId'] . '] ' . $data['name'] . ', we have received your designs';
				$data['view'] = 'emails.enquiry.custom_ticket_customer';

				break;

			case config('evibe.ticket.type.food'):
				$data['sub'] = '[#' . $data['ticketId'] . '] ' . $data['name'] . ', we have received your menu';
				$data['view'] = 'emails.enquiry.custom_ticket_food_customer';

				break;

		}

		if ($data['email'])
		{
			Mail::send($data['view'], ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.company.email'), 'Team Evibe.in')
				  ->to($data['email'])
				  ->subject($data['sub']);

				if (isset($data['image']) && $data['image'])
				{
					$m->attach($data['image']);
				}

				if (isset($data['images']) && (count($data['images']) > 0))
				{
					foreach ($data['images'] as $image)
					{
						$m->attach($image);
					}
				}
			});
		}
		else
		{
			Mail::send($data['view'], ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.company.email'), 'Team Evibe.in')
				  ->to(config('evibe.contact.customer.group'))
				  ->cc(config('evibe.contact.tech.group'))
				  ->subject('[Invalid Email Address]. Sub: ' . $data['sub']);

				if (isset($data['image']) && $data['image'])
				{
					$m->attach($data['image']);
				}

				if (isset($data['images']) && (count($data['images']) > 0))
				{
					foreach ($data['images'] as $image)
					{
						$m->attach($image);
					}
				}
			});
		}
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}
