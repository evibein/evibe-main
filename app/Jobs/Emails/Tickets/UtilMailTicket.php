<?php

namespace App\Jobs\Emails\Tickets;

use App\Jobs\Emails\UtilEmail;
use App\Models\Ticket\Ticket;
use App\Models\Util\SiteErrorLog;
use Carbon\Carbon;

class UtilMailTicket extends UtilEmail
{
	public function getMsgBasedOnPreviousStatus($email, $phone)
	{
		try
		{
			$ticket = Ticket::where(function ($query) use ($email, $phone) {
				$query->where('email', $email)
				      ->orwhere('phone', $phone);
			})
			                ->where('deleted_at', null)
			                ->orderBy('updated_at', 'desc')
			                ->get();

			$currentTimestamp = Carbon::now()->timestamp;
			$message = "";

			if ($ticket->count() > 1)
			{
				if ($ticketIsBooked = $ticket->where('status_id', config('evibe.ticket.status.booked'))->first())
				{
					$status = $this->checkTimeInterval($ticketIsBooked);
					if ($status == 1)
					{
						if ($ticketIsBooked->event_date < $currentTimestamp)
						{
							$message = config('evibe.ticket.messages.customer_booked_party_done_less_30');
						}
						else
						{
							$message = config('evibe.ticket.messages.customer_booked_party_not_done_less_30');
						}
					}
					else
					{
						if ($ticketIsBooked->event_date < $currentTimestamp)
						{
							$message = config('evibe.ticket.messages.customer_booked_party_done_more_30');
						}
						else
						{
							$message = config('evibe.ticket.messages.customer_booked_party_not_done_more_30');
						}
					}
				}
				elseif ($ticket->where('status_id', config('evibe.ticket.status.confirmed'))->first())
				{
					// More than 30days is not possible because, It will go to cancelled state
					$message = config('evibe.ticket.messages.customer_confirmed_status_less_30');
				}
				else
				{
					$otherStatus = [config('evibe.ticket.status.auto_paid'), config('evibe.ticket.status.auto_cancel'), config('evibe.ticket.status.cancelled'), config('evibe.ticket.status.service_auto_pay')];
					$ticketOtherStatus = $ticket->filter(function ($item) use ($otherStatus) {
						return in_array($item->status_id, $otherStatus);
					})->first();

					if ($ticketOtherStatus)
					{
						$status = $this->checkTimeInterval($ticketOtherStatus);

						if ($ticketOtherStatus->status_id == config('evibe.ticket.status.auto_cancel'))
						{
							$message = config('evibe.ticket.messages.customer_auto_cancelled');
						}
						elseif ($ticketOtherStatus->status_id == config('evibe.ticket.status.cancelled'))
						{
							$message = config('evibe.ticket.messages.customer_cancelled');
						}
						elseif ($status == 1)
						{
							$message = config('evibe.ticket.messages.default_less_30');
						}
					}
					else
					{
						$message = config('evibe.ticket.messages.default_more_30');
					}
				}
			}
			elseif ($ticket->count() == 1)
			{
				$message = "New customer, haven't raised any enquiry from us in the past";
			}

			return $message;
		} catch (\Exception $e)
		{
			$inputData = [
				"url"        => request()->fullUrl(),
				"exception"  => $e->getMessage(),
				"code"       => $e->getCode(),
				"project_id" => config('evibe.project_id'),
				"details"    => $e->getTraceAsString(),
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			];

			SiteErrorLog::create($inputData);

			return "";
		}
	}

	//function to check the Time Interval that it is less than 30 days or more than 30 days
	public function checkTimeInterval($ticket)
	{
		$bookingUpdatedDate = strtotime($ticket->updated_at);
		if ($bookingUpdatedDate && $bookingUpdatedDate > 0)
		{
			$diffInDays = round((time() - $bookingUpdatedDate) / (3600 * 24));
		}
		else
		{
			$bookingCreatedDate = strtotime($ticket->created_at);
			if ($bookingCreatedDate && $bookingCreatedDate > 0)
			{
				$diffInDays = round((time() - $bookingCreatedDate) / (3600 * 24));
			}
		}

		//1 for less than or equal to 30 days 0 for more than 30
		$status = (isset($diffInDays) && ($diffInDays <= 30)) ? 1 : 0;

		return $status;
	}
}