<?php

namespace App\Jobs\Emails\Tickets;

use Illuminate\Support\Facades\Mail;

class MailGoogleAdLandingEnquiryToTeamJob extends UtilMailTicket
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */

	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$sub = "[#" . $data["ticketId"] . "] New ticket from " . $data["name"] . " - " . $data["phone"];

		Mail::send('emails.enquiry.google_ad_landing_ticket_alert', ['data' => $data], function ($m) use ($sub) {
			$m->from(config('evibe.contact.company.email'), 'GoogleAd Ticket')
			  ->to(config('evibe.contact.customer.group'))
			  ->subject($sub);
		});
	}
}