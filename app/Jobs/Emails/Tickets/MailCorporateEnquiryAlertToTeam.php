<?php

namespace App\Jobs\Emails\Tickets;

use App\Jobs\Emails\UtilEmail;
use Illuminate\Support\Facades\Mail;
use Exception;

class MailCorporateEnquiryAlertToTeam extends UtilEmail
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */

	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$data['sub'] = '[#' . $data['ticketId'] . '] Corporate Enquiry by ' . $data['name'];

		Mail::send('emails.tickets.corporate_enquiry_team', ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.contact.company.email'), 'Corporate Enquiry')
			  ->to(config('evibe.contact.business.group'))
			  ->subject($data['sub']);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}
