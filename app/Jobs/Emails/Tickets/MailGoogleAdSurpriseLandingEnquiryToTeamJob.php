<?php

namespace App\Jobs\Emails\Tickets;

use Illuminate\Support\Facades\Mail;

class MailGoogleAdSurpriseLandingEnquiryToTeamJob extends UtilMailTicket
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */

	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$data["source"] = isset($data["source"]) ? $data["source"] : "Ad";

		if (isset($data["isPIAB"]) && $data["isPIAB"])
		{
			$data["sub"] = "New ticket from " . $data["source"] . " PIAB landing page - " . $data["phone"];
			$data["mailView"] = 'emails.offer-landing.piab';
			$data["fromText"] = 'PIAB Ad Offer';
			$data["toEmail"] = config('evibe.contact.support.group');
		}
		else
		{
			$data["sub"] = "[#" . (isset($data["ticketId"]) && $data["ticketId"]) ? $data["ticketId"] : "Order" . "] New ticket from " . $data["source"] . " surprises landing page - " . $data["phone"];
			$data["mailView"] = 'emails.offer-landing.surprises';
			$data["fromText"] = 'Ad Ticket';
			$data["toEmail"] = config('evibe.contact.customer.group');
		}

		Mail::send($data['mailView'], ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.contact.company.email'), $data['fromText'])
			  ->to($data['toEmail'])
			  ->subject($data['sub']);
		});
	}
}