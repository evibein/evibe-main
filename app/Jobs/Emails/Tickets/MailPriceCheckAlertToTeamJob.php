<?php

namespace App\Jobs\Emails\Tickets;

use Illuminate\Support\Facades\Mail;
use Exception;

class MailPriceCheckAlertToTeamJob extends UtilMailTicket
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$subject = '[#' . $data['ticketId'] . '] Price of ' . $data['productName'] . ' has been checked for ' . $data['occasion'];

		Mail::send('emails.auto-book.price-check-alert-team', ['data' => $data], function ($m) use ($subject) {
			$m->from(config('evibe.contact.company.system_alert_email'), "AB Feasibility")
			  ->to(config('evibe.contact.operations.alert_no_action_email'))
			  ->subject($subject);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}