<?php

namespace App\Jobs\Emails\Tickets;

use Illuminate\Support\Facades\Mail;
use Exception;

class MailTicketSuccessToCustomerJob extends UtilMailTicket
{
	private $data;
	private $isCouponMail;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data, $isCouponMail = false)
	{
		$this->data = $data;
		$this->isCouponMail = $isCouponMail;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		if (!is_null($data['event_date']) || ($data['event_date'] > 0))
		{
			$data['sub'] = "[#" . (isset($data["ticketId"]) && $data["ticketId"]) ? $data["ticketId"] : "Order" . "] Regarding your event on " . date("d M Y", $data["event_date"]);

		}
		else
		{
			$data['sub'] = "[#" . (isset($data["ticketId"]) && $data["ticketId"]) ? $data["ticketId"] : "Order" . "] Regarding your event";
		}

		if ($data['email'])
		{
			if ($this->isCouponMail)
			{
				$data['sub'] = "Congrats. You have unlocked the offer";
				$data['coupon'] = config('evibe.valentines-day-landingpages.coupon-code');
				$data['maxLimit'] = config('evibe.valentines-day-landingpages.max-limit');
				Mail::send('emails.util.vday-coupon-code', ['data' => $data], function ($m) use ($data) {
					$m->from(config('evibe.contact.customer.group'), 'Team Evibe')
					  ->to($data['email'])
					  ->subject($data['sub']);
				});
			}
			else
			{
				Mail::send('emails.enquiry.enquiry_ticket_alert_customer', ['data' => $data], function ($m) use ($data) {
					$m->from(config('evibe.contact.customer.group'), 'Team Evibe')
					  ->to($data['email'])
					  ->subject($data['sub']);
				});
			}
		}
		else
		{
			Mail::send('emails.enquiry.enquiry_ticket_alert_customer', ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.customer.group'), 'Team Evibe')
				  ->to(config('evibe.contact.customer.group'))
				  ->cc(config('evibe.contact.tech.group'))
				  ->subject('[Invalid Email Address]. Sub: ' . $data['sub']);
			});
		}
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}