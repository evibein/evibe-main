<?php

namespace App\Jobs\Emails\Tickets;

use App\Models\Util\City;
use Illuminate\Support\Facades\Mail;
use Exception;

class MailAutoPopupAlertToTeamJob extends UtilMailTicket
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$city = City::find($data["city"]);
		$data["cityName"] = $city ? $city->name : "";

		$subject = ($data["isEnquiry"] != 2) ? "[#" . $data['ticketId'] . "] New Auto Popup enquiry from " . $data["name"] . " in " . $data["cityName"] : "[#" . $data['ticketId'] . "] New Enquiry from " . $data["name"] . " in " . $data["cityName"];
		$alertType = ($data["isEnquiry"] != 2) ? "Auto Popup Alert" : "Enquiry Alert";

		if ($data["surprisesLandingCityName"] != "" && $data["surprisesLandingTheatreName"] != "")
		{
			$subject = "[#" . $data['ticketId'] . "] New Theatre enquiry from " . $data["name"] . " in " . $data["surprisesLandingCityName"];
			$alertType = "Theatre Ad alert";
			$data["cityName"] = $data["surprisesLandingCityName"];
		}

		if (isset($data["imageUrl"]) && ($data["imageUrl"] != ""))
		{
			$subject = "[#" . $data['ticketId'] . "] New Delivery Image enquiry from " . $data["name"] . " in " . $data["cityName"];
			$alertType = "Delivery image alert";
		}

		if (isset($data["addOns"]) && count($data["addOns"]))
		{
			$subject .= " with " . count($data["addOns"]) . " add-ons selected";
		}

		$data['customerPreviousStatus'] = $this->getMsgBasedOnPreviousStatus($data['email'], $data['phone']);

		Mail::send('emails.enquiry.enquiry_auto_popup_alert_team', ['data' => $data], function ($m) use ($subject, $alertType) {
			$m->from(config('evibe.contact.company.email'), $alertType)
			  ->to(config('evibe.contact.customer.group'))
			  ->subject($subject);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}