<?php

namespace App\Jobs\Emails\Tickets;

use App\Models\Types\TypeEvent;
use App\Models\Types\TypeTicket;
use App\Models\Util\City;
use Illuminate\Support\Facades\Mail;
use Exception;

class MailAutoBookingTicketAlertToTeamJob extends UtilMailTicket
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$subject = '[#' . $data['ticketId'] . '] AutoBooking of ' . $data['typeTicket'] . ' has been initiated for ' . $data['occasion'];

		Mail::send('emails.auto-book.new-ticket-alert-team', ['data' => $data], function ($m) use ($subject)
		{
			$m->from(config('evibe.contact.company.email'), "New AutoBook Ticket")
			  ->to(config('evibe.contact.customer.group'))
			  ->subject($subject);
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}