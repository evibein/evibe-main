<?php

namespace App\Jobs\Emails;

use App\Jobs\BaseJob;
use Aws\Laravel\AwsFacade as AWS;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class UtilEmail extends BaseJob
{
	public function sendMail($data)
	{
		$ses = AWS::createClient('Ses');

		if ($data['to'])
		{
			$ses->sendEmail([
				                'Source'           => $data['from'],
				                'ReplyToAddresses' => array_key_exists('replyTo', $data) ? $data['replyTo'] : [],
				                'Destination'      => [
					                'ToAddresses'  => $data['to'],
					                'CcAddresses'  => array_key_exists('cc', $data) ? $data['cc'] : [],
					                'BccAddresses' => array_key_exists('bcc', $data) ? $data['bcc'] : []
				                ],
				                'Message'          => [
					                'Subject' => [
						                'Data' => $data['sub']
					                ],
					                'Body'    => [
						                'Html' => [
							                'Data' => $data['body']
						                ]
					                ]
				                ]
			                ]);
		}
		else
		{
			$ses->sendEmail([
				                'Source'           => $data['from'],
				                'ReplyToAddresses' => config('evibe.contact.customer.group'),
				                'Destination'      => [
					                'ToAddresses'  => config('evibe.contact.customer.group'),
					                'CcAddresses'  => config('evibe.contact.tech.group'),
					                'BccAddresses' => []
				                ],
				                'Message'          => [
					                'Subject' => [
						                'Data' => '[Invalid Email Address]. Sub: ' . $data['sub']
					                ],
					                'Body'    => [
						                'Html' => [
							                'Data' => $data['body']
						                ]
					                ]
				                ]
			                ]);
		}
	}

	public function getTrackMyOrderUrlTemplate($ticketId)
	{
		$token = Hash::make($ticketId . config('evibe.token.track-my-order'));

		return "<div style='padding: 25px 0 25px;text-align:center;'>
			<a href='" . route("track.orders") . "?ref=email&id=" . $ticketId . "&token=" . $token . "' style='font-size:20px;text-decoration:none;background-color:#4584ee;color:#ffffff;padding:8px 20px;border-radius:4px;' target='_blank'>
			Track My Order
			</a></div>";
	}

	public function EmailReceiptSuccessToTeam($ticketId, $notification = "", $sub = "")
	{
		$data = [
			"ticketId"     => $ticketId,
			"notification" => $notification != "" ? $notification : "Email was sent successfully to the customer"
		];

		$sub = $sub != "" ? $sub : "Email Delivered Successfully";

		Mail::send('emails/report/team/email-deliver-success', ['data' => $data], function ($m) use ($sub, $ticketId) {
			$m->from(config('evibe.contact.company.system_alert_email'), 'Evibe Team')
			  ->to(config('evibe.contact.operations.alert_no_action_email'))
			  ->subject("[#" . $ticketId . "] " . $sub);
		});
	}
}