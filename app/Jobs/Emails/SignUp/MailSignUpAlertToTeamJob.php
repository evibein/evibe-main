<?php

namespace App\Jobs\Emails\SignUp;

use App\Jobs\Emails\UtilEmail;
use Exception;

class MailSignUpAlertToTeamJob extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$to = [config('evibe.contact.business.group')];
		$from = 'Evibe Alerts<ping@evibe.in>';
		$signUpSub = '[Evibe.in] New partner sign-up alert - ' . $data['person'];
		$data['message'] = $data['message'] ? $data['message'] : ' --- ';
		$body = 'Hello Team, <br><br>' .
			'Congrats, we have a partner sign-up request. Check the details below: ' .
			'<br><br>' .
			'<em>Name: </em><strong>' . $data['person'] . '</strong><br>' .
			'<em>Company Name: </em><strong>' . $data['company_name'] . '</strong><br>' .
			'<em>Email: </em><strong>' . $data['email'] . '</strong><br>' .
			'<em>Phone Number: </em><strong>' . $data['phone'] . '</strong><br>' .
			'<em>Category: </em><strong>' . $data['categoryName'] . '</strong><br>' .
			'<em>City: </em><strong>' . $data['city_name'] . '</strong><br>' .
			'<em>Message: </em><strong>' . $data['message'] . '</strong><br>' .
			'<br><br>' .
			'Best Regards,<br>' .
			'Admin<br>' .
			'<br>';

		$mailData = [
			'to'   => $to,
			'from' => $from,
			'sub'  => $signUpSub,
			'body' => $body
		];

		$this->sendMail($mailData);
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}