<?php

namespace App\Jobs\Emails\SignUp;

use App\Jobs\Emails\UtilEmail;
use Exception;
use Illuminate\Support\Facades\Mail;

class MailCLDSignUpAlertToTeamJob extends UtilEmail
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 3;

	/**
	 * The number of seconds the job can run before timing out.
	 *
	 * @var int
	 */
	public $timeout = 120;

	public function __construct($data)
	{
		$this->data = $data;
		$this->onConnection("sqs_loggers")
		     ->onQueue(config('queue.connections.sqs_loggers.queue'));
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		Mail::send('emails.partner.cld_new_signup', ["data" => $data], function ($mail) use ($data) {
			$mail->from(config('evibe.contact.company.email'), 'Evibe.in');
			$mail->to(config('evibe.contact.tech.group'));
			$mail->subject("New partner sign up request for CLD");
		});
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}