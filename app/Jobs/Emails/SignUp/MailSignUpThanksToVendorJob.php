<?php

namespace App\Jobs\Emails\SignUp;

use App\Jobs\Emails\UtilEmail;
use Exception;

class MailSignUpThanksToVendorJob extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$to = [$data['email']];
		$from = 'Team Evibe <business@evibe.in>';
		$feedSub = '[Evibe.in] Thanks for your sign-up request';
		$body = 'Hello ' . $data['person'] . ', <br><br>' .
			'Thank you for showing interest in getting associated with us.' .
			' However, we would like to let you know that we are not a typical listing website and hence the first step is to talk / meet you directly to understand your offerings in detail.' .
			'<br><br>' .
			' We shall call you sometime soon to take this forward. Otherwise, please feel free to call us on ' . config('evibe.contact.company.phone') . '.' .
			'<br><br>' .
			' Look forward to get associated with you.' .
			'<br><br>' .
			'Best Regards,<br>' .
			'Team Evibe<br>' .
			'<a href="http://youtu.be/8HdTF43Djko" target="_blank">Every birthday is special</a><br>' .
			'<br><br>' .
			'--------------------------------------------<br>' .
			'Your Sign-up Details<br>' .
			'--------------------------------------------<br><br>' .
			'<em>Name: </em><strong>' . $data['person'] . '</strong><br>' .
			'<em>Company Name: </em><strong>' . $data['company_name'] . '</strong><br>' .
			'<em>Email: </em><strong>' . $data['email'] . '</strong><br>' .
			'<em>Phone Number: </em><strong>' . $data['phone'] . '</strong><br>' .
			'<em>Message: </em><strong>' . $data['message'] . '</strong><br>' .
			'<em>City: </em><strong>' . $data['city_name'] . '</strong><br>' .
			'<br><br>';

		$mailData = [
			'to'   => $to,
			'from' => $from,
			'sub'  => $feedSub,
			'body' => $body
		];

		$this->sendMail($mailData);
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}