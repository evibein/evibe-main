<?php

namespace App\Jobs\Emails\Feedback;

use App\Jobs\Emails\UtilEmail;
use Exception;

class MailWebsiteFeedbackToCustomerJob extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$to = [$data['email']];
		$from = 'Team Evibe <feedback@evibe.in>';
		$feedSub = '[Evibe.in] Thank you for the feedback';
		$body = 'Hello ' . $data['name'] . ', <br><br>' .
			'We thank you very much for taking time and sending us feedback.' .
			' We are always on our toes to serve our customers in the best possible way.' .
			' And we totally believe this is not possible without your support and feedback.' .
			' We wholeheartedly appreciate your efforts. Thanks a ton :-)<br><br>' .
			'We will keep you posted about the latest updates and improvements from our side.' .
			'<br><br>' .
			'Best Regards,<br>' .
			'Team Evibe<br>' .
			'<a href="http://youtu.be/8HdTF43Djko" target="_blank">Every birthday is special</a><br>' .
			'<br><br>';

		$mailData = [
			'to'   => $to,
			'from' => $from,
			'sub'  => $feedSub,
			'body' => $body
		];

		$this->sendMail($mailData);
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}