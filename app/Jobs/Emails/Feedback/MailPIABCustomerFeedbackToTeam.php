<?php

namespace App\Jobs\Emails\Feedback;

use App\Jobs\Emails\UtilEmail;
use Exception;
use Illuminate\Support\Facades\Mail;

class MailPIABCustomerFeedbackToTeam extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		Mail::send($data['mailView'], ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.contact.company.email'), 'Party In A Box')
			  ->to(config('evibe.contact.support.group'))
			  ->subject($data['mailSub']);
		});
	}

	public function failed(Exception $exception)
	{
		$data['exception'] = $exception;
		$this->sendFailedJobsToTeam($data);
	}
}