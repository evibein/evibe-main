<?php

namespace App\Jobs\Emails\Feedback;

use App\Jobs\Emails\UtilEmail;
use Exception;

class MailWebsiteFeedbackToTeam extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$to = ['feedback@evibe.in'];
		$from = 'Evibe Alerts<ping@evibe.in>';
		$feedSub = 'New feedback alert';
		$body = 'Hello Team, <br><br>' .
			'Wohoo, we have a new feedback from our user. Check below: ' .
			'<br><br>' .
			'<em>Name: </em><strong>' . $data['name'] . '</strong><br>' .
			'<em>Phone: </em><strong>' . $data['phone'] . '</strong><br>' .
			'<em>Email: </em><strong>' . $data['email'] . '</strong><br>' .
			'<em>Feedback: </em><strong>' . $data['comments'] . '</strong><br>' .
			'<br><br>' .
			'Best Regards,<br>' .
			'Admin<br>' .
			'<br>';

		$mailData = [
			'to'   => $to,
			'from' => $from,
			'sub'  => $feedSub,
			'body' => $body
		];

		$this->sendMail($mailData);
	}

	public function failed(Exception $exception)
	{
		$this->sendFailedJobsToTeam($exception);
	}
}