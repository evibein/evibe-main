<?php

namespace App\Jobs;

use App\Models\Tracking\AffiliateTracking;

class HandlePartyOneRedirectRequest extends BaseJob
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		AffiliateTracking::create([
			                          "client_ip_address" => $data['clientIpAddress'],
			                          "affiliate_url"     => $data['affiliateUrl'],
			                          "affiliate_name"    => "Party One",
		                          ]);
	}
}