<?php

namespace App\Jobs;

use App\Models\Log\LogCouponUsage;

class HandleLogCouponUsage extends BaseJob
{
	private $data;

	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 3;

	/**
	 * The number of seconds the job can run before timing out.
	 *
	 * @var int
	 */
	public $timeout = 120;

	public function __construct($data)
	{
		$this->data = $data;
		$this->onConnection("sqs_loggers")
		     ->onQueue(config('queue.connections.sqs_loggers.queue'));
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		LogCouponUsage::create([
			                       "coupon_code"     => $data['couponCode'],
			                       "coupon_id"       => $data['couponId'],
			                       "booking_amount"  => $data['bookingAmount'],
			                       "name"            => $data['name'],
			                       "phone"           => $data['phone'],
			                       "email"           => $data['email'],
			                       "discount_amount" => $data['discountAmount'],
			                       "ticket_id"       => $data['ticketId'],
			                       "user_id"         => $data['userId'],
			                       "city_id"         => $data['cityId'],
			                       "occasion_id"     => $data['occasionId']
		                       ]);
	}
}