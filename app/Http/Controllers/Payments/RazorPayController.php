<?php

namespace App\Http\Controllers\Payments;

use App\Jobs\Emails\Payment\PaymentTransactionInfoUpdate;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketBooking;
use App\Models\Wallet\Wallet;
use App\Models\Wallet\WalletRedeemed;
use Evibe\Implementers\PaymentGateway;
use Illuminate\Support\Facades\Auth;
use Razorpay\Api\Api;

use Illuminate\Support\Facades\Log;
use App\Http\Requests;

class RazorPayController extends BasePaymentController implements PaymentGateway
{
	/* @implements */
	public function onPaymentInit($ticketId)
	{
		$validation = $this->validateUserInputAndNotifyTeam(request()->input(), $ticketId, "Razorpay", config("evibe.type-payment-gateway.razorpay"));

		if (!$validation['success'])
		{
			$validationError = array_key_exists("error", $validation) ? $validation['error'] : "Please input all the required fields and try again.";

			return response()->json(['success' => false, 'error' => $validationError]);
		}

		// in paisa for razorPay
		$advanceAmount = $validation['advanceAmount'] * 100;

		return response()->json(['success' => true, 'amount' => $advanceAmount]);
	}

	/* @implements */
	public function onPaymentReceived()
	{
		$verification = $this->captureReceivedPayment(request()->input());
		$ticketId = request()->input('ticketId');
		$response = ['success' => false];

		if ($verification['success'])
		{
			$this->dispatch(new PaymentTransactionInfoUpdate([
				                                                 "ticketId"     => $ticketId,
				                                                 "paymentRefId" => request()->input('razorpay_payment_id'),
				                                                 "isSuccess"    => true
			                                                 ]));

			$response['success'] = true;
			$response['redirectTo'] = route('pay.common.intermediate', $ticketId);
		}

		return response()->json($response);
	}

	private function captureReceivedPayment($receivedData)
	{
		$key = config('razorpay.key');
		$secret = config('razorpay.secret');
		$ticketId = $receivedData['ticketId'];
		$paymentId = $receivedData['razorpay_payment_id'];
		$response = ['success' => false];

		$ticket = Ticket::with('bookings.mapping', 'bookings.provider')->find($ticketId);

		if (!$ticket)
		{
			Log::error("RazorPay ShowSuccessPage::Ticket not found::ticketId: " . $ticketId);
		}
		else
		{
			$ticketBookings = TicketBooking::where('ticket_id', $ticketId)
			                               ->where('is_advance_paid', config('evibe.ticket.advance.unpaid'))
			                               ->whereNull('deleted_at');

			if ($receivedData['isVenueDeal'] == 1)
			{
				$totalAdvance = $ticketBookings->sum('token_amount');
			}
			else
			{
				$totalAdvance = $ticketBookings->sum('advance_amount');
			}

			$walletDiscount = 0;
			$loginUser = Auth::user();
			if ($loginUser)
			{
				$wallet = Wallet::where("user_id", $loginUser->id)
				                ->first();

				if ($wallet)
				{
					$walletRedeem = WalletRedeemed::where("ticket_id", $ticket->id)
					                              ->first();

					if ($walletRedeem)
					{
						$walletDiscount = $walletRedeem->amount;
					}
				}
			}

			$totalAdvance = $totalAdvance - $ticketBookings->sum('prepay_discount_amount') + $ticket->internet_handling_fee - $walletDiscount;

			// @todo: check 2 - advance paid and correct advance to be paid

			// capture razorpay payment
			$api = new Api($key, $secret);
			$payment = $api->payment->fetch($paymentId);
			$payment->capture(['amount' => $totalAdvance * 100]);

			$response['success'] = true;
			$this->updateTicketBookingsWithPaymentReference($paymentId, $ticket, config('evibe.type-payment-gateway.razorpay'));
		}

		return $response;
	}
}