<?php

namespace App\Http\Controllers\Payments;

use App\Evibe\Implementers\HandlesPayments;
use App\Jobs\Emails\Payment\Customer\MailBookingUpdatedReceiptToCustomer;
use App\Jobs\Emails\Payment\Customer\MailRequestNewPaymentToCustomer;
use App\Jobs\Emails\Payment\PaymentInitiationUpdate;
use App\Jobs\Emails\Payment\PaymentTransactionInfoUpdate;
use App\Jobs\Emails\Payment\Team\MailRequestNewPaymentToTeam;
use App\Jobs\Emails\Payment\Vendor\MailBookingUpdatedReceiptToPartner;
use App\Jobs\Emails\Util\MailAddOnsInclusionToTeam;
use App\Jobs\SMS\SMSBookingUpdatedReceiptToCustomer;
use App\Jobs\SMS\SMSBookingUpdatedReceiptToPartner;
use App\Jobs\UpdateScheduleSettlementBooking;
use App\Models\Ticket\Ticket;
use App\Jobs\HandlePostPaymentProcessJob;
use App\Jobs\SMS\SMSBookingToCustomerJob;
use App\Jobs\SMS\SMSBookingToPartnerJob;
use App\Jobs\Emails\Payment\Customer\MailBookingReceiptToCustomerJob;
use App\Jobs\Emails\Payment\Vendor\MailBookingReceiptToPartnerJob;

use App\Models\Ticket\TicketFollowup;
use App\Models\Types\TypeProof;
use App\Models\Util\Country;
use App\Models\Util\CustomerOrderProof;
use Carbon\Carbon;
use Evibe\Passers\PaySuccessUpdateTicket;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;

class TicketBookingPaymentController extends BasePaymentController implements HandlesPayments
{
	/* @implements */
	public function showCheckoutPage()
	{
		$ticketId = request('id');
		$token = request('token');
		$hashedStr = request('uKm8');

		// check with ticket id and token
		if ($ticketId && $token && Hash::check($ticketId, $token) && $hashedStr)
		{
			$ticket = Ticket::with('bookings.mapping', 'bookings.provider')
			                ->find($ticketId);

			if ($ticket)
			{
				// second level check with email
				$concatenatedStr = $ticket->email;
				if (!$this->validateToken($concatenatedStr, $hashedStr, $ticketId))
				{
					return "<h1> Invalid Request </h1>";
				}

				// Here to check the payment deadline
				// added extra 60 sec to bypass the bug
				// todo: debug the invalid status issue
				if (($ticket->status_id == config('evibe.ticket.status.cancelled')) && ($ticket->payment_deadline <= Carbon::now()->timestamp + 60))
				{
					$bookingAmount = 0;
					$advanceToPay = 0;
					$eventDate = null;
					$bookings = [];
					$ticketBookings = $ticket->bookings->toArray();

					foreach ($ticketBookings as $ticketBooking)
					{
						$bookingAmount = $bookingAmount + $ticketBooking['booking_amount'];
						$advanceToPay = $advanceToPay + $ticketBooking['advance_amount'];
						$eventDate = $ticketBooking['party_date_time'];
					}

					$data = [
						'ticket'        => $ticket,
						'ticketId'      => $ticketId,
						'bookings'      => $bookings,
						'bookingAmount' => $bookingAmount,
						'advanceToPay'  => $advanceToPay,
						'eventDate'     => $eventDate
					];

					return view('pay/checkout/link-expiry-page', ['data' => $data]);
				}
				elseif (($ticket->status_id == config('evibe.ticket.status.followup')) && $ticket->payment_deadline && ($ticket->payment_deadline <= Carbon::now()->timestamp))
				{
					$ticketBookings = $ticket->bookings->toArray();

					if (count($ticketBookings))
					{
						$data = [
							'ticket' => $ticket,
						];

						return view('pay/checkout/link-expiry-page', ['data' => $data]);
					}
					else
					{
						$this->sendNonExceptionErrorReportToTeam([
							                                         'code'      => config('evibe.error_code.hack'),
							                                         'url'       => request()->fullUrl(),
							                                         'method'    => request()->method(),
							                                         'message'   => '[TicketBookingPaymentController.php - ' . __LINE__ . '] ' . 'Without any bookings, customer is trying to access checkout page',
							                                         'exception' => '[TicketBookingPaymentController.php - ' . __LINE__ . '] ' . 'Without any bookings, customer is trying to access checkout page',
							                                         'trace'     => '[Ticket Id: ' . $ticketId . '] [Status Id: ' . $ticket->status_id . '] [Payment Deadline: ' . ($ticket->payment_deadline ? date('d/M/Y H:i A', $ticket->payment_deadline) : null) . ']',
							                                         'details'   => '[Ticket Id: ' . $ticketId . '] [Status Id: ' . $ticket->status_id . '] [Payment Deadline: ' . ($ticket->payment_deadline ? date('d/M/Y H:i A', $ticket->payment_deadline) : null) . ']'
						                                         ]);

						return "<h1>Invalid Request</h1>";
					}
				}
				elseif ($ticket->status_id != config('evibe.ticket.status.booked') && $ticket->status_id != config('evibe.ticket.status.confirmed'))
				{
					$this->sendNonExceptionErrorReportToTeam([
						                                         'code'      => config('evibe.error_code.hack'),
						                                         'url'       => request()->fullUrl(),
						                                         'method'    => request()->method(),
						                                         'message'   => '[TicketBookingPaymentController.php - ' . __LINE__ . '] ' . 'VALID PAYMENT LINK BUT NOT VALID STATUS :: ' . $ticketId . '::token::' . $token . '::hashedStr::' . $hashedStr,
						                                         'exception' => '[TicketBookingPaymentController.php - ' . __LINE__ . '] ' . 'VALID PAYMENT LINK BUT NOT VALID STATUS :: ' . $ticketId . '::token::' . $token . '::hashedStr::' . $hashedStr,
						                                         'trace'     => '[Ticket Id: ' . $ticketId . '] [Status Id: ' . $ticket->status_id . '] [Payment Deadline: ' . ($ticket->payment_deadline ? date('d/M/Y H:i A', $ticket->payment_deadline) : null) . ']',
						                                         'details'   => '[Ticket Id: ' . $ticketId . '] [Status Id: ' . $ticket->status_id . '] [Payment Deadline: ' . ($ticket->payment_deadline ? date('d/M/Y H:i A', $ticket->payment_deadline) : null) . ']'
					                                         ]);

					return "<h1>Invalid Request</h1>";
				}

				$ticketBookings = $ticket->bookings; // get list of all bookings

				$eventId = $ticket->event_id;
				$cityId = $ticket->city_id;
				if (count($ticketBookings) > 0)
				{
					$data = $this->getTicketData($ticket);
					$totalBookingAmount = 0;
					$totalAdvanceToPay = 0;
					$hasVenueBooking = false;
					$isShowVenueType = false;
					$hasAddOn = false;
					$hasDecor = false;
					$imageUpload = false;
					$needCustomerProof = false;

					foreach ($ticketBookings as $booking)
					{
						$mapTypeId = $booking->mapping->map_type_id;
						$mapId = $booking->mapping->map_id;
						$mappedValues = $this->fillMappingValues([
							                                         'isCollection' => false,
							                                         'mapId'        => $mapId,
							                                         'mapTypeId'    => $mapTypeId
						                                         ]);

						$bookingData = $this->getTicketBookingData($booking, $mappedValues);
						$data['bookings']['list'][] = $bookingData;

						$totalBookingAmount += $booking->booking_amount;
						$totalAdvanceToPay += ($booking->is_advance_paid == 1) ? 0 : $booking->advance_amount;

						// @see: assuming there will not be more than one venue booking per ticket
						if ($booking->is_venue_booking)
						{
							$hasVenueBooking = true;
						}

						if (isset($bookingData['itemMapTypeId']) && ($bookingData['itemMapTypeId'] == config('evibe.ticket.type.decor')))
						{
							$isShowVenueType = true;
							$hasDecor = true;
						}

						if (isset($bookingData['itemMapTypeId']) && ($bookingData['itemMapTypeId'] == config('evibe.ticket.type.add-on')))
						{
							$hasAddOn = true;
						}

						if (isset($bookingData['imageUpload']) && ($bookingData['imageUpload']))
						{
							$imageUpload = true;
						}

						if (isset($bookingData['itemMapTypeId']) && ($bookingData['itemMapTypeId'] == config('evibe.ticket.type.villas')))
						{
							$needCustomerProof = true;
						}
					}

					if (in_array($eventId, [
						config('evibe.occasion.surprises.id'),
						config('evibe.occasion.kids_birthdays.id'),
						config('evibe.occasion.first_birthday.id'),
						config('evibe.occasion.birthday_2-5.id'),
						config('evibe.occasion.birthday_6-12.id'),
						config('evibe.occasion.naming_ceremony.id'),
						config('evibe.occasion.baby-shower.id')
					]))
					{
						$arrayAddOns = [];
						foreach ($ticketBookings as $ticketBooking)
						{
							$mapping = $ticketBooking->mapping()->first();
							$productAddOns = $this->getTicketApplicableAddOns($cityId, $eventId, $mapping->map_type_id, $mapping->map_id, $ticketBooking);

							// disabling add-ons for vday clds
							//// todo: remove
							if ($ticketBooking->mapping && ($ticketBooking->mapping->map_type_id == config('evibe.ticket.type.surprises')) && ($ticketBooking->map_type_id == config('evibe.ticket.type.venue')) && (date('Y/m/d', $ticketBooking->party_date_time) == "2020/02/14"))
							{
								$productAddOns = [];
							}

							$arrayAddOns = array_merge($arrayAddOns, $productAddOns);
						}
						$arrayAddOns = array_unique($arrayAddOns);

						// todo edit after test
						if (count($arrayAddOns))
						{
							$data['addOns'] = $arrayAddOns;
							$data['selectedAddOnIds'] = [];

							// already selected add-ons
							foreach ($ticketBookings as $ticketBooking)
							{
								$mapping = $ticketBooking->mapping()->first();
								if ($mapping->map_type_id == config('evibe.ticket.type.add-on'))
								{
									$data['selectedAddOnIds'][$mapping->map_id] = [
										'bookingUnits' => $ticketBooking->booking_units ?: 1
									];
								}
							}
						}
					}

					$data['couponToken'] = $this->getAccessTokenForCoupon(intval($ticketId), $ticket->created_at);

					$data['bookings']['minBookingDateTime'] = $ticketBookings->min('party_date_time');
					$data['bookings']['maxBookingDateTime'] = $ticketBookings->max('party_date_time');

					$data['bookings']['totalBookingAmount'] = $totalBookingAmount;
					$data['bookings']['totalAdvanceToPay'] = $totalAdvanceToPay;
					$data['hasVenueBooking'] = $hasVenueBooking;
					$data['isShowVenueType'] = $isShowVenueType;
					$data['hasAddOn'] = $hasAddOn;
					$data['hasDecor'] = $hasDecor;
					$data['uploadImage'] = $imageUpload;
					$data['isAutoBooking'] = false;
					$data['needCustomerProof'] = $needCustomerProof;

					if ($needCustomerProof)
					{
						$data['proofs'] = TypeProof::all();
						$customerOrderProofs = CustomerOrderProof::where('ticket_id', $ticketId)
						                                         ->whereNull('deleted_at')
						                                         ->get();

						if ($customerOrderProofs && (count($customerOrderProofs) >= 2))
						{
							$data['customerOrderProofs'] = 1;
						}
					}

					// Disabling date time changing option in checkout
					$data['additional']['PartyStartTimeDisabled'] = true;

					// conditions to show extra benefits selection
					$eventId = $ticket->event_id;
					$timeDifference = $ticketBookings->min('party_date_time') - time();
					$kidsEvents = [
						config('evibe.occasion.kids_birthdays.id'),
						config('evibe.occasion.first_birthday.id'),
						config('evibe.occasion.birthday_2-5.id'),
						config('evibe.occasion.birthday_6-12.id'),
						config('evibe.occasion.naming_ceremony.id'),
						config('evibe.occasion.baby-shower.id')
					];

					$data['extraBenefits'] = false;
					$data['invite'] = false;
					if (in_array($eventId, $kidsEvents))
					{
						$data['extraBenefits'] = true;
						$data['invite'] = false; // @see: removed on 16 Mar 2020

						//if (($timeDifference > 0) && (($timeDifference / (24 * 60 * 60)) >= 2))
						//{
						//	$data['invite'] = true;
						//}
					}

					if (in_array($data['additional']['eventId'], [
						config('evibe.occasion.surprises.id'),
						config('evibe.occasion.kids_birthdays.id'),
						config('evibe.occasion.first_birthday.id'),
						config('evibe.occasion.birthday_2-5.id'),
						config('evibe.occasion.birthday_6-12.id'),
						config('evibe.occasion.naming_ceremony.id'),
						config('evibe.occasion.baby-shower.id')
					]))
					{
						$arrayAddOns = [];
						foreach ($ticketBookings as $ticketBooking)
						{
							$mapping = $ticketBooking->mapping()->first();
							$productAddOns = $this->getTicketApplicableAddOns($data['additional']['cityId'], $eventId, $mapping->map_type_id, $mapping->map_id, $ticketBooking);

							// @see: add-ons related code is repeating twice. Refer to the line "205"
							// disabling add-ons for vday clds
							//// todo: remove
							if ($ticketBooking->mapping && ($ticketBooking->mapping->map_type_id == config('evibe.ticket.type.surprises')) && ($ticketBooking->map_type_id == config('evibe.ticket.type.venue')) && (date('Y/m/d', $ticketBooking->party_date_time) == "2020/02/14"))
							{
								$productAddOns = [];
							}

							$arrayAddOns = array_merge($arrayAddOns, $productAddOns);
						}
						$arrayAddOns = array_unique($arrayAddOns);
						$data['addOns'] = $arrayAddOns;

						$addOns = collect($arrayAddOns);
						if (count($addOns))
						{
							$data['addOnTypeTicketBookingIds'] = $addOns->pluck('type_ticket_booking_id')->toArray();
							$data['addOnTypeTicketBookingIds'] = array_unique(array_filter($data['addOnTypeTicketBookingIds']));
						}
					}

					$tmoToken = Hash::make($ticketId . config('evibe.token.track-my-order'));
					$data['tmoLink'] = route("track.orders") . "?ref=checkout-page&id=" . $ticketId . "&token=" . $tmoToken;

					// countries for calling code
					$data['countries'] = Country::all();

					// Storing data for analytics & getting more insights
					$payInitTrackingData = [
						"ticketId"    => $ticketId,
						"browserInfo" => isset($_SERVER) && isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "",
						"previousUrl" => isset($_SERVER) && isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null,
						"ipAddress"   => isset($_SERVER) && isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : ""
					];

					$this->dispatch(new PaymentInitiationUpdate($payInitTrackingData));

					if (isset($data['additional']['eventId']) && $data['additional']['eventId'])
					{
						switch ($data['additional']['eventId'])
						{
							case config('evibe.occasion.kids_birthdays.id'):
							case config('evibe.occasion.first_birthday.id'):
							case config('evibe.occasion.birthday_2-5.id'):
							case config('evibe.occasion.birthday_6-12.id'):
							case config('evibe.occasion.naming_ceremony.id'):
							case config('evibe.occasion.baby-shower.id'):
								$data['storePromotion'] = false;
								$data['storePromotionTitle'] = "Latest Party Props";
								$data['storePromotionTagLine'] = "Get foil balloon letters, numbers, LED balloons, photo props, photo banners and more.";
								$data['storePromotionImage'] = config('evibe.gallery.host') . "/main/img/store/Kids-1.png";

								$storeUrl = config('evibe.store_host') . "/collections/kids-birthday-party-supplies";
								$storeUrl .= "?utm_source=Checkout&utm_medium=Website&utm_campaign=StorePromotion&utm_content=KidsEvent";

								$data['storePromotionUrl'] = $storeUrl;
								break;

							case config('evibe.occasion.surprises.id'):
								$data['storePromotion'] = false;
								$data['storePromotionTitle'] = "Latest Party Props";
								$data['storePromotionTagLine'] = "Get foil balloon numbers, letters, fairy lights, photo props, sashes and more.";
								$data['storePromotionImage'] = config('evibe.gallery.host') . "/main/img/store/Youth-1.jpg";

								$storeUrl = config('evibe.store_host') . "/collections/youth-birthday-party-supplies";
								$storeUrl .= "?utm_source=Checkout&utm_medium=Website&utm_campaign=StorePromotion&utm_content=Surprises";

								$data['storePromotionUrl'] = $storeUrl;
								break;
						}
					}

					return view('pay/checkout/checkout', ['data' => $data]);
				}
				else
				{
					Log::error("No bookings available::ticketId::" . $ticketId . "::token::" . $token . "::hashedStr::" . $hashedStr);

					return "<h1>Invalid Request</h1>"; // there are no bookings, abort
				}
			}
			else
			{
				Log::error("Ticket, Not Available::ticketId::" . $ticketId . "::token::" . $token . "::hashedStr::" . $hashedStr);

				return "<h1>Invalid Request</h1>"; // ticket not valid
			}
		}
		else
		{
			Log::error("Ticket, token mismatch::ticketId::" . $ticketId . "::token::" . $token . "::hashedStr::" . $hashedStr);

			return "<h1>Invalid Request</h1>"; // ticketId, token mismatch
		}
	}

	/* @implements */
	public function processPayment($ticketId)
	{
		// check if request is from EvibeDash / directly from customer
		$isFromDash = request()->has('isFromDash') ? request('isFromDash') : false;
		$handlerId = request()->has('handlerId') ? (int)request('handlerId') : null;

		$response = [
			'success'    => false,
			'error'      => 'Failed to send booked notifications',
			'redirectTo' => route('pay.common.fail')
		];

		if ($this->sendBookedNotifications($ticketId, $isFromDash, $handlerId))
		{
			$response['success'] = true;
			$response['redirectTo'] = route('pay.normal.success', $ticketId) . "?token=" . Hash::make($ticketId);
			unset($response['error']);

			// handle post payment
			$this->dispatch(new HandlePostPaymentProcessJob($ticketId));

			// Update transaction success in DB
			$this->dispatch(new PaymentTransactionInfoUpdate([
				                                                 "ticketId"  => $ticketId,
				                                                 "isSuccess" => false
			                                                 ]));
		}

		return response()->json($response);
	}

	/* @implements */
	public function sendBookedNotifications($ticketId, $isFromDash = false, $handlerId = null)
	{
		$ticket = Ticket::with('bookings.mapping', 'bookings.provider')->find($ticketId);
		$statusCheck = $this->checkStatusBeforeSendReceipts($ticket, $isFromDash); // returns PreSendReceiptStatus object

		if (!$statusCheck->getSuccess())
		{
			Log::error($statusCheck->getErrorMessage());

			return false;
		}

		// all bookings advance paid
		$ticketBookings = $statusCheck->getBookings();

		$allBookings = [];
		$totalBookingAmount = $totalAdvancePaid = $totalBalanceAmount = 0;
		$data = $this->getTicketData($ticket);
		$data["couponDiscount"] = $ticketBookings->sum('prepay_discount_amount');

		// check if ticket has venue booking, we need to this to
		// send booking emails: include venue info / not
		$data['ticketVenueName'] = "";
		$data['ticketHasVenueBooking'] = false;
		$venueBookingCount = $ticketBookings->where('is_venue_booking', 1)->count();
		if ($venueBookingCount > 0)
		{
			$data['ticketHasVenueBooking'] = true;
			$venueBooking = $ticketBookings->where('is_venue_booking', 1)->first();
			$data['ticketVenueName'] = $venueBooking->provider->name;
		}

		// fetch any venue partner details for the sake of add-ons delivery
		$venuePartnerName = null;
		$venuePartnerPhone = null;
		$venuePartnerAltPhone = null;
		foreach ($ticketBookings as $ticketBooking)
		{
			if (!$venuePartnerName && ($ticketBooking->map_type_id == config('evibe.ticket.type.venue')))
			{
				$venuePartnerName = $ticketBooking->provider->person ? $ticketBooking->provider->person : null;
				$venuePartnerPhone = $ticketBooking->provider->phone ? $ticketBooking->provider->phone : null;
				$venuePartnerAltPhone = $ticketBooking->provider->alt_phone ? $ticketBooking->provider->alt_phone : null;
			}
		}

		$addOnsIncluded = false;
		$addOns = [];
		foreach ($ticketBookings as $booking)
		{
			// send email for each booking
			$mapTypeId = $booking->mapping->map_type_id;
			$mapId = $booking->mapping->map_id;
			$mappedValues = $this->fillMappingValues([
				                                         'isCollection' => false,
				                                         'mapId'        => $mapId,
				                                         'mapTypeId'    => $mapTypeId
			                                         ]);

			// calculating the total advance
			$data['booking'] = $this->getTicketBookingData($booking, $mappedValues);
			$totalBookingAmount += $data['booking']['bookingAmount'];
			$totalAdvancePaid += $data['booking']['advanceAmount'];
			$totalBalanceAmount += $data['booking']['bookingAmount'] - $data['booking']['advanceAmount'];

			if ($venuePartnerName &&
				isset($data['booking']['venueDelivery']) && $data['booking']['venueDelivery'] &&
				isset($data['booking']['provider']['providerTypeId']) && ($data['booking']['provider']['providerTypeId'] != config('evibe.ticket.type.venue')) &&
				isset($data['booking']['itemMapTypeId']) && ($data['booking']['itemMapTypeId'] == config('evibe.ticket.type.add-on')))
			{
				$data['booking']['venuePartnerName'] = $venuePartnerName;
				$data['booking']['venuePartnerPhone'] = $venuePartnerPhone;
				$data['booking']['venuePartnerAltPhone'] = $venuePartnerAltPhone;
			}

			// customer email cc: alt customer email + team (handler + group)
			// vendor email cc: business group + all alt_email{1,2,3}
			$ccAddresses = [config('evibe.contact.customer.group')];
			if ($ticket->handler)
			{
				array_push($ccAddresses, $ticket->handler->username);
			}
			if ($ticket->alt_email)
			{
				array_push($ccAddresses, $ticket->alt_email);
			}
			$data['ccAddresses'] = $ccAddresses;
			$data['vendorCcAddresses'] = $this->getVendorCcAddresses($data['booking']['provider']);
			$data['bookingConceptName'] = $booking->bookingConcept ? $booking->bookingConcept->name : "";

			// send sms and email to vendor for each booking
			$this->initBookedNotificationToPartner($data);

			// update booking with partner booking notification status
			$booking->partner_booking_notification_at = Carbon::now();
			$booking->save();

			// api call to update lookup table (schedule settlement booking)
			// @see: shifted to bottom after the update of ticket status to avoid cancellation link issue

			if ($mapTypeId == config('evibe.ticket.type.add-on'))
			{
				$addOnsIncluded = true;
			}

			// update into main booking array only after all the values are calculated and set
			$allBookings[] = $data['booking'];

			// customer proof
			if ($mapTypeId == config('evibe.ticket.type.villas'))
			{
				$data['needProof'] = true;
				$data['proofUpload'] = false;

				$customerUploadProof = CustomerOrderProof::where('ticket_id', $ticketId)
				                                         ->whereNull('deleted_at')
				                                         ->whereNull('rejected_at')
				                                         ->get();

				if (count($customerUploadProof) >= 2)
				{
					$data['proofUpload'] = true;
				}
			}

			$bookingAddOns = $this->fetchAddOns($ticket->city_id, $ticket->event_id, $booking->mapping->map_type_id, $booking->mapping->map_id, $booking);
			if (is_array($bookingAddOns))
			{
				$addOns = array_merge($addOns, $bookingAddOns);
			}
		}

		$data['addOns'] = collect($addOns)->unique()->take(4);
		$tmoToken = Hash::make($ticketId . config('evibe.token.track-my-order'));
		$tmoLink = route("track.orders") . "?ref=email&id=" . $ticketId . "&token=" . $tmoToken;
		$data['tmoLink'] = $tmoLink;

		$data['bookings']['minBookingDateTime'] = $ticketBookings->min('party_date_time');
		$data['bookings']['maxBookingDateTime'] = $ticketBookings->max('party_date_time');
		$partyDateTime = getPartyDateTimeByBookingsPartyTime($data['bookings']['minBookingDateTime'], $data['bookings']['maxBookingDateTime']);
		$data['partyDateTime'] = $partyDateTime;

		// Managing coupon & internet handling fee
		// Coupon discount will effect only total advance paid
		$totalBookingAmount += $data['additional']['internetHandlingFee'];
		$totalAdvancePaid += $data['additional']['internetHandlingFee'] - $data["couponDiscount"];

		$data['totalBookingAmount'] = $totalBookingAmount;
		$data['totalAdvancePaid'] = $totalAdvancePaid;
		$data['totalBalanceAmount'] = $totalBalanceAmount;

		// send single email & sms to customer
		$this->initBookedNotificationToCustomer($data);

		if ($ticket->e_invite)
		{
			// send e-invite communication to host
			$this->initEInviteNotifications($data);
		}

		if ($addOnsIncluded)
		{
			$data['dashLink'] = config('evibe.dash_host') . '/tickets/' . $data['ticketId'];
			// send add-ons notification to team
			$this->dispatch(new MailAddOnsInclusionToTeam($data));
		}

		// update timestamp & save action
		$forUpdateData = new PaySuccessUpdateTicket();
		$forUpdateData->setHandlerId($handlerId);
		$forUpdateData->setIsFromDash($isFromDash);
		$forUpdateData->setStatusId(config('evibe.ticket.status.booked'));
		$forUpdateData->setSuccessEmailType(config('evibe.ticket.success_type.booked'));

		$response = $this->updateTicketPostSendReceipts($ticket, $forUpdateData);

		// api call to update lookup table (schedule settlement booking)
		foreach ($ticketBookings as $booking)
		{
			$this->dispatch(new UpdateScheduleSettlementBooking([
				                                                    'ticketBookingId' => $booking->id,
				                                                    'handler'         => $ticket->handler ? $ticket->handler : null
			                                                    ]));
		}

		return $response;
	}

	/* @implements */
	public function initBookedNotificationToCustomer($data)
	{
		$twoDaysBefore = $this->isPartyTwoDaysBefore($data);
		$partyTime = getPartyDateTimeByBookingsPartyTime($data['bookings']['minBookingDateTime'], $data['bookings']['maxBookingDateTime']);
		$data['partyDateTime'] = $partyTime;
		$token = Hash::make($data["ticketId"] . "EVBTMO");

		if (isset($data['needProof']) && $data['needProof'] && isset($data['proofUpload']) && (!$data['proofUpload']))
		{
			$customerTpl = config('evibe.sms_tpl.book.customer_proof');

			$replaces = [
				'#customer#' => $data['customer']['name'],
				'#orderId#'  => $data['ticket']['enquiry_id'],
				'#tmoLink#'  => $this->getShortenUrl(route("track.orders") . "?ref=SMS&id=" . $data["ticketId"] . "&token=" . $token)
			];
		}
		else
		{
			if ($twoDaysBefore && $data['totalBalanceAmount'] > 0)
			{
				$customerTpl = config('evibe.sms_tpl.book_type2.customer');

				$replaces = [
					'#field1#' => $data['customer']['name'],
					'#field2#' => $this->formatPriceWithoutChar($data['totalAdvancePaid']),
					'#field3#' => $data['ticket']['enquiry_id'],
					'#field4#' => $this->formatPriceWithoutChar($data['totalBalanceAmount']),
					'#field5#' => $this->getShortenUrl(route("track.orders") . "?ref=SMS&id=" . $data["ticketId"] . "&token=" . $token)
				];
			}
			else
			{
				$customerTpl = config('evibe.sms_tpl.book.customer');
				$replaces = [
					'#field1#' => $data['customer']['name'],
					'#field2#' => $this->formatPriceWithoutChar($data['totalAdvancePaid']),
					'#field3#' => $data['ticket']['enquiry_id'],
					'#field4#' => $this->getShortenUrl(route("track.orders") . "?ref=SMS&id=" . $data["ticketId"] . "&token=" . $token)
				];
			}
		}

		$smsText = str_replace(array_keys($replaces), array_values($replaces), $customerTpl);
		$smsData = [
			'to'   => $data['customer']['phone'],
			'text' => $smsText
		];

		// Sending null data to reduce the size of queue
		$queueData = $data;
		$relations = $queueData["ticket"]->getRelations();
		unset($relations['bookings']);
		$queueData["ticket"]->setRelations($relations);
		$queueData["booking"]["checkoutFields"] = [];

		//send sms and email to customer
		$this->dispatch(new MailBookingReceiptToCustomerJob($queueData));
		$this->dispatch(new SMSBookingToCustomerJob($smsData));
	}

	/* @implements@ */
	public function initBookedNotificationToPartner($data)
	{
		$twoDaysBefore = $this->isPartyTwoDaysBefore($data);
		$bookingAmount = $data['booking']['bookingAmount'];
		$advanceAmount = $data['booking']['advanceAmount'];
		$balanceAmount = $bookingAmount - $advanceAmount;

		if ($twoDaysBefore)
		{
			$vendorTpl = config('evibe.sms_tpl.book_type2.vendor');
			$vendorSmsReplaces = [
				'#field1#' => $data['booking']['provider']['person'],
				'#field2#' => $data['booking']['partyDate'],
				'#field3#' => $data['additional']['venueLocation'],
				'#field4#' => $data['customer']['name'],
				'#field5#' => $this->formatPriceWithoutChar($advanceAmount),
				'#field6#' => $data['customer']['phone'],
				'#field7#' => $this->formatPriceWithoutChar($balanceAmount)
			];

			$vendorSmsText = str_replace(array_keys($vendorSmsReplaces), array_values($vendorSmsReplaces), $vendorTpl);
		}
		else
		{
			// send SMS to vendor
			$vendorTpl = config('evibe.sms_tpl.book.vendor');
			$vendorSmsReplaces = [
				'#field1#' => $data['booking']['provider']['person'],
				'#field2#' => $data['booking']['partyDate'],
				'#field3#' => $this->formatPriceWithoutChar($advanceAmount),
				'#field4#' => $data['customer']['name'],
				'#field5#' => $data['customer']['phone']
			];

			$vendorSmsText = str_replace(array_keys($vendorSmsReplaces), array_values($vendorSmsReplaces), $vendorTpl);
		}

		// partner dash quick link to order details
		$params = $this->getPartnerQuickLoginParams($data['booking']['provider']['userId']);
		$iId = (isset($params['iId']) && $params['iId']) ? $params['iId'] : null;
		$eToken = (isset($params['eToken']) && $params['eToken']) ? $params['eToken'] : null;
		$ticketBookingId = $data['booking']['id'];
		$partnerDashLink = config('evibe.host') . '/partner/orders/' . $ticketBookingId;
		$partnerDashLink .= '?iId=' . $iId . '&eToken=' . $eToken;

		// utm tagging
		$partnerDashEmailLink = $partnerDashLink . '&utm_source=partner-orders-quick-link&utm_campaign=partner-orders&utm_medium=email';
		$partnerDashEmailShortLink = $this->getShortenUrl($partnerDashEmailLink);

		$partnerDashSMSLink = $partnerDashLink . '&utm_source=partner-orders-quick-link&utm_campaign=partner-orders&utm_medium=sms';
		$partnerDashSMSShortLink = $this->getShortenUrl($partnerDashSMSLink);

		$data['partnerDashLink'] = $partnerDashLink;
		$data['partnerDashEmailLink'] = $partnerDashEmailShortLink;
		$data['partnerDashSMSLink'] = $partnerDashSMSShortLink;

		// send SMS to vendor
		$smsData = [
			'to'   => $data['booking']['provider']['phone'],
			'text' => $vendorSmsText
		];

		// Sending null data to reduce the size of queue
		$queueData = $data;
		$relations = $queueData["ticket"]->getRelations();
		unset($relations['bookings']);
		$queueData["ticket"]->setRelations($relations);
		$queueData["booking"]["checkoutFields"] = [];

		//send email and sms to vendor
		$this->dispatch(new MailBookingReceiptToPartnerJob($queueData));
		$this->dispatch(new SMSBookingToPartnerJob($smsData));
	}

	/* @implements */
	public function onPaymentSuccess($ticketId = null)
	{
		if (!is_null($ticketId) && !Hash::check($ticketId, request("token")))
		{
			return redirect(route("pay.normal.success"));
		}

		$data = [];

		if ($ticketId)
		{
			$ticket = Ticket::find($ticketId);
			$data = $this->fetchCustomerOrderDetails($ticket);
			if ($ticket->e_invite)
			{
				// @see: not using this anymore
				//$data['eInviteFormUrl'] = config('evibe.e-invite.g-form-url');
			}
			$data["ticketId"] = $ticketId;

			$tmoToken = Hash::make($ticketId . config('evibe.token.track-my-order'));
			$tmoLink = route("track.orders") . "?ref=SMS&id=" . $ticketId . "&token=" . $tmoToken;
			$data['tmoLink'] = $tmoLink;
			$data["ticketBookings"] = $ticket->bookings;

			// regardless of whether e-invite has been selected or not
			if (in_array($ticket->event_id, [
				config('evibe.occasion.kids_birthdays.id'),
				config('evibe.occasion.naming_ceremony.id'),
				config('evibe.occasion.baby-shower.id'),
				config('evibe.occasion.first_birthday.id'),
				config('evibe.occasion.birthday_2-5.id'),
				config('evibe.occasion.birthday_6-12.id')
			]))
			{
				$data['invitePromotionLink'] = route('e-cards.invites');
			}
		}

		Log::info("payment success page: " . $ticketId);

		return view('pay.success.normal-book', ["data" => $data]);
	}

	public function sendUpdatedReceipt($ticketId)
	{
		$stringifyBookingIds = request('bookingIds');
		$bookingIds = explode(",", $stringifyBookingIds);
		$isFromDash = true;
		$handlerId = request('handlerId');
		$partnerChange = request('partnerChange');

		// send email for each booking id
		$ticket = Ticket::find($ticketId);
		$statusCheck = $this->checkStatusBeforeSendReceipts($ticket, $isFromDash); // returns PreSendReceiptStatus object

		if (!$statusCheck->getSuccess())
		{
			Log::error($statusCheck->getErrorMessage());

			return false;
		}

		// all bookings advance paid
		$ticketBookings = $statusCheck->getBookings();

		$allBookings = [];
		$totalBookingAmount = $totalAdvancePaid = $totalBalanceAmount = 0;
		$data = $this->getTicketData($ticket);
		$data["couponDiscount"] = $ticketBookings->sum('prepay_discount_amount');

		// check if ticket has venue booking, we need to this to
		// send booking emails: include venue info / not
		$data['ticketVenueName'] = "";
		$data['ticketHasVenueBooking'] = false;
		$venueBookingCount = $ticketBookings->where('is_venue_booking', 1)->count();
		if ($venueBookingCount > 0)
		{
			$data['ticketHasVenueBooking'] = true;
			$venueBooking = $ticketBookings->where('is_venue_booking', 1)->first();
			$data['ticketVenueName'] = $venueBooking->provider->name;
		}

		// fetch any venue partner details for the sake of add-ons delivery
		$venuePartnerName = null;
		$venuePartnerPhone = null;
		$venuePartnerAltPhone = null;
		foreach ($ticketBookings as $ticketBooking)
		{
			if (!$venuePartnerName && ($ticketBooking->map_type_id == config('evibe.ticket.type.venue')))
			{
				$venuePartnerName = $ticketBooking->provider->person ? $ticketBooking->provider->person : null;
				$venuePartnerPhone = $ticketBooking->provider->phone ? $ticketBooking->provider->phone : null;
				$venuePartnerAltPhone = $ticketBooking->provider->alt_phone ? $ticketBooking->provider->alt_phone : null;
			}
		}

		$addOns = [];
		foreach ($ticketBookings as $booking)
		{
			// send email for each booking
			$mapTypeId = $booking->mapping->map_type_id;
			$mapId = $booking->mapping->map_id;
			$mappedValues = $this->fillMappingValues([
				                                         'isCollection' => false,
				                                         'mapId'        => $mapId,
				                                         'mapTypeId'    => $mapTypeId
			                                         ]);

			// calculating the total advance
			$data['booking'] = $this->getTicketBookingData($booking, $mappedValues);
			$totalBookingAmount += $data['booking']['bookingAmount'];
			$totalAdvancePaid += $data['booking']['advanceAmount'];
			$totalBalanceAmount += $data['booking']['bookingAmount'] - $data['booking']['advanceAmount'];

			// customer email cc: alt customer email + team (handler + group)
			// vendor email cc: business group + all alt_email{1,2,3}
			$ccAddresses = [config('evibe.contact.customer.group')];
			if ($ticket->handler)
			{
				array_push($ccAddresses, $ticket->handler->username);
			}
			if ($ticket->alt_email)
			{
				array_push($ccAddresses, $ticket->alt_email);
			}
			$data['ccAddresses'] = $ccAddresses;
			$data['vendorCcAddresses'] = $this->getVendorCcAddresses($data['booking']['provider']);
			$data['bookingConceptName'] = $booking->bookingConcept ? $booking->bookingConcept->name : "";

			if ($venuePartnerName &&
				isset($data['booking']['venueDelivery']) && $data['booking']['venueDelivery'] &&
				isset($data['booking']['provider']['providerTypeId']) && ($data['booking']['provider']['providerTypeId'] != config('evibe.ticket.type.venue')) &&
				isset($data['booking']['itemMapTypeId']) && ($data['booking']['itemMapTypeId'] == config('evibe.ticket.type.add-on')))
			{
				$data['booking']['venuePartnerName'] = $venuePartnerName;
				$data['booking']['venuePartnerPhone'] = $venuePartnerPhone;
				$data['booking']['venuePartnerAltPhone'] = $venuePartnerAltPhone;
			}

			// send sms and email to vendor for each booking
			if (in_array($booking->id, $bookingIds))
			{
				//send email and sms to provider
				$orderPartnerShortUrl = $this->generateBookingOrderDetailPageLink($booking);

				// check if normal / updated receipt needs to be sent
				$newReceipt = false;
				if ($partnerChange)
				{
					$newReceipt = true;
				}

				if ($booking->partner_changed_at && $booking->partner_booking_notification_at && ($booking->partner_changed_at > $booking->partner_booking_notification_at))
				{
					$newReceipt = true;
				}

				// partner dash quick link to order details
				$params = $this->getPartnerQuickLoginParams($data['booking']['provider']['userId']);
				$iId = (isset($params['iId']) && $params['iId']) ? $params['iId'] : null;
				$eToken = (isset($params['eToken']) && $params['eToken']) ? $params['eToken'] : null;
				$ticketBookingId = $data['booking']['id'];
				$partnerDashLink = config('evibe.host') . '/partner/orders/' . $ticketBookingId;
				$partnerDashLink .= '?iId=' . $iId . '&eToken=' . $eToken;

				// utm tagging
				$partnerDashEmailLink = $partnerDashLink . '&utm_source=partner-orders-quick-link&utm_campaign=partner-orders&utm_medium=email';
				$partnerDashEmailShortLink = $this->getShortenUrl($partnerDashEmailLink);

				$partnerDashSMSLink = $partnerDashLink . '&utm_source=partner-orders-quick-link&utm_campaign=partner-orders&utm_medium=sms';
				$partnerDashSMSShortLink = $this->getShortenUrl($partnerDashSMSLink);

				$data['partnerDashLink'] = $partnerDashLink;
				$data['partnerDashEmailLink'] = $partnerDashEmailShortLink;
				$data['partnerDashSMSLink'] = $partnerDashSMSShortLink;

				if ($newReceipt)
				{
					$smsTpl = config('evibe.sms_tpl.book.vendor');
					$replaces = [
						'#field1#' => $data['booking']['provider']['person'],
						'#field2#' => $data['booking']['partyDate'],
						'#field3#' => $this->formatPriceWithoutChar($data['booking']['advanceAmount']),
						'#field4#' => $data['booking']['provider']['email'],
						'#field5#' => $data['customer']['phone']
					];
					$smsText = str_replace(array_keys($replaces), array_values($replaces), $smsTpl);
					$smsData = [
						'to'   => $data['booking']['provider']['phone'],
						'text' => $smsText
					];

					// Sending null data to reduce the size of queue
					$queueData = $data;
					$relations = $queueData["ticket"]->getRelations();
					unset($relations['bookings']);
					$queueData["ticket"]->setRelations($relations);
					$queueData["booking"]["checkoutFields"] = [];

					$this->dispatch(new MailBookingReceiptToPartnerJob($queueData));
					$this->dispatch(new SMSBookingToPartnerJob($smsData));

					// send app notification to partner
					try
					{
						$this->sendAppNotificationToPartnerOnNewOrder($booking);
					} catch (\Exception $e)
					{
						Log::error("Error while sending the partner change fresh receipt for booking id :" . $booking->id);
					}
				}
				else
				{
					$smsTpl = config('evibe.sms_tpl.book.update.partner');
					$replaces = [
						'#field1#' => $data['booking']['provider']['person'],
						'#field2#' => $data['booking']['partyDate'],
						'#field3#' => $data['customer']['name'],
						'#field4#' => $data['booking']['provider']['email'],
						'#field5#' => $orderPartnerShortUrl
					];
					$smsText = str_replace(array_keys($replaces), array_values($replaces), $smsTpl);
					$smsData = [
						'to'   => $data['booking']['provider']['phone'],
						'text' => $smsText
					];

					// Sending null data to reduce the size of queue
					$queueData = $data;
					$relations = $queueData["ticket"]->getRelations();
					unset($relations['bookings']);
					$queueData["ticket"]->setRelations($relations);
					$queueData["booking"]["checkoutFields"] = [];

					$this->dispatch(new MailBookingUpdatedReceiptToPartner($queueData));
					$this->dispatch(new SMSBookingUpdatedReceiptToPartner($smsData));

					// send app notification to partner
					try
					{
						$this->sendAppNotificationToPartnerOnUpdatedOrder($booking);
					} catch (\Exception $e)
					{
						Log::error("Error while sending the updated receipt for booking id :" . $booking->id);
					}
				}

				// update booking with partner booking notification status
				$booking->partner_booking_notification_at = Carbon::now();
				$booking->save();
			}

			// job to update lookup table (schedule settlement bookings)
			// @see: shifted to bottom after the update of ticket status to avoid cancellation link issue

			// update into main booking array only after all the values are calculated and set
			$allBookings[] = $data['booking'];

			$bookingAddOns = $this->fetchAddOns($ticket->city_id, $ticket->event_id, $booking->mapping->map_type_id, $booking->mapping->map_id, $booking);
			if (is_array($bookingAddOns))
			{
				$addOns = array_merge($addOns, $bookingAddOns);
			}

		}

		$data['addOns'] = collect($addOns)->unique()->take(4);
		$tmoToken = Hash::make($ticketId . config('evibe.token.track-my-order'));
		$tmoLink = route("track.orders") . "?ref=email&id=" . $ticketId . "&token=" . $tmoToken;
		$data['tmoLink'] = $tmoLink;

		$data['bookings']['minBookingDateTime'] = $ticketBookings->min('party_date_time');
		$data['bookings']['maxBookingDateTime'] = $ticketBookings->max('party_date_time');

		// Managing coupon & internet handling fee
		// Coupon discount will effect only total advance paid
		$totalBookingAmount += $data['additional']['internetHandlingFee'];
		$totalAdvancePaid += $data['additional']['internetHandlingFee'] - $data["couponDiscount"];

		$orderShortUrl = $this->generateCustomerOrderDetailsPageLink($ticket);
		$data['totalBookingAmount'] = $totalBookingAmount;
		$data['totalAdvancePaid'] = $totalAdvancePaid;
		$data['totalBalanceAmount'] = $totalBalanceAmount;
		$data['allBookings'] = $allBookings;

		$partyTime = getPartyDateTimeByBookingsPartyTime($data['bookings']['minBookingDateTime'], $data['bookings']['maxBookingDateTime']);
		$data['partyDateTime'] = $partyTime;

		// send single email & sms to customer
		$smsTpl = config('evibe.sms_tpl.book.update.customer');
		$token = Hash::make($ticket->id . "EVBTMO");

		$replaces = [
			'#field1#' => ucfirst($ticket->name),
			'#field2#' => $ticket->enquiry_id,
			'#field3#' => $this->getShortenUrl(route("track.orders") . "?ref=SMS&id=" . $ticket->id . "&token=" . $token)
		];

		$smsText = str_replace(array_keys($replaces), array_values($replaces), $smsTpl);
		$smsData = [
			'to'   => $data['customer']['phone'],
			'text' => $smsText
		];

		// Sending null data to reduce the size of queue
		$tempAllDataBookings = [];
		$queueData = $data;
		$relations = $queueData["ticket"]->getRelations();
		unset($relations['bookings']);
		$queueData["ticket"]->setRelations($relations);
		$queueData["booking"]["checkoutFields"] = [];
		foreach ($queueData["allBookings"] as $queueBooking)
		{
			$tempAllDataBookings[] = [
				"bookingAmount"      => $queueBooking["bookingAmount"],
				"advanceAmount"      => $queueBooking["advanceAmount"],
				"typeBookingDetails" => $queueBooking["typeBookingDetails"],
				"provider"           => [
					"phone"  => $queueBooking["provider"]["phone"],
					"person" => $queueBooking["provider"]["person"]
				]
			];
		}
		$queueData["allBookings"] = $tempAllDataBookings;

		$this->dispatch(new MailBookingUpdatedReceiptToCustomer($queueData));
		$this->dispatch(new SMSBookingUpdatedReceiptToCustomer($smsData));

		// update timestamp & save action
		$forUpdateData = new PaySuccessUpdateTicket();
		$forUpdateData->setHandlerId($handlerId);
		$forUpdateData->setIsFromDash($isFromDash);
		$forUpdateData->setStatusId(config('evibe.ticket.status.booked'));
		$forUpdateData->setSuccessEmailType(config('evibe.ticket.success_type.booked'));

		$success = $this->updateTicketPostSendReceipts($ticket, $forUpdateData);

		// job to update lookup table (schedule settlement bookings)
		foreach ($ticketBookings as $booking)
		{
			$this->dispatch(new UpdateScheduleSettlementBooking([
				                                                    'ticketBookingId' => $booking->id,
				                                                    'handler'         => $ticket->handler ? $ticket->handler : null
			                                                    ]));
		}

		return response(['success' => $success]);
	}

	public function triggerRequestNewPaymentLink($ticketId)
	{
		$ticket = Ticket::find($ticketId);
		if ($ticket)
		{
			$followUpTime = Carbon::today()->addHours(20)->timestamp;
			$currentTimeHour = Carbon::now()->hour;
			if ($currentTimeHour >= 0 && $currentTimeHour < 10)
			{
				$followUpTime = Carbon::today()->addHours(12)->timestamp;
			}
			elseif ($currentTimeHour >= 10 && $currentTimeHour < 13)
			{
				$followUpTime = Carbon::today()->addHours(15)->timestamp;
			}
			elseif ($currentTimeHour >= 13 && $currentTimeHour < 19)
			{
				$followUpTime = Carbon::today()->addHours(20)->timestamp;
			}
			elseif ($currentTimeHour >= 19 && $currentTimeHour < 24)
			{
				$followUpTime = Carbon::tomorrow()->addHours(10)->addMinutes(30)->timestamp;
			}

			$followupData = [
				'followup_date' => $followUpTime,
				'comments'      => "Customer has requested the payment link",
				'ticket_id'     => $ticketId,
			];

			$isFollowup = TicketFollowup::where('ticket_id', $ticketId)
			                            ->whereNull('is_complete')
			                            ->first();
			if ($isFollowup)
			{
				$isFollowup->update($followupData);
			}
			else
			{
				TicketFollowup::create($followupData);
			}

			$ticket->update([
				                "status_id"      => config("evibe.ticket.status.followup"),
				                "lead_status_id" => config("evibe.ticket.type_lead_status.hot")
			                ]);

			$this->updateTicketAction([
				                          "ticket"   => $ticket,
				                          "statusId" => $ticket->status_id,
				                          "comments" => "Customer requested for new payment link, After old link got expired."
			                          ]);

			$customerData = [
				'id'            => $ticket->id,
				'name'          => $ticket->name,
				'mail'          => $ticket->email,
				'phone'         => $ticket->phone,
				'followUpTime'  => $followUpTime,
				'comments'      => "Regarding to the request of new payment link",
				'ticketId'      => $ticket->id,
				'partyDateTime' => date('d M Y h:i A', $ticket->event_date)
			];

			//$this->dispatch(new MailRequestNewPaymentToTeam($customerData));
			$this->dispatch(new MailRequestNewPaymentToCustomer($customerData));

			$res = [
				'success' => true,
				'message' => 'You have been successfully requested payment link , We will get back to you soon',
			];

			return response()->json($res);
		}
		else
		{
			return response()->json([
				                        'success' => false,
				                        'message' => 'Ticket not found while customer requesting the payment link',
			                        ]);
		}
	}

	private function isPartyTwoDaysBefore($data)
	{
		$twoDaysBefore = false;

		$currentTimestamp = time();
		$fullDaySeconds = 24 * 60 * 60 - 1;
		$partyDate = $data['additional']['plainDateTime'];

		// today
		$todayStart = strtotime('today midnight', $currentTimestamp);

		// tomorrow
		$tomorrowStart = strtotime('tomorrow midnight', $currentTimestamp);
		$tomorrowEnd = $tomorrowStart + $fullDaySeconds;

		// check if party falls today / tomorrow - fetch SMS templates accordingly
		if ($partyDate >= $todayStart && $partyDate <= $tomorrowEnd)
		{
			$twoDaysBefore = true;
		}

		return $twoDaysBefore;
	}
}