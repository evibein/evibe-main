<?php

namespace App\Http\Controllers\Payments;

use App\Jobs\Emails\Payment\PaymentTransactionInfoUpdate;
use App\Models\Ticket\Ticket;
use Evibe\Implementers\PaymentGateway;
use Illuminate\Support\Facades\Log;

class FreeChargeController extends BasePaymentController implements PaymentGateway
{
	/* @implements */
	public function onPaymentInit($ticketId)
	{
		$validation = $this->validateUserInputAndNotifyTeam(request()->input(), $ticketId, "freeCharge", config("evibe.type-payment-gateway.freecharge"));

		if (!$validation['success'])
		{
			$validationError = array_key_exists("error", $validation) ? $validation['error'] : "Please input all the required fields and try again.";

			return response()->json(['success' => false, 'error' => $validationError]);
		}

		$freeChargeData = [
			"amount"        => $validation['advanceAmount'],
			"channel"       => "WEB",
			"currency"      => "INR",
			"customNote"    => "Evibe.in payment !",
			"customerName"  => $validation['ticket']['name'],
			"email"         => $validation['ticket']['email'],
			"furl"          => route('pay.common.fail'),
			"merchantId"    => config("freecharge.merchantId"),
			"merchantTxnId" => 'EVIBE-' . $ticketId,
			"mobile"        => $validation['ticket']['phone'],
			"os"            => "ubuntu-16.04",
			"productInfo"   => 'Evibe.in Advance Payment - ' . $ticketId,
			"surl"          => route('pay.pgs.freeCharge.success')
		];

		$checksumJson = json_encode($freeChargeData, JSON_UNESCAPED_SLASHES) . config("freecharge.merchantKey");
		$checksum = hash('sha256', $checksumJson);

		$freeChargeData["checksum"] = $checksum;

		return response()->json([
			                        'success'    => true,
			                        'freeCharge' => $freeChargeData
		                        ]);
	}

	public function onPaymentReceived()
	{
		$verification = $this->verifyReceivedPayment();
		$ticketId = $verification['ticketId'];

		if ($verification['success'])
		{
			$this->dispatch(new PaymentTransactionInfoUpdate([
				                                                 "ticketId"     => $ticketId,
				                                                 "paymentRefId" => request("txnId"),
				                                                 "isSuccess"    => true
			                                                 ]));

			return $this->showIntermediatePage($ticketId);
		}
		else
		{
			return view('pay/fail/capture-fail');
		}
	}

	private function verifyReceivedPayment()
	{
		$response = [
			'success'  => false,
			'ticketId' => null
		];

		$responseData = [
			"amount"        => request("amount"),
			"authCode"      => request("authCode"),
			"merchantTxnId" => request("merchantTxnId"),
			"status"        => request("status"),
			"txnId"         => request("txnId")
		];

		$checksumJson = json_encode($responseData, JSON_UNESCAPED_SLASHES) . config("freecharge.merchantKey");
		$generatedChecksum = hash('sha256', $checksumJson);

		$ticketId = str_replace("EVIBE-", "", $responseData["merchantTxnId"]);
		$paymentId = $responseData["txnId"];
		$originalChecksum = request("checksum");

		// check if valid ticket
		$ticket = Ticket::with('bookings.mapping', 'bookings.provider')->find($ticketId);

		if (!$ticket)
		{
			Log::error("FreeCharge ShowSuccessPage :: Ticket not found :: ticketId: $ticketId");
		}
		else
		{
			if ($generatedChecksum == $originalChecksum)
			{
				$response['success'] = true;
				$response['ticketId'] = $ticketId;

				$this->updateTicketBookingsWithPaymentReference($paymentId, $ticket, config('evibe.type-payment-gateway.freecharge'));
			}
			else
			{
				Log::error("FreeCharge ShowSuccessPage::Checksum mismatch::ticketId: " . $ticketId . ":: originalCheckSum ::" . $originalChecksum . ":: generatedChecksum ::" . $generatedChecksum);
			}
		}

		return $response;
	}
}