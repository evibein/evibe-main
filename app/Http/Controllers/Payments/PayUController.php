<?php

namespace App\Http\Controllers\Payments;

use App\Jobs\Emails\Payment\PaymentTransactionInfoUpdate;
use App\Models\Product\ProductBooking;
use App\Models\Ticket\Ticket;
use Evibe\Implementers\PaymentGateway;
use Illuminate\Support\Facades\Log;

/**
 * @author Anji <anji@evibe.in>
 * @since  28 Apr 2015
 */
class PayUController extends BasePaymentController implements PaymentGateway
{
	/* @implements */
	public function onPaymentInit($ticketId)
	{
		$productBooking = ProductBooking::where('booking_order_id', $ticketId)->first();
		if ($productBooking)
		{
			$txnId = 'EVIBE-PB-' . $productBooking->id . '-' . time();
			$validation = $this->validateProductUserInputAndNotifyTeam(request()->input(), $productBooking->id, "PayUMoney", config("evibe.type-payment-gateway.payumoney"), $txnId);

			if (!$validation['success'])
			{
				$validationError = array_key_exists('error', $validation) ? $validation['error'] : 'Please input all the required fields and try again.';

				return response()->json(['success' => false, 'error' => $validationError]);
			}
			$advanceAmount = $validation['advanceAmount'];

			// reply with success and data needed by payU
			$payUData = [
				'key'         => config('payumoney.key'),
				'txnid'       => $txnId,
				'amount'      => $advanceAmount,
				'productinfo' => 'Evibe.in Advance Payment - #' . $productBooking->booking_order_id,
				'firstname'   => $productBooking->name,
				'email'       => $productBooking->email,
				'phone'       => $productBooking->phone ? $productBooking->phone : config('evibe.contact.company.phone'),
				'surl'        => route('pay.pgs.payU.success', 'piab'),
				'furl'        => route('pay.common.fail')
			];
		}
		else
		{
			$txnId = 'EVIBE-' . $ticketId . '-' . time();

			$validation = $this->validateUserInputAndNotifyTeam(request()->input(), $ticketId, "PayUMoney", config("evibe.type-payment-gateway.payumoney"), $txnId);
			if (!$validation['success'])
			{
				$validationError = array_key_exists('error', $validation) ? $validation['error'] : 'Please input all the required fields and try again.';

				return response()->json(['success' => false, 'error' => $validationError]);
			}

			$ticket = $validation['ticket'];
			$advanceAmount = $validation['advanceAmount'];

			// reply with success and data needed by payU
			$payUData = [
				'key'         => config('payumoney.key'),
				'txnid'       => $txnId,
				'amount'      => $advanceAmount,
				'productinfo' => 'Evibe.in Advance Payment - #' . $ticket->id,
				'firstname'   => $ticket->name,
				'email'       => $ticket->email,
				'phone'       => $ticket->phone ? $ticket->phone : config('evibe.contact.company.phone'),
				'surl'        => route('pay.pgs.payU.success'),
				'furl'        => route('pay.common.fail')
			];
		}

		$shaText = $payUData['key'] . '|' . $payUData['txnid'] . '|' . $payUData['amount'] . '|';
		$shaText .= $payUData['productinfo'] . '|' . $payUData['firstname'] . '|';
		$shaText .= $payUData['email'] . '|' . $ticketId . '||||||||||' . config('payumoney.salt');

		$payUData['hash'] = strtolower(hash('sha512', $shaText));

		return response()->json([
			                        'success' => true,
			                        'payu'    => $payUData
		                        ]);
	}

	/* @implements */
	public function onPaymentReceived($param = null)
	{
		$verification = $this->verifyReceivedPayment(request()->input(), $param);
		$ticketId = $verification['ticketId'];

		if ($verification['success'])
		{
			$productBooking = ProductBooking::where('booking_order_id', $ticketId)->first();
			if ($productBooking)
			{
				$this->dispatch(new PaymentTransactionInfoUpdate([
					                                                 "piabId"       => $productBooking->id,
					                                                 "paymentRefId" => request()->input('payuMoneyId'),
					                                                 "isSuccess"    => true
				                                                 ]));

				return $this->showProductIntermediatePage($productBooking->id);
			}
			else
			{
				$this->dispatch(new PaymentTransactionInfoUpdate([
					                                                 "ticketId"     => $ticketId,
					                                                 "paymentRefId" => request()->input('payuMoneyId'),
					                                                 "isSuccess"    => true
				                                                 ]));

				return $this->showIntermediatePage($ticketId);
			}
		}
		else
		{
			return view('pay/fail/capture-fail');
		}
	}

	private function verifyReceivedPayment($receivedData, $param = null)
	{
		$ticketId = $receivedData['udf1'];
		$status = $receivedData['status'];
		$key = $receivedData['key'];
		$txnid = $receivedData['txnid'];
		$amount = $receivedData['amount'];
		$productinfo = $receivedData['productinfo'];
		$firstname = $receivedData['firstname'];
		$email = $receivedData['email'];
		$originalHash = $receivedData['hash'];
		$paymentId = $receivedData['payuMoneyId'];
		$salt = (string)config('payumoney.salt');
		$response = [
			'success'  => false,
			'ticketId' => null
		];

		if ($param && ($param == 'piab'))
		{
			// check if valid ticket
			$productBooking = ProductBooking::where('booking_order_id', $ticketId)->first();

			if (!$productBooking)
			{
				Log::error("PayUMoney ShowSuccessPage :: Product Booking not found :: productBookingOrderId: $ticketId");
			}
			else
			{
				// check payU after transaction checksum
				$shaText = $salt . '|' . $status . '||||||||||' . $ticketId . '|' . $email . '|';
				$shaText .= $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
				$retHash = hash('sha512', $shaText);

				Log::error("PayU :: SHATEXT : $shaText");

				// @todo: check advance amount paid & to be paid

				if ($originalHash == $retHash)
				{
					$response['success'] = true;
					$response['ticketId'] = $ticketId;

					$this->updateProductBookingsWithPaymentReference($paymentId, $productBooking, config('evibe.type-payment-gateway.payumoney'));
				}
				else
				{
					Log::error("PayUMoney ShowSuccessPage::Hash mismatch::ticketId: " . $ticketId . ":: originalHash ::" . $originalHash . ":: retHash ::" . $retHash);
				}
			}
		}
		else
		{
			// check if valid ticket
			$ticket = Ticket::with('bookings.mapping', 'bookings.provider')->find($ticketId);

			if (!$ticket)
			{
				Log::error("PayUMoney ShowSuccessPage :: Ticket not found :: ticketId: $ticketId");
			}
			else
			{
				// check payU after transaction checksum
				$shaText = $salt . '|' . $status . '||||||||||' . $ticketId . '|' . $email . '|';
				$shaText .= $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
				$retHash = hash('sha512', $shaText);

				Log::error("PayU :: SHATEXT : $shaText");

				// @todo: check advance amount paid & to be paid

				if ($originalHash == $retHash)
				{
					$response['success'] = true;
					$response['ticketId'] = $ticketId;

					$this->updateTicketBookingsWithPaymentReference($paymentId, $ticket, config('evibe.type-payment-gateway.payumoney'));
				}
				else
				{
					Log::error("PayUMoney ShowSuccessPage::Hash mismatch::ticketId: " . $ticketId . ":: originalHash ::" . $originalHash . ":: retHash ::" . $retHash);
				}
			}
		}

		return $response;
	}
}
