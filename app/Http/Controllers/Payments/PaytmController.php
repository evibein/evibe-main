<?php

namespace App\Http\Controllers\Payments;

use App\Jobs\Emails\Payment\PaymentTransactionInfoUpdate;
use App\Models\Ticket\Ticket;
use Evibe\Implementers\PaymentGateway;
use Illuminate\Support\Facades\Log;

class PaytmController extends BasePaymentController implements PaymentGateway
{
	public function onPaymentInit($ticketId)
	{
		$txnId = 'evibe' . $ticketId . time();
		$validation = $this->validateUserInputAndNotifyTeam(request()->input(), $ticketId, "paytm", config("evibe.type-payment-gateway.paytm"), $txnId);

		if (!$validation['success'])
		{
			$validationError = array_key_exists("error", $validation) ? $validation['error'] : "Please input all the required fields and try again.";

			return response()->json(['success' => false, 'error' => $validationError]);
		}

		$paytmData = [
			"MID"              => config("paytm.mid"),
			"ORDER_ID"         => $txnId,
			"CUST_ID"          => $ticketId,
			"INDUSTRY_TYPE_ID" => "Retail109",
			"CHANNEL_ID"       => "WEB",
			"TXN_AMOUNT"       => $validation['advanceAmount'],
			"WEBSITE"          => config("paytm.type"),
			"CALLBACK_URL"     => route('pay.pgs.paytm.success')
		];

		$paytmData["checksum"] = $this->getPaytmChecksumFromArray($paytmData, config("paytm.key"));

		return response()->json([
			                        'success' => true,
			                        'paytm'   => $paytmData
		                        ]);
	}

	public function onPaymentReceived()
	{
		$verification = $this->verifyReceivedPayment();
		$ticketId = $verification['ticketId'];

		if ($verification['success'])
		{
			$this->dispatch(new PaymentTransactionInfoUpdate([
				                                                 "ticketId"     => $ticketId,
				                                                 "paymentRefId" => request()->input('TXNID'),
				                                                 "isSuccess"    => true
			                                                 ]));

			return $this->showIntermediatePage($ticketId);
		}
		else
		{
			return view('pay/fail/capture-fail');
		}
	}

	private function verifyReceivedPayment()
	{
		$response = [
			'success'  => false,
			'ticketId' => null
		];

		$paramList = request()->all();
		$paytmChecksum = request("CHECKSUMHASH") ?: "";
		$ticketId = substr($paramList["ORDERID"], 5, -10);
		$checkChecksum = [
			"MID"     => config("paytm.mid"),
			"ORDERID" => 'evibe' . $ticketId
		];
		$checkChecksum["CHECKSUMHASH"] = $this->getPaytmChecksumFromArray($checkChecksum, config("paytm.key"));

		// generation of transaction status API
		//Log::info("https://securegw-stage.paytm.in/merchant-status/getTxnStatus?JsonData=" . json_encode($checkChecksum, JSON_UNESCAPED_SLASHES));

		//Verify all parameters received from Paytm pg to your application. Like MID received from paytm pg is same as your applications MID, TXN_AMOUNT and ORDER_ID are same as what was sent by you to Paytm PG for initiating transaction etc.
		$isValidChecksum = $this->verifyPaytmChecksum($paramList, config("paytm.key"), $paytmChecksum); //will return TRUE or FALSE string.

		if ($isValidChecksum == "TRUE")
		{
			if (request("STATUS") == "TXN_SUCCESS")
			{
				// check if valid ticket
				$ticket = Ticket::with('bookings.mapping', 'bookings.provider')->find($ticketId);

				if (!$ticket)
				{
					Log::error("Paytm ShowSuccessPage :: Ticket not found :: ticketId: $ticketId");
				}
				else
				{
					$response['success'] = true;
					$response['ticketId'] = $ticketId;

					$this->updateTicketBookingsWithPaymentReference(isset($paramList["TXNID"]) ? $paramList["TXNID"] : "", $ticket, config('evibe.type-payment-gateway.paytm'));
				}
			}
			else
			{
				Log::error("Paytm ShowSuccessPage :: Transaction status is failure ::ticketId: " . $ticketId);
			}
		}
		else
		{
			Log::error("Paytm ShowSuccessPage::Checksum mismatch::ticketId: " . $ticketId);
		}

		return $response;
	}

	public function verifyPaytmChecksum($arrayList, $key, $checksumvalue)
	{
		if (isset($arrayList["CHECKSUMHASH"]))
		{
			unset($arrayList["CHECKSUMHASH"]);
		}

		// sorting the array in alphabetical order
		ksort($arrayList);
		$str = $this->getArray2StrForVerify($arrayList);

		$key = html_entity_decode($key);
		$iv = "@@@@&&&&####$$$$";
		$paytm_hash = openssl_decrypt($checksumvalue, "AES-128-CBC", $key, 0, $iv);

		$salt = substr($paytm_hash, -4);
		$finalString = $str . "|" . $salt;
		$website_hash = hash("sha256", $finalString);
		$website_hash .= $salt;

		if ($website_hash == $paytm_hash)
		{
			$validFlag = "TRUE";
		}
		else
		{
			$validFlag = "FALSE";
		}

		return $validFlag;
	}

	public function getArray2StrForVerify($arrayList)
	{
		$paramStr = "";
		$flag = 1;
		foreach ($arrayList as $key => $value)
		{
			if ($flag)
			{
				$paramStr .= $this->checkString_e($value);
				$flag = 0;
			}
			else
			{
				$paramStr .= "|" . $this->checkString_e($value);
			}
		}

		return $paramStr;
	}

	public function getPaytmChecksumFromArray($arrayList, $key, $sort = 1)
	{
		if ($sort != 0)
		{
			ksort($arrayList);
		}

		$str = $this->getArray2Str($arrayList);

		// get random string with 4 digits
		$salt = substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(4 / strlen($x)))), 1, 4);

		$finalString = $str . "|" . $salt;
		$hash = hash("sha256", $finalString);
		$hashString = $hash . $salt;

		$key = html_entity_decode($key);
		$iv = "@@@@&&&&####$$$$";
		$checksum = openssl_encrypt($hashString, "AES-128-CBC", $key, 0, $iv);

		return $checksum;
	}

	public function getArray2Str($arrayList)
	{
		$findme = 'REFUND';
		$findmepipe = '|';
		$paramStr = "";
		$flag = 1;
		foreach ($arrayList as $key => $value)
		{
			$pos = strpos($value, $findme);
			$pospipe = strpos($value, $findmepipe);
			if ($pos !== false || $pospipe !== false)
			{
				continue;
			}

			if ($flag)
			{
				$paramStr .= $this->checkString_e($value);
				$flag = 0;
			}
			else
			{
				$paramStr .= "|" . $this->checkString_e($value);
			}
		}

		return $paramStr;
	}

	public function checkString_e($value)
	{
		if ($value == 'null')
		{
			$value = '';
		}

		return $value;
	}
}