<?php

namespace App\Http\Controllers\Payments;

use App\Http\Controllers\Base\BaseController;
use App\Jobs\Emails\Errors\MailLocationMismatchAlertToTeamJob;
use App\Jobs\Emails\Payment\PaymentUserInfoUpdate;
use App\Jobs\Emails\ReportIssue\BookingPartyDateTimeAlertToTeam;
use App\Jobs\Emails\Util\MailEInviteCommunicationToCustomerJob;
use App\Jobs\Emails\Util\MailEInviteCommunicationToTeamJob;
use App\Jobs\Emails\Util\MailLocationVerificationAlertToTeam;
use App\Jobs\ScheduleBookingSettlementsJob;
use App\Jobs\Emails\Util\UpdatePartyLocationAlertToTeamJob;
use App\Jobs\SMS\SMSEInviteCommunicationToCustomerJob;
use App\Jobs\Util\CheckReferralCouponCode;
use App\Models\AddOn\AddOn;
use App\Models\Cake\Cake;
use App\Models\Coupon\Coupon;
use App\Models\Coupon\CouponRedeemed;
use App\Models\Coupon\CouponRedeemedPIAB;
use App\Models\Wallet\Transaction;
use App\Models\Wallet\WalletRedeemed;
use App\Models\Decor\Decor;
use App\Models\Package\Package;
use App\Models\Payment\PaymentTransactionAttempts;
use App\Models\Product\ProductBooking;
use App\Models\Product\ProductBookingAdditionalField;
use App\Models\Product\ProductBookingAdditionalFieldValue;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketBooking;
use App\Models\Ticket\TicketGallery;
use App\Models\Ticket\TicketGoogleAnalyticsData;
use App\Models\Ticket\TicketUpdate;
use App\Models\Trend\Trend;
use App\Models\Types\TypeServices;
use App\Models\Types\TypeVenue;
use App\Models\Util\Area;
use App\Models\Util\CheckoutField;
use App\Models\Util\CheckoutFieldValue;
use App\Models\Util\City;
use App\Models\Util\PostalPinCode;
use App\Models\Util\User;
use App\Models\Vendor\Vendor;
use App\Models\Venue\VenueHall;
use App\Jobs\Emails\Payment\Team\MailPaymentInitiatedJob;

use App\Models\Wallet\Wallet;
use Carbon\Carbon;
use Evibe\Passers\PaySuccessUpdateTicket;
use Evibe\Passers\PreSendReceiptStatus;
use Evibe\Passers\Util\SiteLogData;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class BasePaymentController extends BaseController
{
	public function showIntermediatePage($ticketId)
	{
		$ticket = Ticket::find($ticketId);
		$isAutoBooked = $ticket->is_auto_booked;

		if ($isAutoBooked)
		{
			$token = Hash::make($ticket->phone);
			//$route = $this->getRouteForAutoBookedIntermediatePage($ticket);

			// todo: check for campaigns
			$route = route('auto-book.pay.auto.process', $ticketId);
			$viewData = ['redirectTo' => $route . "?token=" . $token];
		}
		else
		{
			$viewData = ['redirectTo' => route('pay.normal.process', $ticketId)];

			// only for normal bookings, add it to lookup table for settlements
			$this->dispatch(new ScheduleBookingSettlementsJob(['ticketId' => $ticketId]));
		}

		return view('pay/intermediate', ['data' => $viewData]);
	}

	public function showProductIntermediatePage($productBookingId)
	{
		$token = Hash::make($productBookingId);
		$route = route('party-kits.buy.process', $productBookingId);
		$viewData = ['redirectTo' => $route . "?token=" . $token];

		return view('product/pay/intermediate', ['data' => $viewData]);
	}

	public function getTicketData($ticket)
	{
		$partyDateTime = date("d/m/y H:i", $ticket->event_date);
		$partyDate = date("d M Y", $ticket->event_date);
		$partyStartTime = date("h:i A", $ticket->event_date);
		$checkoutFields = $this->getCheckoutPageFields($ticket->id);
		$cityName = getCityName();
		if (!$cityName)
		{
			$defaultCity = City::where('is_default', 1)->first();
			$cityName = $defaultCity ? $defaultCity->name : "";
		}

		// bookings available
		$data = [
			'ticketId'      => $ticket->id,
			'ticket'        => $ticket,
			'customer'      => [
				'name'        => ucfirst($ticket->name),
				'email'       => $ticket->email,
				'altEmail'    => $ticket->alt_email,
				'phone'       => $ticket->phone,
				'callingCode' => $ticket->calling_code,
				'altPhone'    => $ticket->alt_phone,
			],
			'additional'    => [
				'partyDateTime'       => $partyDateTime,
				'partyDate'           => $partyDate,
				'partyStartTime'      => $partyStartTime,
				'checkInDate'         => $partyDate,
				'cityName'            => $cityName,
				'typeTicketId'        => $ticket->type_ticket_id,
				'venueAddressLine1'   => $ticket->address_line1,
				'venueAddressLine2'   => $ticket->address_line2,
				'venueLocation'       => $ticket->area ? $ticket->area->name : $ticket->venue_location,
				'venueZipCode'        => $ticket->zip_code,
				'venueAddress'        => $ticket->venue_address,
				'venueLandmark'       => $ticket->venue_landmark,
				'specialNotes'        => $ticket->spl_notes,
				'guestsCount'         => $ticket->guests_count,
				'cakeDeliveryDate'    => $partyDate,
				'cakeDeliverySlot'    => $ticket->cake_slot,
				'area'                => $ticket->area ? $ticket->area->name : "NA",
				'altPhone'            => $ticket->alt_phone,
				'plainDateTime'       => $ticket->event_date,
				'typeVenues'          => TypeVenue::where('is_checkout_page', 1)->get(),
				'ticketVenueType'     => $ticket->typeCheckoutVenue,
				'checkoutFields'      => $checkoutFields,
				'mapLink'             => $this->getShortenUrl($ticket->map_link),
				'lat'                 => $ticket->lat,
				'lng'                 => $ticket->lng,
				'foodType'            => $ticket->food_type,
				'slot'                => $ticket->slot,
				'checkoutFieldsUser'  => $this->getCheckoutPageFields($ticket->id, 0),
				'checkoutFieldsCrm'   => $this->getCheckoutPageFields($ticket->id, 1), // Crm checkout field
				'paidAt'              => $ticket->paid_at,
				'eventId'             => $ticket->event_id,
				'eventName'           => $ticket->event_id ? $this->getEventName($ticket->event_id) : '',
				'areaId'              => $ticket->area_id,
				'cityId'              => $ticket->city_id,
				'internetHandlingFee' => $ticket->internet_handling_fee,
				'eInvite'             => $ticket->e_invite
			],
			'bookings'      => [
				'list' => []
			],
			'booking'       => '',
			'require'       => [],
			'isAutoBooking' => $ticket->is_auto_booked ? true : false
		];

		if (count($checkoutFields))
		{
			foreach ($checkoutFields as $checkoutField)
			{
				$checkoutFieldValue = $checkoutField->getCheckoutValue($ticket->id);
				$notApplicable = $checkoutField->is_crm && strtolower($checkoutFieldValue) == "n/a" ? true : false;

				if (!$notApplicable)
				{
					$data['additional'][$checkoutField->name] = $checkoutFieldValue;
				}
			}
		}

		$data['gallery'] = [];
		$ticketGallery = TicketGallery::where('ticket_id', $ticket->id)
		                              ->whereNull('deleted_at')
		                              ->get();

		if ($ticketGallery)
		{
			foreach ($ticketGallery as $image)
			{
				$imageLink = config('evibe.gallery.host') . '/ticket/' . $ticket->id . '/images/' . $image->url;
				array_push($data['gallery'], $imageLink);
			}
		}

		$customerDataFields = $this->getCustomerDataFields($ticket->id);
		$data['additional']['checkoutFieldsCustomerData'] = $customerDataFields;

		if (count($customerDataFields))
		{
			foreach ($customerDataFields as $checkoutField)
			{
				$checkoutFieldValue = $checkoutField->getCheckoutValue($ticket->id);
				$notApplicable = $checkoutField->is_crm && strtolower($checkoutFieldValue) == "n/a" ? true : false;

				if (!$notApplicable)
				{
					$data['additional'][$checkoutField->name] = $checkoutFieldValue;
				}
			}
		}

		return $data;
	}

	public function getTicketBookingData($booking, $mappedValues, $isPartnerUpdate = 0)
	{
		$partyEndTime = $booking->party_end_time;
		$partyEndTimeString = null;
		if ($partyEndTime)
		{
			$partyEndTimeString = date("g:i A", $partyEndTime);
			if (strtotime(date('d M Y', $booking->party_date_time)) < strtotime(date('d M Y', $booking->party_end_time)))
			{
				$partyEndTimeString .= " (Next Day)";
			}
		}

		// @todo: remove duplicate 'party_date_time' and 'party_end_time'
		$data = [
			'id'                           => $booking->id,
			'bookingId'                    => $booking->booking_id,
			'checkInTime'                  => date("g:i A", $booking->party_date_time),
			'checkOutTime'                 => $partyEndTimeString,
			'partyDateTime'                => $booking->party_date_time,
			'partyEndTime'                 => $booking->party_end_time,
			'partyDate'                    => date("d M Y", $booking->party_date_time),
			'checkInDate'                  => date("d M Y", $booking->party_date_time),
			'bookingInfo'                  => $booking->booking_info,
			'bookingAmount'                => $booking->booking_amount,
			'productPrice'                 => $booking->product_price,
			'advanceAmount'                => $booking->advance_amount,
			'tokenAmount'                  => $booking->token_amount,
			'advancePaid'                  => $booking->is_advance_paid,
			'estimatedTransportCharges'    => $booking->estimated_transport_charges,
			'transportCharges'             => $booking->transport_charges,
			'specialNotes'                 => $booking->spl_notes,
			/* All price related fields in price format */
			'bookingAmountStr'             => $booking->booking_amount ? $this->formatPrice($booking->booking_amount) : 0,
			'productPriceStr'              => $booking->product_price ? $this->formatPrice($booking->product_price) : 0,
			'advanceAmountStr'             => $booking->advance_amount ? $this->formatPrice($booking->advance_amount) : 0,
			'tokenAmountStr'               => $booking->token_amount ? $this->formatPrice($booking->token_amount) : 0,
			'estimatedTransportChargesStr' => $booking->estimated_transport_charges ? $this->formatPrice($booking->estimated_transport_charges) : 0,
			'transportChargesStr'          => $booking->transport_charges ? $this->formatPrice($booking->transport_charges) : 0,
			/* Product details */
			'itemMapId'                    => $mappedValues['id'],
			'itemMapTypeId'                => $mappedValues['mapTypeId'],
			'type'                         => $mappedValues['type'],
			'code'                         => $mappedValues['code'],
			'name'                         => $mappedValues['name'],
			'imageUpload'                  => isset($mappedValues['imageUpload']) ? $mappedValues['imageUpload'] : 0,
			'venueDelivery'                => isset($mappedValues['venueDelivery']) ? $mappedValues['venueDelivery'] : 0,
			'pricePerPerson'               => $booking->price_per_person,
			'menu'                         => $booking->menu,
			'guests'                       => $booking->min_guests,
			'tax'                          => $booking->tax_percentage,
			'prerequisites'                => $booking->prerequisites,
			'facts'                        => $booking->facts,
			'isVenue'                      => $booking->is_venue_booking,
			'hasCustomization'             => $booking->has_customization,
			'packagePrice'                 => $booking->package_price,
			'groupCount'                   => $booking->group_count,
			'pricePerExtraGuest'           => $booking->price_per_extra_guest,
			'typeTicketBookingId'          => $booking->type_ticket_booking_id,
			'gallery'                      => $booking->gallery,
			'typeBookingDetails'           => $booking->booking_type_details ? $booking->booking_type_details : "--",
			'bookingUnits'                 => $booking->booking_units,
			'updatedMessage'               => ucfirst($booking->updated_message),
			'updatedAt'                    => strtotime($booking->updated_at),
			'checkoutFieldsUser'           => $this->getCheckoutPageFields($booking->ticket_id, 0, $booking->id),
			'checkoutFieldsCrm'            => $this->getCheckoutPageFields($booking->ticket_id, 1, $booking->id), // Crm checkout field
		];

		$balanceAmount = $booking->advance_amount ? ($booking->booking_amount - $booking->advance_amount) : ($booking->booking_amount - $booking->token_amount);
		$data['balanceAmount'] = $balanceAmount;
		$data['balanceAmountStr'] = $balanceAmount ? $this->formatPrice($balanceAmount) : 0;

		// get checkout fields data
		$checkoutFields = $this->getCheckoutPageFields($booking->ticket_id, null, $booking->id);
		if (count($checkoutFields))
		{
			foreach ($checkoutFields as $checkoutField)
			{
				$checkoutFieldValue = $checkoutField->getCheckoutValue($booking->ticket_id, $booking->id);
				$notApplicable = $checkoutField->is_crm && strtolower($checkoutFieldValue) == "n/a" ? true : false;

				if (!$notApplicable)
				{
					$data['checkoutFields'][$checkoutField->identifier] = $checkoutFieldValue;
				}
			}
		}

		// get provider data
		$partner = $booking->provider;
		if ($partner)
		{
			$data['provider'] = [
				'id'             => $partner->id,
				'providerTypeId' => $booking->map_type_id,
				'userId'         => $partner->user_id,
				'name'           => $partner->name,
				'person'         => $partner->person,
				'phone'          => $partner->phone,
				'altPhone'       => $partner->alt_phone,
				'email'          => $partner->email,
				'location'       => $partner->area ? $partner->area->name : "",
				'altEmail'       => $partner->alt_email,
				'altEmail1'      => $partner->alt_email_1,
				'altEmail2'      => $partner->alt_email_2,
				'altEmail3'      => $partner->alt_email_3,
				'self'           => $partner
			];

			// @todo: old keys, check and remove these
			$data['venueName'] = $data['provider']['name'];
			$data['venueLocation'] = $data['provider']['location'];

			if ($booking->map_type_id == config('evibe.ticket.type.venue'))
			{
				$providerType = 'Venue';
			}
			else
			{
				$providerType = 'Planner';
			}

			$data['provider']['providerType'] = $providerType;
		}

		return $data;
	}

	public function getVendorCcAddresses($provider)
	{
		$vendorCcAddresses = [config('evibe.contact.customer.group')];
		//$vendorCcAddresses = [];
		$altEmailFields = [
			'altEmail',
			'altEmail1',
			'altEmail2',
			'altEmail3'
		];

		foreach ($altEmailFields as $field)
		{
			if (isset($provider[$field]) && $provider[$field])
			{
				array_push($vendorCcAddresses, $provider[$field]);
			}
		}

		return $vendorCcAddresses;
	}

	public function validateToken($str, $hashedStr, $ticketId)
	{
		// @todo: re-enable after upgrading login
		//if (!Hash::check($str, $hashedStr))
		//{
		//	Log::error("Second hash check failed::ticketId::" . $ticketId . "::concatenatedStr::" . $str . "::hashedStr::" . $hashedStr);
		//
		//	return false;
		//}

		return true;
	}

	public function validateUserInputAndNotifyTeam($userInput, $ticketId, $gateway, $gatewayId, $txnId = null)
	{
		$response = [
			'success'       => true,
			'ticket'        => null,
			'advanceAmount' => null
		];

		// $response['success'] = false;
		// $response['error'] = "Notice: We are not accepting bookings until 31 March 2020 as a safety measure to contain COVID-19. Please stay safe.";

		// return $response;

		// validate data
		$rules = [
			'phone'          => 'required|digits:10',
			'altPhone'       => 'sometimes|required|digits:10',
			'name'           => 'required|min:4',
			'title'          => 'required',
			'email'          => 'sometimes|required|email',
			'partyDate'      => 'sometimes|required|date|after:yesterday',
			'partyTime'      => 'sometimes|required',
			'customerSource' => 'required'
		];

		$messages = [
			'name.required'              => 'Your full name is required',
			'name.min'                   => 'Your full name cannot be less than 4 characters',
			'email.required'             => 'Your email id is required',
			'email.email'                => 'Your email id should be in the form of example@abc.com',
			'phone.required'             => 'Your phone number is required',
			'phone.digits'               => 'Phone number is not valid. Enter only 10 digits number (ex: 9640204000)',
			'altPhone.required'          => 'Your alternate phone number is required',
			'altPhone.digits'            => 'Alternate phone number is not valid. Enter only 10 digits number (ex: 9640204000)',
			'partyDate.required'         => 'Please enter your party date',
			'partyDate.after'            => 'Please enter a party date from today',
			'partyTime.required'         => 'Party time in Party Info screen is required',
			'partyTime'                  => 'Please enter your party time',
			'venueLandmark.required'     => 'Please enter venue landmark',
			'venueLandmark.min'          => 'Please enter valid venue landmark',
			'acceptsTerms.accepted'      => 'Please read and accept our terms of booking',
			'venueAddressLine1.required' => 'Venue address line 1 is required',
			'venueAddressLine1.min'      => 'Venue address line 1 should be minimum 5 characters long',
			'venueZipCode.required'      => 'Please enter venue PIN code',
			'venueZipCode.digits'        => 'Please enter a valid 6 digit pin code (ex: 560001)',
			'venueLocation.required'     => 'Please enter venue location / area name',
			'venueTypeId.required'       => 'Please select your venue type',
			'venueTypeId.min'            => 'Please select your venue type',
			'title.required'             => 'Please select your title (Mr. / Mrs.)',
			'customerSource.required'    => 'Kindly provide How did you find Evibe.in?'
		];

		// dynamic checkout fields based related to customer data
		$checkoutCustomerDataFields = $this->getCustomerDataFields($ticketId);

		foreach ($checkoutCustomerDataFields as $checkoutField)
		{
			if ($checkoutField->type_field_id == config('evibe.input.date'))
			{
				// no validation required for date
				continue;
			}

			$rules[$checkoutField->name] = 'required';
			$messages[$checkoutField->name . '.required'] = "$checkoutField->identifier is required. (enter n/a if not applicable)";
			if ($checkoutField->type_field_id == config('evibe.input.checkbox'))
			{
				// @see: options are stored as text separated by commas
				$messages[$checkoutField->name . '.required'] = "$checkoutField->identifier are required. Kindly select at least one choice.";
			}
			elseif ($checkoutField->type_field_id == config('evibe.input.option'))
			{
				$messages[$checkoutField->name . '.required'] = "Kindly provide your choice for $checkoutField->identifier. Select N/A is not applicable";
			}
		}

		// validate venue info (last card)
		$rules = array_merge($rules, [
			'venueAddressLine1' => 'sometimes|required|min:5',
			'venueLocation'     => 'sometimes|required',
			'venueLandmark'     => 'sometimes|required|min:5',
			'venueZipCode'      => 'sometimes|required|digits:6',
			'venueTypeId'       => 'sometimes|required|integer|min:1',
			'acceptsTerms'      => 'accepted'
		]);

		$validation = Validator::make($userInput, $rules, $messages);

		if ($validation->fails())
		{
			$response['success'] = false;
			$response['error'] = $validation->messages()->first();

			return $response;
		}

		// dynamic checkout fields based on `ticket_booking` values
		$checkoutFieldValues = (isset($userInput['checkoutFieldValues']) && $userInput['checkoutFieldValues']) ? $userInput['checkoutFieldValues'] : null;
		if (isset($checkoutFieldValues) && $checkoutFieldValues)
		{
			$checkoutPageFields = $this->getCheckoutPageFields($ticketId, 0);

			$checkoutFieldRules = [];
			$checkoutFieldMessages = [];
			foreach ($checkoutPageFields as $checkoutField)
			{
				$checkoutFieldRules[$checkoutField->booking_checkout_unique_id] = 'required';
				$checkoutFieldMessages[$checkoutField->booking_checkout_unique_id . '.required'] = "$checkoutField->identifier is required. (enter n/a if not applicable)";
				if ($checkoutField->type_field_id == config('evibe.input.checkbox'))
				{
					// @see: options are stored as text separated by commas
					$checkoutFieldMessages[$checkoutField->booking_checkout_unique_id . '.required'] = "$checkoutField->identifier are required. Kindly select at least one choice.";
				}
			}

			$validation = Validator::make($userInput['checkoutFieldValues'], $checkoutFieldRules, $checkoutFieldMessages);

			if ($validation->fails())
			{
				$response['success'] = false;
				$response['error'] = $validation->messages()->first();

				return $response;
			}
		}

		// validate images
		$uploadImage = isset($userInput['uploadImage']) ? $userInput['uploadImage'] : 0;
		$uploadSuccess = isset($userInput['uploadSuccess']) ? $userInput['uploadSuccess'] : 0;
		if (isset($uploadImage) && $uploadImage == 1)
		{
			if (isset($uploadSuccess) && $uploadSuccess == 0)
			{
				$response['success'] = false;
				$response['error'] = 'Kindly upload the image/s';

				return $response;
			}
		}

		$ticket = Ticket::with('bookings')
		                ->find($ticketId);

		if (!$ticket)
		{
			Log::error("PAY INIT FAILED, TICKET ID: " . $ticketId, ", GATEWAY: " . $gateway);

			$response['success'] = false;
			$response['error'] = "Network Error: Please check your network connection and try again.";
		}

		// check with DB amount and amount from html page
		$advanceAmountFromUser = $userInput['advanceAmount'];
		$ticketBookings = TicketBooking::where('ticket_id', $ticketId)
		                               ->where('is_advance_paid', config('evibe.ticket.advance.unpaid'))
		                               ->whereNull('deleted_at')
		                               ->get();

		$response['ticket'] = $ticket;

		// update party date time for booking for auto bookings (in some AB, 'party_time' is collected here)
		// in order to avoid AB that use 'slots'
		if ($ticket->is_auto_booked && isset($userInput['partyTime']))
		{
			$partyDate = $userInput['partyDate'];
			$partyTime = $userInput['partyTime'];

			$partyDateTime = strtotime("$partyDate $partyTime");
			$trimPartyTime = strtotime($partyTime) - strtotime('today midnight');
			$calcPartyDateTime = strtotime($partyDate) + $trimPartyTime;

			if ($calcPartyDateTime < time())
			{
				Log::error("Tried to book party before 'now' with id $ticketId");

				$response['success'] = false;
				$response['error'] = "Please select a party time after now";

				return $response;
			}

			$ticket->event_date = $calcPartyDateTime;

			$ticketBooking = TicketBooking::where('ticket_id', $ticketId);
			if (!$ticketBooking)
			{
				Log::error("Ticket Booking not found for ticket with id $ticketId");

				$response['success'] = false;
				$response['error'] = "Ticket booking not found";

				return $response;
			}

			$ticketBooking->update([
				                       'party_date_time' => $partyDateTime,
				                       //'check_in'        => $partyTime,
			                       ]);
		}

		// @see: if someone knowingly removes calling code, leave it
		//$oldCallingCode = $ticket->calling_code;

		$ticket->name = $userInput['name'];
		$ticket->phone = $userInput['phone'];
		$ticket->calling_code = isset($userInput['callingCode']) ? $userInput['callingCode'] : null;
		$ticket->alt_phone = $userInput['altPhone'];
		$ticket->spl_notes = isset($userInput['splNotes']) ? $userInput['splNotes'] : "";
		$ticket->order_details_submitted_at = Carbon::now();
		$ticket->updated_at = Carbon::now();
		$ticket->customer_gender = $userInput['title'];
		$ticket->source_id = $userInput['customerSource'] > 0 ? $userInput['customerSource'] : null;

		// e-invite and e-try card
		$ticket->e_invite = (isset($userInput['eInvite']) && ($userInput['eInvite'] == 'true')) ? 1 : 0;
		$ticket->e_thank_you_card = (isset($userInput['eTYCard']) && ($userInput['eTYCard'] == 'true')) ? 1 : 0;

		// if Auto Booked then only save the email address
		if ($ticket->is_auto_booked)
		{
			$ticket->email = $userInput['email'];
		}

		// update venue details only for ticket with non-venue bookings
		$ticketVenueBookings = $ticket->bookings()
		                              ->where('ticket_bookings.is_venue_booking', 1)
		                              ->get();

		if ($ticketVenueBookings->count() == 0)
		{
			// get the area details or create a new area with or without pinCode
			$areaData = $this->getAreaForTheLocation($userInput['venueLocation'], $userInput['locationDetails'], $userInput['venueZipCode']);
			$areaId = $areaData['areaId'];
			$areaName = $areaData['areaName'];
			//if (isset($areaData['alertPinCodeArea']) && $areaData['alertPinCodeArea'])
			//{
			//	// alert team regarding this
			//	$this->dispatch(new MailLocationVerificationAlertToTeam([
			//		                                                        'ticketId'      => $ticket->id,
			//		                                                        'name'          => $ticket->name,
			//		                                                        'phone'         => $ticket->phone,
			//		                                                        'email'         => $ticket->email,
			//		                                                        'partyLocation' => $areaData['areaName'],
			//		                                                        'pinCode'       => $userInput['venueZipCode'],
			//		                                                        'dashLink'      => config('evibe.dash_host') . '/tickets/' . $ticket->id
			//	                                                        ]));
			//}

			// validate party delivery pin code
			if ($ticket->city_id && $userInput['venueZipCode'])
			{
				$typeArea = Area::where('zip_code', $userInput['venueZipCode'])->first();
				if ($typeArea && $typeArea->city_id && ($typeArea->city_id != $ticket->city_id))
				{
					$productCity = $this->getCityNameFromId($ticket->city_id);
					$pinCodeCity = $this->getCityNameFromId($typeArea->city_id);

					return response()->json([
						                        'success'  => false,
						                        'errorMsg' => "The service belongs to " . $productCity . " and cannot be availed for " . $pinCodeCity . ". Kindly change the party location pin code"
					                        ]);
				}
			}

			$venueAddressLine1 = $userInput['venueAddressLine1'];
			$venueAddressLine2 = $userInput['venueAddressLine2'] ? $userInput['venueAddressLine2'] : "";
			$venueLocation = $areaName;
			$venueZipCode = $userInput['venueZipCode'];
			$venueAddress = "$venueAddressLine1, $venueAddressLine2, $venueLocation, Pin Code: $venueZipCode";
			$lat = $userInput['lat'];
			$lng = $userInput['lng'];

			$ticket->venue_landmark = $userInput['venueLandmark'];
			$ticket->address_line1 = $venueAddressLine1;
			$ticket->address_line2 = $venueAddressLine2;
			$ticket->venue_address = $venueAddress;
			$ticket->venue_location = $venueLocation;
			$ticket->zip_code = $venueZipCode;
			$ticket->lat = $lat;
			$ticket->lng = $lng;
			$ticket->map_address = $userInput['mapAddress'];
			$ticket->map_link = $this->getShortenUrl("http://maps.google.com/maps?q=$lat,$lng");
			$ticket->type_venue_id = array_key_exists('venueTypeId', $userInput) ? $userInput['venueTypeId'] : null;

			if ($areaId)
			{
				$ticket->area_id = $areaId;
			}

			if (!$ticket->is_auto_booked)
			{
				$preDefinedAreaId = isset($userInput['preDefinedAreaId']) ? $userInput['preDefinedAreaId'] : -1;
				if ($areaId && $preDefinedAreaId != $areaId)
				{
					$preDefinedArea = "Unknown";
					if ($preDefinedAreaId != -1)
					{
						$preDefinedArea = Area::find($preDefinedAreaId);
						$preDefinedArea = $preDefinedArea ? $preDefinedArea->name : "Not Found";
					}

					$mailData = [
						'enquiryId'        => $ticket->enquiry_id,
						'phone'            => $userInput['phone'],
						'name'             => $userInput['name'],
						'preDefinedArea'   => $preDefinedArea,
						'preDefinedAreaId' => $preDefinedAreaId,
						'modifiedArea'     => $venueLocation,
						'modifiedAreaId'   => $areaId,
						'ticketId'         => $ticket->id
					];

					$this->dispatch(new MailLocationMismatchAlertToTeamJob($mailData));
				}
			}
		}

		// save the checkout customer data values.
		$checkoutCustomerDataFields = $this->getCustomerDataFields($ticketId);
		if (count($checkoutCustomerDataFields))
		{
			foreach ($checkoutCustomerDataFields as $checkoutField)
			{
				$checkoutValue = CheckoutFieldValue::where(['ticket_id' => $ticketId, 'checkout_field_id' => $checkoutField->id])->first();
				if ($checkoutValue)
				{
					$checkoutValue->update(['value' => $userInput[$checkoutField->name]]);
				}
				else
				{
					CheckoutFieldValue::create([
						                           'ticket_id'         => $ticketId,
						                           'checkout_field_id' => $checkoutField->id,
						                           'value'             => $userInput[$checkoutField->name]
					                           ]);
				}
			}
		}

		// save the checkout field values.
		if (isset($checkoutFieldValues) && $checkoutFieldValues)
		{
			$checkoutFields = $this->getCheckoutPageFields($ticketId, 0);
			if (count($checkoutFields))
			{
				foreach ($checkoutFields as $checkoutField)
				{
					$checkoutValue = CheckoutFieldValue::where(['ticket_id' => $ticketId, 'ticket_booking_id' => $checkoutField->ticket_booking_id, 'checkout_field_id' => $checkoutField->id])->first();
					if ($checkoutValue)
					{
						$checkoutValue->update(['value' => $checkoutFieldValues[$checkoutField->booking_checkout_unique_id]]);
					}
					else
					{
						CheckoutFieldValue::create([
							                           'ticket_id'         => $ticketId,
							                           'ticket_booking_id' => $checkoutField->ticket_booking_id,
							                           'checkout_field_id' => $checkoutField->id,
							                           'value'             => $checkoutFieldValues[$checkoutField->booking_checkout_unique_id]
						                           ]);
					}
				}
			}
		}

		// save ticket with all new data
		$ticket->save();

		// decor finish time in order details
		foreach ($ticketBookings as $ticketBooking)
		{
			$ticketMapping = $ticketBooking->mapping;
			if ($ticketBooking->type_ticket_booking_id && $ticketMapping && ($ticketMapping->map_type_id == config('evibe.ticket.type.decor')))
			{
				// decor finish time in order details
				$decorFinishTimeCheckoutField = config('evibe.checkout_field.decor.delivery.' . $ticketBooking->type_ticket_booking_id);
				if ($decorFinishTimeCheckoutField)
				{
					$existingCheckoutField = CheckoutFieldValue::where('ticket_id', $ticketBooking->ticket_id)
					                                           ->where('ticket_booking_id', $ticketBooking->id)
					                                           ->where('checkout_field_id', $decorFinishTimeCheckoutField)
					                                           ->first();

					$decorFinishTime = date('h:i A', $ticket->event_date);

					if ($existingCheckoutField)
					{
						$existingCheckoutField->value = $decorFinishTime;
						$existingCheckoutField->updated_at = Carbon::now();
						$existingCheckoutField->save();
					}
					else
					{
						CheckoutFieldValue::create([
							                           'ticket_id'         => $ticketBooking->ticket_id,
							                           'ticket_booking_id' => $ticketBooking->id,
							                           'checkout_field_id' => $decorFinishTimeCheckoutField,
							                           'value'             => $decorFinishTime
						                           ]);
					}
				}
			}
		}

		// Save special notes specific to each booking
		$specialNotes = $ticket->spl_notes;
		if (!is_null($specialNotes) && ($specialNotes != ""))
		{
			$eachBookingSpecialNotes = explode("</span>", $specialNotes);
			$ticketBookingsWithSplNotes = $ticketBookings;

			foreach ($ticketBookingsWithSplNotes as $ticketBooking)
			{
				foreach ($eachBookingSpecialNotes as $eachBookingSpecialNote)
				{
					if (strpos($eachBookingSpecialNote, $ticketBooking->booking_id) && (strpos($eachBookingSpecialNote, $ticketBooking->booking_id) > 0))
					{
						// Removing title & booking Id for partners
						$splNotes = explode("<ul>", $eachBookingSpecialNote);
						$ticketBooking->spl_notes = isset($splNotes[1]) ? "<ul>" . $splNotes[1] . "</span>" : null;
						$ticketBooking->save();
					}
				}
			}
		}

		$minBookTime = $ticketBookings->min('party_date_time');
		$maxBookTime = $ticketBookings->max('party_date_time');
		$partyTime = getPartyDateTimeByBookingsPartyTime($minBookTime, $maxBookTime);

		// if booking with the nearest party date time is less that or equal to time of payment, do not allow customer to make payment
		// mail issue to CRM team
		if ($minBookTime <= time())
		{
			$response['success'] = false;
			$response['error'] = "Party date and time should be after current time";

			$emailData['dashLink'] = config('evibe.dash_host') . '/tickets/' . $ticket->id . '/information';
			$emailData['ticketId'] = $ticket->id;

			$this->dispatch(new BookingPartyDateTimeAlertToTeam($emailData));

			return $response;
		}

		$response['advanceAmount'] = $advanceAmountFromUser;

		// check and apply coupon
		if ($userInput['couponCode'] && !is_null($userInput['couponCode']))
		{
			if ($userInput['isVenueDeal'] == 1)
			{
				$totalBookingAmount = $ticketBookings->sum('token_amount');
			}
			else
			{
				$totalBookingAmount = $ticketBookings->sum('booking_amount');
			}

			try
			{
				$couponController = app()->make("CouponController");
				$couponStatus = $couponController->checkStatusOfCoupon($ticket, $ticketBookings, $totalBookingAmount, true);

				if ($couponStatus["success"] == "yes")
				{
					// adjust advance amount based on discount amount
					//$response['advanceAmount'] = $response['advanceAmount'] - $couponStatus["discountAmount"];
					$discountForEachBooking = $couponStatus['discountForEachBooking'];

					// Update the discount price before payment
					foreach ($ticketBookings as $booking)
					{
						$booking->prepay_discount_amount = isset($discountForEachBooking[$booking->id]) ? $discountForEachBooking[$booking->id] : null;
						$booking->prepay_coupon_id = !is_null($couponStatus["couponId"]) ? $couponStatus["couponId"] : null;
						$booking->save();
					}
				}
				else
				{
					Log::error("PAY INIT FAILED, TICKET ID: " . $ticketId, ", GATEWAY: " . $gateway);

					$response['success'] = false;
					$response['error'] = "Network Error: Please check your network connection and try again.";
				}
			} catch (\Exception $exception)
			{
				$siteLogData = new SiteLogData();
				$siteLogData->setUrl("/coupon/validate")
				            ->setException("Coupon status check failed")
				            ->setErrorCode($exception->getCode());

				$this->logSiteError($siteLogData);

				Log::error("PAY INIT FAILED, TICKET ID: " . $ticketId, ", GATEWAY: " . $gateway);

				$response['success'] = false;
				$response['error'] = "Network Error: Please check your network connection and try again.";
			}
		}
		else
		{
			// Empty columns of prepay coupon code so that It will not created any problems in future calculations

			// Update the discount price before payment
			foreach ($ticketBookings as $booking)
			{
				$booking->prepay_discount_amount = null;
				$booking->prepay_coupon_id = null;
				$booking->save();
			}
		}

		// getting amount to be paid from db
		if ($userInput['isVenueDeal'] == 1)
		{
			$advanceAmountFromDB = $ticketBookings->sum('token_amount');
		}
		else
		{
			$advanceAmountFromDB = $ticketBookings->sum('advance_amount');
		}

		$couponDiscountAmount = $ticketBookings->sum('prepay_discount_amount');

		// If user selected International payments we should add the additional charges
		// @see: logic for rounding the number in pay.js should be same as the logic implemented here
		// Adding 3.5% as international payment extra charges
		$internetHandlingFee = 0;
		if ($userInput['internationalPayment'] && ($userInput['internationalPayment'] == 1))
		{
			$internetHandlingFee = round($advanceAmountFromDB * 0.035);
			$ticket->update([
				                "internet_handling_fee" => $internetHandlingFee
			                ]);
		}
		else
		{
			$ticket->update([
				                "internet_handling_fee" => 0
			                ]);
		}

		// If user selected wallet payment
		// @see: logic for rounding the number in pay.js should be same as the logic implemented here
		// Adding max wallet discount amount from config
		$walletDiscount = 0;
		$loginUser = Auth::user();
		if ($loginUser)
		{
			if ($userInput['walletDiscount'] && ($userInput['walletDiscount'] == 1))
			{
				$wallet = Wallet::where("user_id", $loginUser->id)
				                ->first();

				if ($wallet && $wallet->balance >= config("wallet.default.amount"))
				{
					$walletDiscount = config("wallet.default.amount");
					$walletRedeem = WalletRedeemed::where("ticket_id", $ticket->id)
					                              ->first();

					if ($walletRedeem)
					{
						$walletRedeem->update([
							                      "wallet_id" => $wallet->id,
							                      "amount"    => $walletDiscount
						                      ]);
					}
					else
					{
						WalletRedeemed::create([
							                       "ticket_id" => $ticketId,
							                       "wallet_id" => $wallet->id,
							                       "amount"    => $walletDiscount
						                       ]);
					}
				}
			}
			else
			{
				$walletRedeem = WalletRedeemed::where("ticket_id", $ticket->id)
				                              ->first();

				if ($walletRedeem)
				{
					$walletRedeem->update([
						                      "deleted_at" => Carbon::now()
					                      ]);
				}
			}
		}
		else
		{
			$walletRedeem = WalletRedeemed::where("ticket_id", $ticket->id)
			                              ->get();

			if ($walletRedeem->count() > 0)
			{
				foreach ($walletRedeem as $redeem)
				{
					$redeem->update([
						                "deleted_at" => Carbon::now()
					                ]);
				}
			}
		}

		if ($advanceAmountFromDB != ($advanceAmountFromUser + $couponDiscountAmount + $walletDiscount - $internetHandlingFee))
		{
			$checkoutPageCalculatedAmount = $advanceAmountFromUser + $couponDiscountAmount + $walletDiscount - $internetHandlingFee;

			$reportData = [
				"ticketId" => $ticketId,
				"issue"    => "Advance amount modified from $checkoutPageCalculatedAmount to $advanceAmountFromUser"
			];

			//$this->reportAdmin($reportData);
			$this->sendNonExceptionErrorReportToTeam([
				                                         'code'      => config('evibe.error_code.hack'),
				                                         'url'       => request()->fullUrl(),
				                                         'method'    => request()->method(),
				                                         'message'   => '[BasePaymentController.php - ' . __LINE__ . '] ' . 'Advance amount to be paid from user is not matching with advance amount from DB.',
				                                         'exception' => '[BasePaymentController.php - ' . __LINE__ . '] ' . 'Advance amount to be paid from user is not matching with advance amount from DB.',
				                                         'trace'     => '[Ticket Id: ' . $ticketId . '] [Advance amount from User: ' . $checkoutPageCalculatedAmount . '] [Coupon discount: ' . $couponDiscountAmount . '] [Advance amount from DB: ' . $advanceAmountFromDB . ']',
				                                         'details'   => '[Ticket Id: ' . $ticketId . '] [Advance amount from User: ' . $checkoutPageCalculatedAmount . '] [Coupon discount: ' . $couponDiscountAmount . '] [Advance amount from DB: ' . $advanceAmountFromDB . ']'
			                                         ]);

			// If any mismatch happens with amount shown on checkout page vs calculated amount
			// Reason1: If decimals conversion is not tallied between php & JS
			// Reason2: If User tampered html & modified the amount
			// Solution: Remove all the discounts applied & add international charges calculated
			$response['advanceAmount'] = $advanceAmountFromDB + $internetHandlingFee - $couponDiscountAmount - $walletDiscount;
		}

		// Save user info in DB
		$this->dispatch(new PaymentUserInfoUpdate([
			                                          "ticketId"         => $ticket->id,
			                                          "url"              => isset($_SERVER) && isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "",
			                                          "browserInfo"      => isset($_SERVER) && isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "",
			                                          "ipAddress"        => isset($_SERVER) && isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "",
			                                          "paymentStartTime" => time(),
			                                          "paymentGatewayId" => $gatewayId,
			                                          "transactionId"    => $txnId
		                                          ]));

		// notify team & handler on payment init
		$sendPayInitTo = [config('evibe.contact.customer.group')];
		if ($ticket->handler)
		{
			array_push($sendPayInitTo, $ticket->handler->username);
		}
		$emailData = [
			'to'             => $sendPayInitTo,
			'name'           => $userInput['name'],
			'phone'          => $userInput['phone'],
			'date'           => $partyTime,
			'now'            => date('Y-m-d H:i:s'),
			'advanceAmount'  => $userInput['advanceAmount'],
			'gateway'        => $gateway,
			'is_auto_booked' => $ticket->is_auto_booked,
			'browserInfo'    => isset($_SERVER) && isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "",
			'previousUrl'    => isset($_SERVER) && isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "",
			'ticketId'       => $ticketId
		];

		$this->dispatch(new MailPaymentInitiatedJob($emailData));

		return $response;
	}

	public function validateProductUserInputAndNotifyTeam($userInput, $productBookingId, $gateway, $gatewayId, $txnId = null)
	{
		$response = [
			'success'       => true,
			'ticket'        => null,
			'advanceAmount' => null
		];

		// validate data
		$rules = [
			'phone' => 'required|digits:10',
			'name'  => 'required|min:4',
			'title' => 'required',
			'email' => 'sometimes|required|email',
		];

		$messages = [
			'name.required'         => 'Your full name is required',
			'name.min'              => 'Your full name cannot be less than 4 characters',
			'email.required'        => 'Your email id is required',
			'email.email'           => 'Your email id should be in the form of example@abc.com',
			'phone.required'        => 'Your phone number is required',
			'phone.digits'          => 'Phone number is not valid. Enter only 10 digits number (ex: 9640204000)',
			'landmark.required'     => 'Please enter landmark',
			'landmark.min'          => 'Please enter valid landmark',
			'acceptsTerms.accepted' => 'Please read and accept our terms of booking',
			'address.required'      => 'Address is required',
			'address.min'           => 'Address should be minimum 5 characters long',
			'zipCode.required'      => 'Please enter PIN code',
			'zipCode.digits'        => 'Please enter a valid 6 digit pin code (ex: 560001)',
			'title.required'        => 'Please select your title (Mr. / Mrs.)'
		];

		// validate venue info (last card)
		$rules = array_merge($rules, [
			'address'      => 'sometimes|required|min:5',
			'landmark'     => 'sometimes|required|min:5',
			'zipCode'      => 'sometimes|required|digits:6',
			'acceptsTerms' => 'accepted'
		]);

		$validation = Validator::make($userInput, $rules, $messages);

		if ($validation->fails())
		{
			$response['success'] = false;
			$response['error'] = $validation->messages()->first();

			return $response;
		}

		// validate pincode serviceability
		$res = $this->makePinCodeServiceabilityApiCall($userInput['zipCode']);
		if (isset($res['success']) && !$res['success'])
		{
			return response()->json($res);
		}

		// dynamic checkout fields based on `ticket_booking` values
		$additionalFields = $this->getProductBookingAdditionalFields($productBookingId);

		foreach ($additionalFields as $additionalField)
		{
			if (isset($userInput['additionalFields'][$additionalField->id]) && !$userInput['additionalFields'][$additionalField->id])
			{
				$errorMsg = "$additionalField->identifier is required. (enter n/a if not applicable)";;
				if ($additionalField->type_field_id == config('evibe.input.checkbox'))
				{
					$errorMsg = "$additionalField->identifier are required. Kindly select at least one choice.";
				}

				$response['success'] = false;
				$response['error'] = $errorMsg;

				return $response;
			}
		}

		$productBooking = ProductBooking::find($productBookingId);
		if (!$productBooking)
		{
			Log::error("PAY INIT FAILED, PRODUCT BOOKING ID: " . $productBookingId . ", GATEWAY: " . $gateway);

			$response['success'] = false;
			$response['error'] = "Network Error: Please check your network connection and try again.";
		}

		// check with DB amount and amount from html page
		$advanceAmountFromUser = $userInput['advanceAmount'];

		// @see: if someone knowingly removes calling code, leave it
		//$oldCallingCode = $ticket->calling_code;

		$name = $userInput['name'];
		$phone = $userInput['phone'];
		$email = $userInput['email'];

		$productBooking->name = $name;
		$productBooking->phone = $phone;
		$productBooking->calling_code = isset($userInput['callingCode']) ? $userInput['callingCode'] : null;
		$productBooking->updated_at = Carbon::now();
		$productBooking->customer_gender = $userInput['title'];
		$productBooking->email = $email;

		$productBooking->landmark = $userInput['landmark'];
		$productBooking->address = $userInput['address'];
		$productBooking->zip_code = $userInput['zipCode'];

		// search for user if he/she is logged in while booking
		$user = User::find($productBooking->user_id);

		// search for user using email or phone
		if (!$user)
		{
			$user = User::where('username', $email)
			            ->orWhere('phone', $phone)// @see: Is validation based only on email?
			            ->first();
		}

		// create a new user
		if (!$user)
		{
			$user = User::create([
				                     'name'       => $name,
				                     'phone'      => $phone,
				                     'username'   => $email,
				                     'role_id'    => config('evibe.roles.customer'),
				                     'created_at' => Carbon::now(),
				                     'updated_at' => Carbon::now(),
			                     ]);
		}

		// update mobile number if user doesn't have one
		// @todo: if user enters different mobile number
		if ($user)
		{
			if (is_null($user->phone) && !is_null($phone))
			{
				$user->phone = $phone;
				$user->save();
			}
		}

		$productBooking->user_id = $user->id;

		// save product booking with all new data
		$productBooking->save();

		// save additional fields
		if (isset($userInput['additionalFields']) && count($userInput['additionalFields']))
		{
			foreach ($userInput['additionalFields'] as $fieldId => $fieldValue)
			{
				ProductBookingAdditionalFieldValue::create([
					                                           'additional_field_id' => $fieldId,
					                                           'product_booking_id'  => $productBookingId,
					                                           'value'               => $fieldValue,
					                                           'created_at'          => Carbon::now(),
					                                           'updated_at'          => Carbon::now()
				                                           ]);
			}
		}

		$response['advanceAmount'] = $advanceAmountFromUser;
		$advanceAmountFromDB = $productBooking->booking_amount;

		// check and apply coupon
		if ($userInput['couponCode'] && !is_null($userInput['couponCode']))
		{
			$totalBookingAmount = $productBooking->booking_amount;

			/* @see: For now, comment coupon code */
			try
			{
				$couponController = app()->make("CouponController");
				$couponStatus = $couponController->checkStatusOfCouponForPIAB($productBooking, $totalBookingAmount, true);

				if ($couponStatus["success"] == "yes")
				{
					// adjust advance amount based on discount amount
					//$response['advanceAmount'] = $response['advanceAmount'] - $couponStatus["discountAmount"];
					$discountForProductBooking = $couponStatus['discountForEachBooking'];

					// Update the discount price before payment
					$productBooking->prepay_coupon_discount_amount = isset($discountForProductBooking[$productBooking->id]) ? $discountForProductBooking[$productBooking->id] : null;
					$productBooking->prepay_coupon_id = !is_null($couponStatus["couponId"]) ? $couponStatus["couponId"] : null;
					$productBooking->save();
				}
				else
				{
					Log::error("PAY INIT FAILED, PRODUCT BOOKING ID: " . $productBookingId . ", GATEWAY: " . $gateway);

					$response['success'] = false;
					$response['error'] = "Network Error: Please check your network connection and try again.";
				}
			} catch (\Exception $exception)
			{
				$siteLogData = new SiteLogData();
				$siteLogData->setUrl("/coupon/validate")
				            ->setException("Coupon status check failed")
				            ->setErrorCode($exception->getCode())
				            ->setTrace($exception->getMessage());

				$this->logSiteError($siteLogData);

				Log::error("PAY INIT FAILED, PRODUCT BOOKING ID: " . $productBookingId . ", GATEWAY: " . $gateway);

				$response['success'] = false;
				$response['error'] = "Network Error: Please check your network connection and try again.";
			}
		}

		$couponDiscountAmount = $productBooking->prepay_coupon_discount_amount;

		if ($advanceAmountFromDB != ($advanceAmountFromUser + $couponDiscountAmount))
		{
			$reportData = [
				"productBookingId" => $productBookingId,
				"issue"            => "Advance amount modified from $advanceAmountFromDB to $advanceAmountFromUser"
			];

			//$this->reportAdmin($reportData);
			$this->sendNonExceptionErrorReportToTeam([
				                                         'code'      => config('evibe.error_code.hack'),
				                                         'url'       => request()->fullUrl(),
				                                         'method'    => request()->method(),
				                                         'message'   => '[BasePaymentController.php - ' . __LINE__ . '] ' . 'Advance amount to be paid from user is not matching with advance amount from DB.',
				                                         'exception' => '[BasePaymentController.php - ' . __LINE__ . '] ' . 'Advance amount to be paid from user is not matching with advance amount from DB.',
				                                         'trace'     => '[Product Booking Id: ' . $productBookingId . '] [Advance amount from User: ' . $advanceAmountFromUser . '] [Coupon discount: ' . $couponDiscountAmount . '] [Advance amount from DB: ' . $advanceAmountFromDB . ']',
				                                         'details'   => '[Product Booking Id: ' . $productBookingId . '] [Advance amount from User: ' . $advanceAmountFromUser . '] [Coupon discount: ' . $couponDiscountAmount . '] [Advance amount from DB: ' . $advanceAmountFromDB . ']'
			                                         ]);
			$response['advanceAmount'] = $advanceAmountFromDB;
		}

		// Save user info in DB
		$this->dispatch(new PaymentUserInfoUpdate([
			                                          "piabId"           => $productBooking->id,
			                                          "url"              => isset($_SERVER) && isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "",
			                                          "browserInfo"      => isset($_SERVER) && isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "",
			                                          "ipAddress"        => isset($_SERVER) && isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "",
			                                          "paymentStartTime" => time(),
			                                          "paymentGatewayId" => $gatewayId,
			                                          "transactionId"    => $txnId
		                                          ]));

		$response['productBookingStringId'] = $productBooking->booking_order_id;

		return $response;
	}

	public function checkStatusBeforeSendReceipts(Ticket $ticket, $isFromDash = false)
	{
		$success = true;
		$errorMessage = "";
		$ticketId = $ticket->id;
		$ticketBookings = $ticket->bookings()
		                         ->where('is_advance_paid', config('evibe.ticket.advance.paid'))
		                         ->get();

		// disable sending multiple receipts
		if (!$ticket)
		{
			$errorMessage = "SEND RECEIPT :: NO TICKET FOUND :: ID $ticketId";
			$success = false;
		}

		if (!$isFromDash && $ticket->status_id == config('evibe.ticket.status.booked'))
		{
			$success = false;
			$errorMessage = "SEND RECEIPT :: TRYING TO RESEND RECEIPT FOR :: ID $ticketId";
		}

		if (!$ticketBookings->count())
		{
			$success = false;
			$errorMessage = "HACKED :: TRYING TO SEND RECEIPT BEING SENT WITHOUT MAKING PAYMENT :: ID $ticketId";
		}

		$status = new PreSendReceiptStatus();
		$status->setBookings($ticketBookings);
		$status->setErrorMessage($errorMessage);
		$status->setSuccess($success);

		return $status;
	}

	public function checkPaymentTransactionCount($ticketId)
	{
		$retryCount = 0;
		$ticket = Ticket::find($ticketId);

		$paymentTransactionTickets = PaymentTransactionAttempts::whereNull("piab_id")
		                                                       ->whereNotNull("ticket_id")
		                                                       ->where('ticket_id', $ticketId)
		                                                       ->get();

		if (($paymentTransactionTickets->count() > 0) && $ticket)
		{
			$retryCount = $paymentTransactionTickets->count();
		}

		return response()->json([
			                        "success"    => true,
			                        "retryCount" => $retryCount
		                        ]);
	}

	public function updateGoogleClientID()
	{
		$ticketId = request("ticketId");
		$googleClientId = request("googleClientId");

		if (!is_null($ticketId) && !is_null($googleClientId))
		{
			$ticket = Ticket::find($ticketId);
			if ($ticket)
			{
				$ticketGoogleAnalyticsData = TicketGoogleAnalyticsData::where("ticket_id", $ticketId)
				                                                      ->where("google_client_id", $googleClientId)
				                                                      ->get()
				                                                      ->first();

				if (!$ticketGoogleAnalyticsData)
				{
					TicketGoogleAnalyticsData::create([
						                                  "google_client_id" => $googleClientId,
						                                  "ticket_id"        => $ticketId
					                                  ]);
				}
			}
		}

		return response()->json([
			                        "success" => true
		                        ]);
	}

	protected function updateTicketBookingsWithPaymentReference($paymentReference, $ticket, $typePaymentGatewayId)
	{
		// mark bookings as advance paid with payment reference
		foreach ($ticket->bookings as $booking)
		{
			if (!$booking->is_advance_paid)
			{
				$booking->update([
					                 'is_advance_paid'         => config('evibe.ticket.advance.paid'),
					                 'payment_reference'       => $paymentReference,
					                 'type_payment_gateway_id' => $typePaymentGatewayId,
					                 'updated_at'              => date('Y-m-d H:i:s'),
					                 'coupon_id'               => $booking->prepay_coupon_id,
					                 'discount_amount'         => $booking->prepay_discount_amount
				                 ]);

				if (!is_null($booking->coupon_id))
				{
					$userId = Auth::user() ? Auth::user()->id : null;

					$this->processPostCouponRedeem($booking, $userId);
				}
			}
		}

		$this->processPostWalletRedeem($ticket);
	}

	protected function updateProductBookingsWithPaymentReference($paymentReference, $productBooking, $typePaymentGatewayId)
	{
		$productBooking->update([
			                        'payment_reference'       => $paymentReference,
			                        'type_payment_gateway_id' => $typePaymentGatewayId,
			                        'updated_at'              => date('Y-m-d H:i:s'),
			                        'coupon_id'               => $productBooking->prepay_coupon_id,
			                        'coupon_discount_amount'  => $productBooking->prepay_coupon_discount_amount
		                        ]);

		if (!is_null($productBooking->coupon_id))
		{
			$userId = Auth::user() ? Auth::user()->id : null;

			// Adding count for the coupon table
			$couponRedeemed = CouponRedeemedPIAB::create([
				                                             'coupon_id'          => $productBooking->coupon_id,
				                                             'product_booking_id' => $productBooking->id,
				                                             'user_id'            => $userId
			                                             ]);

			if (!$couponRedeemed)
			{
				$this->sendNormalErrorReport("evb-coupon", "Coupon redeemed creation error", "Coupon redeemed not created due to some error, Please go through it.");
			}

			$totalCouponUsageCount = CouponRedeemedPIAB::where('coupon_id', $productBooking->coupon_id)
			                                           ->count();

			$coupon = Coupon::find($productBooking->coupon_id);

			if ($coupon)
			{
				if (!is_null($coupon->max_usage_count) &&
					$coupon->max_usage_count <= $totalCouponUsageCount
				)
				{
					$coupon->usage_done_at = Carbon::now();
					$coupon->save();
				}
			}
			else
			{
				Log::error("No Coupon found :: But applied in ticket booking");
			}
		}
	}

	private function processPostCouponRedeem($booking, $userId)
	{
		// Adding count for the coupon table
		$couponRedeemed = CouponRedeemed::create([
			                                         'coupon_id'         => $booking->coupon_id,
			                                         'ticket_booking_id' => $booking->id,
			                                         'ticket_id'         => $booking->ticket_id,
			                                         'user_id'           => $userId,
			                                         'partner_type_id'   => $booking->map_type_id,
			                                         'partner_id'        => $booking->map_id
		                                         ]);

		if (!$couponRedeemed)
		{
			$this->sendNormalErrorReport("evb-coupon", "Coupon redeemed creation error", "Coupon redeemed not created due to some error, Please go through it.");
		}

		$totalCouponUsageCount = CouponRedeemed::where('coupon_id', $booking->coupon_id)
		                                       ->count();

		$coupon = Coupon::find($booking->coupon_id);

		if ($coupon)
		{
			if (!is_null($coupon->max_usage_count) &&
				$coupon->max_usage_count <= $totalCouponUsageCount
			)
			{
				$coupon->usage_done_at = Carbon::now();
				$coupon->save();
			}
		}
		else
		{
			Log::error("No Coupon found :: But applied in ticket booking");
		}
	}

	private function processPostWalletRedeem($ticket)
	{
		$walletRedeem = WalletRedeemed::where("ticket_id", $ticket->id)
		                              ->first();

		if ($walletRedeem)
		{
			$walletRedeem->update([
				                      "is_redeemed" => 1
			                      ]);

			Transaction::create([
				                    "wallet_id"        => $walletRedeem->wallet_id,
				                    "transaction_name" => "Paid to Evibe",
				                    "type"             => "withdraw",
				                    "amount"           => $walletRedeem->amount,
				                    "confirmed"        => 1,
				                    "uuid"             => rand(11111, 99999) . "-" . rand(11, 99) . "-" . time()
			                    ]);

			$wallet = Wallet::find($walletRedeem->wallet_id);

			$wallet->update([
				                "balance" => $wallet->balance - $walletRedeem->amount >= 0 ? $wallet->balance - $walletRedeem->amount : 0
			                ]);
		}
	}

	public function updateTicketPostSendReceipts(Ticket $ticket, PaySuccessUpdateTicket $data)
	{
		$ticketId = $ticket->id;
		$isFromDash = $data->getIsFromDash();
		$statusId = $data->getStatusId();
		$handlerId = $data->getHandlerId();
		$successEmailType = $data->getSuccessEmailType();
		$success = false;

		// update timestamp
		$updateTicketData = [
			'status_id'  => $statusId,
			'updated_at' => date('Y-m-d H:i:s')
		];

		// if already booked, do not update these
		if (!$ticket->isBooked())
		{
			$updateTicketData['success_email_sent_at'] = date('Y-m-d H:i:s');
			$updateTicketData['success_email_type'] = $successEmailType;
		}

		// this should be updated only once
		// in case of auto booking, the current method will be called for pay success & accept
		if (!$ticket->isBooked() && !$ticket->isAutoPaid())
		{
			$updateTicketData['paid_at'] = time();
		}

		// updated receipt being sent
		if ($ticket->isBooked() && $isFromDash)
		{
			$updateTicketData['updated_receipt_sent_at'] = Carbon::now();
		}

		$ticket->update($updateTicketData);

		if ($ticket->is_auto_booked && !($ticket->area_id))
		{
			// alert team regarding party location
			$this->dispatch(new UpdatePartyLocationAlertToTeamJob([
				                                                      'ticketId'      => $ticket->id,
				                                                      'name'          => $ticket->name,
				                                                      'phone'         => $ticket->phone,
				                                                      'email'         => $ticket->email,
				                                                      'partyLocation' => $ticket->venue_location,
				                                                      'pinCode'       => $ticket->zip_code,
				                                                      'dashLink'      => config('evibe.dash_host') . '/tickets/' . $ticket->id
			                                                      ]));
		}

		// save ticket action
		$comments = $isFromDash ? config('evibe.ticket.status_message.receipt_manual') : config('evibe.ticket.status_message.receipt_automatic');
		$ticketUpdate = [
			'ticket_id'   => $ticketId,
			'status_id'   => $statusId,
			'type_update' => config('evibe.ticket.type_update.auto'),
			'comments'    => $comments,
			'status_at'   => time()
		];

		dispatch(new CheckReferralCouponCode($ticket->email));

		// Creating user in case of manual payment update
		if ($isFromDash)
		{
			$user = User::where('username', $ticket->email)->first();
			if ($user)
			{
				$user->update([
					              "name"     => $ticket->name,
					              "phone"    => $ticket->phone,
					              "password" => is_null($user->password) ? Hash::make("$" . $ticket->phone) : $user->password,
					              "role_id"  => is_null($user->role_id) ? config("evibe.roles.customer") : $user->role_id
				              ]);
			}
			else
			{
				$user = User::create([
					                     "username"   => $ticket->email,
					                     "name"       => $ticket->name,
					                     "phone"      => $ticket->phone,
					                     "password"   => Hash::make("$" . $ticket->phone),
					                     "role_id"    => config("evibe.roles.customer"),
					                     "created_at" => Carbon::now(),
					                     "updated_at" => Carbon::now()
				                     ]);
			}
		}

		if ($isFromDash && $handlerId)
		{
			$ticketUpdate['handler_id'] = $handlerId;
		}

		$newUpdate = TicketUpdate::create($ticketUpdate);
		if ($newUpdate)
		{
			$success = true;
		}

		return $success;
	}

	public function fillMappingValues($options)
	{
		$mappers = [];
		$hasProvider = false;
		$mapTypeId = $options['mapTypeId'];
		$typeId = $options['mapTypeId'];
		$mapId = array_key_exists('mapId', $options) ? $options['mapId'] : false;
		$mapType = "";

		/**
		 * @see whereNull is required as using $someVariable = Vendor; results
		 * @see `mapTypeId` is matched against `type_ticket`  in "evibe.php" config file
		 *      which is in sync with database table type_ticket
		 */
		$selectableVendors = Vendor::withTrashed();

		switch ($mapTypeId)
		{
			// packages
			case config('evibe.ticket.type.package'):
			case config('evibe.ticket.type.food'):
			case config('evibe.ticket.type.villas'):
			case config('evibe.ticket.type.resorts'):
			case config('evibe.ticket.type.priests'):
			case config('evibe.ticket.type.lounges'):
			case config('evibe.ticket.type.tents'):
			case config('evibe.ticket.type.surprises'):
			case config('evibe.ticket.type.generic-package'):
			case config('evibe.ticket.type.venue-deals'):
				$mappers = $mapId ? Package::withTrashed()->with('provider')->find($mapId) : Package::where('is_live', 1)->get();
				$hasProvider = true;
				$mapType = "packages";
				break;

			// planners
			case config('evibe.ticket.type.planner'):
				$mappers = $mapId ? $selectableVendors->find($mapId) : Vendor::where('is_live', 1)->get();
				$mapType = "vendors";
				break;

			// venues (aka halls)
			case config('evibe.ticket.type.venue'):
				$venueHalls = VenueHall::withTrashed()->with('venue');
				$mappers = $mapId ? $venueHalls->find($mapId) : VenueHall::with('venue')->get();
				$mapType = "venues";
				break;

			// services
			case config('evibe.ticket.type.service'):
				$mappers = $mapId ? TypeServices::withTrashed()->find($mapId) : TypeServices::all();
				$hasProvider = false;
				$mapType = "services";
				break;

			// trends
			case config('evibe.ticket.type.trend'):
				$mappers = $mapId ? Trend::withTrashed()->with('provider')->find($mapId) : Trend::all();
				$hasProvider = true;
				$mapType = "trends";
				break;

			// cakes
			case config('evibe.ticket.type.cake'):
				$mappers = $mapId ? Cake::withTrashed()->with('provider')->find($mapId) : Cake::all();
				$hasProvider = true;
				$mapType = "cakes";
				break;

			// decors
			case config('evibe.ticket.type.decor'):
				$mappers = $mapId ? Decor::withTrashed()->with('provider')->find($mapId) : Decor::where('is_live', 1)->get();
				$hasProvider = true;
				$mapType = "decors";
				break;

			// add-ons
			case config('evibe.ticket.type.add-on'):
				$mappers = $mapId ? AddOn::withTrashed()->find($mapId) : AddOn::where('is_live', 1)->get();
				$hasProvider = false;
				$mapType = "add-ons";
				break;
		}

		/**
		 * @todo generate public link based on mapType
		 * @todo venues and vendors should be separated for viewing
		 */
		$mapper = $mappers;
		$mappingValues = [
			'id'            => '',
			'code'          => '',
			'name'          => '',
			'price'         => '',
			'info'          => '',
			'worth'         => '',
			'pic'           => '',
			'deleted'       => '',
			'provider'      => [
				'name'      => '',
				'person'    => '',
				'phone'     => '',
				'altPhone'  => '',
				'email'     => '',
				'location'  => '',
				'altEmail'  => '',
				'altEmail1' => '',
				'altEmail2' => '',
				'altEmail3' => '',
				'self'      => null
			],
			'link'          => '',
			'url'           => '',
			'cityUrl'       => '',
			'publicLink'    => '',
			'type'          => '',
			'imageUpload'   => 0,
			'venueDelivery' => 0,
			'self'          => null
		];

		$packageType = [
			config('evibe.ticket.type.package'),
			config('evibe.ticket.type.food'),
			config('evibe.ticket.type.villas'),
			config('evibe.ticket.type.resorts'),
			config('evibe.ticket.type.priests'),
			config('evibe.ticket.type.lounges'),
			config('evibe.ticket.type.tents'),
			config('evibe.ticket.type.surprises'),
			config('evibe.ticket.type.generic-package'),
			config('evibe.ticket.type.venue-deals')
		];
		if (in_array($mapTypeId, $packageType))
		{
			$mapTypeId = config('evibe.ticket.type.package');
		}

		if ($mapper)
		{
			// special case for tickets raised for services and add-ons
			if ($mapTypeId == config('evibe.ticket.type.add-on'))
			{
				$mappingValues = [
					'id'            => $mapper->id,
					'name'          => $mapper->name,
					'code'          => $mapper->code,
					'price'         => $mapper->price_per_unit,
					'worth'         => $mapper->price_worth_per_unit,
					'maxUnits'      => $mapper->max_units,
					'info'          => $mapper->info,
					'imageUpload'   => $mapper->image_upload,
					'venueDelivery' => $mapper->venue_delivery,
					'deleted'       => $mapper->deleted_at,
					'type'          => $mapType,
				];
			}
			elseif ($mapTypeId == config('evibe.ticket.type.service'))
			{
				$servicePrice = 'Rs. ' . $this->formatPrice($mapper->min_price);
				$servicePrice .= $mapper->max_price ? ' - Rs. ' . $this->formatPrice($mapper->max_price) : '';

				$mappingValues = [
					'id'         => $mapper->id,
					'name'       => $mapper->name,
					'code'       => $mapper->code,
					'price'      => $servicePrice,
					'worth'      => $mapper->worth_price,
					'range_info' => $mapper->range_info,
					'info'       => $mapper->info,
					'max_price'  => $mapper->max_price,
					'deleted'    => $mapper->deleted_at,
					'type'       => $mapType,
				];
			}
			elseif ($mapTypeId == config('evibe.ticket.type.venue'))
			{
				// venues (aka halls)
				$cityUrl = $mapper->venue->city->url;

				$mappingValues = [
					'id'       => $mapper->id,
					'code'     => $mapper->code,
					'name'     => $mapper->name,
					'info'     => $mapper->venue->info,
					'tax'      => $mapper->venue->tax_percent,
					'worth'    => $mapper->getPriceWorth(),
					'price'    => $mapper->venue->price_min_veg,
					'deleted'  => $mapper->deleted_at,
					'provider' => [
						'name'      => $mapper->venue->name,
						'person'    => $mapper->venue->person,
						'phone'     => $mapper->venue->phone,
						'altPhone'  => $mapper->venue->alt_phone,
						'email'     => $mapper->venue->email,
						'location'  => $mapper->venue->area->name,
						'altEmail'  => $mapper->venue->alt_email,
						'altEmail1' => $mapper->venue->alt_email_1,
						'altEmail2' => $mapper->venue->alt_email_2,
						'altEmail3' => $mapper->venue->alt_email_3,
						'self'      => $mapper->venue
					],
					'url'      => $mapper->url,
					'cityUrl'  => $cityUrl,
					'type'     => ucfirst($mapType),
					'self'     => $mapper
				];
			}
			else
			{
				// for decors / cakes / trends / packages / planners
				$mapName = ($mapTypeId == config('evibe.ticket.type.cake')) ? $mapper->title : $mapper->name;
				$cityUrl = $hasProvider ? $mapper->provider ? $mapper->provider->city->url : 'Not Available' : $mapper->city->url;

				if ($mapTypeId == config('evibe.ticket.type.decor'))
				{
					$price = $mapper->min_price;
					$prerequisites = $mapper->more_info;
				}
				else
				{
					$price = $mapper->price;
					$prerequisites = $mapper->prerequisites;
				}

				$mappingValues = [
					'id'            => $mapper->id,
					'code'          => $mapper->code,
					'name'          => $mapName,
					'price'         => $price,
					'info'          => $mapper->info,
					'prerequisites' => $prerequisites,
					'facts'         => $mapper->facts,
					'worth'         => $mapper->getPriceWorth(),
					'deleted'       => $mapper->deleted_at,
					'provider'      => [
						'name'      => $hasProvider ? $mapper->provider ? $mapper->provider->name : '' : $mapper->name,
						'person'    => $hasProvider ? $mapper->provider ? $mapper->provider->person : '' : $mapper->person,
						'phone'     => $hasProvider ? $mapper->provider ? $mapper->provider->phone : '' : $mapper->phone,
						'altPhone'  => $hasProvider ? $mapper->provider ? $mapper->provider->alt_phone : '' : $mapper->alt_phone,
						'email'     => $hasProvider ? $mapper->provider ? $mapper->provider->email : '' : $mapper->email,
						'location'  => $hasProvider ? $mapper->provider ? $mapper->provider->area->name : '' : $mapper->area->name,
						'altEmail'  => $hasProvider ? $mapper->provider ? $mapper->provider->alt_email : '' : $mapper->alt_email,
						'altEmail1' => $hasProvider ? $mapper->provider ? $mapper->provider->alt_email_1 : '' : $mapper->alt_email_1,
						'altEmail2' => $hasProvider ? $mapper->provider ? $mapper->provider->alt_email_2 : '' : $mapper->alt_email_2,
						'altEmail3' => $hasProvider ? $mapper->provider ? $mapper->provider->alt_email_3 : '' : $mapper->alt_email_3,
						'self'      => $hasProvider ? $mapper->provider : $mapper
					],
					'url'           => $mapper->url,
					'cityUrl'       => $cityUrl,
					'type'          => ucfirst($mapType),
					'self'          => $mapper
				];

			}
		}

		/* Adding map_type_id, whether product or partner */
		$mappingValues['mapTypeId'] = $typeId;

		return $mappingValues;
	}

	public function onPaymentFailure()
	{
		$ticketId = null;
		$retryPaymentLink = null;

		// Failure transaction tracking code for payumoney
		if (request()->has("udf1") && request("udf1") > 0)
		{
			$ticketId = request("udf1");
			$ticket = Ticket::find($ticketId);
			$paymentTransaction = PaymentTransactionAttempts::where("ticket_id", $ticketId)->orderBy("id", "desc")->first();

			if ($paymentTransaction && $ticket)
			{
				$errorDetailsString = "";
				$errorDetails = ["isConsentPayment", "mihpayid", "mode", "status", "unmappedstatus", "txnid", "amount", "addedon", "productinfo", "firstname", "email", "phone", "udf1", "field9", "PG_TYPE", "bank_ref_num", "bankcode", "error", "error_Message", "payuMoneyId"];

				foreach ($errorDetails as $errorDetail)
				{
					$errorDetailsString .= "\"" . $errorDetail . "\"-\"" . request($errorDetail) . "\",";
				}

				$paymentTransaction->update([
					                            "comments" => $paymentTransaction->comments . "," . $errorDetailsString
				                            ]);

				if ($ticket->is_auto_booked == 1)
				{
					$retryPaymentLink = route("auto-book.pay.auto.checkout");
					$retryPaymentLink .= "?id=" . $ticket->id;
					$retryPaymentLink .= "&token=" . Hash::make($ticket->id);
					$retryPaymentLink .= "&uKm8=" . Hash::make($ticket->phone);
				}
				else
				{
					$retryPaymentLink = route("pay.normal.checkout");
					$retryPaymentLink .= "?id=" . $ticket->id;
					$retryPaymentLink .= "&token=" . Hash::make($ticket->id);
					$retryPaymentLink .= "&uKm8=" . Hash::make($ticket->email);
				}
				$retryPaymentLink .= "&ref=retryPaymentBtn";
			}
		}

		$data = [
			"ticketId"        => $ticketId,
			"retryPaymentUrl" => $retryPaymentLink
		];

		return view('pay/fail/fail', ["data" => $data]);
	}

	public function getTicketAndSingleBookingData($ticket, $booking)
	{
		$data = $this->getTicketData($ticket);
		$mappedValues = $this->fillMappingValues([
			                                         'isCollection' => false,
			                                         'mapId'        => $booking->mapping->map_id,
			                                         'mapTypeId'    => $booking->mapping->map_type_id
		                                         ]);
		$data['booking'] = $this->getTicketBookingData($booking, $mappedValues);

		return $data;
	}

	public function getTicketAndPartnerBookingsData($ticket, $bookings, $partnerId, $partnerTypeId)
	{
		$data = $this->getTicketData($ticket);
		$data['bookings'] = [];

		foreach ($bookings as $booking)
		{
			if ($booking->map_id == $partnerId && $booking->map_type_id == $partnerTypeId)
			{
				$mappedValues = $this->fillMappingValues([
					                                         'isCollection' => false,
					                                         'mapId'        => $booking->mapping->map_id,
					                                         'mapTypeId'    => $booking->mapping->map_type_id
				                                         ]);
				$bookingData = $this->getTicketBookingData($booking, $mappedValues);
				$data['bookings'][] = $bookingData;

				if (!isset($data['self']) && isset($bookingData['provider']))
				{
					$data = array_merge($data, $bookingData['provider']);
				}
			}
		}

		return $data;
	}

	public function formCheckoutPageData()
	{
		// todo: need to form checkout data after validation in a single place
	}

	private function reportAdmin($reportData)
	{
		// @todo: need to send email
		$issue = is_array($reportData) && array_key_exists('issue', $reportData) ? $reportData['issue'] : 'SOME ISSUE';
		Log::error('ADMIN REPORT :: ' . $issue);
	}

	private function getRouteForAutoBookedIntermediatePage($ticket)
	{
		$route = route('auto-book.generic-package.pay.auto.process', $ticket->id);

		if ($ticket->event_id == config('evibe.occasion.raksha-bandhan.id'))
		{
			return route('auto-book.campaign.generic-campaign.pay.auto.process', $ticket->id);
		}

		if ($ticket->event_id == config('evibe.occasion.independence-day.id'))
		{
			return route('auto-book.campaign.generic-campaign.pay.auto.process', $ticket->id);
		}

		if ($ticket->event_id == config('evibe.occasion.christmas.id'))
		{
			return route('auto-book.campaign.generic-campaign.pay.auto.process', $ticket->id);
		}

		if ($ticket->event_id == config('evibe.occasion.valentine-day.id'))
		{
			return route('auto-book.campaign.generic-campaign.pay.auto.process', $ticket->id);
		}

		if (isset($ticket->type_ticket_id))
		{
			try
			{
				$typeTicketId = $ticket->type_ticket_id;
				$typeTicketName = config('evibe.auto-book.' . $typeTicketId, false);

				$route = route('auto-book.' . $typeTicketName . '.pay.auto.process', $ticket->id);
			} catch (\Exception $e)
			{
				// route does not exists, use generic
				$route = route('auto-book.generic-package.pay.auto.process', $ticket->id);
			}
		}

		return $route;
	}

	protected function getCustomerDataFields($ticketId)
	{
		$ticket = Ticket::find($ticketId);
		$ticketEvent = $ticket->event_id;
		$ticketEvent = $this->getParentEventId($ticketEvent);
		$checkoutFields = CheckoutField::where('event_id', $ticketEvent)
		                               ->where('is_customer_data', 1)
		                               ->get();

		return $checkoutFields;
	}

	protected function getCheckoutPageFields($ticketId, $isCrm = null, $ticketBookingId = null)
	{
		$ticket = Ticket::find($ticketId);
		$ticketEvent = $ticket->event_id;
		$ticketEvent = $this->getParentEventId($ticketEvent);

		$ticketBookings = TicketBooking::where('ticket_id', $ticketId);
		if ($ticketBookingId)
		{
			$ticketBookings->where('id', $ticketBookingId);
		}
		$ticketBookings = $ticketBookings->get();

		$checkoutFields = null;
		$bookingCheckoutFields = null;
		foreach ($ticketBookings as $ticketBooking)
		{
			$bookingCheckoutFields = CheckoutField::select('checkout_field.*', DB::raw($ticketBooking->id . ' as ticket_booking_id'))
			                                      ->where('type_ticket_booking_id', $ticketBooking->type_ticket_booking_id)
			                                      ->where(function ($query) use ($ticketEvent) {
				                                      $query->where('event_id', $ticketEvent)
				                                            ->orWhereNull('event_id');
			                                      })
			                                      ->get();

			if (!$checkoutFields)
			{
				$checkoutFields = $bookingCheckoutFields;
			}
			elseif ($checkoutFields && $bookingCheckoutFields)
			{
				$checkoutFields = $checkoutFields->toBase()->merge($bookingCheckoutFields);
			}
		}

		/*
		$typeTicketBookingIds = TicketBooking::where('ticket_id', $ticketId);
		if ($ticketBookingId)
		{
			$typeTicketBookingIds->where('id', $ticketBookingId);
		}
		$typeTicketBookingIds = $typeTicketBookingIds->distinct()
		                                             ->pluck('type_ticket_booking_id');

		$checkoutFields = CheckoutField::whereIn('type_ticket_booking_id', $typeTicketBookingIds)
		                               ->where(function ($query) use ($ticketEvent) {
			                               $query->where('event_id', $ticketEvent)
			                                     ->orWhereNull('event_id');
		                               });
		*/

		if ($checkoutFields)
		{
			if (!is_null($isCrm))
			{
				$checkoutFields = $checkoutFields->where('is_crm', $isCrm);
			}

			if ($ticket->is_auto_booked == 1)
			{
				$checkoutFields = $checkoutFields->where('is_auto_booking', 1);
			}
			else
			{
				$checkoutFields = $checkoutFields->where('is_auto_booking', 0);
			}
		}

		if ($checkoutFields)
		{
			foreach ($checkoutFields as $key => $checkoutField)
			{
				$checkoutFields[$key]->booking_checkout_unique_id = $checkoutField->ticket_booking_id . '_' . $checkoutField->id;
			}
		}

		return $checkoutFields;
	}

	protected function getProductBookingAdditionalFields($productBookingId)
	{
		$productBooking = ProductBooking::find($productBookingId);
		if (!$productBooking)
		{
			return null;
		}

		$additionalFields = ProductBookingAdditionalField::where('product_category_id', $productBooking->product_category_id)
		                                                 ->whereNull('deleted_at')
		                                                 ->get();

		return $additionalFields;
	}

	protected function generateBookingOrderDetailPageLink($booking, $shortLink = true)
	{
		$token2 = $this->generateHashedSecretToken($booking);
		$link = route('order.partner', $booking->id) . '?t1=' . Hash::make($booking->id) . '&t2=' . $token2;
		if ($shortLink)
		{
			$link = $this->getShortenUrl($link);
		}

		return $link;
	}

	protected function generateCustomerOrderDetailsPageLink($ticket, $shortLink = true)
	{
		$token2 = $this->generateHashedSecretToken($ticket);
		$link = route('order.customer', $ticket->id) . '?t1=' . Hash::make($ticket->id) . '&t2=' . $token2;
		if ($shortLink)
		{
			$link = $this->getShortenUrl($link);
		}

		return $link;
	}

	// for order details page for partner or customer
	protected function generateHashedSecretToken($obj)
	{
		if ($obj->secret_token)
		{
			$hashedToken = Hash::make($obj->secret_token);
		}
		else
		{
			$token = str_random(7);
			$obj->update(['secret_token' => $token]);
			$hashedToken = Hash::make($token);
		}

		return $hashedToken;
	}

	// send push notification to Evibe.in partner app
	public function sendAppNotificationToPartnerOnNewOrder($order)
	{
		$ticket = $order->ticket;
		$area = $ticket->area ? $ticket->area->name : ($ticket->city ? $ticket->city->name : "Bangalore");

		$data = [
			'screen'   => config('evibe.partner_app_key.order.details'),
			'screenId' => $order->id,
			'title'    => "New order for " . date('d/m/y', $order->party_date_time),
			'message'  => "Party at $area from $ticket->name",
		];

		$this->sendAppNotificationToPartner($order, $data);
	}

	protected function sendAppNotificationToPartnerOnUpdatedOrder($order)
	{
		$ticket = $order->ticket;
		$area = $ticket->area ? $ticket->area->name : "Bangalore";

		$data = [
			'screen'   => config('evibe.partner_app_key.order.details'),
			'screenId' => $order->id,
			'title'    => "Order updated for " . date('d/m/y', $order->party_date_time),
			'message'  => "Party at $area from $ticket->name",
		];

		$this->sendAppNotificationToPartner($order, $data);
	}

	private function sendAppNotificationToPartner($order, $data)
	{
		$res = ['success' => false];
		$accessToken = $this->getAccessToken($order);
		$url = config('evibe.api.base_url') . config('evibe.api.partner.prefix') . 'notification/create?isFromMain=true';

		try
		{
			$client = new Client();
			$client->request('POST', $url, [
				'headers' => [
					'access-token' => $accessToken
				],
				'json'    => $data
			]);

			$res['success'] = true;
		} catch (ClientException $e)
		{
			$apiResponse = $e->getResponse()->getBody(true);
			$apiResponse = \GuzzleHttp\json_decode($apiResponse);
			$res['error'] = $apiResponse->errorMessage;
		}

		return $res;
	}

	public function getAccessTokenForCoupon($ticketId, $createdAt)
	{
		return Hash::make($ticketId . $createdAt);
	}

	public function fetchCustomerOrderDetails($ticket)
	{
		$data = [];

		if ($ticket)
		{
			// collect the data for showing the order details
			$allBookings = [];
			$hasVenue = false;
			$data = $this->getTicketData($ticket);
			$data['totalBookingAmount'] = $data['totalAdvanceAmount'] = $data['totalBalanceAmount'] = 0;
			$data['venueName'] = '';
			$bookings = TicketBooking::where(['is_advance_paid' => 1, 'ticket_id' => $ticket->id])->get();

			foreach ($bookings as $booking)
			{
				$mapTypeId = $booking->mapping->map_type_id;
				$mapId = $booking->mapping->map_id;
				$mappedValues = $this->fillMappingValues([
					                                         'isCollection' => false,
					                                         'mapId'        => $mapId,
					                                         'mapTypeId'    => $mapTypeId
				                                         ]);
				$allBookings[] = $this->getTicketBookingData($booking, $mappedValues, 1);

				if ($booking->is_venue_booking == 1 && $booking->provider)
				{
					$data['altPhone'] = $booking->provider->alt_phone;
					$data['venueName'] = $booking->provider->name;
					$hasVenue = true;
				}
				$data['totalBookingAmount'] += $booking->booking_amount;
				$data['totalAdvanceAmount'] += $booking->advance_amount;
				$data['totalBalanceAmount'] += $booking->booking_amount - $booking->advance_amount;
			}
			$data['hasVenue'] = $hasVenue;
			$data['bookings'] = $allBookings;

			$data['minBookingDateTime'] = $bookings->min('party_date_time');
			$data['maxBookingDateTime'] = $bookings->max('party_date_time');
			$partyDateTime = getPartyDateTimeByBookingsPartyTime($data['minBookingDateTime'], $data['maxBookingDateTime']);
			$data['partyDateTime'] = $partyDateTime;
		}

		return $data;
	}

	public function generateSecretToken($ticket)
	{
		if ($ticket->secret_token)
		{
			$hashedToken = Hash::make($ticket->secret_token);
		}
		else
		{
			$token = str_random(7);
			$ticket->update(['secret_token' => $token]);
			$hashedToken = Hash::make($token);
		}

		return $hashedToken;
	}

	public function initEInviteNotifications($data)
	{
		$mailData = [
			'customerName'     => isset($data['customer']['name']) ? ucwords($data['customer']['name']) : 'Customer',
			'customerPhone'    => isset($data['customer']['phone']) ? ucwords($data['customer']['phone']) : null,
			'customerEmail'    => isset($data['customer']['email']) ? ucwords($data['customer']['email']) : null,
			'customerAltEmail' => isset($data['customer']['altEmail']) ? ucwords($data['customer']['altEmail']) : null,
			'partyDate'        => isset($data['additional']['partyDate']) ? $data['additional']['partyDate'] : 'upcoming days',
			'eventId'          => isset($data['additional']['eventId']) ? $data['additional']['eventId'] : config('evibe.occasion.kids_birthdays.id'),
			'cityId'           => isset($data['additional']['cityId']) ? $data['additional']['cityId'] : config('evibe.city.Bengaluru'),
			'formUrl'          => config('evibe.e-invite.g-form-url'),
			'ticketId'         => isset($data['ticketId']) ? $data['ticketId'] : null,
			'dashLink'         => isset($data['ticketId']) && $data['ticketId'] ? config('evibe.dash_host') . '/tickets/' . $data['ticketId'] : null
		];

		$smsTpl = config('evibe.sms_tpl.invite.notify-form');
		$replaces = [
			'#customerName#' => isset($data['customer']['name']) ? ucwords($data['customer']['name']) : 'Customer',
			'#formLink#'     => config('evibe.e-invite.g-form-url')
		];
		$smsText = str_replace(array_keys($replaces), array_values($replaces), $smsTpl);

		//send sms and email to customer
		// @see: form is not being used right now
		//$this->dispatch(new MailEInviteCommunicationToCustomerJob($mailData));

		if (isset($data['customer']['phone']) && $data['customer']['phone'])
		{
			$smsData = [
				'to'   => $data['customer']['phone'],
				'text' => $smsText
			];
			// @see: form is not being used right now
			//$this->dispatch(new SMSEInviteCommunicationToCustomerJob($smsData));
		}

		// send email to tech team
		$this->dispatch(new MailEInviteCommunicationToTeamJob($mailData));
	}

	protected function makePinCodeServiceabilityApiCall($pinCode, $paymentTime = null)
	{
		// make API call
		$token = config('evibe.token.delhivery');
		$url = config('evibe.delhivery.pincode-serviceability') . '?token=' . $token . '&filter_codes=' . $pinCode;
		$paymentTime = $paymentTime ?: time();

		try
		{
			$client = new Client();
			$res = $client->request('GET', $url, [
				'json' => []
			]);

			$res = $res->getBody();
			$res = \GuzzleHttp\json_decode($res, true);

			if (isset($res) && $res)
			{
				// valid response
				if (isset($res['delivery_codes']) && $res['delivery_codes'])
				{
					// pin code is serviceable

					$deliveryDateString = $paymentTime + (3 * 24 * 60 * 60);
					$deliveryDate = date('jS M Y', $deliveryDateString);
					$deliveryText = "";
					$deliveryDateText = date('jS M Y', $deliveryDateString);

					// if HYD - 1 day
					// if any other metro city - 2 days
					// rest all possible locations - 3 days
					// @see: some issue with postal pin code API

					$res = $this->makePostalPinCodeApiCall($pinCode);
					if (isset($res['success']) && $res['success'] && isset($res['city']) && $res['city'])
					{
						$city = $res['city'];
						$metroCities = [
							'Hyderabad',
							'Secunderabad',
							'Bangalore',
							'Bengaluru',
							'Mumbai',
							'Bombay',
							'New Delhi',
							'Delhi',
							'Kolkata',
							'Chennai',
							'Madras',
							'Pune',
							'Kanpur',
							'Visakhapatnam',
							'Nagpur'
						];

						if (($city == 'Hyderabad') || ($city == 'Secunderabad'))
						{
							$deliveryDateString = $paymentTime + (1 * 24 * 60 * 60);
							$deliveryText = "Tomorrow";
						}
						elseif (in_array($city, $metroCities))
						{
							$deliveryDateString = $paymentTime + (2 * 24 * 60 * 60);
						}
						else
						{
							$deliveryDateString = $paymentTime + (3 * 24 * 60 * 60);
						}

						$deliveryDate = date('jS M Y', $deliveryDateString);
						$deliveryDateText = $deliveryText ? $deliveryText . " (" . $deliveryDate . ")" : $deliveryDate;
					}

					return [
						'success'          => true,
						'deliveryDate'     => $deliveryDate,
						'deliveryDateText' => $deliveryDateText
					];
				}
				else
				{
					// pin code is not serviceable
					return [
						'success'  => false,
						'errorMsg' => 'Sorry! delivery is not available for ' . $pinCode
					];
				}
			}
			else
			{
				// invalid response / unauthorised call
				return [
					'success'  => false,
					'errorMsg' => 'Uhu! Something is not right. Please refresh the page and try again.'
				];
			}

		} catch (\Exception $exception)
		{
			$errorMessage = "failed to fetch availability for pincode: $pinCode";
			$siteLogData = new SiteLogData();
			$siteLogData->setUrl(route("product-kits.delivery"))
			            ->setException($errorMessage)
			            ->setErrorCode($exception->getCode());

			$this->logSiteError($siteLogData);

			return [
				'success'  => false,
				'error'    => true,
				"errorMsg" => $errorMessage
			];
		}
	}

	protected function makePostalPinCodeApiCall($pinCode)
	{
		if (!$pinCode)
		{
			// report error
			return [
				'success' => false,
			];
		}

		/*
		 * Shifted from API to DB call
		 */

		//$url = "http://postalpincode.in/api/pincode/" . $pinCode;
		//
		//try
		//{
		//	$client = new Client();
		//	$res = $client->request('GET', $url, []);
		//
		//	$res = $res->getBody();
		//
		//	$res = \GuzzleHttp\json_decode($res, true);
		//
		//} catch (ClientException $e)
		//{
		//	$this->sendErrorReport($e);
		//
		//	$apiResponse = $e->getResponse()->getBody(true);
		//	$apiResponse = \GuzzleHttp\json_decode($apiResponse);
		//	$res['error'] = $apiResponse->errorMessage;
		//
		//	$this->saveError([
		//		                 'fullUrl' => request()->fullUrl(),
		//		                 'message' => 'Error occurred in Base controller while doing guzzle request',
		//		                 'code'    => 0,
		//		                 'details' => $res['error']
		//	                 ]);
		//
		//	return ['success' => false];
		//}

		// @see: curl request stopped working after some time
		//$ch = curl_init();
		//curl_setopt($ch, CURLOPT_URL, $url);
		//curl_setopt($ch, CURLOPT_HEADER, false);
		//curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		//$cResponse = (curl_exec($ch));
		//curl_close($ch);
		//$res = json_decode($cResponse, true);

		/*
		if (isset($res['Status']) && ($res['Status'] == "Success") && isset($res['PostOffice']) && count($res['PostOffice']))
		{
			foreach ($res['PostOffice'] as $postOffice)
			{
				if (isset($postOffice['Division']) && $postOffice['Division'] && isset($postOffice['State']) && $postOffice['State'])
				{
					$removableWords = [
						'City',
						'HQ',
						'East',
						'West',
						'North',
						'South',
						'Central'
					];

					$cityString = $postOffice['Division'];
					$cityArray = explode(' ', $cityString);
					$city = "";
					foreach ($cityArray as $item)
					{
						if (in_array($item, $removableWords))
						{
							// do not attach in city name formation
						}
						else
						{
							$city .= $item . ' ';
						}
					}
					$city = rtrim($city);

					return [
						'success' => true,
						'city'    => $city ?: $postOffice['Division'],
						'state'   => $postOffice['State'],
					];
				}

			}
		}
		*/

		$postalPinCode = PostalPinCode::where('pin_code', $pinCode)->first();
		if ($postalPinCode && $postalPinCode->division && $postalPinCode->state)
		{
			$removableWords = [
				'City',
				'HQ',
				'East',
				'West',
				'North',
				'South',
				'Central'
			];

			$cityString = $postalPinCode->division;
			$cityArray = explode(' ', $cityString);
			$city = "";
			foreach ($cityArray as $item)
			{
				if (in_array($item, $removableWords))
				{
					// do not attach in city name formation
				}
				else
				{
					$city .= $item . ' ';
				}
			}
			$city = rtrim($city);

			return [
				'success' => true,
				'city'    => $city ? ucwords(strtolower($city)) : ucwords(strtolower($postalPinCode->division)),
				'state'   => ucwords(strtolower($postalPinCode->state)),
			];
		}

		// API response failed at some point
		return [
			'success' => false
		];
	}
}