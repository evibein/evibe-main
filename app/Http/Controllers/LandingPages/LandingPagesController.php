<?php

namespace App\Http\Controllers\LandingPages;

use App\Http\Controllers\Base\BaseController;
use App\Models\LandingPages\OcassionLandingPages;
use App\Models\LandingPages\OcassionLandingPageCards;
use Illuminate\Support\Facades\Cache;
use App\Models\GoogleReviews\GoogleReview;


class LandingPagesController extends BaseController
{
	public function showVDayPage($pageUrl)
	{
		$pageId = "";
		$pageDetails = OcassionLandingPages::where('url', $pageUrl)
		                                   ->first();
		if ($pageDetails)
		{
			$cacheKey = $this->deviceSpecificCacheKey(request()->fullUrl());
			$cacheKey = base64_encode($cacheKey);
			if (Cache::has($cacheKey))
			{
				$data = Cache::get($cacheKey);

				return view("util.occasion-landing-page")->with(['data' => $data]);
			}

			$pageId = $pageDetails['id'];
			if ($pageId)
			{
				$cardsDetails = OcassionLandingPageCards::where('page_id', $pageId)->get();
				$campaignUrls = [];
				switch ($pageUrl)
				{
					case config('evibe.valentines-day-landingpages.for-hyderabad.url'):
						$campaignUrls = config('evibe.valentines-day-landingpages.for-hyderabad.campaignUrls');
						break;
					case config('evibe.valentines-day-landingpages.for-bangalore.url'):
						$campaignUrls = config('evibe.valentines-day-landingpages.for-bangalore.campaignUrls');
						break;
					case config('evibe.valentines-day-landingpages.for-pune.url'):
						$campaignUrls = config('evibe.valentines-day-landingpages.for-pune.campaignUrls');
						break;
					case config('evibe.valentines-day-landingpages.for-delhi.url'):
						$campaignUrls = config('evibe.valentines-day-landingpages.for-delhi.campaignUrls');
						break;
					case config('evibe.valentines-day-landingpages.for-mumbai.url'):
						$campaignUrls = config('evibe.valentines-day-landingpages.for-mumbai.campaignUrls');
						break;
					case config('evibe.valentines-day-landingpages.for-chennai.url'):
						$campaignUrls = config('evibe.valentines-day-landingpages.for-chennai.campaignUrls');
						break;
				}
				$products = [];
				$count = 0;
				$campaignController = app()->make("App\Http\Controllers\Campaigns\CampaignController");
				foreach ($campaignUrls as $cUrl)
				{
				
					// Assuming only one section is allowed for vDay Page
					$temp = $campaignController->landingPage($cUrl['url'], true);
					if ($temp != "")
					{
						$products[$count] = $temp[0];
						$products[$count]['heading'] = $cUrl['heading'];
						$products[$count++]['cUrl'] = $cUrl['url'];

					}
				}
				$data = [
					"pageDetails"   => $pageDetails,
					"cardsDetails"  => $cardsDetails,
					"products"      => $products,
					"isLandingPage" => "2"
				];

				// Storing the session data for one week, since we are not modifying the data much
				Cache::put($cacheKey, $data, config('evibe.cache.refresh-time'));

				return view("util.occasion-landing-page")->with(['data' => $data]);
			}
			else
			{
				// currently taking user to home @see:Inform Team
				return redirect('/');
			}
		}
		else
		{
			//do nothing, taking user to home
			return redirect('/');
		}
	}
	public function showVirtualBdyPage(){
		$cacheKey = $this->deviceSpecificCacheKey(request()->fullUrl());
		$cacheKey = base64_encode($cacheKey);
		if (Cache::has($cacheKey))
		{
			$data = Cache::get($cacheKey);
			return view("util.virtual-birthday-party.virtual-bdy-layout")->with(['data' => $data]);
			
		}

		$data = [
			"seoTitle" 	 		=> "Evibe.in Virtual Celebrations: fun & hassle-free lockdown parties.",
			"seoDesc"	 		=> "Make this birthday memorable for a lifetime with our virtual birthday party.",
			"keywords"	 		=> "virtual birthday, Evibe.in",
			"PageHeading"		=> 'Evibe.in Virtual Celebrations'
		];
	
		Cache::put($cacheKey, $data, config('evibe.cache.refresh-time'));
		
		return view("util.virtual-birthday-party.virtual-bdy-layout")->with(['data' => $data]);
			
	
		
	}
	public function showVirtualBdyPageTest(){
		$cacheKey = $this->deviceSpecificCacheKey(request()->fullUrl());
		$cacheKey = base64_encode($cacheKey);
		if (Cache::has($cacheKey))
		{
			$data = Cache::get($cacheKey);
			return view("util.virtual-birthday-party.virtual-bdy-seo-test")->with(['data' => $data]);
			
		}

		$data = [
			"seoTitle" 	 		=> "Evibe.in Virtual Celebrations: fun & hassle-free lockdown parties.",
			"seoDesc"	 		=> "Make this birthday memorable for a lifetime with our virtual birthday party.",
			"keywords"	 		=> "virtual birthday, Evibe.in",
			"PageHeading"		=> 'Evibe.in Virtual Celebrations'
		];
	
		Cache::put($cacheKey, $data, config('evibe.cache.refresh-time'));
		
		return view("util.virtual-birthday-party.virtual-bdy-seo-test")->with(['data' => $data]);
			
	
		
	}
	public function showLockdownPackages($cityUrl){
		$validUrlObj = $this->validateUrlParams($cityUrl);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}
		$cityUrl = strtolower($cityUrl);

		$cacheKey = $this->deviceSpecificCacheKey(request()->fullUrl());
		$cacheKey = base64_encode($cacheKey);
		if (Cache::has($cacheKey))
		{
			$data = Cache::get($cacheKey);
			return view("util.lockdown-packages-landing-page")->with(['data' => $data]);
			
		}
		$googleReviews = GoogleReview::where('is_show', '1')->limit(6)->get();

		if($cityUrl == "bangalore")
		{
			$cards = config('evibe.lockdown-landing-pages.bangalore');

		}
		elseif($cityUrl == "hyderabad")
		{
			$cards = config('evibe.lockdown-landing-pages.hyderabad');

		}
		if(isset($cards))
		{
			$data =  [ 
			"seoTitle" 	 		=> "Best Lockdown Packages | Evibe.in",
			"seoDesc"	 		=> "Evibe brings you the top packages for your occasion. ",
			"keywords"	 		=> "Lockdown, Evibe.in",
			"cards"		 		=>	$cards,
			'reviews'           =>  $googleReviews
			];
			Cache::put($cacheKey, $data, config('evibe.cache.refresh-time'));
		
			return view("util.lockdown-packages-landing-page")->with(['data' => $data]);
		}
		else
		{
			return redirect('/');
		}
		
	}
}