<?php

namespace App\Http\Controllers\ECards;

use App\Http\Controllers\Base\BaseController;
use App\Models\Invite\InviteTemplate;

class ECardsController extends BaseController
{
	public function showDiwaliTemplates()
	{
		$supportedTemplateIds = ["39", "40", "41"];
		$data = [];

		$inviteTemplates = InviteTemplate::whereIn('id', $supportedTemplateIds)->get();

		if (!count($inviteTemplates))
		{
			$data['errorMsg'] = "Some error occurred while fetching invitation templates. Kindly refresh the page and try again. if the issue still persists, write to " . config('evibe.contact.invitations.group');
		}

		$data['inviteTemplates'] = $inviteTemplates;
		$data['cardTypeId'] = "";
		$data['seo'] = [
			"title"       => "Create Your Customized Party Invitation For Free | Evibe.in",
			"description" => "Get your own digital invite to download and send online for free. Easy to customize.",
			"kw"          => "free ecards, digital invitation templates, whatsapp downloadable, customizable invites"
		];

		return view('invites.template-selection-dynamic', ['data' => $data]);
	}

	public function showInviteTemplates()
	{
		$ticketId = request('tId');
		$typeCardId = config("evibe.type-card.invite");

		$data = $this->showECardTemplates($typeCardId, $ticketId);
		$data['cardTypeId'] = $typeCardId;
		$data['seo'] = [
			"title"       => "Get Whatsapp Download Premium Invitations | Evibe.in",
			"description" => "Customize and download beautiful online invitation cards, birthday wish cards in your style for free and whatsapp them with your family, friends and relatives.",
			"kw"          => "wish-cards, birthday wish-cards, free ecards, digital invitation templates, whatsapp downloadable, customizable invites"

		];

		$data['trending'] = array_filter($data["inviteTemplates"]->toArray(), function ($query) {
			return in_array($query["id"], ["6", "10", "12", "19"]);
		});

		return view('invites.template-selection', ['data' => $data]);
	}

	public function showWishCardTemplates()
	{
		$ticketId = request('tId');
		$typeCardId = config("evibe.type-card.wish-card");

		$data = $this->showECardTemplates($typeCardId, $ticketId);
		$data['cardTypeId'] = $typeCardId;
		$data['seo'] = [
			"title"       => "Get Free Whatsapp Download Premium Wishcards | Evibe.in",
			"description" => "Customize and download beautiful online birthday wish cards in your style for free and whatsapp them with your family, friends and relatives.",
			"kw"          => "wish-cards, birthday wish-cards, free ecards, digital invitation templates, whatsapp downloadable, customizable invites"
		];

		return view('invites.template-selection', ['data' => $data]);
	}

	public function showThankYouCardTemplates()
	{
		$ticketId = request('tId');
		$typeCardId = config("evibe.type-card.thank-you-card");

		$data = $this->showECardTemplates($typeCardId, $ticketId);
		$data['cardTypeId'] = $typeCardId;
		$data['seo'] = [
			"title"       => "Get Free Whatsapp Download Premium Thankyou Cards | Evibe.in",
			"description" => "Customize and download beautiful online thank you cards, birthday wish cards in your style for free and whatsapp them with your family, friends and relatives.",
			"kw"          => "wish-cards, birthday wish-cards, free ecards, digital invitation templates, whatsapp downloadable, customizable invites"
		];

		return view('invites.template-selection', ['data' => $data]);
	}

	protected function showECardTemplates($typeCardId, $ticketId)
	{
		$data = [];

		try
		{
			$inviteTemplates = InviteTemplate::where('type_card_id', $typeCardId)->get();

			if (!count($inviteTemplates))
			{
				$data['errorMsg'] = "Some error occurred while fetching invitation templates. Kindly refresh the page and try again. if the issue still persists, write to " . config('evibe.contact.invitations.group');
			}

			$data['inviteTemplates'] = $inviteTemplates;
			$data['ticketId'] = $ticketId;
			$data['inviteStepIndex'] = 1; // @see: hardcoded

			return $data;

		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);
			$data['errorMsg'] = "Some error occurred while fetching invitation templates. Kindly refresh the page and try again. if the issue still persists, write to " . config('evibe.contact.invitations.group');

			return $data;
		}
	}
}