<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Payments\BasePaymentController;
use App\Jobs\Emails\Feedback\MailPIABCustomerFeedbackToTeam;
use App\Jobs\Emails\Payment\Customer\MailProductBookingReceiptToCustomerJob;
use App\Jobs\Emails\Payment\PaymentInitiationUpdate;
use App\Jobs\Emails\Payment\PaymentTransactionInfoUpdate;
use App\Jobs\Emails\Payment\Team\MailProductBookingAlertToTeamJob;
use App\Jobs\SMS\Payment\Customer\SMSAutoBookingPaymentSuccessToCustomerJob;
use App\Models\Product\Product;
use App\Models\Product\ProductBooking;
use App\Models\Product\ProductBookingAdditionalField;
use App\Models\Product\ProductCategory;
use App\Models\Product\ProductFeedback;
use App\Models\Product\ProductStyle;
use App\Models\Product\ProductStyleGallery;
use App\Models\Util\Country;
use App\Models\Util\SiteErrorLog;
use App\Models\Util\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ProductBookingController extends BasePaymentController
{
	public function initProductBooking($productStyleId)
	{
		try
		{
			$productStyle = ProductStyle::find($productStyleId);
			if (!$productStyle)
			{
				$this->sendNonExceptionErrorReportToTeam([
					                                         'code'      => config('evibe.error_code.create_function'),
					                                         'url'       => request()->fullUrl(),
					                                         'method'    => request()->method(),
					                                         'message'   => '[ProductBookingController.php - ' . __LINE__ . '] ' . 'Unable to fetch product style data.',
					                                         'exception' => '[ProductBookingController.php - ' . __LINE__ . '] ' . 'Unable to fetch product style data.',
					                                         'trace'     => '[Product Style Id: ' . $productStyleId . ']',
					                                         'details'   => '[Product Style Id: ' . $productStyleId . ']'
				                                         ]);

				return response()->json([
					                        'success' => false,
				                        ]);
			}

			$productType = Product::find($productStyle->product_id);
			if (!$productType)
			{
				$this->sendNonExceptionErrorReportToTeam([
					                                         'code'      => config('evibe.error_code.create_function'),
					                                         'url'       => request()->fullUrl(),
					                                         'method'    => request()->method(),
					                                         'message'   => '[ProductBookingController.php - ' . __LINE__ . '] ' . 'Unable to fetch product type data.',
					                                         'exception' => '[ProductBookingController.php - ' . __LINE__ . '] ' . 'Unable to fetch product type data.',
					                                         'trace'     => '[Product Style Id: ' . $productStyleId . '] [Product Id: ' . $productStyle->product_id . ']',
					                                         'details'   => '[Product Style Id: ' . $productStyleId . '] [Product Id: ' . $productStyle->product_id . ']'
				                                         ]);

				return response()->json([
					                        'success' => false,
				                        ]);
			}

			$productCategory = ProductCategory::find($productType->product_category_id);
			if (!$productCategory)
			{
				$this->sendNonExceptionErrorReportToTeam([
					                                         'code'      => config('evibe.error_code.create_function'),
					                                         'url'       => request()->fullUrl(),
					                                         'method'    => request()->method(),
					                                         'message'   => '[ProductBookingController.php - ' . __LINE__ . '] ' . 'Unable to fetch product category data.',
					                                         'exception' => '[ProductBookingController.php - ' . __LINE__ . '] ' . 'Unable to fetch product category data.',
					                                         'trace'     => '[Product Style Id: ' . $productStyleId . '] [Product Id: ' . $productStyle->product_id . '] [Product Category Id:' . $productType->product_category_id . ']',
					                                         'details'   => '[Product Style Id: ' . $productStyleId . '] [Product Id: ' . $productStyle->product_id . '] [Product Category Id:' . $productType->product_category_id . ']'
				                                         ]);

				return response()->json([
					                        'success' => false,
				                        ]);
			}

			$productUrl = config('evibe.host') . '/party-kits/' . $productCategory->url . '/' . $productType->url . '/' . $productStyle->url;
			$productName = $productCategory->name . ' - ' . $productType->name . ' - ' . $productStyle->name;
			$bookingInfo = "<a href='" . $productUrl . "?ref=pay-checkout' target='_blank'>" . $productName . "</a>";

			$user = auth()->user();

			$productPrice = $productStyle->price;
			$deliveryCharges = 0;
			$bookingAmount = $productPrice + $deliveryCharges;

			$productBookingData = [
				'name'                      => $user ? $user->name : null,
				'email'                     => $user ? $user->username : null,
				'phone'                     => $user ? $user->phone : null,
				'booking_order_id'          => "PIAB" . time(),
				'booking_info'              => $bookingInfo,
				'user_id'                   => $user ? $user->id : null,
				'product_booking_status_id' => config('evibe.product-booking-status.initiated'),
				'booking_units'             => 1,
				'product_category_id'       => $productCategory->id,
				'product_id'                => $productType->id,
				'product_style_id'          => $productStyle->id,
				'product_price'             => $productPrice,
				'delivery_charges'          => $deliveryCharges,
				'booking_amount'            => $bookingAmount,
				'zip_code'                  => request('delPinCode') ?: null,
				'created_at'                => date('Y-m-d H:i:s'),
				'updated_at'                => date('Y-m-d H:i:s'),
			];

			$productBooking = ProductBooking::create($productBookingData);
			if (!$productBooking)
			{
				$this->sendNonExceptionErrorReportToTeam([
					                                         'code'      => config('evibe.error_code.create_function'),
					                                         'url'       => request()->fullUrl(),
					                                         'method'    => request()->method(),
					                                         'message'   => '[ProductBookingController.php - ' . __LINE__ . '] ' . 'Unable to product booking from product booking data.',
					                                         'exception' => '[ProductBookingController.php - ' . __LINE__ . '] ' . 'Unable to product booking from product booking data.',
					                                         'trace'     => '[Product Booking Data: ' . $productBookingData . ']',
					                                         'details'   => '[Product Booking Data: ' . $productBookingData . ']'
				                                         ]);
			}

			// @todo: product booking log

			$checkoutLink = route('party-kits.buy.checkout');
			$checkoutLink .= "?id=" . $productBooking->id;
			$checkoutLink .= "&pbId=" . $productBooking->booking_order_id;
			$checkoutLink .= "&token=" . Hash::make($productBooking->id);

			if ($checkoutLink)
			{
				return response()->json([
					                        'success'     => true,
					                        'redirectUrl' => $checkoutLink
				                        ]);
			}

		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);

			return response()->json([
				                        'success' => false
			                        ]);
		}
	}

	public function showCheckoutPage()
	{
		$productBookingId = request()->input('id');
		$token = request()->input('token');

		if ($productBookingId && $token && Hash::check($productBookingId, $token))
		{
			$productBooking = ProductBooking::find($productBookingId);
			if ($productBooking)
			{
				$data = $this->getProductBookingData($productBookingId, $productBooking);

				// @todo: need to check with TMO controller
				//$tmoToken = Hash::make($ticketId . config('evibe.token.track-my-order'));
				//$data['tmoLink'] = route("track.orders") . "?ref=checkout-page&id=" . $ticketId . "&token=" . $tmoToken;
				$data['tmoLink'] = route("track.orders") . "?ref=checkout-page";

				// countries for calling code
				$data['countries'] = Country::all();
				$bookingAmount = (isset($data["booking"]) && isset($data["booking"]["bookingAmount"])) ? $data["booking"]["bookingAmount"] : 0;
				$data['couponToken'] = $this->getAccessTokenForCoupon(intval($productBookingId), $productBooking->created_at);

				$payInitTrackingData = [
					"piabId"      => $productBooking->id,
					"browserInfo" => isset($_SERVER) && isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "",
					"previousUrl" => isset($_SERVER) && isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null,
					"ipAddress"   => isset($_SERVER) && isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : ""
				];

				$this->dispatch(new PaymentInitiationUpdate($payInitTrackingData));

				return view('product/pay/checkout', ['data' => $data]);
			}
			else
			{
				$this->sendNonExceptionErrorReportToTeam([
					                                         'code'      => config('evibe.error_code.update_function'),
					                                         'url'       => request()->fullUrl(),
					                                         'method'    => request()->method(),
					                                         'message'   => '[ProductBookingController.php - ' . __LINE__ . '] ' . 'Unable to find product booking info from id',
					                                         'exception' => '[ProductBookingController.php - ' . __LINE__ . '] ' . 'Unable to find product booking info from id',
					                                         'trace'     => '[Product Booking Id: ' . $productBookingId . ']',
					                                         'details'   => '[Product Booking Id: ' . $productBookingId . ']'
				                                         ]);

				return "<h1>Invalid Request</h1>";
			}
		}
		else
		{
			$this->sendNonExceptionErrorReportToTeam([
				                                         'code'      => config('evibe.error_code.update_function'),
				                                         'url'       => request()->fullUrl(),
				                                         'method'    => request()->method(),
				                                         'message'   => '[ProductBookingController.php - ' . __LINE__ . '] ' . 'Token mismatch issue.',
				                                         'exception' => '[ProductBookingController.php - ' . __LINE__ . '] ' . 'Token mismatch issue.',
				                                         'trace'     => '[Product Booking Id: ' . $productBookingId . '] [Token: ' . $token . ']',
				                                         'details'   => '[Product Booking Id: ' . $productBookingId . '] [Token: ' . $token . ']'
			                                         ]);

			return "<h1>Invalid Request</h1>";
		}
	}

	public function validateBookingContactDetails($productBookingId)
	{
		try
		{

			$productBooking = ProductBooking::find($productBookingId);
			if (!$productBooking)
			{
				Log::error("Unable to find booking for productBookingId: $productBookingId");

				return response()->json([
					                        'success' => false,
				                        ]);
			}

			$rules = [
				'phone' => 'required|digits:10',
				'name'  => 'required|min:4',
				'title' => 'required',
				'email' => 'sometimes|required|email',
			];

			$messages = [
				'name.required'  => 'Your full name is required',
				'name.min'       => 'Your full name cannot be less than 4 characters',
				'email.required' => 'Your email id is required',
				'email.email'    => 'Your email id should be in the form of example@abc.com',
				'phone.required' => 'Your phone number is required',
				'phone.digits'   => 'Phone number is not valid. Enter only 10 digits number (ex: 9640204000)',
				'title.required' => 'Please select your title (Mr. / Mrs.)'
			];

			$validation = Validator::make(request()->all(), $rules, $messages);

			if ($validation->fails())
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => $validation->messages()->first()
				                        ]);
			}

			$productBooking->name = request('name');
			$productBooking->email = request('email');
			$productBooking->phone = request('phone');
			$productBooking->calling_code = request('callingCode');
			$productBooking->customer_gender = request('title');

			$productBooking->updated_at = Carbon::now();
			$productBooking->save();

			return response()->json([
				                        'success' => true
			                        ]);

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());

			return response()->json([
				                        'success' => false,
			                        ]);
		}
	}

	public function validateBookingDeliveryDetails($productBookingId)
	{
		try
		{

			$productBooking = ProductBooking::find($productBookingId);
			if (!$productBooking)
			{
				Log::error("Unable to find booking for productBookingId: $productBookingId");

				return response()->json([
					                        'success' => false,
				                        ]);
			}

			$rules = [
				'zipCode'  => 'sometimes|required|digits:6',
				'address'  => 'sometimes|required|min:5',
				'landmark' => 'sometimes|required|min:5',
				'city'     => 'sometimes|required',
				'state'    => 'sometimes|required',
			];

			$messages = [
				'landmark.required' => 'Please enter landmark',
				'landmark.min'      => 'Please enter valid landmark',
				'address.required'  => 'Address is required',
				'address.min'       => 'Address should be minimum 5 characters long',
				'zipCode.required'  => 'Please enter venue PIN code',
				'zipCode.digits'    => 'Please enter a valid 6 digit pin code (ex: 560001)',
				'city.required'     => 'City is required',
				'state.required'    => 'State is required',
			];

			$validation = Validator::make(request()->all(), $rules, $messages);

			if ($validation->fails())
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => $validation->messages()->first()
				                        ]);
			}

			$zipCode = request('zipCode');

			// validate pin code serviceability
			$res = $this->makePinCodeServiceabilityApiCall($zipCode);
			if (isset($res['success']) && !$res['success'])
			{
				return response()->json($res);
			}

			$productBooking->zip_code = $zipCode;
			$productBooking->address = request('address');
			$productBooking->landmark = request('landmark');
			$productBooking->city = request('city');
			$productBooking->state = request('state');

			$productBooking->updated_at = Carbon::now();
			$productBooking->save();

			return response()->json([
				                        'success' => true
			                        ]);

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());

			return response()->json([
				                        'success' => false,
			                        ]);
		}
	}

	public function validateBookingOrderDetails($productBookingId)
	{

	}

	public function updateBookingPriceDetails($productBookingId)
	{
		try
		{

			$productBooking = ProductBooking::find($productBookingId);
			if (!is_null($productBooking))
			{
				$orderPriceDetails = [];
				$productWorthAmount = 0;
				$couponDiscountAmount = 0;

				$productStyle = ProductStyle::find($productBooking->product_style_id);
				if ($productStyle)
				{
					$productWorthAmount = $productStyle->price_worth;
				}

				array_push($orderPriceDetails, [
					'productId'               => $productBooking->product_style_id,
					'productTypeId'           => config('evibe.ticket.type.product'),
					'productTitle'            => $productBooking->booking_info,
					'productPrice'            => $productBooking->product_price,
					'productBookingAmount'    => $productBooking->booking_amount,
					'productWorthAmount'      => $productWorthAmount,
					'productTransportCharges' => $productBooking->delivery_charges,
				]);

				$totalBookingAmount = $productBooking->booking_amount;
				$totalAdvanceToPay = $productBooking->booking_amount;
				$couponDiscountAmount = $productBooking->prepay_discount_amount;

				// advance amount may be '0' in case of payment success tickets
				if ($totalBookingAmount)
				{
					return response()->json([
						                        'success'              => true,
						                        'advanceAmount'        => $totalAdvanceToPay,
						                        'bookingAmount'        => $totalBookingAmount,
						                        'couponDiscountAmount' => $couponDiscountAmount,
						                        'orderPriceDetails'    => $orderPriceDetails
					                        ]);
				}
				else
				{
					$this->sendNonExceptionErrorReportToTeam([
						                                         'code'      => config('evibe.error_code.update_function'),
						                                         'url'       => request()->fullUrl(),
						                                         'method'    => request()->method(),
						                                         'message'   => '[ProductBookingController.php - ' . __LINE__ . '] ' . 'There is some issue in calculating booking amount.',
						                                         'exception' => '[ProductBookingController.php - ' . __LINE__ . '] ' . 'There is some issue in calculating booking amount.',
						                                         'trace'     => '[Product Booking Id: ' . $productBooking->id . ']',
						                                         'details'   => '[Product Booking Id: ' . $productBooking->id . ']'
					                                         ]);

					return response()->json([
						                        'success' => false
					                        ]);
				}
			}
			else
			{
				SiteErrorLog::create([
					                     'url'        => request()->fullUrl(),
					                     'exception'  => "Product Booking Not Found",
					                     'code'       => config('evibe.error_code.fetch_function'),
					                     'details'    => "Some error occurred in ProductBookingController [Line: " . __LINE__ . "]. Unable to find productBooking [Product Booking Id: $productBookingId]",
					                     'created_at' => date('Y-m-d H:i:s'),
					                     'updated_at' => date('Y-m-d H:i:s')
				                     ]);

				return response()->json([
					                        'success' => false,
				                        ]);
			}

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());

			return response()->json([
				                        'success' => false,
			                        ]);
		}
	}

	public function processPayment($productBookingId)
	{
		$response = [
			'success'    => false,
			'error'      => 'Failed to send booked notifications',
			'redirectTo' => route('pay.common.fail')
		];

		if ($this->sendProductBookedNotifications($productBookingId))
		{
			$token = request()->input('token');
			$response['success'] = true;
			$response['redirectTo'] = route('party-kits.buy.success', $productBookingId) . "?token=" . $token;
			unset($response['error']);

			$productBooking = ProductBooking::find($productBookingId);
			if (!$productBooking)
			{
				// @todo: report
			}

			// Update transaction success in DB
			$this->dispatch(new PaymentTransactionInfoUpdate([
				                                                 "piabId"    => $productBooking->id,
				                                                 "isSuccess" => false
			                                                 ]));
		}

		return response()->json($response);
	}

	public function onPaymentSuccess($productBookingId)
	{
		$token = request()->input('token');
		$productBooking = ProductBooking::find($productBookingId);
		if (!$productBooking)
		{
			return "<h1>Invalid Request</h1>";
		}
		if (Hash::check($productBookingId, $token))
		{
			$data = $this->getProductBookingData($productBookingId, $productBooking);

			$data['totalBookingAmount'] = $productBooking->booking_amount;
			$data['totalProductsPrice'] = $productBooking->product_price;
			$data['totalDeliveryCharges'] = $productBooking->delivery_charges;
			$data['totalCouponDiscountAmount'] = $productBooking->coupon_discount_amount;

			// @todo: need to check with TMO controller
			//$tmoToken = Hash::make($ticketId . config('evibe.token.track-my-order'));
			//$data['tmoLink'] = route("track.orders") . "?ref=checkout-page&id=" . $ticketId . "&token=" . $tmoToken;
			$data['tmoLink'] = route("track.orders") . "?ref=checkout-page";

			return view('product/pay/success', ["data" => $data]);
		}
		else
		{
			SiteErrorLog::create([
				                     'url'        => request()->fullUrl(),
				                     'exception'  => "ProductBooking: Tried to hack",
				                     'code'       => "Error",
				                     'details'    => "Some error occurred in ProductBookingController [Line: " . __LINE__ . "]. Tried to change the product booking id with invalid token :: $productBookingId",
				                     'created_at' => date('Y-m-d H:i:s'),
				                     'updated_at' => date('Y-m-d H:i:s')
			                     ]);

			return "<h1>Invalid Request</h1>";
		}
	}

	private function sendProductBookedNotifications($productBookingId, $isFromDash = false, $handlerId = null)
	{
		$productBooking = ProductBooking::find($productBookingId);
		if (!$productBooking)
		{
			Log::error("Unable to find product booking from id: $productBookingId");

			return false;
		}

		if ($productBooking->product_booking_status_id == config('evibe.product-booking-status.paid'))
		{
			Log::error("SEND RECEIPT :: TRYING TO RESEND RECEIPT FOR :: ID $productBookingId");

			return false;
		}

		// update expected delivery date
		if ($productBooking->zip_code)
		{
			$res = $this->makePinCodeServiceabilityApiCall($productBooking->zip_code);
			if (isset($res['success']) && $res['success'] && isset($res['deliveryDate']) && $res['deliveryDate'])
			{
				$productBooking->expected_delivery_date = Carbon::createFromTimestamp(strtotime($res['deliveryDate']))->toDateTimeString();
				$productBooking->save();
			}
		}

		$totalProductAmount = $productBooking->product_price;
		$totalDeliveryCharge = $productBooking->delivery_charge;
		$totalBookingAmount = $productBooking->booking_amount;
		$totalCouponDiscountAmount = $productBooking->coupon_discount_amount;
		$totalAmountPaid = $totalBookingAmount - $totalCouponDiscountAmount;
		$data = $this->getProductBookingData($productBookingId, $productBooking);

		$data['totalProductAmount'] = $totalProductAmount;
		$data['totalProductAmountStr'] = $totalProductAmount ? $this->formatPrice($totalProductAmount) : 0;
		$data['totalDeliveryCharge'] = $totalDeliveryCharge;
		$data['totalDeliveryChargeStr'] = $totalDeliveryCharge ? $this->formatPrice($totalDeliveryCharge) : 0;
		$data['totalBookingAmount'] = $totalBookingAmount;
		$data['totalBookingAmountStr'] = $totalBookingAmount ? $this->formatPrice($totalBookingAmount) : 0;
		$data['totalCouponDiscountAmount'] = $totalCouponDiscountAmount;
		$data['totalCouponDiscountAmountStr'] = $totalCouponDiscountAmount ? $this->formatPrice($totalCouponDiscountAmount) : 0;
		$data['totalAmountPaid'] = $totalAmountPaid;
		$data['totalAmountPaidStr'] = $totalAmountPaid ? $this->formatPrice($totalAmountPaid) : 0;

		// @todo: need to check with TMO controller
		//$tmoToken = Hash::make($ticketId . config('evibe.token.track-my-order'));
		//$data['tmoLink'] = route("track.orders") . "?ref=checkout-page&id=" . $ticketId . "&token=" . $tmoToken;
		$data['tmoLink'] = route("track.orders") . "?ref=checkout-page";

		// send single email & sms to customer
		$this->initProductBookedNotificationToCustomer($data);
		// send email notification to team
		$this->initProductBookingNotificationToTeam($data);
		$statusId = config('evibe.product-booking-status.paid');

		$productBooking->product_booking_status_id = $statusId;
		$productBooking->paid_at = time();
		$productBooking->updated_at = Carbon::now();
		$productBooking->save();

		return true;
	}

	public function getProductBookingData($productBookingId, $productBooking = null)
	{
		if (!$productBooking)
		{
			$productBooking = ProductBooking::find($productBookingId);
		}

		if (!$productBooking)
		{
			return [];
		}

		$productStyleId = $productBooking->product_style_id;
		$productProfileImg = ProductStyleGallery::where('product_style_id', $productStyleId)
		                                        ->where('is_profile', 1)
		                                        ->first();
		$productImgFullUrl = config('evibe.gallery.host') . '/product-styles/' . $productStyleId . '/images/profile/' . $productProfileImg->url;

		$additionalFields = ProductBookingAdditionalField::with('fieldOptions')
		                                                 ->where('product_category_id', $productBooking->product_category_id)
		                                                 ->whereNull('deleted_at')
		                                                 ->get();

		$additionalFieldsData = [];
		foreach ($additionalFields as $additionalField)
		{
			$fieldOptionsData = [];
			$fieldOptions = $additionalField->fieldOptions;
			foreach ($fieldOptions as $fieldOption)
			{
				array_push($fieldOptionsData, [
					'id'   => $fieldOption->id,
					'name' => $fieldOption->name
				]);
			}

			array_push($additionalFieldsData, [
				'id'          => $additionalField->id,
				'name'        => $additionalField->name,
				'identifier'  => $additionalField->identifier,
				'hintMessage' => $additionalField->hint_message,
				'typeFieldId' => $additionalField->type_field_id,
				'options'     => $fieldOptionsData,
				'value'       => $additionalField->getFieldValue($productBookingId)
			]);
		}

		$data = [
			'customer'         => [
				'name'           => $productBooking->name,
				'email'          => $productBooking->email,
				'phone'          => $productBooking->phone,
				'altEmail'       => $productBooking->alt_email,
				'altPhone'       => $productBooking->alt_phone,
				'customerGender' => $productBooking->customer_gender,
				'callingCode'    => $productBooking->calling_code
			],
			'delivery'         => [
				'zipCode'      => $productBooking->zip_code,
				'address'      => $productBooking->address,
				'landmark'     => $productBooking->landmark,
				'city'         => $productBooking->city,
				'state'        => $productBooking->state,
				'fullAddress'  => $productBooking->address . ', ' . $productBooking->city . ', ' . $productBooking->state . ', ' . $productBooking->zip_code,
				'deliveryDate' => $productBooking->expected_delivery_date ?: null
			],
			'booking'          => [
				'id'                   => $productBooking->id,
				'bookingOrderId'       => $productBooking->booking_order_id,
				'bookingInfo'          => $productBooking->booking_info,
				'productPrice'         => $productBooking->product_price,
				'deliveryCharges'      => $productBooking->delivery_charges,
				'couponDiscountAmount' => $productBooking->coupon_discount_amount,
				'bookingAmount'        => $productBooking->booking_amount,
				'paidAt'               => $productBooking->paid_at,
				'statusId'             => $productBooking->product_booking_status_id,
				'productImgFullUrl'    => $productImgFullUrl,
				'sku'                  => $productBooking->style->code,
				'categoryId'           => $productBooking->product_category_id
			],
			'additionalFields' => $additionalFieldsData
		];

		return $data;
	}

	public function submitFeedback()
	{
		try
		{
			$rules = [
				'feedbackComment' => 'required|min:4',
				'name'            => 'required|min:4',
				'phone'           => 'required|digits:10',
				'email'           => 'sometimes|required|email',
			];

			$messages = [
				'name.required'            => 'Your name is required',
				'name.min'                 => 'Your name cannot be less than 4 characters',
				'email.required'           => 'Your email id is required',
				'email.email'              => 'Your email id should be in the form of example@abc.com',
				'phone.required'           => 'Your phone number is required',
				'phone.digits'             => 'Phone number is not valid. Enter only 10 digits number (ex: 9640204000)',
				'feedbackComment.required' => 'Please tell us your feedback or ask any question',
				'feedbackComment.min'      => 'Your feedback or question cannot be less thatn 4 characters'
			];

			$validation = Validator::make(request()->all(), $rules, $messages);

			if ($validation->fails())
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => $validation->messages()->first()
				                        ]);
			}

			$name = request('name');
			$email = request('email');
			$phone = request('phone');
			$user = auth()->user();

			// search for user using email or phone
			if (!$user)
			{
				$user = User::where('username', $email)
				            ->orWhere('phone', $phone)// @see: Is validation based only on email?
				            ->first();
			}

			// update mobile number if it wasn't there
			if ($user && !$user->phone && $phone)
			{
				$user->phone = $phone;
				$user->save();
			}

			$userId = $user ? $user->id : null;

			$feedbackComment = request('feedbackComment');
			$productStyleId = request('productStyleId');

			/* store in DB */
			ProductFeedback::create([
				                        'name'             => $name,
				                        'email'            => $email,
				                        'phone'            => $phone,
				                        'user_id'          => $userId,
				                        'feedback'         => $feedbackComment,
				                        'product_style_id' => $productStyleId,
				                        'created_at'       => Carbon::now(),
				                        'updated_at'       => Carbon::now()
			                        ]);

			/* form data to send email */
			$data = [
				'feedbackComment' => $feedbackComment,
				'name'            => $name,
				'phone'           => $phone,
				'email'           => $email,
				'userId'          => $userId
			];

			if ($productStyleId)
			{
				$productStyle = ProductStyle::find($productStyleId);
				if ($productStyle)
				{
					$productType = Product::find($productStyle->product_id);
					if ($productType)
					{
						$productCategory = ProductCategory::find($productType->product_category_id);
						if ($productCategory)
						{
							$productUrl = config('evibe.host') . '/party-kits/' . $productCategory->url . '/' . $productType->url . '/' . $productStyle->url;
							$productName = $productCategory->name . ' - ' . $productType->name . ' - ' . $productStyle->name;
							$productInfo = "<a href='" . $productUrl . "?ref=pay-checkout' target='_blank'>" . $productName . "</a>";

							$data['productInfo'] = $productInfo;
						}
					}
					$data['productStyleCode'] = $productStyle->code;
					$data['productStyleId'] = $productStyleId;
				}
			}

			$data['mailView'] = 'emails.product.customer-feedback-team';
			$data['mailSub'] = 'A customer has a query about our PIAB product';

			//send sms and email to customer
			$this->dispatch(new MailPIABCustomerFeedbackToTeam($data));

			return response()->json([
				                        'success' => true,
			                        ]);
		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);

			return response()->json([
				                        'success' => false
			                        ]);
		}
	}

	public function checkDeliveryServiceability()
	{
		try
		{
			$deliveryPincode = request('deliveryPincode');
			$res = $this->makePinCodeServiceabilityApiCall($deliveryPincode);

			return response()->json($res);

		} catch (\Exception $exception)
		{
			$this->sendErrorReportToTeam($exception);

			return response()->json([
				                        'success' => false
			                        ]);
		}
	}

	public function getDeliveryPinData($pinCode)
	{
		try
		{
			$res = $this->makePostalPinCodeApiCall($pinCode);

			return response()->json($res);

		} catch (\Exception $exception)
		{
			$this->sendErrorReportToTeam($exception);

			return response()->json([
				                        'success' => false
			                        ]);
		}
	}

	private function initProductBookedNotificationToCustomer($data)
	{
		$tplCustomer = config('evibe.sms_tpl.product-book.pay-success.customer');
		$replaces = [
			'#customer#'       => $data['customer']['name'],
			'#product#'        => substr(strip_tags($data['booking']['bookingInfo']), 0, 10) . "...",
			'#bookingOrderId#' => '#' . $data['booking']['bookingOrderId'],
			'#deliveryText#'   => $data['delivery']['deliveryDate'] ? date('jS M Y', strtotime($data['delivery']['deliveryDate'])) : "within 3 - 5 business days",
			'#tmoLink#'        => $this->getShortenUrl(route("track.orders") . "?ref=SMS")
		];

		$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);

		$smsData = [
			'to'   => $data['customer']['phone'],
			'text' => $smsText
		];

		$data['mailView'] = 'emails.product.pay-success.customer-receipt';
		$data['mailSub'] = 'Order Placed - #' . $data['booking']['bookingOrderId'];

		//send sms and email to customer
		$this->dispatch(new MailProductBookingReceiptToCustomerJob($data));
		$this->dispatch(new SMSAutoBookingPaymentSuccessToCustomerJob($smsData));
	}

	private function initProductBookingNotificationToTeam($data)
	{
		$data['mailView'] = 'emails.product.pay-success.alert-team';
		$data['mailSub'] = 'A customer booked Party In A Box - #' . $data['booking']['bookingOrderId'];

		//send sms and email to customer
		$this->dispatch(new MailProductBookingAlertToTeamJob($data));
	}
}