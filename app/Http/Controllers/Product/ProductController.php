<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Base\BaseResultsController;
use App\Models\Product\Product;
use App\Models\Product\ProductCategory;
use App\Models\Product\ProductStyle;
use App\Models\Product\ProductStyleGallery;
use Illuminate\Support\Facades\Cache;

class ProductController extends BaseResultsController
{
	public function getProductsHomeData()
	{
		$cacheKey = $this->deviceSpecificCacheKey(request()->fullUrl());
		$cacheKey = base64_encode($cacheKey);
		if (Cache::has($cacheKey))
		{
			$homeData = Cache::get($cacheKey);
		}
		else
		{
			$homeData = $this->formProductsHomeData();
			Cache::put($cacheKey, $homeData, config('evibe.cache.refresh-time'));
		}

		return $this->renderDataToViewFiles('product/home', $homeData);
	}

	public function formProductsHomeData()
	{
		$productCategories = ProductCategory::all();

		$productCategoriesList = [];

		foreach ($productCategories as $category)
		{
			$categoryImgFullUrl = config('evibe.gallery.host') . '/main/img/piab/piab-cat-' . $category->id . '.jpg';

			array_push($productCategoriesList, [
				'id'         => $category->id,
				'url'        => $category->url,
				'name'       => $category->name,
				'info'       => $category->info,
				'imgFullUrl' => $categoryImgFullUrl,
				'isLive'     => $category->is_live
			]);
		}

		// PIAB customer reviews reviews
		$customerReviews = config('evibe.piab-customer-reviews');

		$homeData = [
			'productCategoriesList' => $productCategoriesList,
			'seo'                   => [
				'pageTitle'       => "Latest party kits delivered to your doorstep",
				'pageDescription' => "Balloon decorations, table decorations, party props for your party, delivered to your doorstep",
				'pageHeader'      => "Party In A Box",
				'keyWords'        => "party in a box, home celebrations, diy celebration kits, party kits",
				'pageUrl'         => request()->capture()->fullUrl()
			],
			'cacheProductIds'       => 'PIAB',
			'customerReviews'       => $customerReviews
		];

		return $homeData;
	}

	public function getProductsListData($categoryUrl)
	{
		$productCategory = ProductCategory::where('url', $categoryUrl)->first();
		if (!$productCategory)
		{
			$this->sendNonExceptionErrorReportToTeam([
				                                         'code'      => config('evibe.error_code.fetch_function'),
				                                         'url'       => request()->fullUrl(),
				                                         'method'    => request()->method(),
				                                         'message'   => '[ProductController.php - ' . __LINE__ . '] ' . 'Unable to fetch PIAB category data.',
				                                         'exception' => '[ProductController.php - ' . __LINE__ . '] ' . 'Unable to fetch PIAB category data.',
				                                         'trace'     => '[Product Category Url: ' . $categoryUrl . ']',
				                                         'details'   => '[Product Category Url: ' . $categoryUrl . ']'
			                                         ]);

			return redirect(route('party-kits.home'));
		}

		$productTypes = Product::with('styles', 'styles.gallery')
		                       ->where('product_category_id', $productCategory->id)
		                       ->where('is_live', 1)
		                       ->whereNull('deleted_at')
		                       ->orderBy('priority', 'ASC')
		                       ->get();
		if (!count($productTypes))
		{
			$this->sendNonExceptionErrorReportToTeam([
				                                         'code'      => config('evibe.error_code.fetch_function'),
				                                         'url'       => request()->fullUrl(),
				                                         'method'    => request()->method(),
				                                         'message'   => '[ProductController.php - ' . __LINE__ . '] ' . 'Unable to fetch PIAB products for the category.',
				                                         'exception' => '[ProductController.php - ' . __LINE__ . '] ' . 'Unable to fetch PIAB products for the category.',
				                                         'trace'     => '[Product Category Url: ' . $categoryUrl . '] [Product Category Id: ' . $productCategory->id . ']',
				                                         'details'   => '[Product Category Url: ' . $categoryUrl . '] [Product Category Id: ' . $productCategory->id . ']'
			                                         ]);

			return redirect(route('party-kits.home'));
		}

		$cacheKey = $this->deviceSpecificCacheKey(request()->fullUrl());
		$cacheKey = base64_encode($cacheKey);
		if (Cache::has($cacheKey))
		{
			$listData = Cache::get($cacheKey);
		}
		else
		{
			$listData = $this->formProductsListData($productCategory, $productTypes);
			Cache::put($cacheKey, $listData, config('evibe.cache.refresh-time'));
		}

		return $this->renderDataToViewFiles('product/list', $listData);
	}

	public function formProductsListData($productCategory, $productTypes)
	{
		$metaImg = config('evibe.gallery.host') . '/main/img/piab/piab-cat-' . $productCategory->id . '.jpg';
		$partyKitsData = [];

		foreach ($productTypes as $productType)
		{
			$productStyles = $productType->styles;
			$productStyles = $productStyles->where('is_live', 1)->sortBy('priority');
			$stylesData = [];

			foreach ($productStyles as $productStyle)
			{
				$productStyleProfileImg = ProductStyleGallery::where('product_style_id', $productStyle->id)
				                                             ->where('is_profile', 1)
				                                             ->whereNull('deleted_at')
				                                             ->first();

				$styleProfileImgFullUrl = config('evibe.gallery.host') . '/product-styles/' . $productStyle->id . '/images/results/' . $productStyleProfileImg->url;

				array_push($stylesData, [
					'id'                => $productStyle->id,
					'url'               => $productStyle->url,
					'name'              => $productStyle->name,
					'code'              => $productStyle->code,
					'price'             => $productStyle->price,
					'priceWorth'        => $productStyle->price_worth,
					'profileImgFullUrl' => $styleProfileImgFullUrl,
				]);
			}

			array_push($partyKitsData, [
				'id'     => $productType->id,
				'url'    => $productType->url,
				'name'   => $productType->name,
				'code'   => $productType->code,
				'info'   => $productType->info,
				'styles' => $stylesData
			]);
		}

		// seo
		$pageTitle = $productCategory->name . ' Party Kits - Best Party Supplies - FREE Shipping';

		$productCategoriesList = [];
		$productCategories = ProductCategory::all();
		foreach ($productCategories as $category)
		{
			$categoryImgFullUrl = config('evibe.gallery.host') . '/main/img/piab/piab-cat-' . $category->id . '.jpg';

			array_push($productCategoriesList, [
				'id'         => $category->id,
				'url'        => $category->url,
				'name'       => $category->name,
				'info'       => $category->info,
				'imgFullUrl' => $categoryImgFullUrl,
				'isLive'     => $category->is_live
			]);
		}

		// PIAB customer reviews reviews
		$customerReviews = config('evibe.piab-customer-reviews');

		$listData = [
			'partyKitsData'     => $partyKitsData,
			'categoryName'      => $productCategory->name,
			'categoryUrl'       => $productCategory->url,
			'categoryId'        => $productCategory->id,
			'seo'               => [
				'pageTitle'       => $pageTitle,
				'pageDescription' => "Book the latest and trendy decorations, props, other party requirements in various styles and have them delivered to your doorsteps",
				'pageHeader'      => $productCategory->name,
				'keyWords'        => $productCategory->name . ", birthday props, birthday decorations, home decorations",
				'pageUrl'         => request()->capture()->fullUrl(),
				'metaImage'       => $metaImg
			],
			'productCategories' => $productCategoriesList,
			'customerReviews'   => $customerReviews
		];

		return $listData;
	}

	public function getProductProfileData($categoryUrl, $typeUrl, $styleUrl)
	{
		$productCategory = ProductCategory::where('url', $categoryUrl)->first();
		if (!$productCategory)
		{
			$this->sendNonExceptionErrorReportToTeam([
				                                         'code'      => config('evibe.error_code.fetch_function'),
				                                         'url'       => request()->fullUrl(),
				                                         'method'    => request()->method(),
				                                         'message'   => '[ProductController.php - ' . __LINE__ . '] ' . 'Unable to fetch PIAB category data.',
				                                         'exception' => '[ProductController.php - ' . __LINE__ . '] ' . 'Unable to fetch PIAB category data.',
				                                         'trace'     => '[Product Category Url: ' . $categoryUrl . ']',
				                                         'details'   => '[Product Category Url: ' . $categoryUrl . ']'
			                                         ]);

			return redirect(route('party-kits.home'));
		}

		$productType = Product::where('url', $typeUrl)->where('product_category_id', $productCategory->id)->first();
		if (!$productType)
		{
			$this->sendNonExceptionErrorReportToTeam([
				                                         'code'      => config('evibe.error_code.fetch_function'),
				                                         'url'       => request()->fullUrl(),
				                                         'method'    => request()->method(),
				                                         'message'   => '[ProductController.php - ' . __LINE__ . '] ' . 'Unable to fetch PIAB type data.',
				                                         'exception' => '[ProductController.php - ' . __LINE__ . '] ' . 'Unable to fetch PIAB type data.',
				                                         'trace'     => '[Product Category Url: ' . $categoryUrl . '] [Product Type Url: ' . $typeUrl . ']',
				                                         'details'   => '[Product Category Url: ' . $categoryUrl . '] [Product Type Url: ' . $typeUrl . ']'
			                                         ]);

			return redirect(route('party-kits.list', $categoryUrl));
		}

		$productStyle = ProductStyle::where('url', $styleUrl)->where('product_id', $productType->id)->first();
		if (!$productStyle)
		{
			$this->sendNonExceptionErrorReportToTeam([
				                                         'code'      => config('evibe.error_code.fetch_function'),
				                                         'url'       => request()->fullUrl(),
				                                         'method'    => request()->method(),
				                                         'message'   => '[ProductController.php - ' . __LINE__ . '] ' . 'Unable to fetch PIAB style data.',
				                                         'exception' => '[ProductController.php - ' . __LINE__ . '] ' . 'Unable to fetch PIAB style data.',
				                                         'trace'     => '[Product Category Url: ' . $categoryUrl . '] [Product Type Url: ' . $typeUrl . '] [Product Style Url: ' . $styleUrl . ']',
				                                         'details'   => '[Product Category Url: ' . $categoryUrl . '] [Product Type Url: ' . $typeUrl . '] [Product Style Url: ' . $styleUrl . ']'
			                                         ]);

			return redirect(route('party-kits.list', $categoryUrl));
		}

		$cacheKey = $this->deviceSpecificCacheKey(request()->fullUrl());
		$cacheKey = base64_encode($cacheKey);

		if (Cache::has($cacheKey))
		{
			$profileData = Cache::get($cacheKey);
		}
		else
		{
			$profileData = $this->formProductProfileData($productCategory, $productType, $productStyle);
			Cache::put($cacheKey, $profileData, config('evibe.cache.refresh-time'));
		}

		return $this->renderDataToViewFiles('product/profile', $profileData);
	}

	public function formProductProfileData($productCategory, $productType, $productStyle)
	{
		$gallery = [];
		$metaImg = null;

		// @todo: figuring out gallery paths
		// gallery
		$galleryObj = ProductStyleGallery::where('product_style_id', $productStyle->id)
		                                 ->orderBy('priority', 'ASC')
		                                 ->get();

		foreach ($galleryObj as $item)
		{
			if (!empty($item->getPath()))
			{
				array_push($gallery, [
					'productStyleId' => $item->product_style_id,
					'url'            => $item->getPath("profile"),
					'mobileUrl'      => $item->getPath("mobile"),
					'thumb'          => $item->getPath("thumbs"),
					'title'          => $item->title,
					'type'           => $item->type_id
				]);

				if (!$metaImg)
				{
					$metaImg = $item->getPath("profile");
				}
			}
		}

		$partyKitData = [
			'id'              => $productStyle->id,
			'name'            => $productStyle->name,
			'code'            => $productStyle->code,
			'url'             => $productStyle->url,
			'price'           => $productStyle->price,
			'priceWorth'      => $productStyle->price_worth,
			'info'            => $productStyle->info,
			'inclusions'      => $productStyle->inclusions,
			'exclusions'      => $productStyle->exclusions,
			'gallery'         => $gallery,
			'productType'     => [
				'id'   => $productType->id,
				'url'  => $productType->url,
				'name' => $productType->name
			],
			'productCategory' => [
				'id'   => $productCategory->id,
				'url'  => $productCategory->url,
				'name' => $productCategory->name,
			]
		];

		// seo
		$pageTitle = $productCategory->name . ' ' . $productType->name . ' Party Kits - Best Party Supplies - FREE Shipping';

		$productCategoriesList = [];
		$productCategories = ProductCategory::all();
		foreach ($productCategories as $category)
		{
			$categoryImgFullUrl = config('evibe.gallery.host') . '/main/img/piab/piab-cat-' . $category->id . '.jpg';

			array_push($productCategoriesList, [
				'id'         => $category->id,
				'url'        => $category->url,
				'name'       => $category->name,
				'info'       => $category->info,
				'imgFullUrl' => $categoryImgFullUrl,
				'isLive'     => $category->is_live
			]);
		}

		// party kits belonging to same category (excluding kits from same type)
		$similarPartyKits = [];
		// party kits belonging to same type
		$similarTypePartyKits = [];
		$similarProductTypes = Product::where('product_category_id', $productCategory->id)
			//->where('id', '!=', $productType->id)
			                          ->where('is_live', 1)
		                              ->whereNull('deleted_at')
		                              ->get();
		if (count($similarProductTypes))
		{
			foreach ($similarProductTypes as $similarProductType)
			{
				$similarProductStyles = $similarProductType->styles;

				if (count($similarProductStyles))
				{
					foreach ($similarProductStyles as $similarProductStyle)
					{
						if ($similarProductStyle->id == $productStyle->id)
						{
							continue;
						}
						$profileImgFullUrl = "";
						$profileThumbImgFullUrl = "";
						$productStyleProfileImgs = ProductStyleGallery::where('product_style_id', $similarProductStyle->id)
						                                              ->orderBy('priority')
						                                              ->whereNull('deleted_at')
						                                              ->get();

						foreach ($productStyleProfileImgs as $productStyleProfileImg)
						{
							if ($productStyleProfileImg->url && !$profileImgFullUrl)
							{
								$profileImgFullUrl = config('evibe.gallery.host') . '/product-styles/' . $similarProductStyle->id . '/images/results/' . $productStyleProfileImg->url;
								$profileThumbImgFullUrl = config('evibe.gallery.host') . '/product-styles/' . $similarProductStyle->id . '/images/thumbs/' . $productStyleProfileImg->url;
							}
						}

						$data = [
							'id'                     => $similarProductStyle->id,
							'name'                   => $similarProductStyle->name,
							'code'                   => $similarProductStyle->code,
							'url'                    => $similarProductStyle->url,
							'price'                  => $similarProductStyle->price,
							'priceWorth'             => $similarProductStyle->price_worth,
							'profileImgFullUrl'      => $profileImgFullUrl,
							'profileThumbImgFullUrl' => $profileThumbImgFullUrl,
							'productType'            => [
								'id'   => $similarProductType->id,
								'url'  => $similarProductType->url,
								'name' => $similarProductType->name
							],
							'productCategory'        => [
								'id'   => $productCategory->id,
								'url'  => $productCategory->url,
								'name' => $productCategory->name,
							]
						];

						if ($similarProductType->id == $productType->id)
						{
							// @see: need to implement through DB if necessary
							/*
							$styleName = strtolower($similarProductStyle->name);
							$imageName = "";
							if (strpos($styleName, 'pink') !== false) {
								$imageName  = $imageName . 'pink-';
							}
							if (strpos($styleName, 'blue') !== false) {
								$imageName  = $imageName . 'blue-';
							}
							if (strpos($styleName, 'gold') !== false) {
								$imageName  = $imageName . 'gold-';
							}
							if (strpos($styleName, 'red') !== false) {
								$imageName  = $imageName . 'red-';
							}
							if (strpos($styleName, 'white') !== false) {
								$imageName  = $imageName . 'white-';
							}
							if (strpos($styleName, 'black') !== false) {
								$imageName  = $imageName . 'black-';
							}
							if (strpos($styleName, 'multi') !== false) {
								$imageName  = $imageName . 'multi-';
							}

							$imageName = rtrim($imageName, '-');
							if(!$imageName)
							{
								$imageName = 'multi';
							}
							$styleColourImgFullUrl = config('evibe.gallery.host') . '/main/img/piab/' . $imageName . '.png';
							$data['styleColourFullUrl'] = $styleColourImgFullUrl;
							*/

							array_push($similarTypePartyKits, $data);
						}
						else
						{
							array_push($similarPartyKits, $data);
						}
					}
				}
			}
		}

		// PIAB customer reviews reviews
		$customerReviews = config('evibe.piab-customer-reviews');

		$profileData = [
			'partyKitData'         => $partyKitData,
			'seo'                  => [
				'pageTitle'       => $pageTitle,
				'pageDescription' => "Book the latest and trendy decorations, props, other party requirements in various styles and have them delivered to your doorsteps",
				'pageHeader'      => $productCategory->name,
				'keyWords'        => $productCategory->name . ", birthday props, birthday decorations, home decorations",
				'pageUrl'         => request()->capture()->fullUrl(),
				'metaImage'       => $metaImg
			],
			'productCategories'    => $productCategoriesList,
			'similarTypePartyKits' => $similarTypePartyKits,
			'similarPartyKits'     => $similarPartyKits,
			'productCategoryName'  => $productCategory->name,
			'customerReviews'      => $customerReviews
		];

		return $profileData;
	}
}