<?php

namespace App\Http\Controllers\AutoBook\ActionControllers;

use App\Http\Controllers\AutoBook\ActionControllers\Util\BasePackageAutoBookingActionController;

class VillaAutoBookingActionController extends BasePackageAutoBookingActionController
{
	public function showActionsToVendor($ticketId)
	{
		$data = $this->showActionsToVendorBasePackage($ticketId);
		if (!is_null($data))
		{
			return view('auto-booking/vendor/ask', ["data" => $data]);
		}

		return "<h1>Invalid Request</h1>";
	}

	public function confirmAutoBooking($ticketId)
	{
		return $this->confirmAutoBookingBasePackage($ticketId);
	}

	public function cancelAutoBooking($ticketId)
	{
		return $this->cancelAutoBookingBasePackage($ticketId);
	}

	public function showAcceptSuccessPage($ticketId)
	{
		$data = $this->showAcceptSuccessPageBasePackage($ticketId);

		return view('auto-booking/vendor/confirmed', ["data" => $data]);
	}

	public function showRejectSuccessPage($ticketId)
	{
		return $this->showRejectSuccessPageBasePackage($ticketId);
	}
}