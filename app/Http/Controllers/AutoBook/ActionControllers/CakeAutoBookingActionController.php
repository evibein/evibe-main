<?php

namespace App\Http\Controllers\AutoBook\ActionControllers;

use App\Http\Controllers\AutoBook\ActionControllers\Util\BaseAutoBookingActionController;
use App\Models\Ticket\Ticket;
use Evibe\Passers\AutoBookUtil;

class CakeAutoBookingActionController extends BaseAutoBookingActionController
{
	protected $weightName;
	protected $slotName;
	protected $deliveryChargeName;
	protected $cakeTypeName;
	protected $cakeFlavourName;
	protected $cakeMessageName;

	public function __construct()
	{
		parent::__construct();

		$this->weightName = $this->getCheckoutFieldName(config('evibe.checkout_field.cake.weight'));
		$this->slotName = $this->getCheckoutFieldName(config('evibe.checkout_field.cake.delivery_slot'));
		$this->deliveryChargeName = $this->getCheckoutFieldName(config('evibe.checkout_field.cake.delivery_charge'));
		$this->cakeTypeName = $this->getCheckoutFieldName(config('evibe.checkout_field.cake.type'));
		$this->cakeFlavourName = $this->getCheckoutFieldName(config('evibe.checkout_field.cake.flavour'));
		$this->cakeMessageName = $this->getCheckoutFieldName(config('evibe.checkout_field.cake.message'));
	}

	public function showActionsToVendor($ticketId)
	{
		$tId = new AutoBookUtil();
		$tId->setTicketId($ticketId);

		$data = $this->validateTicketBeforeAskingVendor($tId);
		if (!is_null($data))
		{
			$cakePrice = $this->getCakePrice($data);

			$data['cakeSlot'] = $data['additional'][$this->slotName];
			$data['cakeType'] = $data['additional'][$this->cakeTypeName];
			$data['cakeWeight'] = $data['additional'][$this->weightName];
			$data['cakeFlavour'] = $data['additional'][$this->cakeFlavourName];
			$data['cakeMessage'] = $data['additional'][$this->cakeMessageName];
			$data['cakeDeliveryCharge'] = $data['additional'][$this->deliveryChargeName];
			$data['cakePrice'] = $cakePrice['price'];
			$data['cakeOrderAmt'] = $cakePrice['orderAmount'];
			$data['balanceAmt'] = $cakePrice['balanceAmount'];
			$data['url'] = $ticketId . "?token=" . $data['dataToken'] . "&s=1";
			$data['acceptUrl'] = route('auto-book.cake.actions.partner.accept', $data['url']);
			$data['rejectUrl'] = route('auto-book.cake.actions.partner.reject', $data['url']);

			return view('auto-booking/vendor/cake/ask', ["data" => $data]);
		}

		return "<h1>Invalid Request</h1>";
	}

	public function confirmAutoBooking($ticketId)
	{
		$data = [
			'ticketId'      => $ticketId,
			'mapTypeId'     => config('evibe.ticket.type.cake'),
			'token'         => request()->input('token'),
			'redirectRoute' => route('auto-book.cake.actions.accept.success', $ticketId)
		];

		return $this->sendNotificationAndConfirmAutoBooking($data);
	}

	public function cancelAutoBooking($ticketId)
	{
		$data = [
			'ticketId'      => $ticketId,
			'mapTypeId'     => config('evibe.ticket.type.cake'),
			'token'         => request()->input('token'),
			'redirectRoute' => route('auto-book.cake.actions.reject.success', $ticketId)
		];

		return $this->sendNotificationAndRejectAutoBooking($data);
	}

	public function showAcceptSuccessPage($ticketId)
	{
		$ticket = Ticket::find($ticketId);
		$statusCheck = $this->checkStatusBeforeSendReceipts($ticket);
		$data = $this->getTicketAndSingleBookingData($ticket, $statusCheck->getBookings()->first());

		$cakePrice = $this->getCakePrice($data);
		$data['cakeSlot'] = $data['additional'][$this->slotName];
		$data['cakeType'] = $data['additional'][$this->cakeTypeName];
		$data['cakeWeight'] = $data['additional'][$this->weightName];
		$data['balanceAmt'] = $cakePrice['balanceAmount'];

		return view('auto-booking/vendor/cake/confirmed', ["data" => $data]);
	}

	public function showRejectSuccessPage($ticketId)
	{
		$ticket = Ticket::find($ticketId);

		if ($ticket)
		{
			$data = [
				'ticket'   => $ticket,
				'ticketId' => $ticketId
			];

			return view('auto-booking/vendor/cake/cancelled', ["data" => $data]);
		}
		else
		{
			return redirect(route("missing"));
		}
	}

	private function getCakePrice($data)
	{
		$price = $data['booking']['bookingAmount'];
		$balanceAmount = $this->formatPriceWithoutChar($data['booking']['bookingAmount'] - $data['booking']['advanceAmount']);
		$deliveryCharge = 0;

		if (!empty($data['additional'][$this->deliveryChargeName]))
		{
			$deliveryCharge = (int)$data['additional'][$this->deliveryChargeName];
		}

		if ($deliveryCharge)
		{
			$price = (int)($price - $deliveryCharge);
		}

		return [
			'price'         => $this->formatPriceWithoutChar($price),
			'orderAmount'   => $data['booking']['bookingAmount'],
			'balanceAmount' => $balanceAmount
		];
	}
}
