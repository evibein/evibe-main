<?php

namespace App\Http\Controllers\AutoBook\ActionControllers;

use App\Http\Controllers\AutoBook\ActionControllers\Util\BaseAutoBookingActionController;
use App\Models\Decor\Decor;
use App\Models\Ticket\Ticket;
use Evibe\Passers\AutoBookUtil;
use Illuminate\Support\Facades\Log;

class DecorAutoBookingActionController extends BaseAutoBookingActionController
{
	public function showActionsToVendor($ticketId)
	{
		$tId = new AutoBookUtil();
		$tId->setTicketId($ticketId);

		$data = $this->validateTicketBeforeAskingVendor($tId);
		if (!is_null($data))
		{
			$ticket = Ticket::find($ticketId);
			if (!$ticket)
			{
				return "<h1>Invalid Request</h1>";
			}
			$occasionId = $ticket->event_id;
			$decor = Decor::find($data['booking']['itemMapId']);

			if (!$decor)
			{
				return "<h1>Invalid Request</h1>";
			}
			$baseBookingAmount = $decor->min_price;

			// get decor checkout fields with values
			$decorFields = $this->getDecorCheckoutFields($occasionId, $data['booking']['typeTicketBookingId']);
			$decorCheckoutFieldsWithValues = $this->getDecorCheckoutFieldsWithValuesInArray($decorFields, $data['ticketId']);

			$url = $ticketId . "?token=" . $data['dataToken'] . "&s=1";
			$bookingAmount = $data['booking']['bookingAmount'];
			$advanceAmount = $data['booking']['advanceAmount'];
			$data = array_merge($data, [
				"decorPrice"     => $baseBookingAmount,
				"deliveryAmt"    => $bookingAmount - $baseBookingAmount,
				"balanceAmt"     => $bookingAmount - $advanceAmount,
				"checkoutFields" => $decorCheckoutFieldsWithValues,
				"url"            => $url,
				"acceptUrl"      => route('auto-book.decor.actions.partner.accept', $url),
				"rejectUrl"      => route('auto-book.decor.actions.partner.reject', $url)
			]);

			return view('auto-booking/vendor/decor/ask', ["data" => $data]);
		}

		return "<h1>Invalid Request</h1>";
	}

	public function confirmAutoBooking($ticketId)
	{
		$data = [
			'ticketId'      => $ticketId,
			'mapTypeId'     => config('evibe.ticket.type.decor'),
			'token'         => request()->input('token'),
			'redirectRoute' => route('auto-book.decor.actions.accept.success', $ticketId)
		];

		return $this->sendNotificationAndConfirmAutoBooking($data);
	}

	public function cancelAutoBooking($ticketId)
	{
		$data = [
			'ticketId'      => $ticketId,
			'mapTypeId'     => config('evibe.ticket.type.decor'),
			'token'         => request()->input('token'),
			'redirectRoute' => route('auto-book.decor.actions.reject.success', $ticketId)
		];

		return $this->sendNotificationAndRejectAutoBooking($data);
	}

	public function showAcceptSuccessPage($ticketId)
	{
		$ticket = Ticket::find($ticketId);
		$statusCheck = $this->checkStatusBeforeSendReceipts($ticket);
		$data = $this->getTicketAndSingleBookingData($ticket, $statusCheck->getBookings()->first());

		// get decor checkout fields with values
		$occasionId = $ticket->event_id;
		$decorFields = $this->getDecorCheckoutFields($occasionId, $data['booking']['typeTicketBookingId']);
		$decorCheckoutFieldsWithValues = $this->getDecorCheckoutFieldsWithValuesInArray($decorFields, $data['ticketId']);

		$data['balanceAmt'] = $data['booking']['bookingAmount'] - $data['booking']['advanceAmount'];
		$data['checkoutFields'] = $decorCheckoutFieldsWithValues;

		return view('auto-booking/vendor/decor/confirmed', ["data" => $data]);
	}

	public function showRejectSuccessPage($ticketId)
	{
		$ticket = Ticket::find($ticketId);

		if ($ticket)
		{
			$data = [
				'ticket'   => $ticket,
				'ticketId' => $ticketId
			];

			return view('auto-booking/vendor/decor/cancelled', ["data" => $data]);
		}
		else
		{
			return redirect(route("missing"));
		}
	}
}
