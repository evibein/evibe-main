<?php

namespace App\Http\Controllers\AutoBook\ActionControllers;

use App\Http\Controllers\AutoBook\ActionControllers\Util\BaseAutoBookingActionController;
use App\Models\Ticket\Ticket;

use Evibe\Passers\AutoBookUtil;

class VenueDealsAutoBookingActionController extends BaseAutoBookingActionController
{
	public function showActionsToVendor($ticketId)
	{
		$tId = new AutoBookUtil();
		$tId->setTicketId($ticketId);
		$data = $this->validateTicketBeforeAskingVendor($tId);

		if (!is_null($data))
		{
			$data['url'] = $ticketId . "?token=" . $data['dataToken'] . "&s=1";
			$data['acceptUrl'] = route('auto-book.venue-deals.actions.vendor.accept', $data['url']);
			$data['rejectUrl'] = route('auto-book.venue-deals.actions.vendor.reject', $data['url']);

			return view('auto-booking/vendor/venue-deal/ask', ["data" => $data]);
		}

		return "<h1>Invalid Request</h1>";
	}

	public function confirmAutoBooking($ticketId)
	{
		$data = [
			'ticketId'      => $ticketId,
			'mapTypeId'     => config('evibe.ticket.type.package'),
			'token'         => request()->input('token'),
			'redirectRoute' => route('auto-book.venue-deals.actions.accept.success', $ticketId)
		];

		return $this->sendNotificationAndConfirmAutoBooking($data);
	}

	public function cancelAutoBooking($ticketId)
	{
		$data = [
			'ticketId'      => $ticketId,
			'mapTypeId'     => config('evibe.ticket.type.package'),
			'token'         => request()->input('token'),
			'redirectRoute' => route('auto-book.venue-deals.actions.reject.success', $ticketId)
		];

		return $this->sendNotificationAndRejectAutoBooking($data);
	}

	public function showAcceptSuccessPage($ticketId)
	{
		$ticket = Ticket::find($ticketId);
		$statusCheck = $this->checkStatusBeforeSendReceipts($ticket);
		$data = $this->getTicketAndSingleBookingData($ticket, $statusCheck->getBookings()->first());

		return view('auto-booking/vendor/confirmed', ["data" => $data]);
	}

	public function showRejectSuccessPage($ticketId)
	{
		$ticket = Ticket::find($ticketId);

		if ($ticket)
		{
			$data = [
				'ticket'   => $ticket,
				'ticketId' => $ticketId
			];

			return view('auto-booking/vendor/cancelled', ["data" => $data]);
		}
		else
		{
			return redirect(route("missing"));
		}
	}
}
