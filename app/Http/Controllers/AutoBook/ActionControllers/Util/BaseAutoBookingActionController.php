<?php

namespace App\Http\Controllers\AutoBook\ActionControllers\Util;

use App\Http\Controllers\AutoBook\HandlerControllers\Util\BaseAutoBookingHandlerController;
use App\Models\Ticket\Ticket;

use Evibe\Passers\AutoBookUtil;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class BaseAutoBookingActionController extends BaseAutoBookingHandlerController
{
	public function showActionsToVendor($ticketId, $partnerId, $partnerTypeId)
	{
		// @see: important !!!
		/*
		 * Assumptions:
		 * + Per Auto Booking, one partner will be assigned
		 * + One partner can have multiple bookings
		 * + [ Accept / Reject will be for a partner rather than ticket ] - ideally this should be the case, but for now, accept / reject will be for a ticket like usual for PRIMARY BOOKING (not add-ons), but all the bookings will be considered as available.
		 *   Notifications to partners will be sent like wise.
		 * + Accept / Reject in DASH will also repeat the above logic.
		 */
		// todo: Change once add-to cart is launched. [Also backend logic to confirm partial order, diff. partners, etc,.]

		$actionIds = new AutoBookUtil();
		$actionIds->setTicketId($ticketId);
		$actionIds->setPartnerId($partnerId);
		$actionIds->setPartnerTypeId($partnerTypeId);

		$data = $this->validateTicketBeforeAskingVendor($actionIds);
		if (!is_null($data))
		{
			$ticket = Ticket::find($ticketId);
			if (!$ticket)
			{
				return "<h1>Invalid Request</h1>";
			}

			$urlIds = [$ticketId, $partnerId, $partnerTypeId];
			$urlParams = "?token=" . $data['dataToken'] . "&s=1";
			$url = $ticketId . '/' . $partnerId . '/' . $partnerTypeId . "?token=" . $data['dataToken'] . "&s=1";

			$minBookingDateTime = 0;
			$maxBookingDateTime = 0;
			$totalBookingAmount = 0;
			$totalAdvancePaid = 0;
			$totalBalanceAmount = 0;

			foreach ($data['bookings'] as $booking)
			{
				if (!$minBookingDateTime)
				{
					$minBookingDateTime = $booking['partyDateTime'];
				}

				if (!$maxBookingDateTime)
				{
					$maxBookingDateTime = $booking['partyDateTime'];
				}

				if ($booking['partyDateTime'] < $minBookingDateTime)
				{
					$minBookingDateTime = $booking['partyDateTime'];
				}

				if ($booking['partyDateTime'] > $maxBookingDateTime)
				{
					$maxBookingDateTime = $booking['partyDateTime'];
				}

				$totalBookingAmount += $booking['bookingAmount'];
				$totalAdvancePaid += $booking['advanceAmount'];
				$totalBalanceAmount += $booking['bookingAmount'] - $booking['advanceAmount'];
			}

			$data['totalBookingAmount'] = $totalBookingAmount;
			$data['totalBookingAmountStr'] = $totalBookingAmount ? $this->formatPrice($totalBookingAmount) : null;
			$data['totalAdvancePaid'] = $totalAdvancePaid;
			$data['totalAdvancePaidStr'] = $totalAdvancePaid ? $this->formatPrice($totalAdvancePaid) : null;
			$data['totalBalanceAmount'] = $totalBalanceAmount;
			$data['totalBalanceAmountStr'] = $totalBalanceAmount ? $this->formatPrice($totalBalanceAmount) : null;

			$data['minBookingDateTime'] = $minBookingDateTime;
			$data['maxBookingDateTime'] = $maxBookingDateTime;

			$partyDateTime = getPartyDateTimeByBookingsPartyTime($data['minBookingDateTime'], $data['maxBookingDateTime']);
			$data['partyDateTime'] = $partyDateTime;
			$data['typeTicketAddOn'] = config('evibe.ticket.type.add-on');

			$data = array_merge($data, [
				"url"       => $url,
				"acceptUrl" => route('auto-book.actions.partner.accept', $urlIds) . $urlParams,
				"rejectUrl" => route('auto-book.actions.partner.reject', $urlIds) . $urlParams
			]);

			return view('auto-booking/vendor/ask-multiple-bookings', ["data" => $data]);
		}

		return "<h1>Invalid Request</h1>";
	}

	public function confirmAutoBooking($ticketId, $partnerId, $partnerTypeId)
	{
		$ticket = Ticket::find($ticketId);
		if (!$ticket)
		{
			Log::error("unable to find ticket: $ticketId");
			// todo: report error
		}

		$mapTypeId = $ticket->type_ticket_id;
		if (in_array($ticket->type_ticket_id, [
			config('evibe.ticket.type.package'),
			config('evibe.ticket.type.resorts'),
			config('evibe.ticket.type.villas'),
			config('evibe.ticket.type.lounges'),
			config('evibe.ticket.type.food'),
			config('evibe.ticket.type.surprises'),
			config('evibe.ticket.type.priests'),
			config('evibe.ticket.type.tents'),
			config('evibe.ticket.type.generic-package'),
		]))
		{
			$mapTypeId = config('evibe.ticket.type.package');
		}

		$data = [
			'ticketId'      => $ticketId,
			'typeTicketId'  => $ticket->type_ticket_id,
			'mapTypeId'     => $mapTypeId,
			'token'         => request()->input('token'),
			'redirectRoute' => route('auto-book.actions.accept.success', [$ticketId, $partnerId, $partnerTypeId])
		];

		return $this->sendNotificationAndConfirmPartnerBookings($data);
	}

	public function cancelAutoBooking($ticketId, $partnerId, $partnerTypeId)
	{
		$ticket = Ticket::find($ticketId);
		if (!$ticket)
		{
			Log::error("unable to find ticket: $ticketId");
			// todo: report error
		}

		$mapTypeId = $ticket->type_ticket_id;
		if (in_array($ticket->type_ticket_id, [
			config('evibe.ticket.type.package'),
			config('evibe.ticket.type.resorts'),
			config('evibe.ticket.type.villas'),
			config('evibe.ticket.type.lounges'),
			config('evibe.ticket.type.food'),
			config('evibe.ticket.type.surprises'),
			config('evibe.ticket.type.priests'),
			config('evibe.ticket.type.tents'),
			config('evibe.ticket.type.generic-package'),
		]))
		{
			$mapTypeId = config('evibe.ticket.type.package');
		}

		$data = [
			'ticketId'      => $ticketId,
			'typeTicketId'  => $ticket->type_ticket_id,
			'mapTypeId'     => $mapTypeId,
			'token'         => request()->input('token'),
			'redirectRoute' => route('auto-book.actions.reject.success', [$ticketId, $partnerId, $partnerTypeId])
		];

		return $this->sendNotificationAndRejectPartnerBookings($data);
	}

	public function showAcceptSuccessPage($ticketId, $partnerId, $partnerTypeId)
	{
		$ticket = Ticket::find($ticketId);
		$statusCheck = $this->checkStatusBeforeSendReceipts($ticket);
		$data = $this->getTicketData($ticket);
		$ticketBookings = $statusCheck->getBookings();

		$minBookingDateTime = 0;
		$maxBookingDateTime = 0;
		$totalBookingAmount = 0;
		$totalAdvancePaid = 0;
		$totalBalanceAmount = 0;

		// to unset $data['bookings']['list'];
		$data['bookings'] = [];

		// @see: to check the validity of the requesting partner
		$requestPartner = false;

		foreach ($ticketBookings as $ticketBooking)
		{
			if ($ticketBooking->map_id == $partnerId && $ticketBooking->map_type_id == $partnerTypeId)
			{
				$requestPartner = true;

				$mappedValues = $this->fillMappingValues([
					                                         'isCollection' => false,
					                                         'mapId'        => $ticketBooking->mapping->map_id,
					                                         'mapTypeId'    => $ticketBooking->mapping->map_type_id
				                                         ]);
				$bookingData = $this->getTicketBookingData($ticketBooking, $mappedValues);

				if (isset($bookingData['provider']) && !(isset($data['self'])))
				{
					$data = array_merge($data, $bookingData['provider']);
				}

				array_push($data['bookings'], $bookingData);

				if (!$minBookingDateTime)
				{
					$minBookingDateTime = $bookingData['partyDateTime'];
				}

				if (!$maxBookingDateTime)
				{
					$maxBookingDateTime = $bookingData['partyDateTime'];
				}

				if ($bookingData['partyDateTime'] < $minBookingDateTime)
				{
					$minBookingDateTime = $bookingData['partyDateTime'];
				}

				if ($bookingData['partyDateTime'] > $maxBookingDateTime)
				{
					$maxBookingDateTime = $bookingData['partyDateTime'];
				}

				$totalBookingAmount += $bookingData['bookingAmount'];
				$totalAdvancePaid += $bookingData['advanceAmount'];
				$totalBalanceAmount += $bookingData['bookingAmount'] - $bookingData['advanceAmount'];
			}
		}
		
		if (!$requestPartner)
		{
			return "<h1>Sorry! you do not have access to this order.</h1>";
		}

		$data['totalBookingAmount'] = $totalBookingAmount;
		$data['totalBookingAmountStr'] = $totalBookingAmount ? $this->formatPrice($totalBookingAmount) : 0;
		$data['totalAdvancePaid'] = $totalAdvancePaid;
		$data['totalAdvancePaidStr'] = $totalAdvancePaid ? $this->formatPrice($totalAdvancePaid) : 0;
		$data['totalBalanceAmount'] = $totalBalanceAmount;
		$data['totalBalanceAmountStr'] = $totalBalanceAmount ? $this->formatPrice($totalBalanceAmount) : 0;

		$data['minBookingDateTime'] = $minBookingDateTime;
		$data['maxBookingDateTime'] = $maxBookingDateTime;

		$partyDateTime = getPartyDateTimeByBookingsPartyTime($data['minBookingDateTime'], $data['maxBookingDateTime']);
		$data['partyDateTime'] = $partyDateTime;

		$data['decorTypeId'] = config('evibe.ticket.type.decor');
		$data['venueDealsTypeId'] = config('evibe.ticket.type.venue-deals');
		$data['venuePartnerTypeId'] = config('evibe.ticket.type.venue');

		return view('auto-booking/vendor/confirmed-multiple-bookings', ["data" => $data]);
	}

	public function showRejectSuccessPage($ticketId, $partnerId, $partnerTypeId)
	{
		$ticket = Ticket::find($ticketId);

		if ($ticket)
		{
			$data = [
				'ticket'   => $ticket,
				'ticketId' => $ticketId
			];

			return view('auto-booking/vendor/cancelled-multiple-bookings', ["data" => $data]);
		}
		else
		{
			return redirect(route("missing"));
		}
	}

	public function validateTicketBeforeAskingVendor(AutoBookUtil $abUtil)
	{
		$data = null;
		$ticketId = $abUtil->getTicketId();
		$partnerId = $abUtil->getPartnerId();
		$partnerTypeId = $abUtil->getPartnerTypeId();
		$ticket = Ticket::find($ticketId);
		$token = request()->input('token');

		if (!$ticket)
		{
			Log::error("Could not find ticket with id $ticketId");

			return $data;
		}

		$statusCheck = $this->checkStatusBeforeSendReceipts($ticket);
		if (!$statusCheck->getSuccess())
		{
			Log::error($statusCheck->getErrorMessage());

			return $data;
		}

		if (!Hash::check($ticket->phone, $token))
		{
			return $data;
		}

		if ($ticket->status_id == config('evibe.ticket.status.auto_cancel'))
		{
			Log::error("Trying to check the availability for Auto Cancel ticket, Ticket Id :: " . $ticketId);

			return $data;
		}

		$dataToken = Hash::make($ticket->phone . $ticket->created_at);

		if ($partnerId && $partnerTypeId)
		{
			// partner specific auto bookings
			$data = $this->getTicketAndPartnerBookingsData($ticket, $statusCheck->getBookings(), $partnerId, $partnerTypeId);
			$presentPartnerId = (isset($data['id']) && $data['id']) ? $data['id'] : null;
			$presentPartnerTypeId = (isset($data['providerTypeId']) && $data['providerTypeId']) ? $data['providerTypeId'] : null;

			// @see: if there are not bookings,
			if (isset($data['bookings']) && !(count($data['bookings'])))
			{
				Log::error("Trying to accept/reject request of another partner. Partner might have been changed");

				return null;
			}
		}
		else
		{
			// single auto booking (old)
			$data = $this->getTicketAndSingleBookingData($ticket, $statusCheck->getBookings()->first());

			// @see: although single bookings will also go to the above case,
			$presentPartnerId = (isset($data['booking']['provider']['id']) && $data['booking']['provider']['id']) ? $data['booking']['provider']['id'] : null;
			$presentPartnerTypeId = (isset($data['booking']['provider']['providerTypeId']) && $data['booking']['provider']['providerTypeId']) ? $data['booking']['provider']['providerTypeId'] : null;
		}

		if ($partnerId && $partnerTypeId && $presentPartnerId && $presentPartnerTypeId)
		{
			if (($partnerId != $presentPartnerId) || ($partnerTypeId != $presentPartnerTypeId))
			{
				Log::error("Trying to accept/reject request of another partner. Partner might have been changed");

				return null;
			}
		}

		$data['dataToken'] = $dataToken;

		return $data;
	}
}