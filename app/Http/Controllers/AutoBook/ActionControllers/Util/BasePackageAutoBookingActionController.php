<?php

namespace App\Http\Controllers\AutoBook\ActionControllers\Util;

use App\Models\Ticket\Ticket;
use Evibe\Passers\AutoBookUtil;

class BasePackageAutoBookingActionController extends BaseAutoBookingActionController
{
	public function showActionsToVendorBasePackage($ticketId)
	{
		$abUtil = new AutoBookUtil();
		$abUtil->setTicketId($ticketId);
		$data = $this->validateTicketBeforeAskingVendor($abUtil);

		if (!is_null($data))
		{
			$typeTicketName = config('evibe.auto-book.' . $data['additional']['typeTicketId']) ? config('evibe.auto-book.' . $data['additional']['typeTicketId']) : 'generic-package';
			$data['url'] = $ticketId . "?token=" . $data['dataToken'] . "&s=1";
			$data['acceptUrl'] = route('auto-book.' . $typeTicketName . '.actions.vendor.accept', $data['url']);
			$data['rejectUrl'] = route('auto-book.' . $typeTicketName . '.actions.vendor.reject', $data['url']);
		}

		return $data;
	}

	public function confirmAutoBookingBasePackage($ticketId)
	{
		$ticket = Ticket::find($ticketId);
		$typeTicketId = $ticket->type_ticket_id;
		$typeTicketName = config('evibe.auto-book.' . $typeTicketId) ? config('evibe.auto-book.' . $typeTicketId) : 'generic-package';
		$data = [
			'ticketId'      => $ticketId,
			'mapTypeId'     => config('evibe.ticket.type.package'),
			'token'         => request()->input('token'),
			'redirectRoute' => route('auto-book.' . $typeTicketName . '.actions.accept.success', $ticketId)
		];

		return $this->sendNotificationAndConfirmAutoBooking($data);
	}

	public function cancelAutoBookingBasePackage($ticketId)
	{
		$ticket = Ticket::find($ticketId);
		$typeTicketId = $ticket->type_ticket_id;
		$typeTicketName = config('evibe.auto-book.' . $typeTicketId) ? config('evibe.auto-book.' . $typeTicketId) : 'generic-package';
		$data = [
			'ticketId'      => $ticketId,
			'mapTypeId'     => config('evibe.ticket.type.package'),
			'token'         => request()->input('token'),
			'redirectRoute' => route('auto-book.' . $typeTicketName . '.actions.reject.success', $ticketId)
		];

		return $this->sendNotificationAndRejectAutoBooking($data);
	}

	public function showAcceptSuccessPageBasePackage($ticketId)
	{
		$ticket = Ticket::find($ticketId);
		$statusCheck = $this->checkStatusBeforeSendReceipts($ticket);
		$data = $this->getTicketAndSingleBookingData($ticket, $statusCheck->getBookings()->first());

		return $data;
	}

	public function showRejectSuccessPageBasePackage($ticketId)
	{
		$ticket = Ticket::find($ticketId);

		if ($ticket)
		{
			$data = [
				'ticket'   => $ticket,
				'ticketId' => $ticketId
			];

			return view('auto-booking/vendor/cancelled', ["data" => $data]);
		}
		else
		{
			return redirect(route("missing"));
		}
	}
}