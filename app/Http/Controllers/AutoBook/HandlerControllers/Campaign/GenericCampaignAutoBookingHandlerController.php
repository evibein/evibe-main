<?php

namespace App\Http\Controllers\AutoBook\HandlerControllers\Campaign;

use App\Http\Controllers\AutoBook\HandlerControllers\Util\BaseAutoBookingHandlerController;
use App\Jobs\Emails\Payment\Customer\MailAutoBookingCampaignAlertToCustomerJob;
use App\Jobs\Emails\Payment\Team\MailAutoBookingCampaignAlertToTeamJob;
use App\Jobs\Emails\Payment\Vendor\MailAutoBookingCampaignAlertToPartnerJob;
use App\Jobs\HandlePostPaymentProcessJob;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketBooking;
use App\Models\Ticket\TicketMapping;
use App\Models\Util\SiteErrorLog;
use Carbon\Carbon;
use Evibe\Passers\AutoBookUtil;
use Evibe\Passers\PaySuccessUpdateTicket;
use Illuminate\Support\Facades\Hash;

class GenericCampaignAutoBookingHandlerController extends BaseAutoBookingHandlerController
{
	public function initAutoBooking($packageId, $campaignEventId, $uploadImage)
	{
		$res = ['success' => false];

		try
		{
			// all are packages
			$typeTicketId = config('evibe.ticket.type.package');
			$previousUrl = url()->previous();

			// get package details along with provider
			$entityData = $this->getEntityDetailsByMapValues($packageId, $typeTicketId);
			$provider = $entityData['provider'];

			// deadline calculation
			// if booked before 12 today, put deadline tomorrow 8 AM
			// if booked after 12 today, put deadline tomorrow 4 PM
			// reminder will be sent at 9 AM and 5 PM everyday for un-acted tickets.
			$currentHour = date("H"); // returns in 24 Hrs format
			$deadlineDateAndTime = strtotime("today 4 PM");
			if ($currentHour > 12)
			{
				$deadlineDateAndTime = strtotime("tomorrow 8 AM");
			}

			// create ticket with appropriate details
			$ticketInsertData = [
				'event_id'                => $campaignEventId ? $campaignEventId : $entityData['event_id'], // default form package
				'is_auto_booked'          => 1,
				'type_ticket_id'          => $entityData['type_ticket_id'],
				'status_id'               => config('evibe.ticket.status.initiated'),
				'auto_book_deadline_date' => $deadlineDateAndTime,
				'created_at'              => Carbon::now(),
				'updated_at'              => Carbon::now(),
				'enquiry_source_id'       => config('evibe.ticket.enquiry_source.autobook')
			];

			if ($ticketInsertData['event_id'] == config('evibe.occasion.raksha-bandhan.id'))
			{
				$ticketInsertData['event_date'] = config('evibe.occasion.raksha-bandhan.event-date');
			}

			$ticket = Ticket::create($ticketInsertData);

			if (!$ticket)
			{
				SiteErrorLog::create([
					                     'url'        => request()->fullUrl(),
					                     'exception'  => strtoupper(config('evibe.auto-book.' . $typeTicketId)) . " AutoBooking: Error while creating ticket",
					                     'code'       => "Error",
					                     'details'    => "Some error occurred in GenericCampaignAutoBookingHandlerController [Line: " . __LINE__ . "], while creating a ticket",
					                     'created_at' => date('Y-m-d H:i:s'),
					                     'updated_at' => date('Y-m-d H:i:s')
				                     ]);

				return $res;
			}

			$this->updateTicketAction([
				                          'ticket'   => $ticket,
				                          'comments' => "Auto booking ticket created by customer"
			                          ]);

			// save enquiry id
			$ticketId = $ticket->id;
			$enquiryId = config('evibe.ticket.enq.pre.auto_booked') . $ticketId;
			$ticket->enquiry_id = $enquiryId;
			$ticket->save();

			// calculate total booking amount, advance amount [ 100% advance ]
			$totalBookingAmount = $entityPrice = $entityData['price'];
			$advanceAmount = $totalBookingAmount;

			// create ticket mapping
			$mappingInsertData = [
				'ticket_id'   => $ticketId,
				'map_id'      => $packageId,
				'map_type_id' => $typeTicketId,
				'created_at'  => Carbon::now(),
				'updated_at'  => Carbon::now()
			];

			// create the mapping of package to the ticket
			$ticketMapping = TicketMapping::create($mappingInsertData);
			if (!$ticketMapping)
			{
				SiteErrorLog::create([
					                     'url'        => request()->fullUrl(),
					                     'exception'  => strtoupper(config('evibe.auto-book.' . $typeTicketId)) . " AutoBooking: Error while creating ticket mapping",
					                     'code'       => "Error",
					                     'details'    => "Some error occurred in GenericCampaignAutoBookingHandlerController [Line: " . __LINE__ . "], while creating ticket mapping",
					                     'created_at' => date('Y-m-d H:i:s'),
					                     'updated_at' => date('Y-m-d H:i:s')
				                     ]);

				return $res;
			}
			$ticketMappingId = $ticketMapping->id;

			// create booking info: html full url
			$bookingInfo = "<a href='" . $previousUrl . "?ref=pay-checkout' target='_blank'>" . $entityData['name'] . "</a>";

			// create ticket booking
			$bookingInsertData = [
				'booking_id'             => $enquiryId,
				'booking_info'           => $bookingInfo,
				'booking_amount'         => $totalBookingAmount,
				'advance_amount'         => $advanceAmount,
				'map_id'                 => $provider['id'],
				'map_type_id'            => $entityData['map_type_id'],
				'ticket_id'              => $ticketId,
				'ticket_mapping_id'      => $ticketMappingId,
				'package_price'          => $entityPrice,
				'is_venue_booking'       => 0,
				'type_ticket_booking_id' => config('evibe.booking_type.raksha-bandhan'),
				'booking_type_details'   => ucwords(config('evibe.auto-book.' . $typeTicketId)),
				'created_at'             => Carbon::now(),
				'updated_at'             => Carbon::now()
			];

			if ($ticketInsertData['event_id'] == config('evibe.occasion.raksha-bandhan.id'))
			{
				$bookingInsertData['party_date_time'] = config('evibe.occasion.raksha-bandhan.event-date');
			}

			$ticketBooking = TicketBooking::create($bookingInsertData);
			if (!$ticketBooking)
			{
				SiteErrorLog::create([
					                     'url'        => request()->fullUrl(),
					                     'exception'  => strtoupper(config('evibe.auto-book.' . $typeTicketId)) . " AutoBooking: Error while creating ticket booking",
					                     'code'       => "Error",
					                     'details'    => "Some error occurred in GenericCampaignAutoBookingHandlerController [Line: " . __LINE__ . "], while creating ticket booking",
					                     'created_at' => date('Y-m-d H:i:s'),
					                     'updated_at' => date('Y-m-d H:i:s')
				                     ]);

				return $res;
			}

			$checkoutLink = route('auto-book.campaign.generic-campaign.pay.auto.checkout');
			$checkoutLink .= "?id=" . $ticketId;
			$checkoutLink .= "&token=" . Hash::make($ticketId);
			$checkoutLink .= "&uKm8=" . Hash::make(config('evibe.campaign-token'));
			$checkoutLink .= "&upImg=" . $uploadImage;

			if ($checkoutLink)
			{
				return $res = [
					'success'     => true,
					'redirectUrl' => $checkoutLink
				];
			}

		} catch (\Exception $e)
		{
			SiteErrorLog::create([
				                     'url'        => request()->fullUrl(),
				                     'exception'  => "Campaign AutoBooking: Some error occurred while processing Package AB init",
				                     'code'       => $e->getCode(),
				                     'details'    => $e->getMessage(),
				                     'created_at' => date('Y-m-d H:i:s'),
				                     'updated_at' => date('Y-m-d H:i:s')
			                     ]);

			if (isset($ticket))
			{
				$ticket->forceDelete();
			}
		}
	}

	public function showCheckoutPage()
	{
		$ticketId = request()->input('id');
		$token = request()->input('token');
		$hashedStr = request()->input('uKm8');
		$uploadImage = request()->input('upImg');
		$typeToken = 2; // for campaigns without mobile number

		$dataForValidation = new AutoBookUtil();
		$dataForValidation->setTicketId($ticketId);
		$dataForValidation->setToken($token);
		$dataForValidation->setHashedStr($hashedStr);
		$dataForValidation->setTypeToken($typeToken);
		$ticket = $this->validateTicket($dataForValidation);

		if (!is_null($ticket))
		{
			$ticketBooking = $ticket->bookings;
			$booking = $ticketBooking->first();

			$data = $this->getTicketData($ticket);
			$data['additional']['extraGuests'] = 0;
			$totalBookingAmount = $booking->booking_amount;
			$totalAdvanceToPay = ($booking->is_advance_paid == 1) ? 0 : $booking->advance_amount;

			// get mapId and mapTypeId from mapping
			$mapTypeId = $booking->mapping->map_type_id;
			$mapId = $booking->mapping->map_id;

			$mappedValues = $this->fillMappingValues([
				                                         'isCollection' => false,
				                                         'mapId'        => $mapId,
				                                         'mapTypeId'    => $mapTypeId
			                                         ]);

			$data['booking'] = $this->getTicketBookingData($booking, $mappedValues);
			$data['couponToken'] = $this->getAccessTokenForCoupon(intval($ticketId), $ticket->created_at);

			$provider = $data['booking']['provider']['self'];
			$data['additional']['itemInfo'] = $mappedValues['info'];

			$data['booking']['totalBookingAmount'] = $totalBookingAmount;
			$data['booking']['totalAdvanceToPay'] = $totalAdvanceToPay;
			$data['isVenueBooking'] = $booking->is_venue_booking;
			$data['uploadImage'] = $uploadImage;

			$typeCampaignName = config('evibe.auto-book.campaign.' . $ticket->event_id) ? config('evibe.auto-book.campaign.' . $ticket->event_id) : 'base';
			$redirectView = 'pay/checkout/auto/campaign/' . $typeCampaignName;

			return view($redirectView, ['data' => $data]);
		}
		else
		{
			SiteErrorLog::create([
				                     'url'        => request()->fullUrl(),
				                     'exception'  => "Unauthorized Access",
				                     'code'       => 403,
				                     'details'    => "Some error occurred in GenericCampaignAutoBookingHandlerController [Line: " . __LINE__ . "]. Token mismatch [Ticket id: $ticketId, token: $token, hashedStr: $hashedStr]",
				                     'created_at' => date('Y-m-d H:i:s'),
				                     'updated_at' => date('Y-m-d H:i:s')
			                     ]);

			return "<h1>Invalid Request</h1>"; // ticketId, token mismatch
		}
	}

	public function processPayment($ticketId)
	{
		$response = [
			'success'    => false,
			'error'      => 'Failed to send booked notifications',
			'redirectTo' => route('pay.common.fail')
		];

		if ($this->sendBookedNotifications($ticketId))
		{
			$token = request()->input('token');
			$response['success'] = true;
			$response['redirectTo'] = route('auto-book.campaign.generic-campaign.pay.auto.success', $ticketId) . "?token=" . $token;
			unset($response['error']);

			// handle post payment
			$this->dispatch(new HandlePostPaymentProcessJob($ticketId));
		}

		return response()->json($response);
	}

	public function onPaymentSuccess($ticketId)
	{
		$token = request()->input('token');
		$ticket = Ticket::with('bookings.mapping', 'bookings.provider')->find($ticketId);
		if (!$ticket)
		{
			return "<h1>Invalid Request</h1>";
		}
		$ticketBookings = $ticket->bookings;
		$booking = $ticketBookings[0];

		if (Hash::check($ticket->phone, $token))
		{
			$data = $this->getTicketAndSingleBookingData($ticket, $booking);

			return view('pay/success/generic-campaign', ["data" => $data]);
		}
		else
		{
			SiteErrorLog::create([
				                     'url'        => request()->fullUrl(),
				                     'exception'  => "Campaign AutoBooking: Tried to hack",
				                     'code'       => "Error",
				                     'details'    => "Some error occurred in GenericCampaignAutoBookingHandlerController [Line: " . __LINE__ . "]. Tried to change the ticket id with invalid token :: $ticketId",
				                     'created_at' => date('Y-m-d H:i:s'),
				                     'updated_at' => date('Y-m-d H:i:s')
			                     ]);

			return "<h1>Invalid Request</h1>";
		}
	}

	private function sendBookedNotifications($ticketId, $isFromDash = false, $handlerId = null)
	{
		$dataForNotification = new PaySuccessUpdateTicket();
		$dataForNotification->setTicketNotificationId($ticketId);
		$dataForNotification->setIsFromDash($isFromDash);
		$dataForNotification->setHandlerId($handlerId);

		$data = $this->getNotificationData($dataForNotification);
		if (!is_null($data))
		{
			// vendor ask page URL
			$token = Hash::make($data['customer']['phone']);

			// send notification to customer, vendor, team
			$this->initBookedNotificationToCustomer($data);
			$this->initBookedNotificationToPartner($data);
			$this->dispatch(new MailAutoBookingCampaignAlertToTeamJob($data));

			$this->createAutoBookingNotificationToApp($data['firstBooking']);

			// update timestamp & save action
			$forUpdateData = new PaySuccessUpdateTicket();
			$forUpdateData->setHandlerId($handlerId);
			$forUpdateData->setIsFromDash($isFromDash);
			$forUpdateData->setStatusId(config('evibe.ticket.status.booked'));
			$forUpdateData->setSuccessEmailType(config('evibe.ticket.success_type.booked'));

			return $this->updateTicketPostSendReceipts($data['ticket'], $forUpdateData);
		}

		return false;
	}

	private function initBookedNotificationToPartner($data)
	{
		//$tplVendor = config('evibe.sms_tpl.auto_book.decor.pay_success.partner');
		//$replaces = [
		//	'#field1#' => $data['booking']['provider']['person'],
		//	'#field2#' => date('d-M-y', strtotime($data['booking']['checkInDate'])) . ' (' . date('D', strtotime($data['booking']['checkInDate'])) . '), ' . $data['booking']['checkInTime'],
		//	'#field3#' => $this->truncateText($data['additional']['venueLocation'], 10),
		//	'#field4#' => $this->truncateText($data['booking']['name'], 10),
		//	'#field5#' => $this->formatPrice($data['booking']['advanceAmount']),
		//	'#field6#' => $this->getShortenUrl($data['vendorAskUrl'])
		//];
		//
		//$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplVendor);
		//$smsData = [
		//	'to'   => $data['booking']['provider']['phone'],
		//	'text' => $smsText
		//];

		// send email and sms to partner
		$this->dispatch(new MailAutoBookingCampaignAlertToPartnerJob($data));
		//$this->dispatch(new SMSCampaignAutoBookingAlertToPartner($smsData));
	}

	private function initBookedNotificationToCustomer($data)
	{
		//$tplCustomer = config('evibe.sms_tpl.auto_book.decor.pay_success.customer');
		//$replaces = [
		//	'#field1#' => $data['customer']['name'],
		//	'#field2#' => $this->formatPrice($data['booking']['advanceAmount']),
		//	'#field3#' => $this->truncateText($data['booking']['name'], 10),
		//	'#field4#' => date('d-M-y', strtotime($data['booking']['checkInDate'])) . ' (' . date('D', strtotime($data['booking']['checkInDate'])) . '), ' . $data['booking']['checkInTime'],
		//	'#field5#' => $data['additional']['venueLocation'],
		//	'#field6#' => $data['customer']['email']
		//];
		//$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);
		//$smsData = [
		//	'to'   => $data['customer']['phone'],
		//	'text' => $smsText
		//];

		// Send email and sms to customer
		$this->dispatch(new MailAutoBookingCampaignAlertToCustomerJob($data));
		//$this->dispatch(new SMSCampaignAutoBookingAlertToCustomer($smsData));
	}
}