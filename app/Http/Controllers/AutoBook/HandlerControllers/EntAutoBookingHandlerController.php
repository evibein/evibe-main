<?php

namespace App\Http\Controllers\AutoBook\HandlerControllers;

use App\Evibe\Implementers\HandlesPayments;
use App\Http\Controllers\AutoBook\HandlerControllers\Util\BaseAutoBookingHandlerController;
use App\Jobs\Emails\Payment\Customer\MailAutoBookingServiceAlertToCustomerJob;
use App\Jobs\Emails\Payment\Team\MailAutoBookingServiceAlertToTeamJob;
use App\Jobs\HandlePostPaymentProcessJob;
use App\Jobs\SMS\SMSServiceAutoBookingAlertToCustomerJob;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketBooking;
use App\Models\Ticket\TicketMapping;
use App\Models\Types\TypeServices;
use App\Models\Util\City;
use Evibe\Passers\AutoBookUtil;
use Evibe\Passers\PaySuccessUpdateTicket;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;

class EntAutoBookingHandlerController extends BaseAutoBookingHandlerController implements HandlesPayments
{
	public function initAutoBooking($mapId, $occasionId)
	{
		$products = [];
		array_push($products, [
			'productId'     => $mapId,
			'productTypeId' => config('evibe.ticket.type.entertainment'),
			'isAddOn'       => false
		]);
		$addOns = request('addOns');
		if (!is_null($addOns) && count($addOns) > 0)
		{
			foreach ($addOns as $addOnId => $addOnCount)
			{
				if ($addOnCount > 0)
				{
					array_push($products, [
						'productId'     => $addOnId,
						'productTypeId' => config('evibe.ticket.type.add-on'),
						'isAddOn'       => true,
						'unitCount'     => $addOnCount
					]);
				}
			}
		}
		$res = $this->validateAndInitAutoBooking($products, $occasionId);

		return response()->json($res);
	}

	public function initEntAutoBooking($mapId, $occasionId)
	{
		$res = ['success' => false];

		try
		{
			// validate user data
			$rules = [
				'phone'     => 'required|phone',
				'partyDate' => 'required|date|after:today',
				'partyTime' => 'required'
			];

			$messages = [
				'phone.required'     => 'Please enter your phone number',
				'phone.phone'        => 'Phone number is invalid (ex: 9640204000)',
				'partyDate.required' => 'Please select your party date',
				'partyDate.date'     => 'Please select a valid party date',
				'partyTime.required' => 'Party time is required'
			];

			$validator = validator(request()->input(), $rules, $messages);

			if ($validator->fails())
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => $validator->messages()->first()
				                        ]);
			}

			$ent = TypeServices::find($mapId);
			if (!$ent)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => "could not find the service"
				                        ]);
			}

			$ticketTypeId = config('evibe.ticket.type.entertainment');

			// collect user input
			$phone = request('phone');
			$partyDate = request()->input('partyDate', false);
			$partyTime = request()->input('partyTime', null);

			$totalBookAmount = $ent->min_price;
			$tokenAdvance = $this->getRoundedTokenAmount($totalBookAmount);
			$advanceSelect = request('advanceSelect', 0);

			if ($advanceSelect && $advanceSelect == 1)
			{
				$tokenAdvance = $totalBookAmount;
			}

			$rawCheckInTime = $partyTime;
			$checkInTime = date("H:i", strtotime($rawCheckInTime));
			$checkInDate = date('Y-m-d', strtotime(str_replace('-', '/', $partyDate)));
			$checkInDateTime = strtotime("$checkInDate $checkInTime");

			// deadline calculation
			// if booked before 12 today, put deadline tomorrow 8 AM
			// if booked after 12 today, put deadline tomorrow 4 PM
			// reminder will be sent at 9 AM and 5 PM everyday for un-acted tickets.
			$currentHour = date("H"); // returns in 24 Hrs format
			$deadlineDateAndTime = strtotime("today 4 PM");
			if ($currentHour > 12)
			{
				$deadlineDateAndTime = strtotime("tomorrow 8 AM");
			}

			// create ticket with appropriate details
			$ticketInsertData = [
				'phone'                   => $phone,
				'event_date'              => $checkInDateTime,
				'type_ticket_id'          => $ticketTypeId,
				'is_auto_booked'          => 1,
				'status_id'               => config('evibe.ticket.status.initiated'),
				'city_id'                 => getCityId(),
				'auto_book_deadline_date' => $deadlineDateAndTime,
				'event_id'                => $occasionId,
				'created_at'              => date('Y-m-d H:i:s'),
				'updated_at'              => date('Y-m-d H:i:s'),
			];

			// Create the ticket with phone number and event date or check in date
			$ticket = Ticket::create($ticketInsertData);

			if (!$ticket)
			{
				Log::error("AUTO_BOOKING_ERROR (ENT):: while creating ticket");

				return response()->json($res);
			}

			// save enquiry id
			$ticketId = $ticket->id;
			$enquiryId = config("evibe.ticket.enq.pre.auto_booked") . $ticketId;
			$ticket->enquiry_id = $enquiryId;
			$ticket->save();

			// create ticket mapping
			$mappingInsertData = [
				'ticket_id'   => $ticketId,
				'map_id'      => $mapId,
				'map_type_id' => config('evibe.ticket.type.service'),
				'created_at'  => date('Y-m-d H:i:s'),
				'updated_at'  => date('Y-m-d H:i:s')
			];

			// create the mapping of package to the ticket
			$ticketMapping = TicketMapping::create($mappingInsertData);
			if (!$ticketMapping)
			{
				Log::error("AUTO_BOOKING_ERROR_ENT :: while creating ticket mapping");

				return response()->json($res);
			}
			$ticketMappingId = $ticketMapping->id;

			$previousUrl = url()->previous();

			// create booking info: html full url
			$bookingInfo = "<a href='" . $previousUrl . "?ref=pay-checkout' target='_blank'>" . $ent->name . "</a>";

			// create ticket booking
			$bookingInsertData = [
				'booking_id'             => $enquiryId,
				'booking_info'           => $bookingInfo,
				'party_date_time'        => $checkInDateTime,
				'check_in'               => $rawCheckInTime,
				'booking_amount'         => $totalBookAmount,
				'advance_amount'         => $tokenAdvance,
				'ticket_id'              => $ticketId,
				'ticket_mapping_id'      => $ticketMappingId,
				'is_venue_booking'       => 0,
				'type_ticket_booking_id' => config('evibe.booking_type.ent'),
				'booking_type_details'   => "Service Auto Booking",  // ucwords(config('evibe.auto-book.' . $ticketTypeId)),
				'created_at'             => date('Y-m-d H:i:s'),
				'updated_at'             => date('Y-m-d H:i:s')
			];

			$ticketBooking = TicketBooking::create($bookingInsertData);

			if (!$ticketBooking)
			{
				Log::error("AUTO_BOOKING_ERROR :: while creating ticket booking");

				return response()->json($res);
			}

			$checkoutLink = route('auto-book.ent.pay.auto.checkout');
			$checkoutLink .= "?id=" . $ticketId;
			$checkoutLink .= "&token=" . Hash::make($ticketId);
			$checkoutLink .= "&uKm8=" . Hash::make($phone);

			if ($checkoutLink)
			{
				return response()->json(['success' => true, 'redirectUrl' => $checkoutLink]);
			}
		} catch (Exception $e)
		{
			Log::error("Some error occurred while initialising the ent auto booking");

			if (isset($ticket))
			{
				$ticket->forceDelete();
			}
		}
	}

	public function showCheckoutPage()
	{
		$ticketId = request()->input('id');
		$token = request()->input('token');
		$hashedStr = request()->input('uKm8');

		$dataForValidation = new AutoBookUtil();
		$dataForValidation->setTicketId($ticketId);
		$dataForValidation->setToken($token);
		$dataForValidation->setHashedStr($hashedStr);

		$ticket = $this->validateTicket($dataForValidation);
		if (!is_null($ticket))
		{
			$ticketBooking = $ticket->bookings;
			$booking = $ticketBooking->first();

			$data = $this->getTicketData($ticket);

			$totalBookingAmount = $booking->booking_amount;
			$totalAdvanceToPay = ($booking->is_advance_paid == 1) ? 0 : $booking->advance_amount;

			// get mapId and mapTypeId from mapping
			$mapTypeId = $booking->mapping->map_type_id;
			$mapId = $booking->mapping->map_id;

			$mappedValues = $this->fillMappingValues([
				                                         'isCollection' => false,
				                                         'mapId'        => $mapId,
				                                         'mapTypeId'    => $mapTypeId
			                                         ]);

			$ent = TypeServices::find($mapId);
			$baseBookingAmount = $ent->min_price;

			$data['booking'] = $this->getTicketBookingData($booking, $mappedValues);
			$data['couponToken'] = $this->getAccessTokenForCoupon(intval($ticketId), $ticket->created_at);

			$data['additional']['delivery'] = $totalBookingAmount - $baseBookingAmount;
			$data['additional']['itemInfo'] = $mappedValues['info'];

			$data['booking']['baseBookingAmount'] = $baseBookingAmount;
			$data['booking']['totalBookingAmount'] = $totalBookingAmount;
			$data['booking']['totalAdvanceToPay'] = $totalAdvanceToPay;
			$data['isVenueBooking'] = $booking->is_venue_booking;

			return view('pay/checkout/auto/ent', ['data' => $data]);
		}
		else
		{
			Log::error("Ticket, token mismatch::ticketId::" . $ticketId . "::token::" . $token . "::hashedStr::" . $hashedStr);

			return "<h1>Invalid Request</h1>"; // ticketId, token mismatch
		}
	}

	public function processPayment($ticketId)
	{
		$response = [
			'success'    => false,
			'error'      => 'Failed to send booked notifications',
			'redirectTo' => route('pay.common.fail')
		];

		if ($this->sendBookedNotifications($ticketId))
		{
			$token = request()->input('token');
			$response['success'] = true;
			$response['redirectTo'] = route('auto-book.ent.pay.auto.success', $ticketId) . "?token=" . $token;
			unset($response['error']);

			// handle post payment
			$this->dispatch(new HandlePostPaymentProcessJob($ticketId));
		}

		return response()->json($response);
	}

	public function onPaymentSuccess($ticketId)
	{
		$token = request()->input('token');
		$ticket = Ticket::with('bookings.mapping')->find($ticketId);
		if (!$ticket)
		{
			return "<h1>Invalid Request</h1>";
		}
		$ticketBookings = $ticket->bookings;
		$booking = $ticketBookings[0];
		$city = City::find($ticket->city_id);
		$cityUrl = $city->url;

		if (Hash::check($ticket->phone, $token))
		{
			$data = $this->getTicketAndSingleBookingData($ticket, $booking);
			$data['eventId'] = $ticket->event_id;
			$data = array_merge($data, $this->mergeView($cityUrl, $ticket->event_id));

			checkAndSetCitySessions($city);

			return view('pay/success/ent', ["data" => $data]);
		}
		else
		{
			Log::error("HACK :: Tried to change the ticket id with invalid token :: $ticketId");

			return "<h1>Invalid Request</h1>";
		}
	}

	private function sendBookedNotifications($ticketId, $isFromDash = false, $handlerId = null)
	{
		$dataForNotification = new PaySuccessUpdateTicket();
		$dataForNotification->setTicketNotificationId($ticketId);
		$dataForNotification->setIsFromDash($isFromDash);
		$dataForNotification->setHandlerId($handlerId);

		$data = $this->getNotificationData($dataForNotification);
		if (!is_null($data))
		{
			// vendor ask page URL
			$token = Hash::make($data['customer']['phone']);
			$confirmationLink = route('auto-book.decor.actions.partner.ask', $data['ticketId']) . "?token=" . $token;
			$data['vendorAskUrl'] = $this->getShortenUrl($confirmationLink);

			// send notification to customer, team
			$this->initBookedNotificationToCustomer($data);
			$this->dispatch(new MailAutoBookingServiceAlertToTeamJob($data));

			//$this->createAutoBookingNotificationToApp($data['firstBooking']);

			// update timestamp & save action
			$forUpdateData = new PaySuccessUpdateTicket();
			$forUpdateData->setHandlerId($handlerId);
			$forUpdateData->setIsFromDash($isFromDash);
			$forUpdateData->setStatusId(config('evibe.ticket.status.service_auto_pay'));
			$forUpdateData->setSuccessEmailType(config('evibe.ticket.success_type.booked'));

			return $this->updateTicketPostSendReceipts($data['ticket'], $forUpdateData);
		}

		return false;
	}

	private function initBookedNotificationToCustomer($data)
	{
		$tplCustomer = config('evibe.sms_tpl.auto_book.decor.pay_success.customer');
		$token = Hash::make($data["ticketId"] . "EVBTMO");

		$replaces = [
			'#field1#' => $data['customer']['name'],
			'#field2#' => $this->formatPrice($data['booking']['advanceAmount']),
			'#field3#' => $data['ticket']['enquiry_id'],
			'#field4#' => "4",
			'#field5#' => $this->getShortenUrl(route("track.orders") . "?ref=SMS&id=" . $data["ticketId"] . "&token=" . $token)
		];
		$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);
		$smsData = [
			'to'   => $data['customer']['phone'],
			'text' => $smsText
		];

		// Send email and sms to customer
		$this->dispatch(new MailAutoBookingServiceAlertToCustomerJob($data));
		$this->dispatch(new SMSServiceAutoBookingAlertToCustomerJob($smsData));
	}
}