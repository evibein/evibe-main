<?php

namespace App\Http\Controllers\AutoBook\HandlerControllers\Util;

use App\Http\Controllers\Base\BaseCakeController;
use App\Http\Controllers\Payments\BasePaymentController;
use App\Http\Controllers\Util\TicketsController;
use App\Jobs\Emails\Payment\PaymentInitiationUpdate;
use App\Jobs\Emails\Payment\PaymentTransactionInfoUpdate;
use App\Jobs\Emails\Errors\MailLocationMismatchAlertToTeamJob;
use App\Jobs\Emails\Util\MailCustomerEnquireForNonActiveProduct;
use App\Jobs\Emails\Util\MailLocationVerificationAlertToTeam;
use App\Jobs\Emails\Util\MailTeamCustomerEnquireForNonActiveProduct;
use App\Jobs\Emails\Errors\MailTransportChargesErrorToTeamJob;
use App\Jobs\Emails\Payment\Customer\MailAutoBookingPaymentReceiptToCustomerJob;
use App\Jobs\Emails\Payment\Team\MailABAlertToTeamJob;
use App\Jobs\Emails\Payment\Vendor\MailAutoBookingPaymentAlertToPartnerJob;
use App\Jobs\Emails\ReportIssue\MissingCityAlertToTeam;
use App\Jobs\HandleAutoBookingNotificationToApp;
use App\Jobs\HandleAutoBookingOrderNotificationToApp;
use App\Jobs\HandleAutoBookingPaymentErrorNotificationJob;
use App\Jobs\HandlePostPaymentProcessJob;
use App\Jobs\SMS\Payment\Customer\SMSAutoBookingPaymentSuccessToCustomerJob;
use App\Jobs\SMS\Payment\Vendor\SMSAutoBookingPaymentAlertToPartnerJob;
use App\Jobs\SMS\SMSActiveNonLiveToCustomer;
use App\Models\AddOn\AddOn;
use App\Models\Cake\Cake;
use App\Models\Decor\Decor;
use App\Models\Package\Package;
use App\Models\Package\PackageTag;
use App\Models\PartyBag\ShortlistOptions;
use App\Models\Ticket\TicketBooking;
use App\Models\Ticket\TicketBookingGallery;
use App\Models\Ticket\TicketMapping;
use App\Models\Ticket\TicketUpdate;
use App\Models\Types\TypeProof;
use App\Models\Types\TypeServices;
use App\Models\Types\TypeTicket;
use App\Models\Util\Area;
use App\Models\Util\CheckoutField;
use App\Models\Util\CheckoutFieldValue;
use App\Models\Util\City;
use App\Models\Util\Country;
use App\Models\Util\CustomerOrderProof;
use App\Models\Util\DeliverySlot;
use App\Models\Util\SiteErrorLog;
use App\Models\Util\StarOption;
use App\Models\Util\User;
use App\Models\Vendor\Vendor;
use App\Models\Wallet\Wallet;
use Carbon\Carbon;
use Evibe\Passers\AutoBookUtil;
use Evibe\Passers\PaySuccessUpdateTicket;
use Evibe\Passers\PriceCheck;
use Evibe\Passers\Util\LogAjax;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use App\Models\Ticket\Ticket;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use ReCaptcha\ReCaptcha;

class BaseAutoBookingHandlerController extends BasePaymentController
{
	public function validateAndInitAutoBooking($products, $eventId = null)
	{
		try
		{
			if (!count($products))
			{
				return $res = [
					'success' => false,
					'error'   => 'At least one product should be selected to create AutoBooking ticket'
				];
			}

			$primeProductsCount = 0;
			$primeProduct = [];

			$productPackageTypes = [
				config('evibe.ticket.type.package'),
				config('evibe.ticket.type.resorts'),
				config('evibe.ticket.type.villas'),
				config('evibe.ticket.type.lounges'),
				config('evibe.ticket.type.food'),
				config('evibe.ticket.type.surprises'),
				config('evibe.ticket.type.priests'),
				config('evibe.ticket.type.tents'),
				config('evibe.ticket.type.generic-package'),
			];

			$productSpecific['rules'] = [
				config('evibe.ticket.type.decor')         => [
					'partyPinCode' => 'required|digits:6',
					'partyDate'    => 'required|date|after:today'
				],
				config('evibe.ticket.type.cake')          => [
					'name'             => 'required',
					'email'            => 'required|email',
					'cakeDeliveryDate' => 'date|after:today'
				],
				config('evibe.ticket.type.package')       => [
					'checkInDate' => 'sometimes|required|date',
					'guestsCount' => 'sometimes|required',
					'partyTime'   => 'sometimes|required'
				],
				config('evibe.ticket.type.entertainment') => [
					'partyDate' => 'required|date|after:today',
					'partyTime' => 'required'
				]
			];
			$productSpecific['messages'] = [
				config('evibe.ticket.type.decor')         => [
					'partyPinCode.required' => 'Please enter the pincode of your party venue',
					'partyPinCode.digits'   => 'Please enter a valid pincode'
				],
				config('evibe.ticket.type.cake')          => [
					'name.required'    => 'Please enter your name',
					'email.required'   => 'Please enter your email',
					'email.email'      => 'Please enter a valid email',
					'cakeDeliveryDate' => 'Please enter delivery date after today'
				],
				config('evibe.ticket.type.package')       => [
					'checkInDate.required' => 'Please select your check in date',
					'checkInDate.date'     => 'Please select a valid check in date',
					'guestsCount.required' => 'Guests count is required',
					'partyTime.required'   => 'Party time is required'
				],
				config('evibe.ticket.type.entertainment') => [
					'partyDate.required' => 'Please select your party date',
					'partyDate.date'     => 'Please select a valid party date',
					'partyTime.required' => 'Party time is required'
				]
			];

			// validate user data
			// mobile made options to support removal of phone number for packages
			$rules = [
				'phone' => 'required|digits:10',
			];

			$messages = [
				'phone.required' => 'Please enter your mobile number',
				'phone.digits'   => 'Phone number is invalid (ex: 9640204000)',
			];

			foreach ($products as $product)
			{
				$productTypeId = $product['productTypeId'];

				if (in_array($productTypeId, $productPackageTypes))
				{
					$productTypeId = config('evibe.ticket.type.package');
				}
				$rules = isset($productSpecific['rules'][$productTypeId]) ? array_merge($rules, $productSpecific['rules'][$productTypeId]) : $rules;
				$messages = isset($productSpecific['messages'][$productTypeId]) ? array_merge($messages, $productSpecific['messages'][$productTypeId]) : $messages;

				if (!$product['isAddOn'])
				{
					// last non-add-on product will become prime product
					$primeProductsCount++;
					$primeProduct = $product;
				}
			}

			if (!$primeProductsCount)
			{
				return $res = [
					'success' => false,
					'error'   => 'Payment cannot be made for add-ons without any product'
				];
			}

			$validator = validator(request()->input(), $rules, $messages);

			if ($validator->fails())
			{
				return $res = [
					'success' => false,
					'error'   => $validator->messages()->first()
				];
			}

			// Allowing CLD booking on same date till 6 PM
			// Allowing non-CLD booking on same date till 12 PM
			/* Now surprises packages are being managed based on 'book_before_hrs' - check in packages switch case below
			foreach ($products as $product)
			{
				if ($product["productTypeId"] == config("evibe.ticket.type.surprises"))
				{
					$plannerPackageTag = PackageTag::where("planner_package_id", $product["productId"])
					                               ->where("tile_tag_id", config("evibe.surprise-relation-tags.candle-light-dinner"))
					                               ->first();

					if (($plannerPackageTag && (strtotime(request("checkInDate")) == Carbon::today()->startOfDay()->timestamp) && (Carbon::now()->timestamp > Carbon::today()->startOfDay()->addHours(18)->timestamp)) ||
						(!$plannerPackageTag && (strtotime(request("checkInDate")) == Carbon::today()->startOfDay()->timestamp) && (Carbon::now()->timestamp > Carbon::today()->startOfDay()->addHours(12)->timestamp)))
					{
						return $res = [
							'success' => false,
							'error'   => 'Sorry, this package is not available for your selected date. Please try another date.'
						];
					}
				}
			}
			*/

			// collect user input
			$phone = request('phone');
			$callingCode = request('callingCode') ? request('callingCode') : null;
			$partyPinCode = request('partyPinCode') ? request('partyPinCode') : null;

			$name = request('name');
			$email = request('email');

			// deadline calculation
			// if booked before 12 today, put deadline tomorrow 8 AM
			// if booked after 12 today, put deadline tomorrow 4 PM
			// reminder will be sent at 9 AM and 5 PM everyday for un-acted tickets.
			$currentHour = date("H"); // returns in 24 Hrs format
			$deadlineDateAndTime = strtotime("today 4 PM");
			if ($currentHour > 12)
			{
				$deadlineDateAndTime = strtotime("tomorrow 8 AM");
			}

			$cityId = 1; // default: Bangalore
			$cityId = (getCityId() > 0) ? getCityId() : $cityId;

			$city = City::find($cityId);
			checkAndSetCitySessions($city);

			// @see: disabling booking for unwanted cities
			$validCityIds = [
				// config('evibe.city.Bengaluru'),
				// config('evibe.city.Hyderabad')
			];
			if (in_array($cityId, $validCityIds)) {
				// proceed
			} else {
				return $res = [
					'success' => false,
					'error'   => 'Due to unforeseen reasons, we\'re not accepting new orders. Sorry for the inconvenience!'
				];
			}

			// create ticket with appropriate details
			$ticketInsertData = [
				'name'                    => $name,
				'email'                   => $email,
				'phone'                   => $phone,
				'calling_code'            => $callingCode,
				'is_auto_booked'          => 1,
				'status_id'               => config('evibe.ticket.status.initiated'),
				'city_id'                 => $cityId,
				'auto_book_deadline_date' => $deadlineDateAndTime,
				'event_id'                => $eventId ?: 1, // default, which will be updated later
				'zip_code'                => $partyPinCode,
				'created_at'              => date('Y-m-d H:i:s'),
				'updated_at'              => date('Y-m-d H:i:s'),
				'enquiry_source_id'       => config('evibe.ticket.enquiry_source.autobook')
			];

			if ($primeProductsCount == 1)
			{
				$ticketInsertData['type_ticket_id'] = $primeProduct['productTypeId'];
			}

			// Create or update the ticket with phone number and event date or check in date
			if ($phone && strlen($phone) == 10)
			{
				$pastTicket = Ticket::where('phone', 'LIKE', $phone)
				                    ->where('created_at', '>', Carbon::now()->subDays(10)->startOfDay())
				                    ->where("is_auto_booked", 1)
				                    ->where('ticket.status_id', config('evibe.ticket.status.initiated'))
				                    ->first();

				if ($pastTicket)
				{
					$pastTicket->update([
						                    "status_id" => config("evibe.ticket.status.duplicate")
					                    ]);

					TicketUpdate::create([
						                     'ticket_id'   => $pastTicket->id,
						                     'status_id'   => config("evibe.ticket.status.duplicate"),
						                     'type_update' => config('evibe.ticket.type_update.auto'),
						                     'comments'    => "Customer is trying to book from another ticket." . ' <a href="' . $_SERVER['HTTP_REFERER'] . '">Option Link</a>',
						                     'status_at'   => time(),
						                     'created_at'  => date('Y-m-d H:i:s'),
						                     'updated_at'  => date('Y-m-d H:i:s')
					                     ]);
				}
			}

			$ticket = Ticket::create($ticketInsertData);

			if (!$ticket)
			{
				SiteErrorLog::create([
					                     'url'        => request()->fullUrl(),
					                     'exception'  => "AutoBooking: Error while creating ticket",
					                     'code'       => "Error",
					                     'details'    => "Some error occurred in BaseAutoBookingHandlerController [Line: " . __LINE__ . "], while creating a ticket",
					                     'created_at' => date('Y-m-d H:i:s'),
					                     'updated_at' => date('Y-m-d H:i:s')
				                     ]);

				return $res = ['success' => false];
			}

			$this->updateTicketAction([
				                          'ticket'   => $ticket,
				                          'comments' => "Auto booking ticket created by customer. " . '<a href="' . $_SERVER['HTTP_REFERER'] . '">Option Link</a>'
			                          ]);

			// save enquiry id
			$ticketId = $ticket->id;
			$enquiryId = config("evibe.ticket.enq.pre.auto_booked") . $ticketId;
			$ticket->enquiry_id = $enquiryId;
			$previousUrl = url()->previous();

			// todo: even if providers are not there, make auto payment to all bookings? similar to services
			foreach ($products as $product)
			{
				// booking related defaults
				$bookingInfo = null;
				$productPrice = 0;
				$bookingAmount = 0;
				$advanceAmount = 0;
				$typeTicketBookingId = null;
				$productValidity = false;
				$estimatedTransCharges = null;
				$transCharges = null;
				$partnerId = null;
				$partnerTypeId = null;
				$isVenueBooking = 0;
				$partyDateTime = 0;
				$partyEndTime = null;
				$bookingUnits = 1;
				$profileImageUrl = null;

				switch ($product['productTypeId'])
				{
					case config('evibe.ticket.type.decor'):

						$transportCharges = request('transportCharges') ? request('transportCharges') : null;
						$totalBookAmount = request('totalBookAmount');
						$tokenAdvance = request('tokenAdvance');
						$partyDate = request('partyDate') ? strtotime(str_replace("-", "/", request("partyDate"))) : null;

						$ticket->event_date = $partyDate;
						$decor = Decor::with("tags")->find($product['productId']);
						if (!$decor)
						{
							//return $res = [
							//	'success' => false,
							//	'error'   => "could not find decor"
							//];

							// todo: report error

							continue;
						}

						$productValidity = true;
						$productPrice = $decor->min_price;

						// verify partner and fallback for city_id
						$partner = Vendor::find($decor->provider_id);
						if (!$partner)
						{
							//return $res = [
							//	'success' => false,
							//	'error'   => "could not find decor partner"
							//];

							// todo: report error

							continue;
						}
						$partnerId = $partner ? $partner->id : null;
						$partnerTypeId = config('evibe.ticket.type.planner');
						$isVenueBooking = 0;

						// if no city for provider || city of provider doesn't match session
						if ((!$partner->city_id) || ($partner->city_id && ($cityId != $partner->city_id)))
						{
							$optionData = [
								'name' => $decor->name,
								'code' => $decor->code,
								'type' => config('evibe.type-product-title.' . ($product['productTypeId']))
							];

							$this->dispatch(new MissingCityAlertToTeam($optionData));
						}

						// todo: form booking info
						// create booking info: html full url
						$productUrl = $this->fetchProductUrl($cityId, $eventId, $product['productTypeId'], $product['productId'], $decor->url);
						$bookingInfo = "<a href='" . $productUrl . "?ref=pay-checkout' target='_blank'>" . $decor->name . "</a>";

						// get type ticket booking id from tags
						$typeTicketBookingId = $this->getTypeBookingIdFromTags($decor);

						// verify booking and token amount
						$providerPinCode = $partner->zip;
						$res = $this->makeCurlRequestToGoogleMatrixApi($providerPinCode, $partyPinCode);
						if (($res['status'] == 'OK') && ($res['rows'][0]['elements'][0]['status'] === 'OK'))
						{
							$partyDist = $res['rows'][0]['elements'][0]['distance']['value'];
							$distFreeMeters = ($decor->kms_free ? $decor->kms_free : config('evibe.transport.free-kms')) * 1000;
							$distMaxMeters = ($decor->kms_max) * 1000;
							$baseBookingAmount = $decor->min_price;
							$transMin = $decor->trans_min;
							$transMax = $decor->trans_max;
							$diffDist = $distMaxMeters - $distFreeMeters;
							$diffTransFare = $transMax - $transMin;
							if ($diffDist > 0)
							{
								$farePerMeter = $diffTransFare / $diffDist;
							}
							else
							{
								$farePerMeter = 0;
								$typeTicketName = TypeTicket::find($product['productTypeId'])->name;
								$typeProvider = TypeTicket::find($partnerTypeId)->name;

								$data = [
									'ticketId'     => $ticketId,
									'optionInfo'   => $bookingInfo,
									'optionCode'   => $decor->code,
									'typeTicket'   => $typeTicketName,
									'providerName' => $decor->provider->name,
									'providerCode' => $decor->provider->code,
									'typeProvider' => $typeProvider
								];
								$this->dispatch(new MailTransportChargesErrorToTeamJob($data));
							}

							$extraDist = $partyDist - $distFreeMeters;
							if ($extraDist > 0)
							{
								$transCharges = intval($extraDist * $farePerMeter);
							}
							else
							{
								$transCharges = 0;
							}
							$estimatedTransCharges = $transCharges;
							if ($partyDist <= $distFreeMeters)
							{
								$transCharges = 0;
							}
							$totalBookingAmount = $transCharges + $baseBookingAmount;
							$roundedTokenAmt = $this->getRoundedTokenAmount($totalBookingAmount);

							if ($totalBookAmount != $totalBookingAmount)
							{
								$totalBookAmount = $totalBookingAmount;
								SiteErrorLog::create([
									                     'url'        => request()->fullUrl(),
									                     'exception'  => "Decor AutoBooking: Tried to hack",
									                     'code'       => "Error",
									                     'details'    => "Some error occurred in BaseAutoBookingHandlerController [Line: " . __LINE__ . "]. Wrong booking amount for ticket id => " . $ticket->id,
									                     'created_at' => date('Y-m-d H:i:s'),
									                     'updated_at' => date('Y-m-d H:i:s')
								                     ]);
							}
							if ($tokenAdvance != $roundedTokenAmt)
							{
								$tokenAdvance = $roundedTokenAmt;
								SiteErrorLog::create([
									                     'url'        => request()->fullUrl(),
									                     'exception'  => "Decor AutoBooking: Tried to hack",
									                     'code'       => "Error",
									                     'details'    => "Some error occurred in BaseAutoBookingHandlerController [Line: " . __LINE__ . "]. Wrong token amount for ticket id => " . $ticket->id,
									                     'created_at' => date('Y-m-d H:i:s'),
									                     'updated_at' => date('Y-m-d H:i:s')
								                     ]);
							}

							$bookingAmount = $totalBookAmount;
							$advanceAmount = $tokenAdvance;
						}
						else
						{
							SiteErrorLog::create([
								                     'url'        => request()->fullUrl(),
								                     'exception'  => "Decor AutoBooking: Tried to hack",
								                     'code'       => "Error",
								                     'details'    => "Some error occurred in BaseAutoBookingHandlerController [Line: " . __LINE__ . "]. Invalid delivery pincode for ticket id => " . $ticket->id,
								                     'created_at' => date('Y-m-d H:i:s'),
								                     'updated_at' => date('Y-m-d H:i:s')
							                     ]);

							return response()->json([
								                        'success' => false,
								                        'error'   => "Entered pincode is invalid"
							                        ]);
						}
						break;

					case config('evibe.ticket.type.cake'):

						$deliveryDate = request('cakeDeliveryDate');
						$slotId = request('cakeSlot');
						$messageOnCake = request('message');
						$price = request('price');
						$deliveryCharge = request('deliveryCharge');

						$cake = Cake::with("tags")->find($product['productId']);
						if (!$cake)
						{
							// todo: report error

							continue;
						}
						$isLive = $cake->is_live;
						if ($isLive == 0)
						{
							return $this->sendNonLiveMailAndMsg("Cake", $deliveryDate, $name, $phone, $email);
						}
						$productValidity = true;

						// verify delivery charge
						$slot = DeliverySlot::findOrFail(trim($slotId));
						$cakeDeliverySlot = $slot->formattedSlot();

						if ($slot->price !== $deliveryCharge)
						{
							SiteErrorLog::create([
								                     'url'        => request()->fullUrl(),
								                     'exception'  => "Cake AutoBooking: Tried to hack",
								                     'code'       => "Error",
								                     'details'    => "Some error occurred in BaseAutoBookingHandlerController [Line: " . __LINE__ . "]. Wrong deliver charge for ticket id => " . $ticket->id,
								                     'created_at' => date('Y-m-d H:i:s'),
								                     'updated_at' => date('Y-m-d H:i:s')
							                     ]);

							$deliveryCharge = $slot->price;
						}

						$transCharges = $deliveryCharge;

						// calculate party start and end time based on the slot
						$partyStartTime = strtotime($deliveryDate) + strtotime($slot->start_time) - strtotime('today midnight');
						$partyEndTime = strtotime($deliveryDate) + strtotime($slot->end_time) - strtotime('today midnight');

						// update calculated event date for ticket
						$ticket->event_date = $partyStartTime;
						$partyDateTime = $partyStartTime;

						// fill the data in checkout field value
						$checkoutFields = [
							config('evibe.checkout_field.cake.message')         => $messageOnCake,
							config('evibe.checkout_field.cake.delivery_charge') => $deliveryCharge,
							config('evibe.checkout_field.cake.delivery_slot')   => $cakeDeliverySlot
						];

						$categories = request('categories');

						$baseCake = new BaseCakeController();
						$priceData = $baseCake->calculateCakePriceFromBackend($categories, $cake->id);

						// re verify price from backed;
						if ((int)$price !== (int)$priceData['price'])
						{
							SiteErrorLog::create([
								                     'url'        => request()->fullUrl(),
								                     'exception'  => "Cake AutoBooking: Tried to hack",
								                     'code'       => "Error",
								                     'details'    => "Some error occurred in BaseAutoBookingHandlerController [Line: " . __LINE__ . "]. Wrong price charge from user for ticket id => " . $ticket->id,
								                     'created_at' => date('Y-m-d H:i:s'),
								                     'updated_at' => date('Y-m-d H:i:s')
							                     ]);

							$price = $priceData['price'];
						}

						$advanceAmount = (int)($price + $deliveryCharge);

						foreach ($categories as $category)
						{
							if ($category['catId'] == config('evibe.cake_cat_value.weight'))
							{
								$checkoutFields[config('evibe.checkout_field.cake.weight')] = $category['name'];
							}
							elseif ($category['catId'] == config('evibe.cake_cat_value.eggless'))
							{
								$checkoutFields[config('evibe.checkout_field.cake.type')] = $category['name'];
							}
							elseif ($category['catId'] == config('evibe.cake_cat_value.flavour'))
							{
								$checkoutFields[config('evibe.checkout_field.cake.flavour')] = $category['name'];
							}

						}

						if ($advanceAmount)
						{
							foreach ($checkoutFields as $key => $value)
							{
								if ($value || $key == config('evibe.checkout_field.cake.delivery_charge'))
								{
									CheckoutFieldValue::create([
										                           'ticket_id'         => $ticket->id,
										                           'checkout_field_id' => $key,
										                           'value'             => $value
									                           ]);
								}
							}
						}

						// verify partner and fallback for city_id
						$partner = Vendor::find($cake->provider_id);
						if (!$partner)
						{
							//return $res = [
							//	'success' => false,
							//	'error'   => "could not find decor partner"
							//];

							// todo: report error

							continue;
						}
						$partnerId = $partner ? $partner->id : null;
						$partnerTypeId = config('evibe.ticket.type.planner');
						$isVenueBooking = 0;

						// if no city for provider || city of provider doesn't match session
						if ((!$partner->city_id) || ($partner->city_id && ($cityId != $partner->city_id)))
						{
							$optionData = [
								'name' => $cake->title,
								'code' => $cake->code,
								'type' => config('evibe.type-product-title.' . ($product['productTypeId']))
							];

							$this->dispatch(new MissingCityAlertToTeam($optionData));
						}

						// todo: form booking info
						// create booking info: html full url
						$productUrl = $this->fetchProductUrl($cityId, $eventId, $product['productTypeId'], $product['productId'], $cake->url);
						$bookingInfo = "<a href='" . $productUrl . "?ref=pay-checkout' target='_blank'>" . $cake->title . "</a>";

						// get type ticket booking id from tags
						$typeTicketBookingId = config('evibe.booking_type.cake');

						$productPrice = $price;
						$bookingAmount = $advanceAmount;
						break;

					case config('evibe.ticket.type.package'):
					case config('evibe.ticket.type.resorts'):
					case config('evibe.ticket.type.villas'):
					case config('evibe.ticket.type.lounges'):
					case config('evibe.ticket.type.food'):
					case config('evibe.ticket.type.surprises'):
					case config('evibe.ticket.type.priests'):
					case config('evibe.ticket.type.tents'):
					case config('evibe.ticket.type.generic-package'):

						$phone = request('phone');
						$entityData = $this->getEntityDetailsByMapValues($product['productId'], config('evibe.ticket.type.package'));

						if (!$entityData)
						{
							// todo: report error

							continue;
						}
						$productValidity = true;

						if (!isset($entityData['provider']))
						{
							// todo: report error

							continue;
						}

						$partner = $entityData['provider'];
						$partnerId = $partner['id'];
						$partnerTypeId = isset($entityData['map_type_id']) ? $entityData['map_type_id'] : null;

						// Check whether the provider is planner or venue
						$isVenueBooking = (request("forVenueDetails") == config('evibe.ticket.type.venue')) ? 1 : 0;
						if ($isVenueBooking == 1)
						{
							$zipCode = (isset($entityData['zip']) && $entityData['zip']) ? $entityData['zip'] : $partner['zip'];
							$fullAddress = (isset($entityData['full_address']) && $entityData['full_address']) ? $entityData['full_address'] : $partner['full_address'];
							$venueAddress = $fullAddress . ", Pin Code: " . $zipCode;
							$areaId = (isset($entityData['area_id']) && $entityData['area_id']) ? $entityData['area_id'] : $partner['area_id'];
							$venueLandmark = (isset($entityData['landmark']) && $entityData['landmark']) ? $entityData['landmark'] : $partner['landmark'];
						}

						// Check in time is fixed as given by package, now get user selected check in date
						// and form full unix timestamp.
						$guestsCount = request('guestsCount', false);
						$checkInDate = request('checkInDate', false);
						$partyTime = request('partyTime', null);
						$checkOutTime = request('checkOutTime', null);

						$rawCheckInTime = !is_null($partyTime) ? $partyTime : $entityData['check_in'];
						$rawCheckOutTime = !is_null($checkOutTime) ? $checkOutTime : $entityData['check_out'];
						$checkInTime = date("H:i", strtotime($rawCheckInTime));
						$checkInDate = date('Y-m-d', strtotime(str_replace('-', '/', $checkInDate)));
						$checkInDateTime = strtotime("$checkInDate $checkInTime");

						$cldIds = Package::join('planner_package_tags', 'planner_package_tags.planner_package_id', '=', 'planner_package.id')
						                 ->select('planner_package.*')
						                 ->where('planner_package_tags.tile_tag_id', config('evibe.type-tag.candle-light-dinners'))
						                 ->where('planner_package.type_ticket_id', config('evibe.ticket.type.surprises'))
						                 ->where('planner_package.is_live', 1)
						                 ->whereNull('planner_package.deleted_at')
						                 ->whereNull('planner_package_tags.deleted_at')
						                 ->pluck('planner_package.id')
						                 ->toArray();

						// @todo: remove CLD constraint when it is being used for another occasion
						// @see: dates are hard-coded here
						if ($product['productTypeId'] == config('evibe.ticket.type.surprises') &&
							(in_array($product['productId'], $cldIds)))
						{
							$christmasIds = config('evibe.campaign-packages.christmas');
							if (is_string($christmasIds))
							{
								$christmasIds = explode(',', preg_replace('/\s/', '', $christmasIds));
							}

							$newYearIds = config('evibe.campaign-packages.new-year');
							if (is_string($newYearIds))
							{
								$newYearIds = explode(',', preg_replace('/\s/', '', $newYearIds));
							}

							//$valentinesDayIds = config('evibe.campaign-packages.valentines-day');
							//if (is_string($valentinesDayIds))
							//{
							//	$valentinesDayIds = explode(',', preg_replace('/\s/', '', $valentinesDayIds));
							//}

							$valentinesDayIds = Package::join('planner_package_tags', 'planner_package_tags.planner_package_id', '=', 'planner_package.id')
							                           ->select('planner_package.*')
							                           ->where('planner_package_tags.tile_tag_id', config('evibe.type-tag.valentines-day-2021'))
							                           ->where('planner_package.type_ticket_id', config('evibe.ticket.type.surprises'))
							                           ->where('planner_package.is_live', 1)
							                           ->whereNull('planner_package.deleted_at')
							                           ->whereNull('planner_package_tags.deleted_at')
							                           ->pluck('planner_package.id')
							                           ->toArray();
						
							$campaignIds = array_merge($christmasIds, $newYearIds, $valentinesDayIds);

							$campaignDates = [];
							if (config('evibe.campaign-dates.christmas'))
							{
								$campaignDates = array_merge($campaignDates, config('evibe.campaign-dates.christmas'));
							}
							if (config('evibe.campaign-dates.new-year'))
							{
								$campaignDates = array_merge($campaignDates, config('evibe.campaign-dates.new-year'));
							}
							if (config('evibe.campaign-dates.valentines-day'))
							{
								//$campaignDates = array_merge($campaignDates, config('evibe.campaign-dates.valentines-day'));
								$campaignDates = array_merge($campaignDates, ['2022-02-14']); // @see: hardcoded
							}
							//$campaignDates = [
							//	config('evibe.campaign-dates.christmas'),
							//	config('evibe.campaign-dates.new-year'),
							//	config('evibe.campaign-dates.valentines-day')
							//];

							if (!in_array($product['productId'], $campaignIds) && in_array($checkInDate, $campaignDates))
							{
								// @todo: any customised msg along with link
								// @todo: campaign landing pages links
								// @todo: city related??
								$campaignLandingLink = null;
								$campaignLandingPage= null;
								if (in_array($checkInDate, config('evibe.campaign-dates.christmas')))
								{
									$campaignLandingLink = config('evibe.christmas-profile-url.' . getCityUrl());
									$campaignLandingPage = 'Exclusive Christmas Collection';
								}
								elseif (in_array($checkInDate, config('evibe.campaign-dates.new-year')))
								{
									$campaignLandingLink = config('evibe.new-year-profile-url.' . getCityUrl());
									$campaignLandingPage = 'Exclusive New Year Collection';
								}
								elseif (in_array($checkInDate, config('evibe.campaign-dates.valentines-day')))
								{
									// @todo: differentiate between venue and non-venue packages and show different collection links
									if(getCityUrl() == "hyderabad" || getCityUrl() == "bangalore")
									{
										$campaignLandingLink = 'valentines-day-2022-romantic-candlelight-dinner-'.getCityUrl();
									}
									
									$campaignLandingPage = 'Valentines Day 2022 Specials';
								}

								if ($campaignLandingLink)
								{
									//$campaignLandingLink = config('evibe.host') . '/' . getCityUrl() . '/collections/' . $campaignLandingLink;
									$campaignLandingLink = config('evibe.host') . '/u/' . $campaignLandingLink;
								}
								

								$res = [
									'success'                => false,
									'campaignUnavailability' => true,
									'campaignLandingLink'    => $campaignLandingLink,
									'campaignLandingPage'    => $campaignLandingPage
								];
								
								return $res;
							}

							// @todo: validation for in campaign packages
						}

						// surprise packages booking time validation
						// validate booking time based on package 'book_before_hrs'
						if (isset($entityData['type_ticket_id']) && ($entityData['type_ticket_id'] == config('evibe.ticket.type.surprises')) && isset($entityData['book_before_hrs']) && $entityData['book_before_hrs'])
						{
							$bookBeforeTime = time() + ($entityData['book_before_hrs'] * 60 * 60);
							if ($checkInDateTime < $bookBeforeTime)
							{
								// can be used when booking feasible time needs to be shown
								/*
								$bookAfterTimeHourEndString = Carbon::createFromTimestamp($bookBeforeTime)->endOfHour()->toDateTimeString();
								if ((strtotime($bookAfterTimeHourEndString) - $bookBeforeTime) > (30 * 60))
								{
									$bookAfterTimeString = $bookAfterTimeHourEndString;
								}
								else
								{
									$bookAfterTimeHourStartString = Carbon::createFromTimestamp($bookBeforeTime)->startOfHour()->toDateTimeString();
									$bookAfterTimeString = $bookAfterTimeHourStartString;
								}
								$bookAfterTime = strtotime($bookAfterTimeHourEndString);
								*/

								return $res = [
									'success' => false,
									'error'   => 'Sorry, this package is not available for your selected date and time. Please try another timing.'
								];
							}
						}

						$checkOutDateTime = null;
						$nextDayCheckOutDateString = strtotime($checkInDate) + 24 * 60 * 60;
						if ($rawCheckOutTime)
						{
							if (strpos(strtolower($rawCheckOutTime), "next day") != false)
							{
								$rawCheckOut = explode('(', $rawCheckOutTime);
								$checkOutDateTime = $nextDayCheckOutDateString + strtotime($rawCheckOut[0]) - strtotime('today midnight');
							}
							elseif (strtotime($rawCheckInTime) >= strtotime($rawCheckOutTime))
							{
								$checkOutDateTime = $nextDayCheckOutDateString + strtotime($rawCheckOutTime) - strtotime('today midnight');
							}
							else
							{
								$checkOutDateTime = strtotime($checkInDate) + strtotime($rawCheckOutTime) - strtotime('today midnight');
							}
						}

						// booking related fields
						$partyDateTime = $checkInDateTime;
						$partyEndTime = $checkOutDateTime;

						// map link
						$mapLat = isset($partner['map_lat']) ? $partner['map_lat'] : 0;
						$mapLong = isset($partner['map_long']) ? $partner['map_long'] : 0;
						$mapLink = "https://www.google.com/maps/place/" . $mapLat . "+" . $mapLong . "/" . $mapLat . "," . $mapLong . ",17z";

						$eventId = $eventId ?: (isset($entityData['event_id']) ? $entityData['event_id'] : null);
						// update ticket details
						$ticket->event_date = $checkInDateTime;
						$ticket->event_id = isset($entityData['event_id']) ? $entityData['event_id'] : 1; // default 1 if not set
						$ticket->guests_count = $guestsCount;
						$ticket->city_id = isset($partner['city_id']) ? $partner['city_id'] : getCityId();
						$ticket->area_id = isset($areaId) ? $areaId : null;
						$ticket->zip_code = isset($zipCode) ? $zipCode : null;
						$ticket->venue_address = isset($venueAddress) ? $venueAddress : null;
						$ticket->venue_landmark = isset($venueLandmark) ? $venueLandmark : null;
						$ticket->map_link = $mapLink;

						// package checkout fields
						if ($guestsCount && config('evibe.checkout_field.bachelor-venue.guest-count'))
						{
							CheckoutFieldValue::create([
								                           'ticket_id'         => $ticket->id,
								                           'checkout_field_id' => config('evibe.checkout_field.bachelor-venue.guest-count'),
								                           'value'             => $guestsCount
							                           ]);
						}

						// calculate total booking amount, advance amount based on guests count
						$totalBookingAmount = $entityPrice = $entityData['price'];
						$pricePerExtraGuest = $entityData['price_per_extra_guest'];
						$groupCount = $entityData['group_count'];
						$tokenAdvance = $entityData['token_advance'];

						if ($product['productTypeId'] == config('evibe.ticket.type.villas'))
						{
							// as event might not yet be declared
							$occasionId = config('evibe.occasion.bachelor.id');

							$dynamicParams = [
								'price'              => $totalBookingAmount,
								'pricePerExtraGuest' => $pricePerExtraGuest,
								'groupCount'         => $groupCount
							];

							$updatedProductData = $this->calculateDynamicParams($dynamicParams,
							                                                    date('m/d/y', $checkInDateTime),
							                                                    $cityId,
							                                                    $occasionId,
							                                                    $product['productTypeId'],
							                                                    $product['productId']);

							$totalBookingAmount = (isset($updatedProductData['price']) && $updatedProductData['price']) ? $updatedProductData['price'] : $totalBookingAmount;
							$groupCount = (isset($updatedProductData['groupCount']) && $updatedProductData['groupCount']) ? $updatedProductData['groupCount'] : $groupCount;
							$pricePerExtraGuest = (isset($updatedProductData['pricePerExtraGuest']) && $updatedProductData['pricePerExtraGuest']) ? $updatedProductData['pricePerExtraGuest'] : $pricePerExtraGuest;
						}

						if ($groupCount < $guestsCount)
						{
							$extra_guest = $guestsCount - $groupCount;
							$totalBookingAmount += ($extra_guest * $pricePerExtraGuest);
						}

						$advanceAmount = $this->getRoundedTokenAmount($totalBookingAmount);
						$advanceSelect = request('advanceSelect', 0);

						if ($advanceSelect && $advanceSelect == 1)
						{
							$advanceAmount = $totalBookingAmount;
						}

						$productPrice = $totalBookingAmount;
						$bookingAmount = $totalBookingAmount;

						// create booking info: html full url
						$productUrl = $this->fetchProductUrl($cityId, $eventId, $product['productTypeId'], $product['productId'], $entityData['url']);
						$bookingInfo = "<a href='" . $productUrl . "?ref=pay-checkout' target='_blank'>" . $entityData['name'] . "</a>";

						$typeTicketBookingId = config('evibe.booking_type.venue'); // @todo: should be based on the booking

						break;

					case config('evibe.ticket.type.entertainment'):

						$partyDate = request('partyDate', false);
						$partyTime = request('partyTime', null);
						$ent = TypeServices::find($product['productId']);
						$isLive = $ent->is_live;
						if (!$ent)
						{
							//return response()->json([
							//	                        'success' => false,
							//	                        'error'   => "could not find the service"
							//                        ]);

							// todo: report error

							continue;
						}
						if ($isLive == 0)
						{
							return $this->sendNonLiveMailAndMsg("Entertainment", $partyDate, $name, $phone, $email);
						}

						$productValidity = true;

						$rawCheckInTime = $partyTime;
						$checkInTime = date("H:i", strtotime($rawCheckInTime));
						$checkInDate = date('Y-m-d', strtotime(str_replace('-', '/', $partyDate)));
						$checkInDateTime = strtotime("$checkInDate $checkInTime");

						$partyDateTime = $checkInDateTime;
						$ticket->event_date = $partyDateTime;

						$totalBookingAmount = $ent->min_price;
						$tokenAdvance = $this->getRoundedTokenAmount($totalBookingAmount);
						$advanceSelect = request('advanceSelect', 0);

						if ($advanceSelect && $advanceSelect == 1)
						{
							$tokenAdvance = $totalBookingAmount;
						}

						$productPrice = $totalBookingAmount;
						$bookingAmount = $totalBookingAmount;
						$advanceAmount = $tokenAdvance;

						// create booking info: html full url
						$productUrl = $this->fetchProductUrl($cityId, $eventId, $product['productTypeId'], $product['productId'], $ent->url);
						$bookingInfo = "<a href='" . $productUrl . "?ref=pay-checkout' target='_blank'>" . $ent->name . "</a>";

						$typeTicketBookingId = config('evibe.booking_type.ent');
						$product['productTypeId'] = config('evibe.ticket.type.service');

						break;

					case config('evibe.ticket.type.add-on'):
						$addOn = AddOn::find($product['productId']);
						if (!$addOn)
						{
							// todo: report error
							continue;
						}

						$productValidity = true;

						$bookingInfo = $addOn->name . ' - ' . $addOn->info;
						$bookingUnits = (isset($product['unitCount']) && ($product['unitCount'] > 0)) ? $product['unitCount'] : 1;
						$productPrice = $addOn->price_per_unit;
						$bookingAmount = $productPrice * $bookingUnits;
						$advanceAmount = $bookingAmount;

						// party timings will be taken from previous bookings(ticket)
						$partyDateTime = $ticket->event_date;

						// assuming that add-ons will be added last
						// todo: change logic
						$ticketMapping = TicketMapping::select('ticket_mapping.*', 'ticket_bookings.map_id AS partner_id', 'ticket_bookings.map_type_id AS partner_type_id')
						                              ->join('ticket_bookings', 'ticket_bookings.ticket_mapping_id', '=', 'ticket_mapping.id')
						                              ->where('ticket_mapping.ticket_id', $ticket->id)
						                              ->whereNull('ticket_mapping.deleted_at')
						                              ->whereNull('ticket_bookings.deleted_at')
						                              ->where('ticket_mapping.ticket_id', $ticket->id)
						                              ->where('ticket_mapping.map_id', $products[0]['productId'])
						                              ->where('ticket_mapping.map_type_id', $products[0]['productTypeId'])
						                              ->first();
						if ($ticketMapping)
						{
							$partnerId = $ticketMapping->partner_id;
							$partnerTypeId = $ticketMapping->partner_type_id;
						}

						$isVenueBooking = 0;
						$typeTicketBookingId = $addOn->type_ticket_booking_id; // todo: to get checkout fields

						// update add-on image as booking image
						$profileImageUrl = config('evibe.gallery.host');
						$profileImageUrl .= '/addons/' . $addOn->id . '/' . $addOn->image_url;

						break;
				}

				if (!$productValidity)
				{
					// todo: report that product doesn't exist
					continue;
				}

				// form ticket mapping data
				$mappingInsertData = [
					'ticket_id'   => $ticketId,
					'map_id'      => $product['productId'],
					'map_type_id' => $product['productTypeId'],
					'created_at'  => date('Y-m-d H:i:s'),
					'updated_at'  => date('Y-m-d H:i:s')
				];

				// create ticket mapping
				$ticketMapping = TicketMapping::create($mappingInsertData);
				if (!$ticketMapping)
				{
					SiteErrorLog::create([
						                     'url'        => request()->fullUrl(),
						                     'exception'  => "AutoBooking: Error while creating ticket mapping",
						                     'code'       => "Error",
						                     'details'    => "Some error occurred in BaseAutoBookingHandlerController [Line: " . __LINE__ . "], while creating a ticket mapping",
						                     'created_at' => date('Y-m-d H:i:s'),
						                     'updated_at' => date('Y-m-d H:i:s')
					                     ]);

					return $res = ['success' => false];
				}

				$ticketMappingId = $ticketMapping->id;

				// star option check
				$starOption = StarOption::where('option_id', $product['productId'])
				                        ->where('option_type_id', $product['productTypeId'])
				                        ->first();

				// create ticket booking
				$bookingInsertData = [
					'booking_id'                  => config('evibe.ticket.enq.pre.auto_booked') . $ticketMappingId,
					'booking_info'                => $bookingInfo,
					'party_date_time'             => $partyDateTime,
					'party_end_time'              => $partyEndTime,
					'product_price'               => $productPrice,
					'booking_units'               => $bookingUnits,
					'booking_amount'              => $bookingAmount,
					'advance_amount'              => $advanceAmount,
					'estimated_transport_charges' => $estimatedTransCharges,
					'transport_charges'           => $transCharges,
					'map_id'                      => $partnerId,
					'map_type_id'                 => $partnerTypeId,
					'ticket_id'                   => $ticketId,
					'ticket_mapping_id'           => $ticketMappingId,
					'is_venue_booking'            => $isVenueBooking,
					'type_ticket_booking_id'      => $typeTicketBookingId,
					'booking_type_details'        => ucwords(config('evibe.auto-book.' . $product['productTypeId'])),
					'is_star_order'               => $starOption ? 1 : 0,
					//'star_partner_amount'         => ($starOption && $starOption->partner_amount) ? $starOption->partner_amount : null,
					'created_at'                  => date('Y-m-d H:i:s'),
					'updated_at'                  => date('Y-m-d H:i:s')
				];

				$ticketBooking = TicketBooking::create($bookingInsertData);

				if (!$ticketBooking)
				{

					SiteErrorLog::create([
						                     'url'        => request()->fullUrl(),
						                     'exception'  => "Decor AutoBooking: Error while creating ticket booking",
						                     'code'       => "Error",
						                     'details'    => "Some error occurred in BaseAutoBookingHandlerController [Line: " . __LINE__ . "]. Unable to create ticket booking [Ticket id: $ticketId]",
						                     'created_at' => date('Y-m-d H:i:s'),
						                     'updated_at' => date('Y-m-d H:i:s')
					                     ]);

					return $res = ['success' => false];
				}

				// upload profile image to booking gallery
				if ($profileImageUrl)
				{
					$validImage = getimagesize($profileImageUrl);
					if ($validImage)
					{
						TicketBookingGallery::create([
							                             'ticket_booking_id' => $ticketBooking->id,
							                             'url'               => $profileImageUrl,
							                             'type'              => 1,
							                             'title'             => pathinfo($profileImageUrl, PATHINFO_FILENAME)
						                             ]);
					}
				}
			}

			$ticket->save();

			$checkoutLink = route('auto-book.pay.auto.checkout');
			$checkoutLink .= "?id=" . $ticketId;
			$checkoutLink .= "&token=" . Hash::make($ticketId);
			$checkoutLink .= "&uKm8=" . Hash::make($phone);

			if ($checkoutLink)
			{
				return $res = ['success' => true, 'redirectUrl' => $checkoutLink];
			}
		} catch (\Exception $e)
		{
			SiteErrorLog::create([
				                     'url'        => request()->fullUrl(),
				                     'exception'  => "BaseAutoBookingHandlerController: " . __LINE__ . ": Some error occurred while processing AB init",
				                     'code'       => $e->getCode(),
				                     'details'    => $e->getMessage(),
				                     'created_at' => date('Y-m-d H:i:s'),
				                     'updated_at' => date('Y-m-d H:i:s')
			                     ]);

			if (isset($ticket))
			{
				$ticket->forceDelete();
			}

			return $res = [
				'success' => false,
				'error'   => $e->getMessage()
			];
		}
	}

	public function sendNonLiveMailAndMsg($productType, $deliveryDate, $name, $phone, $email)
	{
		$name = isset($name) ? $name : "";
		$email = isset($email) ? $email : "";

		$data = [
			"customerName"   => $name,
			"customerEmail"  => $email,
			"customerMobile" => $phone,
			"partyDate"      => $deliveryDate,
			"productType"    => $productType,
			"pageLink"       => request('pageUrl')
		];
		$tplCustomer = config('evibe.sms_tpl.product_non_live');
		$replaces = [
			'#field1#' => $data["customerName"],
			'#field2#' => config('evibe.contact.company.phone'),
		];

		$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);

		$smsData = [
			'to'   => $data["customerMobile"],
			'text' => $smsText
		];

		$this->dispatch(new SMSActiveNonLiveToCustomer($smsData));
		$this->dispatch(new MailTeamCustomerEnquireForNonActiveProduct($data));

		if ($email)
		{
			$this->dispatch(new MailCustomerEnquireForNonActiveProduct($data));
		}

		return $res = [
			'success' => false,
			'error'   => "we are sorry that your package/service in unavailable. Our party planning expert will contact you shortly to help you out.",
			'is_live' => 0
		];
	}

	public function showABCheckoutPage()
	{
		$ticketId = request()->input('id');
		$token = request()->input('token');
		$hashedStr = request()->input('uKm8');

		$dataForValidation = new AutoBookUtil();
		$dataForValidation->setTicketId($ticketId);
		$dataForValidation->setToken($token);
		$dataForValidation->setHashedStr($hashedStr);

		$ticket = $this->validateTicket($dataForValidation);
		if (!is_null($ticket))
		{
			$ticketBookings = $ticket->bookings; // get list of all bookings

			$eventId = $ticket->event_id;
			$cityId = $ticket->city_id;
			if (count($ticketBookings) > 0)
			{
				$data = $this->getTicketData($ticket);
				$totalBookingAmount = 0;
				$totalAdvanceToPay = 0;
				$hasVenueBooking = false;
				$isShowVenueType = false;
				$hasAddOn = false;
				$hasDecor = false;
				$imageUpload = false;
				$needCustomerProof = false;
				$primaryBookingsCount = 0;

				foreach ($ticketBookings as $booking)
				{
					$mapTypeId = $booking->mapping->map_type_id;
					$mapId = $booking->mapping->map_id;
					$mappedValues = $this->fillMappingValues([
						                                         'isCollection' => false,
						                                         'mapId'        => $mapId,
						                                         'mapTypeId'    => $mapTypeId
					                                         ]);

					$bookingData = $this->getTicketBookingData($booking, $mappedValues);
					$data['bookings']['list'][] = $bookingData;

					// Code to make party time as null
					if (isset($bookingData['itemMapTypeId']) && ($bookingData['itemMapTypeId'] != config('evibe.ticket.type.add-on')))
					{
						$primaryBookingsCount++;
					}

					$totalBookingAmount += $booking->booking_amount;
					$totalAdvanceToPay += ($booking->is_advance_paid == 1) ? 0 : $booking->advance_amount;

					if (isset($bookingData['itemMapTypeId']) &&
						(isset($data['additional']['partyStartTime'])) &&
						(($bookingData['itemMapTypeId'] == config('evibe.ticket.type.decor') || ($bookingData['itemMapTypeId'] == config('evibe.ticket.type.cake')))))
					{
						$data['additional']['partyStartTime'] = null;
					}

					// @see: assuming there will not be more than one venue booking per ticket
					if ($booking->is_venue_booking)
					{
						$hasVenueBooking = true;
						// todo: decide party date times for ab here only
						$data['additional']['partyStartTime'] = $booking->party_date_time ? date("g:i A", $booking->party_date_time) : null;

						// todo: need to implement better when auto availability calender is in place
						if ($booking->party_date_time)
						{
							$data['additional']['PartyStartTimeDisabled'] = true;
						}
					}

					if (isset($bookingData['itemMapTypeId']) && ($bookingData['itemMapTypeId'] == config('evibe.ticket.type.decor')))
					{
						$isShowVenueType = true;
						$hasDecor = true;
					}

					if (isset($bookingData['itemMapTypeId']) && ($bookingData['itemMapTypeId'] == config('evibe.ticket.type.add-on')))
					{
						$hasAddOn = true;
					}

					if (isset($bookingData['imageUpload']) && ($bookingData['imageUpload']))
					{
						$imageUpload = true;
					}

					if (isset($bookingData['itemMapTypeId']) && ($bookingData['itemMapTypeId'] == config('evibe.ticket.type.villas')))
					{
						$needCustomerProof = true;
					}
				}

				if ($primaryBookingsCount >= 2)
				{
					$data['additional']['partyStartTime'] = null;
				}

				if (in_array($eventId, [
					config('evibe.occasion.surprises.id'),
					config('evibe.occasion.kids_birthdays.id'),
					config('evibe.occasion.first_birthday.id'),
					config('evibe.occasion.birthday_2-5.id'),
					config('evibe.occasion.birthday_6-12.id'),
					config('evibe.occasion.naming_ceremony.id')
				]))
				{
					$arrayAddOns = [];
					foreach ($ticketBookings as $ticketBooking)
					{
						$mapping = $ticketBooking->mapping()->first();
						$productAddOns = $this->getTicketApplicableAddOns($cityId, $eventId, $mapping->map_type_id, $mapping->map_id, $ticketBooking);

						// disabling add-ons for vday clds
						//// todo: remove
						if ($ticketBooking->mapping && ($ticketBooking->mapping->map_type_id == config('evibe.ticket.type.surprises')) && ($ticketBooking->map_type_id == config('evibe.ticket.type.venue')) && (date('Y/m/d', $ticketBooking->party_date_time) == "2020/02/14"))
						{
							$productAddOns = [];
						}

						$arrayAddOns = array_merge($arrayAddOns, $productAddOns);
					}
					$arrayAddOns = array_unique($arrayAddOns);

					// todo edit after test
					if (count($arrayAddOns))
					{
						$data['addOns'] = $arrayAddOns;
						$data['selectedAddOnIds'] = [];

						// already selected add-ons
						foreach ($ticketBookings as $ticketBooking)
						{
							$mapping = $ticketBooking->mapping()->first();
							if ($mapping->map_type_id == config('evibe.ticket.type.add-on'))
							{
								$data['selectedAddOnIds'][$mapping->map_id] = [
									'bookingUnits' => $ticketBooking->booking_units ?: 1
								];
							}
						}
					}
				}

				$data['couponToken'] = $this->getAccessTokenForCoupon(intval($ticketId), $ticket->created_at);

				$data['bookings']['minBookingDateTime'] = $ticketBookings->min('party_date_time');
				$data['bookings']['maxBookingDateTime'] = $ticketBookings->max('party_date_time');

				$data['bookings']['totalBookingAmount'] = $totalBookingAmount;
				$data['bookings']['totalAdvanceToPay'] = $totalAdvanceToPay;
				$data['hasVenueBooking'] = $hasVenueBooking;
				$data['isShowVenueType'] = $isShowVenueType;
				$data['hasAddOn'] = $hasAddOn;
				$data['hasDecor'] = $hasDecor;
				$data['uploadImage'] = $imageUpload;
				$data['isAutoBooking'] = true;
				$data['needCustomerProof'] = $needCustomerProof;

				if ($needCustomerProof)
				{
					$data['proofs'] = TypeProof::all();
					$customerOrderProofs = CustomerOrderProof::where('ticket_id', $ticketId)
					                                         ->whereNull('deleted_at')
					                                         ->get();

					if ($customerOrderProofs && (count($customerOrderProofs) >= 2))
					{
						$data['customerOrderProofs'] = 1;
					}
				}

				// conditions to show extra benefits selection
				$eventId = $ticket->event_id;
				$timeDifference = $ticket->event_date - time();

				$kidsEvents = [
					config('evibe.occasion.kids_birthdays.id'),
					config('evibe.occasion.first_birthday.id'),
					config('evibe.occasion.birthday_2-5.id'),
					config('evibe.occasion.birthday_6-12.id'),
					config('evibe.occasion.naming_ceremony.id'),
					config('evibe.occasion.baby-shower.id')
				];

				$data['extraBenefits'] = false;
				$data['invite'] = false;
				if (in_array($eventId, $kidsEvents))
				{
					$data['extraBenefits'] = true;

					$data['invite'] = false; // @see: removed on 16 Mar 2020
					//if (($timeDifference > 0) && (($timeDifference / (24 * 60 * 60)) >= 2))
					//{
					//	$data['invite'] = true;
					//}
				}

				if (in_array($data['additional']['eventId'], [
					config('evibe.occasion.surprises.id'),
					config('evibe.occasion.kids_birthdays.id'),
					config('evibe.occasion.first_birthday.id'),
					config('evibe.occasion.birthday_2-5.id'),
					config('evibe.occasion.birthday_6-12.id'),
					config('evibe.occasion.naming_ceremony.id'),
					config('evibe.occasion.baby-shower.id')
				]))
				{
					$arrayAddOns = [];
					foreach ($ticketBookings as $ticketBooking)
					{
						$mapping = $ticketBooking->mapping()->first();
						$productAddOns = $this->getTicketApplicableAddOns($data['additional']['cityId'], $eventId, $mapping->map_type_id, $mapping->map_id);

						// @see: add-ons related code is repeating twice. Refer to the line "1325"
						// disabling add-ons for vday clds
						//// todo: remove
						if ($ticketBooking->mapping && ($ticketBooking->mapping->map_type_id == config('evibe.ticket.type.surprises')) && ($ticketBooking->map_type_id == config('evibe.ticket.type.venue')) && (date('Y/m/d', $ticketBooking->party_date_time) == "2020/02/14"))
						{
							$productAddOns = [];
						}

						$arrayAddOns = array_merge($arrayAddOns, $productAddOns);
					}
					$arrayAddOns = array_unique($arrayAddOns);
					$data['addOns'] = $arrayAddOns;

					$addOns = collect($arrayAddOns);
					if (count($addOns))
					{
						$data['addOnTypeTicketBookingIds'] = $addOns->pluck('type_ticket_booking_id')->toArray();
						$data['addOnTypeTicketBookingIds'] = array_unique(array_filter($data['addOnTypeTicketBookingIds']));
					}
				}

				$tmoToken = Hash::make($ticketId . config('evibe.token.track-my-order'));
				$data['tmoLink'] = route("track.orders") . "?ref=checkout-page&id=" . $ticketId . "&token=" . $tmoToken;

				// countries for calling code
				$data['countries'] = Country::all();

				// Storing data for analytics & getting more insights
				$payInitTrackingData = [
					"ticketId"    => $ticketId,
					"browserInfo" => isset($_SERVER) && isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "",
					"previousUrl" => isset($_SERVER) && isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "",
					"ipAddress"   => isset($_SERVER) && isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : ""
				];

				if (Auth::check())
				{
					$wallet = Wallet::where("user_id", Auth::user()->id)
					                ->first();

					$data["walletBalance"] = $wallet ? $wallet->balance : 0;
					$data["walletExpiry"] = $wallet ? $wallet->expires_on : 0;
				}

				if (isset($data['additional']['eventId']) && $data['additional']['eventId'])
				{
					switch ($data['additional']['eventId'])
					{
						case config('evibe.occasion.kids_birthdays.id'):
						case config('evibe.occasion.first_birthday.id'):
						case config('evibe.occasion.birthday_2-5.id'):
						case config('evibe.occasion.birthday_6-12.id'):
						case config('evibe.occasion.naming_ceremony.id'):
						case config('evibe.occasion.baby-shower.id'):
							$data['storePromotion'] = false;
							$data['storePromotionTitle'] = "Latest Party Props";
							$data['storePromotionTagLine'] = "Get foil balloon letters, numbers, LED balloons, photo props, photo banners and more.";
							$data['storePromotionImage'] = config('evibe.gallery.host') . "/main/img/store/Kids-1.png";

							$storeUrl = config('evibe.store_host') . "/collections/kids-birthday-party-supplies";
							$storeUrl .= "?utm_source=Checkout&utm_medium=Website&utm_campaign=StorePromotion&utm_content=KidsEvent";

							$data['storePromotionUrl'] = $storeUrl;
							break;

						case config('evibe.occasion.surprises.id'):
							$data['storePromotion'] = false;
							$data['storePromotionTitle'] = "Latest Party Props";
							$data['storePromotionTagLine'] = "Get foil balloon numbers, letters, fairy lights, photo props, sashes and more.";
							$data['storePromotionImage'] = config('evibe.gallery.host') . "/main/img/store/Youth-1.jpg";

							$storeUrl = config('evibe.store_host') . "/collections/youth-birthday-party-supplies";
							$storeUrl .= "?utm_source=Checkout&utm_medium=Website&utm_campaign=StorePromotion&utm_content=Surprises";

							$data['storePromotionUrl'] = $storeUrl;
							break;
					}
				}

				$this->dispatch(new PaymentInitiationUpdate($payInitTrackingData));

				return view('pay/checkout/checkout', ['data' => $data]);
			}
			else
			{
				Log::error("No bookings available::ticketId::" . $ticketId . "::token::" . $token . "::hashedStr::" . $hashedStr);

				return "<h1>Invalid Request</h1>"; // there are no bookings, abort
			}
		}
		else
		{
			SiteErrorLog::create([
				                     'url'        => request()->fullUrl(),
				                     'exception'  => "Unauthorized Access",
				                     'code'       => 403,
				                     'details'    => "Some error occurred in DecorAutoBookingHandlerController [Line: " . __LINE__ . "]. Token mismatch [Ticket id: $ticketId, token: $token, hashedStr: $hashedStr]",
				                     'created_at' => date('Y-m-d H:i:s'),
				                     'updated_at' => date('Y-m-d H:i:s')
			                     ]);

			return "<h1>Invalid Request</h1>"; // ticketId, token mismatch
		}
	}

	public function validateCheckoutVenueDetails($ticketId)
	{
		try
		{

			$ticket = Ticket::with('bookings')->find($ticketId);
			if (!$ticket)
			{
				Log::error("Unable to find ticket for ticketId: $ticketId");

				return response()->json([
					                        'success' => false,
				                        ]);
			}

			$rules = [
				'venueAddressLine1' => 'sometimes|required|min:5',
				'venueLocation'     => 'sometimes|required',
				'venueLandmark'     => 'sometimes|required|min:5',
				'venueZipCode'      => 'sometimes|required|digits:6',
				'venueTypeId'       => 'sometimes|required|integer|min:1',
			];

			$messages = [
				'venueLandmark.required'     => 'Please enter venue landmark',
				'venueLandmark.min'          => 'Please enter valid venue landmark',
				'venueAddressLine1.required' => 'Venue address line 1 is required',
				'venueAddressLine1.min'      => 'Venue address line 1 should be minimum 5 characters long',
				'venueZipCode.required'      => 'Please enter venue PIN code',
				'venueZipCode.digits'        => 'Please enter a valid 6 digit pin code (ex: 560001)',
				'venueLocation.required'     => 'Please enter venue location / area name',
				'venueTypeId.required'       => 'Please select your venue type',
				'venueTypeId.min'            => 'Please select your venue type',
			];

			$validation = Validator::make(request()->all(), $rules, $messages);

			if ($validation->fails())
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => $validation->messages()->first()
				                        ]);
			}

			// update venue details only for ticket with non-venue bookings
			$ticketVenueBookings = $ticket->bookings()
			                              ->where('ticket_bookings.is_venue_booking', 1)
			                              ->get();

			if ($ticketVenueBookings->count() == 0)
			{
				$enteredVenueZipCode = request('venueZipCode');

				// get the area details or create a new area with or without pinCode
				$areaData = $this->getAreaForTheLocation(request('venueLocation'), request('locationDetails'), $enteredVenueZipCode);
				$areaId = $areaData['areaId'];
				$areaName = $areaData['areaName'];
				//if (isset($areaData['alertPinCodeArea']) && $areaData['alertPinCodeArea'])
				//{
				//	// alert team regarding this
				//	$this->dispatch(new MailLocationVerificationAlertToTeam([
				//		                                                        'ticketId'      => $ticket->id,
				//		                                                        'name'          => $ticket->name,
				//		                                                        'phone'         => $ticket->phone,
				//		                                                        'email'         => $ticket->email,
				//		                                                        'partyLocation' => $areaData['areaName'],
				//		                                                        'pinCode'       => $enteredVenueZipCode,
				//		                                                        'dashLink'      => config('evibe.dash_host') . '/tickets/' . $ticket->id
				//	                                                        ]));
				//}

				// validate party delivery pin code
				if ($ticket->city_id && $enteredVenueZipCode)
				{
					$typeArea = Area::where('zip_code', $enteredVenueZipCode)->first();
					if ($typeArea && $typeArea->city_id && ($typeArea->city_id != $ticket->city_id))
					{
						$productCity = $this->getCityNameFromId($ticket->city_id);
						$pinCodeCity = $this->getCityNameFromId($typeArea->city_id);

						return response()->json([
							                        'success'  => false,
							                        'errorMsg' => "The service belongs to " . $productCity . " and cannot be availed for " . $pinCodeCity . ". Kindly change the party location pin code"
						                        ]);
					}
				}

				$venueAddressLine1 = request('venueAddressLine1');
				$venueAddressLine2 = request('venueAddressLine2') ? request('venueAddressLine2') : "";
				$venueLocation = $areaName;
				$venueZipCode = $enteredVenueZipCode;
				$venueAddress = "$venueAddressLine1, $venueAddressLine2, $venueLocation, Pin Code: $venueZipCode";
				$lat = request('lat');
				$lng = request('lng');

				$ticket->venue_landmark = request('venueLandmark');
				$ticket->address_line1 = $venueAddressLine1;
				$ticket->address_line2 = $venueAddressLine2;
				$ticket->venue_address = $venueAddress;
				$ticket->venue_location = $venueLocation;
				$ticket->zip_code = $venueZipCode;
				$ticket->lat = $lat;
				$ticket->lng = $lng;
				$ticket->map_address = request('mapAddress');
				$ticket->map_link = $this->getShortenUrl("http://maps.google.com/maps?q=$lat,$lng");
				$ticket->type_venue_id = array_key_exists('venueTypeId', request()->all()) ? request('venueTypeId') : null;

				if ($areaId)
				{
					$ticket->area_id = $areaId;
				}

				if (!$ticket->is_auto_booked)
				{
					$preDefinedAreaId = request('preDefinedAreaId') ? request('preDefinedAreaId') : -1;
					if ($areaId && $preDefinedAreaId != $areaId)
					{
						$preDefinedArea = "Unknown";
						if ($preDefinedAreaId != -1)
						{
							$preDefinedArea = Area::find($preDefinedAreaId);
							$preDefinedArea = $preDefinedArea ? $preDefinedArea->name : "Not Found";
						}

						$mailData = [
							'enquiryId'        => $ticket->enquiry_id,
							'phone'            => request('phone'),
							'name'             => request('name'),
							'preDefinedArea'   => $preDefinedArea,
							'preDefinedAreaId' => $preDefinedAreaId,
							'modifiedArea'     => $venueLocation,
							'modifiedAreaId'   => $areaId,
							'ticketId'         => $ticket->id
						];

						$this->dispatch(new MailLocationMismatchAlertToTeamJob($mailData));
					}
				}
			}

			$ticket->updated_at = Carbon::now();
			$ticket->save();

			return response()->json([
				                        'success' => true
			                        ]);

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());

			return response()->json([
				                        'success' => false,
			                        ]);
		}
	}

	public function validateCheckoutPartyDetails($ticketId)
	{
		try
		{

			$ticket = Ticket::find($ticketId);
			if (!$ticket)
			{
				Log::error("Unable to find ticket for ticketId: $ticketId");

				return response()->json([
					                        'success' => false,
				                        ]);
			}

			$rules = [
				'partyDate' => 'sometimes|required|date|after:yesterday',
				'partyTime' => 'sometimes|required'
			];

			$messages = [
				'partyDate.required' => 'Please enter your party date',
				'partyDate.after'    => 'Please enter a party date from today',
				'partyTime'          => 'Please enter your party time',
			];

			// dynamic checkout fields based related to customer data
			$checkoutPageFields = $this->getCustomerDataFields($ticketId);

			foreach ($checkoutPageFields as $checkoutField)
			{
				if ($checkoutField->type_field_id == config('evibe.input.date'))
				{
					// no validation required for date
					continue;
				}

				$rules[$checkoutField->name] = 'required';
				$messages[$checkoutField->name . '.required'] = "$checkoutField->identifier is required. (enter n/a if not applicable)";
				if ($checkoutField->type_field_id == config('evibe.input.checkbox'))
				{
					// @see: options are stored as text separated by commas
					$messages[$checkoutField->name . '.required'] = "$checkoutField->identifier are required. Kindly select at least one choice.";
				}
				elseif ($checkoutField->type_field_id == config('evibe.input.option'))
				{
					$messages[$checkoutField->name . '.required'] = "Kindly provide your choice for $checkoutField->identifier. Select N/A is not applicable";
				}
			}

			$validation = Validator::make(request()->all(), $rules, $messages);

			if ($validation->fails())
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => $validation->messages()->first()
				                        ]);
			}

			if ($ticket->is_auto_booked && request('partyTime'))
			{
				$partyDate = request('partyDate');
				$partyTime = request('partyTime');

				$partyDateTime = strtotime("$partyDate $partyTime");
				$trimPartyTime = strtotime($partyTime) - strtotime('today midnight');
				$calcPartyDateTime = strtotime($partyDate) + $trimPartyTime;

				if ($calcPartyDateTime < time())
				{
					Log::error("Tried to book party before 'now' with id $ticketId");

					return response()->json([
						                        'success'  => false,
						                        'errorMsg' => 'Please select a party time after now'
					                        ]);
				}

				$ticket->event_date = $calcPartyDateTime;

				$ticketBookings = TicketBooking::where('ticket_id', $ticketId);
				if (!$ticketBookings)
				{
					Log::error("Ticket Booking not found for ticket with id $ticketId");

					return response()->json([
						                        'success'  => false,
						                        'errorMsg' => 'Ticket booking not found'
					                        ]);
				}

				$ticketBookings->update([
					                        'party_date_time' => $partyDateTime,
					                        //'check_in'        => $partyTime,
				                        ]);

				$ticketBookings = $ticketBookings->get();
				foreach ($ticketBookings as $ticketBooking)
				{
					$ticketMapping = $ticketBooking->mapping;
					if ($ticketBooking->type_ticket_booking_id && $ticketMapping && ($ticketMapping->map_type_id == config('evibe.ticket.type.decor')))
					{
						// decor finish time in order details
						$decorFinishTimeCheckoutField = config('evibe.checkout_field.decor.delivery.' . $ticketBooking->type_ticket_booking_id);
						if ($decorFinishTimeCheckoutField)
						{
							$existingCheckoutField = CheckoutFieldValue::where('ticket_id', $ticketBooking->ticket_id)
							                                           ->where('ticket_booking_id', $ticketBooking->id)
							                                           ->where('checkout_field_id', $decorFinishTimeCheckoutField)
							                                           ->first();

							$decorFinishTime = date('h:i A', $partyDateTime);

							if ($existingCheckoutField)
							{
								$existingCheckoutField->value = $decorFinishTime;
								$existingCheckoutField->save();
							}
							else
							{
								CheckoutFieldValue::create([
									                           'ticket_id'         => $ticketBooking->ticket_id,
									                           'ticket_booking_id' => $ticketBooking->id,
									                           'checkout_field_id' => $decorFinishTimeCheckoutField,
									                           'value'             => $decorFinishTime
								                           ]);
							}
						}
					}
				}
			}

			// save the checkout customer data values.
			$checkoutCustomerDataFields = $this->getCustomerDataFields($ticketId);
			foreach ($checkoutCustomerDataFields as $checkoutField)
			{
				$checkoutValue = CheckoutFieldValue::where(['ticket_id' => $ticketId, 'checkout_field_id' => $checkoutField->id])->first();
				if ($checkoutValue)
				{
					$checkoutValue->update(['value' => request($checkoutField->name)]);
				}
				else
				{
					CheckoutFieldValue::create([
						                           'ticket_id'         => $ticketId,
						                           'checkout_field_id' => $checkoutField->id,
						                           'value'             => request($checkoutField->name)
					                           ]);
				}
			}

			$ticket->updated_at = Carbon::now();
			$ticket->save();

			return response()->json([
				                        'success' => true
			                        ]);

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());

			return response()->json([
				                        'success' => false,
			                        ]);
		}
	}

	public function validateCheckoutOrderDetails($ticketId)
	{
		try
		{

			$ticket = Ticket::find($ticketId);
			if (!$ticket)
			{
				Log::error("Unable to find ticket for ticketId: $ticketId");

				return response()->json([
					                        'success' => false,
				                        ]);
			}

			$rules = [];

			$messages = [];

			// dynamic checkout fields based on `ticket_booking` values
			$checkoutFieldValues = request('checkoutFieldValues') ?: null;
			if (isset($checkoutFieldValues) && $checkoutFieldValues)
			{
				$checkoutPageFields = $this->getCheckoutPageFields($ticketId, 0);
				if (count($checkoutPageFields))
				{
					foreach ($checkoutPageFields as $checkoutField)
					{
						$rules[$checkoutField->booking_checkout_unique_id] = 'required';
						$messages[$checkoutField->booking_checkout_unique_id . '.required'] = "$checkoutField->identifier is required. (enter n/a if not applicable)";
						if ($checkoutField->type_field_id == config('evibe.input.checkbox'))
						{
							// @see: options are stored as text separated by commas
							$messages[$checkoutField->booking_checkout_unique_id . '.required'] = "$checkoutField->identifier are required. Kindly select at least one choice.";
						}
					}
				}

				$validation = Validator::make(request('checkoutFieldValues'), $rules, $messages);

				if ($validation->fails())
				{
					return response()->json([
						                        'success'  => false,
						                        'errorMsg' => $validation->messages()->first()
					                        ]);
				}
			}

			// validate images
			$uploadImage = request('uploadImage') ?: 0;
			$uploadSuccess = request('uploadSuccess') ?: 0;
			if (isset($uploadImage) && $uploadImage == 1)
			{
				if (isset($uploadSuccess) && $uploadSuccess == 0)
				{
					return response()->json([
						                        'success'  => false,
						                        'errorMsg' => 'Kindly upload the image/s'
					                        ]);
				}
			}

			// save the checkout field values.
			if (isset($checkoutFieldValues) && $checkoutFieldValues)
			{
				$checkoutFields = $this->getCheckoutPageFields($ticketId, 0);
				if (count($checkoutFields))
				{
					foreach ($checkoutFields as $checkoutField)
					{
						$checkoutValue = CheckoutFieldValue::where(['ticket_id' => $ticketId, 'ticket_booking_id' => $checkoutField->ticket_booking_id, 'checkout_field_id' => $checkoutField->id])->first();
						if ($checkoutValue)
						{
							$checkoutValue->update(['value' => $checkoutFieldValues[$checkoutField->booking_checkout_unique_id]]);
						}
						else
						{
							CheckoutFieldValue::create([
								                           'ticket_id'         => $ticketId,
								                           'ticket_booking_id' => $checkoutField->ticket_booking_id,
								                           'checkout_field_id' => $checkoutField->id,
								                           'value'             => $checkoutFieldValues[$checkoutField->booking_checkout_unique_id]
							                           ]);
						}
					}
				}
			}

			$ticket->spl_notes = request('splNotes');
			// @see: splitting of special notes specific to each booking is being done in BasePaymentController

			$ticket->e_invite = request('eInvite') ? 1 : 0;

			$ticket->updated_at = Carbon::now();
			$ticket->save();

			return response()->json([
				                        'success' => true
			                        ]);

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());

			return response()->json([
				                        'success' => false,
			                        ]);
		}
	}

	public function validateCheckoutProofDetails($ticketId)
	{
		try
		{

			$ticket = Ticket::find($ticketId);
			if (!$ticket)
			{
				Log::error("Unable to find ticket for ticketId: $ticketId");

				return response()->json([
					                        'success' => false,
				                        ]);
			}

			$proofUploadChoice = request('proofUploadChoice');
			$accountabilityCheck = request('accountabilityCheck');
			$cancellationCheck = request('cancellationCheck');
			$needCustomerProof = request('needCustomerProof');
			$proofUploadSuccess = request('proofUploadSuccess');

			if ($needCustomerProof)
			{
				if ($proofUploadChoice)
				{
					if (($proofUploadChoice == 1) && !$proofUploadSuccess)
					{
						return response()->json([
							                        'success'  => false,
							                        'errorMsg' => 'Kindly upload ID proof'
						                        ]);
					}
				}
				else
				{
					if ($proofUploadSuccess)
					{
						// already uploaded
					}
					else
					{
						return response()->json([
							                        'success'  => false,
							                        'errorMsg' => 'Kindly select when you want to upload your ID proof'
						                        ]);
					}
				}
			}

			if (!$accountabilityCheck)
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'Kindly agree to accountability'
				                        ]);
			}

			if ($proofUploadChoice && ($proofUploadChoice != 1) && !$cancellationCheck)
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'Kindly agree to cancellation policy'
				                        ]);
			}

			return response()->json([
				                        'success' => true
			                        ]);

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());

			return response()->json([
				                        'success' => false,
			                        ]);
		}
	}

	public function validateCheckoutContactDetails($ticketId)
	{
		try
		{

			$ticket = Ticket::find($ticketId);
			if (!$ticket)
			{
				Log::error("Unable to find ticket for ticketId: $ticketId");

				return response()->json([
					                        'success' => false,
				                        ]);
			}

			$rules = [
				'phone'    => 'required|digits:10',
				'altPhone' => 'sometimes|required|digits:10',
				'name'     => 'required|min:4',
				'title'    => 'required',
				'email'    => 'sometimes|required|email',
			];

			$messages = [
				'name.required'     => 'Your full name is required',
				'name.min'          => 'Your full name cannot be less than 4 characters',
				'email.required'    => 'Your email id is required',
				'email.email'       => 'Your email id should be in the form of example@abc.com',
				'phone.required'    => 'Your phone number is required',
				'phone.digits'      => 'Phone number is not valid. Enter only 10 digits number (ex: 9640204000)',
				'altPhone.required' => 'Your alternate phone number is required',
				'altPhone.digits'   => 'Alternate phone number is not valid. Enter only 10 digits number (ex: 9640204000)',
				'title.required'    => 'Please select your title (Mr. / Mrs.)'
			];

			$validation = Validator::make(request()->all(), $rules, $messages);

			if ($validation->fails())
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => $validation->messages()->first()
				                        ]);
			}

			return response()->json([
				                        'success' => true
			                        ]);

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());

			return response()->json([
				                        'success' => false,
			                        ]);
		}
	}

	public function updateCheckoutOrderDetails($ticketId)
	{
		try
		{

			$ticket = Ticket::with('bookings', 'bookings.mapping', 'bookings.provider')
			                ->findOrFail($ticketId);
			if (!is_null($ticket))
			{
				$ticketBookings = $ticket->bookings; // get list of all bookings

				$eventId = $ticket->event_id;
				$cityId = $ticket->city_id;
				if (count($ticketBookings) > 0)
				{
					$data = $this->getTicketData($ticket);
					$totalBookingAmount = 0;
					$totalAdvanceToPay = 0;
					$hasVenueBooking = false;
					$isShowVenueType = false;
					$hasAddOn = false;
					$imageUpload = false;

					foreach ($ticketBookings as $booking)
					{
						$mapTypeId = $booking->mapping->map_type_id;
						$mapId = $booking->mapping->map_id;
						$mappedValues = $this->fillMappingValues([
							                                         'isCollection' => false,
							                                         'mapId'        => $mapId,
							                                         'mapTypeId'    => $mapTypeId
						                                         ]);

						$bookingData = $this->getTicketBookingData($booking, $mappedValues);
						$data['bookings']['list'][] = $bookingData;

						if (isset($bookingData['itemMapTypeId']) && ($bookingData['itemMapTypeId'] == config('evibe.ticket.type.decor')) && (isset($data['additional']['partyStartTime'])))
						{
							$data['additional']['partyStartTime'] = null;
						}

						$totalBookingAmount += $booking->booking_amount;
						$totalAdvanceToPay += ($booking->is_advance_paid == 1) ? 0 : $booking->advance_amount;

						// @see: assuming there will not be more than one venue booking per ticket
						if ($booking->is_venue_booking)
						{
							$hasVenueBooking = true;
							// todo: decide party date times for ab here only
							$data['additional']['partyStartTime'] = $booking->party_date_time ? date("g:i A", $booking->party_date_time) : null;
						}

						if (isset($bookingData['itemMapTypeId']) && ($bookingData['itemMapTypeId'] == config('evibe.ticket.type.decor')))
						{
							$isShowVenueType = true;
						}

						if (isset($bookingData['itemMapTypeId']) && ($bookingData['itemMapTypeId'] == config('evibe.ticket.type.add-on')))
						{
							$hasAddOn = true;
						}

						if (isset($bookingData['imageUpload']) && ($bookingData['imageUpload']))
						{
							$imageUpload = true;
						}
					}

					if (in_array($eventId, [
						config('evibe.occasion.surprises.id'),
						config('evibe.occasion.kids_birthdays.id'),
						config('evibe.occasion.first_birthday.id'),
						config('evibe.occasion.birthday_2-5.id'),
						config('evibe.occasion.birthday_6-12.id'),
						config('evibe.occasion.naming_ceremony.id'),
						config('evibe.occasion.baby-shower.id')
					]))
					{
						$arrayAddOns = [];
						foreach ($ticketBookings as $ticketBooking)
						{
							$mapping = $ticketBooking->mapping()->first();
							$productAddOns = $this->getTicketApplicableAddOns($cityId, $eventId, $mapping->map_type_id, $mapping->map_id, $ticketBooking);
							$arrayAddOns = array_merge($arrayAddOns, $productAddOns);
						}
						$arrayAddOns = array_unique($arrayAddOns);

						// todo edit after test
						if (count($arrayAddOns))
						{
							$data['addOns'] = $arrayAddOns;
							$data['selectedAddOnIds'] = [];

							// already selected add-ons
							foreach ($ticketBookings as $ticketBooking)
							{
								$mapping = $ticketBooking->mapping()->first();
								if ($mapping->map_type_id == config('evibe.ticket.type.add-on'))
								{
									$data['selectedAddOnIds'][$mapping->map_id] = [
										'bookingUnits' => $ticketBooking->booking_units ?: 1
									];
								}
							}
						}
					}

					$data['couponToken'] = $this->getAccessTokenForCoupon(intval($ticketId), $ticket->created_at);

					$data['bookings']['minBookingDateTime'] = $ticketBookings->min('party_date_time');
					$data['bookings']['maxBookingDateTime'] = $ticketBookings->max('party_date_time');

					$data['bookings']['totalBookingAmount'] = $totalBookingAmount;
					$data['bookings']['totalAdvanceToPay'] = $totalAdvanceToPay;
					$data['hasVenueBooking'] = $hasVenueBooking;
					$data['isShowVenueType'] = $isShowVenueType;
					$data['hasAddOn'] = $hasAddOn;
					$data['uploadImage'] = $imageUpload;
					$data['isAutoBooking'] = true;

					// conditions to show extra benefits selection
					$eventId = $ticket->event_id;
					$timeDifference = $ticket->event_date - time();

					$kidsEvents = [
						config('evibe.occasion.kids_birthdays.id'),
						config('evibe.occasion.first_birthday.id'),
						config('evibe.occasion.birthday_2-5.id'),
						config('evibe.occasion.birthday_6-12.id'),
						config('evibe.occasion.naming_ceremony.id'),
						config('evibe.occasion.baby-shower.id')
					];

					$data['extraBenefits'] = false;
					$data['invite'] = false;
					if (in_array($eventId, $kidsEvents))
					{
						$data['extraBenefits'] = true;
						$data['invite'] = true;

						// Removing the restriction of showing service only few users
						//if (($timeDifference > 0) && (($timeDifference / (24 * 60 * 60)) >= 2))
						//{
						//	$data['invite'] = true;
						//}
					}

					if (in_array($data['additional']['eventId'], [
						config('evibe.occasion.surprises.id'),
						config('evibe.occasion.kids_birthdays.id'),
						config('evibe.occasion.first_birthday.id'),
						config('evibe.occasion.birthday_2-5.id'),
						config('evibe.occasion.birthday_6-12.id'),
						config('evibe.occasion.naming_ceremony.id'),
						config('evibe.occasion.baby-shower.id')
					]))
					{
						$arrayAddOns = [];
						foreach ($ticketBookings as $ticketBooking)
						{
							$mapping = $ticketBooking->mapping()->first();
							$productAddOns = $this->getTicketApplicableAddOns($data['additional']['cityId'], $eventId, $mapping->map_type_id, $mapping->map_id);
							$arrayAddOns = array_merge($arrayAddOns, $productAddOns);
						}
						$arrayAddOns = array_unique($arrayAddOns);
						$data['addOns'] = $arrayAddOns;

						$addOns = collect($arrayAddOns);
						if (count($addOns))
						{
							$data['addOnTypeTicketBookingIds'] = $addOns->pluck('type_ticket_booking_id')->toArray();
							$data['addOnTypeTicketBookingIds'] = array_unique(array_filter($data['addOnTypeTicketBookingIds']));
						}
					}

					return view('pay/checkout/nav-cards/order-details-content', ['data' => $data]);
				}
				else
				{
					Log::error("No bookings available::ticketId::" . $ticketId);

					return "";
				}
			}
			else
			{
				SiteErrorLog::create([
					                     'url'        => request()->fullUrl(),
					                     'exception'  => "Ticket Not Found",
					                     'code'       => config('evibe.error_code.fetch_function'),
					                     'details'    => "Some error occurred in BaseAutoBookingHandlerController [Line: " . __LINE__ . "]. Unable to find ticket [Ticket id: $ticketId]",
					                     'created_at' => date('Y-m-d H:i:s'),
					                     'updated_at' => date('Y-m-d H:i:s')
				                     ]);

				return "";
			}

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());

			return response()->json([
				                        'error' => true,
			                        ]);
		}
	}

	public function updateCheckoutPriceDetails($ticketId)
	{
		try
		{

			$ticket = Ticket::with('bookings', 'bookings.mapping', 'bookings.provider')
			                ->findOrFail($ticketId);
			if (!is_null($ticket))
			{
				$ticketBookings = $ticket->bookings; // get list of all bookings

				if (count($ticketBookings) > 0)
				{
					$totalBookingAmount = 0;
					$totalAdvanceToPay = 0;
					$couponDiscountAmount = 0;
					$orderPriceDetails = [];

					foreach ($ticketBookings as $booking)
					{
						$totalBookingAmount += $booking->booking_amount;
						$totalAdvanceToPay += ($booking->is_advance_paid == 1) ? 0 : $booking->advance_amount;
						$couponDiscountAmount += $booking->prepay_discount_amount;

						$mapping = $booking->mapping;
						$productTitle = "";
						$productWorthAmount = 0;
						if ($mapping)
						{
							if ($ticket->is_auto_booked)
							{
								$productTitle = $booking->booking_info;

								switch ($mapping->map_type_id)
								{
									case config('evibe.ticket.type.package'):
									case config('evibe.ticket.type.food'):
									case config('evibe.ticket.type.surprises'):
									case config('evibe.ticket.type.priests'):
									case config('evibe.ticket.type.tents'):
									case config('evibe.ticket.type.generic-package'):
										$package = Package::find($mapping->map_id);
										$productWorthAmount = ($package && $package->price_worth) ? $package->price_worth : $productWorthAmount;
										break;

									case config('evibe.ticket.type.cake'):
										// since cake extra params are not being considered in booking, do not calculate worth for cake
										$productWorthAmount = 0;
										break;

									case config('evibe.ticket.type.decor'):
										$decor = Decor::find($mapping->map_id);
										$productWorthAmount = ($decor && $decor->worth) ? $decor->worth : $productWorthAmount;
										break;

									case config('evibe.ticket.type.entertainment'):
										$entService = TypeServices::find($mapping->map_id);
										$productWorthAmount = ($entService && $entService->worth_price) ? $entService->worth_price : $productWorthAmount;
										break;

									case config('evibe.ticket.type.add-on'):
										$addOn = AddOn::find($mapping->map_id);
										$productWorthAmount = ($addOn && $addOn->price_worth_per_unit) ? $addOn->price_worth_per_unit : $productWorthAmount;
										break;
								}
							}
							else
							{
								$productTitle = $booking->booking_type_details;
							}

							if ($mapping->map_type_id == config('evibe.ticket.type.add-on'))
							{
								$addOn = AddOn::find($mapping->map_id);
								$productTitle = ($addOn && $addOn->name) ? $addOn->name : $productTitle;
							}
						}

						if (!$productTitle)
						{
							$productTitle = "Unique Service";
						}

						array_push($orderPriceDetails, [
							'productId'               => $mapping->map_id,
							'productTypeId'           => $mapping->map_type_id,
							'productTitle'            => $productTitle,
							'productPrice'            => $booking->product_price ?: $booking->booking_amount,
							'productBookingAmount'    => $booking->booking_amount,
							'productWorthAmount'      => $productWorthAmount,
							'productTransportCharges' => $booking->transport_charges,
							'isVenueBooking'          => $booking->is_venue_booking
						]);
					}

					// advance amount may be '0' in case of payment success tickets
					if ($totalBookingAmount)
					{
						return response()->json([
							                        'success'              => true,
							                        'advanceAmount'        => $totalAdvanceToPay,
							                        'bookingAmount'        => $totalBookingAmount,
							                        'couponDiscountAmount' => $couponDiscountAmount,
							                        'orderPriceDetails'    => $orderPriceDetails
						                        ]);
					}
					else
					{
						$this->sendNonExceptionErrorReportToTeam([
							                                         'code'      => config('evibe.error_code.update_function'),
							                                         'url'       => request()->fullUrl(),
							                                         'method'    => request()->method(),
							                                         'message'   => '[BaseAutoBookingHandlerController.php - ' . __LINE__ . '] ' . 'There is some issue in calculating booking and advance amount.',
							                                         'exception' => '[BaseAutoBookingHandlerController.php - ' . __LINE__ . '] ' . 'There is some issue in calculating booking and advance amount.',
							                                         'trace'     => '[Ticket Id: ' . $ticket->id . ']',
							                                         'details'   => '[Ticket Id: ' . $ticket->id . ']'
						                                         ]);

						return response()->json([
							                        'success' => false
						                        ]);
					}
				}
				else
				{
					Log::error("No bookings available::ticketId::" . $ticketId);

					return response()->json([
						                        'success' => false,
					                        ]);
				}
			}
			else
			{
				SiteErrorLog::create([
					                     'url'        => request()->fullUrl(),
					                     'exception'  => "Ticket Not Found",
					                     'code'       => config('evibe.error_code.fetch_function'),
					                     'details'    => "Some error occurred in BaseAutoBookingHandlerController [Line: " . __LINE__ . "]. Unable to find ticket [Ticket id: $ticketId]",
					                     'created_at' => date('Y-m-d H:i:s'),
					                     'updated_at' => date('Y-m-d H:i:s')
				                     ]);

				return response()->json([
					                        'success' => false,
				                        ]);
			}

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());

			return response()->json([
				                        'success' => false,
			                        ]);
		}
	}

	public function processPayment($ticketId)
	{
		$response = [
			'success'    => false,
			'error'      => 'Failed to send booked notifications',
			'redirectTo' => route('pay.common.fail')
		];

		if ($this->sendAutoBookedNotifications($ticketId))
		{
			$token = request()->input('token');
			$response['success'] = true;
			$response['redirectTo'] = route('auto-book.pay.auto.success', $ticketId) . "?token=" . $token;
			unset($response['error']);

			// handle post payment
			$this->dispatch(new HandlePostPaymentProcessJob($ticketId));

			// Update transaction success in DB
			$this->dispatch(new PaymentTransactionInfoUpdate([
				                                                 "ticketId"  => $ticketId,
				                                                 "isSuccess" => false
			                                                 ]));
		}

		return response()->json($response);
	}

	private function sendAutoBookedNotifications($ticketId, $isFromDash = false, $handlerId = null)
	{
		$ticket = Ticket::with('bookings.mapping', 'bookings.provider')->find($ticketId);
		$statusCheck = $this->checkStatusBeforeSendReceipts($ticket, $isFromDash); // returns PreSendReceiptStatus object

		if (!$statusCheck->getSuccess())
		{
			Log::error($statusCheck->getErrorMessage());

			return false;
		}

		// all bookings advance paid
		$ticketBookings = $statusCheck->getBookings();

		$allBookings = [];
		$totalBookingAmount = $totalAdvancePaid = $totalBalanceAmount = 0;
		$data = $this->getTicketData($ticket);
		$data["couponDiscount"] = $ticketBookings->sum('prepay_discount_amount');

		// check if ticket has venue booking, we need to this to
		// send payment success emails: include venue info / not
		$data['ticketVenueName'] = "";
		$data['ticketHasVenueBooking'] = false;
		$venueBookingCount = $ticketBookings->where('is_venue_booking', 1)->count();
		if ($venueBookingCount > 0)
		{
			$data['ticketHasVenueBooking'] = true;
			$venueBooking = $ticketBookings->where('is_venue_booking', 1)->first();
			$data['ticketVenueName'] = $venueBooking->provider->name;
		}

		$bookingPartners = [];
		$addOns = [];
		foreach ($ticketBookings as $booking)
		{
			// send email for each booking
			$mapTypeId = $booking->mapping->map_type_id;
			$mapId = $booking->mapping->map_id;
			$mappedValues = $this->fillMappingValues([
				                                         'isCollection' => false,
				                                         'mapId'        => $mapId,
				                                         'mapTypeId'    => $mapTypeId
			                                         ]);

			// calculating the total advance
			$ticketBooking = $allBookings[] = $this->getTicketBookingData($booking, $mappedValues);
			$totalBookingAmount += $ticketBooking['bookingAmount'];
			$totalAdvancePaid += $ticketBooking['advanceAmount'];
			$totalBalanceAmount += $ticketBooking['bookingAmount'] - $ticketBooking['advanceAmount'];

			$bookingPartners = [];
			if (isset($ticketBooking['provider']['id']) && $ticketBooking['provider']['id'] && isset($ticketBooking['provider']['providerTypeId']) && $ticketBooking['provider']['providerTypeId'])
			{
				array_push($bookingPartners, [
					'partnerId'     => $ticketBooking['provider']['id'],
					'partnerTypeId' => $ticketBooking['provider']['providerTypeId']
				]);
			}

			// customer email cc: alt customer email + team (handler + group)
			// vendor email cc: business group + all alt_email{1,2,3}
			$ccAddresses = [config('evibe.contact.customer.group')];
			if ($ticket->handler)
			{
				array_push($ccAddresses, $ticket->handler->username);
			}
			if ($ticket->alt_email)
			{
				array_push($ccAddresses, $ticket->alt_email);
			}
			$data['ccAddresses'] = $ccAddresses;
			//if (isset($ticketBooking['provider']) && $ticketBooking['provider'])
			//{
			//	$data['vendorCcAddresses'] = $this->getVendorCcAddresses($ticketBooking['provider']);
			//}
			$ticketBooking['bookingConceptName'] = $booking->bookingConcept ? $booking->bookingConcept->name : "";

			// vendor ask page URL
			// todo: should be booking specific
			//$token = Hash::make($data['customer']['phone']);
			//$confirmationLink = route('auto-book.actions.partner.ask', $data['ticketId']) . "?token=" . $token;
			//$data['vendorAskUrl'] = $this->getShortenUrl($confirmationLink);

			$data['bookings']['list'][] = $ticketBooking;

			// @see: app notification for each booking (needs to be changed)
			// todo: reactivate
			$invalidPartnerTypes = [
				config('evibe.ticket.type.add-on'),
				config('evibe.ticket.type.entertainment'),
				config('evibe.ticket.type.service')
			];

			if (in_array($mapTypeId, $invalidPartnerTypes))
			{
				// do not send app enquiry as it is based on mapping info, not booking partner info
				// todo: fix this
			}
			else
			{
				$this->createAutoBookingNotificationToApp($booking);
			}

			// customer proof
			if ($mapTypeId == config('evibe.ticket.type.villas'))
			{
				$data['needProof'] = true;
				$data['proofUpload'] = false;

				$customerUploadProof = CustomerOrderProof::where('ticket_id', $ticketId)
				                                         ->whereNull('deleted_at')
				                                         ->whereNull('rejected_at')
				                                         ->get();

				if (count($customerUploadProof) >= 2)
				{
					$data['proofUpload'] = true;
				}
			}

			$bookingAddOns = $this->fetchAddOns($ticket->city_id, $ticket->event_id, $booking->mapping->map_type_id, $booking->mapping->map_id, $booking);
			if (is_array($bookingAddOns))
			{
				$addOns = array_merge($addOns, $bookingAddOns);
			}
		}

		$data['addOns'] = collect($addOns)->unique()->take(4);
		$tmoToken = Hash::make($ticketId . config('evibe.token.track-my-order'));
		$tmoLink = route("track.orders") . "?ref=email&id=" . $ticketId . "&token=" . $tmoToken;
		$data['tmoLink'] = $tmoLink;

		$data['bookings']['minBookingDateTime'] = $ticketBookings->min('party_date_time');
		$data['bookings']['maxBookingDateTime'] = $ticketBookings->max('party_date_time');

		// Managing coupon & internet handling fee
		// Coupon discount will effect only total advance paid
		$totalBookingAmount += $data['additional']['internetHandlingFee'];
		$totalAdvancePaid += $data['additional']['internetHandlingFee'] - $data["couponDiscount"];

		$data['totalBookingAmount'] = $totalBookingAmount;
		$data['totalBookingAmountStr'] = $totalBookingAmount ? $this->formatPrice($totalBookingAmount) : 0;
		$data['totalAdvancePaid'] = $totalAdvancePaid;
		$data['totalAdvancePaidStr'] = $totalAdvancePaid ? $this->formatPrice($totalAdvancePaid) : 0;
		$data['totalBalanceAmount'] = $totalBalanceAmount;
		$data['totalBalanceAmountStr'] = $totalBalanceAmount ? $this->formatPrice($totalBalanceAmount) : 0;
		//$data['allBookings'] = $allBookings;

		$partyDateTime = getPartyDateTimeByBookingsPartyTime($data['bookings']['minBookingDateTime'], $data['bookings']['maxBookingDateTime']);
		$data['partyDateTime'] = $partyDateTime;
		$data['typeTicketAddOn'] = config('evibe.ticket.type.add-on');

		// todo: unique booking partners
		$bookingPartners = $this->uniqueMultidimArray($bookingPartners, ['partnerId', 'partnerTypeId']);

		if (count($bookingPartners))
		{
			// todo: send notification to each partner
			foreach ($bookingPartners as $key => $bookingPartner)
			{
				$bookingPartners[$key]['bookings'] = [];

				// attach required data
				foreach ($data['bookings']['list'] as $booking)
				{
					if (isset($booking['provider']['id']) &&
						$booking['provider']['id'] == $bookingPartner['partnerId'] &&
						isset($booking['provider']['providerTypeId']) &&
						$booking['provider']['providerTypeId'] == $bookingPartner['partnerTypeId'])
					{
						if (!isset($bookingPartner['self']) && $booking['provider'])
						{
							$bookingPartners[$key] = array_merge($bookingPartners[$key], $booking['provider']);
						}

						array_push($bookingPartners[$key]['bookings'], $booking);
					}
				}

				// fetch booking data in job
				if (isset($bookingPartners[$key]['bookings']) && count($bookingPartners[$key]['bookings']))
				{
					$bookingPartners[$key]['additional'] = isset($data['additional']) ? $data['additional'] : [];
					$bookingPartners[$key]['customer'] = isset($data['customer']) ? $data['customer'] : [];
					$bookingPartners[$key]['gallery'] = isset($data['gallery']) ? $data['gallery'] : [];
					$bookingPartners[$key]['ticketId'] = isset($data['ticketId']) ? $data['ticketId'] : null;
					$bookingPartners[$key]['typeTicketAddOn'] = config('evibe.ticket.type.add-on');

					$this->initAutoBookedNotificationToPartner($bookingPartners[$key]);
					// @see: should est how partner is receiving avail check
					// todo: Test. Otherwise shift to booking wise notification for now
					//$this->createAutoBookingOrderNotificationToApp($data['ticketId'], $bookingPartners[$key]['id'], $bookingPartners[$key]['providerTypeId']);
				}
			}
		}

		// send single email & sms to customer
		$this->initAutoBookedNotificationToCustomer($data);

		// send email notification to team
		$this->dispatch(new MailABAlertToTeamJob($data));

		if ($ticket->e_invite)
		{
			// send e-invite communication to host
			$this->initEInviteNotifications($data);
		}

		$statusId = config('evibe.ticket.status.auto_paid');
		if ($ticket->type_ticket_id == config('evibe.ticket.type.entertainment'))
		{
			$statusId = config('evibe.ticket.status.service_auto_pay');
		}

		// update timestamp & save action
		$forUpdateData = new PaySuccessUpdateTicket();
		$forUpdateData->setHandlerId($handlerId);
		$forUpdateData->setIsFromDash($isFromDash);
		$forUpdateData->setStatusId($statusId);
		$forUpdateData->setSuccessEmailType(config('evibe.ticket.success_type.booked'));

		return $this->updateTicketPostSendReceipts($ticket, $forUpdateData);
	}

	private function initAutoBookedNotificationToPartner($partnerData)
	{
		$minBookingDateTime = 0;
		$maxBookingDateTime = 0;
		$totalBookingAmount = 0;
		$totalAdvancePaid = 0;
		$totalBalanceAmount = 0;

		// todo: calculate min and max partyDateTime for partner bookings
		// assuming, auto bookings have same time for now
		foreach ($partnerData['bookings'] as $booking)
		{
			if (!$minBookingDateTime)
			{
				$minBookingDateTime = $booking['partyDateTime'];
			}

			if (!$maxBookingDateTime)
			{
				$maxBookingDateTime = $booking['partyDateTime'];
			}

			if ($booking['partyDateTime'] < $minBookingDateTime)
			{
				$minBookingDateTime = $booking['partyDateTime'];
			}

			if ($booking['partyDateTime'] > $maxBookingDateTime)
			{
				$maxBookingDateTime = $booking['partyDateTime'];
			}

			$totalBookingAmount += $booking['bookingAmount'];
			$totalAdvancePaid += $booking['advanceAmount'];
			$totalBalanceAmount += $booking['bookingAmount'] - $booking['advanceAmount'];
		}

		$partnerData['totalBookingAmount'] = $totalBookingAmount;
		$partnerData['totalBookingAmountStr'] = $totalBookingAmount ? $this->formatPrice($totalBookingAmount) : 0;
		$partnerData['totalAdvancePaid'] = $totalAdvancePaid;
		$partnerData['totalAdvancePaidStr'] = $totalAdvancePaid ? $this->formatPrice($totalAdvancePaid) : 0;
		$partnerData['totalBalanceAmount'] = $totalBalanceAmount;
		$partnerData['totalBalanceAmountStr'] = $totalBalanceAmount ? $this->formatPrice($totalBalanceAmount) : 0;

		$partnerData['minBookingDateTime'] = $minBookingDateTime;
		$partnerData['maxBookingDateTime'] = $maxBookingDateTime;

		$partyDateTime = getPartyDateTimeByBookingsPartyTime($partnerData['minBookingDateTime'], $partnerData['maxBookingDateTime']);
		$partnerData['partyDateTime'] = $partyDateTime;

		$partnerData['vendorCcAddresses'] = $this->getVendorCcAddresses($partnerData);
		$partnerData['decorTypeId'] = config('evibe.ticket.type.decor');
		$partnerData['venueDealsTypeId'] = config('evibe.ticket.type.venue-deals');
		$partnerData['venuePartnerTypeId'] = config('evibe.ticket.type.venue');

		// vendor ask page URL
		$token = Hash::make($partnerData['customer']['phone']);
		// @see: no need of token as they are being sent as params
		//$partnerIdToken = Hash::make($partnerData['partnerId']);
		//$partnerTypeIdToken = Hash::make($partnerData['partnerTypeId']);
		$confirmationLink = route('auto-book.actions.partner.ask', [$partnerData['ticketId'], $partnerData['id'], $partnerData['providerTypeId']]) . "?token=" . $token;
		$partnerData['vendorAskUrl'] = $this->getShortenUrl($confirmationLink);

		// sms
		$firstBookingName = 'booking';
		if (isset($partnerData['bookings']) && count($partnerData['bookings']))
		{
			$firstBookingName = $partnerData['bookings'][0]['name'];
		}
		$tplVendor = config('evibe.sms_tpl.auto_book.decor.pay_success.partner');
		$replaces = [
			'#field1#' => $partnerData['person'],
			'#field2#' => $partnerData['partyDateTime'],
			'#field3#' => $this->truncateText($partnerData['additional']['venueLocation'], 10),
			'#field4#' => $this->truncateText($firstBookingName, 10),
			'#field5#' => $this->formatPrice($partnerData['totalAdvancePaid']),
			'#field6#' => $this->getShortenUrl($partnerData['vendorAskUrl'])
		];

		$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplVendor);
		$smsData = [
			'to'   => $partnerData['phone'],
			'text' => $smsText
		];

		// send email and sms to partner
		$this->dispatch(new MailAutoBookingPaymentAlertToPartnerJob($partnerData));
		$this->dispatch(new SMSAutoBookingPaymentAlertToPartnerJob($smsData));

		// todo: app notification
	}

	private function initAutoBookedNotificationToCustomer($data)
	{
		$token = Hash::make($data["ticketId"] . "EVBTMO");

		if (isset($data['needProof']) && $data['needProof'] && isset($data['proofUpload']) && (!$data['proofUpload']))
		{
			$tplCustomer = config('evibe.sms_tpl.auto_book.pay_success.customer_proof');
			$replaces = [
				'#customer#'      => $data['customer']['name'],
				'#advanceAmount#' => $this->formatPrice($data['totalAdvancePaid']),
				'#orderID#'       => $data['ticket']['enquiry_id'],
				'#tmoLink#'       => $this->getShortenUrl(route("track.orders") . "?ref=SMS&id=" . $data["ticketId"] . "&token=" . $token),
				'#hours#'         => "4"
			];
		}
		else
		{
			$tplCustomer = config('evibe.sms_tpl.auto_book.pay_success.customer');
			$replaces = [
				'#field1#' => $data['customer']['name'],
				'#field2#' => $this->formatPrice($data['totalAdvancePaid']),
				'#field3#' => $data['ticket']['enquiry_id'],
				'#field4#' => "4",
				'#field5#' => $this->getShortenUrl(route("track.orders") . "?ref=SMS&id=" . $data["ticketId"] . "&token=" . $token)
			];
		}

		$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);
		$smsData = [
			'to'   => $data['customer']['phone'],
			'text' => $smsText
		];

		$data['mailView'] = 'emails.auto-book.pay-success.customer-receipt';
		$data['mailSub'] = '[#' . $data["ticketId"] . '] Your payment is successful. Order to be Confirmed';

		// Sending null data to reduce the size of queue
		$queueData = $data;
		$relations = $queueData["ticket"]->getRelations();
		unset($relations['bookings']);
		$queueData["ticket"]->setRelations($relations);
		$queueData["booking"]["checkoutFields"] = [];

		//send sms and email to customer
		$this->dispatch(new MailAutoBookingPaymentReceiptToCustomerJob($queueData));
		$this->dispatch(new SMSAutoBookingPaymentSuccessToCustomerJob($smsData));
	}

	public function onPaymentSuccess($ticketId)
	{
		$token = request()->input('token');
		$ticket = Ticket::with('bookings.mapping', 'bookings.provider')->find($ticketId);
		if (!$ticket)
		{
			return "<h1>Invalid Request</h1>";
		}

		$city = City::find($ticket->city_id);
		$cityUrl = $city->url;

		if (Hash::check($ticket->phone, $token))
		{
			$data = $this->getTicketData($ticket);

			if ($ticket->e_invite)
			{
				// @see: not using this anymore
				//$data['eInviteFormUrl'] = config('evibe.e-invite.g-form-url');
			}

			$data['eventId'] = $ticket->event_id;
			$data['cancellationData'] = $this->calculateCustomerCancellationRefund();
			//$data = array_merge($data, $this->mergeView($cityUrl, $ticket->event_id));

			checkAndSetCitySessions($city);

			$tmoToken = Hash::make($ticketId . "EVBTMO");
			$data['tmoLink'] = route("track.orders") . "?ref=paymentSuccess&id=" . $ticketId . "&token=" . $tmoToken;

			if ($ticket->user_id && ($ticket->enquiry_source_id == config('evibe.ticket.enquiry_source.pb_checkout')) && $ticket->is_auto_booked)
			{
				$shortlist = $this->getShortlistFromUserId($ticket->user_id);
				if ($shortlist)
				{
					ShortlistOptions::where('shortlist_id', $shortlist->id)
					                ->whereNull('is_wishlist')
					                ->whereNull('deleted_at')
					                ->update([
						                         'is_auto_booked' => 1,
						                         'deleted_at'     => Carbon::now()
					                         ]);
				}
			}

			// regardless of whether e-invite has been selected or not
			if (in_array($ticket->event_id, [
				config('evibe.occasion.kids_birthdays.id'),
				config('evibe.occasion.naming_ceremony.id'),
				config('evibe.occasion.baby-shower.id'),
				config('evibe.occasion.first_birthday.id'),
				config('evibe.occasion.birthday_2-5.id'),
				config('evibe.occasion.birthday_6-12.id')
			]))
			{
				$data['invitePromotionLink'] = route('e-cards.invites');
			}

			Log::info("payment success page: " . $ticketId);

			$data["ticketBookings"] = $ticket->bookings;

			//return view('pay/success/base', ["data" => $data]);
			return view('pay.success.auto-book', ["data" => $data]);
		}
		else
		{
			SiteErrorLog::create([
				                     'url'        => request()->fullUrl(),
				                     'exception'  => "AutoBooking: Tried to hack",
				                     'code'       => "Error",
				                     'details'    => "Some error occurred in BaseAutoBookingHandlerController [Line: " . __LINE__ . "]. Tried to change the ticket id with invalid token :: $ticketId",
				                     'created_at' => date('Y-m-d H:i:s'),
				                     'updated_at' => date('Y-m-d H:i:s')
			                     ]);

			return "<h1>Invalid Request</h1>";
		}
	}

	public function validateTicket(AutoBookUtil $dataForValidation)
	{
		$ticketId = $dataForValidation->getTicketId();
		$token = $dataForValidation->getToken();
		$hashedStr = $dataForValidation->getHashedStr();
		$typeToken = $dataForValidation->getTypeToken();

		// Check with ticket id and token
		if ($ticketId && $token && Hash::check($ticketId, $token) && $hashedStr)
		{
			$ticket = Ticket::with('bookings', 'bookings.mapping', 'bookings.provider')
			                ->findOrFail($ticketId);

			// second level check with Phone/zip-code and Token
			$concatenatedStr = $ticket->phone;
			if ($typeToken == 2)
			{
				$concatenatedStr = config('evibe.campaign-token');
			}

			if (!$this->validateToken($concatenatedStr, $hashedStr, $ticketId))
			{
				return null;
			}

			return $ticket;
		}

		return null;
	}

	public function getNotificationData(PaySuccessUpdateTicket $dataForNotification)
	{
		$data = null;
		$ticketId = $dataForNotification->getTicketNotificationId();
		$isFromDash = $dataForNotification->getIsFromDash();
		$ticket = Ticket::with('bookings.mapping', 'bookings.provider')->find($ticketId);
		$statusCheck = $this->checkStatusBeforeSendReceipts($ticket, $isFromDash); // returns PreSendReceiptStatus object

		if (!$statusCheck->getSuccess())
		{
			Log::error($statusCheck->getErrorMessage());

			return $data;
		}

		// compile ticket & bookings data
		$firstBooking = $statusCheck->getBookings()->first();
		$data = $this->getTicketAndSingleBookingData($ticket, $firstBooking);

		if (is_array($data))
		{
			// customer email cc: alt customer email + team (handler + group)
			// vendor email cc: business group + all alt_email{1,2,3}
			$ccAddresses = [config('evibe.contact.customer.group')];
			if ($ticket->handler)
			{
				array_push($ccAddresses, $ticket->handler->username);
			}
			if ($ticket->alt_email)
			{
				array_push($ccAddresses, $ticket->alt_email);
			}

			$data['ccAddresses'] = $ccAddresses;
			// for services, no provider
			if (!($data['additional']['typeTicketId'] && $data['additional']['typeTicketId'] == config('evibe.ticket.type.entertainment')))
			{
				$data['vendorCcAddresses'] = $this->getVendorCcAddresses($data['booking']['provider']);
			}
			$data['firstBooking'] = $firstBooking;
			$data['ticket'] = $ticket;
		}

		return $data;
	}

	public function createAutoBookingNotificationToApp($booking)
	{
		$accessToken = $this->getAccessToken($booking);

		dispatch(new HandleAutoBookingNotificationToApp($booking, $accessToken));
	}

	public function createAutoBookingOrderNotificationToApp($ticketId, $partnerId, $partnerTypeId)
	{
		$ticket = Ticket::with('bookings', 'bookings.mapping')->find($ticketId);
		if (!$ticket)
		{
			// todo: report error
			Log::error("Unable to find ticket: $ticketId");
		}

		$bookings = $ticket->bookings()->where('map_id', $partnerId)->where('map_type_id', $partnerTypeId)->get();
		if (!count($bookings))
		{
			// todo: report error
			Log::error("Unable to find bookings for ticket: $ticketId");
		}
		$accessToken = $this->getAccessToken($bookings->first());

		dispatch(new HandleAutoBookingOrderNotificationToApp($ticket, $bookings, $accessToken));
	}

	public function sendNotificationAndConfirmAutoBooking($data)
	{
		if (!$this->captchaCheck())
		{
			return redirect()->back();
		}

		$data['type'] = 1; // for accept

		$ticket = Ticket::find($data['ticketId']);
		$data['typeTicketId'] = $ticket->type_ticket_id;

		// make an Api Request
		try
		{
			$res = $this->makeApiRequestForSendingPaymentNotification($data);
		} catch (\Exception $e)
		{
			// send notification to team
			$this->reportPaymentActionNotificationError($e, $data['ticketId'], 'success');

			return "<h1>Some error occurred </h1>";
		}

		if (isset($res['hasStatusError']) && $res['hasStatusError'])
		{
			$error = isset($res['error']) && strlen($res['error'] > 0) ? $res['error'] : "Trying to send auto booking confirmation email when ticket status failed ::" . $data['ticketId'];
			Log::error("BaseAutoBookingController :: 48 :: " . $error);

			return "<h1>Invalid Request</h1>";
		}

		if (isset($res['hasTokenMismatchError']) && $res['hasTokenMismatchError'])
		{
			Log::error('BaseAutoBookingController :: 55 :: HACK :: Token mismatch while confirming the ticket :: ' . $data['ticketId']);

			return "<h1>Invalid Request</h1>";
		}

		if (isset($res['success']) && $res['success'])
		{
			return redirect($data['redirectRoute']);
		}
	}

	public function sendNotificationAndRejectAutoBooking($data)
	{
		if (!$this->captchaCheck())
		{
			return redirect()->back();
		}

		$data['type'] = 2; // for reject

		$ticket = Ticket::find($data['ticketId']);
		$data['typeTicketId'] = $ticket->type_ticket_id;

		try
		{
			$res = $this->makeApiRequestForSendingPaymentNotification($data);
		} catch (\Exception $e)
		{
			// report notification error to team
			$this->reportPaymentActionNotificationError($e, $data['ticketId'], 'cancel');

			return "<h1>Some error occurred </h1>";
		}

		if (isset($res['hasStatusError']) && $res['hasStatusError'])
		{
			$error = isset($res['error']) && strlen($res['error'] > 0) ? $res['error'] : "Trying to send auto booking cancelled email when ticket status failed ::" . $data['ticketId'];
			Log::error($error);

			return "<h1>Invalid Request</h1>";
		}

		if (isset($res['hasTokenMismatchError']) && $res['hasTokenMismatchError'])
		{
			Log::error('HACK :: Token mismatch while confirming the ticket :: ' . $data['ticketId']);

			return "<h1>Invalid Request</h1>";
		}

		if (isset($res['success']) && $res['success'])
		{
			return redirect($data['redirectRoute']);
		}

		return false;
	}

	public function sendNotificationAndConfirmPartnerBookings($data)
	{
		if (!$this->captchaCheck())
		{
			return redirect()->back();
		}

		$data['type'] = 1; // for accept

		$ticket = Ticket::find($data['ticketId']);
		$data['typeTicketId'] = $ticket->type_ticket_id;

		// make an Api Request
		try
		{
			$res = $this->makeApiRequestForSendingPartnerBookingPaymentNotification($data);
		} catch (\Exception $e)
		{
			// send notification to team
			$this->reportPaymentActionNotificationError($e, $data['ticketId'], 'success');

			return "<h1>Some error occurred </h1>";
		}

		if (isset($res['hasStatusError']) && $res['hasStatusError'])
		{
			$error = isset($res['error']) && strlen($res['error'] > 0) ? $res['error'] : "Trying to send auto booking confirmation email when ticket status failed ::" . $data['ticketId'];
			Log::error("BaseAutoBookingController :: 48 :: " . $error);

			return "<h1>Invalid Request</h1>";
		}

		if (isset($res['hasTokenMismatchError']) && $res['hasTokenMismatchError'])
		{
			Log::error('BaseAutoBookingController :: 55 :: HACK :: Token mismatch while confirming the ticket :: ' . $data['ticketId']);

			return "<h1>Invalid Request</h1>";
		}

		if (isset($res['success']) && $res['success'])
		{
			return redirect($data['redirectRoute']);
		}
	}

	public function sendNotificationAndRejectPartnerBookings($data)
	{
		if (!$this->captchaCheck())
		{
			return redirect()->back();
		}

		$data['type'] = 2; // for reject

		$ticket = Ticket::find($data['ticketId']);
		$data['typeTicketId'] = $ticket->type_ticket_id;

		try
		{
			$res = $this->makeApiRequestForSendingPartnerBookingPaymentNotification($data);
		} catch (\Exception $e)
		{
			// report notification error to team
			$this->reportPaymentActionNotificationError($e, $data['ticketId'], 'cancel');

			return "<h1>Some error occurred </h1>";
		}

		if (isset($res['hasStatusError']) && $res['hasStatusError'])
		{
			$error = isset($res['error']) && strlen($res['error'] > 0) ? $res['error'] : "Trying to send auto booking cancelled email when ticket status failed ::" . $data['ticketId'];
			Log::error($error);

			return "<h1>Invalid Request</h1>";
		}

		if (isset($res['hasTokenMismatchError']) && $res['hasTokenMismatchError'])
		{
			Log::error('HACK :: Token mismatch while confirming the ticket :: ' . $data['ticketId']);

			return "<h1>Invalid Request</h1>";
		}

		if (isset($res['success']) && $res['success'])
		{
			return redirect($data['redirectRoute']);
		}

		return false;
	}

	public function getDecorCheckoutFields($eventId, $typeTicketBookingId)
	{
		$checkoutFields = CheckoutField::where('type_ticket_booking_id', $typeTicketBookingId)
		                               ->where(function ($query) use ($eventId) {
			                               $query->where('event_id', $eventId)
			                                     ->orWhereNull('event_id');
		                               })
		                               ->where('is_crm', 0)
		                               ->where('is_auto_booking', 1)
		                               ->get();

		return $checkoutFields;
	}

	public function getDecorCheckoutFieldsWithValuesInArray($decorFields, $ticketId)
	{
		$decorCheckoutFields = [];

		foreach ($decorFields as $decorField)
		{
			$decorCheckoutField = [
				$decorField->identifier => $this->getDecorCheckoutValue($ticketId, $decorField->id)
			];
			$decorCheckoutFields = array_merge($decorCheckoutFields, $decorCheckoutField);
		}

		return $decorCheckoutFields;
	}

	public function fetchPrice($optionId, $optionTypeId, $occasionId)
	{
		// @todo: include 'location' and 'locationDetails' after fixing google auto complete API and remove 'partyArea'
		$rules = [
			'name'         => 'required|min:4',
			'phone'        => 'required|phone|min:10',
			'email'        => 'required|email',
			'partyDate'    => 'required_if:unsurePartyDate,0|date|after:today',
			//'location'     => 'required|min:4',
			'partyArea'    => 'required|min:4',
			'partyPinCode' => 'required|digits:6'
		];

		$message = [
			'name.required'         => 'Please enter your name',
			'name.min'              => 'Name cannot be less than 4 character',
			'email.required'        => 'Please enter your email',
			'email.email'           => 'Please enter a valid email',
			'phone.required'        => 'Please enter your phone',
			'phone.phone'           => 'Please enter you 10 digit valid phone number',
			//'location.required'     => 'Please enter party location',
			//'location.min'          => 'Location cannot be less than 4 characters',
			'location.required'     => 'Please enter party location',
			'location.min'          => 'Location cannot be less than 4 characters',
			'partyDate.required_if' => 'Please enter your party date',
			'partyDate.date'        => 'Please enter a valid party date',
			'partyDate.after'       => 'Please enter a party date after today',
			'partyArea.required'    => 'Please enter the party area name',
			'partyPinCode.required' => 'Please enter the Pin Code of your party location',
			'partyPinCode.digits'   => 'Please enter a valid Pin Code'
		];

		$validator = validator(request()->all(), $rules, $message);
		if ($validator->fails())
		{
			return response()->json(['success' => false, 'error' => $validator->messages()->first()]);
		}

		switch ($optionTypeId)
		{
			case config('evibe.ticket.type.decor'):
				$option = Decor::find($optionId);
				if (!$option)
				{
					$this->logError("Failed to find option with id $optionId");

					return response()->json([
						                        "success" => false,
						                        "error"   => "Decor not found"
					                        ]);
				}

				// @todo: after google auto complete integration
				//$areaData = $this->getAreaForTheLocation(request("location"), request("locationDetails"));
				//$areaId = $areaData['areaId'];
				//$areaName = $areaData['areaName'];
				$name = request('name');
				$email = request('email');
				$phone = request('phone');
				$partyArea = request('partyArea');
				$locationDetails = request('locationDetails');
				$partyPinCode = request('partyPinCode');
				$providerPinCode = request('providerPinCode');
				$unsurePartyDate = request('unsurePartyDate');
				$partyDate = request("partyDate") ? strtotime(str_replace("-", "/", request("partyDate"))) : null;

				$areaData = $this->getAreaForTheLocation($partyArea, $locationDetails);
				$areaId = $areaData['areaId'];
				$areaName = $areaData['areaName'];

				if (isset($areaData['cityId']) && $areaData['cityId'] != getCityId())
				{
					$this->logError("Entered other city location");

					return response()->json([
						                        "success" => false,
						                        "error"   => "Kindly enter a location, which belongs to " . getCityName() . " city"
					                        ]);
				}

				$inputData = [
					"city_id"           => getCityId(),
					"event_id"          => $occasionId,
					"type_id"           => $optionId,
					"type_ticket_id"    => config("evibe.ticket.type.decor"),
					"name"              => $name,
					"phone"             => $phone,
					"email"             => $email,
					"event_date"        => $partyDate,
					"unsure_party_date" => $unsurePartyDate ? $unsurePartyDate : 0,
					"party_location"    => $areaName,
					"area_id"           => $areaId,
					"zip_code"          => $partyPinCode,
					"enquiry_source_id" => config('evibe.ticket.enquiry_source.product_enquiry')
				];

				if (getCityId() == -1)
				{
					$this->setCityFromPreviousUrl();
				}

				$inputData["city_id"] = getCityId();

				// @todo: check for user, if not create
				$existingUser = User::where('username', $email)
				                    ->orWhere('phone', $phone)// @see: Is validation based only on email?
				                    ->first();

				// @todo: what if partner(or) any with any other role comes as customer - 'role_id'

				if ($existingUser)
				{
					$userId = $existingUser->id;
				}
				else
				{
					$newUser = User::create([
						                        'name'       => $name,
						                        'phone'      => $phone,
						                        'username'   => $email,
						                        'role_id'    => config('evibe.roles.customer'),
						                        'created_at' => Carbon::now(),
						                        'updated_at' => Carbon::now(),
					                        ]);

					if ($newUser)
					{
						$userId = $newUser->id;
					}
				}

				// @todo: In ticket, store user_id
				$inputData['user_id'] = $userId; // @see: as 'user_id' in 'ticket' table is nullable
				if (!$userId)
				{
					// @todo: inform admin that a ticket has been raised without user_id
					// @todo: may be create a job, just to contact user regarding ticket related important info
				}

				// save ticket
				$ticket = Ticket::create($inputData);
				if (!$ticket)
				{
					$this->logError("Failed to create ticket, email: " . request("email") . ", phone: " . request("phone"));

					return response()->json([
						                        "success" => false,
						                        "error"   => "An error occurred while submitting your request. Please try again later."
					                        ]);
				}

				$this->updateTicketAction([
					                          'ticket'   => $ticket,
					                          'comments' => "Decor, normal ticket created by customer"
				                          ]);

				$ticketId = $ticket->id;
				$enquiryId = config("evibe.ticket.enq.pre.decor") . $ticketId;
				$ticket->enquiry_id = $enquiryId;
				$ticket->save();

				$dataForPriceCheck = new PriceCheck();
				$dataForPriceCheck->setOptionId($optionId);
				$dataForPriceCheck->setOptionTypeId($optionTypeId);
				$dataForPriceCheck->setPartyPinCode($partyPinCode);
				$dataForPriceCheck->setProviderPinCode($providerPinCode);
				$dataForPriceCheck->setDistFree(config('evibe.transport.free-kms'));

				$transPriceData = $this->getUpdatedPriceWithTransportationCharges($dataForPriceCheck);
				// @see: although same action in both functions, made as another (transport-prices)reusable separate function because, in future other factors might come in play

				if (!$transPriceData)
				{
					Log::error("Unable to fetch transportation charges for decor option: $optionId");

					return response()->json([
						                        "success" => false,
						                        "error"   => "An error occurred while submitting your request. Please try again later."
					                        ]);
				}

				// @todo: send notifications
				$emailContentData = new TicketsController();

				$emailContent = array_merge($inputData, $emailContentData->getEmailContentData($ticket, [
					"name" => $option->name,
					"code" => $option->code,
				]));
				$emailContent['ticketId'] = $ticket->id;
				$emailContent['unsurePartyDate'] = $ticket->unsure_party_date;
				$emailContent['totalBookingAmount'] = isset($transPriceData['totalBookingAmount']) ? $transPriceData['totalBookingAmount'] : null;
				$emailContent['transCharges'] = isset($transPriceData['transCharges']) ? $transPriceData['transCharges'] : null;
				if (isset($transPriceData['totalBookingAmount']) && isset($transPriceData['transCharges']))
				{
					$emailContent['baseBookingAmount'] = $transPriceData['totalBookingAmount'] - $transPriceData['transCharges'];
				}
				$emailContent['partyDist'] = isset($transPriceData['partyDist']) ? (int)($transPriceData['partyDist'] / 1000) : null;
				$emailContent['feasibility'] = 3;
				if (isset($transPriceData['partyDist']) && isset($transPriceData['distMaxMeters']))
				{
					if ($transPriceData['partyDist'] <= $transPriceData['distMaxMeters'])
					{
						$emailContent['feasibility'] = 2;
					}

					if (isset($transPriceData['distFreeMeters']) && ($transPriceData['partyDist'] <= $transPriceData['distFreeMeters']))
					{
						$emailContent['feasibility'] = 1;
					}
				}
				$emailContent['partyPinCode'] = $partyPinCode;
				$emailContent['partnerPinCode'] = $providerPinCode;
				$emailContent['dashLink'] = config('evibe.dash_host') . '/tickets/' . $ticket->id . '/information';

				$emailContentData->dispatchPriceCheckMailAlerts($emailContent);
				// @see: end notifications

				$transPriceData['success'] = true;

				return response()->json($transPriceData);

				break;

		}

		return response()->json(['success' => false]);

		// use sessions and update all functions accordingly
		// fetch all data - done
		// validate data - done
		// create user - done
		// check and store userId, ticketId in session
		// create ticket - done
		// update ticket - done
		// team notifications - done
		// calculate all price related values - done
		// render same data into feasibility check price cards - done

	}

	public function calculateBookingPrice($optionId, $optionTypeId, $occasionId)
	{
		$rules = [
			//'name'         => 'required|min:4',
			'phone'        => 'required|phone|min:10',
			//'email'        => 'required|email',
			'partyDate'    => 'required|date|after:today',
			//'location'     => 'required|min:4',
			//'partyArea'    => 'required|min:4',
			'partyPinCode' => 'required|digits:6'
		];

		$message = [
			'name.required'         => 'Please enter your name',
			'name.min'              => 'Name cannot be less than 4 character',
			'email.required'        => 'Please enter your email',
			'email.email'           => 'Please enter a valid email',
			'phone.required'        => 'Please enter your mobile number',
			'phone.phone'           => 'Please enter you 10 digit valid mobile number',
			'location.required'     => 'Please enter party location',
			'location.min'          => 'Location cannot be less than 4 characters',
			'partyDate.required_if' => 'Please enter your party date',
			'partyDate.date'        => 'Please enter a valid party date',
			'partyDate.after'       => 'Please enter a party date after today',
			'partyArea.required'    => 'Please enter the party area name',
			'partyPinCode.required' => 'Please enter the Pin Code of your party location',
			'partyPinCode.digits'   => 'Please enter a valid Pin Code'
		];

		$validator = validator(request()->all(), $rules, $message);
		if ($validator->fails())
		{
			return response()->json(['success' => false, 'error' => $validator->messages()->first()]);
		}

		switch ($optionTypeId)
		{
			case config('evibe.ticket.type.decor'):
				$option = $this->getProductData($optionId, $optionTypeId);
				if (!$option)
				{
					$this->logError("Failed to find option with id $optionId");

					return response()->json([
						                        "success" => false,
						                        "error"   => "Decor not found"
					                        ]);
				}

				$isLive = $option->is_live;

				if ($isLive == 0)
				{
					$data = [
						"customerMobile" => request('phone'),
						"partyDate"      => Carbon::createFromTimestamp(request("partyDate") ? strtotime(str_replace("-", "/", request("partyDate"))) : null)->toDateString(),
						"productType"    => "Decor",
						"pageLink"       => request('pageUrl')
					];
					$tplCustomer = config('evibe.sms_tpl.product_non_live');
					$replaces = [
						'#field1#' => "Customer",
						'#field2#' => config('evibe.contact.company.phone'),
					];

					$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);

					$smsData = [
						'to'   => $data["customerMobile"],
						'text' => $smsText
					];

					$this->dispatch(new SMSActiveNonLiveToCustomer($smsData));

					$this->dispatch(new MailTeamCustomerEnquireForNonActiveProduct($data));

					return response()->json([

						                        'success' => false,
						                        'error'   => "we are sorry that your package/service in unavailable. Our party planning expert will contact you shortly to help you out.",
						                        'is_live' => 0
					                        ]);
				}

				// @todo: after google auto complete integration
				//$areaData = $this->getAreaForTheLocation(request("location"), request("locationDetails"));
				//$areaId = $areaData['areaId'];
				//$areaName = $areaData['areaName'];
				$name = request('name');
				$email = request('email');
				$phone = request('phone');
				$partyArea = request('partyArea');
				$locationDetails = request('locationDetails');
				$partyPinCode = request('partyPinCode');
				$providerPinCode = request('providerPinCode');
				$unsurePartyDate = request('unsurePartyDate');
				$partyDate = request("partyDate") ? strtotime(str_replace("-", "/", request("partyDate"))) : null;

				//$areaData = $this->getAreaForTheLocation($partyArea, $locationDetails);
				//$areaId = $areaData['areaId'];
				//$areaName = $areaData['areaName'];
				//
				//if (isset($areaData['cityId']) && $areaData['cityId'] != getCityId())
				//{
				//	$this->logError("Entered other city location");
				//
				//	return response()->json([
				//		                        "success" => false,
				//		                        "error"   => "Kindly enter a location, which belongs to " . getCityName() . " city"
				//	                        ]);
				//}

				$inputData = [
					"city_id"           => getCityId(),
					"event_id"          => $occasionId,
					"type_id"           => $optionId,
					"type_ticket_id"    => config("evibe.ticket.type.decor"),
					"name"              => $name,
					"phone"             => $phone,
					//"email"             => $email,
					"event_date"        => $partyDate,
					//"unsure_party_date" => $unsurePartyDate ? $unsurePartyDate : 0,
					//"party_location"    => $areaName,
					//"area_id"           => $areaId,
					"zip_code"          => $partyPinCode,
					"enquiry_source_id" => config('evibe.ticket.enquiry_source.product_enquiry')
				];

				if (getCityId() == -1)
				{
					$this->setCityFromPreviousUrl();
				}

				$inputData["city_id"] = getCityId();

				// @todo: check for user, if not create
				$existingUser = User::where('phone', $phone)// @see: Is validation based only on email?
				                    ->first();

				// @todo: what if partner(or) any with any other role comes as customer - 'role_id'

				if ($existingUser)
				{
					$inputData['user_id'] = $existingUser->id; // @see: as 'user_id' in 'ticket' table is nullable
				}
				// @todo: In ticket, store user_id

				// save ticket
				if ($phone && strlen($phone) == 10)
				{
					$pastTicket = Ticket::where('phone', 'LIKE', $phone)
					                    ->where('created_at', '>', Carbon::now()->subDays(10)->startOfDay())
					                    ->where('ticket.status_id', config('evibe.ticket.status.initiated'))
					                    ->first();

					if ($pastTicket)
					{
						$pastTicket->update([
							                    "event_id"       => $occasionId,
							                    "type_id"        => $optionId,
							                    "type_ticket_id" => config("evibe.ticket.type.decor"),
							                    "event_date"     => $partyDate,
							                    "zip_code"       => $partyPinCode,
						                    ]);

						$ticket = $pastTicket;
					}
					else
					{
						$ticket = Ticket::create($inputData);
					}
				}
				else
				{
					$ticket = Ticket::create($inputData);
				}

				if (!$ticket)
				{
					$this->logError("Failed to create ticket, email: " . request("email") . ", phone: " . request("phone"));

					return response()->json([
						                        "success" => false,
						                        "error"   => "An error occurred while submitting your request. Please try again later."
					                        ]);
				}

				$this->updateTicketAction([
					                          'ticket'   => $ticket,
					                          'comments' => "Decor, customer checked product availability for pincode - " . $partyPinCode
				                          ]);

				$ticketId = $ticket->id;
				$enquiryId = config("evibe.ticket.enq.pre.decor") . $ticketId;
				$ticket->enquiry_id = $enquiryId;
				$ticket->save();

				$dataForPriceCheck = new PriceCheck();
				$dataForPriceCheck->setOptionId($optionId);
				$dataForPriceCheck->setOptionTypeId($optionTypeId);
				$dataForPriceCheck->setPartyPinCode($partyPinCode);
				$dataForPriceCheck->setProviderPinCode($providerPinCode);
				$dataForPriceCheck->setDistFree(config('evibe.transport.free-kms'));

				$transPriceData = $this->getUpdatedPriceWithTransportationCharges($dataForPriceCheck);
				// @see: although same action in both functions, made as another (transport-prices)reusable separate function because, in future other factors might come in play

				if (!$transPriceData)
				{
					Log::error("Unable to fetch transportation charges for decor option: $optionId");

					return response()->json([
						                        "success" => false,
						                        "error"   => "An error occurred while submitting your request. Please try again later."
					                        ]);
				}

				// @todo: send notifications
				$emailContentData = new TicketsController();

				$emailContent = array_merge($inputData, $emailContentData->getEmailContentData($ticket, [
					"name" => $option->name,
					"code" => $option->code,
				]));
				$emailContent['ticketId'] = $ticket->id;
				//$emailContent['unsurePartyDate'] = $ticket->unsure_party_date;
				$emailContent['totalBookingAmount'] = isset($transPriceData['totalBookingAmount']) ? $transPriceData['totalBookingAmount'] : null;
				$emailContent['transCharges'] = isset($transPriceData['transCharges']) ? $transPriceData['transCharges'] : null;
				if (isset($transPriceData['totalBookingAmount']) && isset($transPriceData['transCharges']))
				{
					$emailContent['baseBookingAmount'] = $transPriceData['totalBookingAmount'] - $transPriceData['transCharges'];
				}
				$emailContent['partyDist'] = isset($transPriceData['partyDist']) ? (int)($transPriceData['partyDist'] / 1000) : null;
				$emailContent['feasibility'] = 3;
				if (isset($transPriceData['partyDist']) && isset($transPriceData['distMaxMeters']))
				{
					if ($transPriceData['partyDist'] <= $transPriceData['distMaxMeters'])
					{
						$emailContent['feasibility'] = 2;
					}

					if (isset($transPriceData['distFreeMeters']) && ($transPriceData['partyDist'] <= $transPriceData['distFreeMeters']))
					{
						$emailContent['feasibility'] = 1;
					}
				}
				$emailContent['partyPinCode'] = $partyPinCode;
				$emailContent['partnerPinCode'] = $providerPinCode;
				$emailContent['dashLink'] = config('evibe.dash_host') . '/tickets/' . $ticket->id . '/information';

				if ($ticket->user_id)
				{
					$user = User::find($ticket->user_id);
					$emailContent['name'] = $user->name;
					$emailContent['email'] = $user->username;
				}

				$emailContentData->dispatchPriceCheckMailAlerts($emailContent);
				// @see: end notifications

				$transPriceData['success'] = true;

				return response()->json($transPriceData);

				break;

		}

		return response()->json(['success' => false]);
	}

	public function calculatePriceForDelivery($mapId, $mapTypeId)
	{
		$rules = [
			'phone'        => 'required|phone|min:10',
			'partyPinCode' => 'required|digits:6'
		];

		$message = [
			'phone.required'        => 'Please enter your mobile number',
			'phone.phone'           => 'Please enter you 10 digit valid mobile number',
			'partyPinCode.required' => 'Please enter the Pin Code of your party location',
			'partyPinCode.digits'   => 'Please enter a valid Pin Code'
		];

		$option = $this->getProductData($mapId, $mapTypeId);
		$is_live = $option->is_live;
		$validator = validator(request()->all(), $rules, $message);
		if ($validator->fails())
		{
			return response()->json(['success' => false, 'error' => $validator->messages()->first()]);
		}
		elseif ($is_live == 0)
		{
			$tplCustomer = config('evibe.product_non_live.customer');

			$data = [
				"customerMobile" => request('phone'),
				"partyDate"      => Carbon::createFromTimestamp(request("partyDate") ? strtotime(str_replace("-", "/", request("partyDate"))) : null)->toDateString(),
				"productType"    => "Decor",
				"pageLink"       => request('pageUrl')
			];

			$replaces = [
				'#field1#' => "Customer",
				'#field2#' => config('evibe.contact.company.phone'),
			];

			$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);

			$smsData = [
				'to'   => $data["customerMobile"],
				'text' => $smsText
			];

			$this->dispatch(new SMSActiveNonLiveToCustomer($smsData));
			$this->dispatch(new MailTeamCustomerEnquireForNonActiveProduct($data));

			return response()->json([
				                        'success' => false,
				                        'error'   => "we are sorry that your package/service in unavailable. Our party planning expert will contact you shortly to help you out",
				                        'is_live' => 0
			                        ]);
		}
		else
		{
			$sourceUrl = url()->current();
			$request_body = file_get_contents('php://input');
			$data = json_encode($request_body);
			$ajaxData = new LogAjax();
			$ajaxData->setUrl($sourceUrl)
			         ->setPayload($data);
			$this->logAjaxData($ajaxData);

			$providerPinCode = request('providerPinCode');
			$partyPinCode = request('partyPinCode');
			$distFree = 0;
			$distMax = 40; // default to avoid division by 0
			$transMax = 0;
			$transMin = 0;
			$baseBookingAmount = 0;

			$dataForPriceCheck = new PriceCheck();
			$dataForPriceCheck->setOptionId($mapId);
			$dataForPriceCheck->setOptionTypeId($mapTypeId);
			$dataForPriceCheck->setProviderPinCode($providerPinCode);
			$dataForPriceCheck->setPartyPinCode($partyPinCode);
			$dataForPriceCheck->setDistFree($distFree);
			$dataForPriceCheck->setDistMax($distMax);
			$dataForPriceCheck->setTransMax($transMax);
			$dataForPriceCheck->setTransMin($transMin);
			$dataForPriceCheck->setBaseBookingAmount($baseBookingAmount);

			$transPriceData = $this->getUpdatedPriceWithTransportationCharges($dataForPriceCheck);

			if (!$transPriceData)
			{
				return response()->json(['success' => false]);
			}

			$transPriceData['success'] = true;

			return response()->json($transPriceData);
		}
	}

	private function getDecorCheckoutValue($ticketId, $id)
	{
		$checkoutValue = CheckoutFieldValue::where([
			                                           'ticket_id'         => $ticketId,
			                                           'checkout_field_id' => $id,
		                                           ])->first();

		return $checkoutValue->value;
	}

	private function makeApiRequestForSendingPaymentNotification($data)
	{
		$url = config('evibe.api.base_url') . config('evibe.api.pay_notification') . 'send/' . $data['ticketId'];

		$res = ['success' => false];

		try
		{
			$client = new Client();
			$res = $client->request('POST', $url, [
				'json' => $data
			]);

			$res = $res->getBody();
			$res = \GuzzleHttp\json_decode($res, true);

		} catch (ClientException $e)
		{
			$apiResponse = $e->getResponse()->getBody(true);
			$apiResponse = \GuzzleHttp\json_decode($apiResponse);
			$res['error'] = $apiResponse->errorMessage;

			Log::error("FAILED TO GET RESPONSE FOR API CALL URL : $url . Response below");
		}

		return $res;
	}

	private function makeApiRequestForSendingPartnerBookingPaymentNotification($data)
	{
		// todo: edit url
		$url = config('evibe.api.base_url') . config('evibe.api.pay_notification') . 'send/' . $data['ticketId'];

		$res = ['success' => false];

		try
		{
			$client = new Client();
			$res = $client->request('POST', $url, [
				'json' => $data
			]);

			$res = $res->getBody();
			$res = \GuzzleHttp\json_decode($res, true);

		} catch (ClientException $e)
		{
			$apiResponse = $e->getResponse()->getBody(true);
			$apiResponse = \GuzzleHttp\json_decode($apiResponse);
			$res['error'] = $apiResponse->errorMessage;

			Log::error("FAILED TO GET RESPONSE FOR API CALL URL : $url . Response below");
		}

		return $res;
	}

	private function captchaCheck()
	{
		$response = request()->input('g-recaptcha-response');
		$remoteIp = $_SERVER['REMOTE_ADDR'];
		$secret = config('evibe.google.re_cap_secret');

		$reCaptcha = new ReCaptcha($secret);
		$resp = $reCaptcha->verify($response, $remoteIp);
		if ($resp->isSuccess())
		{
			return true;
		}

		return false;
	}

	private function reportPaymentActionNotificationError($e, $ticketId, $type)
	{
		$sub = "[URGENT] [ERROR] :: Some error occurred while partner was accepting / rejecting the auto booking";

		$reportData = [
			'title'    => $e->getMessage(),
			'code'     => $e->getCode(),
			'details'  => $e->getTraceAsString(),
			'ticketId' => $ticketId,
			'type'     => $type,
			'sub'      => $sub
		];

		dispatch(new HandleAutoBookingPaymentErrorNotificationJob($reportData));
	}
}