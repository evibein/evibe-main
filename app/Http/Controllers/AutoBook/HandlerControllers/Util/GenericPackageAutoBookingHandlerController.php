<?php

namespace App\Http\Controllers\AutoBook\HandlerControllers\Util;

use App\Evibe\Implementers\HandlesPayments;

class GenericPackageAutoBookingHandlerController extends BasePackageAutoBookingHandlerController implements HandlesPayments
{
	public function initAutoBooking($mapTypeId, $mapId)
	{
		$type = "Package";
		$typeId = config('evibe.ticket.type.generic-package');
		$result = $this->checkProductLive($mapTypeId, $mapId, $typeId, $type);

		return response()->json($result);
	}

	public function showCheckoutPage()
	{
		$data = $this->showCheckoutPageBasePackage();

		if (!is_null($data))
		{
			return view($data['view'], ['data' => $data]);
		}

		return "<h1>Invalid Request</h1>"; // ticketId, token mismatch
	}

	public function processPayment($ticketId)
	{
		$result = $this->processPaymentBasePackage($ticketId);

		return response()->json($result);
	}

	public function onPaymentSuccess($ticketId)
	{
		$data = $this->onPaymentSuccessBasePackage($ticketId);
		if (!is_null($data))
		{
			$data["ticketId"] = $ticketId;

			return view($data['view'], ['data' => $data]);
		}

		return redirect(route("missing"));
	}
}