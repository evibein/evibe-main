<?php

namespace App\Http\Controllers\AutoBook\HandlerControllers\Util;

use App\Jobs\Emails\Payment\Customer\MailAutoBookingAlertToCustomerJob;
use App\Jobs\Emails\Payment\Team\MailAutoBookingAlertToTeamJob;
use App\Jobs\Emails\Payment\Vendor\MailAutoBookingAlertToPartnerJob;
use App\Jobs\Emails\Util\MailTeamCustomerEnquireForNonActiveProduct;
use App\Jobs\HandlePostPaymentProcessJob;
use App\Jobs\SMS\SMSActiveNonLiveToCustomer;
use App\Jobs\SMS\SMSBookingToCustomerJob;
use App\Jobs\SMS\SMSBookingToPartnerJob;
use App\Models\Package\Package;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketBooking;
use App\Models\Ticket\TicketMapping;

use App\Models\Util\SiteErrorLog;
use Carbon\Carbon;
use Evibe\Passers\AutoBookUtil;
use Evibe\Passers\PaySuccessUpdateTicket;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class BasePackageAutoBookingHandlerController extends BaseAutoBookingHandlerController
{
	public function checkProductLive($mapTypeId, $mapId, $typeId, $type)
	{
		$option = Package::find($mapId);
		if (!$option)
		{
			$this->logError("Failed to find option with id $mapId");

			$result = [
				"success" => false,
				"error"   => $type . " not found"
			];
		}
		elseif ($option->is_live == 0)
		{
			$data = [
				"customerMobile" => request('phone'),
				"partyDate"      => request()->input("checkInDate"),
				"productType"    => "Package",
				"pageLink"       => request('pageUrl')
			];
			$tplCustomer = config('evibe.sms_tpl.product_non_live');
			$replaces = [
				'#field1#' => "Customer",
				'#field2#' => config('evibe.contact.company.phone'),
			];

			$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);

			$smsData = [
				'to'   => $data["customerMobile"],
				'text' => $smsText
			];

			$this->dispatch(new SMSActiveNonLiveToCustomer($smsData));
			$this->dispatch(new MailTeamCustomerEnquireForNonActiveProduct($data));
			$result = [
				'success' => false,
				'error'   => "We are sorry that your package/service in unavailable. Our party planning expert will contact you shortly to help you out.",
				'is_live' => 0
			];
		}
		else
		{
			$result = $this->initAutoBookingBasePackage($mapTypeId, $mapId, $typeId);
		}

		return $result;
	}

	public function initAutoBookingBasePackage($mapTypeId, $mapId, $typeId)
	{
		$products = [];
		// here 'mapTypeId' is 'package'
		array_push($products, [
			'productId'     => $mapId,
			'productTypeId' => $typeId,
			'isAddOn'       => false
		]);

		$addOns = request('addOns');
		if (!is_null($addOns) && count($addOns))
		{
			foreach ($addOns as $addOnId => $addOnCount)
			{
				if ($addOnCount > 0)
				{
					array_push($products, [
						'productId'     => $addOnId,
						'productTypeId' => config('evibe.ticket.type.add-on'),
						'isAddOn'       => true,
						'unitCount'     => $addOnCount
					]);
				}
			}
		}
		$res = $this->validateAndInitAutoBooking($products);

		return $res;
	}

	public function initPackageAutoBookingBasePackage($mapTypeId, $mapId, $typeId)
	{
		$res = ['success' => false];

		try
		{
			// validate user data
			$rules = [
				'phone'       => 'required|phone',
				'checkInDate' => 'sometimes|required|date',
				'guestsCount' => 'sometimes|required',
				'partyTime'   => 'sometimes|required'
			];

			$messages = [
				'phone.required'       => 'Please enter your phone number',
				'phone.phone'          => 'Phone number is invalid (ex: 9640204000)',
				'checkInDate.required' => 'Please select your check in date',
				'checkInDate.date'     => 'Please select a valid check in date',
				'guestsCount.required' => 'Guests count is required',
				'partyTime.required'   => 'Party time is required'
			];

			$validator = Validator::make(request()->input(), $rules, $messages);
			$previousUrl = url()->previous();

			if ($validator->fails())
			{
				return $res = [
					'success' => false,
					'error'   => $validator->messages()->first()
				];
			}

			// fall back to generic package if particular package config does not exist
			$typeTicketId = config('evibe.auto-book.' . $typeId) ? $typeId : config('evibe.ticket.type.generic-package');

			// collect user input
			$phone = request()->input('phone');
			$entityData = $this->getEntityDetailsByMapValues($mapId, $mapTypeId);
			$provider = $entityData['provider'];

			// Check whether the provider is planner or venue
			$isVenueBooking = (request("forVenueDetails") == config('evibe.ticket.type.venue')) ? 1 : 0;
			if ($isVenueBooking == 1)
			{
				$zipCode = $provider['zip'];
				$venueAddress = $provider['full_address'] . ", Pin Code: " . $zipCode;
				$areaId = $provider['area_id'];
				$venueLandmark = $provider['landmark'];
			}

			// Check in time is fixed as given by package, now get user selected check in date
			// and form full unix timestamp.
			$guestsCount = request()->input('guestsCount', false);
			$checkInDate = request()->input('checkInDate', false);
			$partyTime = request()->input('partyTime', null);

			$rawCheckInTime = !is_null($partyTime) ? $partyTime : $entityData['check_in'];
			$rawCheckOutTime = $entityData['check_out'];
			$checkInTime = date("H:i", strtotime($rawCheckInTime));
			$checkInDate = date('Y-m-d', strtotime(str_replace('-', '/', $checkInDate)));
			$checkInDateTime = strtotime("$checkInDate $checkInTime");

			$checkOutDateTime = null;
			$nextDayCheckOutDateString = strtotime($checkInDate) + 24 * 60 * 60;
			if ($rawCheckOutTime)
			{
				if (strpos(strtolower($rawCheckOutTime), "next day") != false)
				{
					$rawCheckOut = explode('(', $rawCheckOutTime);
					$checkOutDateTime = $nextDayCheckOutDateString + strtotime($rawCheckOut[0]) - strtotime('today midnight');
				}
				elseif (strtotime($rawCheckInTime) >= strtotime($rawCheckOutTime))
				{
					$checkOutDateTime = $nextDayCheckOutDateString + strtotime($rawCheckOutTime) - strtotime('today midnight');
				}
				else
				{
					$checkOutDateTime = strtotime($checkInDate) + strtotime($rawCheckOutTime) - strtotime('today midnight');
				}
			}

			// deadline calculation
			// if booked before 12 today, put deadline tomorrow 8 AM
			// if booked after 12 today, put deadline tomorrow 4 PM
			// reminder will be sent at 9 AM and 5 PM everyday for un-acted tickets.
			$currentHour = date("H"); // returns in 24 Hrs format
			$deadlineDateAndTime = strtotime("today 4 PM");
			if ($currentHour > 12)
			{
				$deadlineDateAndTime = strtotime("tomorrow 8 AM");
			}

			// map link
			$mapLat = isset($provider['map_lat']) ? $provider['map_lat'] : 0;
			$mapLong = isset($provider['map_long']) ? $provider['map_long'] : 0;
			$mapLink = "https://www.google.co.in/maps/@$mapLat,$mapLong,15z";

			// create ticket with appropriate details
			$ticketInsertData = [
				'event_id'                => isset($entityData['event_id']) ? $entityData['event_id'] : 1, // default 1 if not set
				'phone'                   => $phone,
				'event_date'              => $checkInDateTime,
				'guests_count'            => $guestsCount,
				'is_auto_booked'          => 1,
				'type_ticket_id'          => $typeTicketId,
				'city_id'                 => isset($provider['city_id']) ? $provider['city_id'] : getCityId(),
				'area_id'                 => isset($areaId) ? $areaId : null,
				'zip_code'                => isset($zipCode) ? $zipCode : null,
				'venue_address'           => isset($venueAddress) ? $venueAddress : null,
				'venue_landmark'          => isset($venueLandmark) ? $venueLandmark : null,
				'status_id'               => config('evibe.ticket.status.initiated'),
				'auto_book_deadline_date' => $deadlineDateAndTime,
				'map_link'                => $mapLink,
				'created_at'              => Carbon::now(),
				'updated_at'              => Carbon::now(),
				'enquiry_source_id'       => config('evibe.ticket.enquiry_source.autobook')
			];

			// Create the ticket with phone number and event date or check in date
			$ticket = Ticket::create($ticketInsertData);

			if (!$ticket)
			{
				SiteErrorLog::create([
					                     'url'        => request()->fullUrl(),
					                     'exception'  => strtoupper(config('evibe.auto-book.' . $typeTicketId)) . " AutoBooking: Error while creating ticket",
					                     'code'       => "Error",
					                     'details'    => "Some error occurred in BasePackageAutoBookingHandlerController [Line: " . __LINE__ . "], while creating a ticket",
					                     'created_at' => date('Y-m-d H:i:s'),
					                     'updated_at' => date('Y-m-d H:i:s')
				                     ]);

				return $res;
			}

			$this->updateTicketAction([
				                          'ticket'   => $ticket,
				                          'comments' => "Auto booking ticket created by customer"
			                          ]);

			// save enquiry id
			$ticketId = $ticket->id;
			$enquiryId = config('evibe.ticket.enq.pre.auto_booked') . $ticketId;
			$ticket->enquiry_id = $enquiryId;
			$ticket->save();

			// calculate total booking amount, advance amount based on guests count
			$totalBookingAmount = $entityPrice = $entityData['price'];
			$pricePerExtraGuest = $entityData['price_per_extra_guest'];
			$groupCount = $entityData['group_count'];
			$tokenAdvance = $entityData['token_advance'];

			if ($groupCount < $guestsCount)
			{
				$extra_guest = $guestsCount - $groupCount;
				$totalBookingAmount += ($extra_guest * $pricePerExtraGuest);
			}

			$advanceAmount = $this->getRoundedTokenAmount($totalBookingAmount);
			$advanceSelect = request('advanceSelect', 0);

			if ($advanceSelect && $advanceSelect == 1)
			{
				$advanceAmount = $totalBookingAmount;
			}

			// create ticket mapping
			$mappingInsertData = [
				'ticket_id'   => $ticketId,
				'map_id'      => $mapId,
				'map_type_id' => $mapTypeId,
				'created_at'  => Carbon::now(),
				'updated_at'  => Carbon::now()
			];

			// create the mapping of package to the ticket
			$ticketMapping = TicketMapping::create($mappingInsertData);
			if (!$ticketMapping)
			{
				SiteErrorLog::create([
					                     'url'        => request()->fullUrl(),
					                     'exception'  => strtoupper(config('evibe.auto-book.' . $typeTicketId)) . " AutoBooking: Error while creating ticket mapping",
					                     'code'       => "Error",
					                     'details'    => "Some error occurred in BasePackageAutoBookingHandlerController [Line: " . __LINE__ . "], while creating ticket mapping",
					                     'created_at' => date('Y-m-d H:i:s'),
					                     'updated_at' => date('Y-m-d H:i:s')
				                     ]);

				return $res;
			}
			$ticketMappingId = $ticketMapping->id;

			// create booking info: html full url
			$bookingInfo = "<a href='" . $previousUrl . "?ref=pay-checkout' target='_blank'>" . $entityData['name'] . "</a>";

			// create ticket booking
			$bookingInsertData = [
				'booking_id'             => $enquiryId,
				'booking_info'           => $bookingInfo,
				'party_date_time'        => $checkInDateTime,
				'party_end_time'         => $checkOutDateTime,
				//'check_in'               => $rawCheckInTime,
				//'check_out'              => $rawCheckOutTime,
				'booking_amount'         => $totalBookingAmount,
				'advance_amount'         => $advanceAmount,
				'map_id'                 => $provider['id'],
				'map_type_id'            => $entityData['map_type_id'],
				'ticket_id'              => $ticketId,
				'ticket_mapping_id'      => $ticketMappingId,
				'package_price'          => $entityPrice,
				'group_count'            => $groupCount,
				'price_per_extra_guest'  => $pricePerExtraGuest,
				'is_venue_booking'       => $isVenueBooking, // @todo: should based on type of booking
				'type_ticket_booking_id' => config('evibe.booking_type.venue'), // @todo: should be based on the booking
				'booking_type_details'   => ucwords(config('evibe.auto-book.' . $typeTicketId)),
				'created_at'             => Carbon::now(),
				'updated_at'             => Carbon::now()
			];

			$ticketBooking = TicketBooking::create($bookingInsertData);
			if (!$ticketBooking)
			{
				SiteErrorLog::create([
					                     'url'        => request()->fullUrl(),
					                     'exception'  => strtoupper(config('evibe.auto-book.' . $typeTicketId)) . " AutoBooking: Error while creating ticket booking",
					                     'code'       => "Error",
					                     'details'    => "Some error occurred in BasePackageAutoBookingHandlerController [Line: " . __LINE__ . "], while creating ticket booking",
					                     'created_at' => date('Y-m-d H:i:s'),
					                     'updated_at' => date('Y-m-d H:i:s')
				                     ]);

				return $res;
			}

			// @todo: add checkout fields: guests count, check-in time, checkout time
			$typeName = config('evibe.auto-book.' . $typeTicketId) ? config('evibe.auto-book.' . $typeTicketId) : 'generic-package';
			$checkoutLink = route('auto-book.' . $typeName . '.pay.auto.checkout');
			$checkoutLink .= "?id=" . $ticketId;
			$checkoutLink .= "&token=" . Hash::make($ticketId);
			$checkoutLink .= "&uKm8=" . Hash::make($phone);

			if ($checkoutLink)
			{
				return $res = [
					'success'     => true,
					'redirectUrl' => $checkoutLink
				];
			}
		} catch (\Exception $e)
		{
			SiteErrorLog::create([
				                     'url'        => request()->fullUrl(),
				                     'exception'  => "Package AutoBooking: Some error occurred while processing Package AB init",
				                     'code'       => $e->getCode(),
				                     'details'    => $e->getMessage(),
				                     'created_at' => date('Y-m-d H:i:s'),
				                     'updated_at' => date('Y-m-d H:i:s')
			                     ]);

			if (isset($ticket))
			{
				$ticket->forceDelete();
			}
		}

		return $res;
	}

	public function showCheckoutPageBasePackage()
	{
		$ticketId = request()->input('id');
		$token = request()->input('token');
		$hashedStr = request()->input('uKm8');

		$dataForValidation = new AutoBookUtil();
		$dataForValidation->setTicketId($ticketId);
		$dataForValidation->setToken($token);
		$dataForValidation->setHashedStr($hashedStr);
		$ticket = $this->validateTicket($dataForValidation);

		if (!is_null($ticket))
		{
			$ticketBooking = $ticket->bookings;
			$booking = $ticketBooking->first();

			$data = $this->getTicketData($ticket);
			$data['additional']['extraGuests'] = 0;
			$totalBookingAmount = $booking->booking_amount;
			$totalAdvanceToPay = ($booking->is_advance_paid == 1) ? 0 : $booking->advance_amount;

			// get mapId and mapTypeId from mapping
			$mapTypeId = $booking->mapping->map_type_id;
			$mapId = $booking->mapping->map_id;

			$mappedValues = $this->fillMappingValues([
				                                         'isCollection' => false,
				                                         'mapId'        => $mapId,
				                                         'mapTypeId'    => $mapTypeId
			                                         ]);

			$data['booking'] = $this->getTicketBookingData($booking, $mappedValues);
			$data['couponToken'] = $this->getAccessTokenForCoupon(intval($ticketId), $ticket->created_at);

			$provider = $data['booking']['provider']['self'];

			if ($data['additional']['guestsCount'] > $provider->group_count)
			{
				$data['additional']['extraGuests'] = $data['additional']['guestsCount'] - $booking->group_count;
			}

			$data['booking']['totalBookingAmount'] = $totalBookingAmount;
			$data['booking']['totalAdvanceToPay'] = $totalAdvanceToPay;
			$data['isVenueBooking'] = $booking->is_venue_booking;

			$typeTicketName = config('evibe.auto-book.' . $data['additional']['typeTicketId']) ? config('evibe.auto-book.' . $data['additional']['typeTicketId']) : 'generic-package';
			$data['view'] = 'pay/checkout/auto/' . $typeTicketName;

			return $data;
		}
		else
		{
			SiteErrorLog::create([
				                     'url'        => request()->fullUrl(),
				                     'exception'  => "Unauthorized Access",
				                     'code'       => 403,
				                     'details'    => "Some error occurred in BasePackageAutoBookingHandlerController [Line: " . __LINE__ . "]. Token mismatch [Ticket id: $ticketId, token: $token, hashedStr: $hashedStr]",
				                     'created_at' => date('Y-m-d H:i:s'),
				                     'updated_at' => date('Y-m-d H:i:s')
			                     ]);

			return null;
		}
	}

	public function processPaymentBasePackage($ticketId)
	{
		$response = [
			'success'    => false,
			'error'      => 'Failed to send booked notifications',
			'redirectTo' => route('pay.common.fail')
		];

		if ($this->sendBookedNotificationsBasePackage($ticketId))
		{
			$token = request()->input('token');

			// to generate redirect route for a particular type ticket
			$ticket = Ticket::find($ticketId);
			$typeTicketId = $ticket->type_ticket_id;
			$typeTicketName = config('evibe.auto-book.' . $typeTicketId) ? config('evibe.auto-book.' . $typeTicketId) : 'generic-package';

			$response['success'] = true;
			$response['redirectTo'] = route('auto-book.' . $typeTicketName . '.pay.auto.success', $ticketId) . "?token=" . $token;
			unset($response['error']);

			// handle post payment
			$this->dispatch(new HandlePostPaymentProcessJob($ticketId));
		}
		else
		{
			SiteErrorLog::create([
				                     'url'        => request()->fullUrl(),
				                     'exception'  => "Package AutoBooking: Failed to sendBookedNotifications for ticket [Id: $ticketId]",
				                     'code'       => "Error",
				                     'details'    => "Some error occurred in BasePackageAutoBookingHandlerController [Line: " . __LINE__ . "]. Failed to send booked notifications",
				                     'created_at' => date('Y-m-d H:i:s'),
				                     'updated_at' => date('Y-m-d H:i:s')
			                     ]);
		}

		return $response;
	}

	public function onPaymentSuccessBasePackage($ticketId)
	{
		$token = request()->input('token');
		$ticket = Ticket::with('bookings.mapping', 'bookings.provider')->find($ticketId);
		$ticketBookings = $ticket->bookings;

		// We are not fetching cancelled booking data
		if (!isset($ticketBookings[0]))
		{
			return null;
		}
		$booking = $ticketBookings[0];

		if (Hash::check($ticket->phone, $token))
		{
			$partyEndTime = $booking->party_end_time;
			$partyEndTimeString = null;
			if ($partyEndTime)
			{
				$partyEndTimeString = date("g:i A", $partyEndTime);
				if (strtotime(date('d M Y', $booking->party_date_time)) < strtotime(date('d M Y', $booking->party_end_time)))
				{
					$partyEndTimeString .= " (Next Day)";
				}
			}
			$data = [
				"advanceAmount" => $booking->advance_amount,
				"bookingInfo"   => $booking->booking_info,
				"bookingId"     => $booking->booking_id,
				"checkInDate"   => date("d M Y", $booking->party_date_time),
				"checkInTime"   => date("g:i A", $booking->party_date_time),
				"checkOutTime"  => $partyEndTimeString,
				"email"         => $ticket->email,
				'ticketId'      => $ticketId,
				"view"          => 'pay/success/auto'
			];

			return $data;
		}
		else
		{
			SiteErrorLog::create([
				                     'url'        => request()->fullUrl(),
				                     'exception'  => "Package AutoBooking: Tried to hack",
				                     'code'       => "Error",
				                     'details'    => "Some error occurred in BasePackageAutoBookingHandlerController [Line: " . __LINE__ . "]. Tried to change the ticket id with invalid token :: $ticketId",
				                     'created_at' => date('Y-m-d H:i:s'),
				                     'updated_at' => date('Y-m-d H:i:s')
			                     ]);

			return null;
		}
	}

	private function sendBookedNotificationsBasePackage($ticketId, $isFromDash = false, $handlerId = null)
	{
		$dataForNotification = new PaySuccessUpdateTicket();
		$dataForNotification->setTicketNotificationId($ticketId);
		$dataForNotification->setIsFromDash($isFromDash);
		$dataForNotification->setHandlerId($handlerId);

		$data = $this->getNotificationData($dataForNotification);
		if (!is_null($data))
		{
			// vendor ask page URL
			$token = Hash::make($data['customer']['phone']);
			$confirmationLinkType = config('evibe.auto-book.' . $data['additional']['typeTicketId']) ? config('evibe.auto-book.' . $data['additional']['typeTicketId']) : 'generic-package';
			$confirmationLink = route('auto-book.' . $confirmationLinkType . '.actions.vendor.ask', $data['ticketId']) . "?token=" . $token;
			$data['vendorAskUrl'] = $this->getShortenUrl($confirmationLink);

			// send notification to customer, vendor, team
			$this->initBookedNotificationToCustomerBasePackage($data);
			$this->initBookedNotificationToPartnerBasePackage($data);
			$this->dispatch(new MailAutoBookingAlertToTeamJob($data));

			// send app notification on Auto Booking
			$this->createAutoBookingNotificationToApp($data['firstBooking']);

			// update timestamp & save action
			$forUpdateData = new PaySuccessUpdateTicket();
			$forUpdateData->setHandlerId($handlerId);
			$forUpdateData->setIsFromDash($isFromDash);
			$forUpdateData->setStatusId(config('evibe.ticket.status.auto_paid'));
			$forUpdateData->setSuccessEmailType(config('evibe.ticket.success_type.booked'));

			return $this->updateTicketPostSendReceipts($data['ticket'], $forUpdateData);
		}

		return false;
	}

	private function initBookedNotificationToCustomerBasePackage($data)
	{
		$tplCustomer = config('evibe.sms_tpl.auto_book.pay_success.customer');
		$token = Hash::make($data["ticketId"] . "EVBTMO");

		$replaces = [
			'#field1#' => $data['customer']['name'],
			'#field2#' => $this->formatPrice($data['booking']['advanceAmount']),
			'#field3#' => $data['ticket']['enquiry_id'],
			'#field4#' => "4",
			'#field5#' => $this->getShortenUrl(route("track.orders") . "?ref=SMS&id=" . $data["ticketId"] . "&token=" . $token)
		];
		$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);
		$smsData = [
			'to'   => $data['customer']['phone'],
			'text' => $smsText
		];

		// Send email and sms to customer
		$this->dispatch(new MailAutoBookingAlertToCustomerJob($data));
		$this->dispatch(new SMSBookingToCustomerJob($smsData));
	}

	private function initBookedNotificationToPartnerBasePackage($data)
	{
		$tplVendor = config('evibe.sms_tpl.auto_book.pay_success.vendor');
		$replaces = [
			'#field1#' => 'Partner',
			'#field2#' => $this->truncateText($data['booking']['venueName'], 15),
			'#field3#' => $data['additional']['guestsCount'],
			'#field4#' => $data['booking']['partyDate'],
			'#field5#' => $data['vendorAskUrl']
		];

		$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplVendor);
		$smsData = [
			'to'   => $data['booking']['provider']['phone'],
			'text' => $smsText
		];

		// send email and sms to partner
		$this->dispatch(new MailAutoBookingAlertToPartnerJob($data));
		$this->dispatch(new SMSBookingToPartnerJob($smsData));
	}
}