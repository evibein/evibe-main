<?php

namespace App\Http\Controllers\AutoBook\HandlerControllers;

use App\Evibe\Implementers\HandlesPayments;
use App\Http\Controllers\AutoBook\HandlerControllers\Util\BaseAutoBookingHandlerController;
use App\Jobs\Emails\Payment\Customer\MailAutoBookingVenueDealsAlertToCustomerJob;
use App\Jobs\Emails\Payment\Team\MailAutoBookingVenueDealsAlertToTeamJob;
use App\Jobs\Emails\Payment\Vendor\MailAutoBookingVenueDealsAlertToPartnerJob;
use App\Jobs\Emails\Util\MailTeamCustomerEnquireForNonActiveProduct;
use App\Jobs\HandlePostPaymentProcessJob;
use App\Jobs\SMS\SMSActiveNonLiveToCustomer;
use App\Jobs\SMS\SMSVenueDealsAutoBookingAlertToCustomerJob;
use App\Jobs\SMS\SMSVenueDealsAutoBookingAlertToPartnerJob;
use App\Models\Package\Package;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketBooking;
use App\Models\Ticket\TicketMapping;

use App\Models\Util\SiteErrorLog;
use Carbon\Carbon;
use Evibe\Passers\AutoBookUtil;
use Evibe\Passers\PaySuccessUpdateTicket;
use Illuminate\Support\Facades\Hash;

class VenueDealsAutoBookingHandlerController extends BaseAutoBookingHandlerController implements HandlesPayments
{
	public function initAutoBooking($mapTypeId, $mapId, $occasionId, $typeId)
	{
		$res = ['success' => false];

		try
		{
			// validate user data
			$rules = [
				'phone'       => 'required|phone',
				'partyDate'   => 'sometimes|required|date|after:today',
				'guestsCount' => 'sometimes|required',
				'partySlot'   => 'sometimes|required|integer|min:1'
			];

			$messages = [
				'phone.required'       => 'Please enter your phone number',
				'phone.phone'          => 'Phone number is invalid (ex: 9640204000)',
				'partyDate.required'   => 'Please select your party date',
				'partyDate.date'       => 'Please select a valid party date',
				'guestsCount.required' => 'Guests count is required',
				'partySlot.required'   => 'Party slot is required',
				'partySlot.integer'    => 'Party slot is invalid',
				'partySlot.min'        => 'Party slot is invalid',
				'partyVdDate.after'    => 'Party date must be after today'
			];

			$validator = validator(request()->input(), $rules, $messages);
			$previousUrl = url()->previous();

			if ($validator->fails())
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => $validator->messages()->first()
				                        ]);
			}
			$option = Package::find($mapId);
			if (!$option)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => "Could not find Venue Deal"
				                        ]);
			}

			// collect user input
			$phone = request()->input('phone');
			$entityData = $this->getEntityDetailsByMapValues($mapId, $mapTypeId);
			$provider = $entityData['provider'];
			$venueAddress = $provider['full_address'] . ", Pin Code: " . $provider['zip'];

			// Check in time is fixed as given by package, now get user selected check in date
			// and form full unix timestamp.
			$guestsCount = request()->input('guestsCount', false);
			$checkInDate = request()->input('partyDate', false);
			$ticketTypeId = $typeId;
			$partySlotId = request()->input('partySlot');
			$eventId = $occasionId;
			$foodType = request()->input('foodType');

			$isLive = $option->is_live;
			if ($isLive == 0)
			{
				$data = [
					"customerMobile" => $phone,
					"partyDate"      => $checkInDate,
					"productType"    => "Venue Deal",
					"pageLink"       => request('pageUrl')
				];
				$tplCustomer = config('evibe.sms_tpl.product_non_live');
				$replaces = [
					'#field1#' => "Customer",
					'#field2#' => config('evibe.contact.company.phone'),
				];

				$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);

				$smsData = [
					'to'   => $data["customerMobile"],
					'text' => $smsText
				];

				$this->dispatch(new SMSActiveNonLiveToCustomer($smsData));
				$this->dispatch(new MailTeamCustomerEnquireForNonActiveProduct($data));

				return response()->json([
					                        'success' => false,
					                        'error'   => "we are sorry that your package/service in unavailable. Our party planning expert will contact you shortly to help you out.",
					                        'is_live' => 0
				                        ]);
			}

			$partySlot = config("evibe.party-slot.$partySlotId") ? config("evibe.party-slot.$partySlotId") : config("evibe.party-slot.1");
			$slotStartString = explode(' - ', $partySlot)[0];
			$slotEndString = explode(' - ', $partySlot)[1];
			$slotStartTime = strtotime($checkInDate) + strtotime($slotStartString) - strtotime('today midnight');
			$slotEndTime = strtotime($checkInDate) + strtotime($slotEndString) - strtotime('today midnight');

			// deadline calculation
			// if booked before 12 today, put deadline tomorrow 8 AM
			// if booked after 12 today, put deadline tomorrow 4 PM
			// reminder will be sent at 9 AM and 5 PM everyday for un-acted tickets.
			$currentHour = date("H"); // returns in 24 Hrs format
			$deadlineDateAndTime = strtotime("today 4 PM");
			if ($currentHour > 12)
			{
				$deadlineDateAndTime = strtotime("tomorrow 8 AM");
			}

			// create ticket with appropriate details
			$ticketInsertData = [
				'phone'                   => $phone,
				'event_date'              => $slotStartTime,
				'guests_count'            => $guestsCount,
				'type_ticket_id'          => $ticketTypeId,
				'type_slot_id'            => $partySlotId,
				'is_auto_booked'          => 1,
				'city_id'                 => $provider['city_id'],
				'area_id'                 => $provider['area_id'],
				'zip_code'                => $provider['zip'],
				'venue_address'           => $venueAddress,
				'venue_landmark'          => $provider['landmark'],
				'status_id'               => config('evibe.ticket.status.initiated'),
				'auto_book_deadline_date' => $deadlineDateAndTime,
				'event_id'                => $eventId,
				'food_type'               => $foodType,
				'created_at'              => Carbon::now(),
				'updated_at'              => Carbon::now(),
				'enquiry_source_id'       => config('evibe.ticket.enquiry_source.autobook')
			];

			// Create the ticket with phone number and event date or check in date
			$ticket = Ticket::create($ticketInsertData);

			if (!$ticket)
			{
				SiteErrorLog::create([
					                     'url'        => request()->fullUrl(),
					                     'exception'  => "Venue-Deals AutoBooking: Error while creating ticket",
					                     'code'       => "Error",
					                     'details'    => "Some error occurred in VenueDealsAutoBookingHandlerController [Line: " . __LINE__ . "], while creating a ticket",
					                     'created_at' => date('Y-m-d H:i:s'),
					                     'updated_at' => date('Y-m-d H:i:s')
				                     ]);

				return response()->json(['success' => false]);
			}

			$this->updateTicketAction([
				                          'ticket'   => $ticket,
				                          'comments' => "Venue deals auto booking ticket created by customer"
			                          ]);

			// save enquiry id
			$ticketId = $ticket->id;
			$enquiryId = config('evibe.ticket.enq.pre.auto_booked') . $ticketId;
			$ticket->enquiry_id = $enquiryId;
			$ticket->save();

			$tokenAmount = $entityData['token_advance'];
			$hallTypeId = isset($entityData['hall_type_id']) ? $entityData['hall_type_id'] : null;
			if ($tokenAmount == 0 && $hallTypeId)
			{
				if ($hallTypeId == config('evibe.type-venue-hall.banquet-hall') && $guestsCount)
				{
					$tokenAmount = (int)($entityData['price'] * $guestsCount * config('evibe.ticket.advance.percentage') * 0.01);
				}
				else
				{
					$tokenAmount = (int)($entityData['price'] * config('evibe.ticket.advance.percentage') * 0.01);
				}
			}

			// create ticket mapping
			$mappingInsertData = [
				'ticket_id'   => $ticketId,
				'map_id'      => $mapId,
				'map_type_id' => $mapTypeId,
				'created_at'  => Carbon::now(),
				'updated_at'  => Carbon::now()
			];

			// create the mapping of package to the ticket
			$ticketMapping = TicketMapping::create($mappingInsertData);
			if (!$ticketMapping)
			{
				SiteErrorLog::create([
					                     'url'        => request()->fullUrl(),
					                     'exception'  => "Venue-Deals AutoBooking: Error while creating ticket mapping",
					                     'code'       => "Error",
					                     'details'    => "Some error occurred in VenueDealsAutoBookingHandlerController [Line: " . __LINE__ . "], while creating a ticket mapping",
					                     'created_at' => date('Y-m-d H:i:s'),
					                     'updated_at' => date('Y-m-d H:i:s')
				                     ]);

				return response()->json(['success' => false]);
			}
			$ticketMappingId = $ticketMapping->id;

			// create booking info: html full url
			$bookingInfo = "<a href='" . $previousUrl . "?ref=pay-checkout' target='_blank'>" . $entityData['name'] . "</a>";

			// create ticket booking
			$bookingInsertData = [
				'booking_id'             => $enquiryId,
				'booking_info'           => $bookingInfo,
				'party_date_time'        => $slotStartTime,
				//'party_end_time'         => $slotEndTime, // @see: not being displayed to partner and customer, but might confuse CRM team
				'token_amount'           => $tokenAmount,
				'booking_amount'         => 0,
				'advance_amount'         => 0,
				'group_count'            => request()->input('guestsCount'),
				'map_id'                 => $provider['id'],
				'map_type_id'            => $entityData['map_type_id'],
				'ticket_id'              => $ticketId,
				'ticket_mapping_id'      => $ticketMappingId,
				'is_venue_booking'       => 1,
				'type_ticket_booking_id' => config('evibe.booking_type.venue'),
				'booking_type_details'   => "Venue Deal",
				'created_at'             => Carbon::now(),
				'updated_at'             => Carbon::now()
			];

			$ticketBooking = TicketBooking::create($bookingInsertData);

			// @todo: add checkout fields: party slot, guests count, food type
			if (!$ticketBooking)
			{
				SiteErrorLog::create([
					                     'url'        => request()->fullUrl(),
					                     'exception'  => "Venue-Deals AutoBooking: Error while creating ticket booking",
					                     'code'       => "Error",
					                     'details'    => "Some error occurred in VenueDealsAutoBookingHandlerController [Line: " . __LINE__ . "]. Unable to create ticket booking [Ticket id: $ticketId]",
					                     'created_at' => date('Y-m-d H:i:s'),
					                     'updated_at' => date('Y-m-d H:i:s')
				                     ]);

				return response()->json(['success' => false]);
			}

			$checkoutLink = route('auto-book.venue-deals.pay.auto.checkout');
			$checkoutLink .= "?id=" . $ticketId;
			$checkoutLink .= "&token=" . Hash::make($ticketId);
			$checkoutLink .= "&uKm8=" . Hash::make($phone);

			if ($checkoutLink)
			{
				return response()->json(['success' => true, 'redirectUrl' => $checkoutLink]);
			}
		} catch (\Exception $e)
		{
			SiteErrorLog::create([
				                     'url'        => request()->fullUrl(),
				                     'exception'  => "Venue-Deals AutoBooking: Some error occurred while processing Decor AB init",
				                     'code'       => $e->getCode(),
				                     'details'    => $e->getMessage(),
				                     'created_at' => date('Y-m-d H:i:s'),
				                     'updated_at' => date('Y-m-d H:i:s')
			                     ]);

			if (isset($ticket))
			{
				$ticket->forceDelete();
			}
		}

		return response()->json($res);
	}

	public function showCheckoutPage()
	{
		$ticketId = request()->input('id');
		$token = request()->input('token');
		$hashedStr = request()->input('uKm8');

		$dataForValidation = new AutoBookUtil();
		$dataForValidation->setTicketId($ticketId);
		$dataForValidation->setToken($token);
		$dataForValidation->setHashedStr($hashedStr);

		$ticket = $this->validateTicket($dataForValidation);
		if (!is_null($ticket))
		{
			$ticketBooking = $ticket->bookings;
			$booking = $ticketBooking->first();

			$data = $this->getTicketData($ticket);
			$data['additional']['extraGuests'] = 0;
			$totalBookingAmount = $booking->booking_amount;
			$totalAdvanceToPay = ($booking->is_advance_paid == 1) ? 0 : $booking->token_amount;

			// get mapId and mapTypeId from mapping
			$mapTypeId = $booking->mapping->map_type_id;
			$mapId = $booking->mapping->map_id;

			$mappedValues = $this->fillMappingValues([
				                                         'isCollection' => false,
				                                         'mapId'        => $mapId,
				                                         'mapTypeId'    => $mapTypeId
			                                         ]);

			$data['booking'] = $this->getTicketBookingData($booking, $mappedValues);
			$data['couponToken'] = $this->getAccessTokenForCoupon(intval($ticketId), $ticket->created_at);

			$data['venueDeal'] = $mappedValues;

			// custom fields
			if ($mapTypeId == config('evibe.ticket.type.package'))
			{
				$customCategories = config('evibe.pkg_fields')['cat'];
				foreach ($customCategories as $key => $customCategory)
				{
					$fieldValues = $this->getCustomFieldValuesByCat($customCategory['id'], $ticket->event_id, $mapId);
					$data['customFields'][$key] = $fieldValues;

					// collect `individual` fields as direct array values
					if ($customCategory['id'] == config('evibe.pkg_fields.cat.individual.id'))
					{
						foreach ($fieldValues as $fieldValue)
						{
							$customKey = $this->getDefaultCustomKeyByCurrentKey($fieldValue['key']);
							$data[$customKey] = $fieldValue['value'];
						}
					}
				}
			}

			$data['booking']['totalBookingAmount'] = $totalBookingAmount;
			$data['booking']['totalAdvanceToPay'] = $totalAdvanceToPay;
			$data['isVenueDeals'] = true;

			return view('pay/checkout/auto/venue-deals', ['data' => $data]);
		}
		else
		{
			SiteErrorLog::create([
				                     'url'        => request()->fullUrl(),
				                     'exception'  => "Unauthorized Access",
				                     'code'       => 403,
				                     'details'    => "Some error occurred in VenueDealsAutoBookingHandlerController [Line: " . __LINE__ . "]. Token mismatch [Ticket id: $ticketId, token: $token, hashedStr: $hashedStr]",
				                     'created_at' => date('Y-m-d H:i:s'),
				                     'updated_at' => date('Y-m-d H:i:s')
			                     ]);

			return "<h1>Invalid Request</h1>"; // ticketId, token mismatch
		}
	}

	public function processPayment($ticketId)
	{
		$response = [
			'success'    => false,
			'error'      => 'Failed to send booked notifications',
			'redirectTo' => route('pay.common.fail')
		];

		if ($this->sendBookedNotifications($ticketId))
		{
			$token = request()->input('token');
			$response['success'] = true;
			$response['redirectTo'] = route('auto-book.venue-deals.pay.auto.success', $ticketId) . "?token=" . $token;
			unset($response['error']);

			// handle post payment
			$this->dispatch(new HandlePostPaymentProcessJob($ticketId));
		}

		return response()->json($response);
	}

	public function onPaymentSuccess($ticketId)
	{
		$token = request()->input('token');
		$ticket = Ticket::with('bookings.mapping', 'bookings.provider')->findOrFail($ticketId);
		$ticketBookings = $ticket->bookings;
		$booking = $ticketBookings[0];

		if (Hash::check($ticket->phone, $token))
		{
			$data = [
				"tokenAmount"   => $booking->token_amount,
				"venueName"     => $booking->provider->name,
				"venueLocation" => $booking->provider->area->name,
				"bookingInfo"   => $booking->booking_info,
				"bookingId"     => $booking->booking_id,
				"checkInDate"   => date("d M Y h:i A", $booking->party_date_time),
				"email"         => $ticket->email,
				"eventId"       => $ticket->event_id,
				"ticketId"      => $ticketId
			];

			return view('pay/success/auto_venue_deals', ["data" => $data]);
		}
		else
		{
			SiteErrorLog::create([
				                     'url'        => request()->fullUrl(),
				                     'exception'  => "Venue-Deals AutoBooking: Tried to hack",
				                     'code'       => "Error",
				                     'details'    => "Some error occurred in VenueDealsAutoBookingHandlerController [Line: " . __LINE__ . "]. Tried to change the ticket id with invalid token :: $ticketId",
				                     'created_at' => date('Y-m-d H:i:s'),
				                     'updated_at' => date('Y-m-d H:i:s')
			                     ]);

			return "<h1>Invalid Request</h1>";
		}
	}

	private function sendBookedNotifications($ticketId, $isFromDash = false, $handlerId = null)
	{
		$dataForNotification = new PaySuccessUpdateTicket();
		$dataForNotification->setTicketNotificationId($ticketId);
		$dataForNotification->setIsFromDash($isFromDash);
		$dataForNotification->setHandlerId($handlerId);

		$data = $this->getNotificationData($dataForNotification);
		if (!is_null($data))
		{
			//getting the individual fields value
			$customCategories = config('evibe.pkg_fields')['cat'];
			foreach ($customCategories as $key => $customCategory)
			{
				$fieldValues = $this->getCustomFieldValuesByCat($customCategory['id'], $data['ticket']->event_id, $data['firstBooking']->mapping->map_id);
				$data['customFields'][$key] = $fieldValues;

				// collect `individual` fields as direct array values
				if ($customCategory['id'] == config('evibe.pkg_fields.cat.individual.id'))
				{
					foreach ($fieldValues as $fieldValue)
					{
						$customKey = $this->getDefaultCustomKeyByCurrentKey($fieldValue['key']);
						$data[$customKey] = $fieldValue['value'];
					}
				}
			}

			// vendor ask page URL
			$token = Hash::make($data['customer']['phone']);
			$confirmationLink = route('auto-book.venue-deals.actions.vendor.ask', $data['ticketId']) . "?token=" . $token;
			$data['vendorAskUrl'] = $this->getShortenUrl($confirmationLink);

			// send email to team
			$this->dispatch(new MailAutoBookingVenueDealsAlertToTeamJob($data));

			// send the Auto Booking Notification  to customer & vendor
			$this->initBookedNotificationToCustomer($data);
			$this->initBookedNotificationToPartner($data);

			$this->createAutoBookingNotificationToApp($data['firstBooking']);
			// update timestamp & save action
			$forUpdateData = new PaySuccessUpdateTicket();
			$forUpdateData->setHandlerId($handlerId);
			$forUpdateData->setIsFromDash($isFromDash);
			$forUpdateData->setStatusId(config('evibe.ticket.status.auto_paid'));
			$forUpdateData->setSuccessEmailType(config('evibe.ticket.success_type.booked'));

			return $this->updateTicketPostSendReceipts($data['ticket'], $forUpdateData);
		}

		return false;
	}

	private function initBookedNotificationToCustomer($data)
	{
		// send SMS to customer
		$tplCustomer = config('evibe.sms_tpl.auto_book.venue_deals.pay_success.customer');
		$token = Hash::make($data["ticketId"] . "EVBTMO");

		$replaces = [
			'#field1#' => $data['customer']['name'],
			'#field2#' => $this->formatPrice($data['booking']['advanceAmount']),
			'#field3#' => $data['ticket']['enquiry_id'],
			'#field4#' => "4",
			'#field5#' => $this->getShortenUrl(route("track.orders") . "?ref=SMS&id=" . $data["ticketId"] . "&token=" . $token)
		];
		$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);

		$smsData = [
			'to'   => $data['customer']['phone'],
			'text' => $smsText
		];

		$this->dispatch(new MailAutoBookingVenueDealsAlertToCustomerJob($data));
		$this->dispatch(new SMSVenueDealsAutoBookingAlertToCustomerJob($smsData));
	}

	private function initBookedNotificationToPartner($data)
	{
		// send SMS to partner
		$tplVendor = config('evibe.sms_tpl.auto_book.venue_deals.pay_success.vendor');
		$replaces = [
			'#field1#' => 'Manager',
			'#field2#' => $this->truncateText($data['booking']['venueName'], 15),
			'#field3#' => $data['additional']['guestsCount'],
			'#field4#' => date("d-M-Y", strtotime($data['booking']['partyDate'])),
			'#field5#' => $data['additional']['slot']->name,
			'#field6#' => $data['vendorAskUrl']
		];

		$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplVendor);

		$smsData = [
			'to'   => $data['booking']['provider']['phone'],
			'text' => $smsText
		];

		$this->dispatch(new MailAutoBookingVenueDealsAlertToPartnerJob($data));
		$this->dispatch(new SMSVenueDealsAutoBookingAlertToPartnerJob($smsData));
	}
}