<?php

namespace App\Http\Controllers\AutoBook\HandlerControllers;

use App\Evibe\Implementers\HandlesPayments;
use App\Http\Controllers\AutoBook\HandlerControllers\Util\BaseAutoBookingHandlerController;

use App\Jobs\Emails\Errors\MailTransportChargesErrorToTeamJob;
use App\Jobs\Emails\Payment\Vendor\MailAutoBookingDecorAlertToPartnerJob;
use App\Jobs\Emails\Payment\Customer\MailAutoBookingDecorAlertToCustomerJob;
use App\Jobs\Emails\Payment\Team\MailAutoBookingDecorAlertToTeamJob;
use App\Jobs\Emails\ReportIssue\MissingCityAlertToTeam;
use App\Jobs\HandlePostPaymentProcessJob;
use App\Jobs\SMS\SMSDecorAutoBookingAlertToCustomer;
use App\Jobs\SMS\SMSDecorAutoBookingAlertToPartner;

use App\Models\Decor\Decor;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketBooking;
use App\Models\Ticket\TicketMapping;
use App\Models\Types\TypeTicket;
use App\Models\Util\City;
use App\Models\Util\SiteErrorLog;
use App\Models\Vendor\Vendor;

use Evibe\Passers\AutoBookUtil;
use Evibe\Passers\PaySuccessUpdateTicket;
use Illuminate\Support\Facades\Hash;

class DecorAutoBookingHandlerController extends BaseAutoBookingHandlerController implements HandlesPayments
{
	public function initAutoBooking($mapId, $occasionId)
	{
		$products = [];
		array_push($products, [
			'productId'     => $mapId,
			'productTypeId' => config('evibe.ticket.type.decor'),
			'isAddOn'       => false
		]);

		$addOns = request('addOns');
		if (count($addOns))
		{
			foreach ($addOns as $addOnId => $addOnCount)
			{
				if ($addOnCount > 0)
				{
					array_push($products, [
						'productId'     => $addOnId,
						'productTypeId' => config('evibe.ticket.type.add-on'),
						'isAddOn'       => true,
						'unitCount'     => $addOnCount
					]);
				}
			}
		}
		$res = $this->validateAndInitAutoBooking($products, $occasionId);

		return response()->json($res);
	}

	public function initDecorAutoBooking($mapId, $occasionId)
	{
		$res = ['success' => false];

		try
		{
			// validate user data
			$rules = [
				'phone'        => 'required|digits:10',
				'partyPinCode' => 'required|digits:6',
				'partyDate'    => 'required|date|after:today'
			];

			$messages = [
				'phone.required'        => 'Please enter your mobile number',
				'phone.digits'          => 'Phone number is invalid (ex: 9640204000)',
				'partyPinCode.required' => 'Please enter the pincode of your party venue',
				'partyPinCode.digits'   => 'Please enter a valid pincode'
			];

			$validator = validator(request()->input(), $rules, $messages);

			if ($validator->fails())
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => $validator->messages()->first()
				                        ]);
			}

			$decor = Decor::with("tags")->find($mapId);
			if (!$decor)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => "could not find decor"
				                        ]);
			}

			$ticketTypeId = config('evibe.ticket.type.decor');
			$mapTypeId = config('evibe.ticket.type.planner');

			// collect user input
			$phone = request('phone');
			$partyPinCode = request('partyPinCode');
			$transportCharges = request('transportCharges');
			$totalBookAmount = request('totalBookAmount');
			$tokenAdvance = request('tokenAdvance');
			$partyDate = request('partyDate') ? strtotime(str_replace("-", "/", request("partyDate"))) : null;

			// deadline calculation
			// if booked before 12 today, put deadline tomorrow 8 AM
			// if booked after 12 today, put deadline tomorrow 4 PM
			// reminder will be sent at 9 AM and 5 PM everyday for un-acted tickets.
			$currentHour = date("H"); // returns in 24 Hrs format
			$deadlineDateAndTime = strtotime("today 4 PM");
			if ($currentHour > 12)
			{
				$deadlineDateAndTime = strtotime("tomorrow 8 AM");
			}

			// verify provider and fallback for city_id
			$provider = Vendor::find($decor->provider_id);
			if (!$provider)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => "could not find decor partner"
				                        ]);
			}

			$cityId = 1; // default: Bangalore
			if ($provider->city_id)
			{
				$cityId = $provider->city_id;
			}
			else
			{
				$cityId = (getCityId() > 0) ? getCityId() : $cityId;
				$optionData = [
					'name' => $decor->name,
					'code' => $decor->code,
					'type' => config('evibe.type-product-title.' . ($ticketTypeId))
				];

				$this->dispatch(new MissingCityAlertToTeam($optionData));
			}

			$city = City::find($cityId);
			checkAndSetCitySessions($city);

			// create ticket with appropriate details
			$ticketInsertData = [
				'phone'                   => $phone,
				'type_ticket_id'          => $ticketTypeId,
				'is_auto_booked'          => 1,
				'status_id'               => config('evibe.ticket.status.initiated'),
				'city_id'                 => $cityId,
				'event_date'              => $partyDate,
				'auto_book_deadline_date' => $deadlineDateAndTime,
				'event_id'                => $occasionId,
				'zip_code'                => $partyPinCode,
				'created_at'              => date('Y-m-d H:i:s'),
				'updated_at'              => date('Y-m-d H:i:s'),
				'enquiry_source_id'       => config('evibe.ticket.enquiry_source.autobook')
			];

			// Create the ticket with phone number and event date or check in date
			$ticket = Ticket::create($ticketInsertData);

			if (!$ticket)
			{
				SiteErrorLog::create([
					                     'url'        => request()->fullUrl(),
					                     'exception'  => "Decor AutoBooking: Error while creating ticket",
					                     'code'       => "Error",
					                     'details'    => "Some error occurred in DecorAutoBookingHandlerController [Line: " . __LINE__ . "], while creating a ticket",
					                     'created_at' => date('Y-m-d H:i:s'),
					                     'updated_at' => date('Y-m-d H:i:s')
				                     ]);

				return response()->json($res);
			}

			$this->updateTicketAction([
				                          'ticket'   => $ticket,
				                          'comments' => "Decor auto booking ticket created by customer"
			                          ]);

			// save enquiry id
			$ticketId = $ticket->id;
			$enquiryId = config("evibe.ticket.enq.pre.auto_booked") . $ticketId;
			$ticket->enquiry_id = $enquiryId;
			$ticket->save();

			// create ticket mapping
			$mappingInsertData = [
				'ticket_id'   => $ticketId,
				'map_id'      => $mapId,
				'map_type_id' => config('evibe.ticket.type.decor'),
				'created_at'  => date('Y-m-d H:i:s'),
				'updated_at'  => date('Y-m-d H:i:s')
			];

			// create the mapping of package to the ticket
			$ticketMapping = TicketMapping::create($mappingInsertData);
			if (!$ticketMapping)
			{
				SiteErrorLog::create([
					                     'url'        => request()->fullUrl(),
					                     'exception'  => "Decor AutoBooking: Error while creating ticket mapping",
					                     'code'       => "Error",
					                     'details'    => "Some error occurred in DecorAutoBookingHandlerController [Line: " . __LINE__ . "], while creating a ticket mapping",
					                     'created_at' => date('Y-m-d H:i:s'),
					                     'updated_at' => date('Y-m-d H:i:s')
				                     ]);

				return response()->json($res);
			}
			$ticketMappingId = $ticketMapping->id;

			$previousUrl = url()->previous();

			// create booking info: html full url
			$bookingInfo = "<a href='" . $previousUrl . "?ref=pay-checkout' target='_blank'>" . $decor->name . "</a>";

			// get type ticket booking id from tags
			$typeTicketBookingId = $this->getTypeBookingIdFromTags($decor);

			// verify booking and token amount
			$providerPinCode = $provider->zip;
			$res = $this->makeCurlRequestToGoogleMatrixApi($providerPinCode, $partyPinCode);
			if (($res['status'] == 'OK') && ($res['rows'][0]['elements'][0]['status'] === 'OK'))
			{
				$partyDist = $res['rows'][0]['elements'][0]['distance']['value'];
				$distFreeMeters = ($decor->kms_free ? $decor->kms_free : config('evibe.transport.free-kms')) * 1000;
				$distMaxMeters = ($decor->kms_max) * 1000;
				$baseBookingAmount = $decor->min_price;
				$transMin = $decor->trans_min;
				$transMax = $decor->trans_max;
				$diffDist = $distMaxMeters - $distFreeMeters;
				$diffTransFare = $transMax - $transMin;
				if ($diffDist > 0)
				{
					$farePerMeter = $diffTransFare / $diffDist;
				}
				else
				{
					$farePerMeter = 0;
					$typeTicketName = TypeTicket::find($ticketTypeId)->name;
					$typeProvider = TypeTicket::find($mapTypeId)->name;

					$data = [
						'ticketId'     => $ticketId,
						'optionInfo'   => $bookingInfo,
						'optionCode'   => $decor->code,
						'typeTicket'   => $typeTicketName,
						'providerName' => $decor->provider->name,
						'providerCode' => $decor->provider->code,
						'typeProvider' => $typeProvider
					];
					$this->dispatch(new MailTransportChargesErrorToTeamJob($data));
				}

				$extraDist = $partyDist - $distFreeMeters;
				if ($extraDist > 0)
				{
					$transCharges = intval($extraDist * $farePerMeter);
				}
				else
				{
					$transCharges = 0;
				}
				$estimatedTransCharges = $transCharges;
				if ($partyDist <= $distFreeMeters)
				{
					$transCharges = 0;
				}
				$totalBookingAmount = $transCharges + $baseBookingAmount;
				$roundedTokenAmt = $this->getRoundedTokenAmount($totalBookingAmount);

				if ($totalBookAmount != $totalBookingAmount)
				{
					$totalBookAmount = $totalBookingAmount;
					SiteErrorLog::create([
						                     'url'        => request()->fullUrl(),
						                     'exception'  => "Decor AutoBooking: Tried to hack",
						                     'code'       => "Error",
						                     'details'    => "Some error occurred in DecorAutoBookingHandlerController [Line: " . __LINE__ . "]. Wrong booking amount for ticket id => " . $ticket->id,
						                     'created_at' => date('Y-m-d H:i:s'),
						                     'updated_at' => date('Y-m-d H:i:s')
					                     ]);
				}
				if ($tokenAdvance != $roundedTokenAmt)
				{
					$tokenAdvance = $roundedTokenAmt;
					SiteErrorLog::create([
						                     'url'        => request()->fullUrl(),
						                     'exception'  => "Decor AutoBooking: Tried to hack",
						                     'code'       => "Error",
						                     'details'    => "Some error occurred in DecorAutoBookingHandlerController [Line: " . __LINE__ . "]. Wrong token amount for ticket id => " . $ticket->id,
						                     'created_at' => date('Y-m-d H:i:s'),
						                     'updated_at' => date('Y-m-d H:i:s')
					                     ]);
				}
			}
			else
			{
				SiteErrorLog::create([
					                     'url'        => request()->fullUrl(),
					                     'exception'  => "Decor AutoBooking: Tried to hack",
					                     'code'       => "Error",
					                     'details'    => "Some error occurred in DecorAutoBookingHandlerController [Line: " . __LINE__ . "]. Invalid delivery pincode for ticket id => " . $ticket->id,
					                     'created_at' => date('Y-m-d H:i:s'),
					                     'updated_at' => date('Y-m-d H:i:s')
				                     ]);

				return response()->json([
					                        'success' => false,
					                        'error'   => "Entered pincode is invalid"
				                        ]);
			}

			// create ticket booking
			$bookingInsertData = [
				'booking_id'                  => $enquiryId,
				'booking_info'                => $bookingInfo,
				'product_price'               => $decor->min_price,
				'booking_amount'              => $totalBookAmount,
				'advance_amount'              => $tokenAdvance,
				'estimated_transport_charges' => $estimatedTransCharges,
				'transport_charges'           => $transCharges,
				'map_id'                      => $decor->provider->id,
				'map_type_id'                 => $mapTypeId,
				'ticket_id'                   => $ticketId,
				'ticket_mapping_id'           => $ticketMappingId,
				'is_venue_booking'            => 0,
				'type_ticket_booking_id'      => $typeTicketBookingId,
				'booking_type_details'        => ucwords(config('evibe.auto-book.' . $ticketTypeId)),
				'created_at'                  => date('Y-m-d H:i:s'),
				'updated_at'                  => date('Y-m-d H:i:s')
			];

			$ticketBooking = TicketBooking::create($bookingInsertData);

			if (!$ticketBooking)
			{

				SiteErrorLog::create([
					                     'url'        => request()->fullUrl(),
					                     'exception'  => "Decor AutoBooking: Error while creating ticket booking",
					                     'code'       => "Error",
					                     'details'    => "Some error occurred in DecorAutoBookingHandlerController [Line: " . __LINE__ . "]. Unable to create ticket booking [Ticket id: $ticketId]",
					                     'created_at' => date('Y-m-d H:i:s'),
					                     'updated_at' => date('Y-m-d H:i:s')
				                     ]);

				return response()->json($res);
			}

			$checkoutLink = route('auto-book.decor.pay.auto.checkout');
			$checkoutLink .= "?id=" . $ticketId;
			$checkoutLink .= "&token=" . Hash::make($ticketId);
			$checkoutLink .= "&uKm8=" . Hash::make($phone);

			if ($checkoutLink)
			{
				return response()->json(['success' => true, 'redirectUrl' => $checkoutLink]);
			}
		} catch (\Exception $e)
		{
			SiteErrorLog::create([
				                     'url'        => request()->fullUrl(),
				                     'exception'  => "Decor AutoBooking: Some error occurred while processing Decor AB init",
				                     'code'       => $e->getCode(),
				                     'details'    => $e->getMessage(),
				                     'created_at' => date('Y-m-d H:i:s'),
				                     'updated_at' => date('Y-m-d H:i:s')
			                     ]);

			if (isset($ticket))
			{
				$ticket->forceDelete();
			}
		}
	}

	public function showCheckoutPage()
	{
		$ticketId = request()->input('id');
		$token = request()->input('token');
		$hashedStr = request()->input('uKm8');

		$dataForValidation = new AutoBookUtil();
		$dataForValidation->setTicketId($ticketId);
		$dataForValidation->setToken($token);
		$dataForValidation->setHashedStr($hashedStr);

		$ticket = $this->validateTicket($dataForValidation);
		if (!is_null($ticket))
		{
			$ticketBooking = $ticket->bookings;
			$booking = $ticketBooking->first();

			$data = $this->getTicketData($ticket);

			$totalBookingAmount = $booking->booking_amount;
			$totalAdvanceToPay = ($booking->is_advance_paid == 1) ? 0 : $booking->advance_amount;

			// get mapId and mapTypeId from mapping
			$mapTypeId = $booking->mapping->map_type_id;
			$mapId = $booking->mapping->map_id;

			$mappedValues = $this->fillMappingValues([
				                                         'isCollection' => false,
				                                         'mapId'        => $mapId,
				                                         'mapTypeId'    => $mapTypeId
			                                         ]);

			$decor = Decor::find($mapId);
			$baseBookingAmount = $decor->min_price;

			$data['booking'] = $this->getTicketBookingData($booking, $mappedValues);
			$data['couponToken'] = $this->getAccessTokenForCoupon(intval($ticketId), $ticket->created_at);

			$data['additional']['delivery'] = $totalBookingAmount - $baseBookingAmount;
			$data['additional']['itemInfo'] = $mappedValues['info'];

			$data['booking']['baseBookingAmount'] = $baseBookingAmount;
			$data['booking']['totalBookingAmount'] = $totalBookingAmount;
			$data['booking']['totalAdvanceToPay'] = $totalAdvanceToPay;

			return view('pay/checkout/auto/decor', ['data' => $data]);
		}
		else
		{
			SiteErrorLog::create([
				                     'url'        => request()->fullUrl(),
				                     'exception'  => "Unauthorized Access",
				                     'code'       => 403,
				                     'details'    => "Some error occurred in DecorAutoBookingHandlerController [Line: " . __LINE__ . "]. Token mismatch [Ticket id: $ticketId, token: $token, hashedStr: $hashedStr]",
				                     'created_at' => date('Y-m-d H:i:s'),
				                     'updated_at' => date('Y-m-d H:i:s')
			                     ]);

			return "<h1>Invalid Request</h1>"; // ticketId, token mismatch
		}
	}

	public function processPayment($ticketId)
	{
		$response = [
			'success'    => false,
			'error'      => 'Failed to send booked notifications',
			'redirectTo' => route('pay.common.fail')
		];

		if ($this->sendBookedNotifications($ticketId))
		{
			$token = request()->input('token');
			$response['success'] = true;
			$response['redirectTo'] = route('auto-book.decor.pay.auto.success', $ticketId) . "?token=" . $token;
			unset($response['error']);

			// handle post payment
			$this->dispatch(new HandlePostPaymentProcessJob($ticketId));
		}

		return response()->json($response);
	}

	public function onPaymentSuccess($ticketId)
	{
		$token = request()->input('token');
		$ticket = Ticket::with('bookings.mapping', 'bookings.provider')->find($ticketId);
		if (!$ticket)
		{
			return "<h1>Invalid Request</h1>";
		}
		$ticketBookings = $ticket->bookings;
		$booking = $ticketBookings[0];
		$city = City::find($ticket->city_id);
		$cityUrl = $city->url;

		if (Hash::check($ticket->phone, $token))
		{
			$data = $this->getTicketAndSingleBookingData($ticket, $booking);
			$data['eventId'] = $ticket->event_id;
			$data = array_merge($data, $this->mergeView($cityUrl, $ticket->event_id));

			checkAndSetCitySessions($city);

			return view('pay/success/decor', ["data" => $data]);
		}
		else
		{
			SiteErrorLog::create([
				                     'url'        => request()->fullUrl(),
				                     'exception'  => "Decor AutoBooking: Tried to hack",
				                     'code'       => "Error",
				                     'details'    => "Some error occurred in DecorAutoBookingHandlerController [Line: " . __LINE__ . "]. Tried to change the ticket id with invalid token :: $ticketId",
				                     'created_at' => date('Y-m-d H:i:s'),
				                     'updated_at' => date('Y-m-d H:i:s')
			                     ]);

			return "<h1>Invalid Request</h1>";
		}
	}

	private function sendBookedNotifications($ticketId, $isFromDash = false, $handlerId = null)
	{
		$dataForNotification = new PaySuccessUpdateTicket();
		$dataForNotification->setTicketNotificationId($ticketId);
		$dataForNotification->setIsFromDash($isFromDash);
		$dataForNotification->setHandlerId($handlerId);

		$data = $this->getNotificationData($dataForNotification);
		if (!is_null($data))
		{
			// vendor ask page URL
			$token = Hash::make($data['customer']['phone']);
			$confirmationLink = route('auto-book.decor.actions.partner.ask', $data['ticketId']) . "?token=" . $token;
			$data['vendorAskUrl'] = $this->getShortenUrl($confirmationLink);

			// send notification to customer, vendor, team
			$this->initBookedNotificationToCustomer($data);
			$this->initBookedNotificationToPartner($data);
			$this->dispatch(new MailAutoBookingDecorAlertToTeamJob($data));

			$this->createAutoBookingNotificationToApp($data['firstBooking']);

			// update timestamp & save action
			$forUpdateData = new PaySuccessUpdateTicket();
			$forUpdateData->setHandlerId($handlerId);
			$forUpdateData->setIsFromDash($isFromDash);
			$forUpdateData->setStatusId(config('evibe.ticket.status.auto_paid'));
			$forUpdateData->setSuccessEmailType(config('evibe.ticket.success_type.booked'));

			return $this->updateTicketPostSendReceipts($data['ticket'], $forUpdateData);
		}

		return false;
	}

	private function initBookedNotificationToPartner($data)
	{
		$tplVendor = config('evibe.sms_tpl.auto_book.decor.pay_success.partner');
		$replaces = [
			'#field1#' => $data['booking']['provider']['person'],
			'#field2#' => date('d-M-y', strtotime($data['additional']['checkInDate'])) . ' (' . date('D', strtotime($data['additional']['checkInDate'])) . '), ' . $data['booking']['checkInTime'],
			'#field3#' => $this->truncateText($data['additional']['venueLocation'], 10),
			'#field4#' => $this->truncateText($data['booking']['name'], 10),
			'#field5#' => $this->formatPrice($data['booking']['advanceAmount']),
			'#field6#' => $this->getShortenUrl($data['vendorAskUrl'])
		];

		$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplVendor);
		$smsData = [
			'to'   => $data['booking']['provider']['phone'],
			'text' => $smsText
		];

		// send email and sms to partner
		$this->dispatch(new MailAutoBookingDecorAlertToPartnerJob($data));
		$this->dispatch(new SMSDecorAutoBookingAlertToPartner($smsData));
	}

	private function initBookedNotificationToCustomer($data)
	{
		$tplCustomer = config('evibe.sms_tpl.auto_book.decor.pay_success.customer');
		$token = Hash::make($data["ticketId"] . "EVBTMO");

		$replaces = [
			'#field1#' => $data['customer']['name'],
			'#field2#' => $this->formatPrice($data['booking']['advanceAmount']),
			'#field3#' => $data['ticket']['enquiry_id'],
			'#field4#' => "4",
			'#field5#' => $this->getShortenUrl(route("track.orders") . "?ref=SMS&id=" . $data["ticketId"] . "&token=" . $token)
		];
		$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);
		$smsData = [
			'to'   => $data['customer']['phone'],
			'text' => $smsText
		];

		// Send email and sms to customer
		$this->dispatch(new MailAutoBookingDecorAlertToCustomerJob($data));
		$this->dispatch(new SMSDecorAutoBookingAlertToCustomer($smsData));
	}
}