<?php

namespace App\Http\Controllers\AutoBook\HandlerControllers;

use App\Evibe\Implementers\HandlesPayments;
use App\Http\Controllers\AutoBook\HandlerControllers\Util\BaseAutoBookingHandlerController;
use App\Http\Controllers\Base\BaseCakeController;
use App\Jobs\Emails\Payment\Customer\MailAutoBookingCakeAlertToCustomerJob;
use App\Jobs\Emails\Payment\Team\MailAutoBookingCakeAlertToTeamJob;
use App\Jobs\Emails\Payment\Vendor\MailAutoBookingCakeAlertToPartnerJob;
use App\Jobs\Emails\ReportIssue\MissingCityAlertToTeam;
use App\Jobs\HandlePostPaymentProcessJob;
use App\Jobs\SMS\SMSCakeAutoBookingAlertToCustomer;
use App\Jobs\SMS\SMSCakeAutoBookingAlertToPartner;
use App\Models\Cake\Cake;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketBooking;
use App\Models\Ticket\TicketMapping;
use App\Models\Util\CheckoutFieldValue;
use App\Models\Util\City;
use App\Models\Util\DeliverySlot;

use App\Models\Util\SiteErrorLog;
use Evibe\Passers\AutoBookUtil;
use Evibe\Passers\PaySuccessUpdateTicket;
use \Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class CakeAutoBookingHandlerController extends BaseAutoBookingHandlerController implements HandlesPayments
{
	protected $weightName;
	protected $slotName;
	protected $deliveryChargeName;
	protected $cakeTypeName;
	protected $cakeFlavourName;
	protected $cakeMessageName;

	public function __construct()
	{
		parent::__construct();

		$this->weightName = $this->getCheckoutFieldName(config('evibe.checkout_field.cake.weight'));
		$this->slotName = $this->getCheckoutFieldName(config('evibe.checkout_field.cake.delivery_slot'));
		$this->deliveryChargeName = $this->getCheckoutFieldName(config('evibe.checkout_field.cake.delivery_charge'));
		$this->cakeTypeName = $this->getCheckoutFieldName(config('evibe.checkout_field.cake.type'));
		$this->cakeFlavourName = $this->getCheckoutFieldName(config('evibe.checkout_field.cake.flavour'));
		$this->cakeMessageName = $this->getCheckoutFieldName(config('evibe.checkout_field.cake.message'));
	}

	public function initAutoBooking($cakeId, $occasionId)
	{
		$products = [];
		array_push($products, [
			'productId'     => $cakeId,
			'productTypeId' => config('evibe.ticket.type.cake'),
			'isAddOn'       => false
		]);

		$addOns = request('addOns');
		if (count($addOns))
		{
			foreach ($addOns as $addOnId => $addOnCount)
			{
				if ($addOnCount > 0)
				{
					array_push($products, [
						'productId'     => $addOnId,
						'productTypeId' => config('evibe.ticket.type.add-on'),
						'isAddOn'       => true,
						'unitCount'     => $addOnCount
					]);
				}
			}
		}
		$res = $this->validateAndInitAutoBooking($products, $occasionId);

		return response()->json($res);
	}

	public function initCakeAutoBooking($cakeId, $occasionId, Request $request)
	{
		$res = ['success' => false];

		try
		{
			// validate user data
			$rules = [
				'name'             => 'required',
				'phone'            => 'required|phone',
				'email'            => 'required|email',
				'cakeDeliveryDate' => 'date|after:today',
			];

			$messages = [
				'phone.required'   => 'Please enter your phone number',
				'phone.phone'      => 'Phone number is invalid (ex: 9640204000)',
				'name.required'    => 'Please enter your name',
				'email.required'   => 'Please enter your email',
				'email.email'      => 'Please enter a valid email',
				'cakeDeliveryDate' => 'Please enter delivery date after today'
			];

			$validator = validator(request()->input(), $rules, $messages);

			if ($validator->fails())
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => $validator->messages()->first()
				                        ]);
			}

			$cake = Cake::find($cakeId);
			if (!$cake)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => "could not find cake"
				                        ]);
			}

			$ticketTypeId = config('evibe.ticket.type.cake');
			$mapTypeId = config('evibe.ticket.type.planner');

			// collect user input
			$name = $request->input('name');
			$phone = $request->input('phone');
			$email = $request->input('email');
			$deliveryDate = $request->input('cakeDeliveryDate');
			$slotId = $request->input('cakeSlot');
			$messageOnCake = $request->input('message');
			$price = $request->input('price');
			$deliveryCharge = $request->input('deliveryCharge');

			// deadline calculation
			// if booked before 12 today, put deadline tomorrow 8 AM
			// if booked after 12 today, put deadline tomorrow 4 PM
			// reminder will be sent at 9 AM and 5 PM everyday for un-acted tickets.
			$currentHour = date("H"); // returns in 24 Hrs format
			$deadlineDateAndTime = strtotime("today 4 PM");
			if ($currentHour > 12)
			{
				$deadlineDateAndTime = strtotime("tomorrow 8 AM");
			}

			$cityId = 1; // default: Bangalore
			if ($cake->provider && $cake->provider->city_id)
			{
				$cityId = $cake->provider->city_id;
			}
			else
			{
				$cityId = (getCityId() > 0) ? getCityId() : $cityId;
				$optionData = [
					'name' => $cake->title,
					'code' => $cake->code,
					'type' => config('evibe.type-product-title.' . ($ticketTypeId))
				];

				$this->dispatch(new MissingCityAlertToTeam($optionData));
			}

			$city = City::find($cityId);
			checkAndSetCitySessions($city);

			// create ticket with appropriate details
			$ticketInsertData = [
				'name'                    => $name,
				'email'                   => $email,
				'phone'                   => $phone,
				'event_date'              => strtotime($deliveryDate),
				'type_ticket_id'          => $ticketTypeId,
				'is_auto_booked'          => 1,
				'status_id'               => config('evibe.ticket.status.initiated'),
				'city_id'                 => $cityId,
				'auto_book_deadline_date' => $deadlineDateAndTime,
				'event_id'                => $occasionId,
				'created_at'              => date('Y-m-d H:i:s'),
				'updated_at'              => date('Y-m-d H:i:s'),
				'enquiry_source_id'       => config('evibe.ticket.enquiry_source.autobook')
			];

			// Create the ticket with phone number and event date or check in date
			$ticket = Ticket::create($ticketInsertData);

			if (!$ticket)
			{
				SiteErrorLog::create([
					                     'url'        => request()->fullUrl(),
					                     'exception'  => "Cake AutoBooking: Error while creating ticket",
					                     'code'       => "Error",
					                     'details'    => "Some error occurred in CakeAutoBookingHandlerController [Line: " . __LINE__ . "], while creating a ticket",
					                     'created_at' => date('Y-m-d H:i:s'),
					                     'updated_at' => date('Y-m-d H:i:s')
				                     ]);

				return response()->json(['success' => false]);
			}

			$this->updateTicketAction([
				                          'ticket'   => $ticket,
				                          'comments' => "Cake auto booking ticket created by customer"
			                          ]);

			// verify delivery charge
			$slot = DeliverySlot::findOrFail(trim($slotId));
			$cakeDeliverySlot = $slot->formattedSlot();

			if ($slot->price !== $deliveryCharge)
			{
				SiteErrorLog::create([
					                     'url'        => request()->fullUrl(),
					                     'exception'  => "Cake AutoBooking: Tried to hack",
					                     'code'       => "Error",
					                     'details'    => "Some error occurred in CakeAutoBookingHandlerController [Line: " . __LINE__ . "]. Wrong deliver charge for ticket id => " . $ticket->id,
					                     'created_at' => date('Y-m-d H:i:s'),
					                     'updated_at' => date('Y-m-d H:i:s')
				                     ]);

				$deliveryCharge = $slot->price;
			}

			// calculate party start and end time based on the slot
			$partyStartTime = strtotime($deliveryDate) + strtotime($slot->start_time) - strtotime('today midnight');
			$partyEndTime = strtotime($deliveryDate) + strtotime($slot->end_time) - strtotime('today midnight');

			// update calculated event date for ticket
			$ticket->event_date = $partyStartTime;

			// fill the data in checkout field value
			$checkoutFields = [
				config('evibe.checkout_field.cake.message')         => $messageOnCake,
				config('evibe.checkout_field.cake.delivery_charge') => $deliveryCharge,
				config('evibe.checkout_field.cake.delivery_slot')   => $cakeDeliverySlot
			];

			$categories = $request->input('categories');

			$baseCake = new BaseCakeController();
			$priceData = $baseCake->calculateCakePriceFromBackend($categories, $cakeId);

			// re verify price from backed;
			if ((int)$price !== (int)$priceData['price'])
			{
				SiteErrorLog::create([
					                     'url'        => request()->fullUrl(),
					                     'exception'  => "Cake AutoBooking: Tried to hack",
					                     'code'       => "Error",
					                     'details'    => "Some error occurred in CakeAutoBookingHandlerController [Line: " . __LINE__ . "]. Wrong price charge from user for ticket id => " . $ticket->id,
					                     'created_at' => date('Y-m-d H:i:s'),
					                     'updated_at' => date('Y-m-d H:i:s')
				                     ]);

				$price = $priceData['price'];
			}

			$advanceAmount = (int)($price + $deliveryCharge);

			foreach ($categories as $category)
			{
				if ($category['catId'] == config('evibe.cake_cat_value.weight'))
				{
					$checkoutFields[config('evibe.checkout_field.cake.weight')] = $category['name'];
				}
				elseif ($category['catId'] == config('evibe.cake_cat_value.eggless'))
				{
					$checkoutFields[config('evibe.checkout_field.cake.type')] = $category['name'];
				}
				elseif ($category['catId'] == config('evibe.cake_cat_value.flavour'))
				{
					$checkoutFields[config('evibe.checkout_field.cake.flavour')] = $category['name'];
				}

			}

			if ($advanceAmount)
			{
				foreach ($checkoutFields as $key => $value)
				{
					if ($value || $key == config('evibe.checkout_field.cake.delivery_charge'))
					{
						CheckoutFieldValue::create([
							                           'ticket_id'         => $ticket->id,
							                           'checkout_field_id' => $key,
							                           'value'             => $value
						                           ]);
					}
				}
			}

			// save enquiry id
			$ticketId = $ticket->id;
			$enquiryId = config('evibe.ticket.enq.pre.auto_booked') . $ticketId;
			$ticket->enquiry_id = $enquiryId;
			$ticket->save();

			// create ticket mapping
			$mappingInsertData = [
				'ticket_id'   => $ticketId,
				'map_id'      => $cakeId,
				'map_type_id' => config('evibe.ticket.type.cake'),
				'created_at'  => date('Y-m-d H:i:s'),
				'updated_at'  => date('Y-m-d H:i:s')
			];

			$previousUrl = url()->previous();

			// create the mapping of package to the ticket
			$ticketMapping = TicketMapping::create($mappingInsertData);
			if (!$ticketMapping)
			{
				SiteErrorLog::create([
					                     'url'        => request()->fullUrl(),
					                     'exception'  => "Cake AutoBooking: Error while creating ticket mapping",
					                     'code'       => "Error",
					                     'details'    => "Some error occurred in CakeAutoBookingHandlerController [Line: " . __LINE__ . "], while creating a ticket mapping",
					                     'created_at' => date('Y-m-d H:i:s'),
					                     'updated_at' => date('Y-m-d H:i:s')
				                     ]);

				return response()->json(['success' => false]);
			}
			$ticketMappingId = $ticketMapping->id;

			// create booking info: html full url
			$bookingInfo = "<a href='" . $previousUrl . "?ref=pay-checkout' target='_blank'>" . $cake->title . "</a>";

			// create ticket booking
			$bookingInsertData = [
				'booking_id'             => $enquiryId,
				'booking_info'           => $bookingInfo,
				'party_date_time'        => $partyStartTime,
				'party_end_time'         => $partyEndTime,
				'booking_amount'         => $advanceAmount,
				'advance_amount'         => $advanceAmount,
				'map_id'                 => $cake->provider->id,
				'map_type_id'            => $mapTypeId,
				'ticket_id'              => $ticketId,
				'ticket_mapping_id'      => $ticketMappingId,
				'is_venue_booking'       => 0,
				'type_ticket_booking_id' => config('evibe.booking_type.cake'),
				'created_at'             => date('Y-m-d H:i:s'),
				'updated_at'             => date('Y-m-d H:i:s')
			];

			$ticketBooking = TicketBooking::create($bookingInsertData);

			if (!$ticketBooking)
			{
				SiteErrorLog::create([
					                     'url'        => request()->fullUrl(),
					                     'exception'  => "Cake AutoBooking: Error while creating ticket booking",
					                     'code'       => "Error",
					                     'details'    => "Some error occurred in CakeAutoBookingHandlerController [Line: " . __LINE__ . "]. Unable to create ticket booking [Ticket id: $ticketId]",
					                     'created_at' => date('Y-m-d H:i:s'),
					                     'updated_at' => date('Y-m-d H:i:s')
				                     ]);

				return response()->json(['success' => false]);
			}

			$checkoutLink = route('auto-book.cake.pay.auto.checkout');
			$checkoutLink .= "?id=" . $ticketId;
			$checkoutLink .= "&token=" . Hash::make($ticketId);
			$checkoutLink .= "&uKm8=" . Hash::make($phone);

			if ($checkoutLink)
			{
				return response()->json(['success' => true, 'redirectUrl' => $checkoutLink]);
			}
		} catch (\Exception $e)
		{
			SiteErrorLog::create([
				                     'url'        => request()->fullUrl(),
				                     'exception'  => "Cake AutoBooking: Some error occurred while processing Decor AB init",
				                     'code'       => $e->getCode(),
				                     'details'    => $e->getMessage(),
				                     'created_at' => date('Y-m-d H:i:s'),
				                     'updated_at' => date('Y-m-d H:i:s')
			                     ]);

			if (isset($ticket))
			{
				$ticket->forceDelete();
			}
		}

		return response()->json(['success' => false]);
	}

	/* @implements */
	public function showCheckoutPage()
	{
		$ticketId = request()->input('id');
		$token = request()->input('token');
		$hashedStr = request()->input('uKm8');

		$dataForValidation = new AutoBookUtil();
		$dataForValidation->setTicketId($ticketId);
		$dataForValidation->setToken($token);
		$dataForValidation->setHashedStr($hashedStr);

		$ticket = $this->validateTicket($dataForValidation);
		if (!is_null($ticket))
		{
			$ticketBooking = $ticket->bookings;
			$booking = $ticketBooking->first();

			$data = $this->getTicketData($ticket);

			$deliveryCharge = (int)CheckoutFieldValue::where([
				                                                 'ticket_id'         => $ticket->id,
				                                                 'checkout_field_id' => config('evibe.checkout_field.cake.delivery_charge')
			                                                 ])->first()->value;

			$totalAdvanceToPay = ($booking->is_advance_paid == 1) ? 0 : $booking->advance_amount;

			// get mapId and mapTypeId from mapping
			$mapTypeId = $booking->mapping->map_type_id;
			$mapId = $booking->mapping->map_id;

			$mappedValues = $this->fillMappingValues([
				                                         'isCollection' => false,
				                                         'mapId'        => $mapId,
				                                         'mapTypeId'    => $mapTypeId
			                                         ]);

			$data['booking'] = $this->getTicketBookingData($booking, $mappedValues);
			$data['couponToken'] = $this->getAccessTokenForCoupon(intval($ticketId), $ticket->created_at);

			$data['additional']['delivery'] = $deliveryCharge;
			$data['additional']['itemInfo'] = $mappedValues['info'];

			$data['booking']['cakePrice'] = ($booking->advance_amount - $deliveryCharge);
			$data['booking']['totalAdvanceToPay'] = $totalAdvanceToPay;

			return view('pay/checkout/auto/cake', ['data' => $data]);
		}
		else
		{
			SiteErrorLog::create([
				                     'url'        => request()->fullUrl(),
				                     'exception'  => "Unauthorized Access",
				                     'code'       => 403,
				                     'details'    => "Some error occurred in CakeAutoBookingHandlerController [Line: " . __LINE__ . "]. Token mismatch [Ticket id: $ticketId, token: $token, hashedStr: $hashedStr]",
				                     'created_at' => date('Y-m-d H:i:s'),
				                     'updated_at' => date('Y-m-d H:i:s')
			                     ]);

			return "<h1>Invalid Request</h1>"; // ticketId, token mismatch
		}
	}

	/* @implements */
	public function processPayment($ticketId)
	{
		$response = [
			'success'    => false,
			'error'      => 'Failed to send booked notifications',
			'redirectTo' => route('pay.common.fail')
		];

		if ($this->sendBookedNotifications($ticketId))
		{
			$token = request()->input('token');
			$response['success'] = true;
			$response['redirectTo'] = route('auto-book.cake.pay.auto.success', $ticketId) . "?token=" . $token;
			unset($response['error']);

			// handle post payment
			$this->dispatch(new HandlePostPaymentProcessJob($ticketId));
		}

		return response()->json($response);
	}

	/* @implements */
	public function onPaymentSuccess($ticketId)
	{
		$token = request()->input('token');
		$ticket = Ticket::with('bookings.mapping', 'bookings.provider')->findOrFail($ticketId);
		$ticketBookings = $ticket->bookings;
		$booking = $ticketBookings[0];
		$city = City::find($ticket->city_id);
		$cityUrl = $city->url;

		if (Hash::check($ticket->phone, $token))
		{
			$data = $this->getTicketAndSingleBookingData($ticket, $booking);
			$data['cakeWeight'] = $data['additional'][$this->weightName];
			$data['cakeSlot'] = $data['additional'][$this->slotName];
			$data['eventId'] = $ticket->event_id;
			$data = array_merge($data, $this->mergeView($cityUrl, $ticket->event_id));

			checkAndSetCitySessions($city);

			return view('pay/success/cake', ["data" => $data]);
		}
		else
		{
			SiteErrorLog::create([
				                     'url'        => request()->fullUrl(),
				                     'exception'  => "Cake AutoBooking: Tried to hack",
				                     'code'       => "Error",
				                     'details'    => "Some error occurred in CakeAutoBookingHandlerController [Line: " . __LINE__ . "]. Tried to change the ticket id with invalid token :: $ticketId",
				                     'created_at' => date('Y-m-d H:i:s'),
				                     'updated_at' => date('Y-m-d H:i:s')
			                     ]);

			return "<h1>Invalid Request</h1>";
		}
	}

	private function sendBookedNotifications($ticketId, $isFromDash = false, $handlerId = null)
	{
		$dataForNotification = new PaySuccessUpdateTicket();
		$dataForNotification->setTicketNotificationId($ticketId);
		$dataForNotification->setIsFromDash($isFromDash);
		$dataForNotification->setHandlerId($handlerId);

		$data = $this->getNotificationData($dataForNotification);
		if (!is_null($data))
		{
			// vendor ask page URL
			$token = Hash::make($data['customer']['phone']);
			$confirmationLink = route('auto-book.cake.actions.partner.ask', $data['ticketId']) . "?token=" . $token;
			$data['vendorAskUrl'] = $this->getShortenUrl($confirmationLink);

			// send notification to customer, vendor, team
			$this->initBookedNotificationToCustomer($data);
			$this->initBookedNotificationToPartner($data);
			$this->dispatch(new MailAutoBookingCakeAlertToTeamJob($data));

			$this->createAutoBookingNotificationToApp($data['firstBooking']);

			// update timestamp & save action
			$forUpdateData = new PaySuccessUpdateTicket();
			$forUpdateData->setHandlerId($handlerId);
			$forUpdateData->setIsFromDash($isFromDash);
			$forUpdateData->setStatusId(config('evibe.ticket.status.auto_paid'));
			$forUpdateData->setSuccessEmailType(config('evibe.ticket.success_type.booked'));

			return $this->updateTicketPostSendReceipts($data['ticket'], $forUpdateData);
		}

		return false;
	}

	private function initBookedNotificationToCustomer($data)
	{
		$tplCustomer = config('evibe.sms_tpl.auto_book.cake.pay_success.customer');
		$token = Hash::make($data["ticketId"] . "EVBTMO");

		$replaces = [
			'#field1#' => $data['customer']['name'],
			'#field2#' => $this->formatPrice($data['booking']['advanceAmount']),
			'#field3#' => $data['ticket']['enquiry_id'],
			'#field4#' => "4",
			'#field5#' => $this->getShortenUrl(route("track.orders") . "?ref=SMS&id=" . $data["ticketId"] . "&token=" . $token)
		];
		$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);
		$smsData = [
			'to'   => $data['customer']['phone'],
			'text' => $smsText
		];

		// Send email and sms to customer
		$this->dispatch(new MailAutoBookingCakeAlertToCustomerJob($data));
		$this->dispatch(new SMSCakeAutoBookingAlertToCustomer($smsData));
	}

	private function initBookedNotificationToPartner($data)
	{
		$tplVendor = config('evibe.sms_tpl.auto_book.cake.pay_success.partner');
		$replaces = [
			'#field1#' => 'Partner',
			'#field2#' => $this->truncateText($data['booking']['name'], 15),
			'#field3#' => $data['additional'][$this->weightName],
			'#field4#' => $data['additional'][$this->slotName],
			'#field5#' => $data['booking']['partyDate'],
			'#field6#' => $this->getShortenUrl($data['vendorAskUrl'])
		];

		$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplVendor);
		$smsData = [
			'to'   => $data['booking']['provider']['phone'],
			'text' => $smsText
		];

		// send email and sms to partner
		$this->dispatch(new MailAutoBookingCakeAlertToPartnerJob($data));
		$this->dispatch(new SMSCakeAutoBookingAlertToPartner($smsData));
	}
}
