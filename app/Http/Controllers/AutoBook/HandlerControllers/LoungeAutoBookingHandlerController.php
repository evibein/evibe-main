<?php

namespace App\Http\Controllers\AutoBook\HandlerControllers;

use App\Evibe\Implementers\HandlesPayments;
use App\Http\Controllers\AutoBook\HandlerControllers\Util\BasePackageAutoBookingHandlerController;

class LoungeAutoBookingHandlerController extends BasePackageAutoBookingHandlerController implements HandlesPayments
{
	public function initAutoBooking($mapTypeId, $mapId, $typeId)
	{
		$type = "Lounge";
		$result = $this->checkProductLive($mapTypeId, $mapId, $typeId, $type);

		return response()->json($result);
	}

	public function showCheckoutPage()
	{
		$data = $this->showCheckoutPageBasePackage();

		if (!is_null($data))
		{
			return view($data['view'], ['data' => $data]);
		}

		return "<h1>Invalid Request</h1>"; // ticketId, token mismatch
	}

	public function processPayment($ticketId)
	{
		$result = $this->processPaymentBasePackage($ticketId);

		return response()->json($result);
	}

	public function onPaymentSuccess($ticketId)
	{
		$data = $this->onPaymentSuccessBasePackage($ticketId);
		if (!is_null($data))
		{
			return view($data['view'], ['data' => $data]);
		}

		return redirect(route("missing"));
	}
}