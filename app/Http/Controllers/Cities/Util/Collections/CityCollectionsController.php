<?php

namespace App\Http\Controllers\Cities\Util\Collections;

use App\Http\Controllers\Base\BaseCollectionController;

class CityCollectionsController extends BaseCollectionController
{
	public function __construct()
	{
		parent::__construct();
		$this->isCityPage = true;
	}

	public function showCollectionsList($cityUrl)
	{
		$listData = $this->getCollectionListData($cityUrl);
		if (!is_array($listData))
		{
			return $listData; // validation failed, redirect to given url
		}

		return view('cities/util/collections/list', ['data' => $listData]);
	}

	public function showCollectionProfile($cityUrl, $collectionUrl)
	{
		$profileData = $this->getCollectionProfileData($cityUrl, $collectionUrl);
		if (!is_array($profileData))
		{
			return $profileData; // validation failed, redirect to given url
		}

		return view('cities/util/collections/profile', ['data' => $profileData]);
	}
}