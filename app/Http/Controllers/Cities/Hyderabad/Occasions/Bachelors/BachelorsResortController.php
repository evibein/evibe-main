<?php

namespace App\Http\Controllers\Cities\Hyderabad\Occasions\Bachelors;

use App\Http\Controllers\Occasions\Bachelors\BachelorsResortController as DefaultBachelorsBaseResortController;

class BachelorsResortController extends DefaultBachelorsBaseResortController
{
	public function __construct()
	{
		parent::__construct();
	}

	//// For hyderabad: resorts + villas are shown in single page
	//public function showCustomResortProfile($params)
	//{
	//	$cityUrl = $params['cityUrl'];
	//	$packageUrl = $params['packageUrl'];
	//	$validUrlObj = isset($params['validation']) ? $params['validation'] : false;
	//	$hashTag = "#" . config('evibe.occasion.bachelor.hashtag');
	//
	//	if (!$validUrlObj)
	//	{
	//		$urlParams = [
	//			['key' => 'city', 'value' => $cityUrl],
	//			['key' => 'resort', 'value' => $packageUrl]
	//		];
	//		$validUrlObj = $this->validateUrlParams($urlParams);
	//
	//		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
	//		{
	//			return redirect($validUrlObj['redirectUrl']);
	//		}
	//	}
	//
	//	$data = $this->getResortProfileData($cityUrl, $packageUrl, $hashTag);
	//
	//	return view('occasion/bachelor/villa/profile', ['data' => $data]);
	//}
}