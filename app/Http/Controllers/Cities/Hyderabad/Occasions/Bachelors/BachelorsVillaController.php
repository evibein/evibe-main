<?php

namespace App\Http\Controllers\Cities\Hyderabad\Occasions\Bachelors;

use App\Http\Controllers\Occasions\Bachelors\BachelorsVillaController as DefaultBachelorsBaseVillaController;

class BachelorsVillaController extends DefaultBachelorsBaseVillaController
{
	public function __construct()
	{
		parent::__construct();

		// villa packages + resort packages (for Hyderabad city)
		$this->setPageId([config('evibe.ticket.type.villas'), config('evibe.ticket.type.resorts')]);
	}

	public function showCustomVillasList($params)
	{
		$cityUrl = $params['cityUrl'];
		$validUrlObj = isset($params['validation']) ? $params['validation'] : false;

		if (!$validUrlObj)
		{
			$urlParams = [['key' => 'city', 'value' => $cityUrl]];
			$validUrlObj = $this->validateUrlParams($urlParams);
			if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
			{
				return redirect($validUrlObj['redirectUrl']);
			}
		}

		$data = $this->getVillaListData($cityUrl);

		return view('occasion/bachelor/villa/list', ['data' => $data]);
	}
}