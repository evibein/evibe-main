<?php

namespace App\Http\Controllers\Cities\Hyderabad\Occasions\Bachelors;

use App\Http\Controllers\Occasions\Bachelors\BachelorsHomeController as BachelorsBaseHomeController;

class BachelorsHomeController extends BachelorsBaseHomeController
{
	public function __construct()
	{
		parent::__construct();
	}

	public function showCustomHome($params)
	{
		$validUrlObj = isset($params['validation']) ? $params['validation'] : false;

		if (!$validUrlObj)
		{
			$cityUrl = $params['cityUrl'];
			$validUrlObj = $this->validateUrlParams($cityUrl);
			if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
			{
				return redirect($validUrlObj['redirectUrl']);
			}
		}

		// override topResorts to topVillas for Hyderabad
		$viewData = $this->getHomeViewData();

		$pageId = config('evibe.ticket.type.villas');
		$topVillas = $viewData['topVillas'];
		$autoBookableData = $this->getPaginatedAutoBookableItems($pageId, $topVillas, 6);
		$viewData['autoBook'] = $autoBookableData['autoBook'];

		return view('occasion/bachelor/home/home', ['data' => $viewData]);
	}

}
