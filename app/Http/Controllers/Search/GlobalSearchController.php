<?php

namespace App\Http\Controllers\Search;

use App\Http\Controllers\Base\BaseListableController;
use App\Jobs\Log\LogGlobalSearchQueryJob;
use App\Jobs\Log\UpdateGlobalSearchData;
use App\Jobs\Log\LogSearchInvalidOptionsJob;
use App\Models\Cake\Cake;
use App\Models\Decor\Decor;
use App\Models\Search\SearchableProduct;
use App\Models\Trend\Trend;
use App\Models\Types\TypeServices;
use App\Models\Package\Package;

use App\Models\Util\City;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class GlobalSearchController extends BaseListableController
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getSearchResults($cityUrl)
	{
		$validUrlObj = $this->validateUrlParams($cityUrl);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$cityId = getCityId();
		$occasionUrl = $this->getOccasionUrl() ?: null;
		$searchQuery = strtolower(trim(request('q')));
		$searchResults = [];
		$clearFilter = false;
		$filters = [
			'category' => request('category', null),
			'priceMin' => request('price_min', null),
			'priceMax' => request('price_max', null),
			'price'    => request('price', null)
		];

		$searchQuery = preg_replace('/[^A-Za-z0-9\- ]/', '', str_replace('"', "'", $searchQuery));

		// log queries
		$logData = [
			'cityId'         => $cityId,
			'eventId'        => $this->getOccasionId(),
			'keyword'        => $searchQuery,
			'pageUrl'        => request()->url(),
			'isGlobalSearch' => 1
		];
		$this->dispatch(new LogGlobalSearchQueryJob($logData));

		// Reset Filters
		if ($filters["price"] || $filters["category"])
		{
			$clearFilter = true;
		}

		$esPageName = 'esPageNumber'; // different pagination params for the list
		$maxResults = 10000; // this is max allowed by Elasticsearch

		// @see: Elasticsearch returns 10 results by default, paginate to max
		// orWhere & complex where queries not available for Scout hence using collection
		$searchProductResults = SearchableProduct::search($searchQuery)
		                                         ->where('city_id', $cityId);

		$searchProductResults = $searchProductResults->paginate($maxResults, $esPageName);
		$searchProductResults = collect($searchProductResults->items())->toArray();

		// remove venue deals
		$searchProductResults = array_filter($searchProductResults, function ($product) {
			return ($product["option_sub_type_id"] != config('evibe.ticket.type.venue-deals'));
		});

		$applicableCategoryIds = array_unique(array_column($searchProductResults, "option_sub_type_id"));

		// Category Filter
		if ($filters["category"] && is_numeric($filters["category"]) && ($filters["category"] > 0))
		{
			$searchProductResults = array_filter($searchProductResults, function ($product) use ($filters) {
				return ($product["option_sub_type_id"] == $filters["category"]);
			});
		}

		// filter by price
		if ($filters["price"] && !is_null($filters["price"]))
		{
			if ($filters["price"] == "asc")
			{
				usort($searchProductResults, [$this, "sortArrayPriceAsc"]);
			}
			elseif ($filters["price"] == "desc")
			{
				usort($searchProductResults, [$this, "sortArrayPriceDesc"]);
			}
		}

		// pagination
		$pageName = 'page'; // different pagination params for the list
		$currentPage = Paginator::resolveCurrentPage($pageName);
		$perPage = 48;
		$offset = ($currentPage * $perPage) - $perPage;

		$searchProductResults = new LengthAwarePaginator(
			array_slice($searchProductResults, $offset, $perPage, true),
			count($searchProductResults),
			$perPage,
			$currentPage,
			['path' => Paginator::resolveCurrentPath(), 'pageName' => $pageName]
		);

		$distinctOptionTypeIds = array_unique($searchProductResults->pluck("option_type_id")->toArray());
		$optionTypeProductsBank = []; // @see: to reduce queries

		foreach ($distinctOptionTypeIds as $distinctOptionTypeId)
		{
			$filteredOptionProducts = $searchProductResults->filter(function ($product) use ($distinctOptionTypeId) {
				if ($product["option_type_id"] == $distinctOptionTypeId)
				{
					return true;
				}

				return false;
			});

			$optionIds = $filteredOptionProducts->pluck("option_id");
			$optionTypeProducts = $this->getProductsByOptionType($distinctOptionTypeId, $optionIds);

			// @see: do not iterate here to preserve relevance score
			foreach ($optionTypeProducts as $optionTypeProduct)
			{
				$key = $distinctOptionTypeId . "_" . $optionTypeProduct->id;
				$optionTypeProductsBank[$key] = $optionTypeProduct;
			}
		}

		foreach ($searchProductResults as $searchProduct)
		{
			$optionTypeId = $searchProduct["option_type_id"];
			$optionSubTypeId = $searchProduct["option_sub_type_id"];
			$optionId = $searchProduct["option_id"];
			$key = $optionTypeId . "_" . $optionId;

			$logData = [
				"productTypeId"    => $optionTypeId,
				"productSubtypeId" => $optionSubTypeId,
				"productId"        => $optionId,
				"errorMessage"     => "Product not found"
			];

			if (!isset($optionTypeProductsBank[$key]))
			{
				$this->dispatch(new LogSearchInvalidOptionsJob($logData));

				continue;
			}

			$product = $optionTypeProductsBank[$key];
			$mapTypeId = $optionSubTypeId; // made by default
			$mapName = $this->getTypeNameById($mapTypeId);
			$cardParams = [
				'name'        => $mapName["name"],
				'map_type_id' => $mapTypeId
			];

			$city = $product->provider ? $product->provider->city : $product->city;
			if ($city)
			{
				$cardData = $product->getSearchableCardInfo($city->url, $occasionUrl, $cardParams);

				if (count($cardData))
				{
					array_push($searchResults, $cardData);
				}
				else
				{
					$logData["errorMessage"] = "Unable to generate the url";

					$this->dispatch(new LogSearchInvalidOptionsJob($logData));
				}
			}
			else
			{
				$logData["errorMessage"] = "No city assigned for the product/partner.";

				$this->dispatch(new LogSearchInvalidOptionsJob($logData));
			}
		}

		// seo
		$pageTitle = "Showing search results for $searchQuery";
		$pageDesc = "List of results matching your search query $searchQuery. See real pictures, reviews, pricing and complete inclusions. Plan your party hassle free. Best deals. Evibe.in guarantee.";

		$allCategoryFilters = config("search.category.filters");
		$applicableCategory = [];
		foreach ($applicableCategoryIds as $categoryId)
		{
			$applicableCategory[$categoryId] = $allCategoryFilters[$categoryId];
		}

		$data = [
			'resultCount'          => 0,
			'query'                => $searchQuery,
			'results'              => $searchResults,
			'searchProductResults' => $searchProductResults,
			'cityId'               => $cityId,
			'filters'              => [
				'active'      => $filters['category'] ? $filters['category'] : null,
				'priceMin'    => $filters['priceMin'] ? $filters['priceMin'] : null,
				'priceMax'    => $filters['priceMax'] ? $filters['priceMax'] : null,
				'categories'  => $applicableCategory,
				'queryParams' => $this->getExistingQueryParams(),
				'clearFilter' => $clearFilter
			],
			'seo'                  => [
				'pageTitle'       => $pageTitle,
				'pageDescription' => $pageDesc
			]
		];

		return view('search/list', ['data' => $data]);
	}

	public function getSearchResultsDBSearch($cityUrl)
	{
		$validUrlObj = $this->validateUrlParams($cityUrl);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$cityId = getCityId();
		$occasionUrl = $this->getOccasionUrl() ?: null;
		$searchQuery = strtolower(trim(request('q')));
		$searchResults = [];
		$clearFilter = false;
		$filters = [
			'category' => request('category', null),
			'priceMin' => request('price_min', null),
			'priceMax' => request('price_max', null),
			'price'    => request('price', null)
		];

		$searchQuery = preg_replace('/[^A-Za-z0-9\- ]/', '', str_replace('"', "'", $searchQuery));

		// log queries
		$logData = [
			'cityId'         => $cityId,
			'eventId'        => $this->getOccasionId(),
			'keyword'        => $searchQuery,
			'pageUrl'        => request()->url(),
			'isGlobalSearch' => 1
		];
		$this->dispatch(new LogGlobalSearchQueryJob($logData));

		// Reset Filters
		if ($filters["price"] || $filters["category"])
		{
			$clearFilter = true;
		}

		// orWhere & complex where queries not available for Scout hence using collection
		$searchProductResults = SearchableProduct::where("city_id", $cityId)
		                                         ->where(function ($dbquery) use ($searchQuery) {
			                                         $dbquery->where('name', 'LIKE', "%" . $searchQuery . "%")
			                                                 ->orWhere('code', 'LIKE', $searchQuery);
		                                         })
		                                         ->where('option_sub_type_id', "!=", config('evibe.ticket.type.venue-deals')); // remove venue deals

		$applicableCategoryIds = array_unique($searchProductResults->pluck("option_sub_type_id")->toArray());

		// Category Filter
		if ($filters["category"] && is_numeric($filters["category"]) && ($filters["category"] > 0))
		{
			$searchProductResults = $searchProductResults->where("option_sub_type_id", $filters["category"]);
		}

		// filter by price
		if ($filters["price"] && !is_null($filters["price"]))
		{
			if ($filters["price"] == "asc")
			{
				$searchProductResults->orderBy("price_min");
			}
			elseif ($filters["price"] == "desc")
			{
				$searchProductResults->orderByDesc("price_min");
			}
		}

		$searchProductResults = $searchProductResults->paginate(48);

		$distinctOptionTypeIds = array_unique($searchProductResults->pluck("option_type_id")->toArray());
		$optionTypeProductsBank = []; // @see: to reduce queries

		foreach ($distinctOptionTypeIds as $distinctOptionTypeId)
		{
			$filteredOptionProducts = $searchProductResults->filter(function ($product) use ($distinctOptionTypeId) {
				if ($product["option_type_id"] == $distinctOptionTypeId)
				{
					return true;
				}

				return false;
			});

			$optionIds = $filteredOptionProducts->pluck("option_id");
			$optionTypeProducts = $this->getProductsByOptionType($distinctOptionTypeId, $optionIds);

			// @see: do not iterate here to preserve relevance score
			foreach ($optionTypeProducts as $optionTypeProduct)
			{
				$key = $distinctOptionTypeId . "_" . $optionTypeProduct->id;
				$optionTypeProductsBank[$key] = $optionTypeProduct;
			}
		}

		foreach ($searchProductResults as $searchProduct)
		{
			$optionTypeId = $searchProduct["option_type_id"];
			$optionSubTypeId = $searchProduct["option_sub_type_id"];
			$optionId = $searchProduct["option_id"];
			$key = $optionTypeId . "_" . $optionId;

			$logData = [
				"productTypeId"    => $optionTypeId,
				"productSubtypeId" => $optionSubTypeId,
				"productId"        => $optionId,
				"errorMessage"     => "Product not found"
			];

			if (!isset($optionTypeProductsBank[$key]))
			{
				$this->dispatch(new LogSearchInvalidOptionsJob($logData));

				continue;
			}

			$product = $optionTypeProductsBank[$key];
			$mapTypeId = $optionSubTypeId; // made by default
			$mapName = $this->getTypeNameById($mapTypeId);
			$cardParams = [
				'name'        => $mapName["name"],
				'map_type_id' => $mapTypeId
			];

			$city = $product->provider ? $product->provider->city : $product->city;
			if ($city)
			{
				$cardData = $product->getSearchableCardInfo($city->url, $occasionUrl, $cardParams);

				if (count($cardData))
				{
					array_push($searchResults, $cardData);
				}
				else
				{
					$logData["errorMessage"] = "Unable to generate the url";

					$this->dispatch(new LogSearchInvalidOptionsJob($logData));
				}
			}
			else
			{
				$logData["errorMessage"] = "No city assigned for the product/partner.";

				$this->dispatch(new LogSearchInvalidOptionsJob($logData));
			}
		}

		// seo
		$pageTitle = "Showing search results for $searchQuery";
		$pageDesc = "List of results matching your search query $searchQuery. See real pictures, reviews, pricing and complete inclusions. Plan your party hassle free. Best deals. Evibe.in guarantee.";

		$allCategoryFilters = config("search.category.filters");
		$applicableCategory = [];
		foreach ($applicableCategoryIds as $categoryId)
		{
			$applicableCategory[$categoryId] = $allCategoryFilters[$categoryId];
		}

		$data = [
			'resultCount'          => 0,
			'query'                => $searchQuery,
			'results'              => $searchResults,
			'searchProductResults' => $searchProductResults,
			'cityId'               => $cityId,
			'filters'              => [
				'active'      => $filters['category'] ? $filters['category'] : null,
				'priceMin'    => $filters['priceMin'] ? $filters['priceMin'] : null,
				'priceMax'    => $filters['priceMax'] ? $filters['priceMax'] : null,
				'categories'  => $applicableCategory,
				'queryParams' => $this->getExistingQueryParams(),
				'clearFilter' => $clearFilter
			],
			'seo'                  => [
				'pageTitle'       => $pageTitle,
				'pageDescription' => $pageDesc
			]
		];

		return view('search/list', ['data' => $data]);
	}

	private function sortArrayPriceAsc($a, $b)
	{
		if (isset($a["price_min"]) && isset($b["price_min"]))
		{
			if ($a["price_min"] == $b["price_min"])
			{
				return 0;
			}

			return ($a["price_min"] < $b["price_min"]) ? -1 : 1;
		}

		return 0;
	}

	private function sortArrayPriceDesc($a, $b)
	{
		if (isset($a["price_min"]) && isset($b["price_min"]))
		{
			if ($a["price_min"] == $b["price_min"])
			{
				return 0;
			}

			return ($a["price_min"] < $b["price_min"]) ? 1 : -1;
		}

		return 0;
	}

	private function getProductsByOptionType($optionTypeId, $optionIds)
	{
		$products = null;

		switch ($optionTypeId)
		{
			case config("evibe.ticket.type.package"):
				$products = Package::with("provider", "provider.city", "event")
				                   ->whereIn("id", $optionIds);
				break;

			case config("evibe.ticket.type.cake"):
				$products = Cake::with("provider", "provider.city")
				                ->whereIn("id", $optionIds);
				break;

			case config("evibe.ticket.type.trend"):
				$products = Trend::with("provider", "provider.city")
				                 ->whereIn("id", $optionIds);
				break;

			case config("evibe.ticket.type.decor"):
				$products = Decor::with("provider", "provider.city")
				                 ->whereIn("id", $optionIds);
				break;

			case config("evibe.ticket.type.service"):
				$products = TypeServices::with("event", "city")
				                        ->whereIn("id", $optionIds);
				break;
		}

		if (!is_null($products))
		{
			$products = $products->where("is_live", 1)
			                     ->forSearchSelectCols()
			                     ->get();
		}

		return $products;
	}

	private function getTypeNameById($typeId)
	{
		// @todo: get names from `type_ticket` table
		$names = [
			config("evibe.ticket.type.package")       => [
				"name"        => "birthday-packages",
				"displayName" => "Birthday Package"
			],
			config("evibe.ticket.type.cake")          => [
				"name"        => "cake",
				"displayName" => "Cake"
			],
			config('evibe.ticket.type.decor')         => [
				"name"        => "decor",
				"displayName" => "Decor"
			],
			config('evibe.ticket.type.trend')         => [
				"name"        => "trend",
				"displayName" => "Trend"
			],
			config('evibe.ticket.type.service')       => [
				"name"        => "entertainment",
				"displayName" => "Entertainment"
			],
			config('evibe.ticket.type.entertainment') => [
				"name"        => "entertainment",
				"displayName" => "Entertainment"
			],
			config('evibe.ticket.type.venue-deals')   => [
				"name"        => "venue-deals",
				"displayName" => "Venue Deals"
			],
			config('evibe.ticket.type.resorts')       => [
				"name"        => "resorts",
				"displayName" => "Resorts"
			],
			config('evibe.ticket.type.villas')        => [
				"name"        => "villas",
				"displayName" => "Villas"
			],
			config('evibe.ticket.type.lounges')       => [
				"name"        => "lounges",
				"displayName" => "Lounges"
			],
			config('evibe.ticket.type.food')          => [
				"name"        => "food",
				"displayName" => "Food"
			],
			config('evibe.ticket.type.surprises')     => [
				"name"        => "surprises",
				"displayName" => "Surprises"
			],
			config('evibe.ticket.type.tents')         => [
				"name"        => "tent",
				"displayName" => "Tent"
			],
			config('evibe.ticket.type.priests')       => [
				"name"        => "priest",
				"displayName" => "Priest"
			]
		];

		return (isset($names[$typeId])) ? $names[$typeId] : ["name" => "Category", "displayName" => "Category"];
	}

	public function getDefaultDataForAutoHint($cityId, $occasionId, $pageTypeId)
	{
		$identifier = $base = "search.auto_hint.";

		foreach ([$cityId, $occasionId, $pageTypeId] as $key)
		{
			if (!is_null($key))
			{
				$identifier .= "$key."; // $key can be false
			}
		}

		$identifier = substr($identifier, 0, strlen($identifier) - 1); // remove last "."

		if (config($identifier))
		{
			$searchProductResults = config($identifier);
		}
		elseif (config($base . $cityId . "." . $occasionId . ".0"))
		{
			$searchProductResults = config($base . $cityId . "." . $occasionId . ".0");
		}
		elseif (config($base . $cityId . ".default"))
		{
			$searchProductResults = config($base . $cityId . ".0");
		}
		else
		{
			$searchProductResults = config($base . "0");
		}

		$count = 0;
		$productResults = [];
		foreach ($searchProductResults as $key => $productResult)
		{
			$count++;
			$productResults[] = [
				"name"       => $productResult,
				"is_default" => "yes",
				"id_str"     => $cityId . "_" . $occasionId . "_" . $pageTypeId . "_" . $count
			];
		}

		return response()->json($productResults);
	}

	public function getJsonDataForAutoHint($cityId, $query)
	{
		// Hiding this since we are stopping elasticsearch
		return response()->json([]);

		$validOccasionIds = ["Kids Events", "House Warming", "Surprises"];
		$esPageName = 'esPageNumber'; // different pagination params for the list
		$maxResults = 10000; // this is max allowed by Elasticsearch

		$query = preg_replace('/[^A-Za-z0-9\- ]/', '', str_replace('"', "'", $query));

		$searchProductResults = SearchableProduct::search($query)
		                                         ->where("city_id", $cityId);

		$searchProductResults = $searchProductResults->paginate($maxResults, $esPageName);
		$searchProductResults = collect($searchProductResults->items())->toArray();
		$searchProductResults = array_filter($searchProductResults, function ($product) use ($validOccasionIds) {
			return (in_array($product["occasion_name"], $validOccasionIds));
		});
		$searchProductResults = array_slice($searchProductResults, 0, 10);

		$productResults = [];
		foreach ($searchProductResults as $productResult)
		{
			array_push($productResults, [
				'id_str'        => $productResult["id"],
				'name'          => $productResult["name"],
				'code'          => $productResult["code"],
				'profile_image' => $productResult["profile_image"],
				'price_min'     => $productResult["price_min"],
				'price_max'     => $productResult["price_max"],
				'profile_url'   => $productResult["profile_url"] . "?utm_source=search-auto&utm_campaign=search&utm_medium=website&query=" . $productResult["name"]
			]);
		}

		return response()->json($productResults);
	}

	public function updateSearchableTable()
	{
		return response()->json([
			                        "success" => false
		                        ]);

		$token = request("accessToken");

		if (!$token || is_null($token))
		{
			return response()->json([
				                        "success" => false,
				                        "message" => "No access token found"
			                        ]);
		}

		if (!Hash::check('updateSearchableTable' . Carbon::today()->startOfDay()->timestamp, $token))
		{
			return response()->json([
				                        "success" => false,
				                        "message" => "Token Mismatch"
			                        ]);
		}

		$occasionUrl = $this->getOccasionUrl() ?: null;

		$searchProductResults = SearchableProduct::all();

		$distinctOptionTypeIds = array_unique($searchProductResults->pluck("option_type_id")->toArray());
		$optionTypeProductsBank = [];
		$searchResults = [];

		foreach ($distinctOptionTypeIds as $distinctOptionTypeId)
		{
			$filteredOptionProducts = $searchProductResults->filter(function ($product) use ($distinctOptionTypeId) {
				if ($product["option_type_id"] == $distinctOptionTypeId)
				{
					return true;
				}

				return false;
			});

			$optionIds = $filteredOptionProducts->pluck("option_id");
			$optionTypeProducts = $this->getProductsByOptionType($distinctOptionTypeId, $optionIds);

			// @see: do not iterate here to preserve relevance score
			foreach ($optionTypeProducts as $optionTypeProduct)
			{
				$key = $distinctOptionTypeId . "_" . $optionTypeProduct->id;
				$optionTypeProductsBank[$key] = $optionTypeProduct;
			}
		}

		foreach ($searchProductResults as $searchProduct)
		{
			$optionTypeId = $searchProduct["option_type_id"];
			$optionSubTypeId = $searchProduct["option_sub_type_id"];
			$optionId = $searchProduct["option_id"];
			$key = $optionTypeId . "_" . $optionId;

			if (!isset($optionTypeProductsBank[$key]))
			{
				continue;
			}

			$product = $optionTypeProductsBank[$key];
			$mapTypeId = $optionSubTypeId; // made by default
			$mapName = $this->getTypeNameById($mapTypeId);
			$cardParams = [
				'name'        => $mapName["name"],
				'map_type_id' => $mapTypeId
			];

			$city = $product->provider ? $product->provider->city : $product->city;
			if ($city)
			{
				$cardData = $product->getSearchableCardInfo($city->url, $occasionUrl, $cardParams);

				if (count($cardData) && isset($cardData["fullUrl"]))
				{
					if (!$cardData["profileImg"] || !$cardData["fullUrl"])
					{
						Log::info("profile Img or URL not found for the option: " . $optionTypeId . "_" . $optionSubTypeId . "_" . $optionId);
					}

					$searchResults[$searchProduct["id"]] = [
						"profileImg" => $cardData["profileImg"],
						"fullUrl"    => $cardData["fullUrl"]
					];
				}
			}
		}

		foreach (collect($searchResults)->chunk(100) as $chunk)
		{
			$this->dispatch(new UpdateGlobalSearchData($chunk));
		}

		return response()->json([
			                        "success" => true,
			                        "message" => "data successfully updated"
		                        ]);
	}
}
