<?php

namespace App\Http\Controllers\PartyBag;

use App\Http\Controllers\Util\TicketsController;
use App\Jobs\Emails\Errors\MailTransportChargesErrorToTeamJob;
use App\Jobs\Emails\ReportIssue\MissingCityAlertToTeam;
use App\Models\Cake\Cake;
use App\Models\Decor\Decor;
use App\Models\Package\Package;
use App\Models\PartyBag\Shortlist;
use App\Models\PartyBag\ShortlistOptionFieldValue;
use App\Models\PartyBag\ShortlistOptions;

use App\Events\AddShortlistOptionEvent;
use App\Events\DeleteShortlistOptionEvent;

use App\Http\Controllers\Base\BaseListableController;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketBooking;
use App\Models\Ticket\TicketBookingGallery;
use App\Models\Ticket\TicketMapping;
use App\Models\Types\TypeEvent;
use App\Models\Types\TypeServices;
use App\Models\Types\TypeTicket;
use App\Models\Util\CheckoutFieldValue;
use App\Models\Util\City;
use App\Models\Util\DeliverySlot;
use App\Models\Util\SiteErrorLog;
use App\Models\Vendor\Vendor;
use Carbon\Carbon;
use Evibe\Passers\PriceCheck;
use Evibe\Passers\Util\SiteLogData;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Evibe\Facades\EvibeUtilFacade as EvibeUtil;

class PartyBagController extends BaseListableController
{
	public $shortListableMappings;

	public function __construct()
	{
		parent::__construct();

		$this->shortListableMappings = $this->listableMappings;
	}

	// add option to  party bag
	public function shortlistOption()
	{
		$res = ['success' => false];
		if (auth()->check())
		{
			$shortlistData = [
				'cityId'          => request('city_id'),
				'occasionId'      => request('occasion_id'),
				'mapId'           => request('map_id'),
				'mapTypeId'       => request('map_type_id'),
				'partyDate'       => request('party_date'),
				'partyPinCode'    => request('party_pin_code'),
				'price'           => request('price'),
				'priceWorth'      => request('price_worth'),
				'productPrice'    => request('product_price'),
				'transportAmount' => request('transport_amount'),
				'advanceAmount'   => request('advance_amount'),
				'categories'      => request('categories'),
				'deliverySlotId'  => request('deliverySlotId'),
			];

			try
			{
				$shortlistOption = $this->shortlistOptionToPartyBag($shortlistData);

				if ($shortlistOption)
				{
					$res['success'] = true;
					$res['shortlistOption'] = $this->getShortlistCardData($shortlistOption);
					$res['budget'] = $this->getPartyBagBudget(["shortlistId" => $shortlistOption->shortlist_id]);
				}

				return response()->json($res);
			} catch (\Exception $exception)
			{
				$trace = "shortlistData: " . print_r($shortlistData, true) .
					"trace: " . $exception->getTraceAsString();

				$siteLogData = new SiteLogData();
				$siteLogData->setTrace($trace)
				            ->setUrl(route("partybag.add"))
				            ->setErrorCode($exception->getCode())
				            ->setException("ShortlistOptionException");

				$this->logSiteError($siteLogData);
			}
		}
		else
		{
			Log::error("User not logged in for shortlisting option");
		}

		return response()->json($res);
	}

	public function wishlistOption()
	{
		$res = ['success' => false];

		if (auth()->check())
		{
			$shortlistData = [
				'cityId'     => request('cityId'),
				'occasionId' => request('occasionId'),
				'mapId'      => request('productId'),
				'mapTypeId'  => request('productTypeId')
			];

			try
			{
				$userId = auth()->user()->id;

				// shortlist for the user with userId
				// party_date is null or party_date >= current_date (Carbon::now())
				$startOfTheDay = Carbon::createFromTimestamp(time())->startOfDay()->toDayDateTimeString();
				$shortlist = Shortlist::where('user_id', $userId)
				                      ->where(function ($query) use ($startOfTheDay) {
					                      $query->whereNull('party_date')
					                            ->orWhere('party_date', '>=', strtotime($startOfTheDay));
				                      })
				                      ->first();

				if (!$shortlist)
				{
					$shortlist = Shortlist::create([
						                               'user_id'    => $userId,
						                               'created_at' => Carbon::now(),
						                               'updated_at' => Carbon::now()
					                               ]);
				}

				$shortlist->event_id = (isset($shortlistData['occasionId']) && $shortlistData['occasionId']) ? $shortlistData['occasionId'] : $shortlist->event_id;
				$shortlist->city_id = (isset($shortlistData['cityId']) && $shortlistData['cityId']) ? $shortlistData['cityId'] : $shortlist->city_id;
				$shortlist->updated_at = Carbon::now();
				$shortlist->save();

				$checkModal = [
					'shortlist_id'     => $shortlist->id,
					'city_id'          => $shortlistData['cityId'],
					'occasion_id'      => $shortlistData['occasionId'],
					'map_id'           => $shortlistData['mapId'],
					'map_type_id'      => $shortlistData['mapTypeId'],
					'is_wishlist'      => 1,
					'booking_price'    => (isset($shortlistData['price']) && $shortlistData['price']) ? $shortlistData['price'] : null,
					'product_price'    => (isset($shortlistData['productPrice']) && $shortlistData['productPrice']) ? $shortlistData['productPrice'] : null,
					'transport_amount' => (isset($shortlistData['transportAmount']) && $shortlistData['transportAmount']) ? $shortlistData['transportAmount'] : null,
					'advance_amount'   => (isset($shortlistData['advanceAmount']) && $shortlistData['advanceAmount']) ? $shortlistData['advanceAmount'] : ((isset($shortlistData['price']) && $shortlistData['price']) ? $shortlistData['price'] : null)
				];

				$shortlistOption = ShortlistOptions::firstOrNew($checkModal);
				$shortlistOption->updated_at = Carbon::now(); // update timestamp whether new / old one
				$shortlistOption->save();
				if ($shortlistOption)
				{
					$res['success'] = true;
				}

				return response()->json($res);
			} catch (\Exception $exception)
			{
				$trace = "shortlistData: " . print_r($shortlistData, true) .
					"trace: " . $exception->getTraceAsString();

				$siteLogData = new SiteLogData();
				$siteLogData->setTrace($trace)
				            ->setUrl(route("partybag.add"))
				            ->setErrorCode($exception->getCode())
				            ->setException("ShortlistOptionException");

				$this->logSiteError($siteLogData);
			}
		}
		else
		{
			Log::error("User not logged in for shortlisting option");
		}

		return response()->json($res);
	}

	public function wishlistOptionStatus()
	{
		$res = ['success' => false];

		if (auth()->check())
		{
			$shortlistData = [
				'cityId'     => request('cityId'),
				'occasionId' => request('occasionId'),
				'mapId'      => request('productId'),
				'mapTypeId'  => request('productTypeId')
			];

			$userId = auth()->user()->id;
			$shortlistIds = Shortlist::where("user_id", $userId)
			                         ->pluck("id")
			                         ->toArray();

			if (count($shortlistIds) > 0)
			{
				$shortlistOption = ShortlistOptions::whereIn("shortlist_id", $shortlistIds)
				                                   ->where('city_id', $shortlistData['cityId'])
				                                   ->where('occasion_id', $shortlistData['occasionId'])
				                                   ->where('map_id', $shortlistData['mapId'])
				                                   ->where('map_type_id', $shortlistData['mapTypeId'])
				                                   ->where('is_wishlist', 1)
				                                   ->first();

				if ($shortlistOption)
				{
					$res['success'] = true;
				}
			}

			return response()->json($res);
		}
		else
		{
			$res = ['success' => false, 'loginFail' => true];
		}

		return response()->json($res);
	}

	public function getWishListOptions()
	{
		$shortlistData = [
			'uncategorizedData' => [],
			'categorizedData'   => []
		];

		if (auth()->check())
		{
			$user = auth()->user();

			$wishListUserData = Shortlist::where('user_id', $user->id)
			                             ->pluck("id")
			                             ->toArray();

			if (count($wishListUserData) > 0)
			{
				$unCategorizedData = [];
				$categorizedData = [];

				$shortlistOptions = ShortlistOptions::whereIn('shortlist_id', $wishListUserData)
				                                    ->where('is_wishlist', "1")
				                                    ->get();

				$allOptions = $shortlistOptions;
				$shortlistOptions = $shortlistOptions->sortByDesc('created_at');

				// get the card details of the short list options
				foreach ($shortlistOptions as $shortlistOption)
				{
					$cardData = $this->getShortlistCardData($shortlistOption);
					if (is_null($cardData))
					{
						continue;
					}

					$mapTypeId = $cardData['mapTypeId'];
					if (!isset($categorizedData[$mapTypeId]))
					{
						$categorizedData[$mapTypeId] = [];
					}

					array_push($unCategorizedData, $cardData);
					array_push($categorizedData[$mapTypeId], $cardData);
				}

				$shortlistData = [
					"allOptions"        => $allOptions,
					'categorizedData'   => $categorizedData,
					'uncategorizedData' => $unCategorizedData
				];
			}
		}

		$data = [
			'results' => $shortlistData['uncategorizedData'],
			'seo'     => [
				'pageTitle'       => 'Wishlist',
				'pageDescription' => ''
			]
		];

		if (getCityId() == -1)
		{
			return redirect('/');
		}
		else
		{
			return view('party-bag.wish-list', ['data' => $data]);
		}
	}

	// temporarily add shortlist option data to the session
	public function tempShortlistOption()
	{
		$res = ['success' => false];

		// check if the user is not authenticated
		if (!auth()->check())
		{
			if (request('city_id') &&
				request('occasion_id') &&
				request('map_type_id') &&
				request('map_id')
			)
			{
				$tempShortlistData = [
					'cityId'     => request('city_id'),
					'occasionId' => request('occasion_id'),
					'mapTypeId'  => request('map_type_id'),
					'mapId'      => request('map_id'),
					'delDate'    => request('delivery_slot'),
				];

				session(['in.evibe.temp_shortlist_data' => $tempShortlistData]);
				$res = ['success' => true];
			}
		}
		else
		{
			Log::error("User not logged in for shortlisting option");
		}

		return response()->json($res);

	}

	// show extra info required to shortlist an item
	public function showPreShortlistModal()
	{
		try
		{

			$partyDate = null;
			$partyPinCode = null;

			if (auth()->check())
			{
				$userId = auth()->user()->id;
				$shortlist = $this->getShortlistFromUserId($userId);

				if ($shortlist)
				{
					$partyDate = $shortlist->party_date ? date('Y/m/d', $shortlist->party_date) : $partyDate;
					$partyPinCode = $shortlist->party_pin_code ?: $partyPinCode;
				}
			}

			$productId = request('productId');
			$productTypeId = request('productTypeId');

			$fieldsAvailable = false;
			$name = null;
			$price = null;
			$priceWorth = null;
			$profileImage = null;
			$checkoutFields = null;
			$deliverySlots = DeliverySlot::all(); // previously declared only in cakes switch case
			$advancePercent = 100;

			if ($productId && $productTypeId)
			{
				switch ($productTypeId)
				{
					case config('evibe.ticket.type.cake'):
						$product = Cake::where('id', $productId)
						               ->whereNull('deleted_at')
						               ->where('is_live', 1)
						               ->first();

						if (!$product)
						{
							Log::error("Unable to find product for - productId: $productId; productTypeId: $productTypeId");
							break;
						}

						$name = $product->title;
						$price = $product->price;
						$priceWorth = $product->price_worth;
						$profileImage = $product->getProfileImg();
						$checkoutFields = $this->getDynamicCakeFieldsWithCategory($productId);
						//$deliverySlots = DeliverySlot::all();
						$advancePercent = 100;
						break;

					case config('evibe.ticket.type.decor'):
						$product = Decor::where('id', $productId)
						                ->whereNull('deleted_at')
						                ->where('is_live', 1)
						                ->first();

						if (!$product)
						{
							Log::error("Unable to find product for - productId: $productId; productTypeId: $productTypeId");
							break;
						}

						$name = $product->name;
						$price = $product->min_price;
						$priceWorth = $product->worth;
						$profileImage = $product->getProfileImg();
						//$checkoutFields = $this->getDynamicDecorFieldsWithCategory($productId);
						$advancePercent = 50;
						break;

					case config('evibe.ticket.type.package'):
					case config('evibe.ticket.type.food'):
					case config('evibe.ticket.type.priests'):
					case config('evibe.ticket.type.tents'):
						$product = Package::where('id', $productId)
						                  ->where('type_ticket_id', $productTypeId)
						                  ->whereNull('deleted_at')
						                  ->where('is_live', 1)
						                  ->first();

						if (!$product)
						{
							Log::error("Unable to find product for - productId: $productId; productTypeId: $productTypeId");
							break;
						}

						$name = $product->name;
						$price = $product->price;
						$priceWorth = $product->price_worth;
						$profileImage = $product->getProfileImg();
						// todo: guest count field
						//$checkoutFields = $this->getDynamicDecorFieldsWithCategory($productId);
						$advancePercent = 50;
						break;

					case config('evibe.ticket.type.entertainment'):
					case config('evibe.ticket.type.service'):
						$product = TypeServices::where('id', $productId)
						                       ->whereNull('deleted_at')
						                       ->where('is_live', 1)
						                       ->first();

						if (!$product)
						{
							Log::error("Unable to find product for - productId: $productId; productTypeId: $productTypeId");
							break;
						}

						$name = $product->name;
						$price = $product->min_price;
						$priceWorth = $product->worth_price;
						$profileImage = $product->getProfilePic();
						// todo: guest count field
						//$checkoutFields = $this->getDynamicDecorFieldsWithCategory($productId);
						$advancePercent = 50;
						break;

					default:
						break;
				}

				$productData = [
					'productId'      => $productId,
					'productTypeId'  => $productTypeId,
					'partyDate'      => $partyDate,
					'partyPinCode'   => $partyPinCode,
					'name'           => $name,
					'price'          => $price,
					'priceWorth'     => $priceWorth,
					'profileImage'   => $profileImage,
					'advancePercent' => $advancePercent,
					'checkoutFields' => $checkoutFields,
					'deliverySlots'  => $deliverySlots,
					'defaultImage'   => config("evibe.gallery.host") . "/img/icons/balloons.png"
				];

				return view('app.modals.shortlist-modal', ['data' => $productData]);
			}
			else
			{
				return "";
			}

		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);

			return response()->json(['error' => true]);
		}
	}

	public function calculateTransport()
	{
		try
		{

			$partyPinCode = request('partyPinCode');
			$productId = request('productId');
			$productTypeId = request('productTypeId');

			if (strlen($partyPinCode) != 6)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Kindly enter a valid pin code'
				                        ]);
			}

			$transportAmount = 0;
			$feasibility = true;

			if ($productId && $productTypeId && ($productTypeId == config('evibe.ticket.type.decor')))
			{
				$decor = Decor::find($productId);
				if ($decor && $decor->provider_id)
				{
					$provider = Vendor::find($decor->provider_id);
					if ($provider && $provider->zip)
					{
						$dataForPriceCheck = new PriceCheck();
						$dataForPriceCheck->setOptionId($productId);
						$dataForPriceCheck->setOptionTypeId($productTypeId);
						$dataForPriceCheck->setPartyPinCode($partyPinCode);
						$dataForPriceCheck->setProviderPinCode($provider->zip);
						$dataForPriceCheck->setDistFree(config('evibe.transport.free-kms'));

						$transPriceData = $this->getUpdatedPriceWithTransportationCharges($dataForPriceCheck);

						$transportAmount = isset($transPriceData['transCharges']) ? $transPriceData['transCharges'] : $transportAmount;
						if (isset($transPriceData['distMaxMeters']) && isset($transPriceData['partyDist']) && ($transPriceData['distMaxMeters'] < $transPriceData['partyDist']))
						{
							$feasibility = false;
						}
					}
				}
			}

			return response()->json([
				                        'success'         => true,
				                        'transportAmount' => $transportAmount,
				                        'feasibility'     => $feasibility
			                        ]);

		} catch (\Exception $exception)
		{
			Log::error('error message: ' . $exception->getMessage());

			return response()->json([
				                        'success' => false,
				                        'error'   => $exception->getMessage()
			                        ]);
		}
	}

	// submit extra info required to shortlist an option
	public function submitPreShortlistModal()
	{
		try
		{

			$productId = request('productId');
			$productTypeId = request('productTypeId');
			$cityId = request('cityId');
			$occasionId = request('occasionId');
			$partyDate = request('partyDate') ? strtotime(request('partyDate')) : null;
			$partyPinCode = request('partyPinCode') ? request('partyPinCode') : null;
			//return response()->json([
			//	                        'success' => false,
			//                        ]);

			if (auth()->check())
			{
				$user = auth()->user();
				$shortlist = $this->getShortlistFromUserId($user->id);
				if ($shortlist)
				{
					$shortlistOptions = ShortlistOptions::where('shortlist_id', $shortlist->id)->whereNull('is_wishlist')->get();
					if (count($shortlistOptions))
					{
						// check for conditions
						if ($shortlist->city_id && $cityId && ($shortlist->city_id != $cityId))
						{
							return response()->json([
								                        'success'     => false,
								                        'falseCity'   => true,
								                        'newCityName' => $this->getCityNameFromId($cityId),
								                        'oldCityName' => $this->getCityNameFromId($shortlist->city_id)
							                        ]);
						}

						if ($shortlist->event_id && $occasionId && ($shortlist->event_id != $occasionId))
						{
							return response()->json([
								                        'success'         => false,
								                        'falseEvent'      => true,
								                        'newOccasionName' => $this->getOccasionNameFromId($occasionId),
								                        'oldOccasionName' => $this->getOccasionNameFromId($shortlist->event_id)
							                        ]);
						}

						// change in party date
						if ($shortlist->party_date && $partyDate && ($shortlist->party_date != $partyDate))
						{
							return response()->json([
								                        'success'        => false,
								                        'falsePartyDate' => true,
								                        'newPartyDate'   => date('Y/m/d', $partyDate),
								                        'oldPartyDate'   => date('Y/m/d', $shortlist->party_date)
							                        ]);
						}

						// change in party pin code
						if ($shortlist->party_pin_code && $partyPinCode && ($shortlist->party_pin_code != $partyPinCode))
						{
							return response()->json([
								                        'success'           => false,
								                        'falsePartyPinCode' => true,
								                        'newPartyPinCode'   => $partyPinCode,
								                        'oldPartyPinCode'   => $shortlist->party_pin_code
							                        ]);
						}

						return response()->json([
							                        'success' => true
						                        ]);
					}
					else
					{
						// no options in th shortlist
						return response()->json([
							                        'success' => true
						                        ]);
					}
				}
				else
				{
					// first time creating a shortlist
					return response()->json([
						                        'success' => true
					                        ]);
				}
			}
			else
			{
				Log::error("User is not logged in");

				return response()->json([
					                        'success' => false,
				                        ]);
			}

		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);

			return response()->json(['error' => true]);
		}
	}

	// clear all the options in the party bag
	public function clearPartyBag()
	{
		try
		{
			if (auth()->check())
			{
				$user = auth()->user();
				$shortlist = $this->getShortlistFromUserId($user->id);
				if ($shortlist)
				{
					$productId = request('productId');
					$productTypeId = request('productTypeId');
					$eventId = request('occasionId');
					$cityId = request('cityId');

					// update shortlist (just in case)
					// clear the shortlist options
					if ($eventId)
					{
						$shortlist->event_id = $eventId;
					}

					if ($cityId)
					{
						$shortlist->city_id = $cityId;
					}

					$shortlist->updated_at = Carbon::now();
					$shortlist->save();

					$shortlistOptions = ShortlistOptions::where('shortlist_id', $shortlist->id)->whereNull('is_wishlist')->get();
					if (count($shortlistOptions))
					{
						foreach ($shortlistOptions as $option)
						{
							if ($productId && $productTypeId && $option->map_id && $option->map_type_id &&
								($productId == $option->map_id) &&
								($productTypeId == $option->map_type_id))
							{
								// do not remove
								// normally doesn't happen, but might happen because of Ajax request
							}
							else
							{
								$option->deleted_at = Carbon::now();
							}
							$option->updated_at = Carbon::now();
							$option->save();
						}

						// Removed shortlist options
						return response()->json([
							                        'success' => true,
						                        ]);
					}
					else
					{
						// There is are no shortlist options currently for this user
						return response()->json([
							                        'success' => true,
						                        ]);
					}
				}
				else
				{
					// There is no shortlist associated to this user
					// Will be created when the option is added in another request
					return response()->json([
						                        'success' => true,
					                        ]);
				}
			}
			else
			{
				Log::error("User is not logged in");

				return response()->json([
					                        'success' => false,
				                        ]);
			}

		} catch (\Exception $exception)
		{
			Log::error("Error :" . $exception->getMessage());

			return response()->response([
				                            'success' => false,
				                            'error'   => $exception->getMessage()
			                            ]);
		}
	}

	// remove option from party bag
	public function undoShortlistOption()
	{
		$res = ['success' => false];

		if (auth()->check())
		{
			$user = auth()->user();
			$shortlist = $this->getShortlistFromUserId($user->id);

			if ($shortlist)
			{
				$cityId = request('city_id');
				$occasionId = request('occasion_id');
				$mapId = request('map_id');
				$mapTypeId = request('map_type_id');

				$shortlistOption = ShortlistOptions::where('shortlist_id', $shortlist->id)
				                                   ->where('city_id', $cityId)
				                                   ->where('occasion_id', $occasionId)
				                                   ->where('map_id', $mapId)
				                                   ->where('map_type_id', $mapTypeId)
				                                   ->whereNull('is_wishlist');

				$shortlistOptionData = $shortlistOption->first();
				$shortlistOptionId = $shortlistOption->value('id');
				if ($shortlistOption->delete())
				{
					$res['success'] = true;
					$res['shortlistOption'] = $this->getShortlistCardData($shortlistOptionData);
					$res['budget'] = $this->getPartyBagBudget(["shortlistId" => $shortlist->id]);
					event(new DeleteShortlistOptionEvent($shortlistOptionId));
				}
			}
		}

		return response()->json($res);
	}

	// redirect to party bag page
	public function getPartyBag()
	{
		$budget = 0;
		$shortlistData = [
			'uncategorizedData' => [],
			'categorizedData'   => []
		];
		$data = [
			'seo' => [
				'pageTitle'       => 'Cart',
				'pageDescription' => 'Shortlist options and enquire at once'
			]
		];

		if (auth()->check())
		{
			$user = auth()->user();

			// fetch the shortlist that party_date as null
			// or party date greater than the current date ( time() )
			$shortlist = $this->getShortlistFromUserId($user->id);

			if ($shortlist)
			{
				$shortlistData = $this->getPartyBagData($shortlist->id);
			}

			$budget = $this->getPartyBagBudget($shortlistData["categorizedData"]);
			$partyDate = ($shortlist && $shortlist->party_date) ? date('Y/m/d', $shortlist->party_date) : '';
			$partyPinCode = ($shortlist && $shortlist->party_pin_code) ? $shortlist->party_pin_code : '';
			$cartProducts = [];

			foreach ($shortlistData['uncategorizedData'] as $product)
			{
				if (!($product['mapTypeId'] == config('evibe.ticket.type.cake')))
				{
					$product['advanceAmount'] = EvibeUtil::getRoundedTokenAmount($product['productPrice']);
				}
				$cartProducts['products'][] = $product;
			}

			$data = [
				'results' => $cartProducts,
				'budget'  => $budget,
				'seo'     => [
					'pageTitle'       => 'Cart',
					'pageDescription' => 'Shortlist options and enquire at once'
				],
				'others'  => [
					'partyDate'    => $partyDate,
					'partyPinCode' => $partyPinCode
				]
			];
		}

		return view('party-bag/list', ['data' => $data]);
	}

	// return party bag data in json format for vuejs
	public function getPartyBagJSONData()
	{
		$data = [];

		if (auth()->check())
		{
			$user = auth()->user();
			$shortlistOptions = [];
			$shortlistBudgetData = [];
			$shortlistOptionsData = [
				'uncategorizedData' => []
			];

			$shortlist = $this->getShortlistFromUserId($user->id);
			if ($shortlist)
			{
				$shortlistOptionsData = $this->getPartyBagData($shortlist->id);
				$shortlistOptions = isset($shortlistOptionsData["allOptions"]) ? $shortlistOptionsData["allOptions"] : collect([]);
				$shortlistBudgetData = $this->getPartyBagBudget($shortlistOptionsData["categorizedData"]);
			}

			$cityId = ($shortlist && $shortlist->city_id) ? $shortlist->city_id : null;
			$eventId = ($shortlist && $shortlist->event_id) ? $shortlist->event_id : null;
			$partyDate = ($shortlist && $shortlist->party_date) ? date('Y/m/d', $shortlist->party_date) : '';
			$partyPinCode = ($shortlist && $shortlist->party_pin_code) ? $shortlist->party_pin_code : '';

			$data = [
				'partybag'         => [
					'count'        => count($shortlistOptions),
					'list'         => $shortlistOptions,
					'data'         => $shortlistOptionsData['uncategorizedData'],
					'budget'       => $shortlistBudgetData,
					'cityId'       => $cityId,
					'occasionId'   => $eventId,
					'partyDate'    => $partyDate,
					'partyPinCode' => $partyPinCode
				],
				'productTypeCake'  => config('evibe.ticket.type.cake'),
				'productTypeDecor' => config('evibe.ticket.type.decor'),
			];
		}

		return response()->json($data);
	}

	// triggered by event listener for adding shortlist data after user login
	public function shortlistOptionToPartyBag($shortlistData)
	{
		if (auth()->check())
		{
			$userId = auth()->user()->id;
			$shortlist = $this->getShortlistFromUserId($userId);
			if (!$shortlist)
			{
				$shortlist = Shortlist::create([
					                               'user_id'    => $userId,
					                               'created_at' => Carbon::now(),
					                               'updated_at' => Carbon::now()
				                               ]);
			}

			$shortlist->event_id = (isset($shortlistData['occasionId']) && $shortlistData['occasionId']) ? $shortlistData['occasionId'] : $shortlist->event_id;
			$shortlist->city_id = (isset($shortlistData['cityId']) && $shortlistData['cityId']) ? $shortlistData['cityId'] : $shortlist->city_id;
			$shortlist->updated_at = Carbon::now();
			$shortlist->save();

			$checkModal = [
				'shortlist_id'     => $shortlist->id,
				'city_id'          => $shortlistData['cityId'],
				'occasion_id'      => $shortlistData['occasionId'],
				'map_id'           => $shortlistData['mapId'],
				'map_type_id'      => $shortlistData['mapTypeId'],
				'booking_price'    => (isset($shortlistData['price']) && $shortlistData['price']) ? $shortlistData['price'] : null,
				'product_price'    => (isset($shortlistData['productPrice']) && $shortlistData['productPrice']) ? $shortlistData['productPrice'] : null,
				'transport_amount' => (isset($shortlistData['transportAmount']) && $shortlistData['transportAmount']) ? $shortlistData['transportAmount'] : null,
				'advance_amount'   => (isset($shortlistData['advanceAmount']) && $shortlistData['advanceAmount']) ? $shortlistData['advanceAmount'] : ((isset($shortlistData['price']) && $shortlistData['price']) ? $shortlistData['price'] : null)
			];

			$shortlistOption = ShortlistOptions::firstOrNew($checkModal);
			$shortlistOption->updated_at = Carbon::now(); // update timestamp whether new / old one
			$shortlistOption->save();

			$checkoutFields = [];
			if (isset($shortlistData['categories']) && count($shortlistData['categories']))
			{
				foreach ($shortlistData['categories'] as $category)
				{
					if ($category['catId'] == config('evibe.cake_cat_value.weight'))
					{
						$checkoutFields[config('evibe.checkout_field.cake.weight')] = $category['name'];
					}
					elseif ($category['catId'] == config('evibe.cake_cat_value.eggless'))
					{
						$checkoutFields[config('evibe.checkout_field.cake.type')] = $category['name'];
					}
					elseif ($category['catId'] == config('evibe.cake_cat_value.flavour'))
					{
						$checkoutFields[config('evibe.checkout_field.cake.flavour')] = $category['name'];
					}

				}
			}

			if (isset($shortlistData['deliverySlotId']) && $shortlistData['deliverySlotId'])
			{
				$deliverySlot = DeliverySlot::find(trim($shortlistData['deliverySlotId']));
				if ($deliverySlot)
				{
					$formattedDeliverySlot = $deliverySlot->formattedSlot();
					$checkoutFields[config('evibe.checkout_field.cake.delivery_slot')] = $formattedDeliverySlot;
				}
			}

			if (count($checkoutFields))
			{
				foreach ($checkoutFields as $key => $value)
				{
					ShortlistOptionFieldValue::create([
						                                  'shortlist_option_id' => $shortlistOption->id,
						                                  'checkout_field_id'   => $key,
						                                  'value'               => $value,
						                                  'created_at'          => Carbon::now(),
						                                  'updated_at'          => Carbon::now(),
					                                  ]);
				}
			}

			event(new AddShortlistOptionEvent($shortlistOption->id));

			return $shortlistOption;
		}

		return false;
	}

	// enquire few options of the party bag at once
	public function enquirePartyBagOptions()
	{
		$shortListableMappings = $this->listableMappings;

		$resMes = [
			'success'  => false,
			'messages' => []
		];
		if (!auth()->check())
		{
			return response()->json($resMes);
		}

		// fetch the user id
		$user = auth()->user();

		// validate rules
		$rules = [
			'eventDate'     => 'required|date',
			'phone'         => 'required|phone',
			'partyLocation' => 'required',
		];

		$messages = [
			'eventDate.required'     => 'Please select your party date',
			'eventDate.date'         => 'Party date entered is not valid',
			'phone.required'         => 'Your 10 digit phone number is required',
			'phone.phone'            => 'Phone number is not valid (ex: 9640204000)',
			'partyLocation.required' => 'Please enter your party location',
		];

		$validator = Validator::make(request()->all(), $rules, $messages);

		if ($validator->fails())
		{
			$messages = $validator->messages();

			foreach ($messages->all() as $message)
			{
				array_push($resMes['messages'], $message);
			}

			return response()->json($resMes);
		}

		$eventDate = strtotime(str_replace('-', '/', request('eventDate')));
		$inputData = [
			'name'              => $user->name,
			'phone'             => request('phone'),
			'email'             => $user->username,
			'event_date'        => $eventDate,
			'type_ticket_id'    => config('evibe.ticket.type.party_bag'),
			'budget'            => request('budget'),
			'enquiry_source_id' => config('evibe.ticket.enquiry_source.pb_enquiry')
		];

		try
		{
			// get all the shortlist for the specific user
			$shortlist = $this->getShortlistFromUserId($user->id);
			if ($shortlist)
			{
				// @see: do not store event_date in shortlist table as the same can be booked (from party bag) by the customers after enq event_date also
				// get all the shortlist options
				// @see: removed so that customer can submit enquiry as many times a he/she can
				$shortlistOptions = ShortlistOptions::where('shortlist_id', $shortlist->id)
				                                    ->whereNull('is_wishlist')
					//->whereNull('enquired_date')
					                                ->get();

				// update the party date for this specific shortlist
				$shortlist->party_date = $eventDate;
				$shortlist->save();

				if (count($shortlistOptions))
				{
					$start = Carbon::createFromTimestamp($eventDate)->startOfDay()->timestamp;
					$end = Carbon::createFromTimestamp($eventDate)->endOfDay()->timestamp;

					// update the current ticket if ticket already created for the party date
					$ticket = Ticket::where('email', auth()->user()->username)
					                ->whereBetween('event_date', [$start, $end])
					                ->first();
					if (!$ticket)
					{
						// create a ticket based on the input user data and the party date
						// if no ticket exists for the user with the same party date
						$ticket = Ticket::create($inputData);
					}

					$this->updateTicketAction([
						                          'ticket'   => $ticket,
						                          "comments" => "Party bag ticket created by customer"
					                          ]);

					$shortlistOptionsUpdated = [];
					if ($ticket != false)
					{
						// create the mappings for the ticket ( Ticket_Mapping )
						foreach ($shortlistOptions as $shortlistOption)
						{
							$shortlistOption->enquired_date = time();
							$shortlistOption->save();

							array_push($shortlistOptionsUpdated, $shortlistOption);
							$mappingInsertData = [
								'ticket_id'   => $ticket->id,
								'map_id'      => $shortlistOption->map_id,
								'map_type_id' => $shortlistOption->map_type_id,
								'created_at'  => date('Y-m-d H:i:s'),
								'updated_at'  => date('Y-m-d H:i:s')
							];

							// create the mapping of package to the ticket
							TicketMapping::create($mappingInsertData);
						}

						// push vendor email to send alerts
						$inputData['vendor_name'] = 'Website Ticket';
						$inputData['vendor_usp'] = 'Website Ticket';

						$enquiryId = config('evibe.ticket.enq.pre.home_page') . $ticket->id;
						if ($inputData == config('evibe.ticket.type.no_results'))
						{
							$enquiryId = config('evibe.ticket.enq.pre.no_result') . $ticket->id;
						}

						// save enquiry id
						$ticket->enquiry_id = $enquiryId;
						$ticket->save();

						$emailContent = array_merge($inputData, (new TicketsController())->getEmailContentData($ticket, [
							"name" => "Party Bag",
							"code" => "Party Bag"
						]));
						$emailContent["ticketId"] = $ticket->id;

						// fetching the option details from the shortlist options selected
						$alertToTeamJobData = [];
						$dataStore = app()->make("EvibeUtilDataStore");
						foreach ($shortlistOptionsUpdated as $shortlistOption)
						{
							$modelName = $shortListableMappings[$shortlistOption->map_type_id]['model'];
							$id = $shortlistOption->map_id;
							$name = $shortListableMappings[$shortlistOption->map_type_id]['name'];
							$cityUrl = $dataStore->getCityUrlFromId($shortlistOption->city_id);
							$occasionUrl = $dataStore->getOccasionUrlFromId($shortlistOption->occasion_id);

							$shortlistParams = [
								'mapId'        => $shortlistOption->map_id,
								'mapTypeId'    => $shortlistOption->map_type_id,
								'occasionId'   => $shortlistOption->occasion_id,
								'cityId'       => $shortlistOption->city_id,
								'enquiredDate' => $shortlistOption->enquired_date,
								'isSelected'   => $shortlistOption->is_selected
							];

							$model = $modelName::find($id);
							if ($model)
							{
								$cardData = $model->getShortListableCardInfo($cityUrl, $occasionUrl, ['name' => $name], $shortlistParams);
								if (count($cardData))
								{
									if (!isset($alertToTeamJobData[$name]))
									{
										$alertToTeamJobData[$name] = [];
									}
									array_push($alertToTeamJobData[$name], $cardData);
								}
							}
						}

						// send this data to the mail alerts to team
						$emailContent['partyBagData'] = $alertToTeamJobData;

						// send email alerts
						(new TicketsController())->dispatchPartyBagMailAlerts($emailContent);

						$resMes['success'] = true;
						$resMes['messages'] = [$shortlistOptionsUpdated];

						return response()->json($resMes);
					}
					else
					{
						return response()->json($resMes);
					}
				}
				else
				{
					$resMes['success'] = false;
					$resMes['messages'] = ['Please select an option'];
				}
			}
		} catch (\Exception $exception)
		{
			$trace = "ticketInputData: " . print_r($inputData, true) .
				"trace: " . $exception->getTraceAsString();

			$siteLogData = new SiteLogData();
			$siteLogData->setTrace($trace)
			            ->setUrl(route("partybag.enquire"))
			            ->setErrorCode($exception->getCode())
			            ->setException("PartyBagEnquireException");

			$this->logSiteError($siteLogData);
		}

		return response()->json($resMes);
	}

	// validate party bag checkout
	public function validatePartyBagCheckout()
	{
		try
		{
			$rules = [
				'partyDate'    => 'required|date|after:yesterday',
				'partyPinCode' => 'required|digits:6'
			];

			$messages = [
				'partyDate.required'    => 'Please enter your party date',
				'partyDate.after'       => 'Please enter a party date from today',
				'partyPinCode.required' => 'Please enter venue PIN code',
				'partyPinCode.digits'   => 'Please enter a valid 6 digit pin code (ex: 560001)'
			];

			$validation = Validator::make(request()->all(), $rules, $messages);

			if ($validation->fails())
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => $validation->messages()->first()
				                        ]);
			}

			if (!auth()->check())
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'An error occurred while updating your data. Please refresh the page and try again'
				                        ]);
			}

			$user = auth()->user();
			$shortlist = $this->getShortlistFromUserId($user->id);
			if (!$shortlist)
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'An error occurred while updating your data. Please refresh the page and try again'
				                        ]);
			}

			// @see: disabling booking for unwanted cities
			$validCityIds = [
				config('evibe.city.Bengaluru'),
				config('evibe.city.Hyderabad')
			];
			if (in_array($shortlist->city_id, $validCityIds)) {
				// proceed
			} else {
				return response()->json([
					'success'  => false,
					'errorMsg' => 'Sorry, we are not currently accepting bookings for your city'
				]);
			}

			$shortlistOptions = ShortlistOptions::where('shortlist_id', $shortlist->id)
			                                    ->whereNull('is_wishlist')
			                                    ->get();

			if (!count($shortlistOptions))
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'Av error occurred while updating your data. Please refresh the page and try again'
				                        ]);
			}

			// todo: entered pin code does not belong to the city of the shortlisted options

			$partyDate = request('partyDate');
			$partyPinCode = request('partyPinCode');

			$shortlistData = $this->getPartyBagData($shortlist->id);
			if (!(isset($shortlistData['uncategorizedData']) && count($shortlistData['uncategorizedData'])))
			{
				Log::error("There are no shortlist options");

				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => "Some error occurred while validating data. Kindly refresh the page and try again"
				                        ]);
			}

			$shortlist->party_date = strtotime($partyDate);
			$shortlist->party_pin_code = $partyPinCode;
			$shortlist->save();

			$shortlistOptionsData = $shortlistData['uncategorizedData'];

			$checkoutFeasibility = true;
			$checkoutFeasibilityMsg = "";
			$checkoutTransportAmount = 0;

			foreach ($shortlistOptionsData as $key => $optionData)
			{
				$transportAmount = 0;
				$feasibility = true;
				$feasibilityMsg = "";

				if (isset($optionData['mapId']) && isset($optionData['mapTypeId']) && ($optionData['mapTypeId'] == config('evibe.ticket.type.decor')))
				{
					$productPrice = $optionData['productPrice'];
					$transportAmount = 0;

					// calculate and update transport prices
					$decor = Decor::find($optionData['mapId']);
					if ($decor && $decor->provider_id)
					{
						$provider = Vendor::find($decor->provider_id);
						if ($provider && $provider->zip)
						{
							$dataForPriceCheck = new PriceCheck();
							$dataForPriceCheck->setOptionId($optionData['mapId']);
							$dataForPriceCheck->setOptionTypeId($optionData['mapTypeId']);
							$dataForPriceCheck->setPartyPinCode($partyPinCode);
							$dataForPriceCheck->setProviderPinCode($provider->zip);
							$dataForPriceCheck->setDistFree(config('evibe.transport.free-kms'));

							$transPriceData = $this->getUpdatedPriceWithTransportationCharges($dataForPriceCheck);

							if (!$transPriceData)
							{
								$feasibility = false;
								$checkoutFeasibility = false;
								$checkoutFeasibilityMsg = 'Entered pincode is invalid. Kindly enter a valid pincode.';

							}
							else
							{
								$transportAmount = isset($transPriceData['transCharges']) ? $transPriceData['transCharges'] : $transportAmount;
								$checkoutTransportAmount += $transportAmount;

								if (isset($transPriceData['distMaxMeters']) && isset($transPriceData['partyDist']) && ($transPriceData['distMaxMeters'] < $transPriceData['partyDist']))
								{
									// @see: cake won't be included here
									$feasibility = false;
									$checkoutFeasibility = false;
									$feasibilityMsg = 'Sorry, delivery of this decor is not available at ' . $partyPinCode . '. Please try with different pin code.';
								}

							}

							$shortlistOptionsData[$key]['feasibility'] = $feasibility;
							$shortlistOptionsData[$key]['feasibilityMsg'] = $feasibilityMsg;
						}
					}

					$bookingPrice = $productPrice + $transportAmount;
					$advanceAmount = $this->getRoundedTokenAmount($bookingPrice);
					//$advanceAmount = floor(($bookingPrice * 50 / 100));

					// update DB
					$shortlistOption = ShortlistOptions::where('shortlist_id', $shortlist->id)
					                                   ->whereNull('is_wishlist')
					                                   ->where('map_id', $optionData['mapId'])
					                                   ->where('map_type_id', $optionData['mapTypeId'])
					                                   ->update([
						                                            'product_price'    => $productPrice,
						                                            'transport_amount' => $transportAmount,
						                                            'booking_price'    => $bookingPrice,
						                                            'advance_amount'   => $advanceAmount,
						                                            'updated_at'       => Carbon::now()
					                                            ]);

					// update array
					$shortlistOptionsData[$key]['productPrice'] = $productPrice;
					$shortlistOptionsData[$key]['transportAmount'] = $transportAmount;
					$shortlistOptionsData[$key]['price'] = $bookingPrice;
					$shortlistOptionsData[$key]['advanceAmount'] = $advanceAmount;
				}
			}

			$shortlistOptionsList = isset($shortlistData["allOptions"]) ? $shortlistData["allOptions"] : collect([]);
			$shortlistBudgetData = $this->getPartyBagBudget($shortlistData["categorizedData"]);

			return response()->json([
				                        'success'                 => true,
				                        'partybag'                => [
					                        'count'        => count($shortlistOptions),
					                        'list'         => $shortlistOptionsList,
					                        'data'         => $shortlistOptionsData,
					                        'budget'       => $shortlistBudgetData,
					                        'cityId'       => $shortlist->city_id,
					                        'occasionId'   => $shortlist->event_id,
					                        'partyDate'    => $partyDate,
					                        'partyPinCode' => $partyPinCode
				                        ],
				                        'checkoutFeasibility'     => $checkoutFeasibility,
				                        'checkoutFeasibilityMsg'  => $checkoutFeasibilityMsg,
				                        'checkoutTransportAmount' => $checkoutTransportAmount
			                        ]);

		} catch (\Exception $exception)
		{
			Log::error("error: " . $exception->getMessage());

			return response()->json([
				                        'success' => false
			                        ]);
		}
	}

	// init party bag auto booking
	public function initPartyBagAutoBooking()
	{
		$previousUrl = url()->previous();

		try
		{

			if (auth()->check())
			{
				$user = auth()->user();

				// fetch the shortlist that party_date as null
				// or party date greater than the current date ( time() )
				$shortlist = $this->getShortlistFromUserId($user->id);
				if (!$shortlist)
				{
					$this->sendNonExceptionErrorReportToTeam([
						                                         'code'      => config('evibe.error_code.create_function'),
						                                         'url'       => request()->fullUrl(),
						                                         'method'    => request()->method(),
						                                         'message'   => '[PartyBagController.php - ' . __LINE__ . '] ' . 'Unable to fetch shortlist for an user when he/she is trying to checkout from party bag',
						                                         'exception' => '[PartyBagController.php - ' . __LINE__ . '] ' . 'Unable to fetch shortlist for an user when he/she is trying to checkout from party bag',
						                                         'trace'     => '[User Id: ' . $user->id . '] [User Email: ' . $user->username . '] [User Mobile: ' . $user->phone . ']',
						                                         'details'   => '[User Id: ' . $user->id . '] [User Email: ' . $user->username . '] [User Mobile: ' . $user->phone . ']'
					                                         ]);

					return redirect($previousUrl);
				}
				$shortlistData = $this->getPartyBagData($shortlist->id);
				if (!(isset($shortlistData['uncategorizedData']) && count($shortlistData['uncategorizedData'])))
				{
					$this->sendNonExceptionErrorReportToTeam([
						                                         'code'      => config('evibe.error_code.create_function'),
						                                         'url'       => request()->fullUrl(),
						                                         'method'    => request()->method(),
						                                         'message'   => '[PartyBagController.php - ' . __LINE__ . '] ' . 'There are no shortlist option for this shortlist. But, customer is trying to checkout from party bag',
						                                         'exception' => '[PartyBagController.php - ' . __LINE__ . '] ' . 'There are no shortlist option for this shortlist. But, customer is trying to checkout from party bag',
						                                         'trace'     => '[User Id: ' . $user->id . '] [User Email: ' . $user->username . '] [User Mobile: ' . $user->phone . '] [Shortlist Id: ' . $shortlist->id . ']',
						                                         'details'   => '[User Id: ' . $user->id . '] [User Email: ' . $user->username . '] [User Mobile: ' . $user->phone . '] [Shortlist Id: ' . $shortlist->id . ']'
					                                         ]);

					return redirect($previousUrl);
				}
				$shortlistData = $shortlistData['uncategorizedData'];

				$cityId = $shortlist->city_id;
				$eventId = $shortlist->event_id;

				$city = City::find($cityId);
				$event = TypeEvent::find($eventId);

				// @see: for now, no venues will be included in party bag
				/*
				foreach ($shortlistData as $shortlistDatum)
				{
					switch ($shortlistDatum['mapTypeId'])
					{
						case config('evibe.ticket.type.package'):
						case config('evibe.ticket.type.resorts'):
						case config('evibe.ticket.type.villas'):
						case config('evibe.ticket.type.lounges'):
						case config('evibe.ticket.type.food'):
						case config('evibe.ticket.type.surprises'):
						case config('evibe.ticket.type.priests'):
						case config('evibe.ticket.type.tents'):
						case config('evibe.ticket.type.generic-package'):
							$package = Package::find($shortlistDatum['mapId']);
							if ($package && ($package->map_type_id == config('evibe.ticket.type.venue')))
							{
								$partnerId = $package->map_id;
								$partnerTypeId = $package->map_type_id;
								$venuePartner = true;
								$eventId = (isset($shortlistDatum['occasionId']) && $shortlistDatum['occasionId']) ? $shortlistDatum['occasionId'] : null;

								$entityData = $this->getEntityDetailsByMapValues($shortlistDatum['mapId'], config('evibe.ticket.type.package'));

								$partner = $entityData['provider'];
								$zipCode = $partner['zip'];
								$venueAddress = $partner['full_address'] . ", Pin Code: " . $zipCode;
								$areaId = $partner['area_id'];
								$venueLandmark = $partner['landmark'];
							}

					}
				}
				*/

				// deadline calculation
				// if booked before 12 today, put deadline tomorrow 8 AM
				// if booked after 12 today, put deadline tomorrow 4 PM
				// reminder will be sent at 9 AM and 5 PM everyday for un-acted tickets.
				$currentHour = date("H"); // returns in 24 Hrs format
				$deadlineDateAndTime = strtotime("today 4 PM");
				if ($currentHour > 12)
				{
					$deadlineDateAndTime = strtotime("tomorrow 8 AM");
				}

				$name = $user->first_name ?: ($user->name ?: null);
				$email = $user->username ?: null;
				$phone = $user->phone ?: null;

				// create ticket with appropriate details
				$ticketInsertData = [
					'name'                    => $name,
					'email'                   => $email,
					'phone'                   => $phone,
					'user_id'                 => $user->id,
					'event_date'              => $shortlist->party_date,
					'is_auto_booked'          => 1,
					'status_id'               => config('evibe.ticket.status.initiated'),
					'city_id'                 => $cityId,
					'auto_book_deadline_date' => $deadlineDateAndTime,
					'event_id'                => $eventId,
					'zip_code'                => $shortlist->party_pin_code,
					'created_at'              => date('Y-m-d H:i:s'),
					'updated_at'              => date('Y-m-d H:i:s'),
					'enquiry_source_id'       => config('evibe.ticket.enquiry_source.pb_checkout')
				];

				// @see: hardcoded
				$ticketInsertData['type_ticket_id'] = config('evibe.ticket.type.package');

				// Create the ticket with phone number and event date or check in date
				$ticket = Ticket::create($ticketInsertData);

				if (!$ticket)
				{
					$this->sendNonExceptionErrorReportToTeam([
						                                         'code'      => config('evibe.error_code.create_function'),
						                                         'url'       => request()->fullUrl(),
						                                         'method'    => request()->method(),
						                                         'message'   => '[PartyBagController.php - ' . __LINE__ . '] ' . 'Unable to create a ticket',
						                                         'exception' => '[PartyBagController.php - ' . __LINE__ . '] ' . 'Unable to create a ticket',
						                                         'trace'     => '[User Id: ' . $user->id . '] [User Email: ' . $user->username . '] [User Mobile: ' . $user->phone . ']',
						                                         'details'   => '[User Id: ' . $user->id . '] [User Email: ' . $user->username . '] [User Mobile: ' . $user->phone . ']'
					                                         ]);

					Log::error("ticket insert data: " . print_r($ticketInsertData, true));

					return redirect($previousUrl);
				}

				$this->updateTicketAction([
					                          'ticket'   => $ticket,
					                          'comments' => "Auto booking ticket created by customer"
				                          ]);

				// save enquiry id
				$ticketId = $ticket->id;
				$enquiryId = config("evibe.ticket.enq.pre.auto_booked") . $ticketId;
				$ticket->enquiry_id = $enquiryId;

				// todo: even if providers are not there, make auto payment to all bookings? similar to services
				foreach ($shortlistData as $shortlistDatum)
				{
					// booking related defaults
					$bookingInfo = null;
					$productPrice = 0;
					$bookingAmount = 0;
					$advanceAmount = 0;
					$typeTicketBookingId = null;
					$productValidity = false;
					$estimatedTransCharges = null;
					$transCharges = null;
					$isVenueBooking = 0;
					$partyDateTime = 0;
					$partyEndTime = null;
					$bookingUnits = 1;
					$profileImageUrl = null;

					switch ($shortlistDatum['mapTypeId'])
					{
						case config('evibe.ticket.type.decor'):

							//$ticket->event_date = $partyDate;
							$decor = Decor::with("tags")->find($shortlistDatum['mapId']);
							if (!$decor)
							{
								continue;
							}

							$productValidity = true;

							// verify partner and fallback for city_id
							$partner = Vendor::find($decor->provider_id);
							if (!$partner)
							{
								continue;
							}
							$partnerId = $partner ? $partner->id : null;
							$partnerTypeId = config('evibe.ticket.type.planner');
							$isVenueBooking = 0;

							// if no city for provider || city of provider doesn't match session
							if ((!$partner->city_id) || ($partner->city_id && ($cityId != $partner->city_id)))
							{
								$optionData = [
									'name' => $decor->name,
									'code' => $decor->code,
									'type' => config('evibe.type-product-title.' . ($shortlistDatum['mapTypeId']))
								];

								$this->dispatch(new MissingCityAlertToTeam($optionData));
							}

							// create booking info: html full url
							$productUrl = $this->fetchProductUrl($cityId, $eventId, $shortlistDatum['mapTypeId'], $shortlistDatum['mapId'], $decor->url);
							$bookingInfo = "<a href='" . $productUrl . "?ref=pay-checkout' target='_blank'>" . $decor->name . "</a>";

							// get type ticket booking id from tags
							$typeTicketBookingId = $this->getTypeBookingIdFromTags($decor);

							// verify booking and token amount
							$providerPinCode = $partner->zip;
							$partyPinCode = $shortlist->party_pin_code;
							$res = $this->makeCurlRequestToGoogleMatrixApi($providerPinCode, $partyPinCode);
							if (($res['status'] == 'OK') && ($res['rows'][0]['elements'][0]['status'] === 'OK'))
							{
								$partyDist = $res['rows'][0]['elements'][0]['distance']['value'];
								$distFreeMeters = ($decor->kms_free ? $decor->kms_free : config('evibe.transport.free-kms')) * 1000;
								$distMaxMeters = ($decor->kms_max) * 1000;
								$baseBookingAmount = $decor->min_price;
								$transMin = $decor->trans_min;
								$transMax = $decor->trans_max;
								$diffDist = $distMaxMeters - $distFreeMeters;
								$diffTransFare = $transMax - $transMin;
								if ($diffDist > 0)
								{
									$farePerMeter = $diffTransFare / $diffDist;
								}
								else
								{
									$farePerMeter = 0;
									$typeTicketName = TypeTicket::find($shortlistDatum['mapTypeId'])->name;
									$typeProvider = TypeTicket::find($partnerTypeId)->name;

									$data = [
										'ticketId'     => $ticketId,
										'optionInfo'   => $bookingInfo,
										'optionCode'   => $decor->code,
										'typeTicket'   => $typeTicketName,
										'providerName' => $decor->provider->name,
										'providerCode' => $decor->provider->code,
										'typeProvider' => $typeProvider
									];
									$this->dispatch(new MailTransportChargesErrorToTeamJob($data));
								}

								$extraDist = $partyDist - $distFreeMeters;
								if ($extraDist > 0)
								{
									$transCharges = intval($extraDist * $farePerMeter);
								}
								else
								{
									$transCharges = 0;
								}
								$estimatedTransCharges = $transCharges;
								if ($partyDist <= $distFreeMeters)
								{
									$transCharges = 0;
								}
								$totalBookingAmount = $transCharges + $baseBookingAmount;
								$roundedTokenAmt = $this->getRoundedTokenAmount($totalBookingAmount);

								if ($shortlistDatum['price'] != $totalBookingAmount)
								{
									$shortlistDatum['price'] = $totalBookingAmount;

									// @todo: logSiteError
									// @see: since price is being over-written, just log the notification (logSiteError)

									SiteErrorLog::create([
										                     'url'        => request()->fullUrl(),
										                     'exception'  => "Partybag Auto Booking: Checkout issue",
										                     'code'       => "Error",
										                     'details'    => "Some error occurred in PartyBagController [Line: " . __LINE__ . "], tried to hack decor price. Wrong booking amount",
										                     'created_at' => date('Y-m-d H:i:s'),
										                     'updated_at' => date('Y-m-d H:i:s')
									                     ]);

									/*
									$this->sendNonExceptionErrorReportToTeam([
										                                         'code'      => config('evibe.error_code.create_function'),
										                                         'url'       => request()->fullUrl(),
										                                         'method'    => request()->method(),
										                                         'message'   => '[PartyBagController.php - ' . __LINE__ . '] ' . 'Tried to hack decor price. Wrong booking amount',
										                                         'exception' => '[PartyBagController.php - ' . __LINE__ . '] ' . 'Tried to hack decor price. Wrong booking amount',
										                                         'trace'     => '[Ticket Id: ' . $ticketId . '] [User Id: ' . $user->id . '] [User Email: ' . $user->username . '] [User Mobile: ' . $user->phone . ']',
										                                         'details'   => '[Ticket Id: ' . $ticketId . '] [User Id: ' . $user->id . '] [User Email: ' . $user->username . '] [User Mobile: ' . $user->phone . ']'
									                                         ]);
									*/
								}
								if ($shortlistDatum['advanceAmount'] != $roundedTokenAmt)
								{
									$shortlistDatum['advanceAmount'] = $roundedTokenAmt;

									// @todo: logSiteError
									// @see: since price is being over-written, just log the notification (logSiteError)

									SiteErrorLog::create([
										                     'url'        => request()->fullUrl(),
										                     'exception'  => "Partybag Auto Booking: Checkout issue",
										                     'code'       => "Error",
										                     'details'    => "Some error occurred in PartyBagController [Line: " . __LINE__ . "], tried to hack decor price. Wrong advance amount",
										                     'created_at' => date('Y-m-d H:i:s'),
										                     'updated_at' => date('Y-m-d H:i:s')
									                     ]);

									/*
									$this->sendNonExceptionErrorReportToTeam([
										                                         'code'      => config('evibe.error_code.create_function'),
										                                         'url'       => request()->fullUrl(),
										                                         'method'    => request()->method(),
										                                         'message'   => '[PartyBagController.php - ' . __LINE__ . '] ' . 'Tried to hack decor price. Wrong advance amount',
										                                         'exception' => '[PartyBagController.php - ' . __LINE__ . '] ' . 'Tried to hack decor price. Wrong advance amount',
										                                         'trace'     => '[Ticket Id: ' . $ticketId . '] [User Id: ' . $user->id . '] [User Email: ' . $user->username . '] [User Mobile: ' . $user->phone . ']',
										                                         'details'   => '[Ticket Id: ' . $ticketId . '] [User Id: ' . $user->id . '] [User Email: ' . $user->username . '] [User Mobile: ' . $user->phone . ']'
									                                         ]);
									*/
								}
							}
							else
							{
								$this->sendNonExceptionErrorReportToTeam([
									                                         'code'      => config('evibe.error_code.create_function'),
									                                         'url'       => request()->fullUrl(),
									                                         'method'    => request()->method(),
									                                         'message'   => '[PartyBagController.php - ' . __LINE__ . '] ' . 'Unable to proceed due to invalid pincode',
									                                         'exception' => '[PartyBagController.php - ' . __LINE__ . '] ' . 'Unable to proceed due to invalid pincode',
									                                         'trace'     => '[Ticket Id: ' . $ticketId . '] [User Id: ' . $user->id . '] [User Email: ' . $user->username . '] [User Mobile: ' . $user->phone . ']',
									                                         'details'   => '[Ticket Id: ' . $ticketId . '] [User Id: ' . $user->id . '] [User Email: ' . $user->username . '] [User Mobile: ' . $user->phone . ']'
								                                         ]);

								return redirect($previousUrl);
							}

							break;

						case config('evibe.ticket.type.cake'):
							$cake = Cake::with("tags")->find($shortlistDatum['mapId']);
							if (!$cake)
							{
								// todo: report error

								continue;
							}
							$productValidity = true;

							if ($cake->price && isset($shortlistDatum['price']) && $shortlistDatum['price'] && ($cake->price > $shortlistDatum['price']))
							{
								$this->sendNonExceptionErrorReportToTeam([
									                                         'code'      => config('evibe.error_code.create_function'),
									                                         'url'       => request()->fullUrl(),
									                                         'method'    => request()->method(),
									                                         'message'   => '[PartyBagController.php - ' . __LINE__ . '] ' . 'Trying to hack cake price',
									                                         'exception' => '[PartyBagController.php - ' . __LINE__ . '] ' . 'Trying to hack cake price',
									                                         'trace'     => '[Ticket Id: ' . $ticketId . '][User Id: ' . $user->id . '] [User Email: ' . $user->username . '] [User Mobile: ' . $user->phone . ']',
									                                         'details'   => '[Ticket Id: ' . $ticketId . '][User Id: ' . $user->id . '] [User Email: ' . $user->username . '] [User Mobile: ' . $user->phone . ']'
								                                         ]);

								Log::error("Tried to hack - cake data: " . print_r($shortlistDatum, true));

								return redirect($previousUrl);
							}

							$shortlistOptionFieldValues = ShortlistOptionFieldValue::where('shortlist_option_id', $shortlistDatum['shortlistOptionId'])->get();
							if (count($shortlistOptionFieldValues))
							{
								foreach ($shortlistOptionFieldValues as $fieldValue)
								{
									CheckoutFieldValue::create([
										                           'ticket_id'         => $ticket->id,
										                           'checkout_field_id' => $fieldValue->checkout_field_id,
										                           'value'             => $fieldValue->value
									                           ]);
								}
							}

							$isVenueBooking = 0;

							// fetching delivery charges from party bag as slotId is not being stored

							// create booking info: html full url
							$productUrl = $this->fetchProductUrl($cityId, $eventId, $shortlistDatum['mapTypeId'], $shortlistDatum['mapId'], $cake->url);
							$bookingInfo = "<a href='" . $productUrl . "?ref=pay-checkout' target='_blank'>" . $cake->title . "</a>";

							// get type ticket booking id from tags
							$typeTicketBookingId = config('evibe.booking_type.cake');

							break;

						case config('evibe.ticket.type.package'):
						case config('evibe.ticket.type.resorts'):
						case config('evibe.ticket.type.villas'):
						case config('evibe.ticket.type.lounges'):
						case config('evibe.ticket.type.food'):
						case config('evibe.ticket.type.surprises'):
						case config('evibe.ticket.type.priests'):
						case config('evibe.ticket.type.tents'):
						case config('evibe.ticket.type.generic-package'):

							// @todo: dynamic params still not included

							$entityData = $this->getEntityDetailsByMapValues($shortlistDatum['mapId'], config('evibe.ticket.type.package'));

							if (!$entityData)
							{
								// todo: report error

								continue;
							}
							$productValidity = true;

							// @see: venue package is not party bag addable for now
							// todo: add partner check when venues are being included
							$isVenueBooking = 0;

							// todo: guests count details

							// create booking info: html full url
							$productUrl = $this->fetchProductUrl($cityId, $eventId, $shortlistDatum['mapTypeId'], $shortlistDatum['mapId'], $entityData['url']);
							$bookingInfo = "<a href='" . $productUrl . "?ref=pay-checkout' target='_blank'>" . $entityData['name'] . "</a>";

							$typeTicketBookingId = config('evibe.booking_type.venue'); // @todo: should be based on the booking

							break;

						case config('evibe.ticket.type.service'):
						case config('evibe.ticket.type.entertainment'):
							$ent = TypeServices::find($shortlistDatum['mapId']);
							if (!$ent)
							{
								//return response()->json([
								//	                        'success' => false,
								//	                        'error'   => "could not find the service"
								//                        ]);

								// todo: report error

								continue;
							}

							$productValidity = true;

							// create booking info: html full url
							$productUrl = $this->fetchProductUrl($cityId, $eventId, $shortlistDatum['mapTypeId'], $shortlistDatum['mapId'], $ent->url);
							$bookingInfo = "<a href='" . $productUrl . "?ref=pay-checkout' target='_blank'>" . $ent->name . "</a>";

							$typeTicketBookingId = config('evibe.booking_type.ent');
							$shortlistDatum['mapTypeId'] = config('evibe.ticket.type.service');

							break;
					}

					if (!$productValidity)
					{
						// todo: report that product doesn't exist
						continue;
					}

					// form ticket mapping data
					$mappingInsertData = [
						'ticket_id'   => $ticketId,
						'map_id'      => $shortlistDatum['mapId'],
						'map_type_id' => $shortlistDatum['mapTypeId'],
						'created_at'  => date('Y-m-d H:i:s'),
						'updated_at'  => date('Y-m-d H:i:s')
					];

					// create ticket mapping
					$ticketMapping = TicketMapping::create($mappingInsertData);

					if (!$ticketMapping)
					{
						$this->sendNonExceptionErrorReportToTeam([
							                                         'code'      => config('evibe.error_code.create_function'),
							                                         'url'       => request()->fullUrl(),
							                                         'method'    => request()->method(),
							                                         'message'   => '[PartyBagController.php - ' . __LINE__ . '] ' . 'Unable to create ticket mapping',
							                                         'exception' => '[PartyBagController.php - ' . __LINE__ . '] ' . 'Unable to create ticket mapping',
							                                         'trace'     => '[Ticket Id: ' . $ticketId . '] [User Id: ' . $user->id . '] [User Email: ' . $user->username . '] [User Mobile: ' . $user->phone . ']',
							                                         'details'   => '[Ticket Id: ' . $ticketId . '] [User Id: ' . $user->id . '] [User Email: ' . $user->username . '] [User Mobile: ' . $user->phone . ']'
						                                         ]);

						return redirect($previousUrl);
					}

					$ticketMappingId = $ticketMapping->id;

					// @see: used sample organisers for now
					// todo: use product specific partners
					$partnerId = config('evibe.default-partner.city.' . $cityId . '.id');
					if (!$partnerId)
					{
						$partnerId = config('evibe.default-partner.id');
					}
					$partnerTypeId = config('evibe.ticket.type.planner');

					// create ticket booking
					$bookingInsertData = [
						'booking_id'             => config('evibe.ticket.enq.pre.auto_booked') . $ticketMappingId,
						'booking_info'           => $bookingInfo,
						//'party_date_time'             => $partyDateTime,
						//'party_end_time'              => $partyEndTime,
						'booking_units'          => $bookingUnits,
						'product_price'          => (isset($shortlistDatum['productPrice']) && $shortlistDatum['productPrice']) ? $shortlistDatum['productPrice'] : null,
						'booking_amount'         => $shortlistDatum['price'],
						'advance_amount'         => (isset($shortlistDatum['advanceAmount']) && $shortlistDatum['advanceAmount']) ? $shortlistDatum['advanceAmount'] : $shortlistDatum['price'],
						'transport_charges'      => (isset($shortlistDatum['transportAmount']) && $shortlistDatum['transportAmount']) ? $shortlistDatum['transportAmount'] : null,
						//'estimated_transport_charges' => $estimatedTransCharges,
						'map_id'                 => $partnerId,
						'map_type_id'            => $partnerTypeId,
						'ticket_id'              => $ticketId,
						'ticket_mapping_id'      => $ticketMappingId,
						'is_venue_booking'       => $isVenueBooking,
						'type_ticket_booking_id' => $typeTicketBookingId,
						'booking_type_details'   => ucwords(config('evibe.auto-book.' . $shortlistDatum['mapTypeId'])),
						'created_at'             => date('Y-m-d H:i:s'),
						'updated_at'             => date('Y-m-d H:i:s')
					];

					$ticketBooking = TicketBooking::create($bookingInsertData);

					if (!$ticketBooking)
					{
						$this->sendNonExceptionErrorReportToTeam([
							                                         'code'      => config('evibe.error_code.create_function'),
							                                         'url'       => request()->fullUrl(),
							                                         'method'    => request()->method(),
							                                         'message'   => '[PartyBagController.php - ' . __LINE__ . '] ' . 'Unable to create ticket booking',
							                                         'exception' => '[PartyBagController.php - ' . __LINE__ . '] ' . 'Unable to create ticket booking',
							                                         'trace'     => '[Ticket Id: ' . $ticketId . '] [User Id: ' . $user->id . '] [User Email: ' . $user->username . '] [User Mobile: ' . $user->phone . ']',
							                                         'details'   => '[Ticket Id: ' . $ticketId . '] [User Id: ' . $user->id . '] [User Email: ' . $user->username . '] [User Mobile: ' . $user->phone . ']'
						                                         ]);

						Log::error("booking insert data: " . print_r($bookingInsertData, true));

						return redirect($previousUrl);
					}

					// upload profile image to booking gallery
					// @see: for add-ons
					if ($profileImageUrl)
					{
						$validImage = getimagesize($profileImageUrl);
						if ($validImage)
						{
							TicketBookingGallery::create([
								                             'ticket_booking_id' => $ticketBooking->id,
								                             'url'               => $profileImageUrl,
								                             'type'              => 1,
								                             'title'             => pathinfo($profileImageUrl, PATHINFO_FILENAME)
							                             ]);
						}
					}
				}

				$ticket->save();

				$checkoutLink = route('auto-book.pay.auto.checkout');
				$checkoutLink .= "?id=" . $ticketId;
				$checkoutLink .= "&token=" . Hash::make($ticketId);
				$checkoutLink .= "&uKm8=" . Hash::make($phone);

				if ($checkoutLink)
				{
					return redirect($checkoutLink);
				}
			}

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());

			// todo: report error to team

			return redirect($previousUrl);
		}
	}

	// function that returns the budget for the party bag
	// returns 0 values if user is not authenticated
	/*
		Budget Calculation Rules
				partyBagWorthMax >=  partyBagMax
				partyBagMax >= partyBagWorthMin
				partyBagWorthMin >= partyBagMin

		Cakes:  check for the base price
				else calculate based on price_per_kg and min_order
				else cake price
			    **minPrice = maxPrice = price
			    **worthMin = worthMax = worth

		Decors: minPrice, maxPrice, worth ( worthMin = worthMax )

		Trends : minPrice = maxPrice = worthMin = worthMax = price

		TypeServices : minPrice, maxPrice ( if maxPrice > minPrice )
					   worth_price ( worthMin = worthMax = worth_price )

		VenueHalls : if ( rent_min ) then  minPrice = rent_min, maxPrice = rent_max
									       worthMin = rent_worth, worthMax = rent_worth
					 if ( price_min_veg ) then minPrice = price_min_veg * cap_min,
	                                           maxPrice = price_min_veg * cap_max
	                                           worthMin = price_worth_veg * cap_min
		                                       worthMax = price_worth_veg * cap_max
					if( venue->price_min_rent ) then minPrice = venue->price_min_rent
													 maxPrice = venue->price_max_rent
	                                                 worthMin = venue->worth_rent
	                                                 worthMax = venue->worth_rent
					if( venue->price_min_veg ) then minPrice = venue->price_min_veg * venue->cap_min
	                                                  maxPrice = venue->price_min_veg * venue->cap_max
	                                                  worthMin = venue->min_veg_worth * venue->cap_min
													  worthMax = venue->min_veg_worth * venue->cap_max
					if ( venue->price_kid ) then minPrice = venue->price_kid * venue->cap_min
												 maxPrice = venue->price_kid * venue->cap_max
												 worthMin = venue->worth_kid * venue->cap_min
												 worthMax = venue->worth_kid * venue->cap_max

		Packages :  if ( venue_deals )
						if ( rent )  minPrice = price,
									 maxPrice = price_max
	                                 worthMin = price_worth
	                                 worthMax = price_worth
	                    if ( not rent )
									   minPrice = price * guestMin
	                                   maxPrice = price_max * guestMax
	                                   worthMin = price_worth * guestMin
	                                   worthMax = price_worth * guestMax
	               rest of the packages
	                                  minPrice = price
	                                  maxPrice = price_max
	                                  worthMin = price_worth
	                                  worthMax = price_worth

		for further details check the respective models for the implementation
		and budget rules are applicable for all the models, and each model should return
	    the respective values compulsory
	*/
	private function getPartyBagBudget($data = false)
	{
		$categorizedData = [];
		$budget = [
			'pb_min'         => 0,
			'pb_max'         => 0,
			'pb_worth_min'   => 0,
			'pb_worth_max'   => 0,
			'pb_price'       => 0,
			'pb_price_worth' => 0
		];

		if (is_array($data) && isset($data["shortlistId"]))
		{
			$shortlistData = $this->getPartyBagData($data["shortlistId"]);
			$categorizedData = $shortlistData["categorizedData"];
			$uncategorizedData = $shortlistData["uncategorizedData"];
		}

		if (!is_bool($data))
		{
			$categorizedData = $data;
		}

		foreach ($this->shortListableMappings as $mappingKey => $mappingValues)
		{
			foreach ($categorizedData as $category => $cardDetailList)
			{
				if ($category == $mappingKey)
				{
					$budget['pb_min'] += $this->getPartyBagMinBudget($cardDetailList, $mappingValues['min_count_budget']);
					$budget['pb_max'] += $this->getPartyBagMaxBudget($cardDetailList, $mappingValues['max_count_budget']);
					$budget['pb_worth_min'] += $this->getPartyBagWorthMinBudget($cardDetailList, $mappingValues['min_count_budget']);
					$budget['pb_worth_max'] += $this->getPartyBagWorthMaxBudget($cardDetailList, $mappingValues['max_count_budget']);
				}
			}
		}

		if (isset($uncategorizedData) && count($uncategorizedData))
		{
			foreach ($uncategorizedData as $datum)
			{
				$budget['pb_price'] += $datum['price'];
				// @see: investigate the details of worth key existance and change accordingly
				if (isset($datum['worth']))
				{
					$budget['pb_price_worth'] += $datum['worth'] ?: $datum['price'];
				}
				else
				{
					$budget['pb_price_worth'] += $datum['price'];
				}
			}
		}

		return $budget;
	}

	// get party bag minimum budget
	private function getPartyBagMinBudget($cardList, $count)
	{
		$minPrice = 0;

		// sort card list based on the price ( low - high )
		$this->sortArrayByKey($cardList, 'pbPriceMin', false, true);

		foreach ($cardList as $card)
		{
			$minPrice = $minPrice + isset($card['pbPriceMin']) ? $card['pbPriceMin'] : 0;
			$count--;
			if ($count <= 0)
			{
				break;
			}
		}

		return $minPrice;
	}

	// get party bag maximum budget
	private function getPartyBagMaxBudget($cardList, $count)
	{
		$maxPrice = 0;

		// sort card list based on the priceMax ( high - low )
		$this->sortArrayByKey($cardList, 'pbPriceMax', false, false);

		foreach ($cardList as $card)
		{
			$maxPrice = $maxPrice + isset($card['pbPriceMax']) ? $card['pbPriceMax'] : 0;
			$count--;
			if ($count <= 0)
			{
				break;
			}
		}

		return $maxPrice;
	}

	// get party bag worth minimum budget
	private function getPartyBagWorthMinBudget($cardList, $count)
	{
		$worthMinPrice = 0;

		// sort card list based on the priceWorthMin ( low - high )
		$this->sortArrayByKey($cardList, 'pbPriceWorthMin', false, true);

		foreach ($cardList as $card)
		{
			$worthMinPrice = $worthMinPrice + isset($card['pbPriceWorthMin']) ? $card['pbPriceWorthMin'] : 0;
			$count--;
			if ($count <= 0)
			{
				break;
			}
		}

		return $worthMinPrice;
	}

	// get party bag worth maximum budget
	private function getPartyBagWorthMaxBudget($cardList, $count)
	{
		$worthMaxPrice = 0;

		// sort card list based on the priceWorthMax ( high - low )
		$this->sortArrayByKey($cardList, 'pbPriceWorthMax', false, false);

		foreach ($cardList as $card)
		{
			$worthMaxPrice = $worthMaxPrice + isset($card['pbPriceWorthMax']) ? $card['pbPriceWorthMax'] : 0;
			$count--;
			if ($count <= 0)
			{
				break;
			}
		}

		return $worthMaxPrice;
	}

	// sort array based on the key ( used to sort the party bag data to fetch the budget
	private function sortArrayByKey(&$array, $key, $string = false, $asc = true)
	{
		if ($string)
		{
			usort($array, function ($a, $b) use (&$key, &$asc) {
				if ($asc)
				{
					return strcmp(strtolower($a[$key]), strtolower($b[$key]));
				}
				else
				{
					return strcmp(strtolower($b[$key]), strtolower($a[$key]));
				}
			});
		}
		else
		{
			usort($array, function ($a, $b) use (&$key, &$asc) {
				if (isset($a[$key]) && isset($b[$key]))
				{
					if ($a[$key] == $b[$key])
					{
						return 0;
					}
					if ($asc)
					{
						return ($a[$key] < $b[$key]) ? -1 : 1;
					}
					else
					{
						return ($a[$key] > $b[$key]) ? -1 : 1;
					}
				}

				return 0;
			});
		}
	}

	// get party bag data for the particular shortlist id
	private function getPartyBagData($shortlistId)
	{
		$unCategorizedData = [];
		$categorizedData = [];

		$shortlistOptions = ShortlistOptions::where('shortlist_id', $shortlistId)
		                                    ->whereNull('is_wishlist')
		                                    ->get();

		$allOptions = $shortlistOptions;
		$shortlistOptions = $shortlistOptions->sortByDesc('created_at');

		// get the card details of the short list options
		foreach ($shortlistOptions as $shortlistOption)
		{
			$cardData = $this->getShortlistCardData($shortlistOption);
			if (is_null($cardData))
			{
				continue;
			}

			$mapTypeId = $cardData['mapTypeId'];
			if (!isset($categorizedData[$mapTypeId]))
			{
				$categorizedData[$mapTypeId] = [];
			}

			array_push($unCategorizedData, $cardData);
			array_push($categorizedData[$mapTypeId], $cardData);
		}

		$data = [
			"allOptions"        => $allOptions,
			'categorizedData'   => $categorizedData,
			'uncategorizedData' => $unCategorizedData
		];

		return $data;
	}

	// function that returns the shortlist option card data
	private function getShortlistCardData($shortlistOption)
	{
		$shortlistMappings = $this->shortListableMappings;
		$productTypeId = $shortlistOption->map_type_id;
		$productId = $shortlistOption->map_id;
		$modelName = isset($shortlistMappings[$productTypeId]['model']) ? $shortlistMappings[$productTypeId]['model'] : false;
		$dataStore = app()->make("EvibeUtilDataStore");

		if ($modelName && ($model = $modelName::find($productId)))
		{
			$name = isset($shortlistMappings[$productTypeId]['name']) ? $shortlistMappings[$productTypeId]['name'] : "Default Name";
			$displayName = isset($shortlistMappings[$productTypeId]['display_name']) ? $shortlistMappings[$productTypeId]['display_name'] : "Default Name";
			$cityUrl = $dataStore->getCityUrlFromId($shortlistOption->city_id);
			$occasionUrl = $dataStore->getOccasionUrlFromId($shortlistOption->occasion_id);

			$shortlistParams = [
				'mapId'        => $productId,
				'mapTypeId'    => $productTypeId,
				'occasionId'   => $shortlistOption->occasion_id,
				'cityId'       => $shortlistOption->city_id,
				'enquiredDate' => $shortlistOption->enquired_date,
				'isSelected'   => $shortlistOption->is_selected,
				'displayName'  => $displayName
			];

			$cardData = $model->getShortListableCardInfo($cityUrl, $occasionUrl, ['name' => $name], $shortlistParams);
			if (count($cardData))
			{
				// If any checkout fields influence the booking price
				$cardData['price'] = $shortlistOption->booking_price ? $shortlistOption->booking_price : ((isset($cardData['price']) && $cardData['price']) ? $cardData['price'] : $shortlistOption->product_price);
				$cardData['productPrice'] = $shortlistOption->product_price;
				$cardData['advanceAmount'] = $shortlistOption->advance_amount;
				$cardData['transportAmount'] = $shortlistOption->transport_amount;
				$cardData['shortlistOptionId'] = $shortlistOption->id;

				return $cardData;
			}
		}

		return null;
	}

	public function getOccasionNameFromId($eventId)
	{
		$event = TypeEvent::find($eventId);

		return $event ? $event->name : null;
	}
}