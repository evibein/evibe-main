<?php

namespace App\Http\Controllers\Inspirations;

use App\Http\Controllers\Base\BaseController;
use App\Models\Inspirations\InspirationPages;
use App\Models\Inspirations\InspirationIdeas;

class InspirationsController extends BaseController
{
	public function fetchFeed($pageUrl, $albumUrl = null)
	{
		$pageDetails = InspirationPages::where('url', $pageUrl)
		                               ->first();

		if ($pageDetails)
		{
			$pageId = $pageDetails['id'];

			$ideas = InspirationIdeas::where('page_id', $pageId);

			$ideas = $ideas->get();
			if (!(count($ideas)))
			{
				return redirect('/');
			}

			$data = [
				"pageDetails" => $pageDetails,
				"feedInfo"    => $ideas,
			];

			if (!(is_null($albumUrl)))
			{
				$albumInfo = $ideas->where('profile_url', $albumUrl)->first();
				if ($albumInfo)
				{
					$albumInfo = $albumInfo->toArray();
					$data['feedInfo'] = $ideas->where('profile_url', '!=', $albumUrl);
					$data['albumInfo'] = $albumInfo;
					$data['backUrl'] = $pageUrl;

					return view("inspirations.album")->with(['data' => $data]);
				}
				else
				{
					return redirect(route("inspirations.feed", $pageUrl));
				}
			}

			return view("inspirations.list")->with(['data' => $data]);
		}
		else
		{
			// Redirect to homepage
			return redirect('/');
		}
	}

	public function storeDetails()
	{
		// $res['success'] = true;

		// $rules = [
		// 	'leadName'  => 'required',
		// 	'leadEmail' => 'required|email',
		// 	'leadPhone' => 'required|phone',
		// ];

		// $messages = [
		// 	"leadPhone.required" => "Please enter your phone number",
		// 	"leadPhone.phone"    => "Phone number is invalid (ex: 9640204000)",
		// 	"leadEmail.required" => "Please enter your email address",
		// 	"leadEmail.email"    => "Phone enter a valid email address",
		// 	"leadName.required"  => "Please enter your full name"
		// ];
		// $validation = validator(request()->input(), $rules, $messages);

		// if ($validation->fails())
		// {
		// 	$res['success'] = false;
		// 	$res['errorMsg'] = $validation->messages()->first();
		// }
		// else
		// {
		// 	$data["name"] = request("leadName");
		// 	$data["email"] = request("leadEmail");
		// 	$data["phone"] = request("leadPhone");
		// 	$data["link"] = request("sourceUrl");

		// 	// $this->dispatch(new MailMemoriesEnquiryToTeamJob($data));

		// 	$res['success'] = true;
		// }

		// return response()->json($res);
	}
	// public function fetchAlbum($pageUrl,$albumUrl)
	// {
	// 	// validate params
	// 	$pageDetails = InspirationPages::where('url',$pageUrl)
	// 									->first();
	// 	if($pageDetails)
	// 	{
	// 		$pageId = $pageDetails['id'];
	// 		$ideas 	= InspirationIdeas::where([['page_id',$pageId],['profile_url',$albumUrl]])
	// 									->first();
	// 		if(!(count($ideas)))
	// 		{
	// 			return redirect('/');	
	// 		}
	// 		$data=[
	// 			"pageDetails" => $pageDetails,
	// 			"feedInfo"		  => $ideas
	// 		];
	// 		return view("inspirations.list")->with(['data' => $data]);
	// 	}
	// 	else
	// 	{
	// 		// Redirect to homepage
	// 		return redirect('/');
	// 	}
	// }

}