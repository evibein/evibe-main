<?php

namespace App\Http\Controllers\Campaigns;

use App\Http\Controllers\Base\BaseController;
use App\Models\Campaigns\CampaignPlan;
use App\Models\Campaigns\CampaignSections;
use App\Models\Campaigns\CampaignProducts;
use App\Models\Campaigns\CampaignGallery;
use App\Models\Ticket\TicketBooking;
use App\Models\Util\Area;
use App\Models\Util\OptionAvailability;
use Carbon\Carbon;
use App\Models\Cake\Cake;
use App\Models\Cake\CakeGallery;
use App\Models\Decor\Decor;
use App\Models\Decor\DecorGallery;
use App\Models\Package\Package;
use App\Models\Types\TypeServices;
use App\Models\Types\TypeEvent;
use Illuminate\Support\Facades\Cache;

class CampaignController extends BaseController
{
	public function landingPage($campaignUrl, $isGetproducts = false)
	{
		$campaignData = $this->isCampaignLive($campaignUrl);

		if ($campaignData)
		{
			$cacheKey = $this->deviceSpecificCacheKey(request()->fullUrl());
			$cacheKey = base64_encode($cacheKey);
			if (Cache::has($cacheKey))
			{
				$data = Cache::get($cacheKey);

				return view("base.campaigns.campaigns-landing-page")->with(['data' => $data]);
			}

			$campaignId = $campaignData['id'];
			$sectionsData = CampaignSections::where('campaign_id', $campaignId)
			                                ->get();

			$sectionsArrayData = [];
			if ($sectionsData->count() > 0)
			{
				$cityId = $campaignData['city'];
				$count = 0;
				$schema = [];

				$occasionId = "";
				foreach ($sectionsData as $section)
				{
					//@see needs to be optimized
					$occasionId = is_null($section['occasionId']) ? 1 : $section['occasionId'];

					if ($isGetproducts)
					{
						$productIds = CampaignProducts::where('section_id', $section['id'])
						                              ->orderByRaw('-priority DESC')
						                              ->pluck('product_type_id', 'product_id')->take(8);
					}
					else
					{
						$productIds = CampaignProducts::where('section_id', $section['id'])
						                              ->orderByRaw('-priority DESC')
						                              ->pluck('product_type_id', 'product_id');
					}

					$products = $this->getProductsData($productIds, $cityId);
					array_push($schema, $this->schemaListItem($products, $campaignData->page_title, $campaignData->page_description));

					$sectionUrl = false;
					if (!(is_null($section['occasion_id'])))
					{
						$pId = "";
						$pTypeId = "";
						foreach ($productIds as $key => $value)
						{
							$pId = $key;
							$pTypeId = $value;
							break;
						}
						$sectionUrl = $this->getSeeAllLink($section['occasion_id'], $pTypeId, $pId, $cityId);
					}

					if (count($products) > 0)
					{
						$sectionsArrayData[$count] = [
							'sectionName'     => $section['section_name'],
							'sectionDesc'     => $section['section_description'],
							'occasionId'      => $section['occasion_id'],
							'sectionUrl'      => $sectionUrl,
							'sectionProducts' => $products
						];
						$count++;
					}
				}

				if (count($sectionsArrayData) > 0)
				{
					if ($isGetproducts)
					{
						return $sectionsArrayData;
					}
					$carouselData = CampaignGallery::where('campaign_id', $campaignId)
					                               ->get()
					                               ->toArray();

					$data = [
						'productsData' => $sectionsArrayData,
						'campaignData' => $campaignData->toArray(),
						'carouselData' => $carouselData,
						'schema'       => $schema,
						'occasionId'   => $occasionId,
						'cityUrl'      => $this->getCityUrl($cityId)
					];

					if (isset($campaignId) && in_array($campaignId, [27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40]))
					{
						$data['hideBrowseAll'] = true;
					}

					if (isset($campaignId) && in_array($campaignId, [27, 28, 31, 32, 35, 36]))
					{
						$data['extraHeight'] = true;
					}

					// Storing the session data for one week, since we are not modifying the data much
					//Cache::put($cacheKey, $data, config('evibe.cache.refresh-time'));
					Cache::put($cacheKey, $data, 60); // since option availability is included for vday todo: remove this

					return view("base.campaigns.campaigns-landing-page")->with(['data' => $data]);
				}
				else
				{

					if ($isGetproducts)
					{
						return "";
					}

					//do nothing, taking user to home
					return redirect('/');
				}
			}
			else
			{
				if ($isGetproducts)
				{
					return "";
				}

				//do nothing, taking user to home
				return redirect('/');
			}
		}
		else
		{
			if ($isGetproducts)
			{
				return "";
			}

			// Redirect to homepage
			return redirect('/');
		}
	}

	//Must be in sync with tables
	public function getCityUrl($cityId)
	{
		$url = false;
		switch ($cityId)
		{
			case config("evibe.city.Bengaluru"):
				$url = "bangalore";
				break;

			case config("evibe.city.Hyderabad"):
				$url = "hyderabad";
				break;
			case config("evibe.city.New Delhi"):
				$url = "delhi";
				break;
			case config("evibe.city.Mumbai"):
				$url = "mumbai";
				break;
			case config("evibe.city.Pune"):
				$url = "pune";
				break;
		}

		return $url;
	}

	public function getSeeAllLink($eventId, $productTypeId, $productId, $cityId)
	{
		$event = TypeEvent::find($eventId);
		$url = false;

		if ($event)
		{
			$cityUrl = getCityUrl($cityId);
			if ($cityUrl)
			{
				switch ($eventId)
				{
					case config("evibe.occasion.kids_birthdays.id"):
						if ($productTypeId == config("evibe.ticket.type.cake"))
						{
							$url = route('city.occasion.birthdays.cakes.list', $cityUrl);
						}
						elseif ($productTypeId == config("evibe.ticket.type.decor"))
						{
							$url = route('city.occasion.birthdays.decors.list', $cityUrl);
						}
						elseif ($productTypeId == config("evibe.ticket.type.package"))
						{
							$url = route('city.occasion.birthdays.packages.list', $cityUrl);
						}
						elseif ($productTypeId == config("evibe.ticket.type.entertainment"))
						{
							$url = route('city.occasion.birthdays.ent.list', $cityUrl);
						}
						elseif ($productTypeId == config("evibe.ticket.type.food"))
						{
							$url = route('city.occasion.birthdays.food.list', $cityUrl);
						}

						break;

					case config("evibe.occasion.surprises.id"):
						$productData = Package::where('id', $productId)
						                      ->first();

						if ($productData)
						{
							if ($productData->isCandleLightDinner($productData->id))
							{
								$url = route("city.cld.list", $cityUrl);
							}
							else
							{
								$url = route('city.occasion.surprises.package.list', $cityUrl);
							}
						}

						break;

					case config("evibe.occasion.house-warming.id"):
						if ($productTypeId == config("evibe.ticket.type.cake"))
						{
							$url = route('city.occasion.house-warming.cakes.list', $cityUrl);
						}
						elseif ($productTypeId == config("evibe.ticket.type.decor"))
						{
							$url = route('city.occasion.house-warming.decors.list', $cityUrl);
						}
						elseif ($productTypeId == config("evibe.ticket.type.priests"))
						{
							$url = route('city.occasion.house-warming.priest.list', $cityUrl);
						}
						elseif ($productTypeId == config("evibe.ticket.type.food"))
						{
							$url = route('city.occasion.house-warming.food.list', $cityUrl);
						}
						elseif ($productTypeId == config("evibe.ticket.type.tents"))
						{
							$url = route('city.occasion.house-warming.tent.list', $cityUrl);
						}

						break;

					case config("evibe.occasion.naming_ceremony.id"):
						if ($productTypeId == config("evibe.ticket.type.decor"))
						{
							$url = route('city.occasion.ncdecors.list', $cityUrl);
						}
						break;

					case config("evibe.occasion.baby-shower.id"):
						if ($productTypeId == config("evibe.ticket.type.decor"))
						{
							$url = route('city.occasion.bsdecors.list', $cityUrl);
						}
						break;
				}
			}
		}

		return $url;
	}

	public function isCampaignLive($campaignUrl)
	{
		$current_date = Carbon::now()->toDateTimeString();

		$live = CampaignPlan::where([
			                            ['expire_date', '>=', $current_date],
			                            ['url_slug', $campaignUrl]
		                            ])
		                    ->first();

		return $live;
	}

	public function getAreaName($areaId)
	{
		if (!(is_null($areaId)))
		{
			$areaDetails = Area::where('id', $areaId)->first();

			return $areaDetails['name'];
		}

		return null;
	}

	// public function getCarouselData($campaignId)
	// {
	// 	$imgData=CampaignGallery::where('id')
	// 							->get();
	// 	return $imgData;
	// }

	public function getProductsData($productsIds, $cityId)
	{
		$count = 0;
		$data = [];
		$productIds = [];

		foreach ($productsIds as $productId => $productTypeId)
		{
			$temp = [];
			// @todo: upgrade the switch case logic
			switch ($productTypeId)
			{
				case config("evibe.ticket.type.cake"): //Cake
					$productData = Cake::where('id', $productId)
					                   ->where('is_live', 1)
					                   ->first();
					if ($productData)
					{
						$prodImg = CakeGallery::where('cake_id', $productId)
						                      ->orderBy('is_profile', 'DESC')
						                      ->orderBy('updated_at', 'DESC')
						                      ->first()
						                      ->getLink();
						if ($productData->provider->rating)
						{
							$temp['reviewCount'] = $productData->provider->rating->review_count;
							$temp['avgRating'] = $productData->provider->rating->avg_rating;
						}
						$temp['occasionId'] = $productData->getOccasionIdForSearch(null);
						$temp["id"] = $productId;
						$temp['ptid'] = $productTypeId;
						$temp["title"] = $productData->title;
						$temp['price'] = $productData->price;
						$temp['img'] = $prodImg;
						$temp['priceworth'] = $productData->price_worth;
						$temp['metric'] = "/KG";
						$temp['link'] = $productData->getLink(null);
					}

					break;

				case config("evibe.ticket.type.decor"): //Decor
					$productData = Decor::where('id', $productId)
					                    ->where('is_live', 1)
					                    ->first();
					if ($productData)
					{
						$prodImg = DecorGallery::where('decor_id', $productId)
						                       ->orderBy('is_profile', 'DESC')
						                       ->orderBy('updated_at', 'DESC')
						                       ->first()
						                       ->getPath();
						$partnerRatings = $productData->provider->getPartnerReview();
						if ($partnerRatings)
						{
							$temp['avgRating'] = $partnerRatings['total']['avg'];
							$temp['reviewCount'] = $partnerRatings['total']['count'];
						}
						$temp['occasionId'] = $productData->getOccasionIdForSearch(null);
						$temp["id"] = $productId;
						$temp['ptid'] = $productTypeId;
						$temp["title"] = $productData->name;
						$temp['price'] = $productData->min_price;
						$temp['priceworth'] = $productData->worth;
						$temp['img'] = $prodImg;
						$temp['metric'] = "";
						$temp['link'] = $productData->getFullLinkForSearch($this->getCityUrl($cityId), null);
					}
					break;

				case config("evibe.ticket.type.entertainment"): // Entertainment
					$productData = TypeServices::where('id', $productId)
					                           ->where('is_live', 1)
					                           ->first();
					if ($productData)
					{
						$prodImg = TypeServices::where('id', $productId)
						                       ->first()
						                       ->getProfilePic();

						$temp['occasionId'] = $productData->getOccasionIdForSearch(null);
						$temp["id"] = $productId;
						$temp['ptid'] = $productTypeId;
						$temp["title"] = $productData->name;
						$temp['price'] = $productData->min_price;
						$temp['img'] = $prodImg;
						$temp['priceworth'] = $productData->worth_price;
						$temp['metric'] = "";
						$temp['link'] = $productData->getFullLinkForSearch($this->getCityUrl($cityId), null);
					}
					break;

				//Packages
				case config("evibe.ticket.type.package"):
				case config("evibe.ticket.type.tents"):

					$productData = Package::where('id', $productId)
					                      ->where('is_live', 1)
					                      ->first();
					if ($productData)
					{
						$prodImg = Package::where('id', $productId)
						                  ->first()
						                  ->getProfileImg();
						$area = $this->getAreaName($productData->area_id);
						$partnerRatings = $productData->provider->getPartnerReview();
						if ($partnerRatings)
						{
							$temp['avgRating'] = $partnerRatings['total']['avg'];
							$temp['reviewCount'] = $partnerRatings['total']['count'];
						}
						$temp['occasionId'] = $productData->event->id;
						$temp["id"] = $productId;
						$temp['ptid'] = $productTypeId;
						$temp["title"] = $productData->name;
						$temp['price'] = $productData->price;
						$temp['img'] = $prodImg;
						$temp['metric'] = "";
						$temp['priceworth'] = $productData->price_worth;
						$temp['area'] = $area;

						if ($temp['occasionId'] == 16)
						{
							$temp['link'] = $productData->getFullLinkForSearch($this->getCityUrl($cityId), null, 'surprises');
						}
						else
						{
							$temp['link'] = $productData->getFullLinkForSearch($this->getCityUrl($cityId), null, 'packages');
						}

					}
					break;

				//priests
				case config("evibe.ticket.type.priests"):
					$productData = Package::where('id', $productId)
					                      ->where('is_live', 1)
					                      ->first();

					if ($productData)
					{
						$prodImg = Package::where('id', $productId)
						                  ->first()
						                  ->getProfileImg();
						$partnerRatings = $productData->provider->getPartnerReview();
						if ($partnerRatings)
						{
							$temp['avgRating'] = $partnerRatings['total']['avg'];
							$temp['reviewCount'] = $partnerRatings['total']['count'];
						}
						$temp['occasionId'] = $productData->event->id;
						$temp["id"] = $productId;
						$temp['ptid'] = $productTypeId;
						$temp["title"] = $productData->name;
						$temp['price'] = $productData->price;
						$temp['img'] = $prodImg;
						$temp['metric'] = "";
						$temp['priceworth'] = $productData->price_worth;
						$temp['link'] = $productData->getFullLinkForSearch($this->getCityUrl($cityId), null, 'priest');
					}
					break;
				case config("evibe.ticket.type.food"):
					$productData = Package::where('id', $productId)
					                      ->where('is_live', 1)
					                      ->first();

					if ($productData)
					{
						$prodImg = Package::where('id', $productId)
						                  ->first()
						                  ->getProfileImg();
						$partnerRatings = $productData->provider->getPartnerReview();
						if ($partnerRatings)
						{
							$temp['avgRating'] = $partnerRatings['total']['avg'];
							$temp['reviewCount'] = $partnerRatings['total']['count'];
						}
						$temp['occasionId'] = $productData->event->id;
						$temp["id"] = $productId;
						$temp['ptid'] = $productTypeId;
						$temp["title"] = $productData->name;
						$temp['price'] = $productData->price;
						$temp['img'] = $prodImg;
						$temp['metric'] = "";
						$temp['priceworth'] = $productData->price_worth;
						$temp['link'] = $productData->getFullLinkForSearch($this->getCityUrl($cityId), null, 'food');
					}
					break;

				//Surprices
				case config("evibe.ticket.type.surprises"):
					$productData = Package::where('id', $productId)
					                      ->where('is_live', 1)
					                      ->first();

					if ($productData)
					{
						$prodImg = Package::where('id', $productId)
						                  ->first()
						                  ->getProfileImg();
						$partnerRatings = $productData->provider->getPartnerReview();
						if ($partnerRatings)
						{
							$temp['avgRating'] = $partnerRatings['total']['avg'];
							$temp['reviewCount'] = $partnerRatings['total']['count'];
						}
						$temp['occasionId'] = $productData->event->id;
						$temp["id"] = $productId;
						$temp['ptid'] = $productTypeId;
						$temp["title"] = $productData->name;
						$temp['price'] = $productData->price;
						$temp['metric'] = "";
						$temp['priceworth'] = $productData->price_worth;
						$temp['img'] = $prodImg;
						$temp['link'] = $productData->getFullLinkForSearch($this->getCityUrl($cityId), null, 'surprises');

						$area = $this->getAreaName($productData->area_id);
						$temp['area'] = $area;
					}
					break;
			}

			if ($temp && isset($temp['id']) && $temp['id'])
			{
				array_push($productIds, $temp['id']);
			}

			$data[$count] = $temp;
			$count++;
		}

		// @complete: generic
		/*
		if(count($productIds) && count($data)) // obvious
		{
			// option availability
			// @see: get min. slots available - to create urgency
			$optionAvailability = OptionAvailability::whereIn('map_id', $productIds)->get();
			if(count($optionAvailability))
			{

			}
		}
		*/

		// @todo: remove after vday
		$availabilityDate = "2020-02-14 00:00:00";
		$availabilityDateStart = Carbon::createFromTimestamp(strtotime($availabilityDate))->startOfDay()->timestamp;
		$availabilityDateEnd = Carbon::createFromTimestamp(strtotime($availabilityDate))->endOfDay()->timestamp;
		$vDayVenuePackages = Package::select('planner_package.id', 'planner_package.code', 'planner_package.name', 'planner_package.price', 'venue.name AS partner_name', 'planner_package.map_id AS partner_id', 'planner_package.map_type_id AS partner_type_id', 'area.name AS area')
		                            ->join('planner_package_tags', 'planner_package_tags.planner_package_id', '=', 'planner_package.id')
		                            ->join('venue', 'venue.id', '=', 'planner_package.map_id')
		                            ->leftJoin('area', 'area.id', '=', 'planner_package.area_id')
		                            ->where('planner_package_tags.tile_tag_id', config('evibe.type-tag.valentines-day-2020'))
		                            ->where('planner_package.map_type_id', config('evibe.ticket.type.venue'))
		                            ->where('planner_package.is_live', 1)
		                            ->where('planner_package.type_ticket_id', config('evibe.ticket.type.surprises'))
		                            ->where('planner_package.city_id', $cityId)
		                            ->get();

		$vDayVenuePackageIds = $vDayVenuePackages->pluck('id');

		$optionAvailabilitySlots = OptionAvailability::where('map_type_id', config('evibe.ticket.type.surprises'))
		                                             ->whereIn('map_id', $vDayVenuePackageIds)
		                                             ->get();

		$bookings = TicketBooking::select('ticket_bookings.id', 'ticket_bookings.party_date_time', 'ticket.id AS ticket_id', 'ticket_mapping.map_id AS option_id', 'ticket_mapping.map_type_id AS option_type_id')
		                         ->join('ticket_mapping', 'ticket_mapping.id', '=', 'ticket_bookings.ticket_mapping_id')
		                         ->join('ticket', 'ticket.id', '=', 'ticket_mapping.ticket_id')
		                         ->whereIn('ticket_mapping.map_id', $vDayVenuePackageIds)
		                         ->whereNull('ticket_bookings.deleted_at')
		                         ->whereNull('ticket_mapping.deleted_at')
		                         ->whereNull('ticket.deleted_at')
		                         ->whereNull('ticket_bookings.cancelled_at')
		                         ->where('ticket.status_id', config('evibe.ticket.status.booked'))
		                         ->where('ticket_bookings.is_advance_paid', 1)
		                         ->where('ticket_bookings.party_date_time', '>=', $availabilityDateStart)
		                         ->where('ticket_bookings.party_date_time', '<=', $availabilityDateEnd)
		                         ->get();

		if (count($data))
		{
			foreach ($data as $key => $datum)
			{
				if (isset($datum['id']) && $datum['id'] && isset($datum['ptid']) && $datum['ptid'] && in_array($datum['id'], $vDayVenuePackageIds->toArray()))
				{
					$productSlots = $optionAvailabilitySlots->where('map_id', $datum['id'])
					                                        ->where('map_type_id', $datum['ptid']);

					// @see: least but not zero
					// since slots are of same day for a product
					if (count($productSlots))
					{
						foreach ($productSlots as $slot)
						{
							$availableSlots = $slot->bookings_available;

							$todaySlotStart = Carbon::createFromTimestamp(strtotime($slot->slot_start_time))->timestamp;
							$todaySlotEnd = Carbon::createFromTimestamp(strtotime($slot->slot_end_time))->timestamp;
							$todayStart = Carbon::createFromTimestamp(strtotime($slot->slot_start_time))->startOfDay()->timestamp;

							$availabilitySlotStart = $availabilityDateStart + ($todaySlotStart - $todayStart);
							$availabilitySlotEnd = $availabilityDateStart + ($todaySlotEnd - $todayStart);

							$slotBookings = $bookings->where('option_id', $datum['id'])
							                         ->where('option_type_id', $datum['ptid'])
							                         ->where('party_date_time', '>=', $availabilitySlotStart)
							                         ->where('party_date_time', '<', $availabilitySlotEnd)
							                         ->count();

							$availableSlots = $availableSlots - $slotBookings;
							if ($availableSlots < 0)
							{
								$availableSlots = 0;
							}

							if (!isset($data[$key]['availableSlots']))
							{
								$data[$key]['availableSlots'] = $availableSlots;
							}
							elseif ($availableSlots < $data[$key]['availableSlots'])
							{
								$data[$key]['availableSlots'] = $availableSlots;
							}
						}
					}
				}
			}
		}

		return $data;
	}
}