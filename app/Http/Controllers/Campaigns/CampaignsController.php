<?php

namespace App\Http\Controllers\Campaigns;

use App\Http\Controllers\Base\BaseController;
use App\Http\Controllers\Base\BasePackageController;
use App\Models\Package\Package;
use App\Models\Types\TypeEvent;

/**
 * @author Anji <anji@evibe.in>
 * @since  3 Apr 2015
 */
class CampaignsController extends BaseController
{
	public function showListPage($campaignUrl)
	{
		$occasion = TypeEvent::where('url', 'LIKE', $campaignUrl)->first();
		if (!$occasion)
		{
			return redirect('');
		}

		$packages = Package::isLive()
		                   ->forEvent($occasion->id)
		                   ->select('planner_package.*')
		                   ->get();

		// seo
		$seo = $this->getSeoText(["campaigns", $occasion->id, "list"], "campaigns");

		$data = [
			'packages'    => $packages,
			'occasionId'  => $occasion->id,
			'campaignUrl' => $campaignUrl,
			'cityId'      => null,
			'mapTypeId'   => config('evibe.ticket.type.package'),
			"seo"         => [
				'pageTitle'       => $seo['pt'],
				'pageHeader'      => $seo['ph'],
				'pageDescription' => $seo['pd'],
				'keyWords'        => $seo['kw']
			]
		];

		$host = config('evibe.host');
		$data['profileBaseUrl'] = "$host/c/" . $campaignUrl . "/";

		return view('campaign.util.list', ['data' => $data]);
	}

	public function showProfilePage($campaignUrl, $packageUrl)
	{
		$package = Package::isLive()
		                  ->where('url', 'LIKE', $packageUrl)
		                  ->get()
		                  ->first();

		if (!$package)
		{
			return redirect(route('campaign.list', $campaignUrl));
		}
		$basePackageGallery = new BasePackageController();
		$gallery = $basePackageGallery->getPackageCategorisedGallery($package->id, $package->name);

		$pageUrl = request()->capture()->fullUrl();
		$packageTitle = $package->name . "Raksha Bandhan Gift";
		$metaImage = "https://gallery.evibe.in/img/logo/logo_evibe.png";
		$shareUrl = $basePackageGallery->getShareUrl($pageUrl, $packageTitle, "#" . $campaignUrl, $metaImage);

		$replaces = ['{item}' => strtolower($package->name)];
		$seo = $this->getSeoText(["campaigns", $package->event_id, "profile"], "campaigns", $replaces);

		$data = [
			'package'     => $package,
			'campaignUrl' => $campaignUrl,
			'gallery'     => $gallery,
			'occasionId'  => $package->event_id,
			'cityId'      => null,
			'mapTypeId'   => config('evibe.ticket.type.package'),
			'shareUrl'    => $shareUrl,
			"seo"         => [
				'pageTitle'       => $seo['pt'],
				'pageHeader'      => $seo['ph'],
				'pageDescription' => $seo['pd'],
				'keyWords'        => $seo['kw']
			]
		];

		return view('campaign.util.profile', ['data' => $data]);
	}

	public function showSimilarPackages($campaignUrl, $packageId)
	{
		$occasion = TypeEvent::where('url', 'LIKE', $campaignUrl)->first();
		if (!$occasion)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => "Occasion Id not found"
			                        ]);
		}

		$packages = Package::isLive()
		                   ->where('event_id', $occasion->id)
		                   ->where('id', '!=', $packageId)
		                   ->select('planner_package.*')
		                   ->take(4)
		                   ->get();
		if (!$packages)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => "Package Id not found"
			                        ]);
		}

		$data = [
			'packages'    => $packages,
			'campaignUrl' => $campaignUrl
		];

		return view('campaign.util.similar-packages', ['data' => $data]);
	}
}