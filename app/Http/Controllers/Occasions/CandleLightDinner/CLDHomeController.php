<?php

namespace App\Http\Controllers\Occasions\CandleLightDinner;

use App\Http\Controllers\Base\BasePackageController;
use App\Models\Package\Package;
use App\Models\Package\PackageTag;
use App\Models\Types\TypeTag;
use App\Models\Util\Area;
use App\Models\Venue\Venue;
use Illuminate\Support\Facades\Cache;

class CLDHomeController extends BasePackageController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionId(config('evibe.occasion.surprises.id'));
		$this->setPageId(config('evibe.ticket.type.surprises'));
		$this->setOccasionUrl(config('evibe.occasion.surprises.cld.url'));
		$this->setPageName(config('evibe.occasion.surprises.cld.name'));
	}

	public function showOptions($cityUrl, $location = null, $fetchWithoutView = false)
	{
		/*
		 * validate URL params & storing the cityId in sessions, sharing the cityid variable across the views with view::share
		 * If city not found redirecting to the home page/city home page
		 */
		$urlParams = [['key' => 'city', 'value' => $cityUrl]];
		$validUrlObj = $this->validateUrlParams($urlParams);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		/*
		 * Caching the fetched data with all the filters applied
		 */
		$cacheKey = $this->deviceSpecificCacheKey(request()->fullUrl());
		$cacheKey = base64_encode($cacheKey);
		if (Cache::has($cacheKey) && !($fetchWithoutView))
		{
			$listData = Cache::get($cacheKey);
		}
		else
		{
			$cityId = getCityId();
			$cityName = getCityName();
			$eventId = $this->getOccasionId();
			$eventUrl = $this->getOccasionUrl();
			$pageId = $this->getPageId();
			$pageName = $this->getPageName();
			$clearFilter = false;
			$active = false;
			$hidePriceCategories = false;
			$categoryUrl = request()->has('category') ? request('category') : '';

			$schemaBreadcrumbList = [
				"/"                   => "Evibe.in",
				"/$cityUrl"           => $cityName,
				"/$cityUrl/$eventUrl" => getTextFromSlug($pageName)
			];

			/**
			 * Getting candle light dinner package Ids & fetch packages from it
			 */
			$packageIds = PackageTag::where('tile_tag_id', config("evibe.type-tag.candle-light-dinners"))
			                        ->pluck("planner_package_id")
			                        ->toArray();

			/* VDay 2020 CLD packages shouldn't be shown in normal Surprises List Pages */
			$vDayPackageIds = PackageTag::where('tile_tag_id', config("evibe.type-tag.valentines-day-2020"))
			                            ->pluck("planner_package_id")
			                            ->toArray();

			$packages = Package::whereIn('id', $packageIds)
			                   ->whereNotIn('id', $vDayPackageIds)
			                   ->isLive()
			                   ->forEvent($eventId)
			                   ->forCity()
			                   ->select('planner_package.*')
			                   ->forPage($pageId);

			/**
			 * Search filter
			 */
			$userSearch = "";
			if (request()->has('search') && trim(request()->get('search')))
			{
				$clearFilter = true;
				$userSearch = trim(request()->get('search'));
				$searchWords = explode(' ', $userSearch);

				$packages->where(function ($searchQuery) use ($searchWords) {
					foreach ($searchWords as $word)
					{
						$searchQuery->orWhere('planner_package.name', 'LIKE', "%$word%")
						            ->orWhere('planner_package.code', 'LIKE', "%$word%")
						            ->orWhere('planner_package.price', 'LIKE', "%$word%");
					}
				});
			}

			/**
			 * Price filter
			 */
			$priceFilterApplied = false;
			$minPrices = $packages->pluck('price');
			$maxPrices = $packages->pluck('price_max');
			$calMinPrice = $packages->pluck('price')->min();
			$calMinPrice = $calMinPrice ? $calMinPrice : 0; // setting to 0 if $calMinPrice is null
			$calMaxPrice = ($maxPrices->max() > $minPrices->max()) ? $maxPrices->max() : $minPrices->max();
			$priceMin = request()->get('price_min') ?: 0;
			$priceMax = request()->get('price_max') ?: 0;
			if ($priceMin || $priceMax)
			{
				$priceFilterApplied = true;
			}
			else
			{
				$priceMin = $priceMin ? $priceMin : $calMinPrice;
				$priceMax = ($priceMax && $priceMax >= $priceMin) ? $priceMax : $calMaxPrice;
			}

			if ($priceMin > $priceMax)
			{
				$priceTemp = $priceMax;
				$priceMax = $priceMin;
				$priceMin = $priceTemp;
			}

			// user has changed min / max price
			if ($calMinPrice != $priceMin || $calMaxPrice != $priceMax)
			{
				$clearFilter = true;
				$packages->whereBetween('planner_package.price', [$priceMin, $priceMax]);
			}

			/**
			 * Location filter
			 */
			$seoLocation = "";
			$allLocations = $this->getPackagesLocationDataForFilters($cityId, $eventId);
			$userLocation = $this->trimSpace($location);
			if ($userLocation && in_array($userLocation, $allLocations['urls']))
			{
				$validLocation = Area::where('url', $userLocation)->first();
				if ($validLocation)
				{
					$clearFilter = true;
					$seoLocation = $location . ",";
					$validLocationId = $validLocation->id;

					$packages->where(function ($query1) use ($validLocationId) {
						$query1->where(function ($query2) use ($validLocationId) {
							$query2->where('planner_package.map_type_id', config('evibe.ticket.type.venue'))
							       ->where('planner_package.area_id', $validLocationId);
						})->orWhere('planner_package.map_type_id', '!=', config('evibe.ticket.type.venue'));
					});

					//$venueIds = Venue::whereIn('id', $packages->pluck("map_id"))
					//                 ->where('area_id', $validLocation->id)
					//                 ->whereNull('deleted_at')
					//                 ->pluck("id")
					//                 ->toArray();
					//
					//$packages->where(function ($query1) use ($venueIds) {
					//	$query1->where(function ($query2) use ($venueIds) {
					//		$query2->where('planner_package.map_type_id', config('evibe.ticket.type.venue'))
					//		       ->whereIn('map_id', $venueIds);
					//	})->orWhere('planner_package.map_type_id', '!=', config('evibe.ticket.type.venue'));
					//});

					$schemaBreadcrumbList["/$cityUrl/$eventUrl/$userLocation"] = $validLocation->name;
				}
				else
				{
					$location = "all";
				}
			}

			/**
			 * SEO schema related queries to get data based on the selected filters/options/category
			 */
			$seoKey = 'list';
			$seoReplaces = [
				'{category_name}'  => "Candle Light Dinners",
				'{category_title}' => "Candle Light Dinners",
				'{category_desc}'  => "Candle Light Dinners",
				'{relation}'       => '',
				'{occasion}'       => '',
				'{area}'           => $seoLocation
			];

			// filter:category - $seokey preference will be given for category
			if ($categoryUrl)
			{
				$categoryName = $categoryUrl ? ucwords(str_replace('-', ' ', $categoryUrl)) : '';

				// @todo: get values from database
				$seoReplaces['{category_name}'] = $categoryName;
				$seoReplaces['{category_title}'] = $categoryName;
				$seoReplaces['{category_desc}'] = $categoryName;

				$userFilterTag = TypeTag::where('url', $categoryUrl)->first();

				if ($userFilterTag)
				{
					$surPackageIds = PackageTag::where('tile_tag_id', $userFilterTag->id)
					                           ->pluck('planner_package_id')
					                           ->toArray();

					$packages->whereIn('planner_package.id', $surPackageIds);
				}

				$active = $categoryUrl;
			}

			$seoConfigKey = "defaults.city.$eventId.candle-light-dinner.list";
			$seo = $this->getSeoText([$cityId, $eventId, $pageId, $seoKey], $seoConfigKey, $seoReplaces);
			$pageTitle = ucwords($seo['pt']);
			$pageDescription = $seo['pd'];
			$pageHeader = ucwords($seo['ph']);
			$keyWords = $seo['kw'];

			$totalPackagesCount = $packages->pluck("id")->count();

			$perPage = config('evibe.paginate-options.generic', 52);
			if ($this->agent->isMobile() && !($this->agent->isTablet()))
			{
				$perPage = config('evibe.paginate-options.generic-mobile', 50);
			}
			if ($fetchWithoutView)
			{
				$perPage = 8;
			}

			/**
			 * sort results
			 * if default (popularity), sort by priority
			 * else sort by user input (ex: price, new-arrivals)
			 */
			$sortParams = $this->getSortParams('planner_package', ['price' => 'planner_package.price']);
			$isTaggingLive = 0;
			if (getcityUrl() == "bangalore")
			{
				$customTaggedProducts = [
					'745'  => "Best Selling",
					'2256' => "Trending",
					'812'  => "Luxury",
					'2735' => "Limited Edition"
				];
			}
			elseif (getcityUrl() == "hyderabad")
			{
				$customTaggedProducts = [
					'1849' => "Best Selling",
					'2171' => "Trending",
					'2481' => "Luxury",
					'2479' => "Limited Edition"
				];
			}
			else
			{
				$customTaggedProducts = [];
			}

			if (!$sortParams['isDefault'])
			{
				// groupBy is fix for distinct
				$packages = $packages->groupBy('planner_package.id')
				                     ->orderBy($sortParams['entity'], $sortParams['order']);

				// fetch page specific options - get()
				$currentPageOptions = $this->getCurrentPageOption($packages, $perPage);
				$packages = $currentPageOptions['options'];
				$currentPage = $currentPageOptions['currentPage'];
				$packages = $packages->all();
			}
			else
			{
				$allPackageIds = $packages->pluck('id')->all(); // get all IDs for merge

				$packages = $packages->groupBy('planner_package.id')
				                     ->orderBy('planner_package.created_at', 'DESC')->get(); // default sort

				$packages = $packages->all();

				$priorityIds = $this->getItemIdsByPriority($eventId, $pageId, $allPackageIds); // get priorities

				//Merge Tagged Products with custom Tags
				if ($this->isLiveProducts(array_keys($customTaggedProducts)))
				{
					$isTaggingLive = 1;
					$priorityIds = array_unique(array_merge(array_keys($customTaggedProducts), $priorityIds));
				}

				usort($packages, function ($a, $b) use ($priorityIds) {
					return $this->sortItemsByPriority($a, $b, $priorityIds);
				});

				// fetch page specific options - get()
				$currentPageOptions = $this->getCurrentPageOption($packages, $perPage);
				$packages = $currentPageOptions['options'];
				$currentPage = $currentPageOptions['currentPage'];

				$packages = $packages->all();

				if ($isTaggingLive == 1 && ($this->agent->isMobile() || $this->agent->isTablet()))
				{
					$firstSlice = array_slice($packages, 0, 7);
					$secondSlice = array_slice($packages, 7);

					shuffle($firstSlice);
					$packages = array_merge($firstSlice, $secondSlice);
				}
			}

			// fetch paginated packages
			$packageList = $this->getPaginatedItems($totalPackagesCount, $packages, $currentPage, $perPage);

			/**
			 * SEO: Schema
			 */

			/* breadcrumb */
			$schema = [
				$this->schemaBreadcrumbList($schemaBreadcrumbList)
			];

			/* product */
			$productsData = [];
			foreach ($packageList as $package)
			{
				$productsData[] = [
					'category'    => $this->getPageName(),
					'sku'         => $package->code,
					'description' => $package->info,
					'name'        => $package->name,
					'image'       => $package->getProfileImg(),
					'price'       => $package->price,
					'url'         => config("evibe.host") . "/" . getCityUrl() . "/" . $package->url . "/" . $this->getOccasionUrl()
				];
			}
			array_push($schema, $this->schemaListItem($productsData, $pageHeader, $pageDescription));

			// data related to price categories
			$priceCategoryData = [
				'min'          => $calMinPrice,
				'max'          => $calMaxPrice,
				'mapTypeId'    => config('evibe.ticket.type.package'),
				'typeTicketId' => $this->getPageId(),
				'eventId'      => $eventId,
				'cityId'       => $cityId
			];

			$data = [
				'packages'      => $packageList,
				'totalCount'    => $totalPackagesCount,
				'occasionId'    => $this->getOccasionId(),
				'cityId'        => getCityId(),
				'mapTypeId'     => $this->getPageId(),
				'sort'          => $sortParams['sortType'],
				'filters'       => [
					'queryParams'         => $this->getExistingQueryParams(),
					'active'              => $active,
					'priceMin'            => $priceMin,
					'priceMax'            => $priceMax,
					'priceCategories'     => $this->getPriceCategoryFilters($priceCategoryData),
					'hidePriceCategories' => $hidePriceCategories,
					'search'              => $userSearch,
					'clearFilter'         => $clearFilter,
					'category'            => $categoryUrl,
					'priceFilterApplied'  => $priceFilterApplied
				],
				'seo'           => [
					'pageTitle'       => $pageTitle,
					'pageHeader'      => $pageHeader,
					'pageDescription' => $pageDescription,
					'keyWords'        => $keyWords,
					'schema'          => $schema
				],
				'taggedItems'   => $customTaggedProducts,
				'isTaggingLive' => $isTaggingLive
			];
			if ($eventId == config('evibe.occasion.surprises.id'))
			{
				$data['filters']['location'] = $location;
				$data['filters']['allLocations'] = $allLocations;
			}
			if ($fetchWithoutView)
			{
				return $data['packages'];
			}
			$listData = array_merge($data, $this->mergeView($cityUrl, $eventId));

			Cache::put($cacheKey, $listData, config('evibe.cache.refresh-time'));
		}

		$listData['profileBaseUrl'] = config('evibe.host') . "/" . $cityUrl . "/" . $this->getOccasionUrl() . "/" . config('evibe.occasion.surprises.package.profile_url') . "/";

		$view = $this->renderDataToViewFiles('occasion.candle-light-dinner.list', $listData);

		return $view;
	}

	public function isLiveProducts($packageIds)
	{
		$countLiveItems = Package::whereIn('id', $packageIds)
		                         ->isLive()
		                         ->count();

		if (count($packageIds) == $countLiveItems)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	public function showProfileData($cityUrl, $packageUrl)
	{
		$hashTag = "#" . config('evibe.occasion.surprises.hashtag');
		$profileData = $this->getProfileData($cityUrl, $packageUrl, $hashTag);

		if (!is_array($profileData))
		{
			return $profileData; // validation failed (or) has custom view
		}

		$profileData = $this->addProductSpecificAddOns($profileData, config('evibe.occasion.surprises.id'), config('evibe.ticket.type.surprises'), $profileData['package']->id);
		$profileData['backUrl'] = route("city.cld.list", getCityUrl());

		$christmasIds = config('evibe.campaign-packages.christmas') ?: "";
		if (is_string($christmasIds))
		{
			$christmasIds = explode(',', preg_replace('/\s/', '', $christmasIds));
		}

		$newYearIds = config('evibe.campaign-packages.new-year') ?: "";
		if (is_string($newYearIds))
		{
			$newYearIds = explode(',', preg_replace('/\s/', '', $newYearIds));
		}

		$valentinesDayIds = Package::join('planner_package_tags', 'planner_package_tags.planner_package_id', '=', 'planner_package.id')
		                           ->select('planner_package.*')
		                           ->where('planner_package_tags.tile_tag_id', config('evibe.type-tag.valentines-day-2020'))
		                           ->where('planner_package.type_ticket_id', config('evibe.ticket.type.surprises'))
		                           ->where('planner_package.is_live', 1)
		                           ->where('planner_package.map_type_id', 3) // @see: should be applicable only for VDay CLDs
		                           ->whereNull('planner_package.deleted_at')
		                           ->whereNull('planner_package_tags.deleted_at')
		                           ->pluck('planner_package.id')
		                           ->toArray();

		if (in_array($profileData['package']->id, $christmasIds) && config('evibe.campaign-dates.christmas'))
		{
			$profileData['campaignDates'] = [];

			foreach (config('evibe.campaign-dates.christmas') as $date)
			{
				array_push($profileData['campaignDates'], date('Y/m/d', strtotime($date)));
			}
		}
		elseif (in_array($profileData['package']->id, $newYearIds) && config('evibe.campaign-dates.new-year'))
		{
			$profileData['campaignDates'] = [];

			foreach (config('evibe.campaign-dates.new-year') as $date)
			{
				array_push($profileData['campaignDates'], date('Y/m/d', strtotime($date)));
			}
		}
		elseif (in_array($profileData['package']->id, $valentinesDayIds) && config('evibe.campaign-dates.valentines-day'))
		{
			$profileData['campaignDates'] = [];
			$profileData['campaignText'] = "Reservations made for non valentine's day (14 Feb 2021) may have limitations in the inclusions. We will keep you posted through order updates.";
			$profileData['campaignFocusDate'] = "2021/02/14";

			foreach (config('evibe.campaign-dates.valentines-day') as $date)
			{
				if ($date >= date('Y-m-d', time()))
				{
					array_push($profileData['campaignDates'], date('Y/m/d', strtotime($date)));
				}
			}

			// disabling add-ons for vday clds
			// todo: remove
			unset($profileData['addOns']);
		}

		$profileData['isCLD'] = true;

		return $this->renderDataToViewFiles('occasion/surprises/packages/profile', $profileData);
	}
}