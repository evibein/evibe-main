<?php

namespace App\Http\Controllers\Occasions\PrePost;

use App\Http\Controllers\Base\BaseHomeController;
use App\Models\Util\Country;

class PrePostHomeController extends BaseHomeController
{
	public function __construct()
	{
		$this->setOccasionUrl(config('evibe.occasion.pre-post.url'));
		$this->setOccasionId(config('evibe.occasion.pre-post.id'));

		parent::__construct();
	}

	public function showHome($cityUrl)
	{
		$validUrlObj = $this->validateUrlParams($cityUrl);

		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$cityId = getCityId();
		$eventId = $this->getOccasionId();
		$customerStories = $this->getCustomerStories([$eventId => 5]);
		$vendorStories = $this->getVendorStories();

		$topVenues = $this->getTopVenues($eventId);
		$topDecors = $this->getTopDecors($eventId);
		$topEntServices = $this->getTopEntServices($eventId);
		$topCakes = $this->getTopCakes($eventId);

		// fetches the collections for pre-post home
		$eventUrl = $this->getOccasionUrl();
		$cityUrl = getCityUrl();
		$host = config('evibe.host');
		$profileUrl = config('evibe.occasion.collections.profile_url');
		$collectionBaseUrl = "$host/$cityUrl/$eventUrl/$profileUrl/";
		$collections = $this->getCollections($cityId, $eventId, 8);

		$isShowTopContainer = count($topVenues) || count($topDecors) || count($topEntServices) || count($topCakes);

		// SEO
		$cityName = getCityName();
		$pageTitle = "Best wedding reception, engagement party planners, party halls, flower decorators, cakes in $cityName";
		$pageDescription = "Plan best engagement party, wedding reception in {{ $cityName }}. Best deals for party halls,
			flower decorators, stage decorations, entertainment options, cakes with quality service.";
		$pageHeader = "";

		$data = [
			'collectionBaseUrl'  => $collectionBaseUrl,
			'collections'        => $collections,
			'customerStories'    => $customerStories,
			'vendorStories'      => $vendorStories,
			'topVenues'          => $topVenues,
			'topDecors'          => $topDecors,
			'topEntOptions'      => $topEntServices,
			'topCakes'           => $topCakes,
			'occasionId'         => $this->getOccasionId(),
			'cityId'             => getCityId(),
			'isShowTopContainer' => $isShowTopContainer,
			'countries'          => Country::all(),
			'seo'                => [
				'pageTitle'       => $pageTitle,
				'pageDescription' => $pageDescription,
				'pageHeader'      => $pageHeader
			]
		];

		$data = array_merge($data, $this->mergeView(getCityUrl(), $this->getOccasionId()));

		return view('occasion/pre-post/home/home', ['data' => $data]);
	}

}