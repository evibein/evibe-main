<?php

namespace App\Http\Controllers\Occasions\PrePost;

use App\Http\Controllers\Base\BaseCakeController;

class PrePostCakeController extends BaseCakeController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.pre-post.url'));
		$this->setOccasionId(config('evibe.occasion.pre-post.id'));
		$this->setPageName(config('evibe.occasion.pre-post.cakes.name'));
		$this->setPageUrl(config('evibe.occasion.pre-post.cakes.results_url'));
	}

	public function showList($cityUrl)
	{
		$listData = $this->getCakeListData($cityUrl);
		if (!is_array($listData))
		{
			return $listData; // has error, redirect to given url
		}

		return $this->renderDataToViewFiles('occasion/pre-post/cake/list', $listData);
	}

	public function showProfile($cityUrl, $cakeUrl)
	{
		$hashTag = "#" . config('evibe.occasion.pre-post.hashtag');
		$profileData = $this->getCakeProfileData($cityUrl, $cakeUrl, $hashTag);
		if (!is_array($profileData))
		{
			return $profileData; // validation failed, redirect
		}

		return $this->renderDataToViewFiles('occasion/pre-post/cake/profile', $profileData);
	}
}