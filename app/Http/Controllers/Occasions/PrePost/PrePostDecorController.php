<?php

namespace App\Http\Controllers\Occasions\PrePost;

use App\Http\Controllers\Base\BaseDecorController;

class PrePostDecorController extends BaseDecorController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.pre-post.url'));
		$this->setOccasionId(config('evibe.occasion.pre-post.id'));
		$this->setPageName(config('evibe.occasion.pre-post.decors.name'));
		$this->setPageUrl(config('evibe.occasion.pre-post.decors.results'));
	}

	public function showList($cityUrl, $categoryUrl = "")
	{
		$listData = $this->getDecorsListProductData($cityUrl, $categoryUrl);
		if (!is_array($listData))
		{
			return $listData; // has error, redirect to given url
		}
		return $this->renderDataToViewFiles('occasion/pre-post/decors/list', $listData);
	}

	public function showProfile($cityUrl, $decorUrl)
	{
		$profileData = $this->getDecorProfileData($cityUrl, $decorUrl);
		if (!is_array($profileData))
		{
			return $profileData; // validation error, redirect to given url
		}

		return $this->renderDataToViewFiles('occasion/pre-post/decors/profile', $profileData);
	}
}