<?php

namespace App\Http\Controllers\Occasions\PrePost;

use App\Http\Controllers\Base\BaseEntController;

class PrePostEntController extends BaseEntController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.pre-post.url'));
		$this->setOccasionId(config('evibe.occasion.pre-post.id'));
		$this->setPageName(config('evibe.occasion.pre-post.ent.name'));
		$this->setPageUrl(config('evibe.occasion.pre-post.ent.results_url'));
	}

	public function showList($cityUrl, $categoryUrl = "")
	{
		$profileRoute = "city.occasion.pre-post.ent.profile";
		$listData = $this->getEntListData($cityUrl, $profileRoute, $categoryUrl);
		if (!is_array($listData))
		{
			return $listData; // validation failed, redirect
		}

		return $this->renderDataToViewFiles('occasion/pre-post/ent/list', $listData);
	}

	public function showDetails($cityUrl, $entUrl)
	{
		$hashTag = "#" . config('evibe.occasion.pre-post.hashtag');
		$profileData = $this->getEntProfileData($cityUrl, $entUrl, $hashTag);
		if (!is_array($profileData))
		{
			return $profileData; // validation failed, redirect
		}

		return $this->renderDataToViewFiles('occasion/pre-post/ent/profile', $profileData);
	}
}