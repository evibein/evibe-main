<?php

namespace App\Http\Controllers\Occasions\PrePost;

use App\Http\Controllers\Base\BaseSiteVisitController;
use App\Jobs\Emails\Tickets\MailSiteVisitAlertToTeamJob;
use App\Jobs\Emails\Tickets\MailSiteVisitSuccessToCustomerJob;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketSiteVisit;
use App\Models\Util\City;
use App\Models\Util\SiteErrorLog;

class PrePostSiteVisitController extends BaseSiteVisitController
{
	public function __construct()
	{
		parent::__construct();
	}

	public function saveSiteVisitEnquiry($cityUrl)
	{
		$inputData = request()->all();
		$city = City::where('url', $cityUrl)->first();

		$ticketData = [
			"city_id"           => $city ? $city->id : 1, // by default
			"event_id"          => config('evibe.occasion.pre-post.id'),
			"name"              => $inputData["svContactName"],
			"phone"             => $inputData["svContactPhone"],
			"email"             => $inputData["svContactEmail"],
			"alt_phone"         => $inputData["svContactAltNumber"],
			"event_date"        => strtotime($inputData["personPartyDate"]),
			"party_location"    => $inputData["svEventLocation"],
			"enquiry_source_id" => config('evibe.ticket.enquiry_source.site_visit'),
			"comments"          => ""
		];

		// save ticket
		$ticket = Ticket::create($ticketData);
		if (!$ticket)
		{
			SiteErrorLog::create([
				                     "code"    => "EVB-M-501",
				                     "details" => "Failed to create ticket, email: " . request("email") . ", phone: " . request("phone")
			                     ]);

			return response()->json([
				                        "success" => false,
				                        "error"   => "An error occurred while submitting your request. Please try again later."
			                        ]);
		}

		$this->updateTicketAction([
			                          'ticket'   => $ticket,
			                          'comments' => "Site visit ticket created by customer"
		                          ]);

		$ticketId = $ticket->id;
		$enquiryId = config("evibe.ticket.enq.pre.site_visit") . $ticketId;
		$ticket->enquiry_id = $enquiryId;
		$ticket->save();

		$personOtherEvent = "";
		$selectedServices = "";

		if (isset($inputData["personOtherEvent"]))
		{
			foreach ($inputData["personOtherEvent"] as $otherEvent)
			{
				$personOtherEvent = $personOtherEvent . "<li>" . $otherEvent . "</li>";
			}
		}

		foreach ($inputData["svSelectedServices"] as $selectedService)
		{
			$selectedServices = $selectedServices . "<li>" . $selectedService . "</li>";
		}

		$ticketSiteVisitData = [
			"ticket_id"             => $ticketId,
			"who_getting_married"   => $inputData["marriagePerson"],
			"person_event"          => $inputData["personEvent"],
			"person_other_events"   => "<ul>" . $personOtherEvent . "</ul>",
			"address_line1"         => $inputData["svAddressLine1"],
			"address_line2"         => $inputData["svAddressLine2"],
			"zip_code"              => $inputData["svZipCode"],
			"person_location"       => $inputData["svLocation"],
			"selected_services"     => "<ul>" . $selectedServices . "</ul>",
			"event_planning_status" => $inputData["svPlanningStatus"],
			"visit_date"            => $inputData["svVisitDate"],
			"visit_slot"            => $inputData["svVisitSlot"]
		];

		// save ticket site visit
		$ticketSiteVisit = TicketSiteVisit::create($ticketSiteVisitData);

		if (!$ticketSiteVisit)
		{
			SiteErrorLog::create([
				                     "code"    => "EVB-M-501",
				                     "details" => "Failed to create ticket, email: " . request("email") . ", phone: " . request("phone")
			                     ]);

			return response()->json([
				                        "success" => false,
				                        "error"   => "An error occurred while submitting your request. Please try again later."
			                        ]);
		}

		$mailData = [
			"ticketId"       => $ticketId,
			"name"           => $inputData["svContactName"],
			"phone"          => $inputData["svContactPhone"],
			"email"          => $inputData["svContactEmail"],
			"eventFor"       => $inputData["marriagePerson"],
			"eventName"      => $inputData["personEvent"],
			"visitDate"      => $inputData["svVisitDate"],
			"visitSlot"      => $inputData["svVisitSlot"],
			"addressLine1"   => $inputData["svAddressLine1"],
			"addressLine2"   => $inputData["svAddressLine2"],
			"zipCode"        => $inputData["svZipCode"],
			"location"       => $inputData["svLocation"],
			"cityName"       => $cityUrl,
			"eventDate"      => $inputData["personPartyDate"],
			"bookLikeliness" => $inputData["svPlanningStatus"]
		];

		$this->dispatch(new MailSiteVisitAlertToTeamJob($mailData));
		$this->dispatch(new MailSiteVisitSuccessToCustomerJob($mailData));

		return response()->json(["success" => true]);
	}
}