<?php

namespace App\Http\Controllers\Occasions\Bachelors;

use App\Http\Controllers\Base\BasePackageController;
use App\Models\Package\Package;
use App\Models\Package\PackageGallery;
use App\Models\Types\TypeTag;

use Illuminate\Support\Facades\DB;

class BachelorsPackageController extends BasePackageController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.bachelor.url'));
		$this->setOccasionId(config('evibe.occasion.bachelor.id'));
	}

	public function showPackagesList($cityUrl)
	{
		// validate URL params
		$urlParams = [['key' => 'city', 'value' => $cityUrl]];
		$validUrlObj = $this->validateUrlParams($urlParams);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$cityName = getCityName();
		$eventId = config('evibe.occasion.bachelor.id'); // Youth party
		$eventUrl = $this->getOccasionUrl();

		// SEO
		$pageTitle = "Best youth party resorts, destinations, bachelorette party places in $cityName";
		$pageDesc = "Book $cityName best bachelor party at top resorts, villas, farm houses, places, destinations & more with fun props, 
			cottage rooms, stay, dinner, swimming pool & more. Best deals & quality service.";
		$pageHeader = "Best Youth Party Deals In $cityName";
		$keywords = "top resorts for youth party in $cityName, bachelor party, bachelor party in $cityName, $cityName bachelor Party, places for bachelor party in $cityName";

		$host = config('evibe.host');
		$profileUrl = config('evibe.occasion.bachelor.package.profile_url');
		$profileBaseUrl = "$host/$cityUrl/$eventUrl/$profileUrl/";

		// get distinct packages with these tags
		$packages = Package::where('planner_package.is_live', 1)
		                   ->where('planner_package.event_id', $eventId)
		                   ->forCity();

		// filter results by price
		$clearFilter = false;
		$prices = Package::select(DB::raw('min(price) min, max(price) max'))->first();
		$calMinPrice = $prices->min;
		$calMaxPrice = $prices->max;
		$priceMin = request('price_min');
		$priceMax = request('price_max');
		$priceMin = $priceMin ? $priceMin : $calMinPrice;
		$priceMax = ($priceMax && $priceMax > $priceMin) ? $priceMax : $calMaxPrice;

		// user has changed min / max price
		if ($calMinPrice != $priceMin || $calMaxPrice != $priceMax)
		{
			$clearFilter = true;
			$packages->whereBetween('planner_package.price', [$priceMin, $priceMax]);
		}

		// filter results by category
		$categories = [];
		$validTags = TypeTag::where('type_event', $eventId)
		                    ->where('map_type_id', config('evibe.ticket.type.package'))
		                    ->get();

		foreach ($validTags as $validTag)
		{
			$vDayCatUrl = $this->generateUrl($validTag->name);
			$categories[$vDayCatUrl] = [
				'id'   => $validTag->id,
				'name' => $validTag->identifier,
				'url'  => $vDayCatUrl
			];
		}

		$active = '';
		$selCat = request()->has('category') ? request('category') : '';
		if (!$selCat || $selCat != 'all')
		{
			$selCatId = array_key_exists($selCat, $categories) ? $categories[$selCat]['id'] : null;
			if ($selCatId)
			{
				$packages->joinTags()
				         ->where('planner_package_tags.tile_tag_id', '=', $selCatId);
				$pageHeader = $categories[$selCat]['name'] . ' ' . $pageHeader;
				$active = $selCat;
			}
		}

		// sort results
		$sort = 'popularity';
		$selectedSort = request()->has('sort') ? request('sort') : '';

		if ($selectedSort == 'plth')
		{
			$sort = 'plth';
			$packages->orderBy('planner_package.price', 'ASC');
		}
		elseif ($selectedSort == 'phtl')
		{
			$sort = 'phtl';
			$packages->orderBy('planner_package.price', 'DESC');
		}
		elseif ($selectedSort == 'new-arrivals')
		{
			$sort = 'new-arrivals';
			$packages->orderBy('planner_package.created_at', 'DESC');
		}
		else
		{
			// default sort by popularity (wrong sort option / no sort option)
			$packages->orderBy('planner_package.priority');
		}

		// fetch results
		$packages = $packages->select('planner_package.*')
		                     ->groupBy('planner_package.id')// fix for distinct
		                     ->get();

		$data = [
			'packages'       => $packages,
			'profileBaseUrl' => $profileBaseUrl,
			'occasionId'     => $this->getOccasionId(),
			'cityId'         => getCityId(),
			'mapTypeId'      => $this->getPageId(),
			'sort'           => $sort,
			'filters'        => [
				'queryParams' => $this->getExistingQueryParams(),
				'active'      => $active,
				'priceMin'    => $priceMin,
				'priceMax'    => $priceMax,
				'clearFilter' => $clearFilter,
				'cats'        => $categories
			],
			'seo'            => [
				'pageTitle'       => $pageTitle,
				'pageHeader'      => $pageHeader,
				'pageDescription' => $pageDesc,
				'keywords'        => $keywords
			]
		];

		$data = array_merge($data, $this->mergeView(getCityUrl(), $this->getOccasionId()));

		// show view
		return view('occasion/bachelor/package/list', ['data' => $data]);
	}

	public function showPackageProfile($cityUrl, $packageUrl)
	{
		$urlParams = [
			['key' => 'city', 'value' => $cityUrl],
			['key' => 'package', 'value' => $packageUrl]
		];

		$validUrlObj = $this->validateUrlParams($urlParams);

		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}
		elseif (array_key_exists('isRedirect', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl'] . "?ref=redirect-perm", 301);
		}

		$package = $validUrlObj['planner-package'];
		$provider = $package->provider;
		$maxCount = 8;
		$packageReviews = $package->getReviews($maxCount);
		$providerReviews = $provider->getDirectReviews($maxCount);
		$occasionId = $this->getOccasionId();

		// SEO
		$cityName = getCityName();
		$pageTitle = "Book " . strtolower($package->name) . " best youth party deal in $cityName";
		$pageDescription = "Book $cityName best " . $package->name . " for youth party. Best prices
			at top resorts, pubs, restaurants, villas, farm houses with stay, food, fun activities & more.";

		// gallery (images + videos)
		$gallery = [];
		$packageGallery = PackageGallery::leftJoin('type_package_gallery', 'type_package_gallery.id', '=', 'planner_package_gallery.category_id')
		                                ->select('planner_package_gallery.id', 'planner_package_gallery.title', 'planner_package_gallery.url', 'planner_package_gallery.type', 'planner_package_gallery.alt_text', 'planner_package_gallery.category_id', 'type_package_gallery.category_name', 'planner_package_gallery.planner_package_id')
		                                ->where('planner_package_gallery.planner_package_id', $package->id)
		                                ->orderBy('planner_package_gallery.is_profile', 'DESC')
		                                ->orderBy('planner_package_gallery.updated_at', 'DESC')
		                                ->orderBy('type_package_gallery.priority', 'DESC')
		                                ->get();

		$gallery['images'] = [];
		$gallery['videos'] = [];
		$showGalleryTabs = false;

		// get categorised data
		foreach ($packageGallery as $item)
		{
			$type = 'images';

			if ($item->type == config('evibe.gallery.type.video'))
			{
				$type = 'videos';
			}

			if ($item->type == config('evibe.gallery.type.image'))
			{
				$categoryName = "Others";

				if ($item->category_id)
				{
					$showGalleryTabs = true;
					$categoryName = $item->category_name;
				}

				$gallery[$type][$categoryName][] = [
					'title' => config('evibe.gallery.image_code') . ' - ' . $item->id,
					'url'   => $item->getPath("profile"),
					'thumb' => $item->getPath("thumbs"),
				];
			}
			else
			{
				$gallery[$type][] = [
					'id'    => $item->id,
					'url'   => $item->url,
					'title' => $item->title
				];
			}
		}

		// show nav tabs if there is at least on valid category
		$gallery['showGalleryTabs'] = $showGalleryTabs;

		$data = [
			'package'         => $package,
			'occasionId'      => $this->getOccasionId(),
			'occasionUrl'     => $this->getOccasionUrl(),
			'cityId'          => getCityId(),
			'mapTypeId'       => $this->getPageId(),
			'services'        => $package->services,
			'gallery'         => $gallery,
			'itinerary'       => [],
			'tags'            => $package->getTags(),
			'similarPackages' => $package->getSimilarPackages($occasionId),
			'seo'             => [
				'pageTitle'       => $pageTitle,
				'pageDescription' => $pageDescription
			],
			'ratings'         => [
				'maxCount' => $maxCount,
				'provider' => [
					'count'     => $providerReviews['totalCount'],
					'avgRating' => $provider->avgRating(),
					'reviews'   => $providerReviews['reviews'],
				],
				'package'  => [
					'count'     => $packageReviews['totalCount'],
					'avgRating' => $packageReviews['avgRating'],
					'reviews'   => $packageReviews['reviews'],
				]
			],
		];

		$data = array_merge($data, $this->mergeView(getCityUrl(), $this->getOccasionId()));

		$shortlistDataParams = [
			'mapId'     => $package->id,
			'mapTypeId' => $this->getPageId(),
		];

		$data = array_merge($data, $this->mergeShortlistData($shortlistDataParams));

		return view('occasion/bachelor/package/profile', ['data' => $data]);
	}
}