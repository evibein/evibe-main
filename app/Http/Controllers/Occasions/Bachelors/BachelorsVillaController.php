<?php

namespace App\Http\Controllers\Occasions\Bachelors;

use App\Http\Controllers\Base\BaseVillaController;

class BachelorsVillaController extends BaseVillaController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.bachelor.url'));
		$this->setOccasionId(config('evibe.occasion.bachelor.id'));
		$this->setPageName(config('evibe.occasion.bachelor.villa.name'));
	}

	public function showVillasList($cityUrl)
	{
		$listData = $this->getVillaListData($cityUrl);
		if (!is_array($listData))
		{
			return $listData; // validation failed, redirect
		}

		return $this->renderDataToViewFiles('occasion/bachelor/villa/list', $listData);
	}

	public function showVillaProfile($cityUrl, $packageUrl)
	{
		$hashTag = "#" . config('evibe.occasion.bachelor.hashtag');
		$profileData = $this->getVillaProfileData($cityUrl, $packageUrl, $hashTag);
		if (!is_array($profileData))
		{
			return $profileData; // validation failed (or) has custom view
		}

		return $this->renderDataToViewFiles('occasion/bachelor/villa/profile', $profileData);
	}
}