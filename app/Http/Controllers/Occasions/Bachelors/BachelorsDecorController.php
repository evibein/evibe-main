<?php

namespace App\Http\Controllers\Occasions\Bachelors;

use App\Http\Controllers\Base\BaseDecorController;

class BachelorsDecorController extends BaseDecorController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.bachelor.url'));
		$this->setOccasionId(config('evibe.occasion.bachelor.id'));
		$this->setPageName(config('evibe.occasion.bachelor.decors.name'));
		$this->setPageUrl(config('evibe.occasion.bachelor.decors.results'));
	}

	public function showList($cityUrl, $categoryUrl = "")
	{
		$listData = $this->getDecorsListData($cityUrl, $categoryUrl);
		if (!is_array($listData))
		{
			return $listData; // has error, redirect to given url
		}

		return $this->renderDataToViewFiles('occasion/bachelor/decors/list', $listData);
	}

	public function showProfile($cityUrl, $decorUrl)
	{
		$profileData = $this->getDecorProfileData($cityUrl, $decorUrl);
		if (!is_array($profileData))
		{
			return $profileData; // validation error, redirect to given url
		}

		return $this->renderDataToViewFiles('occasion/bachelor/decors/profile', $profileData);
	}
}