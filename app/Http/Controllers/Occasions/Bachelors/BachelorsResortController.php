<?php

namespace App\Http\Controllers\Occasions\Bachelors;

use App\Http\Controllers\Base\BaseResortController;

class BachelorsResortController extends BaseResortController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.bachelor.url'));
		$this->setOccasionId(config('evibe.occasion.bachelor.id'));
		$this->setPageName(config('evibe.occasion.bachelor.resort.name'));
	}

	public function showResortsList($cityUrl)
	{
		$profileUrl = config('evibe.occasion.bachelor.resort.profile_url');
		$listData = $this->getResortListData($cityUrl, $profileUrl);
		if (!is_array($listData))
		{
			return $listData; // validation failed, redirect
		}

		return $this->renderDataToViewFiles('occasion/bachelor/resorts/list', $listData);
	}

	public function showResortProfile($cityUrl, $packageUrl)
	{
		$hashTag = "#" . config('evibe.occasion.bachelor.hashtag');
		$profileData = $this->getResortProfileData($cityUrl, $packageUrl, $hashTag);
		if (!is_array($profileData))
		{
			return $profileData; // validation failed (or) has custom view
		}

		return $this->renderDataToViewFiles('occasion/bachelor/resorts/profile', $profileData);
	}
}