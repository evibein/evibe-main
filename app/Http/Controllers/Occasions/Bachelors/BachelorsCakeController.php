<?php

namespace App\Http\Controllers\Occasions\Bachelors;

use App\Http\Controllers\Base\BaseCakeController;

class BachelorsCakeController extends BaseCakeController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.bachelor.url'));
		$this->setOccasionId(config('evibe.occasion.bachelor.id'));
		$this->setPageName(config('evibe.occasion.bachelor.cakes.name'));
		$this->setPageUrl(config('evibe.occasion.bachelor.cakes.results_url'));
	}

	public function showList($cityUrl)
	{
		$listData = $this->getCakeListData($cityUrl);
		if (!is_array($listData))
		{
			return $listData; // has error, redirect to given url
		}

		return $this->renderDataToViewFiles('occasion/bachelor/cake/list', $listData);
	}

	public function showProfile($cityUrl, $cakeUrl)
	{
		$hashTag = "#" . config('evibe.occasion.bachelor.hashtag');
		$profileData = $this->getCakeProfileData($cityUrl, $cakeUrl, $hashTag);
		if (!is_array($profileData))
		{
			return $profileData; // validation failed, redirect
		}

		return $this->renderDataToViewFiles('occasion/bachelor/cake/profile', $profileData);
	}
}