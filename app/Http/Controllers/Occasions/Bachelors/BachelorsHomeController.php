<?php

namespace App\Http\Controllers\Occasions\Bachelors;

use App\Http\Controllers\Base\BaseHomeController;
use App\Models\Package\Package;
use App\Models\Util\Country;

class BachelorsHomeController extends BaseHomeController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.bachelor.url'));
		$this->setOccasionId(config('evibe.occasion.bachelor.id'));
	}

	public function showHome($cityUrl)
	{
		$validUrlObj = $this->validateUrlParams($cityUrl);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$params = [
			'cityName'     => getCityName(),
			'occasion'     => 'Bachelors',
			'method'       => 'showCustomHome',
			'controller'   => 'BachelorsHomeController',
			'methodParams' => [
				'cityUrl'    => getCityUrl(),
				'validation' => $validUrlObj
			]
		];

		$customView = $this->customizedCityControllerRedirect($params);
		if ($customView)
		{
			return $customView;
		}

		$viewData = $this->getHomeViewData();

		return view('occasion/bachelor/home/home', ['data' => $viewData]);
	}

	protected function getHomeViewData()
	{
		$cityId = getCityId();
		$eventId = $this->getOccasionId();
		$customerStories = $this->getCustomerStories([$eventId => 5]);
		$vendorStories = $this->getVendorStories();
		$topResorts = $this->getTopResorts($eventId);
		$topEntOptions = $this->getTopEntServices($eventId);
		$topFoodOptions = $this->getTopFoodPackages($eventId);
		$topVillas = $this->getTopVillas($eventId);
		$topCakes = $this->getTopCakes($eventId);

		// fetches the collections for bachelors home
		$eventUrl = $this->getOccasionUrl();
		$cityUrl = getCityUrl();
		$host = config('evibe.host');
		$profileUrl = config('evibe.occasion.collections.profile_url');
		$collectionBaseUrl = "$host/$cityUrl/$eventUrl/$profileUrl/";
		$collections = $this->getCollections($cityId, $eventId, 8);

		$isShowTopContainer = count($topResorts) || count($topEntOptions) || count($topFoodOptions) || count($topVillas) || count($topCakes);

		// fetches auto bookable packages
		$pageId = config('evibe.ticket.type.resorts');
		$autoBookableData = $this->getPaginatedAutoBookableItems($pageId, $topResorts, 3);

		// SEO
		$cityName = getCityName();
		$pageTitle = "Best youth party planners, organisers in $cityName";
		$pageDescription = "Book $cityName best bachelor party, bachelorette party by best party planners, party organisers at best resorts, villas, 
			farm houses, restaurant lounges, pubs with hookah setup, props, fun cakes & more. Best deals & quality service.";
		$pageHeader = "Let's plan your youth party online";

		$data = [
			'collectionBaseUrl'  => $collectionBaseUrl,
			'collections'        => $collections,
			'topEntOptions'      => $topEntOptions,
			'topResorts'         => $topResorts,
			'topFoodOptions'     => $topFoodOptions,
			'topVillas'          => $topVillas,
			'topCakes'           => $topCakes,
			'autoBook'           => $autoBookableData['autoBook'],
			'customerStories'    => $customerStories,
			'vendorStories'      => $vendorStories,
			'occasionId'         => $this->getOccasionId(),
			'cityId'             => getCityId(),
			'isShowTopContainer' => $isShowTopContainer,
			'countries'          => Country::all(),
			'seo'                => [
				'pageTitle'       => $pageTitle,
				'pageDescription' => $pageDescription,
				'pageHeader'      => ucwords($pageHeader)
			]
		];

		$data = array_merge($data, $this->mergeView(getCityUrl(), $this->getOccasionId()));

		return $data;
	}

	private function getTopResorts($eventId)
	{
		$pageId = config('evibe.ticket.type.resorts');
		$topResortIds = $this->getItemIdsByPriority($eventId, $pageId, [], 20);
		$topResorts = Package::with('gallery')
		                     ->select('planner_package.*')
		                     ->joinTags()
		                     ->forEvent($eventId)
		                     ->forPage($pageId)
		                     ->isLive()
		                     ->groupBy('planner_package.id')// avoid repetition
		                     ->forCity();

		if (count($topResortIds) > 0)
		{
			$topResorts = $topResorts->whereIn('planner_package.id', $topResortIds)
			                         ->get();

			// order topResorts by priority
			$topResorts = $this->getSortedListByPriorities($topResorts, $topResortIds);
			$topResorts = array_slice($topResorts, 0, 8); // take top 8
		}
		else
		{
			$topResorts = $topResorts->orderBy('created_at', 'DESC')
			                         ->take(8)// take latest 8
			                         ->get()
			                         ->all();
		}

		return $topResorts;
	}

	private function getTopVillas($eventId)
	{
		$pageId = config('evibe.ticket.type.villas');
		$topVillasIds = $this->getItemIdsByPriority($eventId, $pageId, [], 20);
		$topVillas = Package::with('gallery')
		                    ->select('planner_package.*')
		                    ->joinTags()
		                    ->forEvent($eventId)
		                    ->forPage($pageId)
		                    ->groupBy('planner_package.id')// avoid repetition
		                    ->isLive()
		                    ->forCity();

		if (count($topVillasIds) > 0)
		{
			$topVillas = $topVillas->whereIn('planner_package.id', $topVillasIds)
			                       ->get();

			// order topResorts by priority
			$topVillas = $this->getSortedListByPriorities($topVillas, $topVillasIds);
			$topVillas = array_slice($topVillas, 0, 8); // take top 8
		}
		else
		{
			$topVillas = $topVillas->orderBy('created_at', 'DESC')
			                       ->take(8)// take latest 8
			                       ->get()
			                       ->all();
		}

		return $topVillas;
	}
}