<?php

namespace App\Http\Controllers\Occasions\Bachelors;

use App\Http\Controllers\Base\BaseFoodController;

class BachelorsFoodController extends BaseFoodController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.bachelor.url'));
		$this->setOccasionId(config('evibe.occasion.bachelor.id'));
		$this->setPageName(config('evibe.occasion.bachelor.food.name'));
	}

	public function showFoodList($cityUrl)
	{
		$profileUrl = config('evibe.occasion.bachelor.food.profile_url');
		$listData = $this->getFoodListData($cityUrl, $profileUrl);
		if (!is_array($listData))
		{
			return $listData; // validation failed, redirect
		}

		return $this->renderDataToViewFiles('occasion/bachelor/food/list', $listData);
	}

	public function showFoodProfile($cityUrl, $packageUrl)
	{
		$hashTag = "#" . config('evibe.occasion.bachelor.hashtag');
		$profileData = $this->getFoodProfileData($cityUrl, $packageUrl, $hashTag);
		if (!is_array($profileData))
		{
			return $profileData;
		}

		return $this->renderDataToViewFiles('occasion/bachelor/food/profile', $profileData);
	}
}