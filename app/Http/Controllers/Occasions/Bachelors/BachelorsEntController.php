<?php

namespace App\Http\Controllers\Occasions\Bachelors;

use App\Http\Controllers\Base\BaseEntController;

class BachelorsEntController extends BaseEntController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.bachelor.url'));
		$this->setOccasionId(config('evibe.occasion.bachelor.id'));
		$this->setPageName(config('evibe.occasion.bachelor.ent.name'));
		$this->setPageUrl(config('evibe.occasion.bachelor.ent.results_url'));
	}

	public function showList($cityUrl, $categoryUrl = "")
	{
		$profileRoute = "city.occasion.bachelor.ent.profile";
		$listData = $this->getEntListData($cityUrl, $profileRoute, $categoryUrl);
		if (!is_array($listData))
		{
			return $listData; // validation failed, redirect
		}

		return $this->renderDataToViewFiles('occasion/bachelor/ent/list', $listData);
	}

	public function showDetails($cityUrl, $entUrl)
	{
		$hashTag = "#" . config('evibe.occasion.bachelor.hashtag');
		$profileData = $this->getEntProfileData($cityUrl, $entUrl, $hashTag);
		if (!is_array($profileData))
		{
			return $profileData; // validation failed, redirect
		}

		return $this->renderDataToViewFiles('occasion/bachelor/ent/profile', $profileData);
	}
}