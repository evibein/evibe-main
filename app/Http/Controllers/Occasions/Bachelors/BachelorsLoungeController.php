<?php

namespace App\Http\Controllers\Occasions\Bachelors;

use App\Http\Controllers\Base\BaseLoungeController;

class BachelorsLoungeController extends BaseLoungeController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.bachelor.url'));
		$this->setOccasionId(config('evibe.occasion.bachelor.id'));
		$this->setPageName(config('evibe.occasion.bachelor.lounge.name'));
	}

	public function showLoungesList($cityUrl)
	{
		$listData = $this->getLoungeListData($cityUrl);
		if (!is_array($listData))
		{
			return $listData; // validation failed, redirect
		}

		return $this->renderDataToViewFiles('occasion/bachelor/lounge/list', $listData);
	}

	public function showLoungeProfile($cityUrl, $packageUrl)
	{
		$hashTag = "#" . config('evibe.occasion.bachelor.hashtag');
		$profileData = $this->getLoungeProfileData($cityUrl, $packageUrl, $hashTag);
		if (!is_array($profileData))
		{
			return $profileData; // validation failed (or) has custom view
		}

		return $this->renderDataToViewFiles('occasion/bachelor/lounge/profile', $profileData);
	}
}