<?php

namespace App\Http\Controllers\Occasions\NamingCeremony;

use App\Http\Controllers\Base\BaseDecorController;

class NamingCeremonyDecorController extends BaseDecorController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.naming_ceremony.url'));
		$this->setOccasionId(config('evibe.occasion.naming_ceremony.id'));
		$this->setPageName(config('evibe.occasion.naming_ceremony.decors.name'));
		$this->setPageUrl(config('evibe.occasion.naming_ceremony.decors.results'));
	}

	public function showList($cityUrl, $categoryUrl = "")
	{
		$listData = $this->getDecorsListProductData($cityUrl, $categoryUrl);
		if (!is_array($listData))
		{
			return $listData; // has error, redirect to given url
		}

		return $this->renderDataToViewFiles('occasion/birthday/decors/list', $listData);
	}

	public function showProfile($cityUrl, $decorUrl)
	{
		// Considering Naming Ceremony as a part of kids birthday rather than a separate occasion
		$this->setOccasionUrl(config('evibe.occasion.kids_birthdays.url'));
		$this->setOccasionId(config('evibe.occasion.kids_birthdays.id'));
		$this->setPageName(config('evibe.occasion.kids_birthdays.decors.name'));
		$this->setPageUrl(config('evibe.occasion.kids_birthdays.decors.results'));

		$profileData = $this->getDecorProfileData($cityUrl, $decorUrl);
		if (!is_array($profileData))
		{
			return $profileData; // validation error, redirect to given url
		}

		$profileData = $this->addProductSpecificAddOns($profileData, config('evibe.occasion.kids_birthdays.id'), config('evibe.ticket.type.decor'), $profileData['decor']->id);

		return $this->renderDataToViewFiles('occasion/birthday/decors/profile', $profileData);
	}
}