<?php

namespace App\Http\Controllers\Occasions\Surprises;

use App\Http\Controllers\Base\BasePackageController;
use App\Models\Package\Package;

class SurprisesPackageController extends BasePackageController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.surprises.url'));
		$this->setOccasionId(config('evibe.occasion.surprises.id'));
		$this->setPageId(config('evibe.ticket.type.surprises'));
		$this->setPageName(config('evibe.occasion.surprises.package.name'));
		$this->setPageUrl(config('evibe.occasion.surprises.package.profile_url'));
	}

	public function showPackagesList($cityUrl, $srString = null, $soString = null, $saString = null, $fetchWithoutView = false, $categoryUrl = null)
	{
		/**
		 * Redirecting to the new candle light dinner page if they come to the old URL
		 */
		if (request("category") == "candle-light-dinners")
		{
			return redirect(route("city.cld.list", $cityUrl) . '?ref=redirect-perm', 301);
		}

		$surData = [];
		if ($srString && $soString && $saString)
		{
			$surData = [
				'srString' => $srString,
				'soString' => $soString,
				'saString' => $saString
			];
		}

		$itemsLimit = $fetchWithoutView ? 8 : 21;

		$listData = $this->getListData($cityUrl, $itemsLimit, false, null, $surData, $fetchWithoutView, $categoryUrl);
		if (!is_array($listData))
		{
			return $listData; // validation failed, redirect
		}
		if ($fetchWithoutView)
		{
			return $listData;
		}
		$host = config('evibe.host');
		$profileUrl = config('evibe.occasion.surprises.package.profile_url');
		$listData['profileBaseUrl'] = "$host/$cityUrl/" . $this->getOccasionUrl() . "/$profileUrl/";

		return $this->renderDataToViewFiles('occasion/surprises/packages/list', $listData);
	}

	public function showPackageProfile($cityUrl, $packageUrl)
	{
		$hashTag = "#" . config('evibe.occasion.surprises.hashtag');
		$profileData = $this->getProfileData($cityUrl, $packageUrl, $hashTag);
		if (!is_array($profileData))
		{
			return $profileData; // validation failed (or) has custom view
		}

		$profileData = $this->addProductSpecificAddOns($profileData, config('evibe.occasion.surprises.id'), config('evibe.ticket.type.surprises'), $profileData['package']->id);

		$christmasIds = config('evibe.campaign-packages.christmas') ?: "";
		{
			$christmasIds = explode(',', preg_replace('/\s/', '', $christmasIds));
		}

		$newYearIds = config('evibe.campaign-packages.new-year') ?: "";
		if (is_string($newYearIds))
		{
			$newYearIds = explode(',', preg_replace('/\s/', '', $newYearIds));
		}

		$valentinesDayIds = Package::join('planner_package_tags', 'planner_package_tags.planner_package_id', '=', 'planner_package.id')
		                           ->select('planner_package.*')
		                           ->where('planner_package_tags.tile_tag_id', config('evibe.type-tag.valentines-day-2020'))
		                           ->where('planner_package.type_ticket_id', config('evibe.ticket.type.surprises'))
		                           ->where('planner_package.is_live', 1)
		                           ->where('planner_package.map_type_id', 3) // @see: should be applicable only for VDay CLDs
		                           ->whereNull('planner_package.deleted_at')
		                           ->whereNull('planner_package_tags.deleted_at')
		                           ->pluck('planner_package.id')
		                           ->toArray();

		if (in_array($profileData['package']->id, $christmasIds) && config('evibe.campaign-dates.christmas'))
		{
			$profileData['campaignDates'] = [];

			foreach (config('evibe.campaign-dates.christmas') as $date)
			{
				array_push($profileData['campaignDates'], date('Y/m/d', strtotime($date)));
			}
		}
		elseif (in_array($profileData['package']->id, $newYearIds) && config('evibe.campaign-dates.new-year'))
		{
			$profileData['campaignDates'] = [];

			foreach (config('evibe.campaign-dates.new-year') as $date)
			{
				array_push($profileData['campaignDates'], date('Y/m/d', strtotime($date)));
			}
		}
		elseif (in_array($profileData['package']->id, $valentinesDayIds) && config('evibe.campaign-dates.valentines-day'))
		{
			$profileData['campaignDates'] = [];
			$profileData['campaignText'] = "Reservations made for non valentine's day (14 Feb 2021) may have limitations in the inclusions. We will keep you posted through order updates.";
			$profileData['campaignFocusDate'] = "2021/02/14";

			foreach (config('evibe.campaign-dates.valentines-day') as $date)
			{
				if ($date >= date('Y-m-d', time()))
				{
					array_push($profileData['campaignDates'], date('Y/m/d', strtotime($date)));
				}
			}
		}

		return $this->renderDataToViewFiles('occasion/surprises/packages/profile', $profileData);
	}
}