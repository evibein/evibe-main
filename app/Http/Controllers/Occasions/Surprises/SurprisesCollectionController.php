<?php

namespace App\Http\Controllers\Occasions\Surprises;

use App\Http\Controllers\Base\BaseCollectionController;

class SurprisesCollectionController extends BaseCollectionController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.surprises.url'));
		$this->setOccasionId(config('evibe.occasion.surprises.id'));
	}

	public function showCollectionList($cityUrl)
	{
		$listData = $this->getCollectionListData($cityUrl);
		if (!is_array($listData))
		{
			return $listData; // validation failed, redirect to given url
		}

		return view('occasion/surprises/collection/list', ['data' => $listData]);
	}

	public function showCollectionProfile($cityUrl, $collectionUrl)
	{
		$profileData = $this->getCollectionProfileData($cityUrl, $collectionUrl);
		if (!is_array($profileData))
		{
			return $profileData; // validation failed, redirect to given url
		}

		return view('occasion/surprises/collection/profile', ['data' => $profileData]);
	}
}