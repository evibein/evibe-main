<?php

namespace App\Http\Controllers\Occasions\Surprises;

use App\Http\Controllers\Base\BaseHomeController;
use App\Models\Package\Package;
use App\Models\Package\PackageTag;
use App\Models\Surprise\SurpriseTagMappings;
use App\Models\Types\TypeTag;
use App\Models\Util\Country;
use Illuminate\Support\Facades\Cache;

class SurprisesHomeController extends BaseHomeController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.surprises.url'));
		$this->setOccasionId(config('evibe.occasion.surprises.id'));
	}

	public function showHome($cityUrl)
	{
		// Redirect to New surprise home page
		$newSurpriseUrl = $cityUrl . "/h/couple-surprises";

		return redirect($newSurpriseUrl, 301);
		$validUrlObj = $this->validateUrlParams($cityUrl);

		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$cityId = getCityId();
		$eventId = $this->getOccasionId();
		$customerStories = $this->getCustomerStories([$eventId => 5]);
		$vendorStories = $this->getVendorStories();

		$cacheKey = $this->deviceSpecificCacheKey("surpriseHomeCategoryTopOptions" . $cityId);
		$cacheKey = base64_encode($cacheKey);
		if (Cache::has($cacheKey))
		{
			$topPackages = Cache::get($cacheKey);
		}
		else
		{
			$topPackages = $this->getTopHomePackages($eventId);
			Cache::put($cacheKey, $topPackages, config('evibe.cache.refresh-time'));
		}

		// fetches the collections for special experiences home
		$eventUrl = $this->getOccasionUrl();
		$cityUrl = getCityUrl();
		$host = config('evibe.host');
		$profileUrl = config('evibe.occasion.collections.profile_url');
		$collectionBaseUrl = "$host/$cityUrl/$eventUrl/$profileUrl/";
		$collections = $this->getCollections($cityId, $eventId, 8);

		// SEO
		$defaultSEOKey = "defaults.city.$eventId.home";
		$seo = $this->getSeoText([$cityId, $eventId, "home"], $defaultSEOKey);
		$pageTitle = ucwords($seo['pt']);
		$pageDescription = $seo['pd'];
		$pageHeader = ucwords($seo['ph']);
		$keyWords = $seo['kw'];

		// surprise category filters
		$surCategories = $this->getSurpriseCategories();

		// promoted reviews
		$promotedReviews = [
			[
				'name'   => 'Ms. Bhavana',
				'photo'  => 'bhavana.jpg',
				'review' => 'The prices were very promising and the team was very prompt in response. Thank you Evibe.in for surprising my friend.'
			],
			[
				'name'   => 'Mr. Ashwin',
				'photo'  => 'ashwin.jpg',
				'review' => 'My wife was really delighted and flattered for Mickey\'s surprise on her Birthday. Thank you Evibe'
			],
			[
				'name'   => 'Ms. Sreeja',
				'photo'  => 'sreeja.jpg',
				'review' => 'Its my 4th wedding anniversary, really really awesome, so memorable night with my husband & my lil angel happy with service from evibe'
			]
		];

		$data = [
			'collectionBaseUrl' => $collectionBaseUrl,
			'collections'       => $collections,
			'topPackages'       => $topPackages,
			'customerStories'   => $customerStories,
			'vendorStories'     => $vendorStories,
			'occasionId'        => $this->getOccasionId(),
			'cityId'            => getCityId(),
			'seo'               => [
				'pageTitle'       => $pageTitle,
				'pageDescription' => $pageDescription,
				'pageHeader'      => $pageHeader,
				'keyWords'        => $keyWords
			],
			'surCategories'     => $surCategories,
			'promotedReviews'   => $promotedReviews,
			'countries'         => Country::all()
		];

		$data = array_merge($data, $this->mergeView(getCityUrl(), $this->getOccasionId()));

		$path = "cities." . $cityUrl . ".occasion.surprises.home.";
		if ($this->agent->isMobile() && !($this->agent->isTablet()))
		{
			$path = $path . 'm';
		}
		else
		{
			$path = $path . 'd';
		}
		$data['views']['home'] = $path;

		return view('occasion/surprises/home/home', ['data' => $data]);
	}

	// override
	protected function getTopHomePackages($eventId)
	{
		$pageId = config('evibe.ticket.type.surprises');
		$topPackageIds = $this->getItemIdsByPriority($eventId, $pageId, [], 20);
		$topPackages = Package::with('gallery')
		                      ->select('planner_package.*')
		                      ->forEvent($eventId)
		                      ->isLive()
		                      ->forCity();
		$topRelPackages = [];

		if (count($topPackageIds) > 0)
		{
			$topPackages = $topPackages->get();

			$packageTags = PackageTag::all();

			// Required categories for the homepage
			// @todo: need to remove this hard-coding
			$requiredRelPackagesIds = [
				180 => "Adventure",
				181 => "Romantic",
				182 => "Unique",
				183 => "Customised",
				184 => "Regular Surprise",
				185 => "Home Decoration"
			];

			foreach ($requiredRelPackagesIds as $relPackageId => $name)
			{
				$surRelPckIds = $packageTags->where('tile_tag_id', $relPackageId)->pluck("planner_package_id")->toArray();

				$allTopRelPackages = $topPackages->filter(function ($package, $key) use ($surRelPckIds) {
					return in_array($package->id, $surRelPckIds);
				});

				$allTopRelPackages = $this->getSortedListByPriorities($allTopRelPackages, $topPackageIds);

				$topRelPackages[$name] = array_slice($allTopRelPackages, 0, 12);
			}
		}
		else
		{
			//$topRelPackages = $topPackages->orderBy('planner_package.created_at', 'DESC')
			//                              ->take(12)
			//                              ->get()
			//                              ->all();
		}

		return $topRelPackages;
	}

	public function getSurpriseCategories()
	{
		$surpriseParentCategories = [
			config('evibe.surprise-parent-tags.relationship'),
			config('evibe.surprise-parent-tags.occasion'),
			config('evibe.surprise-parent-tags.age-group'),
		];
		$surCategories = [];

		$surCatTags = TypeTag::whereIn('parent_id', $surpriseParentCategories)->get();
		if ($surCatTags)
		{
			$surRelTagsArray = $surCatTags->where('parent_id', config('evibe.surprise-parent-tags.relationship'))
			                              ->toArray();

			$surOccasionTagsArray = $surCatTags->where('parent_id', config('evibe.surprise-parent-tags.occasion'))
			                                   ->toArray();

			$surAgeTagsArray = $surCatTags->where('parent_id', config('evibe.surprise-parent-tags.age-group'))
			                              ->toArray();

			$surTagMappings = SurpriseTagMappings::with('occasionTag', 'ageGroupTag')
			                                     ->select('sr_id', 'so_id', 'sa_id')
			                                     ->where('is_negative', 1)
			                                     ->get();

			$surOccTagMappings = $surTagMappings->where('sa_id', null)
			                                    ->toArray();

			$surAgeTagMappings = $surTagMappings->where('so_id', null)
			                                    ->toArray();

			$surCategories = [
				'rel'     => $surRelTagsArray,
				'occ'     => $surOccasionTagsArray,
				'rel-occ' => $surOccTagMappings,
				'age'     => $surAgeTagsArray,
				'rel-age' => $surAgeTagMappings
			];
		}

		return $surCategories;
	}
}