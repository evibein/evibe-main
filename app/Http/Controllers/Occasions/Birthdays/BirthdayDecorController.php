<?php

namespace App\Http\Controllers\Occasions\Birthdays;

use App\Http\Controllers\Base\BaseDecorController;

class BirthdayDecorController extends BaseDecorController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.kids_birthdays.url'));
		$this->setOccasionId(config('evibe.occasion.kids_birthdays.id'));
		$this->setPageName(config('evibe.occasion.kids_birthdays.decors.name'));
		$this->setPageUrl(config('evibe.occasion.kids_birthdays.decors.results'));
	}

	public function showList($cityUrl, $categoryUrl = "", $fetchWithoutView = false, $collectionId = null)
	{

		$listData = $this->getDecorsListProductData($cityUrl, $categoryUrl, $fetchWithoutView, $collectionId);
		if (!is_array($listData))
		{
			return $listData; // has error, redirect to given url
		}
		if ($fetchWithoutView)
		{
			return $listData;
		}

		return $this->renderDataToViewFiles('occasion/birthday/decors/list', $listData);
	}

	public function showProfile($cityUrl, $decorUrl)
	{
		$profileData = $this->getDecorProfileData($cityUrl, $decorUrl);
		if (!is_array($profileData))
		{
			return $profileData; // validation error, redirect to given url
		}

		$profileData = $this->addProductSpecificAddOns($profileData, config('evibe.occasion.kids_birthdays.id'), config('evibe.ticket.type.decor'), $profileData['decor']->id);

		return $this->renderDataToViewFiles('occasion/birthday/decors/profile', $profileData);
	}
}