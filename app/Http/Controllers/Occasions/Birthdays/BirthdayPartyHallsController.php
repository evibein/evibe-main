<?php

namespace App\Http\Controllers\Occasions\Birthdays;

use App\Models\Util\Area;
use App\Models\Types\TypeVenue;
use App\Models\Types\TypeVenueHall;
use App\Models\Venue\KidsVenuesGallery;
use App\Models\Venue\Venue;
use App\Models\Venue\VenueHall;
use App\Models\Venue\KidsPlayAreas;
use App\Http\Controllers\Base\BaseVenueController;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class BirthdayPartyHallsController extends BaseVenueController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.kids_birthdays.url'));
		$this->setOccasionId(config('evibe.occasion.kids_birthdays.id'));
	}

	public function showKidsPlayAreasProfile($cityUrl, $placeId)
	{
		$cityUrl = getCityUrl();

		return redirect(route('city.home', $cityUrl));

		$urlParams = [['key' => 'city', 'value' => $cityUrl]];
		$validUrlObj = $this->validateUrlParams($urlParams);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}
		$cityId = getCityId();

		// Get venues
		$venueDet = KidsPlayAreas::where([['city_id', $cityId], ['place_id', $placeId]])
		                         ->orderBy('rating', 'desc')
		                         ->first();

		$data['seo'] = [
			"pt"       => "$venueDet->name in $cityUrl | Evibe.in",
			"pd"       => "Find The Best Places Near You For Your Kids.",
			"keywords" => "kids, fun areas, kids party",
		];

		$data['venueDetails'] = $venueDet;
		$data['venueReviews'] = $venueDet->getReviews()->get();
		$data['gallery'] = $venueDet->gallery()->take(5)->get();

		return view('occasion/birthday/kids-play-areas/profile', ["data" => $data]);
	}

	public function showKidsPlayAreas()
	{
		$cityUrl = getCityUrl();

		return redirect(route('city.home', $cityUrl));

		$urlParams = [['key' => 'city', 'value' => $cityUrl]];
		$validUrlObj = $this->validateUrlParams($urlParams);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}
		$cityId = getCityId();

		// Get venues
		$venues = KidsPlayAreas::where('city_id', $cityId)
		                       ->orderBy('rating', 'desc')
		                       ->get();
		$venueData = [];
		$vCount = 0;

		$galleryImages = KidsVenuesGallery::whereIn("place_id", $venues->pluck("place_id")->toArray())
		                                  ->get()
		                                  ->sortByDesc("id")
		                                  ->keyBy("place_id")
		                                  ->unique("place_id")
		                                  ->pluck("image_url", "place_id")
		                                  ->toArray();

		foreach ($venues as $venue)
		{
			$temp = [];
			$temp['vName'] = $venue['name'];
			$temp['imgUrl'] = isset($galleryImages[$venue->place_id]) ? $galleryImages[$venue->place_id] : "https://gallery.evibe.in/main/pages/play-area/default-thumb.jpeg";
			$temp['rating'] = $venue['rating'];
			$temp['address'] = $venue['address'];
			$temp['placeId'] = $venue['place_id'];
			$venueData[$vCount++] = $temp;
		}
		$data['seo'] = [
			"pt"       => "Kids Play Areas In $cityUrl | Evibe.in",
			"pd"       => "Find The Best Places Near You For Your Kids.",
			"keywords" => "kids, fun areas, kids party",
		];
		$data['venueData'] = $venueData;

		return view('occasion/birthday/kids-play-areas/list', ["data" => $data]);
	}

	public function showList($cityUrl)
	{
		// validate url parameters
		$urlParams = [['key' => 'city', 'value' => $cityUrl]];
		$validUrlObj = $this->validateUrlParams($urlParams);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$cityId = getCityId();
		$cityName = getCityName();
		$areas = $this->getLocations($cityId);
		$eventId = $this->getOccasionId();
		$pageId = $this->getPageId();
		$hasFilter = false;
		$activeCat = '';
		$activeType = '';
		$activeTypeId = -1;
		$activeCatId = -1;

		// SEO
		$capSeoTxt = "";
		$locSeoTxt = "$cityName";
		$typeSeoTxt = "";
		$catSeoTxt = "";
		$allTypesSeoTxt = "party halls, banquet halls, function halls, party spaces";
		$pdStart = "List of all";
		$pdLast = "with best deals, real pictures, menus & customer reviews";

		// halls
		$halls = VenueHall::with('venue', 'venue.area', 'type')
		                  ->join('venue', 'venue.id', '=', 'venue_hall.venue_id')
		                  ->where('venue.city_id', $cityId)
		                  ->where('venue.is_live', 1)
		                  ->whereNull('venue.deleted_at')
		                  ->select('venue_hall.*')
		                  ->forCity()
		                  ->distinct('venue_hall.id');

		// @see: Filter results - maintain order
		// category should be last - this is to get correct category filter count

		// Price filter
		$minMax = Venue::select(DB::raw('min(price_min_veg) minPriceMin, max(price_min_veg) maxPriceMin, max(price_min_nonveg) maxPriceMax, min(cap_min) calCapMin, max(cap_max) calCapMax'))
		               ->where('is_live', 1)
		               ->first();

		$calMinPrice = $minMax->minPriceMin;
		$calMaxPrice = max($minMax->maxPriceMin, $minMax->maxPriceMax);
		$priceMin = request('price_min');
		$priceMax = request('price_max');
		$priceMin = $priceMin ? $priceMin : $calMinPrice;
		$priceMax = ($priceMax && $priceMax > $priceMin) ? $priceMax : $calMaxPrice;

		// user has changed min / max price
		if ($calMinPrice != $priceMin || $calMaxPrice != $priceMax)
		{
			$hasFilter = true;
			$halls->whereBetween('venue.price_min_veg', [$priceMin, $priceMax]);
		}

		// location filter
		$location = request('location');
		if ($location && $location != "all")
		{
			$matchedArea = Area::where('name', $location)->first();
			if ($matchedArea)
			{
				$halls->where('venue.area_id', $matchedArea->id);
				$hasFilter = true;
				$locSeoTxt = "$location, $cityName";
			}
		}

		// capacity filter
		$calCapMin = $minMax->calCapMin;
		$calCapMax = $minMax->calCapMax;
		$capMin = request('capacity_min');
		$capMax = request('capacity_max');
		$capMin = $capMin ? $capMin : $calCapMin;
		$capMax = ($capMax && $capMax > $capMin) ? $capMax : $calCapMax;

		// user has changed min / max capacity
		if ($calCapMin != $capMin || $calCapMax != $capMax)
		{
			/**
			 * @see using only selected capacity minimum to filter out results.
			 * as hall capacity maximum does not matter as far as it can accommodate it
			 */
			$halls->where(function ($query) use ($capMin, $capMax) {
				$query->where(function ($query1) use ($capMin, $capMax) {
					$query1->where('venue_hall.cap_min', '>=', $capMin)
					       ->where('venue_hall.cap_min', '<=', $capMax);
				})
				      ->orWhere(function ($query2) use ($capMin, $capMax) {
					      $query2->where('venue_hall.cap_max', '>=', $capMin)
					             ->where('venue_hall.cap_max', '<=', $capMax);
				      });
			});

			$hasFilter = true;
			$capSeoTxt = "for $capMin - $capMax guests";
		}

		// Get all decors for type counting
		$allVenues = $halls->groupBy('venue_hall.id')->get(); // groupBy is fix for distinct

		// type filter
		$allTypes = TypeVenue::where('is_filter', 1)->orderBy('identifier')->get();
		$selType = request()->has('type') ? request('type') : '';
		if ($selType && $selType != "all")
		{
			$matchedType = TypeVenue::where('url', 'LIKE', $selType)->first();
			if ($matchedType)
			{
				$halls->where('venue.type_id', $matchedType->id);
				$hasFilter = true;
				$activeType = $matchedType->url;
				$activeTypeId = $matchedType->id;
				$typeSeoTxt = "$matchedType->identifier";
			}
		}

		// Get all decors for category counting
		$allTypeFilteredVenues = $halls->groupBy('venue_hall.id')->get(); // groupBy is fix for distinct

		// category filter
		$allCategories = TypeVenueHall::where('is_filter', 1)->orderBy('identifier')->get();
		$selCategory = request()->has('category') ? request('category') : '';
		if ($selCategory && $selCategory != 'all')
		{
			$matchedCat = TypeVenueHall::where('url', 'LIKE', $selCategory)->first();
			if ($matchedCat)
			{
				$halls->where('venue_hall.type_id', $matchedCat->id);
				$hasFilter = true;
				$activeCat = $matchedCat->url;
				$activeCatId = $matchedCat->id;
				$catSeoTxt = "$matchedCat->identifier";
			}
		}

		// Set SEO texts
		$typeSeoTxt = ($typeSeoTxt && $catSeoTxt) ? "at $typeSeoTxt" : $typeSeoTxt;
		$ptTypes = (!$catSeoTxt && !$typeSeoTxt) ? $allTypesSeoTxt : "$catSeoTxt $typeSeoTxt";
		$phTypes = (!$catSeoTxt && !$typeSeoTxt) ? "Party Halls" : "$catSeoTxt $typeSeoTxt";
		$pageTitle = "Best $ptTypes $capSeoTxt for birthday parties in $locSeoTxt";
		$pageHeader = "Best $phTypes in $locSeoTxt $capSeoTxt";
		$pageDescription = "$pdStart $ptTypes $pdLast";
		$pageKeywords = "$locSeoTxt, $cityName, $typeSeoTxt, $catSeoTxt, $capSeoTxt";

		// for type - sort by count (desc)
		$typeCounts = $this->getVenueFilterCategoryCounts($allVenues, $allTypes, true);
		arsort($typeCounts);
		$orderedTypes = $this->getOrderedByCountsFilters($typeCounts, $allTypes);
		$activeTypePos = array_search($activeTypeId, array_keys($orderedTypes));
		$typeShowMore = $activeTypePos > 4 ? true : false;

		// for category - sort by count (desc)
		$catCounts = $this->getVenueFilterCategoryCounts($allTypeFilteredVenues, $allCategories);
		arsort($catCounts);
		$orderedCats = $this->getOrderedByCountsFilters($catCounts, $allCategories);
		$activeCatPos = array_search($activeCatId, array_keys($orderedCats));
		$catShowMore = $activeCatPos > 4 ? true : false;

		// sort halls
		$sortParams = $this->getSortParams('venue_hall', ['price' => 'venue.price_min_veg']);
		if (!$sortParams['isDefault'])
		{
			// groupBy is fix for distinct
			$halls = $halls->orderBy($sortParams['entity'], $sortParams['order'])->get();
			$arrayHalls = $halls->all(); // to Array for pagination
		}
		else
		{
			$halls = $halls->orderBy('venue_hall.created_at', 'DESC')->get(); // default sort
			$allHallIds = $halls->pluck('id')->all(); // get all IDs for merge
			$arrayHalls = $halls->all(); // to array
			$priorityIds = $this->getItemIdsByPriority($eventId, $pageId, $allHallIds); // get priorities

			usort($arrayHalls, function ($a, $b) use ($priorityIds) {
				return $this->sortItemsByPriority($a, $b, $priorityIds);
			});
		}

		// Paginate results
		$currentPage = Paginator::resolveCurrentPage();
		$perPage = 21;
		$offset = ($currentPage * $perPage) - $perPage;

		$paginatedHalls = new LengthAwarePaginator(
			array_slice($arrayHalls, $offset, $perPage, true),
			$halls->count(),
			$perPage,
			$currentPage,
			['path' => Paginator::resolveCurrentPath()]
		);

		$data = [
			'halls'       => $paginatedHalls,
			'occasionId'  => $this->getOccasionId(),
			'cityId'      => getCityId(),
			'mapTypeId'   => $this->getPageId(),
			'sort'        => $sortParams['sortType'],
			'areas'       => $areas,
			'occasionUrl' => $this->getOccasionUrl(),
			'filters'     => [
				'queryParams'   => $this->getExistingQueryParams(),
				'allTypes'      => $orderedTypes,
				'typeCounts'    => $typeCounts,
				'activeType'    => $activeType,
				'typeShowMore'  => $typeShowMore,
				'allCategories' => $orderedCats,
				'catCounts'     => $catCounts,
				'activeCat'     => $activeCat,
				'catShowMore'   => $catShowMore,
				'priceMin'      => $priceMin,
				'priceMax'      => $priceMax,
				'capMin'        => $capMin,
				'capMax'        => $capMax,
				'hasFilter'     => $hasFilter,
				'location'      => $location,
				'clearFilter'   => false
			],
			'seo'         => [
				'pageTitle'       => $pageTitle,
				'pageDescription' => $pageDescription,
				'pageKeywords'    => $pageKeywords,
				'pageHeader'      => $pageHeader
			]
		];

		$data = array_merge($data, $this->mergeView(getCityUrl(), $this->getOccasionId()));

		return $this->renderDataToViewFiles('occasion/birthday/party-halls/list/', $data);
	}

	public function showProfile($cityUrl, $hallUrl)
	{
		// validate url params
		$urlParams = [
			['key' => 'city', 'value' => $cityUrl],
			['key' => 'hall', 'value' => $hallUrl]
		];

		$validUrlObj = $this->validateUrlParams($urlParams);

		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$hall = $validUrlObj['hall'];
		$venue = $hall->venue;

		// show venues list if not live
		if (!$venue->is_live)
		{
			return redirect(route('city.occasion.birthdays.venues.list', $cityUrl));
		}

		// SEO related
		$typeName = $typeNameForHeader = strtolower($hall->type->name);

		// if type other
		if ($hall->type_id == 1)
		{
			$typeName = 'Best party places';
			$typeNameForHeader = 'Party space';
		}
		else
		{
			$typeName = "Best party " . $typeName;
		}

		$location = $venue->area->name;
		$gallery = array_merge($hall->prepareGallery(), $venue->prepareGallery());

		$pageTitle = $typeName . ' for ';
		$pageTitle .= $hall->cap_min . ' to ' . $hall->cap_float . ' guests at ';
		$pageTitle .= $location . ', ' . $venue->city->name;

		$pageDescription = 'Get best price for ' . $typeNameForHeader . ' at ' . $location . ' for ';
		$pageDescription .= $hall->cap_min . ' to ' . $hall->cap_float . ' guests ';
		$pageDescription .= 'to celebrate birthday party, engagements, youth party & more starting from Rs. ';
		$pageDescription .= $venue->price_min_veg . ' per guest. ';
		$pageDescription .= 'See complete details, real pictures, ratings, menus and facilities.';

		$pageUrl = Request::capture()->fullUrl();
		$metaImage = isset($gallery[0]) && isset($gallery[0]['url']) ? $gallery[0]['url'] : "";
		$decorTitle = $typeName . ' at ' . $location . ", " . $venue->city->name;
		$shareUrl = $this->getShareUrl($pageUrl, $decorTitle, "#" . config('evibe.occasion.kids_birthdays.hashtag'), $metaImage);

		$data = [
			'hall'         => $hall->prepareData(),
			'venue'        => $venue->prepareData(),
			'similarHalls' => $hall->getSimilarHalls(),
			'menus'        => $venue->prepareMenus(),
			'gallery'      => $gallery,
			'extRatings'   => $venue->prepareExtRatings(),
			'occasionUrl'  => $this->getOccasionUrl(),
			'occasionId'   => $this->getOccasionId(),
			'cityId'       => getCityId(),
			'mapTypeId'    => $this->getPageId(),
			'seo'          => [
				'pageTitle'       => $pageTitle,
				'pageDescription' => $pageDescription,
				'pageUrl'         => $pageUrl,
				'metaImage'       => $metaImage
			],
			'shareUrl'     => $shareUrl
		];

		$data = array_merge($data, $this->mergeView(getCityUrl(), $this->getOccasionId()));

		$shortlistDataParams = [
			'mapId'     => $data['hall']['id'],
			'mapTypeId' => $this->getPageId(),
		];

		$data = array_merge($data, $this->mergeShortlistData($shortlistDataParams));

		return $this->renderDataToViewFiles('occasion/birthday/party-halls/profile/', $data);
	}
}