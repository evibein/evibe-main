<?php

namespace App\Http\Controllers\Occasions\Birthdays;

use App\Http\Controllers\Base\BaseTentController;

class BirthdayTentController extends BaseTentController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.kids_birthdays.url'));
		$this->setOccasionId(config('evibe.occasion.kids_birthdays.id'));
		$this->setPageName(config('evibe.occasion.kids_birthdays.tents.name'));
	}

	public function showTentList($cityUrl)
	{
		$profileUrl = config('evibe.occasion.kids_birthdays.tents.profile_url');
		$listData = $this->getTentListData($cityUrl, $profileUrl);
		if (!is_array($listData))
		{
			return $listData; // validation failed, redirect
		}

		return $this->renderDataToViewFiles('occasion/birthday/tents/list', $listData);
	}

	public function showTentProfile($cityUrl, $packageUrl)
	{
		$hashTag = "#" . config('evibe.occasion.kids_birthdays.hashtag');
		$profileData = $this->getTentProfileData($cityUrl, $packageUrl, $hashTag);
		if (!is_array($profileData))
		{
			return $profileData; // validation failed (or) has custom view
		}

		return $this->renderDataToViewFiles('occasion/birthday/tents/profile', $profileData);
	}
}