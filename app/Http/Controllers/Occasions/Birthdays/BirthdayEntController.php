<?php

namespace App\Http\Controllers\Occasions\Birthdays;

use App\Http\Controllers\Base\BaseEntController;

class BirthdayEntController extends BaseEntController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.kids_birthdays.url'));
		$this->setOccasionId(config('evibe.occasion.kids_birthdays.id'));
		$this->setPageName(config('evibe.occasion.kids_birthdays.ent.name'));
		$this->setPageUrl(config('evibe.occasion.kids_birthdays.ent.results_url'));
	}

	public function showList($cityUrl, $categoryUrl = "", $fetchWithoutView = false, $collectionId = null)
	{
		$profileRoute = "city.occasion.birthdays.ent.profile";
		$listData = $this->getEntListData($cityUrl, $profileRoute, $categoryUrl, $fetchWithoutView, $collectionId);
		if (!is_array($listData))
		{
			return $listData; // validation failed, redirect
		}
		if ($fetchWithoutView)
		{
			return $listData;
		}

		return $this->renderDataToViewFiles('occasion/birthday/ent/list', $listData);
	}

	public function showDetails($cityUrl, $entUrl)
	{
		$hashTag = "#" . config('evibe.occasion.kids_birthdays.hashtag');
		$profileData = $this->getEntProfileData($cityUrl, $entUrl, $hashTag);
		if (!is_array($profileData))
		{
			return $profileData; // validation failed, redirect
		}

		$profileData = $this->addProductSpecificAddOns($profileData, config('evibe.occasion.kids_birthdays.id'), config('evibe.ticket.type.entertainment'), $profileData['id']);

		return $this->renderDataToViewFiles('occasion/birthday/ent/profile', $profileData);
	}
}