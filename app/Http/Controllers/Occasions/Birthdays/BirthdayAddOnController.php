<?php

namespace App\Http\Controllers\Occasions\Birthdays;

use App\Http\Controllers\Base\BaseAddOnController;
use App\Models\Types\TypeServices;

use Illuminate\Http\Request;

class BirthdayAddOnController extends BaseAddOnController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.kids_birthdays.url'));
		$this->setOccasionId(config('evibe.occasion.kids_birthdays.id'));
	}

	public function showList($cityUrl)
	{
		$urlParams = [['key' => 'city', 'value' => $cityUrl]];
		$validUrlObj = $this->validateUrlParams($urlParams);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$options = [];
		$pageId = $this->getPageId();
		$eventId = $this->getOccasionId();

		// @see: entertainment options are from services
		$services = TypeServices::with('gallery')
		                        ->forEvent($eventId)
		                        ->forCity()
		                        ->select('type_service.*')
		                        ->where('is_addon', 1)
		                        ->get();

		// sort by priority
		$allServicesId = $services->pluck('id')->all(); // get all IDs for merge
		$priorityIds = $this->getItemIdsByPriority($eventId, $pageId, $allServicesId); // get priorities
		$services = $services->all(); // to Array for sorting

		usort($services, function ($a, $b) use ($priorityIds)
		{
			return $this->sortItemsByPriority($a, $b, $priorityIds);
		});

		foreach ($services as $service)
		{
			$serviceUrl = route('city.occasion.birthdays.add-on.profile', [$cityUrl, $service->url]);
			$serviceData = [
				'id'         => $service->id,
				'name'       => $service->name,
				'info'       => $service->info,
				'worth'      => $service->worth_price,
				'minPrice'   => $service->min_price,
				'maxPrice'   => $service->max_price,
				'rangeInfo'  => $service->range_info,
				'profileImg' => $service->getProfilePic(),
				'fullPath'   => $serviceUrl,
				'code'       => $service->code,
			];

			array_push($options, $serviceData);
		}

		// SEO
		$cityName = getCityName();
		$pageTitle = "Best birthday photography fun eats: chocolate fountain, popcorn, photo booth & more in $cityName";
		$pageDescription = "$cityName best birthday party candid photo shoot, photography, 
			fun eats like popcorn counters, chocolate fountain, ice gola, cotton candy & more. 
			Best prices & quality service.";
		$pageHeader = "All Add-Ons Options For Birthday Parties";

		$data = [
			'type'       => 'add-on', // hardcoded
			'occasionId' => $this->getOccasionId(),
			'options'    => $options,
			'seo'        => [
				"pageTitle"       => ucfirst($pageTitle),
				"pageDescription" => $pageDescription,
				"pageHeader"      => ucwords($pageHeader)
			]
		];

		$data = array_merge($data, $this->mergeView(getCityUrl(), $this->getOccasionId()));

		return view('occasion.birthday.addon.list', ['data' => $data]);
	}

	public function showDetails($cityUrl, $entUrl)
	{
		$urlParams = [['key' => 'city', 'value' => $cityUrl], ['key' => 'type_service', 'value' => $entUrl]];
		$validUrlObj = $this->validateUrlParams($urlParams);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$service = $validUrlObj['type_service'];

		$gallery = [];
		$rawGallery = $service->gallery;
		foreach ($rawGallery as $item)
		{
			array_push($gallery, [
				'id'    => $item->id,
				'url'   => $item->getLink(),
				'thumb' => $item->getLink('thumb'),
				'title' => $item->title,
			]);
		}

		// SEO
		$pageUrl = Request::capture()->fullUrl();
		$cityName = getCityName();
		$pageTitle = "Best " . $service->name . " for birthday party in $cityName";
		$pageDescription = "Book $cityName best " . $service->name . " for birthday party kids entertainment, fun eats, activities. 
					See real pictures, reviews. Best prices and quality service.";

		// Social share
		$metaImage = isset($gallery[0]) && isset($gallery[0]['url']) ? $gallery[0]['url'] : "";
		$serviceTitle = $service->name . " In " . getCityName();
		$shareUrl = $this->getShareUrl($pageUrl, $serviceTitle, "#" . config('evibe . occasion . kids_birthdays . hashtag'), $metaImage);

		$data = [
			'id'         => $service->id,
			'code'       => $service->code,
			'name'       => $service->name,
			'info'       => $service->info,
			'worth'      => $service->worth_price,
			'minPrice'   => $service->min_price,
			'maxPrice'   => $service->max_price,
			'rangeInfo'  => $service->range_info,
			'preq'       => $service->prereq,
			'terms'      => $service->terms,
			'facts'      => $service->facts,
			'gallery'    => $gallery,
			'seo'        => [
				'pageTitle'       => $pageTitle,
				'pageDescription' => $pageDescription,
				'pageUrl'         => $pageUrl,
				'metaImage'       => $metaImage
			],
			'shareUrl'   => $shareUrl,
			'occasionId' => $this->getOccasionId()
		];

		$data = array_merge($data, $this->mergeView(getCityUrl(), $this->getOccasionId()));

		return view('occasion/birthday/addon/profile', ['data' => $data]);
	}
}