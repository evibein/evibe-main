<?php

namespace App\Http\Controllers\Occasions\Birthdays;

use App\Models\Trend\Trend;
use App\Models\Trend\TrendGallery;
use App\Http\Controllers\Base\BaseTrendController;
use Illuminate\Http\Request;
use Evibe\Facades\EvibeUtilFacade as EvibeUtil;
use Illuminate\Support\Facades\Log;

class BirthdayTrendController extends BaseTrendController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.kids_birthdays.url'));
		$this->setOccasionId(config('evibe.occasion.kids_birthdays.id'));
	}

	public function showList($cityUrl)
	{
		// validate url params & redirect to results if not valid
		$urlParams = [['key' => 'city', 'value' => $cityUrl]];
		$validUrlObj = $this->validateUrlParams($urlParams);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$cityId = getCityId();
		$cityName = getCityName();
		$pageId = $this->getPageId();
		$eventId = $this->getOccasionId();

		// @Author - Sushant
		// IS_LIVE option added
		$trends = Trend::where('trending.city_id', '=', $cityId)
		               ->select('trending.*')
		               ->where('trending.is_live', 1)
		               ->forEvent($eventId)
		               ->forCity()// added for the specific city
		               ->get();

		$startingPrice = $trends->min('price');

		// sort by priority
		$allTrendIds = $trends->pluck('id')->all(); // get all IDs for merge
		$priorityIds = $this->getItemIdsByPriority($eventId, $pageId, $allTrendIds); // get priorities
		$trends = $trends->all(); // to Array for sorting
		usort($trends, function ($a, $b) use ($priorityIds)
		{
			return $this->sortItemsByPriority($a, $b, $priorityIds);
		});

		$pageTitle = "Best birthday clown shows, magic shows, tattoo artists, kids entertainment, activities in $cityName";
		$pageHeader = "New Birthday Party Trends in $cityName";
		$pageDescription = "Book $cityName best birthday party kids entertainment, trending activities - 
			fun magic shows, clowns, balloon twisting, foreign tattoo artist for birthday party. 
			Rs. " . $this->formatPrice($startingPrice) . " onwards.";

		$data = [
			'existingQueryParams' => $this->getExistingQueryParams(),
			'trends'              => $trends,
			'occasionId'          => $this->getOccasionId(),
			'cityId'              => getCityId(),
			'mapTypeId'           => $this->getPageId(),
			'hasTrends'           => count($trends) ? true : false,
			'vendorType'          => 'trending',
			'seo'                 => [
				'pageTitle'       => ucfirst($pageTitle),
				'pageHeader'      => ucwords($pageHeader),
				'pageDescription' => $pageDescription
			],
		];

		$data = array_merge($data, $this->mergeView(getCityUrl(), $this->getOccasionId()));

		return view('occasion/birthday/trends/list', ['data' => $data]);
	}

	public function showProfile($cityUrl, $trendUrl)
	{
		$urlParams = [
			['key' => 'city', 'value' => $cityUrl],
			['key' => 'trend', 'value' => $trendUrl]
		];
		$validUrlObj = $this->validateUrlParams($urlParams);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$trend = $validUrlObj['trending'];
		$city = $trend->city->name;
		$provider = $trend->provider;
		$providerName = $provider->type->name . ' in ' . $provider->area->name . ', ' . getCityName();
		$providerUrl = $provider->getLink();
		$trendGallery = TrendGallery::with('trend')
		                            ->where('trending_id', '=', $trend->id)
		                            ->get();

		// For the SEO and share link purpose
		$pageUrl = Request::capture()->fullUrl();
		$pageTitle = "Best " . $trend->name . " - birthday party kids entertainment in $city";
		$pageDescription = "Book $city best " . $trend->name . " - latest birthday party kids entertainment, " .
			"activities & trend for Rs. " . $this->formatPrice($trend->price) . ". Best price & quality service.";

		$trendTitle = $trend->name . " In $city";
		$metaImage = isset($trendGallery[0]) && isset($trendGallery[0]['url']) ? $trendGallery[0]['url'] : "";
		$shareUrl = $this->getShareUrl($pageUrl, $trendTitle, "#" . config('evibe.occasion.kids_birthdays.hashtag'), $metaImage);

		$trendData = [
			'id'           => $trend->id,
			'name'         => $trend->name,
			'price'        => $trend->price,
			'code'         => 'trending-' . $trend->id,
			'info'         => $trend->info,
			'terms'        => $trend->terms,
			'facts'        => $trend->facts,
			'prereq'       => $trend->prerequisites,
			'vendorType'   => 'Trending',
			'city'         => $city,
			'images'       => $trendGallery,
			"providerId"   => $provider->id,
			'providerName' => $providerName,
			'providerUrl'  => $providerUrl,
			'providerCode' => $provider ? $provider->code : "",
			'seo'          => [
				'pageTitle'       => ucwords($pageTitle),
				'pageDescription' => $pageDescription,
				'pageUrl'         => $pageUrl,
				'metaImage'       => $metaImage,
			],
			'shareUrl'     => $shareUrl,
			'occasionId'   => $this->getOccasionId(),
			'ratings'      => $trend->provider->getPartnerReview()
		];

		$trendData = array_merge($trendData, $this->mergeView(getCityUrl(), $this->getOccasionId()));

		$shortlistDataParams = [
			'mapId'     => $trend->id,
			'mapTypeId' => $this->getPageId(),
		];

		$trendData = array_merge($trendData, $this->mergeShortlistData($shortlistDataParams));

		return view('occasion/birthday/trends/profile', ['data' => $trendData]);
	}
}