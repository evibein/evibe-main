<?php

/**
 * @author Anji <anji@evibe.in>
 * @since  26 May 2015
 */

namespace App\Http\Controllers\Occasions\Birthdays;

use App\Http\Controllers\Base\BaseCakeController;

class BirthdayCakeController extends BaseCakeController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.kids_birthdays.url'));
		$this->setOccasionId(config('evibe.occasion.kids_birthdays.id'));
		$this->setPageName(config('evibe.occasion.kids_birthdays.cakes.name'));
		$this->setPageUrl(config('evibe.occasion.kids_birthdays.cakes.results_url'));
	}

	public function showList($cityUrl, $fetchWithoutView = false, $collectionId = null)
	{
		$listData = $this->getCakeListData($cityUrl, $fetchWithoutView, $collectionId);
		if (!is_array($listData))
		{
			return $listData; // has error, redirect to given url
		}
		if ($fetchWithoutView)
		{
			return $listData;
		}

		return $this->renderDataToViewFiles('occasion/birthday/cake/list', $listData);
	}

	public function showProfile($cityUrl, $cakeUrl)
	{
		$hashTag = "#" . config('evibe.occasion.kids_birthdays.hashtag');
		$profileData = $this->getCakeProfileData($cityUrl, $cakeUrl, $hashTag);
		if (!is_array($profileData))
		{
			return $profileData; // validation failed, redirect
		}

		$profileData = $this->addProductSpecificAddOns($profileData, config('evibe.occasion.kids_birthdays.id'), config('evibe.ticket.type.cake'), $profileData['id']);

		return $this->renderDataToViewFiles('occasion/birthday/cake/profile', $profileData);
	}
}