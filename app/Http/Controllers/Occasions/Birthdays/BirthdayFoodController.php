<?php

namespace App\Http\Controllers\Occasions\Birthdays;

use App\Http\Controllers\Base\BaseFoodController;

class BirthdayFoodController extends BaseFoodController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.kids_birthdays.url'));
		$this->setOccasionId(config('evibe.occasion.kids_birthdays.id'));
		$this->setPageName(config('evibe.occasion.kids_birthdays.food.name'));
		$this->setPageUrl(config('evibe.occasion.kids_birthdays.food.results_url'));
	}

	public function showFoodList($cityUrl, $fetchWithoutView = false, $collectionId = null)
	{
		$profileUrl = config('evibe.occasion.kids_birthdays.food.profile_url');
		$listData = $this->getFoodListData($cityUrl, $profileUrl, $fetchWithoutView, $collectionId);
		if (!is_array($listData))
		{
			return $listData; // has error, redirect to given url
		}
		if ($fetchWithoutView)
		{
			return $listData;
		}

		return $this->renderDataToViewFiles('occasion/birthday/food/list', $listData);
	}

	public function showFoodProfile($cityUrl, $packageUrl)
	{
		$hashTag = "#" . config('evibe.occasion.kids_birthdays.hashtag');
		$profileData = $this->getFoodProfileData($cityUrl, $packageUrl, $hashTag);
		if (!is_array($profileData))
		{
			return $profileData;
		}

		$profileData = $this->addProductSpecificAddOns($profileData, config('evibe.occasion.kids_birthdays.id'), config('evibe.ticket.type.food'), $profileData['package']->id);

		return $this->renderDataToViewFiles('occasion/birthday/food/profile', $profileData);
	}
}
