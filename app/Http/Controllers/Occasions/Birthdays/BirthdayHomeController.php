<?php

namespace App\Http\Controllers\Occasions\Birthdays;

use App\Http\Controllers\Base\BaseHomeController;
use App\Models\Util\Country;

class BirthdayHomeController extends BaseHomeController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.kids_birthdays.url'));
		$this->setOccasionId(config('evibe.occasion.kids_birthdays.id'));
	}

	public function showHome($cityUrl)
	{
		$validUrlObj = $this->validateUrlParams($cityUrl);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$eventId = $this->getOccasionId();
		$cityId = getCityId();
		$topEntServices = $this->getTopEntServices($eventId);
		$topCakes = $this->getTopCakes($eventId);
		$topPackages = $this->getTopPackages($eventId);
		$topVenueDeals = $this->getTopVenueDeals($eventId);
		$trendingItems = $this->getTrendingItems($eventId);
		$topDecors = $this->getTopDecors($eventId);

		// fetches the collections for birthday home
		$eventUrl = $this->getOccasionUrl();
		$cityUrl = getCityUrl();
		$host = config('evibe.host');
		$profileUrl = config('evibe.occasion.collections.profile_url');
		$collectionBaseUrl = "$host/$cityUrl/$eventUrl/$profileUrl/";
		$collections = $this->getCollections($cityId, $eventId, 8);

		$isShowTopContainer = count($topPackages) || count($topVenueDeals) || count($trendingItems) || count($topDecors) || count($topCakes) || count($topEntServices);

		// reviews
		$eventTypes = [$eventId => 5];
		$customerStories = $this->getCustomerStories($eventTypes);
		$vendorStories = $this->getVendorStories();

		// SEO
		$seo = $this->getSeoText([getCityId(), $eventId, "landing"], "defaults.city.$eventId.landing");
		$pageTitle = $seo['pt'];
		$pageDescription = $seo['pd'];
		$pageHeader = $seo['ph'];
		$keyWords = $seo['kw'];
		$schema = [$this->schemaBreadcrumbList(["Evibe.in", "Birthday Party Planners", getCityName()])];

		// fetches auto bookable venue deals
		$autoBookableData = $this->getPaginatedAutoBookableItems(config('evibe.ticket.type.venue-deals'), $topVenueDeals, 3);

		$data = [
			'collectionBaseUrl'  => $collectionBaseUrl,
			'collections'        => $collections,
			'topVenueDeals'      => $topVenueDeals,
			'autoBook'           => $autoBookableData['autoBook'],
			'topDecors'          => $topDecors,
			'topCakes'           => $topCakes,
			'topEntServices'     => $topEntServices,
			'topPackages'        => $topPackages,
			'trendingItems'      => $trendingItems,
			'customerStories'    => $customerStories,
			'vendorStories'      => $vendorStories,
			'occasionUrl'        => config('evibe.occasion.kids_birthdays.url'),
			'occasionId'         => config('evibe.occasion.kids_birthdays.id'),
			'cityId'             => getCityId(),
			'isShowTopContainer' => $isShowTopContainer,
			'countries'          => Country::all(),
			'seo'                => [
				'pageTitle'       => ucfirst($pageTitle),
				'pageDescription' => $pageDescription,
				'pageHeader'      => $pageHeader,
				'keyWords'        => $keyWords,
				'schema'          => $schema
			]
		];

		$data = array_merge($data, $this->mergeView(getCityUrl(), $this->getOccasionId()));

		return view('occasion/birthday/home/home', ['data' => $data]);
	}
}