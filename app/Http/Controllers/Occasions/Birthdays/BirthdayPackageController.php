<?php

namespace App\Http\Controllers\Occasions\Birthdays;

use App\Http\Controllers\Base\BasePackageController;
use App\Models\Package\Package;
use App\Models\Types\TypeTag;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class BirthdayPackageController extends BasePackageController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.kids_birthdays.url'));
		$this->setOccasionId(config('evibe.occasion.kids_birthdays.id'));
		$this->setPageName(config('evibe.occasion.kids_birthdays.packages.name'));
		$this->setPageUrl(config('evibe.occasion.kids_birthdays.packages.results_url'));
	}

	public function showList($cityUrl)
	{
		$invalidPageIds = [config('evibe.ticket.type.venue-deals'), config('evibe.ticket.type.food'), config('evibe.ticket.type.tents')];
		$listData = $this->getListData($cityUrl, 21, true, $invalidPageIds);
		if (!is_array($listData))
		{
			return $listData; // has error, redirect to given url
		}

		$host = config('evibe.host');
		$profileUrl = config('evibe.occasion.kids_birthdays.packages.profile_url');
		$listData['profileBaseUrl'] = "$host/$cityUrl/" . $this->getOccasionUrl() . "/$profileUrl/";

		return $this->renderDataToViewFiles('occasion/birthday/packages/list', $listData);
	}

	public function showProfile($cityUrl, $packageUrl)
	{
		$profileData = $this->getProfileData($cityUrl, $packageUrl, "BirthdayPackage");
		if (!is_array($profileData))
		{
			return $profileData; // validation failed (or) has custom view
		}

		$profileData = $this->addProductSpecificAddOns($profileData, config('evibe.occasion.kids_birthdays.id'), config('evibe.ticket.type.package'), $profileData['package']->id);

		return $this->renderDataToViewFiles('occasion/birthday/packages/profile', $profileData);
	}
}