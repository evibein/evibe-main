<?php

namespace App\Http\Controllers\Occasions\Birthdays;

use App\Http\Controllers\Base\BaseVenueDealsController;
use App\Models\Types\TypeSlot;

use Illuminate\Http\Request;

class BirthdayVenueDealsController extends BaseVenueDealsController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionId(config('evibe.occasion.kids_birthdays.id'));
		$this->setOccasionUrl(config('evibe.occasion.kids_birthdays.url'));
		$this->setPageName(config('evibe.occasion.kids_birthdays.venue-deal.name'));
	}

	public function showList($cityUrl, $location = "all", $category = "all")
	{
		$listData = $this->getVenueDealsListData($cityUrl, $location, $category);
		if (!is_array($listData))
		{
			return $listData; // validation failed, redirect
		}

		return view('occasion/birthday/venue-deals/list', ['data' => $listData]);
	}

	public function showProfile($cityUrl, $profileUrl)
	{
		$hashTag = "#" . config('evibe.occasion.kids_birthdays.hashtag');
		$profileData = $this->getVenueDealProfileData($cityUrl, $profileUrl, $hashTag);
		if (!is_array($profileData))
		{
			return $profileData; // validation failed, redirect
		}

		return view('occasion/birthday/venue-deals/profile', ['data' => $profileData]);
	}
}