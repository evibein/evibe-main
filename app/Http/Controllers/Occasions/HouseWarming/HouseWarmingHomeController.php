<?php

namespace App\Http\Controllers\Occasions\HouseWarming;

use App\Http\Controllers\Base\BaseHomeController;
use App\Models\Package\Package;
use App\Models\Util\Country;

class HouseWarmingHomeController extends BaseHomeController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.house-warming.url'));
		$this->setOccasionId(config('evibe.occasion.house-warming.id'));
	}

	public function showHome($cityUrl)
	{
		$validUrlObj = $this->validateUrlParams($cityUrl);

		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$cityId = getCityId();
		$eventId = $this->getOccasionId();
		$customerStories = $this->getCustomerStories([$eventId => 5]);
		$vendorStories = $this->getVendorStories();

		$topDecors = $this->getTopDecors($eventId);
		$topFoodOptions = $this->getTopFoodPackages($eventId);
		$topAddons = $this->getTopEntServices($eventId);
		$topCakes = $this->getTopCakes($eventId);
		//$topTents = $this->getTopTentPackages($eventId);
		//$topPriests = $this->getTopPriestPackages($eventId);

		//fetches the collections for house warming home
		$eventUrl = $this->getOccasionUrl();
		$cityUrl = getCityUrl();
		$host = config('evibe.host');
		$profileUrl = config('evibe.occasion.collections.profile_url');
		$collectionBaseUrl = "$host/$cityUrl/$eventUrl/$profileUrl/";
		$collections = $this->getCollections($cityId, $eventId, 8);

		$isShowTopContainer = count($topDecors) || count($topFoodOptions) || count($topAddons) || count($topCakes);

		// fetches auto bookable packages --- Get only 3 items
		//$pageId = config('evibe.ticket.type.package');
		//$autoBookableData = $this->getPaginatedAutoBookablePackages($pageId, $topPackages, 3);

		// SEO
		$cityName = getCityName();
		$pageTitle = "Best house warming, griha pravesham flower decorations, priests, catering services in $cityName";
		$pageDescription = "Book $cityName best services including flower decorations, catering, traditional food packages, 
							tent house services and qualified priests for housewarming, griha pravehsam. 
							Best prices and 100% delivery guarantee.";
		$pageHeader = "Let's welcome your new home";

		$data = [
			'topDecors'          => $topDecors,
			'topFoodOptions'     => $topFoodOptions,
			'topAddons'          => $topAddons,
			'topCakes'           => $topCakes,
			//'topTents'        => $topTents,
			//'topPriests'      => $topPriests,
			//'autoBook'        => $autoBookableData['autoBook'],
			'customerStories'    => $customerStories,
			'vendorStories'      => $vendorStories,
			'occasionId'         => $this->getOccasionId(),
			'cityId'             => getCityId(),
			'collectionBaseUrl'  => $collectionBaseUrl,
			'collections'        => $collections,
			'isShowTopContainer' => $isShowTopContainer,
			'countries'          => Country::all(),
			'seo'                => [
				'pageTitle'       => $pageTitle,
				'pageDescription' => $pageDescription,
				'pageHeader'      => $pageHeader
			]
		];

		$data = array_merge($data, $this->mergeView(getCityUrl(), $this->getOccasionId()));

		return view('occasion/house-warming/home/home', ['data' => $data]);
	}

	// change it according to the priest package tags
	private function getTopPriestPackages($eventId)
	{
		$pageId = config('evibe.ticket.type.priests');

		$topPriestPkgIds = $this->getItemIdsByPriority($eventId, $pageId, [], 20);
		$topPriestPkgs = Package::with('gallery')->select('planner_package.*')
		                        ->joinTags()
		                        ->forEvent($eventId)
		                        ->forPage($pageId)
		                        ->isLive()
		                        ->forCity();

		if (count($topPriestPkgIds) > 0)
		{
			$topPriestPkgs = $topPriestPkgs->whereIn('planner_package.id', $topPriestPkgIds)
			                               ->get();

			// order topPriestPkgs by priority
			$topPriestPkgs = $this->getSortedListByPriorities($topPriestPkgs, $topPriestPkgIds);
			$topPriestPkgs = array_slice($topPriestPkgs, 0, 3); // take top 3
		}
		else
		{
			$topPriestPkgs = $topPriestPkgs->orderBy('created_at', 'DESC')
			                               ->take(3)// take latest 3
			                               ->get()
			                               ->all();
		}

		return $topPriestPkgs;
	}

	// change it according to the tent package
	private function getTopTentPackages($eventId)
	{
		$pageId = config('evibe.ticket.type.tents');

		$topTentPkgIds = $this->getItemIdsByPriority($eventId, $pageId, [], 20);
		$topTentPkgs = Package::with('gallery')->select('planner_package.*')
		                      ->joinTags()
		                      ->forEvent($eventId)
		                      ->forPage($pageId)
		                      ->isLive()
		                      ->forCity();

		if (count($topTentPkgIds) > 0)
		{
			$topTentPkgs = $topTentPkgs->whereIn('planner_package.id', $topTentPkgIds)
			                           ->get();

			// order topTentPkgs by priority
			$topTentPkgs = $this->getSortedListByPriorities($topTentPkgs, $topTentPkgIds);
			$topTentPkgs = array_slice($topTentPkgs, 0, 3); // take top 3
		}
		else
		{
			$topTentPkgs = $topTentPkgs->orderBy('created_at', 'DESC')
			                           ->take(3)// take latest 3
			                           ->get()
			                           ->all();
		}

		return $topTentPkgs;
	}
}