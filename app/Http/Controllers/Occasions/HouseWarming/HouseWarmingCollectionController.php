<?php

namespace App\Http\Controllers\Occasions\HouseWarming;

use App\Http\Controllers\Base\BaseCollectionController;

class HouseWarmingCollectionController extends BaseCollectionController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.house-warming.url'));
		$this->setOccasionId(config('evibe.occasion.house-warming.id'));
	}

	public function showCollectionList($cityUrl)
	{
		$listData = $this->getCollectionListData($cityUrl);
		if (!is_array($listData))
		{
			return $listData; // validation failed, redirect to given url
		}

		return view('occasion/house-warming/collection/list', ['data' => $listData]);

	}

	public function showCollectionProfile($cityUrl, $collectionUrl)
	{
		$profileData = $this->getCollectionProfileData($cityUrl, $collectionUrl);
		if (!is_array($profileData))
		{
			return $profileData; // validation failed, redirect to given url
		}

		return view('occasion/house-warming/collection/profile', ['data' => $profileData]);
	}
}