<?php

namespace App\Http\Controllers\Occasions\HouseWarming;

use App\Http\Controllers\Base\BasePriestController;

class HouseWarmingPriestController extends BasePriestController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.house-warming.url'));
		$this->setOccasionId(config('evibe.occasion.house-warming.id'));
		$this->setPageName(config('evibe.occasion.house-warming.priest.name'));
	}

	public function showPriestList($cityUrl)
	{
		$profileUrl = config('evibe.occasion.house-warming.priest.profile_url');
		$listData = $this->getPriestListData($cityUrl, $profileUrl);
		if (!is_array($listData))
		{
			return $listData; // validation failed, redirect
		}

		return $this->renderDataToViewFiles('occasion/house-warming/priest/list', $listData);
	}

	public function showPriestProfile($cityUrl, $packageUrl)
	{
		$hashTag = "#" . config('evibe.occasion.house-warming.hashtag');
		$profileData = $this->getPriestProfileData($cityUrl, $packageUrl, $hashTag);
		if (!is_array($profileData))
		{
			return $profileData; // validation failed (or) has custom view
		}

		return $this->renderDataToViewFiles('occasion/house-warming/priest/profile', $profileData);
	}
}