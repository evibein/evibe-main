<?php

namespace App\Http\Controllers\Occasions\HouseWarming;

use App\Http\Controllers\Base\BaseTentController;

class HouseWarmingTentController extends BaseTentController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.house-warming.url'));
		$this->setOccasionId(config('evibe.occasion.house-warming.id'));
		$this->setPageName(config('evibe.occasion.house-warming.tents.name'));
	}

	public function showTentList($cityUrl)
	{
		$profileUrl = config('evibe.occasion.house-warming.tents.profile_url');
		$listData = $this->getTentListData($cityUrl, $profileUrl);
		if (!is_array($listData))
		{
			return $listData; // validation failed, redirect
		}

		return $this->renderDataToViewFiles('occasion/house-warming/tents/list', $listData);

	}

	public function showTentProfile($cityUrl, $packageUrl)
	{
		$hashTag = "#" . config('evibe.occasion.house-warming.hashtag');
		$profileData = $this->getTentProfileData($cityUrl, $packageUrl, $hashTag);
		if (!is_array($profileData))
		{
			return $profileData; // validation failed (or) has custom view
		}

		return $this->renderDataToViewFiles('occasion/house-warming/tents/profile', $profileData);
	}
}