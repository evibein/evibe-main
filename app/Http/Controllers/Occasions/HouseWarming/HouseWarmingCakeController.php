<?php

namespace App\Http\Controllers\Occasions\HouseWarming;

use App\Http\Controllers\Base\BaseCakeController;

class HouseWarmingCakeController extends BaseCakeController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.house-warming.url'));
		$this->setOccasionId(config('evibe.occasion.house-warming.id'));
		$this->setPageName(config('evibe.occasion.house-warming.cakes.name'));
		$this->setPageUrl(config('evibe.occasion.house-warming.cakes.results_url'));
	}

	public function showList($cityUrl)
	{
		$listData = $this->getCakeListData($cityUrl);
		if (!is_array($listData))
		{
			return $listData; // has error, redirect to given url
		}

		return $this->renderDataToViewFiles('occasion/house-warming/cake/list', $listData);
	}

	public function showProfile($cityUrl, $cakeUrl)
	{
		$hashTag = "#" . config('evibe.occasion.kids_birthdays.hashtag');
		$profileData = $this->getCakeProfileData($cityUrl, $cakeUrl, $hashTag);
		if (!is_array($profileData))
		{
			return $profileData; // validation failed, redirect
		}

		return $this->renderDataToViewFiles('occasion/house-warming/cake/profile', $profileData);
	}
}