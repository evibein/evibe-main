<?php

namespace App\Http\Controllers\Occasions\HouseWarming;

use App\Http\Controllers\Base\BaseFoodController;

class HouseWarmingFoodController extends BaseFoodController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.house-warming.url'));
		$this->setOccasionId(config('evibe.occasion.house-warming.id'));
		$this->setPageName(config('evibe.occasion.house-warming.food.name'));
	}

	public function showFoodList($cityUrl)
	{
		$profileUrl = config('evibe.occasion.house-warming.food.profile_url');
		$listData = $this->getFoodListData($cityUrl, $profileUrl);
		if (!is_array($listData))
		{
			return $listData; // validation failed, redirect
		}

		return $this->renderDataToViewFiles('occasion/house-warming/food/list', $listData);
	}

	public function showFoodProfile($cityUrl, $packageUrl)
	{
		$hashTag = "#" . config('evibe.occasion.house-warming.hashtag');
		$profileData = $this->getFoodProfileData($cityUrl, $packageUrl, $hashTag);
		if (!is_array($profileData))
		{
			return $profileData;
		}

		return $this->renderDataToViewFiles('occasion/house-warming/food/profile', $profileData);
	}
}