<?php

namespace App\Http\Controllers\Occasions\HouseWarming;

use App\Http\Controllers\Base\BaseDecorController;

class HouseWarmingDecorController extends BaseDecorController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.house-warming.url'));
		$this->setOccasionId(config('evibe.occasion.house-warming.id'));
		$this->setPageName(config('evibe.occasion.house-warming.decors.name'));
		$this->setPageUrl(config('evibe.occasion.house-warming.decors.results_url'));
	}

	public function showList($cityUrl, $categoryUrl = "")
	{
		$listData = $this->getDecorsListProductData($cityUrl, $categoryUrl);
		if (!is_array($listData))
		{
			return $listData; // has error, redirect to given url
		}

		return $this->renderDataToViewFiles('occasion/house-warming/decors/list', $listData);
	}

	public function showProfile($cityUrl, $decorUrl)
	{
		$profileData = $this->getDecorProfileData($cityUrl, $decorUrl);
		if (!is_array($profileData))
		{
			return $profileData; // validation error, redirect to given url
		}

		return $this->renderDataToViewFiles('occasion/house-warming/decors/profile', $profileData);
	}
}