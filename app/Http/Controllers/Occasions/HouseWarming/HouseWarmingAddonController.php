<?php

namespace App\Http\Controllers\Occasions\HouseWarming;

use App\Http\Controllers\Base\BaseEntController;

class HouseWarmingAddonController extends BaseEntController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.house-warming.url'));
		$this->setOccasionId(config('evibe.occasion.house-warming.id'));
		$this->setPageName(config('evibe.occasion.house-warming.ent.name'));
		$this->setPageUrl(config('evibe.occasion.house-warming.ent.results_url'));
	}

	public function showList($cityUrl,  $categoryUrl = "")
	{
		$profileRoute = "city.occasion.house-warming.ent.profile";
		$listData = $this->getEntListData($cityUrl, $profileRoute, $categoryUrl);
		if (!is_array($listData))
		{
			return $listData; // validation failed, redirect
		}

		return $this->renderDataToViewFiles('occasion/house-warming/add-on/list', $listData);
 	}

	public function showProfile($cityUrl, $entUrl)
	{
		$hashTag = "#" . config('evibe.occasion.house-warming.hashtag');
		$profileData = $this->getEntProfileData($cityUrl, $entUrl, $hashTag);
		if (!is_array($profileData))
		{
			return $profileData; // validation failed, redirect
		}

		return $this->renderDataToViewFiles('occasion/house-warming/add-on/profile', $profileData);
	}
}