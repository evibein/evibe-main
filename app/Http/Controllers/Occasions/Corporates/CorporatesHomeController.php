<?php

namespace App\Http\Controllers\Occasions\Corporates;

use App\Http\Controllers\Base\BaseHomeController;
use App\Jobs\Emails\Tickets\MailCorporateEnquiryAlertToTeam;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TypeTicketSource;

class CorporatesHomeController extends BaseHomeController
{
	public function __construct()
	{
		parent::__construct();

		$this->setOccasionUrl(config('evibe.occasion.corporates.url'));
		$this->setOccasionId(config('evibe.occasion.corporates.id'));
	}

	public function showCorporateHome($cityUrl)
	{
		$validUrlObj = $this->validateUrlParams($cityUrl);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}
		$ticketSources = TypeTicketSource::where('show_customer', 1)
		                                 ->get();

		$defaultKey = "landing.corporates";
		$seo = $this->getSeoText([], $defaultKey);

		$data = [
			'seo'           => [
				"pageTitle" => ucwords($seo['pt']),
				"pageDesc"  => $seo['pd'],
				"keyWords"  => $seo['kw']
			],
			'ticketSources' => $ticketSources
		];

		return view('landing-pages/corporate/home', ['data' => $data]);
	}

	public function saveCorporateEnquiry($cityUrl)
	{
		$rules = [
			'name'         => 'required|min:4',
			'phone'        => 'required|min:10|max:10',
			'email'        => 'required|email',
			'companyName'  => 'required',
			'date'         => 'required',
			'requirements' => 'min:10'
		];
		$message = [
			'name.required'        => 'Please enter your name',
			'name.min'             => 'Name cannot be less than 4 character',
			'email.required'       => 'Please enter your email',
			'email.email'          => 'Please enter a valid email',
			'phone.required'       => 'please enter your phone number',
			'phone.min'            => 'please enter a valid number',
			'phone.max'            => 'please enter a valid number',
			'companyName.required' => 'Please enter the company name',
			'date.required'        => 'Please enter the event date',
			'requirements.min'     => 'Use minimum 10 words to mention your requirements',
		];

		$validator = validator(request()->all(), $rules, $message);
		if ($validator->fails())
		{
			return response()->json(['success' => false, 'error' => $validator->messages()->first()]);
		}

		try
		{
			$userSourceId = request('source');
			$userSourceId = ($userSourceId > 0) ? $userSourceId : null;

			$inputData = [
				'name'              => request('name'),
				'phone'             => request('phone'),
				'email'             => request('email'),
				'event_date'        => strtotime(str_replace('-', '/', request('date'))),
				'company_name'      => request('companyName'),
				'comments'          => request('requirements'),
				'source_id'         => $userSourceId,
				'enquiry_source_id' => config('evibe.ticket.enquiry_source.corporate')
			];

			// save ticket
			$ticket = Ticket::create($inputData);
			if (!$ticket)
			{
				$this->logError("Failed to create ticket, email: " . request("email") . ", phone: " . request("phone"));

				return response()->json([
					                        'success' => false,
					                        'errors'  => 'An error occurred while submitting your request. Please try again later.'
				                        ]);
			}

			$this->updateTicketAction([
				                          'ticket'   => $ticket,
				                          'comments' => "Corporate enquiry raised by customer"
			                          ]);

			$ticketId = $ticket->id;
			$enquiryId = config('evibe.ticket.enq.pre.home_page') . $ticketId;
			$ticket->enquiry_id = $enquiryId;
			$ticket->save();

			$sourceText = '--';
			if ($userSourceId)
			{
				$ticketSource = TypeTicketSource::find($userSourceId);
				$sourceText = $ticketSource ? $ticketSource->name : '--';
			}

			// send email to team
			$emailData = [
				'ticketId'    => $ticketId,
				'name'        => request('name'),
				'email'       => request('email'),
				'phone'       => request('phone'),
				'eventDate'   => str_replace('-', '/', request('date')),
				'comments'    => request('requirements'),
				'companyName' => request('companyName'),
				'cityName'    => $cityUrl,
				'source'      => $sourceText
			];

			$this->dispatch(new MailCorporateEnquiryAlertToTeam($emailData));

			// @todo: send email and SMS alert to customer

		} catch (\Exception $e)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'Some error while uploading, please try again after some time'
			                        ]);
		}

		return response()->json(['success' => true]);
	}
}