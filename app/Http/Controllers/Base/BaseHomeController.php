<?php

namespace App\Http\Controllers\Base;

use App\Models\Cake\Cake;
use App\Models\Decor\Decor;
use App\Models\Package\Package;
use App\Models\Package\PackageTag;
use App\Models\Trend\Trend;
use App\Models\Types\TypeServices;
use App\Models\Util\CustomerStory;
use App\Models\Util\VendorStory;
use App\Models\Venue\VenueHall;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BaseHomeController extends BaseController
{
	public function __construct()
	{
		parent::__construct();

		// @see: nothing here. Do not delete this method
	}

	protected function getTopVenues($eventId)
	{
		$cityId = getCityId();
		$topVenueHallIds = $this->getItemIdsByPriority($eventId, config('evibe.ticket.type.halls'), [], 20);
		$topVenueHalls = VenueHall::with('venue')
		                          ->select('venue_hall.*')
		                          ->ofCity($cityId)
		                          ->forEvent($eventId);

		if (count($topVenueHallIds) > 0)
		{
			$topVenueHalls = $topVenueHalls->whereIn('venue_hall.id', $topVenueHallIds)
			                               ->get();

			// order items by priority
			$topVenueHalls = $this->getSortedListByPriorities($topVenueHalls, $topVenueHallIds);
			$topVenueHalls = array_slice($topVenueHalls, 0, 8);
		}
		else
		{
			$topVenueHalls = $topVenueHalls->whereNull('venue.deleted_at')
			                               ->distinct('venue_hall.id')
			                               ->orderBy('created_at', 'DESC')
			                               ->take(8)
			                               ->get()
			                               ->all();
		}

		return $topVenueHalls;
	}

	/**
	 * @author Vikash <vikash@evibe.in>
	 * @Since  24 June 2016
	 * Venue Deals
	 */
	protected function getTopVenueDeals($eventId)
	{
		$topVenueDealsIds = $this->getItemIdsByPriority($eventId, config('evibe.ticket.type.venue-deals'), [], 20);
		$topVenueDeals = Package::select('planner_package.*')
		                        ->joinTags()
		                        ->forCity()
		                        ->forEvent($eventId)
		                        ->forPage(config('evibe.ticket.type.venue-deals'))
		                        ->isLive()
		                        ->groupBy('planner_package.id');

		if (count($topVenueDealsIds) > 0)
		{
			$topVenueDeals = $topVenueDeals->whereIn('planner_package.id', $topVenueDealsIds)
			                               ->get();

			$topVenueDeals = $this->getSortedListByPriorities($topVenueDeals, $topVenueDealsIds);
			$topVenueDeals = array_slice($topVenueDeals, 0, 8);
		}
		else
		{
			$topVenueDeals = $topVenueDeals->orderBy('created_at', 'DESC')
			                               ->take(8)
			                               ->get()
			                               ->all();
		}

		return $topVenueDeals;
	}

	protected function getTopPackages($eventId)
	{
		$invalidPageTypes = [config('evibe.ticket.type.venue-deals'), config('evibe.ticket.type.food')];
		$pageId = config('evibe.ticket.type.package');

		$cacheKey = "top-options-$pageId-$eventId-" . getCityId();
		$cacheKey = base64_encode($cacheKey);
		if (Cache::has($cacheKey))
		{
			$topPackageIds = Cache::get($cacheKey);
		}
		else
		{
			$topPackageIds = $this->getItemIdsByPriority($eventId, $pageId, []);
			Cache::put($cacheKey, $topPackageIds, 24 * 60);
		}

		$topPackages = Package::select('planner_package.*')
		                      ->joinTags()
		                      ->forCity()
		                      ->forEvent($eventId)
		                      ->forNotPage($invalidPageTypes)
		                      ->isLive()
		                      ->groupBy('planner_package.id');

		if (count($topPackageIds) > 0)
		{
			$topPackages = $topPackages->whereIn('planner_package.id', $topPackageIds)
			                           ->get();

			$topPackages = $this->getSortedListByPriorities($topPackages, $topPackageIds);
			$topPackages = array_slice($topPackages, 0, 8);
		}
		else
		{
			$topPackages = $topPackages->orderBy('created_at', 'DESC')
			                           ->take(8)
			                           ->get()
			                           ->all();
		}

		return $topPackages;
	}

	protected function getTopDecors($eventId)
	{
		$pageId = config('evibe.ticket.type.decor');
		$cacheKey = "top-options-$pageId-$eventId-" . getCityId();
		$cacheKey = base64_encode($cacheKey);
		if (Cache::has($cacheKey))
		{
			$topDecorIds = Cache::get($cacheKey);
		}
		else
		{
			$topDecorIds = $this->getItemIdsByPriority($eventId, $pageId, []);
			Cache::put($cacheKey, $topDecorIds, 24 * 60);
		}

		$topDecors = Decor::with('tags', 'gallery', 'provider', 'provider.city')
		                  ->select('decor.*')
		                  ->forEvent($eventId)
		                  ->isLive()
		                  ->forCity();

		if (count($topDecorIds) > 0)
		{
			$topDecors = $topDecors->whereIn('decor.id', $topDecorIds)
			                       ->get();

			$topDecors = $this->getSortedListByPriorities($topDecors, $topDecorIds);
			$topDecors = array_slice($topDecors, 0, 8);
		}
		else
		{
			$topDecors = $topDecors->orderBy('created_at', 'DESC')
			                       ->take(8)
			                       ->get()
			                       ->all();
		}

		return $topDecors;
	}

	protected function getTopCakes($eventId)
	{
		$pageId = config('evibe.ticket.type.cake');
		$cacheKey = "top-options-$pageId-$eventId-" . getCityId();
		$cacheKey = base64_encode($cacheKey);
		if (Cache::has($cacheKey))
		{
			$topCakeIds = Cache::get($cacheKey);
		}
		else
		{
			$topCakeIds = $this->getItemIdsByPriority($eventId, $pageId, []);
			Cache::put($cacheKey, $topCakeIds, 24 * 60);
		}

		$topCakes = Cake::with('tags', 'gallery', 'provider', 'provider.city')
		                ->select('cake.*')
		                ->forEvent($eventId)
		                ->isLive()
		                ->forCity();

		if (count($topCakeIds) > 0)
		{
			$topCakes = $topCakes->whereIn('cake.id', $topCakeIds)
			                     ->get();

			$topCakes = $this->getSortedListByPriorities($topCakes, $topCakeIds);
			$topCakes = array_slice($topCakes, 0, 12);
		}
		else
		{
			$topCakes = $topCakes->orderBy('created_at')
			                     ->take(12)
			                     ->get()
			                     ->all();
		}

		return $topCakes;
	}

	protected function getTopEntServices($eventId)
	{
		$pageId = config('evibe.ticket.type.service');
		$cacheKey = "top-options-$pageId-$eventId-" . getCityId();
		$cacheKey = base64_encode($cacheKey);
		if (Cache::has($cacheKey))
		{
			$topEntOptionIds = Cache::get($cacheKey);
		}
		else
		{
			$topEntOptionIds = $this->getItemIdsByPriority($eventId, $pageId, []);
			Cache::put($cacheKey, $topEntOptionIds, 24 * 60);
		}

		$topEntOptions = TypeServices::with('gallery')
		                             ->select('type_service.*')
		                             ->forEvent($eventId)
		                             ->forCity();

		if (count($topEntOptionIds) > 0)
		{
			$topEntOptions = $topEntOptions->whereIn('type_service.id', $topEntOptionIds)
			                               ->get();

			$topEntOptions = $this->getSortedListByPriorities($topEntOptions, $topEntOptionIds);
			$topEntOptions = array_slice($topEntOptions, 0, 8);
		}
		else
		{
			$topEntOptions = $topEntOptions->orderBy('created_at', 'DESC')
			                               ->take(8)
			                               ->get()
			                               ->all();
		}

		return $topEntOptions;
	}

	protected function getTrendingItems($eventId)
	{
		$cityId = getCityId();
		$topTrendIds = $this->getItemIdsByPriority($eventId, config('evibe.ticket.type.trend'), [], 20);
		$topTrends = Trend::select('trending.*')
		                  ->forEvent($eventId)
		                  ->isLive()
		                  ->where('trending.city_id', $cityId);

		if (count($topTrendIds) > 0)
		{
			$topTrends = $topTrends->whereIn('trending.id', $topTrendIds)
			                       ->get();

			// order items by priority
			$topTrends = $this->getSortedListByPriorities($topTrends, $topTrendIds);
			$topTrends = array_slice($topTrends, 0, 8);
		}
		else
		{
			$topTrends = $topTrends->orderBy('created_at', 'DESC')
			                       ->forCity()
			                       ->take(8)
			                       ->get()
			                       ->all();
		}

		return $topTrends;
	}

	protected function getCustomerStories($eventTypes)
	{
		$cityId = getCityId();
		$stories = [];

		foreach ($eventTypes as $eventId => $count)
		{
			$currentStories = CustomerStory::with('event')
			                               ->where('event_id', $eventId)
			                               ->forCity($cityId)
			                               ->orderBy(DB::raw("ISNULL(priority), priority"), 'ASC')
			                               ->orderBy('created_at', 'DESC')
			                               ->take($count)
			                               ->get()
			                               ->toArray();

			$stories = array_merge($stories, $currentStories);
		}

		// if stories for a given occasion is less than 3
		// get stories from all the cities for the given occasion
		if (count($stories) && (count($stories) < 3) && count($eventTypes) == 1)
		{
			foreach ($eventTypes as $eventId => $count)
			{
				$topStories = CustomerStory::with('event')
				                           ->where('event_id', $eventId)
				                           ->where('city_id', '!=', $cityId)
				                           ->orderBy(DB::raw("ISNULL(priority), priority"), 'ASC')
				                           ->orderBy('created_at', 'DESC')
				                           ->take(($count - count($stories)))
				                           ->get()
				                           ->toArray();

				$stories = array_merge($stories, $topStories);
			}
		}

		// if stories for a city home page is less than 3
		// get stories from all the cities irrespective of occasion
		if (count($stories) && count($stories) < 3 && count($eventTypes) > 1)
		{
			$topStories = CustomerStory::with('event')
			                           ->where('city_id', '!=', $cityId)
			                           ->orderBy(DB::raw("ISNULL(priority), priority"), 'ASC')
			                           ->orderBy('created_at', 'DESC')
			                           ->take((5 - count($stories)))
			                           ->get()
			                           ->toArray();

			$stories = array_merge($stories, $topStories);
		}

		// no stories found for given occasions
		// return best ones of all in this city
		if (!count($stories))
		{
			$topStories = CustomerStory::with('event')
			                           ->forCity($cityId)
			                           ->orderBy(DB::raw("ISNULL(priority), priority"), 'ASC')
			                           ->orderBy('created_at', 'DESC')
			                           ->take(8)
			                           ->get()
			                           ->toArray();

			$stories = array_merge($stories, $topStories);
		}

		// if no stories found get the latest customer stories irrespective of city
		if (!count($stories))
		{
			$topStories = CustomerStory::with('event')
			                           ->orderBy(DB::raw("ISNULL(priority), priority"), 'ASC')
			                           ->orderBy('created_at', 'DESC')
			                           ->take(8)
			                           ->get()
			                           ->toArray();

			$stories = array_merge($stories, $topStories);
		}

		return $stories;
	}

	protected function getVendorStories($eventTypes = [])
	{
		$stories = VendorStory::with('provider', 'provider.type')
		                      ->orderBy('created_at', 'DESC')
		                      ->get()
		                      ->toArray();

		return $stories;
	}

	/**
	 * Moved from BachelorsHomeController to BaseHomeController
	 * used in HouseWarmingHomeController also
	 *
	 * @since 28 Oct 2016
	 *
	 */
	protected function getTopFoodPackages($eventId)
	{
		$pageId = config('evibe.ticket.type.food');

		$cacheKey = "top-options-$pageId-$eventId-" . getCityId();
		$cacheKey = base64_encode($cacheKey);
		if (Cache::has($cacheKey))
		{
			$topFoodPkgIds = Cache::get($cacheKey);
		}
		else
		{
			$topFoodPkgIds = $this->getItemIdsByPriority($eventId, $pageId, []);
			Cache::put($cacheKey, $topFoodPkgIds, 24 * 60);
		}

		$topFoodPkgIds = $this->getItemIdsByPriority($eventId, $pageId, [], 20);
		$topFoodPkgs = Package::with('gallery')->select('planner_package.*')
		                      ->joinTags()
		                      ->forEvent($eventId)
		                      ->forPage($eventId, $pageId)
		                      ->groupBy('planner_package.id')// avoid repetition
		                      ->isLive()
		                      ->forCity();

		if (count($topFoodPkgIds) > 0)
		{
			$topFoodPkgs = $topFoodPkgs->whereIn('planner_package.id', $topFoodPkgIds)
			                           ->get();

			// order topFoodPkgs by priority
			$topFoodPkgs = $this->getSortedListByPriorities($topFoodPkgs, $topFoodPkgIds);
			$topFoodPkgs = array_slice($topFoodPkgs, 0, 8);
		}
		else
		{
			$topFoodPkgs = $topFoodPkgs->orderBy('created_at', 'DESC')
			                           ->take(8)
			                           ->get()
			                           ->all();
		}

		return $topFoodPkgs;
	}

	protected function getTopSurprisePackages($eventId, $ref = null)
	{
		$pageId = config('evibe.ticket.type.surprises');

		$cacheKey = "surprise-top-options-$ref-$pageId-$eventId-" . getCityId();
		$cacheKey = base64_encode($cacheKey);
		if (Cache::has($cacheKey))
		{
			$sortedSurprisePackageIds = Cache::get($cacheKey);
		}
		else
		{
			$sortedSurprisePackageIds = $this->getItemIdsByPriority($eventId, $pageId, []);
			Cache::put($cacheKey, $sortedSurprisePackageIds, 24 * 60);
		}

		$topSurprisePkgs = Package::with('gallery')->select('planner_package.*')
		                          ->joinTags()
		                          ->forEvent($eventId)
		                          ->forPage($pageId)
		                          ->orderBy('created_at', 'DESC') // default order
		                          ->groupBy('planner_package.id') // avoid repetition
		                          ->isLive()
		                          ->forCity();

		$CLDPackageIds = PackageTag::whereIn("planner_package_id", $sortedSurprisePackageIds)
		                           ->where("tile_tag_id", config("evibe.type-tag.candle-light-dinners"))
		                           ->pluck("planner_package_id")->toArray();

		if ($ref == "cld")
		{
			$topSurprisePkgs = $topSurprisePkgs->whereIn('planner_package.id', $CLDPackageIds);

			/* excluding VDay 2020 CLD packages */
			$vDayCLDPackagesIds = PackageTag::whereIn("planner_package_id", $CLDPackageIds)
			                                ->where("tile_tag_id", config("evibe.type-tag.valentines-day-2020"))
			                                ->pluck("planner_package_id")->toArray();

			$topSurprisePkgs = $topSurprisePkgs->whereNotIn('planner_package.id', $vDayCLDPackagesIds);
		}
		elseif ($ref == "non-cld")
		{
			$topSurprisePkgs = $topSurprisePkgs->whereNotIn('planner_package.id', $CLDPackageIds);
		}

		$topSurprisePkgs = $topSurprisePkgs->get();

		if (count($topSurprisePkgs))
		{
			if (count($sortedSurprisePackageIds) > 0)
			{
				// order topSurprisePkgs by priority
				$topSurprisePkgs = $this->getSortedListByPriorities($topSurprisePkgs, $sortedSurprisePackageIds);
				$topSurprisePkgs = array_slice($topSurprisePkgs, 0, 8);
			}
			else
			{
				$topSurprisePkgs = $topSurprisePkgs->take(8);
			}

			return $topSurprisePkgs;
		}
		else
		{
			return [];
		}
	}
}