<?php

namespace App\Http\Controllers\Base;

use App\Http\Controllers\Occasions\Surprises\SurprisesHomeController;
use App\Models\Package\Package;
use App\Models\Package\PackageGallery;
use App\Models\Package\PackageTag;
use App\Models\Util\Area;
use App\Models\Util\Country;
use App\Models\Util\OptionAvailability;
use App\Models\Venue\Venue;
use Carbon\Carbon;
use Evibe\Facades\EvibeUtilFacade;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class BasePackageController extends BaseResultsController
{
	public function __construct()
	{
		parent::__construct();
		$this->setPageId(config('evibe.ticket.type.package'));
	}

	public function getListData($cityUrl, $perPage = 21, $isIncludePageFilter = true, $invalidPageIds = null, $surData = [], $fetchWithoutView = false, $categoryUrl = null, $collectionId = null)
	{
		// validate URL params
		$urlParams = [['key' => 'city', 'value' => $cityUrl]];
		$validUrlObj = $this->validateUrlParams($urlParams);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$cacheKey = $this->deviceSpecificCacheKey(request()->fullUrl());
		$cacheKey = base64_encode($cacheKey);
		if (Cache::has($cacheKey) && !($fetchWithoutView))
		{
			$listData = Cache::get($cacheKey);
		}
		else
		{

			$listData = $this->getListDataFromDB($cityUrl, $perPage, $isIncludePageFilter, $invalidPageIds, $surData, $categoryUrl, $collectionId);
			if (!($fetchWithoutView))
			{
				Cache::put($cacheKey, $listData, config('evibe.cache.refresh-time'));
			}
		}

		return $listData;
	}

	public function getListDataFromDB($cityUrl, $perPage, $isIncludePageFilter, $invalidPageIds, $surData, $categoryUrl = null, $collectionId = null)
	{
		$cityId = getCityId();
		$cityName = getCityName();
		$eventId = $this->getOccasionId();
		$eventUrl = $this->getOccasionUrl();
		$pageId = $this->getPageId();
		$pageName = $this->getPageName();
		$clearFilter = false;
		$active = false;
		$hidePriceCategories = false;
		if (is_null($categoryUrl))
		{
			$categoryUrl = request()->has('category') ? request('category') : '';
		}

		// SEO
		$itemsListKeys = [
			"Evibe.in",
			$cityName,
			getTextFromSlug($eventUrl),
			getTextFromSlug($pageName)
		];
		$schema = [$this->schemaBreadcrumbList($itemsListKeys)];

		// get distinct packages with these tags
		$packages = Package::isLive()
		                   ->forEvent($eventId)
		                   ->forCity()
		                   ->select('planner_package.*');

		/**
		 * Do not include CLD packages in surprises packages since it got moved outside
		 */
		$cldPackageIds = PackageTag::where("tile_tag_id", config("evibe.type-tag.candle-light-dinners"))
		                           ->pluck("planner_package_id")
		                           ->toArray();

		$packages->whereNotIn("planner_package.id", array_unique($cldPackageIds));

		if ($isIncludePageFilter)
		{
			$packages->forPage($pageId);
		}

		if (!is_null($invalidPageIds))
		{
			$packages->forNotPage($invalidPageIds);
		}

		// for SEO
		$srTag = $saTag = $soTag = null;
		$seoKey = 'list';
		$seoReplaces = [
			'{category_name}'  => '',
			'{category_title}' => '',
			'{category_desc}'  => '',
			'{relation}'       => '',
			'{occasion}'       => ''
		];

		// for filters
		$noSurCatPackages = false;
		$filterData = $this->getFilterData($eventId, $pageId, $categoryUrl, $collectionId);
		$allTagIdsBank = $filterData['allTagIdsBank'];
		$allCategories = $filterData['allCategories'];
		$allFilterTagIds = $filterData['allFilterTagIds'];
		$userFilterId = null;

		/**
		 * Removing CLD filter at the top & getting it to the bottom
		 */
		if (isset($allCategories[config("evibe.type-tag.candle-light-dinners")]))
		{
			$value = $allCategories[config("evibe.type-tag.candle-light-dinners")];
			unset($allCategories[config("evibe.type-tag.candle-light-dinners")]);
			$allCategories[config("evibe.type-tag.candle-light-dinners")] = $value;
		}

		// check if category or filter - preference to category
		if ($surData || $categoryUrl || $collectionId)
		{
			$packageTags = PackageTag::all(); // @todo: optimise fetching all package tags

			if ($surData)
			{
				$srArray = explode("-", $surData['srString']);
				$soArray = explode("-", $surData['soString']);
				$saArray = explode("-", $surData['saString']);
				$srTag = $srArray[0];
				$soTag = $soArray[0];
				$saTag = $saArray[0];

				($srTag != 0) ? ($seoReplaces['{relation}'] = "for " . $srArray[1]) : "";
				($soTag != 0) ? ($seoReplaces['{occasion}'] = "for " . $soArray[1]) : "";

				// Resetting the landing page filter after selection of category
				if ($categoryUrl || $collectionId)
				{
					$srTag = 0;
				}

				if ((string)$srTag == "land")
				{
					$landingOptionIds = config("landing-options.options." . getCityId() . "." . config("evibe.occasion.surprises.id") . "." . config("evibe.ticket.type.package"));
					$landingOptionIds = is_array($landingOptionIds) ? $landingOptionIds : [];
					$packages = $packages->whereIn('planner_package.id', $landingOptionIds);
				}
				else
				{
					if ($srTag || $soTag)
					{
						$seoKey = 'filter';
					}

					$pckIds = $packageTags->pluck('planner_package_id')->toArray();

					if (count($pckIds) > 0)
					{
						if ($srTag != 0)
						{
							$surRelPckIds = $packageTags->where('tile_tag_id', $srTag)
							                            ->pluck('planner_package_id')
							                            ->toArray();

							$pckIds = array_intersect($pckIds, $surRelPckIds);
						}

						if ($soTag != 0)
						{
							$surOccPckIds = $packageTags->where('tile_tag_id', $soTag)
							                            ->pluck('planner_package_id')
							                            ->toArray();

							$pckIds = array_intersect($pckIds, $surOccPckIds);
						}

						if ($saTag != 0)
						{
							$surAgePckIds = $packageTags->where('tile_tag_id', $saTag)
							                            ->pluck('planner_package_id')
							                            ->toArray();

							$pckIds = array_intersect($pckIds, $surAgePckIds);
						}

						$packages = $packages->whereIn('planner_package.id', array_unique($pckIds));

						if (!($packages->count() > 0))
						{
							$noSurCatPackages = true;
						}
					}
				}
			}

			// filter:category - $seokey preference will be given for category
			if ($categoryUrl || $collectionId)
			{
				$seoKey = 'category';
				$categoryName = $categoryUrl ? ucwords(str_replace('-', ' ', $categoryUrl)) : '';

				// @todo: get values from database
				$seoReplaces['{category_name}'] = $categoryName;
				$seoReplaces['{category_title}'] = $categoryName;
				$seoReplaces['{category_desc}'] = $categoryName;

				$hasFilter = $filterData['hasFilter'];

				if ($hasFilter)
				{
					if ($surData)
					{
						$surPackageIds = $packageTags->whereIn('tile_tag_id', $allFilterTagIds)
						                             ->pluck('planner_package_id');
						$packages->whereIn('planner_package.id', $surPackageIds);
					}
					else
					{
						$surPackageIds = $packageTags->whereIn('tile_tag_id', $allFilterTagIds)
						                             ->pluck('planner_package_id');
						$packages->whereIn('planner_package.id', $surPackageIds);
					}

					$clearFilter = true;
					$active = $categoryUrl;
					if (isset($filterData['userFilterTag']) && $filterData['userFilterTag'])
					{
						$userFilterId = $filterData['userFilterTag']->id;
					}
				}
			}
		}

		$seoConfigKey = "defaults.city.$eventId.$pageId.$seoKey";
		$seo = $this->getSeoText([$cityId, $eventId, $pageId, $seoKey], $seoConfigKey, $seoReplaces);
		$pageTitle = ucwords($seo['pt']);
		$pageDescription = $seo['pd'];
		$pageHeader = ucwords($seo['ph']);
		$keyWords = $seo['kw'];

		// surprise category filters (@see: same as surprises home controller)
		$surprisesHome = new SurprisesHomeController();
		$surCategories = $surprisesHome->getSurpriseCategories();

		// filter:search
		$userSearch = "";
		if (request()->has('search') && trim(request()->get('search')))
		{
			$clearFilter = true;
			$userSearch = trim(request()->get('search'));
			$searchWords = explode(' ', $userSearch);

			$packages->where(function ($searchQuery) use ($searchWords) {
				foreach ($searchWords as $word)
				{
					$searchQuery->orWhere('planner_package.name', 'LIKE', "%$word%")
					            ->orWhere('planner_package.code', 'LIKE', "%$word%")
					            ->orWhere('planner_package.price', 'LIKE', "%$word%");
				}
			});
		}

		// @see: do not change this position
		// we need to get total count
		// to get category counts
		$allPackages = $packages->groupBy('planner_package.id')->get();
		$allPackages = $allPackages->filter(function ($package) {
			return $package->provider && $package->provider->is_live;
		});

		// filter:price
		$priceFilterApplied = false;
		$minPrices = $packages->pluck('price');
		$maxPrices = $packages->pluck('price_max');
		$calMinPrice = $packages->pluck('price')->min();
		$calMinPrice = $calMinPrice ? $calMinPrice : 0; // setting to 0 if $calMinPrice is null
		$calMaxPrice = ($maxPrices->max() > $minPrices->max()) ? $maxPrices->max() : $minPrices->max();
		$priceMin = request()->get('price_min') ?: 0;
		$priceMax = request()->get('price_max') ?: 0;
		if ($priceMin || $priceMax)
		{
			$priceFilterApplied = true;
		}
		else
		{
			$priceMin = $priceMin ? $priceMin : $calMinPrice;
			$priceMax = ($priceMax && $priceMax >= $priceMin) ? $priceMax : $calMaxPrice;
		}

		if ($priceMin > $priceMax)
		{
			$priceTemp = $priceMax;
			$priceMax = $priceMin;
			$priceMin = $priceTemp;
		}

		// user has changed min / max price
		if ($calMinPrice != $priceMin || $calMaxPrice != $priceMax)
		{
			$clearFilter = true;
			$packages->whereBetween('planner_package.price', [$priceMin, $priceMax]);
		}

		// filter by location
		$location = request()->get('location');
		$allLocations = $this->getPackagesLocationDataForFilters($cityId, $eventId);
		$userLocation = $this->trimSpace($location);
		if ($userLocation && in_array($userLocation, $allLocations['urls']))
		{
			$validLocation = Area::where('url', $userLocation)->first();
			if ($validLocation)
			{
				$clearFilter = true;
				$validLocationId = $validLocation->id;

				$packages->where(function ($query1) use ($validLocationId) {
					$query1->where(function ($query2) use ($validLocationId) {
						$query2->where('planner_package.map_type_id', config('evibe.ticket.type.venue'))
						       ->where('planner_package.area_id', $validLocationId);
					})->orWhere('planner_package.map_type_id', '!=', config('evibe.ticket.type.venue'));
				});

				//$availableVenueIds = Venue::whereIn('id', $packages->pluck("map_id")->toArray())
				//                          ->where("area_id", $validLocation->id)
				//                          ->whereNull('venue.deleted_at')
				//                          ->pluck("id")
				//                          ->toArray();
				//
				//$packages->where(function ($query1) use ($availableVenueIds) {
				//	$query1->where(function ($query2) use ($availableVenueIds) {
				//		$query2->where('planner_package.map_type_id', config('evibe.ticket.type.venue'))
				//		       ->whereIn('planner_package.map_id', $availableVenueIds);
				//	})->orWhere('planner_package.map_type_id', '!=', config('evibe.ticket.type.venue'));
				//});
			}
			else
			{
				$location = "all";
			}
		}

		$totalPackagesCount = $packages->get()->count();
		if (!($perPage))
		{
			$perPage = config('evibe.paginate-options.generic', 52);
			if ($pageId == config('evibe.ticket.type.food'))
			{
				$perPage = config('evibe.paginate-options.generic-desktop', 51);
			}
			if ($this->agent->isMobile() && !($this->agent->isTablet()))
			{
				$perPage = config('evibe.paginate-options.generic-mobile', 50);
			}
		}
		if ($collectionId)
		{
			$perPage = 8;
		}

		// sort results
		// if default (popularity), sort by priority
		// else sort by user input (ex: price, new-arrivals)
		$sortParams = $this->getSortParams('planner_package', ['price' => 'planner_package.price']);
		if (!$sortParams['isDefault'])
		{
			// groupBy is fix for distinct
			$packages = $packages->groupBy('planner_package.id')
			                     ->orderBy($sortParams['entity'], $sortParams['order']);

			// fetch page specific options - get()
			$currentPageOptions = $this->getCurrentPageOption($packages, $perPage);
			$packages = $currentPageOptions['options'];
			$currentPage = $currentPageOptions['currentPage'];
			$packages = $packages->all();
		}
		else
		{
			$allPackageIds = $packages->pluck('id')->all(); // get all IDs for merge

			$packages = $packages->groupBy('planner_package.id')
			                     ->orderBy('planner_package.created_at', 'DESC')->get(); // default sort

			$packages = $packages->all();

			if (is_null($userFilterId))
			{
				$priorityIds = $this->getItemIdsByPriority($eventId, $pageId, $allPackageIds); // get priorities
			}
			else
			{
				$params = [
					'eventId'         => $eventId,
					'pageId'          => $pageId,
					'userFilterId'    => $userFilterId,
					'allFilterTagIds' => $allFilterTagIds,
					'allItemIds'      => $allPackageIds
				];

				$priorityIds = $this->getItemIdsByPriorityForTags($params);
			}

			usort($packages, function ($a, $b) use ($priorityIds) {
				return $this->sortItemsByPriority($a, $b, $priorityIds);
			});

			// fetch page specific options - get()
			$currentPageOptions = $this->getCurrentPageOption($packages, $perPage);
			$packages = $currentPageOptions['options'];
			$currentPage = $currentPageOptions['currentPage'];

			$packages = $packages->all();
		}

		// fetch paginated packages
		$packageList = $this->getPaginatedItems($totalPackagesCount, $packages, $currentPage, $perPage);

		$cacheProductIds = [];

		// seo:schema
		foreach ($packageList as $package)
		{
			$productData = [
				'category' => $this->getPageName(),
				'name'     => $package->name,
				'image'    => $package->getProfileImg(),
				'price'    => $package->price,
				'url'      => config("evibe.host") . "/" . getCityUrl() . "/" . $this->getOccasionUrl() . "/" . config("evibe.results_url.packages") . "/" . $package->url
			];

			array_push($schema, $this->schemaListItem($productData, $pageHeader, $pageDescription));
			array_push($cacheProductIds, $package->code);
		}

		// data related to price categories
		$priceCategoryData = [
			'min'          => $calMinPrice,
			'max'          => $calMaxPrice,
			'mapTypeId'    => config('evibe.ticket.type.package'),
			'typeTicketId' => $this->getPageId(),
			'eventId'      => $eventId,
			'cityId'       => $cityId
		];

		if (count($packageList) && $this->getPageId() == config('evibe.ticket.type.villas'))
		{
			foreach ($packageList as $pck)
			{
				$dynamicParams = [
					'price'              => $pck->price,
					'priceWorth'         => $pck->price_worth,
					'groupCount'         => $pck->group_count,
					'minGuestCount'      => $pck->guests_min,
					'maxGuestCount'      => $pck->guests_max,
					'pricePerExtraGuest' => $pck->price_per_extra_guest
				];

				$updatedProductData = $this->calculateDynamicParams($dynamicParams,
				                                                    date('m/d/Y', time()),
				                                                    $cityId,
				                                                    $eventId,
				                                                    $pck->type_ticket_id,
				                                                    $pck->id);

				$pck->price = (isset($updatedProductData['price']) && $updatedProductData['price']) ? $updatedProductData['price'] : $pck->price;
				$pck->price_worth = (isset($updatedProductData['priceWorth']) && $updatedProductData['priceWorth']) ? $updatedProductData['priceWorth'] : $pck->price_worth;
				$pck->group_count = (isset($updatedProductData['groupCount']) && $updatedProductData['groupCount']) ? $updatedProductData['groupCount'] : $pck->group_count;
				$pck->guests_min = (isset($updatedProductData['minGuestCount']) && $updatedProductData['minGuestCount']) ? $updatedProductData['minGuestCount'] : $pck->guests_min;
				$pck->guests_max = (isset($updatedProductData['maxGuestCount']) && $updatedProductData['maxGuestCount']) ? $updatedProductData['maxGuestCount'] : $pck->guests_max;
				$pck->price_per_extra_guest = (isset($updatedProductData['pricePerExtraGuest']) && $updatedProductData['pricePerExtraGuest']) ? $updatedProductData['pricePerExtraGuest'] : $pck->price_per_extra_guest;
			}
		}

		$data = [
			'packages'      => $packageList,
			'totalCount'    => $totalPackagesCount,
			'occasionId'    => $this->getOccasionId(),
			'cityId'        => getCityId(),
			'mapTypeId'     => $this->getPageId(),
			'sort'          => $sortParams['sortType'],
			'filters'       => [
				'queryParams'         => $this->getExistingQueryParams(),
				'active'              => $active,
				'priceMin'            => $priceMin,
				'priceMax'            => $priceMax,
				'priceCategories'     => $this->getPriceCategoryFilters($priceCategoryData),
				'hidePriceCategories' => $hidePriceCategories,
				'search'              => $userSearch,
				'clearFilter'         => $clearFilter,
				'catCounts'           => $this->getFilterCategoryCounts($allPackages, $allTagIdsBank),
				'allCategories'       => $allCategories,
				'category'            => $categoryUrl,
				'priceFilterApplied'  => $priceFilterApplied
			],
			'seo'           => [
				'pageTitle'       => $pageTitle,
				'pageHeader'      => $pageHeader,
				'pageDescription' => $pageDescription,
				'keyWords'        => $keyWords,
				'schema'          => $schema
			],
			'surCategories' => $surCategories,
			'surFilters'    => [
				'noSurCatPackages' => $noSurCatPackages,
				'surRelTagId'      => $srTag,
				'surOccTagId'      => $soTag,
				'surAgeTagId'      => $saTag,
			],
			'countries'     => Country::all()
		];

		if ($eventId == config('evibe.occasion.surprises.id'))
		{
			$data['filters']['location'] = $location;
			$data['filters']['allLocations'] = $allLocations;
		}
		if ($collectionId)
		{
			return $data;
		}
		$data = array_merge($data, $this->mergeView($cityUrl, $eventId));
		$data['cacheProductIds'] = $cacheProductIds;

		return $data;
	}

	public function getProfileData($cityUrl, $profileUrl, $hashTag,
	                               $validationKey = "package",
	                               $customControllerParams = null)
	{
		$urlParams = [
			['key' => 'city', 'value' => $cityUrl],
			['key' => $validationKey, 'value' => $profileUrl]
		];
		$validUrlObj = $this->validateUrlParams($urlParams);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}
		elseif (array_key_exists('isRedirect', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl'] . "?ref=redirect-perm", 301);
		}

		$package = $validUrlObj['planner-package'];

		// update product page views counter
		updateProfileViewCounter($this->getPageId(), $package->id);

		$cacheKey = $this->deviceSpecificCacheKey(request()->fullUrl());
		$cacheKey = base64_encode($cacheKey);
		if (Cache::has($cacheKey))
		{
			$profileData = Cache::get($cacheKey);
		}
		else
		{
			$profileData = $this->getProfileDataFromDB($validUrlObj, $cityUrl, $profileUrl, $hashTag, $customControllerParams);
			Cache::put($cacheKey, $profileData, config('evibe.cache.refresh-time'));
		}

		return $profileData;
	}

	public function getProfileDataFromDB($validUrlObj, $cityUrl, $profileUrl, $hashTag, $customControllerParams)
	{
		$package = $validUrlObj['planner-package'];

		$eventId = $this->getOccasionId();
		$cityName = getCityName();
		$cityId = getCityId();
		$pageId = $this->getPageId();

		// check for custom views
		if (is_array($customControllerParams))
		{
			$params = [
				'cityName'     => $cityName,
				'occasion'     => $customControllerParams['occasion'],
				'method'       => $customControllerParams['method'],
				'controller'   => $customControllerParams['controller'],
				'methodParams' => [
					'cityUrl'    => $cityUrl,
					'packageUrl' => $profileUrl,
					'validation' => $validUrlObj
				]
			];

			$customView = $this->customizedCityControllerRedirect($params);
			if ($customView)
			{
				return $customView;
			}
		}

		$highlights = $this->getPkgCustomHighlights($package->id, $eventId); // get highlight fields
		$moreInclusions = $this->getPkgCustomMoreInclusions($package->id, $eventId); // get more inclusions
		$gallery = $this->getPackageCategorisedGallery($package->id, $package->name); // gallery (images + videos)
		$itinerary = $this->getPackageItinerary($package); // itinerary

		// SEO
		if ($this->isCandleLightDinner($package))
		{
			$defaultKey = "defaults.city.$eventId.candle-light-dinner.profile";
			$replaces = [
				'{item}'  => strtolower($package->name),
				'{price}' => $this->formatPrice($package->price),
				'{area}'  => $package->provider->area->name . ","
			];
		}
		else
		{
			$defaultKey = "defaults.city.$eventId.$pageId.profile";
			$replaces = [
				'{item}'  => strtolower($package->name),
				'{price}' => $this->formatPrice($package->price)
			];
		}
		$partnerRatings = $package->provider->getPartnerReview();

		$seo = $this->getSeoText([getCityId(), $eventId, $pageId, "profile"], $defaultKey, $replaces);
		$pageTitle = ucwords($seo['pt']);
		$pageDescription = $seo['pd'];
		$pageHeader = $seo['ph'];
		$keyWords = $seo['kw'];
		$schema = $this->getPackageSchema($package, $partnerRatings);

		// Social share
		$pageUrl = request()->capture()->fullUrl();
		$packageTitle = $package->name . " In " . $cityName;
		$metaImage = $gallery["metaImageURL"];
		$shareUrl = $this->getShareUrl($pageUrl, $packageTitle, $hashTag, $metaImage);

		$unavailableDates = [];
		$validDate = null;
		if ($package->type_ticket_id == config('evibe.ticket.type.villas'))
		{
			$partner = $package->provider;
			if ($partner && $partner->code)
			{
				// package unavailability dates
				// format: Y/m/d
				$unavailableDates = $this->getPackageUnavailabilityDates($package->id, $partner->code, $package->map_type_id);
			}

			// today is obviously invalid
			$dayCount = 1;
			while (!$validDate)
			{
				$nextDate = Carbon::createFromTimestamp(time() + ($dayCount * (24 * 60 * 60)))->toDateTimeString();
				$formattedNextDate = date("Y/m/d", strtotime($nextDate));
				if (in_array($formattedNextDate, $unavailableDates))
				{
					// invalid date
				}
				else
				{
					// valid date
					$validDate = $formattedNextDate;
				}
				$dayCount++;
			}

			// to make sure that a valid date exists for villa packages
			if (!$validDate)
			{
				$tomorrow = Carbon::createFromTimestamp(time() + (1 * (24 * 60 * 60)))->toDateTimeString();
				$validDate = date("Y/m/d", strtotime($tomorrow));
			}
		}

		if (!$validDate)
		{
			// for non-bachelor packages
			$today = Carbon::createFromTimestamp(time())->toDateTimeString();
			$validDate = date("Y/m/d", strtotime($today));
		}

		$dynamicParams = [
			'price'              => $package->price,
			'priceWorth'         => $package->price_worth,
			'groupCount'         => $package->group_count,
			'minGuestCount'      => $package->guests_min,
			'maxGuestCount'      => $package->guests_max,
			'pricePerExtraGuest' => $package->price_per_extra_guest
		];
		$updatedProductData = $this->calculateDynamicParams($dynamicParams,
		                                                    date('m/d/Y', strtotime($validDate)),
		                                                    $cityId,
		                                                    $eventId,
		                                                    $package->type_ticket_id,
		                                                    $package->id);
		// price_max ??

		$package->price = (isset($updatedProductData['price']) && $updatedProductData['price']) ? $updatedProductData['price'] : $package->price;
		$package->price_worth = (isset($updatedProductData['priceWorth']) && $updatedProductData['priceWorth']) ? $updatedProductData['priceWorth'] : $package->price_worth;
		$package->group_count = (isset($updatedProductData['groupCount']) && $updatedProductData['groupCount']) ? $updatedProductData['groupCount'] : $package->group_count;
		$package->guests_min = (isset($updatedProductData['minGuestCount']) && $updatedProductData['minGuestCount']) ? $updatedProductData['minGuestCount'] : $package->guests_min;
		$package->guests_max = (isset($updatedProductData['maxGuestCount']) && $updatedProductData['maxGuestCount']) ? $updatedProductData['maxGuestCount'] : $package->guests_max;
		$package->price_per_extra_guest = (isset($updatedProductData['pricePerExtraGuest']) && $updatedProductData['pricePerExtraGuest']) ? $updatedProductData['pricePerExtraGuest'] : $package->price_per_extra_guest;

		$data = [
			'package'             => $package,
			'occasionId'          => $eventId,
			'cityId'              => $cityId,
			'mapTypeId'           => $pageId,
			'highlights'          => $highlights,
			'moreInclusions'      => $moreInclusions,
			'gallery'             => $gallery,
			'itinerary'           => $itinerary,
			'services'            => $package->services,
			'tags'                => $package->getTags(),
			'similarPackages'     => $package->getSimilarPackages($eventId),
			'seo'                 => [
				'pageTitle'       => $pageTitle,
				'pageDescription' => $pageDescription,
				'pageHeader'      => $pageHeader,
				'keyWords'        => $keyWords,
				'pageUrl'         => $pageUrl,
				'metaImage'       => $metaImage,
				'schema'          => $schema
			],
			'shareUrl'            => $shareUrl,
			'ratings'             => $partnerRatings,
			'isAutoBookable'      => $this->isAutoBookableItem($pageId, $package->id),
			'countries'           => Country::all(),
			'unavailableDates'    => $unavailableDates,
			'validMinBookingDate' => $validDate ? date('Y/m/d', strtotime($validDate)) : null
		];

		// check in slots for CLDs (hardcoded for now)
		if (isset($data['tags']) && count($data['tags']))
		{
			if (isset($data['tags'][config('evibe.product-tags.candle-light-dinner')]))
			{
				$tagId = config('evibe.product-tags.candle-light-dinner');
				$data['checkInSlots'] = config('evibe.check-in-slots.' . $tagId);
			}
			elseif (isset($data['tags'][config('evibe.product-tags.cake-cutting-place')]))
			{
				$tagId = config('evibe.product-tags.cake-cutting-place');
				$optionAvailabilitySlots = OptionAvailability::where('map_id', $package->id)
				                                             ->where('map_type_id', $package->type_ticket_id)
				                                             ->get();

				if (count($optionAvailabilitySlots))
				{
					$checkInSlots = [];
					foreach ($optionAvailabilitySlots as $slot)
					{
						array_push($checkInSlots, [
							'checkInTime'  => date('g:i A', strtotime($slot->slot_start_time)),
							'checkOutTime' => date('g:i A', strtotime($slot->slot_end_time)),
						]);
					}
					$data['checkInSlots'] = $checkInSlots;
				}
			}
		}

		// cancellation policy
		$data['cancellationData'] = $this->calculateCustomerCancellationRefund();

		$data = array_merge($data, $this->mergeView($cityUrl, $eventId));

		$shortlistDataParams = [
			'mapId'     => $package->id,
			'mapTypeId' => $pageId,
		];

		$data = array_merge($data, $this->mergeShortlistData($shortlistDataParams));
		$data['deliveryImages'] = $this->fetchDeliveryImages($package->id, $pageId);

		return $data;
	}

	private function getPackageSchema($package, $partnerRatings)
	{
		// seo:schema:breadcrumblist
		if ($this->getOccasionUrl() == config('evibe.occasion.surprises.cld.url'))
		{
			$bcCityKey = "/" . getCityUrl();
			$bcPageKey = $bcCityKey . "/" . $this->getOccasionUrl();
			$bcProductKey = $bcCityKey . "/" . $package->url . "/" . $this->getOccasionUrl();

			$breadcrumbItemList = [
				"/"           => "Evibe.in",
				$bcCityKey    => getCityName(),
				$bcPageKey    => $this->getPageName(),
				$bcProductKey => $package->name
			];
		}
		else
		{
			$bcCityKey = "/" . getCityUrl();
			$bcEventKey = $bcCityKey . "/" . $this->getOccasionUrl();
			$bcPageKey = $bcEventKey . "/" . $this->getPageUrl();
			$bcProductKey = $bcPageKey . "/" . $package->url;

			$breadcrumbItemList = [
				"/"           => "Evibe.in",
				$bcCityKey    => getCityName(),
				$bcPageKey    => $this->getPageName(),
				$bcProductKey => $package->name
			];
		}

		$schema = [$this->schemaBreadcrumbList($breadcrumbItemList)];

		$reviews = [];
		if ($package && isset($partnerRatings["reviews"]["topRated"]) && (count($partnerRatings["reviews"]["topRated"]) > 0))
		{
			$reviews = $partnerRatings["reviews"]["topRated"];
		}

		// seo:schema:product
		$avgRating = $partnerRatings['total']['avg'];
		$ratingCount = $partnerRatings['total']['count'];
		$productData = [
			'sku'         => $package->code,
			'category'    => $this->getPageName(),
			'name'        => $package->name,
			'image'       => $package->getProfileImg(),
			'gtin'        => $package->code,
			'avgRating'   => $avgRating,
			'ratingCount' => $ratingCount,
			'price'       => $package->price,
			'description' => $package->info,
			'url'         => config('evibe.host') . $bcProductKey,
			'reviews'     => $reviews
		];
		array_push($schema, $this->schemaProduct($productData));

		return $schema;
	}

	public function getPackageCategorisedGallery($packageId, $packageName = null)
	{
		$gallery = [];
		$metaImage = config('evibe.bachelor.default_og_image');
		$metaImageURL = config('evibe.bachelor.default_og_image');

		$packageGallery = PackageGallery::leftJoin('type_package_gallery', 'type_package_gallery.id', '=', 'planner_package_gallery.category_id')
		                                ->select('planner_package_gallery.id', 'planner_package_gallery.title', 'planner_package_gallery.url', 'planner_package_gallery.type', 'planner_package_gallery.alt_text', 'planner_package_gallery.category_id', 'type_package_gallery.category_name', 'planner_package_gallery.planner_package_id', 'planner_package_gallery.is_profile')
		                                ->where('planner_package_gallery.planner_package_id', $packageId)
		                                ->orderBy('planner_package_gallery.type', 'DESC')
		                                ->orderBy('planner_package_gallery.is_profile', 'DESC')
		                                ->orderBy('planner_package_gallery.updated_at', 'DESC')
		                                ->orderBy('type_package_gallery.priority', 'DESC')
		                                ->orderBy(DB::raw('ISNULL(planner_package_gallery.priority), planner_package_gallery.priority'), 'ASC')
		                                ->get();

		$gallery['images'] = [];
		$gallery['videos'] = [];
		$showGalleryTabs = false;
		$showThumbs = false;
		$imageCount = $videoCount = $count = 0;
		$imgCatList = [];

		// get categorised data
		foreach ($packageGallery as $item)
		{
			$type = 'images';

			if ($item->type == config('evibe.gallery.type.video'))
			{
				$videoInfo = $item->getVideoInfo();

				if (!empty($videoInfo['url']) && !empty($videoInfo['thumb']))
				{
					$gallery['images']['videos'][] = [
						'url'   => $videoInfo['url'],
						'title' => '',
						'thumb' => $videoInfo['thumb']
					];
					$videoCount++;
				}
			}

			elseif ($item->type == config('evibe.gallery.type.image'))
			{
				$categoryName = "Others";

				if ($item->category_id && $item->category_name)
				{
					$categoryName = $item->category_name;
					if (!in_array($item->category_id, $imgCatList))
					{
						$imgCatList[] = $item->category_id;
					}
				}

				$imageTitle = config('evibe.gallery.image_code') . ' - ' . $item->id;
				if ($item->title && $item->title != "-")
				{
					$imageTitle = $item->title;
				}

				$gallery[$type][$categoryName][] = [
					//'title' => $imageTitle,
					'title' => $packageName ? $packageName : $imageTitle,
					'url'   => $item->getPath("profile"),
					'thumb' => $item->getPath("thumbs"),
				];

				if ($item->is_profile)
				{
					$metaImageURL = $item->getPath("results");
				}
				elseif ($count == 0)
				{
					$metaImage = $item->getPath("profile");
				}
				$imageCount++;
			}

			if ($count == 0)
			{
				$metaImage = $item->url;
			}
			$count++;
		}

		foreach ($gallery['images'] as $cat => $val)
		{
			if (count($val) > 1)
			{
				$showThumbs = true;
				break;
			}
		}

		if (count($imgCatList) > 1)
		{
			$showGalleryTabs = true;
		}

		if ($imageCount && $videoCount)
		{
			if (!$showGalleryTabs)
			{
				$gallery['images']['Images'] = $gallery['images']['Others'];
				unset($gallery['images']['Others']);
				$showGalleryTabs = true;
			}
		}

		$gallery['showGalleryTabs'] = $showGalleryTabs; // show nav tabs if there is at least on valid category
		$gallery['metaImage'] = $metaImage;
		$gallery['showThumb'] = $showThumbs;
		$gallery['metaImageURL'] = $metaImageURL;

		return $gallery;
	}

	public function getPkgCustomHighlights($packageId, $eventId)
	{
		$catId = config('evibe.pkg_fields.cat.highlights.id');
		$highlights = $this->getCustomFieldValuesByCat($catId, $eventId, $packageId);

		return $highlights;
	}

	public function getPkgCustomMoreInclusions($packageId, $eventId)
	{
		$catId = config('evibe.pkg_fields.cat.inclusions.id');
		$moreInclusions = $this->getCustomFieldValuesByCat($catId, $eventId, $packageId);

		return $moreInclusions;
	}

	public function getPackageItinerary($package)
	{
		$itineraryData = [];
		$itineraries = $package->itinerary;

		foreach ($itineraries as $itinerary)
		{
			$itineraryValues = $itinerary->values;
			$itineraryData[$itinerary->id] = [
				'id'     => $itinerary->id,
				'name'   => $itinerary->name,
				'values' => []
			];

			foreach ($itineraryValues as $itineraryValue)
			{
				array_push($itineraryData[$itinerary->id]['values'], [
					'id'   => $itineraryValue->id,
					'time' => $itineraryValue->time,
					'info' => $itineraryValue->info
				]);
			}
		}

		return $itineraryData;
	}

	public function getPackageRatings($package, $key = "package")
	{
		$provider = $package->provider;
		$maxCount = 8;
		$packageReviews = $package->getReviews($maxCount);
		$providerReviews = $provider->getDirectReviews($maxCount);

		$ratings = [
			'maxCount' => $maxCount,
			'provider' => [
				'count'     => $providerReviews['totalCount'],
				'avgRating' => $provider->avgRating(),
				'reviews'   => $providerReviews['reviews'],
			],
			$key       => [
				'count'     => $packageReviews['totalCount'],
				'avgRating' => $packageReviews['avgRating'],
				'reviews'   => $packageReviews['reviews'],
			]
		];

		return $ratings;
	}

	public function getPackagesLocationDataForFilters($cityId, $eventId)
	{
		return EvibeUtilFacade::getPackagesLocationDataForFilters($cityId, $eventId);
	}

	public function getLocationForFilters($deals)
	{
		return EvibeUtilFacade::getVenueDealFilterLocation($deals);
	}

	private function isCandleLightDinner($package)
	{
		$packageTag = PackageTag::where('tile_tag_id', config("evibe.type-tag.candle-light-dinners"))
		                        ->where('planner_package_id', $package->id)
		                        ->get()
		                        ->first();

		return $packageTag ? true : false;
	}

	private function getPackageUnavailabilityDates($packageId, $partnerCode, $partnerTypeId)
	{
		$userId = config("evibe.default.review_handler");
		$accessToken = $this->getAccessToken('', $userId);

		$url = config("evibe.api.base_url") . config("evibe.api.avail-check.prefix") . "/" . $partnerTypeId . "/" . $partnerCode;

		$jsonData = [
			'productId'  => $packageId,
			'isFromMain' => true
		];

		$res = $this->makeApiCallWithUrl([
			                                 'url'         => $url,
			                                 'method'      => 'POST',
			                                 'accessToken' => $accessToken,
			                                 'jsonData'    => $jsonData
		                                 ]);

		$unavailableDates = [];

		if (isset($res['success']) && $res['success'])
		{
			if (isset($res['availCheckData']) && count($res['availCheckData']))
			{
				foreach ($res['availCheckData'] as $date => $slotsData)
				{
					array_push($unavailableDates, date('Y/m/d', strtotime($date)));
				}
			}

			if (isset($res['ticketData']) && count($res['ticketData']))
			{
				foreach ($res['ticketData'] as $date => $slotsData)
				{
					array_push($unavailableDates, date('Y/m/d', strtotime($date)));
				}
			}
		}

		return $unavailableDates;
	}
}