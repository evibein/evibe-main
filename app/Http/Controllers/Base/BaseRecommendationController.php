<?php

namespace App\Http\Controllers\Base;

use App\Models\Cake\Cake;
use App\Models\Decor\Decor;
use App\Models\Package\Package;
use App\Models\Package\PackageTag;
use App\Models\Trend\Trend;
use App\Models\Types\TypeServices;
use App\Models\Types\TypeTag;
use App\Models\Venue\VenueHall;

use App\Models\Util\SiteErrorLog;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BaseRecommendationController extends BaseController
{
	// Recommendation data filter start.
	protected function getRecommendationData($mapping)
	{
		$mapTypeId = $mapping->map_type_id;
		$mapId = $mapping->map_id;
		$ticket = $mapping->ticket;

		$data = [
			'id'           => $mapping->id,
			'pId'          => $mapId,
			'name'         => '',
			'type'         => '',
			'ticketTypeId' => '',
			'imageUrl'     => '',
			'url'          => '',
			'priceMin'     => '',
			'priceMax'     => '',
			'priceWorth'   => '',
			'code'         => '',
			'extras'       => '',
			'venueHall'    => '',
			'isSelected'   => $mapping->is_selected
		];

		switch ($mapTypeId)
		{
			case config('evibe.ticket.type.package'):
			case config('evibe.ticket.type.resorts'):
			case config('evibe.ticket.type.villas'):
			case config('evibe.ticket.type.lounges'):
			case config('evibe.ticket.type.food'):
			case config('evibe.ticket.type.surprises'):
			case config('evibe.ticket.type.venue-deals'):
			case config('evibe.ticket.type.priests'):
			case config('evibe.ticket.type.tents'):
			case config('evibe.ticket.type.generic-package'):
				$package = Package::find($mapId);

				if ($package)
				{
					$packageWithEvent = $this->getPackageCategoryBasedOnEvent($package, $ticket->event_id);
					$data['name'] = $package->name;
					$data['type'] = $packageWithEvent['type'];
					$data['ticketTypeId'] = $packageWithEvent['ticketTypeId'];
					$data['imageUrl'] = $package->getProfileImg();
					$data['url'] = $packageWithEvent['url'];
					$data['priceMin'] = $package->price;
					$data['priceMax'] = $package->price_max;
					$data['priceWorth'] = $package->getPriceWorth();
					$data['code'] = $package->code;
					$data['occasionId'] = $ticket->event_id;
				}
				break;

			case config('evibe.ticket.type.venue'):
				$venueHall = VenueHall::with('venue')->find($mapId);
				if ($venueHall && $venueHall->venue && $venueHall->venue->is_live == 1)
				{
					$profileUrl = route('city.occasion.birthdays.venues.profile', [getCityUrl(), $venueHall->url]);

					if ($ticket->event_id == config('evibe.occasion.pre-post.id'))
					{
						$profileUrl = route('city.occasion.pre-post.venues.profile', [getCityUrl(), $venueHall->url]);
					}

					$data['name'] = $venueHall->type_id == 1 ? 'Party Space' : $venueHall->type->name;
					$data['name'] .= ' at ' . $venueHall->venue->area->name;
					$data['type'] = 'Venues';
					$data['ticketTypeId'] = config('evibe.ticket.type.venue');
					$data['imageUrl'] = $venueHall->getProfileImg();
					$data['venueHall'] = $venueHall;
					$data['url'] = $profileUrl;
					$data['code'] = $venueHall->code;
					$data['occasionId'] = $ticket->event_id;
				}
				break;

			case config('evibe.ticket.type.service'):
			case config('evibe.ticket.type.entertainment'):
				$service = TypeServices::find($mapId);
				if ($service)
				{
					$profileUrl = route('city.occasion.birthdays.ent.profile', [getCityUrl(), $service->url]);

					if ($ticket->event_id == config('evibe.occasion.bachelor.id'))
					{
						$profileUrl = route('city.occasion.bachelor.ent.profile', [getCityUrl(), $service->url]);
					}
					elseif ($ticket->event_id == config('evibe.occasion.pre-post.id'))
					{
						$profileUrl = route('city.occasion.pre-post.ent.profile', [getCityUrl(), $service->url]);
					}

					$data['name'] = $service->name;
					$data['type'] = 'Entertainment + AddOn';
					$data['ticketTypeId'] = config('evibe.ticket.type.service');
					$data['imageUrl'] = $service->getProfilePic();
					$data['url'] = $profileUrl;
					$data['priceMin'] = $service->min_price;
					$data['priceMax'] = $service->max_price;
					$data['priceWorth'] = $service->worth_price;
					$data['code'] = $service->code;
					$data['occasionId'] = $ticket->event_id;
				}
				break;

			case config('evibe.ticket.type.trend'):
				$trend = Trend::find($mapId);
				if ($trend)
				{
					$data['name'] = $trend->name;
					$data['type'] = 'Trends';
					$data['imageUrl'] = $trend->getProfileImg();
					$data['url'] = route('city.occasion.birthdays.trends.profile', [getCityUrl(), $trend->url]);
					$data['priceMin'] = $trend->price;
					$data['priceMax'] = $trend->price_max;
					$data['priceWorth'] = $trend->price_worth;
					$data['ticketTypeId'] = config('evibe.ticket.type.trend');
					$data['code'] = $trend->code;
				}
				break;

			case config('evibe.ticket.type.cake'):
				$cake = Cake::find($mapId);
				if ($cake)
				{
					$profileUrl = route('city.occasion.birthdays.cakes.profile', [getCityUrl(), $cake->url]);

					if ($ticket->event_id == config('evibe.occasion.bachelor.id'))
					{
						$profileUrl = route('city.occasion.bachelor.cakes.profile', [getCityUrl(), $cake->url]);
					}
					elseif ($ticket->event_id == config('evibe.occasion.pre-post.id'))
					{
						$profileUrl = route('city.occasion.pre-post.cakes.profile', [getCityUrl(), $cake->url]);
					}

					$data['name'] = $cake->title;
					$data['type'] = 'Cakes';
					$data['imageUrl'] = $cake->getProfileImg();
					$data['url'] = $profileUrl;
					$data['priceMin'] = $cake->price;
					$data['extras'] = "/ " . $cake->min_order . " kg";
					$data['ticketTypeId'] = config('evibe.ticket.type.cake');
					$data['code'] = $cake->code;
					$data['occasionId'] = $ticket->event_id;
				}
				break;

			case config('evibe.ticket.type.decor'):
				$decor = Decor::where('is_live', 1)->find($mapId);
				if ($decor)
				{
					$profileUrl = route('city.occasion.birthdays.decors.profile', [getCityUrl(), $decor->url]);

					if ($ticket->event_id == config('evibe.occasion.pre-post.id'))
					{
						$profileUrl = route('city.occasion.pre-post.decors.profile', [getCityUrl(), $decor->url]);
					}

					$data['name'] = $decor->name;
					$data['type'] = 'Decors';
					$data['imageUrl'] = $decor->getProfileImg();
					$data['url'] = $profileUrl;
					$data['priceMin'] = $decor->min_price;
					$data['priceMax'] = $decor->max_price;
					$data['priceWorth'] = $decor->worth;
					$data['ticketTypeId'] = config('evibe.ticket.type.decor');
					$data['code'] = $decor->code;
					$data['occasionId'] = $ticket->event_id;
				}
				break;
		}

		return $data;
	}

	// Categories packages based on event (ex: resort, villa, food for Bachelors)
	public function getPackageCategoryBasedOnEvent($package, $eventId)
	{
		$packageUrl = $package->url;
		$url = route('city.occasion.birthdays.packages.profile', [getCityUrl(), $packageUrl]);
		$type = 'Packages';
		$ticketTypeId = config('evibe.ticket.type.package');

		if (config('evibe.occasion.kids_birthdays.id') == $eventId)
		{
			if ($this->checkPackageType($package, config('evibe.ticket.type.venue-deals')))
			{
				//@todo set the url variable below to venue-deals url
				$type = 'Venue Deals';
				$url = config('evibe.host') . '/' . getCityUrl() . '/' . config('evibe.occasion.kids_birthdays.url') . '/venue-deals/' . $packageUrl;
				$ticketTypeId = config('evibe.ticket.type.venue-deals');
			}
			//food
			elseif ($this->checkPackageType($package, config('evibe.ticket.type.food')))
			{
				$type = 'Food Packages';
				$url = route('city.occasion.birthdays.food.profile', [getCityUrl(), $packageUrl]);
				$ticketTypeId = config('evibe.ticket.type.food');
			}
		}
		elseif (config('evibe.occasion.bachelor.id') == $eventId)
		{
			//resort
			if ($this->checkPackageType($package, config('evibe.ticket.type.resorts')))
			{
				$type = 'Resorts';
				$url = route('city.occasion.bachelor.resort.profile', [getCityUrl(), $packageUrl]);
				$ticketTypeId = config('evibe.ticket.type.resorts');
			}
			//villa
			elseif ($this->checkPackageType($package, config('evibe.ticket.type.villas')))
			{
				$type = 'Villas + Farms';
				$url = route('city.occasion.bachelor.villa.profile', [getCityUrl(), $packageUrl]);
				$ticketTypeId = config('evibe.ticket.type.villas');
			}
			//lounge
			elseif ($this->checkPackageType($package, config('evibe.ticket.type.lounges')))
			{
				$type = 'Lounges';
				$url = route('city.occasion.bachelor.lounge.profile', [getCityUrl(), $packageUrl]);
				$ticketTypeId = config('evibe.ticket.type.lounges');
			}
			//food
			elseif ($this->checkPackageType($package, config('evibe.ticket.type.food')))
			{
				$type = 'Food Packages';
				$url = route('city.occasion.bachelor.food.profile', [getCityUrl(), $packageUrl]);
				$ticketTypeId = config('evibe.ticket.type.food');
			}
		}
		elseif (config('evibe.occasion.surprises.id') == $eventId)
		{
			$url = route('city.occasion.surprises.package.profile', [getCityUrl(), $packageUrl]);
		}

		return $data = [
			'type'         => $type,
			'url'          => $url,
			'ticketTypeId' => $ticketTypeId
		];
	}

	// Get other categories for this event starting price
	protected function getAllCategoriesOfOccasion($occasionId)
	{
		$allCategories = [];

		if ($occasionId == config('evibe.occasion.kids_birthdays.id'))
		{
			$allCategories = [
				[
					'mapTypeId'  => config('evibe.ticket.type.decor'),
					'url'        => route('city.occasion.birthdays.decors.list', getCityUrl()),
					'type'       => 'Decors',
					'startPrice' => $this->getItemMinPrice(config('evibe.ticket.type.decor'), $occasionId),
				],
				[
					'mapTypeId'  => config('evibe.ticket.type.venue'),
					'url'        => route('city.occasion.birthdays.venues.list', getCityUrl()),
					'type'       => 'Venues',
					'startPrice' => $this->getItemMinPrice(config('evibe.ticket.type.venue'))
				],
				[
					'mapTypeId'  => config('evibe.ticket.type.service'),
					'url'        => route('city.occasion.birthdays.ent.list', getCityUrl()),
					'type'       => 'Entertainment',
					'startPrice' => $this->getItemMinPrice(config('evibe.ticket.type.service'), $occasionId)
				],
				[
					'mapTypeId'  => config('evibe.ticket.type.package'),
					'url'        => route('city.occasion.birthdays.packages.list', getCityUrl()),
					'type'       => 'Packages',
					'startPrice' => $prices = $this->getPackageMinPrice($occasionId)
				],
				[
					'mapTypeId'  => config('evibe.ticket.type.trend'),
					'url'        => route('city.occasion.birthdays.trends.list', getCityUrl()),
					'type'       => 'Trends',
					'startPrice' => $this->getItemMinPrice(config('evibe.ticket.type.trend'), $occasionId)
				],
				[
					'mapTypeId'  => config('evibe.ticket.type.cake'),
					'url'        => route('city.occasion.birthdays.cakes.list', getCityUrl()),
					'type'       => 'Cakes',
					'startPrice' => $this->getItemMinPrice(config('evibe.ticket.type.cake'), $occasionId)
				]
			];

			/* @todo: enable after launching venue deals
			 * [
			 * 'mapTypeId'  => config('evibe.ticket.type.venue-deals'),
			 * 'url'        => config('evibe.host') . '/' . getCityUrl() . '/' . config('evibe.occasion.kids_birthdays.url') . '/venue-deals/all',
			 * 'type'       => 'Venue Deals',
			 * 'startPrice' => $this->getPackageMinPrice($occasionId, config('evibe.ticket.type.venue-deals'))
			 * ]
			 */
		}
		elseif ($occasionId == config('evibe.occasion.bachelor.id'))
		{
			$eventId = config('evibe.occasion.bachelor.id');

			$allCategories = [
				[
					'mapTypeId'  => config('evibe.ticket.type.resorts'),
					'url'        => route('city.occasion.bachelor.resort.list', getCityUrl()),
					'startPrice' => $this->getPackageMinPrice($eventId, config('evibe.ticket.type.resorts')),
					'type'       => 'Resorts',
				],

				[
					'mapTypeId'  => config('evibe.ticket.type.villas'),
					'url'        => route('city.occasion.bachelor.villa.list', getCityUrl()),
					'startPrice' => $this->getPackageMinPrice($eventId, config('evibe.ticket.type.villas')),
					'type'       => 'Villas'
				],

				[
					'mapTypeId'  => config('evibe.ticket.type.lounges'),
					'url'        => route('city.occasion.bachelor.lounge.list', getCityUrl()),
					'startPrice' => $this->getPackageMinPrice($eventId, config('evibe.ticket.type.lounges')),
					'type'       => 'Lounges',
				],

				[
					'mapTypeId'  => config('evibe.ticket.type.food'),
					'url'        => route('city.occasion.bachelor.food.list', getCityUrl()),
					'startPrice' => $this->getPackageMinPrice($eventId, config('evibe.ticket.type.food')),
					'type'       => 'Food'
				],

				[
					'mapTypeId'  => config('evibe.ticket.type.service'),
					'url'        => route('city.occasion.bachelor.ent.list', getCityUrl()),
					'startPrice' => $this->getItemMinPrice(config('evibe.ticket.type.service'), $eventId),
					'type'       => 'Entertainment'
				],

				[
					'mapTypeId'  => config('evibe.ticket.type.cake'),
					'url'        => route('city.occasion.bachelor.cakes.list', getCityUrl()),
					'startPrice' => $this->getItemMinPrice(config('evibe.ticket.type.cake'), $eventId),
					'type'       => 'Cakes'
				]
			];
		}
		elseif ($occasionId == config('evibe.occasion.pre-post.id'))
		{
			$eventId = config('evibe.occasion.pre-post.id');
			$allCategories = [
				[
					'mapTypeId'  => config('evibe.ticket.type.venue'),
					'url'        => route('city.occasion.pre-post.venues.list', getCityUrl()),
					'startPrice' => $this->getItemMinPrice(config('evibe.ticket.type.venue'), $eventId),
					'type'       => 'Venues',

				],
				[
					'mapTypeId'  => config('evibe.ticket.type.decor'),
					'url'        => route('city.occasion.pre-post.decors.list', getCityUrl()),
					'startPrice' => $this->getItemMinPrice(config('evibe.ticket.type.decor'), $eventId),
					'type'       => 'Decors'
				],
				[
					'mapTypeId'  => config('evibe.ticket.type.service'),
					'url'        => route('city.occasion.pre-post.ent.list', getCityUrl()),
					'startPrice' => $this->getItemMinPrice(config('evibe.ticket.type.service'), $eventId),
					'type'       => 'Entertainment'
				],
				[
					'mapTypeId'  => config('evibe.ticket.type.cake'),
					'url'        => route('city.occasion.pre-post.cakes.list', getCityUrl()),
					'startPrice' => $this->getItemMinPrice(config('evibe.ticket.type.cake'), $eventId),
					'type'       => 'Cakes'
				],
			];
		}

		return $allCategories;
	}

	/*
	 * function for getting the error of catch block, while getting the
	 * recommended mapping data & similar types of data.
	 */
	protected function handleErrors($e = null, $message = null, $code = null)
	{
		if ($e)
		{
			$code = 'Error';

			if ($e instanceof ModelNotFoundException)
			{
				$exceptionType = 'ModelNotFoundException';
			}
			elseif ($e instanceof NotFoundHttpException)
			{
				$exceptionType = 'NotFoundHttpException';
			}
			elseif ($e instanceof HttpException)
			{
				$exceptionType = 'HttpException';
				$code = $e->getStatusCode();
			}
			elseif ($e instanceof ModelNotFoundException)
			{
				$exceptionType = 'ModelNotFoundException';
			}
			elseif ($e instanceof AuthorizationException)
			{
				$exceptionType = 'AuthorizationException';
			}
			else
			{
				$exceptionType = 'Error Exception';
			}

			$errorData = [
				'url'        => request()->fullUrl(),
				'exception'  => $exceptionType,
				'code'       => $code,
				'details'    => $e->getTraceAsString(),
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			];
		}
		else
		{
			$errorData = [
				'url'        => request()->fullUrl(),
				'exception'  => "Unauthorized Access",
				'code'       => $code,
				'details'    => $message,
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			];
		}

		SiteErrorLog::create($errorData);

		if ($e)
		{
			abort('404');
		}
	}

	/**
	 * function responsible for ref in header and footer
	 *
	 * @see: the key of "refKeyWords" array should not change,
	 *      it is according to generic function defined in BaseController.
	 */
	protected function getRecommendationRefKeys($ticket)
	{
		$eventId = $ticket->event_id;
		$ticketId = $ticket->id;

		$refKeyWords = [
			'occasionImageKeyword'  => 'reco-diff-occasion-tid-' . $ticketId,
			'occasionFooterKeyword' => 'reco-footer-master-tid-' . $ticketId,
			'companyFooterKeyword'  => 'reco-footer-common-tid-' . $ticketId,
			'occasionHeaderKeyword' => 'reco-header-tid-' . $ticketId
		];

		$allRefKeys = $this->generateOccasionFooterLinkRef($eventId, $refKeyWords);

		return $allRefKeys;
	}

	// internal functions
	private function checkPackageType($package, $checkForType)
	{
		$isType = false;
		$eventId = $package->event_id;

		$tagIds = TypeTag::where('type_event', $eventId)
		                 ->where('map_type_id', $checkForType)
		                 ->pluck('id');

		$packageTagObj = PackageTag::whereIn('tile_tag_id', $tagIds)
		                           ->where('planner_package_id', $package->id)
		                           ->get();

		if ($packageTagObj->count() > 0)
		{
			$isType = true;
		}

		return $isType;
	}

	private function getItemMinPrice($mapTypeId, $eventId = null, $forPage = null)
	{
		$startPrice = '';
		switch ($mapTypeId)
		{
			case config('evibe.ticket.type.venue'):
				$venueHall = $this->getVenueWithHall($eventId);
				$startPrice = $venueHall->min('venue_hall.rent_min') ? $venueHall->min('venue_hall.rent_min') : $venueHall->min('venue.price_min_rent');
				break;

			case config('evibe.ticket.type.decor'):
				$startPrice = Decor::where('is_live', 1)
				                   ->forEvent($eventId)
				                   ->select('decor.*')
				                   ->min('decor.min_price');
				break;

			case  config('evibe.ticket.type.service'):
				$startPrice = TypeServices::forEvent($eventId)
				                          ->select('type_service.*')
				                          ->min('type_service.min_price');
				break;

			case config('evibe.ticket.type.trend'):
				$startPrice = Trend::where('city_id', '=', getCityId())
				                   ->select('trending.*')
				                   ->where('trending.is_live', 1)
				                   ->min('trending.price');
				break;

			case config('evibe.ticket.type.cake'):
				$startPrice = Cake::isLive()
				                  ->forEvent($eventId)
				                  ->select('cake.*')
				                  ->min('cake.price');
		}

		return $startPrice;
	}

	private function getPackageMinPrice($eventId, $type = null)
	{
		$package = Package::isLive();

		if ($eventId == config('evibe.occasion.bachelor.id'))
		{
			$packageStartPrice = $package->forEvent($eventId)
			                             ->joinTags()
			                             ->forPage($type)
			                             ->select('planner_package.*')
			                             ->min('planner_package.price');
		}
		else
		{
			$packageStartPrice = $package->forEvent($eventId)
			                             ->select('planner_package.*')
			                             ->min('planner_package.price');
		}

		return $packageStartPrice;
	}

	private function getVenueWithHall($eventId = null)
	{
		$venueHall = VenueHall::join('venue', 'venue.id', '=', 'venue_hall.venue_id')
		                      ->where('venue.city_id', getCityId())
		                      ->where('venue.is_live', 1)
		                      ->whereNull('venue.deleted_at')
		                      ->select('venue_hall.rent_min', 'venue.price_min_rent')
		                      ->distinct('venue_hall.id');
		if ($eventId)
		{
			$venueHall = $venueHall->forEvent($eventId);
		}

		return $venueHall;
	}

}
