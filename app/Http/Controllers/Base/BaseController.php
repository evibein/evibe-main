<?php

namespace App\Http\Controllers\Base;

use App\Http\Controllers\Controller;
use App\Jobs\Emails\Util\MailGenericErrorToAdminJob;
use App\Models\AddOn\AddOn;
use App\Models\AddOn\AddOnTags;
use App\Models\Cake\Cake;
use App\Models\Cake\CakeFieldValues;
use App\Models\Cancellations\CustomerCancellationPolicy;
use App\Models\Collection\Collection;
use App\Models\Decor\Decor;
use App\Models\Decor\DecorTag;
use App\Models\Package\Package;
use App\Models\Package\PackageFieldValue;
use App\Models\Package\PackageTag;
use App\Models\Service\CategoryService;
use App\Models\Service\Service;
use App\Models\Ticket\TicketUpdate;
use App\Models\Trend\Trend;
use App\Models\Types\TypeCakeCategory;
use App\Models\Types\TypeCakeCategoryValue;
use App\Models\Types\TypeEvent;
use App\Models\Types\TypePackageField;
use App\Models\Types\TypeServices;
use App\Models\Types\TypeTag;
use App\Models\Util\AccessToken;
use App\Models\Util\Area;
use App\Models\Util\AjaxLog;
use App\Models\Util\AuthClient;
use App\Models\Util\AutoBooking;
use App\Models\Util\CheckoutField;
use App\Models\Util\City;
use App\Models\Util\DynamicProductParams;
use App\Models\Util\OptionAddOnMapping;
use App\Models\Util\PrevProductUrls;
use App\Models\Util\Priority;
use App\Models\Util\ProductSortOrder;
use App\Models\Util\SiteErrorLog;
use App\Models\Util\User;
use App\Models\Vendor\Vendor;
use App\Models\Venue\Venue;
use App\Models\Venue\VenueHall;
use App\Models\PartyBag\Shortlist;
use App\Models\OptionReview\OptionReview;
use App\Models\OptionReview\OptionReviewGallery;
use App\Http\Models\Partner\DeliveryGallery;
use Carbon\Carbon;
use Evibe\Facades\EvibeUtilFacade as EvibeUtil;
use Evibe\Passers\PriceCheck;
use Evibe\Passers\Util\LogAjax;
use Evibe\Passers\Util\SiteLogData;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use ReCaptcha\ReCaptcha;
use Spatie\SchemaOrg\AggregateRating;
use Spatie\SchemaOrg\Brand;
use Spatie\SchemaOrg\ItemAvailability;
use Spatie\SchemaOrg\ItemList;
use Spatie\SchemaOrg\ListItem;
use Spatie\SchemaOrg\LocalBusiness;
use Spatie\SchemaOrg\Offer;
use Spatie\SchemaOrg\Product;
use Spatie\SchemaOrg\Rating;
use Spatie\SchemaOrg\Review;
use Spatie\SchemaOrg\Schema;
use Spatie\SchemaOrg\Service as SchemaService;
use Spatie\SchemaOrg\Thing;
use Symfony\Component\HttpKernel\Exception\HttpException;

class BaseController extends Controller
{
	public $viewMappingsByEventIds;
	public $agent;
	private $occasionUrl;
	private $occasionId;
	private $allItemIdsByPriority;
	private $allItemIdsBySortOrder;

	public function __construct()
	{
		// @see: nothing here. Do not delete this method
		$this->viewMappingsByEventIds = [
			'2'                                        => 'birthday', // youth-birthday-party
			'3'                                        => 'birthday', // senior-birthday-party
			'4'                                        => 'pre-post', // receptions
			'5'                                        => 'birthday', // corporate-events
			'6'                                        => 'birthday', // team-building
			'7'                                        => 'pre-post', // sangeeths
			'8'                                        => 'birthday', // naming-ceremony
			'9'                                        => 'pre-post', // wedding
			'10'                                       => 'pre-post', // board-meeting
			'12'                                       => 'pre-post', // engagement
			'14'                                       => 'birthday', // house-warming
			'15'                                       => 'birthday', // office-pooja
			'19'                                       => 'birthday', // baby-shower
			config('evibe.occasion.bachelor.id')       => 'bachelor', // youth
			config('evibe.occasion.kids_birthdays.id') => 'birthday',
			config('evibe.occasion.pre-post.id')       => 'pre-post',
			config('evibe.occasion.surprises.id')      => 'surprises',
			config('evibe.occasion.house-warming.id')  => 'house-warming'
		];

		// Tracking CPU utilization
		// Log::info(request()->method() . "_" . request()->fullUrl());

		$this->agent = app()->make("agent");
	}

	//Get delivery Images
	public function fetchDeliveryImages($mapId, $mapTypeId)
	{
		$imageData = null;

		$optionReviewIds = OptionReview::where([
			                                       ['option_id', '=', $mapId],
			                                       ['option_type_id', '=', $mapTypeId]
		                                       ])->pluck('id')->toArray();
		if (count($optionReviewIds))
		{

			//fetchImages
			// @todo: use joins in query for optimisation
			$optionReviewImageIds = OptionReviewGallery::whereIn('option_review_id', $optionReviewIds)
			                                           ->pluck('delivery_gallery_id')
			                                           ->toArray();
			$imageData = DeliveryGallery::whereIn('id', $optionReviewImageIds)
			                            ->orderBy('updated_at', 'desc')
			                            ->take(12)
			                            ->get();
		}

		return $imageData;
	}

	// access token
	public function getAccessToken($order, $userId = null)
	{
		if (is_null($userId))
		{
			$userId = $order->provider->user_id;
		}

		$clientId = config('evibe.api.client_id');
		$user = User::find($userId);
		$accessToken = false;

		if (!$userId || !$user)
		{
			abort('403');
		}

		$token = AccessToken::where('user_id', $userId)
		                    ->where('auth_client_id', $clientId)
		                    ->first();

		if (!$token)
		{
			$client = AuthClient::where('auth_client_id', $clientId)->first();
			if ($client)
			{
				$accessToken = $this->issueAccessToken($user, $client);
			}
			else
			{
				Log::error("Invalid client is trying to make the api request");
			}
		}
		else
		{
			$accessToken = $token->access_token;
		}

		return $accessToken;
	}

	public function getOccasionId()
	{
		return $this->occasionId;
	}

	public function getOccasionUrl()
	{
		return $this->occasionUrl;
	}

	public function getExistingQueryParams()
	{
		$existingQueryParams = [];

		foreach (request()->all() as $key => $value)
		{
			if ($key != 'page')
			{
				$existingQueryParams[$key] = urldecode($value);
			}
		}

		return $existingQueryParams;
	}

	public function getGalleryBaseUrl()
	{
		return config('evibe.gallery.host');
	}

	public function getSimilarTagsIds($tags, $productTypeId, $eventId)
	{
		// cache after validation - check
		$similarTagIdsCacheKey = 'similarProductTagIds-' . implode('-', $tags);
		if (Cache::has($similarTagIdsCacheKey))
		{
			$sortedIds = Cache::get($similarTagIdsCacheKey);
		}
		else
		{
			$tags = is_array($tags) ? $tags : [$tags];
			$sortedIds = [];

			switch ($productTypeId)
			{
				case config('evibe.ticket.type.decor'):

					$sortedIds = Decor::select('decor.id', DB::raw('COUNT(decor.id) as count'))
					                  ->join('decor_tags', 'decor_tags.decor_id', '=', 'decor.id')
					                  ->isLive()
					                  ->forCity()
					                  ->forEvent($eventId)
					                  ->whereIn('decor_tags.tag_id', $tags)
					                  ->groupBy('decor.id')
					                  ->orderBy('count', 'DESC')
					                  ->pluck('id')
					                  ->toArray();
					break;

				case config('evibe.ticket.type.package'):
				case config('evibe.ticket.type.resorts'):
				case config('evibe.ticket.type.villas'):
				case config('evibe.ticket.type.lounges'):
				case config('evibe.ticket.type.food'):
				case config('evibe.ticket.type.surprises'):
				case config('evibe.ticket.type.priests'):
				case config('evibe.ticket.type.tents'):

					$sortedIds = Package::select('planner_package.id', DB::raw('COUNT(planner_package.id) AS count'))
					                    ->join('planner_package_tags', 'planner_package_tags.planner_package_id', '=', 'planner_package.id')
					                    ->isLive()
					                    ->forCity()
					                    ->forEvent($eventId)
					                    ->whereIn('planner_package_tags.tile_tag_id', $tags)
					                    ->groupBy('planner_package.id')
					                    ->orderBy('count', 'DESC')
					                    ->pluck('id')
					                    ->toArray();
					break;

				case config('evibe.ticket.type.entertainment'):

					$sortedIds = TypeServices::select('type_service.id', DB::raw('COUNT(type_service.id) AS count'))
					                         ->join('service_tags', 'service_tags.service_id', '=', 'type_service.id')
					                         ->isLive()
					                         ->forCity()
					                         ->forEvent($eventId)
					                         ->whereIn('service_tags.tag_id', $tags)
					                         ->groupBy('type_service.id')
					                         ->orderBy('count', 'DESC')
					                         ->pluck('id')
					                         ->toArray();
					break;

				case config('evibe.ticket.type.cake'):

					$sortedIds = Cake::select('cake.id', DB::raw('COUNT(cake.id) AS count'))
					                 ->join('cake_tags', 'cake_tags.cake_id', '=', 'cake.id')
					                 ->isLive()
					                 ->forCity()
					                 ->forEvent($eventId)
					                 ->whereIn('cake_tags.tag_id', $tags)
					                 ->groupBy('cake.id')
					                 ->orderBy('count', 'DESC')
					                 ->get();

					$sortedIds = $sortedIds->pluck('id')
					                       ->toArray();
			}

			Cache::put($similarTagIdsCacheKey, $sortedIds, config('evibe.cache.refresh-time'));
		}

		return $sortedIds;
	}

	/**
	 * Sorting items by priority: get ids by priority, usort method
	 *
	 * @author Anji
	 * @since  26 May 2016
	 */
	public function getItemIdsByPriority($occasionId, $pageId, $allIds = [], $count = false, $tagIds = null)
	{
		$pageIds = is_array($pageId) ? $pageId : [$pageId];
		$cityId = getCityId();

		// storing this to avoid repeated queries in case of filter by tags
		$key = $cityId . "_" . $occasionId . "_" . $pageId;
		if (!isset($this->allItemIdsByPriority[$key]))
		{
			$priorityIds = Priority::where('occasion_id', $occasionId)
			                       ->where('city_id', $cityId)
			                       ->whereIn('map_type_id', $pageIds)
			                       ->orderBy(DB::raw('ISNULL(priority), priority'), 'ASC')
			                       ->get();

			$this->allItemIdsByPriority[$key] = $priorityIds;
		}
		$priorityIds = $this->allItemIdsByPriority[$key];

		if (is_null($tagIds))
		{
			$priorityIds = $priorityIds->filter(function ($priorityObj) {
				return is_null($priorityObj->type_tag_id);
			});
		}
		else
		{
			$tagIds = is_array($tagIds) ? $tagIds : [$tagIds];

			$priorityIds = $priorityIds->filter(function ($priorityObj) use ($tagIds) {
				return (!is_null($priorityObj->type_tag_id) && in_array($priorityObj->type_tag_id, $tagIds));
			});
		}

		$itemIdsByPriority = $priorityIds->pluck('map_id')->all();
		$itemIdsByPriority = array_slice($itemIdsByPriority, 0, 12); // fixed slots for priority

		// get ids from computed sort order table
		// storing to avoid running query multiple times in case of filter by tags
		if (!isset($this->allItemIdsBySortOrder[$key]))
		{
			$sortOrderIds = ProductSortOrder::whereIn('product_type_id', $pageIds)
			                                ->where('event_id', $occasionId)
			                                ->where('city_id', $cityId)
			                                ->orderBy('score', 'DESC')
			                                ->pluck('product_id')
			                                ->toArray();

			$this->allItemIdsBySortOrder[$key] = $sortOrderIds;
		}
		$sortOrderIds = $this->allItemIdsBySortOrder[$key];

		$itemIdsByPriority = array_merge($itemIdsByPriority, $sortOrderIds);
		$itemIdsByPriority = array_unique($itemIdsByPriority); // remove duplicate values

		if (is_int($count) && $count)
		{
			$itemIdsByPriority = array_slice($itemIdsByPriority, 0, $count); // used for fetching occasion home page top items
		}

		// merge priority ids from db + all ids = array with sorted keys
		if (count($allIds))
		{
			$itemIdsByPriority = array_merge($itemIdsByPriority, $allIds);
			$itemIdsByPriority = array_unique($itemIdsByPriority);
		}

		return $itemIdsByPriority;
	}

	/**
	 * Get priorities based on the current selected category.
	 *
	 * Algorithm:
	 *  N = total items to be prioritised ($itemsCount)
	 *  R = get priorities for this category ($userFilterId)
	 *  D =  get priorities for this page (category = "all" case)
	 *  if (R == N)  then return; (all the items with this tag have priorities)
	 *  else
	 *      R' = get priorities for related tags (parent and/or siblings, whichever is applicable)
	 *      if (R + R' == N) return;
	 *      else
	 *          R" = get priorities of all other tags (for this page and not in related tags)
	 *          return;
	 * if (R+R'+R" < N) merge with D;
	 *
	 * @todo: R' can be made only for parent Id instead of all related tags
	 * @todo: enhance algorithm by limiting the priorities
	 * @return array
	 */
	public function getItemIdsByPriorityForTags($params = [])
	{
		if (!is_array($params))
		{
			return [];
		}

		$eventId = $params['eventId'];
		$pageId = $params['pageId'];
		$userFilterId = $params['userFilterId'];
		$allFilterTagIds = $params['allFilterTagIds'];
		$allItemIds = $params['allItemIds'];
		$itemsCount = count($allItemIds);

		// remove $userFilterTagId from $allFilterTagIds
		if (($key = array_search($userFilterId, $allFilterTagIds)) !== false)
		{
			unset($allFilterTagIds[$key]);
		}

		$itemIdsByPriority = $userFilterItemPriorityIds = $this->getItemIdsByPriority($eventId, $pageId, [], false, $userFilterId); // R

		if (count($itemIdsByPriority) && (count($itemIdsByPriority) < $itemsCount))
		{
			$relatedFilterItemPriorityIds = $this->getItemIdsByPriority($eventId, $pageId, [], false, $allFilterTagIds); // R'
			$itemIdsByPriority = $userAndRelatedFilterPriorityIds = array_unique(array_merge($userFilterItemPriorityIds, $relatedFilterItemPriorityIds));

			if (count($itemIdsByPriority) && (count($itemIdsByPriority) < $itemsCount))
			{
				$otherFilterTagsIds = TypeTag::forEvent($eventId)
				                             ->forPage($pageId)
				                             ->filterable()
				                             ->pluck('id')
				                             ->toArray();

				$otherFilterItemPriorityIds = $this->getItemIdsByPriority($eventId, $pageId, [], false, $otherFilterTagsIds); // R"
				$itemIdsByPriority = array_unique(array_merge($userAndRelatedFilterPriorityIds, $otherFilterItemPriorityIds));
			}
		}

		if (count($itemIdsByPriority) < $itemsCount)
		{
			$defaultItemIdsByPriority = $this->getItemIdsByPriority($eventId, $pageId, $allItemIds); // D
			$itemIdsByPriority = array_unique(array_merge($itemIdsByPriority, $defaultItemIdsByPriority));
		}

		return $itemIdsByPriority;
	}

	public function getSortedListByPriorities($list, $priorities)
	{
		$list = is_array($list) ? $list : $list->all(); // to Array for sorting
		usort($list, function ($a, $b) use ($priorities) {
			return $this->sortItemsByPriority($a, $b, $priorities);
		});

		return $list;
	}

	/**
	 * @see      Added few more sort types to match existing.
	 * @todo     Remove unused sort types
	 * @modified 25 Jan 2015
	 */
	public function getSortParams($type = 'planner', $keys = null)
	{
		$sort = request('sort');
		$priorityCol = "$type.priority";
		$priceCol = "$type.price";
		$entity = DB::raw("ISNULL($priorityCol), $priorityCol"); // fix to show NULL values last
		$order = 'ASC';
		$sortType = 'popularity';
		$isDefault = true; // by default sort by priority
		if (is_array($keys) && array_key_exists('price', $keys))
		{
			$priceCol = $keys['price'];
		}

		if ($sort)
		{
			switch ($sort)
			{
				case 'pa':
					$order = 'ASC';
					break;

				case 'pd':
					$order = 'DESC';
					break;

				case 'plth':
					$entity = $priceCol;
					$order = 'ASC';
					$sortType = 'plth';
					$isDefault = false;
					break;

				case 'phtl':
					$entity = $priceCol;
					$order = 'DESC';
					$sortType = 'phtl';
					$isDefault = false;
					break;

				case 'new-arrivals':
					$entity = $type . '.created_at';
					$order = 'DESC';
					$sortType = 'new-arrivals';
					$isDefault = false;
					break;

				case 'ca':
					$entity = $priceCol;
					$order = 'ASC';
					$isDefault = false;
					break;

				case 'cd':
					$entity = $priceCol;
					$order = 'DESC';
					$isDefault = false;
					break;
			}
		}

		// @see: fixing issue with some old URLs
		if ($type == "venue_hall" && ($sort == 'ea' || $sort == 'ed'))
		{
			$entity = DB::raw("ISNULL($priorityCol), $priorityCol"); // fix to show NULL values last
			$sortType = 'popularity';
			$isDefault = false;
		}

		return [
			'entity'    => $entity,
			'order'     => $order,
			'sortType'  => $sortType,
			'isDefault' => $isDefault
		];
	}

	/**
	 * generic items for auto bookable items
	 * $items should be either object or array
	 *
	 * @param     $pageId
	 * @param     $items
	 * @param int $perPage
	 *
	 * @return array
	 * @since 5 Nov by Vikash <vikash@evibe.in>
	 */
	public function getPaginatedAutoBookableItems($pageId, $items, $perPage = 21)
	{
		if (!is_array($items))
		{
			$items = $items->toArray();
		}

		// Paginate results
		$currentPage = Paginator::resolveCurrentPage();
		$offset = ($currentPage * $perPage) - $perPage;
		$sliceItems = array_slice($items, $offset, $perPage, true);

		$paginatedItems = new LengthAwarePaginator(
			$sliceItems,
			count($items),
			$perPage,
			$currentPage,
			['path' => Paginator::resolveCurrentPath()]
		);

		// Get list of all auto book-ables.
		$paginatedItemIds = array_map(function ($item) {
			return $item['id'];
		}, $sliceItems);

		if (is_array($pageId))
		{
			$autoBook = AutoBooking::whereIn('map_type_id', $pageId)
			                       ->whereIn('map_id', $paginatedItemIds)
			                       ->pluck('map_id')
			                       ->toArray();
		}
		else
		{
			$autoBook = AutoBooking::where('map_type_id', $pageId)
			                       ->whereIn('map_id', $paginatedItemIds)
			                       ->pluck('map_id')
			                       ->toArray();
		}

		return [
			'paginatedItems' => $paginatedItems,
			'autoBook'       => $autoBook
		];
	}

	public function getPaginatedItems($totalItemsCount, $slicedItems, $currentPage, $perPage = 21)
	{
		if (!is_array($slicedItems))
		{
			$slicedItems = $slicedItems->toArray();
		}

		$paginatedItems = new LengthAwarePaginator(
			$slicedItems,
			$totalItemsCount,
			$perPage,
			$currentPage,
			['path' => Paginator::resolveCurrentPath()]
		);

		return $paginatedItems;
	}

	public function getCurrentPageOption($options, $perPage = 52)
	{
		$currentPage = Paginator::resolveCurrentPage();
		$currentPage = $currentPage ?: 1;
		$offset = ($currentPage * $perPage) - $perPage;

		if (is_array($options))
		{
			$options = array_slice($options, $offset, $perPage, true);
			$collection = collect($options);
		}
		else
		{
			$collection = $options->skip($offset)->take($perPage)->get();
		}

		return [
			'options'     => $collection,
			'currentPage' => $currentPage
		];
	}

	public function setOccasionId($occasionId)
	{
		session(['eventId' => $occasionId]);
		$this->occasionId = $occasionId;

		view()->share('occasionId', $occasionId);
	}

	public function setOccasionUrl($occasionUrl)
	{
		session(['eventUrl' => $occasionUrl]);
		$this->occasionUrl = $occasionUrl;

		view()->share('cityOccasionUrl', $occasionUrl);
	}

	// @todo: redirectUrl should consider occasion
	public function validateUrlParams($params)
	{
		$validUrlObj = [];

		if (is_array($params))
		{
			foreach ($params as $param)
			{
				$key = $param['key'];
				$value = $param['value'];

				switch ($key)
				{
					case 'city':
						$city = City::where('url', 'LIKE', $value)->first();
						if (!$city)
						{
							$validUrlObj['redirectUrl'] = '/' . getCityUrl(); // redirect to home page
							$validUrlObj['isNotValid'] = true;
						}
						else
						{
							checkAndSetCitySessions($city);
							$validUrlObj['city'] = $city;
						}
						break;

					case 'occasion':
						$occasion = TypeEvent::where('url', 'LIKE', $value)->first();
						if (!$occasion)
						{
							$validUrlObj['redirectUrl'] = '/' . getCityUrl(); // redirect to city page
							$validUrlObj['isNotValid'] = true;
						}
						else
						{
							$validUrlObj['occasion'] = $occasion;
						}
						break;

					case 'services':
						$service = Service::where('url', 'LIKE', $value)->first();
						if (!$service)
						{
							$validUrlObj['redirectUrl'] = '/' . getCityUrl();
							$validUrlObj['isNotValid'] = true;
						}
						else
						{
							$validUrlObj['services'] = $service;
						}
						break;

					case 'type_service':
						$service = TypeServices::where('url', 'LIKE', $value)->first();
						if (!$service)
						{
							$validUrlObj['redirectUrl'] = '/' . getCityUrl();
							$validUrlObj['isNotValid'] = true;
						}
						else
						{
							$validUrlObj['type_service'] = $service;
						}
						break;

					case 'planner':
						$planner = Vendor::where('url', 'LIKE', $value)->first();
						if (!$planner)
						{
							$validUrlObj['redirectUrl'] = '/' . getCityUrl() . '/' . config('evibe.results_url.planners');
							$validUrlObj['isNotValid'] = true;
						}
						else
						{
							$validUrlObj['planner'] = $planner;
						}
						break;

					case 'hall':
						$hall = VenueHall::where('url', 'LIKE', $value)->first();
						if (!$hall)
						{
							$tempRedirectUrl = '/' . getCityUrl();
							$tempRedirectUrl .= '/' . config('evibe.occasion.kids_birthdays.url');
							$tempRedirectUrl .= '/' . config('evibe.results_url.venues') . '?ref=invalid-profile';

							$validUrlObj['redirectUrl'] = $tempRedirectUrl;
							$validUrlObj['isNotValid'] = true;
						}
						else
						{
							$validUrlObj['hall'] = $hall;
						}
						break;

					case 'venue':
						$venue = Venue::where('url', 'LIKE', $value)->first();
						if (!$venue)
						{
							$tempRedirectUrl = '/' . getCityUrl();
							$tempRedirectUrl .= '/' . config('evibe.occasion.kids_birthdays.url');
							$tempRedirectUrl .= '/' . config('evibe.results_url.venues') . '?ref=invalid-profile';

							$validUrlObj['redirectUrl'] = $tempRedirectUrl;
							$validUrlObj['isNotValid'] = true;
						}
						else
						{
							$validUrlObj['venue'] = $venue;
						}
						break;

					case 'package':
						$package = Package::where('url', 'LIKE', $value)
						                  ->where('is_live', 1)->first();
						if (!$package)
						{
							$productId = $this->hasPreviousUrl(config("evibe.ticket.type.package"), $value);
							if ($productId > 0)
							{
								$package = Package::find($productId);
								if (!$package)
								{
									$tempRedirectUrl = '/' . getCityUrl();
									$tempRedirectUrl .= '/' . config('evibe.occasion.kids_birthdays.url');
									$tempRedirectUrl .= '/' . config('evibe.results_url.packages') . '?ref=invalid-profile';

									$validUrlObj['redirectUrl'] = $tempRedirectUrl;
									$validUrlObj['isNotValid'] = true;
								}
								else
								{
									$validUrlObj['isRedirect'] = true;
									$validUrlObj['redirectUrl'] = config("evibe.host") . str_replace($value, $package->url, $_SERVER['REQUEST_URI']);
								}
							}
							else
							{
								$tempRedirectUrl = '/' . getCityUrl();
								$tempRedirectUrl .= '/' . $this->getOccasionUrl();

								//@see: Since House warming doesn't have packages home page, redirecting to occasion homepage.
								if ($this->getOccasionUrl() != "house-warming")
								{
									$tempRedirectUrl .= '/' . config('evibe.results_url.packages') . '?ref=invalid-profile';
								}

								$validUrlObj['redirectUrl'] = $tempRedirectUrl;
								$validUrlObj['isNotValid'] = true;
							}
						}
						else
						{
							$validUrlObj['planner-package'] = $package;
						}
						break;

					case 'food':
						$package = Package::where('url', 'LIKE', $value)
						                  ->where('is_live', 1)->first();
						if (!$package)
						{
							$productId = $this->hasPreviousUrl(config("evibe.ticket.type.package"), $value);
							if ($productId > 0)
							{
								$package = Package::find($productId);
								if (!$package)
								{
									$tempRedirectUrl = '/' . getCityUrl();
									$tempRedirectUrl .= '/' . config('evibe.occasion.kids_birthdays.url');
									$tempRedirectUrl .= '/' . config('evibe.results_url.food') . '?ref=invalid-profile';

									$validUrlObj['redirectUrl'] = $tempRedirectUrl;
									$validUrlObj['isNotValid'] = true;
								}
								else
								{
									$validUrlObj['isRedirect'] = true;
									$validUrlObj['redirectUrl'] = config("evibe.host") . str_replace($value, $package->url, $_SERVER['REQUEST_URI']);
								}
							}
							else
							{
								$tempRedirectUrl = '/' . getCityUrl();
								$tempRedirectUrl .= '/' . $this->getOccasionUrl();
								$tempRedirectUrl .= '/' . config('evibe.results_url.packages') . '?ref=invalid-profile';

								$validUrlObj['redirectUrl'] = $tempRedirectUrl;
								$validUrlObj['isNotValid'] = true;
							}
						}
						else
						{
							$validUrlObj['planner-package'] = $package;
						}
						break;

					case 'resort':
						$package = Package::where('url', 'LIKE', $value)
						                  ->where('is_live', 1)->first();
						if (!$package)
						{
							$tempRedirectUrl = '/' . getCityUrl();
							$tempRedirectUrl .= '/' . config('evibe.occasion.bachelor.url');
							$tempRedirectUrl .= '/' . config('evibe.occasion.bachelor.resort.profile_url') . '?ref=invalid-profile';

							$validUrlObj['redirectUrl'] = $tempRedirectUrl;
							$validUrlObj['isNotValid'] = true;
						}
						else
						{
							$validUrlObj['planner-package'] = $package;
						}
						break;

					case 'villa':
						$package = Package::where('url', 'LIKE', $value)
						                  ->where('is_live', 1)->first();
						if (!$package)
						{
							$tempRedirectUrl = '/' . getCityUrl();
							$tempRedirectUrl .= '/' . config('evibe.occasion.bachelor.url');
							$tempRedirectUrl .= '/' . config('evibe.occasion.bachelor.villa.profile_url') . '?ref=invalid-profile';

							$validUrlObj['redirectUrl'] = $tempRedirectUrl;
							$validUrlObj['isNotValid'] = true;
						}
						else
						{
							$validUrlObj['planner-package'] = $package;
						}
						break;

					case 'lounge':
						$package = Package::where('url', 'LIKE', $value)
						                  ->where('is_live', 1)->first();
						if (!$package)
						{
							$tempRedirectUrl = '/' . getCityUrl();
							$tempRedirectUrl .= '/' . config('evibe.occasion.bachelor.url');
							$tempRedirectUrl .= '/' . config('evibe.occasion.bachelor.lounge.profile_url') . '?ref=invalid-profile';

							$validUrlObj['redirectUrl'] = $tempRedirectUrl;
							$validUrlObj['isNotValid'] = true;
						}
						else
						{
							$validUrlObj['planner-package'] = $package;
						}
						break;

					case 'experience':
						$experience = Package::where('url', 'LIKE', $value)->first();
						if (!$experience)
						{
							$validUrlObj['redirectUrl'] = '/' . getCityUrl() . '/' . config('evibe.results_url.experiences');
							$validUrlObj['isNotValid'] = true;
						}
						else
						{
							$validUrlObj['experience'] = $experience;
						}
						break;

					case 'venue-deals':
						$package = Package::where('url', 'LIKE', $value)
						                  ->where('is_live', 1)->first();
						if (!$package)
						{
							$tempRedirectUrl = '/' . getCityUrl();
							$tempRedirectUrl .= '/' . config('evibe.occasion.kids_birthdays.url');
							$tempRedirectUrl .= '/' . config('evibe.occasion.kids_birthdays.venue-deal.results_url') . '?ref=invalid-profile';

							$validUrlObj['redirectUrl'] = $tempRedirectUrl;
							$validUrlObj['isNotValid'] = true;
						}
						else
						{
							$validUrlObj['planner-package'] = $package;
						}
						break;

					case 'services_category':
						$category = CategoryService::where('url', 'LIKE', $value)->first();
						if (!$category)
						{
							abort(404);
						}
						$validUrlObj['category'] = $category;
						break;

					case 'trend':
						$trending = Trend::where('url', 'LIKE', $value)->first();
						if (!$trending)
						{
							$tempRedirectUrl = '/' . getCityUrl();
							$tempRedirectUrl .= '/' . config('evibe.occasion.kids_birthdays.url');
							$tempRedirectUrl .= '/' . config('evibe.results_url.trends') . '?ref=invalid-profile';

							$validUrlObj['redirectUrl'] = $tempRedirectUrl;
							$validUrlObj['isNotValid'] = true;
						}
						else
						{
							$validUrlObj['trending'] = $trending;
						}
						break;

					case 'cake':
						$cake = Cake::where('url', 'LIKE', $value)->first();
						if (!$cake)
						{
							$validUrlObj['redirectUrl'] = '/' . getCityUrl() . '/' . $this->getOccasionId() . '/' . config('evibe.results_url.cakes');
							$validUrlObj['isNotValid'] = true;
						}
						else
						{
							$validUrlObj['cake'] = $cake;
						}
						break;

					case 'decor':
						$decor = Decor::where('url', 'LIKE', $value)->where('is_live', 1)->with('provider')->first();
						if (!$decor)
						{
							$validUrlObj['redirectUrl'] = '/' . getCityUrl() . '/' . $this->getOccasionUrl() . '/' . config('evibe.results_url.decors') . '/all';
							$validUrlObj['isNotValid'] = true;
						}
						else
						{
							$validUrlObj['decor'] = $decor;
						}
						break;

					case 'collection':
						$collection = Collection::where('url', 'LIKE', $value)->first();
						if (!$collection)
						{
							$validUrlObj['redirectUrl'] = '/' . getCityUrl() . '/' . $this->getOccasionUrl() . '/';
							$validUrlObj['isNotValid'] = true;
						}
						else
						{
							$validUrlObj['collection'] = $collection;
						}
				}
			}
		}
		else
		{
			// validate city URL
			$cityUrl = $params;
			$city = City::select("id", "url", "name", "is_default")
			            ->where('url', 'LIKE', $cityUrl)
			            ->first();

			if (!$city)
			{
				if (in_array($cityUrl, config("cities.all")))
				{
					// redirect to default landing page of all passive cities
					$validUrlObj['city'] = $cityUrl;
					$validUrlObj['passiveCity'] = true;
				}
				else
				{
					// redirect to default city home page
					$redirectUrl = '/' . getCityUrl();
					$validUrlObj['isNotValid'] = true;
					$validUrlObj['redirectUrl'] = $redirectUrl;
				}
			}
			else
			{
				$validUrlObj['city'] = $city;
				checkAndSetCitySessions($city);
			}
		}

		return $validUrlObj;
	}

	public function hasPreviousUrl($productTypeId, $url)
	{
		$previousProduct = PrevProductUrls::where("product_type_id", $productTypeId)
		                                  ->where("url", $url)
		                                  ->first();

		return $previousProduct ? $previousProduct->product_id : 0;
	}

	public function generateUrl($usp, $appendTimeStamp = false)
	{
		$usp = trim($usp);
		$pattern = '/[^\w ]+/';
		$replacement = '';
		$url = '';
		$usp_space = preg_replace($pattern, $replacement, $usp);
		if ($appendTimeStamp)
		{
			$url = time() . '-';
		}
		$url .= preg_replace('/\s\s+/', ' ', $usp_space);
		$url = str_replace(' ', '-', $url);
		$url_shorten = strtolower($this->limitText($url, 70));

		return $url_shorten;
	}

	public function formatPrice($price)
	{
		return EvibeUtil::formatPrice($price);
	}

	public function formatPriceWithoutChar($price)
	{
		return EvibeUtil::formatPriceWithoutChar($price);
	}

	public function getRoundedTokenAmount($price)
	{
		return EvibeUtil::getRoundedTokenAmount($price);
	}

	public function formatNumber($number)
	{
		return EvibeUtil::formatNumber($number);
	}

	public function truncateText($text, $len = 10)
	{
		$truncatedText = $text;
		if (strlen($text) > $len)
		{
			$truncatedText = substr($text, 0, $len) . '...';
		}

		return $truncatedText;
	}

	// Get the list of auto bookable packages
	public function sortItemsByPriority($item1, $item2, $priority)
	{
		$item1Id = is_int($item1) ? $item1 : $item1->id;
		$item2Id = is_int($item2) ? $item2 : $item2->id;
		$search1 = array_search($item1Id, $priority);
		$search2 = array_search($item2Id, $priority);

		if ($search1 == $search2)
		{
			return 0;
		}
		elseif ($search1 > $search2)
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}

	// Get list of all map_ids sorted with priority
	public function sortTagsByPriority($item1, $item2, $priority)
	{
		$item1Id = is_int($item1) ? $item1 : $item1->priority;
		$item2Id = is_int($item2) ? $item2 : $item2->priority;
		$search1 = array_search($item1Id, $priority);
		$search2 = array_search($item2Id, $priority);

		if ($search1 == $search2)
		{
			return 0;
		}
		elseif ($search1 > $search2)
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}

	public function isAutoBookableItem($pageId, $itemId)
	{
		$itemCount = AutoBooking::where('map_type_id', $pageId)
		                        ->where('map_id', $itemId)
		                        ->count();

		$isAutoBookable = $itemCount && $itemCount > 0 ? 1 : 0;

		return $isAutoBookable;
	}

	// trim for space in url
	public function getShortenUrl($longUrl)
	{
		return EvibeUtil::getShortUrl($longUrl);
	}

	public function mergeView($cityUrl, $eventId)
	{
		$cityUrl = strtolower($cityUrl); // just in case Uppercase word sent from URL

		if (!isset($this->viewMappingsByEventIds[$eventId]))
		{
			$this->viewMappingsByEventIds[$eventId] = 'birthday'; // default
		}

		$header = "cities." . $cityUrl . ".occasion." . $this->viewMappingsByEventIds[$eventId] . ".header.header";
		$footer = "cities." . $cityUrl . ".occasion." . $this->viewMappingsByEventIds[$eventId] . ".footer.footer";
		$home = "cities." . $cityUrl . ".occasion." . $this->viewMappingsByEventIds[$eventId] . ".home.home";

		$press = "cities." . $cityUrl . ".city-press";

		return [
			'views' => [
				'header' => $header,
				'footer' => $footer,
				'home'   => $home,
				'press'  => $press,
			]
		];
	}

	// merge the required values to add / delete shortlist option
	public function mergeShortlistData($shortlistDataParams)
	{
		return [
			'shortlistData' => [
				'cityId'     => getCityId(),
				'occasionId' => $this->getOccasionId(),
				'mapId'      => $shortlistDataParams['mapId'],
				'mapTypeId'  => $shortlistDataParams['mapTypeId']
			]
		];
	}

	/**
	 * Redirect to city customized controller methods
	 *
	 * @author Saachi
	 * @since  25 Oct 2016
	 */
	public function customizedCityControllerRedirect($params)
	{
		if (!isset($params['occasion']) || !isset($params['controller']) || !isset($params['method']))
		{
			return false;
		}

		$occasionName = $params['occasion'];
		$controllerName = $params['controller'];
		$methodName = $params['method'];
		$cityName = isset($params['cityName']) ? $params['cityName'] : getCityName();
		$methodParams = isset($params['methodParams']) ? $params['methodParams'] : [];

		$filePath = 'App\Http\Controllers\Cities\\' . $cityName . '\\Occasions\\' . $occasionName . '\\' . $controllerName;

		try
		{
			if (class_exists($filePath))
			{
				return app($filePath)->$methodName($methodParams);
			}
		} catch (\ReflectionException $e)
		{
			// class does not exist
			// used the default
		}

		return false;
	}

	// generic function for image upload
	public function uploadImageToServer($imageFile, $imageUploadTo, $args = null)
	{
		$rootPath = config('evibe.gallery.host');
		$parentDirs = config('evibe.gallery.root');
		$fullUploadTo = $parentDirs . $imageUploadTo; // append with parent dirs
		$imageName = $imageFile->getClientOriginalName();
		$originalName = str_replace(" ", "", $imageName);
		$newImageName = trim(time() . '-' . $originalName);
		$tempServerPath = $imageFile->getRealPath();
		$isWatermark = is_array($args) && array_key_exists("isWatermark", $args) ? $args['isWatermark'] : true; // default true
		$isOptimize = is_array($args) && array_key_exists("isOptimize", $args) ? $args['isOptimize'] : true; // default true

		if (!file_exists($fullUploadTo))
		{
			$oldumask = umask(0);
			mkdir($fullUploadTo, 0777, true);
			umask($oldumask);
		}

		try
		{
			// move to the correct place
			move_uploaded_file($tempServerPath, $fullUploadTo . $newImageName);

			// watermark image
			if ($isWatermark)
			{
				$curlPath = $rootPath . 'images/watermark?path=' . '/gallery/' . $imageUploadTo . $newImageName;
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $curlPath);

				if (curl_exec($ch))
				{
					curl_close($ch);
				}
			}

			// optimize image => results + profile + thumbs
			if ($isOptimize)
			{
				$this->optimizeImages($fullUploadTo, $newImageName);
			}

			return $newImageName;

		} catch (\Exception $e)
		{
			Log::error("Exception with uploading image: " . $fullUploadTo . $newImageName);
		}

		return false; // failed case
	}

	public function uploadVideoToServer($videoFile, $videoUploadTo)
	{
		$rootPath = config('evibe.gallery.host');
		$parentDirs = config('evibe.gallery.root');
		$fullUploadTo = $parentDirs . $videoUploadTo; // append with parent dirs
		$videoName = $videoFile->getClientOriginalName();
		$originalName = str_replace(" ", "", $videoName);
		$newVideoName = trim(time() . '-' . $originalName);
		$tempServerPath = $videoFile->getRealPath();

		if (!file_exists($fullUploadTo))
		{
			$oldumask = umask(0);
			mkdir($fullUploadTo, 0777, true);
			umask($oldumask);
		}

		try
		{
			// move to the correct place
			move_uploaded_file($tempServerPath, $fullUploadTo . $newVideoName);

			return $newVideoName;

		} catch (\Exception $e)
		{
			Log::error("Exception with uploading image: " . $fullUploadTo . $newVideoName);
		}

		return false; // failed case
	}

	public function getImageTitle($image)
	{
		return pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
	}

	public function trimSpace($string)
	{
		return str_replace(" ", "", $string);
	}

	public function getSeoText($keys, $baseKey, $replaces = null, $values = null)
	{
		$baseKey = $baseKey ? "seo." . $baseKey : "seo.defaults.base";
		$seo = is_array($values) && count($values) ? $values : config($baseKey);
		$identifier = $baseKey;

		if (is_array($keys) && count($keys))
		{
			$identifier = "seo.";
			foreach ($keys as $key)
			{
				if ($key)
				{
					$identifier .= "$key."; // $key can be false
				}
			}

			$identifier = substr($identifier, 0, strlen($identifier) - 1); // remove last "."
		}

		$seo = $this->getSeoValues(config($identifier), $seo, $replaces);

		return $seo;
	}

	/**
	 * Generic function for footer ref
	 *
	 * @param       $eventId : add an array with three keys (optional)
	 * @param array $keys    : "occasionImageKeyword", "occasionFooterKeyword" , "companyFooterKeyword" &
	 *                       "occasionHeaderKeyword"
	 *
	 * @return array
	 */
	public function generateOccasionFooterLinkRef($eventId, $keys = [])
	{
		$event = TypeEvent::find($eventId);
		$eventUrl = "";

		if ($event)
		{
			$eventUrl = $event->url ? $event->url : str_replace(" ", "-", strtolower($event->name));
		}

		$footerLinkRef = [
			'occasionImageRefKey'  => $event ? $event->url : '', // this key will be used for ref of "Not Looking For this occasion
			'occasionFooterRefKey' => 'footer-master', // this key will be used for footer of special occasion,
			'companyFooterRefKey'  => 'footer', // this key will be used for ref of company footer (company footer),
			'occasionHeaderRefKey' => 'header' // this key will be used for ref the header
		];

		if ($event)
		{
			if (array_key_exists('occasionImageKeyword', $keys))
			{
				$footerLinkRef['occasionImageRefKey'] = $keys['occasionImageKeyword'] ? $eventUrl . '-' . $keys['occasionImageKeyword'] : $eventUrl;
			}

			if (array_key_exists('occasionFooterKeyword', $keys))
			{
				$footerLinkRef['occasionFooterRefKey'] = $keys['occasionFooterKeyword'] ? $eventUrl . '-' . $keys['occasionFooterKeyword'] : $eventUrl;
			}

			if (array_key_exists('companyFooterKeyword', $keys))
			{
				$footerLinkRef['companyFooterRefKey'] = $keys['companyFooterKeyword'] ? $eventUrl . '-' . $keys['companyFooterKeyword'] : $eventUrl;
			}

			if (array_key_exists('occasionHeaderKeyword', $keys))
			{
				$footerLinkRef['occasionHeaderRefKey'] = $keys['occasionHeaderKeyword'] ? $eventUrl . '-' . $keys['occasionHeaderKeyword'] : $eventUrl;
			}
		}

		return $footerLinkRef;
	}

	/**
	 * @param array $listItems
	 *
	 * @return \Spatie\SchemaOrg\BreadcrumbList
	 */
	public function schemaBreadcrumbList($listItems = [])
	{
		$itemListElements = [];
		$count = 1;
		foreach ($listItems as $url => $listItem)
		{
			$itemListElements[] = Schema::listItem()
			                            ->position($count++)
			                            ->name($listItem)
			                            ->doIf(isset($url) && !is_int($url), function (ListItem $li) use ($url, $listItem) {
				                            $li->item(
					                            (new Thing())->setProperty('id', config("evibe.host") . $url)
					                                         ->name($listItem)
				                            );
			                            });
		}

		return Schema::breadcrumbList()
		             ->itemListElement($itemListElements);
	}

	public function schemaProduct($productData = [])
	{
		$product = Schema::product()
		                 ->brand((new Brand())->name("Evibe"))
		                 ->doIf(isset($productData['sku']), function (Product $sku) use ($productData) {
			                 $sku->sku($productData['sku']);
		                 })
		                 ->doIf(isset($productData['category']), function (Product $category) use ($productData) {
			                 $category->category($productData['category']);
		                 })
		                 ->doIf(isset($productData['name']), function (Product $name) use ($productData) {
			                 $name->name($productData['name']);
		                 })
		                 ->doIf(isset($productData['description']), function (Product $description) use ($productData) {
			                 $description->description($productData['description']);
		                 })
		                 ->doIf(isset($productData['image']), function (Product $image) use ($productData) {
			                 $image->image($productData['image']);
		                 })
		                 ->doIf(isset($productData['gtin']), function (Product $gtin) use ($productData) {
			                 $gtin->gtin12($productData['gtin']);
		                 })
		                 ->doIf((isset($productData['avgRating']) && $productData['avgRating']), function (Product $aggregateRating) use ($productData) {
			                 $aggregateRating->aggregateRating((new AggregateRating())
				                                                   ->ratingValue($productData['avgRating'])
				                                                   ->ratingCount($productData['ratingCount']));
		                 })
		                 ->doIf((isset($productData['reviews']) && (count($productData['reviews']) > 0)), function (Product $review) use ($productData) {
			                 $temp = [];
			                 foreach ($productData["reviews"] as $data)
			                 {
				                 $temp[] = (new Review())->author($data["name"])
				                                         ->datePublished(substr($data["created_at"], "0", "10"))
				                                         ->description($data["review"])
				                                         ->name($data["reviewFor"] . " in " . $data["location"] . " Publisher")
				                                         ->reviewRating((new Rating())->bestRating("5")
				                                                                      ->worstRating("0.5")
				                                                                      ->ratingValue($data["rating"]));
			                 }
			                 $review->review($temp);
		                 })
		                 ->doIf((isset($productData['price']) && isset($productData['url'])), function (Product $offers) use ($productData) {
			                 $offers->offers((new Offer())
				                                 ->url($productData['url'])
				                                 ->priceValidUntil(Carbon::now()->addYears(2)->toIso8601String())
				                                 ->priceCurrency("INR")
				                                 ->price($productData['price'])
				                                 ->availability((new ItemAvailability())->name("InStock")));
		                 });

		return $product;
	}

	public function schemaListItem($productData = [], $pageHeader = "", $pageDescription = "")
	{
		$product = Schema::itemList()
		                 ->name($pageHeader)
		                 ->description($pageDescription)
		                 ->doIf((count($productData) > 0), function (ItemList $item) use ($productData) {
			                 $temp = [];
			                 $count = 1;
			                 foreach ($productData as $data)
			                 {
				                 $url = isset($data["url"]) ? $data["url"] : (isset($data["link"]) ? $data["link"] : "");
				                 $name = isset($data["name"]) ? $data["name"] : (isset($data["title"]) ? $data["title"] : "");
				                 $image = isset($data["image"]) ? $data["image"] : (isset($data["img"]) ? $data["img"] : "");

				                 $temp[] = (new ListItem())->position($count)
				                                           ->url($url)
				                                           ->name($name)
				                                           ->image($image);
				                 $count++;
			                 }
			                 $item->itemListElement($temp);
		                 });

		return $product;
	}

	public function schemaService($serviceData = [])
	{
		$service = Schema::service()
		                 ->areaServed(getCityName())
		                 ->doIf(isset($serviceData['name']), function (SchemaService $name) use ($serviceData) {
			                 $name->name($serviceData['name']);
		                 })
		                 ->doIf(isset($serviceData['name']), function (SchemaService $type) use ($serviceData) {
			                 $type->serviceType($serviceData['name']);
		                 })
		                 ->doIf(isset($serviceData['image']), function (SchemaService $image) use ($serviceData) {
			                 $image->image($serviceData['image']);
		                 })
		                 ->doIf(isset($serviceData['price']) && isset($serviceData['price']), function (SchemaService $offers) use ($serviceData) {
			                 $offers->offers((new Offer())->priceCurrency("INR")->price($serviceData['price']));
		                 });

		return $service;
	}

	public function schemaLocalBusiness($lbData = [])
	{
		$localBusiness = Schema::localBusiness()
		                       ->name($lbData['name'])
		                       ->telephone(config('evibe.contact.company.phone'))
		                       ->image(config('evibe.gallery.host') . config('evibe.gallery.logo_path'))
		                       ->doIf(isset($lbData['currenciesAccepted']), function (LocalBusiness $lb) use ($lbData) {
			                       $lb->currenciesAccepted("INR");
		                       })
		                       ->doIf(isset($lbData['priceRange']), function (LocalBusiness $lb) use ($lbData) {
			                       $lb->priceRange($lbData['priceRange']);
		                       })
		                       ->doIf(isset($lbData['address']), function (LocalBusiness $lb) use ($lbData) {
			                       $lb->address($lbData['address']);
		                       })
		                       ->doIf(isset($lbData['description']), function (LocalBusiness $lb) use ($lbData) {
			                       $lb->description($lbData['description']);
		                       })
		                       ->doIf((isset($lbData['avgRating']) && $lbData['avgRating']), function (LocalBusiness $aggregateRating) use ($lbData) {
			                       $aggregateRating->aggregateRating((new AggregateRating())
				                                                         ->ratingValue($lbData['avgRating'])
				                                                         ->ratingCount($lbData['ratingCount']));
		                       });

		return $localBusiness;
	}

	/**
	 * @author Anji <anji@evibe.in>
	 * @since  5 June 2017
	 */
	public function logError($error)
	{
		SiteErrorLog::create([
			                     "code"    => "EVB-M-501",
			                     "details" => $error
		                     ]);
	}

	private function optimizeImages($basePath, $imageName)
	{
		$defaultWidth = 640;
		$defaultHeight = null;
		$types = [
			"profile" => ['width' => 640],
			"results" => ['width' => 394],
			"thumbs"  => ['width' => 80]
		];
		$newImagePath = $basePath . $imageName;
		$img = Image::make($newImagePath);

		foreach ($types as $type => $values)
		{
			$optImgPath = $basePath . $type . "/";

			if (!file_exists($optImgPath))
			{
				mkdir($optImgPath, 0777, true);
			}

			$width = array_key_exists("width", $values) ? $values["width"] : $defaultWidth;
			$height = array_key_exists("height", $values) ? $values["height"] : $defaultHeight;

			$img->resize($width, $height, function ($constraint) {
				$constraint->aspectRatio();
				$constraint->upsize();
			});
			try
			{
				$img->save($optImgPath . $imageName);
			} catch (\Exception $e)
			{
				Log::error("Exception with saving optimised image: " . $optImgPath . $imageName);
			}

		}
	}

	public function issueAccessToken($user, $client)
	{
		$accessToken = new AccessToken;
		$accessToken->access_token = $this->getNewAccessToken();
		$accessToken->auth_client_id = $client->auth_client_id;
		$accessToken->user_id = $user->id;

		$accessToken->save();

		return $accessToken->access_token;
	}

	private function getNewAccessToken($len = 40)
	{
		$stripped = '';
		do
		{
			$bytes = openssl_random_pseudo_bytes($len, $strong);

			// We want to stop execution if the key fails because, well, that is bad.
			if ($bytes === false || $strong === false)
			{
				// @codeCoverageIgnoreStart
				throw new \Exception('Error Generating Key');
				// @codeCoverageIgnoreEnd
			}
			$stripped .= str_replace(['/', '+', '='], '', base64_encode($bytes));
		} while (strlen($stripped) < $len);

		return substr($stripped, 0, $len);
	}

	private function limitText($text, $limit)
	{
		if (str_word_count($text, 0) > $limit)
		{
			$words = str_word_count($text, 2);
			$pos = array_keys($words);
			$text = substr($text, 0, $pos[$limit]) . '...';
		}

		return $text;
	}

	/**
	 * Return SEO values based on the current and default values
	 * We are not considering $replaces in this version.
	 */
	private function getSeoValues($current, $defaults, $replaces = [])
	{
		$values = $defaults;
		$keys = ['pt', 'pd', 'ph', 'kw'];
		$base = config('seo.defaults.base');

		$replaces = !is_array($replaces) ? [] : $replaces;
		$replaces = array_merge($replaces, [
			"{city}" => getCityName()
		]);

		// handle missing keys, values in config
		foreach ($keys as $key)
		{
			$seoKeyText = isset($current[$key]) && $current[$key] ? $current[$key] : (isset($defaults[$key]) ? $defaults[$key] : $base[$key]);
			$seoKeyText = str_replace(array_keys($replaces), array_values($replaces), $seoKeyText);
			$values[$key] = $seoKeyText;
		}

		return $values;
	}

	/*
	 * Collections
	 *
	 * get the collection for the specific cityId & eventId
	 */
	public function getCollections($cityId, $eventId = false, $count = false, $homePage = false)
	{
		$validOccasionIds = [config("evibe.occasion.kids_birthdays.id"), config("evibe.occasion.surprises.id"), config("evibe.occasion.house-warming.id")];
		$collectionArr = [];
		$collections = Collection::where('is_live', 1)
		                         ->where('city_id', $cityId)
		                         ->whereIn("event_id", $validOccasionIds)
		                         ->orderBy(DB::raw('ISNULL(priority), priority'), 'ASC');

		if ($eventId)
		{
			$collections->where('event_id', $eventId);
		}

		if (is_int($count) && $count)
		{
			$collections = $collections->take($count)->get();
		}
		else
		{
			$collections = $collections->get();
		}

		$i = 0;
		if ($homePage)
		{
			foreach ($collections as $collection)
			{
				$collectionArr[$i] = $collection->toArray();
				$collectionArr[$i]['profileImg'] = $collection->getHomePageProfileImg();
				$collectionArr[$i]['coverImg'] = $collection->getListPageCoverImg();
				$i++;
			}
		}
		else
		{
			foreach ($collections as $collection)
			{
				$collectionArr[$i] = $collection->toArray();
				$collectionArr[$i]['profileImg'] = $collection->getListPageProfileImg();
				$collectionArr[$i]['coverImg'] = $collection->getListPageCoverImg();
				$i++;
			}
		}

		return $collectionArr;
	}

	public function getShortlistFromUserId($userId)
	{
		// shortlist for the user with userId
		// party_date is null or party_date >= current_date (Carbon::now())
		$startOfTheDay = Carbon::createFromTimestamp(time())->startOfDay()->toDayDateTimeString();
		$shortlist = Shortlist::where('user_id', $userId)
		                      ->where(function ($query) use ($startOfTheDay) {
			                      $query->whereNull('party_date')
			                            ->orWhere('party_date', '>=', strtotime($startOfTheDay));
		                      })
		                      ->first();

		return $shortlist ? $shortlist : false;
	}

	//For getting the product data
	public function getProductData($mapId, $mapTypeId)
	{
		$option = null;
		switch ($mapTypeId)
		{
			case config('evibe.ticket.type.decor'):
				$option = Decor::find($mapId);
				break;
		}

		return $option;
	}

	/**
	 * Used in payment related controllers
	 */
	public function getCheckoutFieldName($id)
	{
		$checkoutField = CheckoutField::find($id);
		$name = "";

		if ($checkoutField && $checkoutField->name)
		{
			$name = trim($checkoutField->name);
		}

		return $name;
	}

	public function getCustomFieldValuesByCat($catId, $eventId, $packageId)
	{
		$values = [];

		// get all fields related to `$catId` category
		$fields = TypePackageField::where('type_cat_id', $catId)
		                          ->where('occasion_id', $eventId)
		                          ->orderBy(DB::raw('ISNULL(type_package_field.priority), type_package_field.priority'), 'ASC')
		                          ->get();

		foreach ($fields as $field)
		{
			// check & get value for this field in this category for this package
			$fieldData = PackageFieldValue::where('package_id', $packageId)
			                              ->where('type_package_field_id', $field->id)
			                              ->first();
			$iconName = $field->icon_name ? $field->icon_name : 'glyphicon glyphicon-star';

			if ($fieldData)
			{
				array_push($values, [
					'id'    => $field->id,
					'key'   => $field->name,
					'name'  => $field->meta_name, // take the user given name
					'value' => $fieldData->value,
					'icon'  => $iconName
				]);
			}
		}

		return $values;
	}

	public function getDefaultCustomKeyByCurrentKey($currentKey)
	{
		$defaultKey = $currentKey;
		$defaultCustomKeys = config('evibe.pkg_fields.keys');
		if ($searchKey = array_search($currentKey, $defaultCustomKeys))
		{
			$defaultKey = $searchKey;
		}

		return $defaultKey;
	}

	public function logSiteError(SiteLogData $data)
	{
		$inputData = [
			"url"        => $data->getUrl(),
			"exception"  => $data->getException(),
			"code"       => $data->getErrorCode(),
			"project_id" => $data->getProjectId(),
			"details"    => $data->getTrace(),
			"created_at" => Carbon::now(),
			"updated_at" => Carbon::now()
		];

		SiteErrorLog::create($inputData);
	}

	public function setCityFromPreviousUrl()
	{
		$url = url()->previous();
		$trimUrl = str_replace(config('evibe.host') . '/', '', $url);
		$arrayUrl = explode('/', $trimUrl);
		$city = null;

		if (is_array($arrayUrl) && $arrayUrl[0])
		{
			$city = City::where('url', $arrayUrl[0])->first();
		}

		if (!$city)
		{
			// get default city & log case
			$city = City::where("is_default", 1)->first();

			$siteLogData = new SiteLogData();
			$siteLogData->setException("CityNotFoundException")
			            ->setTrace("City not found from previous URL: $url")
			            ->setUrl("set-city-from-previous-url");

			$this->logSiteError($siteLogData);
		}

		checkAndSetCitySessions($city);
	}

	public function getAreaForTheLocation($location, $locationDetails, $userPinCode = null)
	{
		$locationArray = explode(', ', $location);
		$areaName = $locationArray[0];
		$cityId = null;
		$pinCode = null;
		$areaId = null;
		$alertPinCodeArea = false;

		if ($locationDetails)
		{
			$locationArray = json_decode($locationDetails);
			foreach ($locationArray as $locationComponent)
			{
				if ($locationComponent->types[0] == 'locality')
				{
					$cityId = config("evibe.city.$locationComponent->long_name");
				}
				if ($locationComponent->types[0] == 'postal_code')
				{
					$pinCode = $locationComponent->long_name;
				}
			}

			if ($cityId && $areaName)
			{
				$area = Area::where('city_id', $cityId)
				            ->where('name', $areaName)
				            ->orderBy('updated_at', 'DESC')
				            ->first();

				if ($area)
				{
					$areaId = $area->id;
					if (!$area->zip_code && $pinCode)
					{
						$area->zip_code = $pinCode;
						$area->save();
					}
				}
				else
				{
					$areaUrl = str_replace(" ", "-", strtolower($areaName));
					$newArea = Area::create([
						                        'name'       => $areaName,
						                        'url'        => $areaUrl,
						                        'zip_code'   => $pinCode,
						                        'city_id'    => $cityId,
						                        'created_at' => Carbon::now(),
						                        'updated_at' => Carbon::now(),
					                        ]);
					$areaId = $newArea->id;
				}
			}
		}

		if (!$areaId && $areaName)
		{
			$area = Area::where('name', $areaName)
			            ->first();

			if ($area)
			{
				$areaId = $area->id;
				$cityId = $area->city_id;
			}
		}

		if (!$areaId && $userPinCode)
		{
			$area = Area::where('zip_code', $userPinCode)
			            ->first();

			if ($area)
			{
				$areaId = $area->id;
				$areaName = $area->name;
				$cityId = $area->city_id;

				// alert team
				$alertPinCodeArea = true;
			}
		}

		return $data = [
			'areaId'           => $areaId,
			'areaName'         => $areaName,
			'cityId'           => $cityId,
			'alertPinCodeArea' => $alertPinCodeArea
		];
	}

	public function logAjaxData(LogAjax $data)
	{
		$inputData = [
			"url"        => $data->getUrl(),
			"payload"    => $data->getPayload(),
			"project_id" => $data->getProjectId(),
			"created_at" => Carbon::now(),
			"updated_at" => Carbon::now()
		];

		AjaxLog::create($inputData);
	}

	public function updateTicketAction($data)
	{
		$ticket = $data['ticket'];
		if ($ticket)
		{
			$statusId = (isset($data['statusId']) && !is_null($data['statusId'])) ? $data['statusId'] : ($ticket->status_id ? $ticket->status_id : config("evibe.ticket.status.initiated"));
			$comments = isset($data['comments']) && !is_null($data['comments']) ? $data['comments'] : "Normal ticket created by customer";

			TicketUpdate::create([
				                     'ticket_id'   => $ticket->id,
				                     'status_id'   => $statusId,
				                     'type_update' => config('evibe.ticket.type_update.auto'),
				                     'comments'    => $comments,
				                     'status_at'   => time(),
				                     'created_at'  => date('Y-m-d H:i:s'),
				                     'updated_at'  => date('Y-m-d H:i:s')
			                     ]);
		}
	}

	public function renderDataToViewFiles($path, $data)
	{
		$data['typeBrowser'] = $this->agent->browser();

		if ($this->agent->isMobile() && !($this->agent->isTablet()))
		{
			$path = $path . '/m';
			$data['typeDevice'] = config('evibe.user-device.type.1');
		}
		else
		{
			$path = $path . '/d';
			$data['typeDevice'] = config('evibe.user-device.type.2');
		}

		return view($path, ['data' => $data]);
	}

	private function getFilteredOptions($allOptions, $typeTicketId, $minPrice, $maxPrice)
	{
		$options = null;
		switch ($typeTicketId)
		{
			case config("evibe.ticket.type.package"):
			case config("evibe.ticket.type.resorts"):
			case config("evibe.ticket.type.villas"):
			case config("evibe.ticket.type.lounges"):
			case config("evibe.ticket.type.surprises"):
			case config("evibe.ticket.type.food"):
			case config("evibe.ticket.type.priests"):
			case config("evibe.ticket.type.tents"):
				$options = $allOptions->where('price', '>=', $minPrice)
				                      ->where('price', '<=', $maxPrice);
				break;
			case config("evibe.ticket.type.decor"):
				$options = $allOptions->where('min_price', '>=', $minPrice)
				                      ->where('min_price', '<=', $maxPrice);
				break;

			case config("evibe.ticket.type.entertainment"):
				$options = $allOptions->where('min_price', '>=', $minPrice)
				                      ->where('min_price', '<=', $maxPrice);
				break;
		}

		return $options;
	}

	public function getPriceCategoryFilters($data)
	{
		$min = $data['min'];
		$max = $data['max'];
		$mapTypeId = $data['mapTypeId'];
		$typeTicketId = $data['typeTicketId'];
		$eventId = $data['eventId'];
		$cityId = $data['cityId'];

		$priceCats = config("price-filter.defaults");

		if (config("price-filter.$eventId.defaults"))
		{
			$priceCats = config("price-filter.$eventId.defaults");
		}

		if (config("price-filter.$eventId.$typeTicketId"))
		{
			$priceCats = config("price-filter.$eventId.$typeTicketId");
		}

		foreach ($priceCats as $key => $value)
		{
			if ($key == (count($priceCats) - 1))
			{
				break;
			}
			elseif ($value >= $priceCats[$key + 1])
			{
				Log::error("Invalid price range has been given in config");

				return null;
			}
		}

		$allOptions = [];
		$priceData = [];
		switch ($typeTicketId)
		{
			case config("evibe.ticket.type.package"):
			case config("evibe.ticket.type.resorts"):
			case config("evibe.ticket.type.villas"):
			case config("evibe.ticket.type.lounges"):
			case config("evibe.ticket.type.surprises"):
			case config("evibe.ticket.type.food"):
			case config("evibe.ticket.type.priests"):
			case config("evibe.ticket.type.tents"):
				$allOptions = Package::isLive()
				                     ->where('event_id', $eventId)
				                     ->where('city_id', $cityId)
				                     ->where('type_ticket_id', $typeTicketId)
				                     ->get();
				break;
			case config("evibe.ticket.type.decor"):
				$allOptions = Decor::with('provider.city')
				                   ->select('decor.*')
				                   ->isLive()
				                   ->forCity($cityId)
				                   ->forEvent($eventId)
				                   ->get();
				break;

			case config("evibe.ticket.type.entertainment"):
				$allOptions = TypeServices::isLive()
				                          ->forCity($cityId)
				                          ->forEvent($eventId)
				                          ->get();
		}

		// if price filter is not applicable to any specific category
		if (!$allOptions)
		{
			return null;
		}

		foreach ($priceCats as $key => $value)
		{
			if ($key == 0)
			{
				$minPrice = $min;
				$maxPrice = $value;
				$bound = config('evibe.price-category.bounds.low');
			}
			else
			{
				$minPrice = $priceCats[$key - 1];
				$maxPrice = $value;
				$bound = config('evibe.price-category.bounds.normal');
			}

			// @see: if category type is not declared, error will be thrown
			$filteredOptions = $this->getFilteredOptions($allOptions, $typeTicketId, $minPrice, $maxPrice);

			$priceData[$key] = [
				'minPrice' => $minPrice,
				'maxPrice' => $maxPrice,
				'count'    => $filteredOptions->count(),
				'bound'    => $bound
			];
		}

		$catCount = count($priceCats);

		if ($priceData)
		{
			$minPrice = $priceCats[$catCount - 1];
			$maxPrice = $max;

			$priceData[$catCount] = [
				'minPrice' => $minPrice,
				'maxPrice' => $maxPrice,
				'count'    => $this->getFilteredOptions($allOptions, $typeTicketId, $minPrice, $maxPrice)->count(),
				'bound'    => config('evibe.price-category.bounds.high')
			];
		}

		return $priceData;

	}

	public function htmlRemoveSpecialChar($data)
	{
		if (is_array($data))
		{
			array_walk_recursive($data, 'self::htmlSpecialCharsFunction');

			return $data;
		}
		else
		{
			return htmlspecialchars($data, ENT_QUOTES, 'UTF-8');
		}
	}

	public function htmlSpecialCharsFunction(&$value)
	{
		// value is updated by address
		$value = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
	}

	// function that gives parent id from type event table
	public function getParentEventId($eventId)
	{
		$parentEvent = TypeEvent::select('parent_id')->where('id', $eventId)->first();

		if ($parentEvent && $parentEvent->parent_id)
		{
			return $parentEvent->parent_id;
		}

		return $eventId;
	}

	// function that gives event name from id
	public function getEventName($eventId)
	{
		$eventName = '';
		$event = TypeEvent::find($eventId);

		if ($event)
		{
			$eventName = $event->name;
		}

		return $eventName;
	}

	public function sendErrorReport($e, $sendEmail = true)
	{
		$code = 'Error';
		if ($e instanceof HttpException)
		{
			$code = $e->getStatusCode();
		}

		$data = [
			'fullUrl'     => request()->fullUrl(),
			'code'        => $code,
			'previousUrl' => $_SERVER['HTTP_REFERER'],
			'message'     => $e->getMessage(),
			'details'     => $e->getTraceAsString()
		];

		if ($sendEmail)
		{
			$emailData = [
				'code'        => $code,
				'url'         => $data['fullUrl'],
				'method'      => request()->method(),
				'previousUrl' => $data['previousUrl'],
				'message'     => $data['message'],
				'trace'       => $data['details']
			];

			$this->dispatch(new MailGenericErrorToAdminJob($emailData));
		}

		$this->saveError($data);
	}

	protected function sendNormalErrorReport($code, $message, $details)
	{
		$emailData = [
			'code'    => $code ?: "",
			'url'     => request()->fullUrl(),
			'method'  => request()->method(),
			'message' => $message ?: "",
			'trace'   => $details ?: ""
		];

		$this->dispatch(new MailGenericErrorToAdminJob($emailData));
	}

	public function sendNonExceptionErrorReport($data, $sendEmail = true)
	{
		$errorData = [
			'code'       => isset($data['code']) && $data['code'] ? $data['code'] : 0,
			'url'        => isset($data['url']) && $data['url'] ? $data['url'] : config('evibe.host'),
			'method'     => isset($data['method']) && $data['method'] ? $data['method'] : request()->method(),
			'message'    => isset($data['message']) && $data['message'] ? $data['message'] : 'Some error occurred',
			'trace'      => isset($data['trace']) && $data['trace'] ? $data['trace'] : null,
			'project_id' => isset($data['projectId']) && $data['projectId'] ? $data['projectId'] : config('evibe.project_id'),
			'exception'  => isset($data['exception']) && $data['exception'] ? $data['exception'] : 'Unknown exception',
			'details'    => isset($data['details']) && $data['details'] ? $data['details'] : 'No clear details'
		];
		if ($sendEmail)
		{
			$this->dispatch(new MailGenericErrorToAdminJob($errorData));
		}
		$this->saveError($errorData);
	}

	protected function saveError($data)
	{
		SiteErrorLog::create([
			                     'project_id' => config('evibe.project_id'),
			                     'url'        => (isset($data['fullUrl']) && $data['fullUrl']) ? $data['fullUrl'] : ((isset($data['url']) && $data['url']) ? $data['url'] : 'Url not found'),
			                     'exception'  => $data['message'],
			                     'code'       => $data['code'],
			                     'details'    => $data['details']
		                     ]);
	}

	public function makeApiCallWithUrl($options)
	{
		$url = $options['url'];
		$method = $options['method'];
		$accessToken = $options['accessToken'];
		$jsonData = $options['jsonData'];

		try
		{
			$client = new Client();
			$res = $client->request($method, $url, [
				'headers' => [
					'access-token' => $accessToken
				],
				'json'    => $jsonData,
			]);

			$res = $res->getBody();
			$res = \GuzzleHttp\json_decode($res, true);

			return $res;
		} catch (ClientException $e)
		{
			$apiResponse = $e->getResponse()->getBody(true);
			$apiResponse = \GuzzleHttp\json_decode($apiResponse);
			$res['error'] = $apiResponse->errorMessage;

			// @todo: send email to tech team
			SiteErrorLog::create([
				                     "url"     => $url,
				                     "code"    => "API_ERROR",
				                     "details" => $res['error']
			                     ]);

			return false;
		}
	}

	public function getAccessTokenFromUser($user)
	{
		$clientId = config('evibe.api.client_id');
		$accessToken = false;

		$token = AccessToken::where('user_id', $user->id)
		                    ->where('auth_client_id', $clientId)
		                    ->first();

		if (!$token)
		{
			$client = AuthClient::where('auth_client_id', $clientId)->first();
			if ($client)
			{
				$accessToken = $this->issueAccessToken($user, $client);
			}
			else
			{
				Log::error("Invalid client is trying to make the api request");
			}
		}
		else
		{
			$accessToken = $token->access_token;
		}

		return $accessToken;
	}

	public function storeProductListDataInCache($listData, $cacheKey, $cacheCodesKey)
	{
		if (is_array($listData) && isset($listData['cacheProductIds']) && count($listData['cacheProductIds']))
		{
			$codes = $listData['cacheProductIds'];
			Cache::forget($cacheCodesKey);
			$expireTime = Carbon::createFromTimeString(config('evibe.cache.expiry-time'));
			Cache::tags($codes)->put($cacheKey, $listData, $expireTime);
			Cache::put($cacheCodesKey, $codes, $expireTime);
		}
	}

	public function deviceSpecificCacheKey($string)
	{
		$key = $string;
		$parsedString = parse_url($string);

		if (isset($parsedString['scheme']) && isset($parsedString['query']))
		{
			// indicates that given string is a url and has query params
			parse_str($parsedString['query'], $parsedString['query']);
			if (count($parsedString['query']))
			{
				$removableParams = [
					'ref',
					'utm_source',
					'utm_medium',
					'utm_campaign',
					'utm_term',
					'utm_content'
				];

				foreach ($parsedString['query'] as $param => $value)
				{
					if (in_array($param, $removableParams))
					{
						unset($parsedString['query'][$param]);
					}
				}
			}

			$parsedString['query'] = http_build_query($parsedString['query']);

			$key = $parsedString['scheme'];
			$key .= '://';
			$key .= isset($parsedString['host']) ? $parsedString['host'] : '';
			$key .= isset($parsedString['path']) ? $parsedString['path'] : '';
			$key .= '?';
			$key .= $parsedString['query'];
		}

		if ($this->agent->isMobile() && !($this->agent->isTablet()))
		{
			$key .= '-mobile';
		}
		else
		{
			$key .= '-desktop';
		}

		return $key;
	}

	public function uniqueMultidimArray($array, $keys)
	{
		if ($array && count($keys))
		{
			$specialKey = 'arraySortMultiDimKey';

			foreach ($array as $itemKey => $itemArray)
			{
				$specialValue = '';
				foreach ($keys as $key)
				{
					$specialValue .= $array[$itemKey][$key] . '_';
				}
				$array[$itemKey][$specialKey] = $specialValue;
			}

			$tempArray = [];
			$i = 0;
			$keyArray = [];

			foreach ($array as $val)
			{
				if (!in_array($val[$specialKey], $keyArray))
				{
					$keyArray[$i] = $val[$specialKey];
					$tempArray[$i] = $val;
				}
				$i++;
			}

			$array = $tempArray;
			// if needed, can unset values
		}

		return $array;
	}

	public function addProductSpecificAddOns($productData, $eventId, $productTypeId = null, $productId = null, $ticketBooking = null)
	{
		$arrayAddOns = $this->fetchAddOns(getCityId(), $eventId, $productTypeId, $productId, $ticketBooking);

		$productData['addOns'] = $arrayAddOns;

		return $productData;
	}

	public function getTicketApplicableAddOns($cityId, $eventId, $productTypeId = null, $productId = null, $ticketBooking = null)
	{
		$arrayAddOns = $this->fetchAddOns($cityId, $eventId, $productTypeId, $productId, $ticketBooking);

		return $arrayAddOns;
	}

	protected function fetchAddOns($cityId, $eventId, $productTypeId, $productId, $ticketBooking)
	{
		$arrayAddOns = null;

		if ($cityId && $eventId)
		{
			if (in_array($eventId, [
				config('evibe.occasion.first_birthday.id'),
				config('evibe.occasion.birthday_2-5.id'),
				config('evibe.occasion.birthday_6-12.id'),
				config('evibe.occasion.naming_ceremony.id')
			]))
			{
				$eventId = config('evibe.occasion.kids_birthdays.id');
			}

			$addOns = AddOn::forEvent($eventId)
			               ->forCity($cityId)
			               ->isLive();

			if ($productId && $productTypeId)
			{
				// if option specific add-ons exist, fetch them
				// else fetch tag specific add-ons
				// else fetch city and event specific generic add-ons

				$optionAddOnMappingIds = OptionAddOnMapping::where('option_id', $productId)
				                                           ->where('option_type_id', $productTypeId)
				                                           ->pluck('add_on_id')
				                                           ->toArray();

				if (count($optionAddOnMappingIds))
				{
					$addOns = $addOns->whereIn('add_on.id', $optionAddOnMappingIds);
				}
				else
				{
					$tagIds = [];

					switch ($productTypeId)
					{
						// @see: $typeTicketBooking exists only for order processed bookings
						case config('evibe.ticket.type.decor'):
							$tagIds = DecorTag::where('decor_id', $productId)
							                  ->whereNull('deleted_at')
							                  ->pluck('tag_id')
							                  ->toArray();

							break;

						case config('evibe.ticket.type.planner'):
							// based on ttb for a ticket
							// todo: hard-code ttb
							if ($ticketBooking && $ticketBooking->type_ticket_booking_id)
							{
								$typeTicketBookingId = $ticketBooking->type_ticket_booking_id;
								// attach add-ons to a specific tag and use them
								switch ($typeTicketBookingId)
								{
									case config('evibe.type-ticket-booking.theme-decoration'):
										$tagIds = [
											config('evibe.type-tag.simple-theme-decoration'),
											config('evibe.type-tag.custom-theme-decoration')
										];
										break;
									case config('evibe.type-ticket-booking.custom-balloon-decoration'):
										$tagIds = [
											config('evibe.type-tag.custom-balloon-decoration')
										];
										break;
									case config('evibe.type-ticket-booking.cake'):
										$tagIds = [
											config('evibe.type-tag.cake')
										];
										break;
									case config('evibe.type-ticket-booking.decoration-entertainment'):
										$tagIds = [
											config('evibe.type-tag.decoration-entertainment')
										];
										break;
									case config('evibe.type-ticket-booking.entertainment'):
									case config('evibe.type-ticket-booking.organiser'):
									case config('evibe.type-ticket-booking.photographer'):
									case config('evibe.type-ticket-booking.videographer'):
									case config('evibe.type-ticket-booking.special-artist'):
									case config('evibe.type-ticket-booking.add-on-services'):
										$tagIds = [
											config('evibe.type-tag.service-category') // todo: need to verify
										];
										break;
									case config('evibe.type-ticket-booking.catering'):
									case config('evibe.type-ticket-booking.snacks'):
										$tagIds = [
											config('evibe.type-tag.food'), // todo: need to verify
										];
										break;
									default:
										break;
								}
							}
							break;

						case config('evibe.ticket.type.cake'):
							$tagIds = [
								config('evibe.type-tag.cake')
							];
							break;

						case config('evibe.ticket.type.service'):
						case config('evibe.ticket.type.entertainment'):
							$tagIds = [
								config('evibe.type-tag.service-category') // todo: need to verify
							];
							break;

						case config('evibe.ticket.type.package'):
						case config('evibe.ticket.type.surprises'):
						case config('evibe.ticket.type.generic-package'):
							// try for decor + ent
							$tagIds = PackageTag::where('planner_package_id', $productId)
							                    ->whereNull('deleted_at')
							                    ->pluck('tile_tag_id')
							                    ->toArray();
							break;

						case config('evibe.ticket.type.food'):
							// check if it gets included in package or not
							$tagIds = [
								config('evibe.type-tag.food'), // todo: need to verify
							];
							break;

						default:
							$tagIds = [];
							break;
					}

					$taggedAddOnIds = AddOnTags::whereIn('tag_id', $tagIds)
					                           ->whereNull('deleted_at')
					                           ->pluck('add_on_id')
					                           ->toArray();

					if (count($taggedAddOnIds))
					{
						$addOns = $addOns->whereIn('add_on.id', $taggedAddOnIds);
					}
					else
					{
						// get all the applicable occasion related add-ons (generic)
					}
				}
			}

			$addOns = $addOns->get();

			$sortOrderIds = ProductSortOrder::where('product_type_id', config('evibe.ticket.type.add-on'))
			                                ->where('event_id', $eventId)
			                                ->where('city_id', $cityId)
			                                ->orderBy('score', 'DESC')
			                                ->pluck('product_id')
			                                ->toArray();

			$arrayAddOns = $addOns->all();

			usort($arrayAddOns, function ($a, $b) use ($sortOrderIds) {
				return $this->sortItemsByPriority($a, $b, $sortOrderIds);
			});
		}

		return $arrayAddOns;
	}

	//function to calculate the closure date
	public function getClosureDate($ticketCreationDate, $typeLikeliness)
	{
		$ticketCreationDate = $ticketCreationDate->timestamp;
		$closureDate = $ticketCreationDate;
		$today7Pm = Carbon::parse('today 7pm')->timestamp;
		$today12MidNight = Carbon::parse('today 12AM')->timestamp;
		$today12Noon = Carbon::parse('today 12Pm')->timestamp;
		switch (config('evibe.ticket.bookingLikeliness.' . $typeLikeliness))
		{
			case config('evibe.ticket.bookingLikeliness.1'):
				if ($ticketCreationDate > $today7Pm && $ticketCreationDate < $today12MidNight)
				{
					$closureDate = Carbon::createFromTimestamp($ticketCreationDate)->addDay(1)->parse('today 12Pm')->timestamp;
				}
				else
				{
					if ($ticketCreationDate > $today12Noon)
					{
						$closureDate = $today7Pm;
					}

					else
					{
						$closureDate = $today12Noon;
					}
				}
				break;
			case config('evibe.ticket.bookingLikeliness.2'):
				$closureDate = Carbon::createFromTimestamp($closureDate)->parse('12Pm')->addDay(1)->timestamp;
				break;
			case config('evibe.ticket.bookingLikeliness.3'):
				$closureDate = Carbon::createFromTimestamp($closureDate)->parse('12Pm')->addWeek(1)->timestamp;
				break;
			case config('evibe.ticket.bookingLikeliness.4'):
				$closureDate = Carbon::createFromTimestamp($closureDate)->parse('12Pm')->addMonth(1)->timestamp;
				break;
			case config('evibe.ticket.bookingLikeliness.5'):
				$closureDate = null;
				break;
			case config('evibe.ticket.bookingLikeliness.6'):
				$closureDate = Carbon::createFromTimestamp($closureDate)->parse('12Pm')->addDay(3)->timestamp;
				break;
			default:
				$closureDate = null;
				break;
		}

		$dayOfTheWeek = $closureDate ? Carbon::createFromTimestamp($closureDate)->dayOfWeek : $closureDate;
		$closureDate = $dayOfTheWeek == 0 && $closureDate ? Carbon::createFromTimestamp($closureDate)->addDay(1)->timestamp : $closureDate;

		return $closureDate;
	}

	public function makeCurlRequestToGoogleMatrixApi($providerPinCode, $partyPinCode)
	{
		$url = 'https://maps.googleapis.com/maps/api/distancematrix/json';
		$queryString = 'origins=' . $providerPinCode . '&destinations=' . $partyPinCode . '&key=' . config('evibe.google.dist_matrix_key');

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url . '?' . $queryString);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$cResponse = (curl_exec($ch));
		curl_close($ch);
		$res = json_decode($cResponse, true);

		return $res;
	}

	public function calculateCustomerCancellationRefund()
	{
		$cancellationPolicy = CustomerCancellationPolicy::whereNull('deleted_at')->get();

		return $cancellationPolicy;
	}

	// @see: Here valid cake id is either with type_event null or type_event of the current occasion
	public function getDynamicCakeFieldsWithCategory($cakeId)
	{
		$categories = [];
		$cake = Cake::findOrFail($cakeId);
		$cakeFieldValues = CakeFieldValues::where('cake_id', $cakeId)->get();
		$catTypeWeight = config('evibe.cake_cat_value.weight');
		$catTypeEggLess = config('evibe.cake_cat_value.eggless');
		$catTypeFlavour = config('evibe.cake_cat_value.flavour');
		$formattedMinOrder = (float)$cake->min_order . " KG";
		$nameForWeight = TypeCakeCategory::find($catTypeWeight)->name;
		$nameForEgg = "Egg";

		// pushing the first value in array for minimum weight
		$categories[$catTypeWeight]['values'][$formattedMinOrder] = [
			'id'         => -1,
			'name'       => $formattedMinOrder,
			'price'      => $cake->price,
			'priceWorth' => $cake->price_worth,
			'isFixed'    => 1,
			'weight'     => (float)$cake->min_order
		];
		$categories[$catTypeWeight]['name'] = $nameForWeight;

		foreach ($cakeFieldValues as $cakeFieldValue)
		{
			if (!in_array($cakeFieldValue->cake_cat_id, $categories) && $cakeFieldValue->cakeCategory)
			{
				$categories[$cakeFieldValue->cake_cat_id]['name'] = $cakeFieldValue->cakeCategory->name;
			}

			if (array_key_exists($cakeFieldValue->cake_cat_id, $categories) && $cakeFieldValue->catValue)
			{
				$identifier = '';
				if ($catTypeWeight == $cakeFieldValue->cake_cat_id)
				{
					$name = trim(strtoupper($cakeFieldValue->catValue->name));
					$weight = (float)$cakeFieldValue->catValue->value;
				}
				else
				{
					$name = trim($cakeFieldValue->catValue->name);
					$weight = 0;
					if ($cakeFieldValue->is_per_kg)
					{
						$identifier = "(+ Rs. " . $cakeFieldValue->extra_price . " / KG)";
					}
					elseif ($cakeFieldValue->fixed_price)
					{
						$identifier = "(+ Rs. " . $cakeFieldValue->fixed_price . ")";
					}
				}

				// calculate the price.
				if ($cakeFieldValue->is_per_kg == 1)
				{
					$isFixed = 0;
					$price = $cakeFieldValue->extra_price;
					$worth = $cakeFieldValue->extra_worth;
				}
				else
				{
					$isFixed = 1;
					$price = $cakeFieldValue->fixed_price;
					$worth = $cakeFieldValue->fixed_worth;
				}

				if (isset($identifier))
				{
					$identifier = $name . " $identifier";
				}

				$categories[$cakeFieldValue->cake_cat_id]['values'][$name] = [
					'id'         => $cakeFieldValue->id,
					'valueId'    => $cakeFieldValue->cake_cat_value_id,
					'name'       => $name,
					'identifier' => $identifier,
					'price'      => $price,
					'priceWorth' => $worth,
					'isFixed'    => $isFixed,
					'weight'     => $weight
				];
			}
		}

		// adding the all weights more than min weights
		$allWeights = TypeCakeCategoryValue::where('type_cake_cat_id', $catTypeWeight)->where('value', '>', (float)$cake->min_order)->get();

		foreach ($allWeights as $remainingWeight)
		{
			if (!in_array($remainingWeight->id, array_column($categories[$catTypeWeight]['values'], 'valueId')))
			{
				$weightName = trim($remainingWeight->name);
				$categories[$catTypeWeight]['values'][$weightName] = [
					'id'         => 'weight-' . (int)$remainingWeight->id,
					'valueId'    => $remainingWeight->id,
					'name'       => $weightName,
					'price'      => (int)((float)$remainingWeight->value * $cake->price_per_kg),
					'priceWorth' => (float)$remainingWeight->value * ($cake->price_worth / $cake->min_order),
					'isFixed'    => 1,
					'weight'     => (float)$remainingWeight->value
				];
			}
		}

		// check if the type is eggless, if eggless then add type egg also with price 0
		if (array_key_exists($catTypeEggLess, $categories))
		{
			$categories[$catTypeEggLess]['values'][$nameForEgg] = [
				'id'         => "default", // @dummy id
				'name'       => "Egg",
				'price'      => 0,
				'priceWorth' => 0,
				'isFixed'    => 1,
				'weight'     => 0
			];
		}

		// sort type
		if (array_key_exists($catTypeEggLess, $categories))
		{
			$sortedType = collect($categories[$catTypeEggLess]['values'])->sortBy('name')->toArray();
			$categories[$catTypeEggLess]['values'] = $sortedType;
		}

		// sort flavour by price
		if (array_key_exists($catTypeFlavour, $categories))
		{
			$sortedFlavour = collect($categories[$catTypeFlavour]['values'])->sortBy('price')->toArray();
			$categories[$catTypeFlavour]['values'] = $sortedFlavour;
		}

		// sort the weight
		if (array_key_exists($catTypeWeight, $categories))
		{
			$sortedWeight = collect($categories[$catTypeWeight]['values'])->sortBy('name')->toArray();
			$categories[$catTypeWeight]['values'] = $sortedWeight;
		}

		foreach ($categories as $key => $category)
		{
			$categories[$key]['typeFieldId'] = config('evibe.input.option');
		}

		return $categories;
	}

	public function getDynamicDecorFieldsWithCategory($decorId)
	{
		$categories = [];

		array_push($categories, [
			'name'        => 'Party Area Pincode',
			'typeFieldId' => config('evibe.input.number'),
		]);

		return $categories;
	}

	// get all filterable tags for this decor
	// and choose the first one
	// @todo: choose tag with maximum checkout fields
	protected function getTypeBookingIdFromTags($decor)
	{
		$typeBookingId = config('evibe.booking_type.decor');
		$decorTags = $decor->tags;
		$decorTags = $decorTags->filter(function ($tag) {
			return ($tag->is_filter == 1 && $tag->type_booking_id);
		});

		$relevantTag = $decorTags->sortByDesc("created_at")->first();
		if ($relevantTag && $relevantTag->type_booking_id)
		{
			$typeBookingId = $relevantTag->type_booking_id;
		}

		return $typeBookingId;
	}

	public function getEntityDetailsByMapValues($mapId, $mapTypeId)
	{
		$entityData = [];

		switch ($mapTypeId)
		{
			case config('evibe.ticket.type.package'):
				$entityData = $this->getPackageEntityDetails($mapId);
				break;
		}

		return $entityData;
	}

	public function getPackageEntityDetails($packageId)
	{
		$package = Package::with('provider', 'provider.city')->find($packageId);

		if (!$package)
		{
			return [];
		}

		return $package->toArray();
	}

	public function getUpdatedPriceWithTransportationCharges(PriceCheck $priceCheckData)
	{
		$optionId = $priceCheckData->getOptionId();
		$optionTypeId = $priceCheckData->getOptionTypeId();
		$providerPinCode = $priceCheckData->getProviderPinCode();
		$partyPinCode = $priceCheckData->getPartyPinCode();
		$distFree = $priceCheckData->getDistFree();
		$distMax = $priceCheckData->getDistMax();
		$transMax = $priceCheckData->getTransMax();
		$transMin = $priceCheckData->getTransMin();
		$baseBookingAmount = $priceCheckData->getBaseBookingAmount();

		switch ($optionTypeId)
		{
			case config('evibe.ticket.type.decor'):
				$decor = Decor::find($optionId);
				if (!$decor)
				{
					Log::error('Could not find Decor to calculate delivery price');

					return false;
				}
				$distFree = $decor->kms_free;
				$distMax = $decor->kms_max;
				$transMin = $decor->trans_min;
				$transMax = $decor->trans_max;
				$baseBookingAmount = $decor->min_price;
				break;
		}

		$distFreeMeters = $distFree * 1000;
		$distMaxMeters = $distMax * 1000;
		$diffDist = $distMaxMeters - $distFreeMeters;
		$diffTransFare = $transMax - $transMin;
		if ($diffDist > 0)
		{
			$farePerMeter = $diffTransFare / $diffDist;
		}
		else
		{
			$farePerMeter = 0;
			$distFreeMeters = 40 * 1000;
		}

		try
		{
			$res = $this->makeCurlRequestToGoogleMatrixApi($providerPinCode, $partyPinCode);

			if (($res['status'] == 'OK') && ($res['rows'][0]['elements'][0]['status'] === 'OK'))
			{
				$partyDist = $res['rows'][0]['elements'][0]['distance']['value'];
				$extraDist = $partyDist - $distFreeMeters;
				$transCharges = intval($extraDist * $farePerMeter);
				if ($partyDist <= $distFreeMeters) // @see: removed this condition ($partyDist > $distMaxMeters)
				{
					$transCharges = 0;
				}

				$totalBookingAmount = $transCharges + $baseBookingAmount;
				$roundedTokenAmt = $this->getRoundedTokenAmount($totalBookingAmount);

				return $res = [
					//'success'                 => true,
					'partyDist'                => $partyDist,
					'transCharges'             => $transCharges,
					'formatTransCharges'       => $this->formatPrice($transCharges),
					'baseBookingAmount'        => $baseBookingAmount,
					'totalBookingAmount'       => $totalBookingAmount,
					'formatBaseBookingAmount'  => $this->formatPrice($baseBookingAmount),
					'formatTotalBookingAmount' => $this->formatPrice($totalBookingAmount),
					'tokenAmount'              => $roundedTokenAmt,
					'formatTokenAmount'        => $this->formatPrice($roundedTokenAmt),
					'distFreeMeters'           => $distFreeMeters,
					'distMaxMeters'            => $distMaxMeters
				];
			}
		} catch (\Exception $e)
		{
			Log::error($e->getMessage());

			return false;
		}

		return false;
	}

	public function sendErrorReportToTeam($e, $sendEmail = true)
	{
		return EvibeUtil::sendErrorReportToTeam($e, $sendEmail);
	}

	public function sendNonExceptionErrorReportToTeam($data, $sendEmail = true)
	{
		return EvibeUtil::sendNonExceptionErrorReportToTeam($data, $sendEmail);
	}

	public function getCityNameFromId($cityId)
	{
		$city = City::find($cityId);

		return $city ? $city->name : null;
	}

	public function calculateDynamicPrice($price, $date, $cityId = null, $eventId = null, $productTypeId = null, $productId = null)
	{
		$updatedPrice = $price;
		/* disabling dynamic pricing for now */

		/*
		$day = date('N', strtotime($date));
		$date = date('m/d/y', strtotime($date));

		$calcData = config('dynamic-price.defaults');

		if($cityId)
		{
			Log::info("city");
			if(isset(config('dynamic-price.'.$cityId.'.defaults')['percent']))
			{
				Log::info("in city defaults");
				$calcData = config('dynamic-price.'.$cityId.'.defaults');
			}

			if($eventId)
			{
				Log::info("event");
				if(isset(config('dynamic-price.'.$cityId.'.event.'.$eventId.'.defaults')['percent']))
				{
					Log::info("event defaults");
					$calcData = config('dynamic-price.'.$cityId.'.event.'.$eventId.'.defaults');
				}

				if($productTypeId)
				{
					Log::info("category");
					if(config('dynamic-price.'.$cityId.'.event.'.$eventId.'.category.'.$productTypeId.'.defaults'))
					{
						Log::info("category defaults");
						$dynamicPriceArray = config('dynamic-price.'.$cityId.'.event.'.$eventId.'.category.'.$productTypeId.'.defaults');

						if(isset($dynamicPriceArray['defaults']['percent']))
						{
							Log::info("category defaults defaults");
							$calcData = $dynamicPriceArray['defaults'];
						}

						if(isset($dynamicPriceArray['day'][$day]['percent']))
						{
							Log::info("category defaults day");
							$calcData = $dynamicPriceArray['day'][$day];
						}

						if(isset($dynamicPriceArray['date'][$date]['percent']))
						{
							Log::info("category defaults date");
							$calcData = $dynamicPriceArray['date'][$date];
						}
					}

					if($productId)
					{
						Log::info("product");
						if(config('dynamic-price.'.$cityId.'.event.'.$eventId.'.category.'.$productTypeId.'.id.'.$productId))
						{
							Log::info("product defaults");
							$dynamicPriceArray = config('dynamic-price.'.$cityId.'.event.'.$eventId.'.category.'.$productTypeId.'.id.'.$productId);

							if(isset($dynamicPriceArray['defaults']['percent']))
							{
								Log::info("product defaults defaults");
								$calcData = $dynamicPriceArray['defaults'];
							}

							if(isset($dynamicPriceArray['day'][$day]['percent']))
							{
								Log::info("product defaults day");
								$calcData = $dynamicPriceArray['day'][$day];
							}

							if(isset($dynamicPriceArray['date'][$date]['percent']))
							{
								Log::info("product defaults date");
								$calcData = $dynamicPriceArray['date'][$date];
							}
						}
					}
				}
			}

		}

		if(isset($calcData['percent']) && $calcData['percent'] && isset($calcData['type']) && $calcData['type'])
		{
			if($calcData['type'] == 1)
			{
				// addition
				$updatedPrice = (int)round($price + ($price * $calcData['percent'] / 100));
			}
			elseif ($calcData['type'] == 2)
			{
				// subtraction
				$updatedPrice = (int)round($price - ($price * $calcData['percent'] / 100));
			}
		}

		*/

		return $updatedPrice;
	}

	public function calculateDynamicParams($params, $date, $cityId = null, $eventId = null, $productTypeId = null, $productId = null)
	{
		if (!$date || !$cityId || !$eventId)
		{
			return $params;
		}

		$day = date('N', strtotime($date));
		$date = date('m/d/y', strtotime($date));

		$dynamicParams = DynamicProductParams::where('city_id', $cityId)
		                                     ->where('event_id', $eventId);

		if ($productTypeId)
		{
			$dynamicParams = $dynamicParams->where('product_type_id', $productTypeId);
		}

		if ($productId)
		{
			$dynamicParams = $dynamicParams->where('product_id', $productId);
		}

		$dynamicParams = $dynamicParams->get();
		$calcParams = "";
		//dd($dynamicParams);

		// default (without any day or date)
		$defaultParams = $dynamicParams->filter(function ($condition) {
			return (is_null($condition->day_id) && is_null($condition->date));
		});
		if (count($defaultParams))
		{
			$calcParams = $defaultParams->first();
		}

		// any day
		$dayParams = $dynamicParams->where('day_id', $day);
		if (count($dayParams))
		{
			$calcParams = $dayParams->first();
		}

		// any date
		$rangeStartDateString = Carbon::createFromTimestamp(strtotime($date))->startOfDay()->toDateTimeString();
		$rangeEndDateString = Carbon::createFromTimestamp(strtotime($date))->endOfDay()->toDateTimeString();
		$dateParams = $dynamicParams->where('date', '>=', $rangeStartDateString)
		                            ->where('date', '<=', $rangeEndDateString);
		if (count($dateParams))
		{
			$calcParams = $dateParams->first();
		}

		if ($calcParams)
		{
			$price = (isset($params['price']) && $params['price']) ? $params['price'] : null;
			$priceWorth = (isset($params['priceWorth']) && $params['priceWorth']) ? $params['priceWorth'] : null;
			$groupCount = (isset($params['groupCount']) && $params['groupCount']) ? $params['groupCount'] : null;
			$minGuestCount = (isset($params['minGuestCount']) && $params['minGuestCount']) ? $params['minGuestCount'] : null;
			$maxGuestCount = (isset($params['maxGuestCount']) && $params['maxGuestCount']) ? $params['maxGuestCount'] : null;
			$pricePerExtraGuest = (isset($params['pricePerExtraGuest']) && $params['pricePerExtraGuest']) ? $params['pricePerExtraGuest'] : null;

			// percentage change
			if ($calcParams->percent_change && $calcParams->type_change)
			{
				// type_change = 1; addition
				// type_change = 2; subtraction
				$percent = $calcParams->percent_change;
				if ($calcParams->type_change == 1)
				{
					$price = (int)round($price + ($price * $percent / 100));
					$priceWorth = (int)round($priceWorth + ($priceWorth * $percent / 100));
					$pricePerExtraGuest = (int)round($pricePerExtraGuest + ($pricePerExtraGuest * $percent / 100));
				}
				else
				{
					$price = (int)round($price - ($price * $percent / 100));
					$priceWorth = (int)round($priceWorth - ($priceWorth * $percent / 100));
					$pricePerExtraGuest = (int)round($pricePerExtraGuest - ($pricePerExtraGuest * $percent / 100));
				}
			}

			// absolute change
			$price = $calcParams->price ?: $price;
			$priceWorth = $calcParams->price_worth ?: $priceWorth;
			$pricePerExtraGuest = $calcParams->price_per_extra_guest ?: $pricePerExtraGuest;

			// guest count changes
			$groupCount = $calcParams->min_guest_count ?: $groupCount;
			$minGuestCount = $calcParams->min_guest_count ?: $minGuestCount;
			$maxGuestCount = $calcParams->max_guest_count ?: $maxGuestCount;

			$params = [
				'price'              => $price,
				'priceWorth'         => $priceWorth,
				'groupCount'         => $groupCount,
				'minGuestCount'      => $minGuestCount,
				'maxGuestCount'      => $maxGuestCount,
				'pricePerExtraGuest' => $pricePerExtraGuest,
			];
		}

		return $params;
	}

	public function fetchProductUrl($cityId = null, $eventId = null, $productTypeId = null, $productId = null, $productUrl = null)
	{
		$city = City::find($cityId);
		$cityUrl = $city ? $city->url : "";

		$event = TypeEvent::find($eventId);
		$eventUrl = $event ? $event->url : "";

		$fullUrl = "";

		if ($productUrl)
		{
			$fullUrl = config('evibe.host') . '/' . $cityUrl . '/' . $eventUrl . '/';

			switch ($eventId)
			{
				case config('evibe.occasion.kids_birthdays.id'):
					switch ($productTypeId)
					{
						case config('evibe.ticket.type.decor'):
							$fullUrl .= config('evibe.occasion.kids_birthdays.decors.results');
							break;
						case config('evibe.ticket.type.cake'):
							$fullUrl .= config('evibe.occasion.kids_birthdays.cakes.results_url');
							break;
						case config('evibe.ticket.type.package'):
						case config('evibe.ticket.type.generic-package'):
							$fullUrl .= config('evibe.occasion.kids_birthdays.packages.results_url');
							break;
						case config('evibe.ticket.type.food'):
							$fullUrl .= config('evibe.occasion.kids_birthdays.food.results_url');
							break;
						case config('evibe.ticket.type.tents'):
							$fullUrl .= config('evibe.occasion.kids_birthdays.tents.results_url');
							break;
						case config('evibe.ticket.type.entertainment'):
							$fullUrl .= config('evibe.occasion.kids_birthdays.ent.results_url');
							break;
					}

					break;
				case config('evibe.occasion.bachelor.id'):
					switch ($productTypeId)
					{
						case config('evibe.ticket.type.decor'):
							$fullUrl .= config('evibe.occasion.bachelor.decors.results_url');
							break;
						case config('evibe.ticket.type.cake'):
							$fullUrl .= config('evibe.occasion.bachelor.cakes.results_url');
							break;
						case config('evibe.ticket.type.villas'):
							$fullUrl .= config('evibe.occasion.bachelor.villa.results_url');
							break;
					}
					break;
				case config('evibe.occasion.house-warming.id'):
					switch ($productTypeId)
					{
						case config('evibe.ticket.type.decor'):
							$fullUrl .= config('evibe.occasion.house-warming.decors.results_url');
							break;
						case config('evibe.ticket.type.cake'):
							$fullUrl .= config('evibe.occasion.house-warming.cakes.results_url');
							break;
						case config('evibe.ticket.type.food'):
							$fullUrl .= config('evibe.occasion.house-warming.food.results_url');
							break;
						case config('evibe.ticket.type.tents'):
							$fullUrl .= config('evibe.occasion.house-warming.tents.results_url');
							break;
						case config('evibe.ticket.type.priests'):
							$fullUrl .= config('evibe.occasion.house-warming.priest.results_url');
							break;
						case config('evibe.ticket.type.entertainment'):
							$fullUrl .= config('evibe.occasion.house-warming.ent.results_url');
							break;
					}
					break;
				case config('evibe.occasion.pre-post.id'):
					switch ($productTypeId)
					{
						case config('evibe.ticket.type.decor'):
							$fullUrl .= config('evibe.occasion.pre-post.decors.results');
							break;
						case config('evibe.ticket.type.cake'):
							$fullUrl .= config('evibe.occasion.pre-post.cakes.results_url');
							break;
						case config('evibe.ticket.type.entertainment'):
							$fullUrl .= config('evibe.occasion.pre-post.ent.results_url');
							break;
					}
					break;
				case config('evibe.occasion.surprises.id'):
				{
					switch ($productTypeId)
					{
						case config('evibe.ticket.type.surprises'):
							$fullUrl .= config('evibe.occasion.surprises.package.results_url');
							break;
					}

					if ($productId)
					{
						// if the package is CLD, try to form specific URL to avoid redirect
					}
				}
			}

			$fullUrl .= '/' . $productUrl;
		}
		else
		{
			// fetch product url from productTypeId and productId
		}

		return $fullUrl;
	}

	protected function getPartnerQuickLoginParams($partnerUserId)
	{
		$uniqueNumber = 10022014;

		return [
			'iId'    => $partnerUserId + $uniqueNumber,
			'eToken' => Hash::make($partnerUserId)
		];
	}

	public function googleCaptchaCheck()
	{
		$response = request()->input('g-recaptcha-response');
		$remoteIp = $_SERVER['REMOTE_ADDR'];
		$secret = config('evibe.google.re_cap_secret');

		$reCaptcha = new ReCaptcha($secret);
		$resp = $reCaptcha->verify($response, $remoteIp);

		if ($resp->isSuccess())
		{
			return true;
		}

		return false;
	}

	public function showStoreRedirectPage()
	{
		$redirect = request()->input('redirect');

		if (!$redirect)
		{
			$redirect = config('evibe.store_host');
		}

		return view("app.store-redirect", ['redirect' => $redirect]);
	}
}