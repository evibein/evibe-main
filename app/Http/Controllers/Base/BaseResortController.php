<?php

namespace App\Http\Controllers\Base;

class BaseResortController extends BasePackageController
{
	public function __construct()
	{
		parent::__construct();

		$this->setPageId(config('evibe.ticket.type.resorts'));
	}

	public function getResortListData($cityUrl, $profileUrl = null)
	{
		$profileUrl = !is_null($profileUrl) ? $profileUrl : config('evibe.occasion.bachelor.resort.profile_url');
		$resortListData = $this->getListData($cityUrl);
		if (is_array($resortListData))
		{
			$host = config('evibe.host');
			$resortListData['profileBaseUrl'] = "$host/$cityUrl/" . $this->getOccasionUrl() . "/$profileUrl/";
		}

		return $resortListData;
	}

	public function getResortProfileData($cityUrl, $resortUrl, $hashTag = "#Resorts")
	{
		//$customControllerParams = [
		//	'occasion'   => 'Bachelors',
		//	'controller' => 'BachelorsResortController',
		//	'method'     => 'showCustomResortProfile'
		//];

		return $this->getProfileData($cityUrl, $resortUrl, $hashTag, "resort");
	}
}