<?php

namespace App\Http\Controllers\Base;

class BaseVenueController extends BaseResultsController
{
	public function __construct()
	{
		parent::__construct();
		$this->setPageId(config('evibe.ticket.type.halls'));
	}

	protected function getVenueFilterCategoryCounts($halls, $types, $isParentType = false)
	{
		$counts = [];
		$total = 0;
		$haystack = [];

		foreach ($types as $type)
		{
			$counts[$type->id] = 0;
			array_push($haystack, $type->id);
		}

		foreach ($halls as $key => $hall)
		{
			$typeId = $isParentType ? $hall['venue']['type_id'] : $hall['type_id'];
			// get counts of valid filters only (is_filter = 1)
			if (array_key_exists($typeId, $counts))
			{
				$counts[$typeId] = $counts[$typeId] + 1;
				$total += 1;
			}
		}

		return $counts;
	}

	protected function getOrderedByCountsFilters($orderedCounts, $allItemTypes)
	{
		$orderedItems = [];
		foreach ($orderedCounts as $key => $countValue)
		{
			$currentType = $allItemTypes->where('id', $key)->first();
			$orderedItems[$currentType->id] = $currentType;
		}

		return $orderedItems;
	}
}