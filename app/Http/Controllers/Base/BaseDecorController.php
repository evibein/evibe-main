<?php

namespace App\Http\Controllers\Base;

use App\Models\Decor\Decor;
use App\Models\Decor\DecorGallery;
use App\Models\Decor\DecorTag;
use App\Models\Types\TypeTag;
use App\Models\Util\Area;
use App\Models\Util\Country;
use Illuminate\Support\Facades\Cache;

class BaseDecorController extends BaseResultsController
{
	public function __construct()
	{
		parent::__construct();

		$this->setPageId(config('evibe.ticket.type.decor'));
	}

	public function getDecorsListProductData($cityUrl, $categoryUrl = "", $fetchWithoutView = false, $collectionId = null)
	{
		// validate url params & redirect to results if not valid
		$urlParams = [['key' => 'city', 'value' => $cityUrl]];
		$validUrlObj = $this->validateUrlParams($urlParams);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$cacheKey = $this->deviceSpecificCacheKey(request()->fullUrl());
		$cacheKey = base64_encode($cacheKey);
		if (Cache::has($cacheKey) && !($fetchWithoutView))
		{
			$listData = Cache::get($cacheKey);
		}
		else
		{
			$listData = $this->getDecorsListProductDataFromDB($categoryUrl, $fetchWithoutView, $collectionId);
			Cache::put($cacheKey, $listData, config('evibe.cache.refresh-time'));
		}

		return $listData;
	}

	public function getDecorsListProductDataFromDB($categoryUrl = "", $fetchWithoutView = false, $collectionId = null)
	{
		$eventId = $this->getOccasionId();
		$pageId = $this->getPageId();
		$clearFilter = false;
		$userFilterId = null;
		$selectedCategoryTags = [];

		$decors = Decor::select('decor.min_price', 'decor.max_price', 'decor.name', 'decor.code', 'decor.id', 'decor.provider_id', 'decor.url', 'decor.range_info', 'decor.worth')
		               ->isLive()
		               ->forCity(getCityId())
		               ->forEvent($eventId)
		               ->where(function ($query) {
			               $query->where("is_variation_parent", "!=", "1")
			                     ->orWhereNull("is_variation_parent");
		               });

		// Category Filter
		$categoryTags = TypeTag::select('id', 'seo_title', 'seo_desc', 'identifier', 'url', 'name', 'parent_id')
		                       ->forEvent($eventId)
		                       ->filterable();

		$relevantParentTags = [
			'Gender'     => config("evibe.occasion.kids_birthdays.decors.gender_cat_id"),
			'Venue Type' => config("evibe.occasion.kids_birthdays.decors.place_cat_id"),
			'Category'   => config("evibe.occasion.kids_birthdays.decors.category_cat_id"),
			'Theme'      => config("evibe.occasion.kids_birthdays.decors.theme_cat_id")
		];

		if ($eventId == config('evibe.occasion.kids_birthdays.id'))
		{
			$categoryTags->where(function ($query) use ($relevantParentTags) {
				$query->whereNull('parent_id')
				      ->orWhereIn('id', array_values($relevantParentTags))
				      ->orWhereIn('parent_id', array_values($relevantParentTags));
			});
		}
		else
		{
			$categoryTags->whereNull('parent_id');
		}

		$categoryTags = $categoryTags->get()->keyBy('id');

		if ($categoryUrl || $collectionId)
		{
			$decorsData = $this->filterDataForTags($decors, $categoryUrl, $categoryTags, $collectionId);
			$decors = $decorsData['decors'];
			$selectedCategoryTags = $selectedCategoryTags + $decorsData['tags'];
			$clearFilter = true;
		}

		// Gender Search
		$gender = request("gender");
		if ($gender && $gender != "")
		{
			$decorsData = $this->filterDataForTags($decors, $gender, $categoryTags);
			$decors = $decorsData['decors'];
			$clearFilter = true;
		}

		// Gender Search
		$theme = request("theme");
		if ($theme && $theme != "")
		{
			$decorsData = $this->filterDataForTags($decors, $theme, $categoryTags);
			$decors = $decorsData['decors'];
			$clearFilter = true;
		}

		// Gender Search
		$placeType = request("venue-type");
		if ($placeType && $placeType != "")
		{
			$decorsData = $this->filterDataForTags($decors, $placeType, $categoryTags);
			$decors = $decorsData['decors'];
			$clearFilter = true;
		}

		// If search exists
		$userSearch = "";
		if (request()->has('search') && trim(request()->get('search')))
		{
			$clearFilter = true;
			$userSearch = trim(request()->get('search'));
			$searchWords = explode(' ', $userSearch);

			$decors->where(function ($searchQuery) use ($searchWords) {
				foreach ($searchWords as $word)
				{
					$searchQuery->orWhere('decor.name', 'LIKE', "%$word%")
					            ->orWhere('decor.code', 'LIKE', "%$word%");
				}
			});
		}

		// getting max & min prices for decors
		$priceFilterApplied = false;
		$minPrices = $decors->pluck('min_price');
		$maxPrices = $decors->pluck('max_price');
		$calMinPrice = $minPrices->min();
		$calMaxPrice = ($maxPrices->max() > $minPrices->max()) ? $maxPrices->max() : $minPrices->max();

		// Filter by price
		$priceMin = request('price_min') ?: 0;
		$priceMax = request('price_max') ?: 0;
		if ($priceMin || $priceMax)
		{
			$priceFilterApplied = true;
		}
		else
		{
			$priceMin = $priceMin ? $priceMin : $calMinPrice;
			$priceMax = ($priceMax && $priceMax > $priceMin) ? $priceMax : $calMaxPrice;
		}

		if ($priceMin > $priceMax)
		{
			$priceTemp = $priceMax;
			$priceMax = $priceMin;
			$priceMin = $priceTemp;
		}

		// user has changed min / max price
		if ($calMinPrice != $priceMin || $calMaxPrice != $priceMax)
		{
			$clearFilter = true;
			// $hidePriceCategories = true;
			$decors->where(function ($query1) use ($priceMin, $priceMax) {
				$query1->where(function ($query11) use ($priceMin, $priceMax) {
					$query11->where('max_price', '=', 0)
					        ->where('min_price', '>=', $priceMin)
					        ->where('min_price', '<=', $priceMax);
				})->orWhere(function ($query12) use ($priceMin, $priceMax) {
					$query12->where('max_price', '!=', 0)
					        ->where('min_price', '<=', $priceMin)
					        ->where('max_price', '>=', $priceMin)
					        ->where('max_price', '<=', $priceMax);
				})->orWhere(function ($query13) use ($priceMin, $priceMax) {
					$query13->where('max_price', '!=', 0)
					        ->where('min_price', '>=', $priceMin)
					        ->where('min_price', '<=', $priceMax)
					        ->where('max_price', '>=', $priceMax);
				})->orWhere(function ($query14) use ($priceMin, $priceMax) {
					$query14->where('max_price', '!=', 0)
					        ->where('min_price', '>=', $priceMin)
					        ->where('max_price', '<=', $priceMax);
				});
			});
		}

		// SEO
		$defaultKey = "defaults.city.$eventId.$pageId.list";
		$seo = $this->getSeoText([getCityId(), $eventId, $pageId, "list"], $defaultKey);
		$itemsListKeys = [
			"Evibe.in",
			getCityName(),
			getTextFromSlug($this->getOccasionUrl()),
			getTextFromSlug($this->getPageName())
		];

		// given user filter is valid -> filter results
		if (count($selectedCategoryTags) > 0)
		{
			$filterTagName = "";
			$replaces = [
				'{tagTitle}' => "",
				'{tagDesc}'  => "",
				'{tagName}'  => ""
			];

			$count = 0;
			foreach ($selectedCategoryTags as $key => $selectedCategoryTag)
			{
				if (isset($categoryTags[$key]))
				{
					$categoryTag = $categoryTags[$key];
					$filterTagName = $categoryTag->identifier ? $filterTagName . ($count == 0 ? "" : " , ") . $categoryTag->identifier : $filterTagName;

					$replaces = [
						'{tagTitle}' => $categoryTag->seo_title ? $replaces['{tagTitle}'] . ($count == 0 ? "" : " , ") . $categoryTag->seo_title : $replaces['{tagTitle}'],
						'{tagDesc}'  => ($categoryTag->seo_desc ? $replaces['{tagDesc}'] . ($count == 0 ? "" : " , ") . $categoryTag->seo_desc : $replaces['{tagDesc}']),
						'{tagName}'  => $categoryTag->identifier ? $replaces['{tagName}'] . ($count == 0 ? "" : " , ") . $categoryTag->identifier : $replaces['{tagName}']
					];

					$count++;
				}
			}
			$defaultKey = "defaults.city.$eventId.$pageId.filter";
			$seo = $this->getSeoText([getCityId(), $eventId, $pageId, "filter"], $defaultKey, $replaces);
			$seo['pd'] = $seo['pd'] . " Rs. " . $this->formatPrice($calMinPrice) . " onwards.";
			array_push($itemsListKeys, $filterTagName);

			$clearFilter = true;
			$userFilterId = $key;
		}

		$pageTitle = $seo['pt'];
		$pageDescription = $seo['pd'];
		$pageHeader = $seo['ph'];
		$keyWords = $seo['kw'];
		$schema = [$this->schemaBreadcrumbList($itemsListKeys)];

		$perPage = config('evibe.paginate-options.generic-desktop', 51);
		if ($this->agent->isMobile() && !($this->agent->isTablet()))
		{
			$perPage = config('evibe.paginate-options.generic-mobile', 50);
		}
		if ($fetchWithoutView)
		{
			$perPage = 8;
		}

		$totalDecorCount = $decors->count(); // total count for pagination

		// sort results
		// if default (popularity), sort by priority
		// else sort by user input (ex: price, new-arrivals)
		$sortParams = $this->getSortParams('decor', ['price' => 'decor.min_price']);
		if (!$sortParams['isDefault'])
		{
			// groupBy is fix for distinct
			$decors = $decors->groupBy('decor.id')
			                 ->orderBy($sortParams['entity'], $sortParams['order']);

			// fetch page specific options - get()
			$currentPageOptions = $this->getCurrentPageOption($decors, $perPage);
			$decors = $currentPageOptions['options'];
			$currentPage = $currentPageOptions['currentPage'];
			$arrayDecors = $decors->all();
		}
		else
		{
			$allDecorIds = $decors->pluck('id')->all(); // get all IDs for merge

			$decors = $decors->groupBy('decor.id')
			                 ->orderBy('decor.created_at', 'DESC')->get(); // default sort

			$arrayDecors = $decors->all();

			if (is_null($userFilterId))
			{
				$priorityIds = $this->getItemIdsByPriority($eventId, $pageId, $allDecorIds); // get priorities
			}
			else
			{
				$params = [
					'eventId'         => $eventId,
					'pageId'          => $pageId,
					'userFilterId'    => $userFilterId,
					'allFilterTagIds' => $categoryTags->pluck('id')->toArray(),
					'allItemIds'      => $allDecorIds
				];

				$priorityIds = $this->getItemIdsByPriorityForTags($params);
			}

			usort($arrayDecors, function ($a, $b) use ($priorityIds) {
				return $this->sortItemsByPriority($a, $b, $priorityIds);
			});

			// fetch page specific options array after priority
			$currentPageOptions = $this->getCurrentPageOption($arrayDecors, $perPage);
			$decors = $currentPageOptions['options'];
			$currentPage = $currentPageOptions['currentPage'];
			$arrayDecors = $decors->all();
		}

		$cacheProductIds = $decors->pluck('code')->toArray();

		$decorList = $this->getPaginatedItems($totalDecorCount, $arrayDecors, $currentPage, $perPage);

		// data related to price categories
		$priceCategoryData = [
			'min'          => $calMinPrice,
			'max'          => $calMaxPrice,
			'mapTypeId'    => config('evibe.ticket.type.decor'),
			'typeTicketId' => $this->getPageId(),
			'eventId'      => $eventId,
			'cityId'       => getCityId()
		];

		// @todo: get device specific data
		$data = [
			'decors'      => $decorList,
			'totalCount'  => $totalDecorCount,
			'occasionUrl' => $this->getOccasionUrl(),
			'filters'     => [
				'active'              => $categoryUrl,
				'queryParams'         => $this->getExistingQueryParams(),
				'clearFilter'         => $clearFilter,
				'search'              => $userSearch,
				'allCategories'       => $categoryTags,
				'priceMin'            => $priceMin,
				'priceMax'            => $priceMax,
				'priceCategories'     => $this->getPriceCategoryFilters($priceCategoryData),
				'hidePriceCategories' => false,
				'relevantParentTags'  => $relevantParentTags,
				'priceFilterApplied'  => $priceFilterApplied
			],
			'sort'        => $sortParams['sortType'],
			'seo'         => [
				'pageTitle'       => $pageTitle,
				'pageDescription' => $pageDescription,
				'pageHeader'      => ucwords($pageHeader),
				'keyWords'        => $keyWords,
				'schema'          => $schema
			],
			'occasionId'  => $this->getOccasionId(),
			'cityId'      => getCityId(),
			'cityUrl'     => getCityUrl(),
			'mapTypeId'   => $this->getPageId(),
			'countries'   => Country::all()
		];

		$data = array_merge($data, $this->mergeView(getCityUrl(), $this->getOccasionId()));
		$data['cacheProductIds'] = $cacheProductIds;

		return $data;
	}

	public function filterDataForTags($decors, $selectedTagUrl, $allTagUrls, $collectionid = null)
	{
		// Check whether it is having the pipe in the string to enable the multiple category selection
		if (strpos($selectedTagUrl, '__') !== false)
		{
			$categoryUrls = explode("__", $selectedTagUrl);
			$categoryUrls = count($categoryUrls) > 0 ? $categoryUrls : [];
		}
		else
		{
			$categoryUrls = [$selectedTagUrl];
		}

		$selectedCategoryTags = [];
		if ($collectionid)
		{

			switch ($collectionid)
			{
				case config('evibe.occasion-landingpages.birthday-decors.balloon-decoration.id'):

					$selectedCategoryTags = config('evibe.occasion-landingpages.birthday-decors.balloon-decoration.ids');
					break;
				case config('evibe.occasion-landingpages.birthday-decors.theme-decorations.id'):

					$selectedCategoryTags = config('evibe.occasion-landingpages.birthday-decors.theme-decorations.ids');
					break;
				case config('evibe.occasion-landingpages.birthday-decors.craft-decorations.id'):
					$selectedCategoryTags = config('evibe.occasion-landingpages.birthday-decors.craft-decorations.ids');
					break;
			}
		}
		else
		{
			$selectedCategoryTags = array_intersect($allTagUrls->pluck("url", "id")->toArray(), $categoryUrls);
		}
		if (count($selectedCategoryTags) > 0)
		{
			$categoryDecorTags = DecorTag::whereIn('tag_id', array_keys($selectedCategoryTags))
			                             ->pluck('decor_id')
			                             ->toArray();

			$decors->whereIn('decor.id', $categoryDecorTags);
		}

		return [
			'decors' => $decors,
			'tags'   => $selectedCategoryTags
		];
	}

	public function getDecorsListData($cityUrl, $categoryUrl = "")
	{
		// validate url params & redirect to results if not valid
		$urlParams = [['key' => 'city', 'value' => $cityUrl]];
		$validUrlObj = $this->validateUrlParams($urlParams);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$eventId = $this->getOccasionId();
		$pageId = $this->getPageId();
		$clearFilter = false;

		$decors = Decor::with('tags', 'gallery', 'provider', 'provider.city', 'provider.rating')
		               ->select('decor.*')
		               ->isLive()
		               ->forCity()
		               ->forEvent($eventId);

		// Filter by search term
		$userSearch = "";
		if (request()->has('search') && trim(request()->get('search')))
		{
			$clearFilter = true;
			$userSearch = trim(request()->get('search'));
			$searchWords = explode(' ', $userSearch);

			$decors->where(function ($searchQuery) use ($searchWords) {
				foreach ($searchWords as $word)
				{
					$searchQuery->orWhere('decor.name', 'LIKE', "%$word%")
					            ->orWhere('decor.code', 'LIKE', "%$word%");
				}
			});
		}

		// Get all decors for category counting
		$allDecors = $decors->groupBy('decor.id')->get(); // groupBy is fix for distinct
		$currentFilteredDecors = $allDecors; // for fetching default min, max prices

		// Get decors by categories (all / selected category)
		$active = $categoryUrl;
		$filterData = $this->getFilterData($eventId, $pageId, $categoryUrl);
		$allTagIdsBank = $filterData['allTagIdsBank'];
		$allCategories = $filterData['allCategories'];
		$allFilterTagIds = $filterData['allFilterTagIds'];
		$hasFilter = $filterData['hasFilter'];
		$userFilterId = null;
		$hidePriceCategories = false;

		// SEO
		$defaultKey = "defaults.city.$eventId.$pageId.list";
		$seo = $this->getSeoText([getCityId(), $eventId, $pageId, "list"], $defaultKey);
		$itemsListKeys = [
			"Evibe.in",
			getCityName(),
			getTextFromSlug($this->getOccasionUrl()),
			getTextFromSlug($this->getPageName())
		];

		// given user filter is valid -> filter results
		if ($hasFilter)
		{
			$replaces = [
				'{tagTitle}' => $filterData['tagsSeoTitle'],
				'{tagDesc}'  => $filterData['tagsSeoDesc'],
				'{tagName}'  => $filterData['filterTagName']
			];
			$defaultKey = "defaults.city.$eventId.$pageId.filter";
			$seo = $this->getSeoText([getCityId(), $eventId, $pageId, "filter"], $defaultKey, $replaces);
			array_push($itemsListKeys, $filterData['filterTagName']);

			$decors->hasTags($allFilterTagIds);
			$currentFilteredDecors = $decors->groupBy('decor.id')->get();
			$clearFilter = true;
			$userFilterId = $filterData['userFilterTag']->id;
		}

		$pageTitle = $seo['pt'];
		$pageDescription = $seo['pd'];
		$pageHeader = $seo['ph'];
		$keyWords = $seo['kw'];
		//$schema = [$this->schemaBreadcrumbList($itemsListKeys)];

		// Filter by price
		$minPrices = $currentFilteredDecors->pluck('min_price');
		$maxPrices = $currentFilteredDecors->pluck('max_price');
		$calMinPrice = $minPrices->min();
		$calMaxPrice = ($maxPrices->max() > $minPrices->max()) ? $maxPrices->max() : $minPrices->max();
		$priceMin = request('price_min');
		$priceMax = request('price_max');
		$priceMin = $priceMin ? $priceMin : $calMinPrice;
		$priceMax = ($priceMax && $priceMax > $priceMin) ? $priceMax : $calMaxPrice;

		// user has changed min / max price
		if ($calMinPrice != $priceMin || $calMaxPrice != $priceMax)
		{
			$clearFilter = true;
			// $hidePriceCategories = true;
			$decors->where(function ($query1) use ($priceMin, $priceMax) {
				$query1->where(function ($query11) use ($priceMin, $priceMax) {
					$query11->where('max_price', '=', 0)
					        ->where('min_price', '>=', $priceMin)
					        ->where('min_price', '<=', $priceMax);
				})->orWhere(function ($query12) use ($priceMin, $priceMax) {
					$query12->where('max_price', '!=', 0)
					        ->where('min_price', '<=', $priceMin)
					        ->where('max_price', '>=', $priceMin)
					        ->where('max_price', '<=', $priceMax);
				})->orWhere(function ($query13) use ($priceMin, $priceMax) {
					$query13->where('max_price', '!=', 0)
					        ->where('min_price', '>=', $priceMin)
					        ->where('min_price', '<=', $priceMax)
					        ->where('max_price', '>=', $priceMax);
				})->orWhere(function ($query14) use ($priceMin, $priceMax) {
					$query14->where('max_price', '!=', 0)
					        ->where('min_price', '>=', $priceMin)
					        ->where('max_price', '<=', $priceMax);
				});
			});
		}

		// sort results
		// if default (popularity), sort by priority
		// else sort by user input (ex: price, new-arrivals)
		$sortParams = $this->getSortParams('decor', ['price' => 'decor.min_price']);
		if (!$sortParams['isDefault'])
		{
			// groupBy is fix for distinct
			$decors = $decors->groupBy('decor.id')
			                 ->orderBy($sortParams['entity'], $sortParams['order'])
			                 ->get();
			$allDecorIds = $decors->pluck('id')->all(); // get all IDs for merge
			$arrayDecors = $decors->all();
		}
		else
		{
			$decors = $decors->groupBy('decor.id')->orderBy('decor.created_at', 'DESC')->get(); // default sort
			$allDecorIds = $decors->pluck('id')->all(); // get all IDs for merge
			$arrayDecors = $decors->all();

			if (is_null($userFilterId))
			{
				$priorityIds = $this->getItemIdsByPriority($eventId, $pageId, $allDecorIds); // get priorities
			}
			else
			{
				$params = [
					'eventId'         => $eventId,
					'pageId'          => $pageId,
					'userFilterId'    => $userFilterId,
					'allFilterTagIds' => $allFilterTagIds,
					'allItemIds'      => $allDecorIds
				];

				$priorityIds = $this->getItemIdsByPriorityForTags($params);
			}

			usort($arrayDecors, function ($a, $b) use ($priorityIds) {
				return $this->sortItemsByPriority($a, $b, $priorityIds);
			});
		}

		// append page description with starting price if filtered by category
		if ($hasFilter)
		{
			$pageDescription .= " Rs. " . $this->formatPrice($calMinPrice) . " onwards.";
		}

		$perPage = config('evibe.paginate-options.generic-desktop') ? config('evibe.paginate-options.generic-desktop') : 51;
		if ($this->agent->isMobile() && !($this->agent->isTablet()))
		{
			$perPage = config('evibe.paginate-options.generic-mobile') ? config('evibe.paginate-options.generic-mobile') : 50;
		}

		$decorList = $this->getPaginatedAutoBookableItems($pageId, $arrayDecors, $perPage);

		// data related to price categories
		$priceCategoryData = [
			'min'          => $calMinPrice,
			'max'          => $calMaxPrice,
			'mapTypeId'    => config('evibe.ticket.type.decor'),
			'typeTicketId' => $this->getPageId(),
			'eventId'      => $eventId,
			'cityId'       => getCityId()
		];

		// @todo: get device specific data
		$data = [
			'decors'         => $decorList['paginatedItems'],
			'autoBookingIds' => $decorList['autoBook'],
			'totalCount'     => count($arrayDecors),
			'occasionUrl'    => $this->getOccasionUrl(),
			'filters'        => [
				'active'              => $active,
				'queryParams'         => $this->getExistingQueryParams(),
				'catCounts'           => $this->getFilterCategoryCounts($allDecors, $allTagIdsBank),
				'allCategories'       => $allCategories,
				'priceMin'            => $priceMin,
				'priceMax'            => $priceMax,
				'priceCategories'     => $this->getPriceCategoryFilters($priceCategoryData),
				'hidePriceCategories' => $hidePriceCategories,
				'search'              => $userSearch,
				'clearFilter'         => $clearFilter
			],
			'sort'           => $sortParams['sortType'],
			'seo'            => [
				'pageTitle'       => $pageTitle,
				'pageDescription' => $pageDescription,
				'pageHeader'      => ucwords($pageHeader),
				'keyWords'        => $keyWords,
				//'schema'          => $schema,
				'schema'          => []
			],
			'filterData'     => $filterData,
			'occasionId'     => $this->getOccasionId(),
			'cityId'         => getCityId(),
			'cityUrl'        => getCityUrl(),
			'mapTypeId'      => $this->getPageId()
		];

		$data = array_merge($data, $this->mergeView(getCityUrl(), $this->getOccasionId()));

		return $data;
	}

	public function getDecorProfileData($cityUrl, $decorUrl)
	{
		// validate url params
		$urlParams = [
			['key' => 'city', 'value' => $cityUrl],
			['key' => 'decor', 'value' => $decorUrl],
		];
		$validUrlObj = $this->validateUrlParams($urlParams);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$decor = $validUrlObj['decor'];
		// update product page views counter
		updateProfileViewCounter($this->getPageId(), $decor->id);

		$cacheKey = $this->deviceSpecificCacheKey(request()->fullUrl());
		$cacheKey = base64_encode($cacheKey);

		if (Cache::has($cacheKey))
		{

			$profileData = Cache::get($cacheKey);
		}
		else
		{
			$profileData = $this->getDecorProfileDataFromDB($validUrlObj);
			Cache::put($cacheKey, $profileData, config('evibe.cache.refresh-time'));
		}

		return $profileData;
	}

	public function getDecorProfileDataFromDB($validUrlObj)
	{
		// check provider exists & live

		$decor = $validUrlObj['decor'];

		if (!$decor->provider)
		{
			$decorResultsUrl = '/' . getCityUrl() . '/' . $this->getOccasionUrl() . '/' . $this->getPageUrl();

			return redirect($decorResultsUrl);
		}

		// get provider area
		$areaId = $decor->provider->area_id;
		$area = Area::find($areaId);
		if (!$area)
		{
			$decorResultsUrl = '/' . getCityUrl() . '/' . $this->getOccasionUrl() . '/' . $this->getPageUrl();

			return redirect($decorResultsUrl);
		}

		// gallery
		$gallery = [];
		$galleryObj = DecorGallery::with('decor')
		                          ->where('decor_id', $decor->id)
		                          ->orderBy('is_profile', 1)
		                          ->get();

		foreach ($galleryObj as $item)
		{
			if (!empty($item->getPath()))
			{
				array_push($gallery, [
					'url'   => $item->getPath("profile"),
					'thumb' => $item->getPath("thumbs"),
					//'title' => $item->title,
					'title' => $decor->name,
					'type'  => $item->type_id
				]);
			}
		}

		$gallery = collect($gallery)->sortByDesc('type')->values()->toArray();

		// get first tag where seo_title is not null and has highest priority
		$decorTags = $decor->tags;
		$categoryName = "";
		$tagPriority = 99999;

		foreach ($decorTags as $tagObj)
		{
			if ($tagObj->seo_title)
			{
				if (!$categoryName)
				{
					$tagPriority = $tagObj->priority ? $tagObj->priority : 99999;
					$categoryName = $tagObj->identifier;
				}

				if ($tagObj->priority && $tagObj->priority < $tagPriority)
				{
					$categoryName = $tagObj->identifier;
					$tagPriority = $tagObj->priority;
				}
			}
		}

		$categoryName = strtolower($categoryName);

		// SEO
		$eventId = $this->getOccasionId();
		$pageId = $this->getPageId();
		$pageUrl = request()->capture()->fullUrl();
		$defaultKey = "defaults.city.$eventId.$pageId.profile";
		$replaces = [
			'{item}'     => strtolower($decor->name),
			'{category}' => $categoryName ? $categoryName : "decorations"
		];

		$seo = $this->getSeoText([getCityId(), $eventId, $pageId, "profile"], $defaultKey, $replaces);
		$pageTitle = $seo['pt'];
		$pageDescription = $seo['pd'];
		$pageHeader = $seo['ph'];
		$keyWords = $seo['kw'];

		$metaImage = isset($gallery[0]) && isset($gallery[0]['url']) ? $gallery[0]['url'] : "";
		$decorTitle = $decor->name . " In " . getCityName();
		$shareUrl = $this->getShareUrl($pageUrl, $decorTitle, "#" . config('evibe.occasion.kids_birthdays.hashtag'), $metaImage);

		$data = [
			'decor'        => $decor,
			'providerArea' => $area,
			'occasionId'   => $eventId,
			'cityId'       => getCityId(),
			'mapTypeId'    => $pageId,
			'seo'          => [
				'pageTitle'       => ucfirst($pageTitle),
				'pageDescription' => $pageDescription,
				'pageHeader'      => $pageHeader,
				'keyWords'        => $keyWords,
				'pageUrl'         => $pageUrl,
				'metaImage'       => $metaImage
			],
			'shareUrl'     => $shareUrl,
			'gallery'      => $gallery,
			'countries'    => Country::all(),
			'variations'   => $this->getProductVariationsData(config("evibe.ticket.type.decor"), $decor->id, request("varId"))
		];

		// shortlist data
		$shortlistDataParams = [
			'mapId'     => $decor->id,
			'mapTypeId' => $this->getPageId(),
		];

		$data = array_merge($data, $this->mergeShortlistData($shortlistDataParams));

		// cancellation policy
		$data['cancellationData'] = $this->calculateCustomerCancellationRefund();

		// todo: partner ratings - normal for mobile, ajax for desktop (because of schema)
		// @see: schema for desktop through Ajax

		// get device specific data
		if ($this->agent->isMobile() && !($this->agent->isTablet()))
		{
			$data['deliveryImages'] = $this->fetchDeliveryImages($decor->id, $this->getPageId());
			$deviceSpecificData = $this->getDecorProfileMobileSpecificData($decor, $data);

		}
		else
		{
			$deviceSpecificData = $this->getDecorProfileDesktopSpecificData($decor, $data);
		}

		return $deviceSpecificData;
	}

	private function getDecorSchema($decor, $partnerRatings)
	{
		$breadcrumbItemList = [
			"Evibe.in",
			getCityName(),
			$this->getPageName(),
			$decor->name
		];
		$schema = [$this->schemaBreadcrumbList($breadcrumbItemList)];
		$avgRating = $partnerRatings['total']['avg'];
		$ratingCount = $partnerRatings['total']['count'];
		$productData = [
			'category'    => $this->getPageName(),
			'name'        => $decor->name,
			'image'       => $decor->getProfileImg(),
			'avgRating'   => $avgRating,
			'ratingCount' => $ratingCount,
			'price'       => $decor->min_price,
			'description' => $decor->info
		];
		array_push($schema, $this->schemaProduct($productData));

		return $schema;
	}

	private function getDecorProfileMobileSpecificData($decor, $data)
	{

		$partnerRatings = $decor->provider->getPartnerReview();
		$schema = $this->getDecorSchema($decor, $partnerRatings);
		//$data = [
		//	'ratings' => $partnerRatings,
		//	'seo' => [
		//		'schema' => $schema
		//	]
		//];
		$data['ratings'] = $partnerRatings;
		$data['seo']['schema'] = $schema;

		$previousUrl = url()->previous();
		$currentUrl = url()->current();

		// needle: config - 'decorations' should match routes
		if ($previousUrl != $currentUrl &&
			(strpos($previousUrl, config('evibe.results_url.decors')) != false)
		)
		{
			$data['backUrl'] = $previousUrl;
		}

		return $data;
	}

	private function getDecorProfileDesktopSpecificData($decor, $data)
	{
		$eventId = $this->getOccasionId();
		$data['pageName'] = $this->getPageName();

		$data = array_merge($data, $this->mergeView(getCityUrl(), $eventId));

		return $data;
	}
}