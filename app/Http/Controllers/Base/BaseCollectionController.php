<?php

namespace App\Http\Controllers\Base;

use App\Models\Collection\CollectionOptions;
use App\Models\Types\TypeTicket;

use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class BaseCollectionController extends BaseListableController
{
	public $isCityPage = false;
	public $collectionMappings;

	public function __construct()
	{
		parent::__construct();

		$this->collectionMappings = $this->listableMappings;
	}

	public function getCollectionListData($cityUrl)
	{
		$urlParams = [['key' => 'city', 'value' => $cityUrl]];
		$validUrlObj = $this->validateUrlParams($urlParams);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$isCityPage = $this->isCityPage;
		$eventUrl = $this->getOccasionUrl() ? $this->getOccasionUrl() : false;
		$eventId = $this->getOccasionId() ? $this->getOccasionId() : false;
		$cityId = getCityId();
		$cityName = getCityName();
		$pageId = "collections";

		// SEO
		$defaultKey = "defaults.city.";
		$defaultKey .= $eventId ? "$eventId.$pageId.list" : "$pageId.list";
		$seo = $this->getSeoText([getCityId(), $eventId, $pageId, "list"], $defaultKey);
		$itemsListKeys = ["Evibe.in", $cityName];
		if ($eventUrl)
		{
			array_push($itemsListKeys, getTextFromSlug($eventUrl));
		}
		array_push($itemsListKeys, getTextFromSlug($pageId));
		$pageTitle = $seo['pt'];
		$pageDescription = $seo['pd'];
		$pageHeader = $seo['ph'];
		$keyWords = $seo['kw'];
		$schema = [$this->schemaBreadcrumbList($itemsListKeys)];

		$host = config('evibe.host');
		$cityUrl = strtolower($cityUrl);
		$profileUrl = config('evibe.occasion.collections.results_url');
		$collectionBaseUrl = "$host/$cityUrl/";
		$collectionBaseUrl .= $eventUrl ? "$eventUrl/$profileUrl/" : "$profileUrl/";
		$header = "cities." . $cityUrl . ".city-collections-header";

		// pagination
		$collections = $this->getCollections($cityId, $eventId);
		$currentPage = Paginator::resolveCurrentPage();
		$perPage = 21;
		$offset = ($currentPage * $perPage) - $perPage;
		$paginatedCollections = new LengthAwarePaginator(
			array_slice($collections, $offset, $perPage, true),
			count($collections),
			$perPage,
			$currentPage,
			['path' => Paginator::resolveCurrentPath()]
		);

		$data = [
			'collectionBaseUrl' => $collectionBaseUrl,
			'collections'       => $paginatedCollections,
			'seo'               => [
				'pageTitle'       => $pageTitle,
				'pageDescription' => $pageDescription,
				'pageHeader'      => $pageHeader,
				'keyWords'        => $keyWords,
				'schema'          => $schema
			],
		];

		if ($isCityPage)
		{
			$data = array_merge($data, [
				'collectionBaseUrl' => $collectionBaseUrl,
				'bottomHeader'      => $header
			]);
		}
		else
		{
			$data = array_merge($data, $this->mergeView($cityUrl, $eventId));
		}

		return $data;
	}

	public function getCollectionProfileData($cityUrl, $collectionUrl)
	{
		$urlParams = [
			['key' => 'city', 'value' => $cityUrl],
			['key' => 'collection', 'value' => $collectionUrl]
		];
		$validUrlObj = $this->validateUrlParams($urlParams);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$isCityPage = $this->isCityPage;
		$collection = $validUrlObj['collection'];
		$collectionId = $collection->id;
		$collectionOptions = $this->getCollectionOptions($collectionId);
		$totalOptionsCount = count($collectionOptions);
		$header = "";
		$collectionBaseUrl = "";
		$clearFilter = false;
		$eventId = $this->getOccasionId() ? $this->getOccasionId() : false;
		$eventUrl = $this->getOccasionUrl() ? $this->getOccasionUrl() : false;
		$pageId = "collections";
		$cityName = getCityName();

		if ($isCityPage)
		{
			$cityUrl = strtolower($cityUrl); // to handle "Bangalore", "Pune" cases
			$header = "cities." . $cityUrl . ".city-collections-header";
			$host = config('evibe.host');
			$profileUrl = config('evibe.occasion.collections.profile_url');
			$collectionBaseUrl = "$host/$cityUrl/$profileUrl/";
		}

		// SEO
		$defaultKey = "defaults.city.";
		$defaultKey .= $eventId ? "$eventId.$pageId.profile" : "$pageId.profile";
		$replaces = ['{item}' => strtolower($collection->name)];

		$seo = $this->getSeoText([getCityId(), $eventId, $pageId, "profile"], $defaultKey, $replaces);
		$pageTitle = $seo['pt'];
		$pageDescription = $seo['pd'];
		$pageHeader = $seo['ph'];
		$keyWords = $seo['kw'];

		// schema
		$breadcrumbItemList = ["Evibe.in", $cityName];
		if ($eventUrl)
		{
			array_push($breadcrumbItemList, getTextFromSlug($eventUrl));
		}
		array_push($breadcrumbItemList, getTextFromSlug($pageId));
		array_push($breadcrumbItemList, getTextFromSlug($collection->name));
		$schema = [$this->schemaBreadcrumbList($breadcrumbItemList)];

		// Showing category filter options
		$uniqueMapTypes = [];
		$uniqueMapTypeIds = array_unique(array_column($collectionOptions, 'mapTypeId'));
		$uniqueCategories = TypeTicket::whereIn('id', $uniqueMapTypeIds)->get()->toArray();
		$uniqueCategoryByIds = array_combine(array_keys($uniqueCategories), array_column($uniqueCategories, 'id')); // generates array with ids

		foreach ($collectionOptions as $collectionOption)
		{
			$mapTypeId = $collectionOption['mapTypeId'];
			$searchPos = array_search($mapTypeId, $uniqueCategoryByIds);
			$category = isset($uniqueCategories[$searchPos]) ? $uniqueCategories[$searchPos] : null; // find the position of this mapTypeId & then the values

			if (!$category)
			{
				continue;
			}

			if (!isset($uniqueMapTypes[$mapTypeId]))
			{
				$uniqueMapTypes[$collectionOption['mapTypeId']] = [
					"name"  => $category['name'],
					"count" => 0
				];
			}

			$uniqueMapTypes[$collectionOption['mapTypeId']]['count'] += 1;
		}

		// Filter by category
		$selCategory = request()->has('category') ? request('category') : null;
		if (!is_null($selCategory) && $selCategory != 'all')
		{
			$clearFilter = true;
			$selCategoryType = TypeTicket::where('name', $selCategory)->get()->toArray();
			$selCategoryTypeId = isset($selCategoryType[0]) && isset($selCategoryType[0]['id']) ? ($selCategoryType[0]['id']) : null;
			$collectionOptions = $this->getCollectionOptions($collectionId, $selCategoryTypeId);
		}

		$data = [
			'cityId'            => getCityId(),
			'collection'        => $collection,
			'collectionOptions' => $collectionOptions,
			'totalOptionsCount' => $totalOptionsCount,
			'uniqueMapTypes'    => $uniqueMapTypes,
			'selCategory'       => $selCategory,
			'clearFilter'       => $clearFilter,
			'seo'               => [
				'pageTitle'       => $pageTitle,
				'pageDescription' => $pageDescription,
				'pageHeader'      => $pageHeader,
				'keywords'        => $keyWords,
				'schema'          => $schema
			],
		];

		if (!$isCityPage)
		{
			$data = array_merge($data, $this->mergeView($cityUrl, $eventId));
		}
		else
		{
			$data = array_merge($data, [
				'bottomHeader'      => $header,
				'collectionBaseUrl' => $collectionBaseUrl,
			]);
		}

		return $data;
	}

	// get the collection option for the collection_id
	// returns list of collection options
	private function getCollectionOptions($collectionId, $typeId = null)
	{
		$collectionListData = [];
		$dataStore = app()->make("EvibeUtilDataStore");

		if ($typeId)
		{
			$collectionList = CollectionOptions::where('collection_id', $collectionId)
			                                   ->where('map_type_id', $typeId)
			                                   ->orderBy(DB::raw('ISNULL(priority), priority'), 'ASC')
			                                   ->get();
		}
		else
		{
			$collectionList = CollectionOptions::where('collection_id', $collectionId)
			                                   ->orderBy(DB::raw('ISNULL(priority), priority'), 'ASC')
			                                   ->get();
		}

		foreach ($collectionList as $collectionOption)
		{
			$modelName = $this->collectionMappings[$collectionOption->map_type_id]['model'];
			$id = $collectionOption->map_id;
			$name = $this->collectionMappings[$collectionOption->map_type_id]['name'];
			$cityUrl = $dataStore->getCityUrlFromId($collectionOption->city_id);
			$occasionUrl = $collectionOption->event_id ? $dataStore->getOccasionUrlFromId($collectionOption->event_id) : $this->getOccasionUrl();
			$displayName = $this->collectionMappings[$collectionOption->map_type_id]['display_name'];

			$collectionParams = [
				'mapId'       => $collectionOption->map_id,
				'mapTypeId'   => $collectionOption->map_type_id,
				'occasionId'  => $collectionOption->event_id,
				'cityId'      => $collectionOption->city_id,
				'displayName' => $displayName
			];

			$model = $modelName::find($id);

			if ($model)
			{
				$cardData = $model->getCollectionCardInfo($cityUrl, $occasionUrl, ['name' => $name], $collectionParams);
				if (count($cardData) > 5)
				{
					array_push($collectionListData, $cardData);
				}
			}
		}

		return $collectionListData;
	}
}