<?php

namespace App\Http\Controllers\Base;

class BaseVillaController extends BasePackageController
{
	public function __construct()
	{
		parent::__construct();

		$this->setPageId(config('evibe.ticket.type.villas'));
	}

	public function getVillaListData($cityUrl, $profileUrl = null)
	{
		$profileUrl = !is_null($profileUrl) ? $profileUrl : config('evibe.occasion.bachelor.villa.profile_url');
		$villaListData = $this->getListData($cityUrl);
		if (is_array($villaListData))
		{
			$host = config('evibe.host');
			$villaListData['profileBaseUrl'] = "$host/$cityUrl/" . $this->getOccasionUrl() . "/$profileUrl/";
		}

		return $villaListData;
	}

	public function getVillaProfileData($cityUrl, $villaUrl, $hashTag = "#Villa")
	{
		return $this->getProfileData($cityUrl, $villaUrl, $hashTag, "villa");
	}
}