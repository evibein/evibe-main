<?php

namespace App\Http\Controllers\Base;

use App\Http\Controllers\Payments\BasePaymentController;
use App\Models\Cake\Cake;
use App\Models\Cake\CakeGallery;
use App\Models\Decor\Decor;
use App\Models\Decor\DecorGallery;
use App\Models\Package\Package;
use App\Models\Types\TypeServices;
use App\Models\Util\Area;
use App\Models\Types\TypeTag;
use App\Models\Util\ProductVariation;
use App\Models\Util\ProductVariationOption;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Chencha\Share\ShareFacade as Share;

class BaseResultsController extends BaseController
{
	private $pageId;
	private $pageName;
	private $pageUrl;

	public function setPageId($pageId)
	{
		if (is_array($pageId))
		{
			$this->pageId = $pageId;
		}
		else
		{
			$this->pageId = $pageId;
		}
	}

	public function getPageId()
	{
		if (is_array($this->pageId))
		{
			return $this->pageId;
		}

		return $this->pageId;
	}

	public function setPageName($pageName)
	{
		$this->pageName = $pageName;
	}

	public function getPageName()
	{
		return $this->pageName;
	}

	public function setPageUrl($pageUrl)
	{
		$this->pageUrl = $pageUrl;
	}

	public function getPageUrl()
	{
		return $this->pageUrl;
	}

	public function getLocations($cityId)
	{
		$areas = Area::where('city_id', $cityId)
		             ->orderBy('name')
		             ->get();

		return $areas;
	}

	public function getFilterCategoryCounts($list, $catIds)
	{
		$counts = [];
		$haystack = [];
		$total = 0;

		// initialize
		if (is_array($catIds))
		{
			foreach ($catIds as $catId)
			{
				$counts[$catId] = 0;
				array_push($haystack, $catId);
			}
		}

		foreach ($list as $item)
		{
			$itemTags = $item->tags; // to avoid repetitive tags
			foreach ($itemTags as $itemTag)
			{
				$itemTagId = $itemTag->id; // getting from pivot table
				if (in_array($itemTagId, $haystack))
				{
					if (!array_key_exists($itemTagId, $counts))
					{
						$counts[$itemTagId] = 0;
					}
					$counts[$itemTagId] = $counts[$itemTagId] + 1;

					// increase parent tag count
					$parentId = $itemTag->parent_id;
					if ($parentId && in_array($parentId, $haystack))
					{
						$counts[$parentId] = $counts[$parentId] + 1;
					}

					$total += 1;
				}
			}
		}

		$counts['total'] = $total;

		return $counts;
	}

	public function incrementViewsCounter($type, $model)
	{
		$value = "";
		$key = "";

		switch ($type)
		{
			case 'package':
				$key = 'evibe_pkg_ivc';
				break;
		}

		$expiry = 0;

		if (Cookie::has($key))
		{
			$value = Cookie::get($key);
			$expiry = (int)$value - time();
		}

		// first time / cookie expired
		if ($expiry <= 0)
		{
			$expiry = 10 * 60; // cookie expires in 10 minutes
			$value = time() + $expiry;

			$model->incrementViews(); // increment views counter
		}

		Cookie::queue($key, $value, $expiry);
	}

	/**
	 * Getting the url for the share buttons
	 */
	public function getShareUrl($url, $title, $hashtag, $image_url)
	{
		$fb = Share::load($url, $title, $hashtag)->facebook();
		$twitter = Share::load($url, $title, $hashtag)->twitter();
		$pinterest = Share::load($url, $title, $hashtag, $image_url)->pinterest();

		$data = [
			'facebook'  => $fb,
			'twitter'   => $twitter,
			'pinterest' => $pinterest,
		];

		return $data;
	}

	protected function getFilterData($eventId, $pageId, $categoryUrl = '', $collectionId = null)
	{
		$allFilterTagIds = [];
		$tagsSeoTitle = "";
		$tagsSeoDesc = "";
		$filterTagName = "";
		$hasFilter = false;
		$userFilterTag = false;
		$catsData = $this->getAllValidCategories($eventId, $pageId);

		$allCategories = $catsData['cats'];
		$allTagIdsBank = $catsData['bank'];
		// Get Tag Ids directly from collection defination
		if ($collectionId)
		{
			$tagIds = [];
			foreach (config('evibe.occasion-landingpages') as $occasion)
			{
				foreach ($occasion as $collection)
				{
					if (isset($collection['id']) && $collection['id'] == $collectionId)
					{
						$tagIds = array_keys($collection['ids']);
					}
				}
			}
			if ($tagIds)
			{

				$allFilterTagIds = $tagIds;
				$hasFilter = true;
			}

		}
		// style selected, join tags for further filter
		elseif (($categoryUrl && $categoryUrl != 'all'))
		{
			// get tag id based on url
			$userFilterTag = TypeTag::where('url', $categoryUrl)->first();

			if ($categoryUrl == "trending-surprises")
			{
				$hasFilter = true;
				$getAllTags = TypeTag::whereNotIn('id', [185, 186, 125])->where([['type_event', 16], ['map_type_id', $pageId]])->filterable()
				                     ->whereNull('parent_id')->pluck('id');
				// foreach ($getAllTags as $id) {
				// 	array_push($allFilterTagIds, $id);
				// }
				$allFilterTagIds = $getAllTags->toArray();
			}
			elseif ($userFilterTag)
			{
				$hasFilter = true;
				$filterTagId = $userFilterTag->id;
				if ($userFilterTag->id == config('evibe.occasion-landingpages.surprises.filters-tags.trending-dates'))
				{
					foreach (config('evibe.occasion-landingpages.surprises.trending-dates-ids') as $trendingDateName => $trendingDateId)
					{
						array_push($allFilterTagIds, $trendingDateId);
					}
				}
				else
				{
					array_push($allFilterTagIds, $filterTagId);
				}
				$tagsSeoTitle .= $userFilterTag->seo_title ? $userFilterTag->seo_title : "";
				$tagsSeoDesc .= $userFilterTag->seo_desc ? $userFilterTag->seo_desc : "";
				$filterTagName = $userFilterTag->identifier ? $userFilterTag->identifier : $userFilterTag->name;

				// collect all relevant tag ids (if parent, then all child)
				if (isset($allCategories[$filterTagId]))
				{
					// is parent tag
					if (count($allCategories[$filterTagId]['child']))
					{
						$filterTagChildTags = $allCategories[$filterTagId]['child'];
						foreach ($filterTagChildTags as $key => $value)
						{
							array_push($allFilterTagIds, $key);
							$tagsSeoTitle .= $value['seoTitle'] ? $value['seoTitle'] : "";
							$tagsSeoDesc .= $value['seoDesc'] ? $value['seoDesc'] : "";
						}
					}
				}
			}
		}

		$data = [
			'allCategories'   => $allCategories,
			'allTagIdsBank'   => $allTagIdsBank,
			'allFilterTagIds' => $allFilterTagIds,
			'hasFilter'       => $hasFilter,
			'filterTagName'   => $filterTagName,
			'tagsSeoTitle'    => $tagsSeoTitle,
			'tagsSeoDesc'     => $tagsSeoDesc,
			'userFilterTag'   => $userFilterTag
		];

		return $data;
	}

	/**
	 * These methods are only used by cakes & trends
	 *
	 * @todo: get rid of these methods and use existing methods
	 */
	protected function buildURLQueryParams($param, $paramType = null)
	{
		$input = request()->all();
		$inputCount = count($input);
		$paramString = '?';
		$paramValue = "";

		switch ($paramType)
		{
			case 'cost':
				$paramValue = $this->getPriceSortValue(request($param));
				break;

			case 'exp':
				$paramValue = $this->getExpSortValue(request($param));
				break;

			case 'popularity':
				$paramValue = $this->getPopularitySortValue(request($param));
				break;
		}

		if ($inputCount == 0)
		{
			$paramString .= $param . '=' . $paramValue;
		}
		else
		{
			foreach ($input as $key => $value)
			{
				if ($param != $key)
				{
					$paramString .= $key . '=' . $value . '&';
				}
			}

			$paramString .= $param . '=' . $paramValue;

		}

		return $paramString;
	}

	private function getPriceSortValue($val)
	{
		$priceSort = 'ca';

		if ($val == 'ca')
		{
			$priceSort = 'cd';
		}

		return $priceSort;
	}

	private function getExpSortValue($val)
	{
		$expSort = 'ea';

		if ($val == 'ea')
		{
			$expSort = 'ed';
		}

		return $expSort;
	}

	private function getPopularitySortValue($val)
	{
		$popularitySort = 'pa';

		if ($val == 'pa')
		{
			$popularitySort = 'pd';
		}

		return $popularitySort;
	}

	private function getAllValidCategories($eventId, $pageId)
	{
		$allCategories = [];
		$allTagIdsBank = [];
		$allParentTags = TypeTag::forEvent($eventId)
		                        ->forPage($pageId)
		                        ->filterable()
		                        ->whereNull('parent_id')
		                        ->orderBy(DB::raw("ISNULL(priority), priority"), 'ASC')
		                        ->get();

		foreach ($allParentTags as $parentTag)
		{
			$tagId = $parentTag->id;
			array_push($allTagIdsBank, $tagId);

			if (!isset($allCategories[$tagId]))
			{
				$allCategories[$tagId] = [
					'id'       => $tagId,
					'name'     => $parentTag->identifier ? $parentTag->identifier : $parentTag->name,
					'url'      => $parentTag->url,
					'count'    => 0,
					'seoTitle' => $parentTag->seo_title,
					'seoDesc'  => $parentTag->seo_desc,
					'child'    => []
				];
			}

			$childTags = TypeTag::where('parent_id', $tagId)
			                    ->filterable()
			                    ->orderBy(DB::raw("ISNULL(priority), priority"), 'ASC')
			                    ->get();

			foreach ($childTags as $childTag)
			{
				$childTagId = $childTag->id;
				array_push($allTagIdsBank, $childTagId);

				$allCategories[$tagId]['child'][$childTagId] = [
					'id'       => $childTagId,
					'name'     => $childTag->identifier ? $childTag->identifier : $childTag->name, // take identifier
					'url'      => $childTag->url,
					'seoTitle' => $childTag->seo_title,
					'seoDesc'  => $childTag->seo_desc,
					'count'    => 0
				];
			}
		}

		$retData = [
			'cats' => $allCategories,
			'bank' => array_unique($allTagIdsBank) // take unique tags ids
		];

		return $retData;
	}

	public function getSelectedDeliveryImages()
	{
		$userId = config("evibe.default.review_handler");
		$accessToken = $this->getAccessToken('', $userId);
		$url = config('evibe.api.base_url') . config('evibe.api.delivery_images.prefix');
		$options = [
			'method'      => "POST",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => request()->all(),
		];
		$res = $this->makeApiCallWithUrl($options);

		return response()->json($res);
	}

	public function getProductVariationsData($mapTypeId, $mapId, $variationId)
	{
		$productVariationId = "";
		$productVariationOptions = ProductVariationOption::where("map_type_id", $mapTypeId)
		                                                 ->where("map_id", $mapId)
		                                                 ->pluck("product_variation_id")
		                                                 ->toArray();

		if (count($productVariationOptions) == 1)
		{
			// If product has only one option then it will show all the options in that particular variation
			$productVariationId = $productVariationOptions[0];
		}
		elseif (count($productVariationOptions) > 1)
		{
			if (in_array($variationId, $productVariationOptions))
			{
				// If selected product is there in more than one variation & came through one variation
				// show the options from previous variation
				$productVariationId = $variationId;
			}
			else
			{
				// If selected product is there in more than one variation & directly came to the page
				// then sort by priority
				$productVariations = ProductVariation::whereIn("id", $productVariationOptions)
				                                     ->orderBy("priority", "desc")
				                                     ->first();

				$productVariationId = $productVariations ? $productVariations->id : "";
			}
		}

		$productVariationsData = ProductVariationOption::select("map_type_id", "map_id")
		                                               ->where("product_variation_id", $productVariationId)
		                                               ->where("map_id", "!=", $mapId)
		                                               ->get()
		                                               ->toArray();

		return [
			"productVariations" => $productVariationsData,
			"variationId"       => $productVariationId
		];
	}

	public function fetchOptionDataForVariations()
	{
		$variationsData = request("variationsData");
		$variationsOptionsData = [];
		$variationsOptionsId = request("variationId");
		$res = [
			"success" => false
		];

		if (count($variationsData) > 0)
		{
			$basePaymentController = new BasePaymentController();

			foreach ($variationsData as $variationData)
			{
				switch ($variationData["mapTypeId"])
				{
					case config("evibe.ticket.type.cake"): //Cake
						$productData = Cake::where('id', $variationData["mapId"])
						                   ->first();

						$prodImg = CakeGallery::where('cake_id', $variationData["mapId"])
						                      ->first()
						                      ->getLink();

						$variationsOptionsData[$variationData["mapTypeId"] . "_" . $variationData["mapId"]] = [
							"url"   => $productData->getLink(null) . "?ref=variation&varId=" . $variationsOptionsId,
							"image" => $prodImg
						];

						break;

					case config("evibe.ticket.type.decor"): //Decor
						$productData = Decor::where('id', $variationData["mapId"])
						                    ->first();

						$prodImg = DecorGallery::where('decor_id', $variationData["mapId"])
						                       ->first()
						                       ->getPath();

						$variationsOptionsData[$variationData["mapTypeId"] . "_" . $variationData["mapId"]] = [
							"url"   => $productData->getFullLinkForSearch(getCityName(), null) . "?ref=variation&varId=" . $variationsOptionsId,
							"image" => $prodImg
						];

						break;

					case config("evibe.ticket.type.entertainment"): // Entertainment
						$productData = TypeServices::where('id', $variationData["mapId"])
						                           ->first();

						$prodImg = TypeServices::where('id', $variationData["mapId"])
						                       ->first()
						                       ->getProfilePic();

						$variationsOptionsData[$variationData["mapTypeId"] . "_" . $variationData["mapId"]] = [
							"url"   => $productData->getFullLinkForSearch(getCityName(), null) . "?ref=variation&varId=" . $variationsOptionsId,
							"image" => $prodImg
						];

						break;

					//Packages
					case config("evibe.ticket.type.package"):
					case config("evibe.ticket.type.tents"):
						$productData = Package::where('id', $variationData["mapId"])
						                      ->first();

						$prodImg = Package::where('id', $variationData["mapId"])
						                  ->first()
						                  ->getProfileImg();

						$variationsOptionsData[$variationData["mapTypeId"] . "_" . $variationData["mapId"]] = [
							"url"   => $productData->getFullLinkForSearch(getCityName(), null, 'packages') . "?ref=variation&varId=" . $variationsOptionsId,
							"image" => $prodImg
						];

						break;

					//priests
					case config("evibe.ticket.type.priests"):
						$productData = Package::where('id', $variationData["mapId"])
						                      ->first();

						$prodImg = Package::where('id', $variationData["mapId"])
						                  ->first()
						                  ->getProfileImg();

						$variationsOptionsData[$variationData["mapTypeId"] . "_" . $variationData["mapId"]] = [
							"url"   => $productData->getFullLinkForSearch(getCityName(), null, 'priest') . "?ref=variation&varId=" . $variationsOptionsId,
							"image" => $prodImg
						];

						break;
					case config("evibe.ticket.type.food"):
						$productData = Package::where('id', $variationData["mapId"])
						                      ->first();

						$prodImg = Package::where('id', $variationData["mapId"])
						                  ->first()
						                  ->getProfileImg();

						$variationsOptionsData[$variationData["mapTypeId"] . "_" . $variationData["mapId"]] = [
							"url"   => $productData->getFullLinkForSearch(getCityName(), null, 'food') . "?ref=variation&varId=" . $variationsOptionsId,
							"image" => $prodImg
						];

						break;

					//Surprises
					case config("evibe.ticket.type.surprises"):
						$productData = Package::where('id', $variationData["mapId"])
						                      ->first();

						$prodImg = Package::where('id', $variationData["mapId"])
						                  ->first()
						                  ->getProfileImg();

						$variationsOptionsData[$variationData["mapTypeId"] . "_" . $variationData["mapId"]] = [
							"url"   => $productData->getFullLinkForSearch(getCityName(), null, 'surprises') . "?ref=variation&varId=" . $variationsOptionsId,
							"image" => $prodImg
						];

						break;
				}
			}

			$res = [
				"success"               => true,
				"variationsOptionsData" => $variationsOptionsData
			];
		}

		return response()->json($res);
	}
}