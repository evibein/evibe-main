<?php

namespace App\Http\Controllers\Base;

use App\Models\Package\Package;
use App\Models\Cake\Cake;
use App\Models\Decor\Decor;
use App\Models\Trend\Trend;
use App\Models\Types\TypeEvent;
use App\Models\Types\TypeServices;
use App\Models\Util\City;
use App\Models\Venue\VenueHall;
use Illuminate\Support\Facades\Log;

class BaseListableController extends BaseController
{
	public $listableMappings;

	public function __construct()
	{
		parent::__construct();

		$this->listableMappings = [
			config('evibe.ticket.type.cake') => [
				'name'             => 'cake',
				'table'            => 'cake',
				'model'            => Cake::class,
				'is_package'       => false,
				'map_type_id'      => config('evibe.ticket.type.cake'),
				'display_name'     => 'Cake',
				'min_count_budget' => config('evibe.budget.cake.min'),
				'max_count_budget' => config('evibe.budget.cake.max')
			],

			config('evibe.ticket.type.decor') => [
				'name'             => 'decor',
				'table'            => 'decor',
				'model'            => Decor::class,
				'is_package'       => false,
				'map_type_id'      => config('evibe.ticket.type.decor'),
				'display_name'     => 'Decor Style',
				'min_count_budget' => config('evibe.budget.decor.min'),
				'max_count_budget' => config('evibe.budget.decor.max')
			],

			config('evibe.ticket.type.trend') => [
				'name'             => 'trend',
				'table'            => 'trending',
				'model'            => Trend::class,
				'is_package'       => false,
				'map_type_id'      => config('evibe.ticket.type.trend'),
				'display_name'     => 'Trend',
				'min_count_budget' => config('evibe.budget.trend.min'),
				'max_count_budget' => config('evibe.budget.trend.max')
			],

			config('evibe.ticket.type.entertainment') => [
				'name'             => 'entertainment',
				'table'            => 'type_service',
				'model'            => TypeServices::class,
				'is_package'       => false,
				'map_type_id'      => config('evibe.ticket.type.entertainment'),
				'display_name'     => 'Entertainment',
				'min_count_budget' => config('evibe.budget.entertainment.min'),
				'max_count_budget' => config('evibe.budget.entertainment.max')
			],

			config('evibe.ticket.type.service') => [
				'name'             => 'entertainment',
				'table'            => 'type_service',
				'model'            => TypeServices::class,
				'is_package'       => false,
				'map_type_id'      => config('evibe.ticket.type.entertainment'),
				'display_name'     => 'Entertainment',
				'min_count_budget' => config('evibe.budget.entertainment.min'),
				'max_count_budget' => config('evibe.budget.entertainment.max')
			],

			config('evibe.ticket.type.venue-deals') => [
				'name'             => 'venue-deals',
				'table'            => 'planner_package',
				'model'            => Package::class,
				'is_package'       => true,
				'map_type_id'      => config('evibe.ticket.type.venue-deals'),
				'display_name'     => 'Venue Deal',
				'min_count_budget' => config('evibe.budget.venue-deals.min'),
				'max_count_budget' => config('evibe.budget.venue-deals.max')
			],

			config('evibe.ticket.type.package') => [
				'name'             => 'birthday-packages',
				'table'            => 'planner_package',
				'model'            => Package::class,
				'is_package'       => true,
				'map_type_id'      => config('evibe.ticket.type.package'),
				'event_ids'        => [config('evibe.occasion.bachelor.id'), config('evibe.occasion.kids_birthdays.id')],
				'display_name'     => 'Package',
				'min_count_budget' => config('evibe.budget.birthday-packages.min'),
				'max_count_budget' => config('evibe.budget.birthday-packages.max')
			],

			config('evibe.ticket.type.resorts') => [
				'name'             => 'resorts',
				'table'            => 'planner_package',
				'model'            => Package::class,
				'is_package'       => true,
				'map_type_id'      => config('evibe.ticket.type.resorts'),
				'event_ids'        => [config('evibe.occasion.bachelor.id')],
				'display_name'     => 'Resort',
				'min_count_budget' => config('evibe.budget.resorts.min'),
				'max_count_budget' => config('evibe.budget.resorts.max')
			],

			config('evibe.ticket.type.villas') => [
				'name'             => 'villas',
				'table'            => 'planner_package',
				'model'            => Package::class,
				'is_package'       => true,
				'map_type_id'      => config('evibe.ticket.type.villas'),
				'event_ids'        => [config('evibe.occasion.bachelor.id')],
				'display_name'     => 'Villa',
				'min_count_budget' => config('evibe.budget.villas.min'),
				'max_count_budget' => config('evibe.budget.villas.max')
			],

			config('evibe.ticket.type.lounges') => [
				'name'             => 'lounges',
				'table'            => 'planner_package',
				'model'            => Package::class,
				'is_package'       => true,
				'map_type_id'      => config('evibe.ticket.type.lounges'),
				'event_ids'        => [config('evibe.occasion.bachelor.id')],
				'display_name'     => 'Lounges',
				'min_count_budget' => config('evibe.budget.lounges.min'),
				'max_count_budget' => config('evibe.budget.lounges.max')
			],

			config('evibe.ticket.type.food') => [
				'name'             => 'food',
				'table'            => 'planner_package',
				'model'            => Package::class,
				'is_package'       => true,
				'map_type_id'      => config('evibe.ticket.type.food'),
				'event_ids'        => [config('evibe.occasion.bachelor.id'), config('evibe.occasion.kids_birthdays.id')],
				'display_name'     => 'Food',
				'min_count_budget' => config('evibe.budget.food.min'),
				'max_count_budget' => config('evibe.budget.food.max')
			],

			config('evibe.ticket.type.surprises') => [
				'name'             => 'surprises',
				'table'            => 'planner_package',
				'model'            => Package::class,
				'is_package'       => true,
				'map_type_id'      => config('evibe.ticket.type.surprises'),
				'event_ids'        => [config('evibe.occasion.surprises.id')],
				'display_name'     => 'Surprise Packages',
				'min_count_budget' => config('evibe.budget.surprises.min'),
				'max_count_budget' => config('evibe.budget.surprises.max')
			],

			config('evibe.ticket.type.tents') => [
				'name'             => 'tent',
				'table'            => 'planner_package',
				'model'            => Package::class,
				'is_package'       => true,
				'map_type_id'      => config('evibe.ticket.type.tents'),
				'event_ids'        => [config('evibe.occasion.house-warming.id')],
				'display_name'     => 'Tents',
				'min_count_budget' => config('evibe.budget.tents.min'),
				'max_count_budget' => config('evibe.budget.tents.max')
			],

			config('evibe.ticket.type.priests') => [
				'name'             => 'priest',
				'table'            => 'planner_package',
				'model'            => Package::class,
				'is_package'       => true,
				'map_type_id'      => config('evibe.ticket.type.priests'),
				'event_ids'        => [config('evibe.occasion.house-warming.id')],
				'display_name'     => 'Priests',
				'min_count_budget' => config('evibe.budget.priests.min'),
				'max_count_budget' => config('evibe.budget.priests.max')
			],

			config('evibe.ticket.type.halls') => [
				'name'             => 'venue-halls',
				'table'            => 'venue_hall',
				'model'            => VenueHall::class,
				'is_package'       => false,
				'map_type_id'      => config('evibe.ticket.type.halls'),
				'display_name'     => 'Venues',
				'min_count_budget' => config('evibe.budget.venue-halls.min'),
				'max_count_budget' => config('evibe.budget.venue-halls.max')
			]
		];
	}
}