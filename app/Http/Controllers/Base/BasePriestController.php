<?php

namespace App\Http\Controllers\Base;

class BasePriestController extends BasePackageController
{
	public function __construct()
	{
		parent::__construct();

		$this->setPageId(config('evibe.ticket.type.priests'));
	}

	public function getPriestListData($cityUrl, $profileUrl = null)
	{
		$profileUrl = !is_null($profileUrl) ? $profileUrl : config('evibe.occasion.house-warming.priest.profile_url');
		$priestListData = $this->getListData($cityUrl);
		if (is_array($priestListData))
		{
			$host = config('evibe.host');
			$priestListData['profileBaseUrl'] = "$host/$cityUrl/" . $this->getOccasionUrl() . "/$profileUrl/";
		}

		return $priestListData;
	}

	public function getPriestProfileData($cityUrl, $priestUrl, $hashTag = "#Priests")
	{
		return $this->getProfileData($cityUrl, $priestUrl, $hashTag);
	}
}