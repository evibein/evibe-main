<?php

namespace App\Http\Controllers\Base;

use App\Models\OccasionHome\OccasionHomePages;
use App\Models\OccasionHome\OccasionHomeSections;
use App\Models\OccasionHome\OccasionHomeCards;
use App\Models\OccasionHome\OccasionHomeCarousel;
use App\Models\GoogleReviews\GoogleReview;
use App\Models\Types\TypeTag;
use Illuminate\Support\Facades\Cache;
use App\Models\Cake\CakeGallery;
use App\Models\Service\ServiceGallery;

class BaseOccasionHomeController extends BaseController
{
	public function showPage($cityUrl, $pageUrl)
	{
		$pageId = "";
		$pageDetails = OccasionHomePages::where('url', $pageUrl)
		                                ->first();

		if ($pageDetails && $cityUrl)
		{
			$cacheKey = $this->deviceSpecificCacheKey(request()->fullUrl());
			$cacheKey = base64_encode($cacheKey);
			if (Cache::has($cacheKey))
			{
				$data = Cache::get($cacheKey);

				return view("base.home.occasion-home.landing-page")->with(['data' => $data]);
			}

			$urlParams = [['key' => 'city', 'value' => $cityUrl]];
			$validUrlObj = $this->validateUrlParams($urlParams);
			if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
			{
				return redirect($validUrlObj['redirectUrl']);
			}
			$data = [];
			$pageId = $pageDetails['id'];
			if ($pageId)
			{
				// Get Sections
				$sections = OccasionHomeSections::where('page_id', $pageId)->get();
				$carouselData = OccasionHomeCarousel::where('page_id', $pageId);
				$carouselCards = $carouselData->get();
				$carouselCardsDesktop = $carouselData->whereNotNull('img_url_desk')->get();

				$dataStore = app()->make("EvibeUtilDataStore");
				if ($sections)
				{
					$count = 0;
					$sectionsData = [];
					foreach ($sections as $section)
					{
						$host = config('evibe.host');
						$profileUrl = $this->getProfileUrl($section['occasion_id'], $section['type_id']);

						$cardsDetails = OccasionHomeCards::where('section_id', $section['id'])->get();
						$temp = [];

						// Get Products
						$cardProducts = [];
						$cardProductsCount = 0;
						foreach ($cardsDetails as $card)
						{

							$topProducts = $this->getTopProducts($card['occasion_id'], $card['map_type_id'], $card['is_cld'], $card['filter_tag_id'], $card['is_collection'], $card['collection_id']);

							if (count($topProducts))
							{
								$cardProducts[$cardProductsCount]['cardTitle'] = $card['title'];
								$cardProducts[$cardProductsCount]['ctaUrl'] = $card['cta'];
								$cardProducts[$cardProductsCount]['products'] = $topProducts;
								$cardProducts[$cardProductsCount++]['isCld'] = $card['is_cld'];
							}
						}
						$temp['products'] = $cardProducts;
						$temp['sectionTitle'] = $section['heading'];
						$temp['sectionDesc'] = $section['description'];
						$temp['typeSection'] = $section['type_section_id'];

						$temp['profileBaseUrl'] = "$host/$cityUrl/" . $dataStore->getOccasionUrlFromId($section['occasion_id']) . "/$profileUrl/";
						$temp['cards'] = $cardsDetails;
						$temp['cta_url'] = $section['cta_url'];
						$sectionsData[$count++] = $temp;
					}
					$googleReviews = GoogleReview::where('is_show', '1')->limit(6)->get();

					$data['pageTitle'] = $pageDetails['heading'];
					$data['pageDescription'] = $pageDetails['description'];
					$data['seo_title'] = $pageDetails['seo_title'] . "in " . ucfirst($cityUrl) . " | Evibe.in";
					$data['seo_desc'] = $pageDetails['seo_description'];
					$data['seo_keywords'] = $pageDetails['seo_keywords'];
					$data['carouselCards'] = $carouselCards;
					$data['carouselCardsDesktop'] = $carouselCardsDesktop;
					$data['sections'] = $sectionsData;
					$data['reviews'] = $googleReviews;

				}
				else
				{
					return redirect('/');
				}
				Cache::put($cacheKey, $data, 120);

				return view("base.home.occasion-home.landing-page")->with(['data' => $data]);
			}
			else
			{
				// currently taking user to home @see:Inform Team
				return redirect('/');
			}
		}
		else
		{
			//do nothing, taking user to home
			return redirect('/');
		}
	}

	public function getProfileUrl($occasionId, $mapTypeId)
	{
		switch ($occasionId)
		{
			case config('evibe.occasion.surprises.id'):
				return config('evibe.occasion.surprises.package.profile_url');
			case config('evibe.occasion.house-warming.id'):
				switch ($mapTypeId)
				{
					case config('evibe.ticket.type.decor'):
						return config('evibe.occasion.house-warming.decors.profile_url');
						break;
					case config('evibe.ticket.type.cake'):
						return config('evibe.occasion.house-warming.cakes.profile_url');
						break;
				}
		}
	}

	public function getTopProducts($occasionId, $mapTypeId, $isCld, $tagId, $isCollection, $collectionId)
	{
		$cityUrl = (is_null(getCityUrl())) ? getCityUrl() : 'bangalore';
		$listPageProducts = [];

		switch ($occasionId)
		{
			case config('evibe.occasion.surprises.id'):
				if ($isCld)
				{
					$cldPackageController = app()->make("App\Http\Controllers\Occasions\CandleLightDinner\CLDHomeController");
					$listPageProducts = $cldPackageController->showOptions($cityUrl, null, true);
					$listPageProducts = $this->addProfilePic($listPageProducts);
					return $listPageProducts;
				}
				else
				{
					$tagUrl = "";
					if ($tagId)
					{
						$getTagUrl = TypeTag::where('id', $tagId)->first();
						if ($getTagUrl)
						{
							$tagUrl = $getTagUrl['url'];

						}
					}
					elseif ($isCollection && $collectionId)
					{
						// Below condition is seperated from above condition for future purpose, to accumulate other collections
						if (config('evibe.occasion-landingpages.surprises.collections_ids.trending-surprises') == $collectionId)
						{

							$tagUrl = "trending-surprises";
						}
					}
					$suprisePackageController = app()->make("App\Http\Controllers\Occasions\Surprises\SurprisesPackageController");
					$listPageSurpriseProducts = $suprisePackageController->showPackagesList($cityUrl, null, null, null, true, $tagUrl);

					$listPageSurpriseProducts = $listPageSurpriseProducts['packages'];
					$listPageSurpriseProducts = $this->addProfilePic($listPageSurpriseProducts);

					return $listPageSurpriseProducts;

				}

				break;

			case config('evibe.occasion.house-warming.id'):
				switch ($mapTypeId)
				{
					case config('evibe.ticket.type.decor'):
						$houseWarmingPackageController = app()->make("App\Http\Controllers\Occasions\HouseWarming\HouseWarmingDecorController");
						$listPageProducts = $houseWarmingPackageController->showList($cityUrl, null, true);
						$listPageProducts = $listPageProducts['decors'];
						break;
					case config('evibe.ticket.type.cake'):
						$houseWarmingCakeController = app()->make("App\Http\Controllers\Occasions\HouseWarming\HouseWarmingCakeController");
						$listPageProducts = $houseWarmingCakeController->showList($cityUrl, true);
						$listPageProducts = $listPageProducts['cakes'];

						break;
				}

				break;
			case config('evibe.occasion.kids_birthdays.id'):
				switch ($mapTypeId)
				{
					case config('evibe.ticket.type.decor'):
						$birthdayDecorsController = app()->make("App\Http\Controllers\Occasions\Birthdays\BirthdayDecorController");
						$listPageProducts = $birthdayDecorsController->showList($cityUrl, null, true, $collectionId);
						$listPageProducts = $listPageProducts['decors'];
						$listPageProducts = $this->addProfilePic($listPageProducts);
						break;
					case config('evibe.ticket.type.food'):
						$birthdayFoodController = app()->make("App\Http\Controllers\Occasions\Birthdays\BirthdayFoodController");
						$listPageFoodProducts = $birthdayFoodController->showFoodList($cityUrl, true, $collectionId);
						$data = $listPageFoodProducts['packages'];
						$listPageProducts = $this->addProfilePic($data);
						break;

					case config('evibe.ticket.type.entertainment'):
						$birthdayENTController = app()->make("App\Http\Controllers\Occasions\Birthdays\BirthdayEntController");
						$birthdayENTproducts = $birthdayENTController->showList($cityUrl, "", true, $collectionId);
						$data = [];
						if ($birthdayENTproducts)
						{
							$count = 0;
							foreach ($birthdayENTproducts['options'] as $package)
							{
								$temp = [];
								if ($count >= 8)
								{
									break;
								}
								$profilePic = ServiceGallery::where([
									                                    ['type_service_id', $package['id']],
									                                    ['is_profile', 1],
								                                    ])->first();
								$temp = $package;
								if ($profilePic)
								{
									$temp['profilePic'] = config('evibe.gallery.host') . '/services/' . $package['id'] . '/images/results/' . $profilePic['url'];

								}
								else
								{
									$temp['profilePic'] = config('evibe.gallery.host') . '/img/icons/balloons.png';
								}
								$temp['fullUrl'] = $package['fullPath'];
								$temp['min_price'] = $package['minPrice'];
								$data[$count++] = $temp;
							}
						}

						return $data;
						break;

					case config('evibe.ticket.type.cake'):
						$data = [];
						$birthdayCakeController = app()->make("App\Http\Controllers\Occasions\Birthdays\BirthdayCakeController");

						$listPageCakeProducts = $birthdayCakeController->showList($cityUrl, true, $collectionId);

						$count = 0;
						foreach ($listPageCakeProducts['cakes'] as $cake)
						{
							$temp = [];
							if ($count >= 8)
							{
								break;
							}
							$profilePic = CakeGallery::where([
								                                 ['cake_id', $cake['id']],
								                                 ['is_profile', 1],
							                                 ])->first();
							$temp = $cake;
							if ($profilePic)
							{
								$temp['profilePic'] = config('evibe.gallery.host') . '/cakes/' . $cake['id'] . '/results/' . $profilePic['url'];

							}
							else
							{
								$temp['profilePic'] = config('evibe.gallery.host') . '/img/icons/balloons.png';
							}
							$data[$count++] = $temp;

						}

						return $data;
						break;

				}

				break;
		}

		return $listPageProducts;
	}

	public function addProfilePic($packages)
	{
		$data = [];
		$count = 0;
		foreach ($packages as $product)
		{
			$temp = [];
			$temp = $product->toarray();
			if ($product->getProfileImg())
			{
				$temp['profilePic'] = $product->getProfileImg();
			}
			else
			{
				$temp['profilePic'] = config('evibe.gallery.host') . '/img/icons/balloons.png';
			}
			$data[$count++] = $temp;

		}

		return $data;
	}
}