<?php

namespace App\Http\Controllers\Base;

use App\Models\Service\ServiceTags;
use App\Models\Types\TypeServices;
use App\Models\Types\TypeTag;
use App\Models\Util\Country;
use Illuminate\Support\Facades\Cache;

class BaseEntController extends BaseResultsController
{
	public function __construct()
	{
		parent::__construct();

		$this->setPageId(config('evibe.ticket.type.entertainment'));
	}

	public function getEntListData($cityUrl, $profileRoute, $categoryUrl = "", $fetchWithoutView = false, $collectionId = null)
	{
		$urlParams = [['key' => 'city', 'value' => $cityUrl]];
		$validUrlObj = $this->validateUrlParams($urlParams);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$cacheKey = $this->deviceSpecificCacheKey(request()->fullUrl());
		$cacheKey = base64_encode($cacheKey);
		if (Cache::has($cacheKey) && !($fetchWithoutView))
		{
			$listData = Cache::get($cacheKey);
		}
		else
		{
			$listData = $this->getEntListDataFromDB($cityUrl, $profileRoute, $categoryUrl, $collectionId);
			if (!($fetchWithoutView))
			{
				Cache::put($cacheKey, $listData, config('evibe.cache.refresh-time'));
			}
		}

		return $listData;
	}

	public function getEntListDataFromDB($cityUrl, $profileRoute, $categoryUrl = "", $collectionId)
	{
		$options = [];
		$pageId = $this->getPageId();
		$pageName = $this->getPageName();
		$eventId = $this->getOccasionId();
		$eventUrl = $this->getOccasionUrl();
		$cityName = getCityName();
		$cityId = getCityId();
		$clearFilter = false;
		$userFilterId = null;
		$categoryTag = "";
		$categoryTagsServiceIds = [];

		$categoryTags = TypeTag::select('id', 'seo_title', 'seo_desc', 'identifier', 'url', 'name')
		                       ->forEvent($eventId)
		                       ->forPage(config('evibe.ticket.type.entertainment'))
		                       ->filterable()
		                       ->get();
		$allCategoryIds = $categoryTags->pluck('id')->toArray();

		if ($categoryUrl)
		{
			$clearFilter = true;
			// getting data separately without join because, we need seo data from type_tags table
			$categoryTag = $categoryTags->where('url', $categoryUrl)
			                            ->first();

			if ($categoryTag)
			{
				$categoryTagsServiceIds = ServiceTags::where('tag_id', $categoryTag->id)
				                                     ->pluck('service_id')
				                                     ->toArray();
			}
		}
		if ($collectionId)
		{
			$tagIds = [];
			foreach (config('evibe.occasion-landingpages') as $occasion)
			{
				foreach ($occasion as $collection)
				{
					if (isset($collection['id']) && $collection['id'] == $collectionId)
					{
						$tagIds = array_keys($collection['ids']);
					}
				}
			}
			if ($tagIds)
			{
				$categoryTagsServiceIds = ServiceTags::whereIn('tag_id', $tagIds)
				                                     ->pluck('service_id')
				                                     ->toArray();
			}
		}

		// @see: entertainment options are from services
		$services = TypeServices::with('gallery')
		                        ->forEvent($eventId)
		                        ->select('type_service.*')
		                        ->forCity(getCityId())
		                        ->isLive();

		// If category Exists
		if (count($categoryTagsServiceIds))
		{
			$services->whereIn('type_service.id', $categoryTagsServiceIds);
		}

		// If search exists
		$userSearch = "";
		if (request()->has('search') && trim(request()->get('search')))
		{
			$clearFilter = true;
			$userSearch = trim(request()->get('search'));
			$searchWords = explode(' ', $userSearch);

			$services->where(function ($searchQuery) use ($searchWords) {
				foreach ($searchWords as $word)
				{
					$searchQuery->orWhere('name', 'LIKE', "%$word%")
					            ->orWhere('code', 'LIKE', "%$word%");
				}
			});
		}

		// @see: do not change this position
		// we need to get total count
		// to get category counts
		$allServices = $services->groupBy('type_service.id')->get();

		// filter:price
		$priceFilterApplied = false;
		$minPrices = $services->pluck('min_price');
		$maxPrices = $services->pluck('max_price');
		$calMinPrice = $services->pluck('min_price')->min();
		$calMaxPrice = $services->pluck('min_price')->max();
		$calMinPrice = $calMinPrice ? $calMinPrice : 0; // setting to 0 if $calMinPrice is null
		//$calMaxPrice = ($maxPrices->max() > $minPrices->max()) ? $maxPrices->max() : $minPrices->max();
		$calMaxPrice = $calMaxPrice ? $calMaxPrice : $maxPrices->max();
		$priceMin = request()->get('price_min') ?: 0;
		$priceMax = request()->get('price_max') ?: 0;
		if ($priceMin || $priceMax)
		{
			$priceFilterApplied = true;
		}
		else
		{
			$priceMin = $priceMin ? $priceMin : $calMinPrice;
			$priceMax = ($priceMax && $priceMax >= $priceMin) ? $priceMax : $calMaxPrice;
		}

		if ($priceMin > $priceMax)
		{
			$priceTemp = $priceMax;
			$priceMax = $priceMin;
			$priceMin = $priceTemp;
		}

		// user has changed min / max price
		if ($calMinPrice != $priceMin || $calMaxPrice != $priceMax)
		{
			$clearFilter = true;
			$services->whereBetween('min_price', [$priceMin, $priceMax]);
		}

		// SEO
		if ($categoryUrl)
		{
			$categoryName = $categoryUrl ? $categoryTags->where('url', $categoryUrl)->first() : "";
			$categoryName = $categoryName ? $categoryName->seo_title : "";

			$seoReplaces = [
				'{occasion}' => preg_replace('/[-]/', ' ', $eventUrl),
				'{category}' => $categoryName ?: 'services',
			];

			$defaultKey = "defaults.city.$eventId.$pageId.filter";
			$seo = $this->getSeoText([getCityId(), $eventId, $pageId, "filter"], $defaultKey, $seoReplaces);
		}
		else
		{
			$defaultKey = "defaults.city.$eventId.$pageId.list";
			$seo = $this->getSeoText([getCityId(), $eventId, $pageId, "list"], $defaultKey);
		}

		$itemsListKeys = [
			"Evibe.in",
			$cityName,
			getTextFromSlug($eventUrl),
			getTextFromSlug($pageName)
		];
		$pageTitle = $seo['pt'];
		$pageDescription = $seo['pd'];
		$pageHeader = $seo['ph'];
		$keyWords = $seo['kw'];
		$schema = [$this->schemaBreadcrumbList($itemsListKeys)];

		// sort results
		// if default (popularity), sort by priority
		// else sort by user input (ex: price, new-arrivals)
		$sortParams = $this->getSortParams('type_service', ['price' => 'type_service.min_price']);
		if (!$sortParams['isDefault'])
		{
			// groupBy is fix for distinct
			$services = $services->groupBy('type_service.id')
			                     ->orderBy($sortParams['entity'], $sortParams['order'])
			                     ->get();
			$arrayServices = $services->all();
		}
		else
		{
			$services = $services->groupBy('type_service.id')->orderBy('type_service.created_at', 'DESC')->get(); // default sort
			$allServiceIds = $services->pluck('id')->all(); // get all IDs for merge
			$arrayServices = $services->all();

			if (is_null($userFilterId))
			{
				$priorityIds = $this->getItemIdsByPriority($eventId, $pageId, $allServiceIds); // get priorities
			}
			else
			{
				$params = [
					'eventId'         => $eventId,
					'pageId'          => $pageId,
					'userFilterId'    => $userFilterId,
					'allFilterTagIds' => $categoryTags->pluck('id')->toArray(),
					'allItemIds'      => $allServiceIds
				];

				$priorityIds = $this->getItemIdsByPriorityForTags($params);
			}

			usort($arrayServices, function ($a, $b) use ($priorityIds) {
				return $this->sortItemsByPriority($a, $b, $priorityIds);
			});
		}

		$cacheProductIds = [];
		foreach ($services as $service)
		{
			$serviceUrl = route($profileRoute, [$cityUrl, $service->url]);
			//$profileImg = $service->getProfilePic();
			$serviceData = [
				'id'        => $service->id,
				'name'      => $service->name,
				'info'      => $service->info,
				'worth'     => $service->worth_price,
				'minPrice'  => $service->min_price,
				'maxPrice'  => $service->max_price,
				'rangeInfo' => $service->range_info,
				//'profileImg' => $profileImg,
				'fullPath'  => $serviceUrl,
				'code'      => $service->code,
			];

			// schema
			$schemaData = [
				'category' => $pageName,
				'name'     => $service->name,
				//'image'    => $profileImg,
				'price'    => $service->min_price
			];
			array_push($schema, $this->schemaService($schemaData));

			array_push($options, $serviceData);

			array_push($cacheProductIds, $service->code);
		}

		// data related to price categories
		$priceCategoryData = [
			'min'          => $calMinPrice,
			'max'          => $calMaxPrice,
			'mapTypeId'    => config('evibe.ticket.type.entertainment'),
			'typeTicketId' => $this->getPageId(),
			'eventId'      => $eventId,
			'cityId'       => getCityId()
		];

		$data = [
			'type'            => 'ent', // hardcoded
			'occasionId'      => $eventId,
			'cityId'          => $cityId,
			'mapTypeId'       => $pageId,
			'options'         => $options,
			'totalCount'      => count($options),
			'sort'            => $sortParams['sortType'],
			'filters'         => [
				'queryParams'        => $this->getExistingQueryParams(),
				'active'             => $categoryUrl,
				'search'             => $userSearch,
				'clearFilter'        => $clearFilter,
				'catCounts'          => $this->getFilterCategoryCounts($allServices, $allCategoryIds),
				'priceMin'           => $priceMin,
				'priceMax'           => $priceMax,
				'priceCategories'    => $this->getPriceCategoryFilters($priceCategoryData),
				'allCategories'      => $categoryTags,
				'priceFilterApplied' => $priceFilterApplied
			],
			'seo'             => [
				'pageTitle'       => $pageTitle,
				'pageDescription' => $pageDescription,
				'pageHeader'      => $pageHeader,
				'keyWords'        => $keyWords,
				'schema'          => $schema
			],
			'countries'       => Country::all(),
			'cacheProductIds' => $cacheProductIds
		];

		// cancellation policy
		$data['cancellationData'] = $this->calculateCustomerCancellationRefund();
		if ($collectionId)
		{
			return $data;
		}
		$data = array_merge($data, $this->mergeView($cityUrl, $eventId));

		return $data;
	}

	public function getEntProfileData($cityUrl, $entUrl, $hashTag = "#PartyEntertainment")
	{
		$urlParams = [
			['key' => 'city', 'value' => $cityUrl],
			['key' => 'type_service', 'value' => $entUrl]
		];
		$validUrlObj = $this->validateUrlParams($urlParams);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$service = $validUrlObj['type_service'];

		// update product page views counter
		updateProfileViewCounter($this->getPageId(), $service->id);

		$cacheKey = $this->deviceSpecificCacheKey(request()->fullUrl());
		$cacheKey = base64_encode($cacheKey);
		if (Cache::has($cacheKey))
		{
			$profileData = Cache::get($cacheKey);
		}
		else
		{
			$profileData = $this->getEntProfileDataFromDB($validUrlObj, $hashTag);
			Cache::put($cacheKey, $profileData, config('evibe.cache.refresh-time'));
		}

		return $profileData;

	}

	public function getEntProfileDataFromDB($validUrlObj, $hashTag)
	{
		$service = $validUrlObj['type_service'];

		$cityName = getCityName();
		$cityId = getCityId();
		$eventId = $this->getOccasionId();
		$pageId = $this->getPageId();
		$pageUrl = request()->capture()->fullUrl();

		$gallery = [];
		$rawGallery = $service->gallery;
		foreach ($rawGallery as $item)
		{
			if (!empty($item->getLink()))
			{
				array_push($gallery, [
					'id'    => $item->id,
					'type'  => $item->type_id,
					'url'   => $item->getLink("profile"),
					'thumb' => $item->getLink('thumb'),
					//'title' => $item->title,
					'title' => $service->name,
				]);
			}
		}

		$gallery = collect($gallery)->sortByDesc('type')->values()->toArray();

		// SEO
		$defaultKey = "defaults.city.$eventId.$pageId.profile";
		$replaces = [
			'{item}'  => strtolower($service->name),
			'{price}' => $this->formatPrice($service->min_price)
		];

		$seo = $this->getSeoText([$cityId, $eventId, $pageId, "profile"], $defaultKey, $replaces);
		$pageTitle = $seo['pt'];
		$pageDescription = $seo['pd'];
		$pageHeader = $seo['ph'];
		$keyWords = $seo['kw'];

		// schema
		$breadcrumbItemList = [
			"Evibe.in",
			getCityName(),
			$this->getPageName(),
			$service->name
		];
		$serviceData = [
			'category'    => $this->getPageName(),
			'name'        => $service->name,
			'price'       => $service->min_price,
			'description' => $service->info,
			'image'       => $service->getProfilePic()
		];
		$schema = [$this->schemaBreadcrumbList($breadcrumbItemList), $this->schemaService($serviceData)];

		// Social share
		$metaImage = isset($gallery[0]) && isset($gallery[0]['url']) ? $gallery[0]['url'] : "";
		$serviceTitle = $service->name . " In " . $cityName;
		$shareUrl = $this->getShareUrl($pageUrl, $serviceTitle, $hashTag, $metaImage);

		$data = [
			'id'         => $service->id,
			'code'       => $service->code,
			'name'       => $service->name,
			'info'       => $service->info,
			'worth'      => $service->worth_price,
			'minPrice'   => $service->min_price,
			'maxPrice'   => $service->max_price,
			'rangeInfo'  => $service->range_info,
			'preq'       => $service->prereq,
			'terms'      => $service->terms,
			'facts'      => $service->facts,
			'gallery'    => $gallery,
			'seo'        => [
				'pageTitle'       => $pageTitle,
				'pageDescription' => $pageDescription,
				'pageHeader'      => $pageHeader,
				'keyWords'        => $keyWords,
				'schema'          => $schema,
				'pageUrl'         => $pageUrl,
				'metaImage'       => $metaImage
			],
			'shareUrl'   => $shareUrl,
			'occasionId' => $eventId,
			'mapTypeId'  => $pageId,
			'cityId'     => $cityId,
			'countries'  => Country::all()
		];

		// shortlist data
		$shortlistDataParams = [
			'mapId'     => $service->id,
			'mapTypeId' => $this->getPageId(),
		];

		$data = array_merge($data, $this->mergeShortlistData($shortlistDataParams));

		// @see: no partner

		if ($this->agent->isMobile() && !($this->agent->isTablet()))
		{
			$data['deliveryImages'] = $this->fetchDeliveryImages($service->id, $this->getPageId());
			$deviceSpecificData = $this->getEntProfileMobileSpecificData($service, $data);

		}
		else
		{
			$deviceSpecificData = $this->getEntProfileDesktopSpecificData($service, $data);
		}

		return $deviceSpecificData;
	}

	private function getEntProfileMobileSpecificData($service, $data)
	{
		$previousUrl = url()->previous();
		$currentUrl = url()->current();
		$eventId = $this->getOccasionId();

		// needle: config - 'entertainment' should match routes
		if (($previousUrl != $currentUrl) && ($eventId == config('evibe.occasion.house-warming.id')) &&
			(strpos($previousUrl, config('evibe.results_url.add_ons')) != false)
		)
		{
			$data['backUrl'] = $previousUrl;
		}
		elseif ($previousUrl != $currentUrl &&
			(strpos($previousUrl, config('evibe.results_url.entertainment')) != false)
		)
		{
			$data['backUrl'] = $previousUrl;
		}

		return $data;
	}

	private function getEntProfileDesktopSpecificData($service, $data)
	{
		$eventId = $this->getOccasionId();
		$data['pageName'] = $this->getPageName();

		$data = array_merge($data, $this->mergeView(getCityUrl(), $eventId));

		return $data;
	}
}