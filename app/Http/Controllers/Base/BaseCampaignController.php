<?php

namespace App\Http\Controllers\Base;

class BaseCampaignController extends BasePackageController
{
	protected function getCampaignPackages()
	{
		return [
			323 => [
				'price'       => 7500,
				'price_worth' => 12000,
				'price_max'   => 0
			],
			386 => [
				'price'       => 6500,
				'price_worth' => 8500,
				'price_max'   => 0
			],
			579 => [
				'price'       => 15000,
				'price_worth' => 20000,
				'price_max'   => 0
			],
			295 => [
				'price'       => 14000,
				'price_worth' => 20000,
				'price_max'   => 0
			],
			404 => [
				'price'       => 10000,
				'price_worth' => 15000,
				'price_max'   => 0
			],
			591 => [
				'price'       => 9500,
				'price_worth' => 15000,
				'price_max'   => 0
			]
		];
	}

	protected function getCampaignPackageIds()
	{
		return collect($this->getCampaignPackages())->keys();
	}

	public function getCampaignPackagePrices($packageId)
	{
		$packages = $this->getCampaignPackages();
		$selectedPackage = $packages[$packageId];

		return [
			'price'    => $selectedPackage['price'],
			'worth'    => $selectedPackage['price_worth'],
			'priceMax' => $selectedPackage['price_max']
		];
	}

	public function setGiftHamperPackageIds()
	{
		$giftPackages = [702, 703, 706];

		return $giftPackages;
	}

	public function isChristmasGiftHamperPackage($packageId)
	{
		$giftPackages = $this->setGiftHamperPackageIds();

		return in_array($packageId, $giftPackages);
	}
}
