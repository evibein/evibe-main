<?php

namespace App\Http\Controllers\Base;

use App\Models\Cake\Cake;
use App\Models\Cake\CakeGallery;
use App\Models\Cake\CakeTags;
use App\Models\Types\TypeTag;
use App\Models\Util\Country;
use App\Models\Util\DeliverySlot;
use Illuminate\Support\Facades\Cache;

class BaseCakeController extends BaseResultsController
{
	public function __construct()
	{
		parent::__construct();

		$this->setPageId(config('evibe.ticket.type.cake'));
	}

	public function getCakeListData($cityUrl, $fetchWithoutView = false, $collectionId = null)
	{
		$urlParams = [['key' => 'city', 'value' => $cityUrl]];
		$validUrlObj = $this->validateUrlParams($urlParams);
		$filteredCat = null;
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$cacheKey = $this->deviceSpecificCacheKey(request()->fullUrl());
		$cacheKey = base64_encode($cacheKey);
		if (Cache::has($cacheKey) && !($fetchWithoutView))
		{
			$listData = Cache::get($cacheKey);
		}
		else
		{
			$listData = $this->getCakeListDataFromDB($cityUrl, $collectionId);
			Cache::put($cacheKey, $listData, config('evibe.cache.refresh-time'));
		}

		return $listData;
	}

	public function getCakeListDataFromDB($cityUrl, $collectionId = null)
	{
		// pagination & filter variables
		$pageId = $this->getPageId();
		$eventId = $this->getOccasionId();
		$eventUrl = $this->getOccasionUrl();
		$clearFilter = false;
		$selectedCategoryTags = [];

		$cakes = Cake::with('provider', 'gallery')
		             ->isLive()
		             ->forEvent($eventId)
		             ->forCity();

		// Category Filter
		$categoryTags = TypeTag::select('id', 'seo_title', 'seo_desc', 'identifier', 'url', 'name', 'parent_id')
		                       ->forEvent($eventId)
		                       ->filterable();
		if ($collectionId)
		{
			$tagsIds = [];
			foreach (config('evibe.occasion-landingpages.birthday-cakes') as $collection)
			{
				if ($collectionId == $collection['id'])
				{
					$tagIds = $collection['ids'];
				}
			}
			if (count($tagIds))
			{
				$categoryTags = TypeTag::select('id', 'seo_title', 'seo_desc', 'identifier', 'url', 'name', 'parent_id')
				                       ->forEvent($eventId)
				                       ->whereIn('id', array_keys($tagIds))
				                       ->filterable();
			}
		}

		$relevantParentTags = [
			'Theme' => config("evibe.occasion.kids_birthdays.cakes.gender_cat_id"),
			'Type'  => config("evibe.occasion.kids_birthdays.cakes.type_cat_id")
		];

		if ($eventId == config('evibe.occasion.kids_birthdays.id'))
		{
			$categoryTags->where(function ($query) use ($relevantParentTags) {
				$query->whereNull('parent_id')
				      ->orWhereIn('id', array_values($relevantParentTags))
				      ->orWhereIn('parent_id', array_values($relevantParentTags));
			});
		}
		else
		{
			$categoryTags->whereNull('parent_id');
		}

		$categoryTags = $categoryTags->get()->keyBy('id');

		// Search query
		$searchQuery = request()->input('search');
		if ($searchQuery)
		{
			// @see: if 'trim' can be used...
			$searchWords = explode(' ', $searchQuery);
			$clearFilter = true;

			$cakes = $cakes->where(function ($searchQuery) use ($searchWords) {
				foreach ($searchWords as $word)
				{
					$searchQuery->orWhere('cake.title', 'LIKE', "%$word%")
					            ->orWhere('cake.code', 'LIKE', "%$word%");
				}
			});
		}

		$defaultPriceRange = $this->getDefaultMinMaxPrice($eventId);
		$defaultMinPrice = $defaultPriceRange['min'];
		$defaultMaxPrice = $defaultPriceRange['max'];

		// Price filter
		$priceFilterApplied = false;
		$minPrice = request()->has('price_min') ? (int)request()->input('price_min') : 0;
		$maxPrice = request()->has('price_max') ? request()->input('price_max') : 0;
		if ($minPrice > $maxPrice)
		{
			$priceTemp = $maxPrice;
			$maxPrice = $minPrice;
			$minPrice = $priceTemp;
		}

		if ($minPrice || $maxPrice)
		{
			$priceFilterApplied = true;

			$cakes->where(function ($query) use ($minPrice, $maxPrice) {
				if ($minPrice && $maxPrice)
				{
					$query->whereBetween('cake.price_per_kg', [$minPrice, $maxPrice]);
				}
				elseif (!$minPrice && $maxPrice)
				{
					$query->where('cake.price_per_kg', '<=', $minPrice);
				}
				elseif ($minPrice && !$maxPrice)
				{
					$query->where('cake.price_per_kg', '>=', $minPrice);
				}
			});
		}
		else
		{
			// @see: applicable when price filter is implemented
			$minPrice = $defaultMinPrice;
			$maxPrice = $defaultMaxPrice;
		}

		// category filter
		$active = "";
		$categoryUrl = request()->has('theme') ? request('theme') : '';
		$categoryUrl = request()->has('cake-type') ? request('cake-type') : '';
		$filterData = $this->getFilterData($eventId, $pageId, $categoryUrl);

		$allTagIdsBank = $filterData['allTagIdsBank'];
		$allFilterTagIds = $filterData['allFilterTagIds'];
		$userFilterId = null;
		if ($categoryUrl || $collectionId)
		{
			// Check whether it is having the pipe in the string to enable the multiple category selection
			if ($categoryUrl && strpos($categoryUrl, '__') !== false)
			{
				$categoryUrls = explode("__", $categoryUrl);
				$categoryUrls = count($categoryUrls) > 0 ? $categoryUrls : [];
			}
			else
			{
				$categoryUrls = [$categoryUrl];
			}
			if ($collectionId)
			{
				$selectedCategoryTags = $categoryTags->pluck("url", "id")->toArray();
			}
			else
			{
				$selectedCategoryTags = array_intersect($categoryTags->pluck("url", "id")->toArray(), $categoryUrls);
			}

			if (count($selectedCategoryTags) > 0)
			{
				$categoryCakeTags = CakeTags::whereIn('tag_id', array_keys($selectedCategoryTags))
				                            ->pluck('cake_id')
				                            ->toArray();

				$cakes->whereIn('cake.id', $categoryCakeTags);
			}

			$active = $categoryUrl;
			$clearFilter = true;
		}

		// SEO
		$defaultKey = "defaults.city.$eventId.$pageId.list";
		$seo = $this->getSeoText([getCityId(), $eventId, $pageId, "list"], $defaultKey);
		$itemsListKeys = [
			"Evibe.in",
			getCityName(),
			getTextFromSlug($this->getOccasionUrl()),
			getTextFromSlug($this->getPageName())
		];

		// given user filter is valid -> filter results
		if (count($selectedCategoryTags) > 0)
		{
			$filterTagName = "";
			$replaces = [
				'{tagTitle}' => "",
				'{tagDesc}'  => "",
				'{tagName}'  => ""
			];

			$count = 0;
			foreach ($selectedCategoryTags as $key => $selectedCategoryTag)
			{
				if (isset($categoryTags[$key]))
				{
					$categoryTag = $categoryTags[$key];
					$filterTagName = $categoryTag->identifier ? $filterTagName . ($count == 0 ? "" : " , ") . $categoryTag->identifier : $filterTagName;

					$replaces = [
						'{tagTitle}' => $categoryTag->seo_title ? $replaces['{tagTitle}'] . ($count == 0 ? "" : " , ") . $categoryTag->seo_title : $replaces['{tagTitle}'],
						'{tagDesc}'  => ($categoryTag->seo_desc ? $replaces['{tagDesc}'] . ($count == 0 ? "" : " , ") . $categoryTag->seo_desc : $replaces['{tagDesc}']),
						'{tagName}'  => $categoryTag->identifier ? $replaces['{tagName}'] . ($count == 0 ? "" : " , ") . $categoryTag->identifier : $replaces['{tagName}']
					];

					$count++;
				}
			}
			$defaultKey = "defaults.city.$eventId.$pageId.filter";
			$seo = $this->getSeoText([getCityId(), $eventId, $pageId, "filter"], $defaultKey, $replaces);

			$clearFilter = true;
			$userFilterId = $key;
		}

		$pageTitle = $seo['pt'];
		$pageDescription = $seo['pd'];
		$pageHeader = $seo['ph'];
		$keyWords = $seo['kw'];
		$schema = [$this->schemaBreadcrumbList($itemsListKeys)];

		// get filtered packages data
		$cakesList = [];

		$cakes = $cakes->select('cake.*');

		$perPage = config('evibe.paginate-options.generic-desktop', 51);
		if ($this->agent->isMobile() && !($this->agent->isTablet()))
		{
			$perPage = config('evibe.paginate-options.generic-mobile', 50);
		}
		if ($collectionId)
		{
			$perPage = 8;
		}
		$totalCakeCount = $cakes->count(); // total count for pagination

		// sort results
		// if default (popularity), sort by priority
		// else sort by user input (ex: price, new-arrivals)
		$sortParams = $this->getSortParams('cake', ['price' => 'cake.price_per_kg']);
		if (!$sortParams['isDefault'])
		{
			// groupBy is fix for distinct
			$cakes = $cakes->distinct()
			               ->orderBy($sortParams['entity'], $sortParams['order']);

			// fetch page specific options - get()
			$currentPageOptions = $this->getCurrentPageOption($cakes, $perPage);
			$cakes = $currentPageOptions['options'];
			$currentPage = $currentPageOptions['currentPage'];
			$cakes = $cakes->all();
		}
		else
		{
			$allCakeIds = $cakes->pluck('id')->all(); // get all IDs for merge

			$cakes = $cakes->distinct()
			               ->orderBy('cake.created_at', 'DESC')->get(); // default sort

			if (is_null($userFilterId))
			{
				$priorityIds = $this->getItemIdsByPriority($eventId, $pageId, $allCakeIds, false);  // get priorities
			}
			else
			{
				$params = [
					'eventId'         => $eventId,
					'pageId'          => $pageId,
					'userFilterId'    => $userFilterId,
					'allFilterTagIds' => $allFilterTagIds,
					'allItemIds'      => $allCakeIds
				];

				$priorityIds = $this->getItemIdsByPriorityForTags($params);
			}

			$cakes = $cakes->all(); // to Array for sorting

			usort($cakes, function ($a, $b) use ($priorityIds) {
				return $this->sortItemsByPriority($a, $b, $priorityIds);
			});

			// fetch page specific options - get()
			$currentPageOptions = $this->getCurrentPageOption($cakes, $perPage);
			$cakes = $currentPageOptions['options'];
			$currentPage = $currentPageOptions['currentPage'];

			$cakes = $cakes->all(); // to Array for sorting
		}

		$cacheProductIds = [];
		foreach ($cakes as $cake)
		{
			// remove inactive planner packages & venue packages
			if (!$cake->provider)
			{
				continue;
			}

			$profileImg = $cake->getProfileImg();
			$cakeData = [
				'id'           => $cake->id,
				'code'         => $cake->code,
				'name'         => $cake->title,
				'url'          => $cake->url,
				'info'         => $cake->info,
				'price'        => $cake->price,
				'price_per_kg' => $cake->price_per_kg,
				'price_worth'  => $cake->price_worth,
				'views_count'  => $cake->views_count,
				'min_time'     => $cake->min_time,
				'min_order'    => $cake->min_order,
				'type'         => $cake->type == config('evibe.cake.egg') ? 'egg' : 'eggless',
				'flavours'     => $cake->flavours,
				'priority'     => $cake->priority,
				'fullUrl'      => $cake->getLink($eventUrl),
				//'profileImg'   => $profileImg,
				'provider'     => [
					'id'   => $cake->provider_id,
					'code' => $cake->provider->code,
					'url'  => $cake->provider->getLink(),
					'type' => $cake->provider->type->name
				]
			];

			array_push($cacheProductIds, $cake->code);

			if ($cake->provider->rating)
			{
				$cakeData['provider']['reviewCount'] = $cake->provider->rating->review_count;
				$cakeData['provider']['avgRating'] = $cake->provider->rating->avg_rating;
			}

			$shortlistDataParams = [
				'mapId'     => $cake->id,
				'mapTypeId' => $this->getPageId(),
			];

			$cakeData = array_merge($cakeData, $this->mergeShortlistData($shortlistDataParams));

			// schema
			$productData = [
				'category' => $this->getPageName(),
				'name'     => $cake->title,
				'image'    => $profileImg,
				'price'    => $cake->price
			];
			array_push($schema, $this->schemaProduct($productData));

			$cakesList[] = $cakeData;
		}

		// paginate cakes
		$cakeList = $this->getPaginatedItems($totalCakeCount, $cakesList, $currentPage, $perPage);

		$data = [
			'cakes'           => $cakeList,
			'totalCount'      => $totalCakeCount,
			'occasionId'      => $eventId,
			'cityId'          => getCityId(),
			'mapTypeId'       => $pageId,
			'filters'         => [
				'active'             => $active,
				'queryParams'        => $this->getExistingQueryParams(),
				'clearFilter'        => $clearFilter,
				'search'             => $searchQuery,
				'allCategories'      => $categoryTags,
				'defaultMinPrice'    => $defaultMinPrice,
				'defaultMaxPrice'    => $defaultMaxPrice,
				'catCounts'          => $this->getFilterCategoryCounts($cakes, $allTagIdsBank),
				'relevantParentTags' => $relevantParentTags,
				'priceFilterApplied' => $priceFilterApplied
			],
			'sort'            => $sortParams['sortType'],
			'seo'             => [
				'pageTitle'       => $pageTitle,
				'pageHeader'      => $pageHeader,
				'pageDescription' => $pageDescription,
				'keyWords'        => $keyWords,
				'schema'          => $schema
			],
			'countries'       => Country::all(),
			'cacheProductIds' => $cacheProductIds
		];
		if ($collectionId)
		{
			if ($collectionId == 6)
			{
			}

			return $data;
		}
		$data = array_merge($data, $this->mergeView($cityUrl, $eventId));

		return $data;
	}

	public function getCakeProfileData($cityUrl, $cakeUrl, $hashTag = "#evibe")
	{
		$urlParams = [
			['key' => 'city', 'value' => $cityUrl],
			['key' => 'cake', 'value' => $cakeUrl]
		];
		$validUrlObj = $this->validateUrlParams($urlParams);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$cake = $validUrlObj['cake'];

		// update product page views counter
		updateProfileViewCounter($this->getPageId(), $cake->id);

		$cacheKey = $this->deviceSpecificCacheKey(request()->fullUrl());
		$cacheKey = base64_encode($cacheKey);
		if (Cache::has($cacheKey))
		{
			$profileData = Cache::get($cacheKey);
		}
		else
		{
			$profileData = $this->getCakeProfileDataFromDb($validUrlObj, $cityUrl, $hashTag);
			Cache::put($cacheKey, $profileData, config('evibe.cache.refresh-time'));
		}

		return $profileData;
	}

	public function getCakeProfileDataFromDb($validUrlObj, $cityUrl, $hashTag)
	{
		$cake = $validUrlObj['cake'];
		$pageId = $this->getPageId();
		$eventUrl = $this->getOccasionUrl();
		$eventId = $this->getOccasionId();
		$cityName = getCityName();
		$partnerRatings = $cake->provider->getPartnerReview();

		// SEO
		$defaultKey = "defaults.city.$eventId.$pageId.profile";
		$replaces = [
			'{item}'  => strtolower($cake->title),
			'{price}' => $this->formatPrice($cake->price)
		];

		$seo = $this->getSeoText([getCityId(), $eventId, $pageId, "profile"], $defaultKey, $replaces);
		$pageTitle = $seo['pt'];
		$pageDescription = $seo['pd'];
		$pageHeader = $seo['ph'];
		$keyWords = $seo['kw'];
		$cakeTitle = $cake->title . " In " . $cityName;

		// schema
		$breadcrumbItemList = [
			"Evibe.in",
			$cityName,
			$this->getPageName(),
			$cake->title
		];
		$schema = [$this->schemaBreadcrumbList($breadcrumbItemList)];
		$avgRating = $partnerRatings['total']['avg'];
		$ratingCount = $partnerRatings['total']['count'];
		$productData = [
			'category'    => $this->getPageName(),
			'name'        => $cake->title,
			'image'       => $cake->getProfileImg(),
			'avgRating'   => $avgRating,
			'ratingCount' => $ratingCount,
			'price'       => $cake->price,
			'description' => $cake->info
		];
		array_push($schema, $this->schemaProduct($productData));

		// gallery
		$galleryItems = CakeGallery::where('cake_id', $cake->id)
		                           ->orderBy('is_profile', 'DESC')
		                           ->orderBy('updated_at', 'DESC')
		                           ->get();
		$cakeGallery = [];
		$cakeGallery['images'] = [];
		$cakeGallery['videos'] = [];

		foreach ($galleryItems as $item)
		{
			$type = 'images';

			if ($item->type == config('evibe.gallery.type.image'))
			{
				$cakeGallery[$type][] = [
					'id'    => $item->id,
					//'title' => $item->title ? $item->title : $cake->title,
					'title' => $cake->title,
					'url'   => $item->getLink("profile"),
					'thumb' => $item->getLink("thumbs")
				];
			}
			else
			{
				$type = 'videos';
				$gallery[$type][] = [
					'id'    => $item->id,
					'url'   => $item->url,
					'title' => $item->title
				];
			}
		}

		// Social share
		$pageUrl = request()->capture()->fullUrl();
		$metaImage = isset($cakeGallery['images'][0]) && isset($cakeGallery['images'][0]['url']) ? $cakeGallery['images'][0]['url'] : "";
		$shareUrl = $this->getShareUrl($pageUrl, $cakeTitle, $hashTag, $metaImage);

		$cakeData = [
			'id'             => $cake->id,
			'code'           => $cake->code,
			'name'           => $cake->title,
			'info'           => $cake->info,
			'gallery'        => $cakeGallery,
			'base_price'     => $cake->price,
			'price_per_kg'   => $cake->price_per_kg,
			'price_worth'    => $cake->price_worth,
			'views_count'    => $cake->views_count,
			'flavour'        => $cake->recommendedFlavour ? $cake->recommendedFlavour->name : null,
			'fullUrl'        => $cake->getLink($eventUrl),
			'cakeAddOns'     => $this->getDynamicCakeFieldsWithCategory($cake->id),
			'slots'          => DeliverySlot::all(),
			'provider'       => [
				'id'   => $cake->provider_id,
				'code' => $cake->provider->code,
				'url'  => $cake->provider->getLink(),
				'type' => $cake->provider->type->name
			],
			'ratings'        => $partnerRatings,
			'occasionId'     => $eventId,
			'isAutoBookable' => $this->isAutoBookableItem($pageId, $cake->id),
			'seo'            => [
				'pageTitle'       => $pageTitle,
				'pageDescription' => $pageDescription,
				'pageHeader'      => $pageHeader,
				'keyWords'        => $keyWords,
				'schema'          => $schema,
				'pageUrl'         => $pageUrl,
				'metaImage'       => $metaImage
			],
			'shareUrl'       => $shareUrl,
			'mapTypeId'      => $pageId,
			'countries'      => Country::all()
		];

		// tags
		$cakeData['tags'] = [];
		$cakeTags = $cake->tags;
		foreach ($cakeTags as $cakeTag)
		{
			$cakeData['tags'][] = [
				'id'       => $cakeTag->id,
				'tag_name' => $cakeTag->name
			];
		}

		// cancellation policy
		$cakeData['cancellationData'] = $this->calculateCustomerCancellationRefund();

		$shortlistDataParams = [
			'mapId'     => $cake->id,
			'mapTypeId' => $pageId,
		];

		$cakeData = array_merge($cakeData,
		                        $this->mergeShortlistData($shortlistDataParams),
		                        $this->mergeView($cityUrl, $eventId));

		$cakeData['deliveryImages'] = $this->fetchDeliveryImages($cake->id, $this->getPageId());

		return $cakeData;
	}

	private function getDefaultMinMaxPrice($eventId)
	{
		$data = [
			'min' => 0,
			'max' => 0
		];

		$cakes = Cake::isLive()
		             ->forEvent($eventId)
		             ->forCity(getCityId())
		             ->whereNull('cake.deleted_at')
		             ->select('cake.*');

		$data['min'] = $cakes->min('cake.price_per_kg');
		$data['max'] = $cakes->max('cake.price_per_kg');

		if ($data['min'] == $data['max'])
		{
			$data['min'] = 10;
		}

		return $data;
	}

	/**
	 * @param $selectedValues : should be type of array with all selected values as set of array
	 * @param $cakeId
	 *
	 * @return array
	 */
	public function calculateCakePriceFromBackend($selectedValues, $cakeId)
	{
		$existingValues = $this->getDynamicCakeFieldsWithCategory($cakeId);
		$catTypeWeight = config('evibe.cake_cat_value.weight');
		$catTypeFlavour = config('evibe.cake_cat_value.flavour');
		$catTypeEggless = config('evibe.cake_cat_value.eggless');
		$price = $worth = $weight = 0;

		foreach ($selectedValues as $selectedValue)
		{
			// @see, Don't change the order
			if ($selectedValue['catId'] == $catTypeWeight)
			{
				$weightPrice = $existingValues[$catTypeWeight]['values'][$selectedValue['name']];
				$price = $weightPrice['price'];
				$worth = $weightPrice['priceWorth'] ? $weightPrice['priceWorth'] : $weightPrice['price'];
				$weight = $weightPrice['weight'];
			}
			elseif ($selectedValue['catId'] == $catTypeFlavour)
			{
				$flavourPrice = $existingValues[$catTypeFlavour]['values'][$selectedValue['name']];

				if ($flavourPrice['isFixed'])
				{
					$calculatedPrice = $this->getFixedPrice($flavourPrice, $price, $worth);
					$price = $calculatedPrice['price'];
					$worth = $calculatedPrice['worth'];
				}
				else
				{
					// calculate price based on kg
					$calculatedPrice = $this->getPriceByWeight($flavourPrice, $price, $worth, $weight);
					$price = $calculatedPrice['price'];
					$worth = $calculatedPrice['worth'];
				}

			}
			elseif ($selectedValue['catId'] == $catTypeEggless)
			{
				$eggLessPrice = $existingValues[$catTypeEggless]['values'][$selectedValue['name']];

				if ($eggLessPrice['isFixed'])
				{
					$calculatedPrice = $this->getFixedPrice($eggLessPrice, $price, $worth);
					$price = $calculatedPrice['price'];
					$worth = $calculatedPrice['worth'];
				}
				else
				{
					// calculate price based on kg
					$calculatedPrice = $this->getPriceByWeight($eggLessPrice, $price, $worth, $weight);
					$price = $calculatedPrice['price'];
					$worth = $calculatedPrice['worth'];
				}
			}
		}

		return ['price' => $price, 'worth' => $worth];
	}

	private function getFixedPrice($data, $price, $worth)
	{
		$localWorth = $data['priceWorth'] ? $data['priceWorth'] : $data['price'];
		$price = $price + $data['price'];
		$worth = $worth + $localWorth;

		return ['price' => $price, 'worth' => $worth];
	}

	private function getPriceByWeight($data, $price, $worth, $weight)
	{
		$localWorth = $data['priceWorth'] ? $data['priceWorth'] : $data['price'];
		$price = $price + ($weight * $data['price']);
		$worth = $worth + $localWorth;

		return ['price' => $price, 'worth' => $worth];
	}
}