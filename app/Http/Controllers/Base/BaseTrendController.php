<?php

namespace App\Http\Controllers\Base;

class BaseTrendController extends BaseResultsController
{
	public function __construct()
	{
		parent::__construct();
		$this->setPageId(config('evibe.ticket.type.trend'));
	}

}