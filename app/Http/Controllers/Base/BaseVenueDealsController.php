<?php

namespace App\Http\Controllers\Base;

use App\Models\Package\Package;
use App\Models\Types\TypeTag;
use App\Models\Util\Area;
use App\Models\Types\TypeSlot;

use Evibe\Facades\EvibeUtilFacade as EvibeUtil;

class BaseVenueDealsController extends BasePackageController
{
	public function __construct()
	{
		parent::__construct();
		$this->setPageId(config('evibe.ticket.type.venue-deals'));
	}

	public function getVenueDealsListData($cityUrl, $location, $category)
	{
		// validate url params & redirect to results if not valid
		$urlParams = [['key' => 'city', 'value' => $cityUrl]];
		$validUrlObj = $this->validateUrlParams($urlParams);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$clearFilter = false;
		$eventId = $this->getOccasionId();
		$eventUrl = $this->getOccasionUrl();
		$pageId = $this->getPageId();
		$pageName = $this->getPageName();
		$cityName = getCityName();
		$locSeoTxt = "$cityName";
		$catSeoTxt = null;
		$itemsListKeys = [
			"Evibe.in",
			$cityName,
			getTextFromSlug($eventUrl),
			getTextFromSlug($pageName)
		];
		$schema = [$this->schemaBreadcrumbList($itemsListKeys)];

		// deals list
		$venueDealsList = [];
		$venueDeals = Package::with('tags', 'provider', 'provider.city')
		                     ->forEvent($eventId)
		                     ->forPage($pageId)
		                     ->isLive()
		                     ->forCity()
		                     ->select('planner_package.*');

		// filter by location
		$allLocations = $this->getLocationForFilters($venueDeals);
		$userLocation = $this->trimSpace($location);
		if ($userLocation && in_array($userLocation, $allLocations['urls']))
		{
			$validLocation = Area::where('url', $userLocation)->first();
			if ($validLocation)
			{
				$clearFilter = true;
				$locSeoTxt = $validLocation->name;
				$venueDeals->joinVenue()
				           ->where('venue.area_id', $validLocation->id);
			}
			else
			{
				$location = "all";
			}
		}

		// filter by category
		$allCategories = TypeTag::where(['map_type_id' => $pageId, 'type_event' => $eventId, 'is_filter' => 1])
		                        ->select('identifier', 'url')
		                        ->get();
		$userCategory = $this->trimSpace($category);
		if ($category && collect($allCategories)->contains('url', $userCategory))
		{
			$tag = TypeTag::where('url', $userCategory)->first();
			if ($tag)
			{
				// tags are already joined in forPage, so using directly here
				$venueDeals->joinTags()->where('planner_package_tags.tile_tag_id', $tag->id);
				$catSeoTxt = strtolower($tag->name);
			}
			else
			{
				$category = "all";
			}
		}

		// filter by guests count
		$calMinGuests = $venueDeals->pluck('guests_min')->min();
		$calMaxGuests = $venueDeals->pluck('guests_max')->max();
		$guestsMin = (int)trim(request()->get('guests_min'));
		$guestsMax = (int)trim(request()->get('guests_max'));
		$guestsMin = $guestsMin ? $guestsMin : $calMinGuests;
		$guestsMax = $guestsMax ? $guestsMax : $calMaxGuests;

		if (($guestsMin != $calMinGuests || $guestsMax != $calMaxGuests) && $guestsMax >= $guestsMin)
		{
			$clearFilter = true;
			$venueDeals->where(function ($capQuery) use ($guestsMin, $guestsMax)
			{
				$capQuery->where(function ($cq1) use ($guestsMin)
				{
					$cq1->where('guests_min', '<=', $guestsMin)
					    ->where('guests_max', '>=', $guestsMin);
				})
				         ->orWhere(function ($cq2) use ($guestsMax)
				         {
					         $cq2->where('guests_min', '<=', $guestsMax)
					             ->where('guests_max', '>=', $guestsMax);
				         });
			});
		}

		// filter by search term
		$userSearch = "";
		if (request()->has('search') && trim(request()->get('search')))
		{
			$clearFilter = true;
			$userSearch = trim(request()->get('search'));
			$searchWords = explode(' ', $userSearch);

			$venueDeals->where(function ($searchQuery) use ($searchWords)
			{
				foreach ($searchWords as $word)
				{
					$searchQuery->orWhere('planner_package.name', 'LIKE', "%$word%")
					            ->orWhere('planner_package.code', 'LIKE', "%$word%");
				}
			});
		}

		// filter by price
		$priceFilterApplied = false;
		$minPrices = $venueDeals->pluck('price');
		$maxPrices = $venueDeals->pluck('price_max');
		$calMinPrice = $venueDeals->pluck('price')->min();
		$calMinPrice = $calMinPrice ? $calMinPrice : 0; // setting to 0 if $calMinPrice is null
		$calMaxPrice = ($maxPrices->max() > $minPrices->max()) ? $maxPrices->max() : $minPrices->max();
		$priceMin = request()->get('price_min');
		$priceMax = request()->get('price_max');
		if($priceMin || $priceMax)
		{
			$priceFilterApplied = true;
		}
		$priceMin = $priceMin ? $priceMin : $calMinPrice;
		$priceMax = ($priceMax && $priceMax >= $priceMin) ? $priceMax : $calMaxPrice;

		// user has changed min / max price
		if ($calMinPrice != $priceMin || $calMaxPrice != $priceMax)
		{
			$clearFilter = true;
			$venueDeals->where(function ($query1) use ($priceMin, $priceMax)
			{
				$query1->where(function ($query11) use ($priceMin, $priceMax)
				{
					$query11->where('price_max', '=', 0)
					        ->where('price', '>=', $priceMin)
					        ->where('price', '<=', $priceMax);
				})->orWhere(function ($query12) use ($priceMin, $priceMax)
				{
					$query12->where('price_max', '!=', 0)
					        ->where('price', '<=', $priceMin)
					        ->where('price_max', '>=', $priceMin)
					        ->where('price_max', '<=', $priceMax);
				})->orWhere(function ($query13) use ($priceMin, $priceMax)
				{
					$query13->where('price_max', '!=', 0)
					        ->where('price', '>=', $priceMin)
					        ->where('price', '<=', $priceMax)
					        ->where('price_max', '>=', $priceMax);
				})->orWhere(function ($query14) use ($priceMin, $priceMax)
				{
					$query14->where('price_max', '!=', 0)
					        ->where('price', '>=', $priceMin)
					        ->where('price_max', '<=', $priceMax);
				});
			});
		}

		$venueDeals = $venueDeals->groupBy('planner_package.id')
		                         ->orderBy('planner_package.created_at', 'DESC')
		                         ->get();
		$venueDealsId = $venueDeals->pluck('id')->all(); // get all IDs for merge
		$venueDeals = $venueDeals->all(); // to array
		$priorityIds = $this->getItemIdsByPriority($eventId, $pageId, $venueDealsId); // get priorities
		usort($venueDeals, function ($a, $b) use ($priorityIds)
		{
			return $this->sortItemsByPriority($a, $b, $priorityIds);
		});

		// fetch venue deal data
		foreach ($venueDeals as $venueDeal)
		{
			$provider = $venueDeal->provider;
			if (!$provider || !$provider->is_live)
			{
				continue; // remove inactive provider deals
			}

			$venueArea = $provider->area ? $provider->area->name : '';
			$venueDealData = [
				'id'           => $venueDeal->id,
				'code'         => $venueDeal->code,
				'name'         => $venueDeal->name,
				'info'         => $venueDeal->info,
				'url'          => $venueDeal->url,
				'price'        => $venueDeal->price,
				'worth'        => $venueDeal->price_worth,
				'discount'     => $venueDeal->getDiscountPercentage(),
				'priceMax'     => $venueDeal->price_max,
				'profilePic'   => $venueDeal->getProfileImg(),
				'guestMin'     => $venueDeal->guests_min,
				'guestMax'     => $venueDeal->guests_max,
				'provider'     => [
					'id'        => $provider->id,
					'code'      => $provider->code,
					'url'       => $provider->getLink(),
					'type'      => $provider->type->name,
					'avgRating' => $provider->avgRating(),
					'area'      => $venueArea
				],
				'customFields' => []
			];

			// custom fields
			$customCategories = config('evibe.pkg_fields')['cat'];
			foreach ($customCategories as $key => $customCategory)
			{
				$fieldValues = $this->getCustomFieldValuesByCat($customCategory['id'], $eventId, $venueDeal->id);
				$venueDealData['customFields'][$key] = $fieldValues;

				// collect `individual` fields as direct array values
				if ($customCategory['id'] == config('evibe.pkg_fields.cat.individual.id'))
				{
					foreach ($fieldValues as $fieldValue)
					{
						$customKey = $this->getDefaultCustomKeyByCurrentKey($fieldValue['key']);
						$venueDealData[$customKey] = $fieldValue['value'];
					}
				}
			}

			// ratings
			$ratings = $venueDeal->getAvgRating();
			$venueDealData['ratingsCount'] = $ratings['count'];
			$venueDealData['avgRating'] = $ratings['value'];

			$venueDealsList[] = $venueDealData;

			// schema
			$schemaData = [
				'name'        => $provider->name,
				'address'     => $provider->full_address,
				'location'    => $venueArea,
				'avgRating'   => $ratings['value'],
				'ratingCount' => $ratings['count'],
				'priceRange'  => $venueDeal->price
			];
			array_push($schema, $this->schemaLocalBusiness($schemaData));
		}

		// getting venue deals paginated data with auto booking
		$venueDeals = $this->getPaginatedAutoBookableItems($pageId, $venueDealsList, 21);

		// SEO
		$keyType = $clearFilter ? ($catSeoTxt ? "filter-cat" : "filter") : "list";
		$defaultKey = "defaults.city.$eventId.$pageId.$keyType";
		$replaces = [
			'{location}' => $locSeoTxt,
			'{category}' => $catSeoTxt,
			'{list-cat}' => $catSeoTxt ? " at $catSeoTxt" : ""
		];
		$seo = $this->getSeoText([getCityId(), $eventId, $pageId, $keyType], $defaultKey, $replaces);

		$data = [
			'venueDeals'     => $venueDeals['paginatedItems'],
			'occasionId'     => $this->getOccasionId(),
			'cityId'         => getCityId(),
			'mapTypeId'      => $this->getPageId(),
			'autoBookingIds' => $venueDeals['autoBook'],
			'filters'        => [
				'allLocations'  => $allLocations,
				'allCategories' => $allCategories,
				'queryParams'   => $this->getExistingQueryParams(),
				'priceMin'      => $priceMin,
				'priceMax'      => $priceMax,
				'guestsMin'     => $guestsMin,
				'guestsMax'     => $guestsMax,
				'search'        => $userSearch,
				'clearFilter'   => $clearFilter,
				'location'      => $location,
				'category'      => $category
			],
			'seo'            => [
				'pageTitle'       => ucfirst($seo['pt']),
				'pageDescription' => $seo['pd'],
				'pageHeader'      => ucwords($seo['ph']),
				'keyWords'        => $seo['kw'],
				'schema'          => $schema
			]
		];

		$data = array_merge($data, $this->mergeView($cityUrl, $eventId));

		return $data;
	}

	public function getVenueDealProfileData($cityUrl, $profileUrl, $hashTag = "#VenueDeals")
	{
		$urlParams = [['key' => 'city', 'value' => $cityUrl], ['key' => 'venue-deals', 'value' => $profileUrl]];
		$validUrlObj = $this->validateUrlParams($urlParams);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$venueDeal = $validUrlObj['planner-package'];
		$eventId = $this->getOccasionId();
		$cityName = getCityName();
		$cityId = getCityId();
		$pageId = $this->getPageId();
		$pageName = $this->getPageName();
		$provider = $venueDeal->provider;
		$metaImage = config('evibe.gallery.host') . config('evibe.gallery.logo_path');
		$partySlots = TypeSlot::all();

		$gallery = $this->getPackageCategorisedGallery($venueDeal->id, $venueDeal->name);
		$itinerary = $this->getPackageItinerary($venueDeal);
		$ratings = $this->getPackageRatings($venueDeal, "venueDeal");

		// SEO
		$defaultKey = "defaults.city.$eventId.$pageId.profile";
		$venueLocation = $provider->area ? $provider->area->name : "$cityName";
		$venueName = $provider->name ? trim($provider->name) : "hall";
		$replaces = [
			'{venue}'    => $venueName,
			'{item}'     => $venueDeal->name,
			'{location}' => $venueLocation
		];
		$pageUrl = request()->capture()->fullUrl();
		$venueDealTitle = $venueDeal->name . " In " . $cityName;
		$shareUrl = $this->getShareUrl($pageUrl, $venueDealTitle, $hashTag, $metaImage);
		$seo = $this->getSeoText([$cityId, $eventId, $pageId, "profile"], $defaultKey, $replaces);
		$breadcrumbItemList = [
			"Evibe.in",
			$cityName,
			$pageName,
			$venueDeal->title
		];
		$lbData = [
			'name'        => $provider->name,
			'address'     => $provider->full_address,
			'description' => $venueDeal->info,
			'avgRating'   => $ratings['venueDeal']['avgRating'],
			'ratingCount' => $ratings['venueDeal']['count'],
			'priceRange'  => $venueDeal->price
		];

		$schema = [$this->schemaBreadcrumbList($breadcrumbItemList), $this->schemaLocalBusiness($lbData)];

		$data = [
			'venueDeal'      => $venueDeal,
			'partySlots'     => $partySlots,
			'occasionId'     => $eventId,
			'cityId'         => $cityId,
			'mapTypeId'      => $pageId,
			'gallery'        => $gallery,
			'itinerary'      => $itinerary,
			'tags'           => $venueDeal->getTags(),
			'similarDeals'   => $venueDeal->getSimilarPackages($eventId),
			'seo'            => [
				'pageTitle'       => $seo['pt'],
				'pageDescription' => $seo['pd'],
				'pageHeader'      => $seo['ph'],
				'keyWords'        => $seo['kw'],
				'schema'          => $schema,
				'pageUrl'         => $pageUrl,
				'metaImage'       => $metaImage
			],
			'shareUrl'       => $shareUrl,
			'ratings'        => $ratings,
			'extRatings'     => $venueDeal->provider ? $venueDeal->provider->prepareExtRatings() : '',
			'provider'       => $venueDeal->provider ? $venueDeal->provider->prepareData() : '',
			'isAutoBookable' => $this->isAutoBookableItem($pageId, $venueDeal->id),
			'isRentType'     => false // initialize
		];

		// custom fields
		$customCategories = config('evibe.pkg_fields')['cat'];
		foreach ($customCategories as $key => $customCategory)
		{
			$fieldValues = $this->getCustomFieldValuesByCat($customCategory['id'], $eventId, $venueDeal->id);
			$data['customFields'][$key] = $fieldValues;

			// collect `individual` fields as direct array values
			if ($customCategory['id'] == config('evibe.pkg_fields.cat.individual.id'))
			{
				foreach ($fieldValues as $fieldValue)
				{
					$customKey = $this->getDefaultCustomKeyByCurrentKey($fieldValue['key']);
					$data[$customKey] = $fieldValue['value'];
				}
			}
		}

		// check if rent type
		if (isset($data['rent_duration']) && $data['rent_duration'])
		{
			$data['isRentType'] = true;
		}
		$data = array_merge($data, $this->mergeView($cityUrl, $eventId));

		$shortlistDataParams = [
			'mapId'     => $venueDeal->id,
			'mapTypeId' => $this->getPageId(),
		];
		$data = array_merge($data, $this->mergeShortlistData($shortlistDataParams));

		return $data;
	}

	protected function getTopLocations($area, $count = 3)
	{
		$locationCount = 0;
		$locationName = [];

		foreach ($area as $item)
		{
			$locationName[] = $item['name'];
			$locationCount++;

			if ($locationCount == $count)
			{
				break;
			}
		}

		$top3Location = $this->getFormattedString($locationName);

		return $top3Location;
	}

	protected function getTopCategories($tags, $count = 3)
	{
		$catCount = 0;
		$catName = [];
		foreach ($tags as $item)
		{
			$catName[] = $item->identifier;
			$catCount++;

			if ($catCount == $count)
			{
				break;
			}
		}

		$top3Category = $this->getFormattedString($catName);

		return $top3Category;
	}

	private function getFormattedString($arr)
	{
		$last = array_pop($arr);
		$formattedString = count($arr) ? implode(", ", $arr) . ' & ' . $last : $last;

		return $formattedString;
	}
}