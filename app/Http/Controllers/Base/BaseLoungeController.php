<?php

namespace App\Http\Controllers\Base;

class BaseLoungeController extends BasePackageController
{
	public function __construct()
	{
		parent::__construct();

		$this->setPageId(config('evibe.ticket.type.lounges'));
	}

	public function getLoungeListData($cityUrl, $profileUrl = null)
	{
		$profileUrl = !is_null($profileUrl) ? $profileUrl : config('evibe.occasion.bachelor.lounge.profile_url');
		$loungeListData = $this->getListData($cityUrl);
		if (is_array($loungeListData))
		{
			$host = config('evibe.host');
			$loungeListData['profileBaseUrl'] = "$host/$cityUrl/" . $this->getOccasionUrl() . "/$profileUrl/";
		}

		return $loungeListData;
	}

	public function getLoungeProfileData($cityUrl, $foodUrl, $hashTag = "#Resorts")
	{
		return $this->getProfileData($cityUrl, $foodUrl, $hashTag, "resort");
	}
}