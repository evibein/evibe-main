<?php

namespace App\Http\Controllers\Base;

class BaseTentController extends BasePackageController
{
	public function __construct()
	{
		parent::__construct();

		$this->setPageId(config('evibe.ticket.type.tents'));
	}

	public function getTentListData($cityUrl, $profileUrl)
	{
		$profileUrl = !is_null($profileUrl) ? $profileUrl : config('evibe.occasion.kids_birthdays.tents.profile_url');
		$tentListData = $this->getListData($cityUrl);

		if (is_array($tentListData))
		{
			$host = config('evibe.host');
			$tentListData['profileBaseUrl'] = "$host/$cityUrl/" . $this->getOccasionUrl() . "/$profileUrl/";
		}

		return $tentListData;
	}

	public function getTentProfileData($cityUrl, $priestUrl, $hashTag = "#TentServices")
	{
		return $this->getProfileData($cityUrl, $priestUrl, $hashTag);
	}
}