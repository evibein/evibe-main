<?php

namespace App\Http\Controllers\Base;

class BaseSiteVisitController extends BaseController
{
	public function __construct()
	{
		parent::__construct();

		// @see: nothing here. Do not delete this method
	}
}