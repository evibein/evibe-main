<?php

namespace App\Http\Controllers\Base;

class BaseFoodController extends BasePackageController
{
	public function __construct()
	{
		parent::__construct();

		$this->setPageId(config('evibe.ticket.type.food'));
	}

	public function getFoodListData($cityUrl, $profileUrl, $fetchWithoutView = false, $collectionId = null)
	{
		$foodListData = $this->getListData($cityUrl, 20, true, null, [], $fetchWithoutView, null, $collectionId);
		if (is_array($foodListData))
		{
			$host = config('evibe.host');
			$foodListData['profileBaseUrl'] = "$host/$cityUrl/" . $this->getOccasionUrl() . "/$profileUrl/";
		}

		return $foodListData;
	}

	public function getFoodProfileData($cityUrl, $foodUrl, $hashTag = "#evibe")
	{
		return $this->getProfileData($cityUrl, $foodUrl, $hashTag, "food");
	}
}