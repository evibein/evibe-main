<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Base\BaseController;
use App\Jobs\Emails\Availability\MailAutoBookingAvailabilityCheckAlertToTeamJob;
use App\Jobs\Emails\Tickets\MailAutoBookingTicketAlertToTeamJob;
use App\Jobs\Emails\Util\MailCustomerFailToProceedABToTeamJob;
use App\Models\Cake\Cake;
use App\Models\Decor\Decor;
use App\Models\Package\Package;
use App\Models\Service\Service;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketBookingGallery;
use App\Models\Ticket\TicketGallery;
use App\Models\Trend\Trend;
use App\Models\Types\TypeEvent;
use App\Models\Types\TypeTicket;
use App\Models\Util\City;
use Carbon\Carbon;
use Evibe\Passers\Util\SiteLogData;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class AjaxAutoBookingController extends BaseController
{
	public function alertTeamOnEnteringMobileNumber($ticketId)
	{
		$res = ['success' => false];
		try
		{
			$phone = request('phone');
			if ($phone)
			{
				$ticket = Ticket::find($ticketId);
				if (!$ticket)
				{
					Log::error('ticket not found');

					return response()->json($res);
				}

				$booking = $ticket->bookings()->first();
				if (!$booking)
				{
					Log::error('ticket booking not found');

					return response()->json($res);
				}

				$ticket->phone = $phone;

				$ticket->save();

				$occasionName = null;
				$occasion = TypeEvent::find($ticket->event_id);
				if ($occasion)
				{
					$occasionName = $occasion->name;
				}

				$partyDate = $ticket->event_date == null ? '--' : date('d/m/y', $ticket->event_date);

				$typeTicketName = null;
				$typeTicket = TypeTicket::find($ticket->type_ticket_id);
				if ($typeTicket)
				{
					$typeTicketName = $typeTicket->name;
				}

				$cityName = "not city specific (Campaign)";
				$city = City::find($ticket->city_id);
				if ($city)
				{
					$cityName = $city->name;
				}

				$data = [
					'ticketId'                  => $ticket->id,
					'phone'                     => $ticket->phone,
					'name'                      => $ticket->name,
					'email'                     => $ticket->email,
					'pinCode'                   => $ticket->zip_code,
					'occasion'                  => $occasionName,
					'city'                      => $cityName,
					'partyDate'                 => $partyDate,
					'typeTicket'                => $typeTicketName,
					'bookingId'                 => $booking->booking_id,
					'bookingInfo'               => $booking->booking_info,
					'estimatedTransportCharges' => $booking->estimated_transport_charges
				];

				// Alert team that a customer has pre-checked out an auto-book option
				//$this->dispatch(new MailAutoBookingTicketAlertToTeamJob($data));

				return response()->json(['success'  => true,
				                         'oldPhone' => $data['phone']]);
			}
			else
			{
				return response()->json($res);
			}
		} catch (\Exception $e)
		{
			Log::error($e->getMessage());

			// log site error
			$siteLogData = new SiteLogData();
			$siteLogData
				->setUrl(request()->fullUrl())
				->setErrorCode($e->getCode())
				->setTrace($e->getTraceAsString())
				->setException($e->getMessage());

			$this->logSiteError($siteLogData);

			return response()->json(['error' => true]);
		}
	}

	public function alertTeamOnCustomerDidNotProceed($mapId, $typeTicketId, $occasionId)
	{
		try
		{
			if ($mapId && $typeTicketId)
			{
				$optionName = null;
				$optionCode = null;
				$optionCity = null;
				$optionUrl = null;
				$optionInfo = null;

				// input data
				$phone = request('phone');
				$partnerPinCode = request('partnerPinCode');
				$partyPinCode = request('partyPinCode');
				$productFeasibility = request('productFeasibility');
				$typeDevice = request('typeDevice');
				$typeBrowser = request('typeBrowser');
				$partyDist = request('partyDist');
				$distFreeMeters = request('distFreeMeters');
				$transCharges = request('transCharges');
				$isValidOtherCities = request('otherCitiesLimitStatus');

				// @see: feasibility - Green = 1; Blue = 2; Red = 3;
				$feasibility = $productFeasibility == 1 ? 1 : 3;
				if ($feasibility == 1 && $transCharges > 0)
				{
					$feasibility = 2;
				}

				switch ($typeTicketId)
				{
					case config('evibe.ticket.type.package'):
						$option = Package::find($mapId);
						$optionName = $option->name;
						$optionCode = $option->code;
						$optionCity = $option->city_id;
						$optionUrl = $option->url;
						break;

					case config('evibe.ticket.type.decor'):
						$option = Decor::with('provider')->find($mapId);
						$optionName = $option->name;
						$optionCode = $option->code;
						$optionCity = $option->provider->city_id;
						$optionUrl = $option->url;
						break;

					case config('evibe.ticket.type.cake'):
						$option = Cake::find($mapId);
						$optionName = $option->title;
						$optionCode = $option->code;
						$optionUrl = $option->url;
						break;

					case config('evibe.ticket.type.trend'):
						$option = Trend::find($mapId);
						$optionName = $option->name;
						$optionCode = $option->code;
						$optionCity = $option->city_id;
						$optionUrl = $option->url;
						break;

					case config('evibe.ticket.type.service'):
						$option = Service::find($mapId);
						$optionName = $option->name;
						$optionUrl = $option->url;
						break;
				}

				$name = $optionName ? $optionName : "--";
				$code = $optionCode ? $optionCode : "--";
				$cityId = $optionCity ? $optionCity : getCityId();
				$cityName = "Default";
				$occasionName = "Default";

				if ($cityId > 0)
				{
					$city = City::find($cityId);
					$cityName = $city->name;
				}
				if ($occasionId)
				{
					$occasionName = TypeEvent::find($occasionId)->name;
				}

				$statusId = config('evibe.ticket.status.initiated');
				if ($partyDist > 100000 && $isValidOtherCities == "no")
				{
					$statusId = config('evibe.ticket.status.irrelevant');
				}

				$inputData = [
					"phone"             => $phone,
					"city_id"           => $cityId,
					"comments"          => "Customer checked the feasibility of the product.",
					"enquiry_source_id" => config("evibe.ticket.enquiry_source.feasibility_check"),
					"status_id"         => $statusId
				];

				if ($phone && strlen($phone) == 10)
				{
					$pastTicket = Ticket::where('phone', 'LIKE', $phone)
					                    ->where('created_at', '>', Carbon::now()->subDays(10)->startOfDay())
					                    ->where('ticket.status_id', config('evibe.ticket.status.initiated'))
					                    ->first();

					if ($pastTicket)
					{
						$ticket = $pastTicket;

						if ($partyDist > 100000 && $isValidOtherCities == "no")
						{
							$this->updateTicketAction([
								                          'ticket'   => $ticket,
								                          'comments' => "Customer entered pincode is not feasible & not in a range of 100KM"
							                          ]);
						}
					}
					else
					{
						$ticket = Ticket::create($inputData);

						$this->updateTicketAction([
							                          'ticket'   => $ticket,
							                          'comments' => "Auto booking ticket created by customer"
						                          ]);
					}
				}
				else
				{
					$ticket = Ticket::create($inputData);

					$this->updateTicketAction([
						                          'ticket'   => $ticket,
						                          'comments' => "Auto booking ticket created by customer"
					                          ]);
				}

				$ticketId = $ticket->id;

				$data = [
					"phone"          => $phone,
					"optionName"     => $name,
					"optionCode"     => $code,
					"optionCity"     => $cityName,
					"optionType"     => TypeTicket::find($typeTicketId)->name,
					"optionEvent"    => $occasionName,
					"partyPinCode"   => $partyPinCode,
					"partnerPinCode" => $partnerPinCode,
					"feasibility"    => $feasibility,
					"typeDevice"     => $typeDevice,
					"typeBrowser"    => $typeBrowser,
					"partyDist"      => number_format(($partyDist / 1000), 2),
					"distFree"       => number_format(($distFreeMeters / 1000), 2),
					"transCharges"   => $transCharges,
					"ticketId"       => $ticketId,
					"dashLink"       => config('evibe.dash_host') . "/tickets/" . $ticketId . "/information"
				];

				if ($partyDist < 100000 || $isValidOtherCities == "yes")
				{
					$this->dispatch(new MailCustomerFailToProceedABToTeamJob($data));
				}

				return response()->json(['success' => true]);
			}

			return response()->json(['success' => false]);
		} catch (\Exception $exception)
		{
			$siteLogData = new SiteLogData();
			$siteLogData
				->setUrl(route("ajax.auto-book.failed-to-proceed"))
				->setErrorCode($exception->getCode());

			$this->logSiteError($siteLogData);

			return response()->json(['error' => true]);
		}
	}

	public function uploadCustomerImages($ticketId)
	{
		$images = Input::file('images');

		if (!count($images) || (count($images) && is_null($images[0])))
		{
			return response()->json([
				                        'success'  => false,
				                        'errorMsg' => 'Kindly choose an image to upload'
			                        ]);
		}

		if (count($images) > 25)
		{
			return response()->json([
				                        'success'  => false,
				                        'errorMsg' => 'Upload not more than 25 images'
			                        ]);
		}

		foreach ($images as $image)
		{
			$imageRules = [
				'file' => 'required|mimes:png,jpeg,jpg,JPG|max:1024'
			];

			$imageMessages = [
				'file.required' => 'Please upload at least one image',
				'file.mimes'    => 'One of the images is not valid. (only .png, .jpeg, .jpg are accepted)',
				'file.max'      => 'Image size cannot be more than 1 MB'
			];

			$imageValidator = Validator::make(['file' => $image], $imageRules, $imageMessages);

			if ($imageValidator->fails())
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => $imageValidator->messages()->first()
				                        ]);
			}
		}

		try
		{
			$directPath = '/ticket/' . $ticketId . '/images/';

			foreach ($images as $image)
			{
				$option = ['isWatermark' => false];

				$imageName = $this->uploadImageToServer($image, $directPath, $option);

				$imgInsertData = [
					'url'        => $imageName,
					'title'      => $this->getImageTitle($image),
					'ticket_id'  => $ticketId,
					'created_at' => Carbon::now(),
					'updated_at' => Carbon::now()
				];

				TicketGallery::create($imgInsertData);
			}

			return response()->json([
				                        'success' => true,
			                        ]);
		} catch (\Exception $exception)
		{
			Log::error('Error occurred while saving images');
		}
	}

	public function checkAvailability()
	{
		$optionId = request('optionId');
		$optionTypeId = request('optionTypeId');
		$partyDate = request('partyDate');
		$partyTime = request('partyTime');

		$rules = [
			'partyDate' => 'sometimes|required|date|after:yesterday',
			'partyTime' => 'sometimes|required',
			'phone'     => 'required|digits:10'
		];

		$messages = [
			'phone.required'     => 'Your phone number is required',
			'phone.digits'       => 'Phone number is not valid. Enter only 10 digits number (ex: 9640204000)',
			'partyDate.required' => 'Please enter your party date',
			'partyDate.after'    => 'Please enter a party date from today',
			'partyTime.required' => 'Party time in Party Info screen is required',
			'partyTime'          => 'Please enter your party time'
		];

		$validation = Validator::make(request()->all(), $rules, $messages);

		if ($validation->fails())
		{
			return response()->json([
				                        'success'         => false,
				                        'errorMsg'        => $validation->messages()->first(),
				                        'validationError' => true
			                        ]);
		}

		$data = [
			'optionId'     => $optionId,
			'optionTypeId' => $optionTypeId,
			'partyDate'    => $partyDate,
			'partyTime'    => $partyTime
		];

		$checkCategory = [
			config('evibe.ticket.type.surprises')
		];

		// @todo: if applied for surprises, consider "is_venue_booking"
		if (in_array($optionTypeId, $checkCategory))
		{
			$url = config('evibe.api.base_url') . config('evibe.api.product.availability-check');

			try
			{
				$client = new Client();
				$res = $client->request('POST', $url, [
					'json' => $data
				]);

				$res = $res->getBody();
				$res = \GuzzleHttp\json_decode($res, true);

				return response()->json($res);
			} catch (\Exception $exception)
			{
				$errorMessage = "failed to fetch availability for optionId: $optionId and optionTypeId: $optionTypeId";
				$siteLogData = new SiteLogData();
				$siteLogData->setUrl(route("ajax.auto-book.check-availability"))
				            ->setException($errorMessage)
				            ->setErrorCode($exception->getCode());

				$this->logSiteError($siteLogData);

				return response()->json([
					                        'success'      => false,
					                        'error'        => true,
					                        "errorMessage" => $errorMessage
				                        ]);
			}
		}
		else
		{
			return response()->json(['success' => true]);
		}
	}

	public function alertTeamOnFailedAvailabilityCheck()
	{
		$optionId = request('optionId');
		$optionTypeId = request('optionTypeId');
		$phone = request('phone');
		$occasionId = request('occasionId');
		$partyDate = request('partyDate');
		$partyTime = request('partyTime');
		$data = false;

		$checkCategory = [
			config('evibe.ticket.type.surprises')
		];

		if (in_array($optionTypeId, $checkCategory))
		{
			// @todo: get occasion from table or config (using route names)
			$option = Package::find($optionId);
			$occasion = TypeEvent::find($occasionId);
			$city = City::find($option->city_id);
			$optionUrl = config('evibe.host') . "/" . $city->url;
			$optionUrl .= "/" . $occasion->url;
			$optionUrl .= "/" . config('evibe.occasion.' . config('evibe.type-event-route-name.' . $occasionId) . '.' . config('evibe.type-product-route-name.' . $optionTypeId) . '.results_url');
			$optionUrl .= "/" . $option->url;

			$data = [
				'optionName'  => $option->name,
				'optionCode'  => $option->code,
				'optionUrl'   => $optionUrl,
				'optionType'  => config('evibe.type-product-title.' . $optionTypeId),
				'optionEvent' => $occasion->name,
				'optionCity'  => $city->name,
				'phone'       => $phone,
				'partyDate'   => $partyDate,
				'partyTime'   => $partyTime
			];
		}

		if ($data)
		{
			$this->dispatch(new MailAutoBookingAvailabilityCheckAlertToTeamJob($data));
		}
	}

	public function getDynamicPrice()
	{
		try
		{

			$date = request('date');
			$cityId = request('cityId');
			$occasionId = request('occasionId');
			$productTypeId = request('productTypeId');
			$productId = request('productId');

			$date = date('m/d/y', strtotime($date));
			$cacheKey = "original-price-" . $productTypeId . "_" . $productId;
			$productPriceData = [];

			if (Cache::has($cacheKey))
			{
				$productPriceData = Cache::get($cacheKey);
			}
			else
			{
				switch ($productTypeId)
				{
					case config('evibe.ticket.type.villas'):
						$product = Package::find($productId);
						if ($product)
						{
							$productPriceData = [
								'price'              => $product->price,
								'priceWorth'         => $product->price_worth,
								'groupCount'         => $product->group_count,
								'minGuestCount'      => $product->guests_min,
								'maxGuestCount'      => $product->guests_max,
								'pricePerExtraGuest' => $product->price_per_extra_guest
							];
							Cache::put($cacheKey, $productPriceData, config('evibe.cache.refresh-time'));
						}
						else
						{
							Log::error("Unable to find villa package: " . $productTypeId . " - " . $productId);
						}
				}
			}

			$updatedProductData = $this->calculateDynamicParams($productPriceData,
			                                                    $date,
			                                                    $cityId,
			                                                    $occasionId,
			                                                    $productTypeId,
			                                                    $productId);

			return response()->json([
				                        'success'            => true,
				                        'productPrice'       => (isset($updatedProductData['price']) && $updatedProductData['price']) ? $updatedProductData['price'] : $productPriceData['price'],
				                        'priceWorth'         => (isset($updatedProductData['priceWorth']) && $updatedProductData['priceWorth']) ? $updatedProductData['priceWorth'] : $productPriceData['priceWorth'],
				                        'groupCount'         => (isset($updatedProductData['groupCount']) && $updatedProductData['groupCount']) ? $updatedProductData['groupCount'] : $productPriceData['groupCount'],
				                        'minGuestCount'      => (isset($updatedProductData['minGuestCount']) && $updatedProductData['minGuestCount']) ? $updatedProductData['minGuestCount'] : $productPriceData['minGuestCount'],
				                        'maxGuestCount'      => (isset($updatedProductData['maxGuestCount']) && $updatedProductData['maxGuestCount']) ? $updatedProductData['maxGuestCount'] : $productPriceData['maxGuestCount'],
				                        'pricePerExtraGuest' => (isset($updatedProductData['pricePerExtraGuest']) && $updatedProductData['pricePerExtraGuest']) ? $updatedProductData['pricePerExtraGuest'] : $productPriceData['pricePerExtraGuest'],
			                        ]);

		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);

			return response()->json([
				                        'success' => false
			                        ]);
		}
	}
}