<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Base\BaseResultsController;
use App\Models\Cake\Cake;
use App\Models\Decor\Decor;
use App\Models\Package\Package;
use App\Models\OptionReview\OptionReview;
use App\Models\OptionReview\OptionReviewGallery;
use App\Http\Models\Partner\DeliveryGallery;
use App\Models\Search\SearchableProduct;
use App\Models\OccasionHome\OccasionHomeCards;
use App\Models\Types\TypeEvent;
use App\Models\Types\TypeServices;
use App\Models\Ticket\TicketBooking;
use App\Models\ViewCount\ProfileViewCounter;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AjaxProfilePagesController extends BaseResultsController
{
	public function fetchSimilarProducts($mapId, $mapTypeId, $eventId)
	{
		try
		{
			$occasion = TypeEvent::find($eventId);
			$invalidProfile = false;

			$data = [
				'cityUrl'   => getCityUrl(),
				'occasion'  => $occasion,
				'mapTypeId' => $mapTypeId,
			];

			$view = "occasion.util.profile.d_components.similar-products-section";
			$count = 18;

			if ($this->agent->isMobile() && !($this->agent->isTablet()))
			{
				$view = "occasion.util.profile.m_components.similar-products-section";
				$count = 12;
			}

			// Not showing similar options for the removed low performing occasions
			if (in_array($eventId, [config("evibe.occasion.pre-post.id"), config("evibe.occasion.bachelor.id")]))
			{
				$data['similarProducts'] = [];

				return view($view, ['data' => $data]);
			}

			$negPercent = config('evibe.products.percent-ranges.defaults.negPercent');
			$posPercent = config('evibe.products.percent-ranges.defaults.posPercent');

			$titleData = [];
			$similarProducts = [];
			$tags = null;
			$tagsArray = [];
			$defaultImage = null;
			$cityId = getCityId();
			$similarProductsDataCacheKey = 'similarProductsData-' . $eventId . '-' . $mapTypeId . '-' . $mapId;

			switch ($mapTypeId)
			{
				case config('evibe.ticket.type.decor'):
					$decor = Decor::with('tags')->find($mapId);
					if (!$decor)
					{
						$invalidProfile = true;
						break;
					}

					if (Cache::has($similarProductsDataCacheKey))
					{
						$similarProducts = Cache::get($similarProductsDataCacheKey);
					}
					else
					{
						$tags = $decor->tags;

						if ($tags)
						{
							$tagsArray = $tags->pluck('id')->toArray();
						}

						// @see: do not depend directly on priority table
						$similarProductIds = $this->getSimilarTagsIds($tagsArray, $mapTypeId, $eventId);
						$priorityIds = $this->getItemIdsByPriority($eventId, $mapTypeId, [], false, null);

						$similarProductIds = array_merge($similarProductIds, $priorityIds);
						$similarProductIds = array_unique($similarProductIds);
						$similarProductIds = array_filter($similarProductIds);

						$price = $decor->min_price;
						$minPrice = $this->calculatePercentPrice($price, $negPercent, 0);
						$minPrice = $minPrice > 0 ? $minPrice : 0;
						$maxPrice = $this->calculatePercentPrice($price, $posPercent, 1);

						// @see: since loading with provider,
						$similarDecors = Decor::with('provider', 'provider.city')
						                      ->select('decor.*')
						                      ->whereBetween('decor.min_price', [$minPrice, $maxPrice])
						                      ->whereNotIn('decor.id', [$decor->id])
						                      ->isLive()
						                      ->forCity()
						                      ->forEvent($eventId);

						// backdrop if sortIds are empty
						if (count($similarProductIds))
						{
							$placeholders = implode(',', array_fill(0, count($similarProductIds), '?')); // string for the query

							$similarDecors->whereIn('decor.id', $similarProductIds)
							              ->orderByRaw("field(decor.id,{$placeholders})", $similarProductIds);
						}

						$similarDecors = $similarDecors->take($count)
						                               ->get();

						foreach ($similarDecors as $similarDecor)
						{
							array_push($similarProducts, [
								'id'         => $similarDecor->id,
								'name'       => $similarDecor->name,
								'minPrice'   => $similarDecor->min_price,
								'maxPrice'   => $similarDecor->max_price,
								'rangeInfo'  => $similarDecor->range_info,
								'worth'      => $similarDecor->worth,
								'url'        => $similarDecor->url,
								'profileImg' => $similarDecor->getProfileImg(),
								'ratings'    => $similarDecor->provider->getPartnerReview()
							]);
						}

						Cache::put($similarProductsDataCacheKey, $similarProducts, 60);
					}

					$defaultImage = config("evibe.gallery.host") . "/img/icons/balloons.png";

					$titleData = [
						'title' => 'Similar Decor Styles'
					];

					break;

				case config('evibe.ticket.type.package'):
				case config('evibe.ticket.type.resorts'):
				case config('evibe.ticket.type.villas'):
				case config('evibe.ticket.type.lounges'):
				case config('evibe.ticket.type.food'):
				case config('evibe.ticket.type.surprises'):
				case config('evibe.ticket.type.priests'):
				case config('evibe.ticket.type.tents'):
					$package = Package::with('tags')->find($mapId);
					if (!$package)
					{
						$invalidProfile = true;
						break;
					}

					if (Cache::has($similarProductsDataCacheKey))
					{
						$similarProducts = Cache::get($similarProductsDataCacheKey);
					}
					else
					{
						$tags = $package->tags;

						if ($tags)
						{
							$tagsArray = $tags->pluck('id')->toArray();
						}

						// @see: do not depend directly on priority table
						$similarProductIds = $this->getSimilarTagsIds($tagsArray, $mapTypeId, $eventId);
						$priorityIds = $this->getItemIdsByPriority($eventId, $mapTypeId, [], false, null);

						$similarProductIds = array_merge($similarProductIds, $priorityIds);
						$similarProductIds = array_unique($similarProductIds);
						$similarProductIds = array_filter($similarProductIds);

						$price = $package->price;
						$minPrice = $this->calculatePercentPrice($price, $negPercent, 0);
						$minPrice = $minPrice > 0 ? $minPrice : 0;
						$maxPrice = $this->calculatePercentPrice($price, $posPercent, 1);

						$similarPackages = Package::isLive()
						                          ->forEvent($eventId)
						                          ->forCity()
						                          ->select('planner_package.*')
						                          ->where('planner_package.type_ticket_id', $mapTypeId)
						                          ->whereBetween('planner_package.price', [$minPrice, $maxPrice])
						                          ->whereNotIn('planner_package.id', [$package->id]);

						// backdrop if sortIds are empty
						if (count($similarProductIds))
						{
							$placeholders = implode(',', array_fill(0, count($similarProductIds), '?')); // string for the query

							$similarPackages->whereIn('planner_package.id', $similarProductIds)
							                ->orderByRaw("field(planner_package.id,{$placeholders})", $similarProductIds);
						}

						$similarPackages = $similarPackages->take($count)
						                                   ->get();

						foreach ($similarPackages as $similarPackage)
						{
							array_push($similarProducts, [
								'id'         => $similarPackage->id,
								'name'       => $similarPackage->name,
								'minPrice'   => $similarPackage->price,
								'maxPrice'   => $similarPackage->price_max, // @see: missed "price_per_person". Should be included if necessary.
								'worth'      => $similarPackage->price_worth,
								'url'        => $similarPackage->url,
								'profileImg' => $similarPackage->getProfileImg(),
								'ratings'    => $similarPackage->provider->getPartnerReview()
							]);
						}

						Cache::put($similarProductsDataCacheKey, $similarProducts, config('evibe.cache.refresh-time'));
					}

					$defaultImage = config("evibe.gallery.host") . "/img/icons/gift.png";

					$titleData = [
						'title' => 'Similar Packages'
					];

					break;

				case config('evibe.ticket.type.entertainment'):
					$service = TypeServices::with('tags')->find($mapId);
					if (!$service)
					{
						$invalidProfile = true;
						break;
					}

					if (Cache::has($similarProductsDataCacheKey))
					{
						$similarProducts = Cache::get($similarProductsDataCacheKey);
					}
					else
					{
						$tags = $service->tags;

						if ($tags)
						{
							$tagsArray = $tags->pluck('id')->toArray();
						}

						// @see: do not depend directly on priority table
						$similarProductIds = $this->getSimilarTagsIds($tagsArray, $mapTypeId, $eventId);
						$priorityIds = $this->getItemIdsByPriority($eventId, $mapTypeId, [], false, null);

						$similarProductIds = array_merge($similarProductIds, $priorityIds);
						$similarProductIds = array_unique($similarProductIds);
						$similarProductIds = array_filter($similarProductIds);

						$price = $service->min_price;
						$minPrice = $this->calculatePercentPrice($price, $negPercent, 0);
						$minPrice = $minPrice > 0 ? $minPrice : 0;
						$maxPrice = $this->calculatePercentPrice($price, $posPercent, 1);

						$similarServices = TypeServices::whereBetween('type_service.min_price', [$minPrice, $maxPrice])
						                               ->whereNotIn('type_service.id', [$service->id])
						                               ->isLive()
						                               ->forCity()
						                               ->forEvent($eventId);
						// backdrop if sortIds are empty
						if (count($similarProductIds))
						{
							$placeholders = implode(',', array_fill(0, count($similarProductIds), '?')); // string for the query

							$similarServices->whereIn('type_service.id', $similarProductIds)
							                ->orderByRaw("field(type_service.id,{$placeholders})", $similarProductIds);
						}

						$similarServices = $similarServices->take($count)
						                                   ->get();

						foreach ($similarServices as $similarService)
						{
							array_push($similarProducts, [
								'id'         => $similarService->id,
								'name'       => $similarService->name,
								'minPrice'   => $similarService->min_price,
								'maxPrice'   => $similarService->max_price,
								'rangeInfo'  => $similarService->range_info,
								'worth'      => $similarService->worth_price,
								'url'        => $similarService->url,
								'profileImg' => $similarService->getProfilePic()
							]);
						}

						Cache::put($similarProductsDataCacheKey, $similarProducts, config('evibe.cache.refresh-time'));
					}

					$defaultImage = config("evibe.gallery.host") . "/img/icons/confetti_w.png";

					$titleData = [
						'title' => 'Similar Entertainment Services'
					];

					break;

				case config('evibe.ticket.type.cake'):
					$cake = Cake::with('tags')->find($mapId);
					if (!$cake)
					{
						$invalidProfile = true;
						break;
					}

					if (Cache::has($similarProductsDataCacheKey))
					{
						$similarProducts = Cache::get($similarProductsDataCacheKey);
					}
					else
					{
						$tags = $cake->tags;

						if ($tags)
						{
							$tagsArray = $tags->pluck('id')->toArray();
						}

						// @see: do not depend directly on priority table
						$similarProductIds = $this->getSimilarTagsIds($tagsArray, $mapTypeId, $eventId);
						$priorityIds = $this->getItemIdsByPriority($eventId, $mapTypeId, [], false, null);

						$similarProductIds = array_merge($similarProductIds, $priorityIds);
						$similarProductIds = array_unique($similarProductIds);
						$similarProductIds = array_filter($similarProductIds);

						$price = $cake->price_per_kg;
						$minPrice = $this->calculatePercentPrice($price, $negPercent, 0);
						$minPrice = $minPrice > 0 ? $minPrice : 0;
						$maxPrice = $this->calculatePercentPrice($price, $posPercent, 1);

						$similarCakes = Cake::with('provider', 'provider.city')
						                    ->select('cake.*')
						                    ->whereBetween('price_per_kg', [$minPrice, $maxPrice])
						                    ->whereNotIn('cake.id', [$cake->id])
						                    ->isLive()
						                    ->forCity()
						                    ->forEvent($eventId);

						// backdrop if sortIds are empty
						if (count($similarProductIds))
						{
							$placeholders = implode(',', array_fill(0, count($similarProductIds), '?')); // string for the query

							$similarCakes->whereIn('cake.id', $similarProductIds)
							             ->orderByRaw("field(cake.id,{$placeholders})", $similarProductIds);
						}

						$similarCakes = $similarCakes->take($count)
						                             ->get();

						foreach ($similarCakes as $similarCake)
						{
							array_push($similarProducts, [
								'id'         => $similarCake->id,
								'name'       => $similarCake->title,
								'minPrice'   => $similarCake->price_per_kg,
								'minOrder'   => $similarCake->min_order,
								'url'        => $similarCake->url,
								'profileImg' => $similarCake->getProfileImg(),
								'ratings'    => $similarCake->provider->getPartnerReview()
							]);
						}

						Cache::put($similarProductsDataCacheKey, $similarProducts, config('evibe.cache.refresh-time'));
					}

					$defaultImage = config("evibe.gallery.host") . "/img/icons/balloons.png";

					$titleData = [
						'title' => 'Similar Cakes'
					];

					break;
			}

			if (!$invalidProfile)
			{
				$data = array_merge($data, $titleData);
				$data['similarProducts'] = $similarProducts;
				$data['defaultImage'] = $defaultImage;

				return view($view, ['data' => $data]);
			}
			else
			{
				return "";
			}

		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);

			return response()->json(['error' => true]);
		}
	}

	public function fetchSimilarProductsUsingGlobalSearch($eventId, $mapTypeId, $mapId)
	{
		return "";

		$invalidProfile = false;
		$cityId = getCityId();
		$occasion = TypeEvent::find($eventId);
		$occasionName = "";
		$optionName = "";
		$price = "";
		$optionId = "";
		$view = "occasion.util.profile.d_components.similar-products-section-global";
		$categoryCount = 2;

		if ($this->agent->isMobile() && !($this->agent->isTablet()))
		{
			$view = "occasion.util.profile.m_components.similar-products-section-global";
			$categoryCount = 1;
		}

		// Not showing similar options for the removed low performing occasions
		if (in_array($eventId, [config("evibe.occasion.pre-post.id"), config("evibe.occasion.bachelor.id")]))
		{
			$data['similarProducts'] = [];

			return view($view, ['data' => $data]);
		}

		if ($occasion)
		{
			$occasionName = $occasion->name;
		}

		switch ($mapTypeId)
		{
			case config('evibe.ticket.type.decor'):
				$decor = Decor::with('tags')->find($mapId);
				if (!$decor)
				{
					$invalidProfile = true;
					break;
				}
				else
				{
					$optionName = $decor->name;
					$price = $decor->min_price;
					$optionId = $decor->id;
				}
		}

		// Combining name & occasion to get the accurate results
		$searchTerm = $optionName . " " . $occasionName;
		$searchTerm = preg_replace('/[^A-Za-z0-9\- ]/', '', str_replace('"', "'", $searchTerm));

		$searchIrrelevantTerms = ["exclusive", "top", "best", "special", "simple", "basic", "Exclusive", "Top", "Best", "Special", "Simple", "Basic"];

		$words = explode(" ", $searchTerm);
		$words = array_diff($words, $searchIrrelevantTerms);
		$searchTerm = implode(' ', $words);

		$esPageName = 'esPageNumber'; // different pagination params for the list
		$maxResults = 10000; // this is max allowed by Elasticsearch

		// @see: Elasticsearch returns 10 results by default, paginate to max
		// orWhere & complex where queries not available for Scout hence using collection
		$searchProductResults = SearchableProduct::search($searchTerm)
		                                         ->where('city_id', $cityId);

		$searchProductResults = $searchProductResults->paginate($maxResults, $esPageName);
		$searchProductResults = collect($searchProductResults->items())->toArray();

		$validCategoryIds = [config("evibe.ticket.type.cake"), config("evibe.ticket.type.decor"), config("evibe.ticket.type.package"), config("evibe.ticket.type.service"), config("evibe.ticket.type.trend")];
		$sortedOptions = [];

		// Getting top results from selected categories
		array_filter($searchProductResults, function ($product) use ($validCategoryIds, $mapTypeId, $price, $optionId, &$sortedOptions) {
			if (in_array($product["option_sub_type_id"], $validCategoryIds))
			{
				if (!isset($sortedOptions[$product["option_sub_type_id"]]) || count($sortedOptions[$product["option_sub_type_id"]]) < 6)
				{
					if ((($product["option_sub_type_id"] == $mapTypeId) && ($product["price_min"] >= ($price - 2000)) && ($product["option_id"] != $optionId)) || ($product["option_sub_type_id"] != $mapTypeId))
					{
						$sortedOptions[$product["option_sub_type_id"]][] = $product;
					}
				}
			}
		});

		$categoryRelatedOptions = [];
		$otherCategoryOptions = [];

		// Getting top 2 options from each category to show it in the first, rest will be shown next
		if (count($sortedOptions) > 0)
		{
			foreach ($sortedOptions as $key => $options)
			{
				if ($key == $mapTypeId)
				{
					$categoryRelatedOptions = array_merge($categoryRelatedOptions, $options);
				}
				else
				{
					$otherCategoryOptions = array_merge($otherCategoryOptions, array_slice($options, 0, $categoryCount));
					$categoryRelatedOptions = array_merge($categoryRelatedOptions, array_slice($options, $categoryCount, 10));
				}
			}
		}

		// We can shuffle the options if needed
		//return $this->shuffle_assoc($categoryRelatedOptions);

		$data = [
			"cityUrl"         => "bangalore",
			"occasion"        => $occasion,
			"mapTypeId"       => $mapTypeId,
			"title"           => "Customer who viewed this item also viewed",
			"similarProducts" => array_merge($otherCategoryOptions, $categoryRelatedOptions),
			"defaultImage"    => config("evibe.gallery.host") . "/img/icons/balloons.png"
		];

		if (!$invalidProfile)
		{
			return view($view, ['data' => $data]);
		}
		else
		{
			return "";
		}
	}

	public function shuffle_assoc($arrayElements)
	{
		$keys = array_keys($arrayElements);
		shuffle($keys);

		$shuffledArray = [];
		foreach ($keys as $key)
		{
			$shuffledArray[$key] = $arrayElements[$key];
		}

		return $shuffledArray;
	}

	public function fetchOtherProfileCards($mapId, $mapTypeId, $eventId, $category)
	{
		// Get remaining Cards
		$category = "%" . $category;
		$cardsDetails = OccasionHomeCards::where('cta', 'not like', $category)->get();
		$data = [
			'cityUrl'   => getCityUrl(),
			'mapTypeId' => $mapTypeId,
			'category'  => $category,
			'cards'     => $cardsDetails,
		];
		$view = "occasion.util.profile.c_components.package.category-cards-section";

		return view($view, ['data' => $data]);
	}

	public function fetchMostViewedProducts($mapId, $mapTypeId, $eventId)
	{
		try
		{
			$occasion = TypeEvent::find($eventId);
			$invalidProfile = false;

			$data = [
				'cityUrl'   => getCityUrl(),
				'occasion'  => $occasion,
				'mapTypeId' => $mapTypeId,
			];

			$view = "occasion.util.profile.d_components.most-viewed-products-section";
			$count = 18;

			// Not showing similar options for the removed low performing occasions
			if (in_array($eventId, [config("evibe.occasion.pre-post.id"), config("evibe.occasion.bachelor.id")]))
			{
				$data['similarProducts'] = [];

				return view($view, ['data' => $data]);
			}

			// todo: uncomment when implemented for mobile and added view files.
			//if ($this->agent->isMobile() && !($this->agent->isTablet()))
			//{
			//	$view = "occasion.util.m_components.most-viewed-products-section";
			//	$count = 12;
			//}

			$titleData = [];
			$mostViewedProducts = [];
			$defaultImage = null;
			$negPercent = config('evibe.products.percent-ranges.defaults.negPercent');
			$posPercent = config('evibe.products.percent-ranges.defaults.posPercent');

			// use cache to reduce time taken to fetch view count product ids each time
			$cacheKey = '-mvProductIds-' . $mapTypeId;
			if (Cache::has($cacheKey))
			{
				$viewCounterIds = Cache::get($cacheKey);
			}
			else
			{
				$viewCounterIds = ProfileViewCounter::select('product_id', DB::raw('CONCAT(product_type_id, "-", product_id) AS unique_product'), DB::raw('SUM(total_views) AS view_count'))
				                                    ->groupBy('unique_product')
				                                    ->orderBy('view_count', 'DESC')
				                                    ->where('product_type_id', $mapTypeId)
				                                    ->pluck('product_id')
				                                    ->toArray();
				Cache::put($cacheKey, $viewCounterIds, config('evibe.cache.refresh-time'));
			}

			// @see: view count will definitely exist
			$placeholders = implode(',', array_fill(0, count($viewCounterIds), '?')); // string for the query
			$mvProductCount = count($viewCounterIds);
			$mvProductsCacheKey = 'mvProductsData-' . $eventId . '-' . $mapTypeId . '-' . $mapId;

			switch ($mapTypeId)
			{
				case config('evibe.ticket.type.decor'):
					$decor = Decor::find($mapId);
					if (!$decor)
					{
						$invalidProfile = true;
						break;
					}

					// use cache to reduce time to compute mvProducts data
					// @see: cache not used globally to validate product before calculating price range

					if (Cache::has($mvProductsCacheKey))
					{
						$mostViewedProducts = Cache::get($mvProductsCacheKey);
					}
					else
					{
						$price = $decor->min_price;
						$minPrice = $this->calculatePercentPrice($price, $negPercent, 0);
						$minPrice = $minPrice > 0 ? $minPrice : 0;
						$maxPrice = $this->calculatePercentPrice($price, $posPercent, 1);

						$mostViewedDecors = Decor::with('provider.city')
						                         ->select('decor.*')
						                         ->isLive()
						                         ->forCity()
						                         ->forEvent($eventId)
						                         ->whereBetween('decor.min_price', [$minPrice, $maxPrice])
						                         ->whereNotIn('decor.id', [$mapId])
						                         ->whereIn('decor.id', $viewCounterIds)
						                         ->orderByRaw("field(decor.id,{$placeholders})", $viewCounterIds)
						                         ->take($count)
						                         ->get();
						$i = 0;
						foreach ($mostViewedDecors as $mostViewedDecor)
						{
							$mostViewedProducts[$i] = [
								'id'         => $mostViewedDecor->id,
								'name'       => $mostViewedDecor->name,
								'minPrice'   => $mostViewedDecor->min_price,
								'maxPrice'   => $mostViewedDecor->max_price,
								'rangeInfo'  => $mostViewedDecor->range_info,
								'worth'      => $mostViewedDecor->worth,
								'url'        => $mostViewedDecor->url,
								'profileImg' => $mostViewedDecor->getProfileImg(),
								'ratings'    => $mostViewedDecor->provider->getPartnerReview()
							];
							$i++;
						}

						Cache::put($mvProductsCacheKey, $mostViewedProducts, config('evibe.cache.refresh-time'));
					}

					$defaultImage = config("evibe.gallery.host") . "/img/icons/balloons.png";

					$titleData = [
						'title' => 'Most Viewed Decor Styles'
					];

					break;

				case config('evibe.ticket.type.package'):
				case config('evibe.ticket.type.resorts'):
				case config('evibe.ticket.type.villas'):
				case config('evibe.ticket.type.lounges'):
				case config('evibe.ticket.type.food'):
				case config('evibe.ticket.type.surprises'):
				case config('evibe.ticket.type.priests'):
				case config('evibe.ticket.type.tents'):
					$package = Package::find($mapId);
					if (!$package)
					{
						$invalidProfile = true;
						break;
					}

					// use cache to reduce time to compute mvProducts data
					// @see: cache not used globally to validate product before calculating price range

					if (Cache::has($mvProductsCacheKey))
					{
						$mostViewedProducts = Cache::get($mvProductsCacheKey);
					}
					else
					{
						$price = $package->price;
						$minPrice = $this->calculatePercentPrice($price, $negPercent, 0);
						$minPrice = $minPrice > 0 ? $minPrice : 0;
						$maxPrice = $this->calculatePercentPrice($price, $posPercent, 1);

						$mostViewedPackages = Package::isLive()
						                             ->forEvent($eventId)
						                             ->forCity()
						                             ->select('planner_package.*')
						                             ->where('planner_package.type_ticket_id', $mapTypeId)
						                             ->whereBetween('planner_package.price', [$minPrice, $maxPrice])
						                             ->whereNotIn('planner_package.id', [$mapId])
						                             ->whereIn('planner_package.id', $viewCounterIds)
						                             ->orderByRaw("field(planner_package.id,{$placeholders})", $viewCounterIds)
						                             ->take($count)
						                             ->get();
						$i = 0;
						foreach ($mostViewedPackages as $mostViewedPackage)
						{
							$mostViewedProducts[$i] = [
								'id'         => $mostViewedPackage->id,
								'name'       => $mostViewedPackage->name,
								'minPrice'   => $mostViewedPackage->price,
								'maxPrice'   => $mostViewedPackage->price_max,
								'worth'      => $mostViewedPackage->price_worth,
								'url'        => $mostViewedPackage->url,
								'profileImg' => $mostViewedPackage->getProfileImg(),
								'ratings'    => $mostViewedPackage->provider->getPartnerReview()
							];
							$i++;
						}

						Cache::put($mvProductsCacheKey, $mostViewedProducts, config('evibe.cache.refresh-time'));
					}

					$defaultImage = config("evibe.gallery.host") . "/img/icons/gift.png";

					$titleData = [
						'title' => 'Most Viewed Packages'
					];

					break;

				case config('evibe.ticket.type.entertainment'):
					$service = TypeServices::find($mapId);
					if (!$service)
					{
						$invalidProfile = true;
						break;
					}

					// use cache to reduce time to compute mvProducts data
					// @see: cache not used globally to validate product before calculating price range

					if (Cache::has($mvProductsCacheKey))
					{
						$mostViewedProducts = Cache::get($mvProductsCacheKey);
					}
					else
					{
						$price = $service->min_price;
						$minPrice = $this->calculatePercentPrice($price, $negPercent, 0);
						$minPrice = $minPrice > 0 ? $minPrice : 0;
						$maxPrice = $this->calculatePercentPrice($price, $posPercent, 1);

						$mostViewedServices = TypeServices::select('type_service.*')
						                                  ->isLive()
						                                  ->forCity()
						                                  ->forEvent($eventId)
						                                  ->whereBetween('type_service.min_price', [$minPrice, $maxPrice])
						                                  ->whereIn('type_service.id', $viewCounterIds)
						                                  ->whereNotIn('type_service.id', [$mapId])
						                                  ->orderByRaw("field(type_service.id,{$placeholders})", $viewCounterIds)
						                                  ->take($count)
						                                  ->get();
						$i = 0;
						foreach ($mostViewedServices as $mostViewedService)
						{
							$mostViewedProducts[$i] = [
								'id'         => $mostViewedService->id,
								'name'       => $mostViewedService->name,
								'minPrice'   => $mostViewedService->min_price,
								'maxPrice'   => $mostViewedService->max_price,
								'rangeInfo'  => $mostViewedService->range_info,
								'worth'      => $mostViewedService->worth_price,
								'url'        => $mostViewedService->url,
								'profileImg' => $mostViewedService->getProfilePic(),
							];
							$i++;
						}

						Cache::put($mvProductsCacheKey, $mostViewedProducts, config('evibe.cache.refresh-time'));
					}

					$defaultImage = config("evibe.gallery.host") . "/img/icons/confetti_w.png";

					$titleData = [
						'title' => 'Most Viewed Entertainment Services'
					];

					break;

				case config('evibe.ticket.type.cake'):
					$cake = Cake::find($mapId);
					if (!$cake)
					{
						$invalidProfile = true;
						break;
					}

					// use cache to reduce time to compute mvProducts data
					// @see: cache not used globally to validate product before calculating price range

					if (Cache::has($mvProductsCacheKey))
					{
						$mostViewedProducts = Cache::get($mvProductsCacheKey);
					}
					else
					{
						$price = $cake->price_per_kg;
						$minPrice = $this->calculatePercentPrice($price, $negPercent, 0);
						$minPrice = $minPrice > 0 ? $minPrice : 0;
						$maxPrice = $this->calculatePercentPrice($price, $posPercent, 1);

						$mostViewedCakes = Cake::with('provider.city')
						                       ->select('cake.*')
						                       ->isLive()
						                       ->forCity()
						                       ->forEvent($eventId)
						                       ->whereBetween('cake.price_per_kg', [$minPrice, $maxPrice])
						                       ->whereNotIn('cake.id', [$mapId])
						                       ->whereIn('cake.id', $viewCounterIds)
						                       ->orderByRaw("field(cake.id,{$placeholders})", $viewCounterIds)
						                       ->take($count)
						                       ->get();
						$i = 0;
						foreach ($mostViewedCakes as $mostViewedCake)
						{
							$mostViewedProducts[$i] = [
								'id'         => $mostViewedCake->id,
								'name'       => $mostViewedCake->title,
								'minPrice'   => $mostViewedCake->price_per_kg,
								'minOrder'   => $mostViewedCake->min_order,
								'url'        => $mostViewedCake->url,
								'profileImg' => $mostViewedCake->getProfileImg(),
								'ratings'    => $mostViewedCake->provider->getPartnerReview()
							];
							$i++;

						}

						Cache::put($mvProductsCacheKey, $mostViewedProducts, config('evibe.cache.refresh-time'));
					}

					$defaultImage = config("evibe.gallery.host") . "/img/icons/balloons.png";

					$titleData = [
						'title' => 'Most Viewed Cakes'
					];

					break;
			}

			if (!$invalidProfile)
			{
				$data = array_merge($data, $titleData);
				$data['mVProducts'] = $mostViewedProducts;
				$data['defaultImage'] = $defaultImage;

				return view($view, ['data' => $data]);
			}
			else
			{
				return "";
			}

		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);

			return response()->json(['error' => true]);
		}
	}

	public function fetchProductReviewsAndRatings($mapId, $mapTypeId)
	{
		try
		{
			$pageName = request('pageName');
			$productDescriptionData = [];
			switch ($mapTypeId)
			{
				case config('evibe.ticket.type.decor'):
					$decor = Decor::with('provider')->find($mapId);
					if (!$decor)
					{
						break;
					}
					// Check if option reviews are available
					$partnerRatings = $decor->provider->getPartnerReview();
					$schema = $this->getProductSchema($decor, $partnerRatings, $pageName);
					$productDescriptionData = [
						'productName' => $decor->name,
						'providerId'  => $decor->provider->id,
						'ratings'     => $partnerRatings,
						'name'        => $decor->name,
						'seo'         => [
							'schema' => $schema
						],
						'view'        => 'occasion.util.profile.d_components.product-reviews-section'
					];
					break;

				case config('evibe.ticket.type.package'):
				case config('evibe.ticket.type.resorts'):
				case config('evibe.ticket.type.villas'):
				case config('evibe.ticket.type.lounges'):
				case config('evibe.ticket.type.food'):
				case config('evibe.ticket.type.surprises'):
				case config('evibe.ticket.type.priests'):
				case config('evibe.ticket.type.tents'):
					$package = Package::with('provider')->find($mapId);
					if (!$package)
					{
						break;
					}

					$partnerRatings = $package->provider->getPartnerReview();
					$schema = $this->getProductSchema($package, $partnerRatings, $pageName);
					$productDescriptionData = [
						'product'     => $package, // package
						'productName' => $package->name,
						'providerId'  => $package->provider->id,
						'ratings'     => $partnerRatings,
						'name'        => $package->name,
						'seo'         => [
							'schema' => $schema
						],
						'view'        => 'occasion.util.profile.d_components.package.product-reviews-section'
					];

					break;

				case config('evibe.ticket.type.cake'):
					$cake = Cake::with('provider')->find($mapId);
					if (!$cake)
					{
						break;
					}

					$partnerRatings = $cake->provider->getPartnerReview();
					$schema = $this->getProductSchema($cake, $partnerRatings, $pageName);
					$productDescriptionData = [
						'productName' => $cake->title,
						'providerId'  => $cake->provider->id,
						'ratings'     => $partnerRatings,
						'name'        => $cake->title,
						'seo'         => [
							'schema' => $schema
						],
						'view'        => 'occasion.util.profile.d_components.product-reviews-section'
					];
					break;
			}
			$productDescriptionData['deliveryImages'] = $this->fetchDeliveryImages($mapId, $mapTypeId);
			if ($productDescriptionData && isset($productDescriptionData['view']))
			{
				return view($productDescriptionData['view'], ['data' => $productDescriptionData]);
			}
			else
			{
				return ""; // for ent category, there are no reviews
			}

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());

			return response()->json(['error' => true]);
		}
	}

	public function calculateDistance()
	{
		try
		{

			$venuePin = request('venuePin');
			$locationPin = request('locationPin');

			if ($locationPin)
			{
				$res = $this->makeCurlRequestToGoogleMatrixApi($venuePin, $locationPin);
				if (($res['status'] == 'OK') && ($res['rows'][0]['elements'][0]['status'] === 'OK'))
				{
					$distance = $res['rows'][0]['elements'][0]['distance']['value'];

					return response()->json([
						                        'success'  => true,
						                        'distance' => $distance
					                        ]);
				}
				else
				{
					Log::error("Pincode exists, Unable to fetch pincode");
					// todo: report team that zip code doesn't exist for a particular location
				}
			}
			else
			{
				Log::error("Unable to fetch pincode");
				// todo: report team that zip code doesn't exist for a particular location
			}

			return response()->json([
				                        'success'  => false,
				                        'errorMsg' => 'Kindly enter a valid location pin code and try again'
			                        ]);

		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);

			return response()->json(['error' => true]);
		}
	}

	private function getProductSchema($product, $partnerRatings, $pageName)
	{
		$breadcrumbItemList = [
			"Evibe.in",
			getCityName(),
			$pageName,
			$product->name
		];
		$schema = [$this->schemaBreadcrumbList($breadcrumbItemList)];
		$avgRating = $partnerRatings['total']['avg'];
		$ratingCount = $partnerRatings['total']['count'];
		$productData = [
			'category'    => $pageName,
			'name'        => $product->name,
			'image'       => $product->getProfileImg(),
			'avgRating'   => $avgRating,
			'ratingCount' => $ratingCount,
			'price'       => $product->min_price,
			'description' => $product->info
		];
		array_push($schema, $this->schemaProduct($productData));

		return $schema;
	}

	private function calculatePercentPrice($price, $percent, $addition = 0)
	{
		$roundTo = 50;
		$percentAmount = round($price * $percent / 100);

		if ($addition)
		{
			$computedAmount = $price + $percentAmount;
		}
		else
		{
			$computedAmount = $price - $percentAmount;
		}

		$rem = $computedAmount % $roundTo;
		if ($rem != 0)
		{
			$add = $roundTo - $rem;
			$computedAmount += $add;
		}

		return $computedAmount;

	}
}