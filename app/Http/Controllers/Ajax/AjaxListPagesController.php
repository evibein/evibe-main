<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Base\BasePackageController;
use App\Http\Controllers\Base\BaseResultsController;
use App\Models\Cake\Cake;
use App\Models\Cake\CakeGallery;
use App\Models\Decor\Decor;
use App\Models\Decor\DecorGallery;
use App\Models\Package\Package;
use App\Models\Service\ServiceGallery;
use App\Models\Types\TypeServices;
use App\Models\Util\City;
use Illuminate\Support\Facades\Log;

class AjaxListPagesController extends BaseResultsController
{
	public function fetchOptionsRatings($optionTypeId)
	{
		try
		{
			$listIds = request('listIds');

			if ($listIds)
			{
				$data = [];
				$i = 0;

				switch ($optionTypeId)
				{
					case config('evibe.ticket.type.decor'):

						$decors = Decor::with('provider')->whereIn('id', $listIds)->get();

						foreach ($decors as $decor)
						{
							$partnerRatings = $decor->provider->getPartnerReview();
							$avgRating = $partnerRatings['total']['avg'];
							$ratingCount = $partnerRatings['total']['count'];
							$data[$i] = [
								'id'          => $decor->id,
								'name'        => $decor->name,
								'avgRating'   => $avgRating,
								'ratingCount' => $ratingCount
							];
							$i++;
						}

						break;

					case config('evibe.ticket.type.surprises'):

						$packages = Package::with('provider')->whereIn('id', $listIds)->get();

						foreach ($packages as $package)
						{
							$partnerRatings = $package->provider->getPartnerReview();
							$avgRating = $partnerRatings['total']['avg'];
							$ratingCount = $partnerRatings['total']['count'];
							$data[$i] = [
								'id'          => $package->id,
								'name'        => $package->name,
								'avgRating'   => $avgRating,
								'ratingCount' => $ratingCount
							];
							$i++;
						}

						break;
				}

				return response()->json([
					                        'success' => true,
					                        'ratings' => $data,
					                        'count'   => $i - 1
				                        ]);
			}
			else
			{
				return response()->json(['success' => false]);
			}

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());

			return response()->json(['error' => true]);
		}
	}

	public function getOptionImages()
	{
		$optionImageUrls = [];
		$optionIds = request("optionIds");
		$optionTypeId = request("optionTypeId");

		$profileImgUrl = config('evibe.gallery.host');

		switch ($optionTypeId)
		{
			case config('evibe.ticket.type.decor'):
				$decorImages = DecorGallery::select('id', 'url', 'decor_id')
				                           ->whereIn('decor_id', $optionIds)
				                           ->where('type_id', config('evibe.gallery.type.image'))
				                           ->orderBy('is_profile', 'DESC')
				                           ->orderBy('created_at', 'DESC')
				                           ->get()
				                           ->unique('decor_id');

				// Assign the profile image from DB or update it with default image.
				foreach ($optionIds as $optionId)
				{
					$decorImage = $decorImages->where('decor_id', $optionId)->first();

					$optionImageUrls[$optionId] = $decorImage ? $profileImgUrl . '/decors/' . $decorImage->decor_id . '/images/results/' . $decorImage->url : $profileImgUrl . '/img/icons/balloons.png';
				}
				break;

			case config('evibe.ticket.type.entertainment'):
				$entImages = ServiceGallery::select('id', 'url', 'type_service_id')
				                           ->whereIn('type_service_id', $optionIds)
				                           ->where('type_id', config('evibe.gallery.type.image'))
				                           ->orderBy('is_profile', 'DESC')
				                           ->orderBy('created_at', 'DESC')
				                           ->get()
				                           ->unique('type_service_id');

				// Assign the profile image from DB or update it with default image.
				foreach ($optionIds as $optionId)
				{
					$entImage = $entImages->where('type_service_id', $optionId)->first();

					$optionImageUrls[$optionId] = $entImage ? $profileImgUrl . '/services/' . $entImage->type_service_id . '/images/results/' . $entImage->url : $profileImgUrl . '/img/icons/balloons.png';
				}
				break;

			case config('evibe.ticket.type.cake'):
				$cakeImages = CakeGallery::select('id', 'url', 'cake_id')
				                         ->whereIn('cake_id', $optionIds)
				                         ->where('type', config('evibe.gallery.type.image'))
				                         ->orderBy('is_profile', 'DESC')
				                         ->orderBy('created_at', 'DESC')
				                         ->get()
				                         ->unique('cake_id');

				// Assign the profile image from DB or update it with default image.
				foreach ($optionIds as $optionId)
				{
					$cakeImage = $cakeImages->where('cake_id', $optionId)->first();

					$optionImageUrls[$optionId] = $cakeImage ? $profileImgUrl . '/cakes/' . $cakeImage->cake_id . '/results/' . $cakeImage->url : $profileImgUrl . '/img/icons/balloons.png';
				}
				break;
		}

		return response()->json([
			                        "success"         => true,
			                        "optionImageUrls" => $optionImageUrls
		                        ]);
	}
}