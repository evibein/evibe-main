<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Base\BaseController;
use App\Models\Types\TypeTag;
use Illuminate\Support\Facades\Log;

class AjaxUtilController extends BaseController
{
	public function fetchGoogleReviews()
	{
		try
		{

			// fetch reviews
			$url = 'https://maps.googleapis.com/maps/api/place/details/json';
			$queryString = 'placeid=' . config('evibe.google.place-id.bangalore-office') . '&fields=reviews&key=' . config('evibe.google.reviews-key');

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url . '?' . $queryString);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$cResponse = (curl_exec($ch));
			curl_close($ch);
			$res = json_decode($cResponse, true);

			if ($res && isset($res['status']) && ($res['status'] == 'OK'))
			{
				if (isset($res['result']['reviews']) && count($res['result']['reviews']))
				{
					$totalRating = 0;
					$minRating = config('evibe.google.reviews.min-rating');
					foreach ($res['result']['reviews'] as $review)
					{
						$rating = isset($review['rating']) ? $review['rating'] : 0;

						if ($rating < $minRating)
						{
							// all ratings should be above 3
							return "";
						}

						$totalRating += $rating;
					}

					$totalRating = $totalRating / count($res['result']['reviews']);
					if ($totalRating < config('evibe.google.reviews.min-avg-rating'))
					{
						// overall rating should be above 4
						return "";
					}

					if ($this->agent->isMobile() && !($this->agent->isTablet()))
					{
						$view = "app.google-reviews.m";
					}
					else
					{
						$view = "app.google-reviews.d";
					}

					return view($view, ['reviews' => $res['result']['reviews']]);
				}
				else
				{
					Log::error("Unable to fetch reviews from places API");

					return "";
				}
			}

			return "";

		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);

			return response()->json(['error' => true]);
		}
	}

	public function fetchSurpriseCategories()
	{
		try
		{
			/**
			 * Not including CLD because it is shown as separate occasion
			 */
			$tags = TypeTag::where('type_event', config('evibe.occasion.surprises.id'))
			               ->where('map_type_id', config('evibe.ticket.type.surprises'))
			               ->whereNotIn("id", [config("evibe.type-tag.candle-light-dinners")])
			               ->where('is_filter', 1)
			               ->whereNull('deleted_at')
			               ->orderBy('updated_at', 'DESC')
			               ->get();

			$tagsData = [];
			if (count($tags))
			{
				// @see: hardcoded url
				// todo: use dynamic url construction when header top-bard is being upgraded
				foreach ($tags as $tag)
				{
					array_push($tagsData, [
						'categoryName' => $tag->identifier,
						'categoryUrl'  => config('evibe.host') . '/' . getCityUrl() . '/surprise-planners/packages/sr/0-All/so/0-All/sa/0-All?category=' . $tag->url . '&utm_source=top-header&utm_campaign=category-lists&utm_medium=website&utm_content=surprise-packages'
					]);
				}
			}

			return response()->json([
				                        'success'      => true,
				                        'categoryData' => $tagsData
			                        ]);

		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);

			return response()->json(['success' => false]);
		}
	}
}