<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Payments\BasePaymentController;
use App\Jobs\Emails\Util\MailCustomerProofUploadAlertToCustomer;
use App\Jobs\Emails\Util\MailCustomerProofUploadAlertToTeam;
use App\Jobs\SMS\SMSCustomerProofUploadToCustomer;
use App\Models\AddOn\AddOn;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketBooking;
use App\Models\Ticket\TicketBookingGallery;
use App\Models\Ticket\TicketMapping;
use App\Models\Util\CustomerOrderProof;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class AjaxCheckoutController extends BasePaymentController
{
	public function updateCheckoutAddOns($ticketId)
	{
		// @see: this function is also being used for Normal Booking add-ons operations, wich might cause issues if add-ons are being removed and same add-on is added after making payment
		// @todo: optimise this function and make it work for 'is_advance_apid' add-ons also
		$ticket = Ticket::find($ticketId);
		if (!$ticket)
		{
			Log::error("Unable to find ticket with id: $ticketId");

			return response()->json(['success' => false]);
		}

		$addOnsData = request('addOns');

		$ticketBookings = TicketBooking::select('ticket_bookings.*', 'ticket_mapping.map_id AS product_id', 'ticket_mapping.map_type_id AS product_type_id')
		                               ->join('ticket_mapping', 'ticket_mapping.id', '=', 'ticket_bookings.ticket_mapping_id')
		                               ->where('ticket_bookings.ticket_id', $ticketId)
		                               ->whereNull('ticket_bookings.deleted_at')
		                               ->whereNull('ticket_mapping.deleted_at')
		                               ->get();

		$partnerId = null;
		$partnerTypeId = null;

		foreach ($ticketBookings as $ticketBooking)
		{
			if ($ticketBooking->product_type_id != config('evibe.ticket.type.add-on'))
			{
				$partnerId = $ticketBooking->map_id ? $ticketBooking->map_id : $partnerId;
				$partnerTypeId = $ticketBooking->map_type_id ? $ticketBooking->map_type_id : $partnerTypeId;

				if (($ticketBooking->product_type_id == config('evibe.ticket.type.entertainment')) ||
					($ticketBooking->product_type_id == config('evibe.ticket.type.service')))
				{
					$partnerId = $partnerId ? $partnerId : (config('evibe.default-partner.city.' . $ticket->city_id . '.id') ?: config('evibe.default-partner.id'));
					$partnerTypeId = $partnerTypeId ? $partnerTypeId : config('evibe.ticket.type.planner');
				}
			}
		}

		if (!$partnerId || !$partnerTypeId)
		{

			Log::error("ticket/partner doesn't exist with primary bookings");

			return response()->json([
				                        'success' => false
			                        ]);
		}

		$addOnBookings = $ticketBookings->where('product_type_id', config('evibe.ticket.type.add-on'));

		// remove unselected add ons
		if ($addOnBookings && !$addOnsData)
		{
			// remove all add-on bookings
			foreach ($addOnBookings as $booking)
			{
				$booking->updated_at = Carbon::now();
				$booking->deleted_at = Carbon::now();
				$booking->save();

				// remove mapping
				TicketMapping::where('id', $booking->ticket_mapping_id)
				             ->update([
					                      'updated_at' => Carbon::now(),
					                      'deleted_at' => Carbon::now(),
				                      ]);

			}

		}
		elseif (!$addOnBookings && $addOnsData)
		{
			// create new add-on bookings
			foreach ($addOnsData as $addOnId => $addOnCount)
			{
				if (!$addOnCount)
				{
					Log::error("not a valid count for add on : $addOnId");
					continue;
				}

				$addOn = AddOn::find($addOnId);
				if (!$addOn)
				{
					Log::error("unable to find add-on with id: $addOnId");
					continue;
				}

				$mapping = TicketMapping::create([
					                                 'ticket_id'   => $ticketId,
					                                 'map_id'      => $addOnId,
					                                 'map_type_id' => config('evibe.ticket.type.add-on'),
					                                 'created_at'  => date('Y-m-d H:i:s'),
					                                 'updated_at'  => date('Y-m-d H:i:s')
				                                 ]);

				if (!$mapping)
				{
					Log::error("mapping creation. add-on id: $addOnId");
					continue;
				}

				$booking = TicketBooking::create([
					                                 'booking_id'             => config("evibe.ticket.enq.pre.auto_booked") . $mapping->id,
					                                 'booking_info'           => $addOn->name . ' - ' . $addOn->info,
					                                 'party_date_time'        => $ticket->event_date,
					                                 //'party_end_time'              => null,
					                                 'product_price'          => $addOn->price_per_unit,
					                                 'booking_units'          => $addOnCount,
					                                 'booking_amount'         => $addOn->price_per_unit * $addOnCount,
					                                 'advance_amount'         => $addOn->price_per_unit * $addOnCount,
					                                 'map_id'                 => $partnerId,
					                                 'map_type_id'            => $partnerTypeId,
					                                 'ticket_id'              => $ticketId,
					                                 'ticket_mapping_id'      => $mapping->id,
					                                 'is_venue_booking'       => false,
					                                 'type_ticket_booking_id' => $addOn->type_ticket_booking_id,
					                                 'booking_type_details'   => ucwords(config('evibe.auto-book.' . config('evibe.ticket.type.add-on'))),
					                                 'created_at'             => date('Y-m-d H:i:s'),
					                                 'updated_at'             => date('Y-m-d H:i:s')
				                                 ]);

				if (!$booking)
				{
					Log::error("booking creation error. ticket id: $ticketId - mapping id: " . $mapping->id);
					continue;
				}

				// update add-on image as booking image
				$profileImageUrl = config('evibe.gallery.host');
				$profileImageUrl .= '/addons/' . $addOn->id . '/' . $addOn->image_url;

				// upload profile image to booking gallery
				if ($profileImageUrl)
				{
					$validImage = getimagesize($profileImageUrl);
					if ($validImage)
					{
						TicketBookingGallery::create([
							                             'ticket_booking_id' => $booking->id,
							                             'url'               => $profileImageUrl,
							                             'type'              => 1,
							                             'title'             => pathinfo($profileImageUrl, PATHINFO_FILENAME)
						                             ]);
					}
				}
			}

		}
		elseif ($addOnBookings && $addOnsData)
		{
			// todo: crude method, need to be improved and optimised
			// delete existing add-on
			foreach ($addOnBookings as $booking)
			{
				$booking->updated_at = Carbon::now();
				$booking->deleted_at = Carbon::now();
				$booking->save();

				// remove mapping
				TicketMapping::where('id', $booking->ticket_mapping_id)
				             ->update([
					                      'updated_at' => Carbon::now(),
					                      'deleted_at' => Carbon::now(),
				                      ]);

			}

			// create new add-ons
			foreach ($addOnsData as $addOnId => $addOnCount)
			{
				if (!$addOnCount)
				{
					Log::error("not a valid count for add on : $addOnId");
					continue;
				}

				$addOn = AddOn::find($addOnId);
				if (!$addOn)
				{
					Log::error("unable to find add-on with id: $addOnId");
					continue;
				}

				$mapping = TicketMapping::create([
					                                 'ticket_id'   => $ticketId,
					                                 'map_id'      => $addOnId,
					                                 'map_type_id' => config('evibe.ticket.type.add-on'),
					                                 'created_at'  => date('Y-m-d H:i:s'),
					                                 'updated_at'  => date('Y-m-d H:i:s')
				                                 ]);

				if (!$mapping)
				{
					Log::error("mapping creation. add-on id: $addOnId");
					continue;
				}

				$booking = TicketBooking::create([
					                                 'booking_id'             => config("evibe.ticket.enq.pre.auto_booked") . $mapping->id,
					                                 'booking_info'           => $addOn->name . ' - ' . $addOn->info,
					                                 'party_date_time'        => $ticket->event_date,
					                                 //'party_end_time'              => null,
					                                 'product_price'          => $addOn->price_per_unit,
					                                 'booking_units'          => $addOnCount,
					                                 'booking_amount'         => $addOn->price_per_unit * $addOnCount,
					                                 'advance_amount'         => $addOn->price_per_unit * $addOnCount,
					                                 'map_id'                 => $partnerId,
					                                 'map_type_id'            => $partnerTypeId,
					                                 'ticket_id'              => $ticketId,
					                                 'ticket_mapping_id'      => $mapping->id,
					                                 'is_venue_booking'       => false,
					                                 'type_ticket_booking_id' => $addOn->type_ticket_booking_id,
					                                 'booking_type_details'   => ucwords(config('evibe.auto-book.' . config('evibe.ticket.type.add-on'))),
					                                 'created_at'             => date('Y-m-d H:i:s'),
					                                 'updated_at'             => date('Y-m-d H:i:s')
				                                 ]);

				if (!$booking)
				{
					Log::error("booking creation error. ticket id: $ticketId - mapping id: " . $mapping->id);
					continue;
				}

				// update add-on image as booking image
				$profileImageUrl = config('evibe.gallery.host');
				$profileImageUrl .= '/addons/' . $addOn->id . '/' . $addOn->image_url;

				// upload profile image to booking gallery
				if ($profileImageUrl)
				{
					$validImage = getimagesize($profileImageUrl);
					if ($validImage)
					{
						TicketBookingGallery::create([
							                             'ticket_booking_id' => $booking->id,
							                             'url'               => $profileImageUrl,
							                             'type'              => 1,
							                             'title'             => pathinfo($profileImageUrl, PATHINFO_FILENAME)
						                             ]);
					}
				}
			}
		}
		else
		{
			// nothing happens
		}

		return response()->json(['success' => true]);
	}

	public function uploadCustomerOrderProofs($ticketId)
	{
		$customerProofFront = Input::file('customerProofFront');
		$customerProofBack = Input::file('customerProofBack');
		$proofType = Input::get('proofType');

		if (!$proofType)
		{
			return response()->json([
				                        'success'  => false,
				                        'errorMsg' => 'Kindly select an id proof to upload'
			                        ]);
		}

		if (!$customerProofFront)
		{
			return response()->json([
				                        'success'  => false,
				                        'errorMsg' => 'Kindly upload front side of the proof'
			                        ]);
		}

		if (!$customerProofBack)
		{
			return response()->json([
				                        'success'  => false,
				                        'errorMsg' => 'Kindly upload back side of the proof'
			                        ]);
		}

		$proofRules = [
			'file' => 'required|mimes:png,jpeg,jpg,JPG|max:4096'
		];

		$proofMessages = [
			'file.required' => 'Please upload at least one image',
			'file.mimes'    => 'One of the images is not valid. (only .png, .jpeg, .jpg are accepted)',
			'file.max'      => 'Image size cannot be more than 4 MB'
		];

		// validation for proof front side
		$fileValidator = Validator::make(['file' => $customerProofFront], $proofRules, $proofMessages);

		if ($fileValidator->fails())
		{
			return response()->json([
				                        'success'  => false,
				                        'errorMsg' => $fileValidator->messages()->first()
			                        ]);
		}

		// validation for proof back side
		$fileValidator = Validator::make(['file' => $customerProofBack], $proofRules, $proofMessages);

		if ($fileValidator->fails())
		{
			return response()->json([
				                        'success'  => false,
				                        'errorMsg' => $fileValidator->messages()->first()
			                        ]);
		}

		$ticket = Ticket::find($ticketId);
		if (!$ticket)
		{
			return response()->json([
				                        'success'  => false,
				                        'errorMsg' => "Sorry! something is not right. Kindly refresh the page and try again"
			                        ]);
		}

		$files = [
			$customerProofFront,
			$customerProofBack
		];

		try
		{
			// remove any existing proofs
			CustomerOrderProof::where('ticket_id', $ticketId)
			                  ->update([
				                           'updated_at' => Carbon::now(),
				                           'deleted_at' => Carbon::now()
			                           ]);

			$directPath = '/ticket/' . $ticketId . '/customer-proofs/';

			foreach ($files as $key => $file)
			{
				$option = ['isWatermark' => false];

				$fileName = $this->uploadImageToServer($file, $directPath, $option);

				$imgInsertData = [
					'ticket_id'     => $ticketId,
					'type_proof_id' => $proofType,
					'url'           => $fileName,
					'title'         => $this->getImageTitle($file),
					'face'          => $key + 1,
					'created_at'    => Carbon::now(),
					'updated_at'    => Carbon::now()
				];

				$existingProof = CustomerOrderProof::where('ticket_id', $ticketId)
				                                   ->where('type_proof_id', $proofType)
				                                   ->where('face', $key + 1)
				                                   ->whereNull('deleted_at')
				                                   ->whereNull('approved_at')
				                                   ->whereNull('rejected_at')
				                                   ->first();

				if ($existingProof)
				{
					$existingProof->url = $fileName;
					$existingProof->title = $this->getImageTitle($file);
					$existingProof->updated_at = Carbon::now();
					$existingProof->save();
				}
				else
				{
					CustomerOrderProof::create($imgInsertData);
				}
			}

			// alert team
			// alert customer

			$tmoToken = Hash::make($ticket->id . "EVBTMO");
			$tmoLink = $this->getShortenUrl(route("track.orders") . "?ref=SMS&id=" . $ticket->id . "&token=" . $tmoToken);

			$data = [
				'customerName'  => $ticket->name,
				'customerEmail' => $ticket->email,
				'customerPhone' => $ticket->phone,
				'partyDate'     => date('d M Y', $ticket->event_date),
				'ticketId'      => $ticket->id,
				'dashLink'      => config('evibe.dash_host') . '/ops-dashboard/customer-proofs',
				'tmoLink'       => $tmoLink
			];

			$tplCustomer = config('evibe.sms_tpl.customer_proof.upload.customer');
			$replaces = [
				'#customer#' => $ticket->name,
				'#tmoLink#'  => $tmoLink
			];

			$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);
			$smsData = [
				'to'   => $ticket->phone,
				'text' => $smsText
			];

			$this->dispatch(new MailCustomerProofUploadAlertToTeam($data));

			if ($data['customerEmail'])
			{
				$this->dispatch(new MailCustomerProofUploadAlertToCustomer($data));
			}

			if ($data['customerPhone'])
			{
				$this->dispatch(new SMSCustomerProofUploadToCustomer($smsData));
			}

			return response()->json([
				                        'success' => true,
			                        ]);
		} catch (\Exception $exception)
		{
			Log::error('Error occurred while saving images');
		}

	}
}