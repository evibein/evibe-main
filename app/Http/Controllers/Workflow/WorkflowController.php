<?php

namespace App\Http\Controllers\Workflow;

use App\Http\Controllers\Base\BaseController;
use App\Models\Util\SiteErrorLog;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Hash;

class WorkflowController extends BaseController
{
	public function showRequirements($cityUrl, $planToken = null)
	{
		/**
		 * Not using this page anymore
		 * Disabled by JR on 09/01/2020
		 */
		return redirect(route("city.home", getCityUrl()));

		$data["planToken"] = $planToken;

		if (is_null(getCityUrl()))
		{
			session(["cityUrl" => $cityUrl]);
		}

		if ($this->agent->isMobile() && !($this->agent->isTablet()))
		{
			return view("vue-js.workflow.requirements-mob", ["data" => $data]);
		}
		else
		{
			return view("vue-js.workflow.requirements-des", ["data" => $data]);
		}
	}

	public function getRequirements($cityUrl, $planToken = null)
	{
		$userId = config("evibe.default.review_handler");
		$accessToken = $this->getAccessToken('', $userId);

		$url = config('evibe.api.base_url') . config('evibe.api.workflow.prefix') . '/' . $planToken;

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => '',
		];

		$res = $this->makeApiCallWithUrl($options);

		return response()->json($res);
	}

	public function saveOptions()
	{
		$userId = config("evibe.default.review_handler");
		$accessToken = $this->getAccessToken('', $userId);

		$url = config('evibe.api.base_url') . config('evibe.api.workflow.prefix');

		if (request("planToken") != "")
		{
			$url = $url . '/' . request("planToken");
		}

		$questionData = [];
		foreach (request("questions") as $key => $answer)
		{
			if ($answer != '')
			{
				$questionData[] = [
					"questionId"     => $key,
					"answerOptionId" => $answer
				];
			}
		}

		$jsonData = [
			"eventId"       => request("occasion"),
			"eventDetails"  => $questionData,
			"cityId"        => request("cityId"),
			"eventServices" => request("services"),
			"serviceTags"   => request("serviceDetails")
		];

		$options = [
			'method'      => request("planToken") == "" ? "POST" : "PUT",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => $jsonData,
		];

		$res = $this->makeApiCallWithUrl($options);

		if ($res["success"])
		{
			$res["redirectUrl"] = route("city.workflow.options", [getCityUrl(), $res["partyPlanToken"]]);
		}

		return response()->json($res);
	}

	public function showOptions($cityUrl, $searchId)
	{
		$data["searchId"] = $searchId;

		if ($this->agent->isMobile() && !($this->agent->isTablet()))
		{
			return view('vue-js.workflow.options-mob', ["data" => $data]);
		}
		else
		{
			return view('vue-js.workflow.options-des', ["data" => $data]);
		}
	}

	public function getOptions($cityUrl, $planToken)
	{
		$userId = config("evibe.default.review_handler");
		$accessToken = $this->getAccessToken('', $userId);

		$url = config('evibe.api.base_url') . config('evibe.api.workflow.prefix') . '/' . $planToken . "/options";

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => '',
		];

		$res = $this->makeApiCallWithUrl($options);

		return response()->json($res);
	}

	public function shortlistOption($cityUrl, $planToken)
	{
		$userId = config("evibe.default.review_handler");
		$accessToken = $this->getAccessToken('', $userId);

		$url = config('evibe.api.base_url') . config('evibe.api.workflow.prefix') . '/' . $planToken . '/shortlist';

		$jsonData = [
			"productId"     => request("productId"),
			"productTypeId" => request("productTypeId")
		];

		$options = [
			'method'      => "POST",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => $jsonData,
		];

		$res = $this->makeApiCallWithUrl($options);

		if ($res["success"])
		{
			return response()->json($res);
		}
		else
		{
			return response()->json(["success" => false]);
		}

	}

	public function removeShortlistedOption($cityUrl, $planToken)
	{
		$userId = config("evibe.default.review_handler");
		$accessToken = $this->getAccessToken('', $userId);

		$url = config('evibe.api.base_url') . config('evibe.api.workflow.prefix') . '/' . $planToken . '/shortlist';

		$jsonData = [
			"productId"     => request("productId"),
			"productTypeId" => request("productTypeId")
		];

		$options = [
			'method'      => "DELETE",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => $jsonData,
		];

		$res = $this->makeApiCallWithUrl($options);

		if ($res["success"])
		{
			return response()->json($res);
		}
		else
		{
			return response()->json(["success" => false]);
		}
	}

	public function enquireNow($cityUrl, $planToken)
	{
		$userId = config("evibe.default.review_handler");
		$accessToken = $this->getAccessToken('', $userId);

		$url = config('evibe.api.base_url') . config('evibe.api.workflow.prefix') . '/' . $planToken . "/enquire";

		$jsonData = [
			"name"              => request("name"),
			"phone"             => request("phone"),
			"email"             => request("email"),
			"requirement"       => request("requirement"),
			"bookingLikeliness" => request("bookingLikeliness"),
			"timeSlot"          => request("timeSlot")
		];

		$options = [
			'method'      => "POST",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => $jsonData,
		];

		$res = $this->makeApiCallWithUrl($options);

		if ($res["success"] && !is_null($res["success"]))
		{
			$url = route("ticket.thank-you", [$res["ticket"]["id"]]);
			$token = Hash::make($res["ticket"]["id"] . substr($res["ticket"]["created_at"]["date"], 0, -7));
			$url .= "?t=" . $token . "&from=" . route("city.home", $cityUrl);

			$res["redirectUrl"] = $url;
		}

		return response()->json($res);
	}

	public function getShortlistedOptions($cityUrl, $planToken)
	{
		$userId = config("evibe.default.review_handler");
		$accessToken = $this->getAccessToken('', $userId);

		$url = config('evibe.api.base_url') . config('evibe.api.workflow.prefix') . '/' . $planToken . "/shortlist";

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => "",
		];

		$res = $this->makeApiCallWithUrl($options);

		return response()->json($res);
	}

	public function makeApiCallWithUrl($options)
	{
		$url = $options['url'];
		$method = $options['method'];
		$accessToken = $options['accessToken'];
		$jsonData = $options['jsonData'];

		try
		{
			$client = new Client();
			$res = $client->request($method, $url, [
				'headers' => [
					'access-token' => $accessToken
				],
				'json'    => $jsonData,
			]);

			$res = $res->getBody();

			$res = json_decode($res, true);

			return $res;

		} catch (\GuzzleHttp\Exception\GuzzleException $ge)
		{
			$this->handleException($ge, $url, "API_ERROR_GUZZLE");

		} catch (\Exception $e)
		{
			$this->handleException($e, $url, "API_ERROR");
		}
	}

	private function handleException($e, $url, $code)
	{
		$apiResponse = $e->getResponse()->getBody(true);
		$apiResponse = \GuzzleHttp\json_decode($apiResponse);
		$res['error'] = $apiResponse->errorMessage;

		SiteErrorLog::create([
			                     "url"     => $url,
			                     "code"    => $code,
			                     "details" => $res['error']
		                     ]);

		$this->sendErrorReport($e);

		return false;
	}
}
