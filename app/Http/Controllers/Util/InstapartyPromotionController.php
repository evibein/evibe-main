<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;

class InstapartyPromotionController extends BaseController
{
	public function showPromotionalPage()
	{
		$data = [
			'heroCollageImg1' => config('evibe.gallery.host').'/main/landing/party-supplies/img-collage-left-1.jpg',
			'heroCollageImg2' => config('evibe.gallery.host').'/main/landing/party-supplies/img-collage-left-2.jpg',
			'heroCollageImg3' => config('evibe.gallery.host').'/main/landing/party-supplies/img-collage-left-3.png',
			'heroCollageImg4' => config('evibe.gallery.host').'/main/landing/party-supplies/img-collage-right.png',
			'pageTitle'   => "Buy latest party supplies and make your party fab!",
			'headerTag'   => "Your Trusted party Planner",
			//'ctaButton'   => config("landing.$keyWord.cta-button"),
			'meta'        => [
				'description'    => "Best quality party supplies",
				'key-words'      => "party-supplies, home-parties,party-supplies-under-1000",
				'og-title'       => "Your Trusted Party Planner",
				'og-description' => "Do it yourself party supplies for your celebration.",
				'og-url'         => "https://evibe.in/party-supplies",
				'canonical'      => "/party-supplies"
			],
			//'benefits'    => [
			//	'1' => config("landing.$keyWord.benefits.1"),
			//	'2' => config("landing.$keyWord.benefits.2"),
			//	'3' => config("landing.$keyWord.benefits.3"),
			//	'4' => config("landing.$keyWord.benefits.4")
			//],
			//'imageLinks'  => [
			//	'collage'    => $galleryHost . config("landing.$keyWord.image-links.collage"),
			//	'sneakPeaks' => [
			//		'1' => $galleryHost . config("landing.$keyWord.image-links.sneak-peaks.1"),
			//		'2' => $galleryHost . config("landing.$keyWord.image-links.sneak-peaks.2"),
			//		'3' => $galleryHost . config("landing.$keyWord.image-links.sneak-peaks.3"),
			//		'4' => $galleryHost . config("landing.$keyWord.image-links.sneak-peaks.4"),
			//	]
			//]
		];

		if ($this->agent->isMobile() && !($this->agent->isTablet()))
		{
			$data['heroImg'] = config('evibe.gallery.host').'/main/landing/party-supplies/instaparty-evibe-promo-mobile.png';
		}
		else
		{
			$data['heroImg'] = config('evibe.gallery.host').'/main/landing/party-supplies/instaparty-evibe-promo.png';
		}

		return view('util.instaparty-landing', ['data' => $data]);
	}
}