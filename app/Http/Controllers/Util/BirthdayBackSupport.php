<?php

namespace App\Http\Controllers\Util;

/**
 * SEO for all birthday party pages has been updated since 16 June 2016
 *
 * @author Anji <anji@evibe.in>
 * @since  16 June 2016
 * @see    This code should be permanently deleted on or after September 2016
 */
class BirthdayBackSupport extends BackSupport
{
	public function newBirthdayHome($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.home', $cityUrl));
	}

	public function newBirthdayVenueList($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.venues.list', $cityUrl));
	}

	public function newBirthdayVenueLinks($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.venues.links', $cityUrl));
	}

	public function newBirthdayVenueProfile($cityUrl, $profileUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.venues.profile', [$cityUrl, $profileUrl]));
	}

	public function newBirthdayDecorList($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.decors.list', $cityUrl));
	}

	public function newBirthdayDecorProfile($cityUrl, $profileUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.decors.profile', [$cityUrl, $profileUrl]));
	}

	public function newBirthdayEntList($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.ent.list', $cityUrl));
	}

	public function newBirthdayEntProfile($cityUrl, $profileUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.ent.profile', [$cityUrl, $profileUrl]));
	}

	public function newBirthdayPackageList($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.packages.list', $cityUrl));
	}

	public function newBirthdayPackageProfile($cityUrl, $profileUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.packages.profile', [$cityUrl, $profileUrl]));
	}

	public function newBirthdayTrendList($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.trends.list', $cityUrl));
	}

	public function newBirthdayTrendProfile($cityUrl, $profileUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.trends.profile', [$cityUrl, $profileUrl]));
	}

	public function newBirthdayCakeList($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.cakes.list', $cityUrl));
	}

	public function newBirthdayCakeProfile($cityUrl, $profileUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.cakes.profile', [$cityUrl, $profileUrl]));
	}

	public function newBirthdayAddOnList($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.ent.list', $cityUrl));
	}

	public function newBirthdayAddOnProfile($cityUrl, $profileUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.ent.profile', [$cityUrl, $profileUrl]));
	}
}