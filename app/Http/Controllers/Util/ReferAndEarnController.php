<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use App\Jobs\Emails\ReferAndEarn\UpdateReferralShareCount;
use App\Models\Coupon\Coupon;
use App\Models\Types\TypeUserSpecialDate;
use App\Models\Util\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ReferAndEarnController extends BaseController
{
	public function getCustomerStatistics()
	{
		$userId = request("id");
		$token = request("token");

		$user = User::find($userId);

		if ($user)
		{
			$hashContent = str_replace(" ", "", $userId . $user->name);
			if (Hash::check($hashContent, $token))
			{
				Auth::logout();
				Auth::loginUsingId($userId, true);

				$accessToken = $this->getAccessTokenFromUser($user);

				$url = config('evibe.api.base_url') . "common/" . config('evibe.api.referandearn.prefix') . "/statistics/" . $userId;

				$options = [
					'method'      => "GET",
					'url'         => $url,
					'accessToken' => $accessToken,
					'jsonData'    => []
				];

				$res = $this->makeApiCallWithUrl($options);

				if ($res && $res["success"])
				{
					return view("refer-earn.customer-re-dashboard", ["data" => $res]);
				}
			}
		}

		return redirect(route("missing"));
	}

	public function getUniqueReferralCode($ticketId)
	{
		$user = Auth::user();

		if (!$user)
		{
			$user = User::find(config("evibe.default.access_token_handler"));
		}
		$accessToken = $this->getAccessTokenFromUser($user);

		$url = config('evibe.api.base_url') . "common/" . config('evibe.api.referandearn.prefix');

		$options = [
			'method'      => "POST",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				'ticketId' => $ticketId
			]
		];

		$res = $this->makeApiCallWithUrl($options);

		if ($res && $res["success"])
		{
			return response()->json([
				                        'success' => true,
				                        'res'     => $res
			                        ]);
		}
		else
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => "Some error occurred while fetching your data, please reload the page."]);
		}
	}

	public function friendSignUpPage($referralId)
	{
		$user = User::where("referral_code", $referralId)->first();

		if (!$user) {
			return redirect(route("missing"));
		}

		$data = [
			"customerId" => $user->id,
			"typeAction" => "view",
			"sourceId"   => 1
		];

		$this->dispatch(new UpdateReferralShareCount($data));

		if ($user)
		{
			$specialDates = TypeUserSpecialDate::all();

			$data = [
				"specialDates" => $specialDates->where("is_relation", '')->pluck("name", "id")->toArray(),
				"relations"    => $specialDates->where("is_relation", 1)->pluck("name", "id")->toArray(),
				"referralCode" => $referralId
			];

			return view("refer-earn/friend-signup", ["data" => $data]);
		}
		else
		{
			return redirect(route("missing"));
		}
	}

	public function submitFriendSignup()
	{
		$user = Auth::user();

		if (!$user)
		{
			$user = User::find(config("evibe.default.access_token_handler"));
		}
		$accessToken = $this->getAccessTokenFromUser($user);

		$url = config('evibe.api.base_url') . "common/" . config('evibe.api.referandearn.prefix') . "/friend/signup";

		$options = [
			'method'      => "POST",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				'firstName'    => request("firstName"),
				'lastName'     => request("lastName"),
				'phone'        => request("phone"),
				'email'        => request("email"),
				'referralCode' => request("referralCode"),
				'specialDates' => request("specialDates")
			]
		];

		$res = $this->makeApiCallWithUrl($options);

		if ($res)
		{
			return response()->json([
				                        'success' => $res["success"],
				                        'res'     => $res
			                        ]);
		}
		else
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => "Some error occurred while fetching your data, please reload the page."]);
		}
	}

	public function friendThankYouPage($referralCode, $couponCode)
	{
		$user = User::where("referral_code", $referralCode)->first();
		$coupon = Coupon::where("coupon_code", $couponCode)->first();

		if ($user && $coupon)
		{
			$data = [
				"referralCode"      => $referralCode,
				"couponCode"        => $couponCode,
				"couponExpiryDate"  => $coupon->offer_end_time,
				"maxDiscountAmount" => $coupon->max_discount_amount
			];

			return view("refer-earn/friend-thank-you", ["data" => $data]);
		}
		else
		{
			return redirect(route("missing"));
		}
	}

	public function updateReferralShareCount($userId, $source)
	{
		if (config("evibe.referral.source." . $source) <= 0)
		{
			return redirect(route("missing"));
		}

		$data = [
			"customerId" => $userId,
			"typeAction" => "share",
			"sourceId"   => config("evibe.referral.source." . $source)
		];

		$this->dispatch(new UpdateReferralShareCount($data));

		return response()->json([
			                        "success" => true
		                        ]);
	}
}