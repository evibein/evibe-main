<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Payments\BasePaymentController;
use App\Models\Ticket\Ticket;

use App\Http\Requests;
use App\Models\Ticket\TicketBooking;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class OrderController extends BasePaymentController
{
	// order details for customer
	public function showCustomerOrderDetails($ticketId)
	{
		$ticket = Ticket::find($ticketId);
		$authCheck = Auth::check();
		if ($ticket)
		{
			// check for first token i.e, ticket id
			$token1 = request()->input('t1');
			if (($token1 && Hash::check($ticketId, $token1)) || $authCheck)
			{
				// check for second token. i.e, reminder key
				$token2 = request()->input('t2');
				if (($token2 && Hash::check($ticket->secret_token, $token2)) || $authCheck)
				{
					$data = $this->fetchCustomerOrderDetails($ticket);

					if (!$data)
					{
						Log::error("--- Customer order details page error :: Unable to fetch order details ---");

						return "Unable to load your order details. Kindly refresh the page and try again. If the issue still persists, write to us: " . config("evibe.contact.customer.group");
					}

					$data['isOrderPage'] = true;

					return view('order.customer', ['data' => $data]);
				}
				else
				{
					Log::error("--- Customer order details page error :: Invalid url or token2 (reminder token) mismatch ---");

					return $this->getInvalidMarkup();
				}
			}
			else
			{
				Log::error("--- Customer order details page error :: Invalid url or token1 (ticket id) mismatch ---");

				return $this->getInvalidMarkup();
			}
		}
		else
		{
			Log::error("--- Customer order details page error :: ticket id not found ---");

			return $this->getInvalidMarkup();
		}
	}

	// show partner order details
	public function showPartnerOrderDetails($bookingId)
	{
		$booking = TicketBooking::find($bookingId);
		if (!$booking)
		{
			abort('404');
		}
		$ticket = $booking->ticket;

		if ($booking && $ticket)
		{
			$token1 = request()->input('t1');
			$token2 = request()->input('t2');

			if ($token1 && Hash::check($bookingId, $token1))
			{
				if ($token2 && Hash::check($booking->secret_token, $token2))
				{
					// collect ticket and booking data
					$data = $this->getTicketData($ticket);
					$data['hasVenue'] = true;
					$data['venueName'] = '';
					$mapTypeId = $booking->mapping->map_type_id;
					$mapId = $booking->mapping->map_id;
					$mappedValues = $this->fillMappingValues([
						                                         'isCollection' => false,
						                                         'mapId'        => $mapId,
						                                         'mapTypeId'    => $mapTypeId
					                                         ]);
					$data['booking'] = $this->getTicketBookingData($booking, $mappedValues);

					$venuePartnerName = null;
					$venuePartnerPhone = null;
					$venuePartnerAltPhone = null;
					$allBookings = $ticket->bookings;
					foreach ($allBookings as $singleBooking)
					{
						if ($singleBooking->is_venue_booking)
						{
							$data['hasVenue'] = true;
							$data['venueName'] = $singleBooking->provider ? $singleBooking->provider->name : "";
						}

						if (!$venuePartnerName && ($singleBooking->map_type_id == config('evibe.ticket.type.venue')))
						{
							$venuePartnerName = $singleBooking->provider->person ? $singleBooking->provider->person : null;
							$venuePartnerPhone = $singleBooking->provider->phone ? $singleBooking->provider->phone : null;
							$venuePartnerAltPhone = $singleBooking->provider->alt_phone ? $singleBooking->provider->alt_phone : null;
						}
					}

					if ($venuePartnerName &&
						isset($data['booking']['venueDelivery']) && $data['booking']['venueDelivery'] &&
						isset($data['booking']['provider']['providerTypeId']) && ($data['booking']['provider']['providerTypeId'] != config('evibe.ticket.type.venue')) &&
						isset($data['booking']['itemMapTypeId']) && ($data['booking']['itemMapTypeId'] == config('evibe.ticket.type.add-on')))
					{
						$data['booking']['venuePartnerName'] = $venuePartnerName;
						$data['booking']['venuePartnerPhone'] = $venuePartnerPhone;
						$data['booking']['venuePartnerAltPhone'] = $venuePartnerAltPhone;
					}

					return view('order.partner', ['data' => $data]);
				}
				else
				{
					Log::error("--- Partner order details page error :: Invalid url or token2 (reminder token) mismatch ---");

					return $this->getInvalidMarkup();
				}
			}
			else
			{
				Log::error("--- Partner order details page error :: Invalid url or token1 (booking id) mismatch ---");

				return $this->getInvalidMarkup();
			}
		}
		else
		{
			Log::error("--- Partner order details page error :: booking or ticket not found ---");

			return $this->getInvalidMarkup();
		}
	}

	// show partner with multiple order page
	public function showPartnerMultipleOrderDetails($partnerId, $mapTypeId)
	{
		$token = request()->input('token');
		$tomStart = Carbon::tomorrow()->startOfDay()->timestamp;
		$tomEnd = Carbon::tomorrow()->endOfDay()->timestamp;

		$ticketBookings = TicketBooking::join('ticket', 'ticket_bookings.ticket_id', '=', 'ticket.id')
		                               ->where(['map_type_id' => $mapTypeId, 'map_id' => $partnerId])
		                               ->where('ticket.status_id', config('evibe.ticket.status.booked'))
		                               ->whereBetween('ticket_bookings.party_date_time', [$tomStart, $tomEnd])
		                               ->where('ticket_bookings.is_advance_paid', 1)
		                               ->select('ticket_bookings.*', 'ticket.name', 'ticket.area_id')
		                               ->get();

		if ($ticketBookings->count())
		{
			$firstBooking = $ticketBookings[0];
			$actualPartner = $firstBooking->provider;

			if ($actualPartner)
			{
				if ($token && Hash::check($partnerId, $token))
				{
					$bookingData = [];;
					foreach ($ticketBookings as $ticketBooking)
					{
						$bookingAmount = $ticketBooking->booking_amount;
						$advanceAmount = $ticketBooking->advance_amount;
						$balanceAmount = $bookingAmount - $advanceAmount;

						$bookingData[] = [
							'customerName'  => $ticketBooking->name,
							'partyTime'     => date("h:i A", $ticketBooking->party_date_time),
							'location'      => $ticketBooking->area ? $ticketBooking->area->name : "--",
							'balanceAmount' => $balanceAmount,
							'detailsLink'   => $this->generateBookingOrderDetailPageLink($ticketBooking, false) . '&ref=pMultipleOrder',
						];
					}

					$data = [
						'partnerName'  => $actualPartner->person,
						'tomorrowDate' => Carbon::tomorrow()->format("j F Y"),
						'bookingsData' => $bookingData
					];

					return view('order.partner_multiple_order', ['data' => $data]);
				}
				else
				{
					Log::error("--- Partner with multiple order page error :: token not found/mismatch error ---");

					return $this->getInvalidMarkup();
				}
			}
			else
			{
				Log::error("--- Partner with multiple order page error :: partner object not found ---");

				return $this->getInvalidMarkup();
			}
		}
		else
		{
			return $this->getInvalidMarkup("Sorry! No booking for tomorrow");
		}
	}

	private function getInvalidMarkup($text = '')
	{
		$message = $text ? $text : "Invalid Request";

		return "<h1>$message</h1>";
	}
}
