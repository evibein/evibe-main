<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;

class PaymentMethodModesController extends BaseController
{
	public function showAllPaymentModes()
	{
		return view('pay.payment-modes');
	}
}