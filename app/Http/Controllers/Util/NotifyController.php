<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use Illuminate\Support\Facades\Mail;

class NotifyController extends BaseController
{
	public function __construct()
	{
		parent::__construct();
	}

	public function notifyTechTeam()
	{
		if (!config('app.debug') && config('evibe.mail_errors'))
		{
			$url = request("url");
			$textStatus = request("textStatus", "");
			$errorThrown = request("errorThrown", "");

			$emailData = [
				"url"         => $url,
				"textStatus"  => $textStatus,
				"errorThrown" => $errorThrown,
				"browserInfo" => $_SERVER['HTTP_USER_AGENT'],
				"previousUrl" => $_SERVER['HTTP_REFERER']
			];

			$sub = "[#P1] AJAX request failed";
			if (request()->is('*/css*'))
			{
				$sub = "[#P1] CSS Page Load Failed";
			}

			if (!strpos($_SERVER['HTTP_USER_AGENT'], "Googlebot") && !strpos($_SERVER['HTTP_USER_AGENT'], "AdsBot-Google") && ($errorThrown != "") && !is_null($errorThrown))
			{
				Mail::send("emails.errors.report_team", $emailData, function ($m) use ($sub) {
					$m->from(config('evibe.contact.company.email'), "Error Alert")
					  ->to(config('evibe.contact.tech.group'))
					  ->subject($sub);
				});
			}
		}
	}
}