<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use App\Models\BrowsingActivity\BrowsingActivity;
use App\Models\Cake\Cake;
use App\Models\Cake\CakeGallery;
use App\Models\Decor\Decor;
use App\Models\Decor\DecorGallery;
use App\Models\Package\Package;
use App\Models\Types\TypeServices;
use App\Models\ViewCount\ProfileViewCounter;
use Illuminate\Support\Facades\Cookie;

class UserBrowsingHistoryController extends BaseController
{
	// For carousel
	public function userVisitActivityUpdate()
	{
		$data = $this->getBrowsingDataFromDB();

		return view("base.browsing-history-items")->with(['data' => $data]);
	}

	// For separate page
	public function showUserHistoryData()
	{

		$data = $this->getBrowsingDataFromDB();

		return view("base.recent-browsing-history")->with(['data' => $data]);
	}

	public function getBrowsingDataFromDB()
	{
		$userId = getAuthUserID();
		$viewCounterIds = [];
		$productsData = [];
		$data = [];

		// Function is user for both list page & ajax request, so handling likewise to fetch products
		$resultsCount = null;
		if (!request()->is('*my/browsing-history*'))
		{
			$resultsCount = 18;
			if ($this->agent->isMobile())
			{
				$resultsCount = 10;
			}
		}

		if ($userId)
		{
			$viewCounterIds = BrowsingActivity::where('user_id', $userId)
			                                  ->orderBy("created_at", "desc")
			                                  ->pluck('view_counter_profile_id', 'id')
			                                  ->unique()
			                                  ->take($resultsCount)
			                                  ->toArray();

			$profileViewCounter = ProfileViewCounter::whereIn('id', $viewCounterIds)
			                                        ->orderBy("created_at", "desc")
			                                        ->get()
			                                        ->keyBy("id");
		}
		else
		{
			if (Cookie::has('evbbrowsingid'))
			{
				$cookieId = Cookie::get('evbbrowsingid');
				$viewCounterIds = BrowsingActivity::where('cookie_id', $cookieId)
				                                  ->orderBy("created_at", "desc")
				                                  ->pluck('view_counter_profile_id', 'id')
				                                  ->unique()
				                                  ->toArray();

				$profileViewCounter = ProfileViewCounter::whereIn('id', $viewCounterIds)
				                                        ->orderBy('created_at', 'desc')
				                                        ->get()
				                                        ->keyBy("id");

			}
		}
		if (count($viewCounterIds) > 0)
		{

			foreach ($viewCounterIds as $key => $viewCounter)
			{

				if (isset($profileViewCounter[$viewCounter]))
				{

					$productsData[$key] = $profileViewCounter[$viewCounter]["product_id"] . "_" . $profileViewCounter[$viewCounter]["product_type_id"];
				}
			}

			$productsData = array_unique($productsData);

			if ($productsData > 0)
			{
				//GET THE PRODUCT DETAILS
				//stores the products' data
				if ($profileViewCounter)
				{
					$count = 0;

					foreach ($productsData as $product)
					{
						$product = explode("_", $product);
						switch ($product['1'])
						{
							case config("evibe.ticket.type.cake"): //Cake
								$productData = Cake::where('id', $product['0'])
								                   ->first();
								$prodImg = CakeGallery::where('cake_id', $product['0'])
								                      ->first()
								                      ->getLink();
								if ($productData)
								{
									$temp["id"] = $product['0'];
									$temp['ptid'] = $product['1'];
									$temp["title"] = $productData->title;
									$temp['price'] = $productData->price;
									$temp['img'] = $prodImg;
									$temp['priceworth'] = $productData->price_worth;
									$temp['metric'] = "/KG";
									$temp['link'] = $productData->getLink(null);
								}
								break;

							case config("evibe.ticket.type.decor"): //Decor
								$productData = Decor::where('id', $product['0'])
								                    ->first();
								$prodImg = DecorGallery::where('decor_id', $product['0'])
								                       ->first()
								                       ->getPath();
								if ($productData)
								{
									$temp["id"] = $product['0'];
									$temp['ptid'] = $product['1'];
									$temp["title"] = $productData->name;
									$temp['price'] = $productData->min_price;
									$temp['priceworth'] = $productData->worth;
									$temp['img'] = $prodImg;
									$temp['metric'] = "";
									$temp['link'] = $productData->getFullLinkForSearch(getCityName(), null);
								}
								break;

							case config("evibe.ticket.type.entertainment"): // Entertainment
								$productData = TypeServices::where('id', $product['0'])
								                           ->first();
								$prodImg = TypeServices::where('id', $product['0'])
								                       ->first()
								                       ->getProfilePic();
								if ($productData)
								{
									$temp["id"] = $product['0'];
									$temp['ptid'] = $product['1'];
									$temp["title"] = $productData->name;
									$temp['price'] = $productData->min_price;
									$temp['img'] = $prodImg;
									$temp['priceworth'] = $productData->worth_price;
									$temp['metric'] = "";
									$temp['link'] = $productData->getFullLinkForSearch(getCityName(), null);
								}
								break;

							//Packages
							case config("evibe.ticket.type.package"):
							case config("evibe.ticket.type.tents"):
								$productData = Package::where('id', $product['0'])
								                      ->first();
								$prodImg = Package::where('id', $product['0'])
								                  ->first()
								                  ->getProfileImg();

								if ($productData)
								{
									$temp["id"] = $product['0'];
									$temp['ptid'] = $product['1'];
									$temp["title"] = $productData->name;
									$temp['price'] = $productData->price;
									$temp['img'] = $prodImg;
									$temp['metric'] = "";
									$temp['priceworth'] = $productData->price_worth;
									$temp['link'] = $productData->getFullLinkForSearch(getCityName(), null, 'packages');
								}
								break;

							//priests
							case config("evibe.ticket.type.priests"):
								$productData = Package::where('id', $product['0'])
								                      ->first();
								$prodImg = Package::where('id', $product['0'])
								                  ->first()
								                  ->getProfileImg();
								if ($productData)
								{
									$temp["id"] = $product['0'];
									$temp['ptid'] = $product['1'];
									$temp["title"] = $productData->name;
									$temp['price'] = $productData->price;
									$temp['img'] = $prodImg;
									$temp['metric'] = "";
									$temp['priceworth'] = $productData->price_worth;
									$temp['link'] = $productData->getFullLinkForSearch(getCityName(), null, 'priest');
								}
								break;
							case config("evibe.ticket.type.food"):
								$productData = Package::where('id', $product['0'])
								                      ->first();
								$prodImg = Package::where('id', $product['0'])
								                  ->first()
								                  ->getProfileImg();
								if ($productData)
								{
									$temp["id"] = $product['0'];
									$temp['ptid'] = $product['1'];
									$temp["title"] = $productData->name;
									$temp['price'] = $productData->price;
									$temp['img'] = $prodImg;
									$temp['metric'] = "";
									$temp['priceworth'] = $productData->price_worth;
									$temp['link'] = $productData->getFullLinkForSearch(getCityName(), null, 'food');
								}
								break;

							//Surprices
							case config("evibe.ticket.type.surprises"):
								$productData = Package::where('id', $product['0'])
								                      ->first();
								$prodImg = Package::where('id', $product['0'])
								                  ->first()
								                  ->getProfileImg();
								if ($productData)
								{
									$temp["id"] = $product['0'];
									$temp['ptid'] = $product['1'];
									$temp["title"] = $productData->name;
									$temp['price'] = $productData->price;
									$temp['metric'] = "";
									$temp['priceworth'] = $productData->price_worth;
									$temp['img'] = $prodImg;
									$temp['link'] = $productData->getFullLinkForSearch(getCityName(), null, 'surprises');
								}
								break;
						}

						$data[$count] = $temp;
						$count++;
					}
				}
			}
		}

		return $data;
	}
}