<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseHomeController;
use App\Models\Types\TypeEvent;
use App\Models\Util\City;
use App\Models\Util\Country;
use App\Models\GoogleReviews\GoogleReview;

class CityHomeController extends BaseHomeController
{
	public function showCityHomePage($cityUrl)
	{
		$validUrlObj = $this->validateUrlParams($cityUrl);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		// views
		$cityUrl = strtolower($cityUrl); // fix issue with "Bangalore" / "Pune" entered in URL
		$googleReviews = GoogleReview::where('is_show', '1')->limit(6)->get();

		if (array_key_exists('passiveCity', $validUrlObj))
		{
			// SEO
			$passiveCityUrl = $cityUrl;
			$seo = [
				"pt" => "Best birthday party planners, surprise planners, housewarming in $cityUrl",
				"pd" => "Book top party services in Bangalore at best prices & quality. Top themes decorations, candle light dinners, surprises & more. See real pictures & customer reviews.",
				"ph" => "Let's plan your party online",
				"kw" => "birthday planners in $cityUrl, party organisers in $cityUrl, bachelor party organiser in $cityUrl, wedding reception planners in $cityUrl, engagement planners in $cityUrl, surprise planner in $cityUrl"
			];
			$pageTitle = $seo['pt'];
			$pageDescription = $seo['pd'];
			$pageHeader = $seo['ph'];
			$keywords = $seo['kw'];
			$schema = [$this->schemaBreadcrumbList(["Evibe.in", getCityName()])];
			$data = [

				'seo'            => [
					'pageTitle'       => ucfirst($pageTitle),
					'pageDescription' => $pageDescription,
					'pageHeader'      => $pageHeader,
					'keywords'        => $keywords,
					'schema'          => $schema
				],
				'countries'      => Country::all(),
				'passiveCity'    => true,
				'passiveCityUrl' => $passiveCityUrl,
				'reviews'        => $googleReviews
			];

			return view('base.passive-city')->with(['data' => $data]);
		}
		else
		{
			$headerView = "cities.$cityUrl.city-header";
			$contentView = "cities.$cityUrl.city-content";
			$footerView = "cities.$cityUrl.city-footer-top";
			$pressView = "cities.$cityUrl.city-press";
			// SEO
			$seo = $this->getSeoText([getCityId(), "home"], "defaults.city.home");
			$pageTitle = $seo['pt'];
			$pageDescription = $seo['pd'];
			$pageHeader = $seo['ph'];
			$keywords = $seo['kw'];
			$schema = [$this->schemaBreadcrumbList(["Evibe.in", getCityName()])];

			$data = [
				'views'      => [
					'header'  => $headerView,
					'content' => $contentView,
					'footer'  => $footerView,
					'press'   => $pressView
				],
				'seo'        => [
					'pageTitle'       => ucfirst($pageTitle),
					'pageDescription' => $pageDescription,
					'pageHeader'      => $pageHeader,
					'keywords'        => $keywords,
					'schema'          => $schema
				],
				'countries'  => Country::all(),
				'isHomePage' => true,
				'reviews'    => $googleReviews
			];

			return view('base.city')->with(['data' => $data]);
		}
	}

	public function ajaxGetPosts()
	{
		$response = $this->makeApiCallWithUrl([
			                                      'url'         => "http://evibe.in/blog/wp-json/wp/v2/posts?_embed&per_page=4&page=1",
			                                      'method'      => 'GET',
			                                      'accessToken' => '',
			                                      'jsonData'    => ''
		                                      ]);

		$data = [];
		$count = 0;
		foreach ($response as $post)
		{
			$title = $post['title']['rendered'];
			$content = str_limit(html_entity_decode(trim(strip_tags($post['content']['rendered']), "\n")), $limit = 150, $end = '...');
			$link = $post['link'];
			$imageSrc = isset($post["_embedded"]["wp:featuredmedia"]["0"]["source_url"]) ? $post["_embedded"]["wp:featuredmedia"]["0"]["source_url"] : $this->getGalleryBaseUrl() . "/main/img/blog_default.png";

			$data[$count++] = [
				"title"   => $title,
				"content" => $content,
				"link"    => $link,
				"image"   => $imageSrc
			];
		}

		return view("base.home.blog-posts")->with(['data' => $data]);
	}

	public function ajaxGetBenefits()
	{
		return view('base.home.why-us');
	}

	public function ajaxGetCustomerReviews()
	{
		// fetch required data
		$eventTypes = [
			config('evibe.occasion.kids_birthdays.id') => 3,
			config('evibe.occasion.bachelor.id')       => 1,
			config('evibe.occasion.pre-post.id')       => 1
		];

		// stories
		$customerStories = $this->getCustomerStories($eventTypes);

		$data = [
			'customerStories' => $customerStories
		];

		return view("base.home.customer-review")->with(['data' => $data]);
	}

	public function ajaxGetEvibeInMedia($cityUrl)
	{
		if (!$this->validateActiveCityUrl($cityUrl))
		{
			return "";
		}

		return view("cities.$cityUrl.city-press");
	}

	public function ajaxGetPartnerSignup()
	{
		// fetch required data
		$eventTypes = [
			config('evibe.occasion.kids_birthdays.id') => 3,
			config('evibe.occasion.bachelor.id')       => 1,
			config('evibe.occasion.pre-post.id')       => 1
		];

		// stories
		$vendorStories = $this->getVendorStories($eventTypes);

		$data = [
			'vendorStories' => $vendorStories
		];

		return view('base.home.vendor-comment')->with(['data' => $data]);
	}

	public function ajaxGetTopCollections($cityUrl)
	{
		if (!$this->validateActiveCityUrl($cityUrl))
		{
			return "";
		}

		// fetch required data
		$collections = $this->getCollections(getCityId(), null, 5, true);

		// collection base url
		$host = config('evibe.host');
		$profileUrl = config('evibe.occasion.collections.profile_url');
		$collectionBaseUrl = "$host/$cityUrl/$profileUrl/";

		$data = [
			'collectionBaseUrl' => $collectionBaseUrl,
			'collections'       => $collections
		];

		return view('base.home.collections')->with(['data' => $data]);
	}

	private function validateActiveCityUrl($cityUrl)
	{
		$validCityUrls = City::where("is_active", 1)->pluck("url")->toArray();

		if (in_array($cityUrl, $validCityUrls))
		{
			return true;
		}

		return false;
	}

	public function ajaxGetTopOption($eventId)
	{
		$event = TypeEvent::find($eventId);
		$data = [];

		if ($event)
		{
			switch ($eventId)
			{
				case config("evibe.occasion.kids_birthdays.id"):
					//$data[config("evibe.ticket.type.service")] = ["name" => $event->name . " top Ent Services", "options" => $this->getTopEntServices($eventId)];
					//$data[config("evibe.ticket.type.venue-deals")] = ["name" => $event->name . " top Venue Deals", "options" => $this->getTopVenueDeals($eventId)];
					//$data[config("evibe.ticket.type.trend")] = ["name" => $event->name . "trending Items", "options" => $this->getTrendingItems($eventId)];
					$data["categories"][config("evibe.ticket.type.decor")] = [
						"name"    => "Trending Birthday Decors",
						"url"     => route('city.occasion.birthdays.decors.list', getCityUrl()),
						"options" => $this->getTopDecors($eventId)
					];

					$data["categories"][config("evibe.ticket.type.cake")] = [
						"name"    => "Trending Birthday Cakes",
						"url"     => route('city.occasion.birthdays.cakes.list', getCityUrl()),
						"options" => $this->getTopCakes($eventId)
					];

					//$data["categories"][config("evibe.ticket.type.package")] = [
					//	"name"    => "Trending Birthday Packages",
					//	"url"     => route('city.occasion.birthdays.packages.list', getCityUrl()),
					//	"options" => $this->getTopPackages($eventId)
					//];

					break;

				case config("evibe.occasion.surprises.id"):
					$name = request("ref") == "cld" ? "cld" : config("evibe.ticket.type.surprises");
					$ref = "non-cld";
					if (request("ref") == "cld")
					{
						$ref = "cld";
					}
					$data["categories"][$name] = [
						"name"    => request("ref") == "cld" ? "Top Candle Light Dinners" : "Trending Surprise Packages",
						"url"     => request("ref") == "cld" ? route("city.cld.list", getCityUrl()) : route('city.occasion.surprises.package.list', getCityUrl()),
						"options" => $this->getTopSurprisePackages($eventId, $ref)
					];

					break;

				case config("evibe.occasion.pre-post.id"):
					$data["categories"][config("evibe.ticket.type.decor")] = [
						"name"    => "Trending Wedding Decors",
						"url"     => route('city.occasion.pre-post.decors.list', getCityUrl()),
						"options" => $this->getTopDecors($eventId)
					];

					$data["categories"][config("evibe.ticket.type.cake")] = [
						"name"    => "Trending Wedding Cakes",
						"url"     => route('city.occasion.pre-post.cakes.list', getCityUrl()),
						"options" => $this->getTopCakes($eventId)
					];

					$data["categories"][config("evibe.ticket.type.service")] = [
						"name"    => "Trending Wedding Services",
						"url"     => route('city.occasion.pre-post.ent.list', getCityUrl()),
						"options" => $this->getTopEntServices($eventId)
					];

					break;
			};
		}

		$data["eventId"] = $eventId;

		if ($this->agent->isMobile() && !($this->agent->isTablet()))
		{
			return view("base.home.top-options-mob", ["data" => $data]);
		}
		else
		{
			return view("base.home.top-options-des", ["data" => $data]);
		}

	}
}