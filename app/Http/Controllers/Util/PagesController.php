<?php

namespace App\Http\Controllers\Util;

use App\Jobs\Emails\Application\MailCustomApplicationAlertToApplicant;
use App\Jobs\Emails\Application\MailCustomApplicationAlertToTeam;
use App\Jobs\Emails\ReportIssue\ReportIssueAlertToTeam;
use App\Jobs\Emails\SignUp\MailCLDSignUpAlertToTeamJob;
use App\Jobs\Emails\Util\MailCustomerNotInterestedToTeam;
use App\Jobs\HandleStoppingTicketAutoFollowup;
use App\Models\Collection\CollectionOptions;
use App\Models\Package\Package;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketFalloutFeedback;
use App\Models\Ticket\TicketFollowup;
use App\Models\Ticket\TicketReminderStack;
use App\Models\Types\TypeFalloutFeedback;
use App\Models\Util\ReportIssue;
use App\Models\Util\City;

use App\Models\Util\FAQ;
use App\Models\Util\Feedback;
use App\Models\Util\Team;
use App\Models\Util\VendorSignUp;
use App\Models\Vendor\Vendor;
use App\Jobs\Emails\Feedback\MailWebsiteFeedbackToCustomerJob;
use App\Jobs\Emails\Feedback\MailWebsiteFeedbackToTeam;
use App\Jobs\Emails\SignUp\MailSignUpAlertToTeamJob;
use App\Jobs\Emails\SignUp\MailSignUpThanksToVendorJob;
use App\Models\Types\TypeVendor;
use App\Http\Controllers\Base\BaseHomeController;

use App\Models\Venue\Venue;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class PagesController extends BaseHomeController
{
	public function redirectToCity()
	{
		if ($cityUrl = getCityUrl())
		{
			return redirect($cityUrl);
		}
		else
		{
			return view('app/select_city');
		}
	}

	public function showShortUrlProfile($shortUrl)
	{
		$vendor = Vendor::where('short_url', 'LIKE', $shortUrl)->first();

		if ($vendor)
		{
			$typeUrl = 'birthday-planner';
			$vendorUrl = $vendor->url;
			$vendorCityId = $vendor->city_id;
			$vendorCity = City::where('id', $vendorCityId)->first();
			$cityUrl = $vendorCity->url;

			$profileUrl = '/' . $cityUrl . '/' . $typeUrl . '/' . $vendorUrl;

			return redirect($profileUrl);
		}
		else
		{
			return redirect('/');
		}
	}

	/**
	 * About Us page
	 */
	public function showAboutPage()
	{
		// fetch required data
		$team = Team::orderBy('full_name', 'ASC')
		            ->whereNull('left_at')
		            ->get();

		$data = [
			'team' => $team
		];

		return $this->renderDataToViewFiles('app/about-us', $data);
	}

	public function showOffers($cityUrl)
	{
		return view('app/offers')->with(['cityId' => $cityUrl]);
	}

	public function postReportIssue(Request $request)
	{
		$rules = [
			'reportComment' => 'required|min:10',
			'reporterName'  => 'required|min:4',
			'reporterEmail' => 'required|email',
			'reporterPhone' => 'sometimes|digits:10'
		];

		$message = [
			'reportComment.required' => 'Please enter the issue.',
			'reportComment.min'      => 'Please enter the issue in detail.',
			'reporterName.required'  => 'Please enter your name.',
			'reporterName.min'       => 'Your name cannot be less that 4 characters.',
			'reporterPhone.digits'   => 'Please enter your valid phone number. If needed, we will contact you on this number.',
			'reporterEmail.required' => 'Please enter your email id.',
			'reporterEmail.email'    => 'Please enter your valid email address. If needed, we will contact you on this email.',
		];

		$validator = validator($request->all(), $rules, $message);
		if ($validator->fails())
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => $validator->messages()->first()
			                        ]);
		}

		$inputData = [
			'issue'           => request('reportComment'),
			'reporter_name'   => request('reporterName') ? request('reporterName') : null,
			'reporter_phone'  => request('reporterPhone', null),
			'reporter_email'  => request('reporterEmail'),
			'report_page_url' => request('reportUrl'),
			'city_id'         => request('reportCityId', null),
			'occasion_id'     => request('reportEventId', null),
			'map_type_id'     => request('reportPageId', null),
			'option_id'       => request('reportOptionId', null)
		];

		// save issue
		$reportIssue = ReportIssue::create($inputData);

		if ($reportIssue != false)
		{
			// getting map_type_id of the particular package for dash link generation
			$inputData['package_type_id'] = null;
			$package = Package::find($inputData['option_id']);
			if ($package)
			{
				$inputData['package_type_id'] = $package->map_type_id;
			}

			$this->dispatch(new ReportIssueAlertToTeam($inputData));

			return response()->json(['success' => true]);
		}
		else
		{
			return response()->json([
				                        'success' => false,
				                        'errors'  => 'Error while submitting, please try again later.'
			                        ]);
		}
	}

	public function postApplicationForm(Request $request)
	{
		$rules = [
			'name'    => 'required|min:4',
			'email'   => 'required|email',
			'options' => 'required',
			'comment' => 'required|min:10',
			'resume'  => 'required|mimes:pdf,doc,docx,txt|max:2048'
		];
		$message = [
			'name.required'    => 'Please enter your name',
			'name.min'         => 'Name cannot be less than 4 character',
			'email.required'   => 'Please enter your email',
			'email.email'      => 'Please enter a valid email',
			'options.required' => 'please select your job interest area',
			'comment.required' => 'Please mention why you want to join Evibe.in',
			'comment.min'      => 'Use minimum 10 words on why you want to join Evibe.in',
			'resume.required'  => 'Please upload your resume',
			'resume.mimes'     => 'Uploaded file format is not valid. (only .pdf, .doc, .txt are accepted)',
			'resume.max'       => 'Resume size cannot be more than 2 MB'
		];

		$validator = validator($request->all(), $rules, $message);
		if ($validator->fails())
		{
			return response()->json(['success' => false, 'error' => $validator->messages()->first()]);
		}

		$name = $request->input('name');
		$email = $request->input('email');
		$options = $request->input('options');
		$comment = $request->input('comment');
		$resume = $request->file('resume');

		// upload the image to server
		$directPath = '/resumes/';
		$fileExtension = $resume->getClientOriginalExtension();
		$resumeName = $this->uploadResumeToServer($resume, $directPath, $fileExtension);
		$resumeLinks = config('evibe.gallery.host') . '/resumes/' . $resumeName;

		try
		{
			// send email to team
			$emailData = [
				'name'    => $name,
				'email'   => $email,
				'options' => $options,
				'comment' => $comment,
				'resume'  => $resumeLinks
			];

			$this->dispatch(new MailCustomApplicationAlertToApplicant($emailData));
			$this->dispatch(new MailCustomApplicationAlertToTeam($emailData));

		} catch (\Exception $e)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'Some error while uploading, please try again after some time'
			                        ]);
		}

		return response()->json(['success' => true]);
	}

	public function showTermsPage()
	{
		return view('app/terms');
	}

	public function showPrivacyPage()
	{
		return view('app/privacy');
	}

	public function showRefundsPage()
	{
		return view('app/refunds');
	}

	public function showHowItWorksPage()
	{
		return view('app/how_it_works');
	}

	public function showMissingPage()
	{
		return view('errors.404');
	}

	public function showFAQsPage()
	{
		$faqs = FAQ::all();
		$faqsData = ['faqs' => $faqs];

		return view('app/faqs', ['data' => $faqsData]);
	}

	// partner sign-up
	public function showPartnerSignUpLandingPage()
	{
		// fetch required data
		$eventTypes = [
			config('evibe.occasion.kids_birthdays.id') => 3,
			config('evibe.occasion.bachelor.id')       => 1,
			config('evibe.occasion.pre-post.id')       => 1
		];

		$vendorStories = $this->getVendorStories($eventTypes);

		$howItWorks = "";
		$faqs = "";
		try
		{
			$howItWorks = File::get(resource_path('jsons/partner-signup/hiw.json'));
			$faqs = File::get(resource_path('jsons/partner-signup/faqs.json'));
		} catch (\Exception $e)
		{
			Log::error("Issue with loading FAQs | How It Works json files");
		}

		$howItWorks = json_decode($howItWorks, true);
		$faqs = json_decode($faqs, true);

		$data = [
			'vendorStories' => $vendorStories,
			'howItWorks'    => $howItWorks,
			'faqs'          => $faqs,
			'seo'           => [
				'pageTitle'       => 'Event Managers, Artists, Photographers, Venues - Why Partner With Evibe.in',
				'pageDescription' => 'Do you provide any party services? Are you an artist (or) event manager (or) venue? Partner with Evibe.in to grow your Business without any extra efforts. Get confirmed bookings & pay only when you earn. On time payments with 100% transparency.'
			]
		];

		return view('partner/why_partner')->with(['data' => $data]);
	}

	public function showCLDSignUpLandingPage()
	{
		return view('partner.why_partner_cld');
	}

	public function showGroupTableSignUpLandingPage()
	{
		return view('partner.why_partner_group_table');
	}

	public function submitCLDSignUp()
	{
		// validate rules
		$rules = [
			'name'  => 'required',
			'city'  => 'required',
			'phone' => 'required|phone',
		];

		$messages = [
			'name.required'  => 'Please enter your full name',
			'phone.required' => 'Please enter your 10 digits phone number',
			'phone.phone'    => 'Please enter a valid 10 digit phone number',
			'city.required'  => 'Please enter your city',
		];

		$validator = validator(request()->all(), $rules, $messages);
		// validate input
		if ($validator->fails())
		{
			return response()->json([
				                        "success" => false,
				                        "errors"  => $validator->messages()->first()
			                        ]);
		}
		else
		{
			$emailContent = [
				'name'           => request('name'),
				'city'           => request('city'),
				'phone'          => request('phone'),
				'restaurantName' => request('restaurantName'),
				'friendName'     => request('friendName'),
				'isFriend'       => request('isFriend')
			];

			$this->dispatch(new MailCLDSignUpAlertToTeamJob($emailContent));

			return response()->json([
				                        "success" => true
			                        ]);
		}
	}

	public function showVendorSignUpPage()
	{
		$partnerTypes = TypeVendor::pluck('name', 'id');
		$cities = City::pluck('name', 'id');
		$success = session('signup_success', false);

		$data = [
			'partnerTypes' => $partnerTypes,
			'cities'       => $cities,
			'success'      => $success,
			'seo'          => [
				'pageTitle'       => 'Partner Sign Up Form',
				'pageDescription' => 'Sign up form to partner with Evibe.in. Submit your contact information for us to get in touch with you.'
			]
		];

		// views cannot set sessions
		session()->put('success', session('signup_success', false));

		return view('partner/partner_up', $data);
	}

	public function postVendorSignUpDetails(Request $request)
	{
		if (!$this->googleCaptchaCheck())
		{
			return redirect('partner/signup/form#vsignup-form')->withErrors("Please accept the captcha")->withInput();
		}

		// validate rules
		$rules = [
			'vendorSignUpName'        => 'required',
			'vendorSignUpCompanyName' => 'required',
			'vendorSignupCategory'    => 'required',
			'vendorSignupCity'        => 'required',
			'vendorSignUpEmail'       => 'required|email',
			'vendorSignUpPhone'       => 'required|phone'
		];

		$messages = [
			'vendorSignUpName.required'        => 'Please enter your full name',
			'vendorSignUpCompanyName.required' => 'Please enter your company name',
			'vendorSignUpEmail.required'       => 'Please enter your email address',
			'vendorSignUpEmail.email'          => 'Please enter a valid email address',
			'vendorSignUpPhone.required'       => 'Please enter your 10 digits phone number',
			'vendorSignUpPhone.phone'          => 'Please enter a valid phone number',
			'vendorSignupCategory.required'    => 'Please select a category',
			'vendorSignupCity.required'        => 'Please select a city',
		];

		$validator = validator($request->all(), $rules, $messages);

		// validate input
		if ($validator->fails())
		{
			return redirect('partner/signup/form#vsignup-form')->withErrors($validator)->withInput();
		}
		else
		{
			$typeId = request('vendorSignupCategory');
			$cityId = request('vendorSignupCity');

			// push to database
			$inputData = [
				'person'       => request('vendorSignUpName'),
				'company_name' => request('vendorSignUpCompanyName'),
				'email'        => request('vendorSignUpEmail'),
				'phone'        => request('vendorSignUpPhone'),
				'message'      => request('vendorSignUpMessage'),
				'type_id'      => $typeId,
				'created_at'   => Carbon::now(),
				'updated_at'   => Carbon::now(),
				'city_id'      => $cityId,
			];

			$vendorSignUp = VendorSignUp::create($inputData);

			if ($vendorSignUp)
			{
				$emailContent = $inputData;
				$emailContent['categoryName'] = TypeVendor::find($typeId)->name;
				$emailContent['city_name'] = City::find($cityId)->name;

				// send email alerts
				$this->dispatch(new MailSignUpThanksToVendorJob($emailContent));
				$this->dispatch(new MailSignUpAlertToTeamJob($emailContent));

				return redirect()->route('partner.sign-up.show')->with('signup_success', true);
			}
			else
			{
				// TODO: handle error - for now returning the same view
				return redirect()->route('partner.sign-up.show')->with('signup_success', false);
			}
		}
	}

	/**
	 * Feedback page
	 */
	public function showFeedbackPage()
	{
		return view('app/feedback', ['success' => false]);
	}

	public function postWebsiteFeedback()
	{
		// validate rules
		$rules = [
			'feedbackFormName'     => 'required',
			'feedbackFormPhone'    => 'required|phone',
			'feedbackFormEmail'    => 'required|email',
			'feedbackFormComments' => 'required'
		];

		$messages = [
			'feedbackFormName.required'     => 'Please enter your full name',
			'feedbackFormPhone.required'    => 'Please enter your phone number',
			'feedbackFormPhone.phone'       => 'Please enter a valid phone number (10 digits number)',
			'feedbackFormEmail.required'    => 'Please enter your email address',
			'feedbackFormEmail.email'       => 'Please enter a valid email address',
			'feedbackFormComments.required' => 'Please enter your feedback'
		];

		$validator = Validator::make(request()->all(), $rules, $messages);

		// validate input
		if ($validator->fails())
		{
			return redirect('feedback')->withErrors($validator)->withInput();
		}
		else
		{
			// push to database
			$inputData = [
				'name'     => request('feedbackFormName'),
				'phone'    => request('feedbackFormPhone'),
				'email'    => request('feedbackFormEmail'),
				'comments' => request('feedbackFormComments')
			];

			$phone = request('feedbackFormPhone');
			$email = request('feedbackFormEmail');

			// discard spam
			if ($phone == "0910123654" || strpos($email, "qq.com") !== false)
			{
				return request()->json(["success" => "true"]);
			}

			try
			{
				Feedback::create($inputData);

				$emailContent = $inputData;

				// 13/07/21 - commented by @Anji to solve spam issue
				
				// $this->dispatch(new MailWebsiteFeedbackToCustomerJob($emailContent));

				// $this->dispatch(new MailWebsiteFeedbackToTeam($emailContent));

				return view('app/feedback', ['success' => true]);
			} catch (\Exception $e)
			{
				// TODO: handle error - for now returning the same view
				return view('app/feedback', ['success' => false]);
			}
		}
	}

	// Collection Advertisement Card shown in list pages
	public function showCollectionAdvCardInList(Request $request)
	{
		$cityId = $request->input('cityId');
		$occasionId = $request->input('occasionId');
		$mapTypeId = $request->input('mapTypeId');
		$optionsCount = CollectionOptions::where('city_id', $cityId)
		                                 ->where('event_id', $occasionId)
		                                 ->where('map_type_id', $mapTypeId)
		                                 ->count();
		if ($optionsCount > 0)
		{
			$host = config('evibe.host');
			$dataStore = app()->make("EvibeUtilDataStore");
			$cityUrl = $dataStore->getCityUrlFromId($request->input('cityId'));
			$occasionUrl = $dataStore->getOccasionUrlFromId($request->input('occasionId'));
			$collectionUrl = config('evibe.occasion.collections.results_url');
			$path = "$host/$cityUrl/$occasionUrl/$collectionUrl/";

			return response()->json([
				                        'success'  => true,
				                        'pageLink' => $path,
				                        'bgImg'    => config('evibe.gallery.host') . '/main/img/collection_adv.jpg'
			                        ]);
		}
		else
		{
			return response()->json(['success' => false]);
		}
	}

	/**
	 * Show all providers reviews
	 */
	public function showAllProviderReviews($productName, $providerType, $providerId)
	{
		$sortBy = request("sort", "topRated");
		$perPage = 10;
		$currentPage = request("page", 1);
		$currentPage = ($currentPage < 0) ? 1 : $currentPage;
		$skip = ($currentPage - 1) * $perPage;

		switch ($providerType)
		{
			case config("evibe.ticket.type.planner"):
				$partner = Vendor::find($providerId);
				$partner = $partner ? $partner : false;
				break;

			case config("evibe.ticket.type.venue"):
				$partner = Venue::find($providerId);
				$partner = $partner ? $partner : false;
				break;

			default:
				$partner = false;
		}

		if (!$partner)
		{
			return "<h1>Invalid Request</h1>";
		}

		// ensure valid sortBy as we depend on the index while rendering
		$sortBy = (in_array($sortBy, ["topRated", "mostRecent", "leastRated"])) ? $sortBy : "topRated";
		$reviewsData = $partner->getPartnerReview($skip, $perPage, $sortBy);

		$paginatedReviews = new LengthAwarePaginator(
			$reviewsData["reviews"][$sortBy],
			$reviewsData["total"]["count"],
			$perPage,
			$currentPage,
			['path' => Paginator::resolveCurrentPath()]
		);

		$data = [
			"sortBy"         => $sortBy,
			"productUrl"     => url()->previous(),
			"productName"    => getTextFromSlug($productName),
			"highlights"     => $reviewsData["highlights"],
			"reviews"        => $paginatedReviews,
			"isShowLocation" => ($providerType == 2) ? true : false
		];

		return view("app.review.show-all-reviews", $data);
	}

	public function askFalloutFeedback(Request $request)
	{
		$ticketId = $request->input('tId');
		$token = $request->input('token');
		$reminderId = $request->input('reminderId');
		$falloutUrl = $request->fullUrl();

		if (!$ticketId || !$token)
		{
			return "<h1>Invalid Request</h1>";
		}

		$ticket = Ticket::find($ticketId);

		if (!$ticket)
		{
			return "<h1>Invalid Request</h1>";
		}

		$matchToken = $ticket->name . $ticket->phone;

		if (!Hash::check($matchToken, $token) && !Hash::check($ticketId, $token))
		{
			$this->handleErrors(null, "Token mismatch for ticket $ticketId", '403');

			return "<h1>Invalid Request</h1>";
		}

		// @see: do not validate reminders as so existing links might not work
		$reminder = TicketReminderStack::select('ticket_reminders_stack.*', 'type_reminder_group.type_reminder_id')
		                               ->join('type_reminder_group', 'type_reminder_group.id', '=', 'ticket_reminders_stack.type_reminder_group_id')
		                               ->whereNull('ticket_reminders_stack.deleted_at')
		                               ->whereNull('ticket_reminders_stack.invalid_at')
		                               ->whereNull('ticket_reminders_stack.terminated_at')
		                               ->where('ticket_reminders_stack.id', $reminderId)
		                               ->first();

		if ($reminder && $reminder->type_reminder_id == config('evibe.type_reminder.retention'))
		{
			$this->dispatch(new HandleStoppingTicketAutoFollowup([
				                                                     'ticketId'       => $ticketId,
				                                                     'typeReminderId' => config('evibe.type_reminder.retention'),
				                                                     'handlerId'      => $ticket->handler_id ? $ticket->handler_id : config('evibe.default.access_token_handler'),
				                                                     'terminatedAt'   => time(),
				                                                     'stopMessage'    => config('evibe.ticket.status_message.retention_un_subscribe')
			                                                     ]));

			return view('app.fallout-feedback', ['retentionUnSubscribe' => true]);
		}

		// do not allow, if already submitted, display thank you message
		$ticketFalloutFeedback = TicketFalloutFeedback::where('ticket_id', $ticketId)->get();
		if (count($ticketFalloutFeedback))
		{
			return view('app.fallout-feedback', ['invalidThankYou' => true]);
		}

		if ($ticket->status_id == config('evibe.ticket.status.progress') ||
			$ticket->status_id == config('evibe.ticket.status.followup')
		)
		{
			$falloutOptionsArray = TypeFalloutFeedback::all()->toArray();
			$submittedOptions = TicketFalloutFeedback::where('ticket_id', $ticketId)->whereNotNull('type_fallout_feedback_id')->get();
			if ($submittedOptions)
			{
				$submittedOptionIds = $submittedOptions->pluck('type_fallout_feedback_id')->toArray();
				foreach ($falloutOptionsArray as $key => $opt)
				{
					if (in_array($opt['id'], $submittedOptionIds))
					{
						$falloutOptionsArray[$key]['submitted'] = true;
					}
				}
			}
			$commentsOption = TicketFalloutFeedback::where('ticket_id', $ticketId)->whereNotNull('comments')->first();

			$falloutUrlArr = explode('?', $falloutUrl);
			$falloutPostUrl = $falloutUrlArr[0];

			$data = [
				'ticketId' => $ticketId,
				'options'  => $falloutOptionsArray,
				'comments' => $commentsOption ? $commentsOption->comments : null,
				'postUrl'  => $falloutPostUrl . '/save',
			];

			return view('app.fallout-feedback', ['data' => $data]);
		}
		else
		{
			return "<h1>Invalid request</h1>";
		}

	}

	public function saveFalloutFeedback(Request $request)
	{
		try
		{
			$ticketId = $request->input('ticketId');
			$comment = $request->input('comment');
			$selectedOptions = $request->input('selectedOptions');

			if (!$ticket = Ticket::find($ticketId))
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'Unable to find ticket'
				                        ]);
			}

			if (!$selectedOptions && !$comment)
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'Either options or comment should be there to update'
				                        ]);
			}

			// @todo: only addition of options have been supported, subtraction of unselected options needs to be done
			// @see: this is only one time action
			$filteredIdArray = [];
			$oldOptions = TicketFalloutFeedback::where('ticket_id', $ticketId)->whereNull('deleted_at')->pluck('type_fallout_feedback_id')->toArray();
			if ($oldOptions && $selectedOptions)
			{
				foreach ($selectedOptions as $optionId)
				{
					if (!(in_array($optionId, $selectedOptions)))
					{
						array_push($filteredIdArray, $optionId);
					}
				}
			}
			elseif ($selectedOptions)
			{
				$filteredIdArray = $selectedOptions;
			}
			elseif ($oldOptions)
			{
				// do nothing @see: above comments
			}

			if (count($filteredIdArray))
			{
				foreach ($filteredIdArray as $filterId)
				{
					TicketFalloutFeedback::create([
						                              'ticket_id'                => $ticketId,
						                              'type_fallout_feedback_id' => $filterId,
						                              'created_at'               => Carbon::now(),
						                              'updated_at'               => Carbon::now()
					                              ]);
				}
			}

			if ($comment)
			{
				TicketFalloutFeedback::create([
					                              'ticket_id'  => $ticketId,
					                              'comments'   => $comment,
					                              'created_at' => Carbon::now(),
					                              'updated_at' => Carbon::now()
				                              ]);
			}

			$ticketReminder = TicketReminderStack::where('ticket_id', $ticketId)
			                                     ->whereNull('terminated_at')
			                                     ->whereNull('invalid_at')
			                                     ->whereNull('deleted_at')
			                                     ->first();

			// @todo: make an API call to terminate
			if ($ticketReminder)
			{
				$this->dispatch(new HandleStoppingTicketAutoFollowup([
					                                                     'ticketId'       => $ticketId,
					                                                     'typeReminderId' => config('evibe.type_reminder.followup'),
					                                                     'handlerId'      => $ticket->handler_id,
					                                                     'terminatedAt'   => time(),
					                                                     'stopMessage'    => config('evibe.ticket.status_message.customer_not_interested')
				                                                     ]));

				$ticket->status_id = config('evibe.ticket.status.not_interested');
				$ticket->save();
			}

			// stop any live followups
			$ticketFollowup = TicketFollowup::where('ticket_id', $ticketId)
			                                ->whereNull('is_complete')
			                                ->whereNull('deleted_at')
			                                ->first();

			if ($ticketFollowup)
			{
				// at any point of time, only one active followup exists
				$ticketFollowup->done_comment = 'Closed by system, as customer is not interested in booking with us';
				$ticketFollowup->is_complete = 1;
				$ticketFollowup->save();
			}

			$this->dispatch(new MailCustomerNotInterestedToTeam([
				                                                    'ticketId' => $ticketId,
				                                                    'name'     => $ticket->name,
				                                                    'phone'    => $ticket->phone,
				                                                    'email'    => $ticket->email,
				                                                    'eventId'  => $ticket->event_id,
				                                                    'cityId'   => $ticket->city_id
			                                                    ]));

			// mail CRM team

			return response()->json([
				                        'success' => true,
			                        ]);

		} catch (\Exception $e)
		{
			Log::error($e->getMessage());
		}
	}

	/**
	 * Private methods
	 */
	private function uploadResumeToServer($imageFile, $imageUploadTo, $file_extention)
	{
		$parentDirs = config('evibe.gallery.root');
		$fullUploadTo = $parentDirs . $imageUploadTo; // append with parent dirs
		$newImageName = request('email') . "." . $file_extention;
		$tempServerPath = $imageFile->getRealPath();

		if (!file_exists($fullUploadTo))
		{
			mkdir($fullUploadTo, 0777, true);
		}

		try
		{
			// move to the correct place
			move_uploaded_file($tempServerPath, $fullUploadTo . $newImageName);

			return $newImageName;

		} catch (\Exception $e)
		{
			Log::error("Exception with uploading resume: " . $fullUploadTo . $newImageName);
		}

		return false; // failed case
	}
}