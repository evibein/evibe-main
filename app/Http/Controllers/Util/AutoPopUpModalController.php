<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use App\Jobs\Emails\Tickets\MailAutoPopupAlertToTeamJob;
use App\Jobs\Emails\Tickets\MailAutoPopupTicketSuccessToCustomerJob;
use App\Models\AddOn\AddOn;
use App\Models\Cake\Cake;
use App\Models\Decor\Decor;
use App\Models\Package\Package;
use App\Models\Ticket\Ticket;
use App\Models\Trend\Trend;
use App\Models\Types\TypeEvent;
use App\Models\Types\TypeServices;
use App\Models\Types\TypeTicket;
use App\Models\Util\AutoPopUpQuestions;
use App\Models\Util\City;
use App\Models\Util\SiteErrorLog;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class AutoPopUpModalController extends BaseController
{
	public function getPageSpecificQuestions()
	{
		$pageTypeId = request("mapTypeId");
		$eventId = request("eventId");

		$popUpQuestions = AutoPopUpQuestions::select('id', 'question')
		                                    ->where(function ($query) use ($pageTypeId) {
			                                    $query->where('map_type_id', $pageTypeId)
			                                          ->orWhereNull('map_type_id');
		                                    })
		                                    ->where(function ($query) use ($eventId) {
			                                    $query->where('event_id', $eventId)
			                                          ->orWhereNull('event_id');
		                                    })
		                                    ->get()
		                                    ->toArray();

		if (count($popUpQuestions) > 0)
		{
			return response()->json([
				                        "success"   => true,
				                        "questions" => $popUpQuestions
			                        ]);
		}

		// If no questions are available don't show the popup
		return response()->json([
			                        "success" => false,
		                        ]);
	}

	public function saveFormData()
	{
		try
		{
			$inputData = request()->all();

			$rules = [
				"name"  => "required",
				"title" => "required",
				"phone" => "required|phone",
				//"email" => "required|email",
				"date"  => "date|after:yesterday",
			];

			$messages = [
				"name.required"  => "Please enter your full name",
				"phone.required" => "Please enter your phone number",
				"phone.phone"    => "Phone number is invalid (ex: 9640204000)",
				"email.required" => "Please enter your email address",
				"email.email"    => "Email address is invalid",
				"date.required"  => "Please select your party date",
				"date.date"      => "Please select a valid party date",
				"date.after"     => "Please select a valid party date",
				"title.required" => "Please select your title (Mr. / Mrs.)"
			];

			$validator = Validator::make($inputData, $rules, $messages);
			if ($validator->fails())
			{
				return [
					'success' => false,
					'error'   => $validator->messages()->first()
				];
			}

			$mapId = (int)$inputData['mapId'];
			$mapId = $mapId > 0 ? $mapId : null;
			$mapTypeId = (int)$inputData['mapTypeId'];
			$mapTypeId = $mapTypeId > 0 ? $mapTypeId : null;
			$validCities = City::where("is_public", 1)->pluck("id")->toArray();
			$validOccasions = TypeEvent::whereNull("parent_id")->pluck("id")->toArray();
			$sourceId = null;

			$comments = "<b>Ticket Raised from list page</b>";
			$optionDetails = [
				"optionName"  => "",
				"optionPrice" => "",
				"optionUrl"   => ""
			];

			$surprisesLandingCityName = request("surprisesTheatreCity");
			$surprisesLandingTheatreName = request("surprisesTheatreName");

			$pageUrl = $inputData["userPageUrl"];
			if (strpos($pageUrl, "/search?q=") !== false)
			{
				$comments = "Global Search results page: <a href='$pageUrl'>" . $pageUrl . "</a>";
			}

			if (isset($inputData['isCampaignPageEnquiry']) && $inputData['isCampaignPageEnquiry'] > 0)
			{
				$comments = "This is enquiry registered at campaign page. Page Url is: <a href='$pageUrl'>" . $pageUrl . "</a>";
			}

			if (isset($inputData['isInspirationsPageEnquiry']) && $inputData['isInspirationsPageEnquiry'] == 1)
			{
				$comments = "This is enquiry registered at Inspirations Page. Page Url is: <a href='$pageUrl'>" . $pageUrl . "</a>";
			}

			if ($mapId && $mapId > 0)
			{
				$comments = "";

				$comments = "Ticket Raised from profile page <b><br> User requirements:</b><br><ul>$comments</ul>";

				$parentTypeTicket = TypeTicket::find($mapTypeId);
				$mapTypeId = $parentTypeTicket ? ($parentTypeTicket->is_package == 1 ? 1 : $mapTypeId) : $mapTypeId;
				if ($mapTypeId == config("evibe.ticket.type.package"))
				{
					$option = Package::find($mapId);
					if (!$option)
					{
						SiteErrorLog::create([
							                     "code"    => "EVB-M-501",
							                     "details" => "Failed to find package with id $mapId"
						                     ]);

						return response()->json([
							                        "success" => false,
							                        "error"   => "Package not found, Please reload the page & try again"
						                        ]);
					}

					$optionDetails = [
						"optionName"  => $option->name . " [" . $option->code . "]",
						"optionPrice" => $option->price,
						"optionUrl"   => config("evibe.dash_host") . "/packages/view/" . $option->id
					];
				}
				elseif ($mapTypeId == config("evibe.ticket.type.decor"))
				{
					$option = Decor::find($mapId);
					if (!$option)
					{
						SiteErrorLog::create([
							                     "code"    => "EVB-M-501",
							                     "details" => "Failed to find decor with id $mapId"
						                     ]);

						return response()->json([
							                        "success" => false,
							                        "error"   => "Decor not found, Please reload the page & try again"
						                        ]);
					}

					$optionDetails = [
						"optionName"  => $option->name . " [" . $option->code . "]",
						"optionPrice" => $option->min_price,
						"optionUrl"   => config("evibe.dash_host") . "/decors/" . $option->id . "/info"
					];
				}
				elseif ($mapTypeId == config("evibe.ticket.type.entertainment"))
				{
					$option = TypeServices::find($mapId);
					if (!$option)
					{
						SiteErrorLog::create([
							                     "code"    => "EVB-M-501",
							                     "details" => "Failed to find service with id $mapId"
						                     ]);

						return response()->json([
							                        "success" => false,
							                        "error"   => "Service not found, Please reload the page & try again"
						                        ]);
					}

					$optionDetails = [
						"optionName"  => $option->name . " [" . $option->code . "]",
						"optionPrice" => $option->min_price,
						"optionUrl"   => config("evibe.dash_host") . "/services/details/" . $option->id
					];
				}
				elseif ($mapTypeId == config("evibe.ticket.type.cake"))
				{
					$option = Cake::find($mapId);
					if (!$option)
					{
						SiteErrorLog::create([
							                     "code"    => "EVB-M-501",
							                     "details" => "Failed to find cake with id $mapId"
						                     ]);

						return response()->json([
							                        "success" => false,
							                        "error"   => "Cake not found, Please reload the page & try again"
						                        ]);
					}

					$optionDetails = [
						"optionName"  => $option->title . " [" . $option->code . "]",
						"optionPrice" => $option->price,
						"optionUrl"   => config("evibe.dash_host") . "/cakes/" . $option->id . "/info"
					];
				}
				elseif ($mapTypeId == config("evibe.ticket.type.trend"))
				{
					$option = Trend::find($mapId);
					if (!$option)
					{
						SiteErrorLog::create([
							                     "code"    => "EVB-M-501",
							                     "details" => "Failed to find trend with id $mapId"
						                     ]);

						return response()->json([
							                        "success" => false,
							                        "error"   => "Trend not found, Please reload the page & try again"
						                        ]);
					}

					$optionDetails = [
						"optionName"  => $option->name . " [" . $option->code . "]",
						"optionPrice" => $option->price,
						"optionUrl"   => config("evibe.dash_host") . "/trends/view/" . $option->id
					];
				}
			}

			if (isset($inputData['isNavModal']))
			{
				if ($inputData['isNavModal'] == 2)
				{
					$comments = "This is enquiry from navigation modal. customer opted for <b>\"I Need Immediate Help\"</b><br>";
				}
				elseif ($inputData['isNavModal'] == 3)
				{
					$comments = "This is enquiry from navigation modal. customer opted for <b>\"I Need Concierge Service\"</b><br>";
				}
			}

			// Get appropriate event id
			$eventId = isset($inputData["eventId"]) ? $inputData["eventId"] : "";
			$eventId = ($eventId == "" || $eventId == " ") ? null : (int)$eventId;
			$eventId = (($eventId > 0) || in_array($eventId, $validOccasions)) ? $eventId : null;
			$enquirySourceId = ($inputData["isEnquiry"] != 2) ? config('evibe.ticket.enquiry_source.auto_popup') : ($mapId > 0 ? config('evibe.ticket.enquiry_source.product_enquiry') : config('evibe.ticket.enquiry_source.header_enquiry'));
			$createdAt = Carbon::now();
			if ($surprisesLandingCityName != "" && $surprisesLandingTheatreName != "")
			{
				$comments = "Theatre ad: <a href='$pageUrl'>" . $pageUrl . "</a>";
				$enquirySourceId = config('evibe.ticket.enquiry_source.product_enquiry');

				if (request("sourceId") == config("evibe.ticket.type_source.email-ad"))
				{
					$sourceId = config("evibe.ticket.type_source.email-ad");
				}
			}

			if (isset($inputData['isCampaignPageEnquiry']) && $inputData['isCampaignPageEnquiry'] > 0)
			{
				$enquirySourceId = $inputData['isCampaignPageEnquiry'];
			}

			if (isset($inputData['isInspirationsPageEnquiry']) && $inputData['isInspirationsPageEnquiry'] == 1)
			{
				$enquirySourceId = config("evibe.ticket.enquiry_source.inspiration_hub_enquiry");
			}

			if (isset($inputData['isNavModal']) && $inputData['isNavModal'] > 0)
			{
				$enquirySourceId = config("evibe.ticket.enquiry_source.navigation_modal");
			}

			$ticket = Ticket::create([
				                         "customer_gender"   => $inputData["title"],
				                         "name"              => $inputData["name"],
				                         "calling_code"      => (isset($inputData['callingCode']) && $inputData['callingCode']) ? $inputData['callingCode'] : null,
				                         "phone"             => $inputData["phone"],
				                         "email"             => $inputData["email"],
				                         "event_date"        => (isset($inputData["date"]) && $inputData["date"]) ? strtotime($inputData["date"]) : null,
				                         "comments"          => $comments . "<b>User Comment:</b> " . $inputData['requirements'],
				                         "city_id"           => in_array($inputData["cityId"], $validCities) ? $inputData["cityId"] : null,
				                         "event_id"          => $eventId,
				                         "type_id"           => $mapId,
				                         "type_ticket_id"    => $mapTypeId,
				                         "source_id"         => $sourceId,
				                         "enquiry_source_id" => $enquirySourceId,
				                         "created_at"        => $createdAt,
				                         "updated_at"        => $createdAt,
			                         ]);

			if (isset($inputData['isCampaignPageEnquiry']) && $inputData['isCampaignPageEnquiry'] > 0)
			{

				$this->updateTicketAction([
					                          'ticket'   => $ticket,
					                          'comments' => "Auto-popup modal ticket created by customer for campaign page products, want to book"
				                          ]);
			}
			else
			{
				$this->updateTicketAction([
					                          'ticket'   => $ticket,
					                          'comments' => "Auto-popup modal ticket created by customer, want to book "
				                          ]);
			}

			// generate enquiry id
			$ticketId = $ticket->id;
			$enquiryId = config("evibe.ticket.enq.pre.auto_popup") . $ticketId;
			$ticket->enquiry_id = $enquiryId;
			$ticket->save();

			$redirectUrl = route("ticket.thank-you", [$ticketId]);
			$token = Hash::make($ticketId . $ticket->created_at);
			$redirectUrl .= "?t=$token&from=" . urlencode(url()->previous());

			if ($mapId > 0)
			{
				$redirectUrl .= "&enqSource=product-enquiry";
			}
			else
			{
				$redirectUrl .= "&enqSource=list-enquiry";
			}

			// Add-ons data
			$addOns = null;
			$addOnsInputArray = (isset($inputData['addOns']) && $inputData['addOns']) ? $inputData['addOns'] : [];
			$addOnsInputArray = array_filter($addOnsInputArray); // to remove empty key value pairs
			if (count($addOnsInputArray))
			{
				$addOnsIds = [];
				foreach ($addOnsInputArray as $addOnId => $addOnCount)
				{
					if ($addOnCount)
					{
						array_push($addOnsIds, $addOnId);
					}
				}
				$addOns = AddOn::whereIn('id', $addOnsIds)->get();
			}

			$emailData = [
				"ticketId"                    => $ticket->id,
				"name"                        => $ticket->name,
				"phone"                       => $ticket->phone,
				"callingCode"                 => $ticket->calling_code,
				"email"                       => $ticket->email,
				"date"                        => $ticket->event_date,
				"planningStatus"              => $ticket->planning_status,
				"slot"                        => $ticket->customer_preferred_slot,
				"city"                        => $ticket->city_id,
				"comments"                    => $ticket->comments,
				"enquiryId"                   => $enquiryId,
				"bookingLikeliness"           => config("evibe.ticket.bookingLikeliness." . $ticket->booking_likeliness_id, "Not Set"),
				"dashLink"                    => config("evibe.dash_host") . "/tickets/" . $ticketId . "/information",
				"optionDetails"               => $optionDetails,
				"pageUrl"                     => $pageUrl,
				"occasionName"                => !is_null($eventId) ? config('evibe.occasionId.' . $eventId) : "Default",
				"isEnquiry"                   => $inputData["isEnquiry"],
				"surprisesLandingCityName"    => $surprisesLandingCityName,
				"surprisesLandingTheatreName" => $surprisesLandingTheatreName,
				"addOns"                      => $addOns
			];

			//$this->dispatch(new MailAutoPopupAlertToTeamJob($emailData));
			// @see: not asking for email to get the lead easily
			if ($ticket->email)
			{
				$this->dispatch(new MailAutoPopupTicketSuccessToCustomerJob($emailData));
			}

			return response()->json([
				                        "success"    => true,
				                        "redirectTo" => $redirectUrl
			                        ]);

		} catch (\Exception $exception)
		{
			$this->sendErrorReportToTeam($exception);

			return response()->json([
				                        "success" => false,
				                        "error"   => "An error occurred while submitting your request. Please try again later."
			                        ]);
		}
	}

	public function saveProductDeliveryFormData()
	{
		$inputData = request()->all();

		$rules = [
			"name"           => "required",
			"phone"          => "required|phone",
			"email"          => "required|email",
			"date"           => "required|date|after:yesterday",
			"timeSlot"       => "required|not_in:-1",
			"planningStatus" => "required|numeric|min:1"
		];

		$messages = [
			"name.required"           => "Please enter your full name",
			"phone.required"          => "Please enter your phone number",
			"phone.phone"             => "Phone number is invalid (ex: 9640204000)",
			"email.required"          => "Please enter your email address",
			"email.email"             => "Email address is invalid",
			"date.required"           => "Please select your party date",
			"date.date"               => "Please select a valid party date",
			"date.after"              => "Please select a valid party date",
			"timeSlot.required"       => "Please select your available time slot",
			"timeSlot.not_in"         => "Please select your available time slot",
			"planningStatus.required" => "Please select your planning status",
			"planningStatus.min"      => "Please select your planning status"
		];

		$validator = Validator::make($inputData, $rules, $messages);
		if ($validator->fails())
		{
			return [
				'success' => false,
				'error'   => $validator->messages()->first()
			];
		}

		$leadStatusId = "";
		if (in_array(request('planningStatus'), [1, 2, 3, 6]))
		{
			$leadStatusId = config("evibe.ticket.type_lead_status.hot");
		}
		elseif (in_array(request('planningStatus'), [4]))
		{
			$leadStatusId = config("evibe.ticket.type_lead_status.medium");
		}
		elseif (in_array(request('planningStatus'), [5]))
		{
			$leadStatusId = config("evibe.ticket.type_lead_status.cold");
		}

		$createdAt = Carbon::now();
		$eventId = getEventIdFromSession();
		$pageUrl = $inputData["userPageUrl"];

		try
		{
			$ticket = Ticket::create([
				                         "city_id"                 => getCityId(),
				                         "name"                    => $inputData["name"],
				                         "phone"                   => $inputData["phone"],
				                         "email"                   => $inputData["email"],
				                         "event_date"              => strtotime($inputData["date"]),
				                         "booking_likeliness_id"   => $inputData["planningStatus"],
				                         "customer_preferred_slot" => $inputData["timeSlot"],
				                         "enquiry_source_id"       => config('evibe.ticket.enquiry_source.delivery_image'),
				                         "lead_status_id"          => $leadStatusId,
				                         "event_id"                => $eventId,
				                         "created_at"              => $createdAt,
				                         "updated_at"              => $createdAt,
				                         "expected_closure_date"   => $this->getClosureDate($createdAt, $inputData["planningStatus"])
			                         ]);
		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);

			return response()->json([
				                        "success" => false,
				                        "error"   => "An error occurred while submitting your request. Please try again later."
			                        ]);
		}

		$this->updateTicketAction([
			                          'ticket'   => $ticket,
			                          'comments' => "Customer is interested in a delivery Image"
		                          ]);

		// generate enquiry id
		$ticketId = $ticket->id;
		$enquiryId = config("evibe.ticket.enq.pre.product_delivery") . $ticketId;
		$ticket->enquiry_id = $enquiryId;
		$ticket->save();

		$emailData = [
			"ticketId"                    => $ticket->id,
			"name"                        => $ticket->name,
			"phone"                       => $ticket->phone,
			"email"                       => $ticket->email,
			"date"                        => $ticket->event_date,
			"planningStatus"              => $ticket->planning_status,
			"slot"                        => $ticket->customer_preferred_slot,
			"city"                        => $ticket->city_id,
			"comments"                    => $ticket->comments,
			"enquiryId"                   => $enquiryId,
			"bookingLikeliness"           => config("evibe.ticket.bookingLikeliness." . $ticket->booking_likeliness_id, "Not Set"),
			"dashLink"                    => config("evibe.dash_host") . "/tickets/" . $ticketId . "/information",
			"pageUrl"                     => $pageUrl,
			"occasionName"                => !is_null($eventId) ? config('evibe.occasionId.' . $eventId) : "Default",
			"isEnquiry"                   => "",
			"surprisesLandingCityName"    => "",
			"surprisesLandingTheatreName" => "",
			"imageUrl"                    => config("evibe.gallery.host") . "/live-partner-images/" . $inputData["selectedPartnerImage"]
		];

		$this->dispatch(new MailAutoPopupAlertToTeamJob($emailData));
		$this->dispatch(new MailAutoPopupTicketSuccessToCustomerJob($emailData));

		return response()->json([
			                        "success" => true
		                        ]);
	}
}