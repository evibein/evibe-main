<?php

namespace App\Http\Controllers\Util;

use App\Models\Suppression\SuppressionEmailList;
use App\Models\Ticket\Ticket;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Hash;

class EmailCorrectionController extends TicketsController
{

	public function showEmailCorrectionPage($suppressionId)
	{
		$token = request('token');
		if (!Hash::check($suppressionId, $token))
		{
			$data = ['invalidAttempt' => true];

			return view('app.email-correction', ['data' => $data]);
		}

		$suppression = SuppressionEmailList::find($suppressionId);

		if ($suppression)
		{
			$ticket = Ticket::find($suppression->ticket_id);

			$data = [
				'suppressionId' => $suppressionId,
				'customerName'  => $ticket && $ticket->name ? $ticket->name : 'Customer',
				'invalidEmail'  => $suppression->email
			];

			if ($suppression->is_corrected)
			{
				$data['showThankYou'] = true;
			}
		}
		else
		{
			$data = [
				'invalidAttempt' => true
			];
		}

		return view('app.email-correction', ['data' => $data]);
	}

	public function saveEmailAddress()
	{
		$url = config('evibe.api.base_url') . config('evibe.api.aws.sns') . 'update-email';

		$res = ['success' => false];

		try
		{
			$client = new Client();
			$res = $client->request('POST', $url, [
				'json' => [
					'suppressionId'  => request('suppressionId'),
					'correctedEmail' => request('correctedEmail')
				]
			]);

			$res = $res->getBody();
			$res = \GuzzleHttp\json_decode($res, true);

			return response()->json($res);

		} catch (ClientException $e)
		{
			$apiResponse = $e->getResponse()->getBody(true);
			$apiResponse = \GuzzleHttp\json_decode($apiResponse);
			$res['error'] = $apiResponse->errorMessage;

			// @todo: report exception error to admin

			Log::error("FAILED TO GET RESPONSE FOR API CALL URL : $url . Response below");

			return response()->json([
				                        'success' => true,
				                        'error'   => 'Some error occurred while fetching data. Please try again after some time.'
			                        ]);
		}
	}
}