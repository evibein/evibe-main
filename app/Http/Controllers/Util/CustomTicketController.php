<?php

namespace App\Http\Controllers\Util;

use App\Jobs\Emails\Tickets\MailCustomTicketAlertToCustomer;
use App\Jobs\Emails\Tickets\MailCustomTicketAlertToTeam;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketGallery;
use App\Models\Types\TypeEvent;

class CustomTicketController extends TicketsController
{
	public function postDecorEnquiry($occasionId)
	{
		return $this->handleCakeDecorTicket($occasionId, 'Decoration', config('evibe.ticket.type.decor'));
	}

	public function postCakeEnquiry($occasionId)
	{
		return $this->handleCakeDecorTicket($occasionId, 'Cake', config('evibe.ticket.type.cake'));
	}

	public function postFoodEnquiry($occasionId)
	{
		return $this->handleFoodTicket($occasionId, 'Food', config('evibe.ticket.type.food'));
	}

	private function handleCakeDecorTicket($occasionId, $type, $typeId)
	{
		$imageLinks = [];
		$uploadCount = 0;

		$rules = [
			'name'      => 'required|min:4',
			'email'     => 'required|email',
			'phone'     => 'required|phone',
			'partyDate' => 'required|date',
			'location'  => 'required|min:4',
			'budget'    => 'required',
			'comment'   => 'required|min:10'
		];

		$message = [
			'name.required'      => 'Please enter your name',
			'name.min'           => 'Name cannot be less than 4 character',
			'email.required'     => 'Please enter your email',
			'email.email'        => 'Please enter a valid email',
			'phone.required'     => 'Please enter your phone',
			'phone.phone'        => 'Please enter you 10 digit valid phone number',
			'location.required'  => 'Please enter party location',
			'location.min'       => 'Location cannot be less than 4 characters',
			'partyDate.required' => 'Please enter party date',
			'partyDate.date'     => 'Please enter a valid party date',
			'budget'             => 'Please enter budget for your party',
			'budget.numeric'     => 'Please enter valid budget amount.',
			'comment.required'   => 'Please write your requirements',
			'comment.min'        => 'Requirement cannot be less than 10 characters '
		];

		$validator = validator(request()->all(), $rules, $message);
		if ($validator->fails())
		{
			return response()->json(['success' => false, 'error' => $validator->messages()->first()]);
		}

		// validate the images
		$imageFile = request()->file('images');
		$imageCount = count($imageFile);

		if ($imageCount)
		{
			if ($imageCount > 10)
			{
				return response()->json(['success' => false, 'error' => 'You cannot upload more that 10 images']);
			}

			foreach ($imageFile as $image)
			{
				$imgRules = [
					'image' => 'required|mimes:png,jpeg,jpg,JPG|max:2048'
				];

				$imgData = ['image' => $image];

				$imgMessage = [
					'image.required' => 'Please upload at least one image',
					'image.mimes'    => 'One of the images is not valid. (only .png, .jpeg, .jpg are accepted)',
					'image.max'      => 'Image size cannot br more than 2 MB'
				];

				$imgValidator = validator($imgData, $imgRules, $imgMessage);

				if ($imgValidator->fails())
				{
					return response()->json(['success' => false, 'error' => $imgValidator->messages()->first()]);
				}
			}
		}

		// get the area details or create a new area with or without pinCode
		$areaData = $this->getAreaForTheLocation(request()->input('location'), request()->input('locationDetails'));
		$areaId = $areaData['areaId'];
		$areaName = $areaData['areaName'];

		$name = request()->input('name');
		$email = request()->input('email');
		$phone = request()->input('phone');
		$partyDate = date('d/m/Y', strtotime(request()->input('partyDate')));

		$occasion = TypeEvent::find($occasionId)->name;
		$comment = "Enquiry came form $occasion occasion, " . strtolower($type) . " page type. Comments :: ";
		$comment .= request()->input('comment') . ' Location: ' . $areaName;

		$oldTicketId = request()->input('ticketId') ? request()->input('ticketId') : false;
		if ($oldTicketId)
		{
			$ticket = Ticket::find($oldTicketId);

			if (!$ticket)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Sorry, something when wrong. Please try again later.'
				                        ]);
			}
		}
		else
		{
			$ticket = Ticket::create([
				                         "city_id"           => getCityId() > 0 ? getCityId() : 1,
				                         'name'              => $name,
				                         'email'             => $email,
				                         'phone'             => $phone,
				                         'status_id'         => config('evibe.ticket.status.initiated'),
				                         'event_date'        => strtotime(request()->input('partyDate')),
				                         'event_id'          => $occasionId,
				                         'budget'            => request()->input('budget'),
				                         'comments'          => $comment,
				                         'area_id'           => $areaId,
				                         'enquiry_source_id' => ($typeId == config('evibe.ticket.type.decor')) ? config('evibe.ticket.enquiry_source.cd_decor') : config('evibe.ticket.enquiry_source.cd_cake')
			                         ]);

			$ticket->update(['enquiry_id' => config("evibe.ticket.enq.pre.custom_ticket") . $ticket->id]);

		}

		$ticketId = $ticket->id;
		$success = true;

		$this->updateTicketAction([
			                          'ticket'   => $ticket,
			                          'comments' => $oldTicketId ? config('evibe.ticket.status_message.reco_custom_ticket') : "Custom design ticket created by customer",
		                          ]);

		// save ticket data
		if ($imageCount)
		{
			// upload the image to server
			$directPath = '/ticket/' . $ticketId . '/images/';
			foreach ($imageFile as $image)
			{
				$imageName = $this->uploadImageToServer($image, $directPath, ['isWatermark' => false, 'isOptimize' => false]);

				$imgInsertData = [
					'url'       => $imageName,
					'title'     => $this->getImageTitle($image),
					'ticket_id' => $ticketId,
				];

				if (TicketGallery::create($imgInsertData))
				{
					$imageLinks[] = config('evibe.gallery.host') . '/ticket/' . $ticketId . '/images/' . $imageName;
					$uploadCount++;
				}
			}

			if ($imageCount != $uploadCount)
			{
				$success = false;
			}
		}

		if (!$success)
		{
			return response()->json(['success' => false, 'error' => 'Some error occurred while submitting your request']);
		}

		// send email to team
		$emailData = [
			'ticketId'  => $ticketId,
			'name'      => $name,
			'email'     => $email,
			'phone'     => $phone,
			'partyDate' => $partyDate,
			'location'  => $areaName,
			'budget'    => request()->input('budget'),
			'raisedFor' => "$occasion $type",
			'typeId'    => $typeId,
			'comment'   => request()->input('comment'),
			'link'      => config('evibe.dash_host') . '/tickets/' . $ticketId,
			'images'    => $imageLinks,
			'occasion'  => $occasion
		];
		if ($oldTicketId)
		{
			$emailData['teamSub'] = 'Custom ticket raised from recommendations';
			$emailData['fromRecos'] = 1;
		}

		$this->sendNotification($emailData);

		if ($oldTicketId)
		{
			return response()->json(['success' => true]);
		}

		$params = [
			"enqSource" => "custom-design",
			"occasion"  => $type
		];

		return response()->json($this->getTicketSuccessResponse($ticket, $params));
	}

	private function handleFoodTicket($occasionId, $type, $typeId)
	{
		$rules = [
			'name'            => 'required|min:4',
			'email'           => 'required|email',
			'phone'           => 'required|phone',
			'partyDate'       => 'required|date',
			'location'        => 'required|min:4',
			'foodType'        => 'required',
			'budgetPerPerson' => 'required|numeric',
			'foodServiceType' => 'required',
			'menuText'        => 'min:10|required_without:menu',
			'menu'            => 'mimes:txt,doc,docx,pdf,png,jpeg,jpg,JPG,PDF|max:2048|required_without:menuText',
			'venueMinGuests'  => 'required|numeric'
		];

		$message = [
			'name.required'             => 'Please enter your name',
			'name.min'                  => 'Name cannot be less than 4 character',
			'email.required'            => 'Please enter your email',
			'email.email'               => 'Please enter a valid email',
			'phone.required'            => 'Please enter your phone',
			'phone.phone'               => 'Please enter you 10 digit valid phone number',
			'location.required'         => 'Please enter party location',
			'location.min'              => 'Location cannot be less than 4 characters',
			'foodType.required'         => 'Please choose a menu type',
			'partyDate.required'        => 'Please enter party date',
			'partyDate.date'            => 'Please enter a valid party date',
			'budgetPerPerson.required'  => 'Please enter budget for your party',
			'budgetPerPerson.numeric'   => 'Please enter valid budget amount',
			'foodServiceType'           => 'Please choose a food service type',
			'menuText.required_without' => 'Please enter your menu or upload it',
			'menuText.min'              => 'Menu cannot be less than 10 characters',
			'menu.required_without'     => 'Please upload your custom menu',
			'menu.mimes'                => 'Uploaded file format is not valid. (only .pdf, .doc, .docx, .txt, .png, .jpg are accepted)',
			'menu.max'                  => 'Uploaded file size cannot be more than 2 MB',
			'venueMinGuests.required'   => 'Please enter the guests count for your party',
			'venueMinGuests.numeric'    => 'Please enter valid guests count'
		];

		$validator = validator(request()->all(), $rules, $message);
		if ($validator->fails())
		{
			return response()->json(['success' => false, 'error' => $validator->messages()->first()]);
		}

		$name = request()->input('name');
		$email = request()->input('email');
		$phone = request()->input('phone');
		$foodType = request()->input('foodType');
		$foodServiceType = request()->input('foodServiceType');
		$venueMinGuests = request()->input('venueMinGuests');
		$partyDate = date('d/m/Y', strtotime(request()->input('partyDate')));
		$menuText = request()->input('menuText');
		$imageFile = request()->file('menu');
		$occasion = TypeEvent::find($occasionId)->name;

		// get the area details or create a new area with or without pinCode
		$areaData = $this->getAreaForTheLocation(request()->input('location'), request()->input('locationDetails'));
		$areaId = $areaData['areaId'];
		$areaName = $areaData['areaName'];

		$oldTicketId = request()->has('ticketId') ? request()->input('ticketId') : false;
		if ($oldTicketId)
		{
			$ticket = Ticket::find($oldTicketId);

			if (!$ticket)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Some error occurred while submitting your request'
				                        ]);
			}

		}
		else
		{
			// save ticket data
			$ticket = Ticket::create([
				                         "city_id"           => getCityId(),
				                         'name'              => $name,
				                         'email'             => $email,
				                         'phone'             => $phone,
				                         'status_id'         => config('evibe.ticket.status.initiated'),
				                         'event_date'        => strtotime(request()->input('partyDate')),
				                         'event_id'          => $occasionId,
				                         'budget_per_person' => request()->input('budgetPerPerson'),
				                         'venue_min_guests'  => $venueMinGuests,
				                         'food_type'         => $foodType,
				                         'food_service_type' => $foodServiceType,
				                         'comments'          => $menuText,
				                         'area_id'           => $areaId,
				                         'enquiry_source_id' => config('evibe.ticket.enquiry_source.cd_food')
			                         ]);

			$ticket->update(['enquiry_id' => config("evibe.ticket.enq.pre.custom_ticket") . $ticket->id]);
		}

		$ticketId = $ticket->id;

		$this->updateTicketAction([
			                          'ticket'   => $ticket,
			                          'comments' => $oldTicketId ? config('evibe.ticket.status_message.reco_custom_ticket') : "Custom design food ticket created by customer"
		                          ]);

		// upload the image to server
		$imageLink = "";
		if ($imageFile)
		{
			$directPath = '/ticket/' . $ticketId . '/images/';
			$imageName = $this->uploadImageToServer($imageFile, $directPath, ['isWatermark' => false, 'isOptimize' => false]);

			$imgInsertData = [
				'url'       => $imageName,
				'title'     => $this->getImageTitle($imageFile),
				'ticket_id' => $ticketId,
			];

			TicketGallery::create($imgInsertData);
			$imageLink = config('evibe.gallery.host') . '/ticket/' . $ticketId . '/images/' . $imageName;
		}

		// sending mails
		$emailData = [
			'ticketId'        => $ticketId,
			'name'            => $name,
			'email'           => $email,
			'phone'           => $phone,
			'partyDate'       => $partyDate,
			'location'        => $areaName,
			'budgetPerPerson' => request()->input('budgetPerPerson'),
			'raisedFor'       => "$occasion $type",
			'typeId'          => $typeId,
			'link'            => config('evibe.dash_host') . '/tickets/' . $ticketId,
			'occasion'        => $occasion,
			'venueMinGuests'  => $venueMinGuests,
			'foodType'        => $foodType,
			'foodServiceType' => $foodServiceType,
			'menuText'        => $menuText
		];
		if ($oldTicketId)
		{
			$emailData['teamSub'] = '[#' . $ticketId . '] Custom ticket raised from recommendations';
			$emailData['fromRecos'] = 1;
		}
		if ($imageLink)
		{
			$imageData = ['image' => $imageLink];
			$emailData = array_merge($emailData, $imageData);
		}
		$this->sendNotification($emailData);

		if ($oldTicketId)
		{
			return response()->json(['success' => true]);
		}

		$params = [
			"enqSource" => "custom-design",
			"occasion"  => $type
		];

		return response()->json($this->getTicketSuccessResponse($ticket, $params));
	}

	private function sendNotification($data)
	{
		$this->dispatch(new MailCustomTicketAlertToTeam($data));
		$this->dispatch(new MailCustomTicketAlertToCustomer($data));
	}
}