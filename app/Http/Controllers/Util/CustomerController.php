<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use App\Jobs\HandleStoppingTicketAutoFollowup;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketReminderStack;
use App\Models\Ticket\TicketThankYouCards;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CustomerController extends BaseController
{
	public function showThankYouCard()
	{
		$ticketId = request('tId');
		$token = request('token');

		if (!Hash::check($ticketId, $token))
		{
			$data = ['invalidAttempt' => true];

			return view('customer.thank-you-card', ['data' => $data]);
		}

		$ticket = Ticket::find($ticketId);
		if (!$ticket)
		{
			$data = ['error' => true];

			return view('customer.thank-you-card', ['data' => $data]);
		}

		// show thank you card
		$thankYouCard = TicketThankYouCards::where('ticket_id', $ticket->id)
		                                   ->whereNotNull('url')
		                                   ->whereNull('deleted_at')
		                                   ->first();
		if (!$thankYouCard)
		{
			$data = ['error' => true];

			return view('customer.thank-you-card', ['data' => $data]);
		}

		$data = [
			'ticketId'     => $ticketId,
			'customerName' => ucwords($ticket->name),
			'tycImage'     => $thankYouCard->url,
			'tycPath'      => config('evibe.gallery.root') . '/ticket/' . $ticket->id . '/thank-you-cards/' . $thankYouCard->url,
			'tycUrl'       => config('evibe.gallery.host') . '/ticket/' . $ticket->id . '/thank-you-cards/' . $thankYouCard->url,
			'tycThumbUrl'  => config('evibe.gallery.host') . '/ticket/' . $ticket->id . '/thank-you-cards/thumbs/' . $thankYouCard->url
		];

		// make API call to terminate
		$ticketReminder = TicketReminderStack::join('type_reminder_group', 'type_reminder_group.id', '=', 'ticket_reminders_stack.type_reminder_group_id')
		                                     ->whereNull('type_reminder_group.deleted_at')
		                                     ->whereNull('ticket_reminders_stack.deleted_at')
		                                     ->whereNull('ticket_reminders_stack.invalid_at')
		                                     ->whereNull('ticket_reminders_stack.terminated_at')
		                                     ->where('ticket_id', $ticket->id)
		                                     ->where('type_reminder_group.type_reminder_id', config('evibe.type_reminder.thank-you-card'))
		                                     ->select('ticket_reminders_stack.*', 'type_reminder_group.type_reminder_id')
		                                     ->first();
		if ($ticketReminder)
		{
			// trigger to stop auto followup
			$this->dispatch(new HandleStoppingTicketAutoFollowup([
				                                                     'ticketId'       => $ticket->id,
				                                                     'typeReminderId' => config('evibe.type_reminder.thank-you-card'),
				                                                     'handlerId'      => $ticket->handler_id,
				                                                     'terminatedAt'   => time(),
				                                                     'stopMessage'    => 'Customer visited thank you card download page'
			                                                     ]));
		}
		else
		{
			// Unable to find reminder to terminate or reminder has already been terminated
		}

		return view('customer.thank-you-card', ['data' => $data]);
	}

	public function downloadTYCImage()
	{
		$imagePath = request('imagePath');

		return \Illuminate\Support\Facades\Response::download($imagePath);
	}

	public function showOrders()
	{
		$customerId = Auth::user()->username;
		$allUpcomingEventTickets = Ticket::join('ticket_bookings', 'ticket.id', '=', 'ticket_bookings.ticket_id')
		                                 ->where('ticket.email', $customerId)
		                                 ->where('ticket.status_id', config("evibe.ticket.status.booked"))
		                                 ->whereNotNull('ticket.paid_at')
		                                 ->where('ticket.event_date', ">=", Carbon::now()->timestamp)
		                                 ->whereNull('ticket.deleted_at')
		                                 ->get();

		$previousUrl = url()->previous();
		if ($previousUrl == url()->current())
		{
			$previousUrl = "/";
		}

		$ticketData = [];
		foreach ($allUpcomingEventTickets as $ticket)
		{
			$tempData['id'] = $ticket->id;
			$tempData['enquiryId'] = $ticket->enquiry_id;
			$tempData['customerOrderDetailsLink'] = 'my/order/' . $ticket->id;
			$tempData['details'] = $ticket->booking_type_details;
			$tempData['partyDate'] = Carbon::createFromTimestamp($ticket->event_date)->toDateString();
			$tempData['partyLocation'] = isset($ticket->map_link) && $ticket->map_link ? "<a href='$ticket->map_link' target='_blank'>Google Map Link</a>" : $ticket->venue_address . " " . $ticket->venue_landmark;
			$tempData['bookingAmount'] = $ticket->booking_amount;
			array_push($ticketData, $tempData);
		}

		$data = [
			'bookings'    => $ticketData,
			'previousUrl' => $previousUrl
		];

		return $this->renderDataToViewFiles('login/orders', $data);
	}
}