<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use App\Models\Invite\InviteTemplate;

class BackSupport extends BaseController
{
	public function redirectStoriesToReviews($cityUrl)
	{
		return $this->permanentRedirect(route('city.stories.customer', $cityUrl));
	}

	public function redirectToSurprisesHome($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.surprises.home', $cityUrl));
	}

	public function redirectToSurprisesPackage($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.surprises.package.list', $cityUrl));
	}

	public function redirectToSurprisesPackageProfile($cityUrl, $packageUrl)
	{
		return $this->permanentRedirect(route('city.occasion.surprises.package.profile', [$cityUrl, $packageUrl]));
	}

	public function redirectToSurprisesCollection($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.surprises.collection.list', $cityUrl));
	}

	public function redirectToSurprisesCollectionProfile($cityUrl, $collectionUrl)
	{
		return $this->permanentRedirect(route('city.occasion.surprises.collection.profile', [$cityUrl, $collectionUrl]));
	}

	public function redirectExpList($cityUrl)
	{
		// redirect to packages page with tag special experiences
		return $this->permanentRedirect(route('city.occasion.surprises.package.list', $cityUrl));
	}

	public function redirectExpProfile($cityUrl, $expUrl)
	{
		return $this->permanentRedirect(route('city.occasion.surprises.package.profile', [$cityUrl, $expUrl]));
	}

	public function redirectVenuesList($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.venues.list', $cityUrl));
	}

	public function redirectVenuesLinks($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.venues.links', $cityUrl));
	}

	public function redirectVenueProfile($cityUrl, $venueUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.venues.profile', [$cityUrl, $venueUrl]));
	}

	public function redirectDecorsList($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.decors.list', $cityUrl));
	}

	public function redirectDecorBalloons($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.decors.category', [$cityUrl, "simple-balloon-decorations"]));
	}

	public function redirectDecorThemes($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.decors.category', [$cityUrl, "simple-theme-decorations"]));
	}

	public function redirectDecorProfile($cityUrl, $decoUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.decors.profile', [$cityUrl, $decoUrl]));
	}

	public function redirectPackagesList($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.packages.list', $cityUrl));
	}

	public function redirectPackageProfile($cityUrl, $packageUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.packages.profile', [$cityUrl, $packageUrl]));
	}

	public function redirectTrendsList($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.trends.list', $cityUrl));
	}

	public function redirectTrendProfile($cityUrl, $trendUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.trends.profile', [$cityUrl, $trendUrl]));
	}

	public function redirectCakesList($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.cakes.list', $cityUrl));
	}

	public function redirectCakeProfile($cityUrl, $cakeProfile)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.cakes.profile', [$cityUrl, $cakeProfile]));
	}

	public function redirectEnt($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.ent.list', $cityUrl));
	}

	public function redirectAddOns($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.ent.list', $cityUrl));
	}

	public function redirectToHome($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.birthdays.home', $cityUrl));
	}

	public function redirectToEngVenue($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.pre-post.venues.list', $cityUrl));
	}

	public function redirectToEngVenueProfile($cityUrl, $profileUrl)
	{
		return $this->permanentRedirect(route('city.occasion.pre-post.venues.profile', [$cityUrl, $profileUrl]));
	}

	public function redirectToEngCake($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.pre-post.cakes.list', $cityUrl));
	}

	public function redirectToEngCakeProfile($cityUrl, $profileUrl)
	{
		return $this->permanentRedirect(route('city.occasion.pre-post.cakes.profile', [$cityUrl, $profileUrl]));
	}

	public function redirectToEngHome($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.pre-post.home', $cityUrl));
	}

	public function redirectToEngDecor($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.pre-post.decors.list', $cityUrl));
	}

	public function redirectToEngDecorProfile($cityUrl, $profileUrl)
	{
		return $this->permanentRedirect(route('city.occasion.pre-post.decors.profile', [$cityUrl, $profileUrl]));
	}

	public function redirectToEngEnt($cityUrl)
	{
		return $this->permanentRedirect(route('city.occasion.pre-post.ent.list', $cityUrl));
	}

	public function redirectToEngEntProfile($cityUrl, $profileUrl)
	{
		return $this->permanentRedirect(route('city.occasion.pre-post.ent.profile', [$cityUrl, $profileUrl]));
	}

	public function redirectPartnerWhySignup()
	{
		return $this->permanentRedirect(route('partner.benefits'));
	}

	public function redirectPartnerSignup()
	{
		return $this->permanentRedirect(route('partner.sign-up.show'));
	}

	public function redirectUserBrowsingHistory()
	{
		return $this->permanentRedirect(route('user.browsing.history.list.new'));
	}

	public function redirectOldInvitesUrl()
	{
		return $this->permanentRedirect(route("e-cards.invites"));
	}

	public function redirectOldSurprisesUrl()
	{
		return $this->permanentRedirect(route("/h/couple-surprises"));
	}

	public function redirectOldInvites($inviteTemplateId)
	{
		$inviteTemplate = InviteTemplate::find($inviteTemplateId);

		$url = route("e-cards.home");
		if ($inviteTemplate)
		{
			$url = route("iv.create.party-details", $inviteTemplate->seo_url);
		}

		return $this->permanentRedirect($url);
	}

	public function permanentRedirect($to, $append = [])
	{
		if (!is_array($append))
		{
			$append = [];
		}

		$params = array_merge(request()->all(), $append, ['ref' => 'redirect-perm']);
		$constructed = '';

		foreach ($params as $key => $value)
		{
			$glue = '&';
			if (!$constructed)
			{
				$glue = '?';
			}

			$constructed .= $glue . "$key=$value";
		}

		return redirect($to . $constructed, 301);
	}

	public function redirectPartnerProfile()
	{
		return redirect(route("partner.dash.new.login"));
	}

	public function redirectPartyKitsHome()
	{
		return redirect(route("party-kits.home"));
	}

	public function redirectPartyKitsList($categoryUrl)
	{
		return redirect(route("party-kits.list", $categoryUrl));
	}
}
