<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use App\Jobs\Emails\Tickets\MailPriceCheckAlertToTeamJob;
use App\Models\Cake\Cake;
use App\Models\Decor\Decor;
use App\Models\Package\Package;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TypeTicketSource;
use App\Models\Trend\Trend;
use App\Models\Types\TypeEvent;
use App\Models\RequestCity\RequestCity;
use App\Models\Types\TypeServices;
use App\Models\Venue\VenueHall;
use App\Jobs\Emails\Tickets\MailTicketAlertToTeamJob;
use App\Jobs\Emails\Tickets\MailTicketSuccessToCustomerJob;
use App\Jobs\Emails\Tickets\MailTicketSuccessToPartyBagCustomerJob;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class TicketsController extends BaseController
{
	public function __construct()
	{
		parent::__construct();
	}

	public function showThankYouPage($ticketId)
	{
		$ticket = Ticket::with("city")->find($ticketId);
		if (!$ticket)
		{
			$this->logError("showThankYouPage:: Could not find ticket with id $ticketId");

			return view("tickets.invalid");
		}

		$token = request()->input("t", false);
		$from = request()->input("from", false);
		$key = $ticketId . $ticket->created_at;

		if (!$token || !$from || !Hash::check($key, $token))
		{
			return view("tickets.invalid");
		}

		$typeSources = TypeTicketSource::where('show_customer', 1)
		                               ->select('id', 'name')
		                               ->get()
		                               ->toArray();

		$typeOccasions = TypeEvent::where('show_customer', 1)
		                          ->select('id', 'name')
		                          ->get()
		                          ->toArray();

		$formFields = [];
		$typeTicketId = $ticket->type_ticket_id;

		switch ($typeTicketId)
		{
			case config("evibe.ticket.type.cake"):
				$formFields["askGuestsCount"] = false;
				break;
		}

		$header = false;
		if ($ticket->event_id &&
			$ticket->city_id &&
			isset($this->viewMappingsByEventIds[$ticket->event_id])
		)
		{
			$header = "cities." . strtolower($ticket->city->name) .
				".occasion." . $this->viewMappingsByEventIds[$ticket->event_id] .
				".header.header";

			if (!view()->exists($header))
			{
				$header = false;
			}
		}

		$data = [
			"ticketId"      => $ticketId,
			"name"          => $ticket->name,
			"email"         => $ticket->email,
			"from"          => is_null($from) ? $from : config("evibe.host"),
			"typeSources"   => $typeSources,
			"typeOccasions" => $typeOccasions,
			"header"        => $header,
			"fields"        => $formFields,
			"linkRef"       => "thank-you" // required for headers
		];

		return view("tickets.thank-you", $data);
	}

	public function postDecorTicket($occasionId, $decorId)
	{
		$decor = Decor::find($decorId);
		if (!$decor)
		{
			$this->logError("Failed to find decor with id $decorId");

			return response()->json([
				                        "success" => false,
				                        "errors"  => "Decor not found"
			                        ]);
		}

		$isValidInput = $this->validateUserInput(request()->all());
		if ($isValidInput !== true)
		{
			return response()->json($isValidInput);
		}

		$areaData = $this->getAreaForTheLocation(request("location"), request("locationDetails"));
		$areaId = $areaData['areaId'];
		$areaName = $areaData['areaName'];

		$inputData = [
			"city_id"           => getCityId(),
			"event_id"          => $occasionId,
			"type_id"           => $decorId,
			"type_ticket_id"    => config("evibe.ticket.type.decor"),
			"name"              => request("name"),
			"phone"             => request("phone"),
			"email"             => request("email"),
			"event_date"        => strtotime(str_replace("-", "/", request("partyDate"))),
			"party_location"    => $areaName,
			"area_id"           => $areaId,
			"enquiry_source_id" => config('evibe.ticket.enquiry_source.product_enquiry'),
			"lead_status_id"    => config('evibe.ticket.type_lead_status.hot')
		];

		if (getCityId() == -1)
		{
			$this->setCityFromPreviousUrl();
			$inputData["city_id"] = getCityId();
		}

		if ($this->checkForSpam(request('phone'), request('email')))
		{
			return response()->json(['success' => true]);
		}

		// save ticket
		$ticket = Ticket::create($inputData);
		if (!$ticket)
		{
			$this->logError("Failed to create ticket, email: " . request("email") . ", phone: " . request("phone"));

			return response()->json([
				                        "success" => false,
				                        "errors"  => "An error occurred while submitting your request. Please try again later."
			                        ]);
		}

		$this->updateTicketAction([
			                          'ticket'   => $ticket,
			                          'comments' => "Decor, normal ticket created by customer"
		                          ]);

		$ticketId = $ticket->id;
		$enquiryId = config("evibe.ticket.enq.pre.decor") . $ticketId;
		$ticket->enquiry_id = $enquiryId;
		$ticket->save();

		$emailContent = array_merge($inputData, $this->getEmailContentData($ticket, [
			"name" => $decor->name,
			"code" => $decor->code
		]));

		$emailContent["ticketId"] = $ticketId;

		$this->dispatchMailAlerts($emailContent);

		return response()->json($this->getTicketSuccessResponse($ticket));
	}

	public function postPackageTicket($occasionId, $packageId)
	{
		$package = Package::find($packageId);
		if (!$package)
		{
			$this->logError("Failed to find package with id $packageId");

			return response()->json([
				                        "success" => false,
				                        "errors"  => "Package not found"
			                        ]);
		}

		$isValidInput = $this->validateUserInput(request()->all());
		if ($isValidInput !== true)
		{
			return response()->json($isValidInput);
		}

		$inputData = [
			"city_id"           => getCityId(),
			"event_id"          => $occasionId,
			'type_ticket_id'    => $package->type_ticket_id,
			'type_id'           => $packageId,
			'name'              => request('name'),
			'phone'             => request('phone'),
			'email'             => request('email'),
			'event_date'        => strtotime(str_replace('-', '/', request('partyDate'))),
			"enquiry_source_id" => config('evibe.ticket.enquiry_source.product_enquiry'),
			"lead_status_id"    => config('evibe.ticket.type_lead_status.hot')
		];

		if (getCityId() == -1)
		{
			$this->setCityFromPreviousUrl();
			$inputData["city_id"] = getCityId();
		}

		// food, tents, priest etc
		if (request()->has("location"))
		{
			$areaData = $this->getAreaForTheLocation(request("location"), request("locationDetails"));
			$areaId = $areaData['areaId'];
			$areaName = $areaData['areaName'];
			$inputData["party_location"] = $areaName;
			$inputData["area_id"] = $areaId;
		}

		if ($this->checkForSpam(request('phone'), request('email')))
		{
			return response()->json(['success' => true]);
		}

		// save ticket
		$ticket = Ticket::create($inputData);
		if (!$ticket)
		{
			$this->logError("Failed to create ticket, email: " . request("email") . ", phone: " . request("phone"));

			return response()->json([
				                        'success' => false,
				                        "errors"  => "An error occurred while submitting your request. Please try again later."
			                        ]);
		}

		$this->updateTicketAction([
			                          'ticket'   => $ticket,
			                          'comments' => "Package, normal ticket created by customer"
		                          ]);

		$ticketId = $ticket->id;
		$enquiryId = config('evibe.ticket.enq.pre.package') . $ticketId;
		$ticket->enquiry_id = $enquiryId;
		$ticket->save();

		$emailContent = array_merge($inputData, $this->getEmailContentData($ticket, [
			"name" => $package->name,
			"code" => $package->code
		]));

		$emailContent["ticketId"] = $ticketId;

		$this->dispatchMailAlerts($emailContent);

		return response()->json($this->getTicketSuccessResponse($ticket));
	}

	public function postTrendTicket($occasionId, $trendId)
	{
		$trend = Trend::find($trendId);
		if (!$trend)
		{
			$this->logError("Failed to find trend with id $trendId");

			return response()->json([
				                        "success" => false,
				                        "errors"  => "Trend not found"
			                        ]);
		}

		$isValidInput = $this->validateUserInput(request()->all());
		if ($isValidInput !== true)
		{
			return response()->json($isValidInput);
		}

		$inputData = [
			"city_id"           => getCityId(),
			'event_id'          => $occasionId,
			'type_ticket_id'    => config('evibe.ticket.type.trend'),
			'type_id'           => $trendId,
			'name'              => request('name'),
			'phone'             => request('phone'),
			'email'             => request('email'),
			'event_date'        => strtotime(str_replace('-', '/', request('partyDate'))),
			"enquiry_source_id" => config('evibe.ticket.enquiry_source.product_enquiry'),
			"lead_status_id"    => config('evibe.ticket.type_lead_status.hot')
		];

		if (getCityId() == -1)
		{
			$this->setCityFromPreviousUrl();
			$inputData["city_id"] = getCityId();
		}

		if ($this->checkForSpam(request('phone'), request('email')))
		{
			return response()->json(['success' => true]);
		}

		// save ticket
		$ticket = Ticket::create($inputData);
		if (!$ticket)
		{
			$this->logError("Failed to create ticket, email: " . request("email") . ", phone: " . request("phone"));

			return response()->json([
				                        'success' => false,
				                        'errors'  => 'An error occurred while submitting your request. Please try again later.'
			                        ]);
		}

		$this->updateTicketAction([
			                          'ticket'   => $ticket,
			                          'comments' => "Trend, normal ticket created by customer"
		                          ]);

		$ticketId = $ticket->id;
		$enquiryId = config('evibe.ticket.enq.pre.trend') . $ticketId;
		$ticket->enquiry_id = $enquiryId;
		$ticket->save();

		$emailContent = array_merge($inputData, $this->getEmailContentData($ticket, [
			"name" => $trend->name,
			"code" => $trend->code
		]));

		$emailContent["ticketId"] = $ticketId;

		$this->dispatchMailAlerts($emailContent);

		return response()->json($this->getTicketSuccessResponse($ticket));
	}

	public function postServiceTicket($occasionId, $serviceId)
	{
		$service = TypeServices::find($serviceId);
		if (!$service)
		{
			$this->logError("Failed to find service with id $serviceId");

			return response()->json([
				                        "success" => false,
				                        "errors"  => "Service not found"
			                        ]);
		}

		$isValidInput = $this->validateUserInput(request()->all());
		if ($isValidInput !== true)
		{
			return response()->json($isValidInput);
		}
		$areaData = $this->getAreaForTheLocation(request("location"), request("locationDetails"));
		$areaId = $areaData['areaId'];
		$areaName = $areaData['areaName'];

		$inputData = [
			"city_id"           => getCityId(),
			'type_ticket_id'    => config('evibe.ticket.type.service'),
			'type_id'           => $serviceId,
			'event_id'          => $occasionId,
			'name'              => request('name'),
			'phone'             => request('phone'),
			'email'             => request('email'),
			'event_date'        => strtotime(str_replace('-', '/', request('partyDate'))),
			"party_location"    => $areaName,
			"area_id"           => $areaId,
			"enquiry_source_id" => config('evibe.ticket.enquiry_source.product_enquiry'),
			"lead_status_id"    => config('evibe.ticket.type_lead_status.hot')
		];

		if (getCityId() == -1)
		{
			$this->setCityFromPreviousUrl();
			$inputData["city_id"] = getCityId();
		}

		if ($this->checkForSpam(request('phone'), request('email')))
		{
			return response()->json(['success' => true]);
		}

		// save ticket
		$ticket = Ticket::create($inputData);
		if (!$ticket)
		{
			$this->logError("Failed to create ticket, email: " . request("email") . ", phone: " . request("phone"));

			return response()->json([
				                        'success' => false,
				                        "errors"  => "An error occurred while submitting your request. Please try again later."
			                        ]);
		}

		$this->updateTicketAction([
			                          'ticket'   => $ticket,
			                          'comments' => "Services, normal ticket created by customer"
		                          ]);

		$ticketId = $ticket->id;
		$enquiryId = config('evibe.ticket.enq.pre.service') . $ticketId;
		$ticket->enquiry_id = $enquiryId;
		$ticket->save();

		$emailContent = array_merge($inputData, $this->getEmailContentData($ticket, [
			"name" => $service->name,
			"code" => $service->code
		]));

		$emailContent["ticketId"] = $ticketId;

		$this->dispatchMailAlerts($emailContent);

		return response()->json($this->getTicketSuccessResponse($ticket));
	}

	public function postCakeTicket($occasionId, $cakeId)
	{
		$cake = Cake::with('provider')->find($cakeId);
		if (!$cake)
		{
			$this->logError("Failed to find cake with id $cakeId");

			return response()->json([
				                        "success" => false,
				                        "errors"  => "Service not found"
			                        ]);
		}

		$isValidInput = $this->validateUserInput(request()->all());
		if ($isValidInput !== true)
		{
			return response()->json($isValidInput);
		}
		$areaData = $this->getAreaForTheLocation(request("location"), request("locationDetails"));
		$areaId = $areaData['areaId'];
		$areaName = $areaData['areaName'];

		// insert into ticket table
		$inputData = [
			"city_id"           => getCityId(),
			'event_id'          => $occasionId,
			'type_ticket_id'    => config('evibe.ticket.type.cake'),
			'type_id'           => $cakeId,
			'name'              => request('name'),
			'email'             => request('email'),
			'phone'             => request('phone'),
			'event_date'        => strtotime(str_replace('-', '/', request('partyDate'))),
			"party_location"    => $areaName,
			"area_id"           => $areaId,
			"enquiry_source_id" => config('evibe.ticket.enquiry_source.product_enquiry'),
			"lead_status_id"    => config('evibe.ticket.type_lead_status.hot')
		];

		if (getCityId() == -1)
		{
			$this->setCityFromPreviousUrl();
			$inputData["city_id"] = getCityId();
		}

		if ($this->checkForSpam(request('phone'), request('email')))
		{
			return response()->json(['success' => true]);
		}

		// save ticket
		$ticket = Ticket::create($inputData);
		if (!$ticket)
		{
			$this->logError("Failed to create ticket, email: " . request("email") . ", phone: " . request("phone"));

			return response()->json([
				                        'success' => false,
				                        "errors"  => "An error occurred while submitting your request. Please try again later."
			                        ]);
		}

		$this->updateTicketAction([
			                          'ticket'   => $ticket,
			                          'comments' => "Cake, normal ticket created by customer"
		                          ]);

		$ticketId = $ticket->id;
		$enquiryId = config('evibe.ticket.enq.pre.cake') . $ticketId;
		$ticket->enquiry_id = $enquiryId;
		$ticket->save();

		$emailContent = array_merge($inputData, $this->getEmailContentData($ticket, [
			"name" => $cake->title,
			"code" => $cake->code
		]));

		$emailContent["ticketId"] = $ticketId;

		$this->dispatchMailAlerts($emailContent);

		return response()->json($this->getTicketSuccessResponse($ticket));
	}

	public function postVenueTicket($occasionId, $hallId)
	{
		$hall = VenueHall::find($hallId);
		if (!$hall)
		{
			$this->logError("Failed to find venue hall with id $hallId");

			return response()->json([
				                        'success' => false,
				                        'errors'  => "Venue hall not found"
			                        ]);
		}

		$venue = $hall->venue;
		if (!$venue)
		{
			$this->logError("Failed to find venue which has hall with id $hallId");

			return response()->json([
				                        'success' => false,
				                        'errors'  => "Venue not found"
			                        ]);
		}

		$isValidInput = $this->validateUserInput(request()->all());
		if ($isValidInput !== true)
		{
			return response()->json($isValidInput);
		}

		$inputData = [
			"city_id"           => getCityId(),
			'type_ticket_id'    => config('evibe.ticket.type.venue'),
			'type_id'           => $hallId,
			'event_id'          => $occasionId,
			'name'              => request('name'),
			'phone'             => request('phone'),
			'email'             => request('email'),
			'event_date'        => strtotime(str_replace('-', '/', request('partyDate'))),
			"enquiry_source_id" => config('evibe.ticket.enquiry_source.product_enquiry'),
			"lead_status_id"    => config('evibe.ticket.type_lead_status.hot')
		];

		if (getCityId() == -1)
		{
			$this->setCityFromPreviousUrl();
			$inputData["city_id"] = getCityId();
		}

		if ($this->checkForSpam(request('phone'), request('email')))
		{
			return response()->json(['success' => true]);
		}

		// save ticket
		$ticket = Ticket::create($inputData);
		if (!$ticket)
		{
			$this->logError("Failed to create ticket, email: " . request("email") . ", phone: " . request("phone"));

			return response()->json([
				                        'success' => false,
				                        'errors'  => 'An error occurred while submitting your request. Please try again later.'
			                        ]);
		}

		$this->updateTicketAction([
			                          'ticket'   => $ticket,
			                          'comments' => "Venues, normal ticket created by customer"
		                          ]);

		$ticketId = $ticket->id;
		$enquiryId = config('evibe.ticket.enq.pre.venue') . $ticketId;
		$ticket->enquiry_id = $enquiryId;
		$ticket->save();

		$emailContent = array_merge($inputData, $this->getEmailContentData($ticket, [
			"name" => $venue->name,
			"code" => $hall->code
		]));

		$emailContent["ticketId"] = $ticketId;

		$this->dispatchMailAlerts($emailContent);

		return response()->json($this->getTicketSuccessResponse($ticket));
	}

	public function postHeaderTicket($cityUrl)
	{
		$isValidInput = $this->validateUserInput(request()->all());
		if ($isValidInput !== true)
		{
			return response()->json($isValidInput);
		}

		$inputData = [
			"city_id"           => ($cityUrl != "no-city") ? getCityId() : null,
			'type_ticket_id'    => config('evibe.ticket.type.header'),
			'name'              => request('name'),
			'phone'             => request('phone'),
			'email'             => request('email'),
			'event_date'        => strtotime(str_replace('-', '/', request('partyDate'))),
			'comments'          => request('comments'),
			"enquiry_source_id" => config('evibe.ticket.enquiry_source.header_enquiry')
		];

		if (getCityId() == -1)
		{
			$this->setCityFromPreviousUrl();
			$inputData["city_id"] = getCityId();
		}

		if ($this->getOccasionId())
		{
			$inputData["event_id"] = $this->getOccasionId();
		}

		if ($this->checkForSpam(request('phone'), request('email')))
		{
			return response()->json(['success' => true]);
		}

		// save ticket
		$ticket = Ticket::create($inputData);
		if (!$ticket)
		{
			$this->logError("Failed to create ticket, email: " . request("email") . ", phone: " . request("phone"));

			return response()->json([
				                        'success' => false,
				                        'errors'  => 'An error occurred while submitting your request. Please try again later.'
			                        ]);
		}

		$this->updateTicketAction([
			                          'ticket'   => $ticket,
			                          'comments' => "Header Bar, normal ticket created by customer"
		                          ]);

		$ticketId = $ticket->id;
		$enquiryId = config('evibe.ticket.enq.pre.header') . $ticketId;
		$ticket->enquiry_id = $enquiryId;
		$ticket->save();

		$emailContent = array_merge($inputData, $this->getEmailContentData($ticket));

		$emailContent["ticketId"] = $ticketId;

		$this->dispatchMailAlerts($emailContent);

		return response()->json($this->getTicketSuccessResponse($ticket));
	}

	public function postHomeTicket($cityUrl)
	{
		$isValidInput = $this->validateUserInput(request()->all());
		$passiveCity = request('passiveCity');
		if ($isValidInput !== true)
		{
			return response()->json($isValidInput);
		}
		else
		{
			$bookingLikelinessId = request("planningStatus");

			if (isset($passiveCity) && ($passiveCity != "NA"))
			{
				if ($this->checkForSpam(request('phone'), request('email')))
				{
					return response()->json(['success' => true]);
				}
				$inputData = [
					'name'                    => request('name'),
					'phone'                   => request('phone'),
					'calling_code'            => request('callingCode'),
					'email'                   => request('email'),
					'comments'                => request('comments'),
					"customer_preferred_slot" => request("timeSlot"),
					"source_id"               => request('typeSource') ?: null,
					"city"                    => $passiveCity
				];

				// save request
				$requestSaved = RequestCity::create($inputData);
				if ($requestSaved)
				{
					return response()->json([
						                        'success' => true,
					                        ]);
				}
				else
				{
					$this->logError("Failed to create ticket, email: " . request("email") . ", phone: " . request("phone"));

					return response()->json([
						                        'success' => false,
						                        'errors'  => 'An error occurred while submitting your request. Please try again later.'
					                        ]);
				}
			}
			else
			{
				$inputData = [
					"city_id"                 => getCityId(),
					'name'                    => request('name'),
					'phone'                   => request('phone'),
					'calling_code'            => request('callingCode'),
					'email'                   => request('email'),
					'event_date'              => strtotime(str_replace('-', '/', request('partyDate'))),
					'type_ticket_id'          => request()->has("ticketType") ? config('evibe.ticket.type.no_results') : config('evibe.ticket.type.home_page'),
					'comments'                => request('comments'),
					"booking_likeliness_id"   => $bookingLikelinessId,
					"customer_preferred_slot" => request("timeSlot"),
					"enquiry_source_id"       => config('evibe.ticket.enquiry_source.home_enquiry'),
					"expected_closure_date"   => $this->getClosureDate(Carbon::now(), $bookingLikelinessId),
					"source_id"               => request('typeSource') ?: null
				];
				if (getCityId() == -1)
				{
					$this->setCityFromPreviousUrl();
					$inputData["city_id"] = getCityId();
				}

				if ($this->getOccasionId())
				{
					$inputData["event_id"] = $this->getOccasionId();
				}

				if ($this->checkForSpam(request('phone'), request('email')))
				{
					return response()->json(['success' => true]);
				}

				// save ticket
				$ticket = Ticket::create($inputData);
				if (!$ticket)
				{
					$this->logError("Failed to create ticket, email: " . request("email") . ", phone: " . request("phone"));

					return response()->json([
						                        'success' => false,
						                        'errors'  => 'An error occurred while submitting your request. Please try again later.'
					                        ]);
				}

				$this->updateTicketAction([
					                          'ticket'   => $ticket,
					                          'comments' => "Home page, normal ticket created by customer"
				                          ]);

				if (!is_null(request('isPlayAreaBooking')))
				{
					$this->updateTicketAction([
						                          'ticket'   => $ticket,
						                          'comments' => "Enquiry for kids play areas. Product Link <a href='" . request('isPlayAreaBooking') . "'>" . request('isPlayAreaBooking') . "</a>"
					                          ]);
				}

				$updatedSourceId = config('evibe.ticket.enquiry_source.home_enquiry');
				if (request('isVDayLandingPage') == 1)
				{
					$updatedSourceId = config("evibe.ticket.enquiry_source.v-day-landingPage");
					$this->updateTicketAction([
						                          'ticket'   => $ticket,
						                          'comments' => "This Ticket is created for valentine's day campaign page. "
					                          ]);
				}

				if (request('isOccasionHomePage'))
				{
					$updatedSourceId = config("evibe.ticket.enquiry_source.inspiration_hub_enquiry");
					$this->updateTicketAction([
						                          'ticket'   => $ticket,
						                          'comments' => "[Occasion Home page enquiry]. This is enquiry of an Occasion Homepage. Find the page at <a href='" . request('pageUrl') . "'>" . request('pageUrl') . "</a>"
					                          ]);
				}

				$ticketId = $ticket->id;
				$enquiryId = config('evibe.ticket.enq.pre.home_page') . $ticketId;
				if ($inputData == config('evibe.ticket.type.no_results'))
				{
					$enquiryId = config('evibe.ticket.enq.pre.no_results') . $ticketId;
				}
				$ticket->enquiry_source_id = $updatedSourceId;
				$ticket->enquiry_id = $enquiryId;
				$ticket->save();

				if (request('isOccasionHomePage') == 1)
				{
					$emailContent = array_merge($inputData, $this->getEmailContentData($ticket));
					$emailContent["ticketId"] = $ticketId;
					$this->dispatchMailAlerts($emailContent, true);
				}
				else
				{
					$inspirationProductUrl = request('inspirationProductUrl');

					$emailContent = array_merge($inputData, $this->getEmailContentData($ticket, $inspirationProductUrl));
					$emailContent["ticketId"] = $ticketId;
					$typeTicketSource = TypeTicketSource::find($ticket->source_id);
					if ($typeTicketSource)
					{
						$emailContent['source'] = $typeTicketSource->name;
					}

					// Dont send mail to inspiration Hub enquiries
					if (request('inspirationProductUrl') == "NA" || is_null(request('inspirationProductUrl')))
					{
						$this->dispatchMailAlerts($emailContent);
					}
				}

				return response()->json($this->getTicketSuccessResponse($ticket));
			}
		}
	}

	// update additional ticket details
	public function updateTicket($ticketId)
	{
		// validate rules
		$rules = [
			"occasion"    => "required|integer|min:1",
			"guestsCount" => "sometimes|required|min:1",
			"source"      => "required|integer|min:1"
		];

		$messages = [
			"occasion.required"    => "Please select a valid occasion",
			"occasion.min"         => "Please select a valid occasion",
			"occasion.integer"     => "Please select a valid occasion",
			"source.required"      => "Please select a valid source",
			"source.min"           => "Please select a valid source",
			"source.integer"       => "Please select a valid source",
			"guestsCount.required" => "Please enter your guests count",
			"guestsCount.min"      => "Please enter a valid guests count",
		];

		$validator = Validator::make(request()->all(), $rules, $messages);
		if ($validator->fails())
		{
			return response()->json([
				                        "success" => false,
				                        "error"   => $validator->messages()->first()
			                        ]);
		}

		$ticket = Ticket::find($ticketId);
		if (!$ticket)
		{
			$this->logError("TicketsController@updateTicket: failed to find ticket with id $ticketId");

			return response()->json([
				                        "success" => false,
				                        "error"   => "Could not find ticket"
			                        ]);
		}

		$comments = $ticket->comments ? $ticket->comments . ". " : "";
		if (request("moreServices"))
		{
			$comments .= "More Services: " . request("moreServices") . ". ";
		}
		$comments .= "Requirements: " . request("requirements", "--");

		$inputData = [
			"event_id"  => request("occasion"),
			"source_id" => request("source"),
			"comments"  => $comments
		];

		// dynamic fields
		if (request("guestsCount"))
		{
			$inputData["guests_count"] = request("guestsCount");
		}

		if ($ticket->update($inputData))
		{
			return response()->json(['success' => true]);
		}

		return response()->json([
			                        'success' => false,
			                        'errors'  => 'An error occurred while submitting your request. Please try again later.'
		                        ]);
	}

	// @see: public as used in PartyBagController
	public function dispatchPartyBagMailAlerts($data)
	{
		//$this->dispatch(new MailTicketAlertToTeamJob($data));
		$this->sendWebPushNotification($data);

		if (array_key_exists('email', $data) && $data['email'])
		{
			$this->dispatch(new MailTicketSuccessToPartyBagCustomerJob($data));
		}
	}

	private function dispatchMailAlerts($data, $isCouponMail = false)
	{
		//$this->dispatch(new MailTicketAlertToTeamJob($data));
		$this->sendWebPushNotification($data);

		if (array_key_exists('email', $data) && $data['email'])
		{
			$this->dispatch(new MailTicketSuccessToCustomerJob($data, $isCouponMail));
		}
	}

	public function dispatchPriceCheckMailAlerts($data)
	{
		$this->dispatch(new MailPriceCheckAlertToTeamJob($data));
		$this->sendWebPushNotification($data);

		// @todo: after using data effectively, because we must no send all price check emails
		//if (array_key_exists('email', $data) && $data['email'])
		//{
		//	$this->dispatch(new MailTicketSuccessToCustomerJob($data));
		//}
	}

	/**
	 * @author Anji <anji@evibe.in>
	 * @since  26 Oct 2016
	 *
	 * Send web push notifications to EvibeDash
	 */
	private function sendWebPushNotification($data)
	{
		if (config("app.env") != "production")
		{
			return false;
		}

		$ticketId = isset($data["plainTicketId"]) ? $data["plainTicketId"] : "";
		$ticketRaisedFor = isset($data["vendor_name"]) ? $data["vendor_name"] : "Website Ticket";
		$customerName = isset($data["name"]) ? $data["name"] : "Customer";
		$customerPhone = isset($data["phone"]) ? $data["phone"] : "--";
		$time = Carbon::now()->format("h:i A, d M");
		$partyDate = isset($data["event_date"]) ? Carbon::createFromTimestamp($data["event_date"])->format("d M (D)") : "--";
		$ticketDetailsUrl = config("evibe.dash_host") . "/tickets/$ticketId";

		$notificationTitle = "[$time] New website ticket alert for $partyDate";
		$notificationMessage = "For: $ticketRaisedFor. Customer Info: $customerName ($customerPhone)";

		$includedSegments = array_merge(config("onesignal.dash.segments.crm"), config("onesignal.dash.segments.admins"));

		// post request
		$headers = [
			"Content-Type"  => "application/json",
			"Authorization" => "Basic " . config("onesignal.dash.apiKey")
		];

		$payload = [
			"app_id"            => config("onesignal.dash.appId"),
			"included_segments" => $includedSegments,
			"headings"          => [
				"en" => $notificationTitle
			],
			"contents"          => [
				"en" => $notificationMessage
			],
			"url"               => $ticketDetailsUrl
		];

		/**
		 * @author: Anji
		 * @see   : removed on 28 March 2018.
		 *     There is some issue reported by OneSignal
		 */
		/*
		"web_buttons"       => json_encode([
			                                   [
				                                   "id"   => "ticket_details",
				                                   "text" => "Show Ticket Details",
				                                   "url"  => $ticketDetailsUrl
			                                   ]
		                                   ])
		*/

		// @todo: change the API call to Queue Job
		$oneSignalApi = config("onesignal.path") . "/notifications";
		$client = new Client();

		$response = $client->post($oneSignalApi, [
			"headers" => $headers,
			"json"    => $payload
		]);
	}

	private function getOccasionName($occasionId)
	{
		$name = "Default";
		$typeOccasion = TypeEvent::find($occasionId);
		if ($typeOccasion)
		{
			$name = $typeOccasion->name;
		}

		return $name;
	}

	private function validateUserInput($input, $rules = [], $messages = [])
	{
		if (!is_array($rules) || !count($rules))
		{
			$rules = [
				"name"     => "required",
				"phone"    => "required|phone",
				"email"    => "required|email",
				"location" => "sometimes|required|min:3",
				"budget"   => "sometimes|numeric|min:1500",
				"timeSlot" => "sometimes|required|not_in:-1",
				"accepts"  => "sometimes|accepted"
			];
		}

		if (!is_array($messages) || !count($messages))
		{
			$messages = [
				"name.required"      => "Please enter your full name",
				"phone.required"     => "Please enter your phone number",
				"phone.phone"        => "Phone number is invalid (ex: 9640204000)",
				"email.required"     => "Please enter your email address",
				"email.email"        => "Email address is invalid",
				"partyDate.required" => "Please select your party date",
				"partyDate.date"     => "Please select a valid party date",
				"partyDate.after"    => "Please select a valid party date",
				"location.required"  => "Please enter your party location",
				"location.min"       => "Please enter a valid party location",
				"budget.number"      => "Please enter a valid party budget",
				"budget.min"         => "Sorry, we do not have any vendor who can serve at such low budget",
				"accepts.accepted"   => "Please read and accept the terms of service",
				"timeSlot.required"  => "Please select your available time slot",
				"timeSlot.not_in"    => "Please select your available time slot",
			];
		}

		$validator = Validator::make($input, $rules, $messages);
		if ($validator->fails())
		{
			return [
				'success' => false,
				'errors'  => $validator->messages()->first()
			];
		}

		return true;
	}

	// @see: used in CustomTicketController
	public function getTicketSuccessResponse($ticket, $params = [])
	{
		return [
			"success"    => true,
			"ticketId"   => $ticket->id,
			"redirectTo" => $this->getThankYouPageUrl($ticket->id, $ticket->created_at, url()->previous(), $params)
		];
	}

	private function getThankYouPageUrl($ticketId, $createdAt, $from, $params = [])
	{
		$url = route("ticket.thank-you", [$ticketId]);
		$token = Hash::make($ticketId . $createdAt);
		$url .= "?t=$token&from=" . urlencode($from);

		if (count($params) > 0)
		{
			foreach ($params as $key => $param)
			{
				$url .= "&" . $key . "=" . $param;
			}
		}

		return $url;
	}

	//@see: Also used in PartyBagController
	public function getEmailContentData($ticket, $productData = [])
	{
		$emailData = [
			"enquiryId"   => $ticket->enquiry_id,
			"occasion"    => $this->getOccasionName($ticket->event_id),
			"productCode" => isset($productData["code"]) ? $productData["code"] : " -- ",
			"productName" => isset($productData["name"]) ? $productData["name"] : " -- ",
		];

		if ($ticket->city_id && $ticket->city)
		{
			$emailData["city"] = $ticket->city->name;
		}

		if ($ticket->type_ticket_id && $ticket->raisedForType)
		{
			$emailData["productType"] = $ticket->raisedForType->name;
		}

		return $emailData;
	}

	private function checkForSpam($phone, $email)
	{
		// @see: some Chineese SPAM
		if ($phone == "0353022981" || strpos($email, "qq.com") !== false)
		{
			return true;
		}

		return false;
	}
}