<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use App\Models\Cake\Cake;
use App\Models\Decor\Decor;
use App\Models\Package\Package;
use App\Models\Trend\Trend;
use App\Models\Types\TypeServices;
use App\Models\Types\TypeTicket;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class ShowLiveUrlController extends BaseController
{
	public function redirectToUrl($cityUrl, $optionTypeId, $optionId)
	{
		$productData = null;
		$ref = request("ref") ? "?ref=" . request("ref") : "";

		if ($optionTypeId == config('evibe.ticket.type.cake'))
		{
			$cakeDetails = Cake::find($optionId);
			if ($cakeDetails)
			{
				$productData = $cakeDetails->getSearchableCardInfo($cityUrl, '', ["map_type_id" => $optionTypeId]);
			}
		}
		elseif ($optionTypeId == config('evibe.ticket.type.decor'))
		{
			$decorDetails = Decor::find($optionId);
			if ($decorDetails)
			{
				$productData = $decorDetails->getSearchableCardInfo($cityUrl, '', ["map_type_id" => $optionTypeId]);
			}
		}
		elseif ($optionTypeId == config('evibe.ticket.type.service'))
		{
			$serviceDetails = TypeServices::find($optionId);
			if ($serviceDetails)
			{
				$productData = $serviceDetails->getSearchableCardInfo($cityUrl, '', ["map_type_id" => $optionTypeId]);
			}
			else
			{
				$productData = null;
			}
		}
		elseif ($optionTypeId == config('evibe.ticket.type.package'))
		{
			$packageDetails = Package::find($optionId);
			$packageType = TypeTicket::find($packageDetails->type_ticket_id);
			$typeName = "";

			if (!$packageType)
			{
				return redirect()->route("missing");
			}

			switch ($packageType->id)
			{
				case config('evibe.ticket.type.package'):
					$typeName = "packages";
					break;
				case config('evibe.ticket.type.surprises'):
					$typeName = "surprises";
					break;
				case config('evibe.ticket.type.venue-deals'):
					$typeName = "venue-deals";
					break;
				case config('evibe.ticket.type.villas'):
					$typeName = "villas";
					break;
				case config('evibe.ticket.type.lounges'):
					$typeName = "lounges";
					break;
				case config('evibe.ticket.type.food'):
					$typeName = "food";
					break;
				case config('evibe.ticket.type.priests'):
					$typeName = "priest";
					break;
				case config('evibe.ticket.type.tents'):
					$typeName = "tent";
					break;
				case config('evibe.ticket.type.resorts'):
					$typeName = "resorts";
					break;
				default:
					$typeName = "packages";
					break;
			}

			if ($packageDetails)
			{
				$productData = $packageDetails->getSearchableCardInfo($cityUrl, '', ["map_type_id" => $optionTypeId, "name" => $typeName]);
			}
		}
		elseif ($optionTypeId == config('evibe.ticket.type.trend'))
		{
			$trendDetails = Trend::find($optionId);
			if ($trendDetails)
			{
				$productData = $trendDetails->getSearchableCardInfo($cityUrl, '', ["map_type_id" => $optionTypeId]);
			}
		}

		if (is_array($productData) && isset($productData['fullUrl']) && $productData['fullUrl'])
		{
			return redirect($productData['fullUrl'] . $ref);
		}
		else
		{
			return redirect()->route("missing");
		}
	}

	public function deleteCachedHTML($cityUrl, $optionTypeId, $optionId)
	{
		$token = request("accessToken");

		if ($token && !is_null($token) && Hash::check('deleteCacHedHTML' . Carbon::today()->startOfDay()->timestamp, $token))
		{
			if ($optionTypeId == config('evibe.ticket.type.package'))
			{
				$packageDetails = Package::withTrashed()->find($optionId);
				$packageType = TypeTicket::find($packageDetails->type_ticket_id);
				$typeName = "";

				if ($packageDetails && $packageType)
				{
					switch ($packageType->id)
					{
						case config('evibe.ticket.type.package'):
							$typeName = "packages";
							break;
						case config('evibe.ticket.type.surprises'):
							$typeName = "surprises";
							break;
						default:
							$typeName = "packages";
							break;
					}

					$productData = $packageDetails->getSearchableCardInfo($cityUrl, '', ["map_type_id" => $optionTypeId, "name" => $typeName]);

					if (is_array($productData) && isset($productData['fullUrl']) && $productData['fullUrl'])
					{
						$url = str_replace(config("evibe.host"), "", $productData['fullUrl']);
						$segments = explode('/', ltrim($url, '/'));
						$file = implode('_', $segments);

						array_pop($segments);
						$path = '/page-cache/' . implode('/', $segments);

						if (file_exists(public_path() . $path . "/" . $file . '_m.html'))
						{
							File::delete(public_path() . $path . "/" . $file . '_m.html');
						}
						if (file_exists(public_path() . $path . "/" . $file . '_d.html'))
						{
							File::delete(public_path() . $path . "/" . $file . '_d.html');
						}

						return response()->json([
							                        "success" => true
						                        ]);
					}
				}
			}
		}

		return response()->json([
			                        "success" => false
		                        ]);
	}
}