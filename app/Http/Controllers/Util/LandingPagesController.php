<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use App\Http\Controllers\Occasions\Surprises\SurprisesPackageController;
use App\Jobs\Emails\Tickets\MailGoogleAdLandingEnquiryToTeamJob;
use App\Jobs\Emails\Landing\MailSurpriseSignupSuccessToCustomer;
use App\Jobs\Emails\Tickets\MailGoogleAdSurpriseLandingEnquiryToTeamJob;
use App\Jobs\SMS\Landing\SMSPiabLandingUser;
use App\Jobs\SMS\Landing\SMSSurpriseLandingUser;
use App\Models\Coupon\Coupon;
use App\Models\Landing\LandingOffer;
use App\Models\Landing\LandingSignupUser;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketUpdate;
use App\Models\Types\TypeEvent;
use App\Models\Util\City;
use App\Models\Util\User;
use Carbon\Carbon;
use Evibe\Passers\Util\SiteLogData;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Hash;

class LandingPagesController extends BaseController
{
	public function showLandingPage($keyWord)
	{
		$galleryHost = config("evibe.gallery.host");

		if (config("landing.$keyWord"))
		{
			$landingPageData = [
				"cityId"      => config("landing.$keyWord.cityId"),
				'eventId'     => config("landing.$keyWord.event-id"),
				"heroImage"   => config("landing.$keyWord.heroImage"),
				'redirectUrl' => config("landing.$keyWord.redirect-url"),
				'pageTitle'   => config("landing.$keyWord.page-title"),
				'headerTag'   => config("landing.$keyWord.header-tag"),
				'headline-1'  => config("landing.$keyWord.headline-1"),
				'subHeading'  => config("landing.$keyWord.sub-heading"),
				'sneakPeaks'  => config("landing.$keyWord.sneak-peaks"),
				'formTitle'   => config("landing.$keyWord.form-title"),
				'ctaButton'   => config("landing.$keyWord.cta-button"),
				'stats'       => config("landing.$keyWord.stats"),
				"media"       => config("landing.media"),
				'meta'        => [
					'description'    => config("landing.$keyWord.meta.description"),
					'key-words'      => config("landing.$keyWord.meta.key-words"),
					'og-title'       => config("landing.$keyWord.meta.og-title"),
					'og-description' => config("landing.$keyWord.meta.op-description"),
					'og-url'         => config("landing.$keyWord.meta.og-url"),
					'canonical'      => config("landing.$keyWord.meta.canonical")
				],
				'benefits'    => [
					'1' => config("landing.$keyWord.benefits.1"),
					'2' => config("landing.$keyWord.benefits.2"),
					'3' => config("landing.$keyWord.benefits.3"),
					'4' => config("landing.$keyWord.benefits.4")
				],
				'imageLinks'  => [
					'collage'    => $galleryHost . config("landing.$keyWord.image-links.collage"),
					'sneakPeaks' => [
						'1' => $galleryHost . config("landing.$keyWord.image-links.sneak-peaks.1"),
						'2' => $galleryHost . config("landing.$keyWord.image-links.sneak-peaks.2"),
						'3' => $galleryHost . config("landing.$keyWord.image-links.sneak-peaks.3"),
						'4' => $galleryHost . config("landing.$keyWord.image-links.sneak-peaks.4"),
					]
				]
			];

			$landingPageData['intermediateUrl'] = config('evibe.host') . '/ga/landing/intermediate?redirect=' . config("landing.$keyWord.redirect-url");

			return view('landing-pages.birthday-balloon-decoration', ['data' => $landingPageData]);
		}

		return redirect()->to(config('evibe.host'));

	}

	public function submitEnquiryForm()
	{
		$res['success'] = true;
		$rules = [
			'enquiryName'  => 'required|min:4',
			'enquiryPhone' => 'required|digits:10',
			'enquiryEmail' => 'email',
			'enquiryDate'  => 'date'
		];

		$messages = [
			'enquiryName.required'  => 'Your name is required',
			'enquiryName.min'       => 'Your name should be at least 4 characters',
			'enquiryPhone.required' => 'Kindly enter your mobile number',
			'enquiryPhone.digits'   => 'Kindly enter a valid mobile number (ex: 9640204000)',
			'enquiryEmail.email'    => 'Please enter a valid email (check for extra spaces at the start / end)'
		];

		$validation = validator(request()->input(), $rules, $messages);

		if ($validation->fails())
		{
			$res['success'] = false;
			$res['errorMsg'] = $validation->messages()->first();
		}
		else
		{
			try
			{
				$eventId = request('eventId');
				$cityId = request("cityId");
				$ticketData = [
					'name'              => request('enquiryName'),
					'email'             => request('enquiryEmail'),
					'phone'             => request('enquiryPhone'),
					'event_date'        => strtotime(request('enquiryDate')),
					'city_id'           => $cityId,
					'event_id'          => $eventId,
					'source_id'         => config('evibe.ticket.type_source.google-ad'),
					'comments'          => 'Ticket created from google ad words landing page.',
					'enquiry_source_id' => config('evibe.ticket.enquiry_source.ga')
				];

				$ticket = Ticket::create($ticketData);

				$this->updateTicketAction([
					                          'ticket'   => $ticket,
					                          "comments" => "Landing page ticket created by customer"
				                          ]);

				$ticketData['cityName'] = ($city = City::find($cityId)) ? $city->name : "Default City";
				$ticketData['eventName'] = ($typeEvent = TypeEvent::find($eventId)) ? $typeEvent->name : "Default Event";
				$ticketData['event_date'] = request('enquiryDate');
				$ticketData['ticketId'] = $ticket->id;

				$this->dispatch(new MailGoogleAdLandingEnquiryToTeamJob($ticketData));

				return response()->json(['success' => true]);

			} catch (\Exception $e)
			{
				$siteLogData = new SiteLogData();
				$siteLogData->setException($e->getMessage());
				$siteLogData->setTrace($e->getTraceAsString());
				$siteLogData->setErrorCode($e->getCode());
				$this->logSiteError($siteLogData);

				return response()->json(['error' => false]);
			}
		}

		return response()->json($res);
	}

	public function showIntermediatePage()
	{
		$data['redirectUrl'] = request()->input('redirect');

		return view('landing-pages.intermediate', ['data' => $data]);
	}

	public function submitSurprisesEnquiryForm()
	{
		$res['success'] = true;

		$rules = [
			'surpriseLandingSignUpPhone' => 'required|phone',
			'partyDate'                  => 'sometimes|required|date|after:now',
		];

		$messages = [
			"surpriseLandingSignUpPhone.required" => "Please enter your phone number",
			"surpriseLandingSignUpPhone.phone"    => "Phone number is invalid (ex: 9640204000)",
			"partyDate.required"                  => "Please select your special date",
			"partyDate.date"                      => "Please select a valid special date",
			"partyDate.after"                     => "Please select a valid special date",
		];

		$validation = validator(request()->input(), $rules, $messages);

		if ($validation->fails())
		{
			$res['success'] = false;
			$res['errorMsg'] = $validation->messages()->first();
		}
		elseif ((request("surpriseLandingSignUpEmail") == "") && (request("surpriseLandingSignUpPhone") == ""))
		{
			$res['success'] = false;
			$res['errorMsg'] = "Please fill the details to continue";
		}
		else
		{
			try
			{
				$client = new Client();
				$user = User::find(config("evibe.default.access_token_handler"));

				$response = $client->request('POST', config("evibe.api.base_url") . "coupon/generate", [
					'headers' => [
						'access-token' => $this->getAccessTokenFromUser($user)
					],
					'json'    => [
						"couponPrefix"   => "LA",
						"stringLength"   => 8,
						"discountAmount" => "200",
						"maxDiscount"    => "200",
						"minOrder"       => "3000",
						"description"    => "Surprise landing page enquiry success.",
						"offerEndTime"   => Carbon::now()->addMonths(6)->toDateTimeString()
					]
				]);

				$response = $response->getBody();
				$response = \GuzzleHttp\json_decode($response, true);

				if ($response['success'] && !is_null($response["couponCode"]))
				{
					$coupon = Coupon::where("coupon_code", $response["couponCode"])->first();

					if ($coupon)
					{
						LandingSignupUser::create([
							                          "email"          => request("surpriseLandingSignUpEmail"),
							                          "phone"          => request("surpriseLandingSignUpPhone"),
							                          "source_url"     => request("sourceUrl"),
							                          "coupon_code_id" => $coupon->id,
							                          "event_date"     => strtotime(request("partyDate"))
						                          ]);

						$enquiryText = config("evibe.ticket.enq.pre.google_ads");
						$source = "Google Ad";

						if (request("sourceId") == config("evibe.ticket.type_source.facebook-ad"))
						{
							$enquiryText = config("evibe.ticket.enq.pre.facebook_ads");
							$source = "FB Ad";
						}

						if (request("surpriseLandingSignUpPhone") != "")
						{
							$ticketData = [
								'area_id'           => null,
								'handler_id'        => null,
								'event_id'          => getEventIdFromSession(),
								'enquiry_source_id' => config("evibe.ticket.enquiry_source.ga"),
								'event_date'        => strtotime(request("partyDate")),
								'status_id'         => config('evibe.ticket.status.initiated'),
								'phone'             => request("surpriseLandingSignUpPhone"),
								'email'             => request("surpriseLandingSignUpEmail"),
								'created_at'        => date('Y-m-d H:i:s', time()),
								'updated_at'        => date('Y-m-d H:i:s', time())
							];

							$ticket = Ticket::create($ticketData);
							$ticket->enquiry_id = $enquiryText . $ticket->id;
							$ticket->save();

							TicketUpdate::create([
								                     'ticket_id'   => $ticket->id,
								                     'status_id'   => config('evibe.ticket.status.initiated'),
								                     'type_update' => config('evibe.ticket.type_update.auto'),
								                     'comments'    => "Ticket created from surprises landing page - " . $source,
								                     'status_at'   => time(),
								                     'created_at'  => date('Y-m-d H:i:s'),
								                     'updated_at'  => date('Y-m-d H:i:s')
							                     ]);

							$data["phone"] = request("surpriseLandingSignUpPhone");
							$data["validUpto"] = $coupon ? date('d-M-Y', strtotime($coupon->offer_end_time)) : "";
							$data["referralCode"] = $response["couponCode"];
							$data["sourceUrl"] = request("sourceUrl");
							$data["source"] = $source;
							$data["event_date"] = request("partyDate") != "" ? date('d M Y', strtotime(request("partyDate"))) : "";
							$data["eventId"] = getEventIdFromSession();

							$this->dispatch(new SMSSurpriseLandingUser($data));
							//$this->dispatch(new MailGoogleAdSurpriseLandingEnquiryToTeamJob($data));
						}

						if (request("surpriseLandingSignUpEmail") != "")
						{
							$this->dispatch(new MailSurpriseSignupSuccessToCustomer([
								                                                        "email" => request("surpriseLandingSignUpEmail")
							                                                        ]));
						}

						$res['success'] = true;
						$res['referralCode'] = $response["couponCode"];
					}
					else
					{
						$res['success'] = false;
						$res['errorMsg'] = "Unable to fetch you coupon code, Please try again.";
					}
				}
				else
				{
					$res['success'] = false;
					$res['errorMsg'] = "Unable to fetch you coupon code, Please try again.";
				}
			} catch (\Exception $e)
			{
				$siteLogData = new SiteLogData();
				$siteLogData->setException($e->getMessage());
				$siteLogData->setTrace($e->getTraceAsString());
				$siteLogData->setErrorCode($e->getCode());
				$this->logSiteError($siteLogData);

				$this->sendErrorReport($e);

				$res['success'] = false;
				$res['errorMsg'] = "Unable to submit you request, Please try again.";
			}
		}

		return response()->json($res);
	}

	public function showLandingAdSurprisePage($profileUrl)
	{
		$surpriseResultsPage = new SurprisesPackageController();

		view()->share('theatrePrices', config("landing.ads.prices"));

		// @todo: remove hardcoded city URL
		return $surpriseResultsPage->showPackageProfile("bangalore", $profileUrl);
	}

	public function getPriceForOptions()
	{
		$cityName = request("cityName");
		$theatreName = request("theatreName");

		$price = config("landing.ads.prices." . $cityName . "." . "$theatreName");

		return $price;
	}

	public function getOfferData()
	{
		$offerId = request("ofId");
		$offerToken = request("ofTn");
		$ref = request("ref");
		$isPiab = request("isPiab");
		$occasionId = getEventIdFromSession();

		if ($ref == "inorg")
		{
			$offerId = request()->is('*/piab*') ? config("evibe.offer.default.key_piab") : config("evibe.offer.default.key");
			$occasionId = getEventIdFromSession();
		}

		if ($isPiab)
		{
			$offerId = config("evibe.offer.default.key_piab");
			$occasionId = null;
		}

		$offer = LandingOffer::find($offerId);

		$data = ["offer" => $offer, "occasionId" => $occasionId, "isPiab" => $isPiab];

		if ($ref == "inorg" || ($offer
				&& Hash::check($offerId . str_replace(' ', '', $offer->created_at), $offerToken)
				&& (is_null($offer->offer_end_time) || (strtotime($offer->offer_end_time) > time()))))
		{
			return view("landing.offer-landing-page", ["data" => $data]);
		}

		if ($isPiab && $offer)
		{
			return view("landing.offer-landing-page", ["data" => $data]);
		}

		return "";
	}

	public function saveOfferData()
	{
		$res['success'] = true;

		$rules = [
			'offerLandingSignUpPhone' => 'required|phone',
			'partyDate'               => 'sometimes|required|date'
		];

		$messages = [
			"offerLandingSignUpPhone.required" => "Please enter your phone number",
			"offerLandingSignUpPhone.phone"    => "Phone number is invalid (ex: 9640204000)",
			"partyDate.required"               => "Please select your special date",
			"partyDate.date"                   => "Please select a valid special date (dd/mm/yyyy)",
		];

		$validation = validator(request()->input(), $rules, $messages);

		if ($validation->fails())
		{
			$res['success'] = false;
			$res['errorMsg'] = $validation->messages()->first();
		}
		else
		{
			try
			{
				$offerId = request("ofId");
				$offerToken = request("ofTn");
				$ref = request("ref");
				$isPIAB = request("isPiab");
				$coupon = "";

				if ($ref == "inorg")
				{
					$offerId = config("evibe.offer.default.key");
				}

				if ($isPIAB)
				{
					$offerId = config("evibe.offer.default.key_piab");
					$coupon = Coupon::find(config('evibe.offer.default.coupon_piab'));
				}

				// Hardcoded for evibe start offers, need to make it dynamic & should these details in the offer table
				if ($offerId == 4)
				{
					$coupon = Coupon::find(config('evibe.offer.default.evibe_star'));
				}

				// Hardcoded for evibe vday 2020
				if ($offerId == 5)
				{
					$coupon = Coupon::find(config('evibe.offer.default.vday2020'));
				}

				$offer = LandingOffer::find($offerId);

				if ((!$isPIAB) && ($ref != "inorg") && (!$offer || !Hash::check($offerId . str_replace(' ', '', $offer->created_at), $offerToken)))
				{
					return response()->json($res);
				}

				if (!$coupon)
				{
					$client = new Client();
					$user = User::find(config("evibe.default.access_token_handler"));

					$response = $client->request('POST', config("evibe.api.base_url") . "coupon/generate", [
						'headers' => [
							'access-token' => $this->getAccessTokenFromUser($user)
						],
						'json'    => [
							"couponPrefix"   => "OF",
							"stringLength"   => 9,
							"discountAmount" => $offer->offer_amount,
							"maxDiscount"    => $offer->offer_amount,
							"minOrder"       => $offer->offer_min_booking_amount > 0 ? $offer->offer_min_booking_amount : null,
							"description"    => $isPIAB == 1 ? "PIAB Ad offers" : "Ad Offers",
							"offerEndTime"   => $offer->offer_end_time ? $offer->offer_end_time : null
						]
					]);

					$response = $response->getBody();
					$response = \GuzzleHttp\json_decode($response, true);

					if ($response['success'] && !is_null($response["couponCode"]))
					{
						$coupon = Coupon::where("coupon_code", $response["couponCode"])->first();
					}
					else
					{
						$res['success'] = false;
						$res['errorMsg'] = "Unable to fetch you coupon code, Please try again.";
					}
				}

				if ($coupon)
				{
					LandingSignupUser::create([
						                          "phone"          => request("offerLandingSignUpPhone"),
						                          "source_url"     => request("sourceUrl"),
						                          "coupon_code_id" => $coupon->id,
						                          "offer_id"       => request("ofId"),
						                          "event_date"     => strtotime(request("partyDate"))
					                          ]);

					$sourceId = config("evibe.ticket.enquiry_source.ga");
					$enquiryText = config("evibe.ticket.enq.pre.google_ads");
					$source = "Google Ad";

					if ($isPIAB != 1)
					{
						$ticketData = [
							'area_id'           => null,
							'handler_id'        => null,
							'event_id'          => getEventIdFromSession(),
							'enquiry_source_id' => $sourceId,
							'city_id'           => getCityId() > 0 ? getCityId() : null,
							'event_date'        => strtotime(request("partyDate")),
							'status_id'         => config('evibe.ticket.status.initiated'),
							'phone'             => request("offerLandingSignUpPhone"),
							'created_at'        => date('Y-m-d H:i:s', time()),
							'updated_at'        => date('Y-m-d H:i:s', time())
						];

						$ticket = Ticket::create($ticketData);
						$ticket->enquiry_id = $enquiryText . $ticket->id;
						$ticket->save();

						TicketUpdate::create([
							                     'ticket_id'   => $ticket->id,
							                     'status_id'   => config('evibe.ticket.status.initiated'),
							                     'type_update' => config('evibe.ticket.type_update.auto'),
							                     'comments'    => "Ticket created from Google Ad landing page - " . $source,
							                     'status_at'   => time(),
							                     'created_at'  => date('Y-m-d H:i:s'),
							                     'updated_at'  => date('Y-m-d H:i:s')
						                     ]);
					}

					$data["phone"] = request("offerLandingSignUpPhone");
					$data["validUpto"] = $coupon ? date('d-M-Y', strtotime($coupon->offer_end_time)) : "";
					$data["referralCode"] = $coupon->coupon_code;
					$data["sourceUrl"] = request("sourceUrl");
					$data["source"] = $source;
					$data["event_date"] = request("partyDate") != "" ? date('d M Y', strtotime(request("partyDate"))) : "";
					$data["eventId"] = getEventIdFromSession();
					$data["discountAmount"] = $coupon->discount_amount && $coupon->discount_amount > 0 ? $coupon->discount_amount : (($offer && $offer->offer_amount > 0) ? $offer->offer_amount : "50");
					$data["minOrder"] = $coupon->min_order;
					$data["isPIAB"] = $isPIAB;
					$data["ticketId"] = $ticket ? $ticket->id : null;

					if ($isPIAB == 1)
					{
						$this->dispatch(new MailGoogleAdSurpriseLandingEnquiryToTeamJob($data));
						$this->dispatch(new SMSPiabLandingUser($data));
					}
					else
					{
						$this->dispatch(new SMSSurpriseLandingUser($data));
					}

					$res['success'] = true;
					$res['referralCode'] = $coupon->coupon_code;
				}
				else
				{
					$res['success'] = false;
					$res['errorMsg'] = "Unable to fetch you coupon code, Please try again.";
				}
			} catch (\Exception $e)
			{
				$siteLogData = new SiteLogData();
				$siteLogData->setException($e->getMessage());
				$siteLogData->setTrace($e->getTraceAsString());
				$siteLogData->setErrorCode($e->getCode());
				$this->logSiteError($siteLogData);

				$this->sendErrorReport($e);

				$res['success'] = false;
				$res['errorMsg'] = "Unable to submit you request, Please try again.";
			}
		}

		return response()->json($res);
	}
}