<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use App\Jobs\HandleLogCouponUsage;
use App\Models\Coupon\Coupon;
use App\Models\Coupon\CouponMappings;
use App\Models\Coupon\CouponUser;
use App\Models\Product\ProductBooking;
use App\Models\Ticket\Ticket;
use App\Models\Util\SiteErrorLog;
use App\Models\Util\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class CouponController extends BaseController
{
	public function validateCoupon()
	{
		$rules = [
			'name'       => 'required',
			'phone'      => 'required|digits:10',
			'email'      => 'required|email',
			'couponCode' => 'required'
		];

		$messages = [
			'name.required'       => 'Please enter your name, phone & email to apply coupon',
			'phone.required'      => 'Please enter your name, phone & email to apply coupon',
			'email.required'      => 'Please enter your name, phone & email to apply coupon',
			'phone.digits'        => 'Kindly enter a valid mobile number (ex: 9640204000)',
			'email.email'         => 'Please enter a valid email (check for extra spaces at the start / end)',
			'couponCode.required' => 'Please enter coupon code'
		];

		$validator = Validator::make(request()->all(), $rules, $messages);

		if ($validator->fails())
		{
			return response()->json([
				                        'success' => "no",
				                        'error'   => $validator->messages()->first()
			                        ]);
		}

		$ticketId = request("ticketId");
		$ticket = Ticket::find($ticketId);

		if (!$ticket)
		{
			return $this->saveCouponSiteLogErrorAndReturnError("Trying to access other ticket / Ticket not found",
			                                                   "Ticket not found with ticket Id - " . $ticketId . " :: May tried to hack it.");
		}

		$bookings = $ticket->bookings->sortByDesc('booking_amount');

		if ($bookings->count() <= 0)
		{
			return $this->saveCouponSiteLogErrorAndReturnError("No Booking found for that ticket / Deleted.",
			                                                   "Ticket booking not found for ticket with Id - " . $ticketId . " :: May tried to hack it.");
		}

		$totalBookingAmount = $bookings->sum('booking_amount');
		$totalAdvanceAmount = $bookings->sum('advance_amount');

		if (request("isVenueDeal") == 1)
		{
			$totalBookingAmount = $totalAdvanceAmount = $bookings->sum('token_amount');
		}

		if (!(Hash::check($ticket->id . $ticket->created_at, request('couponToken'))))
		{
			return $this->saveCouponSiteLogErrorAndReturnError("Token mismatch.",
			                                                   "Ticket data modified with ticket Id - " . $ticketId . " :: May tried to hack it.");
		}

		// Disabling this check, since booking amount is changing after adding add-ons
		//if ($totalAdvanceAmount != request("totalAdvance"))
		//{
		//	return $this->saveCouponSiteLogErrorAndReturnError("Trying to change advance amount from console.",
		//	                                                   "Ticket ID - " . $ticketId . " :: advance amount mismatch");
		//}

		$couponStatus = $this->checkStatusOfCoupon($ticket, $bookings, $totalBookingAmount);

		if ($couponStatus["success"] == "yes")
		{
			return response()->json($couponStatus);
		}

		return $this->saveCouponSiteLogErrorAndReturnError("Coupon status checking error", $couponStatus["error"]);
	}

	public function validateCouponForPIAB()
	{
		$rules = [
			'name'       => 'required',
			'phone'      => 'required|digits:10',
			'email'      => 'required|email',
			'couponCode' => 'required'
		];

		$messages = [
			'name.required'       => 'Please enter your name, phone & email to apply coupon',
			'phone.required'      => 'Please enter your name, phone & email to apply coupon',
			'email.required'      => 'Please enter your name, phone & email to apply coupon',
			'phone.digits'        => 'Kindly enter a valid mobile number (ex: 9640204000)',
			'email.email'         => 'Please enter a valid email (check for extra spaces at the start / end)',
			'couponCode.required' => 'Please enter coupon code'
		];

		$validator = Validator::make(request()->all(), $rules, $messages);

		if ($validator->fails())
		{
			return response()->json([
				                        'success' => "no",
				                        'error'   => $validator->messages()->first()
			                        ]);
		}

		$piabId = request("productId");
		$piab = ProductBooking::where("booking_order_id", $piabId)->first();

		if (!$piab)
		{
			return $this->saveCouponSiteLogErrorAndReturnError("Trying to access other product booking / Product booking not found",
			                                                   "Product booking not found with booking order id - " . $piabId . " :: May tried to hack it.");
		}

		$totalBookingAmount = $piab->booking_amount;
		$totalAdvanceAmount = $piab->booking_amount;

		if (!(Hash::check($piab->id . $piab->created_at . $totalBookingAmount, request('couponToken'))))
		{
			return $this->saveCouponSiteLogErrorAndReturnError("Token mismatch.",
			                                                   "Product booking data modified with booking order id - " . $piabId . " :: May tried to hack it.");
		}

		if ($totalAdvanceAmount != request("totalAdvance"))
		{
			return $this->saveCouponSiteLogErrorAndReturnError("Trying to change advance amount from console.",
			                                                   "Booking order id - " . $piabId . " :: advance amount mismatch");
		}

		$couponStatus = $this->checkStatusOfCouponForPIAB($piab, $totalBookingAmount);

		if ($couponStatus["success"] == "yes")
		{
			return response()->json($couponStatus);
		}

		return $this->saveCouponSiteLogErrorAndReturnError("Coupon status checking error", $couponStatus["error"]);
	}

	public function checkStatusOfCoupon($ticket, $bookings, $totalBookingAmount, $isInternal = false)
	{
		$currentTime = Carbon::now();
		$discountAmountsByBooking = [];

		// coupon expiry check
		$coupon = Coupon::where('coupon_code', request("couponCode"))
		                ->whereNull('usage_done_at')
		                ->where(function ($startTimeQuery) use ($currentTime) {
			                // check for offer start time
			                $startTimeQuery->whereNull('offer_start_time')
			                               ->orWhere(function ($stq2) use ($currentTime) {
				                               $stq2->whereNotNull('offer_start_time')
				                                    ->where('offer_start_time', '<=', $currentTime);
			                               });
		                })
		                ->where(function ($endTimeQuery) use ($currentTime) {
			                // check for offer end time
			                $endTimeQuery->whereNull('offer_end_time')
			                             ->orWhere(function ($etq2) use ($currentTime) {
				                             $etq2->whereNotNull('offer_end_time')
				                                  ->where('offer_end_time', '>=', $currentTime);
			                             });
		                })
		                ->where(function ($minOrderQuery) use ($totalBookingAmount) {
			                // check for min order
			                $minOrderQuery->whereNull('min_order')
			                              ->orWhere(function ($moq2) use ($totalBookingAmount) {
				                              $moq2->whereNotNull('min_order')
				                                   ->where('min_order', '<=', $totalBookingAmount);
			                              });
		                })
		                ->first();

		if ($coupon)
		{
			$ticketEventId = $this->getParentEventId($ticket->event_id);
			$ticketCityId = $ticket->city_id;
			$isBasicCouponMapValid = false;
			$isCouponUserValid = false;

			$couponMappings = CouponMappings::where('coupon_id', $coupon->id)
			                                ->get();

			$couponUser = CouponUser::where("coupon_id", $coupon->id)->first();

			if (!$couponUser)
			{
				$isCouponUserValid = true;
			}
			else
			{
				$userEmail = request("email");
				if ($ticket->is_auto_booked != 1)
				{
					$userEmail = $ticket->email;
				}

				$user = User::where("username", $userEmail)->first();
				if ($user && ($user->id == $couponUser->user_id))
				{
					$isCouponUserValid = true;
				}
			}

			if ($isCouponUserValid)
			{
				if ($couponMappings->count() == 0)
				{
					$isBasicCouponMapValid = true; // 0 mappings = valid for all
				}
				else
				{
					// validating city & event
					$couponMappings = $couponMappings->filter(function ($mapping) use ($ticketEventId, $ticketCityId) {
						$mappingCityId = $mapping->city_id;
						$mappingEventId = $mapping->occasion_id;

						if (
							(is_null($mappingCityId) || ($mappingCityId == $ticketCityId)) &&
							(is_null($mappingEventId) || ($mappingEventId == $ticketEventId))
						)
						{
							return true;
						}

						return false;
					});

					if ($couponMappings->count() > 0)
					{
						$isBasicCouponMapValid = true;
					}
				}
			}

			if ($isBasicCouponMapValid)
			{
				// null max discount implies give full % discount amount
				// and can be maximum total booking amount (100% discount)
				$maxDiscountAmount = is_null($coupon->max_discount_amount) ? $totalBookingAmount : $coupon->max_discount_amount;

				foreach ($bookings as $booking)
				{
					$productMapping = $booking->mapping;

					if (!$productMapping)
					{
						return response()->json([
							                        "success" => "no",
							                        "error"   => "Ticket mapping not found for ticket with booking Id - " . $booking->id . " :: May tried to hack it."
						                        ]);
					}

					// check against product mappings
					$isCPMValid = $this->isCouponProductMappingsValid($couponMappings, $productMapping->map_type_id, $productMapping->map_id);

					if ($isCPMValid)
					{
						if ($coupon->discount_amount)
						{
							$currentDiscountAmount = min($coupon->discount_amount, $maxDiscountAmount);

							// case 1: direct discount with min order
							// @see: min order was already checked
							$discountAmountsByBooking[$booking->id] = $currentDiscountAmount;

							// calculate balance max discount amount
							$maxDiscountAmount = $maxDiscountAmount - $currentDiscountAmount;
							$maxDiscountAmount = $maxDiscountAmount >= 0 ? $maxDiscountAmount : 0; // avoid -ve values
						}
						elseif ($coupon->discount_percent)
						{
							// case 2: discount % with max discount amount cap
							$currentBookingAmount = $booking->booking_amount;
							if (request("isVenueDeal") == 1)
							{
								$currentBookingAmount = $booking->token_amount; // For venue deals both booking & advance amount is stored under token amount
							}

							$currentDiscountAmount = (int)(($currentBookingAmount * $coupon->discount_percent) / 100);

							// ex: max: 100, curr: 50; max: 50, curr: 100, both should give 50
							$currentDiscountAmount = min($currentDiscountAmount, $maxDiscountAmount);

							$discountAmountsByBooking[$booking->id] = $currentDiscountAmount;

							// calculate balance max discount amount
							$maxDiscountAmount = $maxDiscountAmount - $currentDiscountAmount;
							$maxDiscountAmount = $maxDiscountAmount >= 0 ? $maxDiscountAmount : 0; // avoid -ve values
						}
						else
						{
							$discountAmountsByBooking[$booking->id] = 0; // default
						}
					}
				}
			}
		}

		// log data
		if (!$isInternal)
		{
			$this->logCouponUsage([
				                      "couponCode"     => request("couponCode"),
				                      "couponId"       => $coupon ? $coupon->id : null,
				                      "bookingAmount"  => $totalBookingAmount,
				                      "discountAmount" => array_sum($discountAmountsByBooking),
				                      "name"           => request("name"),
				                      "email"          => request("email"),
				                      "phone"          => request("phone"),
				                      "ticketId"       => $ticket->id,
				                      "userId"         => Auth::user() ? Auth::user()->id : '',
				                      "cityId"         => $ticket->city_id,
				                      "occasionId"     => $ticket->event_id
			                      ]);
		}

		return [
			"success"                => "yes",
			"couponExists"           => $coupon ? "yes" : "no",
			"couponId"               => $coupon ? $coupon->id : null,
			"discountAmount"         => array_sum($discountAmountsByBooking),
			"discountForEachBooking" => $discountAmountsByBooking
		];
	}

	public function checkStatusOfCouponForPIAB($piab, $totalBookingAmount, $isInternal = false)
	{
		$currentTime = Carbon::now();
		$discountAmountsByBooking = [];

		// coupon expiry check
		$coupon = Coupon::where('coupon_code', request("couponCode"))
		                ->whereNull('usage_done_at')
		                ->where(function ($startTimeQuery) use ($currentTime) {
			                // check for offer start time
			                $startTimeQuery->whereNull('offer_start_time')
			                               ->orWhere(function ($stq2) use ($currentTime) {
				                               $stq2->whereNotNull('offer_start_time')
				                                    ->where('offer_start_time', '<=', $currentTime);
			                               });
		                })
		                ->where(function ($endTimeQuery) use ($currentTime) {
			                // check for offer end time
			                $endTimeQuery->whereNull('offer_end_time')
			                             ->orWhere(function ($etq2) use ($currentTime) {
				                             $etq2->whereNotNull('offer_end_time')
				                                  ->where('offer_end_time', '>=', $currentTime);
			                             });
		                })
		                ->where(function ($minOrderQuery) use ($totalBookingAmount) {
			                // check for min order
			                $minOrderQuery->whereNull('min_order')
			                              ->orWhere(function ($moq2) use ($totalBookingAmount) {
				                              $moq2->whereNotNull('min_order')
				                                   ->where('min_order', '<=', $totalBookingAmount);
			                              });
		                })
		                ->first();

		if ($coupon)
		{
			//$ticketEventId = $this->getParentEventId($piab->event_id);
			//$ticketCityId = $ticket->city_id;
			$isBasicCouponMapValid = false;
			$isCouponUserValid = false;

			$couponMappings = CouponMappings::where('coupon_id', $coupon->id)
			                                ->get();

			$couponUser = CouponUser::where("coupon_id", $coupon->id)->first();

			if (!$couponUser)
			{
				$isCouponUserValid = true;
			}
			else
			{
				$userEmail = request("email");
				$user = User::where("username", $userEmail)->first();
				if ($user && ($user->id == $couponUser->user_id))
				{
					$isCouponUserValid = true;
				}
			}

			if ($isCouponUserValid)
			{
				if ($couponMappings->count() == 0)
				{
					$isBasicCouponMapValid = true; // 0 mappings = valid for all
				}
				else
				{
					// validating city & event
					//$couponMappings = $couponMappings->filter(function ($mapping) use ($ticketEventId) {
					//	$mappingCityId = $mapping->city_id;
					//	$mappingEventId = $mapping->occasion_id;
					//
					//	return false;
					//});

					$isBasicCouponMapValid = true;
				}
			}

			if ($isBasicCouponMapValid)
			{
				// null max discount implies give full % discount amount
				// and can be maximum total booking amount (100% discount)
				$maxDiscountAmount = is_null($coupon->max_discount_amount) ? $totalBookingAmount : $coupon->max_discount_amount;

				// check against product mappings
				$isCPMValid = $this->isCouponProductMappingsValid($couponMappings, config("evibe.ticket.type.product"), null);

				if ($isCPMValid)
				{
					if ($coupon->discount_amount)
					{
						$currentDiscountAmount = min($coupon->discount_amount, $maxDiscountAmount);

						// case 1: direct discount with min order
						// @see: min order was already checked
						$discountAmountsByBooking[$piab->id] = $currentDiscountAmount;

						// calculate balance max discount amount
						//$maxDiscountAmount = $maxDiscountAmount - $currentDiscountAmount;
						//$maxDiscountAmount = $maxDiscountAmount >= 0 ? $maxDiscountAmount : 0; // avoid -ve values
					}
					elseif ($coupon->discount_percent)
					{
						// case 2: discount % with max discount amount cap
						$currentBookingAmount = $piab->booking_amount;
						$currentDiscountAmount = (int)(($currentBookingAmount * $coupon->discount_percent) / 100);

						// ex: max: 100, curr: 50; max: 50, curr: 100, both should give 50
						$currentDiscountAmount = min($currentDiscountAmount, $maxDiscountAmount);

						$discountAmountsByBooking[$piab->id] = $currentDiscountAmount;

						// calculate balance max discount amount
						//$maxDiscountAmount = $maxDiscountAmount - $currentDiscountAmount;
						//$maxDiscountAmount = $maxDiscountAmount >= 0 ? $maxDiscountAmount : 0; // avoid -ve values
					}
					else
					{
						$discountAmountsByBooking[$piab->id] = 0; // default
					}
				}
			}
		}

		// log data
		if (!$isInternal)
		{
			$this->logCouponUsage([
				                      "couponCode"     => request("couponCode"),
				                      "couponId"       => $coupon ? $coupon->id : null,
				                      "bookingAmount"  => $totalBookingAmount,
				                      "discountAmount" => array_sum($discountAmountsByBooking),
				                      "name"           => request("name"),
				                      "email"          => request("email"),
				                      "phone"          => request("phone"),
				                      "ticketId"       => null,
				                      "userId"         => Auth::user() ? Auth::user()->id : '',
				                      "cityId"         => null,
				                      "occasionId"     => $piab->event_id
			                      ]);
		}

		return [
			"success"                => "yes",
			"couponExists"           => $coupon ? "yes" : "no",
			"couponId"               => $coupon ? $coupon->id : null,
			"discountAmount"         => array_sum($discountAmountsByBooking),
			"discountForEachBooking" => $discountAmountsByBooking
		];
	}

	private function isCouponProductMappingsValid($couponMappings, $productTypeId, $productId)
	{
		if ($couponMappings->count() == 0)
		{
			return true;
		}

		$productMapCheck = $couponMappings->filter(function ($couponMap) use ($productTypeId, $productId) {
			$couponProductId = $couponMap->product_id;
			$couponProductTypeId = $couponMap->product_type_id;

			if (
				(is_null($couponProductId) || ($couponProductId == $productId)) &&
				(is_null($couponProductTypeId) || ($couponProductTypeId == $productTypeId))
			)
			{
				return true;
			}

			return false;
		});

		if ($productMapCheck->count() > 0)
		{
			return true;
		}

		return false;
	}

	private function logCouponUsage($data)
	{
		//Update log table with all the values
		dispatch(new HandleLogCouponUsage([
			                                  "couponCode"     => $data['couponCode'] ?: null,
			                                  "couponId"       => $data['couponId'] ?: null,
			                                  "name"           => $data['name'] ?: null,
			                                  "phone"          => $data['phone'] ?: null,
			                                  "email"          => $data['email'] ?: null,
			                                  "bookingAmount"  => $data['bookingAmount'] ?: null,
			                                  "discountAmount" => $data['discountAmount'] ?: null,
			                                  "ticketId"       => $data['ticketId'] ?: null,
			                                  "userId"         => $data['userId'] ?: null,
			                                  "cityId"         => $data['cityId'] ?: null,
			                                  "occasionId"     => $data['occasionId'] ?: null
		                                  ]));
	}

	private function saveCouponSiteLogErrorAndReturnError($exception, $message)
	{
		SiteErrorLog::create([
			                     "project_id" => 1,
			                     "url"        => request()->url(),
			                     "exception"  => $exception,
			                     "code"       => "evb-coupon",
			                     "details"    => $message
		                     ]);

		return response()->json([
			                        'success' => "no",
			                        'error'   => "Some error occurred, Please try again by reloading the page."
		                        ]);
	}
}