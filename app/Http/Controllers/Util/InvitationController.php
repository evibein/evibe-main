<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use App\Http\Controllers\ECards\GifGenerator;
use App\Jobs\Emails\Util\MailInviteEnquiryToTeamJob;
use App\Models\Invite\InviteGIFFieldValue;
use App\Models\Invite\InviteTemplate;
use App\Models\Invite\InviteTemplateField;
use App\Models\Invite\TicketTemplateFieldValue;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketInvite;
use App\Models\Ticket\TicketRSVP;
use App\Models\Types\TypeRSVP;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class InvitationController extends BaseController
{
	// Screen2
	public function showInviteTemplateFields($inviteTemplateUrl)
	{
		try
		{
			//$inviteTemplateId = request('inviteTemplateId');
			$ticketInviteId = request('id');
			$editInviteTkn = request('tkn');
			$inviteTemplate = InviteTemplate::where('seo_url', $inviteTemplateUrl)->first();

			if (!$inviteTemplate)
			{
				return redirect(route("e-cards.invites"));
			}

			$inviteTemplateId = $inviteTemplate->id;
			$inviteTemplateFields = InviteTemplateField::where('invite_template_id', $inviteTemplateId)->get();

			if (!count($inviteTemplateFields))
			{
				return redirect(route("e-cards.invites"));
			}

			$ticketTemplateFieldValues = [];
			$ticketInvite = TicketInvite::find($ticketInviteId);

			if ($ticketInvite && !(Hash::check($ticketInviteId . $ticketInvite->created_at . 'iVEdit', $editInviteTkn)))
			{
				return redirect()->back();
			}

			if ($ticketInvite)
			{
				$ticketTemplateFieldValues = TicketTemplateFieldValue::where('ticket_invite_id', $ticketInvite->id)
				                                                     ->get();
			}
			else
			{
				// create new ticket invite
				$ticketInvite = $newTicketInvite = TicketInvite::create([
					                                                        'invite_template_id' => $inviteTemplateId,
					                                                        'created_at'         => Carbon::now(),
					                                                        'updated_at'         => Carbon::now()
				                                                        ]);
			}

			$data = [
				'inviteTemplateId'     => $inviteTemplateId,
				'inviteTemplate'       => $inviteTemplate,
				'inviteTemplateFields' => $inviteTemplateFields,
				'ticketInviteId'       => $ticketInvite->id,
				'fieldValues'          => $ticketTemplateFieldValues,
				'inviteStepIndex'      => 2 // @see: hardcoded
			];

			return view('invites.party-details', ['data' => $data]);

		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);

			return redirect(route("e-cards.invites"));
		}
	}

	// Screen2
	public function validateAndStoreValues()
	{
		try
		{
			$ticketInviteId = request('ticketInviteId');
			$ticketInvite = TicketInvite::find($ticketInviteId);
			$inviteTemplate = InviteTemplate::find($ticketInvite->invite_template_id);

			if (!$ticketInvite)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Some error occurred while storing your details. Kindly write to ' . config('evibe.contact.invitations.group')
				                        ]);
			}

			//if (in_array($inviteTemplate->id, ["39", "40", "41"]) && !Auth::user())
			//{
			//	return response()->json([
			//		                        'success' => false,
			//		                        'login'   => true,
			//		                        'error'   => "Please login to continue"
			//	                        ]);
			//}

			$inviteTemplateFields = InviteTemplateField::where('invite_template_id', $ticketInvite->invite_template_id)->get();
			if (!count($inviteTemplateFields))
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Some error occurred while storing your details. Kindly write to ' . config('evibe.contact.invitations.group')
				                        ]);
			}

			$rules = [];
			$messages = [];

			foreach ($inviteTemplateFields as $dataField)
			{
				$dataFieldRule = 'required';
				$messages[$dataField->name . '.required'] = "$dataField->identifier is required.";

				if ($dataField->type_field_id == config('evibe.input.text'))
				{
					if ($dataField->character_limit)
					{
						$dataFieldRule = $dataFieldRule . '|max:' . $dataField->character_limit;
						$messages[$dataField->name . '.max'] = "$dataField->identifier should not be more that $dataField->character_limit";
					}

					if ($dataField->name == "baby_name")
					{
						$dataFieldRule = $dataFieldRule . 'alpha';
					}
				}
				elseif ($dataField->type_field_id == config('evibe.input.number'))
				{
					$dataFieldRule = $dataFieldRule . '|numeric';
					$messages[$dataField->name . '.numeric'] = "$dataField->identifier should be a number";

					if ($dataField->character_limit)
					{
						$dataFieldRule = $dataFieldRule . '|max:' . $dataField->character_limit;
						$messages[$dataField->name . '.max'] = "$dataField->identifier should be be more that $dataField->character_limit";
					}
				}
				elseif ($dataField->type_field_id == config('evibe.input.date'))
				{
					// @see: there is no restriction to date and time as of now (it is only required)
				}

				$rules[$dataField->name] = $dataFieldRule;
			}

			$validation = Validator::make(request()->all(), $rules, $messages);

			if ($validation->fails())
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => $validation->messages()->first()
				                        ]);
			}

			foreach ($inviteTemplateFields as $dataField)
			{
				$existingFieldValue = TicketTemplateFieldValue::where('ticket_invite_id', $ticketInviteId)
				                                              ->where('invite_template_field_id', $dataField->id)
				                                              ->get()->first();

				if (request($dataField->name) && $existingFieldValue)
				{
					$newFieldValue = $existingFieldValue->replicate();
					$newFieldValue->value = request($dataField->name);
					$newFieldValue->save();
					$existingFieldValue->deleted_at = Carbon::now();
					$existingFieldValue->save();
				}
				elseif (request($dataField->name))
				{
					TicketTemplateFieldValue::create([
						                                 'ticket_invite_id'         => $ticketInviteId,
						                                 'invite_template_field_id' => $dataField->id,
						                                 'value'                    => request($dataField->name),
						                                 'created_at'               => Carbon::now(),
						                                 'updated_at'               => Carbon::now()
					                                 ]);
				}
				elseif ($existingFieldValue)
				{
					$existingFieldValue->value = null;
					$existingFieldValue->updated_at = Carbon::now();
					$existingFieldValue->save();
				}
			}

			if ($inviteTemplate->type_card_id == config("evibe.type-card.gif"))
			{
				$inviteGifValues = InviteGIFFieldValue::where("invite_template_id", $inviteTemplate->id)->first();
				$frames = [];

				for ($i = 1; $i <= $inviteGifValues->frame_count; $i++)
				{
					$name = $i > 9 ? "00" . $i . "." . $inviteGifValues->image_frame_file_format : "000" . $i . "." . $inviteGifValues->image_frame_file_format;

					$img = Image::make(config('evibe.gallery.root') . '/invibe/gif-templates/' . $inviteGifValues->image_frame_file_folder . '/' . $name);

					$fieldValue = TicketTemplateFieldValue::where('ticket_invite_id', $ticketInvite->id)
					                                      ->where('invite_template_field_id', $inviteTemplateFields->first()->id)
					                                      ->get()
					                                      ->first();

					$dataField = InviteTemplateField::where('invite_template_id', $inviteTemplate->id)->first();

					if ($fieldValue && $fieldValue->value)
					{
						if ($dataField->type_field_id == 7)
						{
							$fieldValue->value = date("d F Y, h:i A", Carbon::parse($fieldValue->value)->timestamp);
						}

						if (!is_null($dataField->pre_text))
						{
							$customerReplaces = [
								'#replaceText#' => $fieldValue->value
							];

							$fieldValue->value = str_replace(array_keys($customerReplaces), array_values($customerReplaces), $dataField->pre_text);
						}

						$img->text($fieldValue->value, $dataField->x_pos, $dataField->y_pos, function ($font) use ($dataField) {
							$font->file(public_path('/fonts/' . $dataField->font));
							$font->size($dataField->font_size);
							$font->color($dataField->font_color);
							$font->align($dataField->font_align);
							$font->valign($dataField->font_valign);
						});

						//$img->insert(config("evibe.gallery.root") . "/invibe/watermark-110.png", "bottom-right", 290, 121);

						$newImgPath = config('evibe.gallery.root') . '/invibe/gif-templates/giftemp/';
						if (!file_exists($newImgPath))
						{
							$oldumask = umask(0);
							mkdir($newImgPath, 0777, true);
							umask($oldumask);
						}
						$fullImgPath = $newImgPath . $ticketInviteId . $name;
						$img->save($fullImgPath);
					}

					$frames[] = [
						'image'       => config("evibe.gallery.root") . "/invibe/gif-templates/giftemp/" . $ticketInviteId . $name, // path
						'millisecond' => $inviteGifValues->frame_time // time
					];
				}

				$imgTitle = !is_null($ticketInvite->title) ? $ticketInvite->title : 'Evibe-' . str_replace(" ", "-", $inviteTemplate->name) . '-' . $ticketInvite->id . time();
				$imgUrl = $imgTitle . '.gif';
				$path = config("evibe.gallery.root") . "/invibe/temp/";

				$gc = new GifGenerator();
				$gc->create($frames, 0);
				$gc->store($path, $imgUrl);

				for ($i = 1; $i <= $inviteGifValues->frame_count; $i++)
				{
					$name = $i > 9 ? "00" . $i . "." . $inviteGifValues->image_frame_file_format : "000" . $i . "." . $inviteGifValues->image_frame_file_format;
					@unlink(config("evibe.gallery.root") . "/invibe/gif-templates/giftemp/" . $ticketInviteId . $name);
				}

				$ticketInvite->title = $imgTitle;
				$ticketInvite->url = $imgUrl;
				$ticketInvite->updated_at = Carbon::now();
				$ticketInvite->save();
			}

			return response()->json([
				                        'success'        => true,
				                        'ticketInviteId' => $ticketInviteId,
				                        'inviteHashId'   => Hash::make($ticketInviteId . $ticketInvite->created_at . 'iV')
			                        ]);

		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);

			return response()->json([
				                        'success' => false
			                        ]);
		}
	}

	// Screen3
	public function showInviteDesign($ticketInviteId)
	{
		try
		{
			$ticketInvite = TicketInvite::find($ticketInviteId);
			if (!$ticketInvite || ($ticketInvite && !Hash::check($ticketInviteId . $ticketInvite->created_at . 'iV', request("tkn"))))
			{
				return redirect()->back();
			}

			$inviteTemplateId = $ticketInvite->invite_template_id;
			$inviteTemplate = InviteTemplate::find($inviteTemplateId);
			if (!$inviteTemplate)
			{
				// todo: error message to mention it is unavailable
				return redirect()->back();
			}

			if ($inviteTemplate->type_card_id == config("evibe.type-card.gif"))
			{
				$imgTitle = $ticketInvite->title;
				$imgUrl = $ticketInvite->url;
				$fullImgUrl = config('evibe.gallery.host') . '/invibe/temp/' . $imgUrl;
			}
			else
			{
				$img = Image::make(config('evibe.gallery.root') . '/invibe/print-templates/' . $inviteTemplate->url);

				$inviteTemplateFields = InviteTemplateField::where('invite_template_id', $inviteTemplateId)->get();

				$inviteNameText = 'baby';
				$inviteNameIndex = 'invite';

				foreach ($inviteTemplateFields as $dataField)
				{
					$fieldValue = TicketTemplateFieldValue::where('ticket_invite_id', $ticketInviteId)
					                                      ->where('invite_template_field_id', $dataField->id)
					                                      ->get()->first();

					if ($fieldValue && $fieldValue->value)
					{
						if ($dataField->type_field_id == 7)
						{
							$fieldValue->value = date("d F Y, h:i A", Carbon::parse($fieldValue->value)->timestamp);
						}

						if (!is_null($dataField->pre_text))
						{
							$customerReplaces = [
								'#replaceText#' => $fieldValue->value
							];

							$fieldValue->value = str_replace(array_keys($customerReplaces), array_values($customerReplaces), $dataField->pre_text);
						}

						$img->text($fieldValue->value, $dataField->x_pos, $dataField->y_pos, function ($font) use ($dataField) {
							$font->file(public_path('/fonts/' . $dataField->font));
							$font->size($dataField->font_size);
							$font->color($dataField->font_color);
							$font->align($dataField->font_align);
							$font->valign($dataField->font_valign);
						});
					}

					if (strpos($dataField->name, 'baby_name') !== false)
					{
						if ($fieldValue->value && $fieldValue->value != 'n/a' && $fieldValue->value != 'N/A')
						{
							$babyName = $fieldValue->value;
							$babyNameArray = explode('.', $babyName);
							foreach ($babyNameArray as $key => $item)
							{
								$subItems = explode(' ', $item);
								foreach ($subItems as $string)
								{
									if (strlen($string) >= 4)
									{
										$babyName = $string;
										break;
									}
								}

								if ($babyName)
								{
									break;
								}
							}

							$inviteNameText = strtolower($babyName);
						}
					}
				}

				if ($inviteTemplate->type_card_category_id == config("evibe.type-card-category.pro"))
				{
					$img->fill(config("evibe.gallery.root") . "/invibe/watermark.png");
				}

				$inviteTemplateArray = explode(' ', $inviteTemplate->name);
				foreach ($inviteTemplateArray as $key => $item)
				{
					if (strlen($item) >= 4)
					{
						$inviteNameIndex = strtolower($item);
						break;
					}
				}

				// @see: need to change title appropriately after confirmation
				$imgTitle = 'evibe_' . $inviteNameText . '_' . $ticketInvite->id . '_' . $inviteNameIndex;

				if ($inviteTemplate->id > 21)
				{
					$imgTitle = 'Evibe-' . str_replace(" ", "-", $inviteTemplate->name) . '-' . $ticketInvite->id . time();
				}

				$imgUrl = $imgTitle . '.png';

				// save the image
				$newImgPath = config('evibe.gallery.root') . '/invibe/temp/';
				$newImgUrl = config('evibe.gallery.host') . '/invibe/temp/';
				if (!file_exists($newImgPath))
				{
					$oldumask = umask(0);
					mkdir($newImgPath, 0777, true);
					umask($oldumask);
				}

				$fullImgPath = $newImgPath . $imgUrl;
				$fullImgUrl = $newImgUrl . $imgUrl;
				$img->save($fullImgPath);

				$ticketInvite->title = $imgTitle;
				$ticketInvite->url = $imgUrl;
				$ticketInvite->updated_at = Carbon::now();
				$ticketInvite->save();
			}

			$data = [
				'inviteTemplate'  => $inviteTemplate,
				'ticketInvite'    => $ticketInvite,
				'editHashTkn'     => Hash::make($ticketInviteId . $ticketInvite->created_at . 'iVEdit'),
				'designPath'      => $fullImgUrl,
				'designUrl'       => $fullImgUrl,
				'cardTypeId'      => $inviteTemplate->type_card_id,
				'inviteStepIndex' => 3 // @see: hardcoded
			];

			//$diwaliTemplateIds = ["39", "40", "41"];
			//
			//if(in_array($inviteTemplateId, $diwaliTemplateIds))
			//{
			//	return view('invites.invite-design-dynamic', ['data' => $data]);
			//}

			return view('invites.invite-design', ['data' => $data]);
		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);

			return response()->json([
				                        'success' => false,
				                        'error'   => $exception->getMessage()
			                        ]);
		}
	}

	// Screen3
	public function downloadInviteImage()
	{
		$imagePath = request('imagePath');
		$imagePath = str_replace(config("evibe.gallery.host"), config("evibe.gallery.root"), $imagePath);

		return \Illuminate\Support\Facades\Response::download($imagePath);
	}

	public function validateAndStoreUserDetails()
	{
		$res['success'] = true;

		$rules = [
			'inviteDetailsName'  => 'required',
			'inviteDetailsEmail' => 'required|email',
			'inviteDetailsPhone' => 'required|phone',
		];

		$messages = [
			"inviteDetailsPhone.required" => "Please enter your phone number",
			"inviteDetailsPhone.phone"    => "Phone number is invalid (ex: 9640204000)",
			"inviteDetailsEmail.required" => "Please enter your email address",
			"inviteDetailsEmail.email"    => "Phone enter a valid email address",
			"inviteDetailsName.required"  => "Please enter your full name"
		];

		$validation = validator(request()->input(), $rules, $messages);

		if ($validation->fails())
		{
			$res['success'] = false;
			$res['errorMsg'] = $validation->messages()->first();
		}
		else
		{
			$data["name"] = request("inviteDetailsName");
			$data["email"] = request("inviteDetailsEmail");
			$data["phone"] = request("inviteDetailsPhone");
			$data["link"] = request("sourceUrl");

			$this->dispatch(new MailInviteEnquiryToTeamJob($data));

			$res['success'] = true;
		}

		return response()->json($res);
	}

	public function showInvitationPage($ivCode)
	{
		$url = request()->fullUrl();
		$ticketInvite = TicketInvite::where('rsvp_link', $url)
		                            ->whereNull('deleted_at')
		                            ->orderBy('invite_sent_at', 'DESC')
		                            ->orderBy('updated_at', 'DESC')
		                            ->first();

		if (!$ticketInvite && ($url == (config('evibe.host') . '/iv/Pra87089')))
		{
			// todo: need to fix in DASH and remove this code
			$ticketInvite = TicketInvite::where('ticket_id', 74579)->first();
		}

		if (!$ticketInvite)
		{
			$data = ['error' => true];

			return view('customer.invitation', ['data' => $data]);
		}

		$ticketId = $ticketInvite->ticket_id;
		$ticket = Ticket::find($ticketId);
		if (!$ticket)
		{
			$data = ['error' => true];

			return view('customer.invitation', ['data' => $data]);
		}

		$rsvpOptions = TypeRSVP::all();
		$ticketRSVPs = TicketRSVP::where('ticket_id', $ticketId)
		                         ->whereNull('deleted_at')
		                         ->get();

		$data = [
			'inviteImageUrl'  => config('evibe.gallery.host') . '/ticket/' . $ticketId . '/invites/' . $ticketInvite->url,
			'inviteImagePath' => config('evibe.gallery.root') . '/ticket/' . $ticketId . '/invites/' . $ticketInvite->url,
			'ticketId'        => $ticketId,
			'rsvpOptions'     => $rsvpOptions,
			'ticketRSVPs'     => $ticketRSVPs
		];

		$rsvpYesCount = 0;
		$rsvpMayBeCount = 0;
		$rsvpNoCount = 0;
		if (count($ticketRSVPs))
		{
			foreach ($ticketRSVPs as $ticketRSVP)
			{
				if ($ticketRSVP->type_rsvp_id == config('evibe.type-rsvp.yes'))
				{
					$rsvpYesCount++;
				}
				elseif ($ticketRSVP->type_rsvp_id == config('evibe.type-rsvp.may-be'))
				{
					$rsvpMayBeCount++;
				}
				elseif ($ticketRSVP->type_rsvp_id == config('evibe.type-rsvp.no'))
				{
					$rsvpNoCount++;
				}
			}
		}

		$data['rsvpYesCount'] = $rsvpYesCount;
		$data['rsvpMayBeCount'] = $rsvpMayBeCount;
		$data['rsvpNoCount'] = $rsvpNoCount;

		// assuming that ticket has the most earliest booking's party time
		if ($ticket->event_date < time())
		{
			$data['postPartyRSVP'] = true;
		}

		return view('customer.invitation', ['data' => $data]);

	}

	public function submitRSVP()
	{
		try
		{

			$rules = [
				'rsvpOption' => 'required|numeric|min:1',
				'name'       => 'required|min:4',
				'phone'      => 'required|digits:10',
				//'email'      => 'required|email',
			];

			$messages = [
				'rsvpOption.required' => 'Kindly select your RSVP status',
				'rsvpOption.min'      => 'Kindly select a valid RSVP status',
				'name.required'       => 'Your full name is required',
				'name.min'            => 'Your full name cannot be less than 4 characters',
				//'email.required'      => 'Your email id is required',
				//'email.email'         => 'Your email id should be in the form of example@abc.com',
				'phone.required'      => 'Your phone number is required',
				'phone.digits'        => 'Phone number is not valid. Enter only 10 digits number (ex: 9640204000)',
			];

			$validation = Validator::make(request()->input(), $rules, $messages);

			if ($validation->fails())
			{
				$response['success'] = false;
				$response['error'] = $validation->messages()->first();

				return response()->json($response);
			}

			$ticketId = request()->input('ticketId');
			$ticket = Ticket::find($ticketId);
			if (!$ticket)
			{
				$this->sendNonExceptionErrorReport([
					                                   'code'      => config('evibe.error_code.create_function'),
					                                   'url'       => request()->fullUrl(),
					                                   'method'    => request()->method(),
					                                   'message'   => '[CustomController.php - ' . __LINE__ . '] Unable to find ticket data for the invitation',
					                                   'exception' => '[CustomController.php - ' . __LINE__ . '] Unable to find ticket data for the invitation',
					                                   'trace'     => '[Ticket Id: ' . $ticketId . ']',
					                                   'details'   => '[Ticket Id: ' . $ticketId . ']'
				                                   ]);

				return response()->json([
					                        'success' => false,
				                        ]);
			}

			$rsvpOption = request()->input('rsvpOption');
			$name = request()->input('name');
			$phone = request()->input('phone');
			//$email = request()->input('email');
			$comments = request()->input('comments');

			$rsvpTicket = TicketRSVP::where('ticket_id', $ticketId)
			                        ->where('phone', $phone)
			                        ->whereNull('deleted_at')
			                        ->first();

			if ($rsvpTicket)
			{
				//return response()->json([
				//	                        'success' => false,
				//	                        'error'   => 'You have already submitted your response'
				//                        ]);

				$rsvpTicket->type_rsvp_id = $rsvpOption;
				$rsvpTicket->name = $name;
				$rsvpTicket->comments = $comments;
				$rsvpTicket->updated_at = Carbon::now();
				$rsvpTicket->save();

				return response()->json([
					                        'success'    => true,
					                        'successMsg' => 'Response updated successfully'
				                        ]);
			}
			else
			{
				$rsvpTicket = TicketRSVP::create([
					                                 'type_rsvp_id' => $rsvpOption,
					                                 'ticket_id'    => $ticketId,
					                                 'name'         => $name,
					                                 'phone'        => $phone,
					                                 'comments'     => $comments,
					                                 'created_at'   => Carbon::now(),
					                                 'updated_at'   => Carbon::now(),
				                                 ]);

				return response()->json([
					                        'success'    => true,
					                        'successMsg' => 'Response submitted successfully'
				                        ]);
			}

		} catch (\Exception $exception)
		{
			$this->sendErrorReport($exception);

			return response()->json([
				                        'success' => false,
				                        'error'   => 'Some error occurred while submitting your response. Kindly refresh the page and try again'
			                        ]);
		}
	}
}