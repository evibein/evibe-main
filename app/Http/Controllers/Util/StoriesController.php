<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;

use App\Models\Types\TypeEvent;
use App\Models\Util\City;
use App\Models\Util\CustomerStory;
use Illuminate\Http\Request;

class StoriesController extends BaseController
{
	// get customer stories
	public function showCustomerStories(Request $request, $cityUrl)
	{
		$urlParams = [['key' => 'city', 'value' => $cityUrl]];
		$validUrlObj = $this->validateUrlParams($urlParams);
		if (array_key_exists('isNotValid', $validUrlObj) && $validUrlObj['redirectUrl'])
		{
			return redirect($validUrlObj['redirectUrl']);
		}

		$cityId = getCityId();
		$info = 'Evibe.in customer reviews, customer stories and customer feedback. Evibe.in reviews given by customers.';
		$pageTitle = 'Evibe.in Customer Reviews';

		// requests
		$occasion = $request->input('occasion');
		$selectedCityUrl = $request->input('city');
		$checkCityIds = $cityId;

		if ($selectedCityUrl == "all")
		{
			$checkCityIds = City::pluck('id')->toArray();
		}
		elseif ($selectedCityUrl && $selectedCityUrl != $cityUrl)
		{
			$selectedCity = City::where('url', 'LIKE', $selectedCityUrl)->first();
			if ($selectedCity)
			{
				$checkCityIds = $selectedCity->id;
			}
		}
		else
		{
			// do nothing
		}

		// get the all occasion for filter
		$occasionIds = CustomerStory::forCity($checkCityIds)
		                            ->distinct()
		                            ->pluck('event_id');

		$occasions = TypeEvent::whereIn('id', $occasionIds)
		                      ->select('name', 'url')
		                      ->get();

		$stories = CustomerStory::forCity($checkCityIds);

		// filter by occasion
		if ($occasion)
		{
			$event = TypeEvent::where('url', trim($occasion))->first();
			if ($event)
			{
				$stories = $stories->where('event_id', $event->id);
			}
		}

		$cityList = City::select('name', 'url')
		                ->where('is_public', 1)
		                ->get()
		                ->all();

		$data = [
			'title'     => $pageTitle,
			'info'      => $info,
			'occasions' => $occasions,
			'cities'    => $cityList,
			'stories'   => $stories->orderBy('created_at', 'DESC')->paginate(21)
		];

		return view('app.stories.customer', ['data' => $data]);
	}
}
