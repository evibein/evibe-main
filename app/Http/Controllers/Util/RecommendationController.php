<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseRecommendationController;
use App\Jobs\HandleStoppingTicketAutoFollowup;
use App\Jobs\HandleUpdatingTicketMappingTable;
use App\Models\Ticket\Ticket;
use App\Jobs\Emails\Util\MailRecommendationToTeam;
use App\Jobs\Emails\Util\MailRecommendationFailToTeam;
use App\Jobs\Emails\Util\MailRecommendationRateToTeam;

use App\Models\Ticket\TicketFollowup;
use App\Models\Ticket\TicketMapping;
use App\Models\Ticket\TicketUpdate;
use App\Models\Types\TypeEvent;
use App\Models\Util\City;

use App\Models\Util\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RecommendationController extends BaseRecommendationController
{
	public function showRecommendation(Request $request)
	{
		$ticketId = $request->input('tId');
		$token = $request->input('token');
		$recoUrl = $request->fullUrl();
		$isFromDash = $request->input('isFromDash');

		if (!$ticketId || !$token)
		{
			$this->handleErrors(null, "Invalid ticket id or token for recommendation page on main website", '401');

			return "<h1>Invalid Request</h1>";
		}

		$ticket = Ticket::find($ticketId);

		if (!$ticket)
		{
			$this->handleErrors(null, "Ticket is not available", '403');

			return "<h1>Invalid Request</h1>";
		}

		$matchToken = $ticket->name . $ticket->phone;

		// @todo: remove check with $matchToken in by 15 Sep 2016.
		if (!Hash::check($matchToken, $token) && !Hash::check($ticketId, $token))
		{
			$this->handleErrors(null, "Token mismatch for ticket $ticketId", '403');

			return "<h1>Invalid Request</h1>";
		}

		if ($ticket->status_id == config('evibe.ticket.status.progress') ||
			$ticket->status_id == config('evibe.ticket.status.followup') ||
			$ticket->status_id == config('evibe.ticket.status.no_response')
		)
		{
			$mappings = $ticket->mappings;
			$recommendationData = [];
			$ticketTypeIds = [];
			$similarTypes = [];
			$recommendedType = [];

			// default city if ticket don't have city_id
			$cityId = $ticket->city_id ? $ticket->city_id : 1;
			$city = City::where('city.id', $cityId)->first();

			checkAndSetCitySessions($city);

			foreach ($mappings as $mapping)
			{
				try
				{
					$mappingData = $this->getRecommendationData($mapping);
					$type = ucfirst($mappingData['type']);

					if ($type)
					{
						$recommendationData[$type][] = $mappingData;

						if (!in_array($mappingData['ticketTypeId'], $ticketTypeIds))
						{
							$ticketTypeIds[] = $mappingData['ticketTypeId'];
						}
					}

				} catch (\Exception $e)
				{
					$this->handleErrors($e);
				}
			}
			try
			{
				$allMappings = $this->getAllCategoriesOfOccasion(($ticket->event_id));
				foreach ($allMappings as $eligibleMapping)
				{
					if (!in_array($eligibleMapping['mapTypeId'], $ticketTypeIds))
					{
						$similarTypes[] = $eligibleMapping;
					}
					else
					{
						$recommendedType[$eligibleMapping['mapTypeId']] = $eligibleMapping;
					}

				}
			} catch (\Exception $e)
			{
				$this->handleErrors($e);
			}

			// update ticket mappings + recalculate auto followup
			$this->dispatch(new HandleUpdatingTicketMappingTable([
				                                                     'ticketId'   => $ticketId,
				                                                     'isFromDash' => $isFromDash,
				                                                     'handlerId'  => $ticket->handler_id,
			                                                     ]));

			TicketUpdate::create([
				                     'ticket_id'   => $ticketId,
				                     'status_id'   => $ticket->status_id,
				                     'type_update' => config('evibe.ticket.type_update.auto'),
				                     'comments'    => 'Customer has visited the recommendations page',
				                     'status_at'   => time(),
				                     'created_at'  => date('Y-m-d H:i:s'),
				                     'updated_at'  => date('Y-m-d H:i:s')
			                     ]);

			$refKeys = $this->getRecommendationRefKeys($ticket);
			$shortlistedCount = $mappings->where('is_selected', 1)->count();

			$data = [
				'recommendations'  => $recommendationData,
				'recommendedType'  => $recommendedType,
				'ticket'           => $ticket,
				'similarTypes'     => $similarTypes,
				'refKeys'          => $refKeys,
				'shortlistedCount' => $shortlistedCount,
				'recoUrl'          => $recoUrl
			];

			$data = array_merge($data, $this->mergeView($city->url, $ticket->event_id));

			return view('recos/list')->with('data', $data);
		}
		else
		{
			return "<h1>Invalid request</h1>";
		}
	}

	public function sendRecommendationChoices($ticketId)
	{
		$res = ['success' => false];
		$ticket = Ticket::find($ticketId);

		if (!$ticket)
		{
			return response()->json($res);
		}

		$selectedRecommendations = (array)request('selectedRecos');

		if (count($selectedRecommendations) > 0)
		{
			$data = [
				'selectedRecos' => $selectedRecommendations,
				'ticket'        => $ticket
			];

			$count = 0;
			$now = Carbon::now();
			$selectedIds = [];
			foreach ($selectedRecommendations as $selectedRecommendation)
			{
				$mapping = TicketMapping::find($selectedRecommendation['id']);
				$selectedIds[] = $selectedRecommendation['id'];
				if ($mapping)
				{
					$mapping->update(['is_selected' => 1, 'selected_at' => $now]);
				}
				else
				{
					// remove that mapping which is deleted (very rare case)
					unset($selectedRecommendation[$count]);
				}

				$count++;
			}

			// reset for other mappings for this ticket
			TicketMapping::whereNotIn('id', $selectedIds)
			             ->where("ticket_id", $ticketId)
			             ->update(['is_selected' => null]);

			// add ticket update
			TicketUpdate::create([
				                     'ticket_id'   => $ticketId,
				                     'status_id'   => config('evibe.ticket.status.followup'),
				                     'type_update' => config('evibe.ticket.type_update.auto'),
				                     'handler_id'  => $ticket->handler_id ?: null,
				                     'comments'    => config('evibe.ticket.status_message.reco_select'),
				                     'status_at'   => time(),
				                     'created_at'  => date('Y-m-d H:i:s'),
				                     'updated_at'  => date('Y-m-d H:i:s')
			                     ]);

			$this->dispatch(new MailRecommendationToTeam($data));

			$this->dispatch(new HandleStoppingTicketAutoFollowup([
				                                                     'ticketId'       => $ticketId,
				                                                     'typeReminderId' => config('evibe.type_reminder.followup'),
				                                                     'handlerId'      => $ticket->handler_id,
				                                                     'invalidAt'      => time(),
				                                                     'stopMessage'    => 'Customer responded [Shortlisted some recommendations]'
			                                                     ]));

			$followup = TicketFollowup::whereNull('is_complete')
			                          ->where('ticket_id', $ticketId)
			                          ->where('followup_type', 'recommendation')
			                          ->first();
			if ($followup)
			{
				$followup->update([
					                  'is_complete'  => 1,
					                  'done_comment' => config("messages.recommendation.shortlist.complete")
				                  ]);
			}

			$res = ['success' => true];
		}

		return response()->json($res);
	}

	public function processRecommendationDislike($ticketId)
	{
		$now = Carbon::now();
		$ticket = Ticket::find($ticketId);

		if (!$ticket)
		{
			return response()->json(['success' => false]);
		}
		$ticket->update(['last_reco_dislike_at' => $now]);

		$city = City::find($ticket->city_id);
		$event = TypeEvent::find($ticket->event_id);
		$handler = User::find($ticket->handler_id);
		$partyTime = date('d M Y, h:i A', $ticket->event_date);
		$recoUrl = request('recoUrl');

		$selectedRecommendations = (array)request('selectedRecos');

		$data = [
			'selectedRecos' => $selectedRecommendations,
			'ticket'        => $ticket,
			'ticketId'      => $ticketId,
			'partyTime'     => $partyTime,
			'recoUrl'       => $recoUrl,
			'cityName'      => $city ? $city->name : "Default City",
			'eventName'     => $event ? $event->name : "Default Event",
			'handlerName'   => $handler ? $handler->name : "Default Handler"
		];

		$updateComment = config('evibe.ticket.status_message.reco_dislike');
		if (count($selectedRecommendations) > 0)
		{
			$count = 0;
			$selectedIds = [];
			$updateComment = config('evibe.ticket.status_message.reco_select_dislike');
			foreach ($selectedRecommendations as $selectedRecommendation)
			{
				$selectedIds[] = $selectedRecommendation['id'];
				$mapping = TicketMapping::find($selectedRecommendation['id']);

				if ($mapping)
				{
					$mapping->update(['is_selected' => 1, 'selected_at' => $now]);
				}
				else
				{
					// remove that mapping which is deleted (very rare case)
					unset($selectedRecommendation[$count]);
				}

				$count++;
			}

			TicketMapping::whereNotIn('id', $selectedIds)->update(['is_selected' => null, 'selected_at' => null]);
		}

		// add ticket update
		TicketUpdate::create([
			                     'ticket_id'   => $ticketId,
			                     'status_id'   => config('evibe.ticket.status.progress'),
			                     'type_update' => config('evibe.ticket.type_update.auto'),
			                     'comments'    => $updateComment,
			                     'status_at'   => time(),
			                     'created_at'  => date('Y-m-d H:i:s'),
			                     'updated_at'  => date('Y-m-d H:i:s')
		                     ]);

		$followup = TicketFollowup::whereNull('is_complete')
		                          ->where('ticket_id', $ticketId)
		                          ->where('followup_type', 'recommendation')
		                          ->first();
		if ($followup)
		{
			// auto set followup to tomorrow 11 AM
			$followup->update([
				                  'followup_date' => Carbon::tomorrow()->startOfDay()->addHour(11)->timestamp,
				                  'done_comment'  => config("messages.recommendation.dislike.followup")
			                  ]);
		}

		$this->dispatch(new MailRecommendationFailToTeam($data));

		$this->dispatch(new HandleStoppingTicketAutoFollowup([
			                                                     'ticketId'       => $ticketId,
			                                                     'typeReminderId' => config('evibe.type_reminder.followup'),
			                                                     'handlerId'      => $ticket->handler_id,
			                                                     'invalidAt'      => time(),
			                                                     'stopMessage'    => 'Customer responded [Did not like our recommendations]'
		                                                     ]));

		return response()->json(['success' => true, 'updatedLastDislikedAt' => $now]);
	}

	public function processRecoRatingFeedback()
	{
		$now = Carbon::now();
		$ticketId = request('ticketId');
		$ticket = Ticket::find($ticketId);

		if (!$ticket)
		{
			return response()->json(['success' => false]);
		}

		$rating = request('recoRating') ? request('recoRating') : null;
		$ticketUpdateData = [
			'ticket_id'   => $ticketId,
			'status_id'   => $ticket->status_id,
			'type_update' => config('evibe.ticket.type_update.auto'),
			'comments'    => 'Update related to reco rating feedback',
			'status_at'   => time(),
			'created_at'  => date('Y-m-d H:i:s'),
			'updated_at'  => date('Y-m-d H:i:s')
		];

		if ($rating)
		{
			$ticket->update([
				                'last_reco_rated_at' => $now,
				                'last_reco_rating'   => $rating
			                ]);

			$ticketUpdateData['comments'] = config("messages.recommendation.rating.rated");
			TicketUpdate::create($ticketUpdateData);

			$recoLink = config('evibe.host') . "/recommendation?tId=" . $ticket->id . "&token=" . Hash::make($ticket->name . $ticket->phone);
			$dashTicketLink = config('evibe.dash_host') . '/tickets/' . $ticket->id;
			$handler = $ticket->handler && $ticket->handler->name ? $ticket->handler->name : "default";

			if ($rating)
			{
				$subject = '[#' . $ticketId . '] ' . $ticket->name . " has rated $rating for recommendations sent by $handler";
				$message = "has rated the recommendations sent by us.";
			}
			else
			{
				$subject = '[#' . $ticketId . '] [Skipped] ' . $ticket->name . ' has skipped rating the recommendations';
				$message = 'has skipped rating the recommendations sent by us.';
			}

			$data = [
				'ticket'     => $ticket,
				'handler'    => $handler,
				'ticketLink' => $dashTicketLink,
				'newRating'  => $rating,
				"recoLink"   => $recoLink,
				"sub"        => $subject,
				"msg"        => $message
			];

			$this->dispatch(new MailRecommendationRateToTeam($data));
			if ($rating < 3)
			{
				$this->dispatch(new HandleStoppingTicketAutoFollowup([
					                                                     'ticketId'       => $ticketId,
					                                                     'typeReminderId' => config('evibe.type_reminder.followup'),
					                                                     'handlerId'      => $ticket->handler_id,
					                                                     'invalidAt'      => time(),
					                                                     'stopMessage'    => 'Customer rated < 3 for our recommendations'
				                                                     ]));
			}
		}
		else
		{
			$ticket->update(['last_reco_skipped_at' => $now]);

			$ticketUpdateData['comments'] = config("messages.recommendation.rating.skipped");
			TicketUpdate::create($ticketUpdateData);
		}

		return response()->json(['success' => true]);
	}

	public function processRecommendationNextSteps($ticketId)
	{
		$now = Carbon::now();
		$res = ['success' => false];
		$ticket = Ticket::find($ticketId);
		if (!$ticket)
		{
			return response()->json($res);
		}

		$recoOption = \request('recoOption');
		$recoText = \request('recoText');

		if (!$recoOption)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'Kindly select a next step to proceed'
			                        ]);
		}

		$selectedRecommendations = (array)request('selectedRecos');
		$selectedIds = [];

		if (count($selectedRecommendations) > 0)
		{
			$data = [
				'selectedRecos' => $selectedRecommendations,
				'ticket'        => $ticket,
				'ticketLink'    => config('evibe.dash_host') . '/tickets/' . $ticket->id
			];

			$count = 0;

			foreach ($selectedRecommendations as $selectedRecommendation)
			{
				$selectedIds[] = $selectedRecommendation['id'];
				$mapping = TicketMapping::find($selectedRecommendation['id']);

				if ($mapping)
				{
					$mapping->update(['is_selected' => 1, 'selected_at' => $now]);
				}
				else
				{
					// remove that mapping which is deleted (very rare case)
					unset($selectedRecommendation[$count]);
				}

				$count++;
			}

			TicketMapping::whereNotIn('id', $selectedIds)->update(['is_selected' => null, 'selected_at' => null]);

			if (count($selectedRecommendations) > 0)
			{
				// add ticket update
				TicketUpdate::create([
					                     'ticket_id'   => $ticketId,
					                     'status_id'   => config('evibe.ticket.status.followup'),
					                     'type_update' => config('evibe.ticket.type_update.auto'),
					                     'handler_id'  => $ticket->handler_id ?: null,
					                     'comments'    => config('evibe.ticket.status_message.reco_select'),
					                     'status_at'   => time(),
					                     'created_at'  => date('Y-m-d H:i:s'),
					                     'updated_at'  => date('Y-m-d H:i:s')
				                     ]);

				// save to DB (ticket update)
				TicketUpdate::create([
					                     'ticket_id'   => $ticket->id,
					                     'status_id'   => $ticket->status_id,
					                     'status_at'   => $now,
					                     'type_update' => "Auto",
					                     'comments'    => $recoOption . '. ' . ($recoText ? 'Message: ' . $recoText : ''),
					                     'created_at'  => date('Y-m-d H:i:s'),
					                     'updated_at'  => date('Y-m-d H:i:s')
				                     ]);

				$nextStep = '';

				switch ($recoOption)
				{
					case config('evibe.reco-next-steps.1'):
						$ticket->lead_status_id = config('evibe.ticket.type_lead_status.hot');
						$nextStep = '[WANT TO BOOK]';
						break;

					case config('evibe.reco-next-steps.2'):
						$nextStep = '[NEED MORE DETAILS]';
						break;

					case config('evibe.reco-next-steps.3'):
						$nextStep = '[NEED CUSTOMISATION]';
						break;
				}

				$data['sub'] = '[#' . $ticketId . '] ' . $nextStep . ' ' . ucwords($ticket->name) . ' has selected few choices';
				$data['nextStep'] = $nextStep;
				$data['message'] = $recoText;

				$ticket->save();

				// alert team
				$this->dispatch(new MailRecommendationToTeam($data));

				$this->dispatch(new HandleStoppingTicketAutoFollowup([
					                                                     'ticketId'       => $ticketId,
					                                                     'typeReminderId' => config('evibe.type_reminder.followup'),
					                                                     'handlerId'      => $ticket->handler_id,
					                                                     'invalidAt'      => time(),
					                                                     'stopMessage'    => 'Customer responded [Shortlisted some recommendations]'
				                                                     ]));

				$followup = TicketFollowup::whereNull('is_complete')
				                          ->where('ticket_id', $ticketId)
				                          ->where('followup_type', 'recommendation')
				                          ->first();
				if ($followup)
				{
					$followup->update([
						                  'is_complete'  => 1,
						                  'done_comment' => config("messages.recommendation.shortlist.complete")
					                  ]);
				}

				$res = ['success' => true];
			}
		}

		return response()->json($res);
	}

	public function processNewRecommendations($ticketId)
	{
		// validate
		$now = Carbon::now();
		$res = ['success' => false];
		$ticket = Ticket::find($ticketId);
		if (!$ticket)
		{
			return response()->json($res);
		}

		$recoOption = \request('recoOption');
		$recoText = \request('recoText');

		if (!$recoOption)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'Kindly select a next step to proceed'
			                        ]);
		}

		$ticket->update(['last_reco_dislike_at' => $now]);

		$selectedRecommendations = (array)request('selectedRecos');

		$data = [
			'selectedRecos' => $selectedRecommendations
		];

		$updateComment = config('evibe.ticket.status_message.reco_dislike');
		if (count($selectedRecommendations) > 0)
		{
			$selectedIds = [];
			$count = 0;
			$updateComment = config('evibe.ticket.status_message.reco_select_dislike');

			foreach ($selectedRecommendations as $selectedRecommendation)
			{
				$selectedIds[] = $selectedRecommendation['id'];
				$mapping = TicketMapping::find($selectedRecommendation['id']);

				if ($mapping)
				{
					$mapping->update(['is_selected' => 1, 'selected_at' => $now]);
				}
				else
				{
					// remove that mapping which is deleted (very rare case)
					unset($selectedRecommendation[$count]);
				}

				$count++;
			}

			TicketMapping::whereNotIn('id', $selectedIds)->update(['is_selected' => null, 'selected_at' => null]);
		}

		// add ticket update
		TicketUpdate::create([
			                     'ticket_id'   => $ticketId,
			                     'status_id'   => config('evibe.ticket.status.progress'),
			                     'type_update' => config('evibe.ticket.type_update.auto'),
			                     'comments'    => $updateComment,
			                     'status_at'   => time(),
			                     'created_at'  => date('Y-m-d H:i:s'),
			                     'updated_at'  => date('Y-m-d H:i:s')
		                     ]);

		$followup = TicketFollowup::whereNull('is_complete')
		                          ->where('ticket_id', $ticketId)
		                          ->where('followup_type', 'recommendation')
		                          ->first();
		if ($followup)
		{
			// auto set followup to tomorrow 11 AM
			$followup->update([
				                  'followup_date' => Carbon::tomorrow()->startOfDay()->addHour(11)->timestamp,
				                  'done_comment'  => config("messages.recommendation.dislike.followup")
			                  ]);
		}

		// save to DB (ticket update)
		// cancel ticket
		$ticket->status_id = config('evibe.ticket.status.cancelled');
		TicketUpdate::create([
			                     'ticket_id'   => $ticket->id,
			                     'status_id'   => config('evibe.ticket.status.cancelled'),
			                     'status_at'   => $now,
			                     'type_update' => "Auto",
			                     'comments'    => $recoOption . '. ' . ($recoText ? 'Message: ' . $recoText : ''),
			                     'created_at'  => date('Y-m-d H:i:s'),
			                     'updated_at'  => date('Y-m-d H:i:s')
		                     ]);

		$nextStep = '';

		switch ($recoOption)
		{
			case config('evibe.reco-next-steps.4'):
				$nextStep = '[WANT OPTIONS IN LOWER PRICE RANGE]';
				break;

			case config('evibe.reco-next-steps.5'):
				$nextStep = '[WANT OPTIONS IN HIGHER PRICE RANGE]';
				break;

			case config('evibe.reco-next-steps.6'):
				$nextStep = '[OTHER, MENTIONED IN COMMENT/MESSAGE]';
				break;
		}

		$ticket->save();

		$newTicket = Ticket::create([
			                            'name'              => $ticket->name,
			                            'phone'             => $ticket->phone,
			                            'email'             => $ticket->email,
			                            'comments'          => $ticket->comments,
			                            'source_id'         => $ticket->source_id,
			                            'status_id'         => config('evibe.ticket.status.initiated'),
			                            'city_id'           => $ticket->city_id,
			                            'venue_landmark'    => $ticket->venue_landmark,
			                            'event_date'        => $ticket->event_date,
			                            'event_id'          => $ticket->event_id,
			                            'enquiry_source_id' => $ticket->enquiry_source_id
		                            ]);
		// generate enquiry id
		$newTicket->update(['enquiry_id' => config("evibe.ticket.enq.pre.new_recommendation") . $newTicket->id]);

		$data['sub'] = '[#' . $newTicket->id . '] [CUSTOMER NOT SATISFIED][HOT] ' . ucwords($ticket->name) . ' needs new recommendations';
		$data['nextStep'] = $nextStep;
		$data['message'] = $recoText;

		$this->updateTicketAction([
			                          'ticket'   => $newTicket,
			                          'comments' => "Copy ticket of old ticket with Id: " . $ticket->id . ", which is cancelled due to recos dislike.",
		                          ]);

		$data['ticket'] = $newTicket;
		$data['ticketId'] = $newTicket->id;
		$data['ticketLink'] = config('evibe.dash_host') . '/tickets/' . $newTicket->id;
		$data['oldTicketLink'] = config('evibe.dash_host') . '/tickets/' . $ticket->id;

		$this->dispatch(new MailRecommendationFailToTeam($data));

		// handle auto followup of old ticket
		$this->dispatch(new HandleStoppingTicketAutoFollowup([
			                                                     'ticketId'       => $ticketId,
			                                                     'typeReminderId' => config('evibe.type_reminder.followup'),
			                                                     'handlerId'      => $ticket->handler_id,
			                                                     'invalidAt'      => time(),
			                                                     'stopMessage'    => 'Customer responded [Did not like our recommendations]'
		                                                     ]));

		return response()->json(['success' => true, 'updatedLastDislikedAt' => $now]);
	}
}