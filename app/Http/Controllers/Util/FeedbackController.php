<?php

/**
 * @author Anji <anji@evibe.in>
 * @since  3 Jan 2015
 */

namespace App\Http\Controllers\Util;

use App\Jobs\Emails\ReferAndEarn\EmailToCustomerAfterFeedback;
use App\Jobs\Emails\Review\MailPartnerAutoReviewJob;
use App\Jobs\SMS\ReferAndEarn\SMSReferAndEarnReminder;
use App\Models\Types\TypeEvent;
use App\Models\Util\City;
use Carbon\Carbon;

use App\Http\Controllers\Base\BaseController;

use App\Jobs\Emails\Errors\MailFeedbackSubmitErrorToAdminJob;
use App\Jobs\Emails\Review\MailReviewToTeamJob;

use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketReview;
use App\Models\Ticket\TicketUpdate;
use App\Models\Util\PartnerReviewAnswer;
use App\Models\Util\PartnerReviewQuestion;
use App\Models\Util\TicketReviewQuestion;
use App\Models\Util\TicketReviewQuestionAnswer;
use App\Models\Util\TicketReviewQuestionOption;
use App\Models\Vendor\VendorReview;

use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class FeedbackController extends BaseController
{
	public function showCustomFeedbackPage()
	{
		$ticketId = (int)request('id');
		$token = request('token');

		$this->checkTokens($ticketId, $token);

		$ticket = Ticket::with('bookings', 'bookings.mapping')
		                ->where('id', $ticketId)
		                ->where('status_id', config('evibe.ticket.status.booked'))
		                ->first();

		if (!$ticket)
		{
			return "<h1>Invalid Request</h1>";
		}

		// check if feedback already given
		// if yes - redirect to "thank you" page
		if (!is_null($ticket->feedback_received_at))
		{
			$thanksUrl = route('feedback.success.show');
			$thanksUrl .= '?id=' . $ticketId . '&token=' . $token;

			return redirect($thanksUrl);
		}

		// total booking amount
		$bookings = $ticket->bookings;
		$bookingAmount = 0;

		$bookingData = [
			'ticketId'      => $ticket->id,
			'name'          => $ticket->name,
			'phone'         => $ticket->phone,
			'email'         => $ticket->email,
			'bookingAmount' => 0,
		];

		$vendorsData = [];

		foreach ($bookings as $booking)
		{
			$mapping = $booking->mapping;
			if (!$mapping)
			{
				continue;
			}

			$bookingAmount += $booking->booking_amount;

			// @see: taking ratings for distinct providers (venue / planner)
			// avoiding cases where same provider has multiple bookings for this ticket
			$partnerUniqueKey = $booking->map_type_id . '_' . $booking->map_id;
			if (isset($vendorsData[$partnerUniqueKey]))
			{
				continue;
			}

			$questionData = [];
			$reviewQuestions = PartnerReviewQuestion::where('type_ticket_id', $booking->map_type_id)->get();

			foreach ($reviewQuestions as $reviewQuestion)
			{
				$questionData[] = $reviewQuestion;
			}

			$vendorsData[$partnerUniqueKey] = [
				'id'                => $booking->map_id,
				'name'              => $mapping->map_type_id == config('evibe.ticket.type.venue') ? $booking->provider->name : $booking->provider->person,
				'type'              => $mapping->map_type_id == config('evibe.ticket.type.venue') ? "venue" : "party organiser", // type in text
				'ticketTypeId'      => $booking->mapping->map_type_id,
				'providerMapTypeId' => $booking->map_type_id, // venue or planner
				'bookingId'         => $booking->id,
				'reviewQuestions'   => $questionData
			];
		}

		$bookingData['bookingAmount'] = $bookingAmount;
		$ticketReviewQuestions = TicketReviewQuestion::all();

		$data = [
			'vendors' => $vendorsData,
			'booking' => $bookingData,
			'trqs'    => $ticketReviewQuestions
		];

		return view('custom-feedback/view', ['data' => $data]);
	}

	public function showThanksPage()
	{
		$data = $this->validateBeforeThanks();

		return view('custom-feedback.thanks.show', ['data' => $data]);
	}

	public function showThanksPageWithGoogleLink()
	{
		$data = $this->validateBeforeThanks();

		return view('custom-feedback.thanks.rate', ['data' => $data]);
	}

	public function postCustomerFeedback()
	{
		$ticketId = request('ticketId');
		$ticket = Ticket::findOrFail($ticketId);
		$partnerUserRatings = request('vendorRatings');
		$partnerRatingsData = [];
		$isUserAcceptsReferAndEarn = false;

		// check if vendor is rated
		foreach ($partnerUserRatings as $partnerRating)
		{
			if (array_key_exists('questions', $partnerRating))
			{
				$rules = $validationValues = $messages = [];
				$errorMessage = 'Please rate ' . $partnerRating['name'] . "'s";

				foreach ($partnerRating['questions'] as $question)
				{
					$fieldName = str_replace(" ", "", $question['name']);
					$eName = strtolower($question['name']);

					// validation data for provider rating rules
					$validationValues[$fieldName] = $question['ratings'];

					// validation rules for provider rating
					$rules[$fieldName] = "required|numeric|min:0.5";

					// custom validation message for validation rules
					$messages[$fieldName . '.required'] = "$errorMessage $eName on scale of 5";
					$messages[$fieldName . '.numeric'] = "$errorMessage $eName on scale of 5";
					$messages[$fieldName . '.min'] = "$errorMessage $eName on scale of 5";
				}

				$errors = $this->checkFeedBackValidation($validationValues, $rules, $messages);

				if ($errors)
				{
					return response()->json($errors);
				}
			}
		}

		// validation for other evibe ratings field
		$rules = [
			'customerService'   => 'required|numeric|min:0.5',
			'easeOfBooking'     => 'required|numeric|min:0.5',
			'evibeExperience'   => 'required',
			'bookEvibeConvince' => 'required',
		];

		$messages = [
			"easeOfBooking.required"     => "Please rate ease of booking on a scale of 5",
			"easeOfBooking.numeric"      => "Please rate ease of booking on a scale of 5",
			"easeOfBooking.min"          => "Please rate ease of booking on a scale of 5",
			"customerService.required"   => "Please rate Evibe customer care service on a scale of 5",
			"customerService.numeric"    => "Please rate Evibe customer care service on a scale of 5",
			"customerService.min"        => "Please rate Evibe customer care service on a scale of 5",
			"evibeExperience.required"   => "Please enter your experience with Evibe.in",
			"bookEvibeConvince.required" => "Please enter the reason that convinced you to book from us"
		];

		$errors = $this->checkFeedBackValidation(request()->all(), $rules, $messages);
		if ($errors)
		{
			return response()->json($errors);
		}

		// validate extra ratings questions
		$ticketExtraRatings = request('ticketExtraRatings');
		if (count($ticketExtraRatings) > 0)
		{
			foreach ($ticketExtraRatings as $ticketExtraRating)
			{
				$question = TicketReviewQuestion::find($ticketExtraRating['questionId']);

				if ($question && ($question->is_required == 1))
				{
					if ($question->type_question_id == config('evibe.type_question.text'))
					{
						if (!$ticketExtraRating['answerText'])
						{
							return response()->json([
								                        'success' => false,
								                        'errors'  => ['Please answer question <b>' . $question->question . '</b>']
							                        ]);
						}
					}
					else
					{
						if (empty($ticketExtraRating['answerIds']) || count($ticketExtraRating['answerIds']) == 0)
						{
							return response()->json([
								                        'success' => false,
								                        'errors'  => ['Please answer question <b>' . $question->question . '</b>']
							                        ]);
						}
					}
				}
			}
		}

		try
		{
			// begin the transaction
			//DB::beginTransaction();

			$allRat = 0;
			$allRCount = 0;
			$negativePartnerRatingCount = 0;

			// update planner_review_table
			foreach ($partnerUserRatings as $partnerRating)
			{
				$partnerReviewDBInput = [
					'name'              => $ticket->name,
					'email'             => $ticket->email,
					'phone'             => $ticket->phone,
					'location'          => $ticket->area ? $ticket->area->name : '',
					'event_id'          => $ticket->event_id ? $ticket->event_id : null,
					'map_type_id'       => $partnerRating['providerMapTypeId'],
					'map_id'            => $partnerRating['id'],
					'type_ticket_id'    => $partnerRating['ticketTypeId'],
					'ticket_booking_id' => $partnerRating['ticketBookingId'],
					'review'            => $partnerRating['comment'] ? ($partnerRating['comment'] == "Start typing here..." ? "" : $partnerRating['comment']) : "",
					'uniform'           => isset($partnerRating['uniform']) ? $partnerRating['uniform'] : null,
					'rating'            => 0
				];

				// collect the vendor rating and save it to the answer table.
				$qCount = 0;
				$totalRatings = 0;
				$hasQuestion = false;

				if (array_key_exists('questions', $partnerRating))
				{
					foreach ($partnerRating['questions'] as $question)
					{
						$partnerRating['partnerReview'][] = [
							'question' => $question['name'],
							'ratings'  => $question['ratings']
						];

						$totalRatings += $question['ratings'];
						$qCount++;
					}
					$hasQuestion = true;
				}

				$allRat += $totalRatings;
				$allRCount += $qCount;

				$calculatedRating = $qCount > 0 ? round(($totalRatings / $qCount), 2) : 0;
				$partnerReviewDBInput['rating'] = $calculatedRating;

				if ($calculatedRating < 4)
				{
					$negativePartnerRatingCount += 1;
				}
				elseif ($calculatedRating >= 4)
				{
					$partnerReviewDBInput['is_accepted'] = 1;
					$partnerReviewDBInput['accepted_by_id'] = config("evibe.default.review_handler");
				}

				// save data in database
				$review = VendorReview::create($partnerReviewDBInput);

				if ($review && $review->id)
				{
					// update the every answer with planner review id
					if ($hasQuestion)
					{
						foreach ($partnerRating['questions'] as $question)
						{
							PartnerReviewAnswer::create([
								                            'review_id'   => $review->id,
								                            'question_id' => $question['id'],
								                            'rating'      => $question['ratings']
							                            ]);
						}
					}

					// send the mail to the partner for auto accept reviews through api's
					if ($calculatedRating >= 4)
					{
						$url = config("evibe.api.base_url") . "common/partner/review/auto-accept/success/" . $review->id;
					}
					else
					{
						$url = config("evibe.api.base_url") . "common/partner/review/auto-accept/fail/" . $review->id;
					}

					try
					{
						$this->dispatch(new MailPartnerAutoReviewJob(["reviewId" => $review->id, "url" => $url]));

					} catch (ClientException $e)
					{
						// report admin in case of error
						$response['apiRequestError'] = "Some error occurred while submitting the review, Please try again after reloading the page";

						return response()->json([
							                        'errors'          => "Error while automating feedback by customer",
							                        'apiRequestError' => true
						                        ]);
					}

					$partnerRating['rating'] = $calculatedRating;
					$partnerRatingsData[] = $partnerRating;
				}
			}

			// evibe ratings
			$customerServiceRat = request('customerService');
			$easeOfBookingRat = request('easeOfBooking');
			$priceForQualityRat = request('qualityForService');
			$allRat += $customerServiceRat + $easeOfBookingRat + $priceForQualityRat;
			$allRCount += 3;
			$allRatRound = $allRCount > 0 ? round(($allRat / $allRCount), 2) : 0;

			// update ticket_review table
			$ticketReview = TicketReview::create([
				                                     'ticket_id'          => $ticketId,
				                                     'customer_care'      => $customerServiceRat,
				                                     'ease_of_booking'    => $easeOfBookingRat,
				                                     'price_for_quality'  => $priceForQualityRat,
				                                     'is_recommend_evibe' => request('isRecommend'),
				                                     'review'             => request('evibeExperience') ? (request('evibeExperience') == "Start typing here..." ? "" : request('evibeExperience')) : "",
				                                     'six_star_review'    => request('bookEvibeConvince') ? (request('bookEvibeConvince') == "Start typing here..." ? "" : request('bookEvibeConvince')) : "",
				                                     'billing_amount'     => 0
			                                     ]);

			// creating ticket dynamic question review
			$ticketExtraRatings = request('ticketExtraRatings');
			$extraQuestionData = [];

			if (count($ticketExtraRatings) > 0)
			{
				foreach ($ticketExtraRatings as $ticketExtraRating)
				{
					$answerData = [];
					$question = TicketReviewQuestion::find($ticketExtraRating['questionId']);
					$answerString = '';
					if ($question)
					{
						if ($question->type_question_id == config('evibe.type_question.text'))
						{
							$answerData[] = [
								'review_id'   => $ticketReview->id,
								'answer_text' => $ticketExtraRating['answerText'],
								'question_id' => $question->id,
								'updated_at'  => Carbon::now(),
								'created_at'  => Carbon::now()
							];

							$answerString .= $ticketExtraRating['answerText'];
						}
						else
						{
							if (isset($ticketExtraRating['answerIds']) && count($ticketExtraRating['answerIds']) > 0)
							{
								foreach ($ticketExtraRating['answerIds'] as $optionId)
								{
									$option = TicketReviewQuestionOption::find($optionId);
									$answerData[] = [
										'review_id'   => $ticketReview->id,
										'option_id'   => $optionId,
										'question_id' => $question->id,
										'updated_at'  => Carbon::now(),
										'created_at'  => Carbon::now()
									];

									$answerString .= ', ' . $option->text;

									// check if user wants to participate in refer & earn
									if ($question->id == config("evibe.feedback.question.re.id")
										&& $optionId == config("evibe.feedback.question.re.option.yes"))
									{
										$isUserAcceptsReferAndEarn = true;
									}
								}
							}
						}

						$extraQuestionData[] = [
							'question' => $question->question,
							'answer'   => trim($answerString, ', ')
						];
					}

					TicketReviewQuestionAnswer::insert($answerData);
				}
			}

			$occasion = $ticket->event_id ? TypeEvent::find($ticket->event_id) : null;
			$city = $ticket->city_id ? City::find($ticket->city_id) : null;

			// send email to team
			$alertsData = [
				'ticketId'           => $ticketId,
				'handlerName'        => $ticket->handler ? $ticket->handler->name : "Team",
				'customer'           => [
					'name'  => $ticket->name,
					'email' => $ticket->email,
					'phone' => $ticket->phone
				],
				'occasion'           => ($occasion && $occasion->name) ? $occasion->name : null,
				'city'               => ($city && $city->name) ? $city->name : null,
				'isRecommend'        => request('isRecommend'),
				'customerCare'       => request('customerService'),
				'easeOfBooking'      => request('easeOfBooking'),
				'priceForQuality'    => request('qualityForService'),
				'moreFeedback'       => request('evibeExperience'),
				'sixStar'            => request('bookEvibeConvince'),
				'partnerRatingsData' => $partnerRatingsData,
				'extraQData'         => $extraQuestionData
			];

			$this->dispatch(new MailReviewToTeamJob($alertsData));

			$now = time();
			$ticket->feedback_received_at = date('Y-m-d H:i:s', $now);
			$ticket->updated_at = date('Y-m-d H:i:s', $now);
			$ticket->save();

			// update ticket_update
			$ticketUpdateData = [
				'ticket_id'   => $ticket->id,
				'status_id'   => $ticket->status_id,
				'status_at'   => $now,
				'type_update' => "Auto",
				'comments'    => "Received feedback from customer",
				'created_at'  => date('Y-m-d H:i:s', $now),
				'updated_at'  => date('Y-m-d H:i:s', $now)
			];
			TicketUpdate::create($ticketUpdateData);

			/*
			 * If customer interested in refer-earn & review > 4: show both RE & google review in thank you page
			 * If customer interested in refer-earn & review < 4: show only RE in thank you page
			 * If customer not interested in refer-earn & review > 4: direct redirect to google review page
			 * If customer not interested in refer-earn & review < 4: Show normal thank you page
			*/
			$redirectUrl = route('feedback.success.show');

			if ($isUserAcceptsReferAndEarn)
			{
				$this->dispatch(new EmailToCustomerAfterFeedback($ticket));
				$this->dispatch(new SMSReferAndEarnReminder($ticket));

				if ($allRatRound >= 4 && $negativePartnerRatingCount == 0)
				{
					$redirectUrl = route('feedback.success.rate');
				}
			}
			elseif ($allRatRound >= 4 && $negativePartnerRatingCount == 0)
			{
				$redirectUrl = "https://goo.gl/6wzhUV";
			}
			else
			{
				// do nothing
			}

			$redirectUrl .= '?id=' . $ticketId;
			$redirectUrl .= '&token=' . Hash::make($ticketId);

			// success, commit database
			//DB::commit();
			$response = ['success' => true, 'redirectUrl' => $redirectUrl];

			// @todo: send alerts for each partner
			// if rating > 4, auto accept & notify partner, BD team
			// if rating < 4, alert partner, BD & CRM team

		} catch (\Exception $e)
		{
			$this->sendErrorReport($e);
			// rollback if some of the db query failed
			//DB::rollback();
			$response['success'] = false;
			$response['errors'] = ["Some error occurred while submitting the review, if error persists, please report our team"];
		}

		return response()->json($response);
	}

	public function postFeedbackSubmitFail()
	{
		$data = request()->input();
		if (!config('app.debug'))
		{
			$this->dispatch(new MailFeedbackSubmitErrorToAdminJob($data));
		}
	}

	private function checkTokens($ticketId, $token)
	{
		if (!$ticketId || !$token || !Hash::check($ticketId, $token))
		{
			abort(404);
		}
	}

	private function validateBeforeThanks()
	{
		$ticketId = (int)request('id');
		$token = request('token');

		$this->checkTokens($ticketId, $token);

		$ticket = Ticket::findOrFail($ticketId);

		// show thanks for only customers who gave feedback
		if (is_null($ticket->feedback_received_at))
		{
			abort(404);
		}

		$ticketReview = TicketReview::where("ticket_id", $ticket->id)->first();
		$isUserAcceptReferAndEarn = 0;

		if ($ticketReview)
		{
			$isUserAcceptReferAndEarn = TicketReviewQuestionAnswer::where("review_id", $ticketReview->id)
			                                                      ->where("question_id", config("evibe.feedback.question.re.id"))
			                                                      ->where("option_id", config("evibe.feedback.question.re.option.yes"))
			                                                      ->get()
			                                                      ->count();
		}

		$data = [
			'name'             => $ticket->name,
			'ticketId'         => $ticket->id,
			'isRecommendEvibe' => $isUserAcceptReferAndEarn > 0 ? 1 : 0
		];

		return $data;
	}

	private function checkFeedBackValidation($valueToValidate, $rules, $messages)
	{
		$resMes = [];
		$validation = Validator::make($valueToValidate, $rules, $messages);
		if ($validation->fails())
		{
			$errors = $validation->messages();
			$resMes = [
				'success' => false,
				'errors'  => []
			];

			foreach ($errors->all() as $error)
			{
				array_push($resMes['errors'], $error);
			}
		}

		return $resMes;
	}
}