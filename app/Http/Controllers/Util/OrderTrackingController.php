<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Base\BaseController;
use App\Jobs\Emails\Tickets\MailTicketUpdateRequestToTeamJob;
use App\Jobs\Emails\Util\MailCustomerProofUploadAlertToCustomer;
use App\Jobs\Emails\Util\MailCustomerProofUploadAlertToTeam;
use App\Jobs\SMS\SMSCustomerProofUploadToCustomer;
use App\Jobs\SMS\TMO\SendOTPToCustomer;
use App\Models\Cake\Cake;
use App\Models\Decor\Decor;
use App\Models\Package\Package;
use App\Models\Product\ProductBooking;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketBooking;
use App\Models\Ticket\TicketBookingGallery;
use App\Models\Ticket\TicketPostBookingRequests;
use App\Models\Trend\Trend;
use App\Models\Types\TypeProof;
use App\Models\Types\TypeServices;
use App\Models\Util\CheckoutField;
use App\Models\Util\CustomerOrderProof;
use App\Models\Util\TMO;
use App\Models\Util\User;
use App\Models\Venue\VenueHall;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class OrderTrackingController extends BaseController
{
	public function checkLogin()
	{
		$user = Auth::user();

		if (!$user)
		{
			$ticketId = request("id");
			$token = request("token");

			$ticket = Ticket::find($ticketId);
			$email = $ticket ? $ticket->email : "";

			$user = User::where("username", $email)->first();

			if (!$user || ($user && !Hash::check($ticketId . "EVBTMO", $token)))
			{
				return redirect(route("track.login"));
			}

			Auth::logout();
			Auth::loginUsingId($user->id, true);

		}

		$ref = request("ref");

		if ($ref != "")
		{
			return redirect(route("track.orders.all") . "?ref=" . $ref);
		}
		else
		{
			return redirect(route("track.orders.all"));
		}
	}

	public function showActiveOrders()
	{
		// User Authentication
		$user = Auth::user();

		if ($user)
		{
			$phone = $user->phone > 1000000000 ? $user->phone : "NOTVALID";
			$allUpcomingEventTickets = Ticket::select("*", DB::raw('FROM_UNIXTIME(event_date, \'%y-%m-%d\') as eventDate'))
			                                 ->whereIn('ticket.status_id', [config("evibe.ticket.status.booked"), config("evibe.ticket.status.auto_paid"), config("evibe.ticket.status.service_auto_pay")])
			                                 ->where(function ($query) use ($user, $phone) {
				                                 $query->where('email', $user->username)
				                                       ->orWhere('phone', 'LIKE', "%$phone%")
				                                       ->orWhere('alt_phone', 'LIKE', "%$phone%");
			                                 })
			                                 ->whereNotNull('paid_at')
			                                 ->where('event_date', ">=", Carbon::now()->startOfDay()->timestamp)
			                                 ->whereNull('deleted_at')
			                                 ->orderBy("paid_at", "DESC")
			                                 ->get();

			if ($allUpcomingEventTickets->count() > 0)
			{
				$mergeTickets = [];

				foreach ($allUpcomingEventTickets as $allUpcomingEventTicket)
				{
					$mergeTickets[$allUpcomingEventTicket->eventDate . "-" . $allUpcomingEventTicket->phone][$allUpcomingEventTicket->id] = $allUpcomingEventTicket;
					$mergeTickets[$allUpcomingEventTicket->eventDate . "-" . $allUpcomingEventTicket->phone]["enquiryId"] = $allUpcomingEventTicket->enquiry_id;
					$mergeTickets[$allUpcomingEventTicket->eventDate . "-" . $allUpcomingEventTicket->phone]["statusIds"][] = $allUpcomingEventTicket->status_id;
					$mergeTickets[$allUpcomingEventTicket->eventDate . "-" . $allUpcomingEventTicket->phone]["ticketIds"][] = $allUpcomingEventTicket->id;
				}

				foreach ($mergeTickets as $key => $mergeTicket)
				{
					$ticketBookings = TicketBooking::whereIn("ticket_id", array_keys($mergeTicket))
					                               ->whereNull("deleted_at")
					                               ->orderBy("is_venue_booking")
					                               ->get();

					$mergeTickets[$key]["bookings"] = $ticketBookings;

					$ticketBookingIds = $ticketBookings->pluck("id")->toArray();
					$mergeTickets[$key]["referenceImages"] = TicketBookingGallery::whereIn("ticket_booking_id", array_unique($ticketBookingIds))
					                                                             ->whereNull("deleted_at")
					                                                             ->get();

					foreach ($ticketBookings as $ticketBooking)
					{
						if ($ticketBooking->mapping && ($ticketBooking->mapping->map_type_id == config('evibe.ticket.type.villas')))
						{
							$mergeTickets[$key]["needProof"] = true;
						}
					}

					$mergeTickets[$key]["customerProofs"] = CustomerOrderProof::whereIn("ticket_id", array_keys($mergeTicket))
					                                                          ->whereNull('deleted_at')
					                                                          ->get();

					// fetch add-ons based on the bookings
					$mergeTicketAddOns = [];
					foreach ($ticketBookings as $ticketBooking)
					{
						$ticket = $ticketBooking->ticket;
						$mapping = $ticketBooking->mapping;
						$addOns = $this->fetchAddOns($ticket->city_id, $ticket->event_id, $mapping->map_type_id, $mapping->map_id, $ticketBooking);
						$mergeTicketAddOns = array_merge($mergeTicketAddOns, $addOns);
					}
					$mergeTickets[$key]["addOns"] = collect($mergeTicketAddOns)->unique();
				}

				$data = [
					"mergeTickets" => $mergeTickets,
					"typeProofs"   => TypeProof::all()
				];
			}
			else
			{
				$data = [
					"mergeTickets" => [],
					"typeProofs"   => [],
				];
			}

			$productBookings = ProductBooking::with('status')
			                                 ->where('user_id', $user->id)
			                                 ->whereNotNull('paid_at')
			                                 ->whereNull('deleted_at')
			                                 ->get();

			if (count($productBookings))
			{
				$data["productBookings"] = $productBookings;
			}
			else
			{
				$data["productBookings"] = [];
			}

			return view('login.track-my-order.list.d', ["data" => $data]);
		}
		else
		{
			return redirect(route("track.login"));
		}
	}

	public function showLoginScreen()
	{
		if (Auth::user())
		{
			return redirect(route("track.orders"));
		}

		return view("login.track-my-order.login.d");
	}

	public function showOTPScreen($phone)
	{
		$token = request("token");

		if (Hash::check(substr($phone, 5) . substr($phone, 0, 5), $token))
		{
			$data = ["phone" => $phone];

			return view("login.track-my-order.login.otp", ["data" => $data]);
		}

		return redirect()->back();
	}

	public function getOTP()
	{
		$phone = request("phone");

		if ($phone > 1111111111)
		{
			$hashCode = Hash::make(substr($phone, 5) . substr($phone, 0, 5));

			$tmo = TMO::updateOrCreate([
				                           "phone"      => (int)$phone,
				                           "ip_address" => $_SERVER['REMOTE_ADDR'] == "" ? "0.0.0.0" : $_SERVER['REMOTE_ADDR']
			                           ],
			                           [
				                           "otp"       => rand(111111, 999999),
				                           "otp_count" => 0,
			                           ]);

			if ($tmo)
			{
				$this->dispatch(new SendOTPToCustomer(["phone" => $phone, "otp" => $tmo->otp]));

				return response()->json([
					                        "success"  => true,
					                        "hashCode" => $hashCode
				                        ]);
			}
		}

		return response()->json([
			                        "success" => false
		                        ]);
	}

	public function submitOTP()
	{
		$phone = request("phone");
		$otp = request("otp");
		$ipAddress = $_SERVER['REMOTE_ADDR'] == "" ? "0.0.0.0" : $_SERVER['REMOTE_ADDR'];

		$tmo = TMO::where("phone", $phone)
		          ->where("ip_address", $ipAddress)
		          ->first();

		if ($tmo)
		{
			if ($tmo->otp_count >= 5)
			{
				return response()->json([
					                        "success" => true,
					                        "error"   => "Max OTP limit exceeded, Please try again after sometime."
				                        ]);
			}
			else
			{
				if ($tmo->otp == $otp)
				{
					$user = User::where("phone", $phone)
					            ->first();

					if ($user)
					{
						Auth::logout();
						Auth::loginUsingId($user->id, true);

						return response()->json([
							                        "success"     => true,
							                        "redirectUrl" => route("track.orders")
						                        ]);
					}
					else
					{
						$productBookings = ProductBooking::where('phone', $phone)
						                                 ->whereNull('deleted_at')
						                                 ->whereNotNull('paid_at')
						                                 ->get();

						$ticket = Ticket::where("status_id", config("evibe.ticket.status.booked"))
						                ->where(function ($query) use ($phone) {
							                $query->where("phone", $phone)
							                      ->orWhere("alt_phone", $phone);
						                })->where("event_date", ">", time())
						                ->first();

						if ($ticket)
						{
							$existingUser = User::where('username', $ticket->email)
							                    ->orWhere('phone', $ticket->phone)
							                    ->first();

							if ($existingUser)
							{
								Auth::logout();
								Auth::loginUsingId($existingUser->id, true);

								return response()->json([
									                        "success"     => true,
									                        "redirectUrl" => route("track.orders")
								                        ]);
							}
							else
							{
								$newUser = User::create([
									                        'username'   => $ticket->email,
									                        'password'   => bcrypt($ticket->phone),
									                        'phone'      => $ticket->phone,
									                        'name'       => $ticket->name,
									                        'role_id'    => config('evibe.roles.customer'),
									                        'created_at' => Carbon::now(),
									                        'updated_at' => Carbon::now()
								                        ]);

								if ($newUser)
								{
									Auth::logout();
									Auth::loginUsingId($newUser->id, true);

									return response()->json([
										                        "success"     => true,
										                        "redirectUrl" => route("track.orders")
									                        ]);
								}
							}
						}

						if (count($productBookings) > 0)
						{
							return response()->json([
								                        "success"     => true,
								                        "redirectUrl" => route("track.orders")
							                        ]);
						}

						return response()->json([
							                        "success" => true,
							                        "error"   => "No Valid Upcoming Bookings found with your phone number."
						                        ]);
					}
				}
				else
				{
					$tmo->update(["otp_count" => $tmo->otp_count + 1]);

					return response()->json([
						                        "success" => true,
						                        "error"   => "The OTP entered is incorrect."
					                        ]);
				}
			}
		}

		return response()->json([
			                        "success" => false
		                        ]);
	}

	public function saveRequest($ticketId)
	{
		$user = Auth::user();
		$ticket = Ticket::find($ticketId);

		if ($user && $ticket && (str_replace(' ', '', strtolower($user->username)) == str_replace(' ', '', strtolower($ticket->email)) || $user->phone == $ticket->phone || $user->phone == $ticket->alt_phone))
		{
			$requestType = request("requestType");
			$requestReason = request("requestReason");

			$emailData = [
				"ticketId"       => $ticketId,
				"requestType"    => $requestType,
				"requestReason"  => $requestReason,
				"customerName"   => $ticket->name,
				"customerEmail"  => $ticket->email,
				"userDeviceInfo" => $_SERVER['HTTP_USER_AGENT'] . " --- " . $_SERVER['HTTP_REFERER'] . " --- " . $_SERVER['REMOTE_ADDR'] . " --- UserId:" . $user->id . " --- ticketId:" . $ticket->id
			];

			TicketPostBookingRequests::create([
				                                  "ticket_id" => $ticketId,
				                                  "request"   => $requestType,
				                                  "comments"  => $requestReason,
				                                  "user_id"   => $user->id
			                                  ]);

			$this->dispatch(new MailTicketUpdateRequestToTeamJob($emailData));

			return response()->json([
				                        "success" => true,
			                        ]);
		}

		return response()->json([
			                        "success" => false,
			                        "error"   => "Error while submitting your request, Please try again."
		                        ]);
	}

	public function getABProfileImages()
	{
		if (Auth::user())
		{
			$bookingIds = request("bookingIds");
			$profileImages = [];

			if (!is_null($bookingIds) && count($bookingIds) > 0)
			{
				foreach ($bookingIds as $bookingId)
				{
					$booking = TicketBooking::find($bookingId);
					if ($booking)
					{
						$mapping = $booking->mapping;
						if ($mapping)
						{
							$mapTypeId = $mapping->map_type_id;
							$mapId = $mapping->map_id;
							$profileImage = "";
							switch ($mapTypeId)
							{
								// packages
								case config('evibe.ticket.type.package'):
								case config('evibe.ticket.type.food'):
								case config('evibe.ticket.type.villas'):
								case config('evibe.ticket.type.resorts'):
								case config('evibe.ticket.type.priests'):
								case config('evibe.ticket.type.lounges'):
								case config('evibe.ticket.type.tents'):
								case config('evibe.ticket.type.surprises'):
								case config('evibe.ticket.type.generic-package'):
								case config('evibe.ticket.type.venue-deals'):
									$mappers = Package::withTrashed()->find($mapId);
									$profileImage = $mappers ? $mappers->getProfileImg() : "";
									break;

								// venues (aka halls)
								case config('evibe.ticket.type.venue'):
									$venueHalls = VenueHall::withTrashed()->with('venue');
									$mappers = $venueHalls->find($mapId);
									$profileImage = $mappers ? $mappers->getProfileImg() : "";
									break;

								// services
								case config('evibe.ticket.type.service'):
									$mappers = TypeServices::withTrashed()->find($mapId);
									$profileImage = $mappers ? $mappers->getProfilePic() : "";
									break;

								// trends
								case config('evibe.ticket.type.trend'):
									$mappers = Trend::withTrashed()->find($mapId);
									$profileImage = $mappers ? $mappers->getProfileImg() : "";
									break;

								// cakes
								case config('evibe.ticket.type.cake'):
									$mappers = Cake::withTrashed()->find($mapId);
									$profileImage = $mappers ? $mappers->getProfileImg() : "";
									break;

								// decors
								case config('evibe.ticket.type.decor'):
									$mappers = Decor::withTrashed()->find($mapId);
									$profileImage = $mappers ? $mappers->getProfileImg() : "";
									break;
							}

							$profileImages[$bookingId] = $profileImage;
						}
					}
				}
			}

			return response()->json([
				                        "success"       => true,
				                        "profileImages" => $profileImages
			                        ]);
		}

		return response()->json([
			                        "success" => false,
			                        "error"   => "Error while submitting your request, Please try again."
		                        ]);
	}

	public function getCheckoutFields()
	{
		if (Auth::user())
		{
			$bookingIds = request("bookingIds");
			$checkoutFieldsForBookings = [];

			if (count($bookingIds) > 0)
			{
				foreach ($bookingIds as $bookingId)
				{
					$booking = TicketBooking::find($bookingId);
					if ($booking)
					{
						$mapping = $booking->mapping;
						if ($mapping)
						{
							$ticket = $booking->ticket;
							$ticketEvent = $ticket->event_id;
							$ticketEvent = $this->getParentEventId($ticketEvent);

							$typeTicketBookingId = $booking->type_ticket_booking_id;

							$checkoutFields = CheckoutField::where('type_ticket_booking_id', $typeTicketBookingId)
							                               ->where(function ($query) use ($ticketEvent) {
								                               $query->where('event_id', $ticketEvent)
								                                     ->orWhereNull('event_id');
							                               })
							                               ->get();

							if ($ticket->is_auto_booked == 1)
							{
								$checkoutFields = $checkoutFields->where('is_auto_booking', 1);
							}
							else
							{
								$checkoutFields = $checkoutFields->where('is_auto_booking', 0);
							}

							if (count($checkoutFields))
							{
								foreach ($checkoutFields as $checkoutField)
								{
									$checkoutFieldValue = $checkoutField->getCheckoutValue($booking->ticket_id);
									$notApplicable = $checkoutField->is_crm && strtolower($checkoutFieldValue) == "n/a" ? true : false;

									if (!$notApplicable)
									{
										$checkoutFieldsForBookings[$bookingId][$checkoutField->identifier] = $checkoutFieldValue;
									}
								}
							}

							if ($booking->spl_notes != "")
							{
								$checkoutFieldsForBookings[$bookingId]["Special Notes"] = $booking->spl_notes;
							}
						}
					}
				}
			}

			return response()->json([
				                        "success"        => true,
				                        "checkoutFields" => $checkoutFieldsForBookings
			                        ]);
		}

		return response()->json([
			                        "success" => false,
			                        "error"   => "Error while submitting your request, Please try again."
		                        ]);
	}

	public function uploadCustomerProofs()
	{
		$user = Auth::user();

		if ($user)
		{
			$customerProofFront = Input::file('customerProofFront');
			$customerProofBack = Input::file('customerProofBack');
			$proofType = Input::get('proofType');

			if (!$proofType)
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'Kindly select an id proof to upload'
				                        ]);
			}

			if (!$customerProofFront)
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'Kindly upload front side of the proof'
				                        ]);
			}

			if (!$customerProofBack)
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'Kindly upload back side of the proof'
				                        ]);
			}

			$proofRules = [
				'file' => 'required|mimes:png,jpeg,jpg,JPG|max:4096'
			];

			$proofMessages = [
				'file.required' => 'Please upload at least one image',
				'file.mimes'    => 'One of the images is not valid. (only .png, .jpeg, .jpg are accepted)',
				'file.max'      => 'Image size cannot be more than 4 MB'
			];

			// validation for proof front side
			$fileValidator = Validator::make(['file' => $customerProofFront], $proofRules, $proofMessages);

			if ($fileValidator->fails())
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => $fileValidator->messages()->first()
				                        ]);
			}

			// validation for proof back side
			$fileValidator = Validator::make(['file' => $customerProofBack], $proofRules, $proofMessages);

			if ($fileValidator->fails())
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => $fileValidator->messages()->first()
				                        ]);
			}

			$files = [
				$customerProofFront,
				$customerProofBack
			];

			$phone = $user->phone > 1000000000 ? $user->phone : "NOTVALID";
			$allUpcomingEventTickets = Ticket::select("*", DB::raw('FROM_UNIXTIME(event_date, \'%y-%m-%d\') as eventDate'))
			                                 ->whereIn('ticket.status_id', [config("evibe.ticket.status.booked"), config("evibe.ticket.status.auto_paid"), config("evibe.ticket.status.service_auto_pay")])
			                                 ->where(function ($query) use ($user, $phone) {
				                                 $query->where('email', $user->username)
				                                       ->orWhere('phone', 'LIKE', "%$phone%")
				                                       ->orWhere('alt_phone', 'LIKE', "%$phone%");
			                                 })
			                                 ->whereNotNull('paid_at')
			                                 ->where('event_date', ">=", Carbon::now()->startOfDay()->timestamp)
			                                 ->whereNull('deleted_at')
			                                 ->orderBy("paid_at", "DESC")
			                                 ->get();

			foreach ($allUpcomingEventTickets as $eventTicket)
			{
				$ticketMappings = $eventTicket->mappings;
				if (count($ticketMappings))
				{
					foreach ($ticketMappings as $ticketMapping)
					{
						if ($ticketMapping->map_type_id == config('evibe.ticket.type.villas'))
						{
							try
							{
								// remove any existing proofs
								CustomerOrderProof::where('ticket_id', $eventTicket->id)
								                  ->update([
									                           'updated_at' => Carbon::now(),
									                           'deleted_at' => Carbon::now()
								                           ]);

								$directPath = '/ticket/' . $eventTicket->id . '/customer-proofs/';

								foreach ($files as $key => $file)
								{
									$option = ['isWatermark' => false];

									$fileName = $this->uploadImageToServer($file, $directPath, $option);

									$imgInsertData = [
										'ticket_id'     => $eventTicket->id,
										'type_proof_id' => $proofType,
										'url'           => $fileName,
										'title'         => $this->getImageTitle($file),
										'face'          => $key + 1,
										'created_at'    => Carbon::now(),
										'updated_at'    => Carbon::now()
									];

									$existingProof = CustomerOrderProof::where('ticket_id', $eventTicket->id)
									                                   ->where('type_proof_id', $proofType)
									                                   ->where('face', $key + 1)
									                                   ->whereNull('deleted_at')
									                                   ->whereNull('approved_at')
									                                   ->whereNull('rejected_at')
									                                   ->first();

									if ($existingProof)
									{
										$existingProof->url = $fileName;
										$existingProof->title = $this->getImageTitle($file);
										$existingProof->updated_at = Carbon::now();
										$existingProof->save();
									}
									else
									{
										CustomerOrderProof::create($imgInsertData);
									}
								}

								// alert team
								// alert customer

								$tmoToken = Hash::make($eventTicket->id . "EVBTMO");
								$tmoLink = $this->getShortenUrl(route("track.orders") . "?ref=SMS&id=" . $eventTicket->id . "&token=" . $tmoToken);

								$data = [
									'customerName'  => $eventTicket->name,
									'customerEmail' => $eventTicket->email,
									'customerPhone' => $eventTicket->phone,
									'partyDate'     => date('d M Y', $eventTicket->event_date),
									'ticketId'      => $eventTicket->id,
									'dashLink'      => config('evibe.dash_host') . '/ops-dashboard/customer-proofs',
									'tmoLink'       => $tmoLink
								];

								$tplCustomer = config('evibe.sms_tpl.customer_proof.upload.customer');
								$replaces = [
									'#customer#' => $eventTicket->name,
									'#tmoLink#'  => $tmoLink
								];

								$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);
								$smsData = [
									'to'   => $eventTicket->phone,
									'text' => $smsText
								];

								$this->dispatch(new MailCustomerProofUploadAlertToTeam($data));

								if ($data['customerEmail'])
								{
									$this->dispatch(new MailCustomerProofUploadAlertToCustomer($data));
								}

								if ($data['customerPhone'])
								{
									$this->dispatch(new SMSCustomerProofUploadToCustomer($smsData));
								}

								return response()->json([
									                        'success' => true,
								                        ]);
							} catch (\Exception $exception)
							{
								Log::error('Error occurred while saving images: ' . $exception->getMessage());
							}
						}
					}
				}
			}

			return response()->json([
				                        'success' => true,
			                        ]);
		}
		else
		{
			Log::error("Unable to find user");

			return response()->json([
				                        'success' => false
			                        ]);
		}
	}

	public function fetchCancellationCharges($ticketId)
	{
		$res = [
			'success'             => false,
			'bookingAmount'       => 0,
			'advanceAmount'       => 0,
			'refundAmount'        => 0,
			'cancellationCharges' => 0
		];

		$ticketBooking = TicketBooking::where("ticket_id", $ticketId)
		                              ->whereNull("deleted_at")
		                              ->get();

		$user = Auth::user();

		if (!$ticketBooking || !$user)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'Unable to find ticket booking to cancel'
			                        ]);
		}

		foreach ($ticketBooking as $booking)
		{
			// make API call to fetch partner adjustments list
			$accessToken = $this->getAccessToken('', $user->id);
			$url = config('evibe.api.base_url') . config('evibe.api.finance.cancellations.prefix') . '/' . $booking->id . '/calculate';

			// isCustomer value is hardcoded as 0 since it is requested by the user, 0 if it is cancelled by evibe
			$options = [
				'method'      => "GET",
				'url'         => $url,
				'accessToken' => $accessToken,
				'jsonData'    => [
					'isCustomer' => 1
				]
			];

			$response = $this->makeApiCallWithUrl($options);

			if ($response["success"])
			{
				$res = [
					'success'             => true,
					'cancellationCharges' => $res['cancellationCharges'] + $response['evibeFee'] + $response['partnerFee'],
					'refundAmount'        => $res['refundAmount'] + $response['refundAmount'],
					'bookingAmount'       => $res['bookingAmount'] + $response['bookingAmount'],
					'advanceAmount'       => $res['advanceAmount'] + $response['advanceAmount']
				];
			}
		}

		return response()->json($res);
	}
}