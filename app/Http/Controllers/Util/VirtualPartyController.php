<?php

namespace App\Http\Controllers\Util;

use App\Models\VirtualParty\VirtualParty;
use App\Models\VirtualParty\VirtualPartyGuest;
use App\Models\VirtualParty\VirtualPartyGuestWish;
use App\Http\Controllers\Base\BaseController;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;


class VirtualPartyController extends BaseController
{
	public function showPartyHomePage($pageUrl)
	{
		$pageDetails = VirtualParty::where('url', $pageUrl)
		                           ->first();
		if ($pageDetails)
		{
			$cacheKey = $this->deviceSpecificCacheKey(request()->fullUrl());
			$cacheKey = base64_encode($cacheKey);
			if (Cache::has($cacheKey))
			{
				$data = Cache::get($cacheKey);

				return view("virtual-party.base")->with(['data' => $data]);
			}

			$pageId = $pageDetails['id'];
			$seoTitle = $pageDetails['celebrity_text'] . "'s " . $pageDetails['event'] . " at Evibe's Virtual Party";
			$seoDesc = "Join " . $pageDetails['celebrity_text'] . "'s " . $pageDetails['event'] . " at Evibe's Virtual Party";
			$celebNameArr = explode(" ", $pageDetails['celebrity_text']);
			$celebrityName = "";
			foreach($celebNameArr as $shortName)
			{
				if(strlen($shortName)>2)
				{
					$celebrityName = $shortName;
					break;
				}


			}
			$pageDetails['celebrity_text'] = $celebrityName;

			$data = [
				'pageDetails' => $pageDetails,
				'seoTitle'    => $seoTitle,
				'seoDesc'     => $seoDesc,

			];
			Cache::put($cacheKey, $data, config('evibe.cache.refresh-time'));

			return view("virtual-party.base")->with(['data' => $data]);

		}
		else
		{
			// Redirect to homepage
			return redirect('/');
		}

	}
	public function fetchFeed()
	{

		$pageId = Input::get('pageId');
		$celebName = Input::get('celebName');
		
		$guestIds = VirtualPartyGuest::where('vp_id', $pageId)->pluck('id');
		$feedInfo = VirtualPartyGuestWish::whereIn('vp_guest_id', $guestIds)->orderBy('created_at', 'DESC')->get();
		$data = [
			'feed' => $feedInfo,
			'guests' => $guestIds,
			'pageId' => $pageId,
			'celebName' => $celebName
		];
		return view("virtual-party.feed")->with(['data' => $data]);
	}

	public function validateWishes()
	{
		try
		{

			// upload is not mandatory
			// if uploaded, need to validate

			$rules = [
				'wishComments' => 'required|min:4',
			];

			$message = [
				'wishComments.required' => 'Please enter your wish message',
				'wishComments.min'      => 'Wish message cannot be less than 4 character',
			];

			$validator = validator(Input::all(), $rules, $message);
			if ($validator->fails())
			{
				return response()->json(['success' => false, 'errorMsg' => $validator->messages()->first()]);
			}

			$wishComments = Input::get('wishComments');
			$uploadType = Input::get('uploadType');
			if ($uploadType == 1)
			{
				$wishCard = Input::file('wishCard');
				//if($wishCard)
				//{
				//	Log::info("validate only if exists");
				//}
				//else
				//{
				//	Log::info("or ..");
				//}

				// validate image
				$imageRules = [
					'file' => 'required|mimes:png,jpeg,jpg,JPG|max:5120'
				];

				$imageMessages = [
					'file.required' => 'Please upload a wish card',
					'file.mimes'    => 'Image is not valid. (only .png, .jpeg, .jpg are accepted)',
					'file.max'      => 'Image size cannot be more than 5 MB'
				];

				// validation for proof front side
				$fileValidator = Validator::make(['file' => $wishCard], $imageRules, $imageMessages);

				if ($fileValidator->fails())
				{
					return response()->json([
						                        'success'  => false,
						                        'errorMsg' => $fileValidator->messages()->first()
					                        ]);
				}
			}
			elseif ($uploadType == 2)
			{
				$wishVideo = Input::file('wishVideo');

				// validate video
				$imageRules = [
					'file' => 'mimetypes:video/avi,video/mpeg,video/mp4,video/quicktime|max:102400'
				];

				$imageMessages = [
					'file.required'  => 'Please upload a wish video',
					'file.mimetypes' => 'Video is not valid. (only .avi, .mpeg, .mp4 are accepted)',
					'file.max'       => 'Video size cannot be more than 100 MB'
				];

				// validation for proof front side
				$fileValidator = Validator::make(['file' => $wishVideo], $imageRules, $imageMessages);

				if ($fileValidator->fails())
				{
					return response()->json([
						                        'success'  => false,
						                        'errorMsg' => $fileValidator->messages()->first()
					                        ]);
				}
			}

			return response()->json([
				                        'success' => true
			                        ]);

		} catch (\Exception $exception)
		{
			$this->sendErrorReportToTeam($exception);

			return response()->json([
				                        'success'  => false,
				                        'errorMsg' => $exception->getMessage()
			                        ]);
		}
	}

	public function sendWishes()
	{
		try
		{

			// primary validation
			$rules = [
				'sendWishesName'         => 'required|min:3',
				'sendWishesEmail'        => 'required|email',
				//'sendWishesRelationship' => 'required'
			];

			$message = [
				'sendWishesName.required'         => 'Please enter your name',
				'sendWishesName.min'              => 'Name cannot be less than 3 character',
				'sendWishesEmail.required'        => 'Please enter your email',
				'sendWishesEmail.email'           => 'Please enter a valid email',
				'sendWishesRelationship.required' => 'Please enter your relationship to the celebrity',
			];

			$validator = validator(request()->all(), $rules, $message);
			if ($validator->fails())
			{
				return response()->json(['success' => false, 'errorMsg' => $validator->messages()->first()]);
			}

			// secondary validation - repeat - try to optimise
			$rules = [
				'wishComments' => 'required|min:4',
			];

			$message = [
				'wishComments.required' => 'Please enter your wish message',
				'wishComments.min'      => 'Wish message cannot be less than 4 character',
			];

			$validator = validator(Input::all(), $rules, $message);
			if ($validator->fails())
			{
				return response()->json(['success' => false, 'errorMsg' => $validator->messages()->first() . '. Please refresh the page and try again']);
			}

			$wishComments = Input::get('wishComments');
			$uploadType = Input::get('uploadType');
			$wishCard = null;
			$wishVideo = null;
			if ($uploadType == 1)
			{
				$wishCard = Input::file('wishCard');

				// validate image
				$imageRules = [
					'file' => 'required|mimes:png,jpeg,jpg,JPG|max:5120'
				];

				$imageMessages = [
					'file.required' => 'Please upload a wish card',
					'file.mimes'    => 'One of the images is not valid. (only .png, .jpeg, .jpg are accepted)',
					'file.max'      => 'Image size cannot be more than 5 MB'
				];

				// validation for proof front side
				$fileValidator = Validator::make(['file' => $wishCard], $imageRules, $imageMessages);

				if ($fileValidator->fails())
				{
					return response()->json([
						                        'success'  => false,
						                        'errorMsg' => $fileValidator->messages()->first() . '. Please refresh the page and try again.'
					                        ]);
				}
			}
			elseif ($uploadType == 2)
			{
				$wishVideo = Input::file('wishVideo');

				// validate video
				$imageRules = [
					'file' => 'mimetypes:video/avi,video/mpeg,video/mp4,video/quicktime|max:102400'
				];

				$imageMessages = [
					'file.required'  => 'Please upload a wish video',
					'file.mimetypes' => 'Video is not valid. (only .avi, .mpeg, .mp4 are accepted)',
					'file.max'       => 'Video size cannot be more than 100 MB'
				];

				// validation for proof front side
				$fileValidator = Validator::make(['file' => $wishVideo], $imageRules, $imageMessages);

				if ($fileValidator->fails())
				{
					return response()->json([
						                        'success'  => false,
						                        'errorMsg' => $fileValidator->messages()->first()
					                        ]);
				}
			}

			// save in DB
			$vpId = Input::get('vpId');

			// check if guest exists or create a new guest
			$sendWishesEmail = Input::get('sendWishesEmail');
			$virtualPartyGuest = VirtualPartyGuest::where('vp_id', $vpId)
			                                      ->where('email', $sendWishesEmail)
			                                      ->first();
			if (!$virtualPartyGuest)
			{
				$virtualPartyGuest = VirtualPartyGuest::create([
					                                               'vp_id'        => $vpId,
					                                               'name'         => Input::get('sendWishesName'),
					                                               'email'        => $sendWishesEmail,
					                                               //'relationship' => Input::get('sendWishesRelationship'),
					                                               'created_at'   => Carbon::now(),
					                                               'updated_at'   => Carbon::now()
				                                               ]);
			}

			$sendWishesRSVP = Input::get('sendWishesRSVP');
			if ($sendWishesRSVP && ($sendWishesRSVP == 'No'))
			{
				$virtualPartyGuest->rsvp = $sendWishesRSVP;
				$virtualPartyGuest->updated_at = Carbon::now();
				$virtualPartyGuest->save();
			}

			// save wish details
			$vpGuestWishDetails = [
				'vp_guest_id' => $virtualPartyGuest->id,
				'wish_text'   => utf8_encode($wishComments), // @see: to support emoji
				'upload_type' => $uploadType,
				'created_at'  => Carbon::now(),
				'updated_at'  => Carbon::now()
			];

			if ($wishCard)
			{
				$directPath = '/virtual-party/' . $vpId . '/wishes/' . $virtualPartyGuest->id . '/';
				$option = ['isWatermark' => false, 'isOptimize' => false];
				$fileName = $this->uploadImageToServer($wishCard, $directPath, $option);
				$vpGuestWishDetails['upload_url'] = $fileName;
			}
			elseif ($wishVideo)
			{
				$directPath = '/virtual-party/' . $vpId . '/wishes/' . $virtualPartyGuest->id . '/';
				$option = ['isWatermark' => false, 'isOptimize' => false];
				$fileName = $this->uploadVideoToServer($wishVideo, $directPath);
				$vpGuestWishDetails['upload_url'] = $fileName;
			}

			$virtualPartyGuestWish = VirtualPartyGuestWish::create($vpGuestWishDetails);

			return response()->json([
				                        'success' => true
			                        ]);

		} catch (\Exception $exception)
		{
			$this->sendErrorReportToTeam($exception);

			return response()->json([
				                        'success'  => false,
				                        'errorMsg' => $exception->getMessage()
			                        ]);
		}
	}

	public function submitRSVP()
	{
		try
		{

			// primary validation
			$rules = [
				'acceptRSVPName'         => 'required|min:3',
				'acceptRSVPEmail'        => 'required|email',
				//'acceptRSVPRelationship' => 'required'
			];

			$message = [
				'acceptRSVPName.required'         => 'Please enter your name',
				'acceptRSVPName.min'              => 'Name cannot be less than 3 character',
				'acceptRSVPEmail.required'        => 'Please enter your email',
				'acceptRSVPEmail.email'           => 'Please enter a valid email',
				'acceptRSVPRelationship.required' => 'Please enter your relationship to the celebrity',
			];

			$validator = validator(request()->all(), $rules, $message);
			if ($validator->fails())
			{
				return response()->json(['success' => false, 'errorMsg' => $validator->messages()->first()]);
			}

			$acceptRSVPName = Input::get('acceptRSVPName');
			$acceptRSVPEmail = Input::get('acceptRSVPEmail');
			//$acceptRSVPRelationship = Input::get('acceptRSVPRelationship');

			$vpId = Input::get('vpId');

			// check if guest exists or create a new guest
			$virtualPartyGuest = VirtualPartyGuest::where('vp_id', $vpId)
			                                      ->where('email', $acceptRSVPEmail)
			                                      ->first();
			if (!$virtualPartyGuest)
			{
				$virtualPartyGuest = VirtualPartyGuest::create([
					                                               'vp_id'        => $vpId,
					                                               'name'         => $acceptRSVPName,
					                                               'email'        => $acceptRSVPEmail,
					                                               //'relationship' => $acceptRSVPRelationship,
					                                               'created_at'   => Carbon::now(),
					                                               'updated_at'   => Carbon::now()
				                                               ]);
			}

			$virtualPartyGuest->rsvp = 'Yes';
			$virtualPartyGuest->updated_at = Carbon::now();
			$virtualPartyGuest->save();

			return response()->json([
				                        'success' => true
			                        ]);

		} catch (\Exception $exception)
		{
			$this->sendErrorReportToTeam($exception);

			return response()->json([
				                        'success'  => false,
				                        'errorMsg' => $exception->getMessage()
			                        ]);
		}
	}
}
