<?php

namespace App\Http\Controllers\Partner;

use App\Http\Models\Partner\DeliveryGallery;
use App\Models\Ticket\TicketBooking;
use App\Models\Ticket\TicketUpdate;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class PartnerBookingsController extends BasePartnerController
{
	public function showPartnerDigest()
	{
		$isLastWeek = 0;
		$user = Auth::user();
		$partnerData = $this->getPartnerByUserId($user->id);
		$provider = $partnerData['partner'];

		if (!$provider)
		{
			return redirect(route("missing"));
		}

		$startDay = request('from');
		$endDay = request('to');

		if (!$startDay && !$endDay)
		{
			$startDay = Carbon::today()->startOfWeek()->subWeek(1)->getTimestamp(); // last week monday
			$endDay = Carbon::today()->startOfWeek()->subWeek(1)->endOfWeek()->getTimestamp(); // last week sunday
			$isLastWeek = 1;
		}
		$accessToken = Hash::make($startDay . $endDay);

		$url = config('evibe.api.base_url') . "partner-digest/" . $provider['id'] . "/" . $partnerData['mapTypeId'] . "?token=" . $accessToken . "&from=" . $startDay . "&to=" . $endDay;

		$options = [
			'method'      => "POST",
			'url'         => $url,
			'accessToken' => "",
			'jsonData'    => "",
		];

		$res = $this->makeApiCallWithUrl($options);

		$data = [
			'user'                  => $user,
			'response'              => $res,
			'provider'              => $provider,
			'isLastWeek'            => $isLastWeek,
			'dateRange'             => date("d/m/Y", $startDay) . " to " . date("d/m/Y", $endDay),
			'humanReadableStartDay' => date("jS M Y", $startDay),
			'humanReadableEndDay'   => date("jS M Y", $endDay),
			'pageName'              => "dashboard"
		];

		if (request('isAjax') == 1)
		{
			return view("partner.dashboard.new-orders", ['data' => $data]);
		}

		if (!is_null($res) && $res['success'])
		{
			return view("partner.dashboard.home", ['data' => $data]);
		}
		elseif (!$res['success'])
		{
			return view("partner.dashboard.no-bookings", ['data' => $data]);
		}
		else
		{
			return redirect()->back();
		}
	}

	public function addDashboardFilter()
	{
		$startDate = strtotime(request('startDate'));
		$endDate = strtotime(request('endDate'));

		return response()->json([
			                        'success'     => true,
			                        'redirectUrl' => route("partner.dash.dashboard") . "?from=" . $startDate . "&to=" . $endDate
		                        ]);
	}

	public function showEnquiries()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.partner.prefix') . 'enquiries';

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => ""
		];

		$res = $this->makeApiCallWithUrl($options);

		$data = [
			'user'     => $user,
			'pageName' => "enquiries"
		];

		if ($res)
		{
			$data['res'] = $res;

			return view("partner.bookings.enquiries", ["data" => $data]);
		}
		else
		{
			return view("partner.missing", ["data" => $data]);
		}
	}

	public function sendEnquiryReply($enquiryId)
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.partner.prefix') . 'enquiries/' . $enquiryId . "/reply";

		$options = [
			'method'      => "POST",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				'typeId'        => request('typeId'),
				'replyDecision' => request('replyDecision'),
				'replyText'     => request('replyText')
			]
		];

		$res = $this->makeApiCallWithUrl($options);

		$data = ['user' => $user];

		if ($res)
		{
			$data['res'] = $res;

			return response()->json([
				                        'success'        => true,
				                        'successMessage' => "Enquiry response has been submitted"
			                        ]);
		}
		else
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => "Some error occured while submitting the response, please reload the page."]);
		}
	}

	public function showOrders()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.partner.prefix') . 'orders';

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => ""
		];

		$res = $this->makeApiCallWithUrl($options);

		$data = [
			'user'     => $user,
			'pageName' => "orders"
		];

		if ($res)
		{
			$data['res'] = $res;

			return view("partner.bookings.orders", ["data" => $data]);
		}
		else
		{
			return view("partner.missing", ["data" => $data]);
		}
	}

	public function showOrderProfile($orderId)
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.partner.prefix') . 'orders/' . $orderId;

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => ""
		];

		$res = $this->makeApiCallWithUrl($options);

		$data = [
			'user'     => $user,
			'pageName' => "orders-profile"
		];

		if ($res)
		{
			$data['res'] = $res;

			return view("partner.bookings.orderDetails", ["data" => $data]);
		}
		else
		{
			return redirect("partner.dash.orders");
		}
	}

	public function showDeliveries()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.partner.prefix') . 'deliveries';

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => ""
		];

		$res = $this->makeApiCallWithUrl($options);

		$data = [
			'user'     => $user,
			'pageName' => "pending-deliveries"
		];

		if (request('isAjax') == 1)
		{
			$data['res'] = $res;

			return view("partner.dashboard.pending-deliveries", ["data" => $data]);
		}

		if ($res)
		{
			$data['res'] = $res;

			return view("partner.bookings.deliveries", ["data" => $data]);
		}
		else
		{
			return view("partner.missing", ["data" => $data]);
		}
	}

	public function showDeliveryProfile($orderId)
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.partner.prefix') . 'deliveries/' . $orderId;

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => ""
		];

		$res = $this->makeApiCallWithUrl($options);

		$data = [
			'user'     => $user,
			'pageName' => "pending-deliveries-profile"
		];
		if ($res)
		{
			$data['res'] = $res;

			return view("partner.bookings.deliveryDetails", ["data" => $data]);
		}
		else
		{
			return redirect("partner.dash.deliveries");
		}
	}

	public function uploadImages($orderId)
	{
		$user = Auth::user();
		$partnerData = $this->getPartnerByUserId($user->id);
		$partner = $partnerData['partner'];

		if ($partner)
		{
			$delivery = TicketBooking::bookedTicket()
			                         ->where([
				                                 'ticket_bookings.id' => $orderId,
				                                 'map_type_id'        => $partnerData['mapTypeId'],
				                                 'map_id'             => $partner->id
			                                 ]);

			$delivery = $delivery->forDeliverable()->first();

			if ($delivery)
			{
				if (is_null(request()->file('attachments')))
				{
					return redirect()->back()->with('errorMsg', "Please select atleast one image to upload");
				}

				$images = request()->file('attachments');

				if (count($images) > 10)
				{
					return redirect()->back()->with('errorMsg', "Some error occurred while uploading images, Please reload the page & try again.");
				}

				// validate the image file
				foreach ($images as $image)
				{
					$validator = validator(["image" => $image], ["image" => "mimes:png,jpg,jpeg"]);

					if ($validator->fails())
					{
						return redirect()->back()->with('errorMsg', $validator->messages()->first());
					}
				}

				$uploadPath = '/ticket/' . $delivery->ticket_id . '/' . $orderId . '/partner/';

				// upload the image file
				$imageCounter = 0;
				try
				{
					foreach ($images as $image)
					{
						$imageName = $this->uploadImageToServer($image, $uploadPath);
						$newDeliveryImage = DeliveryGallery::create([
							                                            'url'               => $imageName,
							                                            'title'             => pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME),
							                                            'type'              => 0,
							                                            'ticket_booking_id' => $orderId
						                                            ]);
						if ($newDeliveryImage)
						{
							$updateDeliveryData = [];
							if (is_null($delivery->delivery_created_at))
							{
								$updateDeliveryData['delivery_created_at'] = Carbon::now();
							}
							$updateDeliveryData['delivery_updated_at'] = Carbon::now();

							$delivery->update($updateDeliveryData);
							$imageCounter++;
						}
					}

					// update ticket action
					if ($imageCounter > 0)
					{
						$updateTicketActionData = [
							'ticket_id'   => $delivery->ticket_id,
							'status_id'   => $delivery->status_id, //  getting from cols
							'handler_id'  => $user->id,
							'comments'    => "$imageCounter delivery image uploaded by $user->name",
							'status_at'   => time(),
							'type_update' => 'Auto'
						];
						TicketUpdate::create($updateTicketActionData);
					}

					return redirect()->back()->with('successMsg', "Images uploaded successfully.");

				} catch (\Exception $e)
				{
					Log::error("image upload exception: " . $e->getMessage());
				}
			}
		}

		return redirect()->back()->with('errorMsg', "Some error occurred while uploading images, Please reload the page & try again.");
	}

	public function deleteDeliveryImage($bookingId, $imageId)
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.partner.prefix') . 'deliveries/' . $bookingId . '/image/' . $imageId;

		$options = [
			'method'      => "DELETE",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => ""
		];

		$res = $this->makeApiCallWithUrl($options);

		if ($res)
		{
			return redirect()->route('partner.dash.delivery.details', $bookingId)->with('successMsg', "Image deleted successfully.");
		}
		else
		{
			return redirect("partner.dash.deliveries");
		}
	}

	public function partnerDeliveryDone($bookingId)
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUserId($user);
		$url = config('evibe.api.base_url') . config('evibe.api.partner.prefix') . 'deliveries/' . $bookingId . '/done';

		$options = [
			'method'      => "POST",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => ""
		];

		$res = $this->makeApiCallWithUrl($options);

		if ($res)
		{
			return redirect()->route('partner.dash.deliveries')->with('successMsg', "Image deleted successfully.");
		}
		else
		{
			return redirect("partner.dash.deliveries");
		}
	}
}