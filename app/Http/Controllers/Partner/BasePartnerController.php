<?php

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\Base\BaseController;
use App\Models\Util\AccessToken;
use App\Models\Util\AuthClient;
use App\Models\Util\SiteErrorLog;
use App\Models\Vendor\Vendor;
use App\Models\Venue\Venue;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;

class BasePartnerController extends BaseController
{
	public function getAccessTokenFromUserId($user)
	{
		// default client id
		$clientId = config('evibe.api.client_id');

		$token = AccessToken::where('user_id', $user->id)
		                    ->where('auth_client_id', $clientId)
		                    ->first();

		if (!$token)
		{
			$client = AuthClient::where('auth_client_id', $clientId)->first();
			if ($client)
			{
				$accessToken = $this->issueAccessToken($user, $client);

				return $accessToken;
			}
			else
			{
				return redirect(route("missing"));
			}
		}
		else
		{
			return $token->access_token;
		}
	}

	public function makeApiCallWithUrl($options)
	{
		$url = $options['url'];
		$method = $options['method'];
		$accessToken = $options['accessToken'];
		$jsonData = $options['jsonData'];

		try
		{
			$client = new Client();
			$res = $client->request($method, $url, [
				'headers' => [
					'access-token' => $accessToken
				],
				'json'    => $jsonData,
			]);

			$res = $res->getBody();
			$res = \GuzzleHttp\json_decode($res, true);

			return $res;

		} catch (ClientException $e)
		{
			$apiResponse = $e->getResponse()->getBody(true);
			$apiResponse = \GuzzleHttp\json_decode($apiResponse);
			$res['error'] = $apiResponse->errorMessage;

			// @todo: send email to tech team
			SiteErrorLog::create([
				                     "url"     => $url,
				                     "code"    => "API_ERROR",
				                     "details" => $res['error']
			                     ]);

			return false;
		}
	}

	public function getPartnerByUserId($userId)
	{
		$planner = Vendor::where('user_id', $userId)->first();
		$data = [
			'partner'   => null,
			'mapTypeId' => null
		];

		if ($planner)
		{
			$data['partner'] = $planner;
			$data['mapTypeId'] = config('evibe.ticket.type.planner');

			return $data;
		}

		$venue = Venue::where('user_id', $userId)->first();
		if ($venue)
		{
			$data['partner'] = $venue;
			$data['mapTypeId'] = config('evibe.ticket.type.venue');

			return $data;
		}

		return $data;
	}
}