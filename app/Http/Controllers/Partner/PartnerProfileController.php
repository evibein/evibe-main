<?php

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\Base\BaseListableController;
use App\Http\Controllers\Payments\BasePaymentController;
use App\Jobs\Emails\Partner\SubmitPackageAlertToTeam;
use App\Jobs\Emails\Util\MailPartnerQueryToTeamJob;
use App\Models\Cake\Cake;
use App\Models\Cake\CakeEvent;
use App\Models\Decor\Decor;
use App\Models\Decor\DecorEvent;
use App\Models\Package\Package;
use App\Models\Partner\PartnerPackageOptionGallery;
use App\Models\Partner\PartnerPackageOptionsDiscussions;
use App\Models\Partner\PartnerPackageOptionValues;
use App\Models\Trend\Trend;
use App\Models\Trend\TrendEvent;
use App\Models\Types\TypeEvent;
use App\Models\Types\TypePartnerPackageFields;
use App\Models\Types\TypePartnerPackageOptions;
use App\Models\Partner\PartnerPackageOptions;
use App\Models\Util\AccessToken;
use App\Models\Util\ReviewReply;
use App\Models\Util\SiteErrorLog;
use App\Models\Util\User;
use App\Models\Vendor\Vendor;
use App\Models\Venue\Venue;
use App\Models\Venue\VenueHall;
use App\Models\Venue\VenueHallEvent;
use Carbon\Carbon;
use Elasticsearch\Endpoints\Update;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class PartnerProfileController extends BaseListableController
{
	public function handlePartnerShortUrl($shortUrl, Request $request)
	{
		return redirect(route("partner.dash.dashboard"));
	}

	public function showPartnerProfile($shortUrl, Request $request)
	{
		return redirect(route("partner.dash.dashboard"));
	}

	public function handlePartnerLogin(Request $request)
	{
		$rules = [
			'email'    => 'required|email',
			'password' => 'required|min:6'
		];

		$credentials = [
			'email'    => $request->input('email'),
			'password' => $request->input('password')
		];

		$validation = Validator::make($credentials, $rules);

		// check for validation
		if ($validation->fails())
		{
			return redirect()->back()->withErrors($validation)->withInput();
		}

		$email = $request->input('email');
		$password = $request->input('password');

		if (auth()->attempt(['username' => $email, 'password' => $password]))
		{
			if (request('redirect'))
			{
				return redirect(request('redirect'));
			}

			return redirect(route('partner.dash.dashboard'));
		}
		else
		{
			return redirect()->back()->with('invalidCredential', 'You email id or password is wrong.')->withInput();
		}
	}

	public function getLoginPage()
	{
		if (Auth::check())
		{
			return redirect(route('partner.dash.dashboard'));
		}

		return view('partner.login', ["email" => request('email')]);
	}

	public function forgotPassword()
	{
		return view('partner.forgot_password');
	}

	public function forgotPasswordEmail()
	{
		$rules = [
			'email' => 'required|email',
		];

		$credentials = [
			'email' => request('email'),
		];

		$validation = Validator::make($credentials, $rules);

		// check for validation
		if ($validation->fails())
		{
			return redirect()->back()->withErrors($validation)->withInput();
		}

		$userEmail = request('email');
		$user = User::where('username', $userEmail)->first();
		if ($user)
		{
			if ($user->otp && strtotime($user->updated_at) > Carbon::now()->subMinutes(10)->timestamp)
			{
				$otp = $user->otp;
			}
			else
			{
				$otp = mt_rand(10001, 99999);
				$user->update(['otp' => $otp]);
			}

			return view('partner.forgot_password', ['userId' => $user->id, "emailId" => $userEmail, "token" => Hash::make($user->id . $otp)]);
		}
		else
		{
			return redirect()->back()->with('invalidCredential', 'No user exists with this email, Please recheck and try again.')->withInput();
		}
	}

	public function resetPassword()
	{
		$otp = request("otp");
		$userId = request("id");
		$token = request("token");
		if ($otp && $userId && $token && Hash::check($userId . $otp, $token))
		{
			return view('partner.forgot_password', ['userId' => $userId, "token" => Hash::make($userId . $otp)]);
		}

		return redirect()->back()->with('invalidCredential', 'Some error occurred while getting the data, Please try again.')->withInput();
	}

	public function showPackages($filter)
	{
		$user = Auth::user();

		$partnerData = $this->getPartnerByUserId($user->id);

		if (!isset($partnerData['partner']))
		{
			auth()->logout();

			return redirect('/');
		}

		if ($filter == "submitted" || $filter == "rejected" || $filter == "draft")
		{
			$newPackages = $this->getNewPackages($user->id, $filter);
			$packages = collect($newPackages)->all();
		}
		elseif ($filter == "live")
		{
			$options = $this->getAllOptions($partnerData);
			$packages = collect($options)->where('isLive', 1)->all();
		}
		elseif ($filter == "all")
		{
			$newPackages = $this->getNewPackages($user->id, $filter);
			$options = $this->getAllOptions($partnerData);

			$packages = array_merge($options, $newPackages);
			$packages = collect($packages)->sortByDesc('isLive')->all();
		}
		else
		{
			return redirect(route('partner.dash.view.packages', "all"));
		}

		// pagination
		$currentPage = Paginator::resolveCurrentPage();
		$perPage = 50;
		$offset = ($currentPage * $perPage) - $perPage;
		$packages = new LengthAwarePaginator(
			array_slice($packages, $offset, $perPage, true),
			count($packages),
			$perPage,
			$currentPage,
			['path' => Paginator::resolveCurrentPath()]
		);

		$data = [
			'packages' => $packages,
			'user'     => $user,
			'filter'   => $filter,
			'pageName' => $filter
		];

		return view('partner.package.view', ["data" => $data]);
	}

	public function showPartnerDetails()
	{
		$user = Auth::user();
		$partnerData = $this->getPartnerByUserId($user->id);

		if ($partnerData['partner'])
		{
			$data = [
				"user"     => $user,
				"partner"  => $partnerData['partner'],
				"pageName" => "profile"
			];

			return view("partner.profile", ["data" => $data]);
		}
		auth()->logout();

		return redirect()->back()->with('invalidCredential', 'No partner data exists with the user, Please fill partner signup form to proceed further.')->withInput();
	}

	public function showChangePasswordPage()
	{
		$date = ["user" => Auth::user()];

		return view("partner.profile.change_password", ["data" => $date]);
	}

	public function showMessages()
	{
		$user = Auth::user();
		$packageIds = PartnerPackageOptions::where('user_id', $user->id)->pluck('id')->toArray();
		$messages = PartnerPackageOptionsDiscussions::whereNull('is_complete')->whereIn('partner_option_id', $packageIds)->orderBy("created_at", "desc")->get();

		$data = [
			"user"     => $user,
			"messages" => $messages,
			"pageName" => "messages"
		];

		return view("partner.messages", ["data" => $data]);
	}

	public function showSettlements()
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUser($user);
		$url = config('evibe.api.base_url') . config('evibe.api.finance.settlements.prefix') . '/' . config('evibe.api.finance.settlements.settled-list');

		$errorMsg = "";

		$minSettlementProcessDate = \request('minPDate');
		$maxSettlementProcessDate = \request('maxPDate');

		if ($minSettlementProcessDate && $maxSettlementProcessDate && ($minSettlementProcessDate > $maxSettlementProcessDate))
		{
			$data = [
				"user"                     => $user,
				"pageName"                 => "settlements",
				"errorMsg"                 => "Kindly enter valid settlement start and end dates",
				"minSettlementProcessDate" => $minSettlementProcessDate,
				"maxSettlementProcessDate" => $maxSettlementProcessDate
			];

			return view("partner.settlements.list", ['data' => $data]);
		}

		if (!$minSettlementProcessDate && !$maxSettlementProcessDate)
		{
			// if both the dates are not mentioned, give a default range
			$minDate = Carbon::createFromTimestamp(time())->startOfMonth()->subMonths(3)->toDateTimeString();
			$minSettlementProcessDate = date('Y/m/d', strtotime($minDate));

			$maxDate = Carbon::createFromTimestamp(time())->endOfDay()->toDateTimeString();
			$maxSettlementProcessDate = date('Y/m/d', strtotime($maxDate));
		}

		$partner = Venue::where('user_id', $user->id)->first();
		$partnerTypeId = config("evibe.ticket.type.venue");
		if (!$partner)
		{
			$partner = Vendor::where('user_id', $user->id)->first();
			$partnerTypeId = config("evibe.ticket.type.planner");
		}

		if (!$partner)
		{
			$data = [
				"user"                     => $user,
				"pageName"                 => "settlements",
				"errorMsg"                 => "Some error occurred while fetching your settlements. Kindly refresh the page and try again. If that doesn't work, please write to " . config('evibe.contact.business.group'),
				"minSettlementProcessDate" => $minSettlementProcessDate,
				"maxSettlementProcessDate" => $maxSettlementProcessDate
			];

			return view("partner.settlements.list", ['data' => $data]);
		}

		$partnerId = $partner->id;

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				'partnerId'                => $partnerId,
				'partnerTypeId'            => $partnerTypeId,
				'isPartnerDash'            => true,
				'minSettlementProcessDate' => $minSettlementProcessDate,
				'maxSettlementProcessDate' => $maxSettlementProcessDate,
			]
		];

		$res = $this->makeApiCallWithUrl($options);

		if (isset($res['success']) && !$res['success'])
		{
			$errorMsg = 'Some error occurred while fetching data';

			if (isset($res['error']) && $res['error'])
			{
				$errorMsg = $res['error'];
			}
		}

		$settlementsList = (isset($res['settlementsList']) && count($res['settlementsList'])) ? $res['settlementsList'] : [];

		$data = [
			"user"                     => $user,
			"pageName"                 => "settlements",
			"errorMsg"                 => $errorMsg,
			"settlementsList"          => $settlementsList,
			"minSettlementProcessDate" => (isset($res['filters']['minSettlementProcessDate']) && $res['filters']['minSettlementProcessDate']) ? $res['filters']['minSettlementProcessDate'] : null,
			"maxSettlementProcessDate" => (isset($res['filters']['maxSettlementProcessDate']) && $res['filters']['maxSettlementProcessDate']) ? $res['filters']['maxSettlementProcessDate'] : null
		];

		return view("partner.settlements.list", ['data' => $data]);
	}

	public function showSettlementInfo($settlementId)
	{
		$user = Auth::user();
		$accessToken = $this->getAccessTokenFromUser($user);
		$url = config('evibe.api.base_url') . config('evibe.api.finance.settlements.prefix') . '/' . $settlementId . '/info';

		$errorMsg = "";

		$partner = Venue::where('user_id', $user->id)->first();
		$partnerTypeId = config("evibe.ticket.type.venue");
		if (!$partner)
		{
			$partner = Vendor::where('user_id', $user->id)->first();
			$partnerTypeId = config("evibe.ticket.type.planner");
		}

		if (!$partner)
		{
			$data = [
				"user"     => $user,
				"pageName" => "settlements",
				"errorMsg" => "Some error occurred while fetching your settlement details. Kindly refresh the page and try again. If that doesn't work, please write to " . config('evibe.contact.business.group'),
			];

			return view("partner.settlements.list", ['data' => $data]);
		}

		$partnerId = $partner->id;

		$options = [
			'method'      => "GET",
			'url'         => $url,
			'accessToken' => $accessToken,
			'jsonData'    => [
				'partnerId'     => $partnerId,
				'partnerTypeId' => $partnerTypeId,
				'isPartnerDash' => true
			]
		];

		$res = $this->makeApiCallWithUrl($options);

		if (isset($res['success']) && !$res['success'])
		{
			$errorMsg = 'Some error occurred while fetching data';

			if (isset($res['error']) && $res['error'])
			{
				$errorMsg = $res['error'];
			}
		}

		$settlementDetails = (isset($res['settlementDetails']) && count($res['settlementDetails'])) ? $res['settlementDetails'] : [];

		$data = [
			"user"              => $user,
			"pageName"          => "settlements",
			"errorMsg"          => $errorMsg,
			"settlementDetails" => $settlementDetails
		];

		return view("partner.settlements.info", ['data' => $data]);
	}

	public function getNotificationsMessages()
	{
		$user = Auth::user();
		if ($user)
		{
			$packageIds = PartnerPackageOptions::where('user_id', $user->id)->pluck('id')->toArray();
			$messages = PartnerPackageOptionsDiscussions::whereNull('is_complete')->whereNull('read_at')->whereIn('partner_option_id', $packageIds)->get();

			return view("partner.util.notifications", ['messages' => $messages]);
		}

		return redirect()->back();
	}

	public function getNotificationsCount()
	{
		$user = Auth::user();
		if ($user)
		{
			$packageIds = PartnerPackageOptions::where('user_id', $user->id)->pluck('id')->toArray();
			$messages = PartnerPackageOptionsDiscussions::whereNull('is_complete')->whereNull('read_at')->whereIn('partner_option_id', $packageIds)->get();

			return response()->json(["success" => true,
			                         "count"   => $messages->count()]);
		}

		return response()->json(["success" => false]);
	}

	public function updateOldPassword()
	{
		$user = Auth::user();
		if (Hash::check(request('oldPassword'), $user->password))
		{
			$user->update(['password' => Hash::make(request('password'))]);

			return redirect()->back()->with('successMessage', 'Password has changed succesfully.');
		}
		else
		{
			return redirect()->back()->with('invalidCredential', 'Entered current password was wrong, Please check & try again.')->withInput();
		}
	}

	public function logout()
	{
		auth()->logout();

		return redirect(route("partner.dash.new.login"));
	}

	public function postContact(Request $request)
	{
		$res = ['success' => false];
		try
		{
			$user = Auth::user();
			$userId = $user->id;
			$partner = $this->getPartnerByUserId($userId)['partner'];

			$rules = [
				'subject' => 'required|max:100'
			];

			$messages = [
				'subject.required' => 'Please enter the subject',
				'subject.max'      => 'Please enter the subject within 100 characters'
			];

			$validator = validator($request->all(), $rules, $messages);

			if ($validator->fails())
			{
				return response(['success' => false, 'error' => $validator->messages()->first()]);
			}

			$subject = "[Partner Query] $partner->preson, $partner->name has a query at " . date("d M Y, H:i A", time());
			$replyTo = [$partner->email];

			if ($partner->alt_email)
			{
				array_push($replyTo, $partner->alt_email);
			}
			if ($partner->alt_email_1)
			{
				array_push($replyTo, $partner->alt_email_1);
			}
			if ($partner->alt_email_2)
			{
				array_push($replyTo, $partner->alt_email_2);
			}

			$emailData = [
				'to'      => config('evibe.contact.business.group'),
				'sub'     => $subject,
				'partner' => $partner,
				'replyTo' => $replyTo,
				'subject' => $request->input('subject'),
				'message' => $request->input('message')
			];

			dispatch(new MailPartnerQueryToTeamJob($emailData));
			$res = ['success' => true];

			return response()->json($res);
		} catch (\Exception $e)
		{
			Log::error($e->getTraceAsString());

			return response()->json($res);
		}

	}

	private function getAllOptions($partnerData)
	{
		$partner = $partnerData['partner'];
		$mapTypeId = $partnerData['mapTypeId'];
		$mapId = $partner->id;
		$mappingData = [];
		$partnerOptions = [];

		// venues
		if ($mapTypeId == config('evibe.ticket.type.venue'))
		{
			// venue packages
			$packages = Package::where(['map_type_id' => config('evibe.ticket.type.venue'), 'map_id' => $mapId])->get();
			foreach ($packages as $package)
			{
				$mappingData[] = [
					'mapId'     => $package->id,
					'mapTypeId' => config('evibe.ticket.type.package'),
					'cityId'    => $package->city_id,
					'eventId'   => $package->event_id,
					'isLive'    => $package->is_live
				];
			}

			$halls = VenueHall::with('venue')->where('venue_id', $mapId)->get();
			foreach ($halls as $hall)
			{
				$venue = $hall->venue;
				$hallEvent = VenueHallEvent::where('venue_hall_id', $hall->id)->first();
				$eventId = $hallEvent ? $hallEvent->event_id : 1;

				$mappingData[] = [
					'mapId'     => $hall->id,
					'mapTypeId' => config('evibe.ticket.type.halls'),
					'cityId'    => $venue->city_id,
					'eventId'   => $eventId,
					'isLive'    => $venue->is_live
				];
			}
		}
		else
		{
			// planner/artist packages (non venue packages)
			$packages = Package::where('map_id', $mapId)
			                   ->where('map_type_id', '!=', config('evibe.ticket.type.venue'))
			                   ->get();
			foreach ($packages as $package)
			{
				$mappingData[] = [
					'mapId'     => $package->id,
					'mapTypeId' => config('evibe.ticket.type.package'),
					'cityId'    => $package->city_id,
					'eventId'   => $package->event_id,
					'isLive'    => $package->is_live
				];
			}

			// trend
			$trends = Trend::where('planner_id', $mapId)->get();
			foreach ($trends as $trend)
			{
				$trendEvent = TrendEvent::where('trend_id', $trend->id)->first();
				$eventId = $trendEvent ? $trendEvent->type_event_id : 1;
				$mappingData[] = [
					'mapId'     => $trend->id,
					'mapTypeId' => config('evibe.ticket.type.trend'),
					'cityId'    => $trend->city_id,
					'eventId'   => $eventId,
					'isLive'    => $trend->is_live
				];
			}

			// decor
			$decors = Decor::where('provider_id', $mapId)->get();
			foreach ($decors as $decor)
			{
				$decorEvent = DecorEvent::where('decor_id', $decor->id)->first();
				$eventId = $decorEvent ? $decorEvent->event_id : 1;
				$mappingData[] = [
					'mapId'     => $decor->id,
					'mapTypeId' => config('evibe.ticket.type.decor'),
					'cityId'    => $partner->city_id,
					'eventId'   => $eventId,
					'isLive'    => $decor->is_live
				];
			}

			// cake
			$cakes = Cake::where('provider_id', $mapId)->get();
			foreach ($cakes as $cake)
			{
				$cakeEvent = CakeEvent::where('cake_id', $cake->id)->first();
				$eventId = $cakeEvent ? $cakeEvent->event_id : 1;
				$mappingData[] = [
					'mapId'     => $cake->id,
					'mapTypeId' => config('evibe.ticket.type.cake'),
					'cityId'    => $partner->city_id,
					'eventId'   => $eventId,
					'isLive'    => $cake->is_live
				];
			}
		}

		// create cards
		foreach ($mappingData as $mapping)
		{
			$partnerOption = $this->getCardData($mapping);

			if (!is_null($partnerOption))
			{
				$partnerOptions[] = $partnerOption;
			}
		}

		return $partnerOptions;
	}

	public function getPartnerByUserId($userId)
	{
		$planner = Vendor::where('user_id', $userId)->first();
		$data = [
			'partner'   => null,
			'mapTypeId' => null
		];

		if ($planner)
		{
			$data['partner'] = $planner;
			$data['mapTypeId'] = config('evibe.ticket.type.planner');

			return $data;
		}

		$venue = Venue::where('user_id', $userId)->first();
		if ($venue)
		{
			$data['partner'] = $venue;
			$data['mapTypeId'] = config('evibe.ticket.type.venue');

			return $data;
		}

		return $data;
	}

	public function getCardData($data)
	{
		$id = $data['mapId'];
		$modelName = isset($this->listableMappings[$data['mapTypeId']]['model']) ? $this->listableMappings[$data['mapTypeId']]['model'] : false;

		if ($modelName && ($model = $modelName::find($id)))
		{
			$displayName = $this->listableMappings[$data['mapTypeId']]['display_name'];
			$data['eventId'] = in_array($data['eventId'], $this->getLiveOccasion()) ? $data['eventId'] : 1;

			$dataStore = app()->make("EvibeUtilDataStore");
			$cityUrl = $dataStore->getCityUrlFromId($data['cityId']);
			$occasionUrl = $dataStore->getOccasionUrlFromId($data['eventId']);

			$shortlistParams = [
				'mapId'       => $id,
				'mapTypeId'   => $data['mapTypeId'],
				'occasionId'  => $data['eventId'],
				'cityId'      => $data['cityId'],
				'displayName' => $displayName,
				'isLive'      => $data['isLive']
			];

			if ($data['mapTypeId'] == config('evibe.ticket.type.package') && $model)
			{
				$data['mapTypeId'] = $model->getMapTypeId();
				$shortlistParams['displayName'] = $this->listableMappings[$data['mapTypeId']]['display_name'];
			}

			$name = $this->listableMappings[$data['mapTypeId']]['name'];
			$cardData = $model->getShortListableCardInfo($cityUrl, $occasionUrl, ['name' => $name], $shortlistParams);

			if (count($cardData) && array_key_exists('fullUrl', $cardData))
			{
				return $cardData;
			}
		}

		return null;
	}

	private function getLiveOccasion()
	{
		return [
			config('evibe.occasion.bachelor.id'),
			config('evibe.occasion.kids_birthdays.id'),
			config('evibe.occasion.pre-post.id'),
			config('evibe.occasion.surprises.id'),
			config('evibe.occasion.house-warming.id')
		];
	}

	public function addPackage()
	{
		$user = Auth::user();
		if ($user)
		{
			$data = [
				'typePackages' => TypePartnerPackageOptions::where('is_live', 1)
				                                           ->get(),
				'user'         => $user,
				'seo'          => [
					'pageTitle'       => 'Partner Add Package Form',
					'pageDescription' => 'Sign up form to partner with Evibe.in. Submit your contact information for us to get in touch with you.'
				]];

			return view('partner.add-package.first', ['data' => $data]);
		}
		else
		{
			return view('errors.404');
		}
	}

	public function redirectPageToFields()
	{
		// check for validation
		if (!request("partnerPackageType") || request("partnerPackageType") <= 0)
		{
			return redirect()->back()->with('errorMsg', 'Please select a valid package type.');
		}
		else
		{
			return redirect()->route("partner.dash.add.package.first", ['type' => request("partnerPackageType")]);
		}
	}

	public function getFieldsForType()
	{
		$user = Auth::user();

		$typeId = request('type');

		if ($typeId && $typeId > 0)
		{
			$type = TypePartnerPackageOptions::where("id", request('type'))->first();

			if ($type)
			{
				$packageFields = $this->getFieldsBasedOnTypePackage($user->id, $type->type_package_id);

				$data = [
					'user'          => $user,
					'typePackageId' => $type->type_package_id,
					'packageFields' => $packageFields->count() > 0 ? $packageFields : null,
					'typeEvents'    => TypeEvent::where('show_customer', 1)->get(),
					'seo'           => [
						'pageTitle'       => 'Partner Add Package Form',
						'pageDescription' => 'Sign up form to partner with Evibe.in. Submit your contact information for us to get in touch with you.'
					]];

				return view('partner.add-package.second', ["data" => $data]);
			}
		}

		return redirect(route("partner.dash.add.package"));
	}

	public function editDetails($packageId)
	{
		$user = Auth::user();
		$newPackage = PartnerPackageOptions::find($packageId);

		if ($newPackage)
		{
			$packageFields = $this->getFieldsBasedOnTypePackage($user->id, $newPackage->package_type_id);

			$packageAdditionalInfoValues = PartnerPackageOptionValues::where('partner_option_id', $packageId)
			                                                         ->select("type_partner_option_field_id", "value")
			                                                         ->get();

			$data = [
				'user'          => $user,
				'typePackageId' => $newPackage->package_type_id,
				'packageFields' => $packageFields->count() > 0 ? $packageFields : null,
				'package'       => $newPackage,
				'packageValues' => $packageAdditionalInfoValues,
				'typeEvents'    => TypeEvent::where('show_customer', 1)->get(),
				'is_update'     => 1,
				'seo'           => [
					'pageTitle'       => 'Partner Add Package Form',
					'pageDescription' => 'Sign up form to partner with Evibe.in. Submit your contact information for us to get in touch with you.'
				]];

			return view('partner.add-package.second', ["data" => $data]);
		}

		return redirect(route("partner.dash.add.package"));
	}

	public function redirectPageToGallery()
	{
		$partnerData = $this->getPartnerByUserId(Auth::user()->id);
		$partnerTypeId = ($partnerData['mapTypeId'] == config("evibe.ticket.type.planner")) ? ($partnerData['partner']->is_artist == 1 ? config("evibe.ticket.type.artist") : config("evibe.ticket.type.planner")) : config("evibe.ticket.type.venue");

		$packageFields = TypePartnerPackageFields::where('type_package_id', request("typePackageId"))
		                                         ->where(function ($query) use ($partnerTypeId) {
			                                         $query->where('type_partner_type_id', $partnerTypeId)
			                                               ->orWhereNull('type_partner_type_id');
		                                         })
		                                         ->get();

		$rules = [
			'vendorPackageName'        => 'required',
			'vendorPackagePrice'       => 'required|integer',
			'vendorPackageActualPrice' => 'required|integer|min_as:vendorPackagePrice',
			'vendorPackageInclusions'  => 'required',
			'vendorPackageEventId'     => 'required|integer'
		];

		$messages = [
			'vendorPackageName.required'        => 'Please enter the package name',
			'vendorPackagePrice.required'       => 'Please enter the price of package',
			'vendorPackageActualPrice.required' => 'Please enter actual price of the package',
			'vendorPackageInclusions.required'  => 'Please enter inclusions of the package',
			'vendorPackagePrice.integer'        => 'Please enter a valid price',
			'vendorPackageActualPrice.integer'  => 'Please enter a valid actual price',
			'vendorPackageActualPrice.min_as'   => 'Please enter the actual price more than the discounted price',
			'vendorPackageEventId.required'     => 'Please select a valid event',
			'vendorPackageEventId.integer'      => 'Please select a valid event',
		];

		foreach ($packageFields as $typeField)
		{
			if ($typeField->validation)
			{
				$rules[$typeField->original_field_name] = $typeField->validation;
			}
		}

		$validation = Validator::make(request()->all(), $rules, $messages);

		// check for validation
		if ($validation->fails())
		{
			return redirect()->back()->with('errorMsg', $validation->errors()->first())->withInput();
		}

		$user = Auth::user();
		if ($user)
		{
			$inputData = [
				'package_name'    => request('vendorPackageName'),
				'price'           => request('vendorPackagePrice'),
				'worth'           => request('vendorPackageActualPrice'),
				'inclusions'      => request('vendorPackageInclusions'),
				'user_id'         => $user->id,
				'package_type_id' => request("typePackageId"),
				'event_id'        => request('vendorPackageEventId'),
				'status_id'       => config("evibe.partner.package_status.draft"),
				'is_edit'         => 1
			];

			if (request('isUpdate') && request("newPackageId") && request('isUpdate') == 1)
			{
				$newPackage = PartnerPackageOptions::updateOrCreate(['id' => request("newPackageId")], $inputData);
			}
			else
			{
				$newPackage = PartnerPackageOptions::create($inputData);
			}

			foreach (request()->all() as $key => $value)
			{
				if ($field = $packageFields->where("original_field_name", $key)->first())
				{
					PartnerPackageOptionValues::updateOrCreate([
						                                           'partner_option_id'            => $newPackage->id,
						                                           'type_partner_option_field_id' => $field->id,
					                                           ],
					                                           [
						                                           'value' => $value
					                                           ]);
				}
			}

			return redirect()->route("partner.dash.add.package.second", [$newPackage->id]);
		}

		return redirect()->back()->with('errorMsg', "Error occurred while submitting the package, Please try again.")->withInput();
	}

	public function getGalleryData($packageId)
	{
		$user = Auth::user();

		$newPackage = PartnerPackageOptions::where("id", $packageId)->first();
		if ($newPackage)
		{
			$data = [
				'user'       => $user,
				'newPackage' => $newPackage,
				'packageId'  => $packageId,
				'seo'        => [
					'pageTitle'       => 'Partner Add Package Form',
					'pageDescription' => 'Sign up form to partner with Evibe . in . Submit your contact information for us to get in touch with you . '
				]];

			return view('partner.add-package.third', ["data" => $data])->with('successMsg', "Package has been created successfully, Upload images and click on submit details to submit details.");
		}

		return redirect(route("partner.dash.add.package"));
	}

	public function redirectReview()
	{
		$newPackageId = request("newPackage");

		$images = request()->file('vendorPackageInputFile');

		if (count($images) && !is_null($images[0]))
		{
			foreach ($images as $image)
			{
				$imageRules = [
					'file' => 'required|mimes:png,jpeg,jpg,JPG|max:3072'
				];

				$imageMessages = [
					'file.required' => 'Please upload at least one image or video',
					'file.mimes'    => 'One of the images is not valid . (only . png, .jpeg, .jpg are accepted)',
					'file.max'      => 'Image size cannot br more than 3 MB'
				];

				$imageValidator = validator(['file' => $image], $imageRules, $imageMessages);

				if ($imageValidator->fails())
				{
					return redirect()->back()->with('errorMsg', $imageValidator->errors()->first());
				}
			}

			$directPath = '/package/new/' . $newPackageId . '/';

			foreach ($images as $image)
			{
				$option = ['isWatermark' => false, 'isOptimize' => false];
				$imageName = $this->uploadImageToServer($image, $directPath, $option);

				$imgInsertData = [
					'url'               => $imageName,
					'title'             => $this->getImageTitle($image),
					'partner_option_id' => $newPackageId,
					'type'              => 1
				];

				PartnerPackageOptionGallery::create($imgInsertData);
			}

			return redirect(route("partner.dash.add.package.third", $newPackageId));
		}

		return redirect()->back()->with('errorMsg', "Please upload atleast one image to continue.");
	}

	public function showSubmittedDetails($packageId)
	{
		$addValues = [];
		$newPackage = PartnerPackageOptions::find($packageId);

		$discussion = PartnerPackageOptionsDiscussions::whereNull('is_complete')->where('partner_option_id', $packageId)->first();
		if ($discussion && is_null($discussion->read_at))
		{
			$discussion->update(["read_at" => Carbon::now()]);
		}
		elseif ($discussion && !is_null($discussion->read_at))
		{
			$discussion->update(["last_read_at" => Carbon::now()]);
		}

		if ($newPackage)
		{
			$packageAdditionalInfoFields = TypePartnerPackageFields::where('type_package_id', $newPackage->package_type_id)
			                                                       ->select("id", "field_name")
			                                                       ->get();

			$packageAdditionalInfoValues = PartnerPackageOptionValues::where('partner_option_id', $newPackage->id)
			                                                         ->select("type_partner_option_field_id", "value")
			                                                         ->get();

			foreach ($packageAdditionalInfoFields as $field)
			{
				$packageAdditionalInfoValue = $packageAdditionalInfoValues->where("type_partner_option_field_id", $field->id)->first();
				$addValues[$field->field_name] = $packageAdditionalInfoValue ? $packageAdditionalInfoValue->value : "--";
			}

			$discussions = PartnerPackageOptionsDiscussions::where('partner_option_id', $newPackage->id)->orderBy("created_at", "desc")->get();

			$data = [
				'user'        => Auth::user(),
				'event'       => TypeEvent::select("id", "name")->get(),
				'newPackage'  => $newPackage,
				'addValues'   => $addValues,
				'discussions' => $discussions,
				'images'      => PartnerPackageOptionGallery::where('partner_option_id', $newPackage->id)->get()
			];

			return view('partner.add-package.fourth', ["data" => $data]);
		}

		return redirect()->back();
	}

	public function redirectThankYou($packageId)
	{
		$partnerData = $this->getPartnerByUserId(Auth::user()->id);
		$partner = $partnerData['partner'];

		$newPackage = PartnerPackageOptions::where("id", $packageId)->first();

		$newPackage->update(["is_edit" => 0, "status_id" => config("evibe.partner.package_status.submitted"), "submitted_at" => Carbon::now()]);

		//$this->dispatch(new SubmitPackageAlertToTeam([
		//	                                             "partner"   => $partner,
		//	                                             "package"   => $newPackage,
		//	                                             "is_update" => 0
		//                                             ]));

		return redirect(route("partner.dash.add.package.thankYou"));
	}

	public function upgradeRedirectThankYou($packageId)
	{
		$partnerData = $this->getPartnerByUserId(Auth::user()->id);
		$partner = $partnerData['partner'];
		$newPackage = PartnerPackageOptions::where("id", $packageId)->first();

		$discussions = PartnerPackageOptionsDiscussions::whereNull('is_complete')->where('partner_option_id', $newPackage->id)->first();

		$newPackage->update(["is_edit" => 0, "status_id" => config("evibe.partner.package_status.submitted"), "submitted_at" => Carbon::now()]);

		$discussions->update(["partner_comment" => request("partnerUpgradeResponse"), "responded_at" => Carbon::now()]);

		//$this->dispatch(new SubmitPackageAlertToTeam([
		//	                                             "partner"   => $partner,
		//	                                             "package"   => $newPackage,
		//	                                             "is_update" => 1
		//                                             ]));

		return redirect(route("partner.dash.add.package.thankYou"));
	}

	public function showThankYouPage()
	{
		$data = [
			'user' => Auth::user()
		];

		return view('partner.add-package.thank_you', ["data" => $data]);
	}

	private function getNewPackages($userId, $status = "")
	{
		$newPackages = [];

		$partnerPackages = PartnerPackageOptions::where("user_id", $userId)
		                                        ->whereNull('accepted_at');

		if ($status == "all")
		{
			$partnerPackages->whereNotNull('status_id');
		}
		else
		{
			$partnerPackages->where('status_id', config("evibe.partner.package_status." . $status));
		}

		foreach ($partnerPackages->get() as $partnerPackage)
		{
			$galleryUrl = "";
			$gallery = PartnerPackageOptionGallery::where('partner_option_id', $partnerPackage->id)->first();
			if ($gallery)
			{
				$galleryUrl = config("evibe.gallery.host") . "/package/new/" . $partnerPackage->id . "/" . $gallery->url;
			}

			$newPackages[] = [
				'id'                  => $partnerPackage->id,
				'title'               => $partnerPackage->package_name,
				'status'              => $status,
				'price'               => $partnerPackage->price,
				'worth'               => $partnerPackage->worth,
				'priceMax'            => 0,
				'fullUrl'             => route('partner.dash.new.profile.view', $partnerPackage->id),
				'profileImg'          => isset($galleryUrl) ? $galleryUrl : "",
				'isLive'              => 0,
				'rating'              => "",
				'additionalLine'      => "",
				'additionalPriceLine' => "",
				'isNewPackage'        => $partnerPackage->status_id == config("evibe.partner.package_status.rejected") ? 0 : 1,
				'isDelete'            => $partnerPackage->status_id == config("evibe.partner.package_status.draft") ? 1 : 0
			];
		}

		return $newPackages;
	}

	public function showNewPackageProfile($packageId)
	{
		$user = Auth::user();
		$is_live = request('is_live');

		// profile page data for partner submitted packages
		if (!is_null($is_live) && $is_live == 0)
		{
			$newPackage = PartnerPackageOptions::find($packageId);

			if ($newPackage)
			{
				$packageAdditionalInfoFields = TypePartnerPackageFields::where('type_package_id', $newPackage->package_type_id)
				                                                       ->select("id", "field_name")
				                                                       ->get();

				$packageAdditionalInfoValues = PartnerPackageOptionValues::where('partner_option_id', $newPackage->id)
				                                                         ->select("type_partner_option_field_id", "value")
				                                                         ->get();

				$addValues = [];

				foreach ($packageAdditionalInfoFields as $field)
				{
					$packageAdditionalInfoValue = $packageAdditionalInfoValues->where("type_partner_option_field_id", $field->id)->first();
					$addValues[$field->field_name] = $packageAdditionalInfoValue ? $packageAdditionalInfoValue->value : "--";
				}

				$discussions = PartnerPackageOptionsDiscussions::where('partner_option_id', $newPackage->id)->first();

				$data = [
					'user'        => Auth::user(),
					'event'       => TypeEvent::select("id", "name")->get(),
					'newPackage'  => $newPackage,
					'addValues'   => $addValues,
					'discussions' => $discussions ? explode(',', $discussions->upgrade_option_ids) : "",
					'images'      => PartnerPackageOptionGallery::where('partner_option_id', $newPackage->id)->get()
				];

				return view('partner.package.profile', ["data" => $data]);
			}
		}
		// data for profile page of packages which are live
		elseif (!is_null($is_live))
		{
			$is_live = explode('_', $is_live);

			if ($is_live[0] && $is_live[1] && $is_live[0] > 0 && $is_live[1] > 0)
			{
				$partnerData = $this->getPartnerByUserId($user->id);
				$packages = $this->getAllOptions($partnerData);

				$basePaymentController = new BasePaymentController();
				$packageData = $basePaymentController->fillMappingValues([
					                                                         'isCollection' => false,
					                                                         'mapId'        => $is_live[1],
					                                                         'mapTypeId'    => $is_live[0]
				                                                         ]);

				$package = collect($packages)->where('mapTypeId', $is_live[0])->where('mapId', $is_live[1])->first();

				$data = [
					'user'            => Auth::user(),
					'event'           => TypeEvent::select("id", "name")->get(),
					'newPackage'      => $package,
					'livePackageData' => $packageData,
					'mapTypeId'       => $is_live[0]
				];

				return view('partner.package.profile', ["data" => $data]);
			}
		}

		return redirect()->route("partner.dash.view.packages", "all");
	}

	public function deleteImage($packageId, $imageId)
	{
		$image = PartnerPackageOptionGallery::where("partner_option_id", $packageId)->where('id', $imageId)->first();
		if ($image)
		{
			$image->update(["deleted_at" => Carbon::now()]);
		}

		return redirect()->back()->with('errorMsg', "Image deleted successfully");
	}

	public function deletePackage($newPackageId)
	{
		$package = PartnerPackageOptions::find($newPackageId);
		if ($package)
		{
			$package->update(["deleted_at" => Carbon::now()]);
		}

		return redirect()->back()->with('errorMsg', "Package deleted successfully");
	}

	private function getFieldsBasedOnTypePackage($userId, $typePackageId)
	{
		$partnerData = $this->getPartnerByUserId($userId);
		$partnerTypeId = ($partnerData['mapTypeId'] == config("evibe.ticket.type.planner")) ? ($partnerData['partner']->is_artist == 1 ? config("evibe.ticket.type.artist") : config("evibe.ticket.type.planner")) : config("evibe.ticket.type.venue");

		$packageFields = TypePartnerPackageFields::where('type_package_id', $typePackageId)
		                                         ->where(function ($query) use ($partnerTypeId) {
			                                         $query->where('type_partner_type_id', $partnerTypeId)
			                                               ->orWhereNull('type_partner_type_id');
		                                         })
		                                         ->get();

		return $packageFields;
	}

	public function submitPartnerResponse($reviewId)
	{
		$reviewReply = ReviewReply::create(['review_id' => $reviewId, 'reply' => request('review')]);

		if (!$reviewReply)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => "Review not added, Please try again after some time."
			                        ]);
		}

		return response()->json([
			                        'success' => true
		                        ]);
	}
}
