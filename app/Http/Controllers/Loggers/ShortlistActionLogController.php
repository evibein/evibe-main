<?php

namespace App\Http\Controllers\Loggers;

use App\Models\Util\LogShortlist;
use App\Http\Controllers\Base\BaseController;

class ShortlistActionLogController extends BaseController
{
	public function __construct()
	{
		parent::__construct();
	}

	public function logShortlistAddAction($shortlistOptionId)
	{
		if ($shortlistOptionId)
		{
			$actionId = config('evibe.shortlist_action.add');
			$this->logShortlistAction($shortlistOptionId, $actionId);
		}
	}

	public function logShortlistDeleteAction($shortlistOptionId)
	{
		if ($shortlistOptionId)
		{
			$actionId = config('evibe.shortlist_action.delete');
			$this->logShortlistAction($shortlistOptionId, $actionId);
		}
	}

	private function logShortlistAction($shortlistOptionId, $actionId)
	{
		if ($shortlistOptionId && $actionId)
		{
			$log_shortlist = new LogShortlist();
			$log_shortlist->shortlist_option_id = $shortlistOptionId;
			$log_shortlist->action_id = $actionId;
			$log_shortlist->action_taken_at = date('Y-m-d H:i:s');
			$log_shortlist->save();
		}
	}
}