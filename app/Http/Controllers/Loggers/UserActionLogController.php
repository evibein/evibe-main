<?php

namespace App\Http\Controllers\Loggers;

use App\Models\Util\UserAction;
use App\Http\Controllers\Base\BaseController;

class UserActionLogController extends BaseController
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function logUserLogoutAction($userId)
	{
		if ($userId)
		{
			$actionId = config('evibe.user_action.logout');
			$this->logUserAction($userId, $actionId);
		}
	}

	public function logUserLoginAction($userId)
	{
		if ($userId)
		{
			$actionId = config('evibe.user_action.login');
			$this->logUserAction($userId, $actionId);
		}
	}

	private function logUserAction($userId, $actionId)
	{
		if ($userId && $actionId)
		{
			$user_action = new UserAction();
			$user_action->user_id = $userId;
			$user_action->action_id = $actionId;
			$user_action->action_taken_at = date('Y-m-d H:i:s');
			$user_action->save();
		}
	}
}