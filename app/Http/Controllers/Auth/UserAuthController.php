<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserLoginEvent;
use App\Models\Util\User;
use App\Models\Util\SocialAccount;
use App\Http\Controllers\Base\BaseController;
use App\Events\UserLogoutEvent;
use App\Models\Util\UserAction;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class UserAuthController extends BaseController
{
	public function __construct()
	{
		parent::__construct();
	}

	// fetch user name for vue js component in json format
	public function getUserData()
	{
		$data = [
			"isLoggedIn" => false
		];

		if (auth()->check())
		{
			$user = auth()->user();
			$data = [
				'name'       => $user->name,
				'email'      => $user->username,
				'isLoggedIn' => true
			];
		}

		return response()->json($data);
	}

	public function handleSocialUserEmailSubmit()
	{
		$rules = [
			'userEmail' => 'required|email',
		];

		$messages = [
			'userEmail.required' => 'Please enter your email address',
			'userEmail.email'    => 'Please enter a valid email address',
		];

		$validator = Validator::make(request()->all(), $rules, $messages);
		$previousUrl = app('url')->previous();

		if ($validator->fails())
		{
			return redirect($previousUrl)
				->withErrors($validator)
				->withInput();
		}
		else
		{
			$user = null;
			$providerDetails = [
				'providerName'       => request('providerName'),
				'providerUserId'     => request('userId'),
				'providerUserName'   => request('userName'),
				'providerUserEmail'  => request('userEmail'),
				'providerUserAvatar' => request('userAvatar'),
			];

			$redirectUrl = request('redirectUrl');

			// this check to prevent directly opening this page through URL
			if ($providerDetails['providerName'] != '' &&
				$providerDetails['providerUserId'] != '' &&
				$providerDetails['providerUserName'] != '' &&
				$providerDetails['providerUserEmail'] != ''
			)
			{
				// requires providerName, providerUserId, providerUserName, providerUserEmail, providerUserAvatar
				$user = $this->processSocialLogin($providerDetails);
			}

			if ($user)
			{
				$this->processUserLogin($user);
			}

			// destroy the session variable before redirecting
			session()->forget('in.evibe.provider_data');
			session()->save();

			return redirect()->intended($redirectUrl);
		}
	}

	public function requestSocialUserEmail()
	{
		$data = session('in.evibe.provider_data');

		return view('app/auth_login_email', ['data' => $data]);
	}

	public function logout()
	{
		$urlPrevious = url()->previous();

		$user = auth()->user();
		auth()->logout();

		if ($user)
		{
			// fire the event after logout
			event(new UserLogoutEvent($user->id));
		}

		return redirect()->intended($urlPrevious);
	}

	public function handleSocialUserSignInSuccess($provider, $providerUser, $socialAccount)
	{
		$providerName = class_basename($provider);
		$user = null;

		if ($socialAccount)
		{
			$user = $socialAccount->user;
		}
		else
		{
			if ($providerUser->getEmail())
			{
				$providerDetails = [
					'providerName'       => $providerName,
					'providerUserId'     => $providerUser->getId(),
					'providerUserName'   => $providerUser->getName(),
					'providerUserEmail'  => $providerUser->getEmail(),
					'providerUserAvatar' => $providerUser->getAvatar(),
				];

				$user = $this->processSocialLogin($providerDetails);
			}
		}

		if ($user)
		{
			$this->processUserLogin($user);
		}
	}

	public function handleUserLoginModal()
	{
		if ($user = Auth::user())
		{
			// @todo: Handle this type of cases with proper context & flow
			// Checking if the user has logged in for a week
			//$userAction = UserAction::where('user_id', $user->id)->orderBy('updated_at', 'desc')->first();

			//if ($userAction && $userAction->action_taken_at && ($userAction->action_taken_at < Carbon::now()->subWeek()))
			//{
			//	return response()->json([
			//		                        "success" => false
			//	                        ]);
			//}

			return response()->json([
				                        "success" => true
			                        ]);
		}
		else
		{
			return response()->json([
				                        "success" => false
			                        ]);
		}
	}

	private function processUserLogin($user)
	{
		auth()->login($user, true);
		event(new UserLoginEvent($user->id));
	}

	private function processSocialLogin($providerDetails)
	{
		$providerName = $providerDetails['providerName'];
		$providerUserId = $providerDetails['providerUserId'];
		$providerUserName = $providerDetails['providerUserName'];
		$providerUserEmail = $providerDetails['providerUserEmail'];
		$providerUserAvatar = $providerDetails['providerUserAvatar'];

		// creating a new social account
		$account = new SocialAccount([
			                             'provider_id' => $providerUserId,
			                             'provider'    => $providerName,
			                             'user_email'  => $providerUserEmail,
			                             'name'        => $providerUserName,
			                             'img_url'     => $providerUserAvatar,
		                             ]);

		$user = User::where('username', $providerUserEmail)->first();

		if (!$user)
		{
			// creating a new user
			$user = User::create(['username' => $providerUserEmail,
			                      'name'     => $providerUserName,
			                      'role_id'  => config('evibe.roles.customer'),
			                     ]);

			// @todo: send a mail to the user welcoming to the evibe family
		}

		$account->user()->associate($user);
		$account->save();

		return $user;
	}
}