<?php

namespace App\Http\Controllers\Auth;

use App\Events\SocialUserSigninFailedEvent;
use App\Events\SocialUserSigninSuccessEvent;
use App\Http\Controllers\Base\BaseController;
use App\Models\Util\SocialAccount;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthController extends BaseController
{
	public function redirectToProvider($provider)
	{
		session(['in.evibe.url_intended' => url()->previous()]);

		return Socialite::driver($provider)->redirect();
	}

	public function handleProviderCallback($provider)
	{
		$urlIntended = session('in.evibe.url_intended', url('/'));
		$providerName = class_basename($provider);

		if (request()->has('error'))
		{
			event(new SocialUserSigninFailedEvent($providerName));

			return redirect()->intended($urlIntended);
		}

		try
		{
			$providerUser = Socialite::driver($provider)->user();
			$socialAccount = SocialAccount::where('provider', $providerName)
			                              ->where('provider_id', $providerUser->getId())
			                              ->first();

			// user sign-in for the first time with no permissions for email id
			// repeated user with email permissions denied
			if (!$providerUser->getEmail() && !$socialAccount)
			{
				$data = [
					'provider'    => [
						'providerName' => $providerName,
						'userName'     => $providerUser->getName(),
						'avatar'       => $providerUser->getAvatar(),
						'userId'       => $providerUser->getId()
					],
					'redirectUrl' => $urlIntended
				];

				return redirect('/user/confirm-email')->with('in.evibe.provider_data', $data);
			}

			// event to process information from socialite provider response data
			event(new SocialUserSigninSuccessEvent($provider, $providerUser, $socialAccount));

		} catch (\Exception $e)
		{
			Log::error('Invalid state exception occurred during socialite login');
		}

		session()->forget('in.evibe.url_intended');
		session()->save(); // @see: fix for some rare issues

		return redirect()->intended($urlIntended);
	}
}