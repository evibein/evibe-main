<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Base\BaseController;
use App\Models\BrowsingActivity\BrowsingActivity;
use App\Models\Util\SocialAccount;
use App\Models\Util\User;
use Carbon\Carbon;
use Evibe\Passers\Util\SiteLogData;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class LoginController extends BaseController
{
	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/

	use AuthenticatesUsers;

	/**
	 * Where to redirect users after login / registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/home';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		/*$this->middleware('guest', ['except' => 'logout']);*/
	}

	public function handleCustomerLogin(Request $request)
	{
		$email = $request->input('email');
		$email = str_replace(' ', '', $email);
		$password = $request->input('password');

		$rules = [
			'email'    => 'required|email',
			'password' => 'required'
		];

		$credentials = [
			'email'    => $request->input('email'),
			'password' => $request->input('password')
		];

		$validation = Validator::make($credentials, $rules);

		// check for validation
		if (strpos($email, '+'))
		{
			return [
				'success' => false,
				'error'   => 'Please enter a valid email address'
			];
		}
		elseif ($validation->fails())
		{
			return [
				'success' => false,
				'error'   => $validation->messages()->first()
			];
		}

		if (auth()->attempt(['username' => $email, 'password' => $password]))
		{
			// Updates existing cookies with UserID-used for user browsing history
			$this->syncBrowsingCookieWithDBAfterLogin();

			return [
				'success' => true,
			];
		}
		else
		{
			$checkUserWithSocialAccount = SocialAccount::where('user_email', $email)->first();
			if ($checkUserWithSocialAccount)
			{
				$msg = "Facebook";
				if (strpos($checkUserWithSocialAccount->provider, 'google') == 0)
				{
					$msg = "Google";
				}

				return [
					'success' => false,
					'error'   => "The Email or password you have entered is incorrect. Please try login with " . $msg
				];
			}

			return [
				'success' => false,
				'error'   => 'The Email or Password you have entered is incorrect. Please try again.'
			];
		}
	}

	public function handleCustomerAppLogin(Request $request)
	{
		if ($request->has('access-token') && $request->has('uId') && ($request->input('uId') > 0))
		{
			$accessToken = \App\Models\Util\AccessToken::where("user_id", $request->input('uId'))
			                                           ->where("access_token", $request->input('access-token'))
			                                           ->first();

			if ($accessToken)
			{
				Auth::logout();
				Auth::loginUsingId($request->input('uId'), true);
			}
		}

		return redirect(config("evibe.host") . "?ref=cusLoginTrue");
	}

	public function handleCustomerAppLoginThroughSocial(Request $request, $provider = "")
	{
		$user = User::where("username", $request->input('email'))
		            ->first();

		if ($user)
		{
			$socialAccount = SocialAccount::where("user_id", $user->id)
			                              ->where("user_email", $user->username)
			                              ->where("provider", strtolower($provider))
			                              ->first();

			if ($socialAccount)
			{
				// Do nothing if already logged in fb/google in the past
			}
			else
			{
				SocialAccount::create([
					                      'provider_id' => $request->input("uId"),
					                      'provider'    => $provider,
					                      'user_email'  => $request->input("email"),
					                      'name'        => $request->input("name"),
					                      'img_url'     => $request->input("url"),
					                      'user_id'     => $user->id
				                      ]);
			}
		}
		else
		{
			$user = User::create([
				                     'username' => $request->input("email"),
				                     'name'     => $request->input("name"),
				                     'role_id'  => config('evibe.roles.customer'),
			                     ]);

			SocialAccount::create([
				                      'provider_id' => $request->input("uId"),
				                      'provider'    => $provider,
				                      'user_email'  => $request->input("email"),
				                      'name'        => $request->input("name"),
				                      'img_url'     => $request->input("url"),
				                      'user_id'     => $user->id
			                      ]);
		}

		Auth::logout();
		Auth::loginUsingId($user->id, true);

		return redirect(config("evibe.host") . "?ref=cusLoginTrue");
	}

	public function handleCustomerSignUp(Request $request)
	{
		$name = $request->input('fullName');
		$email = $request->input('email');
		$email = str_replace(' ', '', $email);
		$password = $request->input('password');
		$rePassword = $request->input('rePassword');

		$rules = [
			'name'       => 'required',
			'email'      => 'required|email',
			'password'   => 'required|min:8',
			'rePassword' => 'required|min:8',
		];

		$credentials = [
			'name'       => $name,
			'email'      => $email,
			'password'   => $password,
			'rePassword' => $rePassword,
		];

		$validation = Validator::make($credentials, $rules);

		if (strpos($email, '+'))
		{
			return [
				'success' => false,
				'error'   => 'Please enter a valid email address'
			];
		}
		elseif ($password != $rePassword)
		{
			return [
				'success' => false,
				'error'   => "Passwords do not match, Please check"
			];
		}
		elseif ($validation->fails())
		{
			return [
				'success' => false,
				'error'   => $validation->messages()->first()
			];
		}

		$checkUserWithSocialAccount = SocialAccount::where('user_email', 'like', $email)->first();
		if ($checkUserWithSocialAccount)
		{
			$user = User::where('username', $email)
			            ->where(function ($query) {
				            $query->whereNull("password")
				                  ->orWhere("password", "");
			            })
			            ->first();

			if ($user)
			{
				$user->update([
					              'name'       => $name,
					              'password'   => Hash::make($password),
					              'updated_at' => Carbon::now()->toDateTimeString()
				              ]);

				Auth::logout();
				Auth::loginUsingId($user->id, true);

				return [
					'success'    => true,
					'successMsg' => "Successfully Signed Up"
				];
			}

			$msg = "Facebook";
			if (strpos($checkUserWithSocialAccount->provider, 'google') == 0)
			{
				$msg = "Google";
			}

			return [
				'success' => false,
				'error'   => 'You have already registered with us using your ' . $msg . " Account"
			];
		}
		else
		{
			$userValid = User::where("username", $email)->first();

			if (!$userValid)
			{
				try
				{
					$user = User::create([
						                     "username"   => $email,
						                     "name"       => $name,
						                     "password"   => Hash::make($password),
						                     "role_id"    => config("evibe.roles.customer"),
						                     "updated_at" => Carbon::now()
					                     ]);

					Auth::logout();
					Auth::loginUsingId($user->id, true);

					// Updates existing cookies with UserID-used for user browsing history
					$this->syncBrowsingCookieWithDBAfterLogin();

					return [
						'success'    => true,
						'successMsg' => "Successfully Signed Up"
					];
				} catch (\Exception $e)
				{
					$siteLogData = new SiteLogData();
					$siteLogData->setException($e->getMessage());
					$siteLogData->setTrace($e->getTraceAsString());
					$siteLogData->setErrorCode($e->getCode());
					$this->logSiteError($siteLogData);

					$this->sendErrorReport($e);

					return $res = [
						'success' => false,
						'error'   => "Error occurred while creating your account, Please try again."
					];
				}
			}
			else
			{
				return $res = [
					'success' => false,
					'error'   => "Account already registered, Please try again logging in with social accounts."
				];
			}
		}
	}

	//It updates the  browsing data of cookies in DB when user logs in
	public function syncBrowsingCookieWithDBAfterLogin()
	{
		$userId = getAuthUserID();
		if (Cookie::has('evbbrowsingid'))
		{
			$cookieId = Cookie::get('evbbrowsingid');
			BrowsingActivity::where('cookie_id', $cookieId)
			                ->update(['user_id' => $userId]);

			// Deletes cookie
			Cookie::queue(Cookie::forget('evbbrowsingid'));
		}
	}
}
