<?php

namespace App\Http\Controllers\Wallet;

use App\Http\Controllers\Base\BaseController;
use App\Models\Util\User;
use App\Models\Wallet\Transaction;
use App\Models\Wallet\Transfer;
use App\Models\Wallet\Wallet;
use App\Models\Wallet\WalletShare;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class WalletController extends BaseController
{
	private $wallet;
	private $user;

	public function createEvibeCreditLink($userId, $amount, $loginUser)
	{
		$users = ["34585","34471","34363","33660","34592","33592","31153","33595","34065","34491","34344","34467","34369","31494","31223","34508","33751","33502","34335","34580","34047","33528","34134","33883","34578","34480","34140","33510","34216","33900","31903","34604","34478","33047","29205","30611","33504","34500","34593","34469","34048","33188","8606","30845","33420","34606","34589","31064","25998","34371","34477","33514","34596","34374","34224","34481","34343","34482","30848","33737","34142","34130","32037","34574","34135","34368","30386","34378","34244","34059","34375","33882","34496","34144","34242","34501","27910","34483","30602","34138","34332","34064","33756","33748","34470","34356","34462","33323","34465","34493","34364","33584","34456","34507","34503","34355","34603","16873","34226","32258","34583","33513","34338","34139","34499","34354","16622","34055","13673","34034","33599","25017","33736","34061","33526","34587","34598","28768","34506","32384","34333"];

		foreach ($users as $userId)
		{
			$user = User::find($userId);
			//$loginUser = Auth::user();

			if ($user && $loginUser == config("wallet.default.user_id"))
			{
				$wallet = Wallet::where("user_id", $user->id)
				                ->first();

				if (!$wallet)
				{
					$wallet = Wallet::create([
						                         "user_id" => $user->id,
						                         "slug"    => $this->randomString(10) . $user->id
					                         ]);
				}

				Transaction::create([
					                    "wallet_id"        => $wallet->id,
					                    "transaction_name" => "Evibe Cash",
					                    "type"             => "deposit",
					                    "amount"           => $amount,
					                    "confirmed"        => 1,
					                    "uuid"             => rand(11111, 99999) . "-" . rand(11, 99) . "-" . time(),
					                    "expires_on"       => Carbon::now()->addMonth(3)->timestamp
				                    ]);

				$wallet->update([
					                "balance"    => $wallet->balance + $amount,
					                "expires_on" => Carbon::now()->addMonth(3)->timestamp
				                ]);

				$url = $this->getShortenUrl(route("wallet.credit") . "?userId=" . $userId . "&tkn=" . Hash::make($user->id . $user->created_at));
				Log::info("campaign: " . $user->id . " " . $url);

			}

		}

		return "success";

		//return view("errors.404");
	}

	public function showHomePage()
	{
		if (!$this->validateUser())
		{
			// @todo: Need to implement signin + signup page
			return redirect(route("track.login"));
		}

		if (!$this->validateUserWalletInfo($this->user))
		{
			return view("errors.error");
		}

		$userTransactions = Transaction::where("wallet_id", $this->wallet->id)
		                               ->where("confirmed", "1")
		                               ->orderBy("created_at")
		                               ->get()
		                               ->take(3);

		$data = [
			"wallet"            => $this->wallet,
			"user"              => $this->user,
			"transactions"      => $userTransactions,
			"transactionsCount" => $userTransactions->count()
		];

		return $this->renderDataToViewFiles("wallet/home", $data);
	}

	public function transferEvibeCreditToWallet()
	{
		if (request("userId") > 0)
		{
			$user = User::find(request("userId"));
			if ($user && Hash::check($user->id . $user->created_at, request("tkn")))
			{
				Auth::logout();
				Auth::loginUsingId($user->id, true);

				return redirect(route("wallet.credit"));
			}
		}

		if (!$this->validateUser())
		{
			// @todo: Need to implement signin + signup page
			return redirect(route("track.login"));
		}

		if (!$this->validateUserWalletInfo($this->user))
		{
			return view("errors.error");
		}

		$evibeCreditTransactions = Transaction::where("wallet_id", $this->wallet->id)
		                                      ->where("type", "deposit")
		                                      ->orderBy("created_at")
		                                      ->get();

		$newValidTransaction = $evibeCreditTransactions->where("expires_on", ">", time())
		                                               ->where("confirmed", 0)
		                                               ->sortByDesc("created_at")
		                                               ->first();

		if (!$newValidTransaction)
		{
			$lastValidTransaction = $evibeCreditTransactions->where("confirmed", 1)
			                                                ->sortByDesc("created_at")
			                                                ->first();

			if ($lastValidTransaction)
			{
				if ($lastValidTransaction->claimed_on > 0)
				{
					return redirect(route("wallet.credit.success") . "?uuid=" . $lastValidTransaction->uuid . "&tkn=" . Hash::make($lastValidTransaction->id) . "&redirct=1");
				}
				else
				{

					$lastValidTransaction->update([
						                              "claimed_on" => time()
					                              ]);

					return redirect(route("wallet.credit.success") . "?uuid=" . $lastValidTransaction->uuid . "&tkn=" . Hash::make($lastValidTransaction->id));
				}
			}
			else
			{
				$data = [
					"user"        => $this->user,
					"wallet"      => $this->wallet,
					"transaction" => "",
				];

				return $this->renderDataToViewFiles("wallet/credit-success", $data);
				//return redirect(route("wallet.home"));
			}
		}

		$uuid = rand(11111, 99999) . "-" . rand(11, 99) . "-" . time();
		$newValidTransaction->update([
			                             "confirmed" => 1,
			                             "uuid"      => $uuid,
										 "claimed_on" => time()
		                             ]);

		$updatedBalance = ($this->wallet->balance == 0 || is_null($this->wallet->balance)) ? $newValidTransaction->amount : $this->wallet->balance + $newValidTransaction->amount;
		$updatedExpiryDate = ($this->wallet->expires_on > 0 && $this->wallet->expires_on > $newValidTransaction->expires_on) ? $this->wallet->expires_on : $newValidTransaction->expires_on;

		$this->wallet->update([
			                      "balance"    => $updatedBalance,
			                      "expires_on" => $updatedExpiryDate
		                      ]);

		// Removing all the previous credit transactions which are not redeemed
		$unUsedCreditTransactions = $evibeCreditTransactions->where("id", "!=", $newValidTransaction->id);

		if ($unUsedCreditTransactions->count() > 0)
		{
			foreach ($unUsedCreditTransactions as $unUsedCreditTransaction)
			{
				$unUsedCreditTransaction->update([
					                                 "deleted_at" => Carbon::now()
				                                 ]);
			}
		}

		return redirect(route("wallet.credit.success") . "?uuid=" . $uuid . "&tkn=" . Hash::make($newValidTransaction->id));
	}

	public function evibeCreditSuccess()
	{
		if (!$this->validateUser())
		{
			// @todo: Need to implement signin + signup page
			return redirect(route("track.login"));
		}

		if (!$this->validateUserWalletInfo($this->user))
		{
			return view("errors.error");
		}

		$uuid = request("uuid");
		$token = request("tkn");

		$transaction = Transaction::where("uuid", $uuid)
		                          ->first();

		if ($transaction && Hash::check($transaction->id, $token))
		{
			$data = [
				"user"        => $this->user,
				"wallet"      => $this->wallet,
				"transaction" => $transaction,
			];

			return $this->renderDataToViewFiles("wallet/credit-success", $data);
		}

		return redirect(route("wallet.home"));
	}

	public function userShareUrl()
	{
		if (!$this->validateUser())
		{
			// @todo: Need to implement signin + signup page
			return redirect(route("track.login"));
		}

		if (!$this->validateUserWalletInfo($this->user))
		{
			return view("errors.error");
		}

		if ($this->wallet->balance <= 0)
		{
			return response()->json([
				                        "success" => false,
				                        "error"   => "OOPS! Enough balance in not there in your wallet"
			                        ]);
		}

		$shareUrl = WalletShare::where("wallet_id", $this->wallet->id)
		                       ->where("is_active", "1")
		                       ->whereNull("is_redeemed")
		                       ->first();

		if (!$shareUrl)
		{
			$shareUrl = WalletShare::create([
				                                "wallet_id"  => $this->wallet->id,
				                                "amount"     => $this->wallet->balance,
				                                "is_active"  => 1,
				                                "expires_on" => $this->wallet->expires_on
			                                ]);

			$shareUrl->update([
				                  "url" => $this->randomString(3) . $shareUrl->id . $this->randomString(3)
			                  ]);
		}

		$token = Hash::make($shareUrl->id . $shareUrl->created_at);

		return response()->json([
			                        "success" => true,
			                        "url"     => $this->getShortenUrl(route("wallet.share", $shareUrl->url) . "?tkn=" . $token)
		                        ]);
	}

	public function friendLandingPage($uniqueId)
	{
		$walletShareUrl = WalletShare::where("url", $uniqueId)
		                             ->first();

		if ($walletShareUrl && Hash::check($walletShareUrl->id . $walletShareUrl->created_at, request("tkn")))
		{
			$wallet = Wallet::where("id", $walletShareUrl->wallet_id)
			                ->first();

			$user = User::find($wallet->user_id);

			$data = [
				"wallet"      => $wallet,
				"user"        => $user,
				"walletShare" => $walletShareUrl
			];

			return $this->renderDataToViewFiles("wallet/share", $data);
		}
		else
		{
			return view("errors.error");
		}
	}

	public function claimRewardFromFriend($uniqueId)
	{
		$walletShareUrl = WalletShare::where("url", $uniqueId)
		                             ->first();

		if ($walletShareUrl && ($walletShareUrl->is_active == 1) && Hash::check($walletShareUrl->id . $walletShareUrl->created_at, request("tkn")))
		{
			if (!$this->validateUser() || !$this->validateUserWalletInfo($this->user))
			{
				return response()->json([
					                        "success"    => false,
					                        "loginError" => true
				                        ]);
			}

			if ($walletShareUrl->wallet_id == $this->wallet->id)
			{
				return response()->json([
					                        "success" => false,
					                        "error"   => "Sorry! Can't claim your reward from your own account."
				                        ]);
			}

			if ($walletShareUrl->is_redeemed == 1)
			{
				return response()->json([
					                        "success" => false,
					                        "error"   => "OOPS! Some one already claimed the reward."
				                        ]);
			}

			$friendWallet = Wallet::find($walletShareUrl->wallet_id);
			$friendUser = User::find($friendWallet->user_id);

			// withdraw from friends wallet
			$frienduuid = rand(11111, 99999) . "-" . rand(11, 99) . "-" . time();
			$friendTransaction = Transaction::create([
				                                         "wallet_id"        => $friendWallet->id,
				                                         "transaction_name" => "Evibe Cash",
				                                         //"transaction_name" => "Gift to " . $this->user->name,
				                                         "type"             => "withdraw",
				                                         "amount"           => $walletShareUrl->amount,
				                                         "confirmed"        => 1,
				                                         "uuid"             => $frienduuid
			                                         ]);
			$friendWallet->update([
				                      "balance" => $friendWallet->balance - $walletShareUrl->amount >= 0 ? $friendWallet->balance - $walletShareUrl->amount : 0
			                      ]);

			// deposit to this wallet
			$uuid = rand(11111, 99999) . "-" . rand(11, 99) . "-" . time();
			$transaction = Transaction::create([
				                                   "wallet_id"        => $this->wallet->id,
				                                   "transaction_name" => "Evibe Cash",
				                                   //"transaction_name" => $friendUser->name . " Gift",
				                                   "type"             => "deposit",
				                                   "amount"           => $walletShareUrl->amount,
				                                   "confirmed"        => 1,
				                                   "uuid"             => $uuid,
				                                   "expires_on"       => $walletShareUrl->expires_on
			                                   ]);

			$updatedExpiryDate = ($this->wallet->expires_on > 0 && $this->wallet->expires_on > $walletShareUrl->expires_on) ? $this->wallet->expires_on : $walletShareUrl->expires_on;

			$this->wallet->update([
				                      "balance"    => $this->wallet->balance + $walletShareUrl->amount,
				                      "expires_on" => $updatedExpiryDate
			                      ]);

			// Updating the redeem status
			$walletShareUrl->update([
				                        "is_redeemed" => 1
			                        ]);

			// Update Transfer table
			Transfer::create([
				                 "from_id"     => $friendWallet->id,
				                 "to_id"       => $this->wallet->id,
				                 "status"      => "transfer",
				                 "status_last" => "transfer",
				                 "deposit_id"  => $transaction->id,
				                 "withdraw_id" => $friendTransaction->id,
				                 "amount"      => $walletShareUrl->amount,
			                 ]);

			return response()->json([
				                        "success" => true,
				                        "url"     => route("wallet.credit.success") . "?uuid=" . $uuid . "&tkn=" . Hash::make($transaction->id)
			                        ]);
		}
		else
		{
			return view("errors.error");
		}
	}

	private function validateUser()
	{
		$this->user = Auth::user();

		if (!$this->user)
		{
			return false;
		}

		return true;
	}

	/* If user doesn't have wallet, create one */
	private function validateUserWalletInfo($user)
	{
		$this->wallet = Wallet::where("user_id", $user->id)
		                      ->first();

		try
		{
			if (!$this->wallet)
			{
				$this->wallet = Wallet::create([
					                               "user_id" => $user->id,
					                               "slug"    => $this->randomString(10) . $user->id
				                               ]);
			}

			return true;
		} catch (\Exception $e)
		{
			$this->sendErrorReport($e);

			return false;
		}
	}

	private function randomString($length = 10)
	{
		return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
	}
}