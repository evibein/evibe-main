<?php

namespace App\Http\Middleware;

use App\Models\Util\User;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CustomPartnerAuth
{
	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure                 $next
	 *
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if (Auth::check())
		{
			$roleId = Auth::user()->role_id;
			$allowedRoleIds = [config('evibe.roles.planner'), config('evibe.roles.venue'), config('evibe.roles.artist')];

			$iId = (isset($request['iId']) && $request['iId']) ? $request['iId'] : null;
			$eToken = (isset($request['eToken']) && $request['eToken']) ? $request['eToken'] : null;

			if ($iId && $eToken)
			{
				// trying to open settlement of another partner in currently logged in partner's application
				$uniqueNumber = 10022014;
				$userId = $iId - $uniqueNumber;
				$user = User::find($userId);
				if (Hash::check($userId, $eToken) && $user)
				{
					// logout previous user
					auth()->logout();

					// login new user
					auth()->login($user, true);
					$roleId = $user->role_id;
				}
			}

			// 3: Planner
			// 4: Venue
			// 7: artist
			if (in_array($roleId, $allowedRoleIds))
			{
				return $next($request);
			}
			else
			{
				auth()->logout();

				return redirect(route("partner.dash.new.login"))
					->with('invalidCredential', 'This portal is only accessible for evibe.in partners, Please fill partner signup form to proceed further.');
			}
		}
		else
		{
			$iId = (isset($request['iId']) && $request['iId']) ? $request['iId'] : null;
			$eToken = (isset($request['eToken']) && $request['eToken']) ? $request['eToken'] : null;

			if ($iId && $eToken)
			{
				$uniqueNumber = 10022014;
				$userId = $iId - $uniqueNumber;
				$user = User::find($userId);
				if (Hash::check($userId, $eToken) && $user)
				{
					auth()->login($user, true);

					// limit access only to partners
					$roleId = $user->role_id;
					$allowedRoleIds = [config('evibe.roles.planner'), config('evibe.roles.venue'), config('evibe.roles.artist')];

					if (in_array($roleId, $allowedRoleIds))
					{
						return $next($request);
					}
					else
					{
						auth()->logout();

						return redirect(route("partner.dash.new.login"))
							->with('invalidCredential', 'This portal is only accessible for evibe.in partners, Please fill partner signup form to proceed further.');
					}
				}
			}

			return redirect(route("partner.dash.new.login", ['redirect' => request()->url()]));
		}
	}
}
