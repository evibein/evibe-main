<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Filesystem\Filesystem;
use RenatoMarinho\LaravelPageSpeed\Middleware\CollapseWhitespace;
use RenatoMarinho\LaravelPageSpeed\Middleware\RemoveComments;
use RenatoMarinho\LaravelPageSpeed\Middleware\RemoveQuotes;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CacheResponse
{
	/**
	 * The filesystem instance.
	 *
	 * @var \Illuminate\Filesystem\Filesystem
	 */
	protected $files;

	/**
	 * Constructor.
	 *
	 * @var \Illuminate\Filesystem\Filesystem $files
	 */
	public function __construct(Filesystem $files)
	{
		$this->files = $files;
		$this->agent = app()->make("agent");
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Symfony\Component\HttpFoundation\Request $request
	 * @param  \Closure                                  $next
	 *
	 * @return mixed
	 */
	public function handle(Request $request, Closure $next)
	{
		if ($this->agent->isMobile() && !($this->agent->isTablet()))
		{
			$devicePath = '_m';
		}
		else
		{
			$devicePath = '_d';
		}

		$segments = explode('/', ltrim($request->getPathInfo(), '/'));
		$file = implode('_', $segments) . $devicePath . '.html';

		array_pop($segments);
		$path = '/page-cache/' . implode('/', $segments);

		if (file_exists(public_path() . $path . "/" . $file))
		{
			return response()->file(public_path() . $path . "/" . $file);
		}
		else
		{
			$response = $next($request);

			$html = $response->getContent();

			$removeWhiteSpace = new CollapseWhitespace();
			$removeComments = new RemoveComments();
			$removeQuotes = new RemoveQuotes();

			$newContent = $removeWhiteSpace->apply($html);
			$newContent = $removeComments->apply($newContent);
			$newContent = $removeQuotes->apply($newContent);
			$response->setContent($newContent);

			$path = public_path() . $path;
			$files = (new Filesystem());
			$files->makeDirectory($path, 0777, true, true);
			$files->put($path . "/" . $file, $newContent, true);

			return $response;
		}
	}

	/**
	 * Determines whether the given request/response pair should be cached.
	 *
	 * @param  \Symfony\Component\HttpFoundation\Request  $request
	 * @param  \Symfony\Component\HttpFoundation\Response $response
	 *
	 * @return bool
	 */
	protected function shouldCache(Request $request, Response $response)
	{
		return $request->isMethod('GET') && $response->getStatusCode() == 200;
	}

	protected function join(array $paths)
	{
		$trimmed = array_map(function ($path) {
			return trim($path, '/');
		}, $paths);

		return $this->matchRelativity(
			$paths[0], implode('/', array_filter($trimmed))
		);
	}

	protected function matchRelativity($source, $target)
	{
		return $source[0] == '/' ? '/' . $target : $target;
	}
}