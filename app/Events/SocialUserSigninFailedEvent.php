<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class SocialUserSigninFailedEvent
{
	use SerializesModels;

	public $providerName;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct($providerName)
	{
		$this->providerName = $providerName;
	}
}
