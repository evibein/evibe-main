<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class SocialUserSigninSuccessEvent
{
	use SerializesModels;

	public $provider;
	public $providerUser;
	public $socialAccount;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct($provider, $providerUser, $socialAccount)
	{
		$this->provider = $provider;
		$this->providerUser = $providerUser;
		$this->socialAccount = $socialAccount;
	}
}
