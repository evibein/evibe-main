<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class ProcessShortlistEvent
{
	use SerializesModels;

	public $shortlistData;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct($shortlistData)
	{
		$this->shortlistData = $shortlistData;
	}
}
