<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class AddShortlistOptionEvent
{
	use SerializesModels;

	public $shortlistOptionId;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct($shortlistOptionId)
	{
		$this->shortlistOptionId = $shortlistOptionId;
	}
}
