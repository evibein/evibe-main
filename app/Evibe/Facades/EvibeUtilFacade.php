<?php

/**
 * @author Anji <anji@evibe.in>
 * @since  22 Jan 2015
 */

namespace Evibe\Facades;

use Illuminate\Support\Facades\Facade;

class EvibeUtilFacade extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'evibe_util';
	}
}