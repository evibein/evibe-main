<?php

namespace App\Evibe\Validator;

use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Validator;

class EvibeValidator extends Validator
{
	public function validatePhone($attribute, $value, $parameters)
	{
		if (is_numeric($value) && strlen($value) == 10)
		{
			return true;
		}

		return false;
	}

	public function validateTime($attribute, $value, $parameters)
	{
		if (preg_match('/^(00|0[0-9]|1[012]):[0-5][0-9] ?((a|p)m|(A|P)M)$/', $value))
		{
			return true;
		}

		return false;
	}

	public function validateMinAs($attribute, $value, $parameters, $validator)
	{
		$validatorData = $validator->getData();
		if ($value > $validatorData[$parameters[0]])
		{
			return true;
		}

		return false;
	}

	public function validateIsImage($attribute, $value, $parameters)
	{
		$allowedExts = ["gif", "jpeg", "jpg", "png"];
		$extension = $value->getClientOriginalExtension();
		$mime = request()->file('file')->getMimeType();

		if ((($mime == "image/gif") || ($mime == "image/jpeg")
				|| ($mime == "image/jpg") || ($mime == "image/pjpeg")
				|| ($mime == "image/x-png") || ($mime == "image/png"))
			&& in_array(strtolower($extension), $allowedExts)
		)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function validateVideoUrl($attribute, $value, $parameters)
	{
		$videoID = $parameters[0];
		$headers = get_headers('http://gdata.youtube.com/feeds/api/videos/' . $videoID);

		return strpos($headers[0], '200');
	}

	public function validateYear($attribute, $value, $parameters)
	{
		return $value <= date('Y') && $value > 1920;
	}
}