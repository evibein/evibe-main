<?php

namespace Evibe\Utilities;

use App;
use App\Models\Package\Package;
use App\Models\Types\TypeTag;
use App\Models\Types\TypeEvent;
use App\Models\Util\SiteErrorLog;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpKernel\Exception\HttpException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class EvibeUtil
{
	/**
	 * Mapping array
	 *
	 * @var array $occasionUrlRouteMapping
	 */
	public $occasionUrlRouteMapping;

	public function __construct()
	{
		$this->occasionUrlRouteMapping = [];

		$allEvents = TypeEvent::select("id", "name", "url")->get();

		// initialise with birthday routes
		foreach ($allEvents as $event)
		{
			$this->occasionUrlRouteMapping[$event->url] = 'city.occasion.birthdays.';
		}

		// override required
		$routeMappingsByEventIds = [
			config('evibe.occasion.bachelor.id')          => 'city.occasion.bachelor.',
			config('evibe.occasion.pre-post.id')          => 'city.occasion.pre-post.',
			config('evibe.occasion.surprises.id')         => 'city.occasion.surprises.',
			config('evibe.occasion.house-warming.id')     => 'city.occasion.house-warming.',
			4                                             => 'city.occasion.pre-post.', // receptions
			7                                             => 'city.occasion.pre-post.', // sangeeths
			9                                             => 'city.occasion.pre-post.', // wedding
			12                                            => 'city.occasion.pre-post.', // engagement
			config('evibe.occasion.new-year-2018.id', 29) => 'city.occasion.bachelor.', // new year
		];

		// override defaults
		foreach ($allEvents as $event)
		{
			if (isset($routeMappingsByEventIds[$event->id]))
			{
				$this->occasionUrlRouteMapping[$event->url] = $routeMappingsByEventIds[$event->id];
			}
		}
	}

	/**
	 * formatPrice is used for both price and views count
	 *
	 * @param int $price price value / views count
	 *
	 * @return string formatted according to Indian number system
	 */
	public function formatPrice($price)
	{
		$explrestUnits = "";
		$price = (int)$price;

		if (strlen($price) > 3)
		{
			if (strlen($price) > 5)
			{
				$thecash = round(($price / 100000), 2) . 'L';
			}
			else
			{
				$lastthree = substr($price, strlen($price) - 3, strlen($price));
				$restunits = substr($price, 0, strlen($price) - 3); // extracts the last three digits
				$restunits = (strlen($restunits) % 2 == 1) ? "0" . $restunits : $restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
				$expunit = str_split($restunits, 2);

				for ($i = 0; $i < sizeof($expunit); $i++)
				{
					// creates each of the 2's group and adds a comma to the end
					if ($i == 0)
					{
						$explrestUnits .= (int)$expunit[$i] . ","; // if is first value , convert into integer
					}
					else
					{
						$explrestUnits .= $expunit[$i] . ",";
					}
				}

				$thecash = $explrestUnits . $lastthree;
			}
		}
		else
		{
			$thecash = $price;
		}

		return $thecash; // writes the final format where $currency is the currency symbol.
	}

	public function formatPriceWithoutChar($price)
	{
		$explrestUnits = "";

		if (strlen($price) > 3)
		{
			$lastthree = substr($price, strlen($price) - 3, strlen($price));
			$restunits = substr($price, 0, strlen($price) - 3); // extracts the last three digits
			$restunits = (strlen($restunits) % 2 == 1) ? "0" . $restunits : $restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
			$expunit = str_split($restunits, 2);

			for ($i = 0; $i < sizeof($expunit); $i++)
			{
				// creates each of the 2's group and adds a comma to the end
				if ($i == 0)
				{
					$explrestUnits .= (int)$expunit[$i] . ","; // if is first value , convert into integer
				}
				else
				{
					$explrestUnits .= $expunit[$i] . ",";
				}
			}

			$thecash = $explrestUnits . $lastthree;

		}
		else
		{
			$thecash = $price;
		}

		return $thecash; // writes the final format where $currency is the currency symbol.
	}

	public function getRoundedTokenAmount($totalBookingAmount)
	{
		$tokenAmount = (int)$totalBookingAmount * config('evibe.ticket.advance.percentage') * 0.01;
		$balanceAmount = $totalBookingAmount - $tokenAmount;

		if (($balanceAmount % 100) != 0 && $balanceAmount > 100)
		{
			$round = (int)($balanceAmount % 100);
			$tokenAmount = (int)($totalBookingAmount) - (int)($balanceAmount) + $round;
		}

		return $tokenAmount;
	}

	public function getFormattedRoundedTokenAmount($totalBookingAmount)
	{
		$tokenAmount = $this->getRoundedTokenAmount($totalBookingAmount);

		return $this->formatPrice($tokenAmount);
	}

	// Convert the number into 1k instead of 1,000
	public function formatNumber($number)
	{
		return $number > 999 ? round(($number / 1000), 1) . 'k' : $number;
	}

	public function getShortUrl($longUrl)
	{
		$shortUrl = $longUrl;

		if ($longUrl && (strpos($longUrl, "goo.gl") === false) && (strpos($longUrl, "evib.es") === false))
		{
			try
			{
				$shortUrl = $this->getGooGlShortLink($longUrl);
				//$shortUrl = $this->getEvibesShortLink($longUrl);

			} catch (\Exception $e)
			{
				Log::error("ERROR:: While using the bitly shorter for link " . $longUrl);
			}
		}

		return $shortUrl;
	}

	private function getEvibesShortLink($longUrl)
	{
		// longUrl definitely exists at this point
		$shortUrl = $longUrl;

		if ((strpos($longUrl, "bit.ly") === false) && (strpos($longUrl, "evib.es") === false))
		{
			$url = "http://evib.es/api/v2/action/shorten";
			$method = "POST";
			$accessToken = "";
			$jsonData = [
				'url'           => $longUrl,
				'key'           => config("evibe.evibes.access_token"),
				'custom_ending' => '',
				'is_secret'     => false,
				'response_type' => 'json'
			];

			try
			{
				$client = new Client();
				$res = $client->request($method, $url, [
					'headers' => [
						'access-token' => $accessToken
					],
					'json'    => $jsonData,
				]);

				$res = $res->getBody();
				$res = \GuzzleHttp\json_decode($res, true);
				if (isset($res["action"]) && $res["action"] == "shorten" && isset($res["result"]) && $res["result"] != $longUrl)
				{
					$shortUrl = $res["result"];
				}

			} catch (ClientException $e)
			{
				$apiResponse = $e->getResponse()->getBody(true);
				$apiResponse = \GuzzleHttp\json_decode($apiResponse);
				$res['error'] = $apiResponse->errorMessage;

				$this->saveError([
					                 'fullUrl' => request()->fullUrl(),
					                 'message' => 'Error occurred in Base controller while doing guzzle request',
					                 'code'    => 0,
					                 'details' => $res['error']
				                 ]);

				return false;
			}
		}

		return $shortUrl;
	}

	private function getGooGlShortLink($longUrl)
	{
		// longUrl definitely exists at this point
		$shortUrl = $longUrl;

		if ((strpos($longUrl, "bit.ly") === false) && (strpos($longUrl, "evib.es") === false))
		{
			try
			{
				/* @see: goo.gl to bit.ly
				$base = new App\Http\Controllers\Base\BaseController();
				$baseUrl = "https://www.googleapis.com/urlshortener/v1/url?key=" . config('evibe.google.shortener_key');

				$postData = ['longUrl' => $longUrl];
				$jsonData = json_encode($postData);

				$curlObj = curl_init();

				curl_setopt($curlObj, CURLOPT_URL, $baseUrl);
				curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($curlObj, CURLOPT_HEADER, 0);
				curl_setopt($curlObj, CURLOPT_HTTPHEADER, ['Content-type:application/json']);
				curl_setopt($curlObj, CURLOPT_POST, 1);
				curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);

				$response = curl_exec($curlObj);

				// Change the response json string to object
				curl_close($curlObj);

				$shortUrl = \GuzzleHttp\json_decode($response)->id;
				 */

				$base = new App\Http\Controllers\Base\BaseController();
				$accessToken = config('evibe.bitly.access_token');
				$switchTime = Carbon::createFromTimestamp(time())->startOfMonth()->addDays(15)->startOfDay()->toDateTimeString();
				if(time() > strtotime($switchTime))
				{
					$accessToken = config('evibe.bitly.alt_access_token');
				}
				$encodedUrl = urlencode($longUrl);
				// default format is json
				$baseUrl = "https://api-ssl.bitly.com/v3/shorten?access_token=" . $accessToken . "&longUrl=" . $encodedUrl;

				$curlObj = curl_init();

				curl_setopt($curlObj, CURLOPT_URL, $baseUrl);
				curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);

				$response = curl_exec($curlObj);

				curl_close($curlObj);

				// response is in json format
				// normal decode without 2nd var as 'true' will return in stdObjectClass
				$response = json_decode($response, true);
				if (isset($response['status_txt']) && $response['status_txt'] == "OK")
				{
					$shortUrl = isset($response['data']['url']) && $response['data']['url'] ? $response['data']['url'] : $longUrl;
				}
				else
				{
					$base->sendNonExceptionErrorReport([
						                                   'code'      => config('evibe.error_code.create_function'),
						                                   'url'       => 'Bitly Url Shortener',
						                                   'method'    => 'GET',
						                                   'message'   => '[BaseController.php - ' . __LINE__ . '] ' . 'Unable to create short url for [longUrl: ' . $longUrl . ']',
						                                   'exception' => '[BaseController.php - ' . __LINE__ . '] ' . 'Unable to create short url for [longUrl: ' . $longUrl . ']',
						                                   'trace'     => print_r($response, true),
						                                   'details'   => print_r($response, true)
					                                   ]);
				}

			} catch (\Exception $exception)
			{
				$base->sendErrorReport($exception);
			}
		}

		return $shortUrl;
	}

	public function getEventUrl($eventId)
	{
		$occasion = App\Models\Types\TypeEvent::find($eventId);

		return $occasion->url;
	}

	public function getTruncateString($string)
	{
		return strlen($string) <= 35 ? $string : substr($string, 0, 36) . "...";
	}

	public function getVenueDealFilterLocation($deals)
	{
		$areasBag = [];
		$urlsBag = [];
		$deals = $deals->groupBy('planner_package.id')->get();

		foreach ($deals as $deal)
		{
			$provider = $deal->provider;

			if (!$provider || !$provider->is_live || !($area = $provider->area))
			{
				continue;
			}

			if (!array_key_exists($area->id, $areasBag))
			{
				$areasBag[$area->id] = [
					'id'    => $area->id,
					'count' => 0,
					'name'  => $area->name,
					'url'   => $area->url
				];

				$urlsBag[] = $area->url;
			}

			$areasBag[$area->id]['count'] += 1;
		}

		$sortableArea = collect($areasBag)->sortBy('count')
		                                  ->reverse()
		                                  ->toArray();

		$allArea = [
			'all'  => $sortableArea,
			'urls' => $urlsBag
		];

		return $allArea;
	}

	public function getPackagesLocationDataForFilters($cityId, $eventId)
	{
		// @see: changed method to fetch from all packages rather than only sent packages. Problem with CLDs
		//$packagesData = $packages->pluck('map_type_id', 'map_id')->toArray();

		$packages = Package::where('city_id', $cityId)
		                   ->where('event_id', $eventId)
		                   ->where('is_live', 1)
		                   ->whereNull('deleted_at')
		                   ->get();

		$plannerIds = $packages->where('map_type_id', '!=', config('evibe.ticket.type.venue'))->pluck('map_id')->toArray();
		//$plannerPackages = $packages->where('map_type_id', '!=', config('evibe.ticket.type.venue'))->toArray();
		//$venuePackages = $packages->where('map_type_id', config('evibe.ticket.type.venue'))->toArray();

		//$plannerIds = array_filter($packagesData, function ($var) {
		//	return $var != config("evibe.ticket.type.venue");
		//});
		//$venueIds = array_filter($packagesData, function ($var) {
		//	return $var == config("evibe.ticket.type.venue");
		//});

		$plannerPartnerAreaIds = App\Models\Vendor\Vendor::whereIn("id", array_unique($plannerIds))
		                                                 ->where("is_live", 1)
		                                                 ->pluck("area_id")
		                                                 ->toArray();

		//$venuePartnerAreaIds = App\Models\Venue\Venue::whereIn("id", array_keys($venueIds))
		//                                             ->where("is_live", 1)
		//                                             ->pluck("area_id")
		//                                             ->toArray();

		$venuePartnerAreaIds = $packages->where('map_type_id', config("evibe.ticket.type.venue"))
		                                ->pluck("area_id")
		                                ->toArray();

		$areas = App\Models\Util\Area::select("id", DB::raw('1 as count'), "name", "url")
		                             ->whereIn("id", array_merge($plannerPartnerAreaIds, $venuePartnerAreaIds))
		                             ->get();

		$allArea = [
			'all'  => $areas->keyBy("id")->toArray(),
			'urls' => $areas->pluck("url")->toArray()
		];

		return $allArea;
	}

	public function getBirthdayFooterLink($topLocationLimit = null, $topCatLimit = null)
	{
		$eventId = config('evibe.occasion.kids_birthdays.id');
		$pageId = config('evibe.ticket.type.venue-deals');
		$showMoreLocation = true;
		$showMoreCategory = true;

		$venueDeals = Package::with('tags', 'provider', 'provider.city')
		                     ->joinTags()
		                     ->forEvent($eventId)
		                     ->forPage($pageId)
		                     ->isLive()
		                     ->forCity()
		                     ->select('planner_package.*');

		$locations = $this->getVenueDealFilterLocation($venueDeals);
		$locations = $locations['all'];

		if ($topLocationLimit && is_int($topLocationLimit))
		{
			$totalLocationCount = count($locations);
			$locations = collect($locations)->take($topLocationLimit);

			if ($totalLocationCount <= $topLocationLimit)
			{
				$showMoreLocation = false;
			}
		}

		$allCategories = TypeTag::where(['map_type_id' => $pageId, 'type_event' => $eventId, 'is_filter' => 1])
		                        ->select('identifier', 'url');

		if ($topCatLimit && is_int($topCatLimit))
		{
			$totalCategoryCount = $allCategories->count();
			$allCategories = $allCategories->take($topCatLimit);

			if ($totalCategoryCount <= $topCatLimit)
			{
				$showMoreCategory = false;
			}
		}

		$allCategories = $allCategories->get();

		return [
			'locations'  => [
				'all'      => $locations,
				'showMore' => $showMoreLocation
			],
			'categories' => [
				'all'      => $allCategories,
				'showMore' => $showMoreCategory
			]
		];

	}

	public function sendErrorReportToTeam($e, $sendEmail = true)
	{
		$code = 'Error';
		if ($e instanceof HttpException)
		{
			$code = $e->getStatusCode();
		}

		if (config('evibe.mail_errors'))
		{
			$data = [
				'fullUrl' => request()->fullUrl(),
				'code'    => $code,
				'message' => $e->getMessage(),
				'details' => $e->getTraceAsString()
			];

			if ($sendEmail)
			{
				$emailData = [
					'code'    => $code,
					'url'     => $data['fullUrl'],
					'method'  => request()->method(),
					'message' => $data['message'],
					'trace'   => $data['details']
				];

				$emailData['sub'] = '[Dash Error] Code: ' . $emailData['code'] . ' - ' . date('F j, Y, g:i a');

				Mail::send('emails.errors.generic-error', ['data' => $data], function ($m) use ($emailData) {
					$m->from(config('evibe.contact.company.email'), 'Evibe Errors')
					  ->to(config('evibe.contact.tech.group'))
					  ->replyTo(config('evibe.contact.tech.group'))
					  ->subject($emailData['sub']);
				});
			}
			$this->saveError($data);
		}
	}

	protected function saveError($data)
	{
		SiteErrorLog::create([
			                     'project_id' => config('evibe.project_id'),
			                     'url'        => isset($data['fullUrl']) && $data['fullUrl'] ? $data['fullUrl'] : $data['url'],
			                     'exception'  => $data['message'],
			                     'code'       => $data['code'],
			                     'details'    => $data['details']
		                     ]);
	}

	public function sendNonExceptionErrorReportToTeam($data, $sendEmail = true)
	{
		$errorData = [
			'code'       => isset($data['code']) && $data['code'] ? $data['code'] : 0,
			'url'        => isset($data['url']) && $data['url'] ? $data['url'] : config('evibe.host'),
			'method'     => isset($data['method']) && $data['method'] ? $data['method'] : request()->method(),
			'message'    => isset($data['message']) && $data['message'] ? $data['message'] : 'Some error occurred',
			'trace'      => isset($data['trace']) && $data['trace'] ? $data['trace'] : null,
			'project_id' => isset($data['projectId']) && $data['projectId'] ? $data['projectId'] : config('evibe.project_id'),
			'exception'  => isset($data['exception']) && $data['exception'] ? $data['exception'] : 'Unknown exception',
			'details'    => isset($data['details']) && $data['details'] ? $data['details'] : 'No clear details'
		];
		if ($sendEmail)
		{
			$errorData['sub'] = '[Main Error] Code: ' . $errorData['code'] . ' - ' . date('F j, Y, g:i a');

			Mail::send('emails.errors.generic-error', ['data' => $data], function ($m) use ($errorData) {
				$m->from(config('evibe.contact.company.email'), 'Evibe Errors')
				  ->to(config('evibe.contact.tech.group'))
				  ->replyTo(config('evibe.contact.tech.group'))
				  ->subject($errorData['sub']);
			});
		}
		$this->saveError($errorData);
	}
}