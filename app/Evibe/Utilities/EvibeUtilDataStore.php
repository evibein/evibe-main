<?php

namespace Evibe\Utilities;

use Illuminate\Support\Facades\Log;

class EvibeUtilDataStore
{
	public $cities;
	public $occasions;

	public function __construct()
	{
		// set defaults
		$this->cities = false;
		$this->occasions = false;
	}

	public function getAllCities()
	{
		if (!$this->cities)
		{
			$this->cities = getAllCities();
		}

		return $this->cities;
	}

	public function getAllOccasions()
	{
		if (!$this->occasions)
		{
			$this->occasions = getAllOccasions();
		}

		return $this->occasions;
	}

	public function getCityUrlFromId($cityId)
	{
		$allCities = $this->getAllCities();
		$city = $allCities->where("id", $cityId)->first();
		if (!$city)
		{
			Log::error(":: EvibeUtilDataStore :: Could not find city with id $cityId");

			return null;
		}

		return $city->url;
	}

	public function getOccasionUrlFromId($occasionId)
	{
		$allOccasions = $this->getAllOccasions();
		$occasion = $allOccasions->where("id", $occasionId)->first();
		if (!$occasion)
		{
			Log::error(":: EvibeUtilDataStore :: Could not find occasion with id $occasionId");

			return null;
		}

		return $occasion->url;
	}
}