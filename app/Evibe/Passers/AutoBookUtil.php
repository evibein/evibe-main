<?php

namespace Evibe\Passers;

class AutoBookUtil
{
	private $ticketId;
	private $token;
	private $hashedStr;
	private $totalBookingAmount;
	private $tokenAmount;
	private $typeToken = 1; // default for mobile number
	private $partnerId = null;
	private $partnerTypeId = null;


	/**
	 * @return mixed
	 */
	public function getPartnerId()
	{
		return $this->partnerId;
	}

	/**
	 * @param mixed $partnerId
	 */
	public function setPartnerId($partnerId)
	{
		$this->partnerId = $partnerId;
	}

	/**
	 * @return mixed
	 */
	public function getPartnerTypeId()
	{
		return $this->partnerTypeId;
	}

	/**
	 * @param mixed $partnerTypeId
	 */
	public function setPartnerTypeId($partnerTypeId)
	{
		$this->partnerTypeId = $partnerTypeId;
	}

	public function getTypeToken()
	{
		return $this->typeToken;
	}

	public function setTypeToken($typeToken)
	{
		$this->typeToken = $typeToken;
	}

	public function getTotalBookingAmount()
	{
		return $this->totalBookingAmount;
	}

	public function setTotalBookingAmount($totalBookingAmount)
	{
		$this->totalBookingAmount = $totalBookingAmount;
	}

	public function getTokenAmount()
	{
		return $this->tokenAmount;
	}

	public function setTokenAmount($tokenAmount)
	{
		$this->tokenAmount = $tokenAmount;
	}

	public function getTicketId()
	{
		return $this->ticketId;
	}

	public function setTicketId($ticketId)
	{
		$this->ticketId = $ticketId;
	}

	public function getToken()
	{
		return $this->token;
	}

	public function setToken($token)
	{
		$this->token = $token;
	}

	public function getHashedStr()
	{
		return $this->hashedStr;
	}

	public function setHashedStr($hashedStr)
	{
		$this->hashedStr = $hashedStr;
	}
}