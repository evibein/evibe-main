<?php

namespace Evibe\Passers;

class PaySuccessUpdateTicket
{
	private $isFromDash = false;
	private $statusId;
	private $handlerId;
	private $successEmailType;
	private $tData;
	private $ticketId;

	/**
	 * @return boolean
	 */
	public function getIsFromDash()
	{
		return $this->isFromDash;
	}

	/**
	 * @param boolean $isFromDash
	 */
	public function setIsFromDash($isFromDash)
	{
		$this->isFromDash = $isFromDash;
	}

	/**
	 * @return mixed
	 */
	public function getStatusId()
	{
		return $this->statusId;
	}

	/**
	 * @param mixed $statusId
	 */
	public function setStatusId($statusId)
	{
		$this->statusId = $statusId;
	}

	/**
	 * @return mixed
	 */
	public function getHandlerId()
	{
		return $this->handlerId;
	}

	/**
	 * @param mixed $handlerId
	 */
	public function setHandlerId($handlerId)
	{
		$this->handlerId = $handlerId;
	}

	/**
	 * @return mixed
	 */
	public function getSuccessEmailType()
	{
		return $this->successEmailType;
	}

	/**
	 * @param mixed $successEmailType
	 */
	public function setSuccessEmailType($successEmailType)
	{
		$this->successEmailType = $successEmailType;
	}

	/**
	 * @return mixed
	 */
	public function getTicketUpdateData()
	{
		return $this->tData;
	}

	/**
	 * @param mixed $tData
	 */
	public function setTicketUpdateData($tData)
	{
		$this->tData = $tData;
	}

	public function getTicketNotificationId()
	{
		return $this->ticketId;
	}

	public function setTicketNotificationId($ticketId)
	{
		$this->ticketId = $ticketId;
	}

}