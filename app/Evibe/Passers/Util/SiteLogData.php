<?php
/**
 * User: anji
 * Date: 27/06/17
 * Time: 12:14 PM
 */

namespace Evibe\Passers\Util;

class SiteLogData
{
	private $url;
	private $exception;
	private $errorCode;
	private $trace;
	private $projectId;

	public function __construct()
	{
		// initiate
		$this->setProjectId(config('evibe.project_id'))
		     ->setErrorCode("default")
		     ->setUrl("default/url")
		     ->setException("default")
		     ->setTrace("default trace");
	}

	/**
	 * @return mixed
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * @param mixed $url
	 *
	 * @return SiteLogData
	 */
	public function setUrl($url)
	{
		$this->url = $url;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getException()
	{
		return $this->exception;
	}

	/**
	 * @param mixed $exception
	 *
	 * @return SiteLogData
	 */
	public function setException($exception)
	{
		$this->exception = $exception;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getErrorCode()
	{
		return $this->errorCode;
	}

	/**
	 * @param mixed $errorCode
	 *
	 * @return SiteLogData
	 */
	public function setErrorCode($errorCode)
	{
		$this->errorCode = $errorCode;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getTrace()
	{
		return $this->trace;
	}

	/**
	 * @param mixed $trace
	 *
	 * @return SiteLogData
	 */
	public function setTrace($trace)
	{
		$this->trace = $trace;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getProjectId()
	{
		return $this->projectId;
	}

	/**
	 * @param mixed $projectId
	 *
	 * @return SiteLogData
	 */
	public function setProjectId($projectId)
	{
		$this->projectId = $projectId;

		return $this;
	}
}