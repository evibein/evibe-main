<?php

namespace Evibe\Passers\Util;

class LogAjax
{
	private $url;
	private $payload;
	private $projectId;

	public function __construct()
	{
		$this->setUrl("default/url")
		     ->setPayload("default payload")
			->setProjectId(config('evibe.project_id'));
	}

	/**
	 * @param mixed $url
	 *
	 * @return LogAjax
	 */
	public function setUrl($url)
	{
		$this->url = $url;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * @param mixed $projectId
	 *
	 * @return LogAjax
	 */
	public function setProjectId($projectId)
	{
		$this->projectId = $projectId;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getProjectId()
	{
		return $this->projectId;
	}

	/**
	 * @param mixed $payload
	 *
	 * @return LogAjax
	 */
	public function setPayload($payload)
	{
		$this->payload = $payload;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPayload()
	{
		return $this->payload;
	}
}