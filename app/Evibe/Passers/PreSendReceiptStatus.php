<?php

namespace Evibe\Passers;

/**
 * Object that is used to check status & pass data before sending
 * receipts for auto bookings & normal bookings
 */
class PreSendReceiptStatus
{
	private $success;
	private $errorMessage;
	private $bookings;

	public function setSuccess($success)
	{
		$this->success = $success;
	}

	public function getSuccess()
	{
		return $this->success;
	}

	public function setErrorMessage($errorMessage)
	{
		return $this->errorMessage = $errorMessage;
	}

	public function getErrorMessage()
	{
		return $this->errorMessage;
	}

	public function setBookings($bookings)
	{
		$this->bookings = $bookings;
	}

	public function getBookings()
	{
		return $this->bookings;
	}
}