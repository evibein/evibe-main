<?php

namespace Evibe\Passers;

/**
 * Class PriceCheck
 *
 * @package Evibe\Passers
 */
class PriceCheck
{
	/**
	 * @var
	 */
	private $optionId;
	private $optionTypeId;
	private $providerPinCode;
	private $partyPinCode;
	private $distFree = 20;
	private $distMax = 40; // default to avoid division by 0
	private $transMax = 0;
	private $transMin = 0;
	private $baseBookingAmount = 0;

	/**
	 * @return mixed
	 */
	public function getOptionId()
	{
		return $this->optionId;
	}

	/**
	 * @param mixed $optionId
	 */
	public function setOptionId($optionId)
	{
		$this->optionId = $optionId;
	}

	/**
	 * @return mixed
	 */
	public function getOptionTypeId()
	{
		return $this->optionTypeId;
	}

	/**
	 * @param mixed $optionTypeId
	 */
	public function setOptionTypeId($optionTypeId)
	{
		$this->optionTypeId = $optionTypeId;
	}

	/**
	 * @return mixed
	 */
	public function getPartyPinCode()
	{
		return $this->partyPinCode;
	}

	/**
	 * @param mixed $partyPinCode
	 */
	public function setPartyPinCode($partyPinCode)
	{
		$this->partyPinCode = $partyPinCode;
	}

	/**
	 * @return mixed
	 */
	public function getDistFree()
	{
		return $this->distFree;
	}

	/**
	 * @param mixed $distFree
	 */
	public function setDistFree($distFree)
	{
		$this->distFree = $distFree;
	}

	/**
	 * @return int
	 */
	public function getDistMax()
	{
		return $this->distMax;
	}

	/**
	 * @param int $distMax
	 */
	public function setDistMax($distMax)
	{
		$this->distMax = $distMax;
	}

	/**
	 * @return mixed
	 */
	public function getTransMax()
	{
		return $this->transMax;
	}

	/**
	 * @param mixed $transMax
	 */
	public function setTransMax($transMax)
	{
		$this->transMax = $transMax;
	}

	/**
	 * @return mixed
	 */
	public function getTransMin()
	{
		return $this->transMin;
	}

	/**
	 * @param mixed $transMin
	 */
	public function setTransMin($transMin)
	{
		$this->transMin = $transMin;
	}

	/**
	 * @return mixed
	 */
	public function getBaseBookingAmount()
	{
		return $this->baseBookingAmount;
	}

	/**
	 * @param mixed $baseBookingAmount
	 */
	public function setBaseBookingAmount($baseBookingAmount)
	{
		$this->baseBookingAmount = $baseBookingAmount;
	}

	/**
	 * @return mixed
	 */
	public function getProviderPinCode()
	{
		return $this->providerPinCode;
	}

	/**
	 * @param mixed $providerPinCode
	 */
	public function setProviderPinCode($providerPinCode)
	{
		$this->providerPinCode = $providerPinCode;
	}
}