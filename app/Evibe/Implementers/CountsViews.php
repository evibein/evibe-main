<?php

namespace Evibe\Implementers;

interface CountsViews
{
	public function incrementViews();
}