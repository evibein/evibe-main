<?php

namespace App\Evibe\Implementers;

interface HandlesPayments
{
	public function showCheckoutPage();

	public function processPayment($ticketId);

	public function onPaymentSuccess($ticketId);
}