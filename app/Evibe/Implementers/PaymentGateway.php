<?php

namespace Evibe\Implementers;

interface PaymentGateway
{
	public function onPaymentInit($ticketId);

	public function onPaymentReceived();
}