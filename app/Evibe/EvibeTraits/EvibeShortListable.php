<?php

namespace App\Evibe\EvibeTraits;

trait EvibeShortListable
{
	// to be over written by the Models
	// input $cityUrl, $occasionUrl, $params, $shortlistParams
	public function getShortListableCardInfo($cityUrl, $occasionUrl, $params, $shortlistParams)
	{
		$info = [
			'title'           => '',
			'price'           => '',
			'profileImg'      => '',
			'fullUrl'         => '',
			'additionalLine'  => '',
			'priceMax'        => '',
			'worth'           => '',
			'rangeInfo'       => '',
			'pbPriceMin'      => '',
			'pbPriceMax'      => '',
			'pbPriceWorthMin' => '',
			'pbPriceWorthMax' => ''
		];

		return $info;
	}

	// @override: to be override by the model
	public function getPartyBagPriceMin($params = [])
	{
		return "";
	}

	// @override: to be override by the model
	public function getPartyBagPriceMax($params = [])
	{
		return "";
	}

	// @override: to be override by the model
	public function getPartyBagPriceWorthMin($params = [])
	{
		return "";
	}

	// @override: to be override by the model
	public function getPartyBagPriceWorthMax($params = [])
	{
		return "";
	}
}