<?php

namespace App\Evibe\EvibeTraits;

use App\Models\Types\TypeEvent;

use Evibe\Utilities\EvibeUtil;
use Laravel\Scout\Searchable;

trait EvibeSearchable
{
	use Searchable;

	/**
	 * To use methods of EvibeUtil
	 *
	 * @var EvibeUtil $evibeUtil
	 */
	public $evibeUtil;

	public function __construct()
	{
		parent::__construct();

		$this->evibeUtil = app()->make("evibe_util");
	}

	// @override: to be override by the model
	public function getSearchableCardInfo()
	{
		$info = [
			'id'             => '',
			'title'          => '',
			'price'          => '',
			'profileImg'     => '',
			'fullUrl'        => '',
			'additionalLine' => '',
			'priceMax'       => '',
			'worth'          => '',
			'rangeInfo'      => '',
		];

		return $info;
	}

	// @override: to be override by the model
	public function event()
	{
		// @see: change this as per the model. We are guessing the model
		// belongs to one event.
		return $this->belongsTo(TypeEvent::class, "event_id");
	}

	// @override: to be override by the model
	public function getFullLinkForSearch($cityUrl, $occasionUrl, $params = [])
	{
		return "";
	}

	// @override: to be override by the model
	public function forSearchIsLive()
	{
		return ($this->is_live == 1);
	}

	// @override: to be override by the model
	public function forSearchPrice()
	{
		return ($this->price ? $this->price : 0);
	}

	// check if the route exists
	public function getLinkByRouteName($routeName, $params)
	{
		$link = false;

		try
		{
			$link = route($routeName, $params);
		} catch (\Exception $exception)
		{

		}

		return $link;
	}

	// @override: to be override by the model
	public function forSearchCity()
	{
		return "";
	}

	// @override: to be override by the model
	public function scopeForSearchSelectCols($query)
	{
		return $query->select("id", "name", "info", "url");
	}
}